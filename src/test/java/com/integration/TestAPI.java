package com.integration;



import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.web.client.RestTemplate;


import com.emanager.webservice.Application;
import com.emanager.webservice.JsonRespAccountsVO;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
//@SpringApplicationConfiguration(classes = Application.class)
@ImportResource("classpath:applicationContext*.xml")

public class TestAPI {

	private TestRestTemplate template = new TestRestTemplate();
	RestTemplate restTemplate = new RestTemplate();
    
    
	

	@Test
	public void exampleTest() {
		//JsonRespAccountsVO jsonResponse = this.template.getForObject("http://localhost:8080/accounts/ledgersList/12,0", jsonResponse);
		
		JsonRespAccountsVO jsonResponse = restTemplate.getForObject("http://localhost:8080/secure/accounts/ledgersList/12,0", JsonRespAccountsVO.class);
		
		System.out.println("Society ID is "+jsonResponse.getSocietyID());
		
		assertThat(jsonResponse.getSocietyID()).isEqualTo(11);
	}
	
	
	

}