package com.emanager.Utility;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:applicationContext*.xml")
public class UtilityMain {
	
	

	private static final Logger logger = Logger.getLogger(UtilityMain.class);
	private static UtilityClaass utility=new UtilityClaass();
	
	/*public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(UtilityMain.class, args);
        
        
        logger.error("Message logged at ERROR level");
        logger.warn("Message logged at WARN level");
        logger.info("Message logged at INFO level");
        logger.debug("Message logged at DEBUG level");
        
        
        utility.sendReminders(56);
        
	}*/
}
