/**
 * 
 */
package com.emanager.Utility;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.emanager.server.accounts.DataAccessObjects.LedgerTrnsVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionMetaVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.services.AddressService;
import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.financialReports.domainObject.AmountInWords;
import com.emanager.server.invoice.dataAccessObject.BillDetailsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceDetailsVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.society.valueObject.VehicleVO;
import com.emanager.server.taskAndEventManagement.valueObject.EmailMessage;
import com.emanager.server.taskAndEventManagement.valueObject.NotificationVO;
import com.emanager.server.taskAndEventManagement.valueObject.SMSMessage;


/**
 * @author Sudarshan Sathe
 *
 */


//@SpringBootApplication
//@ImportResource("classpath:applicationContext*.xml")

public class UtilityClaass {

	/**
	 * @param args
	 * 
	 */
	@Autowired
	public  RestTemplate restTemplate;

	public static LedgerService ledgerService;
	

	/**
	 * @param addressServiceBean the addressServiceBean to set
	 */
	public static void setAddressServiceBean(AddressService addressServiceBean) {
		UtilityClaass.addressServiceBean = addressServiceBean;
	}

	public static AddressService addressServiceBean;
	private static NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	private static VelocityEngine velocityEngine;

	private static final Logger logger = Logger.getLogger(UtilityClaass.class);
/*	
	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(UtilityClaass.class, args);
        
        
        logger.error("Message logged at ERROR level");
        logger.warn("Message logged at WARN level");
        logger.info("Message logged at INFO level");
        logger.debug("Message logged at DEBUG level");
        List memberList=reminderMemberList(1);
	}
	*/
	public void sendReminders(int societyID){
		List memberList=new ArrayList();
		List emailList=new ArrayList();
		List smsList=new ArrayList();
		NotificationVO notification=new NotificationVO();
		HTMLToPDFCreator htmlConverter=new HTMLToPDFCreator();
		memberList=reminderMemberList(societyID);
		
	//	for(int i=0;memberList.size()>i;i++){
			MemberVO memberVO=(MemberVO) memberList.get(0);
			EmailMessage emailObj=prepareDemandNoticeEmailObject(memberVO,societyID);
			//EmailMessage emailObj=prepareReminderEmailIreneParkingObject(memberVO);
		//	htmlConverter.doPDFfromHTML(emailObj.getMessage(),memberVO.getFlatNo());
			//SMSMessage smsObj=prepareReminderSMSObject(memberVO);
		if((emailObj.getEmailTO()!=null)&&(emailObj.getEmailTO().length()>0))
			emailList.add(emailObj);
					
	//	}
		
		notification.setEmailList(emailList);
		
		logger.info("Emails count "+emailList.size());
		
		 MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
         Map map = new HashMap<String, String>();
         map.put("Content-Type", "application/json");

         headers.setAll(map);

         Map emailMap = new HashMap();
         emailMap.put("emailList", emailList);
         emailMap.put("smsList", smsList);
         emailMap.put("sendEmail", true);
         emailMap.put("sendSMS", false);

         HttpEntity<?> requestEmail = new HttpEntity<>(emailMap, headers);
         String urlForEmail = "http://localhost:8090/finbot/sendNotification/";

         ResponseEntity<NotificationVO> respEmail = new RestTemplate().postForEntity(urlForEmail, requestEmail, NotificationVO.class);
        
        

         headers.setAll(map);

     /*    Map smsMap = new HashMap();
         smsMap.put("smsList", smsList);

         HttpEntity<?> requestSMS = new HttpEntity<>(smsMap, headers);*/
      //   String urlForSMS = "http://localhost:8090/finbot/sendSMS/";

        // ResponseEntity<Boolean> respSMS = new RestTemplate().postForEntity(urlForSMS, requestSMS, Boolean.class);
		
		
		
	}
	
	public void convertAmountInWord(int societyID){
		List societyList=searchSocietyList();
		//for(int j=0;societyList.size()>j;j++){
		//SocietyVO societyVO=(SocietyVO) societyList.get(j);
		AmountInWords amtInWrds=new AmountInWords();
		
		List txList=getTransactionList(societyID);
		int updateCount=0;
		if(txList.size()>0){
		for(int i=0;txList.size()>i;i++){
			TransactionVO txVO=(TransactionVO) txList.get(i);
			
			txVO.setAmtInWord(amtInWrds.convertToAmt(txVO.getAmount().setScale(2).toString()));
			updateTransactions(txVO,societyID);
		}
		System.out.println("For "+societyID+"  invoices linked ="+updateCount);
		}
		
		//}
	}
	
	public void linkInvoiceToTransactions(){
		List societyList=searchSocietyList();
		//for(int j=0;societyList.size()>j;j++){
		//SocietyVO societyVO=(SocietyVO) societyList.get(j);
		int societyID=8;
		List invoiceList=getInvoiceList(societyID);
		int updateCount=0;
		if(invoiceList.size()>0){
		for(int i=0;invoiceList.size()>i;i++){
			TransactionVO txVO=new TransactionVO();
			TransactionMetaVO txMeta=new TransactionMetaVO();
			InvoiceDetailsVO inv=(InvoiceDetailsVO) invoiceList.get(i);
			
			txMeta.setInvoiceDate(inv.getInvoiceDate());
			txMeta.setInvoiceNO("INV_"+inv.getDelegatedInvoiceID());
			txMeta.setBillingCycleID(inv.getBillCycleID());
			txMeta.setInvoiceTypeID(inv.getInvoiceTypeID());
			txMeta.setBillID(Integer.parseInt(inv.getInvoiceID()));
			txMeta.setStatusID(inv.getStatusID());
			txMeta.setLedgerID(inv.getLedgerID());
			if((inv.getInvoiceTypeID()==1)||(inv.getInvoiceTypeID()==10)){
			txMeta.setInvoiceMode("R");
			}else
				txMeta.setInvoiceMode("O");
			txVO.setTransactionID(inv.getDelegatedInvoiceID());
			txVO.setSocietyID(societyID);
			txVO.setTransactionDate(inv.getInvoiceDate());
			txVO.setAdditionalProps(txMeta);
	
			int k=updateAdditionalProperties(txVO, societyID);
			if(k>0)
				updateCount=updateCount+1;
		}
		System.out.println("For "+societyID+"  invoices linked ="+updateCount);
		}
		
		//}
	}
	
	
	private EmailMessage prepareDemandNoticeEmailObject(MemberVO memberVO,int societyID){
		EmailMessage emailVO=new EmailMessage();
		BigDecimal totalAmount=BigDecimal.ZERO;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy");
		java.util.Date today = new java.util.Date();
		String address="";
		DecimalFormat df = new DecimalFormat("#,###.00");
	
		try {
	
		  List addressList=addressServiceBean.getAddressDetailsList(memberVO.getMemberID(), "M", 12, memberVO.getFlatNo());
			
		
		  
			 LedgerTrnsVO memberLedger=ledgerService.getBillTransactionList(societyID, memberVO.getLedgerID(),sdf.format(today),sdf.format(today),"C");
		
	
		
			emailVO.setAppID(1);
			emailVO.setOrgID(societyID);
			emailVO.setDefaultSender("MSR Queens Town CHS Ltd");
			emailVO.setEmailTO("sudarshan_sathe@esanchalak.com");
			emailVO.setSubject("Demand Notice to "+memberVO.getFlatNo());
			if(addressList.size()>0){
				AddressVO addressVO=(AddressVO) addressList.get(0);
				
				address=addressVO.getAddress();
				}
			
			BigDecimal mmc=new BigDecimal("28800");
			BigDecimal sf=new BigDecimal("12000");
			BigDecimal tenant=new BigDecimal("0.00");
			BigDecimal subTotal=new BigDecimal("40800");
			if(memberVO.getIsRented()==1){
				tenant=new BigDecimal("4080");
				subTotal=new BigDecimal("44880");
			}
			BigDecimal total=subTotal.add(memberLedger.getClosingBalance());
			
		 /*  first, get and initialize an engine  */
        
        velocityEngine.init();
        /*  organize our data  */
      
        VelocityContext map = new VelocityContext();
        map.put("memberName", memberVO.getTitle()+" "+memberVO.getFullName());
        map.put("unitName", memberVO.getFlatNo());
        map.put("aptName", memberVO.getApartmentName());
        map.put("buildingName",memberVO.getBuildingName());
      
        map.put("address", address);
        if(memberLedger.getClosingBalance().intValue()>0){
        map.put("balance", df.format(memberLedger.getClosingBalance()));
        }else
        	map.put("balance", memberLedger.getClosingBalance());
        if(memberVO.getIsRented()==1){
        	 map.put("tenant",df.format(tenant));
        }else
        	 map.put("tenant",tenant);
       
        map.put("subTotal", df.format(subTotal));
        map.put("total",df.format(total));
        
      
 
       
        /*  add that list to a VelocityContext  */
      
        emailVO.setMessage(createMessage(map, "msrqt_demand_notice.html"));
		
		} catch (Exception e) {
			logger.error("Exception in preparing member object "+e);
		}
		
        
		return emailVO;
	}
	
	
	private EmailMessage prepareReminderEmailObject(MemberVO memberVO){
		EmailMessage emailVO=new EmailMessage();
		BigDecimal totalAmount=BigDecimal.ZERO;
		String address="";
		
		emailVO.setDefaultSender("ALDEA ESPANOLA PHASE-II CO-OPERATIVE HOUSING SOCIETY LTD");
		emailVO.setEmailTO("sudarshan_sathe@esanchalak.com");
		emailVO.setSubject("Pending dues towards Society Maintenance/Sinking Fund Charges");
		try {
	/*
		  List addressList=addressServiceBean.getAddressDetailsList(memberVO.getMemberID(), "M", 12, memberVO.getFlatNo());
			
			if(addressList.size()>0){
			AddressVO addressVO=(AddressVO) addressList.get(0);
			
			address=addressVO.getAddress();
			}*/
			/*totalAmount=memberVO.getMmcDetails().add(new BigDecimal("36000"));
			totalAmount=totalAmount.add(memberVO.getCostOfUnit());*/
			
		
		
		
		 /*  first, get and initialize an engine  */
        
        velocityEngine.init();
        /*  organize our data  */
      
        VelocityContext map = new VelocityContext();
        map.put("memberName", memberVO.getFullName());
        map.put("unitName", memberVO.getFlatNo());
        map.put("balance", memberVO.getMmcDetails());
        map.put("corpus", memberVO.getCostOfUnit());
        map.put("address", address);
        map.put("due", memberVO.getTotalDue());
        
 
       
        /*  add that list to a VelocityContext  */
      
        emailVO.setMessage(createMessage(map, "sample.html"));
		} catch (Exception e) {
			logger.error("Exception in preparing member object "+e);
		}
		
        
		return emailVO;
	}
	
	private EmailMessage prepareReminderEmailIreneParkingObject(MemberVO memberVO){
		EmailMessage emailVO=new EmailMessage();
		BigDecimal totalAmount=BigDecimal.ZERO;
		DateUtility dateUtil=new DateUtility();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		java.util.Date today = new java.util.Date();
		String htmlTable="";
		
		emailVO.setDefaultSender("Irene Tower Co-Operative Housing Society Ltd");
		emailVO.setEmailTO(memberVO.getEmail());
		//emailVO.setEmailCc("niranjan@esanchalak.com,sudarshan_sathe@esanchalak.com");
		emailVO.setSubject("["+memberVO.getFlatNo()+" ]: Validate parking slot for your flat ");
		try {

		  List parkingList=getIreneParkingList(memberVO.getMemberID());
			
			if(parkingList.size()>0){
			
				
				htmlTable=htmlTableBuilder(parkingList);
				
			
			
			}
						
		
		
		
		 /*  first, get and initialize an engine  */
        
        velocityEngine.init();
        /*  organize our data  */
      
        VelocityContext map = new VelocityContext();
        
        map.put("unitName", memberVO.getFlatNo());
        map.put("currentDate", sdf.format(today));
        map.put("htmlTable", htmlTable);
        
 
       
        /*  add that list to a VelocityContext  */
      
        emailVO.setMessage(createMessage(map, "samples.html"));
		} catch (Exception e) {
			logger.error("Exception in preparing member object "+e);
		}
		
        
		return emailVO;
	}
	
private String htmlTableBuilder(List list){
		
		String htmlTable = "";
		Iterator iterator=list.iterator();
		int serialNumber  = 1;
		while(iterator.hasNext()){
			VehicleVO vehicleVO=(VehicleVO)iterator.next();
			
			htmlTable  = htmlTable + "<tr> " +
			        "<td align=left>" +
			            serialNumber++ +"  "+ vehicleVO.getVehicleType()+"   "+ vehicleVO.getRegNumber() +	       
					"</td>" +				
					"</tr>";
			
		}
		return htmlTable;
	}
	
	private SMSMessage prepareReminderSMSObject(MemberVO memberVO){
		SMSMessage smsVO=new SMSMessage();
		
		smsVO.setMobileNumber(memberVO.getMobile());
		smsVO.setMessage("We wish to inform you that your cash in hand balance summary as on 20-Mar-2017 as follows.");
		
		
		
		
		
		return smsVO;
	}
	
	private String createMessage(VelocityContext context, String templateURL)
			{

		String emailText = "";
		try {
			 Template t = velocityEngine.getTemplate(templateURL);
		        /*  now render the template into a Writer  */
		        StringWriter writer = new StringWriter();
		        t.merge( context, writer );
		        emailText=writer.toString();
		} catch (VelocityException e) {
			e.printStackTrace();
		}
		logger.debug("creatingMessage");
		return emailText;
	}
	
	private  List reminderMemberList(int societyID){
		List memberList=new ArrayList();
		
		String strSQL = "";
		try{	
			logger.debug("Entry : public List searchMembers(String strSocietyID))" );
			//1. SQL Query
			strSQL = "SELECT m.*,l.*, a.is_Rented as rented FROM members_info m,apartment_details a,ledger_user_mapping_"+societyID+" l WHERE a.apt_id=m.apt_id and m.society_id=:societyID AND TYPE='P' AND m.member_id=l.user_id   ;";
			logger.debug("query : " + strSQL);
			// 2. Parameters.
			Map hmap=new HashMap();
	        hmap.put("societyID", societyID);    
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			            MemberVO membrs = new MemberVO();
			             
			            membrs.setFlatNo(rs.getString("unit_name"));
			            membrs.setTitle(rs.getString("title"));
			            membrs.setFullName(rs.getString("full_name"));
			            membrs.setMobile(rs.getString("mobile"));
			            membrs.setEmail(rs.getString("email"));
			            membrs.setMemberID(rs.getInt("member_id"));
			            membrs.setLedgerID(rs.getInt("ledger_id"));
			            membrs.setApartmentName(rs.getString("apt_name"));
			            membrs.setBuildingName(rs.getString("building_name"));
			            membrs.setIsRented(rs.getInt("rented"));
			           // membrs.setMmcDetails(rs.getBigDecimal("balance"));
			          //  membrs.setCostOfUnit(rs.getBigDecimal("corpus"));			            
			           
			            return membrs;
			        }};
		
			        memberList = namedParameterJdbcTemplate.query(strSQL,hmap,RMapper);
			    		        		        
			        logger.debug("Exit : public List searchMembers(String strSocietyID)"+memberList.size());
			}catch(Exception ex){
				logger.error("Exception in searchMembers : "+ex);
				
			}
		
		
		return memberList;
	}
	
private List getTransactionList(int societyID){
		
		List invoiceList=new ArrayList();
		String strSQL = "";
		try{	
			logger.debug("Entry : public List searchMembers(String strSocietyID))" );
			SocietyVO societyVO=getSocietyDetails(societyID);
			//1. SQL Query
			strSQL = "SELECT * FROM account_transactions_"+societyVO.getDataZoneID()+" WHERE org_id=:societyID AND is_deleted=0 AND amt_in_word IS NULL;";
			logger.debug("query : " + strSQL);
			// 2. Parameters.
			Map hmap=new HashMap();
	        hmap.put("societyID", societyID);    
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			            TransactionVO txVO=new TransactionVO();
			             
			            txVO.setTransactionID(rs.getInt("tx_id"));
			            txVO.setTransactionDate(rs.getString("tx_date"));
						txVO.setAmount(rs.getBigDecimal("amount"));            
			           
			            return txVO;
			        }};
		
			        invoiceList = namedParameterJdbcTemplate.query(strSQL,hmap,RMapper);
			    		        		        
			        logger.debug("Exit : public List searchMembers(String strSocietyID)"+invoiceList.size());
			}catch(Exception ex){
				logger.error("Exception in searchMembers : "+ex);
				
			}
		
		
		return invoiceList;
	}
	
	
	/* Used for adding Additional properties  */
	public int updateTransactions(TransactionVO txAddPropVO,int societyID) {
		int success=0;
		 java.util.Date today = new java.util.Date();
		 final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
		logger.debug("Entry : public int insertAdditionalProperties(TransactionVO txAddPropVO)");
		try {
			
			SocietyVO societyVO=getSocietyDetails(societyID);			   
		
			
			   String str = "UPDATE account_transactions_"+societyVO.getDataZoneID()+" SET amt_in_word=:amt WHERE org_id=:societyID AND is_deleted=0 AND tx_id=:txID;";
			   
			   
			   Map hMap=new HashMap();
			  hMap.put("societyID", societyID);
			  hMap.put("amt", txAddPropVO.getAmtInWord());
			  hMap.put("txID", txAddPropVO.getTransactionID());
			   
			   success=namedParameterJdbcTemplate.update(str, hMap);
			
			logger.debug("Exit : public int insertAdditionalProperties(TransactionVO txAddPropVO)");	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception in public int insertAdditionalProperties(TransactionVO txAddPropVO) "+e);
		}
		
		return success;
	}
	
	
	private List getInvoiceList(int societyID){
		
		List invoiceList=new ArrayList();
		String strSQL = "";
		try{	
			logger.debug("Entry : public List searchMembers(String strSocietyID))" );
			//1. SQL Query
			strSQL = "select i.*,t.tx_id,l.ledger_id from in_invoice_"+societyID+" i,account_transactions_"+societyID+" t,members_info m,ledger_user_mapping_"+societyID+" l where i.apt_id=m.apt_id AND m.member_id=l.user_id AND m.type='P' AND t.invoice_id=i.id AND t.is_deleted=0 AND i.is_deleted=0 ";
			logger.debug("query : " + strSQL);
			// 2. Parameters.
			Map hmap=new HashMap();
	        hmap.put("societyID", societyID);    
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			            InvoiceDetailsVO inMemberVO=new InvoiceDetailsVO();
			             
			            inMemberVO.setLedgerID(rs.getInt("ledger_id"));
						inMemberVO.setInvoiceID(rs.getString("id"));
						
						inMemberVO.setInvoiceDate(rs.getString("invoice_date"));
						inMemberVO.setDelegatedInvoiceID(rs.getInt("tx_id"));
						inMemberVO.setTotalAmount(rs.getBigDecimal("total_amount"));
						inMemberVO.setFromDate(rs.getString("from_date"));
						inMemberVO.setUptoDate(rs.getString("to_date"));
						inMemberVO.setCarriedBalance(rs.getBigDecimal("carried_balance"));
						inMemberVO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
						inMemberVO.setBillCycleID(rs.getInt("billing_process_id"));
						inMemberVO.setDueDate(rs.getString("due_date"));
						inMemberVO.setInvoiceType(rs.getString("invoice_type"));
						inMemberVO.setBalance(rs.getBigDecimal("balance"));
						inMemberVO.setCurrentBalance(rs.getBigDecimal("current_balance"));
						inMemberVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
						inMemberVO.setStatusID(rs.getInt("status_id"));
						            
			           
			            return inMemberVO;
			        }};
		
			        invoiceList = namedParameterJdbcTemplate.query(strSQL,hmap,RMapper);
			    		        		        
			        logger.debug("Exit : public List searchMembers(String strSocietyID)"+invoiceList.size());
			}catch(Exception ex){
				logger.error("Exception in searchMembers : "+ex);
				
			}
		
		
		return invoiceList;
	}
	
	
	/* Used for adding Additional properties  */
	public int insertAdditionalProperties(TransactionVO txAddPropVO) {
		int success=0;
		 java.util.Date today = new java.util.Date();
		 final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
		logger.debug("Entry : public int insertAdditionalProperties(TransactionVO txAddPropVO)");
		try {
			TransactionMetaVO txMeta=txAddPropVO.getAdditionalProps();
			if(txMeta==null){
				txMeta=new TransactionMetaVO();
			}
			if(txAddPropVO.getGenerateInvoice())
				txMeta.setInvoiceNO("INV_"+txAddPropVO.getTransactionID());
			   			   
		
			
			   String str = "INSERT INTO soc_tx_meta "+					
				   "(society_id,tx_id ,vendor_id, invoice_no, invoice_date, tds_section_no, tds_rate, tds_amount, tax_amount , challan_date, challan_no, bsr_code, tag_values,accountant_comments,auditor_comments ,billing_cycle_id, invoice_type_id, status_id ,invoice_mode, ledger_id,bill_id )  " +
	                "VALUES (:societyID,:txID, :vendorID, :invoiceNo, :invoiceDate, :tdsSectionNo, :tdsRate, :tdsAmount, :taxAmount, :challanDate, :challanNo, :bsrCode, :tagValue,:accountantComments,:auditor_comments,:billingCycleID,:invoiceTypeID,:statusID,:invoiceMode,:ledgerID,:billID)";
			   
			   
			   Map hMap=new HashMap();
			   hMap.put("societyID", txAddPropVO.getSocietyID());
			   hMap.put("txID", txAddPropVO.getTransactionID());
			   hMap.put("vendorID", txMeta.getVendorID());
			   hMap.put("invoiceNo", txMeta.getInvoiceNO());
			   hMap.put("invoiceDate", txMeta.getInvoiceDate());
			   hMap.put("tdsSectionNo", txMeta.getTdsSectionNo());
			   hMap.put("tdsRate", txMeta.getTdsRate());
			   hMap.put("tdsAmount", txMeta.getTdsAmount());
			   hMap.put("taxAmount", txMeta.getTaxAmount());
			   hMap.put("challanDate", txMeta.getChallanDate());
			   hMap.put("challanNo", txMeta.getChallanNo());
			   hMap.put("bsrCode", txMeta.getBsrCode());
			   hMap.put("tagValue", txAddPropVO.getHashComments());
			   hMap.put("accountantComments", txMeta.getAccountantComments());
			   hMap.put("auditor_comments", txMeta.getAuditorsComments());
			   hMap.put("invNO", txMeta.getInvoiceNO());
			   hMap.put("invoiceDate", txAddPropVO.getTransactionDate());
			   hMap.put("billingCycleID", txMeta.getBillingCycleID());
			   hMap.put("invoiceTypeID", txMeta.getInvoiceTypeID());
			   hMap.put("statusID", txMeta.getStatusID());
			   hMap.put("invoiceMode", txMeta.getInvoiceMode());
			   hMap.put("ledgerID", txMeta.getLedgerID());
			   hMap.put("billID", txMeta.getBillID());
			   
			   success=namedParameterJdbcTemplate.update(str, hMap);
			
			logger.debug("Exit : public int insertAdditionalProperties(TransactionVO txAddPropVO)");	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception in public int insertAdditionalProperties(TransactionVO txAddPropVO) "+e);
		}
		
		return success;
	}

	/* Used for adding Additional properties  */
	public int updateAdditionalProperties(TransactionVO txAddPropVO,int societyID) {
		int success=0;
		 java.util.Date today = new java.util.Date();
		 final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
		logger.debug("Entry : public int updateAdditionalProperties(TransactionVO txAddPropVO)");
		try {
			TransactionMetaVO txMeta=txAddPropVO.getAdditionalProps();
			if(txMeta==null){
				txMeta=new TransactionMetaVO();
			}			   
			
			   String str = "Update soc_tx_meta set invoice_no=:invNO,invoice_date=:invoiceDate,billing_cycle_id=:billingCycleID, invoice_type_id=:invoiceTypeID, status_id=:statusID ,ledger_id=:ledgerID,invoice_mode=:invoiceMode, bill_id=:billID where tx_id=:txID and society_id=:societyID ;  ";
	                
			   
			   
			   Map hMap=new HashMap();
			   hMap.put("societyID", txAddPropVO.getSocietyID());
			   hMap.put("txID", txAddPropVO.getTransactionID());
			   hMap.put("invNO", txMeta.getInvoiceNO());
			   hMap.put("invoiceDate", txAddPropVO.getTransactionDate());
			   hMap.put("billingCycleID", txMeta.getBillingCycleID());
			   hMap.put("invoiceTypeID", txMeta.getInvoiceTypeID());
			   hMap.put("statusID", txMeta.getStatusID());
			   hMap.put("invoiceMode", txMeta.getInvoiceMode());
			   hMap.put("ledgerID", txMeta.getLedgerID());
			   hMap.put("billID", txMeta.getBillID());
			   
			   
			   success=namedParameterJdbcTemplate.update(str, hMap);
			
			   if(success==0){
				   success=insertAdditionalProperties(txAddPropVO);
			   }
			   
			   
			logger.info("Exit : public int updateAdditionalProperties(TransactionVO txAddPropVO)"+success);	
		} catch (Exception e) {
			
			logger.debug("Exception in public int updateAdditionalProperties(TransactionVO txAddPropVO) "+e);
		}
		
		return success;
	}
	
	public List searchSocietyList(){

		List societyList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry public List searchSocietyList" );
		//1. SQL Query
		strSQL =" select d.society_id,d.society_name,s.datazone_id from society_details d,society_settings s where s.society_id=d.society_id and  d.working=1; " ;
		logger.debug("query : " + strSQL);		
		// 2. Parameters.
		Map hsmap=new HashMap();
		
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            SocietyVO societyVO=new SocietyVO();
		            societyVO.setSocietyID(rs.getInt("society_id"));
		            societyVO.setSocietyName(rs.getString("society_name"));	
		            societyVO.setDataZoneID(rs.getString("datazone_id"));
		             return societyVO;
		            
		        }};
	
		        societyList = namedParameterJdbcTemplate.query(strSQL,hsmap,RMapper);
		        
	
		       
		        logger.debug("Exit : public List searchSocietyList()"+societyList.size());
		}catch (Exception ex){
			logger.error("Exception in searchSocietyList : "+ex);
			
		}
		return societyList;
		
		
	}
	
	public SocietyVO getSocietyDetails(int societyID){

		SocietyVO societyVO=new SocietyVO();
		String strSQL = "";
		try{	
		logger.debug("Entry public SocietyVO getSocietyDetails(int societyVO)" );
		//1. SQL Query
		strSQL =" select d.society_id,d.society_name,s.datazone_id from society_details d,society_settings s where s.society_id=d.society_id and  d.working=1 and d.society_id=:societyID; " ;
		logger.debug("query : " + strSQL);		
		// 2. Parameters.
		Map hsmap=new HashMap();
		hsmap.put("societyID", societyID);
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            SocietyVO societyVO=new SocietyVO();
		            societyVO.setSocietyID(rs.getInt("society_id"));
		            societyVO.setSocietyName(rs.getString("society_name"));	
		            societyVO.setDataZoneID(rs.getString("datazone_id"));
		             return societyVO;
		            
		        }};
	
		        societyVO =(SocietyVO) namedParameterJdbcTemplate.queryForObject(strSQL,hsmap,RMapper);
		        
	
		       
		        logger.debug("Exit : public SocietyVO getSocietyDetails(int societyVO)");
		}catch (Exception ex){
			logger.error("Exception in public SocietyVO getSocietyDetails(int societyVO) : "+ex);
			
		}
		return societyVO;
		
		
	}
	
private List getIreneParkingList(int memberID){
		
		List invoiceList=new ArrayList();
		String strSQL = "";
		try{	
			logger.debug("Entry : private List getIreneParkingList(int memberID)" );
			//1. SQL Query
			strSQL = "select * from temp_ledger where id=:memberID";
			logger.debug("query : " + strSQL);
			// 2. Parameters.
			Map hmap=new HashMap();
	        hmap.put("memberID", memberID);    
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			            VehicleVO vehicleVO=new VehicleVO();
			             
			            vehicleVO.setMemberID(rs.getInt("id"));
			            vehicleVO.setVehicleType(rs.getString("ledger_name"));
						 vehicleVO.setRegNumber(rs.getString("tx_id"));
			           
			            return vehicleVO;
			        }};
		
			        invoiceList = namedParameterJdbcTemplate.query(strSQL,hmap,RMapper);
			    		        		        
			        logger.debug("Exit : private List getIreneParkingList(int memberID)"+invoiceList.size());
			}catch(Exception ex){
				logger.error("Exception in private List getIreneParkingList(int memberID) : "+ex);
				
			}
		
		
		return invoiceList;
	}
	
public void sendSMSReminders(int societyID){
	List memberList=new ArrayList();
	List emailList=new ArrayList();
	List smsList=new ArrayList();
	NotificationVO notification=new NotificationVO();
	HTMLToPDFCreator htmlConverter=new HTMLToPDFCreator();
	memberList=smsMemberList();
	
	for(int i=0;memberList.size()>i;i++){
		MemberVO memberVO=(MemberVO) memberList.get(i);
		SMSMessage smsObj=prepareReminderSMSObjectForSwamiCHS(memberVO);
		//EmailMessage emailObj=prepareReminderEmailIreneParkingObject(memberVO);
	//	htmlConverter.doPDFfromHTML(emailObj.getMessage(),memberVO.getFlatNo());
		//SMSMessage smsObj=prepareReminderSMSObject(memberVO);
	
				
	
	if((smsObj.getMobileNumber()!=null)&&(!smsObj.getMobileNumber().equalsIgnoreCase("0"))){
		smsList.add(smsObj);
	}
	}
	notification.setSmsList(smsList);
	notification.setOrgID(113);
	notification.setSendSMS(true);
	
	logger.info("SMS count "+smsList.size());
	
	 MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
     Map map = new HashMap<String, String>();
     map.put("Content-Type", "application/json");

     headers.setAll(map);

     Map emailMap = new HashMap();
     emailMap.put("emailList", emailList);
     emailMap.put("smsList", smsList);
     emailMap.put("sendEmail", true);
     emailMap.put("sendSMS", true);

     HttpEntity<?> requestEmail = new HttpEntity<>(notification, headers);
     String urlForEmail = "http://localhost:8090/finbot/sendNotification/";

     ResponseEntity<NotificationVO> respEmail = new RestTemplate().postForEntity(urlForEmail, requestEmail, NotificationVO.class);
    
    

     headers.setAll(map);

 /*    Map smsMap = new HashMap();
     smsMap.put("smsList", smsList);

     HttpEntity<?> requestSMS = new HttpEntity<>(smsMap, headers);*/
  //   String urlForSMS = "http://localhost:8090/finbot/sendSMS/";

    // ResponseEntity<Boolean> respSMS = new RestTemplate().postForEntity(urlForSMS, requestSMS, Boolean.class);
	
	
	
}

private  List smsMemberList(){
	List memberList=new ArrayList();
	
	String strSQL = "";
	try{	
		logger.debug("Entry : public List searchMembers(String strSocietyID))" );
		//1. SQL Query
		strSQL = "SELECT * FROM temp_ledger   ;";
		logger.debug("query : " + strSQL);
		// 2. Parameters.
		Map hmap=new HashMap();
        //hmap.put("societyID", societyID);    
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            MemberVO membrs = new MemberVO();
		             
		          
		            membrs.setFullName(rs.getString("ledger_name"));
		            membrs.setMobile(rs.getString("mobile"));
		            membrs.setMmcDetails(rs.getBigDecimal("amount"));	  
		            membrs.setTotalDue(rs.getBigDecimal("balance"));
		           
		            return membrs;
		        }};
	
		        memberList = namedParameterJdbcTemplate.query(strSQL,hmap,RMapper);
		    		        		        
		        logger.debug("Exit : public List searchMembers(String strSocietyID)"+memberList.size());
		}catch(Exception ex){
			logger.error("Exception in searchMembers : "+ex);
			
		}
	
	
	return memberList;
}

private SMSMessage prepareReminderSMSObjectForSwamiCHS(MemberVO memberVO){//1-If new Invoice generated else 0 for reminder
	SMSMessage smsVO=new SMSMessage();
	try {
	logger.debug("Entry : private SMSMessage prepareReminderSMSObject");
	ConfigManager config=new ConfigManager();
	String unitName=memberVO.getFlatNo();
	String totalAmt=memberVO.getMmcDetails().toString();
	String totalDue=memberVO.getTotalDue().toString();
	
	
	String memberName=memberVO.getFullName();
	
	String smsString="Dear "+memberVO.getFullName()+", Pending maintenance Rs "+totalAmt+" before 31-03-2018, Pending maintenance Rs "+totalDue+" from 01-04-2018 to Current Month. If already paid plz contact manager with payment details. -Shree Swami Sanidhya CHS";
 
 	
 	
	
	smsVO.setMobileNumber(memberVO.getMobile());
	smsVO.setMessage(smsString);
	
	
	} catch (Exception e) {
		logger.error("Exception occurred in private SMSMessage prepareReminderSMSObject "+e);
	}
	logger.debug("Exit : private SMSMessage prepareReminderSMSObject");
	
	
	return smsVO;
}


	 /**
		 * @param namedParameterJdbcTemplate the namedParameterJdbcTemplate to set
		 */
		public static void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
			UtilityClaass.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
		}

	/**
	 * @param velocityEngine the velocityEngine to set
	 */
	public static void setVelocityEngine(VelocityEngine velocityEngine) {
		UtilityClaass.velocityEngine = velocityEngine;
	}

	/**
	 * @param ledgerService the ledgerService to set
	 */
	public static void setLedgerService(LedgerService ledgerService) {
		UtilityClaass.ledgerService = ledgerService;
	}
	

	
}
