package com.emanager.Utility;

import java.io.OutputStream;
import java.io.StringReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.PdfWriter;


public class HTMLToPDFCreator {

	
	@SuppressWarnings("deprecation")
	public void doPDFfromHTML(String htmlString,String unitName){
		 try {
			/* Document document = new Document(PageSize.LETTER);
     	    OutputStream file = new FileOutputStream(new File("D:\\QueensTown\\Irene\\ParkingSlot_"+unitName+".pdf"));
     	    
     	   
     	    PdfWriter.getInstance(document, file);
     	    document.open();
     	    for(int i=1; i<2; i++){
		    	    String k = htmlString;
		    	    HTMLWorker htmlWorker = new HTMLWorker(document);
		    	    htmlWorker.parse(new StringReader(k));
		     	    document.newPage();
		    	    
     	    }
     	    document.close();
     	    file.close();*/
			 
			 String pdfFilePath = "D:\\QueensTown\\2019-20\\Demand_Notice_"+unitName+".pdf";
	           OutputStream fos = new FileOutputStream(new File(pdfFilePath));
	           Document document = new Document();
	           PdfWriter pdfWriter = PdfWriter.getInstance(document, fos);
	 
	           //Build html using StringBuilder to be converted to pdf.
	           System.out.println("String contents "+htmlString);
	                             
	           document.open();
	           for(int i=1; i<2; i++){
		    	    String k = htmlString;
		    	    HTMLWorker htmlWorker = new HTMLWorker(document);
		    	    htmlWorker.parse(new StringReader(k));
		     	    document.newPage();
		    	    
    	    }
	           document.close();
	           
	           fos.close();

     } catch (Exception e) {

         e.printStackTrace();
     }
	}
	
}
