package com.emanager.webservice;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.taskAndEventManagement.valueObject.TicketVO;
import com.emanager.server.userManagement.service.UserService;
import com.emanager.server.userManagement.valueObject.UserVO;
import com.emanager.webservice.filters.TokenFilter;

@Controller
@RequestMapping("/userPassword")
public class JSONUserPasswordController {
	
	private static final Logger logger = Logger.getLogger(JSONUserPasswordController.class);
	@Autowired
	MemberService memberServiceBean;
	
	@Autowired
	UserService userService;
	@Autowired
	TokenFilter tokenFilter;
	
	public ConfigManager configManager =new ConfigManager();	
	public EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
	public final String HEADER_SECURITY_TOKEN = "X-AuthToken";
	public final String HEADER_ApartmentID= "X-ApartmentID";
	public final String HEADER_URL= "X-Url";
	public final String HEADER_UserID= "X-UserID";
	public final String HEADER_OrgID= "X-OrgID";
	public final String HEADER_AndroidDeviceID= "X-DeviceID";
	
	@RequestMapping(value="/authenticate", method = RequestMethod.POST)
	public ResponseEntity authenticateUser(@RequestHeader("Authorization") String authorzation,@RequestHeader(value=HEADER_AndroidDeviceID, required = false) String andriodDeiviceID,@RequestBody UserVO loginUser ){
		String success=null;
		UserVO userVO=new UserVO();
		 APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public @ResponseBody UserVO authenticateUser(@RequestBody UserVO loginUser )")	;
		try {
							
			String base64Credentials = authorzation.substring("Basic".length()).trim();
			byte[] decordedValue = 	new Base64().decode(base64Credentials);			
			String credentials = new String(decordedValue, Charset.forName("UTF-8"));;
			
			final String[] values = credentials.split(":",2);
			loginUser.setLoginName(values[0]);
			loginUser.setPassword(values[1]);
			
			logger.info("**********Simple API Server Authenticate User Method********");
			logger.info("Login ID:  "+loginUser.getLoginName());
			logger.info("Password: "+loginUser.getPassword());
			logger.info("App ID: "+loginUser.getAppID());
			
			if(loginUser.getLoginName().length()>0 && loginUser.getPassword().length()>0 && loginUser.getAppID().length()>0){
				loginUser.setAndriodDeviceID(andriodDeiviceID);
				userVO=userService.authenticateUser(loginUser);
								
				if(!userVO.getLoginSuccess()){
					apiResponse.setStatusCode(userVO.getStatusCode());
					apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
					logger.debug("User ID: "+userVO.getUserID()+" failed Message : " + configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
					return new ResponseEntity(apiResponse, HttpStatus.UNAUTHORIZED);				
					
				}else{
					//update device id					
					if(andriodDeiviceID==null){
						
					}else if((andriodDeiviceID.length()>0) && (andriodDeiviceID.equals(userVO.getAndriodDeviceID())==false)){
					  loginUser.setUserID(userVO.getUserID());
					  loginUser.setAndriodDeviceID(andriodDeiviceID);
						Boolean isUpdated=userService.updateUserAndriodDeviceID(loginUser);
					}
				}
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
			
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception in authenticateUser: "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
			return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody UserVO authenticateUser(@RequestBody UserVO loginUser )")	;
		return new ResponseEntity(userVO, HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/socialLoginAuthenticate", method = RequestMethod.POST)
	public ResponseEntity socialLoginAuthenticateUser(@RequestHeader(value=HEADER_AndroidDeviceID, required = false) String andriodDeiviceID,@RequestBody UserVO loginUser ){
		String success=null;
		UserVO userVO=new UserVO();
		 APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public @ResponseBody UserVO socialLoginAuthenticateUser(@RequestBody UserVO loginUser )")	;
		try {
					
			logger.info("**********Social Login API Server Authenticate User Method********");
			logger.info("Login ID:  "+loginUser.getLoginName());
			logger.info("App ID: "+loginUser.getAppID());
			
			if(loginUser.getLoginName().length()>0 && loginUser.getAppID().length()>0){
				loginUser.setAndriodDeviceID(andriodDeiviceID);
				userVO=userService.socialLoginAuthenticateUser(loginUser);
								
				if(!userVO.getLoginSuccess()){
					apiResponse.setStatusCode(userVO.getStatusCode());
					apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
					logger.debug("User ID: "+userVO.getUserID()+" failed Message : " + configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
					return new ResponseEntity(apiResponse, HttpStatus.UNAUTHORIZED);				
					
				}else{
					//update device id					
					if(andriodDeiviceID==null){
						
					}else if((andriodDeiviceID.length()>0) && (andriodDeiviceID.equals(userVO.getAndriodDeviceID())==false)){
					  loginUser.setUserID(userVO.getUserID());
					  loginUser.setAndriodDeviceID(andriodDeiviceID);
						Boolean isUpdated=userService.updateUserAndriodDeviceID(loginUser);
					}
				}
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
			
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception in socialLoginAuthenticateUser: "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
			return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody UserVO socialLoginAuthenticateUser(@RequestBody UserVO loginUser )")	;
		return new ResponseEntity(userVO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/changeOrganization", method = RequestMethod.POST)
	public ResponseEntity changeOrganization(@RequestHeader("X-RefreshToken") String refreshToken, @RequestBody UserVO orgVO ){
		UserVO userVO=new UserVO();
		 APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public ResponseEntity UserVO changeOrganization(UserVO orgVO )")	;
		try {
			logger.debug("Here the userID "+orgVO.getUserID()+" orgID: "+orgVO.getOrgID()+" appID : "+ orgVO.getAppID()+" roleID: "+orgVO.getRoleID());
			orgVO.setRefreshToken(refreshToken);
			UserVO decrptVO=enDeUtility.decodeStringToBean(refreshToken);
			
			//Check refresh token validity
			Boolean isRefreshTokenValid= userService.verifyRefreshToken(decrptVO);			
			if(isRefreshTokenValid){	
				
				if(orgVO.getUserID().length()>0 && orgVO.getOrgID().length()>0 && orgVO.getAppID().length()>0 && orgVO.getRoleID().length()>0){	
				   userVO=userService.changeUserOrgnization(orgVO);
				 
				   if(!userVO.getLoginSuccess()){
						apiResponse.setStatusCode(userVO.getStatusCode());
						apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
						return new ResponseEntity(apiResponse, HttpStatus.UNAUTHORIZED);			
	    			}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			}else{   			
				   logger.info("Refresh Token is invalid ");
				   apiResponse.setStatusCode(526);
        		   apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
        		   return new ResponseEntity(apiResponse,HttpStatus.UNAUTHORIZED);
			 }	
			
		} catch (Exception e) {
			logger.error("Exception in changeOrganization "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
			return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity UserVO changeOrganization(UserVO orgVO )")	;
		return new ResponseEntity(userVO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/organizationList", method = RequestMethod.POST)
	public ResponseEntity getUserOrganizationList(@RequestHeader("X-RefreshToken") String refreshToken, @RequestBody UserVO orgVO ){
		String success=null;
		UserVO userVO=new UserVO();
		 APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public ResponseEntity UserVO getUserOrganizationList(@RequestBody UserVO orgVO )")	;
		try {
			logger.debug("userID: "+orgVO.getUserID()+" appID : "+ orgVO.getAppID());
			UserVO decrptVO=enDeUtility.decodeStringToBean(refreshToken);
			Boolean isRefreshTokenValid= userService.verifyRefreshToken(decrptVO);
			
			if(isRefreshTokenValid){	
				if(orgVO.getUserID().length()>0 && orgVO.getAppID().length()>0){	
				   userVO=userService.getUserOrganizationList(orgVO.getUserID(),orgVO.getAppID());
				   if(!userVO.getLoginSuccess()){
						apiResponse.setStatusCode(userVO.getStatusCode());
						apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
						return new ResponseEntity(apiResponse, HttpStatus.UNAUTHORIZED);				
						
					}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			}else{   			
				   logger.info("Refresh Token is invalid ");
				   apiResponse.setStatusCode(526);
     		       apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
     		       return new ResponseEntity(apiResponse, HttpStatus.UNAUTHORIZED);
			 }		
		} catch (Exception e) {
			logger.error("Exception in  getOrganizationList: "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
			return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity UserVO getUserOrganizationList(@RequestBody UserVO orgVO )")	;
		return new ResponseEntity(userVO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/forgot", method = RequestMethod.POST)
	public ResponseEntity forgotPassword(@RequestBody UserVO orgVO ){
		String success=null;
		UserVO userVO=new UserVO();
		 APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public ResponseEntity forgotPassword(@RequestBody UserVO orgVO )")	;
		try {
			logger.debug("Email ID "+orgVO.getLoginName()+" appID : "+ orgVO.getAppID());
		
			if(orgVO.getAppID().length()>0 && orgVO.getLoginName().length()>0){	
			   userVO=userService.forgotPassword(orgVO.getLoginName(),orgVO.getAppID());
			   if(userVO.getStatusCode()!=200){
					apiResponse.setStatusCode(userVO.getStatusCode());
					apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
					
				}else{
					apiResponse.setStatusCode(userVO.getStatusCode());
					apiResponse.setMessage("Temporary  password has been emailed to you. Please check your email");
					
										
				}
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in  forgotPassword: "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
			return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity forgotPassword(@RequestBody UserVO orgVO )")	;
		return new ResponseEntity(apiResponse, HttpStatus.OK);
	}
	
	@RequestMapping(value="/changePassword", method = RequestMethod.POST)
	public ResponseEntity changedUserPassword(@RequestHeader("X-RefreshToken") String refreshToken,@RequestBody UserVO requestVO ){
		UserVO userVO=new UserVO();
		 APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public ResponseEntity changedUserPassword(@RequestBody UserVO requestVO )")	;
		try {
			logger.info("Password :"+requestVO.getPassword()+" User ID: "+requestVO.getUserID()+" Updated by : "+ requestVO.getUpdatedBy());
			
			UserVO decrptVO=enDeUtility.decodeStringToBean(refreshToken);
			Boolean isRefreshTokenValid= userService.verifyRefreshToken(decrptVO);
			
			if(isRefreshTokenValid){	
				if(requestVO.getPassword().length()>0 && requestVO.getUpdatedBy().length()>0 && requestVO.getUserID().length()>0 ){
					requestVO.setUserID(decrptVO.getUserID());
				  Boolean isSuccess=userService.changedPassword(requestVO.getUserID(), requestVO.getPassword(), requestVO.getUpdatedBy());
			
				   if(isSuccess){									
					   apiResponse.setStatusCode(200);
					   apiResponse.setMessage("User password has been updated successfully.");
					}else{
						apiResponse.setStatusCode(406);
						apiResponse.setMessage("Unable to update password.");
						return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);							
					}
				   
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}   
				
			} else{
				  logger.info("Refresh Token is invalid ");
				   apiResponse.setStatusCode(526);
    		       apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
    		       return new ResponseEntity(apiResponse, HttpStatus.UNAUTHORIZED);
				
			}
		} catch (Exception e) {
			logger.error("Exception in  changedUserPassword: "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity changedUserPassword(@RequestBody UserVO requestVO )")	;
		return new ResponseEntity(apiResponse, HttpStatus.OK);
	}
	
	@RequestMapping(value="/reset", method = RequestMethod.POST)
	public ResponseEntity resetPassword(@RequestBody UserVO orgVO ){
		UserVO userVO=new UserVO();
		 APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public ResponseEntity resetPassword(@RequestBody UserVO orgVO )")	;
		try {
			logger.debug("Password: "+orgVO.getPassword()+" Reset token : "+ orgVO.getAccessToken());
		
			if(orgVO.getPassword().length()>0 && orgVO.getAccessToken().length()>0){	
				Boolean isSuccess=userService.resetPassword(orgVO.getPassword(), orgVO.getAccessToken());
			   if(isSuccess){
				   apiResponse.setStatusCode(200);
				   apiResponse.setMessage("Your password has been reset successfully.");	 
					
				}else{
					apiResponse.setStatusCode(540);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));					
					return new ResponseEntity(apiResponse, HttpStatus.FORBIDDEN);				
					
										
				}
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in  resetPassword: "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
			return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity resetPassword(@RequestBody UserVO orgVO )")	;
		return new ResponseEntity(apiResponse, HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/orgApartmentList", method = RequestMethod.POST)
	public ResponseEntity getUserOrgApartmentList(@RequestHeader("X-RefreshToken") String refreshToken, @RequestBody UserVO orgVO ){
		String success=null;
		UserVO userVO=new UserVO();
		 APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public ResponseEntity UserVO getUserOrgApartmentList(@RequestBody UserVO orgVO )")	;
		try {
			logger.debug("userID: "+orgVO.getUserID()+" appID : "+ orgVO.getAppID());
			UserVO decrptVO=enDeUtility.decodeStringToBean(refreshToken);
			Boolean isRefreshTokenValid= userService.verifyRefreshToken(decrptVO);
			
			if(isRefreshTokenValid){	
				if(orgVO.getUserID().length()>0 && orgVO.getAppID().length()>0){	
				   userVO=userService.getUserOrgApartmentList(orgVO.getUserID(),orgVO.getAppID());
				   if(!userVO.getLoginSuccess()){
						apiResponse.setStatusCode(userVO.getStatusCode());
						apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
						return new ResponseEntity(apiResponse, HttpStatus.UNAUTHORIZED);				
						
					}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			}else{   			
				   logger.info("Refresh Token is invalid ");
				   apiResponse.setStatusCode(526);
     		       apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
     		       return new ResponseEntity(apiResponse, HttpStatus.UNAUTHORIZED);
			 }		
		} catch (Exception e) {
			logger.error("Exception in  getUserOrgApartmentList: "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
			return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity UserVO getUserOrgApartmentList(@RequestBody UserVO orgVO )")	;
		return new ResponseEntity(userVO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/changeApartment", method = RequestMethod.POST)
	public ResponseEntity changeApartment(@RequestHeader("X-RefreshToken") String refreshToken, @RequestBody UserVO orgVO ){
		UserVO userVO=new UserVO();
		 APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public ResponseEntity UserVO changeApartment(UserVO orgVO )")	;
		try {
			logger.debug("Here the userID "+orgVO.getUserID()+" orgID: "+orgVO.getOrgID()+" appID : "+ orgVO.getAppID()+" roleID: "+orgVO.getRoleID());
			orgVO.setRefreshToken(refreshToken);
			UserVO decrptVO=enDeUtility.decodeStringToBean(refreshToken);
			
			//Check refresh token validity
			Boolean isRefreshTokenValid= userService.verifyRefreshToken(decrptVO);			
			if(isRefreshTokenValid){	
				
				if(orgVO.getUserID().length()>0 && orgVO.getOrgID().length()>0 && orgVO.getAppID().length()>0 && orgVO.getRoleID().length()>0){	
				   userVO=userService.changeUserApartment(orgVO);
				 
				   if(!userVO.getLoginSuccess()){
						apiResponse.setStatusCode(userVO.getStatusCode());
						apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
						return new ResponseEntity(apiResponse, HttpStatus.UNAUTHORIZED);			
	    			}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			}else{   			
				   logger.info("Refresh Token is invalid ");
				   apiResponse.setStatusCode(526);
        		   apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
        		   return new ResponseEntity(apiResponse,HttpStatus.UNAUTHORIZED);
			 }	
			
		} catch (Exception e) {
			logger.error("Exception in changeApartment "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
			return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity UserVO changeApartment(UserVO orgVO )")	;
		return new ResponseEntity(userVO, HttpStatus.OK);
	}
	
		
	@RequestMapping(value="/memberRegister", method = RequestMethod.POST)
	public ResponseEntity memberAppRegisterUser(@RequestBody UserVO requestVO ){
		String success=null;
		UserVO userVO=new UserVO();
		 APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public ResponseEntity memberAppRegisterUser(@RequestBody UserVO requestVO )")	;
		try {
			logger.info("Member App Register Requested EmailID: "+requestVO.getLoginName()+" AppID : "+ requestVO.getAppID());
		
			if(requestVO.getAppID().length()>0 && requestVO.getLoginName().length()>0){	
			   requestVO.setRoleID("7");
			   userVO=userService.memberAppRegisterUser(requestVO);
			   
			   if(userVO.getLoginSuccess()){
				   apiResponse.setStatusCode(userVO.getStatusCode());
				   apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));	
   			   }else{
   				  apiResponse.setStatusCode(userVO.getStatusCode());
				  apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
				  return new ResponseEntity(apiResponse, HttpStatus.UNAUTHORIZED);	  				   
   			   }
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in  memberAppRegisterUser: "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
			return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity memberAppRegisterUser(@RequestBody UserVO orgVO )")	;
		return new ResponseEntity(apiResponse, HttpStatus.OK);
	}
  
	@RequestMapping(value="/createSupportTicket", method = RequestMethod.POST)
	public ResponseEntity createSupportTicket(@RequestBody TicketVO requestVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			int success=0;
			logger.debug("Entry : public ResponseEntity createSupportTicket(@RequestBody TicketVO requestVO)");
		try {
				if( requestVO.getName().length()>0 && requestVO.getEmail().length()>0 && requestVO.getMobile().length()>0 &&  requestVO.getMessage().length()>0 
						&& requestVO.getType().length()>0){
					success=memberServiceBean.createSupportTicket(requestVO);		
					
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("We have registered your request successfully");
					
												
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
		} catch (Exception e) {
			logger.error("Exception in createSupportTicket "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
			logger.debug("Exit : public ResponseEntity createSupportTicket(@RequestBody TicketVO requestVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
	}
	

	@RequestMapping(value="/validate/request", method = RequestMethod.POST)
	public ResponseEntity validateRequest(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestHeader(HEADER_URL) String url,
	 @RequestHeader(HEADER_UserID) String userID,@RequestHeader(HEADER_OrgID) String orgID,@RequestHeader(value=HEADER_ApartmentID, required = false) String apartmentID,
	 @RequestHeader HttpHeaders headers){

	MultiValueMap<String, String> headerMap = new LinkedMultiValueMap<String, String>();
	String success=null;
	UserVO requestVO=new UserVO();
	UserVO responseVO=new UserVO();
	APIResponseVO apiResponse=new APIResponseVO();
	logger.debug("Entry : public ResponseEntity validateRequest(@RequestHeader HttpHeaders headers)") ;
	try {

	requestVO.setAccessToken(accessToken);
	requestVO.setRequestURL(url);
	requestVO.setUserID(userID);
	requestVO.setOrgID(orgID);

	if(apartmentID!=null){
	requestVO.setApartmentID(Integer.parseInt(apartmentID));
	}

	if(requestVO.getAccessToken()!=null && requestVO.getRequestURL()!=null){

	responseVO=tokenFilter.validateTokenRequest(requestVO);
	headers.set(HEADER_SECURITY_TOKEN, responseVO.getAccessToken());
	   
	Map map = new HashMap<String, String>();
	   
	       map.put(HEADER_SECURITY_TOKEN,headers.getFirst(HEADER_SECURITY_TOKEN));
	       map.put(HEADER_UserID,headers.getFirst(HEADER_UserID));
	       map.put(HEADER_OrgID,headers.getFirst(HEADER_OrgID));
	       map.put(HEADER_URL,headers.getFirst(HEADER_URL));
	   headerMap.setAll(map);
	   
	if(responseVO.getStatusCode()==200){
	apiResponse.setStatusCode(responseVO.getStatusCode());
	apiResponse.setMessage(responseVO.getMessage());

	}else if(responseVO.getStatusCode()==524){
	apiResponse.setStatusCode(responseVO.getStatusCode());
	apiResponse.setMessage(responseVO.getMessage());
	return new ResponseEntity(apiResponse, HttpStatus.FORBIDDEN);
	}else {
	apiResponse.setStatusCode(responseVO.getStatusCode());
	apiResponse.setMessage(responseVO.getMessage());
	return new ResponseEntity(apiResponse, HttpStatus.UNAUTHORIZED);
	}
	}else{
	apiResponse.setStatusCode(525);
	apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
	return new ResponseEntity(apiResponse, HttpStatus.UNAUTHORIZED);
	}



	} catch (Exception e) {
	// TODO Auto-generated catch block
	logger.error("Exception in validateRequest: "+e);
	apiResponse.setStatusCode(400);
	apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
	return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
	}
	logger.debug("Exit : public ResponseEntity validateRequest(@RequestHeader HttpHeaders headers)") ;
	return new ResponseEntity(apiResponse,headerMap, HttpStatus.OK);


	}

//profile organization list
	
	@RequestMapping(value="/profileOrganizationList", method = RequestMethod.POST)
	public ResponseEntity getUserProfileOrgList(@RequestHeader("X-RefreshToken") String refreshToken, @RequestBody UserVO orgVO ){
		String success=null;
		UserVO userVO=new UserVO();
		 APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public ResponseEntity UserVO getUserProfileOrgList(@RequestBody UserVO orgVO )")	;
		try {
			logger.debug("userID: "+orgVO.getUserID()+" appID : "+ orgVO.getAppID()+" UserType: "+orgVO.getUserType());
			UserVO decrptVO=enDeUtility.decodeStringToBean(refreshToken);
			Boolean isRefreshTokenValid= userService.verifyRefreshToken(decrptVO);
			
			if(isRefreshTokenValid){	
				if(orgVO.getUserID().length()>0 && orgVO.getAppID().length()>0 && orgVO.getUserType().length()>0 ){	
				   userVO=userService.getProfileOrganizationList(orgVO.getUserID(),orgVO.getAppID(),"0",orgVO.getUserType());
				   if(!userVO.getLoginSuccess()){
						apiResponse.setStatusCode(userVO.getStatusCode());
						apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
						return new ResponseEntity(apiResponse, HttpStatus.UNAUTHORIZED);				
						
					}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			}else{   			
				   logger.info("Refresh Token is invalid ");
				   apiResponse.setStatusCode(526);
     		       apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
     		       return new ResponseEntity(apiResponse, HttpStatus.UNAUTHORIZED);
			 }		
		} catch (Exception e) {
			logger.error("Exception in  getUserProfileOrgList: "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
			return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity UserVO getUserProfileOrgList(@RequestBody UserVO orgVO )")	;
		return new ResponseEntity(userVO, HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/changeProfileOrg", method = RequestMethod.POST)
	public ResponseEntity changeProfileOrg(@RequestHeader("X-RefreshToken") String refreshToken, @RequestBody UserVO orgVO ){
		UserVO userVO=new UserVO();
		 APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public ResponseEntity UserVO changeProfileOrg(UserVO orgVO )")	;
		try {
			logger.debug("Here the userID "+orgVO.getUserID()+" orgID: "+orgVO.getOrgID()+" appID : "+ orgVO.getAppID()+" roleID: "+orgVO.getRoleID()+" UserType: "+orgVO.getUserType());
			orgVO.setRefreshToken(refreshToken);
			UserVO decrptVO=enDeUtility.decodeStringToBean(refreshToken);
			
			//Check refresh token validity
			Boolean isRefreshTokenValid= userService.verifyRefreshToken(decrptVO);			
			if(isRefreshTokenValid){	
				
				if(orgVO.getUserID().length()>0 && orgVO.getOrgID().length()>0 && orgVO.getAppID().length()>0 && orgVO.getRoleID().length()>0 && orgVO.getUserType().length()>0){	
				   userVO=userService.changeProfileOrg(orgVO);
				 
				   if(!userVO.getLoginSuccess()){
						apiResponse.setStatusCode(userVO.getStatusCode());
						apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
						return new ResponseEntity(apiResponse, HttpStatus.UNAUTHORIZED);			
	    			}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			}else{   			
				   logger.info("Refresh Token is invalid ");
				   apiResponse.setStatusCode(526);
        		   apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
        		   return new ResponseEntity(apiResponse,HttpStatus.UNAUTHORIZED);
			 }	
			
		} catch (Exception e) {
			logger.error("Exception in changeProfileOrg "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
			return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity UserVO changeProfileOrg(UserVO orgVO )")	;
		return new ResponseEntity(userVO, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value="/customer/authenticate", method = RequestMethod.POST)
	public ResponseEntity authenticateCustomer(@RequestHeader("Authorization") String authorzation,@RequestHeader(value=HEADER_AndroidDeviceID, required = false) String andriodDeiviceID,@RequestBody UserVO loginUser ){
		String success=null;
		UserVO userVO=new UserVO();
		 APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public @ResponseBody UserVO authenticateCustomer(@RequestBody UserVO loginUser )")	;
		try {
							
			String base64Credentials = authorzation.substring("Basic".length()).trim();
			byte[] decordedValue = 	new Base64().decode(base64Credentials);			
			String credentials = new String(decordedValue, Charset.forName("UTF-8"));;
			
			final String[] values = credentials.split(":",2);
			loginUser.setLoginName(values[0]);
			loginUser.setPassword(values[1]);
			
			logger.info("**********Litehouse API Server Authenticate User Method********");
			logger.info("Login ID:  "+loginUser.getLoginName());
			logger.info("Password: "+loginUser.getPassword());
			logger.info("App ID: "+loginUser.getAppID());
			logger.info("User Type: "+loginUser.getUserType());
			logger.info("Org ID: "+loginUser.getOrgID());
			
			if(loginUser.getLoginName().length()>0 && loginUser.getPassword().length()>0 && loginUser.getAppID().length()>0&&
					loginUser.getOrgID().length()>0 && loginUser.getUserType().length()>0){
				loginUser.setAndriodDeviceID(andriodDeiviceID);
				userVO=userService.authenticateUser(loginUser);
								
				if(!userVO.getLoginSuccess()){
					apiResponse.setStatusCode(userVO.getStatusCode());
					apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
					logger.debug("User ID: "+userVO.getUserID()+" failed Message : " + configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
					return new ResponseEntity(apiResponse, HttpStatus.UNAUTHORIZED);				
					
				}else{
					//update device id					
					if(andriodDeiviceID==null){
						
					}else if((andriodDeiviceID.length()>0) && (andriodDeiviceID.equals(userVO.getAndriodDeviceID())==false)){
					  loginUser.setUserID(userVO.getUserID());
					  loginUser.setAndriodDeviceID(andriodDeiviceID);
						Boolean isUpdated=userService.updateUserAndriodDeviceID(loginUser);
					}
					
					//Get details of org
					 UserVO orgList=new UserVO();
					 orgList=userService.getProfileOrganizationList(userVO.getUserID(),loginUser.getAppID(),loginUser.getOrgID(),loginUser.getUserType());
					 logger.info("Org size: "+orgList.getLoginSuccess());
					 if(orgList.getLoginSuccess()){
										
					 SocietyVO orgVO=(SocietyVO)orgList.getSocietyAppList().get(0);
					 
					 userVO.setRoleID(orgVO.getRoleID());
					 userVO.setApartmentID(orgVO.getApartmentID());
					 userVO.setOrgID(loginUser.getOrgID());
					 loginUser.setLoginName(userVO.getLoginName());
					 loginUser.setMobile(userVO.getMobile());
					 loginUser.setLastLogin(userVO.getLastLogin());
					 loginUser.setFullName(userVO.getFullName());
					 loginUser.setIsLocked(userVO.getIsLocked());
					 loginUser.setPasswordValidity(userVO.getPasswordValidity());
					 loginUser.setPasswordType(userVO.getPasswordType());
					 userVO=userService.changeProfileOrg(userVO);
					   
					 userVO.setLoginName(loginUser.getLoginName());
					 userVO.setMobile(loginUser.getMobile());
					 userVO.setLastLogin(loginUser.getLastLogin());
					 userVO.setFullName(loginUser.getFullName());
					 userVO.setIsLocked(loginUser.getIsLocked());
					 userVO.setPasswordValidity(loginUser.getPasswordValidity()); 
					 userVO.setPasswordType(loginUser.getPasswordType()); 
					 userVO.setUserType(loginUser.getUserType());
					 userVO.setSocietyVO(orgVO);
					}else{
						apiResponse.setStatusCode(userVO.getStatusCode());
						apiResponse.setMessage(configManager.getPropertiesValue("error."+orgList.getStatusCode().toString()));
						return new ResponseEntity(apiResponse, HttpStatus.UNAUTHORIZED);	
					}
					
				}
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
			
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception in authenticateCustomer: "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
			return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody UserVO authenticateCustomer(@RequestBody UserVO loginUser )")	;
		return new ResponseEntity(userVO, HttpStatus.OK);
	}
	
	
	// ================ Self Registration Process APIs ==========================//
	
	@RequestMapping(value="/signUpForTrial", method = RequestMethod.POST)
	public ResponseEntity signUpForTrial(@RequestBody UserVO userVO) {
		APIResponseVO apiResponse=new APIResponseVO();
		
		logger.debug("Entry : public void signUpForTrial(@RequestBody UserVO userVO)  ");
		try {
			if(userVO.getAppID().length()>0&&userVO.getAppID()!=null){
			userVO=userService.signUpForTrial(userVO);
			SocietyVO orgVO=userVO.getSocietyVO();
			if(userVO.getStatusCode()==550){
				apiResponse.setStatusCode(userVO.getStatusCode());
				apiResponse.setMessage(orgVO.getSocietyName()+" "+configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));					
				return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				
			}else if(userVO.getStatusCode()==532){
				apiResponse.setStatusCode(userVO.getStatusCode());
				apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));					
				return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				
			}else{
				apiResponse.setStatusCode(200);
				apiResponse.setMessage("Sign up for trial created. ");
			}
		       
          		
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			logger.error("Exception  "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		
		logger.debug("Exit : public void signUpForTrial(@RequestBody UserVO userVO)  ");						
		return new ResponseEntity(apiResponse, HttpStatus.OK);
 
	}
	
	
	@RequestMapping(value="/validateOTP", method = RequestMethod.POST)
	public ResponseEntity validateOneTimePassword(@RequestBody UserVO userVO ){
		String success=null;
		Boolean isValid=false;
		 APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public ResponseEntity forgotPassword(@RequestBody UserVO userVO )")	;
		try {
			logger.debug("UserID "+userVO.getUserID()+" appID : "+ userVO.getAppID());
		
			if(userVO.getOTP().length()>0 && userVO.getAppID().length()>0){	
				isValid=userService.validateOneTimePassword(userVO.getUserID(),userVO.getOTP());
			   
				if(isValid==false){
					apiResponse.setStatusCode(406);
					apiResponse.setMessage("One time password is not verified");
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
					
				}else{
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("One time password is verified");
					
										
				}
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in  validateOneTimePassword: "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
			return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity validateOneTimePassword(@RequestBody UserVO orgVO )")	;
		return new ResponseEntity(apiResponse, HttpStatus.OK);
	}
	
	
	
	
}
