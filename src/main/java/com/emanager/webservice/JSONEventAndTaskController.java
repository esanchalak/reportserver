package com.emanager.webservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.emanager.server.accounts.DataAccessObjects.LedgerTrnsVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.adminReports.Services.PrintReportService;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.dashBoard.Services.AccountDashBoardService;
import com.emanager.server.dashBoard.Services.DashBoardService;
import com.emanager.server.financialReports.services.BalanceSheetService;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.financialReports.services.TrialBalanceService;
import com.emanager.server.invoice.Services.InvoiceService;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.taskAndEventManagement.Service.EventService;
import com.emanager.server.taskAndEventManagement.Service.NotificationService;
import com.emanager.server.taskAndEventManagement.Service.TaskService;
import com.emanager.server.taskAndEventManagement.valueObject.EmailMessage;
import com.emanager.server.taskAndEventManagement.valueObject.EventVO;
import com.emanager.server.taskAndEventManagement.valueObject.NotificationVO;
import com.emanager.server.taskAndEventManagement.valueObject.SMSMessage;
import com.emanager.server.taskAndEventManagement.valueObject.TaskVO;
import com.emanager.server.userManagement.service.UserService;
  
	@Controller
	@RequestMapping("/eventAndTask")
	
	public class JSONEventAndTaskController {
		private static final Logger logger = Logger.getLogger(JSONEventAndTaskController.class);
		@Autowired
	    EventService eventService;
	    @Autowired
	    TaskService taskService;
	    @Autowired
	    NotificationService notificationService;
	    @Autowired
	    MemberService memberServiceBean;
	   
	    public ConfigManager configManager =new ConfigManager();
		
	    
	    //====================== Event Related APIs ===========================//
		
	    /*------------------API for getting list of events -----------------*/
	    @RequestMapping(value="/eventList", method = RequestMethod.POST)
		public ResponseEntity getEventList(@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			EventVO eventVO=new EventVO();
			List eventList=new ArrayList();
			logger.debug("Entry : public ResponseEntity getEventList(@RequestBody RequestWrapper requestWrapper)")	;
			try {
                
								
				   eventList=eventService.getEventList(requestWrapper.getOrgID());
				 
				  if(eventList.size()>0){
					eventVO.setEventList((ArrayList<EventVO>) eventList);
					eventVO.setStatusCode(200);
				   
				  }else{
					  apiResponse.setStatusCode(400);
					  apiResponse.setMessage("No events are available for given organization ");
					  return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				  }
				
			} catch (Exception e) {
				logger.error("Exception in getEventList "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity getEventList(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(eventVO, HttpStatus.OK);	
	 
		}
	    
	    /*------------------API for getting list of events -----------------*/
	    @RequestMapping(value="/eventListForOrg", method = RequestMethod.POST)
		public ResponseEntity getEventListForOrg(@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			JSONResponseVO jsonVO=new JSONResponseVO();
			EventVO eventVO=new EventVO();
			List eventList=new ArrayList();
			logger.debug("Entry : public ResponseEntity getEventListForOrg(@RequestBody RequestWrapper requestWrapper)")	;
			try {                
								
				   eventList=eventService.getEventListForOrg(requestWrapper.getOrgID());
				 
				   if(eventList.size()==0){
					   jsonVO.setStatusCode(0);
					}else{
						jsonVO.setStatusCode(1);
						jsonVO.setObjectList(eventList);
					}	
				
			} catch (Exception e) {
				logger.error("Exception in eventListForOrg "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity eventListForOrg(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jsonVO, HttpStatus.OK);	
	 
		}
		
	    
	    /*------------------API for adding new event -----------------*/
	    @RequestMapping(value="/addEvent", method = RequestMethod.POST)
		public ResponseEntity addEvent(@RequestBody EventVO eventVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			JSONResponseVO jsonVO=new JSONResponseVO();
			int successFlag=0;
			logger.debug("Entry : public ResponseEntity addEvent(@RequestBody EventVO eventVO)")	;
			try {
                	
				if(eventVO.getSocietyID()>0){
								
				   successFlag=eventService.addEvent(eventVO);
				 
				  if(successFlag!=0){
					  apiResponse.setStatusCode(200);
					  apiResponse.setMessage("Event has been added successfully. ");
				  }else{
					  apiResponse.setStatusCode(400);
					  apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					  return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				  }
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				logger.error("Exception in getEventList "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity addEvent(@RequestBody EventVO eventVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);	
	 
		}
	    
	    /*------------------API for updating event -----------------*/
	    @RequestMapping(value="/updateEvent", method = RequestMethod.POST)
		public ResponseEntity updateEvent(@RequestBody EventVO eventVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			int successFlag=0;
			logger.debug("Entry : public ResponseEntity updateEvent(@RequestBody EventVO eventVO)")	;
			try {
                	
				if(eventVO.getSocietyID()>0){
								
				   successFlag=eventService.updateEvent(eventVO, eventVO.getEventID());
				 
				  if(successFlag!=0){
					  apiResponse.setStatusCode(200);
					  apiResponse.setMessage("Event has been updated successfully. ");
				  }else{
					  apiResponse.setStatusCode(400);
					  apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					  return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				  }
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				logger.error("Exception in update Event "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity updateEvent(@RequestBody EventVO eventVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);	
	 
		}
	    
	    /*------------------API for updating event -----------------*/
	    @RequestMapping(value="/updateEventFrequency", method = RequestMethod.POST)
		public ResponseEntity updateEventFrequency(@RequestBody EventVO eventVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			JSONResponseVO jsonVO=new JSONResponseVO();
			int successFlag=0;
			logger.debug("Entry : public ResponseEntity updateEvent(@RequestBody EventVO eventVO)")	;
			try {
                	
				if(eventVO.getEventList().size()>0){
								
				   successFlag=eventService.updateEventFreuency(eventVO.getEventList());
				 
				  if(successFlag!=0){
					  jsonVO.setStatusCode(200);
					 
				  }else{
					  apiResponse.setStatusCode(400);
					  apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					  return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				  }
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				logger.error("Exception in update Event "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity updateEvent(@RequestBody EventVO eventVO)")	;						
			return new ResponseEntity(jsonVO, HttpStatus.OK);	
	 
		}
	    
	    /*------------------API for updating event -----------------*/
	    @RequestMapping(value="/updateEventDateFrequency", method = RequestMethod.POST)
		public ResponseEntity updateEventDateFrequency(@RequestBody EventVO eventVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			JSONResponseVO jsonVO=new JSONResponseVO();
			int successFlag=0;
			logger.debug("Entry : public ResponseEntity updateEventDateFrequency(@RequestBody EventVO eventVO)")	;
			try {
                	
				if( eventVO.getEventID()>0){
								
				   successFlag=eventService.updateEventDateFrequency(eventVO, eventVO.getEventID());
				 
				  if(successFlag!=0){
					  apiResponse.setStatusCode(200);
					  apiResponse.setMessage("Event has been updated successfully.");
					 
				  }else{
					  apiResponse.setStatusCode(400);
						apiResponse.setMessage("Unable to updated event details.");
					  return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				  }
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				logger.error("Exception in update Event and frequency "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity updateEventDateFrequency(@RequestBody EventVO eventVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);	
	 
		}
	    
	    /*------------------API for deleting events -----------------*/
	    @RequestMapping(value="/deleteEvent", method = RequestMethod.POST)
		public ResponseEntity deleteEvent(@RequestBody EventVO eventVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			int successFlag=0;
			logger.debug("Entry : public ResponseEntity deleteEvent(@RequestBody EventVO eventVO)")	;
			try {
                	
				if(eventVO.getSocietyID()>0){
								
				   successFlag=eventService.deleteEvent(eventVO.getEventID());
				 
				  if(successFlag!=0){
					  apiResponse.setStatusCode(200);
					  apiResponse.setMessage("Event has been deleted successfully. ");
				  }else{
					  apiResponse.setStatusCode(400);
					  apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					  return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				  }
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				logger.error("Exception in delete Event "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity deleteEvent(@RequestBody EventVO eventVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);	
	 
		}
	    
	    //====================== Task Related APIs ===========================//
	    
	    /*------------------API for getting list of tasks -----------------*/
	    @RequestMapping(value="/taskList", method = RequestMethod.POST)
	 		public ResponseEntity getTaskList(@RequestBody RequestWrapper requestWrapper) {
	 			APIResponseVO apiResponse=new APIResponseVO();
	 			TaskVO taskVO=new TaskVO();
	 			List taskList=new ArrayList();
	 			logger.debug("Entry : public ResponseEntity getTaskList(@RequestBody RequestWrapper requestWrapper)")	;
	 			try {
	                 
	 				 								
	 				   taskList=taskService.getTaskList(requestWrapper.getOrgID());
	 				 
	 				  if(taskList.size()>0){
	 					taskVO.setTaskList((ArrayList<TaskVO>) taskList);
	 					taskVO.setStatusCode(200);
	 				   
	 				  }else{
	 					  apiResponse.setStatusCode(400);
	 					  apiResponse.setMessage("No tasks are available for given organization ");
	 					  return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
	 				  }
	 				
	 			} catch (Exception e) {
	 				logger.error("Exception in getTaskList "+e);
	 				apiResponse.setStatusCode(404);
	 				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
	 				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
	 			}
	 			logger.debug("Exit : public ResponseEntity getTaskList(@RequestBody RequestWrapper requestWrapper)")	;						
	 			return new ResponseEntity(taskVO, HttpStatus.OK);	
	 	 
	 		}
	    
	    
	    /*------------------API for adding tasks -----------------*/
	    @RequestMapping(value="/addTask", method = RequestMethod.POST)
			public ResponseEntity addTask(@RequestBody TaskVO taskVO) {
				APIResponseVO apiResponse=new APIResponseVO();
				int successFlag=0;
				logger.debug("Entry : public ResponseEntity getEventList(@RequestBody RequestWrapper requestWrapper)")	;
				try {
	                	
					if(taskVO.getOrgID()>0){
									
					   successFlag=taskService.addTask(taskVO);
					 
					  if(successFlag!=0){
						  apiResponse.setStatusCode(200);
						  apiResponse.setMessage("Task has been added successfully. ");
					  }else{
						  apiResponse.setStatusCode(400);
						  apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						  return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					  }
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					
				} catch (Exception e) {
					logger.error("Exception in add Task "+e);
					apiResponse.setStatusCode(404);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
				}
				logger.debug("Exit : public ResponseEntity getEventList(@RequestBody RequestWrapper requestWrapper)")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);	
		 
			}
	    
	    /*------------------API for updating tasks -----------------*/
	    @RequestMapping(value="/updateTask", method = RequestMethod.POST)
			public ResponseEntity updateTask(@RequestBody TaskVO taskVO) {
				APIResponseVO apiResponse=new APIResponseVO();
				int successFlag=0;
				logger.debug("Entry : public ResponseEntity updateTask(@RequestBody TaskVO taskVO)")	;
				try {
	                	
					if(taskVO.getOrgID()>0){
									
					   successFlag=taskService.updateTask(taskVO,taskVO.getTaskID());
					 
					  if(successFlag!=0){
						  apiResponse.setStatusCode(200);
						  apiResponse.setMessage("Task has been updated successfully. ");
					  }else{
						  apiResponse.setStatusCode(400);
						  apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						  return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					  }
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					
				} catch (Exception e) {
					logger.error("Exception in update Task "+e);
					apiResponse.setStatusCode(404);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
				}
				logger.debug("Exit : public ResponseEntity updateTask(@RequestBody TaskVO taskVO)")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);	
		 
			}
		    
	    
	    /*------------------API for deleting tasks -----------------*/
	    @RequestMapping(value="/deleteTask", method = RequestMethod.POST)
		public ResponseEntity deleteTask(@RequestBody TaskVO taskVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			int successFlag=0;
			logger.debug("Entry : public ResponseEntity deleteTask(@RequestBody TaskVO taskVO)")	;
			try {
                	
				if(taskVO.getOrgID()>0){
								
				   successFlag=taskService.deleteTask(taskVO.getTaskID());
				 
				  if(successFlag!=0){
					  apiResponse.setStatusCode(200);
					  apiResponse.setMessage("Task has been deleted successfully. ");
				  }else{
					  apiResponse.setStatusCode(400);
					  apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					  return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				  }
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				logger.error("Exception in delete Task "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity deleteTask(@RequestBody TaskVO taskVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);	
	 
		}
	    
	    
	    /*------------------API for Sending Payment reminder tasks -----------------*/
	    @RequestMapping(value="/sendPaymentReminder", method = RequestMethod.POST)
		public ResponseEntity sendPaymentReminder(@RequestBody EventVO eventVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			int successFlag=0;
			logger.debug("Entry : public ResponseEntity sendPaymentReminder(@RequestBody MemberVO memberVO)")	;
			try {
                	
				if(eventVO.getMemberID()>0){
								
				   NotificationVO notificationVO=notificationService.getMemberBalance(eventVO);
				 
				   		
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				logger.error("Exception in sendPaymentReminder(@RequestBody MemberVO memberVO) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity sendPaymentReminder(@RequestBody MemberVO memberVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);	
	 
		}
	    
	    /*------------------API for Sending Payment reminder tasks -----------------*/
	    @RequestMapping(value="/sendPaymentReminderToSociety", method = RequestMethod.POST)
		public ResponseEntity sendPaymentReminderToSociety(@RequestBody EventVO eventVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			int successFlag=0;
			logger.debug("Entry : public ResponseEntity sendPaymentReminder(@RequestBody MemberVO memberVO)")	;
			try {
                	
				if(eventVO.getSocietyID()>0){
								
				   NotificationVO notificationVO=notificationService.getMemberBalanceForSociety(eventVO);
				 
			
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				logger.error("Exception in sendPaymentReminder(@RequestBody MemberVO memberVO) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity sendPaymentReminder(@RequestBody MemberVO memberVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);	
	 
		}
	   
	    
	    /*------------------API for Sending bill reminders to individual customers in case of Organisations -----------------*/
	    @RequestMapping(value="/sendBillReminderToCustomer", method = RequestMethod.POST)
		public ResponseEntity sendBillPaymentReminderToCustomer(@RequestBody RequestWrapper reqVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			int successFlag=0;
			logger.debug("Entry : public ResponseEntity sendBillPaymentReminderToCustomer(@RequestBody RequestWrapper reqVO)")	;
			try {
                	
				if(reqVO.getLedgerID()>0){
								
				  // NotificationVO notificationVO=notificationService.getCustomerBalance(reqVO.getLedgerID(),reqVO.getOrgID());
				 
				   		
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				logger.error("Exception in sendBillPaymentReminderToCustomer(@RequestBody RequestWrapper reqVO) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity sendPaymentReminder(@RequestBody RequestWrapper reqVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);	
	 
		}
	    
	    /*------------------API for Sending bill reminders to customers of an organisation -----------------*/
	    @RequestMapping(value="/sendBillReminderToOrg", method = RequestMethod.POST)
		public ResponseEntity sendCustomerBillPaymentReminderOfOrg(@RequestBody RequestWrapper reqVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			int successFlag=0;
			logger.debug("Entry : public ResponseEntity sendCustomerBillPaymentReminderOfOrg(@RequestBody RequestWrapper reqVO)")	;
			try {
                	
				if(reqVO.getOrgID()>0){
								
				   NotificationVO notificationVO=notificationService.sendCustomerBillPaymentReminderOfOrg(reqVO.getOrgID());
				 
			
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				logger.error("Exception in sendCustomerBillPaymentReminderOfOrg(@RequestBody RequestWrapper reqVO) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity sendCustomerBillPaymentReminderOfOrg(@RequestBody RequestWrapper reqVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);	
	 
		}
	    
	    /*------------------API for Sending Nomination reminder tasks -----------------*/
	    @RequestMapping(value="/sendNominationReminder", method = RequestMethod.POST)
		public ResponseEntity sendNominationReminder(@RequestBody MemberVO memberVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			NotificationVO notificationVO=new NotificationVO();
			int successFlag=0;
			logger.debug("Entry : public ResponseEntity sendNominationReminder(@RequestBody MemberVO memberVO)")	;
			try {
                	
				if(memberVO.getMemberID()>0){
								
				  notificationVO=notificationService.getNominationStatusForMember(memberVO.getMemberID());
				 
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				logger.error("Exception in sendPaymentReminder(@RequestBody MemberVO memberVO) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity sendPaymentReminder(@RequestBody MemberVO memberVO)")	;						
			return new ResponseEntity(notificationVO, HttpStatus.OK);	
	 
		}
		
	    
	    /*------------------API for Sending Nomination reminder tasks -----------------*/
	    @RequestMapping(value="/sendNominationReminderToSociety", method = RequestMethod.POST)
		public ResponseEntity sendNominationReminderToSociety(@RequestBody EventVO eventVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			NotificationVO notificationVO=new NotificationVO();
			int successFlag=0;
			logger.debug("Entry : public ResponseEntity sendNominationReminderToSociety(@RequestBody EventVO eventVO)")	;
			try {
                	
				if(eventVO.getSocietyID()>0){
								
				  notificationVO=notificationService.sendNominationReminderToSociety(eventVO);
				 
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				logger.error("Exception in  sendNominationReminderToSociety(@RequestBody EventVO eventVO) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity  sendNominationReminderToSociety(@RequestBody EventVO eventVO)")	;						
			return new ResponseEntity(notificationVO, HttpStatus.OK);	
	 
		}
	    
	    
	    
	    /*------------------API for Sending Index II Reminder to Member tasks -----------------*/
	    @RequestMapping(value="/sendDocumentReminder", method = RequestMethod.POST)
		public ResponseEntity sendIndexIIReminder(@RequestBody EventVO eventVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			NotificationVO notificationVO=new NotificationVO();
			int successFlag=0;
			logger.debug("Entry : public ResponseEntity sendDocumentReminder(@RequestBody MemberVO memberVO)")	;
			try {
                	
				if(eventVO.getMemberID()>0){
								
				  notificationVO=notificationService.sendDocumentReminder(eventVO);
				 
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				logger.error("Exception in sendDocumentReminder(@RequestBody MemberVO memberVO) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity sendDocumentReminder(@RequestBody MemberVO memberVO)")	;						
			return new ResponseEntity(notificationVO, HttpStatus.OK);	
	 
		}
		
	    
	    /*------------------API for Sending IndexII reminder tasks to society -----------------*/
	    @RequestMapping(value="/sendDocumentReminderToSociety", method = RequestMethod.POST)
		public ResponseEntity sendDocumentReminderToSociety(@RequestBody EventVO eventVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			NotificationVO notificationVO=new NotificationVO();
			int successFlag=0;
			logger.debug("Entry : public ResponseEntity sendDocumentReminderToSociety(@RequestBody EventVO eventVO)")	;
			try {
                	
				if(eventVO.getSocietyID()>0){
								
				  notificationVO=notificationService.sendDocumentReminderToSociety(eventVO);
				 
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				logger.error("Exception in  sendDocumentReminderToSociety(@RequestBody EventVO eventVO) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity  sendDocumentReminderToSociety(@RequestBody EventVO eventVO)")	;						
			return new ResponseEntity(notificationVO, HttpStatus.OK);	
	 
		}
	    
	    
	    /*------------------API for Sending IndexII reminder tasks to society -----------------*/
	    @RequestMapping(value="/sendSuspenseStatus", method = RequestMethod.POST)
		public ResponseEntity sendSuspenseStatusToSociety(@RequestBody EventVO eventVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			NotificationVO notificationVO=new NotificationVO();
			int successFlag=0;
			logger.debug("Entry : public ResponseEntity sendSuspenseStatusToSociety(@RequestBody EventVO eventVO)")	;
			try {
                	
				if(eventVO.getSocietyID()>0){
								
				  notificationVO=notificationService.sendSuspenseStatusToSociety(eventVO);
				 
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				logger.error("Exception in  sendSuspenseStatusToSociety(@RequestBody EventVO eventVO) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity  sendSuspenseStatusToSociety(@RequestBody EventVO eventVO)")	;						
			return new ResponseEntity(notificationVO, HttpStatus.OK);	
	 
		}
		
	    /*------------------API for Sending IndexII reminder tasks to society -----------------*/
	    @RequestMapping(value="/sendPendingTransactionStatus", method = RequestMethod.POST)
		public ResponseEntity sendPendingTransactionStatusToSociety(@RequestBody EventVO eventVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			NotificationVO notificationVO=new NotificationVO();
			int successFlag=0;
			logger.debug("Entry : public ResponseEntity sendPendingTransactionStatusToSociety(@RequestBody EventVO eventVO)")	;
			try {
                	
				if(eventVO.getSocietyID()>0){
								
				  notificationVO=notificationService.sendPendingTransactionStatusToSociety(eventVO);
				 
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				logger.error("Exception in  sendPendingTransactionStatusToSociety(@RequestBody EventVO eventVO) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity  sendPendingTransactionStatusToSociety(@RequestBody EventVO eventVO)")	;						
			return new ResponseEntity(notificationVO, HttpStatus.OK);	
	 
		}
	    
	    @RequestMapping(value="/auditDocReminder", method = RequestMethod.POST)
		public ResponseEntity sendAuditDocReminder(@RequestBody EventVO eventVO) {
			JSONResponseVO jsonResponse=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
		    int success=0;
				logger.debug("Entry : public ResponseEntity sendAuditDocReminder(@RequestBody EventVO eventVO)" );
			try {
				
				if(eventVO.getSocietyID()>0 ){
				
				success=eventService.sendAuditDocReminderEvent(eventVO);
				if(success==1){
					jsonResponse.setStatusCode(200);
				
				}
				  
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in public ResponseEntity sendAuditDocReminder(@RequestBody EventVO eventVO) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity sendAuditDocReminder(@RequestBody EventVO eventVO)")	;						
			return new ResponseEntity(jsonResponse, HttpStatus.OK);
	 
		}
	    
	    
	    @RequestMapping(value="/sendBankStatementReminder", method = RequestMethod.POST)
		public ResponseEntity sendBankStatementReminder(@RequestBody EventVO eventVO) {
			JSONResponseVO jsonResponse=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
		    int success=0;
				logger.debug("Entry : public ResponseEntity sendBankStatementReminder(@RequestBody EventVO eventVO)" );
			try {
				
				if(eventVO.getSocietyID()>0 ){
				
				success=notificationService.sendBankStatementRequestReminder(eventVO);
				if(success==1){
					jsonResponse.setStatusCode(200);
				
				}
				  
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in public ResponseEntity sendBankStatementReminder(@RequestBody EventVO eventVO) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity sendBankStatementReminder(@RequestBody EventVO eventVO)")	;						
			return new ResponseEntity(jsonResponse, HttpStatus.OK);
	 
		}
	    
	    
	    @RequestMapping(value="/sendCashBalanceReminder", method = RequestMethod.POST)
		public ResponseEntity sendCashBalanceReminder(@RequestBody EventVO eventVO) {
			JSONResponseVO jsonResponse=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
		    int success=0;
				logger.debug("Entry : public ResponseEntity sendCashBalanceReminder(@RequestBody EventVO eventVO)" );
			try {
				
				if(eventVO.getSocietyID()>0 ){
				
				success=notificationService.sendCashBalanceReminder(eventVO);
				if(success==1){
					jsonResponse.setStatusCode(200);
				
				}
				  
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in public ResponseEntity sendCashBalanceReminder(@RequestBody EventVO eventVO) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity sendCashBalanceReminder(@RequestBody EventVO eventVO)")	;						
			return new ResponseEntity(jsonResponse, HttpStatus.OK);
	 
		}
	    
		
		@RequestMapping(value="/tenantExpiredReminder", method = RequestMethod.POST)
		public ResponseEntity sendTenantExpiredReminder(@RequestBody EventVO eventVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			JSONResponseVO jsonResponse=new JSONResponseVO();
		    int success=0;
				logger.debug("Entry : public ResponseEntity sendTenantExpiredReminder(@RequestBody EventVO eventVO)" );
			try {
				
				if(eventVO.getSocietyID()>0 ){
				
				success=eventService.sendTenantExpiredReminder(eventVO);
				if(success==1){
					jsonResponse.setStatusCode(200);
					
				}
				  
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in public ResponseEntity sendTenantExpiredReminder(@RequestBody EventVO eventVO) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity sendTenantExpiredReminder(@RequestBody EventVO eventVO)")	;						
			return new ResponseEntity(jsonResponse, HttpStatus.OK);
	 
		}


	@RequestMapping(value="/tenantAboutToExpireReminder", method = RequestMethod.POST)
	public ResponseEntity sendTenantAboutToExpireReminder(@RequestBody EventVO eventVO) {
		APIResponseVO apiResponse=new APIResponseVO();
		JSONResponseVO jsonResponse=new JSONResponseVO();
	    int success=0;
			logger.debug("Entry : public ResponseEntity sendTenantAboutToExpireReminder(@RequestBody EventVO eventVO)" );
		try {
			
			if(eventVO.getSocietyID()>0 ){
			
			success=eventService.sendTenantAboutTOExpireReminder(eventVO);
			if(success==1){
				jsonResponse.setStatusCode(200);
				
			}
			  
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in public ResponseEntity sendTenantAboutToExpireReminder(@RequestBody EventVO eventVO) "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
		}
		logger.debug("Exit : public ResponseEntity sendTenantAboutToExpireReminder(@RequestBody EventVO eventVO)")	;						
		return new ResponseEntity(jsonResponse, HttpStatus.OK);

	}
	//======== API to send subscription expired reminders================//
	@RequestMapping(value="/subscriptionExpiredReminder", method = RequestMethod.POST)
	public ResponseEntity sendSubscriptionExpiredReminder() {
		APIResponseVO apiResponse=new APIResponseVO();
		JSONResponseVO jsonResponse=new JSONResponseVO();
	    int success=0;
			logger.debug("Entry : public ResponseEntity sendSubscriptionExpiredReminder(@RequestBody EventVO eventVO)" );
		try {
			
			
			
			success=eventService.sendSubscriptionExpiredReminder(2);
			if(success==1){
				jsonResponse.setStatusCode(200);
				
			}
			  
			else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in public ResponseEntity sendSubscriptionExpiredReminder(@RequestBody EventVO eventVO) "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
		}
		logger.debug("Exit : public ResponseEntity sendSubscriptionExpiredReminder(@RequestBody EventVO eventVO)")	;						
		return new ResponseEntity(jsonResponse, HttpStatus.OK);
 
	}
	
	//======== API to send subscription expired reminders================//
		@RequestMapping(value="/subscriptionExpiringReminder", method = RequestMethod.POST)
		public ResponseEntity sendSubscriptionExpiringReminder() {
			APIResponseVO apiResponse=new APIResponseVO();
			JSONResponseVO jsonResponse=new JSONResponseVO();
		    int success=0;
				logger.debug("Entry : public ResponseEntity sendSubscriptionExpiringSoonReminder(@RequestBody EventVO eventVO)" );
			try {
				success=eventService.sendSubscriptionExpiringReminder();
				if(success==1){
					jsonResponse.setStatusCode(200);
					
				}
				  
				else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in public ResponseEntity sendSubscriptionExpiringSoonReminder(@RequestBody EventVO eventVO) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity sendSubscriptionExpiringSoonReminder(@RequestBody EventVO eventVO)")	;						
			return new ResponseEntity(jsonResponse, HttpStatus.OK);
	 
		}
	
	
	 /*------------------API for Sending Payment reminder tasks -----------------*/
    @RequestMapping(value="/sendVirtualAccNumbers", method = RequestMethod.POST)
	public ResponseEntity sendVirtualAccNumbers(@RequestBody SocietyVO societyVO) {
		APIResponseVO apiResponse=new APIResponseVO();
		int successFlag=0;
		logger.debug("Entry : public ResponseEntity sendVirtualAccNumbers(@RequestBody SocietyVO societyVO)")	;
		try {
            	
			if(societyVO.getSocietyID()>0){
							
			   NotificationVO notificationVO=notificationService.sendVirtualAccNumbers(societyVO.getSocietyID());
			 
			  if(notificationVO.getStatusCode()==200){
				  apiResponse.setStatusCode(200);
				  apiResponse.setMessage("Task has been completed successfully. ");
			  }else{
				  apiResponse.setStatusCode(400);
				  apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				  return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			  }
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			logger.error("Exception in public ResponseEntity sendVirtualAccNumbers(@RequestBody SocietyVO societyVO) "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
		}
		logger.debug("Exit : public ResponseEntity sendVirtualAccNumbers(@RequestBody SocietyVO societyVO)")	;						
		return new ResponseEntity(apiResponse, HttpStatus.OK);	
 
	}
    
	//====================++ Group Notifications ++============================//
	@RequestMapping(value="/sendGroupEmailNotifications", method = RequestMethod.POST)
	public ResponseEntity sendEmailNotificationsToAGroup(@RequestBody EmailMessage requestVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			NotificationVO notificationVO=new NotificationVO();
			logger.debug("Entry : public ResponseEntity sendEmailNotificationsToAGroup(@RequestBody EmailMessage requestVO)");
		try {
				if((requestVO.getOrgID()>0) && (requestVO.getGroupName().length()>0)&&(requestVO.getProfileType().length()>0) ){
					notificationVO=eventService.sendEmailandPushNotificationsToAGroup(requestVO);			
								
					if(notificationVO.getStatusCode()==200){
                     
						apiResponse.setStatusCode(200);
						apiResponse.setMessage("Email has been sent successfully");
					}else if(notificationVO.getStatusCode()==404){
						apiResponse.setStatusCode(404);
						apiResponse.setMessage("No email address found for Email broadcast");
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage("Unable to send Email");					
						return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
					}
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
		} catch (Exception e) {
			logger.error("Exception in public ResponseEntity sendEmailNotificationsToAGroup(@RequestBody EmailMessage requestVO) "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
			logger.debug("Exit : public ResponseEntity sendEmailNotificationsToAGroup(@RequestBody EmailMessage requestVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
	}

	
	//====================++ SMS Notifications ++============================//
			@RequestMapping(value="/sendGroupSMSNotifications", method = RequestMethod.POST)
			public ResponseEntity sendGroupSMSNotificationsToAGroup(@RequestBody SMSMessage requestVO) {
					APIResponseVO apiResponse=new APIResponseVO();
					NotificationVO notificationVO=new NotificationVO();
					logger.debug("Entry : public ResponseEntity sendSMSNotificationsToAGroup(@RequestBody SMSMessage requestVO)");
				try {
						if((requestVO.getOrgID()>0) && (requestVO.getGroupName().length()>0) ){
							notificationVO=eventService.sendGroupSMSNotificationsToAGroup(requestVO);			
										
							if(notificationVO.getStatusCode()==200){
	                         
								apiResponse.setStatusCode(200);
								apiResponse.setMessage("SMS has been sent successfully");
							}else if(notificationVO.getStatusCode()==404){
								apiResponse.setStatusCode(404);
								apiResponse.setMessage("No mobile number found for SMS");
								
							}else if(notificationVO.getStatusCode()==542){
								apiResponse.setStatusCode(542);
								apiResponse.setMessage("This organization does not have enough SMS credits to send SMSes "+notificationVO.getDescription());
								return new ResponseEntity(apiResponse, HttpStatus.FORBIDDEN);			
							}else{
								
								apiResponse.setStatusCode(400);
								apiResponse.setMessage("Unable to send SMS");					
								return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
							}
							
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
				} catch (Exception e) {
					logger.error("Exception in public ResponseEntity sendSMSNotificationsToAGroup(@RequestBody SMSMessage requestVO) "+e);
					apiResponse.setStatusCode(404);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
					logger.debug("Exit : public ResponseEntity sendSMSNotificationsToAGroup(@RequestBody SMSMessage requestVO)")	;						
					return new ResponseEntity(apiResponse, HttpStatus.OK);
			 
			}

			 /*------------------API for Sending Notification of Payment reminder before few days -----------------*/
		    @RequestMapping(value="/sendNotificationOfPaymentReminder", method = RequestMethod.POST)
			public ResponseEntity sendNotificationOfPaymentReminder() {
				APIResponseVO apiResponse=new APIResponseVO();
				int successFlag=0;
				logger.debug("Entry : public ResponseEntity sendNotificationOfPaymentReminder()")	;
				try {
		            	
									
					   NotificationVO notificationVO=notificationService.sendNotificationOfPaymentReminder();
					 
					  if(notificationVO.getStatusCode()==200){
						  apiResponse.setStatusCode(200);
						  apiResponse.setMessage("Notification O fPaymentReminder has been completed successfully. ");
					  }else{
						  apiResponse.setStatusCode(400);
						  apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						  return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					  }
				
					
				} catch (Exception e) {
					logger.error("Exception in public ResponseEntity sendNotificationOfPaymentReminder() "+e);
					apiResponse.setStatusCode(404);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
				}
				logger.debug("Exit : public ResponseEntity sendNotificationOfPaymentReminder()")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);	
		 
			}
    
		    /*------------------API for Sending Notification of SMS credit below threshold -----------------*/
		    @RequestMapping(value="/sendNotificationOfLowSMSCredit", method = RequestMethod.POST)
			public ResponseEntity sendNotificationForLowSMSCredit() {
				APIResponseVO apiResponse=new APIResponseVO();
				int successFlag=0;
				logger.debug("Entry : public ResponseEntity sendNotificationForLowSMSCredit()")	;
				try {
		            	
									
					   NotificationVO notificationVO=notificationService.sendNotificationForLowSMSCredit();
					 
					  if(notificationVO.getStatusCode()==200){
						  apiResponse.setStatusCode(200);
						  apiResponse.setMessage("Notification Of low SMS Credit has been completed successfully. ");
					  }else{
						  apiResponse.setStatusCode(400);
						  apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						  return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					  }
				
					
				} catch (Exception e) {
					logger.error("Exception in public ResponseEntity sendNotificationForLowSMSCredit() "+e);
					apiResponse.setStatusCode(404);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
				}
				logger.debug("Exit : public ResponseEntity sendNotificationForLowSMSCredit()")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);	
		 
			}
	    
       }
