package com.emanager.webservice;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.concurrent.SuccessCallback;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.BankStatement;
import com.emanager.server.accounts.DataAccessObjects.ChargesVO;
import com.emanager.server.accounts.DataAccessObjects.OnlinePaymentGatewayVO;
import com.emanager.server.accounts.DataAccessObjects.ScheduledTransactionResponseVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.accountsAutomation.service.AccountsAutoService;
import com.emanager.server.adminReports.Services.PrintReportService;
import com.emanager.server.adminReports.valueObject.PrintReportVO;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.financialReports.services.BudgetService;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.financialReports.valueObject.BudgetDetailsVO;
import com.emanager.server.invoice.Services.InvoiceService;
import com.emanager.server.invoice.dataAccessObject.BillDetailsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceBillingCycleVO;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.BankInfoVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.taskAndEventManagement.Service.EventService;
import com.emanager.server.taskAndEventManagement.Service.NotificationService;
import com.emanager.server.taskAndEventManagement.Service.TaskService;
import com.emanager.server.taskAndEventManagement.valueObject.EmailMessage;
import com.emanager.server.taskAndEventManagement.valueObject.EventVO;
import com.emanager.server.taskAndEventManagement.valueObject.NotificationVO;
import com.emanager.server.taskAndEventManagement.valueObject.SMSGatewayVO;
import com.emanager.server.taskAndEventManagement.valueObject.TaskVO;
import com.emanager.server.userManagement.service.UserService;
import com.emanager.server.userManagement.valueObject.UserVO;




@Controller
@RequestMapping("/finbot")
public class JSONAPIControllerForFinbot {
	
	private static final Logger logger = Logger.getLogger(JSONAPIControllerForFinbot.class);
	@Autowired
	TransactionService transactionService;
	@Autowired
    InvoiceService invoiceService;
	@Autowired
    LedgerService ledgerService;
	@Autowired
    SocietyService societyService;
	@Autowired
	TaskService taskService;
	@Autowired
	ReportService reportService;
	@Autowired
	EventService eventService;
	@Autowired
	AccountsAutoService accountsAutoService;
	@Autowired
	NotificationService notificationService;
	@Autowired
	UserService userService;
	@Autowired
	MemberService memberService;
	@Autowired
	BudgetService budgetService;
	@Autowired
    PrintReportService printReportService;
	
	DateUtility dateUtil=new DateUtility();
	public ConfigManager configManager =new ConfigManager();
	
	/*------------------Generate Transactions API------------------*/
	@RequestMapping(value="/generate/transactions",  method = RequestMethod.POST  )			
	public ResponseEntity generateBulkTransaction(@RequestBody TransactionVO txVO) {
		APIResponseVO apiResponse=new APIResponseVO();
		List transactionList=new ArrayList();
		ScheduledTransactionResponseVO respVO=new ScheduledTransactionResponseVO();
			logger.debug("Entry : public @ResponseBody int generateBulkTransaction(@RequestBody TransactionVO txVO)")	;
		try {
			logger.info("generateBulkTransaction Here society ID "+txVO.getSocietyID()+" TX ID "+txVO.getTransactionID());
			if(txVO.getSocietyID()>0){
			transactionList=transactionService.generateScheduledTransactions(txVO.getSocietyID(),0);
			
			if(transactionList.size()>0){
				respVO=transactionService.addBulkTransactions(transactionList, txVO.getSocietyID());
			
				if(respVO.getSuccessCount()>respVO.getFailureCount()){
					for(String element:respVO.getChargeList()){
						int chargeID=Integer.parseInt(element);
					ChargesVO chargeVO=transactionService.getChargeDetails(chargeID,txVO.getSocietyID());
					int success=transactionService.updateChargeStatus( txVO.getSocietyID(), chargeVO);
					//int updateReminderDate=invoiceService.updateReminderDate(billingVO.getSocietyId());
					}
				}
			
			
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage("No Scheduled charges are available for the given period ");
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
			
			
			}
			
		} catch (Exception e) {
			logger.error("Exception public @ResponseBody int generateBulkTransaction(@RequestBody TransactionVO txVO) "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody int public @ResponseBody int generateBulkTransaction(@RequestBody TransactionVO txVO)")	;						
		return new ResponseEntity(respVO, HttpStatus.OK);
 
	}
	
	
	/*------------------Generate Late fee Transactions API------------------*/
	@RequestMapping(value="/generate/LateFeetransactions",  method = RequestMethod.POST  )			
	public ResponseEntity generateBulkLateFeeTransaction(@RequestBody TransactionVO txVO) {
		APIResponseVO apiResponse=new APIResponseVO();
		List transactionList=new ArrayList();
		ScheduledTransactionResponseVO respVO=new ScheduledTransactionResponseVO();
			logger.debug("Entry : public @ResponseBody int generateBulkLateFeeTransaction(@RequestBody TransactionVO txVO)")	;
		try {
			logger.info("Create late fee transactions for society ID "+txVO.getSocietyID()+" TX ID "+txVO.getBillingCycleID());
			if(txVO.getSocietyID()>0){
			transactionList=transactionService.generateScheduledLateFeeTransactions(txVO.getSocietyID(),txVO.getBillingCycleID());
			
			if(transactionList.size()>0){
				respVO=transactionService.addBulkTransactions(transactionList, txVO.getSocietyID());
			}
			if(respVO.getSuccessCount()>respVO.getFailureCount()){
				for(String element:respVO.getChargeList()){
					int chargeID=Integer.parseInt(element);
				ChargesVO chargeVO=transactionService.getPenaltyChargeDetails(chargeID, txVO.getSocietyID());
				int success=transactionService.updateLateFeeChargeDate(txVO.getSocietyID(), chargeVO);
					success=transactionService.updatePenaltyChargeStatus( txVO.getSocietyID(), chargeVO);
				//int updateReminderDate=invoiceService.updateReminderDate(billingVO.getSocietyId());
				}
			}
			
		
			}
			
			
		} catch (Exception e) {
			logger.error("Exception generateBulkLateFeeTransaction "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody int generateBulkLateFeeTransaction(@RequestBody TransactionVO txVO)")	;						
		return new ResponseEntity(respVO, HttpStatus.OK);
 
	}
	
	
	/*------------------Generate Recurring Invoices API------------------*/
	@RequestMapping(value="/generate/invoices",  method = RequestMethod.POST  )			
	public ResponseEntity generateBulkInvoices(@RequestBody TransactionVO txVO) {
		APIResponseVO apiResponse=new APIResponseVO();
		List invoiceList=new ArrayList();
		ScheduledTransactionResponseVO respVO=new ScheduledTransactionResponseVO();
			logger.debug("Entry :  public ResponseEntity generateBulkInvoices(@RequestBody TransactionVO txVO)")	;
		try {
			logger.info("generateBulkTransaction Here society ID "+txVO.getSocietyID()+" TX ID "+txVO.getTransactionID());
			if(txVO.getSocietyID()>0){
			invoiceList=invoiceService.generateScheduledInvoices(txVO.getSocietyID(), txVO.getBillingCycleID());
			
			if(invoiceList.size()>0){
				respVO=invoiceService.addBulkInvoices(invoiceList,txVO.getSocietyID());
			
				if(respVO.getSuccessCount()>respVO.getFailureCount()){
					for(String element:respVO.getChargeList()){
						int chargeID=Integer.parseInt(element);
					ChargesVO chargeVO=transactionService.getChargeDetails(chargeID,txVO.getSocietyID());
					int success=transactionService.updateChargeStatus( txVO.getSocietyID(), chargeVO);
					//int updateReminderDate=invoiceService.updateReminderDate(billingVO.getSocietyId());
					}
				}
			
			
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage("No Scheduled charges are available for the given period ");
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
			}
			
		} catch (Exception e) {
			logger.error("Exception  public ResponseEntity generateBulkInvoices(@RequestBody TransactionVO txVO) "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity generateBulkInvoices(@RequestBody TransactionVO txVO)")	;						
		return new ResponseEntity(respVO, HttpStatus.OK);
 
	}
	
	/*------------------Generate Recurring Invoices API------------------*/
	@RequestMapping(value="/generate/lateFeeInvoices",  method = RequestMethod.POST  )			
	public ResponseEntity generateBulkLateFeeInvoices(@RequestBody TransactionVO txVO) {
		APIResponseVO apiResponse=new APIResponseVO();
		List invoiceList=new ArrayList();
		ScheduledTransactionResponseVO respVO=new ScheduledTransactionResponseVO();
			logger.debug("Entry :  public ResponseEntity generateBulkLateFeeInvoices(@RequestBody TransactionVO txVO)")	;
		try {
			logger.info("generateBulkTransaction Here society ID "+txVO.getSocietyID()+" TX ID "+txVO.getTransactionID());
			if(txVO.getSocietyID()>0){
			invoiceList=invoiceService.generateScheduledLateFeeInvoices(txVO.getSocietyID(), txVO.getBillingCycleID());
			
			if(invoiceList.size()>0){
				respVO=invoiceService.addBulkInvoices(invoiceList,txVO.getSocietyID());
			
				if(respVO.getSuccessCount()>respVO.getFailureCount()){
					for(String element:respVO.getChargeList()){
						int chargeID=Integer.parseInt(element);
						ChargesVO chargeVO=transactionService.getPenaltyChargeDetails(chargeID, txVO.getSocietyID());
						int success=transactionService.updateLateFeeChargeDate(txVO.getSocietyID(), chargeVO);
							success=transactionService.updatePenaltyChargeStatus( txVO.getSocietyID(), chargeVO);
					//int updateReminderDate=invoiceService.updateReminderDate(billingVO.getSocietyId());
					}
				}
			
			
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage("No Scheduled charges are available for the given period ");
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
			}
			
		} catch (Exception e) {
			logger.error("Exception  public ResponseEntity generateBulkInvoices(@RequestBody TransactionVO txVO) "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity generateBulkInvoices(@RequestBody TransactionVO txVO)")	;						
		return new ResponseEntity(respVO, HttpStatus.OK);
 
	}
	
	/*------------------Generate Recurring Invoices for Organizations API------------------*/
	@RequestMapping(value="/generate/invoicesForOrg",  method = RequestMethod.POST  )			
	public ResponseEntity generateBulkInvoicesForOrg(@RequestBody TransactionVO txVO) {
		APIResponseVO apiResponse=new APIResponseVO();
		List invoiceList=new ArrayList();
		ScheduledTransactionResponseVO respVO=new ScheduledTransactionResponseVO();
			logger.debug("Entry :  public ResponseEntity generateBulkInvoicesForOrg(@RequestBody TransactionVO txVO)")	;
		try {
			logger.info("generateBulkInvoicesForOrg Here society ID "+txVO.getSocietyID()+" TX ID "+txVO.getTransactionID());
			if(txVO.getSocietyID()>0){
			invoiceList=accountsAutoService.generateScheduledInvoicesForOrg(txVO.getSocietyID(), txVO.getBillingCycleID());
			
			if(invoiceList.size()>0){
				respVO=invoiceService.addBulkInvoices(invoiceList,txVO.getSocietyID());
			
				if(respVO.getSuccessCount()>respVO.getFailureCount()){
					for(String element:respVO.getChargeList()){
						int chargeID=Integer.parseInt(element);
					ChargesVO chargeVO=transactionService.getChargeDetails(chargeID,txVO.getSocietyID());
					int success=transactionService.updateChargeStatus( txVO.getSocietyID(), chargeVO);
					//int updateReminderDate=invoiceService.updateReminderDate(billingVO.getSocietyId());
					}
				}
			
			
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage("No Scheduled charges are available for the given period ");
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
			}
			
		} catch (Exception e) {
			logger.error("Exception  public ResponseEntity generateBulkInvoicesForOrg(@RequestBody TransactionVO txVO) "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity generateBulkInvoicesForOrg(@RequestBody TransactionVO txVO)")	;						
		return new ResponseEntity(respVO, HttpStatus.OK);
 
	}
	
	
	/*------------------Update task status------------------*/
	@RequestMapping(value="/update/TaskStatus",  method = RequestMethod.POST  )			
	public ResponseEntity updateTaskStatus(@RequestBody TaskVO txVO) {
		JSONResponseVO jsonResponse=new JSONResponseVO();
		int success=0;
		
			logger.debug("Entry : public ResponseEntity updateTaskStatus(@RequestBody TaskVO txVO)")	;
		try {
			
			if(txVO.getTaskID()>0){
			//transactionList=transactionService.generateScheduledLateFeeTransactions(txVO.getSocietyID(),txVO.getBillingCycleID());
			 success=taskService.updateTaskStatus(txVO);
			
				if(success>0){
					jsonResponse.setStatusCode(200);
					
				}
				
			
		
			}
			
			
		} catch (Exception e) {
			logger.error("Exception public ResponseEntity updateTaskStatus(@RequestBody TaskVO txVO) "+e);
			jsonResponse.setStatusCode(400);
			return new ResponseEntity(jsonResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody int public ResponseEntity updateTaskStatus(@RequestBody TaskVO txVO)")	;						
		return new ResponseEntity(jsonResponse, HttpStatus.OK);
 
	}
	
	/*------------------Update task status------------------*/
	@RequestMapping(value="/update/TaskStatusToRunning",  method = RequestMethod.POST  )			
	public ResponseEntity updateTaskStatusToRunning(@RequestBody TaskVO taskVO) {
		JSONResponseVO jsonResponse=new JSONResponseVO();
		int success=0;
		ScheduledTransactionResponseVO respVO=new ScheduledTransactionResponseVO();
			logger.debug("Entry : public @ResponseBody int updateTaskStatusToRunning(@RequestBody TransactionVO txVO)")	;
		try {
			
			if(taskVO.getTaskID()>0){
			//transactionList=transactionService.generateScheduledLateFeeTransactions(txVO.getSocietyID(),txVO.getBillingCycleID());
				 success=taskService.updateTaskRunningStatus(taskVO);
			
			if(success>0){
				jsonResponse.setStatusCode(200);
				
			}
			}
			
		} catch (Exception e) {
			logger.error("Exception updateTaskStatusToRunning "+e);
			jsonResponse.setStatusCode(400);
			//apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(jsonResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody int updateTaskStatusToRunning(@RequestBody TransactionVO txVO)")	;						
		return new ResponseEntity(jsonResponse, HttpStatus.OK);
 
	}
	
	
	/*------------------Add raw bankStatement------------------*/
	@RequestMapping(value="/statement/insertBankStatement",  method = RequestMethod.POST  )			
	public ResponseEntity addBankStatement(@RequestBody BankStatement bankStatementVO) {
		JSONResponseVO jsonVO=new JSONResponseVO();
	
		int insertStatement=0;
	
			logger.debug("Entry : public ResponseEntity addBankStatement(@RequestBody List bankStatementList)")	;
		try {
			
			if(bankStatementVO.getBankStatementList().size()>0){
				//transactionList=transactionService.generateScheduledLateFeeTransactions(txVO.getSocietyID(),txVO.getBillingCycleID());
					 insertStatement=transactionService.addBankStatements(bankStatementVO);
				
				if(insertStatement>0){
					jsonVO.setStatusCode(200);
					
				}
				
			
			
			}else{
				jsonVO.setStatusCode(400);
			return new ResponseEntity(jsonVO, HttpStatus.BAD_REQUEST);
			}
			
			
			
			
			
		} catch (Exception e) {
			logger.error("Exception public ResponseEntity addBankStatement(@RequestBody List bankStatementList) "+e);
			jsonVO.setStatusCode(400);
			return new ResponseEntity(jsonVO, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity addBankStatement(@RequestBody List bankStatementList)")	;						
		return new ResponseEntity(jsonVO, HttpStatus.OK);
 
	}
	
	/*------------------Add raw bankStatement------------------*/
	@RequestMapping(value="/statement/insertBankTransactions",  method = RequestMethod.POST  )			
	public ResponseEntity addBankTransactions(@RequestBody BankStatement bankStatementVO) {
		JSONResponseVO jsonVO=new JSONResponseVO();
	
		int insertStatement=0;
	
			logger.debug("Entry : public ResponseEntity addBankTransactions(@RequestBody List bankStatementList)")	;
		try {
			
			if(bankStatementVO.getTransactionList().size()>0){
				//transactionList=transactionService.generateScheduledLateFeeTransactions(txVO.getSocietyID(),txVO.getBillingCycleID());
					 insertStatement=transactionService.addBankTransactions(bankStatementVO);
				
				if(insertStatement>0){
					jsonVO.setStatusCode(200);
					
				}
				
			
			
			}else{
				jsonVO.setStatusCode(400);
			return new ResponseEntity(jsonVO, HttpStatus.BAD_REQUEST);
			}
			
			
			
			
			
		} catch (Exception e) {
			logger.error("Exception public ResponseEntity addBankTransactions(@RequestBody List bankStatementList) "+e);
			jsonVO.setStatusCode(400);
			return new ResponseEntity(jsonVO, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity addBankTransactions(@RequestBody List bankStatementList)")	;						
		return new ResponseEntity(jsonVO, HttpStatus.OK);
 
	}

	/*------------------Add raw bankStatement------------------*/
	@RequestMapping(value="/statement/getBankDetails",  method = RequestMethod.POST  )			
	public ResponseEntity getBankDetails(@RequestBody BankStatement bankStatementVO) {
		JSONResponseVO jsonVO=new JSONResponseVO();
		BankStatement bankStatement=new BankStatement();
		int insertStatement=0;
	
			logger.debug("Entry : public ResponseEntity addBankStatement(@RequestBody List bankStatementList)")	;
		try {
			
			if(bankStatementVO.getBankAccNumber().length()>0){
				//transactionList=transactionService.generateScheduledLateFeeTransactions(txVO.getSocietyID(),txVO.getBillingCycleID());
					 bankStatement=ledgerService.getLedgersDetailsForImport(bankStatementVO);
				
				if(bankStatement.getBankLedgerID()>0){
					jsonVO.setStatusCode(200);
					
				}
				
			
			
			}else{
				jsonVO.setStatusCode(400);
			return new ResponseEntity(jsonVO, HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			logger.error("Exception public ResponseEntity addBankStatement(@RequestBody List bankStatementList) "+e);
			jsonVO.setStatusCode(400);
			return new ResponseEntity(jsonVO, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity addBankStatement(@RequestBody List bankStatementList)")	;						
		return new ResponseEntity(bankStatement, HttpStatus.OK);
 
	}
	
	//-----------Used to add one by one transactions from bank statement--------------//
	@RequestMapping(value="/statement/insertSingleTransaction",  method = RequestMethod.POST  )			
	public ResponseEntity addSingleTransaction(@RequestBody TransactionVO txVO) {
		APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public ResponseEntity addSingleTransaction(@RequestBody List bankStatementList)")	;
		try {
			
			if(txVO.getSocietyID()>0){
				
			txVO=accountsAutoService.addSingleTransaction(txVO);
					
					 if(txVO.getStatusCode()==200){
						 apiResponse.setStatusCode(200);
						 apiResponse.setMessage("Transaction imported successfully. ");				
							
					 }else if((txVO.getStatusCode()==534)||(txVO.getStatusCode()==531)){
						 apiResponse.setStatusCode(txVO.getStatusCode());
						 apiResponse.setMessage(txVO.getErrorMessage());
						 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
					 }else if(txVO.getStatusCode()==530){
						 apiResponse.setStatusCode(txVO.getStatusCode());
						 apiResponse.setMessage(txVO.getErrorMessage());
						 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
							
					 }else if((txVO.getStatusCode()==534)||(txVO.getStatusCode()==531)){
						 apiResponse.setStatusCode(txVO.getStatusCode());
						 apiResponse.setMessage(txVO.getErrorMessage());
						 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
							
					 }else{
						 apiResponse.setStatusCode(406);
							apiResponse.setMessage("Unable to import transaction");						
						return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);							 
					 
				}
						
			
			
			
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}			
			
			
			
			
		} catch (Exception e) {
			logger.error("Exception public ResponseEntity addSingleTransaction(@RequestBody List bankStatementList) "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity addSingleTransaction(@RequestBody List bankStatementList)");						
		return new ResponseEntity(apiResponse, HttpStatus.OK);
 
	}
	
	
	//-----------Used to add one by one transactions from bank statement--------------//
		@RequestMapping(value="/statement/insertSinglePayUTransaction",  method = RequestMethod.POST  )			
		public ResponseEntity addSinglePayUTransaction(@RequestBody TransactionVO txVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity addSingleTransaction(@RequestBody List bankStatementList)")	;
			try {
				
				if(txVO.getSocietyID()>0){
					
				txVO=accountsAutoService.addSinglePayUTransaction(txVO);
						
						 if(txVO.getStatusCode()==200){
							 apiResponse.setStatusCode(200);
							 apiResponse.setMessage("Transaction imported successfully. ");				
								
						 }else if((txVO.getStatusCode()==534)||(txVO.getStatusCode()==531)){
							 apiResponse.setStatusCode(txVO.getStatusCode());
							 apiResponse.setMessage(txVO.getErrorMessage());
							 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
						 }else if(txVO.getStatusCode()==530){
							 apiResponse.setStatusCode(txVO.getStatusCode());
							 apiResponse.setMessage(txVO.getErrorMessage());
							 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
								
						 }else if((txVO.getStatusCode()==534)||(txVO.getStatusCode()==531)){
							 apiResponse.setStatusCode(txVO.getStatusCode());
							 apiResponse.setMessage(txVO.getErrorMessage());
							 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
								
						 }else{
							 apiResponse.setStatusCode(406);
								apiResponse.setMessage("Unable to import transaction");						
							return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);							 
						 
					}
							
				
				
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}			
				
				
				
				
			} catch (Exception e) {
				logger.error("Exception public ResponseEntity addSingleTransaction(@RequestBody List bankStatementList) "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity addSingleTransaction(@RequestBody List bankStatementList)");						
			return new ResponseEntity(txVO, HttpStatus.OK);
	 
		}
	
		//-----------Used to add one by one transactions from bankx statement--------------//
				@RequestMapping(value="/bankX/insertSingleBankXTransaction",  method = RequestMethod.POST  )			
				public ResponseEntity addSingleBankxTransaction(@RequestBody TransactionVO txVO) {
							APIResponseVO apiResponse=new APIResponseVO();
							logger.debug("Entry : public ResponseEntity addSingleBankxTransaction(@RequestBody List bankStatementList)")	;
							try {
								
								if(txVO.getSocietyID()>0){
									
								txVO=accountsAutoService.addSingleBankxTransaction(txVO);
										
										 if(txVO.getStatusCode()==200){
											 apiResponse.setStatusCode(200);
											 apiResponse.setMessage("Transaction imported successfully. ");				
												
										 }else if((txVO.getStatusCode()==534)||(txVO.getStatusCode()==531)){
											 apiResponse.setStatusCode(txVO.getStatusCode());
											 apiResponse.setMessage(txVO.getErrorMessage());
											 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
										 }else if(txVO.getStatusCode()==530){
											 apiResponse.setStatusCode(txVO.getStatusCode());
											 apiResponse.setMessage(txVO.getErrorMessage());
											 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
												
										 }else if((txVO.getStatusCode()==534)||(txVO.getStatusCode()==531)){
											 apiResponse.setStatusCode(txVO.getStatusCode());
											 apiResponse.setMessage(txVO.getErrorMessage());
											 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
												
										 }else{
											 apiResponse.setStatusCode(406);
												apiResponse.setMessage("Unable to import transaction");						
											return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);							 
										 
									}
											
								
								
								
								}else{
									apiResponse.setStatusCode(400);
									apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
									return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
								}			
								
								
								
								
							} catch (Exception e) {
								logger.error("Exception public ResponseEntity addSingleBankxTransaction(@RequestBody List bankStatementList) "+e);
								apiResponse.setStatusCode(400);
								apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
								return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
							}
							logger.debug("Exit : public ResponseEntity addSingleBankxTransaction(@RequestBody List bankStatementList)");						
							return new ResponseEntity(txVO, HttpStatus.OK);
					 
						}
		
		
		
	/*------------------import added bankStatement to Table------------------*/
	@RequestMapping(value="/statement/insertTransactions",  method = RequestMethod.POST  )			
	public ResponseEntity addTransactionsFromStatement(@RequestBody BankStatement bankStatementVO) {
		APIResponseVO apiResponse=new APIResponseVO();
		int insertStatement=0;
		List rawStatementList=new ArrayList();
			logger.debug("Entry : public ResponseEntity addBankStatement(@RequestBody List bankStatementList)")	;
		try {
			
			if(bankStatementVO.getBankAccNumber().length()>0){
				
				rawStatementList=accountsAutoService.getRawStatement(bankStatementVO.getBankAccNumber(), bankStatementVO.getFromDate().toString(), bankStatementVO.getToDate().toString());
				//transactionList=transactionService.generateScheduledLateFeeTransactions(txVO.getSocietyID(),txVO.getBillingCycleID());
				if(rawStatementList.size()>0){
				bankStatementVO.setBankStatementList(rawStatementList);
					insertStatement=accountsAutoService.addStatementToDatabase(bankStatementVO);
					
					 if(insertStatement>0){
						 apiResponse.setStatusCode(200);
						 apiResponse.setMessage("Statement imported successfully. ");				
							
					 }else{
						 apiResponse.setStatusCode(406);
							apiResponse.setMessage("Unable to import statment");						
							return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);							 
					 }
				}else{
						apiResponse.setStatusCode(406);
						apiResponse.setMessage("No new transactions found ");						
						return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);	
						
				}
			
			
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}			
			
			
			
			
		} catch (Exception e) {
			logger.error("Exception public ResponseEntity addBankStatement(@RequestBody List bankStatementList) "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity addBankStatement(@RequestBody List bankStatementList)");						
		return new ResponseEntity(apiResponse, HttpStatus.OK);
 
	}
	
	
	/*------------------import added bankStatement to Table------------------*/
	@RequestMapping(value="/statement/reconcile",  method = RequestMethod.POST  )			
	public ResponseEntity reconcileBankStatement(@RequestBody BankStatement bankStatementVO) {
		APIResponseVO apiResponse=new APIResponseVO();	
		int insertStatement=0;
		List rawStatementList=new ArrayList();
			logger.debug("Entry : public ResponseEntity reconcileBankStatement(@RequestBody List bankStatementList)")	;
		try {
			
			if(bankStatementVO.getBankAccNumber().length()>0){
				
				rawStatementList=accountsAutoService.getRawStatement(bankStatementVO.getBankAccNumber(), bankStatementVO.getFromDate().toString(), bankStatementVO.getToDate().toString());
				//transactionList=transactionService.generateScheduledLateFeeTransactions(txVO.getSocietyID(),txVO.getBillingCycleID());
			  if(rawStatementList.size()>0){
				BankStatement bankStatement=(BankStatement) rawStatementList.get(0);
				bankStatementVO.setOrgID(bankStatement.getOrgID());
				insertStatement=accountsAutoService.reconcileTheBankStatement(bankStatementVO);
				
					if(insertStatement>0){
						apiResponse.setStatusCode(200);
						 apiResponse.setMessage("Statement reconcile successfully. ");	
					}else{
						 apiResponse.setStatusCode(406);
						 apiResponse.setMessage("Unable to reconcile statment");						
						return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);							 
					 }
				}else{
					apiResponse.setStatusCode(406);
					apiResponse.setMessage("No new reconcile trasactions found ");						
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);	
					
			     }
			
			
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				
			
			
			
		} catch (Exception e) {
			logger.error("Exception public ResponseEntity reconcileBankStatement(@RequestBody List bankStatementList) "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity reconcileBankStatement(@RequestBody List bankStatementList)")	;						
		return new ResponseEntity(apiResponse, HttpStatus.OK);
 
	}
	
	/*------------------Add raw bankStatement------------------*/
	@RequestMapping(value="/statement/getRawBankStatement",  method = RequestMethod.POST  )			
	public ResponseEntity getRawBankStatement(@RequestBody BankStatement bankStatementVO) {
		JSONResponseVO jsonVO=new JSONResponseVO();
		APIResponseVO apiResponse=new APIResponseVO();	
		List bankStatementList=new ArrayList();
		int insertStatement=0;
	
			logger.debug("Entry : public ResponseEntity addBankStatement(@RequestBody List bankStatementList)")	;
		try {
			
			if(bankStatementVO.getBankLedgerID()>0){
				//transactionList=transactionService.generateScheduledLateFeeTransactions(txVO.getSocietyID(),txVO.getBillingCycleID());
					 bankStatementList=accountsAutoService.getRawStatementFromLedgerID(bankStatementVO.getBankLedgerID(), bankStatementVO.getFromDate().toString(), bankStatementVO.getToDate().toString(),bankStatementVO.getOrgID());
				
				if(bankStatementList.size()>0){
					jsonVO.setStatusCode(200);
					jsonVO.setObjectList(bankStatementList);
				}
				
			
			
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
			
			
			
			
		} catch (Exception e) {
			logger.error("Exception public ResponseEntity addBankStatement(@RequestBody List bankStatementList) "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity addBankStatement(@RequestBody List bankStatementList)")	;						
		return new ResponseEntity(jsonVO, HttpStatus.OK);
 
	}

	
	/*------------------Add PayUMoney Statement------------------*/
	@RequestMapping(value="/statement/insertPayUStatement",  method = RequestMethod.POST  )			
	public ResponseEntity addPayUStatement(@RequestBody OnlinePaymentGatewayVO payUVO) {
		JSONResponseVO jsonVO=new JSONResponseVO();
	
		int insertStatement=0;
	
			logger.debug("Entry : public ResponseEntity addPayUStatement(@RequestBody OnlinePaymentGatewayVO payUVO)")	;
		try {
			
			if(payUVO.getObjectList().size()>0){
				
					 insertStatement=transactionService.addPayUStatements(payUVO);
				
				if(insertStatement>0){
					jsonVO.setStatusCode(200);
					
				}
				
			
			
			}else{
				jsonVO.setStatusCode(400);
			return new ResponseEntity(jsonVO, HttpStatus.BAD_REQUEST);
			}
			
			
			
			
			
		} catch (Exception e) {
			logger.error("Exception public ResponseEntity addPayUStatement(@RequestBody OnlinePaymentGatewayVO payUVO) "+e);
			jsonVO.setStatusCode(400);
			return new ResponseEntity(jsonVO, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity addPayUStatement(@RequestBody OnlinePaymentGatewayVO payUVO)")	;						
		return new ResponseEntity(jsonVO, HttpStatus.OK);
 
	}
	
	
	/*------------------Get payUMoney Statement------------------*/
	@RequestMapping(value="/statement/getPayUStatement",  method = RequestMethod.POST  )			
	public ResponseEntity getPayUStatement(@RequestBody OnlinePaymentGatewayVO payUVO) {
		JSONResponseVO jsonVO=new JSONResponseVO();
		APIResponseVO apiResponse=new APIResponseVO();	
		List statementList=new ArrayList();
		int insertStatement=0;
	
			logger.debug("Entry : public ResponseEntity getPayUStatement(@RequestBody OnlinePaymentGatewayVO payUVO)")	;
		try {
			
			if(payUVO.getOrgID()>0){
				
					 statementList=accountsAutoService.getPayUStatement(payUVO.getOrgID(),payUVO.getFromDate(),payUVO.getToDate());
				
				if(statementList.size()>0){
					jsonVO.setStatusCode(200);
					jsonVO.setObjectList(statementList);
				}
				
			
			
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
			
			
			
			
		} catch (Exception e) {
			logger.error("Exception public ResponseEntity getPayUStatement(@RequestBody OnlinePaymentGatewayVO payUVO) "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity getPayUStatement(@RequestBody OnlinePaymentGatewayVO payUVO)")	;						
		return new ResponseEntity(jsonVO, HttpStatus.OK);
 
	}
	
	
	
	/*------------------process payUMoney Statement------------------*/
	@RequestMapping(value="/statement/processPayUStatement",  method = RequestMethod.POST  )			
	public ResponseEntity processPayUStatement() {
		JSONResponseVO jsonVO=new JSONResponseVO();
		APIResponseVO apiResponse=new APIResponseVO();	
		List statementList=new ArrayList();
		int insertStatement=0;
	
			logger.debug("Entry : public ResponseEntity processPayUStatement()")	;
		try {
			
			
				
					 insertStatement=accountsAutoService.processPayUStatement();
				
				if(statementList.size()>0){
					jsonVO.setStatusCode(200);
					jsonVO.setObjectList(statementList);
				}
				
			
			
			
			
			
			
			
			
		} catch (Exception e) {
			logger.error("Exception public ResponseEntity getPayUStatement(@RequestBody OnlinePaymentGatewayVO payUVO) "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity getPayUStatement(@RequestBody OnlinePaymentGatewayVO payUVO)")	;						
		return new ResponseEntity(jsonVO, HttpStatus.OK);
 
	}
	
	
	
	/*------------------Generate billing cycles------------------*/
	@RequestMapping(value="/invoice/generateBillingCycle",  method = RequestMethod.POST  )			
	public ResponseEntity generateBillingCycle() {
		NotificationVO notificationVO=new NotificationVO();
		APIResponseVO apiResponse=new APIResponseVO();	
	
	
			logger.debug("Entry : public ResponseEntity generateBillingCycle()")	;
		try {
			
		
			
					 int success=accountsAutoService.generateBillingCycles();
				
					 if(success!=0){
						notificationVO .setStatusCode(200);
						 
					 }
			
			
			
			
		} catch (Exception e) {
			logger.error("Exception public ResponseEntity generateBillingCycle() "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity generateBillingCycle(t)")	;						
		return new ResponseEntity(notificationVO, HttpStatus.OK);
 
	}
	
	
	/*------------------Generate billing cycles------------------*/
	@RequestMapping(value="/invoice/autoGenerate",  method = RequestMethod.POST  )			
	public ResponseEntity generateAutoInvoices() {
		JSONResponseVO jsonVO=new JSONResponseVO();
		APIResponseVO apiResponse=new APIResponseVO();	
		List bankStatementList=new ArrayList();
		int insertStatement=0;
	
			logger.debug("Entry : public ResponseEntity generateBillingCycle()")	;
		try {
			
			List societyList = societyService.getSocietyList();
			List responseList=new ArrayList();
			for (int i = 0; i < societyList.size(); i++) {
				SocietyVO societyVO=(SocietyVO) societyList.get(i);
				List billingCycleList=invoiceService.getBillingCycleList(societyVO.getSocietyID());
			
				
				if(billingCycleList.size()>0){
				InvoiceBillingCycleVO billingVO = (InvoiceBillingCycleVO) billingCycleList.get(billingCycleList.size()-1);
				ScheduledTransactionResponseVO resp=invoiceService.generateNormalInvoices(billingVO.getSocietyId(),billingVO.getBillingCycleID());
				ScheduledTransactionResponseVO responseVO=new ScheduledTransactionResponseVO();
				responseVO.setSocietyID(billingVO.getSocietyId());
				responseVO.setSuccessCount(resp.getSuccessCount());
				responseVO.setFailureCount(resp.getFailureCount());
				responseVO.setSocietyName(societyVO.getSocietyName());
				
				if((responseVO.getSuccessCount()>0)||(responseVO.getLateFeeSucccessCount()>0)){
					responseList.add(responseVO);
				}
				
				}
				
			}			if(responseList.size()>0){
						notificationService.sendSummaryNotifiactionToTechsupport(responseList);
			}
			
			
			
			
		} catch (Exception e) {
			logger.error("Exception public ResponseEntity generateAutoInvoices() "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity generateAutoInvoices(t)")	;						
		return new ResponseEntity(apiResponse, HttpStatus.OK);
 
	}
	
	/*------------------Generate billing cycles------------------*/
	@RequestMapping(value="/invoice/autoGenerateLateFee",  method = RequestMethod.POST  )			
	public ResponseEntity generateAutoLateInvoices() {
		JSONResponseVO jsonVO=new JSONResponseVO();
		APIResponseVO apiResponse=new APIResponseVO();	
		List bankStatementList=new ArrayList();
		int insertStatement=0;
	
			logger.debug("Entry : public ResponseEntity generateBillingCycle()")	;
		try {
			
			List societyList = societyService.getSocietyList();
			List responseList=new ArrayList();
			for (int i = 0; i < societyList.size(); i++) {
				SocietyVO societyVO=(SocietyVO) societyList.get(i);
				List billingCycleList=invoiceService.getBillingCycleList(societyVO.getSocietyID());
			
				
				if(billingCycleList.size()>0){
				InvoiceBillingCycleVO billingVO = (InvoiceBillingCycleVO) billingCycleList.get(billingCycleList.size()-1);
				ScheduledTransactionResponseVO respLate=invoiceService.generateLateFeeInvoices(billingVO.getSocietyId(),billingVO.getBillingCycleID());
				ScheduledTransactionResponseVO responseVO=new ScheduledTransactionResponseVO();
				responseVO.setSocietyID(billingVO.getSocietyId());
			
				responseVO.setLateFeeSucccessCount(respLate.getLateFeeSucccessCount());
				responseVO.setLateFeeFailureCount(respLate.getLateFeeFailureCount());
				responseVO.setSocietyName(societyVO.getSocietyName());
				
				if((responseVO.getSuccessCount()>0)||(responseVO.getLateFeeSucccessCount()>0)){
					responseList.add(responseVO);
				}
				
				}
				
			}			if(responseList.size()>0){
						notificationService.sendSummaryNotifiactionToTechsupport(responseList);
			}
			
			
			
			
		} catch (Exception e) {
			logger.error("Exception public ResponseEntity generateAutoInvoices() "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity generateAutoInvoices(t)")	;						
		return new ResponseEntity(apiResponse, HttpStatus.OK);
 
	}
	
	/*----------- API to get list of ledgers --------------*/
	@RequestMapping(value="/ledgersListWithClosingBal", method = RequestMethod.POST)
	public ResponseEntity getLedgersListWithClosingBal(@RequestBody TransactionVO txVO) {
		JSONResponseVO jSonVO=new JSONResponseVO();
		APIResponseVO apiResponse=new APIResponseVO();
		List<AccountHeadVO> ledgerList=new ArrayList();
		SocietyVO societyVo=new SocietyVO();
			logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getLedgersListWithClosingBal(@RequestBody RequestWrapper requestWrapper)")	;
		try {
		
			if(txVO.getSocietyID()>0){
			 societyVo=societyService.getSocietyDetails(txVO.getSocietyID());
		
			
			 ledgerList=ledgerService.getLedgerListWithClosingBalance(txVO.getSocietyID(), txVO.getGroupID(),"G",txVO.getFromDate(),txVO.getToDate());
			 jSonVO.setSocietyID(txVO.getSocietyID());
			 jSonVO.setSocietyName(societyVo.getSocietyName());
			if(ledgerList.size()>0){
			  jSonVO.setObjectList((ArrayList<AccountHeadVO>)ledgerList);
			  jSonVO.setStatusCode(200);
			}
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception in getLedgersList: "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getLedgersList(@RequestBody RequestWrapper requestWrapper)")	;						
		return new ResponseEntity(jSonVO, HttpStatus.OK);
 
	}
	
	/*----------- API to get list of transactions for tally export --------------*/
	@RequestMapping(value="/ledgerListForTally", method = RequestMethod.POST)
	public ResponseEntity getLedgerListForTally(@RequestBody TransactionVO txVO) {
		JSONResponseVO jSonVO=new JSONResponseVO();
		APIResponseVO apiResponse=new APIResponseVO();
		List<AccountHeadVO> ledgerList=new ArrayList();
		SocietyVO societyVo=new SocietyVO();
			logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getLedgerListForTally(@RequestBody RequestWrapper requestWrapper)")	;
		try {
		
			if(txVO.getSocietyID()>0){
			 			
			 ledgerList=ledgerService.getMasterLedgerList(txVO.getSocietyID(), 0,0);
			 jSonVO.setSocietyID(txVO.getSocietyID());
			 jSonVO.setSocietyName(societyVo.getSocietyName());
			if(ledgerList.size()>0){
			  jSonVO.setObjectList(ledgerList);
			  jSonVO.setStatusCode(200);
			}
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception in getTransactionListForTally: "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getTransactionListForTally(@RequestBody RequestWrapper requestWrapper)")	;						
		return new ResponseEntity(jSonVO, HttpStatus.OK);
 
	}
	
	/*----------- API to get list of transactions for tally export --------------*/
	@RequestMapping(value="/transactionListForTally", method = RequestMethod.POST)
	public ResponseEntity getTransactionListForTally(@RequestBody TransactionVO txVO) {
		JSONResponseVO jSonVO=new JSONResponseVO();
		APIResponseVO apiResponse=new APIResponseVO();
		List<TransactionVO> transactionList=new ArrayList();
		SocietyVO societyVo=new SocietyVO();
			logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getTransactionListForTally(@RequestBody RequestWrapper requestWrapper)")	;
		try {
		
			if(txVO.getSocietyID()>0){
			 societyVo=societyService.getSocietyDetails(txVO.getSocietyID());
			
			
			 transactionList=accountsAutoService.getTransactionDetails(txVO.getSocietyID(), txVO.getFromDate(), txVO.getToDate());
			 jSonVO.setSocietyID(txVO.getSocietyID());
			 jSonVO.setSocietyName(societyVo.getSocietyName());
			if(transactionList.size()>0){
			  jSonVO.setObjectList(transactionList);
			  jSonVO.setStatusCode(200);
			}
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception in getTransactionListForTally: "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getTransactionListForTally(@RequestBody RequestWrapper requestWrapper)")	;						
		return new ResponseEntity(jSonVO, HttpStatus.OK);
 
	}
	
	
	//===============--------Subscription license management ------------===============//
	
	//--------------------- API to restrict the access of license expired organizations to read only mode-----------------------------//
	@RequestMapping(value="/restrictSubscription", method = RequestMethod.POST)
			public ResponseEntity restrictSubscriptionExpiredOrgs() {
				APIResponseVO apiResponse=new APIResponseVO();
				JSONResponseVO jsonResponse=new JSONResponseVO();
			    int success=0;
					logger.debug("Entry : public ResponseEntity restrictSubscriptionExpiredOrgs(@RequestBody EventVO eventVO)" );
				try {
					
					
					
					success=societyService.restrictSubscriptionExpiredOrgs();
					if(success==1){
						jsonResponse.setStatusCode(200);
						
					}
					  
					
				} catch (Exception e) {
					logger.error("Exception in public ResponseEntity restrictSubscriptionExpiredOrgs(@RequestBody EventVO eventVO) "+e);
					apiResponse.setStatusCode(404);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
				}
				logger.debug("Exit : public ResponseEntity restrictSubscriptionExpiredOrgs(@RequestBody EventVO eventVO)")	;						
				return new ResponseEntity(jsonResponse, HttpStatus.OK);
		 
			}
	
	/*----------- API to get device details --------------*/
	@RequestMapping(value="/getDeviceDetails", method = RequestMethod.POST)
	public ResponseEntity getUserDeviceDetails(@RequestBody UserVO userVO) {
		APIResponseVO apiResponse=new APIResponseVO();
		UserVO userDeviceVO=new UserVO();
		
			logger.debug("Entry : public ResponseEntity getUserDeviceDetails(@RequestBody UserVO userVO)")	;
		try {
		
			if((userVO.getUserID()!=null)&&(userVO.getAppID()!=null)){
		
				userDeviceVO=userService.getUserDeviceDetails(userVO.getUserID(), userVO.getAppID());
				
		}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception in public ResponseEntity getUserDeviceDetails(@RequestBody UserVO userVO): "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity getUserDeviceDetails(@RequestBody UserVO userVO)")	;						
		return new ResponseEntity(userDeviceVO, HttpStatus.OK);
 
	}
	
	/*------------------Get configured banks details-----------------*/
	@RequestMapping(value="/bank/getConfiguredBankEmailDetails",  method = RequestMethod.POST  )			
	public ResponseEntity getConfiguredBankStatementList() {
		APIResponseVO apiResponse=new APIResponseVO();	
		List bankStatementList=new ArrayList();
		BankInfoVO bankInfoVO=new BankInfoVO();
	
			logger.debug("Entry : public ResponseEntity getConfiguredBankStatementList()")	;
		try {
			
			
				
					 bankStatementList=accountsAutoService.getConfiguredBankStatementList();
				
				if(bankStatementList.size()>0){
					bankInfoVO.setStatusCode(200);
					bankInfoVO.setBankList((ArrayList<BankInfoVO>) bankStatementList);
				}
				
			
			
			

			
			
			
			
			
		} catch (Exception e) {
			logger.error("Exception public ResponseEntity getConfiguredBankStatementList "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity getConfiguredBankStatementList")	;						
		return new ResponseEntity(bankInfoVO, HttpStatus.OK);
 
	}

	/*----------- API to get device details --------------*/
	@RequestMapping(value="/getSMSGatewayDetails", method = RequestMethod.POST)
	public ResponseEntity getSMSGatewayDetails(@RequestBody NotificationVO notificationVO) {
		APIResponseVO apiResponse=new APIResponseVO();
		SMSGatewayVO smsGateway=new SMSGatewayVO();
					logger.debug("Entry : public ResponseEntity getSMSGatewayDetails(@RequestBody NotificationVO notificationVO)")	;
		try {
		
			if(notificationVO.getOrgID()>0){
				
				smsGateway=notificationService.getSMSGatewayDetails(notificationVO.getOrgID());
				
				
		}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(smsGateway, HttpStatus.BAD_REQUEST);
			}	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception in public ResponseEntity getSMSGatewayDetails(@RequestBody NotificationVO notificationVO): "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity getSMSGatewayDetails(@RequestBody NotificationVO notificationVO)")	;						
		return new ResponseEntity(smsGateway, HttpStatus.OK);
 
	}
	
	
	/*----------- API to allot monthly SMS credits --------------*/
	@RequestMapping(value="/allotSMSCredits", method = RequestMethod.POST)
	public ResponseEntity allotSMSCredits() {
		APIResponseVO apiResponse=new APIResponseVO();
		int success=0;
					logger.debug("Entry : public ResponseEntity getSMSGatewayDetails(@RequestBody NotificationVO notificationVO)")	;
		try {
		
			
				
				success=notificationService.allotSMSCredits();
				if(success!=0){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("SMS Credits allotted to "+success+" no of organizations");
					logger.info("SMS Credits allotted to "+success+" no of organizations");
				}
				
				
		
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception in public ResponseEntity allotSMSCredits: "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity allotSMSCredits")	;						
		return new ResponseEntity(apiResponse, HttpStatus.OK);
 
	}
	
	/*----------- API to get Profile list --/Member/Customer/Vendor/Employee/------------*/
	@RequestMapping(value="/getProfileDetailedList", method = RequestMethod.POST)
	public ResponseEntity getProfileDetailedList(@RequestBody NotificationVO notificationVO) {
		APIResponseVO apiResponse=new APIResponseVO();
		List userList=new ArrayList<UserVO>();
					logger.debug("Entry : public ResponseEntity getProfileDetailedList(@RequestBody NotificationVO notificationVO)")	;
		try {
		
			if((notificationVO.getOrgID()>0)&&(notificationVO.getProfileType().length()>0)&&(notificationVO.getGroupName().length()>0)){
				
				userList=notificationService.getProfileDetailedList(notificationVO);
				
				
		}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(userList, HttpStatus.BAD_REQUEST);
			}	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception in public ResponseEntity getProfileDetailedList(@RequestBody NotificationVO notificationVO) "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity getProfileDetailedList(@RequestBody NotificationVO notificationVO)")	;						
		return new ResponseEntity(userList, HttpStatus.OK);
 
	}
	
	
	/*------------------Send Notification API------------------*/
	@RequestMapping(value="/sendNotification",  method = RequestMethod.POST  )			
	public ResponseEntity sendNotification(@RequestBody NotificationVO notificationVO) {
							
			logger.debug("Entry : public ResponseEntity sendNotification(@RequestBody NotificationVO notificationVO)")	;
		try {
			int orgID=notificationVO.getOrgID();
			
			notificationVO=notificationService.SendNotifications(notificationVO);
			
			notificationVO.setOrgID(orgID);
		} catch (Exception e) {
			logger.error("Exception in public ResponseEntity sendNotification(@RequestBody NotificationVO notificationVO) "+e);
			
		}
		logger.debug("Exit : public ResponseEntity sendNotification(@RequestBody NotificationVO notificationVO) ")	;						
		return new ResponseEntity(notificationVO, HttpStatus.OK);
 
	}
	
	
	/*----------- API to transfer transactions from any organization --------------*/
	@RequestMapping(value="/transferOrgTxns", method = RequestMethod.POST)
	public ResponseEntity transferTransactionsFromOrg(@RequestBody SocietyVO societVO) {
		APIResponseVO apiResponse=new APIResponseVO();
		SMSGatewayVO smsGateway=new SMSGatewayVO();
					logger.debug("Entry : public ResponseEntity getSMSGatewayDetails(@RequestBody NotificationVO notificationVO)")	;
		try {
		
			if(societVO.getSocietyID()>0){
		
				int success=transactionService.transferOrganizationsTransaction(societVO.getSocietyID(), societVO.getOrgID());
				
		}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(smsGateway, HttpStatus.BAD_REQUEST);
			}	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception in public ResponseEntity getSMSGatewayDetails(@RequestBody NotificationVO notificationVO): "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity getSMSGatewayDetails(@RequestBody NotificationVO notificationVO)")	;						
		return new ResponseEntity(smsGateway, HttpStatus.OK);
 
	}
	
	
	/*---------------------------- Transactions Module insert,update,delete ------------------*/
	
	@RequestMapping(value="/insert/expenseTransaction",  method = RequestMethod.POST  )			
	public ResponseEntity insertExpenseTransaction(@RequestBody TransactionVO txVO) {
		int success=0;
		APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public @ResponseBody int insertExpenseTransaction( TransactionVO txVO)")	;
		try {
			logger.info("insertTransaction:  org ID: "+txVO.getSocietyID()+" Amount: "+txVO.getAmount());
			if(txVO.getSocietyID()>0){
				
		       txVO=transactionService.addTransaction(txVO, txVO.getSocietyID());
		       
		   	if(txVO.getStatusCode()!=200){
				apiResponse.setStatusCode(txVO.getStatusCode());
				apiResponse.setMessage(txVO.getErrorMessage());
				logger.info("Unable to insert transaction details Status Code: "+txVO.getStatusCode());
				return new ResponseEntity(txVO, HttpStatus.NOT_ACCEPTABLE);				
				
			}else{
				
				apiResponse.setStatusCode(200);
				apiResponse.setMessage("Transaction added successfully ");
						
			}
		       
			
			
			
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(txVO, HttpStatus.BAD_REQUEST);
			}
			logger.info("Result of Transaction insertion : "+txVO.getTransactionID());				
			
		} catch (Exception e) {
			logger.error("Exception  "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(txVO, HttpStatus.BAD_REQUEST);
			
		}
		logger.debug("Exit : public @ResponseBody int insertExpenseTransaction( TransactionVO txVO)")	;						
		return new ResponseEntity(txVO, HttpStatus.OK);
 
	}
	
	@RequestMapping(value="/report/budgetedReportForGroup", method = RequestMethod.POST)
	public ResponseEntity getBudgetedBudgetReportForGroup(@RequestBody BudgetDetailsVO requestWrapper) {
		APIResponseVO apiResponse=new APIResponseVO();
		BudgetDetailsVO rptVO=new BudgetDetailsVO();
			logger.debug("Entry : public @ResponseBody ReportVO getBudgetedBudgetReportForGroup(@RequestBody RequestWrapper requestWrapper)" );
		try {
			
			if(requestWrapper.getOrgID()>0  ){
				
			  rptVO=budgetService.getBudgetedReportForGroup(requestWrapper);
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in getBudgetedBudgetReportForGroup "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
		}
		logger.debug("Exit : public @ResponseBody ReportVO getBudgetedBudgetReportForGroup(@RequestBody RequestWrapper requestWrapper)")	;						
		return new ResponseEntity(rptVO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/getMemberInfo",  method = RequestMethod.POST  )			
	public ResponseEntity getMemberDetails(@RequestBody MemberVO memberVO) {
		int success=0;
		APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public ResponseEntity getMemberDetails(@RequestBody MemberVO memberVO)")	;
		try {
			
			if(memberVO.getMemberID()>0){
				
				//memberVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
				
				memberVO=memberService.searchMembersInfo(Integer.toString(memberVO.getMemberID()));
				
		    if(memberVO.getFullName().length()<0){					
				apiResponse.setStatusCode(400);
				apiResponse.setMessage("Unable to get member details.");					
				return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
			}
			
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in getMemberDetails "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit :public ResponseEntity getMemberDetails(@RequestBody MemberVO memberVO)");						
		return new ResponseEntity(memberVO, HttpStatus.OK);
 
	}
	
	
	 /*Sample body contents for PrintReportVO */
	/*{
		"printParam":{
		"orgID":"11",
		"fromDate":"2016-04-01",
		"toDate":"2016-06-30",
		"dueDate":"2016-06-30",
		"billingCycleID":"5",
		"aptID":"5"
		},
		"reportContentType":"pdf"
		
		}*/
	@RequestMapping(value="/bill", method = RequestMethod.POST)
	public void downloadBill(HttpServletResponse response,@RequestBody PrintReportVO printVO) {
		TransactionVO printTxVO=new TransactionVO();
		APIResponseVO apiResponse=new APIResponseVO();
		HashMap<String, Object> param= printVO.getPrintParam();
		logger.debug("Entry : public @ResponseBody PrintReportVO downloadBill(@RequestBody TransactionVO printVO) ");
		try {
			
			
			param.put("orgID", printVO.getSocietyVO().getSocietyID());
			
				printTxVO.setPrintParam(param);
			    
				printTxVO.setFromDate((String) param.get("fromDate"));
				printTxVO.setToDate((String) param.get("toDate"));
				printTxVO.setDueDate((String) param.get("dueDate"));
				
				printTxVO.setSourceType("BEAN");
				printTxVO.setReportContentType(printVO.getReportContentType());														
				//printTxVO.setReportID("template.bill");				
				
				printVO=printReportService.printBill(printTxVO);
				printVO.setBeanList(null);			
				
				FileInputStream myStream = new FileInputStream(printVO.getReportURL());

				// Set the content type and attachment header.
				response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
				response.addHeader("x-filename",printVO.getReportID());
				response.setContentType("application/pdf");
				
				// Copy the stream to the response's output stream.
				IOUtils.copy(myStream, response.getOutputStream());				

				response.flushBuffer();
			
		} catch (IOException e) {
			logger.error("Exception in downloadBill: "+e);
			
		}
		logger.debug("Exit : public @ResponseBody PrintReportVO downloadBill(@RequestBody TransactionVO printVO) ");						
		
 
	}
	
	
	@RequestMapping(value="/billDetails", method = RequestMethod.POST)
	public ResponseEntity getBillsDetails(@RequestBody RequestWrapper requestWrapper) {
		BillDetailsVO billVO=new BillDetailsVO();
		APIResponseVO apiResponse=new APIResponseVO();
		List billList=new ArrayList();
			logger.debug("Entry : public @ResponseBody BillDetailsVO getBillsDetailsSociety(@RequestBody RequestWrapper requestWrapper)")	;
		try {
			if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0 && requestWrapper.getAptID()>0){
				
				billVO=invoiceService.getBillDetails(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getAptID());
			
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception in getBillsDetailsSociety "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody BillDetailsVO getBillsDetailsSociety(@RequestBody RequestWrapper requestWrapper)");						
		return new ResponseEntity(billVO, HttpStatus.OK);
 
	}
	
	/*----------- API to create new users for any organization --------------*/
	/*@RequestMapping(value="/createUsersForOrg", method = RequestMethod.POST)
	public ResponseEntity createUsersForOrg(@RequestBody NotificationVO notificationVO) {
		APIResponseVO apiResponse=new APIResponseVO();
		SMSGatewayVO smsGateway=new SMSGatewayVO();
					logger.debug("Entry : public ResponseEntity getSMSGatewayDetails(@RequestBody NotificationVO notificationVO)")	;
		try {
		
			if(notificationVO.getOrgID()>0){
		
				  List memberList=memberService.getMembersListFromGroup(notificationVO.getOrgID(), "primary");
				  if(memberList.size()>0)
				  for(int i=0;memberList.size()>i;i++){
					  MemberVO memberVO=(MemberVO) memberList.get(i);
					  UserVO userVO=new UserVO();
					  userVO.setLoginName(memberVO.getEmail());
					  userVO.setAppID("1");
					  userVO.setCustomUserName("O"+notificationVO.getOrgID()+"-"+memberVO.getFlatNo());
					  if((memberVO.getEmail()!=null)&&(memberVO.getEmail().length()>0))
					  userVO=userService.memberAppRegisterUser(userVO);
					  
				  }
				  
				
		}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(smsGateway, HttpStatus.BAD_REQUEST);
			}	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception in public ResponseEntity getSMSGatewayDetails(@RequestBody NotificationVO notificationVO): "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity getSMSGatewayDetails(@RequestBody NotificationVO notificationVO)")	;						
		return new ResponseEntity(smsGateway, HttpStatus.OK);
 
	}*/
	
	
}
