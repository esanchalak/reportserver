package com.emanager.webservice;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.GatewayVO;
import com.emanager.server.accounts.DataAccessObjects.LedgerEntries;
import com.emanager.server.accounts.DataAccessObjects.OnlinePaymentGatewayVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.adminReports.Services.PrintReportService;
import com.emanager.server.adminReports.valueObject.PrintReportVO;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.invoice.Services.InvoiceService;
import com.emanager.server.invoice.dataAccessObject.InvoiceDetailsVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.userManagement.valueObject.UserVO;


@Controller
@RequestMapping("/gateway")
public class JSONPaymentGatewayController {
	
	private static final Logger logger = Logger.getLogger(JSONPaymentGatewayController.class);
	@Autowired
	TransactionService transactionService;
	@Autowired
    InvoiceService invoiceService;
	@Autowired
    LedgerService ledgerService;
	@Autowired
    SocietyService societyService;
	@Autowired
    PrintReportService printReportService;
	DateUtility dateUtil=new DateUtility();
	public ConfigManager configManager =new ConfigManager();
	
	 		
		@RequestMapping(value="/onlinePaymentDetails",  method = RequestMethod.POST  )			
		public ResponseEntity getOnlinePaymentDetails(@RequestBody InvoiceDetailsVO txVO) {
			int success=0;
			InvoiceDetailsVO invoiceVO=new InvoiceDetailsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			AccountHeadVO achVo=new AccountHeadVO();
			String decryptedStrng = null;
			List onlPayVOList=null;
			 OnlinePaymentGatewayVO onlinePayment=new OnlinePaymentGatewayVO();
			logger.debug("Entry : public @ResponseBody int getOnlinePaymentDetails( InvoiceDetailsVO txVO)")	;
			try {
				
	 			Date currentDate=dateUtil.findCurrentDate();
				BigDecimal total=BigDecimal.ZERO;
				EncryptDecryptUtility endeUtil=new EncryptDecryptUtility();
				Object String;
				if(txVO.getSecretKey().length()>0)
				decryptedStrng=endeUtil.decrypt(txVO.getSecretKey());
				
				String[] decrypedArray = decryptedStrng.split(",");
				String societyID=decrypedArray[0];
				String aptID=decrypedArray[2];
				String ledgerID=decrypedArray[1];		
				//String userType=decrypedArray[3];	
				//achVo=ledgerService.getLedgerID(aptID, societyID, "M","APTID");
				 
				achVo=ledgerService.getOpeningClosingBalance(Integer.parseInt(societyID), Integer.parseInt(ledgerID), currentDate.toString(), currentDate.toString(), "L");
				 			
				 invoiceVO.setLedgerID(achVo.getLedgerID());
				 invoiceVO.setBalance(achVo.getClosingBalance());
				 
				 logger.info("Online Payment gateway of Society ID: "+societyID+" APT ID: "+aptID+" Ledger ID: "+ledgerID+" Due: "+invoiceVO.getBalance());				
				 
				 onlinePayment=invoiceService.getOnlinePaymentDetailsForMember(Integer.parseInt(societyID),Integer.parseInt(aptID) ,Integer.parseInt(ledgerID));
				
				 logger.info("Gateway Available : "+onlinePayment.getIsOnlnPymtAccptd());
				 
				 invoiceVO.setIsPaymentGatewayAvailable(onlinePayment.getIsOnlnPymtAccptd());				
				 invoiceVO.setOpGatewayVO(onlinePayment);
				 
				 if((invoiceVO.getIsPaymentGatewayAvailable()==1) && (invoiceVO.getBalance().compareTo(BigDecimal.ZERO)>0)){
					 invoiceVO=invoiceService.calculateProcessingFee(invoiceVO);
				 }
				 
				 
			//logger.info("Online Payment gateway method for geting dueAmount of Society ID: "+societyID+" APT ID: "+aptID+" Due: "+invoiceVO.getBalance());				
				
			} catch (Exception e) {
				logger.error("Exception  getOnlinePaymentDetails"+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				
			}
			logger.debug("Exit : public @ResponseBody int getOnlinePaymentDetails( InvoiceDetailsVO txVO)")	;						
			return new ResponseEntity(invoiceVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/calculateGatewayFees",  method = RequestMethod.POST  )			
		public ResponseEntity calculateGatewayFees(@RequestBody InvoiceDetailsVO invoiceVO) {
			int success=0;
			//InvoiceDetailsVO invoiceVO=new InvoiceDetailsVO();
			APIResponseVO apiResponseVO=new APIResponseVO();
			AccountHeadVO achVo=new AccountHeadVO();
			SocietyVO societyVO =new SocietyVO();
			String decryptedStrng = null;
			List onlPayVOList=null;
			 OnlinePaymentGatewayVO opGatewayVO=new OnlinePaymentGatewayVO();
			logger.debug("Entry : public @ResponseBody int calculateGatewayFees( InvoiceDetailsVO txVO)")	;
			try {
			 logger.info("Calculate gateway fees of Society ID: "+invoiceVO.getSocietyID()+" Due Amount: "+invoiceVO.getBalance());				
			 
			 if(invoiceVO.getSocietyID()>0 && invoiceVO.getBalance().compareTo(BigDecimal.ZERO)>0){	 
				 societyVO=societyService.getSocietyDetails("0", invoiceVO.getSocietyID());
				 opGatewayVO =transactionService.getOnlinePaymentDetails(invoiceVO.getSocietyID(), "C");
				 opGatewayVO.setSocietyVO(societyVO);
				 invoiceVO.setOpGatewayVO(opGatewayVO);
				 
				 invoiceVO=invoiceService.calculateProcessingFee(invoiceVO);
			   
				 
			}else{
				apiResponseVO.setStatusCode(400);
				apiResponseVO.setMessage(configManager.getPropertiesValue("error."+apiResponseVO.getStatusCode()));
				return new ResponseEntity(apiResponseVO, HttpStatus.BAD_REQUEST);
			}
				
			} catch (Exception e) {
				logger.error("Exception  getOnlinePaymentDetails"+e);
				apiResponseVO.setStatusCode(400);
				apiResponseVO.setMessage(configManager.getPropertiesValue("error."+apiResponseVO.getStatusCode()));
				return new ResponseEntity(apiResponseVO, HttpStatus.BAD_REQUEST);
				
			}
			logger.debug("Exit : public @ResponseBody int calculateGatewayFees( InvoiceDetailsVO txVO)")	;						
			return new ResponseEntity(invoiceVO, HttpStatus.OK);
	 
		}
		
		/*------------------Online Receipt Entry API------------------*/
		@RequestMapping(value="/insert/onlinePayment",  method = RequestMethod.POST  )			
		public ResponseEntity addOnlinePayment(@RequestBody TransactionVO txVO) {
			int genTxID=0;
			APIResponseVO apiResponseVO=new APIResponseVO();
				logger.info("Entry : public @ResponseBody int addOnlinePayment( TransactionVO txVO)")	;
			try {
				logger.info("Online transaction initiated for society ID "+txVO.getSocietyID()+" for amount "+txVO.getAmount());
				if(txVO.getSocietyID()>0){
				LedgerEntries debtLedgerEntry=new LedgerEntries();
				LedgerEntries credLedgerEntry=new LedgerEntries();
				AccountHeadVO bankVO=new AccountHeadVO();
			if(txVO.getIsCalculationInclusive().equalsIgnoreCase("0")){
				
				
			}else{
				List journalEntries=txVO.getLedgerEntries();
				txVO.setLedgerEntries(null);
				for(int i=0;journalEntries.size()>i;i++){
					LedgerEntries lEntries= (LedgerEntries) journalEntries.get(i);
					
					if(lEntries.getCreditDebitFlag().equalsIgnoreCase("D")){
						journalEntries.remove(i);
						i--;
					}else {
						credLedgerEntry=lEntries;
						
					}
					
				}
				
				//bankVO=ledgerService.getBankLedger(txVO.getSocietyID());
				bankVO=ledgerService.getPaymentGatewayLedger(txVO.getSocietyID());
				logger.info(" Bank Ledger ID: "+bankVO.getLedgerID());
				if(bankVO.getLedgerID()!=0){
				debtLedgerEntry.setLedgerID(bankVO.getLedgerID());
				debtLedgerEntry.setCreditDebitFlag("D");
				debtLedgerEntry.setAmount(credLedgerEntry.getAmount());
				txVO.setLedgerID(credLedgerEntry.getLedgerID());
				
				journalEntries.add(0,debtLedgerEntry);
				}
				txVO.setLedgerEntries(journalEntries);
				logger.info("Ledger Entry size: "+txVO.getLedgerEntries().size()+" Amount: "+txVO.getAmount()+" Memebr ledgerID:  "+txVO.getLedgerID());								
				if(txVO.getTransactionType().equalsIgnoreCase("Receipt")){
				    txVO=invoiceService.addReceptForInvoiceForOnlinePayment(txVO.getSocietyID(), txVO);
				}else{
					txVO=transactionService.addTransaction(txVO, txVO.getSocietyID());
				}
				
			}	
				
				if(txVO.getTransactionID()!=0){
						
					logger.info("The online payment for Society ID :"+txVO.getSocietyID()+" of Amount : "+txVO.getAmount()+" for the ledgerID  "+txVO.getLedgerID()+" has been inserted successfully");
				}else{
						logger.info("The online payment for Society ID :"+txVO.getSocietyID()+" of Amount : "+txVO.getAmount()+" for the ledgerID  "+txVO.getLedgerID()+" has been failed, Please look into this transaction");
				}
				
				if(txVO.getStatusCode()!=200){
					apiResponseVO.setStatusCode(txVO.getStatusCode());
					apiResponseVO.setMessage(txVO.getErrorMessage());					
					return new ResponseEntity(apiResponseVO, HttpStatus.NOT_ACCEPTABLE);				
					
				}else{
					apiResponseVO.setStatusCode(200);
					apiResponseVO.setMessage("Transaction added successfully ");
				}
			       
              
				
				
				}else{
					apiResponseVO.setStatusCode(400);
					apiResponseVO.setMessage(configManager.getPropertiesValue("error."+apiResponseVO.getStatusCode()));
					return new ResponseEntity(apiResponseVO, HttpStatus.BAD_REQUEST);
				}
				
				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponseVO.setStatusCode(400);
				apiResponseVO.setMessage(configManager.getPropertiesValue("error."+apiResponseVO.getStatusCode()));
				return new ResponseEntity(apiResponseVO, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int insertTransaction( TransactionVO txVO)")	;						
			return new ResponseEntity(apiResponseVO, HttpStatus.OK);
	 
		}
		
		//add this Iclusive onlinePayment gateway controllerr
	/*	@RequestMapping(value="/insert/onlinePayment",  method = RequestMethod.POST  )			
				public ResponseEntity addOnlinePayment(@RequestBody TransactionVO txVO) {
					int genTxID=0;
					APIResponseVO apiResponseVO=new APIResponseVO();
						logger.info("Entry : public @ResponseBody int addOnlinePayment( TransactionVO txVO)")	;
					try {
						logger.info("Online transaction initiated for society ID "+txVO.getSocietyID()+" for amount "+txVO.getAmount());
						if(txVO.getSocietyID()>0){
						LedgerEntries debtLedgerEntry=new LedgerEntries();
						LedgerEntries credLedgerEntry=new LedgerEntries();
						AccountHeadVO bankVO=new AccountHeadVO();
						LedgerEntries debtPgEntry=new LedgerEntries();
						LedgerEntries credPgEntry=new LedgerEntries();
						
					if(txVO.getIsCalculationInclusive().equalsIgnoreCase("1")|| txVO.getIsCalculationInclusive()!=null){//inclusive 
						//member
						List journalEntries=txVO.getLedgerEntries();
						txVO.setLedgerEntries(null);
						for(int i=0;journalEntries.size()>i;i++){
							LedgerEntries lEntries= (LedgerEntries) journalEntries.get(i);
							
							if(lEntries.getCreditDebitFlag().equalsIgnoreCase("D")){
								journalEntries.remove(i);
								i--;
							}else {
								credLedgerEntry=lEntries;
								
							}
							
						}
						
						//bankVO=ledgerService.getBankLedger(txVO.getSocietyID());
						bankVO=ledgerService.getPaymentGatewayLedger(txVO.getSocietyID());
						logger.info(" Bank Ledger ID: "+bankVO.getLedgerID());
						if(bankVO.getLedgerID()!=0){
						debtLedgerEntry.setLedgerID(bankVO.getLedgerID());
						debtLedgerEntry.setCreditDebitFlag("D");
						debtLedgerEntry.setAmount(txVO.getPrincipleAmount());
						txVO.setLedgerID(credLedgerEntry.getLedgerID());
						
						journalEntries.add(0,debtLedgerEntry);
						}
						
						//commision entry
						BigDecimal commmisionAmt=BigDecimal.ZERO;
						commmisionAmt=txVO.getSumAmt().subtract(txVO.getPrincipleAmount());
						AccountHeadVO pgChargesLedger=ledgerService.getLedgerByType(txVO.getSocietyID(), "Pgcharges");
						logger.info(" PG Charges ID: "+pgChargesLedger.getLedgerID()+" Charges: "+commmisionAmt);
				
						//payment gateway charges
						debtPgEntry.setLedgerID(pgChargesLedger.getLedgerID());
						debtPgEntry.setCreditDebitFlag("D");
						debtPgEntry.setAmount(commmisionAmt);
										
						journalEntries.add(1,debtPgEntry);
						
						txVO.setLedgerEntries(journalEntries);
						
						logger.info("Ledger Entry size: "+txVO.getLedgerEntries().size()+" Amount: "+txVO.getAmount()+" Memebr ledgerID:  "+txVO.getLedgerID());								
						if(txVO.getTransactionType().equalsIgnoreCase("Receipt")){
						    txVO=invoiceService.addReceptForInvoiceForOnlinePayment(txVO.getSocietyID(), txVO);
						}else{
							txVO=transactionService.addTransaction(txVO, txVO.getSocietyID());
						}
						
					
						
					}else{
						//Exclusive
						List journalEntries=txVO.getLedgerEntries();
						txVO.setLedgerEntries(null);
						for(int i=0;journalEntries.size()>i;i++){
							LedgerEntries lEntries= (LedgerEntries) journalEntries.get(i);
							
							if(lEntries.getCreditDebitFlag().equalsIgnoreCase("D")){
								journalEntries.remove(i);
								i--;
							}else {
								credLedgerEntry=lEntries;
								
							}
							
						}
						
						//bankVO=ledgerService.getBankLedger(txVO.getSocietyID());
						bankVO=ledgerService.getPaymentGatewayLedger(txVO.getSocietyID());
						logger.info(" Bank Ledger ID: "+bankVO.getLedgerID());
						if(bankVO.getLedgerID()!=0){
						debtLedgerEntry.setLedgerID(bankVO.getLedgerID());
						debtLedgerEntry.setCreditDebitFlag("D");
						debtLedgerEntry.setAmount(credLedgerEntry.getAmount());
						txVO.setLedgerID(credLedgerEntry.getLedgerID());
						
						journalEntries.add(0,debtLedgerEntry);
						}
						txVO.setLedgerEntries(journalEntries);
						logger.info("Ledger Entry size: "+txVO.getLedgerEntries().size()+" Amount: "+txVO.getAmount()+" Memebr ledgerID:  "+txVO.getLedgerID());								
						if(txVO.getTransactionType().equalsIgnoreCase("Receipt")){
						    txVO=invoiceService.addReceptForInvoiceForOnlinePayment(txVO.getSocietyID(), txVO);
						}else{
							txVO=transactionService.addTransaction(txVO, txVO.getSocietyID());
						}
						
					}	
						
						if(txVO.getTransactionID()!=0){
								
							logger.info("The online payment for Society ID :"+txVO.getSocietyID()+" of Amount : "+txVO.getAmount()+" for the ledgerID  "+txVO.getLedgerID()+" has been inserted successfully");
						}else{
								logger.info("The online payment for Society ID :"+txVO.getSocietyID()+" of Amount : "+txVO.getAmount()+" for the ledgerID  "+txVO.getLedgerID()+" has been failed, Please look into this transaction");
						}
						
						if(txVO.getStatusCode()!=200){
							apiResponseVO.setStatusCode(txVO.getStatusCode());
							apiResponseVO.setMessage(txVO.getErrorMessage());					
							return new ResponseEntity(apiResponseVO, HttpStatus.NOT_ACCEPTABLE);				
							
						}else{
							apiResponseVO.setStatusCode(200);
							apiResponseVO.setMessage("Transaction added successfully ");
						}
					       
		              
						
						
						}else{
							apiResponseVO.setStatusCode(400);
							apiResponseVO.setMessage(configManager.getPropertiesValue("error."+apiResponseVO.getStatusCode()));
							return new ResponseEntity(apiResponseVO, HttpStatus.BAD_REQUEST);
						}
						
						
						
					} catch (Exception e) {
						logger.error("Exception  "+e);
						apiResponseVO.setStatusCode(400);
						apiResponseVO.setMessage(configManager.getPropertiesValue("error."+apiResponseVO.getStatusCode()));
						return new ResponseEntity(apiResponseVO, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public @ResponseBody int insertTransaction( TransactionVO txVO)")	;						
					return new ResponseEntity(apiResponseVO, HttpStatus.OK);
			 
				}
*/
		
		/*------------------Online Receipt Entry Webhook API------------------*/
		@RequestMapping(value="/insert/onlinePaymentWebhook",  method = RequestMethod.POST  )
		public ResponseEntity addOnlinePaymentWebHook(@RequestBody GatewayVO gatewayVO) {
		int genTxID=0;
		APIResponseVO apiResponseVO=new APIResponseVO();
		OnlinePaymentGatewayVO opGatewayVO=new OnlinePaymentGatewayVO();
		LedgerEntries debtLedgerEntry=new LedgerEntries();
		LedgerEntries credLedgerEntry=new LedgerEntries();
		AccountHeadVO bankVO=new AccountHeadVO();
		TransactionVO txVO=new TransactionVO();

		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd" );  
		List journalEntries=new ArrayList();

		logger.info("Entry : public @ResponseBody int addOnlinePaymentWebHook( GatewayVO gatewayVO)") ;
		try {
		logger.info("Payment ID - "+gatewayVO.getPaymentId());
		logger.info("Gateway Object "+gatewayVO.toString());
		if((gatewayVO.getUdf1()!=null)&&(gatewayVO.getUdf1().length()>0)){
			
			
			logger.info("Gateway Object "+gatewayVO.toString());

		Date currentDate=dateUtil.findCurrentDate();
		logger.info("Online Payment Webhook initiated for OrgID: "+gatewayVO.getUdf1()+" User: "+gatewayVO.getUdf5()+" for amount "+gatewayVO.getUdf4()+" PaymentID: "+gatewayVO.getPaymentId());
		//Fill TransactionVO
		txVO.setSocietyID(Integer.parseInt(gatewayVO.getUdf1()));
		txVO.setTransactionDate(dateFormat.format(dateFormat));
		txVO.setTransactionMode("Online Payment");
		txVO.setTransactionType("Receipt");
		txVO.setRefNumber(gatewayVO.getPaymentId());
		txVO.setAmount(new BigDecimal(gatewayVO.getUdf4()));
		txVO.setDescription("Being amount received by Online Payment");
		txVO.setCreatedBy(15);
		txVO.setUpdatedBy(15);
		txVO.setSumAmt(gatewayVO.getAmount());
		txVO.setPrincipleAmount(new BigDecimal(gatewayVO.getUdf4()));
		txVO.setAutoInc(true);
		txVO.setDocID("");

		opGatewayVO =transactionService.getOnlinePaymentDetails(txVO.getSocietyID(), "C");
		txVO.setIsCalculationInclusive(opGatewayVO.getIsCalculationInclusive());


		credLedgerEntry.setLedgerID(Integer.parseInt(gatewayVO.getUdf2()));
		credLedgerEntry.setCreditDebitFlag("C");
		credLedgerEntry.setAmount(new BigDecimal(gatewayVO.getUdf4()));
		credLedgerEntry.setStrComments("Online Payment");
		journalEntries.set(0, credLedgerEntry);

		if(txVO.getIsCalculationInclusive().equalsIgnoreCase("0")){


		}else{

		bankVO=ledgerService.getPaymentGatewayLedger(txVO.getSocietyID());
		logger.info(" Bank Ledger ID: "+bankVO.getLedgerID());
		if(bankVO.getLedgerID()!=0){
		debtLedgerEntry.setLedgerID(bankVO.getLedgerID());
		debtLedgerEntry.setCreditDebitFlag("D");
		debtLedgerEntry.setAmount(credLedgerEntry.getAmount());
		txVO.setLedgerID(credLedgerEntry.getLedgerID());

		journalEntries.add(1,debtLedgerEntry);
		  }
		txVO.setLedgerEntries(journalEntries);

		logger.info("Ledger Entry size: "+txVO.getLedgerEntries().size()+" Amount: "+txVO.getAmount()+" Customer ledgerID:  "+txVO.getLedgerID());
		txVO=invoiceService.addReceptForInvoiceForOnlinePayment(txVO.getSocietyID(), txVO);

		}

		if(txVO.getTransactionID()!=0){

		logger.info("The online payment for OrgID :"+txVO.getSocietyID()+" of Amount : "+txVO.getAmount()+" for the ledgerID  "+txVO.getLedgerID()+" has been inserted successfully");
		}else{
		logger.info("The online payment for OrgID :"+txVO.getSocietyID()+" of Amount : "+txVO.getAmount()+" for the ledgerID  "+txVO.getLedgerID()+" has been failed, Please look into this transaction");
		}

		if(txVO.getStatusCode()!=200){
		apiResponseVO.setStatusCode(txVO.getStatusCode());
		apiResponseVO.setMessage(txVO.getErrorMessage());
		//return new ResponseEntity(apiResponseVO, HttpStatus.NOT_ACCEPTABLE);

		}else{
		apiResponseVO.setStatusCode(200);
		apiResponseVO.setMessage("Transaction added successfully ");
		}
		     
		             


		}else{
			
		apiResponseVO.setStatusCode(400);
		apiResponseVO.setMessage(configManager.getPropertiesValue("error."+apiResponseVO.getStatusCode()));
		//return new ResponseEntity(apiResponseVO, HttpStatus.BAD_REQUEST);
		}



		} catch (Exception e) {
		logger.error("Exception  "+e);
		apiResponseVO.setStatusCode(400);
		apiResponseVO.setMessage(configManager.getPropertiesValue("error."+apiResponseVO.getStatusCode()));
		return new ResponseEntity(apiResponseVO, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody int addOnlinePaymentWebHook( TransactionVO txVO)") ;
		return new ResponseEntity(apiResponseVO, HttpStatus.OK);

		}
		
		
		@RequestMapping(value="/receiptDwnld", method = RequestMethod.POST)
		public void downloadReceiptAndVoucher(HttpServletResponse response,@RequestBody TransactionVO printTxVO) {
		//	TransactionVO printTxVO=new TransactionVO();
			PrintReportVO printVO=new PrintReportVO();
			APIResponseVO apiResponse=new APIResponseVO();
			HashMap<String, Object> param= printTxVO.getPrintParam();
			logger.debug("Entry : public @ResponseBody PrintReportVO downloadIncomeExpenseReport(@RequestBody PrintReportVO printVO) ");
			try {
						
					
					//String printType=(String) param.get("printType");
					printTxVO.setSourceType("BEAN");
					printTxVO.setReportContentType("pdf");
					
						//printTxVO.setReportID("template.receipt");						
						printVO=printReportService.printMemberReceiptForMemberApp(printTxVO);
					   

										
					
					printVO.setBeanList(null);				
					FileInputStream myStream = new FileInputStream(printVO.getReportURL());

					// Set the content type and attachment header.
					response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
					response.addHeader("x-filename",printVO.getReportID());
					response.setContentType("application/pdf");
					
					// Copy the stream to the response's output stream.
					IOUtils.copy(myStream, response.getOutputStream());				

					response.flushBuffer();
				
			} catch (IOException e) {
				logger.error("Exception downloadReceiptAndVoucher "+e);
				
			}
			logger.debug("Exit : public @ResponseBody PrintReportVO downloadReceiptAndVoucher(@RequestBody PrintReportVO printVO) ");						
			
	 
		}


		

}
