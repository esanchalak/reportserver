package com.emanager.webservice;

import java.security.Key;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.adminReports.Services.NominationRegService;
import com.emanager.server.adminReports.Services.ShareRegService;
import com.emanager.server.adminReports.valueObject.RptDocumentVO;
import com.emanager.server.adminReports.valueObject.RptVehicleVO;
import com.emanager.server.adminReports.valueObject.ShareRegisterVO;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardVO;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.RenterVO;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.userLogin.services.LoginService;
import com.emanager.server.userManagement.valueObject.UserVO;
import com.sun.mail.handlers.message_rfc822;
	
	
	@Controller
	@RequestMapping("/secure/admin")
	public class JSONAdminController {
		
		private static final Logger logger = Logger.getLogger(JSONAdminController.class);
		@Autowired
		MemberService memberServiceBean;
	    @Autowired
		LoginService loginService;
	    @Autowired
	    ReportService reportService;	 
	    @Autowired
	    SocietyService societyService;
	    @Autowired
	    LedgerService ledgerService;
	    @Autowired
	    ShareRegService shareRegService;
	    @Autowired
	    NominationRegService nominationRegService;
	    public ConfigManager configManager =new ConfigManager();
	    public EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
		public final String HEADER_SECURITY_TOKEN = "X-AuthToken";        
		
		
		/*---------------------Admin Registers---------------------------------*/
		
		@RequestMapping(value="/report/jRegister", method = RequestMethod.POST)
		public ResponseEntity getJRegister(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			MemberVO memberVO=new MemberVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List jRegList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JSONResponseVO getJRegister(@RequestBody RequestWrapper requestWrapper)")	;
			try {
						
				if(requestWrapper.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
				  jRegList =  societyService.getJRegisterList(requestWrapper.getOrgID(), requestWrapper.getGroupID());		
				  if(jRegList.size()>0){
					memberVO.setStatusCode(1);
					memberVO.setSocietyID(requestWrapper.getOrgID());
					memberVO.setObjectList(jRegList);
				  }else{
					memberVO.setStatusCode(0);
			      }
		         }else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					logger.error("Exception in getJRegister "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}		
			logger.debug("Exit : public @ResponseBody JSONResponseVO getJRegister(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(memberVO, HttpStatus.OK);
	 
		}
		
		
		
		@RequestMapping(value="/report/iRegister", method = RequestMethod.POST)
		public ResponseEntity getIRegister(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			MemberVO memberVO=new MemberVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List jRegList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JSONResponseVO getIRegister(@RequestBody RequestWrapper requestWrapper)")	;
			try {
						
				if(requestWrapper.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
				  memberVO =  memberServiceBean.getIRegister(requestWrapper.getOrgID(), requestWrapper.getAptID());		
				  if(jRegList.size()>0){
					memberVO.setStatusCode(1);
					memberVO.setSocietyID(requestWrapper.getOrgID());
					memberVO.setObjectList(jRegList);
				  }else{
					memberVO.setStatusCode(0);
			      }
		         }else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					logger.error("Exception in getJRegister "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}		
			logger.debug("Exit : public @ResponseBody JSONResponseVO getJRegister(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(memberVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/report/shareRegisterForApt", method = RequestMethod.POST)
		public ResponseEntity getShareRegisterForApt(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			ShareRegisterVO shareVO=new ShareRegisterVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List jRegList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JSONResponseVO getShareRegisterForApt(@RequestBody RequestWrapper requestWrapper)")	;
			try {
						
				if(requestWrapper.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
				  shareVO =  shareRegService.getShareDetails( requestWrapper.getAptID());		
				 
		         }else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					logger.error("Exception in getShareRegisterForApt "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}		
			logger.debug("Exit : public @ResponseBody JSONResponseVO getShareRegisterForApt(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(shareVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/nominationRegisterForApt", method = RequestMethod.POST)
		public ResponseEntity getNominationRegisterForApt(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			ShareRegisterVO shareVO=new ShareRegisterVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List nominationList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JSONResponseVO getNominationRegisterForApt(@RequestBody RequestWrapper requestWrapper)")	;
			try {
						
				if(requestWrapper.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
				  nominationList =  memberServiceBean.getNominationRegisterForApt(requestWrapper.getOrgID(), requestWrapper.getAptID());		
				 
				 }else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					logger.error("Exception in getNominationRegisterForApt "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}		
			logger.debug("Exit : public @ResponseBody JSONResponseVO getNominationRegisterForApt(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(nominationList, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/shareRegister", method = RequestMethod.POST)
		public ResponseEntity getShareRegister(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			MemberVO memberVO=new MemberVO();
			List shareRegList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JSONResponseVO getShareRegister(@RequestBody RequestWrapper requestWrapper)")	;
			try {
						
				if(requestWrapper.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					shareRegList =  shareRegService.getShareList(requestWrapper.getGroupID(), requestWrapper.getOrgID());		
					if(shareRegList.size()>0){
						memberVO.setStatusCode(1);
						memberVO.setSocietyID(requestWrapper.getOrgID());
						memberVO.setObjectList(shareRegList);
					}else{
						memberVO.setStatusCode(0);
					}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getShareRegister "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}	
			logger.debug("Exit : public @ResponseBody JSONResponseVO getShareRegister(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(memberVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/report/tenantRegister", method = RequestMethod.POST)
		public ResponseEntity getTenantRegister(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			RenterVO renterVO=new RenterVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List tenantRegList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JSONResponseVO getTenantRegister(@RequestBody RequestWrapper requestWrapper)")	;
			try {
						
				if(requestWrapper.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					tenantRegList =  reportService.tenantDetailsRpt(requestWrapper.getOrgID());		
					if(tenantRegList.size()>0){
						renterVO.setStatusCode(1);
						renterVO.setRenterMembers(tenantRegList);
					}else{
						renterVO.setStatusCode(0);
					}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getTenantRegister "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JSONResponseVO getTenantRegister(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(renterVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/report/nominationRegister", method = RequestMethod.POST)
		public ResponseEntity getNominationRegister(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			MemberVO memberVO=new MemberVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List nominationRegList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JSONResponseVO getNominationRegister(@RequestBody RequestWrapper requestWrapper)")	;
			try {
						
				if(requestWrapper.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					nominationRegList =  nominationRegService.getNominationList(0, requestWrapper.getOrgID());		
					if(nominationRegList.size()>0){
						memberVO.setStatusCode(1);
						memberVO.setSocietyID(requestWrapper.getOrgID());
						memberVO.setObjectList(nominationRegList);
					}else
						memberVO.setStatusCode(0);
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getNominationRegister "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JSONResponseVO getNominationRegister(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(memberVO, HttpStatus.OK);
	 
		}
		
		
		
		@RequestMapping(value="/report/vehicleRegister", method = RequestMethod.POST)
		public ResponseEntity getVehicleRegister(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			RptVehicleVO vehicleVO=new RptVehicleVO();
			APIResponseVO apiResponse=new APIResponseVO();
			
				logger.debug("Entry : public @ResponseBody RptVehicleVO getVehicleRegister(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				if(requestWrapper.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
				  vehicleVO=reportService.getVehicleReportForAPI(requestWrapper.getOrgID());
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getVehicleRegister "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody RPTVehicleVO getVehicleRegister(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(vehicleVO, HttpStatus.OK);
	 
		}
		@RequestMapping(value="/report/mortgageRegister", method = RequestMethod.POST)
		public ResponseEntity getMortgageRegister(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			MemberVO memberVO=new MemberVO();
			List mortgageList=null;
			
				logger.debug("Entry : public @ResponseBody List getMortgageRegister(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				if(requestWrapper.getOrgID()>0){		
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
				  mortgageList=reportService.getMorgageDetails(requestWrapper.getOrgID());
				  if(mortgageList.size()>0){
					  memberVO.setStatusCode(1);
					  memberVO.setSocietyID(requestWrapper.getOrgID());
					  memberVO.setObjectList(mortgageList);
				  }else
					  memberVO.setStatusCode(0);
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getMortgageRegister "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody List getMortgageRegister(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(memberVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/documentRegister", method = RequestMethod.POST)
		public ResponseEntity getDocumentRegister(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			MemberVO memberVO=new MemberVO();
			List documentList=null;
			
				logger.debug("Entry : public @ResponseBody List getDocumentRegister(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				if(requestWrapper.getOrgID()>0){	
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
					documentList=reportService.getSingleDocumentReport(requestWrapper.getOrgID(), requestWrapper.getDocumentID());
				  if(documentList.size()>0){
					  memberVO.setStatusCode(1);
					  memberVO.setSocietyID(requestWrapper.getOrgID());
					  memberVO.setObjectList(documentList);
				  }else
					  memberVO.setStatusCode(0);
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getDocumentRegister "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody List getDocumentRegister(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(memberVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/transferRegister", method = RequestMethod.POST)
		public ResponseEntity getTransferRegister(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			MemberVO memberVO=new MemberVO();
			List transferRegList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JSONResponseVO getTransferRegister(@RequestBody RequestWrapper requestWrapper)")	;
			try {
						
				if((requestWrapper.getOrgID()>0)&&((requestWrapper.getFromDate()!=null)&&(requestWrapper.getToDate()!=null))){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					transferRegList =  shareRegService.getTransferRegister(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate());
					if(transferRegList.size()>0){
						memberVO.setStatusCode(1);
						memberVO.setSocietyID(requestWrapper.getOrgID());
						memberVO.setObjectList(transferRegList);
					}else{
						memberVO.setStatusCode(0);
					}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getTransferRegister "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}	
			logger.debug("Exit : public @ResponseBody JSONResponseVO getTransferRegister(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(memberVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/report/societyList", method = RequestMethod.POST)
		public ResponseEntity getSocietyList(@RequestBody RequestWrapper requestWrapper) {
			List societyList=new ArrayList();
			APIResponseVO apiResponse=new APIResponseVO();
			SocietyVO societyVO=new SocietyVO();
			
				logger.debug("Entry : public @ResponseBody List getSocietyList(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				if(requestWrapper.getUserID()>0){
				societyList=societyService.getSocietyList(requestWrapper.getUserID());
				if(societyList.size()>0){
					societyVO.setStatusCode(1);
					societyVO.setObjectList(societyList);
				}else
					societyVO.setStatusCode(0);
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getSocietyList "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody List getSocietyList(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(societyVO, HttpStatus.OK);
	 
		}
		
		 @RequestMapping(value="/get/bankList", method = RequestMethod.POST)
			public ResponseEntity getBankList(@RequestBody RequestWrapper requestWrapper) {
				APIResponseVO apiResponse=new APIResponseVO();
				MemberVO memberVO=new MemberVO();
				List bankList=null;
				
			logger.debug("Entry : public ResponseEntity getBankList(@RequestBody RequestWrapper requestWrapper)")	;
				try {
						
					  bankList=reportService.getBankList();
						
					  if(bankList.size()>0){
						  memberVO.setStatusCode(1);
						  memberVO.setObjectList(bankList);
					  }else{
						  memberVO.setStatusCode(0);
				       }
					  
				} catch (Exception e) {
					logger.error("Exception in getBankList "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity getBankList(@RequestBody RequestWrapper requestWrapper)")	;						
				return new ResponseEntity(memberVO, HttpStatus.OK);
		 
			}
		 
		 @RequestMapping(value="/get/documentList", method = RequestMethod.POST)
			public ResponseEntity getDocumentList(@RequestBody RptDocumentVO requestVO) {
				APIResponseVO apiResponse=new APIResponseVO();				
				List documentList=null;
				RptDocumentVO documentVO=new RptDocumentVO();
					logger.debug("Entry : public ResponseEntity getDocumentList(@RequestBody RptDocumentVO requestVO)")	;
				try {
							
						documentList=reportService.getDocumentList(requestVO.getType());
					  if(documentList.size()>0){
						  documentVO.setStatusCode(1);						 
						  documentVO.setObjectList(documentList);
					  }else
						  documentVO.setStatusCode(0);
					
					
				} catch (Exception e) {
					logger.error("Exception in getDocumentRegister "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity getDocumentList(@RequestBody RptDocumentVO requestVO)")	;						
				return new ResponseEntity(documentVO, HttpStatus.OK);
		 
			}
		 
		 @RequestMapping(value="/get/statusList", method = RequestMethod.POST)
			public ResponseEntity getDocumentStatusList(@RequestBody RptDocumentVO requestVO) {
				APIResponseVO apiResponse=new APIResponseVO();
				RptDocumentVO statusVO=new RptDocumentVO();
				List documentList=null;
				
					logger.debug("Entry : public ResponseEntity getDocumentStatusList(RptDocumentVO requestVO)")	;
				try {
							
						documentList=reportService.getDocumentStatusList(requestVO.getDocumentID());
						
					  if(documentList.size()>0){
						  statusVO.setStatusCode(1);
						  statusVO.setDocumentID(requestVO.getDocumentID());
						   statusVO.setObjectList(documentList);
					  }else
						  statusVO.setStatusCode(0);
					
					
				} catch (Exception e) {
					logger.error("Exception in getDocumentStatusList "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity getDocumentList(@RequestBody RptDocumentVO requestVO)")	;						
				return new ResponseEntity(statusVO, HttpStatus.OK);
		 
			}
			
	/*	@RequestMapping(value="/societystats", method = RequestMethod.POST)
		public @ResponseBody DashBoardVO getSocietyStats(@RequestBody RequestWrapper requestWrapper) {
			DashBoardVO dbVO=new DashBoardVO();
				logger.debug("Entry : public @ResponseBody DashBoardVO getSocietyStats(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				logger.info("Society Stats: Society ID: "+societyID+" Version: "+version);				
				
				dbVO =  dashBoardService.getSocietyDashBoard(societyID, version);		
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block				
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody DashBoardVO getSocietyStats(@RequestBody RequestWrapper requestWrapper)")	;						
			return dbVO;
	 
		}
		*/
	

	}

