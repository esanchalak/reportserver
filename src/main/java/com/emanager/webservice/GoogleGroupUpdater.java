package com.emanager.webservice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;

import org.apache.log4j.Logger;
//import org.codehaus.jackson.map.introspect.MemberKey;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.admin.directory.Directory;
import com.google.api.services.admin.directory.DirectoryScopes;
import com.google.api.services.admin.directory.model.Group;
import com.google.api.services.admin.directory.model.Member;


public class GoogleGroupUpdater {

	
	private static final Logger logger = Logger.getLogger(GoogleGroupUpdater.class);	

  /** E-mail address of the service account. */
  private static final String SERVICE_ACCOUNT_EMAIL = "209489857832-3tk2sqg1l8cva3sh2qvb6esv5esh6rp4@developer.gserviceaccount.com";

  /** Path to the Service Account's Private Key file */
  private static String SERVICE_ACCOUNT_PKCS12_FILE_PATH = "";
  
  String propertiesFilePath;
  static Properties pro=new Properties();
  /** Account user of the service account. */
  private static final String ACCOUNT_USER="niranjan_arude@esanchalak.com";
  
  /** Application name of the service account. */
  private static final String APPLICATION_NAME="EsanchalakGroups";
  
  /** Global configuration of Google Group OAuth 2.0 scope. */
  private static final String GROUP_SCOPE = DirectoryScopes.ADMIN_DIRECTORY_GROUP.toString().toLowerCase();
  
  /** Global instance of the HTTP transport. */
  private static HttpTransport httpTransport;
  
  /** Global instance of the JSON factory. */
  private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
  
  public static Directory admin;
  
  public void loadRestartParam()
  {
  	
  	
  	try
  	{
  	logger.debug("Entry: public void loadRestartParam ");
  	SERVICE_ACCOUNT_PKCS12_FILE_PATH=propertiesFilePath;
  	logger.debug("Exit: public void loadRestartParam ");
  	
  	}
  	catch(Exception ex)
  	{
  	logger.error("could not find key.p12 file.check file path");
  		
  	}
  
  }
   
    public void googleCredential() throws GeneralSecurityException, IOException{
     // try {
    	logger.debug("Entry :  public void googleCredential()");
    	    	
       try {
		httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		    
		    // Build a service account credential.
		    GoogleCredential credential = new GoogleCredential.Builder().setTransport(httpTransport)
		        .setJsonFactory(JSON_FACTORY)
		        .setServiceAccountId(SERVICE_ACCOUNT_EMAIL)
		        .setServiceAccountScopes(Collections.singleton(GROUP_SCOPE))
		        .setServiceAccountPrivateKeyFromP12File(new File(SERVICE_ACCOUNT_PKCS12_FILE_PATH))
		        .setServiceAccountUser(ACCOUNT_USER)
		        .build();
		    
		    
		      admin = new Directory.Builder(httpTransport, JSON_FACTORY, null)
		        .setApplicationName(APPLICATION_NAME)
		        .setHttpRequestInitializer(credential).build();
	} catch (Exception e) {
		logger.info("Exception occurred in public void googleCredential()"+e);
	}
		logger.debug("Exit :  public void googleCredential()");
    
    
  

  }
  
//Create group method
  public Group createGroup(String groupName, String email,String description) throws IOException{
	  logger.info("-------->"+email+" --->"+groupName+"  ------> "+description);
	  Group result = null;
	try {
		Group group = new Group();
		  		group.setEmail(groupName);
		  		group.setName(email);
		  		group.setDescription(description);
		  result = admin.groups().insert(group).execute();
		  logger.info("Create Group --> "+result);
	} catch (RuntimeException e) {
		logger.error("Exception while creating group "+e);
	}   
      
      
     
	  	  
	return result;
  	
  	
  }

// Update group 
public Group updateGroup(String groupKeyEmailID,String groupName, String email) throws IOException, GeneralSecurityException{
	  
	  Group group = new Group();
      		group.setEmail(groupName);
      		group.setName(email);
      		
      		googleCredential();
      
      Group result= admin.groups().update(groupKeyEmailID, group).execute();
      
      logger.info("Update Group --> "+result);
   
	  	  
	return result;
  	
  	
  }

//Delete group 
public void deleteGroup(String groupName) throws IOException, GeneralSecurityException{
	googleCredential();
	
   admin.groups().delete(groupName).execute(); 
	  	  
	
 }


//Insert member 
public  int createMember(String groupKeyEmailID, String memberEmail, String role) throws IOException, GeneralSecurityException{
	logger.debug("Entry : public  Member createMember(String groupKey, String memberEmail, String role)");
	int i=0;
	try{
	Member memberVO =  new Member();
	 	   memberVO.setEmail(memberEmail);
	 	   memberVO.setRole(role.toUpperCase());
	 
	 	googleCredential();
	 	logger.info("Insert member Email -->"+memberEmail+" into Group Name "+groupKeyEmailID);
	   memberVO = admin.members().insert(groupKeyEmailID, memberVO).execute();
	   i=1;
	  
	}catch (Exception e) {
		logger.info("Exception while creating member in "+groupKeyEmailID+" group "+e);
		i=0;
	}	
	  
	  logger.debug("Exit : public  Member createMember(String groupKey, String memberEmail, String role)");  
	return i;
	
}

//Update member 
public Member updateMember(String groupKeyEmailID, String memberEmail, String role) throws IOException, GeneralSecurityException{
	
	Member memberVO =  new Member();
       	   memberVO.setRole(role.toUpperCase());
       	   
       	googleCredential();
       
	memberVO = admin.members().update(groupKeyEmailID, memberEmail, memberVO).execute();
		
	logger.debug("Update member in group --> "+memberVO);
	
    	  	  
	return memberVO;	
	
}

//Delete member
public int deleteMember(String groupKey, String memberEmail) {
	logger.debug("Entry : public  void deleteMember(String groupKey, String memberKey)");
	int i=0;	
	try{
	
	googleCredential();
	 logger.info("Delete member Email -->"+memberEmail+" from Group Name "+groupKey);  
	  admin.members().delete(groupKey, memberEmail).execute();
	 
	   i=1;
	}catch (Exception e) {
		logger.info("Exception in delete Member "+e);
		i=0;
	}
	  logger.debug("Exit : public void deleteMember(String groupKey, String memberEmail)") ;
		return i;
}

//Insert member in group by CSV file
public ArrayList<String> insertCSVFileMember(String groupKey, String filePath) throws IOException, GeneralSecurityException{
	
	googleCredential();
	  
	  String csvFileToRead = filePath;  
      BufferedReader br = null;  
      String line="";     
      
      ArrayList<String> mylist = new ArrayList<String>();
     
      br = new BufferedReader(new FileReader(csvFileToRead)); 
      
      int i=1;           
      line =  br.readLine();
      
    while (line != null) {  
     
    	try{  
       
         Member member1 =  new Member();
          member1.setEmail(line);
          member1.setRole("MEMBER");
         
         member1 = admin.members().insert(groupKey, member1).execute();
        
         line = br.readLine();
         i++;
       
     }catch (Exception e) {
    	 logger.info("insertCSVFileMember Exception -->"+e+" Emails--"+line);
      
         line = br.readLine();
         mylist.add(line);
         
       }
       
      }  
    

	  	  
	return mylist;
		
}

public String getPropertiesFilePath() {
	return propertiesFilePath;
}

public void setPropertiesFilePath(String propertiesFilePath) {
	this.propertiesFilePath = propertiesFilePath;
}





}

//[END all]


