package com.emanager.webservice;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.commonUtils.services.AddressService;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.ApartmentVO;
import com.emanager.server.society.valueObject.InsertMemberVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.RenterVO;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.taskAndEventManagement.Service.NotificationService;
import com.emanager.server.taskAndEventManagement.valueObject.SMSGatewayVO;
import com.emanager.server.userManagement.service.AppDetailsService;
import com.emanager.server.userManagement.service.UserService;
import com.emanager.server.userManagement.valueObject.AppVO;
import com.emanager.server.userManagement.valueObject.UserVO;

@Controller
@RequestMapping("/secure/onboard")
public class JSONOrgOnboardingController {
	
	private static final Logger logger = Logger.getLogger(JSONOrgOnboardingController.class);
	@Autowired
	MemberService memberServiceBean;
	
	@Autowired
	UserService userService;
	
	@Autowired
	SocietyService societyService;
	
	@Autowired
	NotificationService notificationService;
	
	@Autowired
	AddressService addressService;
	
	@Autowired
	AppDetailsService appDetailsService;
	
	public final String HEADER_SECURITY_TOKEN = "X-AuthToken";    	 
	public EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
	public ConfigManager configManager =new ConfigManager();	

	
	@RequestMapping(value="/insert/organization",  method = RequestMethod.POST  )			
	public ResponseEntity insertOrganization(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody SocietyVO societyVO) {
		int success=0;
		APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public @ResponseBody int insertOrganization(SocietyVO societyVO)")	;
		try {
		
			if(societyVO.getSocietyName().length()>0){
				
		    success=societyService.insertSociety(societyVO);
		    if(success==2){
				apiResponse.setStatusCode(534);
				apiResponse.setMessage(societyVO.getSocietyName()+" Organization already present.");
				
				return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
				
			}else if(success>2){
				apiResponse.setStatusCode(200);
				apiResponse.setMessage(" Organization has been added successfully.");
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage("Unable to add organization details.");
				
				return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
			}
		    logger.info("Result Insert Organization : "+success);		
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in insertOrganization "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody int insertOrganization(SocietyVO societyVO)")	;						
		return new ResponseEntity(apiResponse, HttpStatus.OK);
 
	}
	
	@RequestMapping(value="/insert/organizationAddress",  method = RequestMethod.POST  )			
	public ResponseEntity insertOrganizationAddress(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody SocietyVO societyVO) {
		int success=0;
		APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public @ResponseBody int insertOrganizationAddress(SocietyVO societyVO)")	;
		try {
		
			if(societyVO.getSocietyID()>0){
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				int orgID=Integer.parseInt(decrptVO.getOrgID());	
				
		    success=addressService.addUpdateAddressDetails(societyVO.getSocietyAddress(), societyVO.getSocietyID());
		    
		    if(success>0){
				apiResponse.setStatusCode(200);
				apiResponse.setMessage(" Organization address has been added successfully.");
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage("Unable to add organization address details.");
				
				return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
			}
		    logger.info("Result Insert Organization : "+success);		
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in insertOrganization "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody int insertOrganization(SocietyVO societyVO)")	;						
		return new ResponseEntity(apiResponse, HttpStatus.OK);
 
	}
	
	@RequestMapping(value="/get/organization",  method = RequestMethod.POST  )			
	public ResponseEntity getOrganizationDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody SocietyVO societyVO) {
		int success=0;
		APIResponseVO apiResponse=new APIResponseVO();
        SocietyVO orgDetails =new SocietyVO();
			logger.debug("Entry : public ResponseEntity int getOrganizationDetails(SocietyVO societyVO)")	;
		try {
		
			if(societyVO.getSocietyID()>0){

				
				orgDetails=societyService.getSocietyDetails(societyVO.getSocietyID());
		    
		    
	    logger.info("Result Get Organization : "+orgDetails.getSocietyID());		
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in getOrganizationDetails "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity getOrganizationDetails(SocietyVO societyVO)")	;						
		return new ResponseEntity(orgDetails, HttpStatus.OK);
 
	}
	
	@RequestMapping(value="/get/orgConfiguration",  method = RequestMethod.POST  )			
	public ResponseEntity getOrganizationConfigurationDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody SocietyVO societyVO) {
		int success=0;
		APIResponseVO apiResponse=new APIResponseVO();
        SocietyVO orgDetails =new SocietyVO();
			logger.debug("Entry : public ResponseEntity int getOrganizationDetails(SocietyVO societyVO)")	;
		try {
		
			if(societyVO.getSocietyID()>0){

				
				orgDetails=societyService.getSocietyConfigurationDetails(societyVO.getSocietyID());
		    
		    
	    logger.info("Result Get Organization : "+orgDetails.getSocietyID());		
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in getOrganizationDetails "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity getOrganizationDetails(SocietyVO societyVO)")	;						
		return new ResponseEntity(orgDetails, HttpStatus.OK);
 
	}
	
	@RequestMapping(value="/insert/buildings",  method = RequestMethod.POST  )			
	public ResponseEntity insertBuildingNames(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody List <InsertMemberVO>listOfBuilding) {
		int success=0;
		APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public @ResponseBody int insertBuildingNames(SocietyVO societyVO)")	;
		try {
		
			if(listOfBuilding.size()>0){
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				int orgID=Integer.parseInt(decrptVO.getOrgID());
		    success=societyService.insertBuildingNames(listOfBuilding, orgID);
		    if(success>0){
				apiResponse.setStatusCode(200);
				apiResponse.setMessage("Buildings have been added successfully.");
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage("Unable to add given building details.");
				
				return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
			}
		    logger.info("Result of Insert buildings : "+success);		
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in insertBuildingNames "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody int insertBuildingNames(List listOfBuildings)")	;						
		return new ResponseEntity(apiResponse, HttpStatus.OK);
 
	}
	
	//------------ Get Apartment list -----------------------//
	 @RequestMapping(value="/get/apartmentList", method = RequestMethod.POST)
		public ResponseEntity searchApartmentsList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InsertMemberVO memberVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			List apartmentList=null;
			logger.debug("Entry : public @ResponseBody List searchApartmentsList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InsertMemberVO memberVO)")	;
			try {
				if(memberVO.getSocietyID().length()>0){			
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					memberVO.setSocietyID(decrptVO.getOrgID());
					
					apartmentList=societyService.searchApartmentsList(memberVO.getBuildingID(),Integer.parseInt(memberVO.getSocietyID()));
					
					
				  if(apartmentList.size()>0){
					  memberVO.setStatusCode(200);
					  memberVO.setSocietyID(decrptVO.getOrgID());
					  memberVO.setObjectList(apartmentList);
					  
				  }else
					  memberVO.setStatusCode(400);
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in searchApartmentsList "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody List searchApartmentsList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InsertMemberVO memberVO)")	;						
			return new ResponseEntity(memberVO, HttpStatus.OK);
	 
		}
	 
	 
	//------------ Get Building list -----------------------//
		 @RequestMapping(value="/get/buildingList", method = RequestMethod.POST)
			public ResponseEntity getBuildingList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InsertMemberVO memberVO) {
				APIResponseVO apiResponse=new APIResponseVO();
				List buildingList=null;
				logger.debug("Entry : public @ResponseBody List getBuildingList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InsertMemberVO memberVO)")	;
				try {
					if(Integer.getInteger(memberVO.getSocietyID())>0){			
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						memberVO.setSocietyID(decrptVO.getOrgID());
						
						buildingList=memberServiceBean.getBuildingList(Integer.parseInt(memberVO.getSocietyID()),"");
						
						
					  if(buildingList.size()>0){
						  memberVO.setStatusCode(200);
						  memberVO.setSocietyID(decrptVO.getOrgID());
						  memberVO.setObjectList(buildingList);
						  
					  }else
						  memberVO.setStatusCode(400);
					
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					logger.error("Exception in getBuildingList "+e);
					apiResponse.setStatusCode(404);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public @ResponseBody List getBuildingList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InsertMemberVO memberVO)")	;						
				return new ResponseEntity(memberVO, HttpStatus.OK);
		 
			}
	
	@RequestMapping(value="/import/memberData",  method = RequestMethod.POST  )			
	public ResponseEntity importMemberData(@RequestBody SocietyVO societyVO) {
		int success=0;
		APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public @ResponseBody int importMemberData(SocietyVO societyVO)")	;
		try {
		
			if(societyVO.getSocietyID()>0){
			//	UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
		//		int orgID=Integer.parseInt(decrptVO.getOrgID());
				int orgID=societyVO.getSocietyID();
		    success=societyService.importMemberData(orgID);
		    if(success>0){
				apiResponse.setStatusCode(200);
				apiResponse.setMessage("Member data have been added successfully.");
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage("Unable to add given member details.");
				
				return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
			}
		    logger.info("Result of Insert buildings : "+success);		
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in importMemberData "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody int importMemberData(List listOfBuildings)")	;						
		return new ResponseEntity(apiResponse, HttpStatus.OK);
 
	}
	
	@RequestMapping(value="/delete/building", method = RequestMethod.POST	)
	public ResponseEntity deleteBuilding(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InsertMemberVO memberVO) {
		int success=0;
		APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public @ResponseBody int deleteBuilding(@RequestBody int buildingID)")	;
		try {
			if(memberVO.getBuildingID().length()>0){
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				int orgID=Integer.parseInt(decrptVO.getOrgID());
			success =  societyService.deleteBuilding(Integer.parseInt(memberVO.getBuildingID()));
			
			if(success>0){
				apiResponse.setStatusCode(200);
				apiResponse.setMessage("Building has been deleted successfully.");
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage("Unable to delete selected building.");
				
				return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
			}
			logger.info("Result of deleting building : "+success);
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in deleteBuilding "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
								
		
		logger.debug("Exit : public @ResponseBody List deleteBuilding(@RequestBody int buidlingID)")	;						
		return new ResponseEntity(apiResponse, HttpStatus.OK);
 
	}
	
	@RequestMapping(value="/insert/apartments",  method = RequestMethod.POST  )			
	public ResponseEntity insertApartmentNames(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InsertMemberVO memberVO) {
		int success=0;
		APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public @ResponseBody int insertApartmentNames(SocietyVO societyVO)")	;
		try {
		
			if(memberVO.getListOfApartments().size()>0){
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				int orgID=Integer.parseInt(decrptVO.getOrgID());
			 success=societyService.insertApartmentNames(memberVO.getListOfApartments(), Integer.parseInt(memberVO.getBuildingID()));
		    
			 if(success>0){
				apiResponse.setStatusCode(200);
				apiResponse.setMessage("Apartments have been added successfully.");
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage("Unable to add given apartments details.");
				
				return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
			}
		    logger.info("Result of Insert apartments : "+success);		
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in insertApartmentNames "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody int insertApartmentNames(List listOfBuildings)")	;						
		return new ResponseEntity(apiResponse, HttpStatus.OK);
 
	}
	
	 @RequestMapping(value="/update/apartment",  method = RequestMethod.POST  )			
		public ResponseEntity updateApartmentDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ApartmentVO aptVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public ResponseEntity updateApartmentDetails(@RequestBody ApartmentVO aptVO) ")	;
			try {
				logger.info("Update apartment method APT ID: "+aptVO.getAptID());
				if(aptVO.getAptID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					aptVO.setSocietyID(decrptVO.getOrgID());	
			      success=societyService.updateApartmentDetails(aptVO,aptVO.getAptID());
			    if(success>0){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Apartment details has been updated successfully.");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to update apartment details.");					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				}
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in updateApartment "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity updateApartmentDetails(@RequestBody ApartmentVO aptVO) ")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
	
	@RequestMapping(value="/delete/apartment", method = RequestMethod.POST	)
	public ResponseEntity deleteApartment(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InsertMemberVO memberVO) {
		int success=0;
		APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public @ResponseBody int deleteApartment(@RequestBody int buildingID)")	;
		try {
			if(memberVO.getAptId()>0){
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				memberVO.setSocietyID(decrptVO.getOrgID());	
			success =  societyService.deleteApartment(memberVO.getAptId());
			
			if(success>0){
				apiResponse.setStatusCode(200);
				apiResponse.setMessage("Apartment has been deleted successfully.");
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage("Unable to delete selected apartment.");
				
				return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
			}
			logger.info("Result of deleting apartment : "+success);
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in deleteApartment "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
								
		
		logger.debug("Exit : public @ResponseBody List deleteApartment(@RequestBody int apartmentID)")	;						
		return new ResponseEntity(apiResponse, HttpStatus.OK);
 
	}
	
	@RequestMapping(value="/get/unitTypeList", method = RequestMethod.POST)
	public ResponseEntity getUnitTypeList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InsertMemberVO memberVO) {
		APIResponseVO apiResponse=new APIResponseVO();
		List unitList=null;
		logger.debug("Entry : public @ResponseBody List getUnitTypeList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InsertMemberVO memberVO)")	;
		try {
			if(memberVO.getSocietyID().length()>0){			
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				memberVO.setSocietyID(decrptVO.getOrgID());
				
				unitList=societyService.getUnitList();
				
				
			  if(unitList.size()>0){
				  memberVO.setStatusCode(200);
				  memberVO.setSocietyID(decrptVO.getOrgID());
				  memberVO.setObjectList(unitList);
				  
			  }else
				  memberVO.setStatusCode(400);
			
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in getUnitTypeList "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody List getUnitTypeList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InsertMemberVO memberVO)")	;						
		return new ResponseEntity(memberVO, HttpStatus.OK);
 
	}
	
	@RequestMapping(value="/insert/member",  method = RequestMethod.POST  )			
	public ResponseEntity insertNewMember(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody MemberVO memberVO) {
		int success=0;
		APIResponseVO apiResponse=new APIResponseVO();
		AccountHeadVO memberLedgerVO=new AccountHeadVO();
			logger.debug("Entry : public @ResponseBody int insertNewMember(MemberVO memberVO,AccountHeadVO memberLedgerVO)")	;
		try {
		
			if(memberVO.getFullName().length()>0){
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				int orgID=Integer.parseInt(decrptVO.getOrgID());
										
		       success=societyService.insertNewMember(memberVO, memberVO.getAptID());

		       if(success==2){
					apiResponse.setStatusCode(534);
					apiResponse.setMessage("Primary Member is already exists.");
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);			
		       }else  if(success>2){
				apiResponse.setStatusCode(200);
				apiResponse.setMessage("Member has been added successfully.");
		       }else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage("Unable to add given member details.");
				
				return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
			}
		    logger.info("Result of Insert member : "+success);		
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in insertNewMember "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public @ResponseBody int insertNewMember(MemberVO memberVO,AccountHeadVO memberLedgerVO)")	;						
		return new ResponseEntity(apiResponse, HttpStatus.OK);
 
	}
	
	 @RequestMapping(value="/update/member",  method = RequestMethod.POST  )			
		public ResponseEntity updateMember(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody MemberVO memberVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public ResponseEntity updateMember(@RequestBody MemberVO memberVO) ")	;
			try {
				logger.info("Update member method Member ID: "+memberVO.getMemberID());
				if(memberVO.getMemberID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					memberVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));	
			      success=societyService.updateMember(memberVO, memberVO.getAptID());
			    if(success>0){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Member details has been updated successfully.");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to update member details.");					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				}
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in updateMember "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity updateMember(@RequestBody MemberVO memberVO) ")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
	 
	 @RequestMapping(value="/transfer/member",  method = RequestMethod.POST  )	
	 public ResponseEntity transferMember(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InsertMemberVO memberVO) {
	 int success=0;
	 APIResponseVO apiResponse=new APIResponseVO();
	 logger.debug("Entry : public ResponseEntity transferMember(@RequestBody InsertMemberVO memberVO) ")	;
	 try {
	 logger.info("Transfer member method Member ID: "+memberVO.getAptId());
	 if(memberVO.getAptId()>0){
		 UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
		 memberVO.setSocietyID(decrptVO.getOrgID());	
		      success=memberServiceBean.transferMember(memberVO, memberVO.getAptId());
		    if(success>0){
		 apiResponse.setStatusCode(200);
		 apiResponse.setMessage("Member details has been transferred successfully.");
		 }else{
		 apiResponse.setStatusCode(400);
		 apiResponse.setMessage("Unable to transfer member details.");	
		 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);	
	 }

	 }else{
	 apiResponse.setStatusCode(400);
	 apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
	 return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
	 }
	 } catch (Exception e) {
	 logger.error("Exception in transferMember "+e);
	 apiResponse.setStatusCode(400);
	 apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
	 return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
	 }
	 logger.debug("Exit : public ResponseEntity transferMember(@RequestBody InsertMemberVO memberVO) ")	;	
	 return new ResponseEntity(apiResponse, HttpStatus.OK);

	 }

	 
	 //============SMS Credit related APIs ======================//
	 //-----------------Get SMS counts for an organization-----------------//
	 @RequestMapping(value="/sms/smsGatewayDetails",  method = RequestMethod.POST  )			
		public ResponseEntity getSMSGatewayDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody SMSGatewayVO requestVO) {
			SMSGatewayVO smsVO=new SMSGatewayVO();
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public ResponseEntity getSMSGatewayDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestVO) ")	;
			try {
					if(requestVO.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
					smsVO=notificationService.getSMSGatewayDetails(requestVO.getOrgID());
					smsVO.setSmsPassword(null);
			    if(smsVO!=null){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("SMS details has been received successfully.");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to get  SMS gateway details.");					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				}
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in public ResponseEntity getSMSGatewayDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestVO) "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public public ResponseEntity getSMSGatewayDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestVO) ")	;						
			return new ResponseEntity(smsVO, HttpStatus.OK);
	 
		}
	 
	 //-----------------Add SMS gateway for an organization-----------------//
	 @RequestMapping(value="/sms/addSMSCredits",  method = RequestMethod.POST  )			
		public ResponseEntity addSMSCredits(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody SMSGatewayVO smsVO) {
				APIResponseVO apiResponse=new APIResponseVO();
				int success=0;
				logger.debug("Entry : public ResponseEntity addSMSCredits(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestVO) ")	;
			try {
					if(smsVO.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					smsVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
					success=notificationService.addSMSCredit(smsVO);
			   if(smsVO!=null){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("SMS  details has been updated successfully.");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to update  SMS gateway details.");					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				}
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in public ResponseEntity getSMSGatewayDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestVO) "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public public ResponseEntity getSMSGatewayDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestVO) ")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
	 
	 
	//--------------------- API to update license of organizations -----------------------------//
		@RequestMapping(value="/updateSubscription", method = RequestMethod.POST)
				public ResponseEntity updateOrgAppSubscription(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody AppVO appVO) {
					APIResponseVO apiResponse=new APIResponseVO();
					JSONResponseVO jsonResponse=new JSONResponseVO();
				    int success=0;
						logger.debug("Entry : public ResponseEntity updateOrgAppSubscription(@RequestBody AppVO appVO)" );
					try {
						
						if(appVO.getAppID().length()>0){
							
						success=appDetailsService.updateSubscriptionOfOrganziationApp(appVO);
						if(success==1){
							apiResponse.setStatusCode(200);
							apiResponse.setMessage("App subscription has been updated successfully.");
							
						}
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
						  
						
					} catch (Exception e) {
						logger.error("Exception in public ResponseEntity updateOrgAppSubscription(@RequestBody EventVO eventVO) "+e);
						apiResponse.setStatusCode(404);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
					}
					logger.debug("Exit : public ResponseEntity restrictSubscriptionExpiredOrgs(@RequestBody AppVO appVO)")	;						
					return new ResponseEntity(apiResponse, HttpStatus.OK);
			 
				}
		
	 
	 

}
