package com.emanager.webservice;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.adminReports.Services.PrintReportService;
import com.emanager.server.adminReports.valueObject.NominationRegisterVO;
import com.emanager.server.adminReports.valueObject.PrintReportVO;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.userManagement.valueObject.UserVO;

@Controller
@RequestMapping("/assets")
public class JSONAssetsController {
	
	private static final Logger logger = Logger.getLogger(JSONAssetsController.class);
	@Autowired
	LedgerService ledgerService;
  
	public ConfigManager configManager =new ConfigManager();
   
    
    
	/*------------------Delete customer API------------------*/
	@RequestMapping(value="/delete/ledgerProfile",  method = RequestMethod.POST  )			
	public ResponseEntity deleteLedgerProfile(@RequestBody AccountHeadVO accountHeadVO) {
		int success=0;
		APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity deleteLedgerProfile(@RequestBody AccountHeadVO accountHeadVO)")	;
		try {
			logger.info("Here org ID "+accountHeadVO.getOrgID());
			if(accountHeadVO.getOrgID()>0){
				
				
				success=ledgerService.deleteLedgerProfile(accountHeadVO.getLedgerID(), accountHeadVO.getOrgID());
			
			if(success==1){
				apiResponse.setStatusCode(200);
				apiResponse.setMessage("Profile deleted successfully.");
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage("Unable to deleted profile.");
				return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
			}
			
			
		}else{
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
			

			logger.info("Result of public ResponseEntity deleteLedgerProfile(@RequestBody AccountHeadVO accountHeadVO): "+success);				
			
		} catch (Exception e) {
			logger.error("Exception  "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity deleteLedgerProfile(@RequestBody AccountHeadVO accountHeadVO) ")	;						
		return new ResponseEntity(apiResponse, HttpStatus.OK);
 
	}
	
	/*------------------Delete ledger API------------------*/
	@RequestMapping(value="/delete/ledger",  method = RequestMethod.POST  )			
	public ResponseEntity deleteLedger(@RequestBody AccountHeadVO ledgerVO) {
		int success=0;
		APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity deleteLedger(@RequestBody AccountHeadVO ledgerVO)")	;
		try {
			logger.info("Here org ID "+ledgerVO.getOrgID());
			if((ledgerVO.getOrgID()>0)||(ledgerVO.getLedgerID()>0)){
								
				ledgerVO=ledgerService.deleteLedger(ledgerVO.getOrgID(), ledgerVO.getLedgerID());
			
			if(ledgerVO.getStatusCode()==200){
				apiResponse.setStatusCode(ledgerVO.getStatusCode());
				apiResponse.setMessage(ledgerVO.getMessage());
			}else{
				apiResponse.setStatusCode(ledgerVO.getStatusCode());
				apiResponse.setMessage(ledgerVO.getMessage());
				return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
			}
			
			
		}else{
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
			

			logger.info("Result of public ResponseEntity deleteLedger : "+success);				
			
		} catch (Exception e) {
			logger.error("Exception  "+e);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity deleteLedger(@RequestBody AccountHeadVO ledgerVO) ")	;						
		return new ResponseEntity(apiResponse, HttpStatus.OK);
 
	}

}
