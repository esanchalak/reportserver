package com.emanager.webservice;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emanager.server.adminReports.Services.PrintReportService;
import com.emanager.server.adminReports.valueObject.NominationRegisterVO;
import com.emanager.server.adminReports.valueObject.PrintReportVO;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.society.services.MemberService;

@Controller
@RequestMapping("/download")
public class JSONDownloadController {
	
	private static final Logger logger = Logger.getLogger(JSONDownloadController.class);
	@Autowired
	MemberService memberServiceBean;
  
    @Autowired
    PrintReportService printReportService;
    
    @Autowired
    ReportService reportService;
    
    
    /*Sample body contents for PrintReportVO */
	/*{
		"printParam":{
		"orgID":"11",		
		"aptID":"5"
		},
		"reportContentType":"pdf"
		
		}*/
	@RequestMapping(value="/nominationForm", method = RequestMethod.POST)
	public void downloadNominationForm(HttpServletResponse response,@RequestBody PrintReportVO printVO) {
		NominationRegisterVO printTxVO=new NominationRegisterVO();
		APIResponseVO apiResponse=new APIResponseVO();
		HashMap<String, Object> param= printVO.getPrintParam();
		logger.debug("Entry : public void downloadNominationForm(HttpServletResponse response,@RequestBody PrintReportVO printVO) ");
		try {
			
				printTxVO.setPrintParam(param);
			    										
				printTxVO.setSourceType("BEAN");
				printTxVO.setReportContentType(printVO.getReportContentType());														
							
				printVO=printReportService.printNomination(printTxVO);
				printVO.setBeanList(null);			
				
				FileInputStream myStream = new FileInputStream(printVO.getReportURL());

				// Set the content type and attachment header.
				response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
				response.addHeader("x-filename",printVO.getReportID());
				response.setContentType("application/pdf");
				
				// Copy the stream to the response's output stream.
				IOUtils.copy(myStream, response.getOutputStream());				

				response.flushBuffer();
			
		} catch (IOException e) {
			logger.error("Exception in downloadNominationForm: "+e);
			
		}
		logger.debug("Exit : public void downloadNominationForm(HttpServletResponse response,@RequestBody PrintReportVO printVO) ");						
		
 
	}

}
