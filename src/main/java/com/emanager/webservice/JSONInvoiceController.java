package com.emanager.webservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emanager.Utility.UtilityClaass;
import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.accountsAutomation.service.AccountsAutoService;
import com.emanager.server.accountsAutomation.valueObject.PackageDetailsVO;
import com.emanager.server.adminReports.Services.PrintReportService;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.dashBoard.Services.AccountDashBoardService;
import com.emanager.server.dashBoard.Services.DashBoardService;
import com.emanager.server.financialReports.services.AccountingStatusService;
import com.emanager.server.financialReports.services.BalanceSheetService;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.financialReports.services.TrialBalanceService;
import com.emanager.server.financialReports.valueObject.MonthwiseChartVO;
import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.invoice.Services.CustomerService;
import com.emanager.server.invoice.Services.InvoiceService;
import com.emanager.server.invoice.dataAccessObject.InvoiceVO;
import com.emanager.server.invoice.dataAccessObject.ItemDetailsVO;
import com.emanager.server.invoice.dataAccessObject.ProfileDetailsVO;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.userManagement.service.UserService;
import com.emanager.server.userManagement.valueObject.UserVO;
import com.emanager.server.vendorManagement.service.VendorService;


@Controller
@RequestMapping("/secure/taxInvoice")
public class JSONInvoiceController {

	private static final Logger logger = Logger.getLogger(JSONInvoiceController.class);
	
	@Autowired
    ReportService reportService;
    @Autowired
    TransactionService transactionService;
    @Autowired
    TransactionService txService;
    @Autowired
    InvoiceService invoiceService;
    @Autowired
    PrintReportService printReportService;
    @Autowired
    MemberService memberServiceBean;
    @Autowired
    DashBoardService dashBoardService;
    @Autowired
    AccountDashBoardService accountDashBoardService;
    @Autowired
    SocietyService societyService;
    @Autowired
    LedgerService ledgerService;
    @Autowired
    TrialBalanceService trialBalanceService;
    @Autowired
    BalanceSheetService balanceSheetService;
    @Autowired
    UserService userService;   
    @Autowired
    UtilityClaass utilityClass;
    @Autowired
    CustomerService customerServiceBean; 
    @Autowired
    VendorService vendorServiceBean; 
    @Autowired
    AccountsAutoService accountsAutoService;
    
    DateUtility dateUtil=new DateUtility();
    public ConfigManager configManager =new ConfigManager();
    public EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
	public final String HEADER_SECURITY_TOKEN = "X-AuthToken";  
    
    @RequestMapping(value="/get/invoice",  method = RequestMethod.POST  )			
	public ResponseEntity getInvoice(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
    	InvoiceVO invoiceVODetails =new InvoiceVO();
		APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity getInvoice(@RequestBody InvoiceVO invoiceVO)")	;
		try {
			logger.info("getInvoice : org ID: "+invoiceVO.getOrgID()+" Invoice ID: "+invoiceVO.getInvoiceID());
			if((invoiceVO.getOrgID()>0)&&(invoiceVO.getInvoiceID()>0)){
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				invoiceVODetails=invoiceService.getInvoiceDetails(invoiceVO);
			}else{
				logger.info("Unable to fetch invoice details of invoice ID : "+invoiceVO.getInvoiceID());
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
								
			} catch (Exception e) {
				logger.error("Exception in getInvoice: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
		logger.debug("Exit : public ResponseEntity getInvoice(@RequestBody InvoiceVO invoiceVO)")	;						
		return new ResponseEntity(invoiceVODetails, HttpStatus.OK);
 
	}
    
    @RequestMapping(value="/get/invoiceByInvoiceNumber",  method = RequestMethod.POST  )			
	public ResponseEntity getInvoiceByInvoiceNumber(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
    	InvoiceVO invoiceVODetails =new InvoiceVO();
		APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity getInvoiceByInvoiceNumber(@RequestBody InvoiceVO invoiceVO)")	;
		try {
			logger.info("getInvoice : org ID: "+invoiceVO.getOrgID()+" Invoice Number: "+invoiceVO.getInvoiceNumber());
			if((invoiceVO.getOrgID()>0)&&(invoiceVO.getInvoiceNumber().length()>0)){
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				invoiceVODetails=invoiceService.getInvoiceDetailsByInvoiceNumber(invoiceVO);
			}else{
				logger.info("Unable to fetch invoice details of invoice ID : "+invoiceVO.getInvoiceID());
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
								
			} catch (Exception e) {
				logger.error("Exception in getInvoiceByInvoiceNumber: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
		logger.debug("Exit : public ResponseEntity getInvoiceByInvoiceNumber(@RequestBody InvoiceVO invoiceVO)")	;						
		return new ResponseEntity(invoiceVODetails, HttpStatus.OK);
 
	}
    
     @RequestMapping(value="/insert/invoice",  method = RequestMethod.POST  )	
	 public ResponseEntity insertGSTInvoice(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
    	 InvoiceVO invoiceVODetails =new InvoiceVO();
	 APIResponseVO apiResponse=new APIResponseVO();
	 logger.debug("Entry :  public ResponseEntity insertGSTInvoice(@RequestBody InvoiceVO invoiceVO)")	;
	 try {
	 logger.info("insertGSTInvoice:  org ID: "+invoiceVO.getOrgID()+" Amount: "+invoiceVO.getInvoiceAmount());
		if(invoiceVO.getOrgID()>0 ){
			UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
			invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
			 invoiceVODetails=invoiceService.insertGSTInvoice(invoiceVO);
			
			 logger.info("Result of insertGSTInvoice insertion : "+invoiceVO.getInvoiceID());	   
			if(invoiceVODetails.getStatusCode()!=200){				
				 apiResponse.setStatusCode(invoiceVODetails.getStatusCode());
				 apiResponse.setMessage(invoiceVODetails.getStatusMessage());
				 logger.info("Unable to insert invoice details Status Code: "+apiResponse.getStatusCode());
			    return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);	
			 		
			 }else{
				 apiResponse.setStatusCode(200);
				 apiResponse.setMessage("Invoice inserted successfully ");			 
			 }
		 }else{
		    apiResponse.setStatusCode(400);
		    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
		   return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		 }
	   

	 } catch (Exception e) {
	    logger.error("Exception in insertGSTInvoice  "+e);
	    apiResponse.setStatusCode(400);
	    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
	    return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);

	 }
	 logger.debug("Exit :  public ResponseEntity insertGSTInvoice(@RequestBody InvoiceVO invoiceVO)")	;	
	 return new ResponseEntity(apiResponse, HttpStatus.OK);

	 }
     
     
     @RequestMapping(value="/update/invoice",  method = RequestMethod.POST  )			
 	public ResponseEntity updateInvoice(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {     	
     	int success=0;
 		APIResponseVO apiResponse=new APIResponseVO();
 			logger.debug("Entry : public ResponseEntity updateInvoice(@RequestBody InvoiceVO invoiceVO)")	;
 		try {
 			logger.info("updateInvoice org ID: "+invoiceVO.getOrgID()+" Invoice ID: "+invoiceVO.getInvoiceID());
 			if(invoiceVO.getOrgID()>0){
 				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
 				invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
 				invoiceVO=invoiceService.updateGSTInvoice(invoiceVO);
 				 				
 				if(invoiceVO.getStatusCode()!=200){				
 					 apiResponse.setStatusCode(invoiceVO.getStatusCode());
 					 apiResponse.setMessage(invoiceVO.getStatusMessage());
 					 logger.info("Unable to update invoiceID: "+invoiceVO.getInvoiceID()+" Status Code: "+apiResponse.getStatusCode());
 				    return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);	
 				 		
 				 }else{
 					apiResponse.setStatusCode(200);
 					 apiResponse.setMessage("Invoice updated successfully ");			
 					 
 				 }
 			}else{
 				logger.info("Unable to update invoice ID : "+invoiceVO.getInvoiceID());
 				apiResponse.setStatusCode(400);
 				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
 				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
 			}
 								
 			} catch (Exception e) {
 				logger.error("Exception in updateInvoice: "+e);
 				apiResponse.setStatusCode(400);
 				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
 				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
 			}
 			
 		logger.debug("Exit :public ResponseEntity updateInvoice(@RequestBody InvoiceVO invoiceVO)")	;						
 		return new ResponseEntity(apiResponse, HttpStatus.OK);
  
 	}
     
     @RequestMapping(value="/delete/invoice",  method = RequestMethod.POST  )			
  	public ResponseEntity deleteGSTInvoice(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
       	int success=0;
  		APIResponseVO apiResponse=new APIResponseVO();
  			logger.debug("Entry : public ResponseEntity deleteGSTInvoice(@RequestBody InvoiceVO invoiceVO) ")	;
  		try {
  			logger.info("Delete Invoice  : org ID: "+invoiceVO.getOrgID()+" Invoice ID: "+invoiceVO.getInvoiceID());
  			if(invoiceVO.getOrgID()>0){
  				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
  				invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
  				invoiceVO=invoiceService.deleteGSTInvoice(invoiceVO);
  				
  				if(invoiceVO.getStatusCode()!=200){				
					 apiResponse.setStatusCode(invoiceVO.getStatusCode());
					 apiResponse.setMessage(invoiceVO.getStatusMessage());
					 logger.info("Unable to delete invoiceID: "+invoiceVO.getInvoiceID()+" Status Code: "+apiResponse.getStatusCode());
				    return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);	
				 		
				 }else{
					apiResponse.setStatusCode(200);
					 apiResponse.setMessage("Invoice deleted successfully ");			
					 
				 }
  			}else{
  				logger.info("Unable to delete invoice details of : "+invoiceVO.getInvoiceID());
  				apiResponse.setStatusCode(400);
  				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
  				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
  			}
  								
  			} catch (Exception e) {
  				logger.error("Exception in deleteGSTInvoice: "+e);
  				apiResponse.setStatusCode(400);
  				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
  				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
  			}
  			
  		logger.debug("Exit :public ResponseEntity deleteGSTInvoice(@RequestBody InvoiceVO invoiceVO) ")	;						
  		return new ResponseEntity(apiResponse, HttpStatus.OK);
   
  	}
     
     
      @RequestMapping(value="/customerList", method = RequestMethod.POST)
		public ResponseEntity getCustomerList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JSONResponseVO jsonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List customerList=null;
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getInvoiceListForMember(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;
			try {
				
				if(requestWrapper.getOrgID()>0 ){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
				   customerList=customerServiceBean.getCustomerDetaiilsList(requestWrapper.getOrgID());
				   if(customerList.size()==0){
					   jsonVO.setStatusCode(0);
					}else{
						jsonVO.setStatusCode(1);
						jsonVO.setObjectList(customerList);
					}	
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getCustomerList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getInvoiceListForMember(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;						
			return  new ResponseEntity(jsonVO, HttpStatus.OK);
	 
		}
	 
	 @RequestMapping(value="/ledgerInvoiceList", method = RequestMethod.POST)
		public ResponseEntity getLedgerInvoiceList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
			JSONResponseVO jsonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			AccountHeadVO accountHeadVO =new AccountHeadVO();
			List invoiceList=null;
				logger.debug("Entry : public ResponseEntity getLedgerInvoiceList(@RequestBody InvoiceVO invoiceVO)")	;
			try {
				
				if(invoiceVO.getOrgID()>0 ){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
					invoiceList=invoiceService.getInvoicesForCustomerLedger(invoiceVO);
					accountHeadVO=ledgerService.getOpeningClosingBalance(invoiceVO.getOrgID(),invoiceVO.getEntityID(),invoiceVO.getFromDate(),invoiceVO.getUptoDate(),"L");
				    jsonVO.setBalance(accountHeadVO.getClosingBalance());
				    
					if(invoiceList.size()==0){
					   jsonVO.setStatusCode(0);
					}else{
						jsonVO.setStatusCode(1);
						jsonVO.setObjectList(invoiceList);
					}	
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getLedgerInvoiceList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getLedgerInvoiceList(@RequestBody InvoiceVO invoiceVO)")	;						
			return  new ResponseEntity(jsonVO, HttpStatus.OK);
	 
		}
	 
	 
	 /*----------- API to get list of ledgers --------------*/
		@RequestMapping(value="/ledgersProfileList", method = RequestMethod.POST)
		public ResponseEntity getLedgerProfileList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVo) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List ledgerList=new ArrayList();
			SocietyVO societyVo=new SocietyVO();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getLedgersList(@RequestBody RequestWrapper requestWrapper)")	;
			try {
			
				if(txVo.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					txVo.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));	
				 societyVo=societyService.getSocietyDetails(txVo.getSocietyID());
				 jSonVO.setSocietyVO(societyVo);
				
				 ledgerList=ledgerService.getLedgerProfileList(txVo.getSocietyID(),txVo.getType() );
				 jSonVO.setSocietyID(txVo.getSocietyID());
				if(ledgerList.size()>0){
				  jSonVO.setObjectList(ledgerList);
				  jSonVO.setStatusCode(1);
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}	
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getLedgersList: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getLedgersList(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		/*----------- API to get list of ledgers --------------*/
		@RequestMapping(value="/currentNonCurrentLedgersProfileList", method = RequestMethod.POST)
		public ResponseEntity getCurrentNonCurrentLedgerProfileList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVo) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List ledgerList=new ArrayList();
			SocietyVO societyVo=new SocietyVO();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getCurrentNonCurrentLedgerProfileList(@RequestBody RequestWrapper requestWrapper)")	;
			try {
			
				if(txVo.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					txVo.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));	
				 societyVo=societyService.getSocietyDetails(txVo.getSocietyID());
				 jSonVO.setSocietyVO(societyVo);
				
				 ledgerList=ledgerService.getCurrentNonCurrentLedgerProfileList(txVo.getSocietyID(),txVo.getType() );
				 jSonVO.setSocietyID(txVo.getSocietyID());
				if(ledgerList.size()>0){
				  jSonVO.setObjectList(ledgerList);
				  jSonVO.setStatusCode(1);
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}	
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getCurrentNonCurrentLedgerProfileList: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getCurrentNonCurrentLedgerProfileList(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		/*----------- API to Report group of Invoice lineItems --------------*/
		 @RequestMapping(value="/update/ReportGroupInvoiceLineItem",  method = RequestMethod.POST  )			
		 	public ResponseEntity updateReportGroupInvoiceLineItem(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {     	
		     	int success=0;
		 		APIResponseVO apiResponse=new APIResponseVO();
		 			logger.debug("Entry : public ResponseEntity updateInvoice(@RequestBody InvoiceVO invoiceVO)")	;
		 		try {
		 			logger.info("updateInvoice org ID: "+invoiceVO.getOrgID()+" Invoice LineItemID: "+invoiceVO.getInvLineItemID());
		 			if(invoiceVO.getOrgID()>0){
		 				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
		 				invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
		 				invoiceVO=invoiceService.updateReportGroupInvoiceLineItem(invoiceVO);
		 				 				
		 				if(invoiceVO.getStatusCode()!=200){				
		 					 apiResponse.setStatusCode(invoiceVO.getStatusCode());
		 					 apiResponse.setMessage(invoiceVO.getStatusMessage());
		 					 logger.info("Unable to update LineItemID: "+invoiceVO.getInvLineItemID()+" Status Code: "+apiResponse.getStatusCode());
		 				    return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);	
		 				 		
		 				 }else{
		 					apiResponse.setStatusCode(200);
		 					 apiResponse.setMessage("Report group  updated successfully ");			
		 					 
		 				 }
		 			}else{
		 				logger.info("Unable to update LineItemID : "+invoiceVO.getInvLineItemID());
		 				apiResponse.setStatusCode(400);
		 				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
		 				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		 			}
		 								
		 			} catch (Exception e) {
		 				logger.error("Exception in updateReportGroupInvoiceLineItem: "+e);
		 				apiResponse.setStatusCode(400);
		 				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
		 				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		 			}
		 			
		 		logger.debug("Exit :public ResponseEntity updateInvoice(@RequestBody InvoiceVO invoiceVO)")	;						
		 		return new ResponseEntity(apiResponse, HttpStatus.OK);
		  
		 	}
		
		/*------------------Insert customer API------------------*/
		@RequestMapping(value="/insert/ledgerProfile",  method = RequestMethod.POST  )			
		public ResponseEntity addLedgerWithProfiles(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody AccountHeadVO achVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
			AccountHeadVO ledgerVO=new AccountHeadVO();
				logger.debug("Entry : public ResponseEntity addLedgerWithProfiles(@RequestBody AccountHeadVO achVO)")	;
			try {
				logger.info("Here society ID "+achVO.getOrgID());
				if(achVO.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					achVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
					ledgerVO=ledgerService.addLedgerWithProfiles(achVO);
				
				if(ledgerVO.getStatusCode()==200){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Ledger profile added successfully.");
				}else{
					apiResponse.setStatusCode(ledgerVO.getStatusCode());
					apiResponse.setMessage(ledgerVO.getMessage());
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
				}
				
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

				logger.info("Result of public ResponseEntity addLedgerWithProfiles(@RequestBody AccountHeadVO achVO)r : "+success);				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity addLedgerWithProfiles(@RequestBody AccountHeadVO achVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		/*------------------Insert customer API------------------*/
		@RequestMapping(value="/update/ledgerProfile",  method = RequestMethod.POST  )			
		public ResponseEntity updateledgerProfile(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody AccountHeadVO achVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public ResponseEntity updateledgerProfile(@RequestBody AccountHeadVO achVO)")	;
			try {
				logger.info("Here society ID "+achVO.getOrgID());
				if(achVO.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					achVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
					success=ledgerService.updateLedgerProfileDetails(achVO);
					
				if(success>0){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Profile updated successfully.");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to updated profile.");
					return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
				}
				
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

				logger.info("Result of public ResponseEntity updateledgerProfile(@RequestBody AccountHeadVO achVO) : "+success);				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity updateledgerProfile(@RequestBody AccountHeadVO achVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		/*------------------Delete customer API------------------*/
		@RequestMapping(value="/delete/ledgerProfile",  method = RequestMethod.POST  )			
		public ResponseEntity deleteLedgerProfile(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody AccountHeadVO accountHeadVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public ResponseEntity deleteLedgerProfile(@RequestBody AccountHeadVO accountHeadVO)")	;
			try {
				logger.info("Here society ID "+accountHeadVO.getOrgID());
				if(accountHeadVO.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					accountHeadVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
					success=ledgerService.deleteLedgerProfile(accountHeadVO.getLedgerID(), accountHeadVO.getOrgID());
				
				if(success==1){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Profile deleted successfully.");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to deleted profile.");
					return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
				}
				
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

				logger.info("Result of public ResponseEntity deleteLedgerProfile(@RequestBody AccountHeadVO accountHeadVO): "+success);				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity deleteCustomer(@RequestBody CustomerVO customerVO) ")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		/*------------------Get ledger  API------------------*/
		@RequestMapping(value="/get/ledgerProfile",  method = RequestMethod.POST  )			
		public ResponseEntity getLedgerProfile(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody AccountHeadVO achVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
			ProfileDetailsVO profileVO=new ProfileDetailsVO();
				logger.debug("Entry : public ResponseEntity getLedgerProfile(@RequestBody AccountHeadVO achVO)")	;
			try {
				logger.info("Here society ID "+achVO.getOrgID());
				if(achVO.getOrgID()>0 && achVO.getLedgerID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					achVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				
				profileVO=ledgerService.getLedgerProfile(achVO.getLedgerID(), achVO.getOrgID());
				achVO.setProfileVO(profileVO);			
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(profileVO, HttpStatus.BAD_REQUEST);
			}
				

				logger.info("Result of public ResponseEntity getLedgerProfile(@RequestBody AccountHeadVO achVO) : "+success);				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getledgerProfile(@RequestBody AccountHeadVO achVO)")	;						
			return new ResponseEntity(achVO, HttpStatus.OK);
	 
		}
		
		
		// ===Parameters ==//
		// OrgID, FromDate, ToDate
		
		/*------------------Get TDS Calculation Report------------------*/
		@RequestMapping(value="/get/tdsCalculationReport",  method = RequestMethod.POST  )			
		public ResponseEntity getTDSCalculationReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
			JsonRespAccountsVO json=new JsonRespAccountsVO();
			List tdsReportList=new ArrayList();
				logger.debug("Entry : public ResponseEntity getTDSCalculationReport(@RequestBody TransactionVO achVO)")	;
			try {
				logger.info("Here society ID "+txVO.getSocietyID());
				if(txVO.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
				tdsReportList=invoiceService.getTDSCalculationReport(txVO);
				
				if(tdsReportList.size()>0){
					json.setObjectList(tdsReportList);
					json.setStatusCode(200);
				}
					
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

				logger.info("Result of public ResponseEntity getTDSCalculationReport(@RequestBody TransactionVO achVO) : "+success);				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getTDSCalculationReport(@RequestBody TransactionVO achVO)")	;						
			return new ResponseEntity(json, HttpStatus.OK);
	 
		}
		
		// ===Parameters ==//
		// OrgID, FromDate, ToDate
		
		/*------------------Get TDS Calculation Report based on invoice------------------*/
		@RequestMapping(value="/get/tdsCalculationReportOnInvoices",  method = RequestMethod.POST  )			
		public ResponseEntity getTDSCalculationRptOnInvoices(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
			JsonRespAccountsVO json=new JsonRespAccountsVO();
			List tdsReportList=new ArrayList();
				logger.debug("Entry : public ResponseEntity getTDSCalculationRptOnInvoices(@RequestBody InvoiceVO invVO)")	;
			try {
				logger.info("Here org ID "+invVO.getOrgID());
				if(invVO.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					invVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
				tdsReportList=invoiceService.getTDSCalculationRptOnInvoices(invVO);
				
				if(tdsReportList.size()>0){
					json.setObjectList(tdsReportList);
					json.setStatusCode(200);
				}
					
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

				logger.info("Result of public ResponseEntity getTDSCalculationRptOnInvoices(@RequestBody InvoiceVO invVO) : "+success);				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getTDSCalculationRptOnInvoices(@RequestBody InvoiceVO invVO)")	;						
			return new ResponseEntity(json, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/gstInvoiceList", method = RequestMethod.POST)
		public ResponseEntity getInvoicesForOrg(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List invoiceList=new ArrayList();
			
				logger.debug("Entry : public ResponseEntity getInvoicesForOrg(@RequestBody InvoiceVO invoiceVO)")	;
			try {
				
				if((invoiceVO.getOrgID()>0) && (invoiceVO.getFromDate().length()>0)  &&  (invoiceVO.getUptoDate().length()>0)){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
					invoiceList=invoiceService.getGSTInvoicesForOrg(invoiceVO);
					if(invoiceList.size()==0){
						jSonVO.setStatusCode(0);
					}else{
						jSonVO.setStatusCode(200);
						jSonVO.setObjectList(invoiceList);
					}			
				   
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getInvoicesForOrg "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getInvoicesForOrg(@RequestBody InvoiceVO invoiceVO)")	;						
			return  new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/gstSimpleInvoiceList", method = RequestMethod.POST)
		public ResponseEntity getInvoicesDetailsListForOrg(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List invoiceList=new ArrayList();
			
				logger.debug("Entry : public ResponseEntity getInvoicesDetailsListForOrg(@RequestBody InvoiceVO invoiceVO)")	;
			try {
				
				if((invoiceVO.getOrgID()>0) && (invoiceVO.getFromDate().length()>0)  &&  (invoiceVO.getUptoDate().length()>0)){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
					invoiceList=invoiceService.getQuickGSTInvoicesForOrg(invoiceVO);
					if(invoiceList.size()==0){
						jSonVO.setStatusCode(0);
					}else{
						jSonVO.setStatusCode(200);
						jSonVO.setObjectList(invoiceList);
					}			
				   
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getInvoicesDetailsListForOrg "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getInvoicesDetailsListForOrg(@RequestBody InvoiceVO invoiceVO)")	;						
			return  new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		/* Get gst invoice list like day book   */
		@RequestMapping(value="/gstInvoiceListDayBook", method = RequestMethod.POST)
		public ResponseEntity getGSTInvoicesListDayBook(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List invoiceList=new ArrayList();
			
				logger.debug("Entry : public ResponseEntity getGSTInvoicesListDayBook(@RequestBody InvoiceVO invoiceVO)")	;
			try {
				
				if((invoiceVO.getOrgID()>0) && (invoiceVO.getFromDate().length()>0)  &&  (invoiceVO.getUptoDate().length()>0)){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
					invoiceList=invoiceService.getGSTInvoicesListDayBook(invoiceVO);
					if(invoiceList.size()==0){
						jSonVO.setStatusCode(0);
					}else{
						jSonVO.setStatusCode(200);
						jSonVO.setObjectList(invoiceList);
					}			
				   
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getGSTInvoicesListDayBook "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getGSTInvoicesListDayBook(@RequestBody InvoiceVO invoiceVO)")	;						
			return  new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/gstChapterList", method = RequestMethod.POST)
		public ResponseEntity getGSTChapterList(@RequestBody ItemDetailsVO itemVO) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List chapterList=new ArrayList();
			
				logger.debug("Entry :public ResponseEntity getGSTChapterList(@RequestBody ItemDetailsVO itemVO)")	;
			try {
							
				  chapterList=invoiceService.getGSTChapterList();
					if(chapterList.size()==0){
						jSonVO.setStatusCode(0);
					}else{
						jSonVO.setStatusCode(200);
						jSonVO.setObjectList(chapterList);
					}			
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getGSTChapterList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getGSTChapterList(@RequestBody ItemDetailsVO itemVO)")	;						
			return  new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/hsnSacList", method = RequestMethod.POST)
		public ResponseEntity getHsnSacList(@RequestBody ItemDetailsVO itemVO) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List chapterList=new ArrayList();
			
				logger.debug("Entry :public ResponseEntity getHsnSacList(@RequestBody ItemDetailsVO itemVO)")	;
			try {
				
				if(itemVO.getChapterID()>0 ){		
					
				  chapterList=invoiceService.getHsnSacList(itemVO.getChapterID());
				  
					if(chapterList.size()==0){
						jSonVO.setStatusCode(0);
					}else{
						jSonVO.setStatusCode(200);
						jSonVO.setObjectList(chapterList);
					}			
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getHsnSacList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getHsnSacList(@RequestBody ItemDetailsVO itemVO)")	;						
			return  new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/productServiceList", method = RequestMethod.POST)
		public ResponseEntity getProductServiceList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ItemDetailsVO itemVO) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List chapterList=new ArrayList();
			
			logger.debug("Entry :public ResponseEntity getProductServiceList(@RequestBody ItemDetailsVO itemVO)")	;
			try {
				
				if(itemVO.getOrgID()>0 ){		
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					itemVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
					
				  chapterList=invoiceService.getProductServiceLedgerRelationList(itemVO.getOrgID());
				  
					if(chapterList.size()==0){
						jSonVO.setStatusCode(0);
					}else{
						jSonVO.setStatusCode(200);
						jSonVO.setObjectList(chapterList);
					}			
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getProductServiceList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getProductServiceList(@RequestBody ItemDetailsVO itemVO)")	;						
			return  new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		 @RequestMapping(value="/insert/productService",  method = RequestMethod.POST  )	
		 public ResponseEntity insertProductService(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ItemDetailsVO itemDetailsVO) {
			 int success=0;
		 APIResponseVO apiResponse=new APIResponseVO();
		 logger.debug("Entry :  public ResponseEntity insertProductService(@RequestBody ItemDetailsVO itemDetailsVO)")	;
		 try {
		 logger.info("Insert Product Service:  org ID: "+itemDetailsVO.getOrgID()+" ledger ID: "+itemDetailsVO.getLedgerID());
			if(itemDetailsVO.getOrgID()>0 ){
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				itemDetailsVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
				success=invoiceService.insertProductService(itemDetailsVO);
				
				 logger.info("Result of insertProductService inserted : "+success);	   
				 if(success==1){				
					 apiResponse.setStatusCode(200);
					 apiResponse.setMessage("Proudct/Service inserted successfully ");
				 }else if(success==2) {
					 apiResponse.setStatusCode(409);
					 apiResponse.setMessage("HSN/SAC code is invalid Please enter valid hsn/sac  ");					 
					 return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);	
				 }else if(success==534) {
					 apiResponse.setStatusCode(534);
					 apiResponse.setMessage("Proudct/Service already present.");					 
					 return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);	
				 } else{
					 apiResponse.setStatusCode(404);
					 apiResponse.setMessage("Unable to insert product or service");
					 logger.info("Unable to insert product details Status Code: "+apiResponse.getStatusCode());
				    return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);	
				 		
				 }
			 }else{
			    apiResponse.setStatusCode(400);
			    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			   return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			 }
		   

		 } catch (Exception e) {
		    logger.error("Exception  "+e);
		    apiResponse.setStatusCode(400);
		    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
		    return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);

		 }
		 logger.debug("Exit :  public ResponseEntity insertGSTInvoice(@RequestBody InvoiceVO invoiceVO)")	;	
		 return new ResponseEntity(apiResponse, HttpStatus.OK);

		 }
		 
		 @RequestMapping(value="/update/productService",  method = RequestMethod.POST  )			
			public ResponseEntity updateProductService(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ItemDetailsVO itemDetailsVO) {
				int success=0;
				APIResponseVO apiResponse=new APIResponseVO();
					logger.debug("Entry : public ResponseEntity updateProductService(@RequestBody ItemDetailsVO itemDetailsVO)")	;
				try {
					logger.info("Update product or serviceID: "+itemDetailsVO.getItemID());
					if(itemDetailsVO.getItemID()>0){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						itemDetailsVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
						
						success=invoiceService.updateProductService(itemDetailsVO);
					
					if(success==1){
						apiResponse.setStatusCode(200);
						apiResponse.setMessage("Product or service updated successfully.");
					}else if(success==2) {
						 apiResponse.setStatusCode(409);
						 apiResponse.setMessage("HSN/SAC code is invalid Please enter valid hsn/sac ");					 
						 return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);			
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage("Unable to update product or service.");
						return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
					}
					
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
					

					logger.info("Result of updateProductService : "+success);				
					
				} catch (Exception e) {
					logger.error("Exception  "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity deleteProductService(@RequestBody ItemDetailsVO itemDetailsVO) ")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);
		 
			}
		 
		 @RequestMapping(value="/delete/productService",  method = RequestMethod.POST  )			
			public ResponseEntity deleteProductService(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ItemDetailsVO itemDetailsVO) {
				int success=0;
				APIResponseVO apiResponse=new APIResponseVO();
					logger.debug("Entry : public ResponseEntity deleteProductService(@RequestBody ItemDetailsVO itemDetailsVO)")	;
				try {
					logger.info("Delete product or serviceID: "+itemDetailsVO.getItemID());
					if(itemDetailsVO.getOrgID()>0 && itemDetailsVO.getItemID()>0){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						itemDetailsVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
						
						success=invoiceService.deleteProductService(itemDetailsVO);
					
					if(success==1){
						apiResponse.setStatusCode(200);
						apiResponse.setMessage("Product or service deleted successfully.");
					}else if(success==2){
						apiResponse.setStatusCode(400);
						apiResponse.setMessage("This product or service has few invoices link. ");
						return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage("Unable to delete product or service.");
						return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
					}
					
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
					

					logger.info("Result of deleteProductService : "+success);				
					
				} catch (Exception e) {
					logger.error("Exception  "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity deleteProductService(@RequestBody ItemDetailsVO itemDetailsVO) ")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);
		 
			}
		 
		 /*********** Product Rate Card ***********/
		 @RequestMapping(value="/insert/productRateCard",  method = RequestMethod.POST  )	
		 public ResponseEntity insertProductRateCard(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ItemDetailsVO itemDetailsVO) {
			 int success=0;
		 APIResponseVO apiResponse=new APIResponseVO();
		 logger.debug("Entry :  public ResponseEntity insertProductRateCard(@RequestBody ItemDetailsVO itemDetailsVO)")	;
		 try {
		 logger.info("Insert Product Rate Card:  org ID: "+itemDetailsVO.getOrgID());
			if(itemDetailsVO.getOrgID()>0 ){
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				itemDetailsVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
				success=invoiceService.insertProductRateCard(itemDetailsVO);
				
				 logger.info("Result of insertProductRateCard inserted : "+success);	   
				 if(success==1){				
					 apiResponse.setStatusCode(200);
					 apiResponse.setMessage("Proudct Rate Card inserted successfully ");
				  }else if(success==534) {
					 apiResponse.setStatusCode(534);
					 apiResponse.setMessage("Proudct Rate Card already present.");					 
					 return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);	
				 } else{
					 apiResponse.setStatusCode(404);
					 apiResponse.setMessage("Unable to insert product rate card");
					 logger.info("Unable to insert product rate card Status Code: "+apiResponse.getStatusCode());
				    return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);	
				 		
				 }
			 }else{
			    apiResponse.setStatusCode(400);
			    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			   return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			 }
		   

		 } catch (Exception e) {
		    logger.error("Exception  "+e);
		    apiResponse.setStatusCode(400);
		    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
		    return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);

		 }
		 logger.debug("Exit :  public ResponseEntity insertProductRateCard(@RequestBody InvoiceVO invoiceVO)")	;	
		 return new ResponseEntity(apiResponse, HttpStatus.OK);

		 }
		 
		 @RequestMapping(value="/update/productRateCard",  method = RequestMethod.POST  )			
			public ResponseEntity updateProductRateCard(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ItemDetailsVO itemDetailsVO) {
				int success=0;
				APIResponseVO apiResponse=new APIResponseVO();
					logger.debug("Entry : public ResponseEntity updateProductRateCard(@RequestBody ItemDetailsVO itemDetailsVO)")	;
				try {
					logger.info("Update product rate cardID: "+itemDetailsVO.getRateCardID());
					if(itemDetailsVO.getRateCardID()>0){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						itemDetailsVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
						
						success=invoiceService.updateProductRateCard(itemDetailsVO);
					
					if(success==1){
						apiResponse.setStatusCode(200);
						apiResponse.setMessage("Product rate card updated successfully.");
							
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage("Unable to update product rate card.");
						return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
					}
					
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
					

					logger.info("Result of updateProductRateCard : "+success);				
					
				} catch (Exception e) {
					logger.error("Exception  "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity updateProductRateCard(@RequestBody ItemDetailsVO itemDetailsVO) ")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);
		 
			}
		 
		 @RequestMapping(value="/delete/productRateCard",  method = RequestMethod.POST  )			
			public ResponseEntity deleteProductRateCard(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ItemDetailsVO itemDetailsVO) {
				int success=0;
				APIResponseVO apiResponse=new APIResponseVO();
					logger.debug("Entry : public ResponseEntity deleteProductRateCard(@RequestBody ItemDetailsVO itemDetailsVO)")	;
				try {
					logger.info("Delete product Rate Card ID: "+itemDetailsVO.getRateCardID());
					if(itemDetailsVO.getOrgID()>0 && itemDetailsVO.getRateCardID()>0){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						itemDetailsVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
						
						success=invoiceService.deleteProductRateCard(itemDetailsVO);
					
					if(success==1){
						apiResponse.setStatusCode(200);
						apiResponse.setMessage("Product rate card deleted successfully.");
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage("Unable to delete product rate card .");
						return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
					}
					
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
					

					logger.info("Result of deleteProductRateCard : "+success);				
					
				} catch (Exception e) {
					logger.error("Exception  "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity deleteProductRateCard(@RequestBody ItemDetailsVO itemDetailsVO) ")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);
		 
			}
		 
		 @RequestMapping(value="/insert/productRateCardItem",  method = RequestMethod.POST  )	
		 public ResponseEntity insertProductRateCardItem(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ItemDetailsVO itemDetailsVO) {
			 int success=0;
		 APIResponseVO apiResponse=new APIResponseVO();
		 logger.debug("Entry :  public ResponseEntity insertProductRateCardItem(@RequestBody ItemDetailsVO itemDetailsVO)")	;
		 try {
		 logger.info("Insert Product Rate Card Id: "+itemDetailsVO.getRateCardID());
			if(itemDetailsVO.getRateCardID()>0 && itemDetailsVO.getItemID()>0){
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				itemDetailsVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
				
				success=invoiceService.insertProductRateCardItem(itemDetailsVO);
				
				 logger.info("Result of insertProductRateCardItem inserted : "+success);	   
				 if(success==1){				
					 apiResponse.setStatusCode(200);
					 apiResponse.setMessage("Proudct Rate Card Item inserted successfully ");
				  }else if(success==534) {
					 apiResponse.setStatusCode(534);
					 apiResponse.setMessage("Proudct Rate Card Item already present.");					 
					 return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);	
				 } else{
					 apiResponse.setStatusCode(404);
					 apiResponse.setMessage("Unable to insert product rate card item");
					 logger.info("Unable to insert product rate card item Status Code: "+apiResponse.getStatusCode());
				    return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);	
				 		
				 }
			 }else{
			    apiResponse.setStatusCode(400);
			    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			   return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			 }
		   

		 } catch (Exception e) {
		    logger.error("Exception  "+e);
		    apiResponse.setStatusCode(400);
		    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
		    return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);

		 }
		 logger.debug("Exit :  public ResponseEntity insertProductRateCardItem(@RequestBody InvoiceVO invoiceVO)")	;	
		 return new ResponseEntity(apiResponse, HttpStatus.OK);

		 }
		 
		 @RequestMapping(value="/update/productRateCardItem",  method = RequestMethod.POST  )			
			public ResponseEntity updateProductRateCardItem(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ItemDetailsVO itemDetailsVO) {
				int success=0;
				APIResponseVO apiResponse=new APIResponseVO();
					logger.debug("Entry : public ResponseEntity updateProductRateCardItem(@RequestBody ItemDetailsVO itemDetailsVO)")	;
				try {
					logger.info("Update product rate card itemID: "+itemDetailsVO.getRateCardItemID());
					if(itemDetailsVO.getRateCardItemID()>0){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						itemDetailsVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
						
						success=invoiceService.updateProductRateCardItem(itemDetailsVO);
					
					if(success==1){
						apiResponse.setStatusCode(200);
						apiResponse.setMessage("Product rate card item updated successfully.");
							
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage("Unable to update product rate card item.");
						return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
					}
					
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
					

					logger.info("Result of updateProductRateCardItem : "+success);				
					
				} catch (Exception e) {
					logger.error("Exception  "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity updateProductRateCardItem(@RequestBody ItemDetailsVO itemDetailsVO) ")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);
		 
			}
		 
		 @RequestMapping(value="/delete/productRateCardItem",  method = RequestMethod.POST  )			
			public ResponseEntity deleteProductRateCardItem(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ItemDetailsVO itemDetailsVO) {
				int success=0;
				APIResponseVO apiResponse=new APIResponseVO();
					logger.debug("Entry : public ResponseEntity deleteProductRateCardItem(@RequestBody ItemDetailsVO itemDetailsVO)")	;
				try {
					logger.info("Delete product Rate Card ItemID: "+itemDetailsVO.getRateCardItemID());
					if(itemDetailsVO.getOrgID()>0 && itemDetailsVO.getRateCardItemID()>0){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						itemDetailsVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
						
						success=invoiceService.deleteProductRateCardItem(itemDetailsVO);
					
					if(success==1){
						apiResponse.setStatusCode(200);
						apiResponse.setMessage("Product Rate Card item deleted successfully.");
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage("Unable to delete product rate card item .");
						return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
					}
					
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
					

					logger.info("Result of deleteProductRateCardItem : "+success);				
					
				} catch (Exception e) {
					logger.error("Exception  "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity deleteProductRateCardItem(@RequestBody ItemDetailsVO itemDetailsVO) ")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);
		 
			}
		 
		 @RequestMapping(value="/rateCardList", method = RequestMethod.POST)
			public ResponseEntity getRateCardList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ItemDetailsVO itemVO) {
				JSONResponseVO jSonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				List chapterList=new ArrayList();
				
				logger.debug("Entry :public ResponseEntity getRateCardList(@RequestBody ItemDetailsVO itemVO)")	;
				try {
					
					if(itemVO.getOrgID()>0 ){		
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						itemVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
						
					  chapterList=invoiceService.getRateCardList(itemVO.getOrgID());
					  
						if(chapterList.size()==0){
							jSonVO.setStatusCode(0);
						}else{
							jSonVO.setStatusCode(200);
							jSonVO.setObjectList(chapterList);
						}			
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in getRateCardList "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity getRateCardList(@RequestBody ItemDetailsVO itemVO)")	;						
				return  new ResponseEntity(jSonVO, HttpStatus.OK);
		 
			}
		 
		 
		 @RequestMapping(value="/productRateCardItemList", method = RequestMethod.POST)
			public ResponseEntity getProductRateCardItemList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ItemDetailsVO itemVO) {
				JSONResponseVO jSonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				List chapterList=new ArrayList();
				
				logger.debug("Entry :public ResponseEntity getProductRateCardItemList(@RequestBody ItemDetailsVO itemVO)")	;
				try {
					
					if(itemVO.getOrgID()>0 && itemVO.getRateCardID()>0){		
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						itemVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
						
					  chapterList=invoiceService.getProductRateCardItemList(itemVO.getOrgID(),itemVO.getRateCardID());
					  
						if(chapterList.size()==0){
							jSonVO.setStatusCode(0);
						}else{
							jSonVO.setStatusCode(200);
							jSonVO.setObjectList(chapterList);
						}			
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in getProductRateCardItemList "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity getProductRateCardItemList(@RequestBody ItemDetailsVO itemVO)")	;						
				return  new ResponseEntity(jSonVO, HttpStatus.OK);
		 
			}
		 
		 /*------------------Get Product Rate Card Details  API------------------*/
			@RequestMapping(value="/get/productRateCardDetails",  method = RequestMethod.POST  )			
			public ResponseEntity getProductRateCardDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ItemDetailsVO rateCardVO) {
				int success=0;
				APIResponseVO apiResponse=new APIResponseVO();
				ItemDetailsVO rateCardVO1=new ItemDetailsVO();
					logger.debug("Entry : public ResponseEntity getProductRateCardDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ItemDetailsVO rateCardVO)")	;
				try {
				
					if(rateCardVO.getItemID()>0 && rateCardVO.getRateCardID()>0){
										
						rateCardVO1=invoiceService.getProductRateCardDetails(rateCardVO.getItemID(), rateCardVO.getRateCardID());
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(rateCardVO1, HttpStatus.BAD_REQUEST);
				}
					

					logger.info("Result of  getProductRateCardDetails : "+rateCardVO1.getItemID());				
					
				} catch (Exception e) {
					logger.error("Exception  "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : Public ResponseEntity getProductRateCardDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ItemDetailsVO rateCardVO)")	;						
				return new ResponseEntity(rateCardVO1, HttpStatus.OK);
		 
			}
		 
		 //==========Linking and Unlinking of receipts with invoices ==============//
		 
		 @RequestMapping(value="/unlinkedReceiptList", method = RequestMethod.POST)
			public ResponseEntity getUnlinkedRceiptList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
				JSONResponseVO jsonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				List unlinkedReceiptList=null;
					logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getUnlinkedRceiptList(@RequestBody RequestWrapper requestWrapper)")	;
				try {
					
					if(requestWrapper.getOrgID()>0 ){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
					   unlinkedReceiptList=transactionService.getUnlinkedRceiptList(requestWrapper.getOrgID(), requestWrapper.getLedgerID(),requestWrapper.getFromDate(),requestWrapper.getToDate());
					   if(unlinkedReceiptList.size()==0){
						   jsonVO.setStatusCode(0);
						}else{
							jsonVO.setStatusCode(1);
							jsonVO.setObjectList(unlinkedReceiptList);
						}	
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in getUnlinkedRceiptList "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getUnlinkedRceiptList(@RequestBody RequestWrapper requestWrapper)")	;						
				return  new ResponseEntity(jsonVO, HttpStatus.OK);
		 
			}
		 
		 @RequestMapping(value="/linkReceiptToInvoice",  method = RequestMethod.POST  )	
		 public ResponseEntity linkRceiptList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
			 int success=0;
		 APIResponseVO apiResponse=new APIResponseVO();
		 logger.debug("Entry :  public ResponseEntity linkRceiptList(@RequestBody InvoiceVO invoiceVO)")	;
		 try {
		 logger.info("Link receipt to invoice:  org ID: "+invoiceVO.getOrgID()+" no of receipts : "+invoiceVO.getReceiptList().size());
			if(invoiceVO.getOrgID()>0 ){
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				
				success=transactionService.linkRceiptList(invoiceVO.getOrgID(), invoiceVO.getReceiptList());
				
				 logger.info("Result of linkRceiptList : Linked receipt count : "+success);	   
				 if(success!=0){				
					 apiResponse.setStatusCode(200);
					 apiResponse.setMessage(success+" no of receipt linked");
				 
				 }else{
					 apiResponse.setStatusCode(404);
					 apiResponse.setMessage("Unable to link receipt to invoice");
					 logger.info("Unable to link receipt to invoice : "+apiResponse.getStatusCode());
				    return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);	
				 		
				 }
			 }else{
			    apiResponse.setStatusCode(400);
			    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			   return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			 }
		   

		 } catch (Exception e) {
		    logger.error("Exception  "+e);
		    apiResponse.setStatusCode(400);
		    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
		    return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);

		 }
		 logger.debug("Exit : public ResponseEntity linkRceiptList(@RequestBody InvoiceVO invoiceVO)")	;	
		 return new ResponseEntity(apiResponse, HttpStatus.OK);

		 }
		 
		 
		 @RequestMapping(value="/unlinkReceiptInvoice",  method = RequestMethod.POST  )			
		  	public ResponseEntity unlinkReceiptFromInvoice(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
		       	int success=0;
		  		APIResponseVO apiResponse=new APIResponseVO();
		  			logger.debug("Entry : public ResponseEntity unlinkReceiptFromInvoice(@RequestBody TransactionVO txVO) ")	;
		  		try {
		  			logger.info("Unlink Invoice  : org ID: "+txVO.getSocietyID()+" Invoice ID: "+txVO.getInvoiceID()+" Receipt ID: "+txVO.getReceiptID());
		  			if(txVO.getSocietyID()>0){
		  				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
		  				txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
		  				success=transactionService.unlinkReceiptFromInvoice(txVO.getSocietyID(), txVO);
		  				
		  				if(success==0){				
							 apiResponse.setStatusCode(400);
							 apiResponse.setMessage("Unable to unlink invoiceID: "+txVO.getInvoiceID()+" Status Code: "+apiResponse.getStatusCode());
							 logger.info("Unable to unlink invoiceID: "+txVO.getInvoiceID()+" Status Code: "+apiResponse.getStatusCode());
						    return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);	
						 		
						 }else{
							apiResponse.setStatusCode(200);
							 apiResponse.setMessage("Invoice unlinked successfully ");			
							 
						 }
		  			}else{
		  				logger.info("Unable to unlink invoice details of : "+txVO.getInvoiceID());
		  				apiResponse.setStatusCode(400);
		  				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
		  				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		  			}
		  								
		  			} catch (Exception e) {
		  				logger.error("Exception in unlinkReceiptFromInvoice(@RequestBody TransactionVO txVO): "+e);
		  				apiResponse.setStatusCode(400);
		  				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
		  				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		  			}
		  			
		  		logger.debug("Exit :public ResponseEntity unlinkReceiptFromInvoice(@RequestBody TransactionVO txVO) ")	;						
		  		return new ResponseEntity(apiResponse, HttpStatus.OK);
		   
		  	}
			 
		 
		 //==========Linking and Unlinking of Invoices with invoice ==============//
		 
		 @RequestMapping(value="/unlinkedInvoiceList", method = RequestMethod.POST)
			public ResponseEntity getUnlinkedInvoiceList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
				JSONResponseVO jsonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				List unlinkedInvoiceList=null;
					logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getUnlinkedInvoiceList(@RequestBody RequestWrapper requestWrapper)")	;
				try {
					
					if(requestWrapper.getOrgID()>0 ){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
						unlinkedInvoiceList=invoiceService.getUnlinkedInvoiceList(requestWrapper.getOrgID(), requestWrapper.getLedgerID(),requestWrapper.getFromDate(),requestWrapper.getToDate());
					   if(unlinkedInvoiceList.size()==0){
						   jsonVO.setStatusCode(0);
						}else{
							jsonVO.setStatusCode(1);
							jsonVO.setObjectList(unlinkedInvoiceList);
						}	
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in getUnlinkedInvoiceList "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getUnlinkedInvoiceList(@RequestBody RequestWrapper requestWrapper)")	;						
				return  new ResponseEntity(jsonVO, HttpStatus.OK);
		 
			}
		 
		 @RequestMapping(value="/linkInvToInvoice",  method = RequestMethod.POST  )	
		 public ResponseEntity linkInvoiceList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
			 int success=0;
		 APIResponseVO apiResponse=new APIResponseVO();
		 logger.debug("Entry :  public ResponseEntity linkInvoiceList(@RequestBody InvoiceVO invoiceVO)")	;
		 try {
		 logger.info("Link receipt to invoice:  org ID: "+invoiceVO.getOrgID()+" no of invoices : "+invoiceVO.getLinkInvList().size());
			if(invoiceVO.getOrgID()>0 ){
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				
				success=invoiceService.linkInvoiceList(invoiceVO.getOrgID(), invoiceVO.getLinkInvList());
				
				 logger.info("Result of linkInvoiceList : Linked invoices count : "+success);	   
				 if(success!=0){				
					 apiResponse.setStatusCode(200);
					 apiResponse.setMessage(success+" no of Invoice linked");
				 
				 }else{
					 apiResponse.setStatusCode(404);
					 apiResponse.setMessage("Unable to link invoice to invoice");
					 logger.info("Unable to link receipt to invoice : "+apiResponse.getStatusCode());
				    return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);	
				 		
				 }
			 }else{
			    apiResponse.setStatusCode(400);
			    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			   return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			 }
		   

		 } catch (Exception e) {
		    logger.error("Exception  "+e);
		    apiResponse.setStatusCode(400);
		    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
		    return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);

		 }
		 logger.debug("Exit : public ResponseEntity linkInvoiceList(@RequestBody InvoiceVO invoiceVO)")	;	
		 return new ResponseEntity(apiResponse, HttpStatus.OK);

		 }
		 
		 
		 @RequestMapping(value="/unlinkLinkedInvoice",  method = RequestMethod.POST  )			
		  	public ResponseEntity unlinkLinkedInvoiceFromInvoice(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
		       	int success=0;
		  		APIResponseVO apiResponse=new APIResponseVO();
		  			logger.debug("Entry : public ResponseEntity unlinkLinkedInvoiceFromInvoice(@RequestBody TransactionVO txVO) ")	;
		  		try {
		  			logger.info("Unlink Invoice  : org ID: "+invoiceVO.getOrgID()+" Invoice ID: "+invoiceVO.getInvoiceID()+" Linked incoice ID: "+invoiceVO.getLinkedInvoiceID());
		  			if(invoiceVO.getOrgID()>0){
		  				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
		  				invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
		  				success=invoiceService.unlinkInvoiceFromInvoice(invoiceVO.getOrgID(), invoiceVO);
		  				
		  				if(success==0){				
							 apiResponse.setStatusCode(400);
							 apiResponse.setMessage("Unable to unlink invoiceID: "+invoiceVO.getInvoiceID()+" Status Code: "+apiResponse.getStatusCode());
							 logger.info("Unable to unlink invoiceID: "+invoiceVO.getInvoiceID()+" Status Code: "+apiResponse.getStatusCode());
						    return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);	
						 		
						 }else{
							apiResponse.setStatusCode(200);
							 apiResponse.setMessage("Invoice unlinked successfully ");			
							 
						 }
		  			}else{
		  				logger.info("Unable to unlink invoice details of : "+invoiceVO.getInvoiceID());
		  				apiResponse.setStatusCode(400);
		  				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
		  				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		  			}
		  								
		  			} catch (Exception e) {
		  				logger.error("Exception in unlinkLinkedInvoiceFromInvoice(@RequestBody TransactionVO txVO): "+e);
		  				apiResponse.setStatusCode(400);
		  				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
		  				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		  			}
		  			
		  		logger.debug("Exit :public ResponseEntity unlinkLinkedInvoiceFromInvoice(@RequestBody TransactionVO txVO) ")	;						
		  		return new ResponseEntity(apiResponse, HttpStatus.OK);
		   
		  	}

		 
		 
			
		 	//========== Package Details for Organization ==============//
		 
		 @RequestMapping(value="/getPackageList", method = RequestMethod.POST)
			public ResponseEntity getPackageList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
				JSONResponseVO jsonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				List packageList=null;
					logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getPackageList(@RequestBody RequestWrapper requestWrapper)")	;
				try {
					
					if(requestWrapper.getOrgID()>0 ){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
						packageList=accountsAutoService.getPackageDetailsList(requestWrapper.getOrgID());
					   if(packageList.size()==0){
						   jsonVO.setStatusCode(0);
						}else{
							jsonVO.setStatusCode(1);
							jsonVO.setObjectList(packageList);
						}	
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in packageList "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getPackageList(@RequestBody RequestWrapper requestWrapper)")	;						
				return  new ResponseEntity(jsonVO, HttpStatus.OK);
		 
			}
	
		 @RequestMapping(value="/getPackageChargeList", method = RequestMethod.POST)
			public ResponseEntity getPackageChargeList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PackageDetailsVO requestWrapper) {
				JSONResponseVO jsonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				List packageChargeList=null;
					logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getPackageChargeList(@RequestBody RequestWrapper requestWrapper)")	;
				try {
					
					if(requestWrapper.getOrgID()>0 ){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
						packageChargeList=accountsAutoService.getApplicableCharges(requestWrapper.getOrgID(), requestWrapper.getPackageID(),0);
					   if(packageChargeList.size()==0){
						   jsonVO.setStatusCode(0);
						}else{
							jsonVO.setStatusCode(1);
							jsonVO.setObjectList(packageChargeList);
						}	
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in getPackageChargeList "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getPackageChargeList(@RequestBody RequestWrapper requestWrapper)")	;						
				return  new ResponseEntity(jsonVO, HttpStatus.OK);
		 
			}
	 
		 
		 @RequestMapping(value="/getPackageProfileList", method = RequestMethod.POST)
			public ResponseEntity getPackageProfileList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PackageDetailsVO requestWrapper) {
				JSONResponseVO jsonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				List packageProfileList=null;
					logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getPackageChargeList(@RequestBody RequestWrapper requestWrapper)")	;
				try {
					
					if(requestWrapper.getOrgID()>0 ){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
						packageProfileList=accountsAutoService.getProfileDetailsListForPackages(requestWrapper.getOrgID(), requestWrapper.getPackageID());
					   if(packageProfileList.size()==0){
						   jsonVO.setStatusCode(0);
						}else{
							jsonVO.setStatusCode(1);
							jsonVO.setObjectList(packageProfileList);
						}	
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in getPackageChargeList "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getPackageChargeList(@RequestBody RequestWrapper requestWrapper)")	;						
				return  new ResponseEntity(jsonVO, HttpStatus.OK);
		 
			}
		 
		 
		 // Currency List
		 @RequestMapping(value="/getCurrencyList", method = RequestMethod.POST)
			public ResponseEntity getCurrencyList(@RequestBody RequestWrapper requestWrapper) {
				JSONResponseVO jsonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				List currencyList=null;
					logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getCurrencyList(@RequestBody RequestWrapper requestWrapper)")	;
				try {
					
					currencyList=invoiceService.getCurrencyList();
					   if(currencyList.size()==0){
						   jsonVO.setStatusCode(0);
						}else{
							jsonVO.setStatusCode(1);
							jsonVO.setObjectList(currencyList);
						}	
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in getCurrencyList "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getCurrencyList(@RequestBody RequestWrapper requestWrapper)")	;						
				return  new ResponseEntity(jsonVO, HttpStatus.OK);
		 
			}
		 
		//=============== Age wise receivables and payable reports ================//
		 
		 @RequestMapping(value="/getAgewiseReceivableList", method = RequestMethod.POST)
			public ResponseEntity getAgewiseReceivableList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
				JSONResponseVO jSonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				List receivablesList=new ArrayList();
				
					logger.debug("Entry : public ResponseEntity getAgewiseReceivableList(@RequestBody InvoiceVO invoiceVO)")	;
				try {
					
					if((invoiceVO.getOrgID()>0) ){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						
						receivablesList=invoiceService.getAgewiseReceivalesList(invoiceVO.getOrgID(), invoiceVO.getIsDueDate());
						if(receivablesList.size()==0){
							jSonVO.setStatusCode(0);
						}else{
							jSonVO.setStatusCode(200);
							jSonVO.setObjectList(receivablesList);
						}			
					   
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in getAgewiseReceivableList "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity getAgewiseReceivableList(@RequestBody InvoiceVO invoiceVO)")	;						
				return  new ResponseEntity(jSonVO, HttpStatus.OK);
		 
			}
		 
		 
		 @RequestMapping(value="/getAgewisePayableList", method = RequestMethod.POST)
			public ResponseEntity getAgewisePayableList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
				JSONResponseVO jSonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				List payableList=new ArrayList();
				
					logger.debug("Entry : public ResponseEntity getAgewisePayableList(@RequestBody InvoiceVO invoiceVO)")	;
				try {
					
					if((invoiceVO.getOrgID()>0) ){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						
						payableList=invoiceService.getAgewisePayablesList(invoiceVO.getOrgID(), invoiceVO.getIsDueDate());
						if(payableList.size()==0){
							jSonVO.setStatusCode(0);
						}else{
							jSonVO.setStatusCode(200);
							jSonVO.setObjectList(payableList);
						}			
					   
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in getAgewisePayableList "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity getAgewisePayableList(@RequestBody InvoiceVO invoiceVO)")	;						
				return  new ResponseEntity(jSonVO, HttpStatus.OK);
		 
			}
		 
		 
		 //============================ Invoicing App - Reports ==============================//
		 //Product Sales by Customers. (Reports)
		 @RequestMapping(value="/getSalesByCustomer", method = RequestMethod.POST)
			public ResponseEntity getSalesByCustomer(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
				JSONResponseVO jSonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				List invoiceList=new ArrayList();
				
					logger.debug("Entry : public ResponseEntity getSalesByCustomer(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;
				try {
					
					if((invoiceVO.getOrgID()>0) ){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						
						invoiceList=invoiceService.getSalesByCustomer(invoiceVO.getOrgID(), invoiceVO.getFromDate(),invoiceVO.getUptoDate(),invoiceVO.getLedgerID());
						if(invoiceList.size()==0){
							jSonVO.setStatusCode(0);
						}else{
							jSonVO.setStatusCode(200);
							jSonVO.setObjectList(invoiceList);
						}			
					   
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in getSalesByCustomer "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity getSalesByCustomer(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;						
				return  new ResponseEntity(jSonVO, HttpStatus.OK);
		 
			}
		 
		//Month wise sales trend
		 @RequestMapping(value="/getMonthlySalesTrend", method = RequestMethod.POST)
			public ResponseEntity getMonthlySalesTrend(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
				JSONResponseVO jSonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				MonthwiseChartVO reportVO=new MonthwiseChartVO();
				List reportList=new ArrayList();
				
					logger.debug("Entry : public ResponseEntity getMonthlySalesTrend(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;
				try {
					
					if((invoiceVO.getOrgID()>0) ){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						
						reportVO=invoiceService.getMonthWiseSalesReportForChart(invoiceVO.getOrgID(), invoiceVO.getFromDate(),invoiceVO.getUptoDate());
						if(reportVO.getTransactionType()==null){
							jSonVO.setStatusCode(0);
						}else{
							reportList.add(reportVO);
							jSonVO.setStatusCode(200);
							jSonVO.setObjectList(reportList);
						}			
					   
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in getMonthlySalesTrend "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity getMonthlySalesTrend(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;						
				return  new ResponseEntity(jSonVO, HttpStatus.OK);
		 
			}
		 
		 //Top 20 Sales Products. 
		 @RequestMapping(value="/getTop20SalesProduct", method = RequestMethod.POST)
			public ResponseEntity getTop20SalesProduct(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
				JSONResponseVO jSonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				List invoiceList=new ArrayList();
				
					logger.debug("Entry : public ResponseEntity getTop20SalesProduct(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;
				try {
					
					if((invoiceVO.getOrgID()>0) ){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						
						invoiceList=invoiceService.getTop20SalesProduct(invoiceVO.getOrgID(), invoiceVO.getFromDate(),invoiceVO.getUptoDate());
						if(invoiceList.size()==0){
							jSonVO.setStatusCode(0);
						}else{
							jSonVO.setStatusCode(200);
							jSonVO.setObjectList(invoiceList);
						}			
					   
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in getTop20SalesProduct "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity getTop20SalesProduct(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;						
				return  new ResponseEntity(jSonVO, HttpStatus.OK);
		 
			}
		 
		//============================ Invoicing App - Reports ==============================//
		// Product wise total sales. (Reports)
		 @RequestMapping(value="/getSalesReportProductWise", method = RequestMethod.POST)
			public ResponseEntity getSalesReportProductWise(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
				JSONResponseVO jSonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				List invoiceList=new ArrayList();
				
					logger.debug("Entry : public ResponseEntity getSalesReportProductWise(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;
				try {
					
					if((invoiceVO.getOrgID()>0) ){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						
						invoiceList=invoiceService.getSalesReportProductWise(invoiceVO.getOrgID(), invoiceVO.getFromDate(),invoiceVO.getUptoDate(),invoiceVO.getProductID());
						if(invoiceList.size()==0){
							jSonVO.setStatusCode(0);
						}else{
							jSonVO.setStatusCode(200);
							jSonVO.setObjectList(invoiceList);
						}			
					   
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in getSalesReportProductWise "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity getSalesReportProductWise(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;						
				return  new ResponseEntity(jSonVO, HttpStatus.OK);
		 
			}
		 
		//============================ Invoicing App - Reports ==============================//
			// Product wise total sales. (Reports)
			 @RequestMapping(value="/getSalesReportProductWiseDetails", method = RequestMethod.POST)
				public ResponseEntity getSalesReportProductWiseDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
					JSONResponseVO jSonVO=new JSONResponseVO();
					APIResponseVO apiResponse=new APIResponseVO();
					List invoiceList=new ArrayList();
					
						logger.debug("Entry : public ResponseEntity getSalesReportProductWiseDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;
					try {
						
						if((invoiceVO.getOrgID()>0) ){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
							
							invoiceList=invoiceService.getSalesReportProductWiseDetails(invoiceVO.getOrgID(), invoiceVO.getFromDate(),invoiceVO.getUptoDate(),invoiceVO.getProductID(),invoiceVO.getLedgerID());
							if(invoiceList.size()==0){
								jSonVO.setStatusCode(0);
							}else{
								jSonVO.setStatusCode(200);
								jSonVO.setObjectList(invoiceList);
							}			
						   
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						logger.error("Exception in getSalesReportProductWiseDetails "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public ResponseEntity getSalesReportProductWiseDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;						
					return  new ResponseEntity(jSonVO, HttpStatus.OK);
			 
				}
		 
		//Total Sales by Customers 
		 @RequestMapping(value="/getTotalSalesByCustomer", method = RequestMethod.POST)
			public ResponseEntity getTotalSalesByCustomer(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
				JSONResponseVO jSonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				List invoiceList=new ArrayList();
				
					logger.debug("Entry : public ResponseEntity getSalesByCustomer(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;
				try {
					
					if((invoiceVO.getOrgID()>0) ){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						
						invoiceList=invoiceService.getTotalSalesByCustomer(invoiceVO.getOrgID(), invoiceVO.getFromDate(),invoiceVO.getUptoDate());
						if(invoiceList.size()==0){
							jSonVO.setStatusCode(0);
						}else{
							jSonVO.setStatusCode(200);
							jSonVO.setObjectList(invoiceList);
						}			
					   
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in getSalesByCustomer "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity getSalesByCustomer(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;						
				return  new ResponseEntity(jSonVO, HttpStatus.OK);
		 
			}
		 
		//Total Sales by Customers 
		 @RequestMapping(value="/getTotalSalesByCustomerDetails", method = RequestMethod.POST)
			public ResponseEntity getTotalSalesByCustomerDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
				JSONResponseVO jSonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				List invoiceList=new ArrayList();
				
					logger.debug("Entry : public ResponseEntity getTotalSalesByCustomerDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;
				try {
					
					if((invoiceVO.getOrgID()>0) ){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						
						invoiceList=invoiceService.getTotalSalesByCustomerDetails(invoiceVO.getOrgID(), invoiceVO.getFromDate(),invoiceVO.getUptoDate(),invoiceVO.getLedgerID());
						if(invoiceList.size()==0){
							jSonVO.setStatusCode(0);
						}else{
							jSonVO.setStatusCode(200);
							jSonVO.setObjectList(invoiceList);
						}			
					   
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in getTotalSalesByCustomerDetails "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity getTotalSalesByCustomerDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;						
				return  new ResponseEntity(jSonVO, HttpStatus.OK);
		 
			}
		 
		//Get Monthwise Sales report
		 @RequestMapping(value="/getMonthWiseSalesReport", method = RequestMethod.POST)
			public ResponseEntity getMonthWiseSalesReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
				JSONResponseVO jSonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				List invoiceList=new ArrayList();
				
					logger.debug("Entry : public ResponseEntity getMonthWiseSalesReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;
				try {
					
					if((invoiceVO.getOrgID()>0) ){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						
						invoiceList=invoiceService.getMonthWiseSalesReport(invoiceVO.getOrgID(), invoiceVO.getFromDate(),invoiceVO.getUptoDate());
						if(invoiceList.size()==0){
							jSonVO.setStatusCode(0);
						}else{
							jSonVO.setStatusCode(200);
							jSonVO.setObjectList(invoiceList);
						}			
					   
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in getMonthWiseSalesReport "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity getMonthWiseSalesReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;						
				return  new ResponseEntity(jSonVO, HttpStatus.OK);
		 
			}
		 
		//Get Monthwise Sales report
		 @RequestMapping(value="/getMonthWiseSalesReportDetails", method = RequestMethod.POST)
			public ResponseEntity getMonthWiseSalesReportDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO) {
				JSONResponseVO jSonVO=new JSONResponseVO();
				APIResponseVO apiResponse=new APIResponseVO();
				List invoiceList=new ArrayList();
				
					logger.debug("Entry : public ResponseEntity getMonthWiseSalesReportDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;
				try {
					
					if((invoiceVO.getOrgID()>0) ){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						invoiceVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						
						invoiceList=invoiceService.getMonthWiseSalesReportDetails(invoiceVO.getOrgID(), invoiceVO.getFromDate(),invoiceVO.getUptoDate(),invoiceVO.getProductID());
						if(invoiceList.size()==0){
							jSonVO.setStatusCode(0);
						}else{
							jSonVO.setStatusCode(200);
							jSonVO.setObjectList(invoiceList);
						}			
					   
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception in getMonthWiseSalesReport "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity getMonthWiseSalesReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody InvoiceVO invoiceVO)")	;						
				return  new ResponseEntity(jSonVO, HttpStatus.OK);
		 
			}
		 
}
