package com.emanager.webservice;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.security.web.util.matcher.IpAddressMatcher;

import com.emanager.server.commonUtils.domainObject.ConfigManager;

public class IpCheckService {
	private static final Logger logger = Logger.getLogger(IpCheckService.class);
    @SuppressWarnings("static-access")
	public boolean isValid(HttpServletRequest request) {
    	
    	ConfigManager c=new ConfigManager();
        //This  service is a bean so you can inject other dependencies,
            //for example load the white list of IPs from the database 
        IpAddressMatcher matcher = new IpAddressMatcher(c.getPropertiesValue("ip.address"));
        String address = request.getRemoteAddr();
    try {
    	logger.info("Users IP Address = " +address+" & Application Allowed IP Address = "+c.getPropertiesValue("ip.address"));
        return matcher.matches(request);
        
    } catch (Exception e) {
    	logger.error("Exception occured at IP Checker "+e);
        return false;
    }
    }
}