package com.emanager.webservice;

import java.math.BigDecimal;
import java.util.List;

public class JSONResponseVO {
	private List objectList;	
	private int statusCode;
	private int societyID;
	private int aptID;
	private String memberName;
	private int memberID;
	private int ledgerID;
	private String fromDate;
	private String uptoDate;
	private BigDecimal balance;
	private String societyName;
	
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public int getAptID() {
		return aptID;
	}
	public void setAptID(int aptID) {
		this.aptID = aptID;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public int getLedgerID() {
		return ledgerID;
	}
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	public int getMemberID() {
		return memberID;
	}
	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public List getObjectList() {
		return objectList;
	}
	public void setObjectList(List objectList) {
		this.objectList = objectList;
	}
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getUptoDate() {
		return uptoDate;
	}
	public void setUptoDate(String uptoDate) {
		this.uptoDate = uptoDate;
	}
	/**
	 * @return the societyName
	 */
	public String getSocietyName() {
		return societyName;
	}
	/**
	 * @param societyName the societyName to set
	 */
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	
	
}
