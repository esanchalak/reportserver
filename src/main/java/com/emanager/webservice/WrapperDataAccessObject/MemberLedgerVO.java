package com.emanager.webservice.WrapperDataAccessObject;

import java.math.BigDecimal;

public class MemberLedgerVO {
	
	private String memberName;
	private String mobile;
	private String email;
	private String unitName;
	private int memberID;
	private int buildingID;
	private int aptID;
	private int societyID;
	private int ledgerID;
	private String ledgerName;
	private BigDecimal openingBalance;
	private BigDecimal closingBalance;
	private BigDecimal crClosingBalance;
	private BigDecimal drClosingBalance;
	
	
	
	public int getAptID() {
		return aptID;
	}
	public void setAptID(int aptID) {
		this.aptID = aptID;
	}
	public int getBuildingID() {
		return buildingID;
	}
	public void setBuildingID(int buildingID) {
		this.buildingID = buildingID;
	}
	public BigDecimal getClosingBalance() {
		return closingBalance;
	}
	public void setClosingBalance(BigDecimal closingBalance) {
		this.closingBalance = closingBalance;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getLedgerID() {
		return ledgerID;
	}
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	public int getMemberID() {
		return memberID;
	}
	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public BigDecimal getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(BigDecimal openingBalance) {
		this.openingBalance = openingBalance;
	}
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public BigDecimal getCrClosingBalance() {
		return crClosingBalance;
	}
	public void setCrClosingBalance(BigDecimal crClosingBalance) {
		this.crClosingBalance = crClosingBalance;
	}
	public BigDecimal getDrClosingBalance() {
		return drClosingBalance;
	}
	public void setDrClosingBalance(BigDecimal drClosingBalance) {
		this.drClosingBalance = drClosingBalance;
	}
	public String getLedgerName() {
		return ledgerName;
	}
	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}

}
