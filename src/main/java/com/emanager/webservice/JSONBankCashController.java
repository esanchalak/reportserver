package com.emanager.webservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.emanager.server.accounts.DataAccessObjects.LedgerTrnsVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.adminReports.Services.PrintReportService;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.dashBoard.Services.AccountDashBoardService;
import com.emanager.server.dashBoard.Services.DashBoardService;
import com.emanager.server.financialReports.services.BalanceSheetService;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.financialReports.services.TrialBalanceService;
import com.emanager.server.invoice.Services.InvoiceService;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.userManagement.service.UserService;
import com.emanager.server.userManagement.valueObject.UserVO;
  
	@Controller
	@RequestMapping("/secure/bankCashAccount")
	
	public class JSONBankCashController {
		private static final Logger logger = Logger.getLogger(JSONBankCashController.class);
		@Autowired
	    ReportService reportService;
	    @Autowired
	    TransactionService transactionService;
	    @Autowired
	    TransactionService txService;
	    @Autowired
	    InvoiceService invoiceService;
	    @Autowired
	    PrintReportService printReportService;
	    @Autowired
	    MemberService memberServiceBean;
	    @Autowired
	    DashBoardService dashBoardService;
	    @Autowired
	    AccountDashBoardService accountDashBoardService;
	    @Autowired
	    SocietyService societyService;
	    @Autowired
	    LedgerService ledgerService;
	    @Autowired
	    TrialBalanceService trialBalanceService;
	    @Autowired
	    BalanceSheetService balanceSheetService;
	    @Autowired
	    UserService userService;       
	    public ConfigManager configManager =new ConfigManager();
	    public EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
		public final String HEADER_SECURITY_TOKEN = "X-AuthToken";
		
	    @RequestMapping(value="/masterLedgersList", method = RequestMethod.POST)
		public ResponseEntity getMasterLedgersList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List ledgerList=new ArrayList();
			SocietyVO societyVo=new SocietyVO();
			logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getMasterLedgersList(@RequestBody RequestWrapper requestWrapper)")	;
			try {
                
				if(requestWrapper.getOrgID()>0 ){

				   UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				   requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
				   societyVo=societyService.getSocietyDetails(requestWrapper.getOrgID());
				   jSonVO.setSocietyVO(societyVo);
				
				   ledgerList=ledgerService.getMasterLedgerList(requestWrapper.getOrgID(), requestWrapper.getGroupID(), requestWrapper.getCategoryID());
				   jSonVO.setSocietyID(requestWrapper.getOrgID());
				  if(ledgerList.size()>0){
				    jSonVO.setObjectList(ledgerList);
				    jSonVO.setStatusCode(1);
				  }
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getMasterLedgersList "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getMasterLedgersList(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);	
	 
		}
		
	    @RequestMapping(value="/transactionList", method = RequestMethod.POST)
		public ResponseEntity getTransctionsList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			LedgerTrnsVO ledgerTxVO=null;
				logger.debug("Entry : public @ResponseBody LedgerTrnsVO getTransctionsList(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				if(requestWrapper.getOrgID()>0){
					   UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					   requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				ledgerTxVO=ledgerService.getQuickLedgerTransactionList(requestWrapper.getOrgID(), requestWrapper.getLedgerID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),"ALL");
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getTransctionsList "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody LedgerTrnsVO getTransctionsList(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(ledgerTxVO, HttpStatus.OK);
	 
		}
		
		/*------------------Insert Receipt transaction API------------------*/
	    @RequestMapping(value="/insert/transaction",  method = RequestMethod.POST  )			
		public ResponseEntity insertTransaction(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody int insertTransaction( TransactionVO txVO)")	;
			try {
				logger.info("insertTransaction org ID: "+txVO.getSocietyID()+" Amount: "+txVO.getAmount());
				if(txVO.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));	
			       txVO=transactionService.addTransaction(txVO, txVO.getSocietyID());
			       
			   	if(txVO.getStatusCode()!=200){
					apiResponse.setStatusCode(txVO.getStatusCode());
					apiResponse.setMessage(txVO.getErrorMessage());
					logger.info("Unable to insert transaction Status Code: "+txVO.getStatusCode());
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
					
				}else{
					
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Transaction added successfully ");
							
				}
			       		
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.info("Result of Transaction insertion : "+txVO.getTransactionID());				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				
			}
			logger.debug("Exit : public @ResponseBody int insertTransaction( TransactionVO txVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		@RequestMapping(value="/insert/receipt",  method = RequestMethod.POST  )			
		public ResponseEntity insertReceiptTransaction(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody int insertReceiptTransaction(@RequestBody TransactionVO txVO) ")	;
			try {
				logger.info("insertReceiptTransaction org ID: "+txVO.getSocietyID());
				if(txVO.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));	
				txVO=invoiceService.addReceptForInvoice(txVO.getSocietyID() ,txVO,txVO.getInvoiceDetailsVO());
				
				if(txVO.getStatusCode()!=200){
					apiResponse.setStatusCode(txVO.getStatusCode());
					apiResponse.setMessage(txVO.getErrorMessage());
					logger.info("Unable to insert receipt transaction Status Code: "+txVO.getStatusCode());
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
					
				}else {
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Transaction added successfully ");
				}
				
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
							
				
			} catch (Exception e) {
				logger.error("Exception insertReceiptTransaction "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int insertReceiptTransaction(@RequestBody TransactionVO txVO) ")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		
		/*------------------Update transaction API------------------*/
		@RequestMapping(value="/update/transaction",  method = RequestMethod.POST  )			
		public ResponseEntity updateTransaction(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
			int success=0;
			APIResponseVO apiRespose=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody int updateTransaction( TransactionVO txVO)")	;
			try {
				logger.info("updateTransaction: org ID: "+txVO.getSocietyID()+" TX ID "+txVO.getTransactionID());
				if(txVO.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));	
			      txVO=transactionService.updateTransaction(txVO, txVO.getSocietyID(), txVO.getTransactionID());
				
			 	if(txVO.getStatusCode()!=200){
					apiRespose.setStatusCode(txVO.getStatusCode());
					apiRespose.setMessage(txVO.getErrorMessage());
					logger.info("Unable to update transaction ID : "+txVO.getTransactionID()+ " Status Code: "+txVO.getStatusCode());
					return new ResponseEntity(apiRespose, HttpStatus.NOT_ACCEPTABLE);				
					
				}else {
					apiRespose.setStatusCode(200);
					apiRespose.setMessage("Transaction updated successfully ");
				}
				
				
			}else{
				apiRespose.setStatusCode(400);
				apiRespose.setMessage(configManager.getPropertiesValue("error."+apiRespose.getStatusCode()));
				return new ResponseEntity(apiRespose, HttpStatus.BAD_REQUEST);
			}
					
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiRespose.setStatusCode(400);
				apiRespose.setMessage(configManager.getPropertiesValue("error."+apiRespose.getStatusCode()));
				return new ResponseEntity(apiRespose, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int updateTransaction( TransactionVO txVO)")	;						
			return new ResponseEntity(apiRespose, HttpStatus.OK);
	 
		}
		
		/*------------------Get transaction API------------------*/
		@RequestMapping(value="/get/transaction",  method = RequestMethod.POST  )			
		public ResponseEntity getTransaction(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
			TransactionVO transactionVO=new TransactionVO();
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody TransactionVO getTransaction(@RequestBody TransactionVO txVO)")	;
			try {
				logger.info("getTransaction org ID: "+txVO.getSocietyID()+" TX ID: "+txVO.getTransactionID());
				if(txVO.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));	
				transactionVO=transactionService.getTransctionDetails(txVO.getSocietyID(), txVO.getTransactionID());
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
				} catch (Exception e) {
					logger.error("Exception  "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			logger.debug("Exit : public @ResponseBody TransactionVO getTransaction(@RequestBody TransactionVO txVO)")	;						
			return new ResponseEntity(transactionVO, HttpStatus.OK);
	 
		}
		
		/*------------------Delete transaction API------------------*/
		@RequestMapping(value="/delete/transaction",  method = RequestMethod.POST  )			
		public ResponseEntity deleteTransaction(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody int deleteTransaction(@RequestBody TransactionVO txVO)")	;
			try {
				logger.info("deleteTransaction org ID: "+txVO.getSocietyID()+" TX ID: "+txVO.getTransactionID());
				if(txVO.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
			    	txVO=transactionService.deleteTransaction(txVO, txVO.getSocietyID(), txVO.getSubGroupID());
				
				if(txVO.getStatusCode()!=200){
					apiResponse.setStatusCode(txVO.getStatusCode());
					apiResponse.setMessage(txVO.getErrorMessage());
					logger.info("Unable to delete transaction ID : "+txVO.getTransactionID()+ " Status Code: "+txVO.getStatusCode());
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
					
				}else {
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Transaction deleted successfully ");
				}
				}
				
				
			} catch (Exception e) {
				logger.error("Exception deleteTransaction "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int deleteTransaction(@RequestBody TransactionVO txVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/unpaidInvoiceList", method = RequestMethod.POST)
		public ResponseEntity getUnpaidInvoiceList(@RequestBody RequestWrapper requestWrapper) {
			JSONResponseVO jsonVO=new JSONResponseVO();
			List invoiceList=null;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody JSONResponseVO getUnpaidInvoiceList(@RequestBody RequestWrapper requestWrapper) ");
			try {
				if(requestWrapper.getOrgID()>0){
				 invoiceList= invoiceService.getUnpaidInvoicesForMember(requestWrapper.getOrgID(),Integer.toString(requestWrapper.getLedgerID()));
				 if(invoiceList.size()==0){
						jsonVO.setStatusCode(0);
					}else{
						jsonVO.setStatusCode(1);
						jsonVO.setObjectList(invoiceList);
					}		
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getUnpaidInvoiceList:  "+e);
				  apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JSONResponseVO getUnpaidInvoiceList(@RequestBody RequestWrapper requestWrapper) ");						
			return new ResponseEntity(jsonVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/updateBankDateList",  method = RequestMethod.POST  )			
		public ResponseEntity updateBankDateList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody JsonRespAccountsVO bankDateVO) {
			int success=0;
			List transactionList=null;
			TransactionVO bankTxVO=new TransactionVO();
			APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public @ResponseBody int updateBankDateList(@RequestBody JsonRespAccountsVO bankDateVO");
			try {
			   if(bankDateVO.getTxList().size()>0){
				   UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				   bankDateVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));  
				   
		 		 for(int i=0;i<bankDateVO.getTxList().size();i++){
		 			//logger.info(" "+bankDateVO.getTxList().get(i) );
		 			TransactionVO txVO=(TransactionVO) bankDateVO.getTxList().get(i);
		 			bankTxVO=transactionService.updateTrasactionBankDate(txVO.getBankDate(), Integer.toString(txVO.getTransactionID()), Integer.toString(bankDateVO.getSocietyID()));
		 			
		 			if(bankTxVO.getStatusCode()==200){
		 				apiResponse.setStatusCode(200);
						apiResponse.setMessage("Bank date updated successfully ");
		 			}else{
		 				apiResponse.setStatusCode(bankTxVO.getStatusCode());
						apiResponse.setMessage(bankTxVO.getErrorMessage());
						return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
		 			}
		 			
				 }
		 		
			  }else{
				  apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			  }
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				success=0;
				logger.error("Exception  updateBankDateList"+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int updateBankDateList(@RequestBody JsonRespAccountsVO bankDateVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/reconcilationList", method = RequestMethod.POST)
		public ResponseEntity getReconcilationList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			LedgerTrnsVO ledgerTxVO=null;
			APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public @ResponseBody LedgerTrnsVO getReconcilationList(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				if(requestWrapper.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
				ledgerTxVO=ledgerService.getReconcilationList(requestWrapper.getOrgID(), requestWrapper.getLedgerID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getType());
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody LedgerTrnsVO getReconcilationList(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(ledgerTxVO, HttpStatus.OK);
	 
		}
		
		
		
		
       }
