package com.emanager.webservice;

import java.nio.charset.Charset;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.society.services.MemberService;

import com.emanager.server.userManagement.domainObject.UserAuthenticationDomain;
import com.emanager.server.userManagement.service.AppDetailsService;
import com.emanager.server.userManagement.service.UserService;
import com.emanager.server.userManagement.valueObject.AppVO;
import com.emanager.server.userManagement.valueObject.UserVO;


  @Controller
  @RequestMapping("/secure/user")
  public class JSONUserController {
	  private static final Logger logger = Logger.getLogger(JSONUserController.class);
		@Autowired
		MemberService memberServiceBean;
		
		@Autowired
		UserService userService;
		
		@Autowired
		AppDetailsService appDetailsService;
		
		public ConfigManager configManager =new ConfigManager();
		public EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
		public final String HEADER_SECURITY_TOKEN = "X-AuthToken"; 
		
	   
		@RequestMapping(value="/userList", method = RequestMethod.POST)
		public ResponseEntity getUserList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody UserVO orgVO ){
			 List userList=null;
			UserVO userVO=new UserVO();
			 APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity UserVO getUserList(@RequestBody UserVO orgVO )")	;
			try {
				logger.info(" orgID: "+orgVO.getOrgID()+" appID : "+ orgVO.getAppID());
				if(orgVO.getOrgID().length()>0 && orgVO.getAppID().length()>0){	
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					orgVO.setOrgID(decrptVO.getOrgID());	
					orgVO.setAppID(decrptVO.getAppID());
					
				   userList=userService.getUserList(orgVO.getOrgID(), orgVO.getAppID());
				   userVO.setObjectList(userList);
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in  getOrganizationList: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity UserVO getUserList(@RequestBody UserVO orgVO )")	;
			return new ResponseEntity(userVO, HttpStatus.OK);
		}
		
		@RequestMapping(value="/appwiseUserList", method = RequestMethod.POST)
		public ResponseEntity getAppwiseUserList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody UserVO orgVO ){
			 List userList=null;
			UserVO userVO=new UserVO();
			 APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity UserVO getAppwiseUserList(@RequestBody UserVO orgVO )")	;
			try {
				logger.info(" orgID: "+orgVO.getOrgID()+" appID : "+ orgVO.getAppID());
				if(orgVO.getOrgID().length()>0 && orgVO.getAppID().length()>0){	
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					orgVO.setOrgID(decrptVO.getOrgID());	
					
					
				   userList=userService.getAppwiseUserList(orgVO.getOrgID(), orgVO.getAppID());
				   userVO.setObjectList(userList);
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in  getAppwiseUserList: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity UserVO getAppwiseUserList(@RequestBody UserVO orgVO )")	;
			return new ResponseEntity(userVO, HttpStatus.OK);
		}
		
		
		@RequestMapping(value="/roleList", method = RequestMethod.POST)
		public ResponseEntity getRoleList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody UserVO orgVO ){
			 List roleList=null;
			UserVO userVO=new UserVO();
			 APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity UserVO getRoleList(@RequestBody UserVO orgVO )")	;
			try {
				logger.info(" orgID: "+orgVO.getOrgID()+" appID : "+ orgVO.getAppID());
				if(orgVO.getOrgID().length()>0 && orgVO.getAppID().length()>0){	
					
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					orgVO.setOrgID(decrptVO.getOrgID());	
					//orgVO.setAppID(decrptVO.getAppID());
					
					roleList=userService.getRoles(orgVO.getOrgID(), orgVO.getAppID());
				  							
					userVO.setObjectList(roleList);
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in  getRoleList: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity UserVO getRoleList(@RequestBody UserVO orgVO )")	;
			return new ResponseEntity(userVO, HttpStatus.OK);
		}
		
		
		@RequestMapping(value="/insert", method = RequestMethod.POST)
		public ResponseEntity createUser(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody UserVO requestVO ){
			UserVO userVO=new UserVO();
			 APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity createUser(@RequestBody UserVO requestVO )")	;
			try {
				logger.info("Login email ID :"+requestVO.getLoginName()+" orgID: "+requestVO.getOrgID()+" appID : "+ requestVO.getAppID());
				if(requestVO.getLoginName().length()>0 && requestVO.getOrgID().length()>0 && requestVO.getAppID().length()>0){	
					
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestVO.setOrgID(decrptVO.getOrgID());	
					//requestVO.setAppID(decrptVO.getAppID());
					
					userVO=userService.createUser(requestVO);
					apiResponse.setUserID(userVO.getUserID());
					   if(userVO.getStatusCode()==200){									
						   apiResponse.setStatusCode(200);
						   apiResponse.setMessage("User has been created successfully.");
						}else if(userVO.getStatusCode()==532){
							apiResponse.setStatusCode(userVO.getStatusCode());
							apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
							return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);							
						}else{
							apiResponse.setStatusCode(userVO.getStatusCode());
							apiResponse.setMessage("Unable to create new user.");
							return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);							
						}
					
				} else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
					
				}
			} catch (Exception e) {
				logger.error("Exception in  createUser: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity createUser(@RequestBody UserVO requestVO )")	;
			return new ResponseEntity(apiResponse, HttpStatus.OK);
		}
		
		@RequestMapping(value="/update", method = RequestMethod.POST)
		public ResponseEntity updateUser(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody UserVO requestVO ){
			UserVO userVO=new UserVO();
			 APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity updateUser(@RequestBody UserVO requestVO )")	;
			try {
				logger.info("User ID: "+requestVO.getUserID()+" orgID: "+requestVO.getOrgID()+" appID : "+ requestVO.getAppID());
				if(requestVO.getUserID().length()>0 && requestVO.getOrgID().length()>0 && requestVO.getAppID().length()>0){	
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestVO.setOrgID(decrptVO.getOrgID());	
					//requestVO.setAppID(decrptVO.getAppID());
					
					Boolean isUpdated=userService.updateUser(requestVO);
					   if(isUpdated){									
						   apiResponse.setStatusCode(200);
						   apiResponse.setMessage("User has been updated successfully.");
						}else{
							apiResponse.setStatusCode(409);
							apiResponse.setMessage("Unable to update user details");
							return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);							
						}
					
				} else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
					
				}
			} catch (Exception e) {
				logger.error("Exception in  updateUser: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity updateUser(@RequestBody UserVO requestVO )")	;
			return new ResponseEntity(apiResponse, HttpStatus.OK);
		}
		
		@RequestMapping(value="/getUserDetails", method = RequestMethod.POST)
		public ResponseEntity getUserDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody UserVO requestVO ){
			UserVO userVO=new UserVO();
			 APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity getUserDetails(@RequestBody UserVO requestVO )")	;
			try {
				logger.info("User ID: "+requestVO.getUserID());
				if(requestVO.getUserID().length()>0 ){	
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestVO.setOrgID(decrptVO.getOrgID());	
					requestVO.setAppID(decrptVO.getAppID());
					
					   userVO = userService.getUser(requestVO);
					   
					   if(userVO.getUserID().length()==0){									
						   apiResponse.setStatusCode(409);
							apiResponse.setMessage("Unable to fetch user details");
							return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);		
						}
					
				} else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
					
				}
			} catch (Exception e) {
				logger.error("Exception in  getUserDetails: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getUserDetails(@RequestBody UserVO requestVO )")	;
			return new ResponseEntity(userVO, HttpStatus.OK);
		}
		
		@RequestMapping(value="/revokeOrgAccess", method = RequestMethod.POST)
		public ResponseEntity revokeOrgAccess(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody UserVO requestVO ){
			UserVO userVO=new UserVO();
			 APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity revokeOrgAccess(@RequestBody UserVO requestVO )")	;
			try {
				logger.info("User ID: "+requestVO.getUserID()+" orgID: "+requestVO.getOrgID()+" appID : "+ requestVO.getAppID());
				if(requestVO.getUserID().length()>0 && requestVO.getOrgID().length()>0 && requestVO.getAppID().length()>0){	
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestVO.setOrgID(decrptVO.getOrgID());	
					//requestVO.setAppID(decrptVO.getAppID());
					
					Boolean isSuccess=userService.revokeOrgAccess(requestVO.getUserID(),requestVO.getAppID(),requestVO.getOrgID() );
					   if(isSuccess){									
						   apiResponse.setStatusCode(200);
						   apiResponse.setMessage("User access has been revoked successfully.");
						}else{
							apiResponse.setStatusCode(406);
							apiResponse.setMessage("Unable to revoke access.");
							return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);							
						}
					
				} else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
					
				}
			} catch (Exception e) {
				logger.error("Exception in  revokeOrgAccess: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity revokeOrgAccess(@RequestBody UserVO requestVO )")	;
			return new ResponseEntity(apiResponse, HttpStatus.OK);
		}
		
		@RequestMapping(value="/permitOrgAccess", method = RequestMethod.POST)
		public ResponseEntity permitOrgAccess(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody UserVO requestVO ){
			UserVO userVO=new UserVO();
			 APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity permitOrgAccess(@RequestBody UserVO requestVO )")	;
			try {
				logger.info("Login name: "+requestVO.getLoginName()+" orgID: "+requestVO.getOrgID()+" appID : "+ requestVO.getAppID()+" Role ID: "+requestVO.getRoleID());
				if(requestVO.getLoginName().length()>0 && requestVO.getOrgID().length()>0 && requestVO.getAppID().length()>0 && requestVO.getRoleID().length()>0){	
					
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestVO.setOrgID(decrptVO.getOrgID());	
					//requestVO.setAppID(decrptVO.getAppID());
					
					 userVO=userService.permitOrgAccess(requestVO);
					   if(userVO.getStatusCode()==200){									
						   apiResponse.setStatusCode(200);
						   apiResponse.setMessage("User access has been permited successfully.");
						   
						}else if(userVO.getStatusCode()==532){
							apiResponse.setStatusCode(532);
							apiResponse.setMessage("User has already permited access of organization.");
							return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);							
						}else if(userVO.getStatusCode()==406){
							apiResponse.setStatusCode(406);
							apiResponse.setMessage("Unable to permit access.");
							return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);							
						}else{
							apiResponse.setStatusCode(userVO.getStatusCode());
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);	
							
						}
					
				} else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
					
				}
			} catch (Exception e) {
				logger.error("Exception in  permitOrgAccess: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity permitOrgAccess(@RequestBody UserVO requestVO )")	;
			return new ResponseEntity(apiResponse, HttpStatus.OK);
		}
		
		@RequestMapping(value="/setPassword", method = RequestMethod.POST)
		public ResponseEntity setUserPassword(@RequestBody UserVO requestVO ){
			UserVO userVO=new UserVO();
			 APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity setUserPassword(@RequestBody UserVO requestVO )")	;
			try {
				logger.info("Password :"+requestVO.getPassword()+" User ID: "+requestVO.getUserID()+" Updated by : "+ requestVO.getUpdatedBy());
				if(requestVO.getPassword().length()>0 && requestVO.getUpdatedBy().length()>0 && requestVO.getUserID().length()>0 ){	
					Boolean isSuccess=userService.changedPassword(requestVO.getUserID(), requestVO.getPassword(), requestVO.getUpdatedBy());
				
					   if(isSuccess){									
						   apiResponse.setStatusCode(200);
						   apiResponse.setMessage("User password has been updated successfully.");
						}else{
							apiResponse.setStatusCode(406);
							apiResponse.setMessage("Unable to update password.");
							return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);							
						}
					
				} else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
					
				}
			} catch (Exception e) {
				logger.error("Exception in  setUserPassword: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity setUserPassword(@RequestBody UserVO requestVO )")	;
			return new ResponseEntity(apiResponse, HttpStatus.OK);
		}
		
		@RequestMapping(value="/changePassword", method = RequestMethod.POST)
		public ResponseEntity changedUserPassword(@RequestBody UserVO requestVO ){
			UserVO userVO=new UserVO();
			 APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity changedUserPassword(@RequestBody UserVO requestVO )")	;
			try {
				logger.info("Password :"+requestVO.getPassword()+" User ID: "+requestVO.getUserID()+" Updated by : "+ requestVO.getUpdatedBy());
				if(requestVO.getPassword().length()>0 && requestVO.getUpdatedBy().length()>0 && requestVO.getUserID().length()>0 ){	
					Boolean isSuccess=userService.changedPassword(requestVO.getUserID(), requestVO.getPassword(), requestVO.getUpdatedBy());
				
					   if(isSuccess){									
						   apiResponse.setStatusCode(200);
						   apiResponse.setMessage("User password has been updated successfully.");
						}else{
							apiResponse.setStatusCode(406);
							apiResponse.setMessage("Unable to update password.");
							return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);							
						}
					
				} else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
					
				}
			} catch (Exception e) {
				logger.error("Exception in  changedUserPassword: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity changedUserPassword(@RequestBody UserVO requestVO )")	;
			return new ResponseEntity(apiResponse, HttpStatus.OK);
		}
		
		
		@RequestMapping(value="/permissionList", method = RequestMethod.POST)
		public ResponseEntity getAllPermissions(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody UserVO orgVO ){
			 List userList=null;
			UserVO userVO=new UserVO();
			 APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity UserVO getAllPermissions(@RequestBody UserVO orgVO )")	;
			try {
				logger.debug(" appID : "+ orgVO.getAppID());
				if(orgVO.getAppID().length()>0){	
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					//orgVO.setAppID(decrptVO.getAppID());
					
				   userList=userService.getAllPermissions(orgVO.getAppID());
				   userVO.setObjectList(userList);
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in  getAllPermissions: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity UserVO getAllPermissions(@RequestBody UserVO orgVO )")	;
			return new ResponseEntity(userVO, HttpStatus.OK);
		}
		
		@RequestMapping(value="/role/insert", method = RequestMethod.POST)
		public ResponseEntity createRole(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody UserVO requestVO ){
			UserVO userVO=new UserVO();
			 APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity createRole(@RequestBody UserVO requestVO )")	;
			try {
				logger.info("App ID :"+requestVO.getAppID()+" org ID: "+requestVO.getOrgID()+" Updated by : "+ requestVO.getUpdatedBy());
				if(requestVO.getAppID().length()>0 && requestVO.getUpdatedBy().length()>0 && requestVO.getOrgID().length()>0 && requestVO.getPermissionList().size()>0 ){	
				
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestVO.setOrgID(decrptVO.getOrgID());	
					//requestVO.setAppID(decrptVO.getAppID());
					
					Boolean isSuccess=userService.createRole(requestVO);
				
					   if(isSuccess){									
						   apiResponse.setStatusCode(200);
						   apiResponse.setMessage("New role has been created successfully.");
						}else{
							apiResponse.setStatusCode(406);
							apiResponse.setMessage("Unable to create role.");
							return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);							
						}
					
				} else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
					
				}
			} catch (Exception e) {
				logger.error("Exception in  createRole: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity createRole(@RequestBody UserVO requestVO )")	;
			return new ResponseEntity(apiResponse, HttpStatus.OK);
		}
		
		
		@RequestMapping(value="/role/permission", method = RequestMethod.POST)
		public ResponseEntity getUserRolePermission(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody UserVO orgVO ){
			 List roleList=null;
			UserVO userVO=new UserVO();
			 APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity UserVO getUserRolePermission(@RequestBody UserVO orgVO )")	;
			try {
				logger.info(" roleID: "+orgVO.getRoleID()+" orgID: "+orgVO.getOrgID()+" appID : "+ orgVO.getAppID());
				if(orgVO.getRoleID().length()>0 && orgVO.getOrgID().length()>0 && orgVO.getAppID().length()>0){	
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					orgVO.setOrgID(decrptVO.getOrgID());	
					//orgVO.setAppID(decrptVO.getAppID());
					
					userVO=userService.getUserRolePermission(orgVO.getRoleID(), orgVO.getAppID(), orgVO.getOrgID());
				     if(userVO.getStatusCode()!=200){
				    	 apiResponse.setStatusCode(406);
						 apiResponse.setMessage("Unable to fetch user role permission.");
						 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);							
				    	 
				     }
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in  getUserRolePermission: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity UserVO getUserRolePermission(@RequestBody UserVO orgVO )")	;
			return new ResponseEntity(userVO, HttpStatus.OK);
		}
		
		@RequestMapping(value="/role/update", method = RequestMethod.POST)
		public ResponseEntity updateRole(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody UserVO requestVO ){
			UserVO userVO=new UserVO();
			 APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity updateRole(@RequestBody UserVO requestVO )")	;
			try {
				logger.debug("Role ID: "+requestVO.getRoleID()+" App ID :"+requestVO.getAppID()+" org ID: "+requestVO.getOrgID()+" Updated by : "+ requestVO.getUpdatedBy());
				if(requestVO.getRoleID().length()>0 && requestVO.getAppID().length()>0 && requestVO.getUpdatedBy().length()>0 && requestVO.getOrgID().length()>0 && requestVO.getPermissionList().size()>0 ){	
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestVO.setOrgID(decrptVO.getOrgID());	
					requestVO.setAppID(decrptVO.getAppID());
					
					Boolean isSuccess=userService.updateRole(requestVO);
				
					   if(isSuccess){									
						   apiResponse.setStatusCode(200);
						   apiResponse.setMessage("Role has been updated successfully.");
						}else{
							apiResponse.setStatusCode(406);
							apiResponse.setMessage("Unable to update role.");
							return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);							
						}
					
				} else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
					
				}
			} catch (Exception e) {
				logger.error("Exception in  updateRole: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity updateRole(@RequestBody UserVO requestVO )")	;
			return new ResponseEntity(apiResponse, HttpStatus.OK);
		}
		
		@RequestMapping(value="/role/delete", method = RequestMethod.POST)
		public ResponseEntity deleteRole(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody UserVO requestVO ){
			UserVO deleteVO=new UserVO();
			 APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity deleteRole(@RequestBody UserVO requestVO )")	;
			try {
				logger.debug("Role ID: "+requestVO.getRoleID()+" App ID :"+requestVO.getAppID()+" Deleted by : "+ requestVO.getUpdatedBy());
				if(requestVO.getRoleID().length()>0 && requestVO.getAppID().length()>0 && requestVO.getUpdatedBy().length()>0  ){	
				
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestVO.setOrgID(decrptVO.getOrgID());	
					requestVO.setAppID(decrptVO.getAppID());
					
					deleteVO=userService.deleteRole(requestVO);
				
					   if(deleteVO.getStatusCode()==200){									
						   apiResponse.setStatusCode(200);
						   apiResponse.setMessage("Role has been deleted successfully.");
						}else if(deleteVO.getStatusCode()==533){
							apiResponse.setStatusCode(deleteVO.getStatusCode());
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);							
						}else{
							apiResponse.setStatusCode(406);
							apiResponse.setMessage("Unable to delete role.");
							return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);							
						}
					
				} else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
					
				}
			} catch (Exception e) {
				logger.error("Exception in  deleteRole: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity deleteRole(@RequestBody UserVO requestVO )")	;
			return new ResponseEntity(apiResponse, HttpStatus.OK);
		}
		
		
		// ================ Self Registration Process APIs ==========================//
		
		@RequestMapping(value="/signUpForTrialForRegdUser", method = RequestMethod.POST)
		public ResponseEntity signUpForTrialForRegisteredUser(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody UserVO userVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			
			logger.debug("Entry : public void signUpForTrialForRegisteredUser(@RequestBody UserVO userVO)  ");
			try {
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				userVO.setAppID(decrptVO.getAppID());
				
				if(userVO.getAppID().length()>0&&userVO.getAppID()!=null){
				userVO=userService.signUpForTrialForRegisteredUser(userVO);
				
				if(userVO.getStatusCode()==550){
					apiResponse.setStatusCode(userVO.getStatusCode());
					apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
					
				}else if(userVO.getStatusCode()==551){
					apiResponse.setStatusCode(userVO.getStatusCode());
					apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
					
				}else{
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Sign up for trial created. ");
				}
			       	          
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
			logger.debug("Exit : public void signUpForTrialForRegisteredUser(@RequestBody UserVO userVO)  ");						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		
		
		// ================ App Management APIs ==========================//	
		
		@RequestMapping(value="/organizationAssignAppList", method = RequestMethod.POST)
		public ResponseEntity getOrgAssignAppList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken, @RequestBody AppVO appVO ){
			String success=null;
			List appList=null;
			APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity UserVO getOrgAssignAppList(@RequestBody UserVO orgVO )")	;
			try {
				
				if(appVO.getOrgID()>0 ){
				 logger.debug("orgID: "+appVO.getOrgID());
				 UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
			
				 appVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
													
			    	appList=appDetailsService.getOrgAssignAppList(appVO.getOrgID());
			    	 
			    	if(appList.size()>0){
			    	   appVO.setOrgAppList(appList);
			    	}
			    	
				}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
					
				
			} catch (Exception e) {
				logger.error("Exception in  getOrgAssignAppList: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity UserVO getOrgAssignAppList(@RequestBody UserVO orgVO )")	;
			return new ResponseEntity(appVO, HttpStatus.OK);
		}
		
		/***********************Update user by external referenceID************/
		@RequestMapping(value="/updateByReferenceID", method = RequestMethod.POST)
		public ResponseEntity updateUserByRefernceID(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody UserVO requestVO ){
		UserVO userVO=new UserVO();
		APIResponseVO apiResponse=new APIResponseVO();
		logger.debug("Entry : public ResponseEntity updateUserByRefernceID(@RequestBody UserVO requestVO )") ;
		try {
		logger.info("Email: "+requestVO.getLoginName());
		if(requestVO.getLoginName().length()>0 ){
		UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
		requestVO.setOrgID(decrptVO.getOrgID());
		requestVO.setAppID(decrptVO.getAppID());

		  logger.info(" orgID: "+requestVO.getOrgID()+" appID : "+ requestVO.getAppID());
		 
		userVO = userService.getUserDetailsByLoginName(requestVO.getLoginName());
		if(userVO.getLoginSuccess()){
		requestVO.setUserID(userVO.getUserID());
		}else{
		 apiResponse.setStatusCode(404);
		 apiResponse.setMessage("No user details found");
		 return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
		}
		 

		Boolean isUpdated=userService.updateUser(requestVO);

		  if(isUpdated){
		  apiResponse.setStatusCode(200);
		  apiResponse.setMessage("User has been updated successfully.");
		  logger.info(" User has been updated successfully. ");
		}else{
		apiResponse.setStatusCode(409);
		apiResponse.setMessage("Unable to update user details of userID: " +requestVO.getUserID());
		logger.info("Unable to update user details of userID: ");
		return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
		}

		} else{
		apiResponse.setStatusCode(400);
		apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
		return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);

		}
		} catch (Exception e) {
		logger.error("Exception in  updateUserByRefernceID: "+e);
		apiResponse.setStatusCode(400);
		apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
		return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
		}
		logger.debug("Exit : public ResponseEntity updateUserByRefernceID(@RequestBody UserVO requestVO )") ;
		return new ResponseEntity(apiResponse, HttpStatus.OK);
		}
		
}
