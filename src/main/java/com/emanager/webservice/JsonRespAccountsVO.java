package com.emanager.webservice;

import java.util.List;

import com.emanager.server.accounts.DAO.TransactionDAO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.society.valueObject.SocietyVO;



public class JsonRespAccountsVO {
	
	
	private List objectList;	
	private int statusCode;
	private int societyID;
	private String aptID;
	private String memberName;
	private String memberID;
	private String ledgerID;
	private String fromDate;
	private String uptoDate;
	private SocietyVO societyVO;
	private List <TransactionVO>txList;
	
	public String getAptID() {
		return aptID;
	}
	public void setAptID(String aptID) {
		this.aptID = aptID;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getLedgerID() {
		return ledgerID;
	}
	public void setLedgerID(String ledgerID) {
		this.ledgerID = ledgerID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public List getObjectList() {
		return objectList;
	}
	public void setObjectList(List objectList) {
		this.objectList = objectList;
	}
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getUptoDate() {
		return uptoDate;
	}
	public void setUptoDate(String uptoDate) {
		this.uptoDate = uptoDate;
	}
	public SocietyVO getSocietyVO() {
		return societyVO;
	}
	public void setSocietyVO(SocietyVO societyVO) {
		this.societyVO = societyVO;
	}
	public List<TransactionVO> getTxList() {
		return txList;
	}
	public void setTxList(List<TransactionVO> txList) {
		this.txList = txList;
	}
	
	
	
	
}
