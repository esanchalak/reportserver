package com.emanager.webservice.filters;

import org.apache.catalina.connector.ClientAbortException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.userManagement.service.UserService;
import com.emanager.server.userManagement.valueObject.UserVO;
import com.emanager.webservice.APIResponseVO;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;


@Component
@Order(2)
public class AuthenticationFilter implements Filter {
	
	private static final Logger logger = Logger.getLogger(AuthenticationFilter.class);
	public final String HEADER_SECURITY_TOKEN = "X-AuthToken";
	public final String HEADER_ApartmentID= "X-ApartmentID";
	
	@Autowired
	UserService userService;	
	@Autowired
	MemberAppFilter memberAppFilter;
	
	public EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
	public ConfigManager configManager =new ConfigManager();
	
	
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
      
    }
	

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    	logger.debug("###### Authentication Filter #################");
      
       // request.
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        AntPathMatcher urlMatch = new AntPathMatcher();
        APIResponseVO apiResponse=new APIResponseVO();
        try { 
        	
	        if (urlMatch.match("/secure/**", httpRequest.getRequestURI().substring(httpRequest.getContextPath().length()))) {
	        	 logger.debug("###### Token Filter #################");
	        	 String token = httpRequest.getHeader(HEADER_SECURITY_TOKEN);
	        	 logger.debug("Token found");
	        	 if(token==null){
	        		 logger.info("Token is blank");
	        		 apiResponse.setStatusCode(525);
	        		 apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
	        		 httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, apiResponse.toString());
	        	 }else{
	        		 
	        		Boolean isAccessTokenValid = userService.verifyAccessToken(token);
	        		
	        		if(isAccessTokenValid == false){
	        			UserVO refreshVO =new UserVO();
	        			refreshVO=enDeUtility.decodeStringToBean(token);	
	        			logger.debug(" Refresh token: "+refreshVO.getRefeshTokenValidity());
	        			Boolean isRefreshTokenValid= userService.verifyRefreshToken(refreshVO);
	        			logger.info("User ID: "+refreshVO.getUserID()+" Access Token is invalid so check refresh token validity");
	        			if(isRefreshTokenValid){	        				
	        				//match access token
	        				logger.info("User ID: "+refreshVO.getUserID()+ " Refresh token is valid so generate new acces token ");
	        				String newAccesstoken= userService.verifyGenerateAccessToken(token);
	        				if(newAccesstoken.compareTo("535")==0){
	        					apiResponse.setStatusCode(535);
	       	        		    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
	        					httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, apiResponse.getMessage());
	        				}else if(newAccesstoken.compareTo("0")==0){
	        					apiResponse.setStatusCode(527);
	       	        		    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
	        					httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, apiResponse.getMessage());
	        				}else{
	    	        			logger.info("User ID: "+refreshVO.getUserID()+ " New access token generated successfully ");
	        				    httpResponse.setHeader(HEADER_SECURITY_TOKEN, newAccesstoken);
	        				    chain.doFilter(request, response);
	        				}
                           
	        			 }else{   			
	        				   logger.info("User ID: "+refreshVO.getUserID()+ " Refresh Token is invalid ");
	        				   apiResponse.setStatusCode(526);
	       	        		   apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
	        				   httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, apiResponse.getMessage());
	        			 }
	        			
	        			
	        		}else{
	        			//check permission 
	        			UserVO decrptVO=enDeUtility.decodeStringToBean(token);	
	        			httpResponse.setHeader(HEADER_SECURITY_TOKEN, token);
	        			if(!decrptVO.getRoleID().isEmpty()){
	        				
	        				Boolean permissionExists = userService.verifyPermission(decrptVO.getRoleID(), httpRequest.getRequestURI().substring(httpRequest.getContextPath().length()));
	        						        						
	        				 logger.debug(httpRequest.getRequestURI().substring(httpRequest.getContextPath().length())+" "+permissionExists);
	        			     if(permissionExists){
	        			    	
	        			    	 //Check Member App filter
	        			    	if(decrptVO.getAppID().equalsIgnoreCase("1")){
	        			    		
	        			    		String apartmentID = httpRequest.getHeader(HEADER_ApartmentID);
	        			    		if(apartmentID!=null){
	        			    			decrptVO.setApartmentID(Integer.parseInt(apartmentID));		
	        			    			
		        			    		UserVO memberAuthVO=memberAppFilter.validateMemberApp(decrptVO);
		        			    		logger.debug("apartmentID : "+apartmentID+" "+memberAuthVO.getLoginSuccess());
		        			    		if(memberAuthVO.getLoginSuccess()){
		        			    			chain.doFilter(request, response);		        			    			
		        			    		}else{
		        			    			apiResponse.setStatusCode(524);
		    		       	        	    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
		    	        			    	httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, apiResponse.getMessage());		        			    			
		        			    		}
		        			    		
	        			    		}else{        			    			
	        			    			apiResponse.setStatusCode(524);
	    		       	        	    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
	    	        			    	httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, apiResponse.getMessage());
	        			    		}
	        			    		
	        			    	}else if(decrptVO.getAppID().equalsIgnoreCase("5")){//customer
		        			    		
		        			    		String profileID = httpRequest.getHeader(HEADER_ApartmentID);
		        			    		if(profileID!=null){
		        			    			decrptVO.setProfileID(Integer.parseInt(profileID));		
		        			    			
			        			    		UserVO memberAuthVO=memberAppFilter.validateCustomerApp(decrptVO);
			        			    		logger.debug("profileID : "+profileID+" Validation: "+memberAuthVO.getLoginSuccess());
			        			    		if(memberAuthVO.getLoginSuccess()){
			        			    			chain.doFilter(request, response);		        			    			
			        			    		}else{
			        			    			apiResponse.setStatusCode(524);
			    		       	        	    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			    	        			    	httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, apiResponse.getMessage());		        			    			
			        			    		}
			        			    		
		        			    		}else{        			    			
		        			    			apiResponse.setStatusCode(524);
		    		       	        	    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
		    	        			    	httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, apiResponse.getMessage());
		        			    		}	
	        			    		
	        			    		
	        			    		
	        			        }else{	        			    		
	        			    		chain.doFilter(request, response);
	        			   	    }
	        			    		        			    	
	        			    	 
	        			    }else{
	        			    	apiResponse.setStatusCode(524);
		       	        	    apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
	        			    	httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, apiResponse.getMessage());
	        			    }
	        			    
	        			  
	        			    
	        			    
	        			
	        			}else{
	        				apiResponse.setStatusCode(524);
	       	        		apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
	        				httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, apiResponse.getMessage());
	        			}
	        			
	        			
	        			
	        		
	        		}
	        	  
	        		
	        		 
	        	 }
	             
	            
	        
	        }else{
	        	 	chain.doFilter(request, response);
	        	 	
	        }
        } catch (ClientAbortException e) {
        	logger.info("Exception occured at Authentication ClientAborted the request "+"User ID: "+enDeUtility.decodeStringToBean(httpRequest.getHeader(HEADER_SECURITY_TOKEN)).getUserID()+e);  
        	apiResponse.setStatusCode(400);
       		apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
        	httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, apiResponse.getMessage());
        } catch (IOException e) {
        	logger.error("Exception occured at Authentication IOException the request "+"User ID: "+enDeUtility.decodeStringToBean(httpRequest.getHeader(HEADER_SECURITY_TOKEN)).getUserID()+e);  
        	apiResponse.setStatusCode(400);
       		apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
        	httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, apiResponse.getMessage());
        } catch (IllegalStateException e) {
        	logger.error("Exception occured at Authentication IllegalStateException filter "+e);  
        	apiResponse.setStatusCode(400);
       		apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
        	httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, apiResponse.getMessage());
        } catch (Exception e) {
        	logger.error("Exception occured at Authentication filter "+e);
        	apiResponse.setStatusCode(400);
       		apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
        	httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, apiResponse.getMessage());
        }
           

    }

    @Override
    public void destroy() {

    }

	
}
