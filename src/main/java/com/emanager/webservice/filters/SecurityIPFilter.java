package com.emanager.webservice.filters;
import org.apache.log4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.security.web.util.matcher.IpAddressMatcher;
import org.springframework.stereotype.Component;

import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.webservice.JSONAccountsController;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/*
 * 
 * This class filters request based on IP
 * 
 * */

@Component
@Order(1)
public class SecurityIPFilter implements Filter{
	
	private static final Logger logger = Logger.getLogger(SecurityIPFilter.class);
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
      
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    	 logger.debug("###### Security IP Filter #################");
        ConfigManager c=new ConfigManager();
        Boolean flag =false;
       // request.
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        
      //  IpAddressMatcher matcher = new IpAddressMatcher(c.getPropertiesValue("ip.address"));
      //  String address = request.getRemoteAddr();
       
              
        try {
        	String maper=c.getPropertiesValue("ip.address");
            ArrayList aList= new ArrayList(Arrays.asList(maper.split(",")));
            String address = request.getRemoteAddr();
            logger.debug("Users IP Address = " +address+" & Application Allowed IP Address = "+c.getPropertiesValue("ip.address"));
                  
            if(aList.contains(request.getRemoteAddr())){
               flag=true;
             }
         //   Boolean flag= matcher.matches(request.getRemoteAddr());
         // condition send error response
        	
         if(flag==false){
        	 logger.info("Request not authorized : Users IP Address = " +address+" & Application Allowed IP Address = "+c.getPropertiesValue("ip.address"));
        	httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Request not authorized");
          }else{        	
        	  // else valid request move ahead.
            chain.doFilter(request, response);
          }
        } catch (IllegalStateException e) {
        	logger.info("Exception occured at IP Checker  IllegalStateException "+e);
        	httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Request not authorized");
        } catch (Exception e) {
        	logger.error("Exception occured at IP Checker "+e);
        	httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Request not authorized");
        }
        
     
        

    }

    @Override
    public void destroy() {

    }
}