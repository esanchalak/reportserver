package com.emanager.webservice.filters;

import org.apache.log4j.Logger;

import com.emanager.server.invoice.dataAccessObject.ProfileDetailsVO;
import com.emanager.server.userManagement.service.UserService;
import com.emanager.server.userManagement.valueObject.UserVO;


public class MemberAppFilter {
	
	private static final Logger logger = Logger.getLogger(MemberAppFilter.class);
	
	public UserService userService;
	
	  public UserVO validateMemberApp(UserVO userVO) {
		  UserVO memberAuthVO=new UserVO();	 
		  Boolean isPresent=false; 
		try {
			logger.debug("User ID: "+userVO.getUserID()+" Apartment ID: "+ userVO.getApartmentID()+" Type: M");
			isPresent=userService.verifyUserApartment(userVO.getUserID(), userVO.getApartmentID(),"M");
			
			memberAuthVO.setLoginSuccess(isPresent);
		} catch (Exception e) {
			isPresent=false;
			memberAuthVO.setLoginSuccess(isPresent);
			logger.error("Exception in validateMemberApp "+e);
		}        
		  	  
		return memberAuthVO;
	  	
	  	
	  }
	  
	  public UserVO validateCustomerApp(UserVO userVO) {
		  UserVO memberAuthVO=new UserVO();	 
		  Boolean isPresent=false; //V,C,E
		try {
			logger.debug("UserID: "+userVO.getUserID()+" ProfileID: "+ userVO.getProfileID());
		
			if(userVO.getProfileID()>0){
				isPresent=userService.verifyProfileDetails(userVO.getUserID(), userVO.getProfileID());
				
			}
			memberAuthVO.setLoginSuccess(isPresent);
			
		} catch (Exception e) {
			isPresent=false;
			memberAuthVO.setLoginSuccess(isPresent);
			logger.error("Exception in validateCustomerApp "+e);
		}        
		  	  
		return memberAuthVO;
	  	
	  	
	  }

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}


	


}
