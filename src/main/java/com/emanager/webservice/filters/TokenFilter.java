package com.emanager.webservice.filters;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.userManagement.service.UserService;
import com.emanager.server.userManagement.valueObject.UserVO;

public class TokenFilter {
	private static final Logger logger = Logger.getLogger(TokenFilter.class);
	
	public UserService userService;
	@Autowired
	MemberAppFilter memberAppFilter;
	public EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
	public ConfigManager configManager =new ConfigManager();
	
	
	  public UserVO validateTokenRequest(UserVO requestVO) {
		  UserVO tokenAuthVO=new UserVO(); 
		  UserVO checkUser=new UserVO(); 
		try {
			
			checkUser=enDeUtility.decodeStringToBean(requestVO.getAccessToken());	
			Boolean isAccessTokenValid = userService.verifyAccessToken(requestVO.getAccessToken());
			tokenAuthVO.setAccessToken(requestVO.getAccessToken());    
						
	        int tokenUserId= Integer.parseInt(checkUser.getUserID());
	        int tokenOrgId= Integer.parseInt(checkUser.getOrgID());
	        int reqUserId= Integer.parseInt(requestVO.getUserID());
	        int reqOrgId= Integer.parseInt(requestVO.getOrgID());
	        
			//authenticate userId and orgId
		    if((tokenUserId!=reqUserId) || (tokenOrgId!=reqOrgId)){	
		    	tokenAuthVO.setStatusCode(527);
				tokenAuthVO.setMessage(configManager.getPropertiesValue("error."+tokenAuthVO.getStatusCode()));    					
		        return tokenAuthVO;
		        
		    }else if(isAccessTokenValid == false){ //check validity of access token
    			
    			logger.debug(" Refresh token: "+checkUser.getRefeshTokenValidity());
    			Boolean isRefreshTokenValid= userService.verifyRefreshToken(checkUser);
    			logger.info("User ID: "+checkUser.getUserID()+" Access Token is invalid so check refresh token validity");
    			if(isRefreshTokenValid){	        				
    				//match access token
    				logger.info("User ID: "+checkUser.getUserID()+ " Refresh token is valid so generate new acces token ");
    				String newAccesstoken= userService.verifyGenerateAccessToken(requestVO.getAccessToken());
    				if(newAccesstoken.compareTo("535")==0){
    					tokenAuthVO.setStatusCode(535);
    					tokenAuthVO.setMessage(configManager.getPropertiesValue("error."+tokenAuthVO.getStatusCode()));    					
    				}else if(newAccesstoken.compareTo("0")==0){
    					tokenAuthVO.setStatusCode(527);
    					tokenAuthVO.setMessage(configManager.getPropertiesValue("error."+tokenAuthVO.getStatusCode()));
    				}else{
	        			logger.info("User ID: "+checkUser.getUserID()+ " New access token generated successfully ");
	        			tokenAuthVO.setStatusCode(200);
    					tokenAuthVO.setMessage("Access token is valid");
    					tokenAuthVO.setAccessToken(newAccesstoken);    				         					
    				}
                   
    			 }else{   		
    				tokenAuthVO.setStatusCode(526);
 					tokenAuthVO.setMessage(configManager.getPropertiesValue("error."+tokenAuthVO.getStatusCode()));
  	       		 }
    			
    			
    		}else{
                   UserVO permissionVO=this.validatePermission(requestVO); 	
                   tokenAuthVO.setStatusCode(permissionVO.getStatusCode());
				   tokenAuthVO.setMessage(permissionVO.getMessage());
    		}
		
		} catch (Exception e) {			
			tokenAuthVO.setStatusCode(400);
			tokenAuthVO.setMessage(configManager.getPropertiesValue("error."+tokenAuthVO.getStatusCode()));
			logger.error("Exception in validateTokenRequest "+e);
		}        
		  	  
		return tokenAuthVO;
	  	
	  	
	  }
	  
	  public UserVO validatePermission(UserVO requestVO) {
		  UserVO permissionVO=new UserVO(); 		 
		try {
			//check permission 
		
			UserVO decrptVO=enDeUtility.decodeStringToBean(requestVO.getAccessToken());	
		   
			if(!decrptVO.getRoleID().isEmpty()){
				
				Boolean permissionExists = userService.verifyPermission(decrptVO.getRoleID(), requestVO.getRequestURL());
				//decrptVO.getPermissionHashMap().containsKey(requestVO.getRequestURL());
				 logger.debug(requestVO.getRequestURL()+" "+permissionExists);
			     if(permissionExists){
			    	
			    	 //Check Member App filter
			    	if(decrptVO.getAppID().equalsIgnoreCase("1")){
			    		
			    		String apartmentID = String.valueOf(requestVO.getApartmentID());
			    		if(apartmentID!=null){
			    			decrptVO.setApartmentID(Integer.parseInt(apartmentID));		
			    			
    			    		UserVO memberAuthVO=memberAppFilter.validateMemberApp(decrptVO);
    			    		logger.debug("apartmentID : "+apartmentID+" "+memberAuthVO.getLoginSuccess());
    			    		if(memberAuthVO.getLoginSuccess()){
    			    			permissionVO.setStatusCode(200);
    			    			permissionVO.setMessage("Permission is valid");       		        			    			
    			    		}else{
    			    			permissionVO.setStatusCode(524);
    			    			permissionVO.setMessage(configManager.getPropertiesValue("error."+permissionVO.getStatusCode()));
    			    		}
    			    		
			    		}else{        			    			
			    			permissionVO.setStatusCode(524);
			    			permissionVO.setMessage(configManager.getPropertiesValue("error."+permissionVO.getStatusCode()));
			    		}
			    		
			    	}else if(decrptVO.getAppID().equalsIgnoreCase("5")){//customer
			    		
			    		String profileID = String.valueOf(requestVO.getApartmentID());
			    		if(profileID!=null){
			    			decrptVO.setProfileID(Integer.parseInt(profileID));		
			    			
    			    		UserVO memberAuthVO=memberAppFilter.validateCustomerApp(decrptVO);
    			    		logger.debug("profileID : "+profileID+" Validation: "+memberAuthVO.getLoginSuccess());
    			    		if(memberAuthVO.getLoginSuccess()){
    			    			permissionVO.setStatusCode(200);
    			    			permissionVO.setMessage("Permission is valid");    	        			    			
    			    		}else{
    			    			permissionVO.setStatusCode(524);
    			    			permissionVO.setMessage(configManager.getPropertiesValue("error."+permissionVO.getStatusCode()));
	        			    		        			    			
    			    		}
    			    		
			    		}else{        			    			
			    			permissionVO.setStatusCode(524);
			    			permissionVO.setMessage(configManager.getPropertiesValue("error."+permissionVO.getStatusCode()));
        			    	
			    		}	
		    			
			    		
			    		
			        }else{	        			    		
			        	permissionVO.setStatusCode(200);
		    			permissionVO.setMessage("Permission is valid");    
			   	    }
			    		        			    	
			    	 
			    }else{
			    	permissionVO.setStatusCode(524);
	    			permissionVO.setMessage(configManager.getPropertiesValue("error."+permissionVO.getStatusCode()));
			    }
			    
			}else{
				permissionVO.setStatusCode(524);
    			permissionVO.setMessage(configManager.getPropertiesValue("error."+permissionVO.getStatusCode()));
        	 	
            }
			
		} catch (Exception e) {			
			permissionVO.setStatusCode(400);
			permissionVO.setMessage(configManager.getPropertiesValue("error."+permissionVO.getStatusCode()));
			logger.error("Exception in validatePermission "+e);
		}        
		  	  
		return permissionVO;
		}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	  
}
