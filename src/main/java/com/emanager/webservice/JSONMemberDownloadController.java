package com.emanager.webservice;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.Key;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
//import org.codehaus.jackson.annotate.JsonBackReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.LedgerEntries;
import com.emanager.server.accounts.DataAccessObjects.LedgerTrnsVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.adminReports.Services.NominationRegService;
import com.emanager.server.adminReports.Services.PrintReportService;
import com.emanager.server.adminReports.Services.ShareRegService;
import com.emanager.server.adminReports.valueObject.PrintReportVO;
import com.emanager.server.adminReports.valueObject.RptVehicleVO;
import com.emanager.server.commonUtils.services.AddressService;
import com.emanager.server.commonUtils.valueObject.DropDownVO;
import com.emanager.server.dashBoard.Services.AccountDashBoardService;
import com.emanager.server.dashBoard.Services.DashBoardService;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardAccountVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardVO;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.financialReports.valueObject.RptTransactionVO;
import com.emanager.server.invoice.Services.InvoiceService;
import com.emanager.server.invoice.dataAccessObject.InvoiceDetailsVO;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.MemberWSVO;
import com.emanager.server.society.valueObject.RenterVO;
import com.emanager.server.userLogin.services.LoginService;
import com.emanager.server.userLogin.valueObject.LoginVO;
import com.emanager.server.userManagement.valueObject.UserVO;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;

@Controller
@RequestMapping("/secure/download/member/")
public class JSONMemberDownloadController {
	
	private static final Logger logger = Logger.getLogger(JSONMemberDownloadController.class);
	@Autowired
	MemberService memberServiceBean;
  
    @Autowired
    PrintReportService printReportService;
    
    @Autowired
    ReportService reportService;
    
    public ConfigManager configManager =new ConfigManager();
    public EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
 	public final String HEADER_SECURITY_TOKEN = "X-AuthToken"; 
 
	
	/*======================Accounts Related Downloads =============================*/
	

	@RequestMapping(value="/invoice", method = RequestMethod.POST)
	public ResponseEntity downloadInvoice(@RequestBody PrintReportVO printVO) {
		InvoiceDetailsVO printInvoiceForm=new InvoiceDetailsVO();     
		APIResponseVO apiResponse=new APIResponseVO();
		
		logger.debug("Entry : public @ResponseBody ReportVO downloadInvoice(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String invoiceID, @PathVariable String exportType, @PathVariable String reportID) ")	;
		try {
			       	  
			
	              printInvoiceForm.setPrintParam(printVO.getPrintParam());
		      	  
	            printInvoiceForm.setReportContentType(printVO.getReportContentType());
				printInvoiceForm.setReportID("template."+printVO.getReportID());
				printInvoiceForm.setSourceType("BEAN");
				printVO=printReportService.printInvoice(printInvoiceForm);
						
			
		} catch (Exception e) {
			logger.error("Exception  "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
		}
		logger.debug("Exit : public @ResponseBody PrintReportVO downloadInvoice(@PathVariable String societyID,@PathVariable String aptID,@PathVariable String invoiceID, @PathVariable String exportType, @PathVariable String reportID) ");						
		return new ResponseEntity(printVO, HttpStatus.OK);
 
	}
		
	 /*Sample body contents for PrintReportVO */
	/*{
		"printParam":{
		"orgID":"11",
		"fromDate":"2016-04-01",
		"toDate":"2016-06-30",
		"dueDate":"2016-06-30",
		"billingCycleID":"5",
		"aptID":"5"
		},
		"reportContentType":"pdf"
		
		}*/
	@RequestMapping(value="/bill", method = RequestMethod.POST)
	public void downloadBill(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
		TransactionVO printTxVO=new TransactionVO();
		APIResponseVO apiResponse=new APIResponseVO();
		HashMap<String, Object> param= printVO.getPrintParam();
		logger.debug("Entry : public @ResponseBody PrintReportVO downloadBill(@RequestBody TransactionVO printVO) ");
		try {
			UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
			param.put("orgID", decrptVO.getOrgID());
				printTxVO.setPrintParam(param);
			    
				printTxVO.setFromDate((String) param.get("fromDate"));
				printTxVO.setToDate((String) param.get("toDate"));
				printTxVO.setDueDate((String) param.get("dueDate"));
				
				printTxVO.setSourceType("BEAN");
				printTxVO.setReportContentType(printVO.getReportContentType());														
				//printTxVO.setReportID("template.bill");				
				
				printVO=printReportService.printBill(printTxVO);
				printVO.setBeanList(null);			
				
				FileInputStream myStream = new FileInputStream(printVO.getReportURL());

				// Set the content type and attachment header.
				response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
				response.addHeader("x-filename",printVO.getReportID());
				response.setContentType("application/pdf");
				
				// Copy the stream to the response's output stream.
				IOUtils.copy(myStream, response.getOutputStream());				

				response.flushBuffer();
			
		} catch (IOException e) {
			logger.error("Exception in downloadBill: "+e);
			
		}
		logger.debug("Exit : public @ResponseBody PrintReportVO downloadBill(@RequestBody TransactionVO printVO) ");						
		
 
	}
	
		
	 /*Sample body contents for PrintReportVO */
	/*{
		"printParam":{
		"orgID":"11",
		"receiptID":"44",
		"ledgerID":"44",
		
		},
		"reportContentType":"pdf"
		
		}*/
	@RequestMapping(value="/receipt", method = RequestMethod.POST)
	public void downloadReceipt(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
		TransactionVO printTxVO=new TransactionVO();
		APIResponseVO apiResponse=new APIResponseVO();
		HashMap<String, Object> param= printVO.getPrintParam();
		logger.debug("Entry : public void downloadReceipt(@RequestBody TransactionVO printVO) ");
		try {
			UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
			param.put("orgID", decrptVO.getOrgID());
				printTxVO.setPrintParam(param);				
				
				printTxVO.setSourceType("BEAN");
				printTxVO.setReportContentType(printVO.getReportContentType());														
				//printTxVO.setReportID("template.receipt");				
				
				printVO=printReportService.printMemberReceipt(printTxVO);
				printVO.setBeanList(null);			
				
				FileInputStream myStream = new FileInputStream(printVO.getReportURL());

				// Set the content type and attachment header.
				response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
				response.addHeader("x-filename",printVO.getReportID());
				response.setContentType("application/pdf");
				
				// Copy the stream to the response's output stream.
				IOUtils.copy(myStream, response.getOutputStream());				

				response.flushBuffer();
			
		} catch (IOException e) {
			logger.error("Exception in downloadReceipt: "+e);
			
		}
		logger.debug("Exit : public void downloadReceipt(@RequestBody TransactionVO printVO) ");						
		
 
	}
	
	
	/*Sample body contents for PrintReportVO */
	/*{
		"printParam":{
		"orgID":"11",
		"ledgerID":"1",
		
		"fromDate":"2016-04-01",
		"toDate":"2016-06-30"
		
		},
		"reportContentType":"pdf"
		
		}*/
	@RequestMapping(value="/individualLedger", method = RequestMethod.POST)
	public void downloadIndividualLedger(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
		TransactionVO printTxVO=new TransactionVO();
		APIResponseVO apiResponse=new APIResponseVO();
		HashMap<String, Object> param= printVO.getPrintParam();
		logger.debug("Entry : public void downloadIndividualLedger(HttpServletResponse response,@RequestBody PrintReportVO printVO) ");
		try {
			 				 
			UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
			param.put("orgID", decrptVO.getOrgID());
			    printTxVO.setPrintParam(param);
			    
				printTxVO.setFromDate((String) param.get("fromDate"));
				printTxVO.setToDate((String) param.get("toDate"));
				printTxVO.setLedgerID(Integer.parseInt((String) param.get("ledgerID")));
				
				printTxVO.setReportContentType(printVO.getReportContentType());
				//printTxVO.setReportID("template.memberLedger");					
				printTxVO.setSourceType("BEAN");
				printVO=printReportService.printIndividualLedgerReport(printTxVO);
			    printVO.setBeanList(null);
			    
			    FileInputStream myStream = new FileInputStream(printVO.getReportURL());

				// Set the content type and attachment header.
				response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
				response.addHeader("x-filename",printVO.getReportID());
				response.setContentType("application/pdf");
				
				// Copy the stream to the response's output stream.
				IOUtils.copy(myStream, response.getOutputStream());				

				response.flushBuffer();
			
		} catch (IOException e) {
			logger.error("Exception in downloadReceipt: "+e);
			
		}
		logger.debug("Exit : public void downloadIndividualLedger (HttpServletResponse response,@RequestBody PrintReportVO printVO) ");						
		 
	}
	
	
	/*Sample body contents for PrintReportVO */
	/*{
		"printParam":{
		"society_id":"11"
		
		},
		"reportContentType":"pdf"
		
		}*/
 
	@RequestMapping(value="/ledgerBalance", method = RequestMethod.POST)
	public ResponseEntity downloadLedgerBalance(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
		RptTransactionVO printTxVO=new RptTransactionVO();
		APIResponseVO apiResponse=new APIResponseVO();
		HashMap<String, Object> param= printVO.getPrintParam();
		logger.debug("Entry : public @ResponseBody PrintReportVO downloadLedgerBalance(@PathVariable String societyID, @PathVariable String exportType)  ");
		try {
			DateUtility dateUtility=new DateUtility();
			java.sql.Date curdate=dateUtility.findCurrentDate();
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			String currentDate=sdf.format(curdate);
			
			UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
			param.put("orgID", decrptVO.getOrgID());  	
			    printTxVO.setPrintParam(param);
			    
			    printTxVO.setBuildingId("0");
			    printTxVO.setDisplayDate(currentDate);
			   
				printTxVO.setReportContentType(printVO.getReportContentType());
				//printTxVO.setReportID("template.ledgerDueReport");					
				printTxVO.setSourceType("BEAN");
				printVO=printReportService.printLedgerDueReport(printTxVO);
			    printVO.setBeanList(null);
			
		} catch (Exception e) {
			logger.error("Exception downloadLedgerBalance "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
		}
		logger.debug("Exit : public @ResponseBody PrintReportVO downloadLedgerBalance(@PathVariable String societyID, @PathVariable String exportType)  ");						
		return new ResponseEntity(printVO, HttpStatus.OK);
 
	}
	
	/*Sample body contents for PrintReportVO */
	/*{
		"printParam":{
		"society_id":"11",
		"fromDate":"2016-04-01",
		"toDate":"2016-06-30"			
		},
		"reportContentType":"pdf"
		
		}*/
 
	
	@RequestMapping(value="/incomeExpense", method = RequestMethod.POST)
	public ResponseEntity downloadIncomeExpenseReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken ,@RequestBody PrintReportVO printVO) {
		
		ReportVO printTxVO=new ReportVO();
		APIResponseVO apiResponse=new APIResponseVO();
		HashMap<String, Object> param= printVO.getPrintParam();
		 List collection = new ArrayList();
		logger.debug("Entry : public @ResponseBody PrintReportVO downloadIncomeExpense(@PathVariable String societyID, @PathVariable String fromDate, @PathVariable String toDate, @PathVariable String exportType)  "+param.get("society_id"));
		try {
			DateUtility dateUtility=new DateUtility();
			java.sql.Date curdate=dateUtility.findCurrentDate();
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			String currentDate=sdf.format(curdate);
			
			UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
			param.put("orgID", decrptVO.getOrgID());
			
			int societyID=Integer.parseInt((String) param.get("orgID"));
			String fromDate=(String) param.get("fromDate");
			String toDate=(String) param.get("toDate");
			printTxVO = reportService.getIncomeExpenseReport(societyID,fromDate,toDate,0,"S");
			
			  				
			  	printVO.setPrintParam(param);	    				   
			  	collection.add(printTxVO);
			  	printVO.setBeanList(collection);
			  	printVO.setReportContentType(printVO.getReportContentType());
			 // 	printVO.setReportID("template.incExpReport");					
			  	printVO.setSourceType("BEAN");					
				
			  	printVO=printReportService.printReport(printVO);
			  	printVO.setBeanList(null);
			
		} catch (Exception e) {
			logger.error("Exception downloadIncomeExpenseReport "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
		}
		logger.debug("Exit : public @ResponseBody PrintReportVO downloadLedgerBalance(@PathVariable String societyID, @PathVariable String exportType)  ");						
		return new ResponseEntity(printVO, HttpStatus.OK);
 
	}
	

	

	/*Sample body contents for PrintReportVO */
	/*{
		"printParam":{
	"society_id":"11",
	"fromDate":"2016-04-01",
	"toDate":"2016-06-30",
	"billing_cycle_id":"0"		
		},
		"reportContentType":"pdf",
		
		
		}*/

	@RequestMapping(value="/societyBills", method = RequestMethod.POST)
	public ResponseEntity downloadSocietyBills(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
		TransactionVO printTxForm=new TransactionVO();     
		APIResponseVO apiResponse=new APIResponseVO();
		HashMap<String, Object> param= printVO.getPrintParam();
		logger.debug("Entry : public @ResponseBody downloadSocietyBills(@RequestBody PrintReportVO printVO) ")	;
		try {
			       	  
			UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
			param.put("orgID", decrptVO.getOrgID());
			
			printTxForm.setPrintParam(param);
			printTxForm.setFromDate((String) param.get("fromDate"));
			printTxForm.setToDate((String) param.get("toDate"));
			printTxForm.setReportContentType(printVO.getReportContentType());
			//printTxForm.setReportID("template.societyBills");
			printTxForm.setSourceType("BEAN");
			
				printVO=printReportService.printAllMemberBillReport(printTxForm);
						
			
		} catch (Exception e) {
			logger.error("Exception downloadSocietyBills(@RequestBody PrintReportVO printVO) "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
		}
		logger.debug("Exit : public @ResponseBody PrintReportVO downloadSocietyBills(@RequestBody PrintReportVO printVO) ");						
		return new ResponseEntity(printVO, HttpStatus.OK);
 
	}

}