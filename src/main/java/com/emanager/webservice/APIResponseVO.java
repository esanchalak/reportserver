package com.emanager.webservice;

import com.fasterxml.jackson.annotation.JsonInclude;

//APIResponseVO
@JsonInclude(JsonInclude.Include.NON_NULL)
public class APIResponseVO {
	
	
	private int statusCode;
	private String message;
	private String userID;
	
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	
	

}
