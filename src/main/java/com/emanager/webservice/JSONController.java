package com.emanager.webservice;

import java.math.BigDecimal;
import java.security.Key;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.LedgerEntries;
import com.emanager.server.accounts.DataAccessObjects.LedgerTrnsVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.adminReports.Services.NominationRegService;
import com.emanager.server.adminReports.Services.PrintReportService;
import com.emanager.server.adminReports.Services.ShareRegService;
import com.emanager.server.adminReports.valueObject.PrintReportVO;
import com.emanager.server.adminReports.valueObject.RptVehicleVO;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.commonUtils.services.AddressService;
import com.emanager.server.commonUtils.valueObject.DropDownVO;
import com.emanager.server.dashBoard.Services.AccountDashBoardService;
import com.emanager.server.dashBoard.Services.DashBoardService;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardAccountVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardVO;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.financialReports.valueObject.RptTransactionVO;
import com.emanager.server.invoice.Services.InvoiceService;
import com.emanager.server.invoice.dataAccessObject.InvoiceDetailsVO;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.MemberWSVO;
import com.emanager.server.society.valueObject.RenterVO;
import com.emanager.server.userLogin.services.LoginService;
import com.emanager.server.userLogin.valueObject.LoginVO;
	
	
	//@Controller
	//@RequestMapping("/app")
	public class JSONController {
		
		private static final Logger logger = Logger.getLogger(JSONController.class);
		@Autowired
		MemberService memberServiceBean;
	    @Autowired
		LoginService loginService;
	    @Autowired
	    ReportService reportService;
	    @Autowired
	    TransactionService transactionService;
	    @Autowired
	    TransactionService txService;
	    @Autowired
	    InvoiceService invoiceService;
	    @Autowired
	    PrintReportService printReportService;
	    @Autowired
	    AddressService addressServiceBean;
	    @Autowired
	    DashBoardService dashBoardService;
	    @Autowired
	    AccountDashBoardService accountDashBoardService;
	    @Autowired
	    SocietyService societyService;
	    @Autowired
	    LedgerService ledgerService;
	    @Autowired
	    ShareRegService shareRegService;
	    @Autowired
	    NominationRegService nominationRegService;
	   	    
	   
	    
		@RequestMapping(value="/members/{name}", method = RequestMethod.GET)
		public @ResponseBody List getMembersInJson(@PathVariable String name) {
	 			List<MemberVO> memberlist = null; 
				logger.debug("Entry : public @ResponseBody List getShopInJSON(@PathVariable String name)")	;
			try {
				memberlist = memberServiceBean.getMemberListForMobileApp(name);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody List getShopInJSON(@PathVariable String name)")	;						
			return memberlist;
	 
		}
		
		@RequestMapping(value="/authenticate/{username},{password}", method = RequestMethod.GET)
		public @ResponseBody LoginVO getLogin(@PathVariable String username,@PathVariable String password ){
			String success=null;
			LoginVO loginVO=new LoginVO();
			EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
			logger.debug("Entry : public @ResponseBody LoginVO getLogin(@PathVariable String username,@PathVariable String password )")	;
			try {
				logger.info("Here the email "+username.length()+" "+username);
				loginVO.setLoginUserId(enDeUtility.encrypt(username));
				loginVO.setLoginUserPassword(enDeUtility.encrypt(password));
				
				
				loginVO=loginService.checkLogin(loginVO);
				if(loginVO.getLoginSuccess()){
					success=loginVO.getLoginUserFullName();
				}else
					success="Wrong";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody LoginVO getLogin(@PathVariable String username,@PathVariable String password )")	;
			return loginVO;
		}
		
		
		@RequestMapping(value="/buildingList/{societyID}", method = RequestMethod.GET)
		public @ResponseBody List getBuildingDetails(@PathVariable int societyID){
			List buildingList=null;
			logger.debug("Entry : public @ResponseBody List getBuildingDetails(@PathVariable String societyID)")	;
			try {
				buildingList=memberServiceBean.getBuildingList(societyID,"");
				 DropDownVO drpDwn = new DropDownVO();
				 drpDwn.setData("0");
				 drpDwn.setLabel("All");
				 buildingList.add(0, drpDwn);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			
			logger.debug("Exit : public @ResponseBody List getBuildingDetails(@PathVariable String societyID)")	;
			return buildingList;
		}
		
		@RequestMapping(value="/buildingWiseMembers/{buildingID},{societyID}", method = RequestMethod.GET)
		public @ResponseBody List getMemberListBuildingWise(@PathVariable int buildingID,@PathVariable int societyID){
			List buildingList=null;
			logger.debug("Entry :public @ResponseBody List getMemberListBuildingWise(@PathVariable String buildingID)")	;
			try {
				buildingList=memberServiceBean.getMemberListBuildingWise(buildingID,societyID);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			
			logger.debug("Exit : public @ResponseBody List getMemberListBuildingWise(@PathVariable String buildingID,@PathVariable String societyID)")	;
			return buildingList;
		}
		
		@RequestMapping(value="/buildingWiseForApt/{aptID}", method = RequestMethod.GET)
		public @ResponseBody List getMemberListOfApartmentsBuilding(@PathVariable int aptID){
			List memberList=null;
			logger.debug("Entry :public @ResponseBody List getMemberListOfApartmentsBuilding(@PathVariable String aptID)")	;
			try {
				memberList=memberServiceBean.getMemberListOfApartmentsBuilding(aptID);
				memberList=memberServiceBean.convertMemberListForWebservice(memberList);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			
			logger.debug("Exit :public @ResponseBody List getMemberListOfApartmentsBuilding(@PathVariable String aptID)")	;
			return memberList;
		}
		
		@RequestMapping(value="/memberInfo/{emailID:.*}", method = RequestMethod.GET)
		public @ResponseBody MemberWSVO getMemberInfo(@PathVariable String emailID){
			MemberWSVO memberVO=new MemberWSVO();
			logger.debug("Entry :public @ResponseBody MemberWSVO getMemberInfo(@PathVariable String emailID)"+emailID)	;
			try {
				memberVO=memberServiceBean.getMembersInfoWithMail(emailID);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			
			logger.debug("Exit :public @ResponseBody MemberWSVO getMemberInfo(@PathVariable String emailID)")	;
			return memberVO;
		}
		
		/*@RequestMapping(value="/memberDetails/{memberID},{societyID}", method = RequestMethod.GET)
		public @ResponseBody List getMemberDetails(@PathVariable String memberID, @PathVariable String societyID){
			MemberVO memberVO=new MemberVO();
			List MemberList=new java.util.ArrayList();
			logger.debug("Entry : public @ResponseBody MemberVO getMemberDetails(@PathVariable String memberID, @PathVariable String societyID)");
			try {
				RptTransactionVO rptVO=new RptTransactionVO();
				
				System.out.println("here is the member details"+memberID+" "+societyID);
				
				memberVO=memberServiceBean.getMemberInfo(memberID);
				
				
				rptVO=reportService.getLastTrasaction(memberID, societyID);
				
				if((societyID.equalsIgnoreCase("15"))||(societyID.equalsIgnoreCase("11"))){
					
					memberVO.setDueForMMC("Rs."+rptVO.getDueForMMC());
					memberVO.setDueForSF("Rs."+rptVO.getDueForSF());
					memberVO.setDueForRM("Rs."+rptVO.getDueForRM());
					memberVO.setDueForOther("Rs."+rptVO.getDueForOther());
					memberVO.setLateFeeForMMC("Rs."+rptVO.getLateFeeForMMC());
					memberVO.setLateFeeForSF("Rs."+rptVO.getLateFeeForSF());
					memberVO.setMmcPaidUpto(rptVO.getMmcPaidUptodate());
					memberVO.setSfPaidUpto(rptVO.getSFPaidUptodate());
					memberVO.setMmcDue("Rs."+rptVO.getDue());
					memberVO.setRentFess("Rs."+rptVO.getRentalFees());
					memberVO.setLateFees("Rs."+rptVO.getLateFees());
					memberVO.setTotalDue("Rs."+rptVO.getActualTotal());
					memberVO.setArrears("Rs."+rptVO.getBalance());
					
				
				}else{
					SimpleDateFormat df=new SimpleDateFormat("MMM-yy");
					memberVO.setLastDate(df.format(rptVO.getDisplay_date()));
					memberVO.setSocietyMMC("Rs."+rptVO.getSocietyMonthlyCharges());
					if((rptVO.getCommentForDue().equalsIgnoreCase("0"))||(rptVO.getCommentForDue()==null)){
						//memberVO.setMmcDue("MMC : Rs :"+"0.00"+" \n\n"+"Tenant fees : Rs :"+"0.00"+" \n\n"+"Late Fees : Rs :"+"0.00"
					 	  //+" \n\n"+"Total : Rs :"+""+"0.00");
						memberVO.setMmcDue("Rs.0.00");
						memberVO.setRentFess("Rs.0.00");
						memberVO.setLateFees("Rs.0.00");
						memberVO.setTotalDue("Rs.0.00");
						memberVO.setArrears("Rs.0.00");
					}else{					
						memberVO.setDueForMMC("Rs."+rptVO.getDue());
						memberVO.setLateFeeForMMC("Rs. "+rptVO.getLateFees());
						memberVO.setDueForSF("Rs."+rptVO.getDueForSF());
						memberVO.setDueForRM("Rs."+rptVO.getDueForRM());
						memberVO.setDueForOther("Rs."+rptVO.getDueForOther());
						memberVO.setLateFeeForSF("Rs."+rptVO.getLateFeeForSF());
						memberVO.setMmcPaidUpto(df.format(rptVO.getDisplay_date()));
						memberVO.setSfPaidUpto(rptVO.getSFPaidUptodate());
						memberVO.setMmcDue("Rs."+rptVO.getDue());
						memberVO.setRentFess("Rs."+rptVO.getRentalFees());
						memberVO.setLateFees("Rs."+rptVO.getLateFees());
						memberVO.setTotalDue("Rs."+rptVO.getActualTotal());
						memberVO.setArrears("Rs."+rptVO.getBalance());
						
					}
				}
				MemberList.add(memberVO);
				
			} catch (NullPointerException e) {
				// TODO Auto-generated catch block
				logger.info("Exception : No records found for the member_id ="+memberID+" "+e);
			} catch(Exception e){
				logger.error("Exception  "+e);
			}
			
			logger.debug("Exit : public @ResponseBody MemberVO getMemberDetails(@PathVariable String memberID, @PathVariable String societyID)");
			return MemberList;
		}
		
		@RequestMapping(value="/individualLedger/{societyID},{memberID}", method = RequestMethod.GET)
		public @ResponseBody List getIndividualLedger(@PathVariable String societyID ,@PathVariable String memberID){
			List ledgerList=null;
			logger.debug("Entry : public @ResponseBody MemberVO getMemberDetails(@PathVariable String memberID, @PathVariable String societyID)");
			logger.debug("here is the call");
			try {
				ledgerList=transactionService.getFilteredLatest(000, societyID, "M", memberID);
				for(int i=0;ledgerList.size()>i;i++){
				
					TransactionVO transactionVO=(TransactionVO) ledgerList.get(i);
					transactionVO.setTablePrimaryID(transactionVO.getTxAmount()+"");
					
					
					
				}
			} catch (RuntimeException e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody MemberVO getMemberDetails(@PathVariable String memberID, @PathVariable String societyID)");
			return ledgerList;
		}
		*/
		
		@RequestMapping(value="/dueReport/{societyID},{buildingID}", method = RequestMethod.GET)
		public @ResponseBody List getmemberDues(@PathVariable int societyID ,@PathVariable String buildingID){
			List dueReportList=null;
			logger.debug("Entry : public @ResponseBody MemberVO getMemberDetails(@PathVariable String memberID, @PathVariable String societyID)");
			logger.debug("here is the call");
			
			try {
				Calendar nowCal = Calendar.getInstance();
				 int cYear = nowCal.get(Calendar.YEAR);
				 int month = nowCal.get(Calendar.MONTH);
				 month=month+1;
				
				String lastDay = month+"/"+cYear;
				System.out.println("here date is"+lastDay);
				dueReportList=reportService.getMemberDue(month, cYear,societyID, buildingID);
				
				logger.info("here defaulter list is "+dueReportList.size());
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			
			logger.debug("Exit : public @ResponseBody MemberVO getMemberDetails(@PathVariable String memberID, @PathVariable String societyID)");
			return dueReportList;
		}
		
		
		@RequestMapping(value="/expenseReport/{year},{societyID},{monthIndex},{period}", method = RequestMethod.GET)
		public @ResponseBody List getMonthlyExpenseRpt(@PathVariable int year ,@PathVariable int societyID,@PathVariable int monthIndex,@PathVariable int period){
			List dueReportList=null;
			logger.debug("Entry : public @ResponseBody List getMonthlyExpenseRpt(@PathVariable String month ,@PathVariable int year,@PathVariable int monthIndex,@PathVariable int societyID)");
			logger.debug("here is the call");
			
			try {
				
				//System.out.println("here date is"+lastDay);
				//dueReportList=reportService.monthlyIncomeExpenditureReportDebit( year, societyID, monthIndex,"0");
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			
			logger.debug("Exit : public @ResponseBody List getMonthlyExpenseRpt(@PathVariable String month ,@PathVariable int year,@PathVariable int monthIndex,@PathVariable int societyID)");
			return dueReportList;
		}
		
		
/*		@RequestMapping(value="/categoryReport/{year},{societyID},{categoryID},{monthIndex},{type},{period}", method = RequestMethod.GET)
		public @ResponseBody List getCategoryWiseMonthlyExpenseRpt(@PathVariable int year ,@PathVariable int societyID,@PathVariable int categoryID,@PathVariable int monthIndex,@PathVariable String type,@PathVariable String period){
			List dueReportList=null;
			logger.debug("Entry : public @ResponseBody List getMonthlyExpenseRpt(@PathVariable String month ,@PathVariable int year,@PathVariable int monthIndex,@PathVariable int societyID)");
			logger.debug("here is the call");
			
			try {
				
				//System.out.println("here date is"+lastDay);
				dueReportList=reportService.expenseCategorywiseDetails(year,societyID,categoryID,monthIndex,type,period);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			
			logger.debug("Exit : public @ResponseBody List getMonthlyExpenseRpt(@PathVariable String month ,@PathVariable int year,@PathVariable int monthIndex,@PathVariable int societyID)");
			return dueReportList;
		}*/
		
		
		@RequestMapping(value="/getPassword/{email},{password}", method = RequestMethod.GET)
		public @ResponseBody int getPassword(@PathVariable String email,@PathVariable String password ){
			int success=0;
			EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
			logger.debug("Entry : public @ResponseBody List getPassword(@PathVariable String email )");
			try {
				logger.info("Here the email "+email.length()+" "+email);
				
				
				
				success=loginService.getPassword(enDeUtility.encrypt(email));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody List getPassword(@PathVariable String email )");
			return success;
		}
		
		
		@RequestMapping(value="/demo/{name}", method = RequestMethod.GET)
		public @ResponseBody List getDemoJSON(@PathVariable String name) {
	 			List<MemberVO> memberlist = null; 
				logger.debug("Entry : public @ResponseBody List getShopInJSON(@PathVariable String name)")	;
			try {
				memberlist = memberServiceBean.getMemberListForMobileApp(name);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody List getShopInJSON(@PathVariable String name)")	;						
			return memberlist;
	 
		}
	 
		@RequestMapping(value="/tallyLedger/{societyID},{memberID},{fromDate},{uptoDate}", method = RequestMethod.GET)
		public @ResponseBody List getTransctionList(@PathVariable int societyID,@PathVariable int memberID, @PathVariable String fromDate,@PathVariable String uptoDate) {
	 			List<LedgerEntries> ledgerEntrylist = null; 
	 			AccountHeadVO achVo=new AccountHeadVO();
				logger.debug("Entry : public @ResponseBody List getShopInJSON(@PathVariable String name)")	;
			try {
				achVo=ledgerService.getLedgerID(memberID, societyID, "M","MEMBERID");
				
				ledgerEntrylist = txService.getTransctionList(societyID, achVo.getLedgerID(), fromDate, uptoDate,"ALL");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody List getShopInJSON(@PathVariable String name)")	;						
			return ledgerEntrylist;
	 
		}
	 
		@RequestMapping(value="/tallyBalance/{societyID},{memberID},{fromDate},{uptoDate}", method = RequestMethod.GET)
		public @ResponseBody AccountHeadVO getOpeningClosingBal(@PathVariable int societyID,@PathVariable int memberID, @PathVariable String fromDate,@PathVariable String uptoDate) {
	 			AccountHeadVO achVo=new AccountHeadVO();
				logger.debug("Entry : public @ResponseBody List getShopInJSON(@PathVariable String name)")	;
			try {
				achVo=ledgerService.getLedgerID(memberID, societyID, "M","MEMBERID");
				
							
				achVo = ledgerService.getOpeningClosingBalance(societyID, achVo.getLedgerID(), fromDate, uptoDate,"L");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody List getShopInJSON(@PathVariable String name)")	;						
			return achVo;
	 
		}
		
		
		@RequestMapping(value="/dueBalance/{societyID},{aptID}", method = RequestMethod.GET)
		public @ResponseBody InvoiceDetailsVO getInvoiceBalance(@PathVariable int societyID,@PathVariable int aptID) {
	 			InvoiceDetailsVO invoiceVO=new InvoiceDetailsVO();
	 	
			logger.debug("Entry : public @ResponseBody InvoiceDetailsVO getDueBalance(@PathVariable String societyID,@PathVariable String aptID)")	;
			try {
				AccountHeadVO achVo=new AccountHeadVO();
	 			List balanceList=new ArrayList();
				BigDecimal total=BigDecimal.ZERO;
				
				achVo=ledgerService.getLedgerID(aptID, societyID, "M","APTID");
				 
				 balanceList= invoiceService.getUnpaidInvoicesForMember(societyID, Integer.toString(achVo.getLedgerID()));
				 
				 for(int i=0;i<balanceList.size();i++){
					 InvoiceDetailsVO invoice=(InvoiceDetailsVO) balanceList.get(i);
					 total=total.add(invoice.getPaidAmount());						 
				 }
				 invoiceVO.setLedgerID(achVo.getLedgerID());
				 invoiceVO.setBalance(total);
				 invoiceVO.setListOfLineItems(balanceList);
				 
				logger.debug("Rest API DueBalance method Society ID: "+societyID+" APT ID: "+aptID+" Due: "+total);			
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody InvoiceDetailsVO getDueBalance(@PathVariable String societyID,@PathVariable String aptID)")	;						
			return invoiceVO;
	 
		}
		
		@RequestMapping(value="/invoiceList/{societyID},{aptID},{fromDate},{uptoDate}", method = RequestMethod.GET)
		public @ResponseBody JSONResponseVO getInvoiceListForMember(@PathVariable int societyID,@PathVariable int aptID, @PathVariable String fromDate,@PathVariable String uptoDate) {
			JSONResponseVO jSonVO=new JSONResponseVO();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getInvoiceListForMember(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;
			try {
				
				jSonVO=invoiceService.getInvoiceForMember(societyID, aptID, fromDate, uptoDate);
							
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getInvoiceListForMember(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;						
			return jSonVO;
	 
		}
		
		@RequestMapping(value="/receiptList/{societyID},{aptID},{fromDate},{uptoDate}", method = RequestMethod.GET)
		public @ResponseBody JSONResponseVO getReceiptForMember(@PathVariable int societyID,@PathVariable int aptID, @PathVariable String fromDate,@PathVariable String uptoDate) {
			JSONResponseVO jSonVO=new JSONResponseVO();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getInvoiceListForMember(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;
			try {
				
				jSonVO=txService.getReceiptForMember(societyID, aptID, fromDate, uptoDate);
							
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getInvoiceListForMember(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;						
			return jSonVO;
	 
		}
		
		@RequestMapping(value="/groupList/{rootID}", method = RequestMethod.GET)
		public @ResponseBody JsonRespAccountsVO getGroupList(@PathVariable int rootID) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			List groupList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getInvoiceListForMember(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;
			try {
				//UserVO user=userTokenHandler.parseUserFromToken(token);
				
				//if(societyID==user.getSocietyID()){
				int orgID=35;
				groupList=ledgerService.getAllGroupList(rootID,orgID);
				
				if(groupList.size()>0){
				jSonVO.setObjectList(groupList);
				jSonVO.setStatusCode(1);
				}
							
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getInvoiceListForMember(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;						
			return jSonVO;
	 
		}
		
		
		@RequestMapping(value="/ledgersList/{societyID},{groupID}", method = RequestMethod.GET)
		public @ResponseBody JsonRespAccountsVO getLedgersList(@PathVariable int societyID,@PathVariable int groupID) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			List ledgerList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getLedgersList(@PathVariable int societyID,@PathVariable int groupID)")	;
			try {
				//UserVO user=userTokenHandler.parseUserFromToken(token);
				
				//if(societyID==user.getSocietyID()){
				ledgerList=ledgerService.getLedgerList(societyID, groupID);
				jSonVO.setSocietyID(societyID);
				if(ledgerList.size()>0){
				jSonVO.setObjectList(ledgerList);
				jSonVO.setStatusCode(1);
				}
							
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getLedgersList(@PathVariable int societyID,@PathVariable int groupID)")	;						
			return jSonVO;
	 
		}
		
		@RequestMapping(value="/transactionList/{orgID},{ledgerID},{fromDate},{toDate}", method = RequestMethod.GET)
		public @ResponseBody LedgerTrnsVO getTransctionsList(@PathVariable int orgID,@PathVariable int ledgerID,@PathVariable String fromDate,@PathVariable String toDate) {
			
			LedgerTrnsVO ledgerTxVO=null;
				logger.debug("Entry : public @ResponseBody LedgerTrnsVO getInvoiceListForMember(@PathVariable int orgID,@PathVariable int ledgerID,@PathVariable String fromDate,@PathVariable String toDate)")	;
			try {
				
				ledgerTxVO=ledgerService.getLedgerTransactionList(orgID, ledgerID, fromDate, toDate,"ALL");
					
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody LedgerTrnsVO getInvoiceListForMember(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;						
			return ledgerTxVO;
	 
		}
		
		@RequestMapping(value="/closingbalance/{societyID},{ledgerID},{fromDate},{uptoDate}", method = RequestMethod.GET)
		public @ResponseBody AccountHeadVO getClosingBal(@PathVariable int societyID,@PathVariable int ledgerID, @PathVariable String fromDate,@PathVariable String uptoDate) {
	 			AccountHeadVO achVo=new AccountHeadVO();
				logger.debug("Entry : public @ResponseBody AccountHeadVO getClosingBal(@PathVariable String societyID,@PathVariable int ledgerID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;
			try {
				
				
							
				achVo = ledgerService.getOpeningClosingBalance(societyID, ledgerID, fromDate, uptoDate,"L");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody AccountHeadVO getClosingBal(@PathVariable String societyID,@PathVariable int ledgerID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;						
			return achVo;
	 
		}
		
		
		@RequestMapping(value="/receivableList/{societyID},{buildingID},{uptoDate}", method = RequestMethod.GET)
		public @ResponseBody JsonRespAccountsVO getReceivableList(@PathVariable int societyID,@PathVariable int buildingID,@PathVariable String uptoDate) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			List receivableList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getReceivableList(@PathVariable int societyID,@PathVariable int buildingID,@PathVariable String uptoDate)")	;
			try {
				//UserVO user=userTokenHandler.parseUserFromToken(token);
				
				//if(societyID==user.getSocietyID()){
				receivableList=reportService.getReceivableList(uptoDate, societyID, buildingID);
				jSonVO.setSocietyID(societyID);
				if(receivableList.size()>0){
				jSonVO.setObjectList(receivableList);
				jSonVO.setStatusCode(1);
				}
							
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getReceivableList(@PathVariable int societyID,@PathVariable int buildingID,@PathVariable String uptoDate) "+e);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getReceivableList(@PathVariable int societyID,@PathVariable int buildingID,@PathVariable String uptoDate)")	;						
			return jSonVO;
	 
		}
		
		
		@RequestMapping(value="/accountStats/{societyID},{fromDate},{toDate}", method = RequestMethod.GET)
		public @ResponseBody DashBoardAccountVO getAccountStatus(@PathVariable int societyID,@PathVariable String fromDate,@PathVariable String toDate) {
			DashBoardAccountVO dbVO=new DashBoardAccountVO();
				logger.debug("Entry : public @ResponseBody DashBoardAccountVO getAccountStatus(@PathVariable int societyID)")	;
			try {
				logger.info("Society Stats: Society ID: "+societyID);				
				
				dbVO =  accountDashBoardService.getAccountsDashBoard(societyID, fromDate, toDate);	
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block				
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody DashBoardAccountVO getAccountStatus(@PathVariable int societyID)")	;						
			return dbVO;
	 
		}
		
		
		
		@RequestMapping(value="/download/invoice/{societyID},{aptID},{invoiceID},{exportType},{reportID}", method = RequestMethod.GET)
		public @ResponseBody PrintReportVO downloadInvoice(@PathVariable String societyID,@PathVariable String aptID,@PathVariable String invoiceID, @PathVariable String exportType, @PathVariable String reportID) {
			InvoiceDetailsVO printInvoiceForm=new InvoiceDetailsVO();     
			PrintReportVO printVO=new PrintReportVO();   
			HashMap<String, Object> param= new HashMap<String, Object>();
			logger.debug("Entry : public @ResponseBody ReportVO downloadInvoice(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String invoiceID, @PathVariable String exportType, @PathVariable String reportID) ")	;
			try {
				       	  
				     param.put("society_id", societyID);
				     param.put("apt_id", aptID);
				     param.put("invoice_id", invoiceID);
		              printInvoiceForm.setPrintParam(param);
			      	  
		            printInvoiceForm.setReportContentType(exportType);
					printInvoiceForm.setReportID("template."+reportID);
					printInvoiceForm.setSourceType("BEAN");
					printVO=printReportService.printInvoice(printInvoiceForm);
							
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody PrintReportVO downloadInvoice(@PathVariable String societyID,@PathVariable String aptID,@PathVariable String invoiceID, @PathVariable String exportType, @PathVariable String reportID) ");						
			return printVO;
	 
		}
		
		
		@RequestMapping(value="/download/unpaidInvoices/{societyID},{aptID},{exportType},{reportID}", method = RequestMethod.GET)
		public @ResponseBody PrintReportVO downloadUnpaidInvoiceReport(@PathVariable int societyID,@PathVariable int aptID, @PathVariable String exportType, @PathVariable String reportID) {
			RptTransactionVO printTxVO=new RptTransactionVO();
			PrintReportVO printReport=new PrintReportVO();
			HashMap<String, Object> param= new HashMap<String, Object>();
			logger.debug("Entry : public @ResponseBody PrintReportVO downloadUnpaidInvoiceReport(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String exportType, @PathVariable String reportID) "+aptID)	;
			try {
				   MemberVO memberVO= new MemberVO();
				   memberVO=memberServiceBean.getMemberInfo(aptID);
				 
				  	param.put("society_id", societyID);
					param.put("apt_id", aptID);					
					printTxVO.setPrintParam(param);
					
					printTxVO.setLedgerName(memberVO.getFlatNo()+" - "+memberVO.getFullName());
					printTxVO.setReportContentType(exportType);
					printTxVO.setReportID("template."+reportID);					
					printTxVO.setSourceType("BEAN");
					printReport=printReportService.printUnpaidInvoicesMember(printTxVO);
							
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in downloadUnpaidInvoiceReport "+e);
			}
			logger.debug("Exit : public @ResponseBody PrintReportVO downloadUnpaidInvoiceReport(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String exportType, @PathVariable String reportID) ");						
			return printReport;
	 
		}
		
		
		@RequestMapping(value="/download/individualLedger/{societyID},{ledgerID},{fromDate},{toDate},{exportType}", method = RequestMethod.GET)
		public @ResponseBody PrintReportVO downloadIndividualLedger(@PathVariable String societyID, @PathVariable String ledgerID, @PathVariable String fromDate, @PathVariable String toDate, @PathVariable String exportType) {
			TransactionVO printTxVO=new TransactionVO();
			PrintReportVO printReport=new PrintReportVO();
			HashMap<String, Object> param= new HashMap<String, Object>();
			logger.debug("Entry : public @ResponseBody PrintReportVO downloadIndividualLedger(@PathVariable String societyID,@PathVariable String ledgerID,@PathVariable String fromDate,@PathVariable String toDate, @PathVariable String exportType) "+ledgerID+fromDate+toDate);
			try {
				 				 
				  	param.put("society_id", societyID);
				    printTxVO.setPrintParam(param);
				    
					printTxVO.setFromDate(fromDate);
					printTxVO.setToDate(toDate);
					printTxVO.setLedgerID(Integer.parseInt(ledgerID));
					
					printTxVO.setReportContentType(exportType);
					printTxVO.setReportID("template.memberLedger");					
					printTxVO.setSourceType("BEAN");
					printReport=printReportService.printIndividualLedgerReport(printTxVO);
				    printReport.setBeanList(null);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in downloadIndividualLedger "+e);
			}
			logger.debug("Exit : public @ResponseBody PrintReportVO downloadIndividualLedger(@PathVariable String societyID,@PathVariable String ledgerID,@PathVariable String fromDate,@PathVariable String toDate, @PathVariable String exportType) ");						
			return printReport;
	 
		}
	
		
		
		/**************************Tenants API************************************/
		
		@RequestMapping(value="/tenants/{apartmentID}", method = RequestMethod.GET)
		public @ResponseBody RenterVO getTenants(@PathVariable int apartmentID) {
			RenterVO rentrVO = new RenterVO();
			List renterMemberList=null;
			List addressList=null;
				logger.debug("Entry : public @ResponseBody List getTenants(@PathVariable String apartmentID)")	;
			try {
				//rentrVO =  memberServiceBean.getRenterDetails(apartmentID);				
				logger.info("Renter ID: "+rentrVO.renterID);
				
				renterMemberList=memberServiceBean.getRenterMemberDetails(rentrVO.getRenterID());				
				logger.info("Renter Members Size: "+renterMemberList.size());
				rentrVO.setRenterMembers(renterMemberList);
								
				addressList=addressServiceBean.getAddressDetailsRenter(rentrVO.getRenterID(), "R", 0, "0");	
				logger.info("Renter Address Size: "+addressList.size());
				rentrVO.setAddressList(addressList);
							
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody List getTenants(@PathVariable String apartmentID)")	;						
			return rentrVO;
	 
		}
		
		
		@RequestMapping(value="/delete/tenant/{renterID},{apartmentID}", method = RequestMethod.GET)
		public @ResponseBody int deleteTenant(@PathVariable int renterID,@PathVariable int apartmentID) {
			int success=0;
				logger.debug("Entry : public @ResponseBody int deleteTenant(@@PathVariable String renterID,@PathVariable String apartmentID)")	;
			try {
				success =  memberServiceBean.deleteRenterDetails(renterID, apartmentID);		
				
				logger.info("Result Delete Tenant: "+success);				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				success=0;
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody List deleteTenant(@PathVariable String renterID,@PathVariable String apartmentID)")	;						
			return success;
	 
		}
		
		@RequestMapping(value="/delete/tenant/member/{renterMemberID}", method = RequestMethod.GET)
		public @ResponseBody int deleteTenantMember(@PathVariable int renterMemberID) {
			int success=0;
				logger.debug("Entry : public @ResponseBody int deleteTenantMember(@PathVariable String renterMemberID)")	;
			try {
				success =  memberServiceBean.deleteRenterMember(renterMemberID);		
				
				logger.info("Result Delete Tenant Member: "+success);				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				success=0;
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody List deleteTenantMember(@PathVariable String renterMemberID)")	;						
			return success;
	 
		}
		
		@RequestMapping(value="/insert/tenant",  method = RequestMethod.POST  )			
		public @ResponseBody int insertTenant(RenterVO renterVO) {
			int success=0;
				logger.info("Entry : public @ResponseBody int insertTenant( RenterVO renterVO)"+renterVO.getAge())	;
			try {
				logger.info("Apartment ID: "+renterVO.getAptID());
				
			    success=memberServiceBean.insertTenantForWebservice(renterVO);
				
				logger.info("Result Insert Tenant : "+success);				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				success=0;
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody int insertTenant( RenterVO renterVO)")	;						
			return success;
	 
		}
		
		@RequestMapping(value="/insert/tenant/member",  method = RequestMethod.POST  )			
		public @ResponseBody int insertTenantMember(RenterVO renterVO) {
			int success=0;
				logger.debug("Entry : public @ResponseBody int insertTenantMember( RenterVO renterVO)")	;
			try {
				logger.info("APT ID: "+renterVO.getAptID()+" Tenant ID: "+renterVO.getRenterID());
				
			    success=memberServiceBean.addRenterMemberDetails(renterVO);
				
				logger.info("Result Insert Tenant Member: "+success);				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				success=0;
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody int insertTenantMember( RenterVO renterVO)")	;						
			return success;
	 
		}
		
		/**************************Society Stats API************************************/
		
		@RequestMapping(value="/societystats/{societyID},{version}", method = RequestMethod.GET)
		public @ResponseBody DashBoardVO getSocietyStats(@PathVariable int societyID,@PathVariable String version) {
			DashBoardVO dbVO=new DashBoardVO();
				logger.debug("Entry : public @ResponseBody DashBoardVO getSocietyStats(@PathVariable String societyID,@PathVariable String version)")	;
			try {
				logger.info("Society Stats: Society ID: "+societyID+" Version: "+version);				
				
				dbVO =  dashBoardService.getSocietyDashBoard(societyID, version);		
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block				
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody DashBoardVO getSocietyStats(@PathVariable String societyID,@PathVariable String version)")	;						
			return dbVO;
	 
		}
		
		
		/*---------------------Admin Registers---------------------------------*/
		
		@RequestMapping(value="/jRegister/{societyID},{buildingID}", method = RequestMethod.GET)
		public @ResponseBody JSONResponseVO getJRegister(@PathVariable int societyID,@PathVariable int buildingID) {
			JSONResponseVO jsonVO=new JSONResponseVO();
			List jRegList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JSONResponseVO getJRegister(@PathVariable int societyID,@PathVariable int buildingID)")	;
			try {
						
				
				jRegList =  societyService.getJRegisterList(societyID, buildingID);		
				if(jRegList.size()>0){
					jsonVO.setStatusCode(1);
					jsonVO.setSocietyID(societyID);
					jsonVO.setObjectList(jRegList);
				}else
					jsonVO.setStatusCode(0);
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block				
				logger.error("Exception in getJRegister "+e);
			}
			logger.debug("Exit : public @ResponseBody JSONResponseVO getJRegister(@PathVariable int societyID,@PathVariable int buildingID)")	;						
			return jsonVO;
	 
		}
		
		
		@RequestMapping(value="/shareRegister/{societyID},{buildingID}", method = RequestMethod.GET)
		public @ResponseBody JSONResponseVO getShareRegister(@PathVariable int societyID,@PathVariable int buildingID) {
			JSONResponseVO jsonVO=new JSONResponseVO();
			List shareRegList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JSONResponseVO getShareRegister(@PathVariable int societyID,@PathVariable int buildingID)")	;
			try {
						
				
				shareRegList =  shareRegService.getShareList(buildingID, societyID);		
				if(shareRegList.size()>0){
					jsonVO.setStatusCode(1);
					jsonVO.setSocietyID(societyID);
					jsonVO.setObjectList(shareRegList);
				}else
					jsonVO.setStatusCode(0);
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block				
				logger.error("Exception in getShareRegister  "+e);
			}
			logger.debug("Exit : public @ResponseBody JSONResponseVO getShareRegister(@PathVariable int societyID,@PathVariable int buildingID)")	;						
			return jsonVO;
	 
		}
		
		
		@RequestMapping(value="/tenantRegister/{societyID}", method = RequestMethod.GET)
		public @ResponseBody JSONResponseVO getTenantRegister(@PathVariable int societyID) {
			JSONResponseVO jsonVO=new JSONResponseVO();
			List tenantRegList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JSONResponseVO getTenantRegister(@PathVariable int societyID)")	;
			try {
						
				
				tenantRegList =  reportService.tenantDetailsRpt(societyID);		
				if(tenantRegList.size()>0){
					jsonVO.setStatusCode(1);
					jsonVO.setSocietyID(societyID);
					jsonVO.setObjectList(tenantRegList);
				}else
					jsonVO.setStatusCode(0);
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block				
				logger.error("Exception in getTenantRegister "+e);
			}
			logger.debug("Exit : public @ResponseBody JSONResponseVO getTenantRegister(@PathVariable int societyID,@PathVariable int buildingID)")	;						
			return jsonVO;
	 
		}
		
		
		@RequestMapping(value="/nominationRegister/{societyID}", method = RequestMethod.GET)
		public @ResponseBody JSONResponseVO getNominationRegister(@PathVariable int societyID) {
			JSONResponseVO jsonVO=new JSONResponseVO();
			List nominationRegList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JSONResponseVO getNominationRegister(@PathVariable int societyID)")	;
			try {
						
				
				nominationRegList =  nominationRegService.getNominationList(0, societyID);		
				if(nominationRegList.size()>0){
					jsonVO.setStatusCode(1);
					jsonVO.setSocietyID(societyID);
					jsonVO.setObjectList(nominationRegList);
				}else
					jsonVO.setStatusCode(0);
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block				
				logger.error("Exception in getNominationRegister "+e);
			}
			logger.debug("Exit : public @ResponseBody JSONResponseVO getNominationRegister(@PathVariable int societyID)")	;						
			return jsonVO;
	 
		}
		
		
		
		@RequestMapping(value="/vehicleRegister/{societyID}", method = RequestMethod.GET)
		public @ResponseBody RptVehicleVO getVehicleRegister(@PathVariable int societyID) {
			RptVehicleVO vehicleVO=new RptVehicleVO();
			
				logger.debug("Entry : public @ResponseBody RptVehicleVO getVehicleRegister(@PathVariable int societyID)")	;
			try {
						
				vehicleVO=reportService.getVehicleReportForAPI(societyID);
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block				
				logger.error("Exception in getVehicleRegister "+e);
			}
			logger.debug("Exit : public @ResponseBody RPTVehicleVO getVehicleRegister(@PathVariable int societyID)")	;						
			return vehicleVO;
	 
		}
		
		
		
		
		

		

		

	}

