package com.emanager.webservice;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emanager.Utility.UtilityClaass;
import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.ChargesVO;
import com.emanager.server.accounts.DataAccessObjects.LedgerEntries;
import com.emanager.server.accounts.DataAccessObjects.LedgerTrnsVO;
import com.emanager.server.accounts.DataAccessObjects.OnlinePaymentGatewayVO;
import com.emanager.server.accounts.DataAccessObjects.ScheduledTransactionResponseVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.accountsAutomation.service.AccountsAutoService;
import com.emanager.server.adminReports.Services.PrintReportService;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.dashBoard.Services.AccountDashBoardService;
import com.emanager.server.dashBoard.Services.DashBoardService;
import com.emanager.server.dashBoard.dataAccessObject.AccountStatusVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardAccountVO;
import com.emanager.server.financialReports.services.AccountingStatusService;
import com.emanager.server.financialReports.services.BalanceSheetService;
import com.emanager.server.financialReports.services.BudgetService;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.financialReports.services.TrialBalanceService;
import com.emanager.server.financialReports.valueObject.BudgetDetailsVO;
import com.emanager.server.financialReports.valueObject.MonthwiseChartVO;
import com.emanager.server.financialReports.valueObject.ReportDetailsVO;
import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.invoice.Services.InvoiceService;
import com.emanager.server.invoice.dataAccessObject.BillDetailsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceDetailsVO;
import com.emanager.server.invoice.dataAccessObject.ProfileDetailsVO;
import com.emanager.server.projects.service.ProjectDetailsService;
import com.emanager.server.projects.valueObjects.ProjectDetailsVO;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.BankInfoVO;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.userManagement.service.UserService;
import com.emanager.server.userManagement.valueObject.UserVO;
	
	@Controller
	@RequestMapping("/secure/accounts")
	public class JSONAccountsController {
		
		private static final Logger logger = Logger.getLogger(JSONAccountsController.class);
		@Autowired
	    ReportService reportService;
	    @Autowired
	    TransactionService transactionService;
	    @Autowired
	    TransactionService txService;
	    @Autowired
	    InvoiceService invoiceService;
	    @Autowired
	    PrintReportService printReportService;
	    @Autowired
	    MemberService memberServiceBean;
	    @Autowired
	    DashBoardService dashBoardService;
	    @Autowired
	    AccountDashBoardService accountDashBoardService;
	    @Autowired
	    SocietyService societyService;
	    @Autowired
	    LedgerService ledgerService;
	    @Autowired
	    TrialBalanceService trialBalanceService;
	    @Autowired
	    BalanceSheetService balanceSheetService;
	    @Autowired
	    UserService userService;   
	    @Autowired 
	    AccountingStatusService accountingStatusRptService;
	    @Autowired
	    UtilityClaass utilityClass;
	    @Autowired
	    ProjectDetailsService projectDetailsService;
	    @Autowired
	    BudgetService budgetService;
	    @Autowired
		AccountsAutoService accountsAutoService;
	    
	    DateUtility dateUtil=new DateUtility();
	    public ConfigManager configManager =new ConfigManager();
	    public EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
		public final String HEADER_SECURITY_TOKEN = "X-AuthToken";
	   
	  	@RequestMapping(value="/invoiceList", method = RequestMethod.POST)
		public ResponseEntity getInvoiceListForMember(@RequestBody RequestWrapper requestWrapper) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getInvoiceListForMember(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getAptID()>0 && requestWrapper.getFromDate().length()>0  &&  requestWrapper.getToDate().length()>0){
				   jSonVO=invoiceService.getInvoiceForMember(requestWrapper.getOrgID(), requestWrapper.getAptID(), requestWrapper.getFromDate(), requestWrapper.getToDate());
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getInvoiceListForMember "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getInvoiceListForMember(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;						
			return  new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/unpaidInvoiceList", method = RequestMethod.POST)
		public ResponseEntity getUnpaidInvoiceList(@RequestBody RequestWrapper requestWrapper) {
			JSONResponseVO jsonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List invoiceList=null;
				logger.debug("Entry : public @ResponseBody JSONResponseVO getUnpaidInvoiceList(@RequestBody RequestWrapper requestWrapper) ");
			try {
				if(requestWrapper.getOrgID()>0){
				 invoiceList= invoiceService.getUnpaidInvoicesForMember(requestWrapper.getOrgID(),Integer.toString(requestWrapper.getLedgerID()));
				 if(invoiceList.size()==0){
						jsonVO.setStatusCode(0);
					}else{
						jsonVO.setStatusCode(1);
						jsonVO.setObjectList(invoiceList);
					}		
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getUnpaidInvoiceList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JSONResponseVO getUnpaidInvoiceList(@RequestBody RequestWrapper requestWrapper) ");						
			return new ResponseEntity(jsonVO, HttpStatus.OK);
	 
		}
		
		/* Get list of all invoices of the given society for that particular period  */
		@RequestMapping(value="/allInvoiceList", method = RequestMethod.POST)
		public ResponseEntity getInvoicesForSociety(@RequestBody InvoiceDetailsVO invoiceDetailsVO) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List invoiceList=new ArrayList();
			
				logger.info("Entry : public @ResponseBody JsonRespAccountsVO getInvoiceListForMember(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String fromDate,@PathVariable String uptoDate)"+invoiceDetailsVO.getSocietyID()+invoiceDetailsVO.getFromDate()+invoiceDetailsVO.getUptoDate())	;
			try {
				
				if((invoiceDetailsVO.getSocietyID()>0) && (invoiceDetailsVO.getFromDate().length()>0)  &&  (invoiceDetailsVO.getUptoDate().length()>0)){
				   invoiceList=invoiceService.getInvoicesForSociety(invoiceDetailsVO);
					if(invoiceList.size()==0){
						jSonVO.setStatusCode(0);
					}else{
						jSonVO.setStatusCode(200);
						jSonVO.setObjectList(invoiceList);
					}			
				   
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getInvoiceListForMember "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getInvoiceListForMember(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;						
			return  new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		/* Get list of all one time invoices of the given society   */
		@RequestMapping(value="/allOneTimeInvoiceList", method = RequestMethod.POST)
		public ResponseEntity getOnetimeInvoicesForSociety(@RequestBody InvoiceDetailsVO invoiceDetailsVO) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List invoiceList=new ArrayList();
			
				logger.info("Entry : public ResponseEntity getOnetimeInvoicesForSociety(@RequestBody InvoiceDetailsVO invoiceDetailsVO)"+invoiceDetailsVO.getSocietyID())	;
			try {
				
				if(invoiceDetailsVO.getSocietyID()>0 ){
				   invoiceList=invoiceService.getOnetimeInvoicesForSociety(invoiceDetailsVO);
					if(invoiceList.size()==0){
						jSonVO.setStatusCode(0);
					}else{
						jSonVO.setStatusCode(200);
						jSonVO.setObjectList(invoiceList);
					}			
				   
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getOnetimeInvoicesForSociety "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getOnetimeInvoicesForSociety(@RequestBody InvoiceDetailsVO invoiceDetailsVO)")	;						
			return  new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		/* Get list of all invoices of the given ledger for that particular period  */
		@RequestMapping(value="/ledgerInvoiceList", method = RequestMethod.POST)
		public ResponseEntity getInvoicesForLedger(@RequestBody InvoiceDetailsVO invoiceDetailsVO) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List invoiceList=new ArrayList();
			
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getInvoiceListForMember(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;
			try {
				
				if((invoiceDetailsVO.getSocietyID()>0) && (invoiceDetailsVO.getLedgerID()>0) && (invoiceDetailsVO.getFromDate().length()>0)  &&  (invoiceDetailsVO.getUptoDate().length()>0)){
				   invoiceList=invoiceService.getInvoicesForLedger(invoiceDetailsVO);
					if(invoiceList.size()==0){
						jSonVO.setStatusCode(0);
					}else{
						jSonVO.setStatusCode(200);
						jSonVO.setObjectList(invoiceList);
					}			
				   
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getInvoiceListForMember "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getInvoiceListForMember(@PathVariable String societyID,@PathVariable String aptID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;						
			return  new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/invoiceTypeList", method = RequestMethod.POST)
		public ResponseEntity getInvoiceTypeList(@RequestBody InvoiceDetailsVO invoiceDetailsVO) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List invoiceTypeList=new ArrayList();
				logger.debug("Entry :public ResponseEntity getInvoiceTypeList()")	;
			try {
				
				invoiceTypeList=invoiceService.getInvoiceTypeList(invoiceDetailsVO.getGstType());
				if(invoiceTypeList.size()>0){
					jSonVO.setStatusCode(200);
					jSonVO.setObjectList(invoiceTypeList);
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getInvoiceTypeList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit :public ResponseEntity getInvoiceTypeList()")	;						
			return  new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		
		/*=================Bill Related APIs ===================================*/
		
		@RequestMapping(value="/billList", method = RequestMethod.POST)
		public ResponseEntity getBillListForMember(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List billList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getBillListForMember(@RequestBody RequestWrapper requestWrapper)");
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0 && requestWrapper.getAptID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
				billList=invoiceService.getBillsForMember(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getAptID());
				jSonVO.setSocietyID(requestWrapper.getOrgID());
				jSonVO.setFromDate(requestWrapper.getFromDate());
				jSonVO.setUptoDate(requestWrapper.getToDate());
				if(billList.size()==0){
					jSonVO.setStatusCode(0);
				}else{
					jSonVO.setStatusCode(1);
					jSonVO.setObjectList(billList);
				}			
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getUnpaidInvoiceList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getBillListForMember(@RequestBody RequestWrapper requestWrapper)");						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		

		@RequestMapping(value="/billDetailsInvoiceBased", method = RequestMethod.POST)
		public ResponseEntity getInvoiceBasedBillDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			BillDetailsVO billVO=new BillDetailsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List billList=new ArrayList();
				logger.debug("Entry : public @ResponseBody BillDetailsVO getInvoiceBasedBillDetails(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0 && requestWrapper.getLedgerID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
					billVO=invoiceService.getInvoiceBasedBillDetails(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getLedgerID());
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getInvoiceBasedBillDetails "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody BillDetailsVO getInvoiceBasedBillDetails(@RequestBody RequestWrapper requestWrapper)");						
			return new ResponseEntity(billVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/billDetailsOld", method = RequestMethod.POST)
		public ResponseEntity getBillsDetailsSociety(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			BillDetailsVO billVO=new BillDetailsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List billList=new ArrayList();
				logger.debug("Entry : public @ResponseBody BillDetailsVO getBillsDetailsSociety(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0 && requestWrapper.getAptID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
					billVO=invoiceService.getBillsDetailsSociety(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getAptID());
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getBillsDetailsSociety "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody BillDetailsVO getBillsDetailsSociety(@RequestBody RequestWrapper requestWrapper)");						
			return new ResponseEntity(billVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/billDetails", method = RequestMethod.POST)
		public ResponseEntity getBillsDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			BillDetailsVO billVO=new BillDetailsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List billList=new ArrayList();
				logger.debug("Entry : public @ResponseBody BillDetailsVO getBillsDetailsSociety(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0 && requestWrapper.getAptID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
					billVO=invoiceService.getBillDetails(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getAptID());
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getBillsDetailsSociety "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody BillDetailsVO getBillsDetailsSociety(@RequestBody RequestWrapper requestWrapper)");						
			return new ResponseEntity(billVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/billCycleList", method = RequestMethod.POST)
		public ResponseEntity getBillCycleListForSociety(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List billCycleList=new ArrayList();
				logger.debug("Entry : public @ResponseBody List getBillCycleListForSociety(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				if(requestWrapper.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
				billCycleList=invoiceService.getBillingCycleList(requestWrapper.getOrgID());
				jSonVO.setSocietyID(requestWrapper.getOrgID());
				
				if(billCycleList.size()==0){
					jSonVO.setStatusCode(0);
				}else{
					jSonVO.setStatusCode(1);
					jSonVO.setObjectList(billCycleList);
				}			
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getBillCycleListForSociety "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody List getBillCycleListForSociety(@RequestBody RequestWrapper requestWrapper)");						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		@RequestMapping(value="/billListForSociety", method = RequestMethod.POST)
		public ResponseEntity getBillListForSociety(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List billList=new ArrayList();
				logger.debug("Entry : public @ResponseBody List getBillListForSociety(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0 && requestWrapper.getBillingCycleID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
					billList=invoiceService.getBillsForSociety(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getBillingCycleID());
				jSonVO.setSocietyID(requestWrapper.getOrgID());
				jSonVO.setFromDate(requestWrapper.getFromDate());
				jSonVO.setUptoDate(requestWrapper.getToDate());
				if(billList.size()==0){
					jSonVO.setStatusCode(0);
				}else{
					jSonVO.setStatusCode(1);
					jSonVO.setObjectList(billList);
				}			
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getBillCycleListForSociety "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody List getBillListForSociety(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/billListForOrg", method = RequestMethod.POST)
		public ResponseEntity getBillListForOrg(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List billList=new ArrayList();
				logger.debug("Entry : public @ResponseBody List getBillListForOrg(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0 && requestWrapper.getBillingCycleID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
					billList=invoiceService.getBillsForOrg(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getBillingCycleID());
				jSonVO.setSocietyID(requestWrapper.getOrgID());
				jSonVO.setFromDate(requestWrapper.getFromDate());
				jSonVO.setUptoDate(requestWrapper.getToDate());
				if(billList.size()==0){
					jSonVO.setStatusCode(0);
				}else{
					jSonVO.setStatusCode(1);
					jSonVO.setObjectList(billList);
				}			
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getBillListForOrg "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody List getBillListForOrg(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/billListLedger", method = RequestMethod.POST)
		public ResponseEntity getBillListForLedger(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List billList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getBillListForLedger(@RequestBody RequestWrapper requestWrapper)");
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0 && requestWrapper.getLedgerID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
				billList=invoiceService.getBillsForLedger(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getLedgerID());
				jSonVO.setSocietyID(requestWrapper.getOrgID());
				jSonVO.setFromDate(requestWrapper.getFromDate());
				jSonVO.setUptoDate(requestWrapper.getToDate());
				if(billList.size()==0){
					jSonVO.setStatusCode(0);
				}else{
					jSonVO.setStatusCode(1);
					jSonVO.setObjectList(billList);
				}			
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getBillListForLedger "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getBillListForLedger(@RequestBody RequestWrapper requestWrapper)");						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/receiptList", method = RequestMethod.POST)
		public ResponseEntity getReceiptListForMember(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody JSONResponseVO getReceiptListForMember(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					jSonVO=txService.getReceiptForMember(requestWrapper.getOrgID(), requestWrapper.getAptID(), requestWrapper.getFromDate(), requestWrapper.getToDate());
							
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getReceiptListForMember "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JSONResponseVO getReceiptForMember(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
	
		
		@RequestMapping(value="/detailedtransactionList", method = RequestMethod.POST)
		public ResponseEntity getTransctionsList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			LedgerTrnsVO ledgerTxVO=null;
				logger.debug("Entry : public @ResponseBody LedgerTrnsVO getTransctionsList(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				if(requestWrapper.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
				ledgerTxVO=ledgerService.getLedgerTransactionList(requestWrapper.getOrgID(), requestWrapper.getLedgerID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),"ALL");
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getTransctionsList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody LedgerTrnsVO getTransctionsList(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(ledgerTxVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/transactionList", method = RequestMethod.POST)
		public ResponseEntity getQuickTransctionsList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			LedgerTrnsVO ledgerTxVO=null;
				logger.debug("Entry : public @ResponseBody LedgerTrnsVO getQuickTransctionsList(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				if(requestWrapper.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
				ledgerTxVO=ledgerService.getQuickLedgerTransactionList(requestWrapper.getOrgID(), requestWrapper.getLedgerID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),"ALL");
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getTransctionsList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody LedgerTrnsVO getQuickTransctionsList(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(ledgerTxVO, HttpStatus.OK);
	 
		}
		
		/*@RequestMapping(value="/closingbalance", method = RequestMethod.POST)
		public @ResponseBody AccountHeadVO getClosingBal(@RequestBody RequestWrapper requestWrapper) {
	 			AccountHeadVO achVo=new AccountHeadVO();
				logger.debug("Entry : public @ResponseBody AccountHeadVO getClosingBal(@PathVariable String societyID,@PathVariable int ledgerID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;
			try {
				
				if(requestWrapper.getOrgID()>0){
							
				achVo = ledgerService.getOpeningClosingBalance(requestWrapper.getOrgID(), requestWrapper.getLedgerID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),"L");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody AccountHeadVO getClosingBal(@PathVariable String societyID,@PathVariable int ledgerID, @PathVariable String fromDate,@PathVariable String uptoDate)")	;						
			return achVo;
	 
		}*/
		
		
		@RequestMapping(value="/receivableList", method = RequestMethod.POST)
		public ResponseEntity getReceivableList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List receivableList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getReceivableList(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  receivableList=reportService.getReceivableListInBillFormat(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate());
				  jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(receivableList.size()>0){
				   jSonVO.setObjectList(receivableList);
				   jSonVO.setStatusCode(1);
				}
							
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getReceivableList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getReceivableList(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/receivableListForAllTags", method = RequestMethod.POST)
		public ResponseEntity getReceivableListForAllTags(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List receivableList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getReceivableListForAllTags(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  receivableList=reportService.getReceivableListForAllTags(requestWrapper.getOrgID(),  requestWrapper.getToDate());
				  jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(receivableList.size()>0){
				   jSonVO.setObjectList(receivableList);
				   jSonVO.setStatusCode(1);
				}
							
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getReceivableListForAllTags "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getReceivableListForAllTags(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/receivableListSingleTag", method = RequestMethod.POST)
		public ResponseEntity getReceivableListForSingleTags(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List receivableList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getReceivableListForSingleTags(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				
				if(requestWrapper.getOrgID()>0 &&  requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  receivableList=reportService.getReceivableListForSingleTags(requestWrapper.getOrgID(),  requestWrapper.getToDate(),requestWrapper.getType());
				  jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(receivableList.size()>0){
				   jSonVO.setObjectList(receivableList);
				   jSonVO.setStatusCode(1);
				}
							
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getReceivableListForSingleTags "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getReceivableListForSingleTags(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/report/balanceSheet", method = RequestMethod.POST)
		public ResponseEntity getBalanceSheetReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			ReportVO rptVO=new ReportVO();
				logger.debug("Entry : public @ResponseBody ReportVO getBalanceSheetReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
			
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					rptVO=balanceSheetService.getBalanceSheet(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate());
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getReceivableList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getBalanceSheetReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/report/trialBalance", method = RequestMethod.POST)
		public ResponseEntity getTrialBalanceReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List trialBalanceList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getTrialBalanceReport(@PathVariable int societyID,@PathVariable String fromDate,@PathVariable String uptoDate)" );
			try {
			
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				 trialBalanceList=trialBalanceService.getTrialBalanceReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate());
				 jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(trialBalanceList.size()>0){
				 jSonVO.setObjectList(trialBalanceList);
				 jSonVO.setStatusCode(1);
				 jSonVO.setFromDate(requestWrapper.getFromDate());
				 jSonVO.setUptoDate(requestWrapper.getToDate());
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getTrialBalanceReport "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getTrialBalanceReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/report/incomeExpense", method = RequestMethod.POST)
		public ResponseEntity getIncomeExpenseReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportVO rptVO=new ReportVO();
				logger.debug("Entry : public @ResponseBody ReportVO getIncomeExpenseReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  rptVO=reportService.getIncomeExpenseReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),0,"S");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in incomeExpense "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getBalanceSheetReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/profitLoss", method = RequestMethod.POST)
		public ResponseEntity getProfitLossReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportVO rptVO=new ReportVO();
				logger.debug("Entry : public @ResponseBody ReportVO getProfitLossReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  rptVO=reportService.getProfitLossReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),"S");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getProfitLossReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getProfitLossReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		//-----------Consolidated reports ------------//
		@RequestMapping(value="/report/consolidatedBalanceSheet", method = RequestMethod.POST)
		public ResponseEntity getConsolidatedBalanceSheetReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			ReportVO rptVO=new ReportVO();
				logger.debug("Entry : public @ResponseBody ReportVO getBalanceSheetReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
			
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					rptVO=balanceSheetService.getConsolidatedBalanceSheetReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),"S");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getReceivableList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getBalanceSheetReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/report/consolidatedProfitLoss", method = RequestMethod.POST)
		public ResponseEntity getConsolidateProfitLossReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportVO rptVO=new ReportVO();
				logger.debug("Entry : public @ResponseBody ReportVO getConsolidateProfitLossReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  rptVO=reportService.getConsolidatedBalanceSheetReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),"S");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getConsolidateProfitLossReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getConsolidateProfitLossReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/consolidatedTrialBalance", method = RequestMethod.POST)
		public ResponseEntity getConsolidatedTrialBalanceReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List trialBalanceList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getConsolidatedTrialBalanceReport(@PathVariable int societyID,@PathVariable String fromDate,@PathVariable String uptoDate)" );
			try {
			
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				 trialBalanceList=trialBalanceService.getConsolidatedTrialBalanceReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate());
				 jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(trialBalanceList.size()>0){
				 jSonVO.setObjectList(trialBalanceList);
				 jSonVO.setStatusCode(1);
				 jSonVO.setFromDate(requestWrapper.getFromDate());
				 jSonVO.setUptoDate(requestWrapper.getToDate());
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getConsolidatedTrialBalanceReport "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getConsolidatedTrialBalanceReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/consolidatedIncomeExpense", method = RequestMethod.POST)
		public ResponseEntity getConsolidatedIncomeExpenseReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportVO rptVO=new ReportVO();
				logger.debug("Entry : public @ResponseBody ReportVO getConsolidatedIncomeExpenseReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  rptVO=reportService.getConsolidatedIncomeExpenseReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),0,"S");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getConsolidatedIncomeExpenseReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getConsolidatedIncomeExpenseReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/consolidatedTrialBalanceDetails", method = RequestMethod.POST)
		public ResponseEntity getConsolidatedTrialBalanceDetailsReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List trialBalanceList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getConsolidatedTrialBalanceDetailsReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					trialBalanceList=trialBalanceService.getLedgerReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getGroupID(),1);
				 jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(trialBalanceList.size()>0){
				  jSonVO.setObjectList(trialBalanceList);
				  jSonVO.setStatusCode(1);
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getConsolidatedTrialBalanceDetailsReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}		
				
			
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getConsolidatedTrialBalanceDetailsReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		//----Consolidated month wise Cash Flow report----------//
		@RequestMapping(value="/report/consolidatedMonthWiseCashFlowtBreakUpReport", method = RequestMethod.POST)
		public ResponseEntity getConsolidatedMonthWiseCashFlowBreakUpReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			MonthwiseChartVO rptVO=new MonthwiseChartVO();
				logger.debug("Entry : public ResponseEntity getConsolidatedMonthWiseCashFlowBreakUpReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  rptVO=reportService.getConsolidatedReceiptPaymentReportForChart(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),"CF");				 
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in public ResponseEntity getConsolidatedMonthWiseCashFlowBreakUpReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity getConsolidatedMonthWiseCashFlowBreakUpReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/consolidatedCashBasedLedgerReport", method = RequestMethod.POST)
		public ResponseEntity getConsolidatedCashBasedLedgerReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List trialBalanceList=new ArrayList();
				logger.info("Entry : public @ResponseBody JsonRespAccountsVO getConsolidatedCashBasedLedgerReport(@RequestBody RequestWrapper requestWrapper)"+requestWrapper.getGroupID() );
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				 trialBalanceList=reportService.getConsolidatedCashBasedLedgerReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getGroupID(),requestWrapper.getType());
				 jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(trialBalanceList.size()>0){
				  jSonVO.setObjectList(trialBalanceList);
				  jSonVO.setStatusCode(1);
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getConsolidatedCashBasedLedgerReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}		
				
			
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getConsolidatedCashBasedLedgerReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		
		//==============Ind AS Reporting system with CustomisableReports=========//
		
		@RequestMapping(value="/report/balanceSheetIndAS", method = RequestMethod.POST)
		public ResponseEntity getIndASBalanceSheetReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			ReportVO rptVO=new ReportVO();
				logger.debug("Entry : public @ResponseBody ReportVO getIndASBalanceSheetReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
			
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					rptVO=balanceSheetService.getIndASBalanceSheetReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),requestWrapper.getGroupID());
					}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getIndASBalanceSheetReport "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getIndASBalanceSheetReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
	
		@RequestMapping(value="/report/profitLossIndAS", method = RequestMethod.POST)
		public ResponseEntity getIndASProfitLossReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportVO rptVO=new ReportVO();
				logger.debug("Entry : public @ResponseBody ReportVO getIndASProfitLossReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					 rptVO=reportService.getIndASProfitLossReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),"S",requestWrapper.getGroupID());		
					 }else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getIndASProfitLossReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getIndASProfitLossReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/cashBasedReceiptPayment", method = RequestMethod.POST)
		public ResponseEntity getCashBasedReceiptPaymentReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportVO rptVO=new ReportVO();
				logger.debug("Entry : public @ResponseBody ReportVO getCashBasedReceiptPaymentReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  rptVO=reportService.getCashBasedReceiptPaymentReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),"IE");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getCashBasedReceiptPaymentReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getCashBasedReceiptPaymentReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/cashFlowReport", method = RequestMethod.POST)
		public ResponseEntity getCashFlowtReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportVO rptVO=new ReportVO();
				logger.debug("Entry : public ResponseEntity getCashFlowtReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  rptVO=reportService.getCashBasedReceiptPaymentReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),"CF");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getCashBasedReceiptPaymentReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity getCashFlowtReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/monthWiseBreakUpReport", method = RequestMethod.POST)
		public ResponseEntity getMonthWiseBreakUpReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			MonthwiseChartVO rptVO=new MonthwiseChartVO();
				logger.debug("Entry : public ResponseEntity getCashFlowtReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  rptVO=reportService.getIncomeExpenseReportForChart(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate());
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getMonthWiseBreakUpReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getMonthWiseBreakUpReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/monthWiseReceiptPaymentBreakUpReport", method = RequestMethod.POST)
		public ResponseEntity getMonthWiseReceiptPaymentBreakUpReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			MonthwiseChartVO rptVO=new MonthwiseChartVO();
				logger.debug("Entry : public @ResponseBody ReportVO getMonthWiseReceiptPaymentBreakUpReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  rptVO=reportService.getReceiptPaymentReportForChart(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),"IE");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getMonthWiseReceiptPaymentBreakUpReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getMonthWiseReceiptPaymentBreakUpReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/monthWiseCashFlowtBreakUpReport", method = RequestMethod.POST)
		public ResponseEntity getMonthWiseCashFlowBreakUpReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			MonthwiseChartVO rptVO=new MonthwiseChartVO();
				logger.debug("Entry : public ResponseEntity getMonthWiseCashFlowBreakUpReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  rptVO=reportService.getReceiptPaymentReportForChart(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),"CF");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in public ResponseEntity getMonthWiseCashFlowBreakUpReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity getMonthWiseCashFlowBreakUpReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/monthWiseReportForReceivables", method = RequestMethod.POST)
		public ResponseEntity getMonthWiseBreakUpReportForReceivables(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			MonthwiseChartVO rptVO=new MonthwiseChartVO();
				logger.debug("Entry : public ResponseEntity getMonthWiseBreakUpReportForReceivables(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  rptVO=reportService.getMonthWiseBreakUpReportForReceivables(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate());
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getMonthWiseBreakUpReportForReceivables "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getMonthWiseBreakUpReportForReceivables(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/report/monthWiseReportForReceivablesForCostCenters", method = RequestMethod.POST)
		public ResponseEntity getMonthWiseBreakUpReportForReceivablesForCostCenters(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			MonthwiseChartVO rptVO=new MonthwiseChartVO();
				logger.debug("Entry : public ResponseEntity getMonthWiseBreakUpReportForReceivablesForCostCenters(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  rptVO=projectDetailsService.getMonthWiseBreakUpReportForReceivablesForCostCenters(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),requestWrapper.getProjectAcronyms());
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getMonthWiseBreakUpReportForReceivablesForCostCenters "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getMonthWiseBreakUpReportForReceivablesForCostCenters(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/receivablesByCostCenter", method = RequestMethod.POST)
		public ResponseEntity getReceivableReportByCostCenter(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List trialBalanceList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getReceivableReportByCostCenter(@RequestBody RequestWrapper requestWrapper)" );
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					trialBalanceList=projectDetailsService.getReceivableReportByCostCenter(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getProjectAcronyms());
				 jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(trialBalanceList.size()>0){
				  jSonVO.setObjectList(trialBalanceList);
				  jSonVO.setStatusCode(1);
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getReceivableReportByCostCenter "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}		
				
			
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getReceivableReportByCostCenter(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/monthWiseReportForReceivablesForCostCategory", method = RequestMethod.POST)
		public ResponseEntity getMonthWiseBreakUpReportForReceivablesForCostCategory(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			MonthwiseChartVO rptVO=new MonthwiseChartVO();
				logger.debug("Entry : public ResponseEntity getMonthWiseBreakUpReportForReceivablesForCostCategory(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  rptVO=projectDetailsService.getMonthWiseBreakUpReportForReceivablesForCostCategory(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),requestWrapper.getProjectCategoryID());
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getMonthWiseBreakUpReportForReceivablesForCostCategory "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getMonthWiseBreakUpReportForReceivablesForCostCategory(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/receivablesByCostCategory", method = RequestMethod.POST)
		public ResponseEntity getReceivableReportByCostCategory(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List trialBalanceList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getReceivableReportByCostCategory(@RequestBody RequestWrapper requestWrapper)" );
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					trialBalanceList=projectDetailsService.getReceivableReportByCostCategory(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getProjectCategoryID());
				 jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(trialBalanceList.size()>0){
				  jSonVO.setObjectList(trialBalanceList);
				  jSonVO.setStatusCode(1);
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getReceivableReportByCostCategory "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}		
				
			
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getReceivableReportByCostCategory(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/trialBalanceDetails", method = RequestMethod.POST)
		public ResponseEntity getTrialBalanceDetailsReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List trialBalanceList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getTrialBalanceDetailsReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					trialBalanceList=trialBalanceService.getLedgerReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getGroupID(),0);
				 jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(trialBalanceList.size()>0){
				  jSonVO.setObjectList(trialBalanceList);
				  jSonVO.setStatusCode(1);
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getTrialBalanceDetailsReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}		
				
			
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getTrialBalanceReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/trialBalanceGroupDetails", method = RequestMethod.POST)
		public ResponseEntity getTrialBalanceGroupDetailsReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List trialBalanceList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getTrialBalanceGroupDetailsReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					trialBalanceList=trialBalanceService.getGroupLedgerReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getGroupID());
				 jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(trialBalanceList.size()>0){
				  jSonVO.setObjectList(trialBalanceList);
				  jSonVO.setStatusCode(1);
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getTrialBalanceGroupDetailsReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}		
				
			
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getTrialBalanceGroupDetailsReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/trialBalanceDetailsFromCategory", method = RequestMethod.POST)
		public ResponseEntity getTrialBalanceDetailsReportFromCategory(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List trialBalanceList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getTrialBalanceDetailsReportFromCategory(@RequestBody RequestWrapper requestWrapper)" );
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					trialBalanceList=trialBalanceService.getLedgerReportFromCategory(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getGroupID(),0);
				 jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(trialBalanceList.size()>0){
				  jSonVO.setObjectList(trialBalanceList);
				  jSonVO.setStatusCode(1);
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getTrialBalanceDetailsReportFromCategory "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}		
				
			
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getTrialBalanceDetailsReportFromCategory(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/balanceSheetDetailsForLabel", method = RequestMethod.POST)
		public ResponseEntity getBalanceSheetDetailsForLabelReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List trialBalanceList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getBalanceSheetDetailsForLabelReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					trialBalanceList=balanceSheetService.getBalanceSheetDetailsForLabelReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getGroupID(),0);
				 jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(trialBalanceList.size()>0){
				  jSonVO.setObjectList(trialBalanceList);
				  jSonVO.setStatusCode(1);
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getBalanceSheetDetailsForLabelReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}		
				
			
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getBalanceSheetDetailsForLabelReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/profitLossDetailsForLabel", method = RequestMethod.POST)
		public ResponseEntity getProfitLossDetailsForLabelReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List trialBalanceList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getProfitLossDetailsForLabelReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					trialBalanceList=reportService.getProfitLossDetailsForLabelReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getGroupID(),0);
				 jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(trialBalanceList.size()>0){
				  jSonVO.setObjectList(trialBalanceList);
				  jSonVO.setStatusCode(1);
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getProfitLossDetailsForLabelReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}		
				
			
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getProfitLossDetailsForLabelReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/cashBasedLedgerReport", method = RequestMethod.POST)
		public ResponseEntity getCashBasedLedgerReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List trialBalanceList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO cashBasedLedgerReport(@RequestBody RequestWrapper requestWrapper)"+requestWrapper.getGroupID() );
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				 trialBalanceList=reportService.getCashBasedLedgerReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getGroupID(),requestWrapper.getType());
				 jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(trialBalanceList.size()>0){
				  jSonVO.setObjectList(trialBalanceList);
				  jSonVO.setStatusCode(1);
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getTrialBalanceDetailsReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}		
				
			
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getTrialBalanceReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		/*------------------API for getting monthWiseBreakUpReport -----------------*/
	    @RequestMapping(value="/report/incomeExpenseForChart", method = RequestMethod.POST)
		public ResponseEntity getIncomeExpenseReportForChart(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			JSONResponseVO jsonResponseVO=new JSONResponseVO();
			List reportList=new ArrayList();
			logger.debug("Entry : public ResponseEntity getIncomeExpenseReportForChart(@RequestBody RequestWrapper requestWrapper)")	;
			try {
	            
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));				
				   reportList=reportService.getMonthWiseGroupReportForChart(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),"R");
				 
				  if(reportList.size()>0){
					jsonResponseVO.setObjectList(reportList);
					jsonResponseVO.setStatusCode(200);
				   
				  }else{
					  apiResponse.setStatusCode(400);
					  apiResponse.setMessage("No events are available for given organization ");
					  return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				  }
				
			} catch (Exception e) {
				logger.error("Exception in getIncomeExpenseReportForChart "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity getIncomeExpenseReportForChart(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jsonResponseVO, HttpStatus.OK);	
	 
		}

	    /*------------------API for getting monthWiseBreakUpReport of Receipt Payment -----------------*/
	    @RequestMapping(value="/report/receiptPaymentForChart", method = RequestMethod.POST)
		public ResponseEntity getReceiptPaymentReportForChart(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			JSONResponseVO jsonResponseVO=new JSONResponseVO();
			List reportList=new ArrayList();
			logger.debug("Entry : public ResponseEntity getReceiptPaymentReportForChart(@RequestBody RequestWrapper requestWrapper)")	;
			try {
	            
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));			
				   reportList=reportService.getMonthWiseReceiptPaymentGroupReportForChart(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),"R");
				 
				  if(reportList.size()>0){
					jsonResponseVO.setObjectList(reportList);
					jsonResponseVO.setStatusCode(200);
				   
				  }else{
					  apiResponse.setStatusCode(400);
					  apiResponse.setMessage("No events are available for given organization ");
					  return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				  }
				
			} catch (Exception e) {
				logger.error("Exception in getReceiptPaymentReportForChart "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public ResponseEntity getReceiptPaymentReportForChart(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jsonResponseVO, HttpStatus.OK);	
	 
		}
		
		@RequestMapping(value="/report/accountStats", method = RequestMethod.POST)
		public ResponseEntity getAccountStatus(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			DashBoardAccountVO dbVO=new DashBoardAccountVO();
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody DashBoardAccountVO getAccountStatus(@RequestBody RequestWrapper requestWrapper)")	;
			try {
							
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					dbVO =  accountDashBoardService.getAccountsDashBoard(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate());	
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in accountStats "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}		
				
			logger.debug("Exit : public @ResponseBody DashBoardAccountVO getAccountStatus(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(dbVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/getFinanceAccountStatus", method = RequestMethod.POST)
		public ResponseEntity getFinanceAccountStatus(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			DashBoardAccountVO dbVO=new DashBoardAccountVO();
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody DashBoardAccountVO getFinanceAccountStatus(@RequestBody RequestWrapper requestWrapper)")	;
			try {
							
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					dbVO =  accountDashBoardService.getFinanceAccountsDashBoard(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate());	
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getFinanceAccountStatus "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}		
				
			logger.debug("Exit : public @ResponseBody DashBoardAccountVO getFinanceAccountStatus(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(dbVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/report/paymentReceiptList", method = RequestMethod.POST)
		public ResponseEntity getPaymentReceiptListReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List trialBalanceList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getPaymentReceiptListReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					trialBalanceList=transactionService.getPaymentReceiptListReport(requestWrapper.getOrgID(),requestWrapper.getLedgerID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getType());
				 jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(trialBalanceList.size()>0){
				  jSonVO.setObjectList(trialBalanceList);
				  jSonVO.setStatusCode(1);
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getPaymentReceiptListReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}		
				
			
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getPaymentReceiptListReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		/*======================
		 * Sample Parameters
		 * {"societyID":"11","fromDate":"2018-04-01","toDate":"2018-04-30","transactionType":"all","statusDescription":"all"}
		 * 
		 * */
		
		
		@RequestMapping(value="/report/allTransactionReport", method = RequestMethod.POST)
		public ResponseEntity getAllTransactionReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
			JsonRespAccountsVO jsoVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public ResponseEntity getAllTransactionReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO)")	;
			try {
							
				if(txVO.getSocietyID()>0 && txVO.getFromDate().length()>0 && txVO.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				    txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
				    
					int transactionCount=transactionService.getAllTransactionsCount(txVO);
					if(transactionCount==0){
						apiResponse.setStatusCode(525);
						apiResponse.setMessage("No transactions found for this period.");
						return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
						
					}else if(transactionCount<=2000){
						List transactionsList=transactionService.getAllTransactionsList(txVO);
						jsoVO.setStatusCode(200);
						jsoVO.setObjectList(transactionsList);
											
					}else{
						apiResponse.setStatusCode(525);
						apiResponse.setMessage("The transactions count for this period is high, please select a shorter period.");
						return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
					}
					
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getAllTransactionReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}		
				
			logger.debug("Exit : public ResponseEntity getAllTransactionReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO)")	;						
			return new ResponseEntity(jsoVO, HttpStatus.OK);
	 
		}
		
		/*======================
		 * Sample Parameters
		 * {"societyID":"11","fromDate":"2018-04-01","toDate":"2018-04-30","transactionType":"all","statusDescription":"all","ledgerID":"18"}
		 * 
		 * */
		
		@RequestMapping(value="/report/allNonRecordedTransactionsForLedger", method = RequestMethod.POST)
		public ResponseEntity getAllNonRecordedTransactionsReportForLedger(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
			JsonRespAccountsVO jsoVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry :public ResponseEntity getAllNonRecordedTransactionsReportForLedger(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO)")	;
			try {
							
				if(txVO.getSocietyID()>0 && txVO.getFromDate().length()>0 && txVO.getToDate().length()>0){
				   UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
				
						List transactionsList=transactionService.getAllNonRecordedTransactionsReportForLedger(txVO);
						jsoVO.setStatusCode(200);
						jsoVO.setObjectList(transactionsList);				
					
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getAllNonRecordedTransactionsReportForLedger "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}		
				
			logger.debug("Exit : public ResponseEntity getAllNonRecordedTransactionsReportForLedger(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO)")	;						
			return new ResponseEntity(jsoVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/auditReport", method = RequestMethod.POST)
		public ResponseEntity getAuditReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportVO balanceSheetVO=new ReportVO();
			ReportVO profitLossVO=new ReportVO();
			SocietyVO societyVO=new SocietyVO();
			ReportDetailsVO reportVO=new ReportDetailsVO();
				logger.debug("Entry : public @ResponseBody ReportVO getAuditReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  profitLossVO=reportService.getIncomeExpenseReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),0,"S");
				  balanceSheetVO=balanceSheetService.getBalanceSheet(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate());
				  societyVO=societyService.getSocietyDetails(requestWrapper.getOrgID());
				  reportVO.setSocietyVO(societyVO);
				  reportVO.setBalanceSheetVO(balanceSheetVO);
				  reportVO.setProfitLossVO(profitLossVO);
				  
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getAuditReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getAuditReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(reportVO, HttpStatus.OK);
	 
		}
		//============== Budget Reports ===============//
		
		@RequestMapping(value="/report/addBudgetDetails", method = RequestMethod.POST)
		public ResponseEntity addBudgetDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody BudgetDetailsVO budgetVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportDetailsVO reportVO=new ReportDetailsVO();
				logger.debug("Entry : public @ResponseBody ReportVO addBudgetDetails(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(budgetVO.getOrgID()>0 ){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					budgetVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				 int success=budgetService.addBudgetDetails(budgetVO);
				 if(success>0){
					 apiResponse.setStatusCode(200);
					 apiResponse.setMessage("Successfully added budget details ");
				 }else{
					 apiResponse.setStatusCode(406);
					 apiResponse.setMessage("Unable to add budget details");
					 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
					 }
				 
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in addBudgetDetails "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO addBudgetDetails(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/updateBudgetDetails", method = RequestMethod.POST)
		public ResponseEntity updateBudgetDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody BudgetDetailsVO budgetVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportDetailsVO reportVO=new ReportDetailsVO();
				logger.debug("Entry : public @ResponseBody ReportVO updateBudgetDetails(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(budgetVO.getOrgID()>0 ){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					budgetVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				 int success=budgetService.updateBudgetDetails(budgetVO);
				 if(success>0){
					 apiResponse.setStatusCode(200);
					 apiResponse.setMessage("Successfully updated budget details ");
				 }else{
					 apiResponse.setStatusCode(406);
					 apiResponse.setMessage("Unable to update budget details");
					 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
					 }
				 
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in updateBudgetDetails "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO updateBudgetDetails(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/deleteBudgetDetails", method = RequestMethod.POST)
		public ResponseEntity deleteBudgetDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody BudgetDetailsVO budgetVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportDetailsVO reportVO=new ReportDetailsVO();
				logger.debug("Entry : public @ResponseBody ReportVO updateBudgetDetails(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(budgetVO.getOrgID()>0 ){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					budgetVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				 int success=budgetService.deleteBudgetDetails(budgetVO);
				 if(success>0){
					 apiResponse.setStatusCode(200);
					 apiResponse.setMessage("Successfully deleted budget details ");
				 }else{
					 apiResponse.setStatusCode(406);
					 apiResponse.setMessage("Unable to delete budget details");
					 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
					 }
				 
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in deleteBudgetDetails "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO deleteBudgetDetails(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/getBudgetDetails", method = RequestMethod.POST)
		public ResponseEntity getBudgetDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody BudgetDetailsVO budgetVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			BudgetDetailsVO budgetDetailsVO=new BudgetDetailsVO();
			ReportDetailsVO reportVO=new ReportDetailsVO();
				logger.debug("Entry : public @ResponseBody ReportVO getBudgetDetails(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(budgetVO.getOrgID()>0 ){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					budgetVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				budgetDetailsVO=budgetService.getBudgetDetails(budgetVO);
			
				 
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getBudgetDetails "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getBudgetDetails(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(budgetDetailsVO, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/report/getBudgetDetailsList", method = RequestMethod.POST)
		public ResponseEntity getBudgetDetailsList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody BudgetDetailsVO budgetVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportDetailsVO reportVO=new ReportDetailsVO();
			List budgetList=null;
				logger.debug("Entry : public @ResponseBody ReportVO getBudgetDetailsList(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(budgetVO.getOrgID()>0 ){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					budgetVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				 budgetList=budgetService.getBudgetDetailsList(budgetVO);
				 if(budgetList.size()>0){
					 reportVO.setObjectList(budgetList);
				 }else{
					 apiResponse.setStatusCode(200);
					 apiResponse.setMessage("No records found");
					 return new ResponseEntity(reportVO, HttpStatus.OK);
					 }
				  
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getBudgetDetailsList "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getBudgetDetailsList(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(reportVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/addBudgetItemDetails", method = RequestMethod.POST)
		public ResponseEntity addBudgetItemDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody BudgetDetailsVO budgetVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportDetailsVO reportVO=new ReportDetailsVO();
				logger.debug("Entry : public @ResponseBody ReportVO addBudgetItemDetails(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(budgetVO.getOrgID()>0 ){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					budgetVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				 int success=budgetService.addBudgetItemDetails(budgetVO);
				 if(success>0){
					 apiResponse.setStatusCode(200);
					 apiResponse.setMessage("Successfully added budget items "+success);
				 }else{
					 apiResponse.setStatusCode(406);
					 apiResponse.setMessage("Unable to add budget details");
					 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
					 }
				 
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in addBudgetItemDetails "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO addBudgetItemDetails(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/addBulkBudgetItems", method = RequestMethod.POST)
		public ResponseEntity addBulkBudgetItemDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody BudgetDetailsVO budgetVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportDetailsVO reportVO=new ReportDetailsVO();
				logger.debug("Entry : public @ResponseBody ReportVO addBulkBudgetItemDetails(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(budgetVO.getOrgID()>0 ){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					budgetVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				 int success=budgetService.addBulkBudgetItems(budgetVO);
				 if(success>0){
					 apiResponse.setStatusCode(200);
					 apiResponse.setMessage("Successfully added budget items "+success);
				 }else{
					 apiResponse.setStatusCode(406);
					 apiResponse.setMessage("Unable to add budget details");
					 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
					 }
				 
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in addBulkBudgetItemDetails "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO addBulkBudgetItemDetails(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/updateBudgetItemDetails", method = RequestMethod.POST)
		public ResponseEntity updateBudgetItemDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody BudgetDetailsVO budgetVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportDetailsVO reportVO=new ReportDetailsVO();
				logger.debug("Entry : public @ResponseBody ReportVO updateBudgetItemDetails(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(budgetVO.getOrgID()>0 ){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					budgetVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				 int success=budgetService.updateBudgetItemDetails(budgetVO);
				 if(success>0){
					 apiResponse.setStatusCode(200);
					 apiResponse.setMessage("Successfully updated budget item details ");
				 }else{
					 apiResponse.setStatusCode(406);
					 apiResponse.setMessage("Unable to add budget details");
					 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
					 }
				 
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in updateBudgetItemDetails "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO updateBudgetItemDetails(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/deleteBudgetItemDetails", method = RequestMethod.POST)
		public ResponseEntity deleteBudgetItemDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody BudgetDetailsVO budgetVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportDetailsVO reportVO=new ReportDetailsVO();
				logger.debug("Entry : public @ResponseBody ReportVO deleteBudgetItemDetails(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(budgetVO.getOrgID()>0 ){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					budgetVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				 int success=budgetService.deleteBudgetItemDetails(budgetVO);
				 if(success>0){
					 apiResponse.setStatusCode(200);
					 apiResponse.setMessage("Successfully deleted budget details ");
				 }else{
					 apiResponse.setStatusCode(406);
					 apiResponse.setMessage("Unable to add budget details");
					 return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
					 }
				 
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in deleteBudgetDetails "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO deleteBudgetItemDetails(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/getBudgetItemDetailsList", method = RequestMethod.POST)
		public ResponseEntity getBudgetItemDetailsList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody BudgetDetailsVO budgetVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportDetailsVO reportVO=new ReportDetailsVO();
			List budgetList=null;
				logger.debug("Entry : public @ResponseBody ReportVO getBudgetItemDetailsList(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(budgetVO.getOrgID()>0 ){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					budgetVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					budgetList=budgetService.getBudgetLineItemList(budgetVO.getOrgID(), budgetVO.getBudgetID(), budgetVO.getGroupID(), budgetVO.getFromDate(), budgetVO.getToDate(), budgetVO.getFrequency());
				 if(budgetList.size()>0){
					 reportVO.setObjectList(budgetList);
				 } else{
						 apiResponse.setStatusCode(200);
						 apiResponse.setMessage("No records found");
						 return new ResponseEntity(reportVO, HttpStatus.OK);
						 }	
				  
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getBudgetItemDetailsList "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getBudgetItemDetailsList(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(reportVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/getBudgetItemDetails", method = RequestMethod.POST)
		public ResponseEntity getBudgetItemDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody BudgetDetailsVO budgetVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			BudgetDetailsVO budgetDetailsVO=new BudgetDetailsVO();
			ReportDetailsVO reportVO=new ReportDetailsVO();
				logger.debug("Entry : public @ResponseBody ReportVO getBudgetItemDetails(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(budgetVO.getOrgID()>0 ){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					budgetVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				budgetDetailsVO=budgetService.getBudgetItemDetails(budgetVO);
			
				 
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getBudgetItemDetails "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getBudgetItemDetails(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(budgetDetailsVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/budgetProfitLoss", method = RequestMethod.POST)
		public ResponseEntity getBudgetedProfitLossReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportVO rptVO=new ReportVO();
				logger.debug("Entry : public @ResponseBody ReportVO getBudgetedProfitLossReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  rptVO=budgetService.getBudgetedProfitLossReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate());
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getBudgetedProfitLossReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getBudgetedProfitLossReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/budgetedReport", method = RequestMethod.POST)
		public ResponseEntity getBudgetedBudgetReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody BudgetDetailsVO requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			ReportVO rptVO=new ReportVO();
				logger.debug("Entry : public @ResponseBody ReportVO getBudgetedBudgetReport(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  rptVO=budgetService.getBudgetedReport(requestWrapper);
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getBudgetedBudgetReport "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getBudgetedBudgetReport(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/budgetedReportForGroup", method = RequestMethod.POST)
		public ResponseEntity getBudgetedBudgetReportForGroup(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody BudgetDetailsVO requestWrapper) {
			APIResponseVO apiResponse=new APIResponseVO();
			BudgetDetailsVO rptVO=new BudgetDetailsVO();
				logger.debug("Entry : public @ResponseBody ReportVO getBudgetedBudgetReportForGroup(@RequestBody RequestWrapper requestWrapper)" );
			try {
				
				if(requestWrapper.getOrgID()>0 && requestWrapper.getBudgetDate().length()>0 ){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				  rptVO=budgetService.getBudgetedReportForGroup(requestWrapper);
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getBudgetedBudgetReportForGroup "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}
			logger.debug("Exit : public @ResponseBody ReportVO getBudgetedBudgetReportForGroup(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(rptVO, HttpStatus.OK);
	 
		}
		
		/*------------------Get budgeted projects/ cost centers for a organization------------------*/
		@RequestMapping(value="/report/budgetedProjectDetailsList",  method = RequestMethod.POST  )			
		public ResponseEntity getBudgetedProjectListWithDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
			JsonRespAccountsVO json=new JsonRespAccountsVO();
			List projectList=new ArrayList();
				logger.debug("Entry : public ResponseEntity getBudgetedProjectListWithDetails(@RequestBody ProjectDetailsVO projectVO)")	;
			try {
				logger.debug("Here society ID "+projectVO.getOrgID());
				if(projectVO.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				projectList=budgetService.getBudgetedProjectListWithDetails(projectVO.getOrgID(), projectVO.getFromDate(),projectVO.getToDate());
				
				if(projectList.size()>0){
					json.setObjectList(projectList);
					json.setStatusCode(200);
				}
					
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

				logger.debug("Result of public ResponseEntity getBudgetedProjectListWithDetails(@RequestBody ProjectDetailsVO projectVO) : "+success);				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getBudgetedProjectListWithDetails(@RequestBody ProjectDetailsVO projectVO)")	;						
			return new ResponseEntity(json, HttpStatus.OK);
	 
		}
		
		
		/*------------------Get budgeted projects/ cost centers for a organization------------------*/
		@RequestMapping(value="/report/budgetedProjectDetails",  method = RequestMethod.POST  )			
		public ResponseEntity getBudgetedProjectWithDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody BudgetDetailsVO budgetVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
			JsonRespAccountsVO json=new JsonRespAccountsVO();
			ProjectDetailsVO projectVO=new ProjectDetailsVO();
			List projectList=new ArrayList();
				logger.debug("Entry : public ResponseEntity getBudgetedProjectWithDetails(@RequestBody BudgetDetailsVO budgetVO)")	;
			try {
				logger.debug("Here society ID "+budgetVO.getOrgID());
				if(budgetVO.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					budgetVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				projectVO=budgetService.getBudgetedProjectWithDetails(budgetVO.getOrgID(), budgetVO.getFromDate(), budgetVO.getToDate(),budgetVO.getProjectAcronyms());
				
				
					
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

				logger.debug("Result of public ResponseEntity getBudgetedProjectWithDetails(@RequestBody BudgetDetailsVO budgetVO) : "+success);				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getBudgetedProjectWithDetails(@RequestBody BudgetDetailsVO budgetVO)")	;						
			return new ResponseEntity(projectVO, HttpStatus.OK);
	 
		}
		
		
		/*---------------------------- Transactions Module insert,update,delete ------------------*/
		
		@RequestMapping(value="/insert/transaction",  method = RequestMethod.POST  )			
		public ResponseEntity insertTransaction(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody int insertTransaction( TransactionVO txVO)")	;
			try {
				logger.info("insertTransaction:  org ID: "+txVO.getSocietyID()+" Amount: "+txVO.getAmount());
				if(txVO.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));	
			       txVO=transactionService.addTransaction(txVO, txVO.getSocietyID());
			       
			   	if(txVO.getStatusCode()!=200){
					apiResponse.setStatusCode(txVO.getStatusCode());
					apiResponse.setMessage(txVO.getErrorMessage());
					logger.info("Unable to insert transaction details Status Code: "+txVO.getStatusCode());
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
					
				}else{
					
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Transaction added successfully ");
							
				}
			       
				
				
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.info("Result of Transaction insertion : "+txVO.getTransactionID());				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				
			}
			logger.debug("Exit : public @ResponseBody int insertTransaction( TransactionVO txVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		

		
		
		//Testing purpose only
		/*@RequestMapping(value="/accept/{societyID},{ledgerID}", method = RequestMethod.GET)
		public @ResponseBody DashBoardAccountVO getTest(@PathVariable int societyID,@PathVariable int ledgerID) {
			DashBoardAccountVO dbVO=new DashBoardAccountVO();
			EncryptDecryptUtility endeUtil=new EncryptDecryptUtility();
				logger.debug("Entry : public @ResponseBody DashBoardAccountVO getAccountStatus(@PathVariable int societyID)")	;
			try {
				logger.info("Society Stats: Society ID: "+societyID);				
				String entryString=societyID+","+ledgerID;
				logger.info("Before Encrypting string "+entryString);
				String encryptString=endeUtil.encrypt(entryString);
				logger.info("After Encrypting String "+encryptString);
				
				logger.info("After Decrypting the String "+endeUtil.decrypt(encryptString));
				String abc=endeUtil.decrypt(encryptString);
				String[] animalsArray = abc.split(",");
				
				logger.info("Here "+animalsArray[1]);
				
				
				
				
			} catch (Exception e) {
				
				logger.error("Exception  "+e);
			}
			logger.debug("Exit : public @ResponseBody DashBoardAccountVO getAccountStatus(@PathVariable int societyID)")	;						
			return dbVO;
	 
		}
		*/
	   

		/*------------------Insert Receipt transaction API------------------*/
		@RequestMapping(value="/insert/receipt",  method = RequestMethod.POST  )			
		public ResponseEntity insertReceiptTransaction(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody int insertReceiptTransaction(@RequestBody TransactionVO txVO) ")	;
			try {
				logger.info("insertReceiptTransaction :  org ID: "+txVO.getSocietyID());
				if(txVO.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));	
				txVO=invoiceService.addReceptForInvoice(txVO.getSocietyID() ,txVO,txVO.getInvoiceDetailsVO());
				
				if(txVO.getStatusCode()!=200){
					apiResponse.setStatusCode(txVO.getStatusCode());
					apiResponse.setMessage(txVO.getErrorMessage());
					logger.info("Unable to insert receipt transaction of transaction ID : "+txVO.getTransactionID()+ " Status Code: "+txVO.getStatusCode());
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
					
				}else {
					logger.info("Result of inserReceiptTransaction :  Sucess  and transaction ID :"+txVO.getTransactionID());	
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Transaction added successfully ");
				}
				
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

							
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int insertReceiptTransaction(@RequestBody TransactionVO txVO) ")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/insert/invoice",  method = RequestMethod.POST  )			
		public ResponseEntity insertInvoice(@RequestBody TransactionVO txVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody int insertTransaction( TransactionVO txVO)")	;
			try {
				logger.info("insertTransaction:  org ID: "+txVO.getSocietyID()+" Amount: "+txVO.getAmount());
				if(txVO.getSocietyID()>0){
			       txVO=transactionService.addInvoice(txVO, txVO.getSocietyID());
			       
			   	if(txVO.getStatusCode()!=200){
					apiResponse.setStatusCode(txVO.getStatusCode());
					apiResponse.setMessage(txVO.getErrorMessage());
					logger.info("Unable to insert transaction details Status Code: "+txVO.getStatusCode());
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
					
				}else{
					
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Transaction added successfully ");
							
				}
			       
				
				
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.info("Result of Transaction insertion : "+txVO.getTransactionID());				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				
			}
			logger.debug("Exit : public @ResponseBody int insertTransaction( TransactionVO txVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		
		/*------------------Insert Receipt against Invoice / Bill  transaction API------------------*/
		@RequestMapping(value="/insert/invoiceReceipt",  method = RequestMethod.POST  )			
		public ResponseEntity insertInvoiceReceiptTransaction(@RequestBody TransactionVO txVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody int insertReceiptTransaction(@RequestBody TransactionVO txVO) ")	;
			try {
				logger.info("Here society ID "+txVO.getSocietyID());
				if(txVO.getSocietyID()>0){
				txVO=invoiceService.addInvoiceReceptTransaction(txVO.getSocietyID(), txVO);
				
				if(txVO.getStatusCode()!=200){
					apiResponse.setStatusCode(txVO.getStatusCode());
					apiResponse.setMessage(txVO.getErrorMessage());
					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
					
				}else {
					logger.info("Result of inserReceiptTransaction :  Sucess  and transaction ID :"+txVO.getTransactionID());	
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Transaction added successfully ");
				
				}
				
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

								
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int insertReceiptTransaction(@RequestBody TransactionVO txVO) ")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/insert/memberEntry",  method = RequestMethod.POST  )			
		public ResponseEntity addOnlinePayment(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
			int genTxID=0;
			APIResponseVO apiResponseVO=new APIResponseVO();
				logger.info("Entry : public @ResponseBody int addOnlinePayment( TransactionVO txVO)")	;
			try {
				logger.info("Member Offline transaction initiated for society ID "+txVO.getSocietyID()+" for amount "+txVO.getAmount());
				if(txVO.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
					
				LedgerEntries debtLedgerEntry=new LedgerEntries();
				LedgerEntries credLedgerEntry=new LedgerEntries();
				AccountHeadVO oppLedgerVO=new AccountHeadVO();
				List journalEntries=txVO.getLedgerEntries();
				txVO.setLedgerEntries(null);
				
				
				if(txVO.getTransactionMode().equalsIgnoreCase("Cash")){
					oppLedgerVO=ledgerService.getBankCashPrimaryLedger(txVO.getSocietyID(),"C");
				}else{
				
					oppLedgerVO=ledgerService.getBankCashPrimaryLedger(txVO.getSocietyID(),"b");
				}
				
				logger.info(" Opposite Ledger ID: "+oppLedgerVO.getLedgerID());
				
				if((oppLedgerVO.getLedgerID()!=0)&& (txVO.getTransactionType().equalsIgnoreCase("Receipt"))){
					for(int i=0;journalEntries.size()>i;i++){
						LedgerEntries lEntries= (LedgerEntries) journalEntries.get(i);
						
						if(lEntries.getCreditDebitFlag().equalsIgnoreCase("D")){
							journalEntries.remove(i);
							i--;
						}else {
							credLedgerEntry=lEntries;
							
						}
						
					}
					
					debtLedgerEntry.setLedgerID(oppLedgerVO.getLedgerID());
					debtLedgerEntry.setCreditDebitFlag("D");
					debtLedgerEntry.setAmount(credLedgerEntry.getAmount());
					txVO.setLedgerID(credLedgerEntry.getLedgerID());
					
					journalEntries.add(0,debtLedgerEntry);
				}else if((oppLedgerVO.getLedgerID()!=0)&& (txVO.getTransactionType().equalsIgnoreCase("Payment"))){
					for(int i=0;journalEntries.size()>i;i++){
						LedgerEntries lEntries= (LedgerEntries) journalEntries.get(i);
						
						if(lEntries.getCreditDebitFlag().equalsIgnoreCase("C")){
							journalEntries.remove(i);
							i--;
						}else {
							credLedgerEntry=lEntries;
							
						}
						
					}
					
					debtLedgerEntry.setLedgerID(oppLedgerVO.getLedgerID());
					debtLedgerEntry.setCreditDebitFlag("C");
					debtLedgerEntry.setAmount(credLedgerEntry.getAmount());
					txVO.setLedgerID(credLedgerEntry.getLedgerID());
					
					journalEntries.add(0,debtLedgerEntry);
				}
				txVO.setLedgerEntries(journalEntries);
				
				logger.info("Ledger Entry size: "+txVO.getLedgerEntries().size()+" Amount: "+txVO.getAmount()+" Memebr ledgerID:  "+txVO.getLedgerID());								
				if(txVO.getTransactionType().equalsIgnoreCase("Receipt")){
				    txVO=invoiceService.addReceptForInvoiceForOnlinePayment(txVO.getSocietyID(), txVO);
				}else{
					txVO=transactionService.addTransaction(txVO, txVO.getSocietyID());
				}
				
				
				if(txVO.getTransactionID()!=0){
						
					logger.info("The Member offine payment for Society ID :"+txVO.getSocietyID()+" of Amount : "+txVO.getAmount()+" for the ledgerID  "+txVO.getLedgerID()+" has been inserted successfully");
				}else{
						logger.info("The Member offine payment for Society ID :"+txVO.getSocietyID()+" of Amount : "+txVO.getAmount()+" for the ledgerID  "+txVO.getLedgerID()+" has been failed, Please look into this transaction");
				}
				
				if(txVO.getStatusCode()!=200){
					apiResponseVO.setStatusCode(txVO.getStatusCode());
					apiResponseVO.setMessage(txVO.getErrorMessage());					
					return new ResponseEntity(apiResponseVO, HttpStatus.NOT_ACCEPTABLE);				
					
				}else{
					apiResponseVO.setStatusCode(200);
					apiResponseVO.setMessage("Transaction added successfully ");
				}
			       
              
				
				
				}else{
					apiResponseVO.setStatusCode(400);
					apiResponseVO.setMessage(configManager.getPropertiesValue("error."+apiResponseVO.getStatusCode()));
					return new ResponseEntity(apiResponseVO, HttpStatus.BAD_REQUEST);
				}
				
				
				
			} catch (Exception e) {
				logger.error("Exception in addOnlinePayment "+e);
				apiResponseVO.setStatusCode(400);
				apiResponseVO.setMessage(configManager.getPropertiesValue("error."+apiResponseVO.getStatusCode()));
				return new ResponseEntity(apiResponseVO, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int addOnlinePayment( TransactionVO txVO)")	;						
			return new ResponseEntity(apiResponseVO, HttpStatus.OK);
	 
		}

		
		/*------------------Update transaction API------------------*/
		@RequestMapping(value="/update/transaction",  method = RequestMethod.POST  )			
		public ResponseEntity updateTransaction(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
			int success=0;
			APIResponseVO apiRespose=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody int updateTransaction( TransactionVO txVO)")	;
			try {
				logger.info("updateTransaction:  orgID:  "+txVO.getSocietyID()+" TX ID: "+txVO.getTransactionID());
				if(txVO.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
			       txVO=transactionService.updateTransaction(txVO, txVO.getSocietyID(), txVO.getTransactionID());
				
			 	if(txVO.getStatusCode()!=200){
					apiRespose.setStatusCode(txVO.getStatusCode());
					apiRespose.setMessage(txVO.getErrorMessage());
					logger.info("Unable to update transaction details of transaction ID : "+txVO.getTransactionID()+ " Status Code: "+txVO.getStatusCode());
					return new ResponseEntity(apiRespose, HttpStatus.NOT_ACCEPTABLE);				
					
				}else {
					apiRespose.setStatusCode(200);
					apiRespose.setMessage("Transaction updated successfully ");
				}
				
				
			}else{
				apiRespose.setStatusCode(400);
				apiRespose.setMessage(configManager.getPropertiesValue("error."+apiRespose.getStatusCode()));
				return new ResponseEntity(apiRespose, HttpStatus.BAD_REQUEST);
			}
				
				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiRespose.setStatusCode(400);
				apiRespose.setMessage(configManager.getPropertiesValue("error."+apiRespose.getStatusCode()));
				return new ResponseEntity(apiRespose, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int updateTransaction( TransactionVO txVO)")	;						
			return new ResponseEntity(apiRespose, HttpStatus.OK);
	 
		}
		
		/*------------------Get transaction API------------------*/
		@RequestMapping(value="/get/transaction",  method = RequestMethod.POST  )			
		public ResponseEntity getTransaction(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
			TransactionVO transactionVO=new TransactionVO();
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody TransactionVO getTransaction(@RequestBody TransactionVO txVO)")	;
			try {
				logger.info("getTransaction : org ID: "+txVO.getSocietyID()+" TX ID: "+txVO.getTransactionID());
				if(txVO.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));	
				  transactionVO=transactionService.getTransctionDetails(txVO.getSocietyID(), txVO.getTransactionID());
				}else{
					logger.info("Unable to fetch transaction details of transaction ID : "+txVO.getTransactionID()+ " Status Code: "+txVO.getStatusCode());
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
									
				} catch (Exception e) {
					logger.error("Exception in getTransaction: "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
			logger.debug("Exit : public @ResponseBody TransactionVO getTransaction(@RequestBody TransactionVO txVO)")	;						
			return new ResponseEntity(transactionVO, HttpStatus.OK);
	 
		}
		
		/*------------------Delete transaction API------------------*/
		@RequestMapping(value="/delete/transaction",  method = RequestMethod.POST  )			
		public ResponseEntity deleteTransaction(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody int deleteTransaction(@RequestBody TransactionVO txVO)")	;
			try {
				logger.info("deleteTransaction org ID: "+txVO.getSocietyID()+" TX ID: "+txVO.getTransactionID());
				if(txVO.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));	
				txVO=transactionService.deleteTransaction(txVO, txVO.getSocietyID(), txVO.getSubGroupID());
				
				if(txVO.getStatusCode()!=200){
					logger.info("Unable to delete transaction ID : "+txVO.getTransactionID()+ " Status Code: "+txVO.getStatusCode());
					apiResponse.setStatusCode(txVO.getStatusCode());
					apiResponse.setMessage(txVO.getErrorMessage());					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
					
				}else {
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Transaction deleted successfully ");
				}
				}else{					
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
						
				
			} catch (Exception e) {
				logger.error("Exception deleteTransaction: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int deleteTransaction(@RequestBody TransactionVO txVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		/*------------------Delete transaction API------------------*/
		@RequestMapping(value="/change/transactionStatus",  method = RequestMethod.POST  )			
		public ResponseEntity changeTransactionStatus(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody int changeTransactionStatus(@RequestBody TransactionVO txVO)")	;
			try {
				logger.info("In Change transaction Status - org ID: "+txVO.getSocietyID()+" TX ID: "+txVO.getTransactionID());
				if(txVO.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));	
				    txVO=transactionService.changeTransactionStatus(txVO, txVO.getSocietyID(), txVO.getSubGroupID());
				
					if(txVO.getStatusCode()!=200){
						logger.info("Unable to update status transaction ID : "+txVO.getTransactionID()+ " Status Code: "+txVO.getStatusCode());
						apiResponse.setStatusCode(txVO.getStatusCode());
						apiResponse.setMessage(txVO.getErrorMessage());					
						return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
						
					}else {
						apiResponse.setStatusCode(200);
						apiResponse.setMessage("Transaction status updated successfully ");
					}
				}else{					
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
						
				
			} catch (Exception e) {
				logger.error("Exception changeTransactionStatus: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int changeTransactionStatus(@RequestBody TransactionVO txVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		/*------------------Generate Transactions API------------------*/
		@RequestMapping(value="/generate/transactions",  method = RequestMethod.POST  )			
		public ResponseEntity generateBulkTransaction(@RequestBody TransactionVO txVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			List transactionList=new ArrayList();
			ScheduledTransactionResponseVO respVO=new ScheduledTransactionResponseVO();
				logger.debug("Entry : public @ResponseBody int generateBulkTransaction(@RequestBody TransactionVO txVO)")	;
			try {
				logger.info("generateBulkTransaction Here society ID "+txVO.getSocietyID()+" TX ID "+txVO.getTransactionID());
				if(txVO.getSocietyID()>0){
				transactionList=transactionService.generateScheduledTransactions(txVO.getSocietyID(),0);
				
				if(transactionList.size()>0){
					respVO=transactionService.addBulkTransactions(transactionList, txVO.getSocietyID());
				
					if(respVO.getSuccessCount()>respVO.getFailureCount()){
						for(String element:respVO.getChargeList()){
							int chargeID=Integer.parseInt(element);
						ChargesVO chargeVO=transactionService.getChargeDetails(chargeID,txVO.getSocietyID());
						int success=transactionService.updateChargeStatus( txVO.getSocietyID(), chargeVO);
						//int updateReminderDate=invoiceService.updateReminderDate(billingVO.getSocietyId());
						}
					}
				
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("No Scheduled charges are available for the given period ");
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
				
				
				}
				
			} catch (Exception e) {
				logger.error("Exception deleteTransaction "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int deleteTransaction(@RequestBody TransactionVO txVO)")	;						
			return new ResponseEntity(respVO, HttpStatus.OK);
	 
		}
		
		
		/*------------------Generate Late fee Transactions API------------------*/
		@RequestMapping(value="/generate/LateFeetransactions",  method = RequestMethod.POST  )			
		public ResponseEntity generateBulkLateFeeTransaction(@RequestBody TransactionVO txVO) {
			APIResponseVO apiResponse=new APIResponseVO();
			List transactionList=new ArrayList();
			ScheduledTransactionResponseVO respVO=new ScheduledTransactionResponseVO();
				logger.debug("Entry : public @ResponseBody int generateBulkTransaction(@RequestBody TransactionVO txVO)")	;
			try {
				logger.info("Create late fee transactions for society ID "+txVO.getSocietyID()+" TX ID "+txVO.getBillingCycleID());
				if(txVO.getSocietyID()>0){
				transactionList=transactionService.generateScheduledLateFeeTransactions(txVO.getSocietyID(),txVO.getBillingCycleID());
				
				if(transactionList.size()>0){
					respVO=transactionService.addBulkTransactions(transactionList, txVO.getSocietyID());
				}
				if(respVO.getSuccessCount()>respVO.getFailureCount()){
					for(String element:respVO.getChargeList()){
						int chargeID=Integer.parseInt(element);
					ChargesVO chargeVO=transactionService.getChargeDetails(chargeID,txVO.getSocietyID());
					int success=transactionService.updateLateFeeChargeDate(txVO.getSocietyID(), chargeVO);
						success=transactionService.updateChargeStatus( txVO.getSocietyID(), chargeVO);
					//int updateReminderDate=invoiceService.updateReminderDate(billingVO.getSocietyId());
					}
				}
				
			
				}
				
				
			} catch (Exception e) {
				logger.error("Exception deleteTransaction "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int deleteTransaction(@RequestBody TransactionVO txVO)")	;						
			return new ResponseEntity(respVO, HttpStatus.OK);
	 
		}
		
		
		
		//================== Ledger Related APIs =================================//
		/*----------- API to get list of groups --------------*/
		@RequestMapping(value="/groupList", method = RequestMethod.POST)
		public ResponseEntity getGroupList(@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List groupList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getGroupList(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				
		  		groupList=ledgerService.getAllGroupList(requestWrapper.getRootID(),requestWrapper.getOrgID());
				
				if(groupList.size()>0){
				 jSonVO.setObjectList(groupList);
				 jSonVO.setStatusCode(1);
				}
							
				
			} catch (Exception e) {
				logger.error("Exception in getGroupList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getGroupList(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);	 
		}
		
		/*----------- API to get list of ledgers --------------*/
		@RequestMapping(value="/ledgersList", method = RequestMethod.POST)
		public ResponseEntity getLedgersList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List ledgerList=new ArrayList();
			SocietyVO societyVo=new SocietyVO();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getLedgersList(@RequestBody RequestWrapper requestWrapper)")	;
			try {
			
				if(requestWrapper.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				 societyVo=societyService.getSocietyDetails(requestWrapper.getOrgID());
				 jSonVO.setSocietyVO(societyVo);
				
				 ledgerList=ledgerService.getLedgerList(requestWrapper.getOrgID(), requestWrapper.getGroupID());
				 jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(ledgerList.size()>0){
				  jSonVO.setObjectList(ledgerList);
				  jSonVO.setStatusCode(1);
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}	
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getLedgersList: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getLedgersList(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		/*----------- API to get list of ledgers --------------*/
		@RequestMapping(value="/ledgersListWithClosingBal", method = RequestMethod.POST)
		public ResponseEntity getLedgersListWithClosingBal(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List ledgerList=new ArrayList();
			SocietyVO societyVo=new SocietyVO();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getLedgersListWithClosingBal(@RequestBody RequestWrapper requestWrapper)")	;
			try {
			
				if(requestWrapper.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));	
				 societyVo=societyService.getSocietyDetails(requestWrapper.getOrgID());
				 jSonVO.setSocietyVO(societyVo);
				
				 ledgerList=ledgerService.getLedgerListWithClosingBalance(requestWrapper.getOrgID(), requestWrapper.getGroupID(),"G",requestWrapper.getFromDate(),requestWrapper.getToDate());
				 jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(ledgerList.size()>0){
				  jSonVO.setObjectList(ledgerList);
				  jSonVO.setStatusCode(1);
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}	
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getLedgersList: "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getLedgersList(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		/*----------- API to get list of ledgers-high level --------------*/
		@RequestMapping(value="/masterLedgersList", method = RequestMethod.POST)
		public ResponseEntity getMasterLedgersList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List ledgerList=new ArrayList();
			SocietyVO societyVo=new SocietyVO();
			logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getMasterLedgersList(@RequestBody RequestWrapper requestWrapper)")	;
			try {
                
				if(requestWrapper.getOrgID()>0 ){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
				   societyVo=societyService.getSocietyDetails(requestWrapper.getOrgID());
				   jSonVO.setSocietyVO(societyVo);
				
				   ledgerList=ledgerService.getMasterLedgerList(requestWrapper.getOrgID(), requestWrapper.getGroupID(),requestWrapper.getCategoryID());
				   jSonVO.setSocietyID(requestWrapper.getOrgID());
				  if(ledgerList.size()>0){
				    jSonVO.setObjectList(ledgerList);
				    jSonVO.setStatusCode(1);
				  }
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getMasterLedgersList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getMasterLedgersList(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);	
	 
		}
		
		/*----------- API to get list of ledgers-high level --------------*/
		@RequestMapping(value="/masterLedgersListRootWise", method = RequestMethod.POST)
		public ResponseEntity getMasterLedgersListRootWise(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List ledgerList=new ArrayList();
			SocietyVO societyVo=new SocietyVO();
			logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getMasterLedgersListRootWise(@RequestBody RequestWrapper requestWrapper)")	;
			try {
                
				if(requestWrapper.getOrgID()>0 ){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
				   societyVo=societyService.getSocietyDetails(requestWrapper.getOrgID());
				   jSonVO.setSocietyVO(societyVo);
				
				   ledgerList=ledgerService.getMasterLedgersListRootWise(requestWrapper.getOrgID(), requestWrapper.getRootID());
				   jSonVO.setSocietyID(requestWrapper.getOrgID());
				  if(ledgerList.size()>0){
				    jSonVO.setObjectList(ledgerList);
				    jSonVO.setStatusCode(1);
				  }
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getMasterLedgersList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getMasterLedgersListRootWise(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);	
	 
		}
		
		/*------------------Insert ledger API------------------*/
		@RequestMapping(value="/insert/ledger",  method = RequestMethod.POST  )			
		public ResponseEntity insertLedger(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody AccountHeadVO ledgerVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public ResponseEntity insertLedger(@RequestBody AccountHeadVO ledgerVO)")	;
			try {
				logger.info("Here society ID "+ledgerVO.getOrgID());
				if(ledgerVO.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					ledgerVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					ledgerVO=ledgerService.insertLedger(ledgerVO, ledgerVO.getOrgID());
				
				if(ledgerVO.getStatusCode()==200){
					apiResponse.setStatusCode(ledgerVO.getStatusCode());
					apiResponse.setMessage(ledgerVO.getMessage());
				}else{
					apiResponse.setStatusCode(ledgerVO.getStatusCode());
					apiResponse.setMessage(ledgerVO.getMessage());
					return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
				}
				
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

				logger.info("Result of public ResponseEntity insertLedger : "+success);				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity insertLedger(@RequestBody AccountHeadVO ledgerVO) ")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		
		/*------------------Update ledger API------------------*/
		@RequestMapping(value="/update/ledger",  method = RequestMethod.POST  )			
		public ResponseEntity updateLedger(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody AccountHeadVO ledgerVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public ResponseEntity updateLedger(@RequestBody AccountHeadVO ledgerVO)")	;
			try {
				logger.info("Here society ID "+ledgerVO.getOrgID());
				if((ledgerVO.getOrgID()>0)||(ledgerVO.getLedgerID()>0)){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					ledgerVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					ledgerVO=ledgerService.updateLedger(ledgerVO, ledgerVO.getOrgID(), ledgerVO.getLedgerID());
				
				if(ledgerVO.getStatusCode()==200){
					apiResponse.setStatusCode(ledgerVO.getStatusCode());
					apiResponse.setMessage(ledgerVO.getMessage());
				}else{
					apiResponse.setStatusCode(ledgerVO.getStatusCode());
					apiResponse.setMessage(ledgerVO.getMessage());
					return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
				}
				
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

				logger.info("Result of public ResponseEntity updateLedger : "+success);				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity updateLedger(@RequestBody AccountHeadVO ledgerVO) ")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		
		/*------------------Delete ledger API------------------*/
		@RequestMapping(value="/delete/ledger",  method = RequestMethod.POST  )			
		public ResponseEntity deleteLedger(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody AccountHeadVO ledgerVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public ResponseEntity deleteLedger(@RequestBody AccountHeadVO ledgerVO)")	;
			try {
				logger.info("Here society ID "+ledgerVO.getOrgID());
				if((ledgerVO.getOrgID()>0)||(ledgerVO.getLedgerID()>0)){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					ledgerVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
					ledgerVO=ledgerService.deleteLedger(ledgerVO.getOrgID(), ledgerVO.getLedgerID());
				
				if(ledgerVO.getStatusCode()==200){
					apiResponse.setStatusCode(ledgerVO.getStatusCode());
					apiResponse.setMessage(ledgerVO.getMessage());
				}else{
					apiResponse.setStatusCode(ledgerVO.getStatusCode());
					apiResponse.setMessage(ledgerVO.getMessage());
					return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
				}
				
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

				logger.info("Result of public ResponseEntity deleteLedger : "+success);				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity deleteLedger(@RequestBody AccountHeadVO ledgerVO) ")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		/*------------------Get ledger details API------------------*/
		@RequestMapping(value="/getLedgerDetails",  method = RequestMethod.POST  )			
		public ResponseEntity getLedgerDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody AccountHeadVO ledgerVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public ResponseEntity getLedgerDetails(@RequestBody AccountHeadVO ledgerVO)")	;
			try {
				logger.info("Here society ID "+ledgerVO.getOrgID());
				if((ledgerVO.getOrgID()>0)&&(ledgerVO.getLedgerID()>0)){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					ledgerVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
					ledgerVO=ledgerService.getLedgerDetails(ledgerVO.getLedgerID(), ledgerVO.getOrgID());
				
				if(ledgerVO.getLedgerID()>0){
					ledgerVO.setStatusCode(200);
					ledgerVO.setMessage("Ledger details fetched.");
				}else{
					apiResponse.setStatusCode(ledgerVO.getStatusCode());
					apiResponse.setMessage(ledgerVO.getMessage());
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getLedgerDetails(@RequestBody AccountHeadVO ledgerVO) ")	;						
			return new ResponseEntity(ledgerVO, HttpStatus.OK);
	 
		}
		
		
		/*----------- API to get list of categories --------------*/
		@RequestMapping(value="/categoryList", method = RequestMethod.POST)
		public ResponseEntity getCategoryList(@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List groupList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getCategoryList(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				
		  		groupList=ledgerService.getCategoryList(requestWrapper.getOrgID());
				
				if(groupList.size()>0){
				 jSonVO.setObjectList(groupList);
				 jSonVO.setStatusCode(1);
				}
							
				
			} catch (Exception e) {
				logger.error("Exception in getCategoryList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getCategoryList(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);	 
		}
		
		
		
		/*------------------Insert Group API------------------*/
		@RequestMapping(value="/insert/group",  method = RequestMethod.POST  )			
		public ResponseEntity insertGroup(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody AccountHeadVO ledgerVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public ResponseEntity insertGroup(@RequestBody AccountHeadVO ledgerVO)")	;
			try {
				logger.info("Here society ID "+ledgerVO.getOrgID());
				if(ledgerVO.getOrgID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					ledgerVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					ledgerVO=ledgerService.insertGroup(ledgerVO, ledgerVO.getOrgID());
				
				if(ledgerVO.getStatusCode()==200){
					apiResponse.setStatusCode(ledgerVO.getStatusCode());
					apiResponse.setMessage(ledgerVO.getMessage());
				}else{
					apiResponse.setStatusCode(ledgerVO.getStatusCode());
					apiResponse.setMessage(ledgerVO.getMessage());
					return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
				}
				
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

				logger.info("Result of public ResponseEntity insertGroup : "+success);				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity insertGroup(@RequestBody AccountHeadVO ledgerVO) ")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		
		/*------------------Update group API------------------*/
		@RequestMapping(value="/update/group",  method = RequestMethod.POST  )			
		public ResponseEntity updateGroup(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody AccountHeadVO ledgerVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public ResponseEntity updateGroup(@RequestBody AccountHeadVO ledgerVO)")	;
			try {
				logger.info("Here society ID "+ledgerVO.getOrgID());
				if((ledgerVO.getOrgID()>0)||(ledgerVO.getAccountGroupID()>0)){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					ledgerVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					ledgerVO=ledgerService.updateGroup(ledgerVO, ledgerVO.getAccountGroupID());
				
				if(ledgerVO.getStatusCode()==200){
					apiResponse.setStatusCode(ledgerVO.getStatusCode());
					apiResponse.setMessage(ledgerVO.getMessage());
				}else{
					apiResponse.setStatusCode(ledgerVO.getStatusCode());
					apiResponse.setMessage(ledgerVO.getMessage());
					return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
				}
				
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

				logger.info("Result of public ResponseEntity updateGroup : "+success);				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity updateGroup(@RequestBody AccountHeadVO ledgerVO) ")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		
		/*------------------Delete group API------------------*/
		@RequestMapping(value="/delete/group",  method = RequestMethod.POST  )			
		public ResponseEntity deleteGroup(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody AccountHeadVO ledgerVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public ResponseEntity deleteGroup(@RequestBody AccountHeadVO ledgerVO)")	;
			try {
				logger.info("Here society ID "+ledgerVO.getOrgID());
				if((ledgerVO.getOrgID()>0)||(ledgerVO.getAccountGroupID()>0)){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					ledgerVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
					ledgerVO=ledgerService.deleteGroup(ledgerVO.getOrgID(), ledgerVO.getAccountGroupID());
				
				if(ledgerVO.getStatusCode()==200){
					apiResponse.setStatusCode(ledgerVO.getStatusCode());
					apiResponse.setMessage(ledgerVO.getMessage());
				}else{
					apiResponse.setStatusCode(ledgerVO.getStatusCode());
					apiResponse.setMessage(ledgerVO.getMessage());
					return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
				}
				
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

				logger.info("Result of public ResponseEntity deleteGroup : "+success);				
				
			} catch (Exception e) {
				logger.error("Exception  "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity deleteGroup(@RequestBody AccountHeadVO ledgerVO) ")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/report/categoryWiseSummary", method = RequestMethod.POST)
		public ResponseEntity getCategoryWiseSummary(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List objectList=new ArrayList();
				logger.debug("Entry : public ResponseEntity getCategoryWiseSummary(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper)" );
			try {
			
				if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				 objectList=trialBalanceService.getCategoryWiseSummary(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),requestWrapper.getRootID());
				 jSonVO.setSocietyID(requestWrapper.getOrgID());
				if(objectList.size()>0){
				 jSonVO.setObjectList(objectList);
				 jSonVO.setStatusCode(200);
				 jSonVO.setFromDate(requestWrapper.getFromDate());
				 jSonVO.setUptoDate(requestWrapper.getToDate());
				}
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getCategoryWiseSummary "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getCategoryWiseSummary(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		
		/*------------------Add society transaction lock API------------------*/
		@RequestMapping(value="/addSocietyTxLock",  method = RequestMethod.POST  )			
		public ResponseEntity addSocietyTransactionLock(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public ResponseEntity addSocietyTransactionLock(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				logger.info("Here society ID "+requestWrapper.getOrgID());
				if((requestWrapper.getOrgID()>0)&&(requestWrapper.getToDate()!=null)&&(requestWrapper.getToDate().length()>0)){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					success=societyService.updateLockDateForSociety(requestWrapper.getOrgID(), requestWrapper.getToDate());
				
				if(success>0){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Society transaction lock has been set to "+dateUtil.getConvetedDate(requestWrapper.getToDate())+" .");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("There is some problem while updating society transaction lock, Please contact helpdesk@esanchalak.com.");
				}
				
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

				logger.info("Result of public ResponseEntity addSocietyTransactionLock : "+success);				
				
			} catch (Exception e) {
				logger.error("Exception in addSocietyTransactionLock "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity addSocietyTransactionLock(@RequestBody RequestWrapper requestWrapper) ")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		
		
		/*------------------Add society GST  transaction lock API------------------*/
		@RequestMapping(value="/addGstTxLock",  method = RequestMethod.POST  )			
		public ResponseEntity addGstTransactionLock(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public ResponseEntity addGstTransactionLock(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				logger.info("Here society ID "+requestWrapper.getOrgID());
				if((requestWrapper.getOrgID()>0)&&(requestWrapper.getToDate()!=null)&&(requestWrapper.getToDate().length()>0)){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					success=societyService.updateLockDateForGSTTx(requestWrapper.getOrgID(), requestWrapper.getToDate());
				
				if(success>0){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Org GST lock has been set to "+dateUtil.getConvetedDate(requestWrapper.getToDate())+" .");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("There is some problem while updating GST transaction lock, Please contact helpdesk@esanchalak.com.");
				}
				
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

				logger.info("Result of public ResponseEntity addGstTransactionLock : "+success);				
				
			} catch (Exception e) {
				logger.error("Exception in addGstTransactionLock "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity addGstTransactionLock(@RequestBody RequestWrapper requestWrapper) ")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		
		/*------------------Add society Audit  transaction lock API------------------*/
		@RequestMapping(value="/addAuditTxLock",  method = RequestMethod.POST  )			
		public ResponseEntity addAuditTransactionLock(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public ResponseEntity addAuditTransactionLock(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				logger.info("Here society ID "+requestWrapper.getOrgID());
				if((requestWrapper.getOrgID()>0)&&(requestWrapper.getToDate()!=null)&&(requestWrapper.getToDate().length()>0)){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					success=societyService.updateLockDateForAuditTx(requestWrapper.getOrgID(), requestWrapper.getToDate());
				
				if(success>0){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Org Audit lock has been set to "+dateUtil.getConvetedDate(requestWrapper.getToDate())+" .");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("There is some problem while updating Audit transaction lock, Please contact helpdesk@esanchalak.com.");
				}
				
				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				

				logger.info("Result of public ResponseEntity addAuditTransactionLock : "+success);				
				
			} catch (Exception e) {
				logger.error("Exception in addAuditTransactionLock "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity addAuditTransactionLock(@RequestBody RequestWrapper requestWrapper) ")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		
		//================== Accounts Status Report APIs =================================//
				/*----------- API to get list of account status --------------*/
				@RequestMapping(value="/report/accountStatus", method = RequestMethod.POST)
				public ResponseEntity getAccountStatusList(@RequestBody RequestWrapper requestWrapper) {
					JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
					APIResponseVO apiResponse=new APIResponseVO();
					List accountStatusList=new ArrayList();
						logger.debug("Entry : public ResponseEntity getAccountStatus(@RequestBody RequestWrapper requestWrapper)")	;
					try {
						
				  		if(requestWrapper.getUserID()!=0){
				  		
				  		accountStatusList=accountingStatusRptService.getAccountingStatusReport(requestWrapper.getUserID(),requestWrapper.getAppID(), requestWrapper.getFromDate(), requestWrapper.getToDate());
						
						if(accountStatusList.size()>0){
						 jSonVO.setObjectList(accountStatusList);
						 jSonVO.setStatusCode(200);
						}
				  		}else{
				  			apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				  			
				  		}
						
					} catch (Exception e) {
						logger.error("Exception in getAccountStatus "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public ResponseEntity getAccountStatus(@RequestBody RequestWrapper requestWrapper)")	;						
					return new ResponseEntity(jSonVO, HttpStatus.OK);	 
				}
		
				/* Sample request parameter for sending accounts status Report are as follows 
					{
					"societyID":"12",
					"userID":"2",
					"fromDate":"2016-04-01",
					"toDate":"2017-03-31",
					"toEmail":"sudarshan_sathe@esanchalak.com",
					"period":"Apr 16 to Mar 17"
					}
				*/
				/*------------------Send Account status Report API------------------*/
				@RequestMapping(value="/report/sendAccountStatus",  method = RequestMethod.POST  )			
				public ResponseEntity sendAccountStatus(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody AccountStatusVO accountStatusVO) {
					int success=0;
					APIResponseVO apiResponse=new APIResponseVO();
						logger.debug("Entry : public ResponseEntity sendAccountStatus(@RequestBody AccountStatusVO accountStatusVO)")	;
					try {
						logger.info("Here society ID "+accountStatusVO.getSocietyID());
						if((accountStatusVO.getSocietyID()>0)&&(accountStatusVO.getUserID()>0)){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							accountStatusVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
							success=accountingStatusRptService.sendAccountStatusReport(accountStatusVO);
						
						if(success>0){
							apiResponse.setStatusCode(200);
							apiResponse.setMessage("Account Status Report has been sent successfully.");
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage("There is some problem while sending Account Status Report, Please contact helpdesk@esanchalak.com.");
						}
						
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
						

						logger.info("Result of public ResponseEntity sendAccountStatus : "+success);				
						
					} catch (Exception e) {
						logger.error("Exception in sendAccountStatus "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public ResponseEntity sendAccountStatus(@RequestBody AccountStatusVO accountStatusVO) ")	;						
					return new ResponseEntity(apiResponse, HttpStatus.OK);
			 
				}
				
				
				/*----------- API to get list of tds vendor ledgers --------------*/
				@RequestMapping(value="/vendorLedgersList", method = RequestMethod.POST)
				public ResponseEntity getVendorLedgersList(@RequestBody RequestWrapper requestWrapper) {
					JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
					APIResponseVO apiResponse=new APIResponseVO();
					List ledgerList=new ArrayList();
					SocietyVO societyVo=new SocietyVO();
						logger.debug("Entry : public ResponseEntity getVendorLedgersList(@RequestBody RequestWrapper requestWrapper)")	;
					try {
					
						if(requestWrapper.getOrgID()>0){
						 societyVo=societyService.getSocietyDetails(requestWrapper.getOrgID());
						 jSonVO.setSocietyVO(societyVo);
						
						 ledgerList=ledgerService.getLedgerListForTDSCalculation(requestWrapper.getOrgID(), 0);
						 jSonVO.setSocietyID(requestWrapper.getOrgID());
						if(ledgerList.size()>0){
						  jSonVO.setObjectList(ledgerList);
						  jSonVO.setStatusCode(1);
						}
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}	
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						logger.error("Exception in getLedgersList: "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public ResponseEntity getVendorLedgersList(@RequestBody RequestWrapper requestWrapper")	;						
					return new ResponseEntity(jSonVO, HttpStatus.OK);
			 
				}
				
				/*----------- API to get due balance of member --------------*/				
				@RequestMapping(value="/dueBalance", method = RequestMethod.POST)
				 public ResponseEntity getInvoiceBalance(@RequestBody RequestWrapper requestWrapper) {
				 			InvoiceDetailsVO invoiceVO=new InvoiceDetailsVO();
				 			APIResponseVO apiResponse=new APIResponseVO();
				 			AccountHeadVO achVo=new AccountHeadVO();
							EncryptDecryptUtility endeUtil=new EncryptDecryptUtility();
							
						logger.debug("Entry : public @ResponseBody InvoiceDetailsVO getInvoiceBalance(@RequestBody RequestWrapper requestWrapper)")	;
						try {
							
							if(requestWrapper.getOrgID()>0 && requestWrapper.getAptID()>0){
							achVo=ledgerService.getLedgerID(requestWrapper.getAptID(), requestWrapper.getOrgID(), "M","APTID");
							Date currentDate=dateUtil.findCurrentDate();
							
							ProfileDetailsVO profileVO=ledgerService.getLedgerProfile(achVo.getLedgerID(),requestWrapper.getOrgID());
				            BankInfoVO bankVO=societyService.getBankDetails(requestWrapper.getOrgID());
				        	if((profileVO.getVirtualAccNo()!=null)&&(profileVO.getVirtualAccNo().length()>0)){
								bankVO.setAccountNo(profileVO.getVirtualAccNo());
							}
															 
							 achVo=ledgerService.getOpeningClosingBalance(requestWrapper.getOrgID(),achVo.getLedgerID(), currentDate.toString(), currentDate.toString(), "L");
							 							
							 invoiceVO.setLedgerID(achVo.getLedgerID());
							 invoiceVO.setBalance(achVo.getClosingBalance());
							 invoiceVO.setBankInfoVO(bankVO);
							 
							// invoiceVO.setSecretKey(endeUtil.encrypt(requestWrapper.getOrgID()+","+achVo.getLedgerID()+","+requestWrapper.getAptID()));
							 							
							}else{
								apiResponse.setStatusCode(400);
								apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
								return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
							}
							logger.debug("Online Payment gateway method for geting dueAmount of Society ID: "+requestWrapper.getOrgID()+" APT ID: "+requestWrapper.getAptID()+" Due: "+achVo.getClosingBalance()+" ");			
							
						} catch (Exception e) {
							
							logger.error("Exception  "+e);
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
							
						}
						logger.debug("Exit : public @ResponseBody InvoiceDetailsVO getInvoiceBalance(@RequestBody RequestWrapper requestWrapper)");						
						return new ResponseEntity(invoiceVO, HttpStatus.OK);
				 
					}
				
				
				/*------------------Get TDS Calculation Report------------------*/
				@RequestMapping(value="/get/tdsCalculationReport",  method = RequestMethod.POST  )			
				public ResponseEntity getTDSCalculationReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
					int success=0;
					APIResponseVO apiResponse=new APIResponseVO();
					JsonRespAccountsVO json=new JsonRespAccountsVO();
					List tdsReportList=new ArrayList();
						logger.debug("Entry : public ResponseEntity getTDSCalculationReport(@RequestBody TransactionVO achVO)")	;
					try {
						logger.info("Here society ID "+txVO.getSocietyID());
						if(txVO.getSocietyID()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
						tdsReportList=invoiceService.getTDSCalculationReport(txVO);
						
						if(tdsReportList.size()>0){
							json.setObjectList(tdsReportList);
							json.setStatusCode(200);
						}
							
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
						

						logger.info("Result of public ResponseEntity getTDSCalculationReport(@RequestBody TransactionVO achVO) : "+success);				
						
					} catch (Exception e) {
						logger.error("Exception  "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public ResponseEntity getTDSCalculationReport(@RequestBody TransactionVO achVO)")	;						
					return new ResponseEntity(json, HttpStatus.OK);
			 
				}
				
				
				//============== Project (Cost Center) Reports ===============//
				
				@RequestMapping(value="/report/addProjectDetails", method = RequestMethod.POST)
				public ResponseEntity addProjectDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					APIResponseVO apiResponse=new APIResponseVO();
					ReportDetailsVO reportVO=new ReportDetailsVO();
						logger.debug("Entry : public @ResponseBody ReportVO addProjectDetails(@RequestBody ProjectDetailsVO projectVO)" );
					try {
						
						if(projectVO.getOrgID()>0 ){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						 int success=projectDetailsService.addProjectDetails(projectVO);
						 if(success>0){
							 apiResponse.setStatusCode(200);
							 apiResponse.setMessage("Successfully added project details ");
						 }else	 if(success==-1){
							 apiResponse.setStatusCode(553);
							 apiResponse.setMessage("Project Accronyms already exists, please select some other accronym. ");
							 return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);

						 }
						 
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
					} catch (Exception e) {
						logger.error("Exception in addProjectDetails "+e);
						apiResponse.setStatusCode(404);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
					}
					logger.debug("Exit : public @ResponseBody ReportVO addProjectDetails(@RequestBody ProjectDetailsVO projectVO)")	;						
					return new ResponseEntity(apiResponse, HttpStatus.OK);
			 
				}
				
				@RequestMapping(value="/report/updateProjectDetails", method = RequestMethod.POST)
				public ResponseEntity updateProjectDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					APIResponseVO apiResponse=new APIResponseVO();
					ReportDetailsVO reportVO=new ReportDetailsVO();
						logger.debug("Entry : public @ResponseBody ReportVO updateProjectDetails(@RequestBody ProjectDetailsVO projectVO)" );
					try {
						
						if(projectVO.getOrgID()>0 ){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
							
						 int success=projectDetailsService.updateProjectDetails(projectVO);
						 if(success>0){
							 apiResponse.setStatusCode(200);
							 apiResponse.setMessage("Successfully updated project details ");
								 
						  }else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage("Unable to update project details.");
							return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
						 }
						 
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}	 
					} catch (Exception e) {
						logger.error("Exception in updateProjectDetails "+e);
						apiResponse.setStatusCode(404);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
					}
					logger.debug("Exit : public @ResponseBody ReportVO updateProjectDetails(@RequestBody RequestWrapper requestWrapper)")	;						
					return new ResponseEntity(apiResponse, HttpStatus.OK);
			 
				}
				
				@RequestMapping(value="/report/deleteProjectDetails", method = RequestMethod.POST)
				public ResponseEntity deleteProjectDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					APIResponseVO apiResponse=new APIResponseVO();
					ReportDetailsVO reportVO=new ReportDetailsVO();
						logger.debug("Entry : public @ResponseBody ReportVO deleteProjectDetails(@RequestBody ProjectDetailsVO projectVO)" );
					try {
						
						if(projectVO.getOrgID()>0 ){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						 int success=projectDetailsService.deleteProjectDetails(projectVO);
						 if(success>0){
							 apiResponse.setStatusCode(200);
							 apiResponse.setMessage("Successfully deleted project details ");
						 }else{
								apiResponse.setStatusCode(400);
								apiResponse.setMessage("Unable to delete project details.");
								return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
						 }
						 
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
					} catch (Exception e) {
						logger.error("Exception in deleteProjectDetails "+e);
						apiResponse.setStatusCode(404);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
					}
					logger.debug("Exit : public @ResponseBody ReportVO deleteProjectDetails(@RequestBody RequestWrapper requestWrapper)")	;						
					return new ResponseEntity(apiResponse, HttpStatus.OK);
			 
				}
				
				@RequestMapping(value="/report/addProjectItemDetails", method = RequestMethod.POST)
				public ResponseEntity addProjectItemDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					APIResponseVO apiResponse=new APIResponseVO();
					ReportDetailsVO reportVO=new ReportDetailsVO();
						logger.debug("Entry : public @ResponseBody ReportVO addBudgetItemDetails(@RequestBody RequestWrapper requestWrapper)" );
					try {
						
						if(projectVO.getOrgID()>0 ){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						 int success=projectDetailsService.addProjectItemDetails(projectVO);
						 if(success>0){
							 apiResponse.setStatusCode(200);
							 apiResponse.setMessage("Successfully added project items "+success);
						 }
						 
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
					} catch (Exception e) {
						logger.error("Exception in addProjectItemDetails "+e);
						apiResponse.setStatusCode(404);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
					}
					logger.debug("Exit : public @ResponseBody ReportVO addProjectItemDetails(@RequestBody RequestWrapper requestWrapper)")	;						
					return new ResponseEntity(apiResponse, HttpStatus.OK);
			 
				}
				
				@RequestMapping(value="/report/addBulkProjectItems", method = RequestMethod.POST)
				public ResponseEntity addBulkProjectItemDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					APIResponseVO apiResponse=new APIResponseVO();
					ReportDetailsVO reportVO=new ReportDetailsVO();
						logger.debug("Entry : public @ResponseBody ReportVO addBulkProjectItemDetails(@RequestBody RequestWrapper requestWrapper)" );
					try {
						
						if(projectVO.getOrgID()>0 ){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						 int success=projectDetailsService.addBulkProjectItemDetails(projectVO.getItemList());
						 if(success>0){
							 apiResponse.setStatusCode(200);
							 apiResponse.setMessage("Successfully added prject items "+success);
						 }
						 
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
					} catch (Exception e) {
						logger.error("Exception in addBulkProjectItemDetails "+e);
						apiResponse.setStatusCode(404);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
					}
					logger.debug("Exit : public @ResponseBody ReportVO addBulkProjectItemDetails(@RequestBody RequestWrapper requestWrapper)")	;						
					return new ResponseEntity(apiResponse, HttpStatus.OK);
			 
				}
				
				@RequestMapping(value="/report/updateProjectItemDetails", method = RequestMethod.POST)
				public ResponseEntity updateProjectItemDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					APIResponseVO apiResponse=new APIResponseVO();
					ReportDetailsVO reportVO=new ReportDetailsVO();
						logger.debug("Entry : public @ResponseBody ReportVO updateProjectItemDetails(@RequestBody RequestWrapper requestWrapper)" );
					try {
						
						if(projectVO.getOrgID()>0 ){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						 int success=projectDetailsService.updateProjectItemDetails(projectVO);
						 if(success>0){
							 apiResponse.setStatusCode(200);
							 apiResponse.setMessage("Successfully updated project item details ");
						 }else{
							 
								apiResponse.setStatusCode(400);
				  				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				  				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						 }
						 
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
					} catch (Exception e) {
						logger.error("Exception in updateProjectItemDetails "+e);
						apiResponse.setStatusCode(404);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
					}
					logger.debug("Exit : public @ResponseBody ReportVO updateProjectItemDetails(@RequestBody RequestWrapper requestWrapper)")	;						
					return new ResponseEntity(apiResponse, HttpStatus.OK);
			 
				}
				
				@RequestMapping(value="/report/deleteProjecttItemDetails", method = RequestMethod.POST)
				public ResponseEntity deleteProjectItemDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					APIResponseVO apiResponse=new APIResponseVO();
					ReportDetailsVO reportVO=new ReportDetailsVO();
						logger.debug("Entry : public @ResponseBody ReportVO deleteProjectItemDetails(@RequestBody RequestWrapper requestWrapper)" );
					try {
						
						if(projectVO.getOrgID()>0 ){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						 int success=projectDetailsService.deleteProjectItemDetails(projectVO);
						 if(success>0){
							 apiResponse.setStatusCode(200);
							 apiResponse.setMessage("Successfully deleted project details ");
						 }else{
								apiResponse.setStatusCode(400);
				  				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				  				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						 }
						 
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
					} catch (Exception e) {
						logger.error("Exception in deleteProjectDetails "+e);
						apiResponse.setStatusCode(404);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
					}
					logger.debug("Exit : public @ResponseBody ReportVO deleteProjectItemDetails(@RequestBody RequestWrapper requestWrapper)")	;						
					return new ResponseEntity(apiResponse, HttpStatus.OK);
			 
				}
				
				/*------------------Get projects/ cost centers item list for an organization------------------*/
				@RequestMapping(value="/get/projectItemList",  method = RequestMethod.POST  )			
				public ResponseEntity getProjectItemList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					int success=0;
					APIResponseVO apiResponse=new APIResponseVO();
					JsonRespAccountsVO json=new JsonRespAccountsVO();
					List projectList=new ArrayList();
						logger.debug("Entry : public ResponseEntity getProjectItemList(@RequestBody ProjectDetailsVO projectVO)")	;
					try {
						logger.debug("Here society ID "+projectVO.getOrgID());
						if(projectVO.getOrgID()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						projectList=projectDetailsService.getProjectDetailsListWithDetails(projectVO.getOrgID(), projectVO.getFromDate(),projectVO.getToDate());
						
						if(projectList.size()>0){
							json.setObjectList(projectList);
							json.setStatusCode(200);
						}
							
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
						

						logger.debug("Result of public ResponseEntity getProjectItemList(@RequestBody ProjectDetailsVO projectVO) : "+success);				
						
					} catch (Exception e) {
						logger.error("Exception  "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public ResponseEntity getProjectItemList(@RequestBody ProjectDetailsVO projectVO)")	;						
					return new ResponseEntity(json, HttpStatus.OK);
			 
				}

				
				/*------------------Get projects/ cost centers for a organization------------------*/
				@RequestMapping(value="/get/projectList",  method = RequestMethod.POST  )			
				public ResponseEntity getProjectList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					int success=0;
					APIResponseVO apiResponse=new APIResponseVO();
					JsonRespAccountsVO json=new JsonRespAccountsVO();
					List projectList=new ArrayList();
						logger.debug("Entry : public ResponseEntity getProjectList(@RequestBody ProjectDetailsVO projectVO)")	;
					try {
						logger.debug("Here society ID "+projectVO.getOrgID());
						if(projectVO.getOrgID()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						projectList=projectDetailsService.getProjectDetailsList(projectVO.getOrgID());
						
						if(projectList.size()>0){
							json.setObjectList(projectList);
							json.setStatusCode(200);
						}
							
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
						

						logger.debug("Result of public ResponseEntity getProjectList(@RequestBody ProjectDetailsVO projectVO) : "+success);				
						
					} catch (Exception e) {
						logger.error("Exception  "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public ResponseEntity getProjectList(@RequestBody ProjectDetailsVO projectVO)")	;						
					return new ResponseEntity(json, HttpStatus.OK);
			 
				}
				
				/*------------------Get projects/ cost centers for a organization------------------*/
				@RequestMapping(value="/get/projectDetailsList",  method = RequestMethod.POST  )			
				public ResponseEntity getProjectListWithDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					int success=0;
					APIResponseVO apiResponse=new APIResponseVO();
					JsonRespAccountsVO json=new JsonRespAccountsVO();
					List projectList=new ArrayList();
						logger.debug("Entry : public ResponseEntity getProjectList(@RequestBody ProjectDetailsVO projectVO)")	;
					try {
						logger.debug("Here society ID "+projectVO.getOrgID());
						if(projectVO.getOrgID()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						projectList=projectDetailsService.getProjectDetailsListWithDetails(projectVO.getOrgID(), projectVO.getFromDate(),projectVO.getToDate());
						
						if(projectList.size()>0){
							json.setObjectList(projectList);
							json.setStatusCode(200);
						}
							
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
						

						logger.debug("Result of public ResponseEntity getProjectList(@RequestBody ProjectDetailsVO projectVO) : "+success);				
						
					} catch (Exception e) {
						logger.error("Exception  "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public ResponseEntity getProjectList(@RequestBody ProjectDetailsVO projectVO)")	;						
					return new ResponseEntity(json, HttpStatus.OK);
			 
				}
	
	
				/*------------------Get projects/ cost centers for a organization------------------*/
				@RequestMapping(value="/get/projectDetails",  method = RequestMethod.POST  )			
				public ResponseEntity getProjectDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					int success=0;
					ProjectDetailsVO prjVO=new ProjectDetailsVO();
					APIResponseVO apiResponse=new APIResponseVO();
							
						logger.debug("Entry : public ResponseEntity getProjectDetails(@RequestBody ProjectDetailsVO projectVO)")	;
					try {
						logger.info("Here society ID "+projectVO.getOrgID());
						if(projectVO.getOrgID()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						prjVO=projectDetailsService.getProjectDetails(projectVO.getOrgID(),projectVO.getProjectID(), projectVO.getFromDate(), projectVO.getToDate());
						
						
							
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
						

						logger.info("Result of public ResponseEntity getProjectDetails(@RequestBody ProjectDetailsVO projectVO) : "+success);				
						
					} catch (Exception e) {
						logger.error("Exception  "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public ResponseEntity getProjectDetails(@RequestBody ProjectDetailsVO projectVO)")	;						
					return new ResponseEntity(prjVO, HttpStatus.OK);
			 
				}
		
				/*------------------Get projects/ cost centers for  tags  organization------------------*/
				@RequestMapping(value="/get/projectListWithTags",  method = RequestMethod.POST  )			
				public ResponseEntity projectDetailsListWithTags(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					int success=0;
					APIResponseVO apiResponse=new APIResponseVO();
					JsonRespAccountsVO json=new JsonRespAccountsVO();
					List projectList=new ArrayList();
						logger.debug("Entry : public ResponseEntity getProjectList(@RequestBody ProjectDetailsVO projectVO)")	;
					try {
						logger.debug("Here society ID "+projectVO.getOrgID());
						if(projectVO.getOrgID()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						projectList=projectDetailsService.getProjectListWithTagWise(projectVO.getOrgID(),projectVO.getType());
						
						if(projectList.size()>0){
							json.setObjectList(projectList);
							json.setStatusCode(200);
						}
							
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
						

						logger.debug("Result of public ResponseEntity getProjectList(@RequestBody ProjectDetailsVO projectVO) : "+success);				
						
					} catch (Exception e) {
						logger.error("Exception  "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public ResponseEntity getProjectList(@RequestBody ProjectDetailsVO projectVO)")	;						
					return new ResponseEntity(json, HttpStatus.OK);
			 
				}
				
				/*------------------Get projects/ cost centers detailed list for a organization------------------*/
				@RequestMapping(value="/get/projectDetailsListTagWise",  method = RequestMethod.POST  )			
				public ResponseEntity getProjectListWithDetailsTagWise(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					int success=0;
					APIResponseVO apiResponse=new APIResponseVO();
					JsonRespAccountsVO json=new JsonRespAccountsVO();
					List projectList=new ArrayList();
						logger.debug("Entry : public ResponseEntity getProjectListWithDetailsTagWise(@RequestBody ProjectDetailsVO projectVO)")	;
					try {
						logger.debug("Here society ID "+projectVO.getOrgID());
						if(projectVO.getOrgID()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						projectList=projectDetailsService.getProjectDetailsListWithDetailsTagWise(projectVO.getOrgID(), projectVO.getFromDate(),projectVO.getToDate());
						
						if(projectList.size()>0){
							json.setObjectList(projectList);
							json.setStatusCode(200);
						}
							
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
						

						logger.debug("Result of public ResponseEntity getProjectListWithDetailsTagWise(@RequestBody ProjectDetailsVO projectVO) : "+success);				
						
					} catch (Exception e) {
						logger.error("Exception in getProjectListWithDetailsTagWise  for OrgID "+projectVO.getOrgID()+"  "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public ResponseEntity getProjectListWithDetailsTagWise(@RequestBody ProjectDetailsVO projectVO)")	;						
					return new ResponseEntity(json, HttpStatus.OK);
			 
				}
				
				/*------------------Get projects/ cost centers detailed list for a organization------------------*/
				@RequestMapping(value="/get/projectSummaryListTagWise",  method = RequestMethod.POST  )			
				public ResponseEntity getProjectSummaryListWithDetailsTagWise(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					int success=0;
					APIResponseVO apiResponse=new APIResponseVO();
					JsonRespAccountsVO json=new JsonRespAccountsVO();
					List projectList=new ArrayList();
						logger.debug("Entry : public ResponseEntity getProjectSummaryListWithDetailsTagWise(@RequestBody ProjectDetailsVO projectVO)")	;
					try {
						logger.debug("Here society ID "+projectVO.getOrgID());
						if(projectVO.getOrgID()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						projectList=projectDetailsService.getProjectDetailsSummaryListWithDetailsTagWise(projectVO.getOrgID(), projectVO.getFromDate(), projectVO.getToDate());
						
						if(projectList.size()>0){
							json.setObjectList(projectList);
							json.setStatusCode(200);
						}
							
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
						

						logger.debug("Result of public ResponseEntity getProjectSummaryListWithDetailsTagWise(@RequestBody ProjectDetailsVO projectVO) : "+success);				
						
					} catch (Exception e) {
						logger.error("Exception in getProjectSummaryListWithDetailsTagWise  for OrgID "+projectVO.getOrgID()+"  "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public ResponseEntity getProjectSummaryListWithDetailsTagWise(@RequestBody ProjectDetailsVO projectVO)")	;						
					return new ResponseEntity(json, HttpStatus.OK);
			 
				}
				
				@RequestMapping(value="/get/projectTransactionList", method = RequestMethod.POST)
				public ResponseEntity getTransactionListOfProjects(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
					APIResponseVO apiResponse=new APIResponseVO();
					LedgerTrnsVO ledgerTxVO=null;
						logger.debug("Entry : public @ResponseBody LedgerTrnsVO getTransactionListOfProjects(@RequestBody RequestWrapper requestWrapper)")	;
					try {
						if(requestWrapper.getOrgID()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
							
						ledgerTxVO=ledgerService.getTransactionListOfProjects(requestWrapper.getOrgID(), requestWrapper.getLedgerID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),requestWrapper.getType());
							
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
					} catch (Exception e) {
						logger.error("Exception in getTransactionListOfProjects "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public @ResponseBody LedgerTrnsVO getTransactionListOfProjects(@RequestBody RequestWrapper requestWrapper)")	;						
					return new ResponseEntity(ledgerTxVO, HttpStatus.OK);
			 
				}
				
				/*------------------Get projects/ cost centers detailed list for a organization------------------*/
				@RequestMapping(value="/get/projectDetailsTagWise",  method = RequestMethod.POST  )			
				public ResponseEntity getProjectDetailsWithDetailsTagWise(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					int success=0;
					APIResponseVO apiResponse=new APIResponseVO();
					ProjectDetailsVO proVO=new ProjectDetailsVO();
					List projectList=new ArrayList();
						logger.debug("Entry : public ResponseEntity getProjectDetailsWithDetailsTagWise(@RequestBody ProjectDetailsVO projectVO)")	;
					try {
						logger.debug("Here society ID "+projectVO.getOrgID());
						if(projectVO.getOrgID()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						    proVO=projectDetailsService.getProjectDetailsTagWise(projectVO.getOrgID(), projectVO.getProjectAcronyms(), projectVO.getFromDate(), projectVO.getToDate(),projectVO.getRootID());
					
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
						

						logger.debug("Result of public ResponseEntity getProjectDetailsWithDetailsTagWise(@RequestBody ProjectDetailsVO projectVO) : "+success);				
						
					} catch (Exception e) {
						logger.error("Exception in getProjectDetailsWithDetailsTagWise  for OrgID "+projectVO.getOrgID()+"  "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public ResponseEntity getProjectDetailsWithDetailsTagWise(@RequestBody ProjectDetailsVO projectVO)")	;						
					return new ResponseEntity(proVO, HttpStatus.OK);
			 
				}
				
				
				/*------------------Get projects/ cost centers detailed list for a organization------------------*/
				@RequestMapping(value="/get/projectDetailsCategoryWise",  method = RequestMethod.POST  )			
				public ResponseEntity getProjectDetailsCategoryWise(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					int success=0;
					APIResponseVO apiResponse=new APIResponseVO();
					ProjectDetailsVO proVO=new ProjectDetailsVO();
					List projectList=new ArrayList();
						logger.debug("Entry : public ResponseEntity getProjectDetailsWithDetailsTagWise(@RequestBody ProjectDetailsVO projectVO)")	;
					try {
						logger.debug("Here society ID "+projectVO.getOrgID());
						if(projectVO.getOrgID()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
							proVO=projectDetailsService.getProjectDetailsCategoryWise(projectVO.getOrgID(), projectVO.getProjectCategoryID(), projectVO.getFromDate(), projectVO.getToDate(),projectVO.getRootID());
						
						
							
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
						

						logger.debug("Result of public ResponseEntity getProjectDetailsWithDetailsTagWise(@RequestBody ProjectDetailsVO projectVO) : "+success);				
						
					} catch (Exception e) {
						logger.error("Exception in getProjectDetailsWithDetailsTagWise  for OrgID "+projectVO.getOrgID()+"  "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public ResponseEntity getProjectDetailsWithDetailsTagWise(@RequestBody ProjectDetailsVO projectVO)")	;						
					return new ResponseEntity(proVO, HttpStatus.OK);
			 
				}
				
				@RequestMapping(value="/get/categoryTransactionList", method = RequestMethod.POST)
				public ResponseEntity getTransactionListCategoryWise(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
					APIResponseVO apiResponse=new APIResponseVO();
					LedgerTrnsVO ledgerTxVO=null;
						logger.debug("Entry : public @ResponseBody LedgerTrnsVO getTransactionListCategoryWise(@RequestBody RequestWrapper requestWrapper)")	;
					try {
						if(requestWrapper.getOrgID()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
							
						ledgerTxVO=ledgerService.getTransactionListOfProjectsWithCategory(requestWrapper.getOrgID(), requestWrapper.getLedgerID(), requestWrapper.getFromDate(), requestWrapper.getToDate(),requestWrapper.getGroupID());
							
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
					} catch (Exception e) {
						logger.error("Exception in getTransactionListCategoryWise "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public @ResponseBody LedgerTrnsVO getTransactionListCategoryWise(@RequestBody RequestWrapper requestWrapper)")	;						
					return new ResponseEntity(ledgerTxVO, HttpStatus.OK);
			 
				}
				
				/*------------------Get projects/ cost centers for  Category  organization------------------*/
				@RequestMapping(value="/get/costCenterCategoryList",  method = RequestMethod.POST  )			
				public ResponseEntity getCostCenterCategoryList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					int success=0;
					APIResponseVO apiResponse=new APIResponseVO();
					JsonRespAccountsVO json=new JsonRespAccountsVO();
					List projectList=new ArrayList();
						logger.debug("Entry : public ResponseEntity getCostCenterCategoryList(@RequestBody ProjectDetailsVO projectVO)")	;
					try {
						logger.debug("Here society ID "+projectVO.getOrgID());
						if(projectVO.getOrgID()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						projectList=projectDetailsService.getCostCeneterCategoryList(projectVO.getOrgID());
						
						if(projectList.size()>0){
							json.setObjectList(projectList);
							json.setStatusCode(200);
						}
							
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
						

						logger.debug("Result of public ResponseEntity getCostCenterCategoryList(@RequestBody ProjectDetailsVO projectVO) : "+success);				
						
					} catch (Exception e) {
						logger.error("Exception  "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					logger.debug("Exit : public ResponseEntity getCostCenterCategoryList(@RequestBody ProjectDetailsVO projectVO)")	;						
					return new ResponseEntity(json, HttpStatus.OK);
			 
				}
				
				
				@RequestMapping(value="/report/cashBasedCostCenterReport", method = RequestMethod.POST)
				public ResponseEntity getCashBasedCostCenterReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					APIResponseVO apiResponse=new APIResponseVO();
					MonthwiseChartVO rptVO=new MonthwiseChartVO();
						logger.debug("Entry : public ResponseEntity getCashBasedCostCenterReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper)" );
					try {
						
						if(projectVO.getOrgID()>0 && projectVO.getFromDate().length()>0 && projectVO.getToDate().length()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						  rptVO=projectDetailsService.getCashBasedCostCenterReport(projectVO.getOrgID(), projectVO.getFromDate(), projectVO.getToDate(),"CF",projectVO.getProjectAcronyms());
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
					} catch (Exception e) {
						logger.error("Exception in public ResponseEntity getCashBasedCostCenterReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) "+e);
						apiResponse.setStatusCode(404);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
					}
					logger.debug("Exit : public ResponseEntity getCashBasedCostCenterReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper)")	;						
					return new ResponseEntity(rptVO, HttpStatus.OK);
			 
				}
				
				@RequestMapping(value="/report/cashBasedCostCenterLedgerReport", method = RequestMethod.POST)
				public ResponseEntity getCashBasedCostCenterLedgerReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO requestWrapper) {
					JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
					APIResponseVO apiResponse=new APIResponseVO();
					List ledgerList=new ArrayList();
						logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getCashBasedCostCenterLedgerReport(@RequestBody RequestWrapper requestWrapper)"+requestWrapper.getGroupID() );
					try {
						if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						 ledgerList=projectDetailsService.getCashBasedCostCenterLedgerReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getGroupID(),requestWrapper.getType(),0,requestWrapper.getProjectAcronyms());
						 jSonVO.setSocietyID(requestWrapper.getOrgID());
						if(ledgerList.size()>0){
						  jSonVO.setObjectList(ledgerList);
						  jSonVO.setStatusCode(1);
						}
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
					} catch (Exception e) {
						logger.error("Exception in getCashBasedCostCenterLedgerReport "+e);
						apiResponse.setStatusCode(404);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
					}		
						
					
					logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getCashBasedCostCenterLedgerReport(@RequestBody RequestWrapper requestWrapper)")	;						
					return new ResponseEntity(jSonVO, HttpStatus.OK);
			 
				}
				
				@RequestMapping(value="/report/cashBasedCostCenterCategoryReport", method = RequestMethod.POST)
				public ResponseEntity getCashBasedCostCenterCategoryReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO projectVO) {
					APIResponseVO apiResponse=new APIResponseVO();
					MonthwiseChartVO rptVO=new MonthwiseChartVO();
						logger.debug("Entry : public ResponseEntity getCashBasedCostCenterCategoryReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper)" );
					try {
						
						if(projectVO.getOrgID()>0 && projectVO.getFromDate().length()>0 && projectVO.getToDate().length()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							projectVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						  rptVO=projectDetailsService.getCashBasedCostCenterCategoryReport(projectVO.getOrgID(), projectVO.getFromDate(), projectVO.getToDate(),"CF",projectVO.getProjectCategoryID());
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
					} catch (Exception e) {
						logger.error("Exception in public ResponseEntity getCashBasedCostCenterCategoryReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) "+e);
						apiResponse.setStatusCode(404);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
					}
					logger.debug("Exit : public ResponseEntity getCashBasedCostCenterCategoryReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper)")	;						
					return new ResponseEntity(rptVO, HttpStatus.OK);
			 
				}
				
				@RequestMapping(value="/report/cashBasedCostCenterCategoryLedgerReport", method = RequestMethod.POST)
				public ResponseEntity getCashBasedCostCenterCategoryLedgerReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody ProjectDetailsVO requestWrapper) {
					JsonRespAccountsVO jSonVO=new JsonRespAccountsVO();
					APIResponseVO apiResponse=new APIResponseVO();
					List ledgerList=new ArrayList();
						logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getCashBasedCostCenterCategoryLedgerReport(@RequestBody RequestWrapper requestWrapper)"+requestWrapper.getGroupID() );
					try {
						if(requestWrapper.getOrgID()>0 && requestWrapper.getFromDate().length()>0 && requestWrapper.getToDate().length()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						 ledgerList=projectDetailsService.getCashBasedCostCenterCategoryLedgerReport(requestWrapper.getOrgID(), requestWrapper.getFromDate(), requestWrapper.getToDate(), requestWrapper.getGroupID(),requestWrapper.getType(),0,requestWrapper.getProjectCategoryID());
						 jSonVO.setSocietyID(requestWrapper.getOrgID());
						if(ledgerList.size()>0){
						  jSonVO.setObjectList(ledgerList);
						  jSonVO.setStatusCode(1);
						}
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
					} catch (Exception e) {
						logger.error("Exception in getCashBasedCostCenterCategoryLedgerReport "+e);
						apiResponse.setStatusCode(404);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
					}		
						
					
					logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getCashBasedCostCenterCategoryLedgerReport(@RequestBody RequestWrapper requestWrapper)")	;						
					return new ResponseEntity(jSonVO, HttpStatus.OK);
			 
				}
				
				
				/*------------------Get list of organizations in given datazone------------------*/
				@RequestMapping(value="/get/organizationListInDatazone",  method = RequestMethod.POST  )			
				public ResponseEntity getOrganizationListInDatazone(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody SocietyVO societyVO) {
					int success=0;
					APIResponseVO apiResponse=new APIResponseVO();
					JsonRespAccountsVO json=new JsonRespAccountsVO();
					List orgList=new ArrayList();
						logger.debug("Entry : public ResponseEntity getOrganizationListInDatazone(@RequestBody SocietyVO societyVO)")	;
					try {
						logger.debug("Here organization ID "+societyVO.getSocietyID());
						if(societyVO.getSocietyID()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
							societyVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
						orgList=societyService.getSocietyListInDatazone(societyVO.getDataZoneID());
						
						if(orgList.size()>0){
							json.setObjectList(orgList);
							json.setStatusCode(200);
						}else{
							apiResponse.setStatusCode(406);
							apiResponse.setMessage("Unable to get budget details");
							return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
							}
							
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
						

						logger.debug("Result of public ResponseEntity getOrganizationListInDatazone(@RequestBody SocietyVO societyVO) : "+success);				
						
					} catch (Exception e) {
						logger.error("Exception  "+e);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
					return new ResponseEntity(json, HttpStatus.OK);
			 
				}
				
				/*----------- API to Report group of Transaction lineItems --------------*/
				 @RequestMapping(value="/update/ReportGroupTransactionLineItem",  method = RequestMethod.POST  )			
				 	public ResponseEntity updateReportGroupTransactionLineItem(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO transactionVO) {     	
				     	int success=0;
				 		APIResponseVO apiResponse=new APIResponseVO();
				 			logger.debug("Entry : public ResponseEntity updateReportGroupTransactionLineItem(@RequestBody TransactionVO transactionVO)")	;
				 		try {
				 			logger.info("UpdateTxReportGroup org ID: "+transactionVO.getSocietyID()+" Tx LineItemID: "+transactionVO.getTxLineItemID());
				 			if(transactionVO.getTxLineItemID()>0){
				 				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				 				transactionVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
				 				transactionVO=transactionService.updateReportGroupJournalEntry(transactionVO);
				 				 				
				 				if(transactionVO.getStatusCode()!=200){				
				 					 apiResponse.setStatusCode(transactionVO.getStatusCode());
				 					 apiResponse.setMessage(transactionVO.getErrorMessage());
				 					 logger.info("Unable to update LineItemID: "+transactionVO.getTxLineItemID()+" Status Code: "+apiResponse.getStatusCode());
				 				    return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);	
				 				 		
				 				 }else{
				 					apiResponse.setStatusCode(200);
				 					 apiResponse.setMessage("Report group  updated successfully ");			
				 					 
				 				 }
				 			}else{
				 				logger.info("Unable to update LineItemID : "+transactionVO.getTxLineItemID());
				 				apiResponse.setStatusCode(400);
				 				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				 				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				 			}
				 								
				 			} catch (Exception e) {
				 				logger.error("Exception in updateReportGroupTransactionLineItem: "+e);
				 				apiResponse.setStatusCode(400);
				 				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				 				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				 			}
				 			
				 		logger.debug("Exit :public ResponseEntity updateReportGroupTransactionLineItem(@RequestBody TransactionVO transactionVO)")	;						
				 		return new ResponseEntity(apiResponse, HttpStatus.OK);
				  
				 	}
				
				 
				 /*------------------Reallocate interest amount for ledger------------------*/
					@RequestMapping(value="/reallocateInterest",  method = RequestMethod.POST  )			
					public ResponseEntity reallocateInterestBalance(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
						APIResponseVO apiResponse=new APIResponseVO();
						int success=0;
						logger.debug("Entry :  public ResponseEntity reallocateInterestBalance(@RequestBody TransactionVO txVO)")	;
						try {
							logger.info("reallocateInterestBalance Here society ID "+txVO.getSocietyID()+" for ledger ID "+txVO.getLedgerID()+" fromDate "+txVO.getFromDate()+" toDate "+txVO.getToDate());
							if(txVO.getSocietyID()>0){
				 				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				 				txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
							
							success=transactionService.reallocateInterestAmounts(txVO);
							if(success!=0){
								apiResponse.setStatusCode(200);
			 					apiResponse.setMessage("Interest amount updated successfully ");		
							}else{
							
								apiResponse.setStatusCode(406);
								apiResponse.setMessage("Unable update interest amounts");
								return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
								
							}
							}
							else{
							
								apiResponse.setStatusCode(406);
								apiResponse.setMessage("Unable update interest amounts");
								return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
								
							}
							
							
						} catch (Exception e) {
							logger.error("Exception  public ResponseEntity reallocateInterestBalance(@RequestBody TransactionVO txVO) "+e);
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
						logger.debug("Exit : public ResponseEntity reallocateInterestBalance(@RequestBody TransactionVO txVO)")	;						
						return new ResponseEntity(apiResponse, HttpStatus.OK);
				 
					}
					
					
					 /*------------------Reallocate interest amount for Organization------------------*/
					@RequestMapping(value="/reallocateInterestForOrg",  method = RequestMethod.POST  )			
					public ResponseEntity reallocateInterestBalanceForOrg(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
						APIResponseVO apiResponse=new APIResponseVO();
						int success=0;
						logger.debug("Entry :  public ResponseEntity reallocateInterestBalanceForOrg(@RequestBody TransactionVO txVO)")	;
						try {
							logger.info("reallocateInterestBalance Here society ID "+txVO.getSocietyID()+" for ledger ID "+txVO.getLedgerID()+" fromDate "+txVO.getFromDate()+" toDate "+txVO.getToDate());
							
							if(txVO.getSocietyID()>0){
				 				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				 				txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
							
							success=transactionService.reallocateInterestAmountsForOrg(txVO);
							if(success!=0){
								apiResponse.setStatusCode(200);
			 					apiResponse.setMessage("Interest amount updated successfully ");		
							}else{
							
								apiResponse.setStatusCode(406);
								apiResponse.setMessage("Unable update interest amounts");
								return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
								
							}
							}
							else{
							
								apiResponse.setStatusCode(406);
								apiResponse.setMessage("Unable update interest amounts");
								return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
								
							}
							
						} catch (Exception e) {
							logger.error("Exception  public ResponseEntity reallocateInterestBalanceForOrg(@RequestBody TransactionVO txVO) "+e);
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
						logger.debug("Exit : public ResponseEntity reallocateInterestBalanceForOrg(@RequestBody TransactionVO txVO)")	;						
						return new ResponseEntity(apiResponse, HttpStatus.OK);
				 
					}
					
		
					
		//============== PayU money statement related API ==============//
		/*------------------Get payUMoney Statement------------------*/
		@RequestMapping(value="/statement/getPayUStatement",  method = RequestMethod.POST  )			
		public ResponseEntity getPayUStatement(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody OnlinePaymentGatewayVO payUVO) {
			JSONResponseVO jsonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();	
			List statementList=new ArrayList();
			int insertStatement=0;
					
							logger.debug("Entry : public ResponseEntity getPayUStatement(@RequestBody OnlinePaymentGatewayVO payUVO)")	;
						try {
							
							if((payUVO.getFromDate()!=null)&&(payUVO.getFromDate().length()>0)){
				 				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				 				payUVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
							
								
									 statementList=accountsAutoService.getPayUStatementForSummary(payUVO.getFromDate(),payUVO.getToDate());
								
								if(statementList.size()>0){
									jsonVO.setStatusCode(200);
									jsonVO.setObjectList(statementList);
								}
								
							
							
							}else{
								apiResponse.setStatusCode(400);
								apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
								return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
							}
							
							
							
							
							
						} catch (Exception e) {
							logger.error("Exception public ResponseEntity getPayUStatement(@RequestBody OnlinePaymentGatewayVO payUVO) "+e);
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
						logger.debug("Exit : public ResponseEntity getPayUStatement(@RequestBody OnlinePaymentGatewayVO payUVO)")	;						
						return new ResponseEntity(jsonVO, HttpStatus.OK);
				 
					}
		
		@RequestMapping(value="/statement/getUnsettledPayUStatement",  method = RequestMethod.POST  )			
		public ResponseEntity getPayUStatementForUnsettled(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody OnlinePaymentGatewayVO payUVO) {
			JSONResponseVO jsonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();	
			List statementList=new ArrayList();
			int insertStatement=0;
					
							logger.debug("Entry : public ResponseEntity getPayUStatementForUnsettled(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody OnlinePaymentGatewayVO payUVO)")	;
						try {
							
							if((payUVO.getFromDate()!=null)&&(payUVO.getFromDate().length()>0)){
				 				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				 				payUVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
							
								
									 statementList=accountsAutoService.getPayUStatementNotReconciled();
								
								if(statementList.size()>0){
									jsonVO.setStatusCode(200);
									jsonVO.setObjectList(statementList);
								}
								
							
							
							}else{
								apiResponse.setStatusCode(400);
								apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
								return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
							}
							
							
							
							
							
						} catch (Exception e) {
							logger.error("Exception public ResponseEntity getPayUStatementForUnsettled(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody OnlinePaymentGatewayVO payUVO) "+e);
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
						logger.debug("Exit : public ResponseEntity getPayUStatementForUnsettled(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody OnlinePaymentGatewayVO payUVO)")	;						
						return new ResponseEntity(jsonVO, HttpStatus.OK);
				 
					}
		
		/*------------------Add PayUMoney Statement------------------*/
		@RequestMapping(value="/statement/insertPayUTransaction",  method = RequestMethod.POST  )			
		public ResponseEntity addPayUTransaction(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody OnlinePaymentGatewayVO payUVO) {
			JSONResponseVO jsonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			int insertStatement=0;
		
				logger.debug("Entry : public ResponseEntity addPayUStatement(@RequestBody OnlinePaymentGatewayVO payUVO)")	;
			try {
				
				if(payUVO.getOrgID()>0){
	 				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
	 				
						 insertStatement=accountsAutoService.addPayUStatements(payUVO);
					
						 if(insertStatement>0){
								apiResponse.setStatusCode(200);
								apiResponse.setMessage("Transaction successfully added.");
							}else if(insertStatement==-1){
								apiResponse.setStatusCode(534);
								apiResponse.setMessage("Transaction with same parent payment ID already present please check the transaction.");
								return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
							} else{
								apiResponse.setStatusCode(400);
								apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
								return new ResponseEntity(apiResponse, HttpStatus.CONFLICT);
							}
					
				
				
				}else{
					jsonVO.setStatusCode(400);
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				
				
				
				
				
			} catch (Exception e) {
				logger.error("Exception public ResponseEntity addPayUStatement(@RequestBody OnlinePaymentGatewayVO payUVO) "+e);
				jsonVO.setStatusCode(400);
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity addPayUStatement(@RequestBody OnlinePaymentGatewayVO payUVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}

		
		//============ Member App related APIs =============//
		
		
		/*======================
		 * Sample Parameters
		 * {"societyID":"11","fromDate":"2018-04-01","toDate":"2018-04-30","transactionType":"all","statusDescription":"all","ledgerID":"18"}
		 * 
		 * */
		
		@RequestMapping(value="/report/allNonRecordedTransactionsForMemberAppLedger", method = RequestMethod.POST)
		public ResponseEntity getAllNonRecordedTransactionsReportForMemberAppLedger(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO) {
			JsonRespAccountsVO jsoVO=new JsonRespAccountsVO();
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry :public ResponseEntity getAllNonRecordedTransactionsReportForMemberAppLedger(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO)")	;
			try {
							
				if(txVO.getSocietyID()>0 ){
				   UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					txVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
				
						List transactionsList=transactionService.getAllNonRecordedTransactionsReportForMemberAppLedger(txVO);
						jsoVO.setStatusCode(200);
						jsoVO.setObjectList(transactionsList);				
					
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getAllNonRecordedTransactionsReportForLedger "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
			}		
				
			logger.debug("Exit : public ResponseEntity getAllNonRecordedTransactionsReportForLedger(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO txVO)")	;						
			return new ResponseEntity(jsoVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/billListForMemberApp", method = RequestMethod.POST)
		public ResponseEntity getBillListForMemberApp(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List billList=new ArrayList();
				logger.debug("Entry : public @ResponseBody JsonRespAccountsVO getBillListForMember(@RequestBody RequestWrapper requestWrapper)");
			try {
				if(requestWrapper.getOrgID()>0 && requestWrapper.getAptID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					
				billList=invoiceService.getBillsForMemberApp(requestWrapper.getOrgID(), requestWrapper.getAptID());
				jSonVO.setSocietyID(requestWrapper.getOrgID());
				jSonVO.setFromDate(requestWrapper.getFromDate());
				jSonVO.setUptoDate(requestWrapper.getToDate());
				if(billList.size()==0){
					jSonVO.setStatusCode(0);
				}else{
					jSonVO.setStatusCode(1);
					jSonVO.setObjectList(billList);
				}			
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getUnpaidInvoiceList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JsonRespAccountsVO getBillListForMember(@RequestBody RequestWrapper requestWrapper)");						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/receiptListForMemberApp", method = RequestMethod.POST)
		public ResponseEntity getReceiptListForMemberApp(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
			JSONResponseVO jSonVO=new JSONResponseVO();
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody JSONResponseVO getReceiptListForMember(@RequestBody RequestWrapper requestWrapper)")	;
			try {
				if(requestWrapper.getOrgID()>0 ){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
					jSonVO=txService.getReceiptForMemberApp(requestWrapper.getOrgID(), requestWrapper.getAptID());
							
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getReceiptListForMember "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody JSONResponseVO getReceiptForMember(@RequestBody RequestWrapper requestWrapper)")	;						
			return new ResponseEntity(jSonVO, HttpStatus.OK);
	 
		}
		
		
		//==================Get available financial years list of given org===========//
		
		
		@RequestMapping(value="/getFinancialYearList", method = RequestMethod.POST)
		public ResponseEntity getFiancialYearList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken, @RequestBody UserVO orgVO ){
			String success=null;
			UserVO userVO=new UserVO();
			 APIResponseVO apiResponse=new APIResponseVO();
			 List fyList=new ArrayList();
			 JsonRespAccountsVO jsonVO=new JsonRespAccountsVO();
			logger.debug("Entry : public ResponseEntity getFiancialYearList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken, @RequestBody UserVO orgVO )")	;
			try {
				
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				int orgID=Integer.parseInt(decrptVO.getOrgID());
				SocietyVO societyVO=societyService.getSocietyDetails(orgID);
				
				fyList=societyService.getFinancialYearListForOrg(orgID);
				if(fyList.size()!=0){
					jsonVO.setObjectList(fyList);
					jsonVO.setStatusCode(200);
 					
				}else if(fyList.size()==0){
					apiResponse.setStatusCode(406);
					apiResponse.setMessage("No financial years available for "+societyVO.getSocFullName());
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
 					
				}
				
				else{
				
					apiResponse.setStatusCode(406);
					apiResponse.setMessage("Unable get financial year");
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
					
				}		
			} catch (Exception e) {
				logger.error("Exception in  get financial years : "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getFiancialYearList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken, @RequestBody UserVO orgVO )")	;
			return new ResponseEntity(jsonVO, HttpStatus.OK);
		}
		
		
		//==================Get pending transaction report for all orgs Created 06-05-21===========//
		
		
		@RequestMapping(value="/getPendingApprovalTxReport", method = RequestMethod.POST)
		public ResponseEntity getPendingApprovalTxReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken){
			String success=null;
			UserVO userVO=new UserVO();
			 APIResponseVO apiResponse=new APIResponseVO();
			 List societyList=new ArrayList();
			 JsonRespAccountsVO jsonVO=new JsonRespAccountsVO();
			logger.debug("Entry : public ResponseEntity getPendingApprovalTxReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken)")	;
			try {
				
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				
				
				societyList=accountingStatusRptService.getPendingApprovalTxReport();
				if(societyList.size()!=0){
					jsonVO.setObjectList(societyList);
					jsonVO.setStatusCode(200);
 					
				}else if(societyList.size()==0){
					apiResponse.setStatusCode(406);
					apiResponse.setMessage("No pending transactions available in any organizations ");
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
 					
				}
				
				else{
				
					apiResponse.setStatusCode(406);
					apiResponse.setMessage("Unable get financial year");
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
					
				}		
			} catch (Exception e) {
				logger.error("Exception in  get getPendingApprovalTxReport years : "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+userVO.getStatusCode().toString()));
				return new ResponseEntity(apiResponse,HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getPendingApprovalTxReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken )")	;
			return new ResponseEntity(jsonVO, HttpStatus.OK);
		}

		public InvoiceService getInvoiceService() {
			return invoiceService;
		}

		public void setInvoiceService(InvoiceService invoiceService) {
			this.invoiceService = invoiceService;
		}

		

		

	}

