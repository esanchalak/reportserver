package com.emanager.webservice;

import java.util.List;

import org.apache.log4j.Logger;
//import org.codehaus.jackson.annotate.JsonBackReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emanager.server.adminReports.Services.NominationRegService;
import com.emanager.server.adminReports.Services.PrintReportService;
import com.emanager.server.adminReports.valueObject.NominationRegisterVO;
import com.emanager.server.adminReports.valueObject.RptDocumentVO;
import com.emanager.server.adminReports.valueObject.RptMorgageVO;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.commonUtils.services.AddressService;
import com.emanager.server.commonUtils.valueObject.DropDownVO;
import com.emanager.server.dashBoard.Services.AccountDashBoardService;
import com.emanager.server.dashBoard.Services.DashBoardService;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.MemberWSVO;
import com.emanager.server.society.valueObject.RenterVO;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.society.valueObject.VehicleVO;
import com.emanager.server.taskAndEventManagement.Service.NotificationService;
import com.emanager.server.taskAndEventManagement.valueObject.EmailMessage;
import com.emanager.server.taskAndEventManagement.valueObject.EventVO;
import com.emanager.server.taskAndEventManagement.valueObject.NotificationVO;
import com.emanager.server.taskAndEventManagement.valueObject.SMSMessage;
import com.emanager.server.taskAndEventManagement.valueObject.TicketVO;
import com.emanager.server.userLogin.services.LoginService;
import com.emanager.server.userManagement.valueObject.UserVO;
	
	
	@Controller
	@RequestMapping("/secure/member")
	public class JSONMemberController {
		
		private static final Logger logger = Logger.getLogger(JSONMemberController.class);
		@Autowired
		MemberService memberServiceBean;
	    @Autowired
		LoginService loginService;
	    @Autowired
	    ReportService reportService;
	    
	    @Autowired
	    PrintReportService printReportService;
	    @Autowired
	    AddressService addressServiceBean;
	    @Autowired
	    DashBoardService dashBoardService;
	    @Autowired
	    AccountDashBoardService accountDashBoardService;
	    @Autowired
	    SocietyService societyService;
	    @Autowired
	    NominationRegService nominationRegService;
	    @Autowired
	    NotificationService notificationService;
	    public ConfigManager configManager =new ConfigManager();
	    public EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
		public final String HEADER_SECURITY_TOKEN = "X-AuthToken";    	    
	  
	  		
		
		@RequestMapping(value="/get/buildingList", method = RequestMethod.POST)
		public ResponseEntity getBuildingDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody MemberVO requestVO){
			List buildingList=null;
			APIResponseVO apiResponse=new APIResponseVO();
			JSONResponseVO jsonVO=new JSONResponseVO();
			logger.debug("Entry : public @ResponseBody List getBuildingDetails(@RequestBody MemberVO requestVO)")	;
			try {
				if(requestVO.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
				buildingList=memberServiceBean.getBuildingList(requestVO.getSocietyID(),"");
				 DropDownVO drpDwn = new DropDownVO();
				 drpDwn.setData("0");
				 drpDwn.setLabel("All");
				 buildingList.add(0, drpDwn);
				 if(buildingList.size()>0){
					 jsonVO.setStatusCode(1);
					 jsonVO.setSocietyID(requestVO.getSocietyID());
					 jsonVO.setObjectList(buildingList);
				 }else
					 jsonVO.setStatusCode(0);
				 
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getBuildingDetails "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}	
			
			logger.debug("Exit : public @ResponseBody List getBuildingDetails(@RequestBody MemberVO requestVO)")	;
			return new ResponseEntity(jsonVO, HttpStatus.OK);
		}
		
		
		@RequestMapping(value="/memberList", method = RequestMethod.POST)
		public ResponseEntity getMemberList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody MemberVO requestVO){
			List memberList=null;
			MemberVO memberVO=new MemberVO();
			APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry :public @ResponseBody List getMemberList(@RequestBody MemberVO requestVO)"+requestVO.getSocietyID())	;
			try {
				if(requestVO.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
				memberList=memberServiceBean.getActivePrimaryMemberList(requestVO.getSocietyID());
				
				if(memberList.size()>0){
					memberVO.setStatusCode(1);
					memberVO.setSocietyID(requestVO.getSocietyID());
					memberVO.setObjectList(memberList);
				}else
					memberVO.setStatusCode(0);
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getMemberList "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}	
			logger.debug("Exit : public @ResponseBody List getMemberList(@RequestBody MemberVO requestVO)")	;
			return new ResponseEntity(memberVO, HttpStatus.OK);
		}
		
		@RequestMapping(value="/get/buildingWiseMembers", method = RequestMethod.POST)
		public ResponseEntity getMemberListBuildingWise(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody MemberVO requestVO){
			List memberList=null;
			MemberVO memberVO=new MemberVO();
			APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry :public @ResponseBody List getMemberListBuildingWise(@RequestBody MemberVO requestVO)")	;
			try {
				if(requestVO.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
				memberList=memberServiceBean.getMemberListBuildingWise(requestVO.getAptID(),requestVO.getSocietyID());
				
				if(memberList.size()>0){
					memberVO.setStatusCode(1);
					memberVO.setSocietyID(requestVO.getSocietyID());
					memberVO.setObjectList(memberList);
				}else
					memberVO.setStatusCode(0);
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getMemberListBuildingWise "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}	
			logger.debug("Exit : public @ResponseBody List getMemberListBuildingWise(@RequestBody MemberVO requestVO)")	;
			return new ResponseEntity(memberVO, HttpStatus.OK);
		}
		
		@RequestMapping(value="/get/buildingWiseForApt", method = RequestMethod.POST)
		public ResponseEntity getMemberListOfApartmentsBuilding(@RequestBody MemberVO requestVO){
			List memberList=null;
			List convertedList=null;
			APIResponseVO apiResponse=new APIResponseVO();
			MemberVO memberVO=new MemberVO();
			logger.debug("Entry :public @ResponseBody List getMemberListOfApartmentsBuilding(@RequestBody MemberVO requestVO)")	;
			try {
				if(requestVO.getAptID()>0){
				
				memberList=memberServiceBean.getMemberListOfApartmentsBuilding(requestVO.getAptID());
				convertedList=memberServiceBean.convertMemberListForWebservice(memberList);
				
				if(convertedList.size()>0){
					memberVO.setStatusCode(1);
					memberVO.setObjectList(convertedList);
				}else
					memberVO.setStatusCode(0);
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getMemberListOfApartmentsBuilding "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}	
			logger.debug("Exit :public @ResponseBody List getMemberListOfApartmentsBuilding(@RequestBody MemberVO requestVO)")	;
			return new ResponseEntity(memberVO, HttpStatus.OK);
		}
		
		@RequestMapping(value="/memberInfo", method = RequestMethod.POST)
		public ResponseEntity getMemberInfo(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody MemberVO requestVO){
			MemberWSVO memberVO=new MemberWSVO();
			APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry :public @ResponseBody MemberWSVO getMemberInfo(@PathVariable String emailID)"+requestVO.getEmail())	;
			try {
				if(requestVO.getEmail().length()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
				memberVO=memberServiceBean.getMembersInfoWithMail(requestVO.getEmail());
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getMemberListOfApartmentsBuilding "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
			logger.debug("Exit :public @ResponseBody MemberWSVO getMemberInfo(@PathVariable String emailID)")	;
			return new ResponseEntity(memberVO, HttpStatus.OK);
		}
		
				
			 

		/**************************Tenants API************************************/
		
	    @RequestMapping(value="/get/tenant", method = RequestMethod.POST)
		public ResponseEntity getTenants(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RenterVO renterVO) {
			RenterVO rentrVO = new RenterVO();
			APIResponseVO apiResponse=new APIResponseVO();
			MemberVO memberVO= new MemberVO();
			List renterMemberList=null;
			List addressList=null;
				logger.debug("Entry : public @ResponseBody List getTenants(@RequestBody RenterVO renterVO)")	;
			try {
				if(renterVO.getRenterID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					renterVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				rentrVO =  memberServiceBean.getRenterDetails(renterVO.getAptID(),renterVO.getRenterID());				
				logger.info("Renter ID: "+rentrVO.renterID);
			 
				if(renterVO.getAptID()>0){
				
				memberVO=memberServiceBean.getMemberInfo(renterVO.getAptID());	
				rentrVO.setMemberName(memberVO.getFlatNo()+" - "+memberVO.getFullName());
				
				addressList=addressServiceBean.getAddressDetailsRenter(rentrVO.getRenterID(), "R", memberVO.getSocietyID(), "0");	
				logger.info("Renter Address Size: "+addressList.size());
				rentrVO.setAddressList(addressList);
				}							
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getTenants "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody List getTenants(@RequestBody RenterVO renterVO)")	;						
			return new ResponseEntity(rentrVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/insert/tenant",  method = RequestMethod.POST  )			
		public ResponseEntity insertTenant(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RenterVO renterVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody int insertTenant( RenterVO renterVO)")	;
			try {
				logger.info("insertTenant method Apartment ID: "+renterVO.getAptID());
				if(renterVO.getAptID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					renterVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
			    success=memberServiceBean.insertTenantForWebservice(renterVO);
			    if(success>0){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Tenant has been added successfully.");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to add tenant details.");
					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				}
			    logger.info("Result Insert Tenant : "+success);		
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in insertTenant "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int insertTenant( RenterVO renterVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/update/tenant",  method = RequestMethod.POST  )			
		public ResponseEntity updateTenant(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RenterVO renterVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody int updateTenant( RenterVO renterVO)")	;
			try {
				logger.info("updateTenant method Renter ID: "+renterVO.getRenterID()+" Apartment ID: "+renterVO.getAptID());
				if(renterVO.getRenterID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					renterVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
			    success=memberServiceBean.updateRenterDetails(renterVO, renterVO.getRenterID());
			    if(success>0){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Tenant details has been updated successfully.");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to update tenant details.");
					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				}
				logger.info("Result Update Tenant : "+success);				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in updateTenant "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int updateTenant( RenterVO renterVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/delete/tenant", method = RequestMethod.POST	)
		public ResponseEntity deleteTenant(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RenterVO renterVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public @ResponseBody int deleteTenant(@RequestBody RenterVO renterVO)")	;
			try {
				if(renterVO.getRenterID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					renterVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				success =  memberServiceBean.deleteRenterDetails(renterVO.getRenterID(), renterVO.getAptID());		
				if(success>0){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Tenant has been deleted successfully.");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to delete tenant.");
					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				}
				logger.info("Result Delete Tenant: "+success);
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in deleteTenant "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
									
			
			logger.debug("Exit : public @ResponseBody List deleteTenant(@RequestBody RenterVO renterVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/insert/tenant/member",  method = RequestMethod.POST  )			
		public ResponseEntity insertTenantMember(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RenterVO renterVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody int insertTenantMember( RenterVO renterVO)")	;
			try {
				logger.info("insertTenantMember method Renter ID: "+renterVO.getRenterID());
				if(renterVO.getRenterID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					renterVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
			    success=memberServiceBean.addRenterMemberDetails(renterVO);
			    
			    if(success>0){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Tenant has been added successfully.");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to add tenant member.");
					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				}
				
				logger.info("Result Insert Tenant Member: "+success);				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in insertTenantMember "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int insertTenantMember( RenterVO renterVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/delete/tenant/member", method = RequestMethod.POST)
		public ResponseEntity deleteTenantMember(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RenterVO renterVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody int deleteTenantMember(@PathVariable String renterMemberID)")	;
			try {
				if(renterVO.getRenterID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					renterVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				success =  memberServiceBean.deleteRenterMember(renterVO.getRenterID());		
				if(success>0){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Tenants member has been deleted successfully.");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to delete tenant member.");
					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				}
				logger.info("Result Delete Tenant Member: "+success);				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in deleteTenantMember "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody List deleteTenantMember(@PathVariable String renterMemberID)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
		
		@RequestMapping(value="/tenantList", method = RequestMethod.POST)
		public ResponseEntity getAllTenants(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RenterVO renterVO) {
			RenterVO rentrVO = new RenterVO();
			MemberVO memberVO= new MemberVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List renterList=null;
			List addressList=null;
				logger.debug("Entry : public @ResponseBody List getAllTenants(@RequestBody RenterVO renterVO)")	;
			try {
				if(renterVO.getAptID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					renterVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				renterList =  memberServiceBean.getRenterHistoryDetails(renterVO.getAptID());				
				logger.debug("Renter List size: "+renterList.size());
				
				memberVO=memberServiceBean.getMemberInfo(renterVO.getAptID());	
				
				 for(int i=0;i<renterList.size();i++){
					 RenterVO rentrVO1 = (RenterVO) renterList.get(i);
					// logger.debug(rentrVO1.getRenterID()+" Renter: "+rentrVO1.isCurrentRenter);
					  if(rentrVO1.isCurrentRenter.equalsIgnoreCase("1")){
						  addressList=addressServiceBean.getAddressDetailsRenter(rentrVO1.getRenterID(), "R",memberVO.getSocietyID(), "0");
						  rentrVO1.setAddressList(addressList);
						  //logger.debug("Addres List size: "+addressList.size());
					  }
				
				 }
				 logger.info("Renter List size: "+renterList.size());
				rentrVO.setRenterMembers(renterList);				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getAllTenants "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody List getAllTenants(@RequestBody RenterVO renterVO)")	;						
			return new ResponseEntity(rentrVO, HttpStatus.OK);
	 
		}
		
		@RequestMapping(value="/tenantMemberList", method = RequestMethod.POST)
		public ResponseEntity getTenantMemberList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RenterVO renterVO) {
			RenterVO rentrVO = new RenterVO();
			APIResponseVO apiResponse=new APIResponseVO();
			List renterMemberList=null;
			
			logger.debug("Entry : public @ResponseBody RenterVO getTenantMemberList(@RequestBody RenterVO renterVO)");
			try {
				if(renterVO.getRenterID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					renterVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				renterMemberList=memberServiceBean.getRenterMemberDetails(renterVO.getRenterID());					
				logger.info("Renter Member List size: "+renterMemberList.size());
				rentrVO.setRenterMembers(renterMemberList);				
			}else{
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Exception in getTenantMemberList "+e);
			apiResponse.setStatusCode(404);
			apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
			return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
		}
			logger.debug("Exit : public @ResponseBody RenterVO getTenantMemberList(@RequestBody RenterVO renterVO)")	;						
			return new ResponseEntity(rentrVO, HttpStatus.OK);
	 
		}
		
		
/**************************Vehicle API************************************/
		
	    @RequestMapping(value="/vehicleList", method = RequestMethod.POST)
		public ResponseEntity getVehicles(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody VehicleVO vehicleVO) {
	    	JSONResponseVO jsonVo=new JSONResponseVO ();
	    	APIResponseVO apiResponse=new APIResponseVO();
			List vehicleList=null;
			
		logger.debug("Entry : public @ResponseBody RenterVO getVehicles(@RequestBody VehicleVO vehicleVO)")	;
			try {
				if(vehicleVO.getMemberID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					vehicleVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
				vehicleList =  memberServiceBean.getVehicleList(vehicleVO.getMemberID());				
				logger.info("MemberID: "+vehicleVO.getMemberID()+" Vehicle Size: "+vehicleList.size());			     
				
				 if(vehicleList.size()>0){
					 vehicleVO.setStatusCode(1);
					 vehicleVO.setObjectList(vehicleList);
				}else{
					vehicleVO.setStatusCode(0);
					
				}		
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getVehicles "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody RenterVO getVehicles(@RequestBody VehicleVO vehicleVO)")	;						
			return new ResponseEntity(vehicleVO, HttpStatus.OK);
	 
		}
	    
	    @RequestMapping(value="/insert/vehicle",  method = RequestMethod.POST  )			
		public ResponseEntity insertVehicle(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody VehicleVO vehicleVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody int insertVehicle(@RequestBody VehicleVO vehicleVO)")	;
			try {
				logger.info("insertVehicle method MEMBER ID: "+vehicleVO.getMemberID());
				if(vehicleVO.getMemberID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					vehicleVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
			    success=memberServiceBean.addVehicleDetails(vehicleVO, vehicleVO.getMemberID(), vehicleVO.getSocietyID());
			    if(success>0){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Vehicle details has been added successfully.");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to add vehicle details.");					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				}
				logger.info("Result Insert Vehicle: "+success);				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in insertVehicle "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int insertVehicle( RenterVO renterVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
	    
	    @RequestMapping(value="/get/vehicle",  method = RequestMethod.POST  )			
		public ResponseEntity getVehicleDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody VehicleVO vehicleVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity getVehicleDetails(@RequestBody VehicleVO vehicleVO)")	;
			try {
				
				if(vehicleVO.getVehicleID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					vehicleVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
					vehicleVO=memberServiceBean.getVehicleDetails(vehicleVO.getVehicleID());
					
			    if(vehicleVO.getVehicleNo().length()<0){					
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to get vehicle details.");					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				}
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getVehicleDetails "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity getVehicleDetails( VehicleVO vehicleVO)")	;						
			return new ResponseEntity(vehicleVO, HttpStatus.OK);
	 
		}
	 
	    @RequestMapping(value="/update/vehicle",  method = RequestMethod.POST  )			
		public ResponseEntity updateVehicle(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody VehicleVO vehicleVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public ResponseEntity updateVehicle(@RequestBody VehicleVO vehicleVO)")	;
			try {
				logger.info("Update Vehicle method Vehicle ID: "+vehicleVO.getVehicleID());
				if(vehicleVO.getVehicleID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					vehicleVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
			      success=memberServiceBean.updateVehicleDetails(vehicleVO, vehicleVO.getVehicleID());
			    if(success>0){
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Vehicle details has been updated successfully.");
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to update vehicle details.");					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				}
					
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in updateVehicle "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public ResponseEntity updateVehicle(@RequestBody VehicleVO vehicleVO)")	;						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
	    
		@RequestMapping(value="/delete/vehicle", method = RequestMethod.POST)
		public ResponseEntity deleteVehicle(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody VehicleVO vehicleVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
				logger.debug("Entry : public @ResponseBody int deleteVehicle(@RequestBody VehicleVO vehicleVO) ")	;
			try {
				if(vehicleVO.getVehicleID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					vehicleVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
				success =  memberServiceBean.deleteVehicleDetails(vehicleVO.getVehicleID());		
				
				 if(success>0){
						apiResponse.setStatusCode(200);
						apiResponse.setMessage("Vehicle details has been deleted successfully.");
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage("vehicle details not deleted.");
						
						return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
					}
				
				logger.info("Vehicle ID : "+vehicleVO.getVehicleID()+" Result Delete Vehicle: "+success);				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in deleteVehicle "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit : public @ResponseBody int deleteVehicle(@RequestBody VehicleVO vehicleVO)");						
			return new ResponseEntity(apiResponse, HttpStatus.OK);
	 
		}
		
	/**************************Member API************************************/	
		
		@RequestMapping(value="/get",  method = RequestMethod.POST  )			
		public ResponseEntity getMemberDetails(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody MemberVO memberVO) {
			int success=0;
			APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity getMemberDetails(@RequestBody MemberVO memberVO)")	;
			try {
				
				if(memberVO.getMemberID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					memberVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
					
					memberVO=memberServiceBean.searchMembersInfo(Integer.toString(memberVO.getMemberID()));
					
			    if(memberVO.getFullName().length()<0){					
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to get member details.");					
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				}
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getMemberDetails "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			logger.debug("Exit :public ResponseEntity getMemberDetails(@RequestBody MemberVO memberVO)");						
			return new ResponseEntity(memberVO, HttpStatus.OK);
	 
		}
				
		
		 @RequestMapping(value="/update",  method = RequestMethod.POST  )			
			public ResponseEntity updateMember(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody MemberVO memberVO) {
				int success=0;
				APIResponseVO apiResponse=new APIResponseVO();
					logger.debug("Entry : public ResponseEntity updateMember(@RequestBody MemberVO memberVO) ")	;
				try {
					logger.info("Update Vehicle method Member ID: "+memberVO.getMemberID());
					if(memberVO.getMemberID()>0){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						memberVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));	
				      success=memberServiceBean.updateMemberDetails(memberVO, memberVO.getMemberID());
				    if(success>0){
						apiResponse.setStatusCode(200);
						apiResponse.setMessage("Member details has been updated successfully.");
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage("Unable to update member details.");					
						return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
					}
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					logger.error("Exception in updateMember "+e);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity updateMember(@RequestBody MemberVO memberVO) ")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);
		 
			}
		 
		 /**************************Document  API*******************************/	
		 
		 @RequestMapping(value="/get/documentList", method = RequestMethod.POST)
			public ResponseEntity getMemberDocumentList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RequestWrapper requestWrapper) {
				APIResponseVO apiResponse=new APIResponseVO();
				MemberVO memberVO=new MemberVO();
				List documentList=null;
				List documentStatusList=null;
					logger.debug("Entry : public @ResponseBody List getMemberDocumentList(@RequestBody RequestWrapper requestWrapper)")	;
				try {
					if(requestWrapper.getMemberID()>0){			
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						requestWrapper.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
						
						documentList=memberServiceBean.getMemberDocumentList(requestWrapper.getMemberID());
						
						
					  if(documentList.size()>0){
						  memberVO.setStatusCode(1);
						  memberVO.setSocietyID(requestWrapper.getOrgID());
						  memberVO.setObjectList(documentList);
						  
					  }else
						  memberVO.setStatusCode(0);
					
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					logger.error("Exception in getMemberDocumentList "+e);
					apiResponse.setStatusCode(404);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public @ResponseBody List getMemberDocumentList(@RequestBody RequestWrapper requestWrapper)")	;						
				return new ResponseEntity(memberVO, HttpStatus.OK);
		 
			}
		 
		 @RequestMapping(value="/update/document", method = RequestMethod.POST)
			public ResponseEntity updateMemberDocument(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RptDocumentVO documentVO) {
				APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public @ResponseBody List updateMemberDocumentList(@RequestBody RptDocumentVO documentVO)")	;
			 int success=0;	
			try {
					if(documentVO.getMemberID()>0){		
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						documentVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
						
						success=memberServiceBean.updateDocument(documentVO);
						 if(success>0){
								apiResponse.setStatusCode(200);
								apiResponse.setMessage("Member document has been updated successfully.");
							}else{
								apiResponse.setStatusCode(400);
								apiResponse.setMessage("Unable to update member document details.");					
								return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
							}
					
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					logger.error("Exception in updateMemberDocument "+e);
					apiResponse.setStatusCode(404);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public @ResponseBody List updateMemberDocumentList(@RequestBody RptDocumentVO documentVO)")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);
		 
			}
		    
		 
		 @RequestMapping(value="/update/documentList", method = RequestMethod.POST)
			public ResponseEntity updateMemberDocumentList(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody MemberVO memberVO) {
				APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public @ResponseBody List updateMemberDocumentList(@RequestBody RequestWrapper requestWrapper)")	;
			 int success=0;	
			try {
					if(memberVO.getMemberID()>0){	
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						memberVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
						
						success=memberServiceBean.updateDocumentList(memberVO);
						 if(success>0){
								apiResponse.setStatusCode(200);
								apiResponse.setMessage("Member document has been updated successfully.");
							}else{
								apiResponse.setStatusCode(400);
								apiResponse.setMessage("Unable to update member document details.");					
								return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
							}
					
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					logger.error("Exception in updateMemberDocumentList "+e);
					apiResponse.setStatusCode(404);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public @ResponseBody List updateMemberDocumentList(@RequestBody RequestWrapper requestWrapper)")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);
		 
			}
		 
		 /**************************Mortgage  API*******************************/	
			 
		 @RequestMapping(value="/insert/mortgage", method = RequestMethod.POST)
			public ResponseEntity insertMortgage(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RptMorgageVO mortgageVO) {
				APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry :public ResponseEntity insertMortgage(@RequestBody RptMorgageVO mortgageVO)")	;
			 int success=0;	
			try {
					if(mortgageVO.getMemberId()>0){	
						
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						mortgageVO.setSocietyId(Integer.parseInt(decrptVO.getOrgID()));
						
						success=reportService.insertMoragageDetails(mortgageVO);
						 if(success>0){
								apiResponse.setStatusCode(200);
								apiResponse.setMessage("Mortgage details has been inserted successfully.");
							}else{
								apiResponse.setStatusCode(400);
								apiResponse.setMessage("Unable to insert mortgage details.");					
								return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
							}
					
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					logger.error("Exception in insertMortgage "+e);
					apiResponse.setStatusCode(404);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity insertMortgage(@RequestBody RptMorgageVO morgageVO)")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);
		 
			}
		 
		 
		 @RequestMapping(value="/update/mortgage", method = RequestMethod.POST)
			public ResponseEntity updateMortgage(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RptMorgageVO mortgageVO) {
				APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry :public ResponseEntity updateMortgage(@RequestBody RptMorgageVO mortgageVO)")	;
			 int success=0;	
			try {
					if(mortgageVO.getMorgId().length()>0){		
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						mortgageVO.setSocietyId(Integer.parseInt(decrptVO.getOrgID()));
						success=reportService.updateMorgage(mortgageVO, mortgageVO.getMorgId());
						 if(success>0){
								apiResponse.setStatusCode(200);
								apiResponse.setMessage("Mortgage details has been updated successfully.");
							}else{
								apiResponse.setStatusCode(400);
								apiResponse.setMessage("Unable to updated mortgage details.");					
								return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
							}
					
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					logger.error("Exception in updateMortgage "+e);
					apiResponse.setStatusCode(404);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity updateMortgage(@RequestBody RptMorgageVO morgageVO)")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);
		 
			}
		 
		 
		 @RequestMapping(value="/delete/mortgage", method = RequestMethod.POST)
			public ResponseEntity deleteMortgage(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody RptMorgageVO mortgageVO) {
				APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry :public ResponseEntity deleteMortgage(@RequestBody RptMorgageVO mortgageVO)")	;
			 int success=0;	
			try {
					if(mortgageVO.getMorgId().length()>0){		
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						mortgageVO.setSocietyId(Integer.parseInt(decrptVO.getOrgID()));
						
						success=reportService.removMorgage(mortgageVO.getMorgId());
						 if(success>0){
								apiResponse.setStatusCode(200);
								apiResponse.setMessage("Mortgage details has been deleted successfully.");
							}else{
								apiResponse.setStatusCode(400);
								apiResponse.setMessage("Unable to deleted mortgage details.");					
								return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
							}
					
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					logger.error("Exception in deleteMortgage "+e);
					apiResponse.setStatusCode(404);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity deleteMortgage(@RequestBody RptMorgageVO morgageVO)")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);
		 
			}
		 
		 
		 /**************************Nomiantion  API*******************************/	
		 @RequestMapping(value="/insert/nominee", method = RequestMethod.POST)
			public ResponseEntity insertNominee(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody NominationRegisterVO nomRegVO) {
				APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry :public ResponseEntity insertNominee(@RequestBody NominationRegisterVO nomRegVO)")	;
			 int success=0;	
			try {
					if(nomRegVO.getMemberId()>0){	
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						nomRegVO.setSocietyId(decrptVO.getOrgID());
						success=nominationRegService.addNominations(nomRegVO.getNomineeList());
						 if(success>0){
								apiResponse.setStatusCode(200);
								apiResponse.setMessage("Nominee details has been inserted successfully.");
							}else{
								apiResponse.setStatusCode(400);
								apiResponse.setMessage("Unable to insert nominee details.");					
								return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
							}
					
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					logger.error("Exception in insertNominee "+e);
					apiResponse.setStatusCode(404);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity insertNominee(@RequestBody NominationRegisterVO nomRegVO)")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);
		 
			}
		 
		 @RequestMapping(value="/update/nominee", method = RequestMethod.POST)
			public ResponseEntity updateNominee(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody NominationRegisterVO nomRegVO) {
				APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry :public ResponseEntity updateNominee(@RequestBody NominationRegisterVO nomRegVO)")	;
			 int success=0;	
			try {
					if(nomRegVO.getNominationId()>0 && nomRegVO.getMemberId()>0){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						nomRegVO.setSocietyId(decrptVO.getOrgID());
						success=nominationRegService.updateRecordingDate(nomRegVO);
						
						 if(success>0){
								apiResponse.setStatusCode(200);
								apiResponse.setMessage("Nomination details has been updated successfully.");
							}else{
								apiResponse.setStatusCode(400);
								apiResponse.setMessage("Unable to update nomination details.");					
								return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
							}
					
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					logger.error("Exception in updateNominee "+e);
					apiResponse.setStatusCode(404);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity updateNominee(@RequestBody NominationRegisterVO nomRegVO)")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);
		 
			}
		 
		 
		 @RequestMapping(value="/delete/nominee", method = RequestMethod.POST)
			public ResponseEntity deleteNominee(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody NominationRegisterVO nomRegVO) {
				APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry :public ResponseEntity deleteNominee(@RequestBody NominationRegisterVO nomRegVO)")	;
			 int success=0;	
			try {
					if(nomRegVO.getMemberId()>0){
						UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
						nomRegVO.setSocietyId(decrptVO.getOrgID());
						success=nominationRegService.deleteNominees(nomRegVO);
						
						 if(success>0){
								apiResponse.setStatusCode(200);
								apiResponse.setMessage("Nomination details has been deleted successfully.");
							}else{
								apiResponse.setStatusCode(400);
								apiResponse.setMessage("Unable to deleted nomination details.");					
								return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
							}
					
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
				} catch (Exception e) {
					logger.error("Exception in deleteNominee "+e);
					apiResponse.setStatusCode(404);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
				logger.debug("Exit : public ResponseEntity deleteNominee(@RequestBody NominationRegisterVO nomRegVO)")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);
		 
			}
		 
		 
		 /**************************Ticketing  API*******************************/	
		 
		@RequestMapping(value="/ticketList", method = RequestMethod.POST)
		public ResponseEntity getTicketList(@RequestBody TicketVO requestVO) {
				APIResponseVO apiResponse=new APIResponseVO();
				TicketVO ticketVO =new TicketVO();
				SocietyVO societyVO=new SocietyVO();
				logger.debug("Entry : public ResponseEntity getTicketList(@RequestBody TicketVO requestVO)");
			try {
					if(requestVO.getStatus_name().length()>0 && requestVO.getUser_id()>0){
						if(requestVO.getOrg_id()!=0){
							  societyVO=societyService.getSocietyDetails(requestVO.getOrg_id());
							}else{
								societyVO.setOrgID(0);
							}
						
							ticketVO=memberServiceBean.getTicketList(societyVO.getOrgID(),requestVO.getUser_id(), requestVO.getStatus_name());	
													
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
			} catch (Exception e) {
				logger.error("Exception in getTicketList "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				logger.debug("Exit : public ResponseEntity getTicketList(@RequestBody RenterVO renterVO)")	;						
				return new ResponseEntity(ticketVO, HttpStatus.OK);
		 
		}
		
		@RequestMapping(value="/get/ticket", method = RequestMethod.POST)
		public ResponseEntity getTicketDetails(@RequestBody TicketVO requestVO) {
				APIResponseVO apiResponse=new APIResponseVO();
				TicketVO ticketVO =new TicketVO();
				logger.debug("Entry : public ResponseEntity getTicketDetails(@RequestBody TicketVO requestVO)");
			try {
					if(requestVO.getStatus_name().length()>0 &&  requestVO.getTicket_no()>0 ){
						ticketVO=memberServiceBean.getTicketDetails(requestVO.getTicket_no(), requestVO.getStatus_name());					
													
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
			} catch (Exception e) {
				logger.error("Exception in getTicketDetails "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				logger.debug("Exit : public ResponseEntity getTicketDetails(@RequestBody TicketVO requestVO)")	;						
				return new ResponseEntity(ticketVO, HttpStatus.OK);
		 
		}
		
		
		@RequestMapping(value="/insert/ticket", method = RequestMethod.POST)
		public ResponseEntity insertTicket(@RequestBody TicketVO requestVO) {
				APIResponseVO apiResponse=new APIResponseVO();
				int success=0;
				logger.debug("Entry : public ResponseEntity insertTicket(@RequestBody TicketVO requestVO)");
			try {
					if( requestVO.getUser_id()>0 && requestVO.getSubject().length()>0 &&  requestVO.getMessage().length()>0 && requestVO.getTopic_id()>0 ){
						success=memberServiceBean.createTicket(requestVO);		
						
						if(success>0){
							apiResponse.setStatusCode(200);
							apiResponse.setMessage("Request has been created successully");
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage("Unable to create ticket ");					
							return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
						}
													
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
			} catch (Exception e) {
				logger.error("Exception in insertTicket "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				logger.debug("Exit : public ResponseEntity insertTicket(@RequestBody TicketVO requestVO)")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);
		 
		}
		
		
		@RequestMapping(value="/tickets/helpTopicList", method = RequestMethod.POST)
		public ResponseEntity getTicketHelpTopicList(@RequestBody TicketVO requestVO) {
				APIResponseVO apiResponse=new APIResponseVO();
				TicketVO ticketVO =new TicketVO();
				logger.debug("Entry : public ResponseEntity getTicketHelpTopics(@RequestBody TicketVO requestVO)");
			try {
					if(requestVO.getType().length()>0  ){
						ticketVO=memberServiceBean.getTicketHelpTopics(requestVO.getType());					
													
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
			} catch (Exception e) {
				logger.error("Exception in getTicketHelpTopicList "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				logger.debug("Exit : public ResponseEntity getTicketHelpTopicList(@RequestBody TicketVO requestVO)")	;						
				return new ResponseEntity(ticketVO, HttpStatus.OK);
		 
		}
		 
		
		@RequestMapping(value="/get/orgRulesRegulations", method = RequestMethod.POST)
		public ResponseEntity getOrgRulesAndRegulations(@RequestBody TicketVO requestVO) {
				APIResponseVO apiResponse=new APIResponseVO();
				TicketVO ticketVO =new TicketVO();
				logger.debug("Entry : public ResponseEntity getOrgRules(@RequestBody TicketVO requestVO)");
			try {
					if(requestVO.getOrg_id()>0 &&  requestVO.getType().length()>0 ){
						ticketVO=memberServiceBean.getOrgRulesAndRegulations(requestVO.getOrg_id(), requestVO.getType());					
													
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
			} catch (Exception e) {
				logger.error("Exception in getOrgRules "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				logger.debug("Exit : public ResponseEntity getOrgRules(@RequestBody TicketVO requestVO)")	;						
				return new ResponseEntity(ticketVO, HttpStatus.OK);
		 
		}
		 
		//======API for getting memberList by groupName===========//
		@RequestMapping(value="/memberListFromGroupName", method = RequestMethod.POST)
		public ResponseEntity getMembersListFromGroup(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody MemberVO requestVO){
			List memberList=null;
			MemberVO memberVO=new MemberVO();
			APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry :public @ResponseBody List getMembersListFromGroup(@RequestBody MemberVO requestVO)"+requestVO.getSocietyID())	;
			try {
				if(requestVO.getSocietyID()>0){
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					requestVO.setSocietyID(Integer.parseInt(decrptVO.getOrgID()));
				memberList=memberServiceBean.getMembersListFromGroup(requestVO.getSocietyID(), memberVO.getType());
				
				if(memberList.size()>0){
					memberVO.setStatusCode(1);
					memberVO.setSocietyID(requestVO.getSocietyID());
					memberVO.setObjectList(memberList);
				}else
					memberVO.setStatusCode(0);
				
				}else{
					apiResponse.setStatusCode(400);
					apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
					return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
				}
			} catch (Exception e) {
				logger.error("Exception in getMembersListFromGroup "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}	
			logger.debug("Exit : public @ResponseBody List getMembersListFromGroup(@RequestBody MemberVO requestVO)")	;
			return new ResponseEntity(memberVO, HttpStatus.OK);
		}
		
		
		//====================++ SMS Notifications ++============================//
		@RequestMapping(value="/sendSMSNotifications", method = RequestMethod.POST)
		public ResponseEntity sendSMSNotificationsToAGroup(@RequestBody SMSMessage requestVO) {
				APIResponseVO apiResponse=new APIResponseVO();
				NotificationVO notificationVO=new NotificationVO();
				logger.debug("Entry : public ResponseEntity sendSMSNotificationsToAGroup(@RequestBody SMSMessage requestVO)");
			try {
					if((requestVO.getOrgID()>0) && (requestVO.getGroupName().length()>0) ){
						notificationVO=memberServiceBean.sendSMSandPushNotificationsToAGroup(requestVO);			
									
						if(notificationVO.getStatusCode()==200){
                         
							apiResponse.setStatusCode(200);
							apiResponse.setMessage("SMS has been sent successfully");
						}else if(notificationVO.getStatusCode()==404){
							apiResponse.setStatusCode(404);
							apiResponse.setMessage("No mobile number found for SMS");
							
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage("Unable to send SMS");					
							return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
						}
						
					}else{
						apiResponse.setStatusCode(400);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
			} catch (Exception e) {
				logger.error("Exception in public ResponseEntity sendSMSNotificationsToAGroup(@RequestBody SMSMessage requestVO) "+e);
				apiResponse.setStatusCode(404);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
				logger.debug("Exit : public ResponseEntity sendSMSNotificationsToAGroup(@RequestBody SMSMessage requestVO)")	;						
				return new ResponseEntity(apiResponse, HttpStatus.OK);
		 
		}
		 
		//====================++ Email Notifications ++============================//
				@RequestMapping(value="/sendEmailNotifications", method = RequestMethod.POST)
				public ResponseEntity sendEmailNotificationsToAGroup(@RequestBody EmailMessage requestVO) {
						APIResponseVO apiResponse=new APIResponseVO();
						NotificationVO notificationVO=new NotificationVO();
						logger.debug("Entry : public ResponseEntity sendEmailNotificationsToAGroup(@RequestBody EmailMessage requestVO)");
					try {
							if((requestVO.getOrgID()>0) && (requestVO.getGroupName().length()>0) ){
								notificationVO=memberServiceBean.sendEmailandPushNotificationsToAGroup(requestVO);			
											
								if(notificationVO.getStatusCode()==200){
		                         
									apiResponse.setStatusCode(200);
									apiResponse.setMessage("Email has been sent successfully");
								}else if(notificationVO.getStatusCode()==404){
									apiResponse.setStatusCode(404);
									apiResponse.setMessage("No email address found for Email broadcast");
									
								}else{
									apiResponse.setStatusCode(400);
									apiResponse.setMessage("Unable to send Email");					
									return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
								}
								
							}else{
								apiResponse.setStatusCode(400);
								apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
								return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
							}
					} catch (Exception e) {
						logger.error("Exception in public ResponseEntity sendEmailNotificationsToAGroup(@RequestBody EmailMessage requestVO) "+e);
						apiResponse.setStatusCode(404);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
					}
						logger.debug("Exit : public ResponseEntity sendEmailNotificationsToAGroup(@RequestBody EmailMessage requestVO)")	;						
						return new ResponseEntity(apiResponse, HttpStatus.OK);
				 
				}
		 
				/*	------------------API for Sending Payment reminder tasks ----------------- */
			    @RequestMapping(value="/sendPaymentReminder", method = RequestMethod.POST)
				public ResponseEntity sendPaymentReminder(@RequestBody EventVO eventVO) {
					APIResponseVO apiResponse=new APIResponseVO();
					int successFlag=0;
					logger.debug("Entry : public ResponseEntity sendPaymentReminder(@RequestBody MemberVO memberVO)")	;
					try {
		                	
						if(eventVO.getMemberID()>0){
										
						   NotificationVO notificationVO=notificationService.getMemberBalance(eventVO);
						 
						   if(notificationVO.getSentEmailsCount()==0){
								logger.info("Unable to send payment reminder to MemberID: "+eventVO.getMemberID());
								apiResponse.setStatusCode(400);
								apiResponse.setMessage("Unable to send payment reminder ");
								return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
							}else{
								apiResponse.setStatusCode(200);
								apiResponse.setMessage("Reminder has been scheduled.");
								logger.info("Reminder has been scheduled for MemberID: "+eventVO.getMemberID());
							}
						   		
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
						
					} catch (Exception e) {
						logger.error("Exception in sendPaymentReminder(@RequestBody MemberVO memberVO) "+e);
						apiResponse.setStatusCode(404);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
					}
					logger.debug("Exit : public ResponseEntity sendPaymentReminder(@RequestBody MemberVO memberVO)")	;						
					return new ResponseEntity(apiResponse, HttpStatus.OK);	
			 
				}
			    
			 /*   ------------------API for Sending Payment reminder tasks -----------------*/
			    @RequestMapping(value="/sendPaymentReminderToSociety", method = RequestMethod.POST)
				public ResponseEntity sendPaymentReminderToSociety(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody EventVO eventVO) {
					APIResponseVO apiResponse=new APIResponseVO();
					int successFlag=0;
					logger.debug("Entry : public ResponseEntity sendPaymentReminder(@RequestBody MemberVO memberVO)")	;
					try {
		                	
						if(eventVO.getSocietyID()>0){
							UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);		
						    notificationService.insertAuditActivity(decrptVO.getUserID(),decrptVO.getAppID(),decrptVO.getOrgID(),"reminderToAll","reminder");
						 
						   NotificationVO notificationVO=notificationService.getMemberBalanceForSociety(eventVO);						   
						 			  
						   apiResponse.setStatusCode(200);
							apiResponse.setMessage("Reminder has been scheduled.");
							logger.info("Reminder has been scheduled for orgID: "+eventVO.getSocietyID()+" By UserID: "+decrptVO.getUserID());
					
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
						
					} catch (Exception e) {
						logger.error("Exception in sendPaymentReminderToSociety(@RequestBody MemberVO memberVO) "+e);
						apiResponse.setStatusCode(404);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
					}
					logger.debug("Exit : public ResponseEntity sendPaymentReminder(@RequestBody MemberVO memberVO)")	;						
					return new ResponseEntity(apiResponse, HttpStatus.OK);	
			 
				}

			    @RequestMapping(value="/sendPaymentReminderToCustomer", method = RequestMethod.POST)
				public ResponseEntity sendPaymentReminderToCustomer(@RequestBody EventVO eventVO) {
					APIResponseVO apiResponse=new APIResponseVO();
					int successFlag=0;
					logger.debug("Entry : public ResponseEntity sendPaymentReminderToCustomer(@RequestBody EventVO eventVO)")	;
					try {
		                	
						if(eventVO.getMemberID()>0){
										
							NotificationVO notificationVO=notificationService.getCustomerBalance(eventVO);
							
						   if(notificationVO.getSentEmailsCount()==0){
								logger.info("Unable to send payment reminder to OrgId: "+eventVO.getSocietyID()+" of LedgerID: "+eventVO.getMemberID());
								apiResponse.setStatusCode(400);
								apiResponse.setMessage("Unable to send payment reminder ");
								return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
							}else{
								apiResponse.setStatusCode(200);
								apiResponse.setMessage("Reminder has been scheduled.");
								logger.info("Reminder has been scheduled for OrgId: "+eventVO.getSocietyID()+" LedgerID: "+eventVO.getMemberID());
							}
						   		
						}else{
							apiResponse.setStatusCode(400);
							apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
							return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
						}
						
					} catch (Exception e) {
						logger.error("Exception in sendPaymentReminderToCustomer(@RequestBody EventVO eventVO) "+e);
						apiResponse.setStatusCode(404);
						apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
						return new ResponseEntity(apiResponse, HttpStatus.NO_CONTENT);
					}
					logger.debug("Exit : public ResponseEntity sendPaymentReminderToCustomer(@RequestBody EventVO eventVO)")	;						
					return new ResponseEntity(apiResponse, HttpStatus.OK);	
			 
				}


				public void setNotificationService(NotificationService notificationService) {
					this.notificationService = notificationService;
				}
				
	}

