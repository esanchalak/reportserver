package com.emanager.webservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.ClientAbortException;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.adminReports.Services.PrintReportService;
import com.emanager.server.adminReports.valueObject.PrintReportVO;
import com.emanager.server.commonUtils.domainObject.CommonUtility;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.financialReports.valueObject.RptTransactionVO;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.taskAndEventManagement.Service.NotificationService;
import com.emanager.server.taskAndEventManagement.valueObject.EmailMessage;
import com.emanager.server.taskAndEventManagement.valueObject.NotificationVO;
import com.emanager.server.userManagement.valueObject.UserVO;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;


@Controller
@RequestMapping("/secure/download/admin/")
public class JSONAdminDownloadController {
	
	private static final Logger logger = Logger.getLogger(JSONAdminDownloadController.class);
	@Autowired
	MemberService memberServiceBean;
  
    @Autowired
    PrintReportService printReportService;
    
    @Autowired
    ReportService reportService;
    
    @Autowired
    SocietyService societyService;
    
    @Autowired
    NotificationService notificationService;
    
    public ConfigManager configManager =new ConfigManager();
    public EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
	public final String HEADER_SECURITY_TOKEN = "X-AuthToken";    
	CommonUtility commonUtilty = new CommonUtility();
	
    /*Sample body contents for PrintReportVO */
	/*{
		"printParam":{
		"orgID":"11",
		"ledgerID":"1",		
		"fromDate":"2016-04-01",
		"toDate":"2016-06-30"
		"type":"bank" //Type of report: bank: Bank Register, cash:Cash Register, transaction: transaction report 				 
		},
		"reportContentType":"pdf"
		
		}*/
	@RequestMapping(value="/transactionReport", method = RequestMethod.POST)
	public void downloadTransactionReport(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
		TransactionVO printTxVO=new TransactionVO();
		APIResponseVO apiResponse=new APIResponseVO();
		HashMap<String, Object> param= printVO.getPrintParam();
		logger.debug("Entry : public @ResponseBody PrintReportVO downloadTransactionReport(@RequestBody PrintReportVO printVO) ");
		try {
			
			UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
			param.put("orgID", decrptVO.getOrgID());
			
				printTxVO.setPrintParam(param);
				String ledgerID=(String) param.get("ledgerID");
			    
				printTxVO.setFromDate((String) param.get("fromDate"));
				printTxVO.setToDate((String) param.get("toDate"));
				printTxVO.setLedgerID(Integer.parseInt(ledgerID));
				printTxVO.setType((String)printTxVO.getPrintParam().get("type"));
				
				printTxVO.setReportContentType(printVO.getReportContentType());
				if(printTxVO.getType().equalsIgnoreCase("bank")){
					printTxVO.setReportID("bankTransaction");
				}else if (printTxVO.getType().equalsIgnoreCase("cash")){
					printTxVO.setReportID("cashTransaction");
				}else if (printTxVO.getType().equalsIgnoreCase("transaction")){
					printTxVO.setReportID("accountLedger");
				}
									
				printTxVO.setSourceType("BEAN");
				printVO=printReportService.printTransactionReport(printTxVO);
				printVO.setBeanList(null);
								
				FileInputStream myStream = new FileInputStream(printVO.getReportURL());

				// Set the content type and attachment header.
				response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
				response.addHeader("x-filename",printVO.getReportID());
				response.setContentType("application/pdf");
				
				// Copy the stream to the response's output stream.
				IOUtils.copy(myStream, response.getOutputStream());				

				response.flushBuffer();
			
		} catch (IOException e) {
			logger.error("Exception downloadTransactionReport "+e);
			
		}
		logger.debug("Exit : public @ResponseBody PrintReportVO downloadTransactionReport(@RequestBody PrintReportVO printVO) ");						
		
 
	}
	
	 /*Sample body contents for PrintReportVO */
		/*{
			"printParam":{
			"orgID":"11",
			"fromDate":"2016-04-01",
			"toDate":"2016-06-30"
			"type":"IE" //Type of report: IE: Income Expense report, BS:Balance Sheet, TB: trial balance 				 
			 RP:Receipt Payment Report
			},
			"reportContentType":"pdf"
			
			}*/
		@RequestMapping(value="/incomeExpenseReport", method = RequestMethod.POST)
		public void downloadIncomeExpenseReport(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
			ReportVO printTxVO=new ReportVO();
			APIResponseVO apiResponse=new APIResponseVO();
			HashMap<String, Object> param= printVO.getPrintParam();
			logger.debug("Entry : public @ResponseBody PrintReportVO downloadIncomeExpenseReport(@RequestBody PrintReportVO printVO) ");
			try {
				
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				param.put("orgID", decrptVO.getOrgID());
				
					printTxVO.setPrintParam(param);
				    
					printTxVO.setFromDate((String) param.get("fromDate"));
					printTxVO.setUptDate((String) param.get("toDate"));
					
					String sourceType=(String) param.get("type");
					printTxVO.setSourceType("BEAN");
					printTxVO.setReportContentType(printVO.getReportContentType());
					if(sourceType.equalsIgnoreCase("IE")){
						//printTxVO.setReportID("template.incomeExpense");						
						printVO=printReportService.printIncomeExpenseReport(printTxVO);					    
					}else if (sourceType.equalsIgnoreCase("PL")){
						//printTxVO.setReportID("template.receiptPayment");				
						printVO=printReportService.printIncomeExpenseReport(printTxVO);						   
					}else if (sourceType.equalsIgnoreCase("RP")){
						//printTxVO.setReportID("template.receiptPayment");						
						printVO=printReportService.printReceiptPaymentReport(printTxVO);					   
					}else if (sourceType.equalsIgnoreCase("BS")){
						//printTxVO.setReportID("template.balanceSheet");
						printVO=printReportService.printBalanceSheetReport(printTxVO);
					}else if (sourceType.equalsIgnoreCase("TB")){
						//printTxVO.setReportID("template.trialBalance");						
						printVO=printReportService.printTrialBalanceReport(printTxVO);
					   
					}else if (sourceType.equalsIgnoreCase("CF")){
						printVO=printReportService.printCashFlowReport(printTxVO);
					}
										
					
					printVO.setBeanList(null);				
					FileInputStream myStream = new FileInputStream(printVO.getReportURL());

					// Set the content type and attachment header.
					response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
					response.addHeader("x-filename",printVO.getReportID());
					response.setContentType("application/pdf");
					
					// Copy the stream to the response's output stream.
					IOUtils.copy(myStream, response.getOutputStream());				

					response.flushBuffer();
				
			} catch (IOException e) {
				logger.error("Exception downloadIncomeExpenseReport "+e);
				
			}
			logger.debug("Exit : public @ResponseBody PrintReportVO downloadIncomeExpenseReport(@RequestBody PrintReportVO printVO) ");						
			
	 
		}
		
		/*Sample body contents for PrintReportVO */
		/*{
			"printParam":{
			"orgID":"11",
			"fromDate":"2016-04-01",
			"toDate":"2016-06-30",
			"billingCycleID":"5"
			},
			"reportContentType":"pdf"
			
			}*/
		@RequestMapping(value="/societySummaryBillReport", method = RequestMethod.POST)
		public void downloadSocietySummaryBillReport(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
			TransactionVO printTxVO=new TransactionVO();
			APIResponseVO apiResponse=new APIResponseVO();
			HashMap<String, Object> param= printVO.getPrintParam();
			logger.debug("Entry : public @ResponseBody PrintReportVO downloadSocietySummaryBillReport(@RequestBody TransactionVO printVO) ");
			try {
				
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				param.put("orgID", decrptVO.getOrgID());
				
					printTxVO.setPrintParam(param);
				    
					printTxVO.setFromDate((String) param.get("fromDate"));
					printTxVO.setToDate((String) param.get("toDate"));
					printTxVO.setDueDate((String) param.get("dueDate"));
					
					printTxVO.setSourceType("BEAN");
					printTxVO.setReportContentType(printVO.getReportContentType());
														
					//printTxVO.setReportID("template.societyBills");						
					printVO=printReportService.printSocietySummaryBillReport(printTxVO);
					printVO.setBeanList(null);			
					
					FileInputStream myStream = new FileInputStream(printVO.getReportURL());

					// Set the content type and attachment header.
					response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
					response.addHeader("x-filename",printVO.getReportID());
					response.setContentType("application/pdf");
					
					// Copy the stream to the response's output stream.
					IOUtils.copy(myStream, response.getOutputStream());				

					response.flushBuffer();
				
			} catch (ClientAbortException e) {
				logger.info("Exception in downloadSocietySummaryBillReport: "+e);
			} catch (IOException e) {
				logger.error("Exception in downloadSocietySummaryBillReport: "+e);
			} catch (Exception e) {
				logger.error("Exception in downloadSocietySummaryBillReport: "+e);
			}
			logger.debug("Exit : public @ResponseBody PrintReportVO downloadSocietySummaryBillReport(@RequestBody TransactionVO printVO) ");						
			
	 
		}
		
		 /*Sample body contents for PrintReportVO */
		/*{
			"printParam":{
			"orgID":"11",
			"fromDate":"2016-04-01",
			"toDate":"2016-06-30",
			"billingCycleID":"5"
			},
			"reportContentType":"pdf"
			
			}*/
		@RequestMapping(value="/billReport", method = RequestMethod.POST)
		public void downloadAllMemberBillReport(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
			TransactionVO printTxVO=new TransactionVO();
			APIResponseVO apiResponse=new APIResponseVO();
			HashMap<String, Object> param= printVO.getPrintParam();
			logger.debug("Entry : public @ResponseBody PrintReportVO downloadAllMemberBillReport(@RequestBody TransactionVO printVO) ");
			try {
				
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				param.put("orgID", decrptVO.getOrgID());
				
					printTxVO.setPrintParam(param);
				    
					printTxVO.setFromDate((String) param.get("fromDate"));
					printTxVO.setToDate((String) param.get("toDate"));
					printTxVO.setDueDate((String) param.get("dueDate"));
					
					printTxVO.setSourceType("BEAN");
					printTxVO.setReportContentType(printVO.getReportContentType());
														
					//printTxVO.setReportID("template.societyBills");						
					printVO=printReportService.printAllMemberBillReport(printTxVO);
					printVO.setBeanList(null);			
					
					FileInputStream myStream = new FileInputStream(printVO.getReportURL());

					// Set the content type and attachment header.
					response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
					response.addHeader("x-filename",printVO.getReportID());
					response.setContentType("application/pdf");
					
					// Copy the stream to the response's output stream.
					IOUtils.copy(myStream, response.getOutputStream());				

					response.flushBuffer();
				
			} catch (ClientAbortException e) {
				logger.info("Exception in downloadAllMemberBillReport: "+e);
			} catch (IOException e) {
				logger.error("Exception in downloadAllMemberBillReport: "+e);
			} catch (Exception e) {
				logger.error("Exception in downloadAllMemberBillReport: "+e);
			}
			logger.debug("Exit : public @ResponseBody PrintReportVO downloadAllMemberBillReport(@RequestBody TransactionVO printVO) ");						
			
	 
		}
		
		 /*Sample body contents for PrintReportVO */
		/*{
			"printParam":{
			"orgID":"11",
			"fromDate":"2016-04-01",
			"toDate":"2016-06-30",
			"dueDate":"2016-06-30",
			"billingCycleID":"5",
			"aptID":"5"
			},
			"reportContentType":"pdf"
			
			}*/
		@RequestMapping(value="/bill", method = RequestMethod.POST)
		public void downloadBill(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
			TransactionVO printTxVO=new TransactionVO();
			APIResponseVO apiResponse=new APIResponseVO();
			HashMap<String, Object> param= printVO.getPrintParam();
			logger.debug("Entry : public @ResponseBody PrintReportVO downloadBill(@RequestBody TransactionVO printVO) ");
			try {
				
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				param.put("orgID", decrptVO.getOrgID());
				
					printTxVO.setPrintParam(param);
				    
					printTxVO.setFromDate((String) param.get("fromDate"));
					printTxVO.setToDate((String) param.get("toDate"));
					printTxVO.setDueDate((String) param.get("dueDate"));
					
					printTxVO.setSourceType("BEAN");
					printTxVO.setReportContentType(printVO.getReportContentType());														
					//printTxVO.setReportID("template.bill");				
					
					printVO=printReportService.printBill(printTxVO);
					printVO.setBeanList(null);			
					
					FileInputStream myStream = new FileInputStream(printVO.getReportURL());

					// Set the content type and attachment header.
					response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
					response.addHeader("x-filename",printVO.getReportID());
					response.setContentType("application/pdf");
					
					// Copy the stream to the response's output stream.
					IOUtils.copy(myStream, response.getOutputStream());				

					response.flushBuffer();
				
			} catch (IOException e) {
				logger.error("Exception in downloadBill: "+e);
				
			}
			logger.debug("Exit : public @ResponseBody PrintReportVO downloadBill(@RequestBody TransactionVO printVO) ");						
			
	 
		}
		
		 /*Sample body contents for PrintReportVO */
		/*{
			"printParam":{
			"orgID":"11",
			"fromDate":"2016-04-01",
			"toDate":"2016-06-30",
			"ledgerID":"5"
			},
			"reportContentType":"pdf"
			
			}*/
		@RequestMapping(value="/customerBill", method = RequestMethod.POST)
		public void downloadCustomerBill(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
			TransactionVO printTxVO=new TransactionVO();
			APIResponseVO apiResponse=new APIResponseVO();
			HashMap<String, Object> param= printVO.getPrintParam();
			logger.debug("Entry : public @ResponseBody PrintReportVO downloadCustomerBill(@RequestBody TransactionVO printVO) ");
			try {
				
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				param.put("orgID", decrptVO.getOrgID());
				
					printTxVO.setPrintParam(param);
				    
					printTxVO.setFromDate((String) param.get("fromDate"));
					printTxVO.setToDate((String) param.get("toDate"));
								
					printTxVO.setSourceType("BEAN");
					printTxVO.setReportContentType(printVO.getReportContentType());														
										
					printVO=printReportService.printBillCustomer(printTxVO);
					printVO.setBeanList(null);			
					
					FileInputStream myStream = new FileInputStream(printVO.getReportURL());

					// Set the content type and attachment header.
					response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
					response.addHeader("x-filename",printVO.getReportID());
					response.setContentType("application/pdf");
					
					// Copy the stream to the response's output stream.
					IOUtils.copy(myStream, response.getOutputStream());				

					response.flushBuffer();
				
			} catch (IOException e) {
				logger.error("Exception in downloadCustomerBill: "+e);
				
			}
			logger.debug("Exit : public @ResponseBody PrintReportVO downloadCustomerBill(@RequestBody TransactionVO printVO) ");						
			
	 
		}
		
		/*Sample body contents for PrintReportVO */
		/*{
			"printParam":{
		"orgID":"11",
		"fromDate":"2016-04-01",
		"toDate":"2016-06-30",
	
			},
			"reportContentType":"pdf",
			
			
			}*/

		@RequestMapping(value="/allCustomerBill", method = RequestMethod.POST)
		public void downloadAllCustomerBills(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
			TransactionVO printTxForm=new TransactionVO();     
			APIResponseVO apiResponse=new APIResponseVO();
			HashMap<String, Object> param= printVO.getPrintParam();
			logger.debug("Entry : public @ResponseBody downloadAllCustomerBills(@RequestBody PrintReportVO printVO) ")	;
			try {
				       	  
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				param.put("orgID", decrptVO.getOrgID());
				
				printTxForm.setPrintParam(param);
				printTxForm.setFromDate((String) param.get("fromDate"));
				printTxForm.setToDate((String) param.get("toDate"));
				printTxForm.setReportContentType(printVO.getReportContentType());
				
				printTxForm.setSourceType("BEAN");
				
					printVO=printReportService.printAllCustomerBillReport(printTxForm);
                   printVO.setBeanList(null);			
					
               	FileInputStream myStream = new FileInputStream(printVO.getReportURL());

					// Set the content type and attachment header.
					response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
					response.addHeader("x-filename",printVO.getReportID());
					response.setContentType("application/pdf");
					
					// Copy the stream to the response's output stream.
					IOUtils.copy(myStream, response.getOutputStream());				

					response.flushBuffer();		
							
				
			} catch (IOException e) {
				logger.error("Exception in downloadAllCustomerBills: "+e);
				
			}
			logger.debug("Exit : public @ResponseBody PrintReportVO downloadCustomerBills(@RequestBody PrintReportVO printVO) ");						
			
	 
		}
		
		
		/*Sample body contents for PrintReportVO */
		/*{
			"printParam":{
			"orgID":"11",
			invoiceID:"4",			
			},
			"reportContentType":"pdf"
			
			}*/
		@RequestMapping(value="/customerInvoice", method = RequestMethod.POST)
		public void downloadCustomerInvoice(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
			TransactionVO printTxVO=new TransactionVO();
			APIResponseVO apiResponse=new APIResponseVO();
			HashMap<String, Object> param= printVO.getPrintParam();
			logger.debug("Entry : public @ResponseBody PrintReportVO downloadCustomerInvoice(@RequestBody TransactionVO printVO) ");
			try {
				
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				param.put("orgID", decrptVO.getOrgID());
				
					printTxVO.setPrintParam(param);
				    
					printTxVO.setSourceType("BEAN");
					printTxVO.setReportContentType(printVO.getReportContentType());														
								
					
					printVO=printReportService.printCustomerInvoice(printTxVO);
					printVO.setBeanList(null);			
					
					FileInputStream myStream = new FileInputStream(printVO.getReportURL());

					// Set the content type and attachment header.
					response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
					response.addHeader("x-filename",printVO.getReportID());
					response.setContentType("application/pdf");
					
					// Copy the stream to the response's output stream.
					IOUtils.copy(myStream, response.getOutputStream());				

					response.flushBuffer();
				
			} catch (IOException e) {
				logger.error("Exception in downloadCustomerInvoice: "+e);
				
			}
			logger.debug("Exit : public @ResponseBody PrintReportVO downloadCustomerInvoice(@RequestBody TransactionVO printVO) ");						
			
	 
		}
		
		/*Sample body contents for PrintReportVO */
		/*{
			"printParam":{
			"orgID":"11"
			"lastDate":"2016-05-31"
			},
			"reportContentType":"pdf"
			
			}*/
	 
		@RequestMapping(value="/ledgerBalance", method = RequestMethod.POST)
		public void downloadLedgerBalance(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken, @RequestBody PrintReportVO printVO) {
			RptTransactionVO printTxVO=new RptTransactionVO();
			APIResponseVO apiResponse=new APIResponseVO();
			HashMap<String, Object> param= printVO.getPrintParam();
			logger.debug("Entry : public @ResponseBody PrintReportVO downloadLedgerBalance(@PathVariable String societyID, @PathVariable String exportType)  ");
			try {
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				param.put("orgID", decrptVO.getOrgID());			  	
				    printTxVO.setPrintParam(param);
		            printTxVO.setDisplayDate((String) param.get("lastDate"));
				    printTxVO.setBuildingId("0");
				    				   
					printTxVO.setReportContentType(printVO.getReportContentType());
					//printTxVO.setReportID("template.ledgerDueReport");					
					printTxVO.setSourceType("BEAN");
					printVO=printReportService.printLedgerDueReport(printTxVO);
				    printVO.setBeanList(null);
				    
				    FileInputStream myStream = new FileInputStream(printVO.getReportURL());

					// Set the content type and attachment header.
					response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
					response.addHeader("x-filename",printVO.getReportID());
					response.setContentType("application/pdf");
					
					// Copy the stream to the response's output stream.
					IOUtils.copy(myStream, response.getOutputStream());				

					response.flushBuffer();
				
			} catch (Exception e) {
				logger.error("Exception in downloadLedgerBalance: "+e);
			}
			logger.debug("Exit : public @ResponseBody PrintReportVO downloadLedgerBalance(@PathVariable String societyID, @PathVariable String exportType)  ");						
			
	 
		}
		

		 /*Sample body contents for PrintReportVO */
			/*{
				"printParam":{
				"orgID":"11",
				"fromDate":"2016-04-01",
				"toDate":"2016-06-30"
				"type":"IE" //Type of report: IE: Income Expense report, BS:Balance Sheet, TB: trial balance 				 
				},
				"reportContentType":"pdf"
				
				}*/
			@RequestMapping(value="/receiptAndVoucher", method = RequestMethod.POST)
			public void downloadReceiptAndVoucher(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO printTxVO) {
			//	TransactionVO printTxVO=new TransactionVO();
				PrintReportVO printVO=new PrintReportVO();
				APIResponseVO apiResponse=new APIResponseVO();
				HashMap<String, Object> param= printTxVO.getPrintParam();
				logger.debug("Entry : public @ResponseBody PrintReportVO downloadIncomeExpenseReport(@RequestBody PrintReportVO printVO) ");
				try {
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					param.put("orgID", decrptVO.getOrgID());
						printTxVO.setPrintParam(param);		
						
						String printType=(String) param.get("printType");
						printTxVO.setSourceType("BEAN");
						printTxVO.setReportContentType(printTxVO.getReportContentType());
						if(printType.equalsIgnoreCase("VCH")){
							//printTxVO.setReportID("template.voucher");														
							printVO=printReportService.printVoucherAndPaymentReceipt(printTxVO);					    
						}else if (printType.equalsIgnoreCase("RCPT")){
							//printTxVO.setReportID("template.voucher");
							printVO=printReportService.printVoucherAndPaymentReceipt(printTxVO);
						}else if (printType.equalsIgnoreCase("MRCPT")){
							//printTxVO.setReportID("template.receipt");						
							printVO=printReportService.printMemberReceipt(printTxVO);
						   
						}else if (printType.equalsIgnoreCase("CHQ")){

						printVO=printReportService.printPaymentCheque(printTxVO);
 
						}else if (printType.equalsIgnoreCase("VCHRPT")){
	                          //Voucher report
							printVO=printReportService.printVouchuerReport(printTxVO);

							}
											
						
						printVO.setBeanList(null);				
						FileInputStream myStream = new FileInputStream(printVO.getReportURL());

						// Set the content type and attachment header.
						response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
						response.addHeader("x-filename",printVO.getReportID());
						response.setContentType("application/pdf");
						
						// Copy the stream to the response's output stream.
						IOUtils.copy(myStream, response.getOutputStream());				

						response.flushBuffer();
					
				} catch (IOException e) {
					logger.error("Exception downloadReceiptAndVoucher "+e);
					
				}
				logger.debug("Exit : public @ResponseBody PrintReportVO downloadReceiptAndVoucher(@RequestBody PrintReportVO printVO) ");						
				
		 
			}
			
			
			/*Sample body contents for PrintReportVO */
			/*{
				"printParam":{
				"orgID":"11",
				"memberType":"P", //P:primary, A:Associate, ALL: All member
				"missingType":"0" //0: mobile missing, 1: email missing, 2: all members list
				"type":"MOBILE" // report name: MOBILE: Mobile Missing, EMAIL: missing,
				},
				"reportContentType":"pdf"
				
				}*/
			@RequestMapping(value="/memberContactInfo", method = RequestMethod.POST)
			public void downloadMemberContactDetails(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO printTxVO) {
			//	TransactionVO printTxVO=new TransactionVO();
				PrintReportVO printVO=new PrintReportVO();
				APIResponseVO apiResponse=new APIResponseVO();
				HashMap<String, Object> param= printTxVO.getPrintParam();
				logger.debug("Entry : public @ResponseBody PrintReportVO downloadMemberContactDetails(@RequestBody PrintReportVO printVO) ");
				try {
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					param.put("orgID", decrptVO.getOrgID());
						printTxVO.setPrintParam(param);		
						
						String missingType=(String) param.get("missingType");
						printTxVO.setSourceType("BEAN");
						printTxVO.setReportContentType(printTxVO.getReportContentType());
						if(missingType.equalsIgnoreCase("0")){
							//printTxVO.setReportID("template.missingInfo");	
							printVO=printReportService.printMissingContactReport(printTxVO);					    
						}else if (missingType.equalsIgnoreCase("1")){
							//printTxVO.setReportID("template.missingInfo");
							printVO=printReportService.printMissingContactReport(printTxVO);
						}else if (missingType.equalsIgnoreCase("2")){
							//printTxVO.setReportID("template.memberInfoList");						
							printVO=printReportService.printSocietyMemberContactReport(printTxVO);
						   
						}
											
						
						printVO.setBeanList(null);				
						FileInputStream myStream = new FileInputStream(printVO.getReportURL());

						// Set the content type and attachment header.
						response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
						response.addHeader("x-filename",printVO.getReportID());
						response.setContentType("application/pdf");
						
						// Copy the stream to the response's output stream.
						IOUtils.copy(myStream, response.getOutputStream());				

						response.flushBuffer();
					
				} catch (IOException e) {
					logger.error("Exception downloadMemberContactDetails "+e);
					
				}
				logger.debug("Exit : public @ResponseBody PrintReportVO downloadMemberContactDetails(@RequestBody PrintReportVO printVO) ");						
				
		 
			}
			
			/*Sample body contents for PrintReportVO */
			/*{
				"printParam":{
				"orgID":"11",
				"registerType":"TR", //TR:Tenant Register, VR:Vehicle, SR: Share, NR:Nomination, MR:Mortgage
				                     //DR:Document register, IR: I register , JR: J register
				},
				"reportContentType":"pdf"
				
				}*/
			@RequestMapping(value="/registers", method = RequestMethod.POST)
			public void downloadRegisters(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO printTxVO) {
			
				PrintReportVO printVO=new PrintReportVO();
				APIResponseVO apiResponse=new APIResponseVO();
				SocietyVO societyVO = new SocietyVO();
				HashMap<String, Object> param= printTxVO.getPrintParam();
				logger.debug("Entry : public void downloadRegisters(@RequestBody PrintReportVO printVO) ");
				try {
					
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					param.put("orgID", decrptVO.getOrgID());
						printTxVO.setPrintParam(param);		
						
						String registerType=(String) param.get("registerType");
						printTxVO.setSourceType("BEAN");
						printTxVO.setReportContentType(printTxVO.getReportContentType());
						if(registerType.equalsIgnoreCase("VR")){
							//printTxVO.setReportID("template.vehicleRegister");	
							printVO=printReportService.printVehicleReport(printTxVO);					    
						}else if (registerType.equalsIgnoreCase("TR")){
							//printTxVO.setReportID("template.tenantRegister");
							printVO=printReportService.printTenantRegister(printTxVO);
						}else if (registerType.equalsIgnoreCase("SR")){
							//printTxVO.setReportID("template.shareRegister");						
							printVO=printReportService.printShareRegister(printTxVO);						   
						}else if (registerType.equalsIgnoreCase("NR")){
							//printTxVO.setReportID("template.nomineeRegister");						
							printVO=printReportService.printNominationRegister(printTxVO);						   
						}else if (registerType.equalsIgnoreCase("MR")){
							//printTxVO.setReportID("template.mortgageRegister");						
							printVO=printReportService.printMortgageRegister(printTxVO);						   
						}else if (registerType.equalsIgnoreCase("DR")){
							//printTxVO.setReportID("template.documentRegister");						
							printVO=printReportService.printDocumentReport(printTxVO);						   
						}else if (registerType.equalsIgnoreCase("JR")){
							//printTxVO.setReportID("template.jRegister");						
							printVO=printReportService.printJRegister(printTxVO);						   
						}else if (registerType.equalsIgnoreCase("IR")){
						   societyVO=societyService.getSocietyDetails("0", Integer.parseInt(decrptVO.getOrgID()));   	               
				          HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
				      
							printVO.setReportID(temmplateHashmap.get("iRegister").trim());	
							printVO.setPrintParam(param);		
							printVO.setSourceType("DATABASE");
							printVO.setReportContentType("pdf");
							printVO=printReportService.printReport(printVO);					   
						}
						
											
						
						printVO.setBeanList(null);				
						FileInputStream myStream = new FileInputStream(printVO.getReportURL());

						// Set the content type and attachment header.
						response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
						response.addHeader("x-filename",printVO.getReportID());
						response.setContentType("application/pdf");
						
						// Copy the stream to the response's output stream.
						IOUtils.copy(myStream, response.getOutputStream());				

						response.flushBuffer();
					
				} catch (IOException e) {
					logger.error("Exception downloadRegisters "+e);
					
				}
				logger.debug("Exit : public void downloadRegisters(@RequestBody PrintReportVO printVO) ");						
				
		 
			}
			
			
			 /*Sample body contents for PrintReportVO */
			/*{
				"printParam":{
				"orgID":"11",
				"fromDate":"2016-04-01",
				"toDate":"2016-06-30",				
				},
				"reportContentType":"pdf"
				
				}*/
			@RequestMapping(value="/allMemberIndividualReport", method = RequestMethod.POST)
			public void downloadAllMemberLedgerReport(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
				TransactionVO printTxVO=new TransactionVO();
				APIResponseVO apiResponse=new APIResponseVO();
				HashMap<String, Object> param= printVO.getPrintParam();
				logger.debug("Entry : public @ResponseBody PrintReportVO downloadAllMemberBillReport(@RequestBody TransactionVO printVO) ");
				try {
					
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					param.put("orgID", decrptVO.getOrgID());
					
						printTxVO.setPrintParam(param);
					    
						printTxVO.setFromDate((String) param.get("fromDate"));
						printTxVO.setToDate((String) param.get("toDate"));
												
						printTxVO.setSourceType("BEAN");
						printTxVO.setReportContentType(printVO.getReportContentType());
															
						//printTxVO.setReportID("template.societyBills");						
						printVO=printReportService.printAllMemberLedgerReport(printTxVO);
						printVO.setBeanList(null);			
						
						FileInputStream myStream = new FileInputStream(printVO.getReportURL());

						// Set the content type and attachment header.
						response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
						response.addHeader("x-filename",printVO.getReportID());
						response.setContentType("application/pdf");
						
						// Copy the stream to the response's output stream.
						IOUtils.copy(myStream, response.getOutputStream());				

						response.flushBuffer();
					
				} catch (IOException e) {
					logger.error("Exception in downloadAllMemberLedgerReport: "+e);
					
				}
				logger.debug("Exit : public @ResponseBody PrintReportVO downloadAllMemberLedgerReport(@RequestBody TransactionVO printVO) ");						
				
		 
			}
			
			 /*Sample body contents for PrintReportVO */
			/*{
				"printParam":{
				"orgID":"11",
				"fromDate":"2016-04-01",
				"toDate":"2016-06-30",				
				},
				"reportContentType":"pdf"
				
				}*/
			@RequestMapping(value="/memberReceiptList", method = RequestMethod.POST)
			public void downloadMemberRecipetList(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
				TransactionVO printTxVO=new TransactionVO();
				APIResponseVO apiResponse=new APIResponseVO();
				HashMap<String, Object> param= printVO.getPrintParam();
				logger.debug("Entry : public @ResponseBody PrintReportVO downloadMemberRecipetList(@RequestBody TransactionVO printVO) ");
				try {
					
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					param.put("orgID", decrptVO.getOrgID());
					
						printTxVO.setPrintParam(param);
					    
						printTxVO.setFromDate((String) param.get("fromDate"));
						printTxVO.setToDate((String) param.get("toDate"));
												
						printTxVO.setSourceType("BEAN");
						printTxVO.setReportContentType(printVO.getReportContentType());
															
						//printTxVO.setReportID("template.societyBills");						
						printVO=printReportService.printMemberReceiptList(printTxVO);
						printVO.setBeanList(null);			
						
						FileInputStream myStream = new FileInputStream(printVO.getReportURL());

						// Set the content type and attachment header.
						response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
						response.addHeader("x-filename",printVO.getReportID());
						response.setContentType("application/pdf");
						
						// Copy the stream to the response's output stream.
						IOUtils.copy(myStream, response.getOutputStream());				

						response.flushBuffer();
					
				} catch (IOException e) {
					logger.error("Exception in downloadMemberRecipetList: "+e);
					
				}
				logger.debug("Exit : public @ResponseBody PrintReportVO downloadMemberRecipetList(@RequestBody TransactionVO printVO) ");						
				
		 
			}
			
			
			

			
			 /*Sample body contents for PrintReportVO */
			/*{
			"printParam":{
			"orgID":"11",
			invoiceID:"4",	
			emailTo:"a@e.com",	
			"subject":"Bill number",
			"message":"message",,	
			},
			"reportContentType":"pdf"

			}*/
			@RequestMapping(value="/sendEmailCustomerInvoice", method = RequestMethod.POST)
			public ResponseEntity sendEmailCustomerInvoice(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
			TransactionVO printTxVO=new TransactionVO();
			APIResponseVO apiResponse=new APIResponseVO();
			HashMap<String, Object> param= printVO.getPrintParam();
			NotificationVO notificationVO=new NotificationVO();
			EmailMessage emailVO=new EmailMessage();
			List emailList=new ArrayList<EmailMessage>();
			ConfigManager confMgnr=new ConfigManager();
			logger.debug("Entry : public @ResponseBody PrintReportVO sendEmailCustomerInvoice(@RequestBody TransactionVO printVO) ");
			try {

				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				param.put("orgID", decrptVO.getOrgID());
			
				printTxVO.setPrintParam(param);
				   
				printTxVO.setSourceType("BEAN");
				printTxVO.setReportContentType(printVO.getReportContentType());	
				
				printVO=printReportService.printCustomerInvoice(printTxVO);
				printVO.setBeanList(null);
				//Write file to other server
			    Channel channel;
					try {
						  String user = confMgnr.getPropertiesValue("pushServer.user");
						  String pass = confMgnr.getPropertiesValue("pushServer.password");
						  String hostConn = confMgnr.getPropertiesValue("pushServer.IP");
						  int portNo = Integer.valueOf(confMgnr.getPropertiesValue("pushServer.port"));
							   JSch jsch = new JSch();
							   com.jcraft.jsch.Session sessions = null;
							try {
								sessions = jsch.getSession(user, hostConn, portNo);
							
							   sessions.setPassword(pass);
							   sessions.setConfig("StrictHostKeyChecking", "no");
							   System.out.println("Establishing Connection...");
							   sessions.connect();
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								logger.error("Unable to establish connection to server");
							}
						  
						channel = sessions.openChannel("sftp");
				
			           channel.connect();
			           ChannelSftp sftspChannel = (ChannelSftp) channel;
			          
			           sftspChannel.cd(confMgnr.getPropertiesValue("pushServer.directory"));
			           File localFile = new File( printVO.getReportURL());
			           sftspChannel.put(localFile.getAbsolutePath(),localFile.getName());
					}catch(Exception e){
						logger.error("Exception e "+e);
					}
					
			   //Fill email object				
				String invoiceID =(String) param.get("invoiceID");
				String emailId =(String) param.get("emailTo");
				String subject =(String) param.get("subject");
				String message =(String) param.get("message");
				
				logger.info("Email  "+emailId+" Subject: "+subject+" message "+message );		        
				emailVO.setDefaultSender(printVO.getSocietyVO().getSocietyName());
				emailVO.setEmailTO(emailId);
				emailVO.setSubject(subject);
				
				File file= new File(printVO.getReportURL());	
				String attachemnetURL=confMgnr.getPropertiesValue("pushServer.URL")+file.getName();
				message=message+" <a href="+attachemnetURL+"><img src='https://www.esanchalak.com/member/img/attachement.png' style='width:20px;height:20px;' /> Invoice.pdf </a>";
				logger.info("Invoice pdf url "+attachemnetURL);
				emailVO.setMessage(message);
				
				emailList.add(emailVO);
				    
				notificationVO.setEmailList(emailList);
				notificationVO.setSendEmail(true);
				notificationVO.setSendSMS(false);				
				notificationVO.setSendPushNotifications(false);
				
				notificationVO=notificationService.SendNotifications(notificationVO);
	
				if(notificationVO.getSentEmailsCount()==0){
					logger.info("Unable to send email Invoice no: "+invoiceID);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to send email invoice.");
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				}else{
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Email has been sent successfully.");
					logger.info("Attachemnt Email has been sent successfully Invoice no: "+invoiceID);
				}

			   } catch (Exception e) {
				   logger.error("Exception in sendEmailCustomerInvoice: "+e);
			   }
		    logger.debug("Exit : public @ResponseBody PrintReportVO sendEmailCustomerInvoice(@RequestBody TransactionVO printVO) ");	
			return new ResponseEntity(apiResponse, HttpStatus.OK); 

		 }
			
			
		 /*Sample body contents for PrintReportVO */
			/*{
			"printParam":{
			"orgID":"11",
			"billNo":"4",
			"ledgerID":"4",
			"fromDate":"2018-04-01",
			"toDate":"2018-04-01",
			emailTo:"a@e.com",	
			"subject":"Bill number",
			"message":"message",
			},
			"reportContentType":"pdf"

			}*/
			@RequestMapping(value="/sendEmailCustomerBill", method = RequestMethod.POST)
			public ResponseEntity sendEmailCustomerBill(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
			TransactionVO printTxVO=new TransactionVO();
			APIResponseVO apiResponse=new APIResponseVO();
			HashMap<String, Object> param= printVO.getPrintParam();
			NotificationVO notificationVO=new NotificationVO();
			EmailMessage emailVO=new EmailMessage();
			List emailList=new ArrayList<EmailMessage>();
			ConfigManager confMgnr=new ConfigManager();
			logger.debug("Entry : public @ResponseBody PrintReportVO sendEmailCustomerBill(@RequestBody PrintReportVO printVO) ");
			try {

				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				param.put("orgID", decrptVO.getOrgID());
			
				printTxVO.setPrintParam(param);
				
				printTxVO.setFromDate((String) param.get("fromDate"));
				printTxVO.setToDate((String) param.get("toDate"));
				   
				printTxVO.setSourceType("BEAN");
				printTxVO.setReportContentType(printVO.getReportContentType());	
				
				printVO=printReportService.printBillCustomer(printTxVO);
				printVO.setBeanList(null);
				logger.info(" Url: "+confMgnr.getPropertiesValue("pushServer.URL"));
				//Write file to other server
				   Channel channel;
					try {
						  String user = confMgnr.getPropertiesValue("pushServer.user");
						  String pass = confMgnr.getPropertiesValue("pushServer.password");
						  String hostConn = confMgnr.getPropertiesValue("pushServer.IP");
						  int portNo = Integer.valueOf(confMgnr.getPropertiesValue("pushServer.port"));
						  logger.info(user+" pass: "+pass+" "+hostConn+"  "+portNo);
						  JSch jsch = new JSch();
							   com.jcraft.jsch.Session sessions = null;
							try {
								sessions = jsch.getSession(user, hostConn, portNo);
							
							   sessions.setPassword(pass);
							   sessions.setConfig("StrictHostKeyChecking", "no");
							   System.out.println("Establishing Connection...");
							   sessions.connect();
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								logger.error("Unable to establish connection to server");
							}
						  
						channel = sessions.openChannel("sftp");
				
			           channel.connect();
			           ChannelSftp sftspChannel = (ChannelSftp) channel;
			          
			           sftspChannel.cd(confMgnr.getPropertiesValue("pushServer.directory"));
			           File localFile = new File( printVO.getReportURL());
			           sftspChannel.put(localFile.getAbsolutePath(),localFile.getName());
					}catch(Exception e){
						logger.error("Exception e "+e);
					}
					
			   //Fill email object				
				String billNo =(String) param.get("billNo");
				String emailId =(String) param.get("emailTo");
				String subject =(String) param.get("subject");
				String message =(String) param.get("message");
				
				logger.info("Email  "+emailId+" Subject: "+subject+" message "+message );		        
				emailVO.setDefaultSender(printVO.getSocietyVO().getSocietyName());
				emailVO.setEmailTO(emailId);
				emailVO.setSubject(subject);
				
				File file= new File(printVO.getReportURL());				
				String attachemnetURL=confMgnr.getPropertiesValue("pushServer.URL")+file.getName();
				message=message+" <a href="+attachemnetURL+"><img src='https://www.esanchalak.com/member/img/attachement.png' style='width:20px;height:20px;' /> Bill.pdf </a>";
				logger.info("Bill pdf url "+attachemnetURL);
				emailVO.setMessage(message);
				
				emailList.add(emailVO);
				    
				notificationVO.setEmailList(emailList);
				notificationVO.setSendEmail(true);
				notificationVO.setSendSMS(false);				
				notificationVO.setSendPushNotifications(false);
				
				notificationVO=notificationService.SendNotifications(notificationVO);
	
				if(notificationVO.getSentEmailsCount()==0){
					logger.info("Unable to send email Bill no: "+billNo);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to send email bill.");
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				}else{
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Email has been sent successfully.");
					logger.info("Attachemnt Email has been sent successfully Bill no: "+billNo);
				}

			   } catch (Exception e) {
				   logger.error("Exception in sendEmailCustomerBill: "+e);
			   }
		    logger.debug("Exit : public @ResponseBody PrintReportVO sendEmailCustomerBill(@RequestBody PrintReportVO printVO) ");	
			return new ResponseEntity(apiResponse, HttpStatus.OK); 

		 }
			
			
			 /*Sample body contents for PrintReportVO */
			/*{
			"printParam":{
			"orgID":"11",
			"ledgerID":"4",
			"fromDate":"2018-04-01",
			"toDate":"2018-04-01",
			emailTo:"a@e.com",	
			"subject":"Bill number",
			"message":"message",	
			},
			"reportContentType":"pdf"

			}*/
			@RequestMapping(value="/sendEmailCustomerLedger", method = RequestMethod.POST)
			public ResponseEntity sendEmailCustomerLedger(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
			TransactionVO printTxVO=new TransactionVO();
			APIResponseVO apiResponse=new APIResponseVO();
			HashMap<String, Object> param= printVO.getPrintParam();
			NotificationVO notificationVO=new NotificationVO();
			EmailMessage emailVO=new EmailMessage();
			List emailList=new ArrayList<EmailMessage>();
			ConfigManager confMgnr=new ConfigManager();
			logger.debug("Entry : public @ResponseBody PrintReportVO sendEmailCustomerLedger(@RequestBody PrintReportVO printVO) ");
			try {

				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				param.put("orgID", decrptVO.getOrgID());
			
				printTxVO.setPrintParam(param);
				
				printTxVO.setFromDate((String) param.get("fromDate"));
				printTxVO.setToDate((String) param.get("toDate"));
				printTxVO.setLedgerID(Integer.parseInt((String) param.get("ledgerID")));
                printTxVO.setType("transaction");
                printTxVO.setReportID("accountLedger");
					   
				printTxVO.setSourceType("BEAN");
				printTxVO.setReportContentType(printVO.getReportContentType());	
				
				printVO=printReportService.printTransactionReport(printTxVO);
				printVO.setBeanList(null);
				//Write file to other server
				   Channel channel;
					try {
						  String user = confMgnr.getPropertiesValue("pushServer.user");
						  String pass = confMgnr.getPropertiesValue("pushServer.password");
						  String hostConn = confMgnr.getPropertiesValue("pushServer.IP");
						  int portNo = Integer.valueOf(confMgnr.getPropertiesValue("pushServer.port"));
						  
						  JSch jsch = new JSch();
							   com.jcraft.jsch.Session sessions = null;
							try {
								sessions = jsch.getSession(user, hostConn, portNo);
							
							   sessions.setPassword(pass);
							   sessions.setConfig("StrictHostKeyChecking", "no");
							   System.out.println("Establishing Connection...");
							   sessions.connect();
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								logger.error("Unable to establish connection to server");
							}
						  
						channel = sessions.openChannel("sftp");
				
			           channel.connect();
			           ChannelSftp sftspChannel = (ChannelSftp) channel;
			          
			           sftspChannel.cd(confMgnr.getPropertiesValue("pushServer.directory"));
			           File localFile = new File(printVO.getReportURL());
			           
			           sftspChannel.put(localFile.getAbsolutePath(),localFile.getName());
					}catch(Exception e){
						logger.error("Exception e "+e);
					}
					
			   //Fill email object				
				String ledgerID =(String) param.get("ledgerID");
				String emailId =(String) param.get("emailTo");
				String subject =(String) param.get("subject");
				String message =(String) param.get("message");
				
				logger.info("Email  "+emailId+" Subject: "+subject+" message "+message );		        
				emailVO.setDefaultSender(printVO.getSocietyVO().getSocietyName());
				emailVO.setEmailTO(emailId);
				emailVO.setSubject(subject);
				
				File file= new File(printVO.getReportURL());	
				String attachemnetURL=confMgnr.getPropertiesValue("pushServer.URL")+file.getName();

				message=message+" <a href="+attachemnetURL+"><img src='https://www.esanchalak.com/member/img/attachement.png' style='width:20px;height:20px;' /> Account_Ledger.pdf </a>";
				logger.info("Ledger pdf URL: "+attachemnetURL);
				emailVO.setMessage(message);
				
				emailList.add(emailVO);
				    
				notificationVO.setEmailList(emailList);
				notificationVO.setSendEmail(true);
				notificationVO.setSendSMS(false);				
				notificationVO.setSendPushNotifications(false);
				
				notificationVO=notificationService.SendNotifications(notificationVO);
	
				if(notificationVO.getSentEmailsCount()==0){
					logger.info("Unable to send email ledgerID: "+ledgerID);
					apiResponse.setStatusCode(400);
					apiResponse.setMessage("Unable to send email ledger.");
					return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);				
				}else{
					apiResponse.setStatusCode(200);
					apiResponse.setMessage("Email has been sent successfully.");
					logger.info("Attachemnt Email has been sent successfully ledgerID : "+ledgerID);
				}

			   } catch (Exception e) {
				   logger.error("Exception in sendEmailCustomerLedger: "+e);
			   }
		    logger.debug("Exit : public @ResponseBody PrintReportVO sendEmailCustomerLedger(@RequestBody PrintReportVO printVO) ");	
			return new ResponseEntity(apiResponse, HttpStatus.OK); 

		 }	
			
			
			 /*Sample body contents for PrintReportVO */
			/*{
			"printParam":{
			"orgID":"11",
			"ledgerID":"4",
			"transactionID":"14",
			emailTo:"a@e.com",
			"subject":"Receipt number",
			"message":"message",
			},
			"reportContentType":"pdf"

			}*/
			@RequestMapping(value="/sendEmailMemberReceipt", method = RequestMethod.POST)
			public ResponseEntity sendEmailMemberReceipt(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
			TransactionVO printTxVO=new TransactionVO();
			APIResponseVO apiResponse=new APIResponseVO();
			HashMap<String, Object> param= printVO.getPrintParam();
			NotificationVO notificationVO=new NotificationVO();
			EmailMessage emailVO=new EmailMessage();
			List emailList=new ArrayList<EmailMessage>();
			ConfigManager confMgnr=new ConfigManager();
			logger.debug("Entry : public @ResponseBody PrintReportVO sendEmailMemberReceipt(@RequestBody PrintReportVO printVO) ");
			try {

			UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
			param.put("orgID", decrptVO.getOrgID());
			printTxVO.setPrintParam(param);
			printTxVO.setSourceType("BEAN");
			printTxVO.setReportContentType(printVO.getReportContentType());

			printVO=printReportService.printMemberReceipt(printTxVO);
			printVO.setBeanList(null);
			//Write file to other server
			  Channel channel;
			try {
			 String user = confMgnr.getPropertiesValue("pushServer.user");
			 String pass = confMgnr.getPropertiesValue("pushServer.password");
			 String hostConn = confMgnr.getPropertiesValue("pushServer.IP");
			 int portNo = Integer.valueOf(confMgnr.getPropertiesValue("pushServer.port"));
			 
			 JSch jsch = new JSch();
			  com.jcraft.jsch.Session sessions = null;
			try {
			sessions = jsch.getSession(user, hostConn, portNo);

			  sessions.setPassword(pass);
			  sessions.setConfig("StrictHostKeyChecking", "no");
			  System.out.println("Establishing Connection...");
			  sessions.connect();
			} catch (Exception e1) {
			// TODO Auto-generated catch block
			logger.error("Unable to establish connection to server");
			}
			 
			channel = sessions.openChannel("sftp");

			          channel.connect();
			          ChannelSftp sftspChannel = (ChannelSftp) channel;
			         
			          sftspChannel.cd(confMgnr.getPropertiesValue("pushServer.directory"));
			          File localFile = new File(printVO.getReportURL());
			         
			          sftspChannel.put(localFile.getAbsolutePath(),localFile.getName());
			}catch(Exception e){
			logger.error("Exception e "+e);
			}
			
			 //Fill email object
			String ledgerID =(String) param.get("ledgerID");
			String emailId =(String) param.get("emailTo");
			String subject =(String) param.get("subject");
			String message =(String) param.get("message");

			logger.info("Email  "+emailId+" Subject: "+subject+" message "+message );        
			emailVO.setDefaultSender(printVO.getSocietyVO().getSocietyName());
			emailVO.setEmailTO(emailId);
			emailVO.setSubject(subject);

			File file= new File(printVO.getReportURL());
			String attachemnetURL=confMgnr.getPropertiesValue("pushServer.URL")+file.getName();

			message=message+" <a href="+attachemnetURL+"><img src='https://www.esanchalak.com/member/img/attachement.png' style='width:20px;height:20px;' /> Receipt.pdf </a>";
			logger.info("Receipt pdf URL: "+attachemnetURL);
			emailVO.setMessage(message);

			emailList.add(emailVO);
			   
			notificationVO.setEmailList(emailList);
			notificationVO.setSendEmail(true);
			notificationVO.setSendSMS(false);
			notificationVO.setSendPushNotifications(false);

			notificationVO=notificationService.SendNotifications(notificationVO);

			if(notificationVO.getSentEmailsCount()==0){
			logger.info("Unable to send email ledgerID: "+ledgerID);
			apiResponse.setStatusCode(400);
			apiResponse.setMessage("Unable to send email receipt.");
			return new ResponseEntity(apiResponse, HttpStatus.NOT_ACCEPTABLE);
			}else{
			apiResponse.setStatusCode(200);
			apiResponse.setMessage("Email has been sent successfully.");
			logger.info("Attachemnt Email has been sent successfully ledgerID : "+ledgerID);
			}

			  } catch (Exception e) {
			  logger.error("Exception in sendEmailMemberReceipt: "+e);
			  }
			   logger.debug("Exit : public @ResponseBody PrintReportVO sendEmailMemberReceipt(@RequestBody PrintReportVO printVO) ");
			return new ResponseEntity(apiResponse, HttpStatus.OK);

			} 
			
			
			 /*Sample body contents for PrintReportVO */
			/*{
				"printParam":{
				"orgID":"11",
				"dueDate":"2016-04-01",
				"aptID":"5"
				},
				"reportContentType":"pdf"
				
				}*/
			@RequestMapping(value="/memberDemandNotice", method = RequestMethod.POST)
			public void downloadMemberDemandNotice(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody PrintReportVO printVO) {
				TransactionVO printTxVO=new TransactionVO();
				APIResponseVO apiResponse=new APIResponseVO();
				HashMap<String, Object> param= printVO.getPrintParam();
				logger.debug("Entry : public @ResponseBody PrintReportVO downloadMemberDemandNotice(@RequestBody TransactionVO printVO) ");
				try {
					
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					param.put("orgID", decrptVO.getOrgID());
					
						printTxVO.setPrintParam(param);
					    
						printTxVO.setSourceType("BEAN");
						printTxVO.setReportContentType(printVO.getReportContentType());														
											
						printVO=printReportService.printMemberDemandNotice(printTxVO);
						printVO.setBeanList(null);			
						
						FileInputStream myStream = new FileInputStream(printVO.getReportURL());

						// Set the content type and attachment header.
						response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
						response.addHeader("x-filename",printVO.getReportID());
						response.setContentType("application/pdf");
						
						// Copy the stream to the response's output stream.
						IOUtils.copy(myStream, response.getOutputStream());				

						response.flushBuffer();
					
				} catch (IOException e) {
					logger.error("Exception in downloadMemberDemandNotice: "+e);
					
				}
				logger.debug("Exit : public @ResponseBody PrintReportVO downloadMemberDemandNotice(@RequestBody TransactionVO printVO) ");						
				
		 
			}	
			
			
			/*Sample body contents for PrintReportVO */
			/*{
				"printParam":{
				"orgID":"11",
				"fromDate":"2018-04-01",
				"toDate":"2019-03-31",
				"registerType":"AL", //AL:All Ledger, GL:Group, CL: Customer, VL:Vendor, 
				},
				"reportContentType":"pdf"
				
				}*/
			@RequestMapping(value="/allLedgers", method = RequestMethod.POST)
			public void downloadAllLedgers(HttpServletResponse response,@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody TransactionVO printTxVO) {
			
				PrintReportVO printVO=new PrintReportVO();
				APIResponseVO apiResponse=new APIResponseVO();
				SocietyVO societyVO = new SocietyVO();
				HashMap<String, Object> param= printTxVO.getPrintParam();
				logger.debug("Entry : public void downloadAllLedgers(@RequestBody PrintReportVO printVO) ");
				try {
					
					UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
					param.put("orgID", decrptVO.getOrgID());
						printTxVO.setPrintParam(param);		
						
						printTxVO.setFromDate((String) param.get("fromDate"));
						printTxVO.setToDate((String) param.get("toDate"));
						
						String subGroupID=(String) param.get("subGroupID");					
						String ledgerType=(String) param.get("ledgerType");
						printTxVO.setType(ledgerType);
						
						printTxVO.setSourceType("BEAN");
						printTxVO.setReportContentType(printTxVO.getReportContentType());
						if(ledgerType.equalsIgnoreCase("AL")){				
							printTxVO.setSubGroupID(0);
							printVO=printReportService.printAllLedgerReport(printTxVO);					    
						}else if (ledgerType.equalsIgnoreCase("GL")){	
							printTxVO.setSubGroupID(Integer.parseInt(subGroupID));
							printVO=printReportService.printAllLedgerReport(printTxVO);
						}else if (ledgerType.equalsIgnoreCase("CL")){		
							printTxVO.setSubGroupID(1);
							printVO=printReportService.printAllLedgerReport(printTxVO);						   
						}else if (ledgerType.equalsIgnoreCase("VL")){		
							printTxVO.setSubGroupID(8);
							printVO=printReportService.printAllLedgerReport(printTxVO);						   
						}						
											
						
						printVO.setBeanList(null);				
						FileInputStream myStream = new FileInputStream(printVO.getReportURL());

						// Set the content type and attachment header.
						response.addHeader("Content-disposition", "attachment;filename="+printVO.getReportID());
						response.addHeader("x-filename",printVO.getReportID());
						response.setContentType("application/pdf");
						
						// Copy the stream to the response's output stream.
						IOUtils.copy(myStream, response.getOutputStream());				

						response.flushBuffer();
					
				} catch (IOException e) {
					logger.error("Exception downloadRegisters "+e);
					
				}
				logger.debug("Exit : public void downloadRegisters(@RequestBody PrintReportVO printVO) ");						
				
		 
			}
}