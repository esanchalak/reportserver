package com.emanager.webservice;

public class RequestWrapper {
   
	private int orgID;
	private int userID;
	private int groupID;
	private int aptID;
	private int ledgerID;
	private String fromDate;
	private String toDate;
	private String type;
	private int rootID;
	private int billingCycleID;
	private int memberID;
	private int documentID;
	private int appID;
	private int categoryID;
		
	public int getAppID() {
		return appID;
	}
	public void setAppID(int appID) {
		this.appID = appID;
	}
	public int getDocumentID() {
		return documentID;
	}
	public void setDocumentID(int documentID) {
		this.documentID = documentID;
	}
	public int getMemberID() {
		return memberID;
	}
	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}
	public int getBillingCycleID() {
		return billingCycleID;
	}
	public void setBillingCycleID(int billingCycleID) {
		this.billingCycleID = billingCycleID;
	}
	public int getRootID() {
		return rootID;
	}
	public void setRootID(int rootID) {
		this.rootID = rootID;
	}
	public int getAptID() {
		return aptID;
	}
	public void setAptID(int aptID) {
		this.aptID = aptID;
	}
	public int getGroupID() {
		return groupID;
	}
	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}
	public int getLedgerID() {
		return ledgerID;
	}
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	public int getOrgID() {
		return orgID;
	}
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the categoryID
	 */
	public int getCategoryID() {
		return categoryID;
	}
	/**
	 * @param categoryID the categoryID to set
	 */
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}
		
	
	
}
