package com.emanager.webservice;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.taxation.dataAccessObject.gst.GstRequestVO;
import com.emanager.server.taxation.dataAccessObject.gst.GstTax;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Gstr3B;
import com.emanager.server.taxation.services.GSTReportService;
import com.emanager.server.userManagement.valueObject.UserVO;


@Controller
@RequestMapping("/secure/gstReports")
public class JSONGSTReportController {

	private static final Logger logger = Logger.getLogger(JSONInvoiceController.class);
	
	@Autowired
	GSTReportService gstReportService;
    
    DateUtility dateUtil=new DateUtility();
    public ConfigManager configManager =new ConfigManager();
    public EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
	public final String HEADER_SECURITY_TOKEN = "X-AuthToken";   
   
    @RequestMapping(value="/get/gst1Report",  method = RequestMethod.POST  )			
	public ResponseEntity getGst1Report(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody GstRequestVO gstReqVO) {
    	GstTax gstRptVO=new GstTax();
		APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity getGst1Report(@RequestBody GstRequestVO gstReqVO)")	;
		try {
		
			if(gstReqVO.getOrgID()>0){
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				gstReqVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				
				gstRptVO=gstReportService.getGST1Report(gstReqVO);
			}else{
				logger.info("Unable to fetch gst Report  for Organisation ID  : "+gstReqVO.getOrgID());
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
								
			} catch (Exception e) {
				logger.error("Exception in get HSN Report : "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
		logger.debug("Exit : public ResponseEntity getGst1Report(@RequestBody GstRequestVO gstReqVO)")	;						
		return new ResponseEntity(gstRptVO, HttpStatus.OK);
 
	}
    
    @RequestMapping(value="/get/gst2Report",  method = RequestMethod.POST  )			
   	public ResponseEntity getGst2Report(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody GstRequestVO gstReqVO) {
       	GstTax gstRptVO=new GstTax();
   		APIResponseVO apiResponse=new APIResponseVO();
   			logger.debug("Entry : public ResponseEntity getGst2Report(@RequestBody GstRequestVO gstReqVO)")	;
   		try {
   		
   			if(gstReqVO.getOrgID()>0){
   				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
   				gstReqVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
   				
   				gstRptVO=gstReportService.getGST2Report(gstReqVO);
   			}else{
   				logger.info("Unable to fetch gst Report  for Organisation ID  : "+gstReqVO.getOrgID());
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   								
   			} catch (Exception e) {
   				logger.error("Exception in get HSN Report : "+e);
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   			
   		logger.debug("Exit : public ResponseEntity getGst2Report(@RequestBody GstRequestVO gstReqVO)")	;						
   		return new ResponseEntity(gstRptVO, HttpStatus.OK);
    
   	}
    
    @RequestMapping(value="/get/gstr3BReport",  method = RequestMethod.POST  )			
	public ResponseEntity getGstr3BReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody GstRequestVO gstReqVO) {
    	Gstr3B gstRptVO=new Gstr3B();
		APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity getGst1Report(@RequestBody GstRequestVO gstReqVO)")	;
		try {
		
			if(gstReqVO.getOrgID()>0){
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				gstReqVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				
				gstRptVO=gstReportService.getGSTR3BReport(gstReqVO);
			}else{
				logger.info("Unable to fetch gst Report  for Organisation ID  : "+gstReqVO.getOrgID());
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
								
			} catch (Exception e) {
				logger.error("Exception in get GSTR 3B Report : "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
		logger.debug("Exit : public ResponseEntity getGstr3BReport(@RequestBody GstRequestVO gstReqVO)")	;						
		return new ResponseEntity(gstRptVO, HttpStatus.OK);
 
	}
    
    @RequestMapping(value="/get/getOTPrequest",  method = RequestMethod.POST  )			
	public ResponseEntity getGstOtpRequest(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody GstRequestVO gstReqVO) {
    	GstRequestVO gstRptVO=new GstRequestVO();
		APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity public ResponseEntity getGstOtpRequest(@RequestBody GstRequestVO gstReqVO)")	;
		try {
		
			if(gstReqVO.getOrgID()>0){
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				gstReqVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				
				gstRptVO=gstReportService.getGstOtpRequest(gstReqVO);
			}else{
				logger.info("Unable to fetch gst Report  for Organisation ID  : "+gstReqVO.getOrgID());
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
								
			} catch (Exception e) {
				logger.error("Exception in public ResponseEntity getGstOtpRequest : "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
		logger.debug("Exit : public ResponseEntity public ResponseEntity getGstOtpRequest(@RequestBody GstRequestVO gstReqVO)")	;						
		return new ResponseEntity(gstRptVO, HttpStatus.OK);
 
	}
    
    @RequestMapping(value="/get/getAuthToken",  method = RequestMethod.POST  )			
	public ResponseEntity getAuthToken(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody GstRequestVO gstReqVO) {
    	GstRequestVO gstRptVO=new GstRequestVO();
		APIResponseVO apiResponse=new APIResponseVO();
			logger.debug("Entry : public ResponseEntity public ResponseEntity getAuthToken(@RequestBody GstRequestVO gstReqVO)")	;
		try {
		
			if(gstReqVO.getOrgID()>0){
				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
				gstReqVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
				
				gstRptVO=gstReportService.getAuthToken(gstReqVO);
			}else{
				logger.info("Unable to fetch gst Report  for Organisation ID  : "+gstReqVO.getOrgID());
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
								
			} catch (Exception e) {
				logger.error("Exception in public ResponseEntity getAuthToken : "+e);
				apiResponse.setStatusCode(400);
				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
			}
			
		logger.debug("Exit : public ResponseEntity public ResponseEntity getAuthToken(@RequestBody GstRequestVO gstReqVO)")	;						
		return new ResponseEntity(gstRptVO, HttpStatus.OK);
 
	}
    
    
    @RequestMapping(value="/get/getGSTR1B2Breport",  method = RequestMethod.POST  )			
   	public ResponseEntity getGSTR1B2Breport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody GstRequestVO gstReqVO) {
       	GstRequestVO gstRptVO=new GstRequestVO();
   		APIResponseVO apiResponse=new APIResponseVO();
   			logger.debug("Entry : public ResponseEntity public ResponseEntity getGSTR1B2Breport(@RequestBody GstRequestVO gstReqVO)")	;
   		try {
   		
   			if(gstReqVO.getOrgID()>0){
   				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
   				gstReqVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
   				
   				gstRptVO=gstReportService.getGstr1B2Breport(gstReqVO);
   			}else{
   				logger.info("Unable to fetch gst Report  for Organisation ID  : "+gstReqVO.getOrgID());
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   								
   			} catch (Exception e) {
   				logger.error("Exception in public ResponseEntity getGSTR1B2Breport : "+e);
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   			
   		logger.debug("Exit : public ResponseEntity public ResponseEntity getGSTR1B2Breport(@RequestBody GstRequestVO gstReqVO)")	;						
   		return new ResponseEntity(gstRptVO, HttpStatus.OK);
    
   	}
    
    @RequestMapping(value="/get/getGstr2aB2Breport",  method = RequestMethod.POST  )			
   	public ResponseEntity getGstr2aB2Breport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody GstRequestVO gstReqVO) {
       	GstRequestVO gstRptVO=new GstRequestVO();
   		APIResponseVO apiResponse=new APIResponseVO();
   			logger.debug("Entry : public ResponseEntity public ResponseEntity getGstr2aB2Breport(@RequestBody GstRequestVO gstReqVO)")	;
   		try {
   		
   			if(gstReqVO.getOrgID()>0){
   				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
   				gstReqVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
   				
   				gstRptVO=gstReportService.getGSTR2AB2Breport(gstReqVO);
   			}else{
   				logger.info("Unable to fetch gst Report  for Organisation ID  : "+gstReqVO.getOrgID());
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   								
   			} catch (Exception e) {
   				logger.error("Exception in public ResponseEntity getGstr2aB2Breport : "+e);
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   			
   		logger.debug("Exit : public ResponseEntity public ResponseEntity getGstr2aB2Breport(@RequestBody GstRequestVO gstReqVO)")	;						
   		return new ResponseEntity(gstRptVO, HttpStatus.OK);
    
   	}
    
    @RequestMapping(value="/get/getCashLedgers",  method = RequestMethod.POST  )			
   	public ResponseEntity getCashLedgers(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody GstRequestVO gstReqVO) {
       	GstRequestVO gstRptVO=new GstRequestVO();
   		APIResponseVO apiResponse=new APIResponseVO();
   			logger.debug("Entry : public ResponseEntity public ResponseEntity getCashLedgers(@RequestBody GstRequestVO gstReqVO)")	;
   		try {
   		
   			if(gstReqVO.getOrgID()>0){
   				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
   				gstReqVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
   				
   				gstRptVO=gstReportService.getCashLedgersreport(gstReqVO);
   			}else{
   				logger.info("Unable to fetch gst Report  for Organisation ID  : "+gstReqVO.getOrgID());
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   								
   			} catch (Exception e) {
   				logger.error("Exception in public ResponseEntity getCashLedgers : "+e);
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   			
   		logger.debug("Exit : public ResponseEntity public ResponseEntity getCashLedgers(@RequestBody GstRequestVO gstReqVO)")	;						
   		return new ResponseEntity(gstRptVO, HttpStatus.OK);
    
   	}
   
    @RequestMapping(value="/post/saveGstr1",  method = RequestMethod.POST  )			
   	public ResponseEntity saveGSTR1Report(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody GstRequestVO gstReqVO) {
       	GstRequestVO gstRptVO=new GstRequestVO();
   		APIResponseVO apiResponse=new APIResponseVO();
   		
   			logger.debug("Entry : public ResponseEntity public ResponseEntity saveGSTR1Report(@RequestBody GstRequestVO gstReqVO)")	;
   		try {
   		
   			if(gstReqVO.getOrgID()>0){
   				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
   				gstReqVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
   				
   				gstRptVO=gstReportService.saveGSTR1Report(gstReqVO);
   			}else{
   				logger.info("Unable to fetch gst Report  for Organisation ID  : "+gstReqVO.getOrgID());
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   								
   			} catch (Exception e) {
   				logger.error("Exception in public ResponseEntity submitGSTR1Report : "+e);
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   			
   		logger.debug("Exit : public ResponseEntity public ResponseEntity saveGSTR1Report(@RequestBody GstRequestVO gstReqVO)")	;						
   		return new ResponseEntity(gstRptVO, HttpStatus.OK);
    
   	}
    
    @RequestMapping(value="/post/submitGstr1",  method = RequestMethod.POST  )			
   	public ResponseEntity submitGSTR1Report(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody GstRequestVO gstReqVO) {
       	GstRequestVO gstRptVO=new GstRequestVO();
   		APIResponseVO apiResponse=new APIResponseVO();
   		
   			logger.debug("Entry : public ResponseEntity public ResponseEntity submitGSTR1Report(@RequestBody GstRequestVO gstReqVO)")	;
   		try {
   		
   			if(gstReqVO.getOrgID()>0){
   				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
   				gstReqVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
   				
   				gstRptVO=gstReportService.submitGSTR1Report(gstReqVO);
   			}else{
   				logger.info("Unable to fetch gst Report  for Organisation ID  : "+gstReqVO.getOrgID());
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   								
   			} catch (Exception e) {
   				logger.error("Exception in public ResponseEntity submitGSTR1Report : "+e);
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   			
   		logger.debug("Exit : public ResponseEntity public ResponseEntity submitGSTR1Report(@RequestBody GstRequestVO gstReqVO)")	;						
   		return new ResponseEntity(gstRptVO, HttpStatus.OK);
    
   	}
    
    @RequestMapping(value="/post/retFileGstr1",  method = RequestMethod.POST  )			
   	public ResponseEntity retFileGSTR1Report(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody GstRequestVO gstReqVO) {
       	GstRequestVO gstRptVO=new GstRequestVO();
   		APIResponseVO apiResponse=new APIResponseVO();
   		
   			logger.debug("Entry : public ResponseEntity public ResponseEntity retFileGSTR1Report(@RequestBody GstRequestVO gstReqVO)")	;
   		try {
   		
   			if(gstReqVO.getOrgID()>0){
   				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
   				gstReqVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
   				
   				gstRptVO=gstReportService.retFileGSTR1Report(gstReqVO);
   			}else{
   				logger.info("Unable to fetch gst Report  for Organisation ID  : "+gstReqVO.getOrgID());
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   								
   			} catch (Exception e) {
   				logger.error("Exception in public ResponseEntity retFileGSTR1Report : "+e);
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   			
   		logger.debug("Exit : public ResponseEntity public ResponseEntity retFileGSTR1Report(@RequestBody GstRequestVO gstReqVO)")	;						
   		return new ResponseEntity(gstRptVO, HttpStatus.OK);
    
   	}
    
    @RequestMapping(value="/get/getGstr1Status",  method = RequestMethod.POST  )			
   	public ResponseEntity getGstr1Status(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody GstRequestVO gstReqVO) {
       	GstRequestVO gstRptVO=new GstRequestVO();
   		APIResponseVO apiResponse=new APIResponseVO();
   			logger.debug("Entry : public ResponseEntity public ResponseEntity getCashLedgers(@RequestBody GstRequestVO gstReqVO)")	;
   		try {
   		
   			if(gstReqVO.getOrgID()>0){
   				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
   				gstReqVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
   				
   				gstRptVO=gstReportService.getGstr1Status(gstReqVO);
   			}else{
   				logger.info("Unable to fetch gst Report  for Organisation ID  : "+gstReqVO.getOrgID());
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   								
   			} catch (Exception e) {
   				logger.error("Exception in public ResponseEntity getGstr1Status : "+e);
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   			
   		logger.debug("Exit : public ResponseEntity public ResponseEntity getGstr1Status(@RequestBody GstRequestVO gstReqVO)")	;						
   		return new ResponseEntity(gstRptVO, HttpStatus.OK);
    
   	}
    
    //=============GST R 3 B ===============//
 
    @RequestMapping(value="/post/saveGstr3B",  method = RequestMethod.POST  )			
   	public ResponseEntity saveGSTR3BReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody GstRequestVO gstReqVO) {
       	GstRequestVO gstRptVO=new GstRequestVO();
   		APIResponseVO apiResponse=new APIResponseVO();
   		
   			logger.debug("Entry : public ResponseEntity public ResponseEntity saveGSTR1Report(@RequestBody GstRequestVO gstReqVO)")	;
   		try {
   		
   			if(gstReqVO.getOrgID()>0){
   				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
   				gstReqVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
   				
   				gstRptVO=gstReportService.saveGSTR3BReport(gstReqVO);
   			}else{
   				logger.info("Unable to fetch gst Report  for Organisation ID  : "+gstReqVO.getOrgID());
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   								
   			} catch (Exception e) {
   				logger.error("Exception in public ResponseEntity saveGSTR1Report : "+e);
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   			
   		logger.debug("Exit : public ResponseEntity public ResponseEntity saveGSTR1Report(@RequestBody GstRequestVO gstReqVO)")	;						
   		return new ResponseEntity(gstRptVO, HttpStatus.OK);
    
   	}
    
    
    
    //============== Public GST APIs =================//
    @RequestMapping(value="/get/gstInInfoReport",  method = RequestMethod.POST  )			
   	public ResponseEntity getGstInInfoReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody GstRequestVO gstReqVO) {
       	GstRequestVO gstRptVO=new GstRequestVO();
   		APIResponseVO apiResponse=new APIResponseVO();
   			logger.debug("Entry : public ResponseEntity getGstInInfoReport(@RequestBody GstRequestVO gstReqVO)")	;
   		try {
   		
   			if(gstReqVO.getOrgID()>0){
   				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
   				gstReqVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
   				
   				gstRptVO=gstReportService.getGstInInfoReport(gstReqVO);
   			}else{
   				logger.info("Unable to fetch gst info Report  for Organisation ID  : "+gstReqVO.getOrgID());
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   								
   			} catch (Exception e) {
   				logger.error("Exception in get GSTin info Report : "+e);
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   			
   		logger.debug("Exit : public ResponseEntity getGstInInfoReport(@RequestBody GstRequestVO gstReqVO)")	;						
   		return new ResponseEntity(gstRptVO, HttpStatus.OK);
    
   	}
    
    
    @RequestMapping(value="/get/gstRFiledHistoryReport",  method = RequestMethod.POST  )			
   	public ResponseEntity getGstRFiledHistoryReport(@RequestHeader(HEADER_SECURITY_TOKEN) String accessToken,@RequestBody GstRequestVO gstReqVO) {
       	GstRequestVO gstRptVO=new GstRequestVO();
   		APIResponseVO apiResponse=new APIResponseVO();
   			logger.debug("Entry : public ResponseEntity getGstRFiledHistoryReport(@RequestBody GstRequestVO gstReqVO)")	;
   		try {
   		
   			if(gstReqVO.getOrgID()>0){
   				UserVO decrptVO=enDeUtility.decodeStringToBean(accessToken);
   				gstReqVO.setOrgID(Integer.parseInt(decrptVO.getOrgID()));
   				
   				gstRptVO=gstReportService.getGstRFiledHistoryReport(gstReqVO);
   			}else{
   				logger.info("Unable to fetch gstr filed history Report  for Organisation ID  : "+gstReqVO.getOrgID());
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   								
   			} catch (Exception e) {
   				logger.error("Exception in get GSTr filed history Report : "+e);
   				apiResponse.setStatusCode(400);
   				apiResponse.setMessage(configManager.getPropertiesValue("error."+apiResponse.getStatusCode()));
   				return new ResponseEntity(apiResponse, HttpStatus.BAD_REQUEST);
   			}
   			
   		logger.debug("Exit : public ResponseEntity getGstRFiledHistoryReport(@RequestBody GstRequestVO gstReqVO)")	;						
   		return new ResponseEntity(gstRptVO, HttpStatus.OK);
    
   	}
    
}