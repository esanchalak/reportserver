package com.emanager.server.dashBoard.Domain;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

//import org.apache.batik.css.engine.value.css2.SrcManager;
import org.apache.log4j.Logger;
//import org.apache.poi.hssf.record.DVALRecord;
//import org.krysalis.barcode4j.impl.upcean.UPCA;
import org.springframework.dao.EmptyResultDataAccessException;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.dashBoard.DAO.DashBoardDAO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardAccountVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardFacilityVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardMemberVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardSocietyVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardTenantVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardVehicleVO;
import com.emanager.server.financialReports.domainObject.TotalChargesReportDomain;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.financialReports.valueObject.RptTransactionVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;



public class DashBoardDomain {
	DashBoardDAO dashBoardDAO;
	
	ReportService reportService;
	SocietyService societyService;
	private static final Logger logger = Logger.getLogger(DashBoardDomain.class);
	
	DashBoardAccountVO newAccontVO=new DashBoardAccountVO();
	
	
	public DashBoardVO getSocietyDashBoard(int societyId,String version){
	logger.debug("Entry : public DashBoardVO getSocietyDashBoard(int societyId)"+version);
	DashBoardVO dbVO=new DashBoardVO();
	
	
	try {
		dbVO.setDbSocietyVO(getSocietyDetails(societyId));
		
		dbVO.setDbMemberVO(getMemberDetails(societyId));
		
		dbVO.setDbTenantVO(getTenantDetails(societyId));
		
		dbVO.setDbVehicleVO(getVehicleDetails(societyId));
		
		dbVO.setDbAccountVO(getNewAccountDetails(societyId, version));
		
		//dbVO.setDbFacilityVO(getFacilityDetails(societyId));
	}catch(EmptyResultDataAccessException Ex){
		logger.info("No Data found getSocietyDashBoard "+Ex);
	}catch(NullPointerException Ex){
		logger.info("No Data found getSocietyDashBoard "+Ex);
			
	} catch (Exception e) {
		
		logger.error("Exception occured at getSocietyDashBoard "+e.getMessage());
	}
		
	logger.debug("Exit : public DashBoardVO getSocietyDashBoard(int societyId)");	
	return dbVO;
	}
	
	
	
	public DashBoardSocietyVO getSocietyDetails(int societyId){
		logger.debug("Entry : public DashBoardSocietyVO getSocietyDetails(int societyId)");
		DashBoardSocietyVO dbVO=new DashBoardSocietyVO();
		try{
		
		dbVO=dashBoardDAO.countTotalSoldUnSoldUnits(societyId);
		dbVO.setNoOfunits(dashBoardDAO.countTotalUnits(societyId));
		
		dbVO.setNoOfBuildings(dashBoardDAO.countTotalBuildings(societyId));
		
		logger.info("Here no of flats "+dbVO.getNoOfBuildings()+" no of sold "+dbVO.getNoSoldUnits()+" no of unsold "+dbVO.getNoUnsoldUnits());
		
		
		
		
		}catch(EmptyResultDataAccessException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
		}catch(NullPointerException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
		}catch (Exception e) {
			logger.error("Exception : public DashBoardSocietyVO getSocietyDetails(int societyId)"+e);
		}
		
		logger.debug("Exit : public DashBoardSocietyVO getSocietyDetails(int societyId)");	
		return dbVO;
		}
	
	public DashBoardMemberVO getMemberDetails(int societyId){
		logger.debug("Entry : public DashBoardMemberVO getMemberDetails(int societyId)");
		DashBoardMemberVO dbVO=new DashBoardMemberVO();
		try{
		dbVO=dashBoardDAO.countMembers(societyId);
		
		dbVO.setMissingNomination(dashBoardDAO.countNonNomineeMembers(societyId));
		
		dbVO.setMissingShare(dashBoardDAO.countSharCerteMissing(societyId));
		}catch(EmptyResultDataAccessException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
		}catch(NullPointerException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
		}catch (Exception e) {
			logger.error("Exception : public DashBoardMemberVO getMemberDetails(int societyId)"+e);
		}
			
		logger.debug("Exit : public DashBoardMemberVO getMemberDetails(int societyId)");	
		return dbVO;
		}
	
	public DashBoardTenantVO getTenantDetails(int societyId){
		logger.debug("Entry : public DashBoardTenantVO getTenantDetails(int societyId)");
		DashBoardTenantVO dbVO=new DashBoardTenantVO();
		try{
		
		dbVO=dashBoardDAO.countMissingTenants(societyId);
		dbVO.setNoOfTenants(dashBoardDAO.countTenants(societyId).size());
		int noTenantsWithoutInfo=dbVO.getNoOfTenants()-dbVO.getNoOfTenantsWithInfo();
		dbVO.setNoOfTenantsWithoutInfo(noTenantsWithoutInfo);
		
		logger.info("Here "+dbVO.getNoOfTenants()+" "+dbVO.getNoOfTenantsWithInfo()+" "+dbVO.getMissingEmail());
		
		}catch(EmptyResultDataAccessException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
		}catch(NullPointerException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
		}catch (Exception e) {
			logger.error("Exception : public DashBoardTenantVO getTenantDetails(int societyId)"+e);
		}
		logger.debug("Exit : public DashBoardTenantVO getTenantDetails(int societyId)");	
		return dbVO;
		}
	
	public DashBoardAccountVO getAccountDetails(int societyId,String version){
		logger.debug("Entry : public DashBoardAccountVO getAccountDetails(int societyId)");
		DashBoardAccountVO dbVO=new DashBoardAccountVO();
		
		//if(version.equalsIgnoreCase("1.1")){
			dbVO=getNewAccountDetails(societyId, version);
		/*}else
			dbVO=getOldAccountDetails(societyId, version);*/
		logger.debug("Exit : public DashBoardAccountVO getAccountDetails(int societyId)");	
		return dbVO;
		}
	
	public DashBoardAccountVO getOldAccountDetails(int societyId,String version){
		logger.debug("Entry : public DashBoardAccountVO getAccountDetails(int societyId)");
		DashBoardAccountVO dbVO=new DashBoardAccountVO();
		try{
		Date today=new Date();	
		java.sql.Date currentDate = new java.sql.Date(today.getTime());
		SocietyVO societyVO=societyService.getSocietyDetails(societyId);
		dbVO=dashBoardDAO.getAccountsDetails(societyId);
		newAccontVO=calculateDueMembers(societyId,""+currentDate);
		dbVO.setDueMembers(newAccontVO.getDueMembers());
		dbVO.setTotalDues(newAccontVO.getTotalDues());
		dbVO.setLastCashTx(getLatestTxDate(societyVO, "Cash",version,""+currentDate));
		dbVO.setLastChqTx(getLatestTxDate(societyVO,"Bank",version,""+currentDate));
		}catch(EmptyResultDataAccessException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
		}catch(NullPointerException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
		}catch (Exception e) {
			logger.error("Exp getOldAccountDetails"+e);
		}	
		logger.debug("Exit : public DashBoardAccountVO getAccountDetails(int societyId)");	
		return dbVO;
		}
	public DashBoardAccountVO getNewAccountDetails(int societyId,String version){
		logger.debug("Entry : public DashBoardAccountVO getAccountDetails(int societyId)");
		DashBoardAccountVO dbVO=new DashBoardAccountVO();
		try{
			SocietyVO societyVO=societyService.getSocietyDetails(societyId);
			Date today=new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(today);
			cal.add(Calendar.MONTH, 1);
			int month = cal.get(Calendar.MONTH);
			int year  = cal.get(Calendar.YEAR);
			
			if(month<=3){
				year=year-1;
			}
			String startDate=year+"-04-01";
			String toDate=(year+1)+"-03-31";
			dbVO=dashBoardDAO.getSusupenseEntry(societyVO, dbVO,startDate,toDate);
			java.sql.Date currentDate = new java.sql.Date(today.getTime());
			DashBoardAccountVO incomeVO=dashBoardDAO.getIncomeExpenseTotal(societyVO,1, startDate, toDate);
			DashBoardAccountVO expnseVO=dashBoardDAO.getIncomeExpenseTotal(societyVO, 2, startDate, toDate);		
			
			dbVO.setSumOfDeposits(incomeVO.getTotal());
			dbVO.setSumOfExpenses(expnseVO.getTotal());
		//List defaulterList=reportService.getMemberDueNew(toDate, societyId,"0","APT");
		//newAccontVO=calculateDueMembers(societyId);
		//dbVO.setDueMembers(defaulterList.size());
		//dbVO.setTotalDues(newAccontVO.getTotalDues());
		dbVO.setLastCashTx(getLatestTxDate(societyVO, "Cash",version,toDate));
		dbVO.setLastChqTx(getLatestTxDate(societyVO,"Bank",version,toDate));
		}catch(EmptyResultDataAccessException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
		}catch(NullPointerException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
		}catch (Exception e) {
			logger.error("Exp getNewAccountDetails"+e);
		}	
		logger.debug("Exit : public DashBoardAccountVO getAccountDetails(int societyId)");	
		return dbVO;
		}
	public DashBoardAccountVO calculateDueMembers(int societyId,String uptoDate){
		logger.debug("Entry : public DashBoardAccountVO calculateDueMembers(int societyId)");
		DashBoardAccountVO dbVO=new DashBoardAccountVO();
		BigDecimal totalAmount=BigDecimal.ZERO;
		List defaulterList=new ArrayList();
		try{
		
		Date today=new Date();
		java.sql.Date currentDate = new java.sql.Date(today.getTime());
		
		
		defaulterList=reportService.getMemberDueNew(uptoDate, societyId,"0","APT");
		if(defaulterList.size()>0){
		for(int i=0;defaulterList.size()>i;i++){
			RptTransactionVO ldgrVO=(RptTransactionVO) defaulterList.get(i);
			totalAmount=totalAmount.add(ldgrVO.getActualTotal());
			
		}
		}
		dbVO.setTotalDues(totalAmount);
		dbVO.setDueMembers(defaulterList.size());
		
		}catch (Exception e) {
			logger.error("Exception occured in calculateDueMembers "+e);
		}
		
		logger.debug(defaulterList.size()+"No of defaulters have the amount of dues ="+totalAmount);
			
		logger.debug("Exit : public DashBoardAccountVO calculateDueMembers(int societyId)");	
		return dbVO;
	}
	
	public String getLatestTxDate(SocietyVO societyVO,String type,String version,String uptoDate){
		logger.debug("Entry : public Date getLatestTxDate(int societyId)");
		String lastDate="";
		DashBoardAccountVO dbVOBank=new DashBoardAccountVO();
		
		dbVOBank=dashBoardDAO.findLatestDate(societyVO,type,version,uptoDate);
		
		try {
		SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		
			date = sdfSource.parse(dbVOBank.getLastChqTx());
		

        // create SimpleDateFormat object with desired date format
        SimpleDateFormat sdfDestination = new SimpleDateFormat(
                "dd-MMM-yy");

        // parse the date into another format
        lastDate = sdfDestination.format(date);
		}catch(EmptyResultDataAccessException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
		}catch(NullPointerException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			logger.error("Exception : private String getLatestTxDate(int societyId,String type,String version)"+e);
		}
			
		logger.debug("Exit : public Date getLatestTxDate(int societyId)"+lastDate);	
		return lastDate;
		}
	
	public DashBoardVehicleVO getVehicleDetails(int societyId){
		logger.debug("Entry : public DashBoardVehicleVO getVehicleDetails(int societyId)");
		DashBoardVehicleVO dbVO=new DashBoardVehicleVO();
		
		dbVO=dashBoardDAO.countVehicles(societyId);
		
			
		logger.debug("Exit : public DashBoardVehicleVO getVehicleDetails(int societyId)");	
		return dbVO;
		}
	/*public DashBoardFacilityVO getFacilityDetails(int societyId){
		logger.debug("Entry : public DashBoardFacilityVO getFacilityDetails(int societyId)");
		List contractExpiryList=null;
		Calendar calendar = Calendar.getInstance();
		java.util.Date curdt=new java.util.Date();
		   calendar.setTime(curdt);
		   java.sql.Date today = new java.sql.Date(calendar.getTime().getTime());
		DashBoardFacilityVO dbVO=new DashBoardFacilityVO();
		
		try {
			dbVO=dashBoardDAO.getContractDetails(societyId);
			
			dbVO.setExpiredSoon(dashBoardDAO.getSoonExpiringContracts(societyId));
			
			contractExpiryList=dashBoardDAO.getAfterContractExpiryList(societyId);
			
			for(int i=0;contractExpiryList.size()>i;i++){
				MemberVO member=(MemberVO) contractExpiryList.get(i);
				Date maxdt= member.getCurrentDate();
				
				 if( maxdt.compareTo(today)> 0  ){
					 contractExpiryList.remove(i);
					 --i;
				 }
				
			}
		}catch(EmptyResultDataAccessException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
		}catch(NullPointerException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
			dbVO.setExpiredContracts(contractExpiryList.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception  public DashBoardFacilityVO getFacilityDetails(int societyId)", e);
		}
		
		
			
		logger.debug("Exit : public DashBoardFacilityVO getFacilityDetails(int societyId)");	
		return dbVO;
		}*/
    
	public DashBoardAccountVO getSocietyAccountStatus(int societyId,String fromDate,String toDate){
		logger.debug("Entry : public DashBoardVO getSocietyDashBoard(int societyId)");
		DashBoardAccountVO dbVO = new DashBoardAccountVO();;
		try {
		SocietyVO societyVO=societyService.getSocietyDetails(societyId);
			
			
			
			//java.sql.Date currentDate = new java.sql.Date(today.getTime());
			
		
			dbVO=dashBoardDAO.getNewAccountsDetails(societyVO,fromDate,toDate);
			dbVO.setLastCashTx(getLatestTxDate(societyVO, "Cash", "1.1",toDate));
			dbVO.setLastChqTx(getLatestTxDate(societyVO, "Bank", "1.1",toDate));
			dbVO.setMissingEntries(dashBoardDAO.getMissingBillsEntry(societyVO,toDate));
			dbVO.setServiceTxDue(BigDecimal.ZERO);
			dbVO.setTdsDue(BigDecimal.ZERO);
			dbVO.setAccountantName(societyVO.getAccountant());
			dbVO.setAccClosureDate(societyVO.getTxCloseDate());
			dbVO.setSocietyName(societyVO.getSocietyName());
			dbVO.setAuditQryCnt(dashBoardDAO.getCountOfAuditQueries(societyId));
			
			}catch(EmptyResultDataAccessException Ex){
				logger.info("No Data found getSocietyDashBoard "+Ex);
			}catch(NullPointerException Ex){
				logger.info("No Data found getSocietyDashBoard "+Ex);
			}catch (Exception e) {
				logger.error("Exp getNewAccountDetails"+e);
			}	
		
		
			
		logger.debug("Exit : public DashBoardVO getSocietyDashBoard(int societyId)");	
		return dbVO;
	}

	
	public DashBoardAccountVO getSuspenceEntryDetails(int societyId){
		logger.debug("Entry : public DashBoardAccountVO getAccountDetails(int societyId)");
		DashBoardAccountVO dbVO=new DashBoardAccountVO();
		SocietyVO societyVO=societyService.getSocietyDetails(societyId);
		try{
			Date today=new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(today);
			cal.add(Calendar.MONTH, 1);
			int month = cal.get(Calendar.MONTH);
			int year  = cal.get(Calendar.YEAR);
			
			if(month<=3){
				year=year-1;
			}
			String startDate=year+"-04-01";
			String toDate=(year+1)+"-03-31";
			dbVO=dashBoardDAO.getSusupenseEntry(societyVO, dbVO,startDate,toDate);
		
		}catch(EmptyResultDataAccessException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
		}catch(NullPointerException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
		}catch (Exception e) {
			logger.error("Exp getNewAccountDetails"+e);
		}	
		logger.debug("Exit : public DashBoardAccountVO getAccountDetails(int societyId)");	
		return dbVO;
		}

	public DashBoardAccountVO getPendingTransactionsCount(int societyId){
		logger.debug("Entry : public DashBoardAccountVO getPendingTransactionsCount(int societyId)");
		DashBoardAccountVO dbVO=new DashBoardAccountVO();
		SocietyVO societyVO=societyService.getSocietyDetails(societyId);
			int count=0;
			count=dashBoardDAO.getPendingTransactionsCount(societyVO);
			dbVO.setPendingTxCount(count);
	
		logger.debug("Exit : public DashBoardAccountVO getPendingTransactionsCount(int societyId)");	
		return dbVO;
		}
	
	public DashBoardDAO getDashBoardDAO() {
		return dashBoardDAO;
	}



	public void setDashBoardDAO(DashBoardDAO dashBoardDAO) {
		this.dashBoardDAO = dashBoardDAO;
	}



	public ReportService getReportService() {
		return reportService;
	}



	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}



	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}







	
}