package com.emanager.server.dashBoard.Domain;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
//import org.apache.poi.hssf.record.DVALRecord;
import org.springframework.dao.EmptyResultDataAccessException;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.commonUtils.valueObject.DropDownVO;
import com.emanager.server.dashBoard.DAO.DashBoardDAO;
import com.emanager.server.dashBoard.Services.DashBoardService;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardAccountVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardFacilityVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardMemberVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardSocietyVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardTenantVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardVehicleVO;
import com.emanager.server.financialReports.domainObject.TotalChargesReportDomain;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.financialReports.services.TrialBalanceService;
import com.emanager.server.financialReports.valueObject.MonthwiseChartVO;
import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;




public class AccountDashBoardDomain {
	DashBoardDAO dashBoardDAO;
	DashBoardDomain dashBoardDomain;
	ReportService reportService;
	SocietyService societyService;
	LedgerService ledgerService;
	TrialBalanceService trialBalanceService;
	private static final Logger logger = Logger.getLogger(AccountDashBoardDomain.class);
	
	DashBoardAccountVO newAccontVO=new DashBoardAccountVO();
	
	public DashBoardAccountVO getAccountsDashBoard(int societyId,String fromDate,String uptoDate){
		logger.debug("Entry : public DashBoardVO getAccountsDashBoard(int societyId,String fromDate,String uptoDate)");
		DashBoardAccountVO dbVO = new DashBoardAccountVO();
		try {
					
			dbVO=getAccountsDashBoardDetails(societyId, fromDate, uptoDate);
			
			
		} catch (Exception e) {
			logger.error("Exception ocurred at getAccountsDashBoard", e);
		}
		
			
		logger.debug("Exit : public DashBoardVO getAccountsDashBoard(int societyId,String fromDate,String uptoDate)");	
		return dbVO;
		}
		
	
	private DashBoardAccountVO getAccountsDashBoardDetails(int societyId,String fromDate,String uptoDate){
		logger.debug("Entry : public DashBoardVO getAccountsDashBoard(int societyId,String fromDate,String uptoDate)");
		DashBoardAccountVO dbVO = new DashBoardAccountVO();
		List objectList=new ArrayList<DropDownVO>();
		AccountHeadVO profitLossVO=new AccountHeadVO();
		AccountHeadVO cashVO=new AccountHeadVO();
		AccountHeadVO bankVO=new AccountHeadVO();
		List ledgerList=new ArrayList();
		BigDecimal differenceInOpening=BigDecimal.ZERO;
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(societyId);
			dbVO=dashBoardDAO.getSusupenseEntry(societyVO, dbVO,fromDate,uptoDate);
		
			
			dbVO.setLastCashTx(dashBoardDomain.getLatestTxDate(societyVO, "Cash", "1.1",uptoDate));
			dbVO.setLastChqTx(dashBoardDomain.getLatestTxDate(societyVO, "Bank", "1.1",uptoDate));
			cashVO=getGroupBalance(societyId, 5, fromDate, uptoDate);
			bankVO=getGroupBalance(societyId, 4, fromDate, uptoDate);
			dbVO.setCashBalance(cashVO.getClosingBalance());
			dbVO.setBankBalance(bankVO.getClosingBalance());
			dbVO.setPendingTxCount(dashBoardDAO.getPendingTransactionsCount(societyVO));
		
			dbVO.setTdsDue(BigDecimal.ZERO);
			dbVO.setAccountantName("Esanchalak");
			dbVO.setAccClosureDate(societyVO.getTxCloseDate());
			dbVO.setSocietyName(societyVO.getSocietyName());
			
			
			/*DropDownVO incmVO=new DropDownVO();
			incmVO.setLabel("INCOME (A)");
			if(dbVO.getSumOfDeposits()!=null)
			incmVO.setData(dbVO.getSumOfDeposits().toString());
			else incmVO.setData("0");
			
		
				objectList.add(incmVO);
			
			
			DropDownVO expVO=new DropDownVO();
			expVO.setLabel("EXPENSE (B)");
			if(dbVO.getSumOfExpenses()!=null)
			expVO.setData(dbVO.getSumOfExpenses().toString());
			else expVO.setData("0");
				
				objectList.add(expVO);
			
			
			DropDownVO proffVO=new DropDownVO();
			proffVO.setLabel("PROFIT/(LOSS)");
			if(profitLossVO.getClosingBalance()!=null)
			proffVO.setData(profitLossVO.getClosingBalance().toString());
			else proffVO.setData("0");
				objectList.add(proffVO);
			
			
			DropDownVO rcVO=new DropDownVO();
			rcVO.setLabel("RECIEVEABLE ACCOUNTS");
			if(newAccontVO.getTotalDues()!=null)
			rcVO.setData(newAccontVO.getTotalDues().toString());
			else rcVO.setData("0");
				objectList.add(rcVO);
			
				if(differenceInOpening.intValue()!=0){
					DropDownVO diffInBal=new DropDownVO();
					diffInBal.setLabel("DIFF IN OPENING BALANCE");
					diffInBal.setData(differenceInOpening.abs().toString());
					objectList.add(diffInBal);
					
				}
				
				
				
			DropDownVO servVO=new DropDownVO();
			servVO.setLabel("SERVICE TAX PAYABLE");
			servVO.setData(dbVO.getServiceTxDue().toString());
			
				objectList.add(servVO);
			
			
			DropDownVO tdsVO=new DropDownVO();
			tdsVO.setLabel("TDS PAYABLE");
			tdsVO.setData(dbVO.getTdsDue().toString());
			
				objectList.add(tdsVO);
				
			
			
			dbVO.setObjectList(objectList);*/
			
		} catch (Exception e) {
			logger.error("Exception ocurred at getAccountsDashBoard", e);
		}
		
			
		logger.debug("Exit : public DashBoardVO getAccountsDashBoard(int societyId,String fromDate,String uptoDate)");	
		return dbVO;
		}
	
	private AccountHeadVO getGroupBalance(int societyID,int groupID,String fromDate,String uptoDate){
		AccountHeadVO accntVO=new AccountHeadVO();
		logger.debug("Entry : private AccountHeadVO getGroupBalance(int societyID,int groupID,String fromDate,String uptoDate)");
		try{
			
			accntVO=ledgerService.getGroupBalance(societyID, groupID, fromDate, uptoDate);
			
		}catch (Exception e) {
			logger.error("Exception occurred in getGroupBalance "+e);
		}
		
		logger.debug("Exit : private AccountHeadVO getGroupBalance(int societyID,int groupID,String fromDate,String uptoDate)");
		return accntVO;
	}

	
	public DashBoardAccountVO getFinanceAccountsDashBoard(int orgID,String fromDate,String uptoDate){
		logger.debug("Entry : public DashBoardVO getFinanceAccountsDashBoard(int societyId,String fromDate,String uptoDate)");
		DashBoardAccountVO dbVO = new DashBoardAccountVO();
		try {
			ReportVO profitLossVO=new ReportVO();
			MonthwiseChartVO cashflowVO=new MonthwiseChartVO();
			List cashList=new ArrayList();
			List bankList=new ArrayList();
			BigDecimal receivableDues=BigDecimal.ZERO;
			BigDecimal payableDues=BigDecimal.ZERO;
			
			profitLossVO=reportService.getProfitLossReport(orgID, fromDate, uptoDate, "S");
			
			cashflowVO=reportService.getReceiptPaymentReportForChart(orgID, fromDate, uptoDate,"CF");
			
			bankList=trialBalanceService.getLedgerReport(orgID, fromDate, uptoDate, 4,0);
			
			cashList=trialBalanceService.getLedgerReport(orgID, fromDate, uptoDate, 5,0);
			
			receivableDues=calculateReceivablePayableDues(trialBalanceService.getLedgerReportFromCategory(orgID, fromDate, uptoDate, 1,0));
			
			payableDues=calculateReceivablePayableDues(trialBalanceService.getLedgerReportFromCategory(orgID, fromDate, uptoDate, 96,0));
			
			dbVO.setSocietyId(""+orgID);
			
			dbVO.setFromDate(fromDate);
			
			dbVO.setToDate(uptoDate);
			
			dbVO.setProfitLossVO(profitLossVO);
			
			dbVO.setCashFlowVO(cashflowVO);
			
			dbVO.setBankList(bankList);
			
			dbVO.setCashList(cashList);
			
			dbVO.setRecievableDues(receivableDues);
			
			dbVO.setPayableDues(payableDues);
			
		} catch (Exception e) {
			logger.error("Exception ocurred at getFinanceAccountsDashBoard", e);
		}
		
			
		logger.debug("Exit : public DashBoardVO getFinanceAccountsDashBoard(int societyId,String fromDate,String uptoDate)");	
		return dbVO;
		}
	
	
	private BigDecimal calculateReceivablePayableDues(List receivablePayableList){
		BigDecimal total=BigDecimal.ZERO;
		
		if(receivablePayableList.size()>0){
			
			for(int i=0;receivablePayableList.size()>i;i++){
				AccountHeadVO achVO=(AccountHeadVO) receivablePayableList.get(i);
				
				if(achVO.getClosingBalance().compareTo(BigDecimal.ZERO)>0){
					total=total.add(achVO.getClosingBalance());
				}
				
				
			}
			
			
		}
		
		return total;
	}
	
	
	public void setDashBoardDAO(DashBoardDAO dashBoardDAO) {
		this.dashBoardDAO = dashBoardDAO;
	}


	public void setDashBoardDomain(DashBoardDomain dashBoardDomain) {
		this.dashBoardDomain = dashBoardDomain;
	}


	public void setLedgerService(LedgerService ledgerService) {
		this.ledgerService = ledgerService;
	}


	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}


	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}


	/**
	 * @param trialBalanceService the trialBalanceService to set
	 */
	public void setTrialBalanceService(TrialBalanceService trialBalanceService) {
		this.trialBalanceService = trialBalanceService;
	}
		

	





	
}