package com.emanager.server.dashBoard.dataAccessObject;

public class DashBoardMemberVO {
	
	private String societyId;
	private String societyName;
	private int primaryMembers;
	private int assoMembers;
	private int missingPhone;
	private int missingEmail;
	private int missingNomination;
	private int missingShare;
	
	
	public int getAssoMembers() {
		return assoMembers;
	}
	public void setAssoMembers(int assoMembers) {
		this.assoMembers = assoMembers;
	}
	public int getMissingEmail() {
		return missingEmail;
	}
	public void setMissingEmail(int missingEmail) {
		this.missingEmail = missingEmail;
	}
	public int getMissingNomination() {
		return missingNomination;
	}
	public void setMissingNomination(int missingNomination) {
		this.missingNomination = missingNomination;
	}
	public int getMissingPhone() {
		return missingPhone;
	}
	public void setMissingPhone(int missingPhone) {
		this.missingPhone = missingPhone;
	}
	public int getMissingShare() {
		return missingShare;
	}
	public void setMissingShare(int missingShare) {
		this.missingShare = missingShare;
	}
	public int getPrimaryMembers() {
		return primaryMembers;
	}
	public void setPrimaryMembers(int primaryMembers) {
		this.primaryMembers = primaryMembers;
	}
	public String getSocietyId() {
		return societyId;
	}
	public void setSocietyId(String societyId) {
		this.societyId = societyId;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	
	
	
}
