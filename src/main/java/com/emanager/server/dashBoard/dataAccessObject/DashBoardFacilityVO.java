package com.emanager.server.dashBoard.dataAccessObject;


public class DashBoardFacilityVO {
	
	private String societyId;
	private String societyName;
	private int validContracts;
	private int expiredContracts;
	private int renewal;
	private int expiredSoon;
	
	public int getExpiredContracts() {
		return expiredContracts;
	}
	public void setExpiredContracts(int expiredContracts) {
		this.expiredContracts = expiredContracts;
	}
	public int getExpiredSoon() {
		return expiredSoon;
	}
	public void setExpiredSoon(int expiredSoon) {
		this.expiredSoon = expiredSoon;
	}
	public int getRenewal() {
		return renewal;
	}
	public void setRenewal(int renewal) {
		this.renewal = renewal;
	}
	public String getSocietyId() {
		return societyId;
	}
	public void setSocietyId(String societyId) {
		this.societyId = societyId;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	public int getValidContracts() {
		return validContracts;
	}
	public void setValidContracts(int validContracts) {
		this.validContracts = validContracts;
	}	
	
}
