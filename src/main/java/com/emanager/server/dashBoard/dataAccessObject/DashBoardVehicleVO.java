package com.emanager.server.dashBoard.dataAccessObject;


public class DashBoardVehicleVO {
	
	private String societyId;
	private String societyName;
	private int twoWheelers;
	private int fourWheelers;
	
	public int getFourWheelers() {
		return fourWheelers;
	}
	public void setFourWheelers(int fourWheelers) {
		this.fourWheelers = fourWheelers;
	}
	public String getSocietyId() {
		return societyId;
	}
	public void setSocietyId(String societyId) {
		this.societyId = societyId;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	public int getTwoWheelers() {
		return twoWheelers;
	}
	public void setTwoWheelers(int twoWheelers) {
		this.twoWheelers = twoWheelers;
	}

	
	
	
	
	
		
	
}
