package com.emanager.server.dashBoard.dataAccessObject;

import java.math.BigDecimal;
import java.util.List;

import com.emanager.server.financialReports.valueObject.MonthwiseChartVO;
import com.emanager.server.financialReports.valueObject.ReportVO;

public class DashBoardAccountVO {
	
	private String societyId;
	private String societyName;
	private int dueMembers;
	private BigDecimal totalDues;
	private String tdsStatus;
	private BigDecimal sumOfDeposits;
	private BigDecimal sumOfExpenses;
	private int suspenseEntries;
	private String lastChqTx;
	private String lastCashTx;
	private int notReconcilled;
	private String lastActivity;
	private BigDecimal serviceTxDue;
	private BigDecimal tdsDue;
	private String accClosureDate;
	private String accountantName;
	private int missingEntries;
	private List objectList;
	private BigDecimal cashBalance;
	private BigDecimal bankBalance;
	private BigDecimal total;
	private int auditQryCnt;
	private String committeeMail;
	private String statusPeriod;
	private String statusSendDate;
	private int suspenceReceiptCount;
	private BigDecimal suspenceReceiptSum;
	private int suspencePaymentCount;
	private BigDecimal suspencePaymentSum;
	private int pendingTxCount;
	private ReportVO profitLossVO;
	private MonthwiseChartVO cashFlowVO;
	private List cashList;
	private List bankList;
	private BigDecimal recievableDues;
	private BigDecimal payableDues;
	private String fromDate;
	private String toDate;
	
	public int getDueMembers() {
		return dueMembers;
	}
	public void setDueMembers(int dueMembers) {
		this.dueMembers = dueMembers;
	}
	public String getLastCashTx() {
		return lastCashTx;
	}
	public void setLastCashTx(String lastCashTx) {
		this.lastCashTx = lastCashTx;
	}
	public String getLastChqTx() {
		return lastChqTx;
	}
	public void setLastChqTx(String lastChqTx) {
		this.lastChqTx = lastChqTx;
	}
	public int getNotReconcilled() {
		return notReconcilled;
	}
	public void setNotReconcilled(int notReconcilled) {
		this.notReconcilled = notReconcilled;
	}
	public String getSocietyId() {
		return societyId;
	}
	public void setSocietyId(String societyId) {
		this.societyId = societyId;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	public BigDecimal getSumOfDeposits() {
		return sumOfDeposits;
	}
	public void setSumOfDeposits(BigDecimal sumOfDeposits) {
		this.sumOfDeposits = sumOfDeposits;
	}
	public BigDecimal getSumOfExpenses() {
		return sumOfExpenses;
	}
	public void setSumOfExpenses(BigDecimal sumOfExpenses) {
		this.sumOfExpenses = sumOfExpenses;
	}
	public int getSuspenseEntries() {
		return suspenseEntries;
	}
	public void setSuspenseEntries(int suspenseEntries) {
		this.suspenseEntries = suspenseEntries;
	}
	public String getTdsStatus() {
		return tdsStatus;
	}
	public void setTdsStatus(String tdsStatus) {
		this.tdsStatus = tdsStatus;
	}
	public BigDecimal getTotalDues() {
		return totalDues;
	}
	public void setTotalDues(BigDecimal totalDues) {
		this.totalDues = totalDues;
	}
	public String getAccClosureDate() {
		return accClosureDate;
	}
	public void setAccClosureDate(String accClosureDate) {
		this.accClosureDate = accClosureDate;
	}
	
	public String getLastActivity() {
		return lastActivity;
	}
	public void setLastActivity(String lastActivity) {
		this.lastActivity = lastActivity;
	}
	public BigDecimal getServiceTxDue() {
		return serviceTxDue;
	}
	public void setServiceTxDue(BigDecimal serviceTxDue) {
		this.serviceTxDue = serviceTxDue;
	}
	public BigDecimal getTdsDue() {
		return tdsDue;
	}
	public void setTdsDue(BigDecimal tdsDue) {
		this.tdsDue = tdsDue;
	}
	public String getAccountantName() {
		return accountantName;
	}
	public void setAccountantName(String accountantName) {
		this.accountantName = accountantName;
	}
	public int getMissingEntries() {
		return missingEntries;
	}
	public void setMissingEntries(int missingEntries) {
		this.missingEntries = missingEntries;
	}
	public List getObjectList() {
		return objectList;
	}
	public void setObjectList(List objectList) {
		this.objectList = objectList;
	}
	public BigDecimal getBankBalance() {
		return bankBalance;
	}
	public void setBankBalance(BigDecimal bankBalance) {
		this.bankBalance = bankBalance;
	}
	public BigDecimal getCashBalance() {
		return cashBalance;
	}
	public void setCashBalance(BigDecimal cashBalance) {
		this.cashBalance = cashBalance;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public int getAuditQryCnt() {
		return auditQryCnt;
	}
	public void setAuditQryCnt(int auditQryCnt) {
		this.auditQryCnt = auditQryCnt;
	}
	public String getCommitteeMail() {
		return committeeMail;
	}
	public void setCommitteeMail(String committeeMail) {
		this.committeeMail = committeeMail;
	}
	public String getStatusPeriod() {
		return statusPeriod;
	}
	public void setStatusPeriod(String statusPeriod) {
		this.statusPeriod = statusPeriod;
	}
	public String getStatusSendDate() {
		return statusSendDate;
	}
	public void setStatusSendDate(String statusSendDate) {
		this.statusSendDate = statusSendDate;
	}
	/**
	 * @return the suspenceReceiptCount
	 */
	public int getSuspenceReceiptCount() {
		return suspenceReceiptCount;
	}
	/**
	 * @return the suspenceReceiptSum
	 */
	public BigDecimal getSuspenceReceiptSum() {
		return suspenceReceiptSum;
	}
	/**
	 * @return the suspencePaymentCount
	 */
	public int getSuspencePaymentCount() {
		return suspencePaymentCount;
	}
	/**
	 * @return the suspencePaymentSum
	 */
	public BigDecimal getSuspencePaymentSum() {
		return suspencePaymentSum;
	}
	/**
	 * @param suspenceReceiptCount the suspenceReceiptCount to set
	 */
	public void setSuspenceReceiptCount(int suspenceReceiptCount) {
		this.suspenceReceiptCount = suspenceReceiptCount;
	}
	/**
	 * @param suspenceReceiptSum the suspenceReceiptSum to set
	 */
	public void setSuspenceReceiptSum(BigDecimal suspenceReceiptSum) {
		this.suspenceReceiptSum = suspenceReceiptSum;
	}
	/**
	 * @param suspencePaymentCount the suspencePaymentCount to set
	 */
	public void setSuspencePaymentCount(int suspencePaymentCount) {
		this.suspencePaymentCount = suspencePaymentCount;
	}
	/**
	 * @param suspencePaymentSum the suspencePaymentSum to set
	 */
	public void setSuspencePaymentSum(BigDecimal suspencePaymentSum) {
		this.suspencePaymentSum = suspencePaymentSum;
	}
	/**
	 * @return the pendingTxCount
	 */
	public int getPendingTxCount() {
		return pendingTxCount;
	}
	/**
	 * @param pendingTxCount the pendingTxCount to set
	 */
	public void setPendingTxCount(int pendingTxCount) {
		this.pendingTxCount = pendingTxCount;
	}
	/**
	 * @return the profitLossVO
	 */
	public ReportVO getProfitLossVO() {
		return profitLossVO;
	}
	/**
	 * @param profitLossVO the profitLossVO to set
	 */
	public void setProfitLossVO(ReportVO profitLossVO) {
		this.profitLossVO = profitLossVO;
	}
	/**
	 * @return the cashFlowVO
	 */
	public MonthwiseChartVO getCashFlowVO() {
		return cashFlowVO;
	}
	/**
	 * @param cashFlowVO the cashFlowVO to set
	 */
	public void setCashFlowVO(MonthwiseChartVO cashFlowVO) {
		this.cashFlowVO = cashFlowVO;
	}
	/**
	 * @return the cashList
	 */
	public List getCashList() {
		return cashList;
	}
	/**
	 * @param cashList the cashList to set
	 */
	public void setCashList(List cashList) {
		this.cashList = cashList;
	}
	/**
	 * @return the bankList
	 */
	public List getBankList() {
		return bankList;
	}
	/**
	 * @param bankList the bankList to set
	 */
	public void setBankList(List bankList) {
		this.bankList = bankList;
	}
	/**
	 * @return the recievableDues
	 */
	public BigDecimal getRecievableDues() {
		return recievableDues;
	}
	/**
	 * @param recievableDues the recievableDues to set
	 */
	public void setRecievableDues(BigDecimal recievableDues) {
		this.recievableDues = recievableDues;
	}
	/**
	 * @return the payableDues
	 */
	public BigDecimal getPayableDues() {
		return payableDues;
	}
	/**
	 * @param payableDues the payableDues to set
	 */
	public void setPayableDues(BigDecimal payableDues) {
		this.payableDues = payableDues;
	}
	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	
	
	
	
	
	
	
	
	
	
}
