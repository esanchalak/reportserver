package com.emanager.server.dashBoard.dataAccessObject;


public class AccountStatusVO {
	
	private int societyID;
	private String societyName;
	private int userID;
	private String memberName;
	private String reportDate;
	private int reportID;
	private String toEmail;
	private String bccEmail;
	private String note;
	private String status;
	private DashBoardAccountVO dbAccVO;
	private String fromDate;
	private String toDate;
	private String period;
	private int isTest;
	
	
	public String getBccEmail() {
		return bccEmail;
	}
	public void setBccEmail(String bccEmail) {
		this.bccEmail = bccEmail;
	}
	public DashBoardAccountVO getDbAccVO() {
		return dbAccVO;
	}
	public void setDbAccVO(DashBoardAccountVO dbAccVO) {
		this.dbAccVO = dbAccVO;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	public int getReportID() {
		return reportID;
	}
	public void setReportID(int reportID) {
		this.reportID = reportID;
	}
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getToEmail() {
		return toEmail;
	}
	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public int getIsTest() {
		return isTest;
	}
	public void setIsTest(int isTest) {
		this.isTest = isTest;
	}
	
	
	
	
	
	
	
	
	
	
}
