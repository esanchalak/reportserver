package com.emanager.server.dashBoard.dataAccessObject;

import java.math.BigDecimal;

public class DashBoardVO {
	
	private String societyId;
	private String societyName;
	private DashBoardAccountVO dbAccountVO;
	private DashBoardFacilityVO dbFacilityVO;
	private DashBoardMemberVO dbMemberVO;
	private DashBoardSocietyVO dbSocietyVO;
	private DashBoardTenantVO dbTenantVO;
	private DashBoardVehicleVO dbVehicleVO;
	
	
	public DashBoardAccountVO getDbAccountVO() {
		return dbAccountVO;
	}
	public void setDbAccountVO(DashBoardAccountVO dbAccountVO) {
		this.dbAccountVO = dbAccountVO;
	}
	public DashBoardFacilityVO getDbFacilityVO() {
		return dbFacilityVO;
	}
	public void setDbFacilityVO(DashBoardFacilityVO dbFacilityVO) {
		this.dbFacilityVO = dbFacilityVO;
	}
	public DashBoardMemberVO getDbMemberVO() {
		return dbMemberVO;
	}
	public void setDbMemberVO(DashBoardMemberVO dbMemberVO) {
		this.dbMemberVO = dbMemberVO;
	}
	public DashBoardSocietyVO getDbSocietyVO() {
		return dbSocietyVO;
	}
	public void setDbSocietyVO(DashBoardSocietyVO dbSocietyVO) {
		this.dbSocietyVO = dbSocietyVO;
	}
	public DashBoardTenantVO getDbTenantVO() {
		return dbTenantVO;
	}
	public void setDbTenantVO(DashBoardTenantVO dbTenantVO) {
		this.dbTenantVO = dbTenantVO;
	}
	public DashBoardVehicleVO getDbVehicleVO() {
		return dbVehicleVO;
	}
	public void setDbVehicleVO(DashBoardVehicleVO dbVehicleVO) {
		this.dbVehicleVO = dbVehicleVO;
	}
	public String getSocietyId() {
		return societyId;
	}
	public void setSocietyId(String societyId) {
		this.societyId = societyId;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	
	
	
	
}
