package com.emanager.server.dashBoard.dataAccessObject;

public class DashBoardSocietyVO {
	
	private String societyId;
	private String societyName;
	private int noOfunits;
	private int noOfBuildings;
	private int noSoldUnits;
	private int noUnsoldUnits;
	
	public int getNoOfBuildings() {
		return noOfBuildings;
	}
	public void setNoOfBuildings(int noOfBuildings) {
		this.noOfBuildings = noOfBuildings;
	}
	public int getNoOfunits() {
		return noOfunits;
	}
	public void setNoOfunits(int noOfunits) {
		this.noOfunits = noOfunits;
	}
	public int getNoSoldUnits() {
		return noSoldUnits;
	}
	public void setNoSoldUnits(int noSoldUnits) {
		this.noSoldUnits = noSoldUnits;
	}
	public int getNoUnsoldUnits() {
		return noUnsoldUnits;
	}
	public void setNoUnsoldUnits(int noUnsoldUnits) {
		this.noUnsoldUnits = noUnsoldUnits;
	}
	public String getSocietyId() {
		return societyId;
	}
	public void setSocietyId(String societyId) {
		this.societyId = societyId;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	
}
