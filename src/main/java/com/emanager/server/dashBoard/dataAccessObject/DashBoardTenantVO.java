package com.emanager.server.dashBoard.dataAccessObject;

public class DashBoardTenantVO {
	
	private String societyId;
	private String societyName;
	private int noOfTenants;
	private int noOfTenantsWithInfo;
	private int noOfTenantsWithoutInfo;
	private int noc;
	private int missingPhone;
	private int missingEmail;
	private int forRent;
	private int moveIn;
	private int moveOut;
	
	
	public int getForRent() {
		return forRent;
	}
	public void setForRent(int forRent) {
		this.forRent = forRent;
	}
	public int getMissingEmail() {
		return missingEmail;
	}
	public void setMissingEmail(int missingEmail) {
		this.missingEmail = missingEmail;
	}
	public int getMissingPhone() {
		return missingPhone;
	}
	public void setMissingPhone(int missingPhone) {
		this.missingPhone = missingPhone;
	}
	public int getMoveIn() {
		return moveIn;
	}
	public void setMoveIn(int moveIn) {
		this.moveIn = moveIn;
	}
	public int getMoveOut() {
		return moveOut;
	}
	public void setMoveOut(int moveOut) {
		this.moveOut = moveOut;
	}
	public int getNoc() {
		return noc;
	}
	public void setNoc(int noc) {
		this.noc = noc;
	}
	public int getNoOfTenants() {
		return noOfTenants;
	}
	public void setNoOfTenants(int noOfTenants) {
		this.noOfTenants = noOfTenants;
	}
	public String getSocietyId() {
		return societyId;
	}
	public void setSocietyId(String societyId) {
		this.societyId = societyId;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	public int getNoOfTenantsWithInfo() {
		return noOfTenantsWithInfo;
	}
	public void setNoOfTenantsWithInfo(int noOfTenantsWithInfo) {
		this.noOfTenantsWithInfo = noOfTenantsWithInfo;
	}
	public int getNoOfTenantsWithoutInfo() {
		return noOfTenantsWithoutInfo;
	}
	public void setNoOfTenantsWithoutInfo(int noOfTenantsWithoutInfo) {
		this.noOfTenantsWithoutInfo = noOfTenantsWithoutInfo;
	}
	
	
	
	
	
	
}
