package com.emanager.server.dashBoard.Services;

import javax.security.auth.login.AccountException;

import org.apache.log4j.Logger;

import com.emanager.server.dashBoard.Domain.AccountDashBoardDomain;
import com.emanager.server.dashBoard.Domain.DashBoardDomain;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardAccountVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardVO;


public class AccountDashBoardService {
	
	private static final Logger logger = Logger.getLogger(AccountDashBoardService.class);
	AccountDashBoardDomain accountDashBoardDomain;
	public DashBoardAccountVO getAccountsDashBoard(int societyId,String fromDate,String uptoDate){
	logger.debug("Entry : public DashBoardVO getAccountsDashBoard(int societyId,String fromDate,String uptoDate)");
	DashBoardAccountVO dbVO = new DashBoardAccountVO();
	try {
				
		dbVO=accountDashBoardDomain.getAccountsDashBoard(societyId,fromDate,uptoDate);
		
	} catch (Exception e) {
		logger.error("Exception ocurred at getAccountsDashBoard", e);
	}
	
		
	logger.debug("Exit : public DashBoardVO getAccountsDashBoard(int societyId,String fromDate,String uptoDate)");	
	return dbVO;
	}
	
	
	public DashBoardAccountVO getFinanceAccountsDashBoard(int orgID,String fromDate,String uptoDate){
		logger.debug("Entry : public DashBoardAccountVO getFinanceAccountsDashBoard(int orgID,String fromDate,String uptoDate)");
		DashBoardAccountVO dbVO = new DashBoardAccountVO();
		try {
					
			dbVO=accountDashBoardDomain.getFinanceAccountsDashBoard(orgID,fromDate,uptoDate);
			
		} catch (Exception e) {
			logger.error("Exception public DashBoardAccountVO getFinanceAccountsDashBoard(int orgID,String fromDate,String uptoDate)", e);
		}
		
			
		logger.debug("Exit : public DashBoardAccountVO getFinanceAccountsDashBoard(int orgID,String fromDate,String uptoDate)");	
		return dbVO;
		}
	
	public AccountDashBoardDomain getAccountDashBoardDomain() {
		return accountDashBoardDomain;
	}
	public void setAccountDashBoardDomain(
			AccountDashBoardDomain accountDashBoardDomain) {
		this.accountDashBoardDomain = accountDashBoardDomain;
	}
	
	

}