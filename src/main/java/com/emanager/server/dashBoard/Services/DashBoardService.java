package com.emanager.server.dashBoard.Services;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;

import com.emanager.server.dashBoard.Domain.DashBoardDomain;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardAccountVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardMemberVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardVO;

public class DashBoardService {
	DashBoardDomain dashBoardDomain;
	private static final Logger logger = Logger.getLogger(DashBoardService.class);
	
	public DashBoardVO getSocietyDashBoard(int societyId,String version){
	logger.debug("Entry : public DashBoardVO getSocietyDashBoard(int societyId)");
	DashBoardVO dbVO = new DashBoardVO();;
	try {
		
		
		dbVO=dashBoardDomain.getSocietyDashBoard(societyId,version);
	} catch (Exception e) {
		logger.error("Exception ocurred at getSocietyDashBoard", e);
	}
	
		
	logger.debug("Exit : public DashBoardVO getSocietyDashBoard(int societyId)");	
	return dbVO;
	}
	
	public DashBoardAccountVO getSocietyAccountStatus(int societyId,String fromDate,String toDate){
		logger.debug("Entry : public DashBoardVO getSocietyDashBoard(int societyId)");
		DashBoardAccountVO dbVO = new DashBoardAccountVO();;
		try {
			
			
			dbVO=dashBoardDomain.getSocietyAccountStatus(societyId,fromDate,toDate);
		} catch (Exception e) {
			logger.error("Exception ocurred at getSocietyDashBoard", e);
		}
		
			
		logger.debug("Exit : public DashBoardVO getSocietyDashBoard(int societyId)");	
		return dbVO;
	}
    
	public DashBoardMemberVO getMemberDetails(int societyId){
		logger.debug("Entry : public DashBoardMemberVO getMemberDetails(int societyId)");
		DashBoardMemberVO dbVO=new DashBoardMemberVO();
		try{
		
		dbVO=dashBoardDomain.getMemberDetails(societyId);
			
		}catch(EmptyResultDataAccessException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
		}catch(NullPointerException Ex){
			logger.info("No Data found getSocietyDashBoard "+Ex);
		}catch (Exception e) {
			logger.error("Exception : public DashBoardMemberVO getMemberDetails(int societyId)"+e);
		}
			
		logger.debug("Exit : public DashBoardMemberVO getMemberDetails(int societyId)");	
		return dbVO;
		}
	
	public DashBoardAccountVO getSuspenceEntryDetails(int societyId){
		logger.debug("Entry : public DashBoardAccountVO getSuspenceEntryDetails(int societyId)");
		DashBoardAccountVO dbVO=new DashBoardAccountVO();
		try{
			
			dbVO=dashBoardDomain.getSuspenceEntryDetails(societyId);
		
		}catch(EmptyResultDataAccessException Ex){
			logger.info("No Data found getSuspenceEntryDetails "+Ex);
		}catch(NullPointerException Ex){
			logger.info("No Data found getSuspenceEntryDetails "+Ex);
		}catch (Exception e) {
			logger.error("Exp getSuspenceEntryDetails"+e);
		}	
		logger.debug("Exit : public DashBoardAccountVO getSuspenceEntryDetails(int societyId)");	
		return dbVO;
		}
	
	public DashBoardAccountVO getPendingTransactionsCount(int societyId){
		logger.debug("Entry : public DashBoardAccountVO getPendingTransactionsCount(int societyId)");
		DashBoardAccountVO dbVO=new DashBoardAccountVO();
		try{
			
			dbVO=dashBoardDomain.getPendingTransactionsCount(societyId);
		
		}catch(EmptyResultDataAccessException Ex){
			logger.info("No Data found getPendingTransactionsCount "+Ex);
		}catch(NullPointerException Ex){
			logger.info("No Data found getPendingTransactionsCount "+Ex);
		}catch (Exception e) {
			logger.error("Exp getPendingTransactionsCount"+e);
		}	
		logger.debug("Exit : public DashBoardAccountVO getPendingTransactionsCount(int societyId)");	
		return dbVO;
		}
	
	public DashBoardDomain getDashBoardDomain() {
		return dashBoardDomain;
	}

	public void setDashBoardDomain(DashBoardDomain dashBoardDomain) {
		this.dashBoardDomain = dashBoardDomain;
	}
}