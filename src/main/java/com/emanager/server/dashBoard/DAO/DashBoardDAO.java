package com.emanager.server.dashBoard.DAO;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.emanager.server.dashBoard.dataAccessObject.DashBoardAccountVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardFacilityVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardMemberVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardSocietyVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardTenantVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardVehicleVO;
import com.emanager.server.society.valueObject.SocietyVO;

public class DashBoardDAO {
	
	   
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	private static final Logger logger = Logger.getLogger(DashBoardDAO.class);

	public int countTotalUnits(int societyId) {

		int i=0;

		try {
			logger.debug("Entry : public int countTotalUnits(int societyId)"+societyId);
		
		String strSQL = "select count(apt_name) as aptName from apartment_details a, building_details b where a.building_id=b.building_id and b.society_id=:societyId; "; 
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyId", societyId);
			
			//3. RowMapper.
			i =  namedParameterJdbcTemplate.queryForObject(strSQL,namedParameters,Integer.class);
			//i =  namedParameterJdbcTemplate.queryForInt(strSQL,namedParameters);

			
			logger.debug("Exit:	public DashBoardSocietyVO countTotalUnits(int societyId)");
		} catch (NullPointerException ex) {
			logger.info("No details found : "+ex);

		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);
		} catch (Exception ex) {
			logger.error("Exception in countTotalUnits : "+ex);

		}
		return i;

	}
	public int countTotalBuildings(int societyId) {

		int i=0;

		try {
			logger.debug("Entry : public int countTotalBuildings(int societyId)"+societyId);
		
		String strSQL = "select count(building_name) as buildingName from building_details b where b.society_id=:societyId; "; 
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyId", societyId);
			
			//3. RowMapper.
			i =  namedParameterJdbcTemplate.queryForObject(strSQL,namedParameters,Integer.class);

			//i =  namedParameterJdbcTemplate.queryForInt(strSQL,namedParameters);

			
			logger.debug("Exit:	public DashBoardSocietyVO countTotalBuildings(int societyId)");
		} catch (NullPointerException ex) {
			logger.info("No Data Found : "+ex);
		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);
			
		} catch (Exception ex) {
			logger.error("Exception in countTotalUnits : "+ex);

		}
		return i;

	}
	

	public DashBoardSocietyVO countTotalSoldUnSoldUnits(int societyId) {

		DashBoardSocietyVO dbVO=new DashBoardSocietyVO();

		try {
			logger.debug("Entry : public int countTotalSoldUnSoldUnits(int societyId)"+societyId);
		
		String strSQL = "SELECT SUM(CASE WHEN unit_sold_status = 'Sold' THEN 1 ELSE 0 END) AS sold,SUM(CASE WHEN unit_sold_status != 'Sold' THEN 1 ELSE 0 END) AS notSold,s.society_name,s.society_id FROM apartment_details a, building_details b,society_details s WHERE a.building_id=b.building_id AND b.society_id=:societyId and s.society_id=b.society_id ;"; 
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyId", societyId);
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  DashBoardSocietyVO dbVO = new DashBoardSocietyVO();
			            dbVO.setNoSoldUnits(rs.getInt("sold"));
			            dbVO.setNoUnsoldUnits(rs.getInt("notSold"));
			            dbVO.setSocietyId(rs.getString("society_id"));
			            dbVO.setSocietyName(rs.getString("society_name"));
			            return dbVO;
				}
			};
			

			dbVO =  (DashBoardSocietyVO) namedParameterJdbcTemplate.queryForObject(strSQL, namedParameters, RMapper);

			
			logger.debug("Exit:	public DashBoardSocietyVO countTotalSoldUnSoldUnits(int societyId)");
		} catch (NullPointerException ex) {
			logger.info("No details found : "+ex);
		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);
			
		} catch (Exception ex) {
			logger.error("Exception in countTotalUnits : "+ex);

		}
		return dbVO;

	}
	
	public DashBoardMemberVO countMembers(int societyId) {

		DashBoardMemberVO dbVO=new DashBoardMemberVO();

		try {
			logger.debug("Entry : public DashBoardMemberVO countMembers(int societyId)"+societyId);
		
		String strSQL = "SELECT SUM(CASE WHEN (TYPE = 'P' AND is_current=1) THEN 1 ELSE 0 END) AS pri,SUM(CASE WHEN ( TYPE = 'A' AND is_current=1) THEN 1 ELSE 0 END) AS asso, "+
						"SUM(CASE WHEN ((email = '' OR email IS NULL) AND TYPE='P' AND is_current=1) THEN 1 ELSE 0 END) AS email, "+
						"SUM(CASE WHEN ((mobile = '0000000000' OR mobile IS NULL) AND TYPE='P' AND a.unit_sold_status='Sold' AND is_current=1) THEN 1 ELSE 0 END) AS mobile FROM member_details m, apartment_details a,building_details b WHERE m.apt_id=a.apt_id AND a.building_id=b.building_id AND b.society_id=:societyId;"; 
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyId", societyId);
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  DashBoardMemberVO dbVO = new DashBoardMemberVO();
			            dbVO.setPrimaryMembers(rs.getInt("pri"));
			            dbVO.setAssoMembers(rs.getInt("asso"));
			            dbVO.setMissingEmail(rs.getInt("email"));
			            dbVO.setMissingPhone(rs.getInt("mobile"));
			            
			            return dbVO;
				}
			};
			

			dbVO =  (DashBoardMemberVO) namedParameterJdbcTemplate.queryForObject(strSQL, namedParameters, RMapper);

			
			logger.debug("Exit:	public DashBoardMemberVO countMembers(int societyId)");
		} catch (NullPointerException ex) {
			logger.info("No details found : "+ex);
		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);
			
		} catch (Exception ex) {
			logger.error("Exception in countMembers : "+ex);

		}
		return dbVO;

	}
	
	public int countNonNomineeMembers(int societyId) {

		int i=0;

		try {
			logger.debug("Entry : public int countNonNomineeMembers(int societyId)"+societyId);
		
		String strSQL = "SELECT COUNT(m.member_id) FROM (member_details m,apartment_details a,building_details b) LEFT JOIN nominee_details n ON m.member_id=n.member_id  WHERE n.member_id IS NULL AND m.apt_id=a.apt_id "+
						"AND a.building_id=b.building_id AND b.society_id=:societyId AND m.type='P' AND m.is_current='1'; "; 
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyId", societyId);
			
			//3. RowMapper.
			
			i =  namedParameterJdbcTemplate.queryForObject(strSQL,namedParameters,Integer.class);
			//i =  namedParameterJdbcTemplate.queryForInt(strSQL,namedParameters);

			
			logger.debug("Exit:	public int countNonNomineeMembers(int societyId)");
		} catch (NullPointerException ex) {
			logger.info("No details found : "+ex);
		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);
			
		} catch (Exception ex) {
			logger.error("Exception in countNonNomineeMembers : "+ex);

		}
		return i;

	}
	
	public int countSharCerteMissing(int societyId) {

		int i=0;

		try {
			logger.debug("Entry : public int countSharCerteMissing(int societyId)"+societyId);
		
		String strSQL = "SELECT COUNT(m.member_id) FROM (member_details m,apartment_details a,building_details b) LEFT JOIN share_cert_details n ON m.apt_id=n.apt_id  WHERE n.apt_id IS NULL AND m.apt_id=a.apt_id "+
						"AND a.building_id=b.building_id AND b.society_id=:societyId AND m.type='P' AND m.is_current='1'; "; 
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyId", societyId);
			
			//3. RowMapper.
			
			i =  namedParameterJdbcTemplate.queryForObject(strSQL,namedParameters,Integer.class);
			//i =  namedParameterJdbcTemplate.queryForInt(strSQL,namedParameters);

			
			logger.debug("Exit:	public int countSharCerteMissing(int societyId)");
		} catch (NullPointerException ex) {
			logger.info("No details found : "+ex);
		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);
			
		} catch (Exception ex) {
			logger.error("Exception in countNonNomineeMembers : "+ex);

		}
		return i;

	}
	
	
	public List countTenants(int societyId) {

		List tenantList=null;

		try {
			logger.debug("Entry : public int countTenants(int societyId)"+societyId);
		
		/*String strSQL = "SELECT * FROM renter_details r,member_details m,apartment_details a,building_details b WHERE a.apt_id=m.apt_id AND m.apt_id=r.apt_id "+
						"AND a.building_id=b.building_id AND b.society_id=:societyId AND r.is_current_renter=1 AND r.is_deleted=0  AND m.type='P' group by r.apt_id; "; */
		String strSQL= " SELECT * FROM apartment_details a,building_details b WHERE a.building_id=b.building_id and a.is_Rented=1 AND b.society_id=:societyId;";
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyId", societyId);
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  DashBoardTenantVO dbVO = new DashBoardTenantVO();
			            
			            return dbVO;
				}
			};
			

			tenantList =  namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);

			
			logger.debug("Exit:	public int countTenants(int societyId)"+tenantList.size());
		} catch (NullPointerException ex) {
			logger.info("No details found : "+ex);
		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);
			
		} catch (Exception ex) {
			logger.error("Exception in countTenants : "+ex);

		}
		return tenantList;

	}
	
	public DashBoardTenantVO countMissingTenants(int societyId) {

		DashBoardTenantVO dbVO=new DashBoardTenantVO();

		try {
			logger.debug("Entry : public DashBoardTenantVO countMissingTenants(int societyId)"+societyId);
		
		String strSQL = "SELECT SUM(r.is_current_renter ) AS rented, SUM(CASE WHEN ((r.email = '' OR r.email IS NULL)  AND is_current_renter=1) THEN 1 ELSE 0 END) AS email, "+ 
						"SUM(CASE WHEN ((r.mobile = '0000000000' OR r.mobile IS NULL) AND TYPE='P' AND is_current_renter=1) THEN 1 ELSE 0 END) AS mobile, "+
						" SUM(CASE WHEN ((a.available_for_rent = '1' OR a.available_for_rent IS NULL) AND TYPE='P' AND is_current_renter=1) THEN 1 ELSE 0 END) AS rented , "+
						"SUM(CASE WHEN ((r.is_noc_given = '0' OR r.mobile IS NULL) AND TYPE='P' AND is_current_renter=1) THEN 1 ELSE 0 END) AS noc FROM renter_details r,member_details m,apartment_details a,building_details b WHERE a.apt_id=m.apt_id AND m.apt_id=r.apt_id "+	 
						" AND a.building_id=b.building_id AND b.society_id=:societyId AND r.is_current_renter=1 and a.is_Rented=1  AND r.is_deleted=0 AND m.type='P';";
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyId", societyId);
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  DashBoardTenantVO dbVO = new DashBoardTenantVO();
					    dbVO.setNoOfTenantsWithInfo(rs.getInt("rented"));
			            dbVO.setNoc(rs.getInt("noc"));
			            dbVO.setForRent(rs.getInt("rented"));
			            dbVO.setMissingEmail(rs.getInt("email"));
			            dbVO.setMissingPhone(rs.getInt("mobile"));
			            return dbVO;
				}
			};
			

			dbVO =  (DashBoardTenantVO) namedParameterJdbcTemplate.queryForObject(strSQL, namedParameters, RMapper);

			
			logger.debug("Exit:	public DashBoardTenantVO countMissingTenants(int societyId)");
		} catch (NullPointerException ex) {
			logger.info("No details found : "+ex);

		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);
		} catch (Exception ex) {
			logger.error("Exception in countMissingTenants : "+ex);

		}
		return dbVO;

	}
	
	public DashBoardVehicleVO countVehicles(int societyId) {

		DashBoardVehicleVO dbVO=new DashBoardVehicleVO();

		try {
			logger.debug("Entry : public DashBoardVehicleVO countVehicles(int societyId)"+societyId);
		
		String strSQL = "SELECT SUM(CASE WHEN v.vehicle_type = '0'  THEN 1 ELSE 0 END) AS twov,SUM(CASE WHEN v.vehicle_type = '1'  THEN 1 ELSE 0 END) AS fourv " +
				        "FROM vehicle_details v,member_details m,apartment_details a,building_details b "+
				        "WHERE a.apt_id=m.apt_id AND a.building_id=b.building_id AND b.society_id=:societyId AND m.member_id=v.member_id AND m.type='P' AND m.is_current=1 ORDER BY m.apt_id;";
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyId", societyId);
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  DashBoardVehicleVO dbVO = new DashBoardVehicleVO();
			            dbVO.setTwoWheelers(rs.getInt("twov"));
			            dbVO.setFourWheelers(rs.getInt("fourv"));
			           
			            return dbVO;
				}
			};
			

			dbVO =  (DashBoardVehicleVO) namedParameterJdbcTemplate.queryForObject(strSQL, namedParameters, RMapper);

			
			logger.debug("Exit:	public DashBoardVehicleVO countVehicles(int societyId)");
		} catch (NullPointerException ex) {
			logger.info("No details found : "+ex);

		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);
		} catch (Exception ex) {
			logger.error("Exception in countVehicles : "+ex);

		}
		return dbVO;

	}
	
	public DashBoardAccountVO getAccountsDetails(int societyId) {

		DashBoardAccountVO dbVO=new DashBoardAccountVO();
		Date today=new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.MONTH, 1);
		int month = cal.get(Calendar.MONTH);
		int year  = cal.get(Calendar.YEAR);
		
		if(month<=3){
			year=year-1;
		}
		String startDate=year+"-04-01";
		
				
		
		try {
			logger.debug("Entry : public DashBoardAccountVO getAccountsDetails(int societyId)"+startDate);
		
		String strSQL = "SELECT SUM(CASE WHEN (tx_type = 'Cr'  AND is_deleted='0') THEN tx_ammount ELSE 0 END) AS credit, "+
						"SUM(CASE WHEN (tx_type = 'Db'  AND is_deleted='0' and ac.category_id!=10) THEN tx_ammount ELSE 0 END) AS debit, "+
						"SUM(CASE WHEN (tx_type = 'Cr'  AND is_deleted='0' AND s.acc_head_id=73) THEN 1 ELSE 0 END) AS suspense , "+
						"SUM(CASE WHEN ((bank_date = '' OR bank_date IS NULL)  AND is_deleted='0' AND acc_type='1') THEN 1 ELSE 0 END) AS reconcile FROM soc_tx_"+societyId+" s,account_category ac ,account_head ah WHERE tx_date BETWEEN :fromDate AND CURDATE() and s.acc_head_id=ah.acc_head_id and ah.category_id=ac.category_id   AND is_deleted='0';";
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyId", societyId);
			namedParameters.put("fromDate", startDate);
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  DashBoardAccountVO dbVO = new DashBoardAccountVO();
			            dbVO.setNotReconcilled(rs.getInt("reconcile"));
			            dbVO.setSumOfDeposits(rs.getBigDecimal("credit"));
			            dbVO.setSumOfExpenses(rs.getBigDecimal("debit"));
			            dbVO.setSuspenseEntries(rs.getInt("suspense"));
			           
			            return dbVO;
				}
			};
			

			dbVO =  (DashBoardAccountVO) namedParameterJdbcTemplate.queryForObject(strSQL, namedParameters, RMapper);

			
			logger.debug("Exit:	public DashBoardAccountVO getAccountsDetails(int societyId)");
		} catch (NullPointerException ex) {
			logger.info("No details found : "+ex);

		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);
		} catch (Exception ex) {
			logger.error("Exception in getAccountsDetails : "+ex);

		}
		return dbVO;

	}
	
	public DashBoardAccountVO getNewAccountsDetails(SocietyVO societyVO,String startDate,String uptoDate) {

		DashBoardAccountVO dbVO=new DashBoardAccountVO();
		
		
				
		
		try {
			logger.debug("Entry : public DashBoardAccountVO getAccountsDetails(int societyId)"+startDate);
		
		String strSQL = "SELECT SUM(CASE WHEN (tx_type = 'Receipt'  AND s.is_deleted='0') THEN s.amount ELSE 0 END) AS credit, "+
						" SUM(CASE WHEN (tx_type = 'Payment'  AND s.is_deleted='0' ) THEN s.amount ELSE 0 END) AS debit "+
						"  FROM account_transactions_"+societyVO.getDataZoneID()+" s WHERE (tx_date BETWEEN :fromDate AND :uptoDate) "+
						" AND s.is_deleted='0' and s.org_id=:orgID ;";
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("orgID", societyVO.getSocietyID());
			namedParameters.put("fromDate", startDate);
			namedParameters.put("uptoDate", uptoDate);
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  DashBoardAccountVO dbVO = new DashBoardAccountVO();
			            
			            dbVO.setSumOfDeposits(rs.getBigDecimal("credit"));
			            dbVO.setSumOfExpenses(rs.getBigDecimal("debit"));
			            
			            return dbVO;
				}
			};
			

			dbVO =  (DashBoardAccountVO) namedParameterJdbcTemplate.queryForObject(strSQL, namedParameters, RMapper);
			dbVO=getSusupenseEntry(societyVO,dbVO,startDate,uptoDate);
			
			logger.debug("Exit:	public DashBoardAccountVO getAccountsDetails(int societyId)");
		} catch (NullPointerException ex) {
			logger.info("No details found : "+ex);

		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);
		} catch (Exception ex) {
			logger.error("Exception in getAccountsDetails : "+ex);

		}
		return dbVO;

	}
	
	
	public DashBoardAccountVO getIncomeExpenseTotal(SocietyVO societyVO,int rootID,String startDate,String uptoDate) {

		DashBoardAccountVO dbVO=new DashBoardAccountVO();
		
		
				
		
		try {
			logger.debug("Entry : public DashBoardAccountVO getIncomeExpenseTotal(int societyId,String startDate,String uptoDate)"+startDate+uptoDate+rootID);
		
		String strSQL = "SELECT ac.group_name,al.ledger_name AS ledgerName,ale.*,SUM(ale.amount) AS txAmount ,ac.id AS groupID " +
					" FROM account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups ac "+
					" WHERE ats.tx_id=ale.tx_id AND ale.ledger_id=al.id AND al.sub_group_id=ac.id AND ac.root_id=:rootID AND ats.is_deleted=0 AND ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:orgID and  "+
					" ( ats.tx_date >= :fromDate AND ats.tx_date <= :uptoDate) GROUP BY ac.root_id order by ac.id;";
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("orgID", societyVO.getSocietyID());
			namedParameters.put("fromDate", startDate);
			namedParameters.put("uptoDate", uptoDate);
			namedParameters.put("rootID", rootID);
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  DashBoardAccountVO dbVO = new DashBoardAccountVO();
			            
			            dbVO.setTotal(rs.getBigDecimal("txAmount"));
			            
			            return dbVO;
				}
			};
			

			dbVO =  (DashBoardAccountVO) namedParameterJdbcTemplate.queryForObject(strSQL, namedParameters, RMapper);
		
			
			logger.debug("Exit:	public DashBoardAccountVO getIncomeExpenseTotal(int societyId,String startDate,String uptoDate)");
		} catch (EmptyResultDataAccessException ex) {
			logger.info("No details found : for Organization "+societyVO.getSocietyID()+" "+ex);
			dbVO.setTotal(BigDecimal.ZERO);
		
		
		} catch (NullPointerException ex) {
			logger.info("No details found : "+ex);

		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);
		} catch (Exception ex) {
			logger.error("Exception in getAccountsDetails : "+ex);

		}
		return dbVO;

	}
	
	public DashBoardAccountVO getSusupenseEntry(SocietyVO societyVO,DashBoardAccountVO dbVO,String fromDate,String uptoDate) {

		DashBoardAccountVO dbVONew=new DashBoardAccountVO();
	
		
				
		
		try {
			logger.debug("Entry : public DashBoardAccountVO getAccountsDetails(int societyId)"+fromDate);
		
		String strSQL = "SELECT  SUM(CASE WHEN (al.sub_group_id = 36  AND s.is_deleted='0' ) THEN 1 ELSE 0 END) AS suspense  ,"+ 
										" SUM(CASE WHEN (al.sub_group_id = 36  AND s.is_deleted='0' AND tx_type='Receipt' ) THEN 1 ELSE 0 END) AS susReceiptCnt ,  "+
										"SUM(CASE WHEN (al.sub_group_id = 36  AND s.is_deleted='0' AND tx_type='Receipt' ) THEN s.amount ELSE 0 END) AS sumReceipt , "+
										 " SUM(CASE WHEN (al.sub_group_id = 36  AND s.is_deleted='0' AND tx_type='Payment') THEN 1 ELSE 0 END) AS susPaymentCnt,  " +
										"SUM(CASE WHEN (al.sub_group_id = 36  AND s.is_deleted='0' AND tx_type='Payment' ) THEN s.amount ELSE 0 END) AS sumPayment ,"+
										" SUM(CASE WHEN ((bank_date = '' OR bank_date IS NULL)  AND s.is_deleted='0' AND al.sub_group_id=4) THEN 1 ELSE 0 END) AS reconcile FROM account_transactions_"+societyVO.getDataZoneID()+" s,account_ledger_entry_"+societyVO.getDataZoneID()+" ac ,account_ledgers_"+societyVO.getDataZoneID()+" al WHERE (tx_date BETWEEN :fromDate AND :uptoDate) AND s.org_id=ac.org_id and s.org_id=al.org_id and al.org_id=:orgID and ac.tx_id=s.tx_id AND ac.ledger_id=al.id "+
										" AND s.is_deleted='0';";
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("orgID", societyVO.getSocietyID());
			namedParameters.put("fromDate", fromDate);
			namedParameters.put("uptoDate", uptoDate);
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  DashBoardAccountVO dbVO = new DashBoardAccountVO();
					  dbVO.setNotReconcilled(rs.getInt("reconcile"));
			            dbVO.setSuspenseEntries(rs.getInt("suspense"));
			            dbVO.setSuspencePaymentCount(rs.getInt("susPaymentCnt"));
			            dbVO.setSuspencePaymentSum(rs.getBigDecimal("sumPayment"));
			            dbVO.setSuspenceReceiptCount(rs.getInt("susReceiptCnt"));
			            dbVO.setSuspenceReceiptSum(rs.getBigDecimal("sumReceipt"));
			           
			            return dbVO;
				}
			};
			

			dbVONew =  (DashBoardAccountVO) namedParameterJdbcTemplate.queryForObject(strSQL, namedParameters, RMapper);
			dbVO.setSuspenseEntries(dbVONew.getSuspenseEntries());
			dbVO.setNotReconcilled(dbVONew.getNotReconcilled());
			dbVO.setSuspencePaymentCount(dbVONew.getSuspencePaymentCount());
			dbVO.setSuspencePaymentSum(dbVONew.getSuspencePaymentSum());
			dbVO.setSuspenceReceiptCount(dbVONew.getSuspenceReceiptCount());
			dbVO.setSuspenceReceiptSum(dbVONew.getSuspenceReceiptSum());
			
			logger.debug("Exit:	public DashBoardAccountVO getAccountsDetails(int societyId)");
		} catch (NullPointerException ex) {
			logger.info("No details found : "+ex);
		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);
			
		} catch (Exception ex) {
			logger.error("Exception in getAccountsDetails : "+ex);

		}
		return dbVO;

	}
	
	public int getMissingBillsEntry(SocietyVO societyVO,String uptoDate) {

		int count=0;
		Date today=new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.MONTH, 1);
		int month = cal.get(Calendar.MONTH);
		int year  = cal.get(Calendar.YEAR);
		
		if(month<=3){
			year=year-1;
		}
		String startDate=year+"-04-01";
		
				
		
		try {
			logger.debug("Entry : public DashBoardAccountVO getMissingBillsEntry(int societyId)"+startDate);
		
		String strSQL = "SELECT COALESCE(SUM(s.tx_number = ''), 0) comments  FROM account_transactions_"+societyVO.getDataZoneID()+" s WHERE (tx_date BETWEEN :fromDate AND CURDATE()) AND s.is_deleted='0' AND ( s.tx_number='0' OR s.tx_number='') AND tx_date<:uptoDate AND s.tx_type='Payment' and s.org_id=:orgID;";
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("orgID", societyVO.getSocietyID());
			namedParameters.put("fromDate", startDate);
			namedParameters.put("uptoDate", uptoDate);
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  DashBoardAccountVO dbVO = new DashBoardAccountVO();
					  dbVO.setMissingEntries(rs.getInt("missing"));
			           
			            return dbVO;
				}
			};
			
			count =    namedParameterJdbcTemplate.queryForObject(strSQL, namedParameters,Integer.class);
			//count =    namedParameterJdbcTemplate.queryForInt(strSQL, namedParameters);
			
			logger.debug("Exit:	public DashBoardAccountVO getMissingBillsEntry(int societyId)");
		} catch (NullPointerException ex) {
			logger.info("No details found : "+ex);
		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);
			
		} catch (Exception ex) {
			logger.error("Exception in getMissingBillsEntry : "+ex);

		}
		return count;

	}
	
	public int getPendingTransactionsCount(SocietyVO societyVO) {
		int count=0;
	try {
			logger.debug("Entry : public int getPendingTransactionsCount(SocietyVO societyVO)");
			
		String strSQL = "SELECT count(*) as pending  FROM account_transactions_"+societyVO.getDataZoneID()+" s WHERE  s.is_deleted='4' and s.org_id=:orgID;";
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("orgID", societyVO.getSocietyID());
		
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  DashBoardAccountVO dbVO = new DashBoardAccountVO();
					  dbVO.setPendingTxCount(rs.getInt("pending"));
			           
			            return dbVO;
				}
			};
			
			count =    namedParameterJdbcTemplate.queryForObject(strSQL, namedParameters,Integer.class);
			//count =    namedParameterJdbcTemplate.queryForInt(strSQL, namedParameters);
			
			logger.debug("Exit:	public DashBoardAccountVO getPendingTransactionsCount(int societyId)");
		} catch (NullPointerException ex) {
			logger.info("No details found : "+ex);
		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);
			
		} catch (Exception ex) {
			logger.error("Exception in getMissingBillsEntry : "+ex);

		}
		return count;

	}
	
	public DashBoardAccountVO findLatestDate(SocietyVO societyVO,String type,String version,String uptoDate) {

		DashBoardAccountVO dbVO=new DashBoardAccountVO();
		Date today=new Date();
		int groupID=0;
		String accType="";
		String strCondition="";
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.MONTH, 1);
		int month = cal.get(Calendar.MONTH);
		int year  = cal.get(Calendar.YEAR);
		
		if(month<=3){
			year=year-1;
		}
		String startDate=year+"-04-01";
		
		if(type.equalsIgnoreCase("Bank")){
			strCondition= " AND al.sub_group_id=4";
			accType="1";
		}else if(type.equalsIgnoreCase("Cash")){
			accType="0";
			strCondition= " AND al.sub_group_id=5";
		}else if(type.equalsIgnoreCase("Status")){
			strCondition="";
		}
		
				
		
		try {
			logger.debug("Entry : public DashBoardAccountVO getAccountsDetails(int societyId)"+startDate);
		String strSQL="";
		
		 strSQL = "SELECT MAX(tx_date) AS dates ,al.* FROM account_transactions_"+societyVO.getDataZoneID()+" ac,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al WHERE ac.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:orgID and ac.tx_id=ale.tx_id AND ale.ledger_id=al.id AND tx_date<:uptoDate AND ac.is_deleted=0 "+strCondition+";";

		//strSQL = "SELECT MAX(tx_date) AS dates FROM soc_tx_"+societyId+" WHERE acc_type=:accType AND is_deleted='0';";
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("orgID", societyVO.getSocietyID());
			namedParameters.put("accType", accType);
			namedParameters.put("groupID", groupID);
			namedParameters.put("uptoDate", uptoDate);
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  DashBoardAccountVO dbVO = new DashBoardAccountVO();
			            dbVO.setLastChqTx(rs.getString("dates"));
			            return dbVO;
				}
			};
			

			dbVO =  (DashBoardAccountVO) namedParameterJdbcTemplate.queryForObject(strSQL, namedParameters, RMapper);

			
			logger.debug("Exit:	public DashBoardAccountVO getAccountsDetails(int societyId)");
		} catch (NullPointerException ex) {
			logger.info("No details found : "+ex);
		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);
			
		} catch (Exception ex) {
			logger.error("Exception in getAccountsDetails : "+ex);

		}
		return dbVO;

	}
	
	public DashBoardFacilityVO getContractDetails(int societyId) {

		DashBoardFacilityVO dbVO=new DashBoardFacilityVO();
				
		
		try {
			logger.debug("Entry : DashBoardFacilityVO getContractDetails(int societyId)"+societyId);
		
		String strSQL = "SELECT SUM(CASE WHEN (contract_status = '1' ) THEN 1 ELSE 0 END) AS current ,SUM(CASE WHEN (contract_status = '0'  ) THEN 1 ELSE 0 END) AS expired "+
                        " FROM assets_details,contract_details,contractor_details,assets_category_list WHERE assets_details.society_id=:societyId  AND assets_details.asset_id=contract_details.asset_id "+
	                    " AND contract_details.contractor_id=contractor_details.contractor_id "+
	                    " AND assets_category_list.asset_category_id=assets_details.asset_category_id ORDER BY contract_details.contract_id  DESC";
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyId", societyId);
			
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  DashBoardFacilityVO dbVO = new DashBoardFacilityVO();
			            dbVO.setValidContracts(rs.getInt("current"));
			            dbVO.setExpiredContracts(rs.getInt("expired"));
			           
			           
			            return dbVO;
				}
			};
			

			dbVO =  (DashBoardFacilityVO) namedParameterJdbcTemplate.queryForObject(strSQL, namedParameters, RMapper);

			
			logger.debug("Exit:	DashBoardFacilityVO getContractDetails(int societyId)");
		} catch (NullPointerException ex) {
			logger.info("No details found : "+ex);

		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);	
		} catch (Exception ex) {
			logger.error("Exception in getContractDetails : "+ex);

		}
		return dbVO;

	}
	
	public int getSoonExpiringContracts(int societyId) {

		int i=0;

		try {
			logger.debug("Entry : public int getSoonExpiringContracts(int societyId)"+societyId);
		
		String strSQL = "SELECT count(contract_details.contract_id) as cnt "+
				       " FROM assets_category_list,contract_details,contractor_details,assets_details "+
				       " WHERE contractor_details.contractor_id=contract_details.contractor_id AND assets_category_list.asset_category_id=assets_details.asset_category_id "+
				       " AND assets_details.asset_id=contract_details.asset_id AND contract_details.is_deleted='0' AND contract_end_date > NOW() " +
				       " AND TO_DAYS(contract_end_date)-TO_DAYS(CURDATE()) <=15   AND assets_details.society_id= :societyId "; 
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyId", societyId);
			
			//3. RowMapper.
			
			i =  namedParameterJdbcTemplate.queryForObject(strSQL,namedParameters,Integer.class);
			//i =  namedParameterJdbcTemplate.queryForInt(strSQL,namedParameters);

			
			logger.debug("Exit:	public int getSoonExpiringContracts(int societyId)"+i);
		} catch (NullPointerException ex) {
			logger.info("No details found : "+ex);

			
		} catch (Exception ex) {
			logger.error("Exception in getSoonExpiringContracts : "+ex);

		}
		return i;

	}
	
	/*public List getAfterContractExpiryList(int societyId) {
		List contractExpiryList=null;
		logger.debug("Entry : public List getAfterContractExpiryList(int societyId)"+ societyId);
		
		String query = " SELECT asset_unique_id,asset_name,assets_category_list.asset_category_id,asset_category_name,contract_details.contract_id,contract_end_date,contractor_name,MAX(contract_end_date) AS maxdt,cont_mobile " +
					   " FROM assets_category_list,contract_details,contractor_details,assets_details "
					 + " WHERE contractor_details.contractor_id=contract_details.contractor_id "
					 + " AND assets_category_list.asset_category_id=assets_details.asset_category_id "
					 + " AND assets_details.asset_id=contract_details.asset_id "
					 + " AND contract_details.is_deleted='0' "
					 + " AND assets_details.society_id= :societyId " 
					 + " GROUP BY assets_details.asset_id ";
		
		logger.debug("Query is  : " + query);

		Map nameparameter = new HashMap();
		nameparameter.put("societyId", societyId);
		try {
			RowMapper RMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					MemberVO memberVO = new MemberVO();

					memberVO.setAssetName(rs.getString("asset_name"));
					memberVO.setAssetCateName(rs.getString("asset_category_name"));
					memberVO.setContractorName(rs.getString("contractor_name"));
					memberVO.setContractEndDate(rs.getDate("contract_end_date"));
					memberVO.setMobile(rs.getString("cont_mobile"));
					memberVO.setCurrentDate(rs.getDate("maxdt"));

					return memberVO;

				}

			};

			contractExpiryList = namedParameterJdbcTemplate.query(query,nameparameter, RMapper);
			logger.debug("in function getContractExpiryList()");

			if (contractExpiryList == null && 0 == contractExpiryList.size()) {
				logger.debug("size Of the List : " + contractExpiryList.size());
				
			}
		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : "+ex);
		} catch (Exception e) {

			logger.info("No details found : "+e);
		}
		logger	.debug("Exit :  public List getAfterContractExpiryList(int societyId)");
		return contractExpiryList;

	}*/
	
	public int getCountOfAuditQueries(int societyId) {

		int i=0;

		try {
			logger.debug("Entry : public getCountOfAuditQueries(int societyId)"+societyId);
		
		String strSQL = "SELECT IFNULL( SUM( ROUND (  ( LENGTH(tag_values) - LENGTH( REPLACE ( tag_values, '#', '') )  ) / LENGTH('#')  ) ),0) AS counts FROM soc_tx_meta WHERE society_id=:societyID;";  
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyID", societyId);
			
			//3. RowMapper.
			i =  namedParameterJdbcTemplate.queryForObject(strSQL,namedParameters,Integer.class);
			//i =  namedParameterJdbcTemplate.queryForInt(strSQL,namedParameters);

			
			logger.debug("Exit:	public int getCountOfAuditQueries(int societyId)"+i);
		} catch (EmptyResultDataAccessException ex) {
			logger.info("No details found : "+ex);

			
		} catch (Exception ex) {
			logger.error("Exception in getCountOfAuditQueries(int societyId) : "+ex);

		}
		return i;

	}
	
	
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	
	
}
