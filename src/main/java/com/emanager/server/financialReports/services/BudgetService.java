package com.emanager.server.financialReports.services;

import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.financialReports.domainObject.BudgetDomain;
import com.emanager.server.financialReports.valueObject.BudgetDetailsVO;
import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.projects.valueObjects.ProjectDetailsVO;

public class BudgetService {
	
	BudgetDomain budgetDomain;
	Logger log=Logger.getLogger(BudgetService.class);
	
	//-----Add Budget Details -------//
	public int addBudgetDetails(BudgetDetailsVO budgetVO){
		log.debug("Entry : public int addBudgetDetails(BudgetDetailsVO budgetVO)");
		int success=0;
		try{
			
			success=budgetDomain.addBudgetDetails(budgetVO);			
			
		}catch(Exception e){
			log.error("Exception in adding budget details "+e);
		}
		
		
		log.debug("Exit : public int addBudgetDetails(BudgetDetailsVO budgetVO)");
		return success;
	}
	
	//-----Update Budget Details -------//
		public int updateBudgetDetails(BudgetDetailsVO budgetVO){
			log.debug("Entry : public int updateBudgetDetails(BudgetDetailsVO budgetVO)");
			int success=0;
			try{
				
				success=budgetDomain.updateBudgetDetails(budgetVO);			
				
			}catch(Exception e){
				log.error("Exception in updating budget details "+e);
			}
			
			
			log.debug("Exit : public int updateBudgetDetails(BudgetDetailsVO budgetVO)");
			return success;
		}
	
		//-----Delete Budget Details -------//
				public int deleteBudgetDetails(BudgetDetailsVO budgetVO){
					log.debug("Entry : public int deleteBudgetDetails(BudgetDetailsVO budgetVO)");
					int success=0;
					try{
						
						success=budgetDomain.deleteBudgetDetails(budgetVO);			
						
					}catch(Exception e){
						log.error("Exception in deleting budget details "+e);
					}
					
					
					log.debug("Exit : public int deleteBudgetDetails(BudgetDetailsVO budgetVO)");
					return success;
				}
		
				//-----Get Budget Details  List-------//
				public List getBudgetDetailsList(BudgetDetailsVO budgetVO){
					log.debug("Entry : public List getBudgetDetailsList(BudgetDetailsVO budgetVO)");
					List budgetList=null;
					try{
						
						budgetList=budgetDomain.getBudgetDetailsList(budgetVO);			
						
					}catch(Exception e){
						log.error("Exception in getting budget details "+e);
					}
					
					
					log.debug("Exit : public int getBudgetDetailsList(BudgetDetailsVO budgetVO)");
					return budgetList;
				}
				
				
				//-----Get Budget Details  -------//
				public BudgetDetailsVO getBudgetDetails(BudgetDetailsVO budgetVO){
					log.debug("Entry : public BudgetDetailsVO getBudgetDetails(BudgetDetailsVO budgetVO)");
					BudgetDetailsVO budgetDetailsVO=new BudgetDetailsVO();
					try{
						
						budgetDetailsVO=budgetDomain.getBudgetDetails(budgetVO);			
						
					}catch(Exception e){
						log.error("Exception in getting budget details "+e);
					}
					
					
					log.debug("Exit : public BudgetDetailsVO getBudgetDetails(BudgetDetailsVO budgetVO)");
					return budgetDetailsVO;
				}
				
				
				//-----Add Budget Item Details -------//
				public int addBudgetItemDetails(BudgetDetailsVO budgetVO){
					log.debug("Entry : public int addBudgetItemDetails(BudgetDetailsVO budgetVO)");
					int success=0;
					try{
						
						success=budgetDomain.addBudgetItemDetails(budgetVO);			
						
					}catch(Exception e){
						log.error("Exception in adding budget details "+e);
					}
					
					
					log.debug("Exit : public int addBudgetItemDetails(BudgetDetailsVO budgetVO)");
					return success;
				}
				
				//-----Add bulk Budget Item Details -------//
				public int addBulkBudgetItemDetails(List budgetList){
					log.debug("Entry : public int addBulkBudgetItemDetails(List budgetList)");
					int success=0;
					try{
						
						success=budgetDomain.addBulkBudgetItemDetails(budgetList);			
						
					}catch(Exception e){
						log.error("Exception in adding budget details "+e);
					}
					
					
					log.debug("Exit : public int addBulkBudgetItemDetails(List budgetList)");
					return success;
				}
				
				//-----Update Budget Item Details -------//
					public int updateBudgetItemDetails(BudgetDetailsVO budgetVO){
						log.debug("Entry : public int updateBudgetItemDetails(BudgetDetailsVO budgetVO)");
						int success=0;
						try{
							
							success=budgetDomain.updateBudgetItemDetails(budgetVO);			
							
						}catch(Exception e){
							log.error("Exception in updating budget details "+e);
						}
						
						
						log.debug("Exit : public int updateBudgetItemDetails(BudgetDetailsVO budgetVO)");
						return success;
					}
				
					//-----Delete Budget Item Details -------//
					public int deleteBudgetItemDetails(BudgetDetailsVO budgetVO){
								log.debug("Entry : public int deleteBudgetItemDetails(BudgetDetailsVO budgetVO)");
								int success=0;
								try{
									
									success=budgetDomain.deleteBudgetItemDetails(budgetVO);			
									
								}catch(Exception e){
									log.error("Exception in deleting budget details "+e);
								}
								
								
								log.debug("Exit : public int deleteBudgetItemDetails(BudgetDetailsVO budgetVO)");
								return success;
							}
					
							//-----Get Budget Item Details  List-------//
							public List getBudgetItemDetailsList(BudgetDetailsVO budgetVO){
								log.debug("Entry : public List getBudgetItemDetailsList(BudgetDetailsVO budgetVO)");
								List budgetList=null;
								try{
									
									budgetList=budgetDomain.getBudgetItemDetailsList(budgetVO);			
									
								}catch(Exception e){
									log.error("Exception in getting budget details "+e);
								}
								
								
								log.debug("Exit : public int getBudgetItemDetailsList(BudgetDetailsVO budgetVO)");
								return budgetList;
							}
							
							
							//-----Get Budget Details  -------//
							public BudgetDetailsVO getBudgetItemDetails(BudgetDetailsVO budgetVO){
								log.debug("Entry : public BudgetDetailsVO getBudgetItemDetails(BudgetDetailsVO budgetVO)");
								BudgetDetailsVO budgetDetailsVO=new BudgetDetailsVO();
								try{
									
									budgetDetailsVO=budgetDomain.getBudgetItemDetails(budgetVO);			
									
								}catch(Exception e){
									log.error("Exception in getting budget details "+e);
								}
								
								
								log.debug("Exit : public BudgetDetailsVO getBudgetItemDetails(BudgetDetailsVO budgetVO)");
								return budgetDetailsVO;
							}
				
							//-----Add bulk Budget Item Details -------//
							public int addBulkBudgetItems(BudgetDetailsVO budgetVO){
								log.debug("Entry : public int addBulkBudgetItems(BudgetDetailsVO budgetVO)");
								int success=0;
								try{
									
									success=budgetDomain.addBulkBudgetItems(budgetVO);			
									
								}catch(Exception e){
									log.error("Exception in public int addBulkBudgetItems(BudgetDetailsVO budgetVO) "+e);
								}
								
								
								log.debug("Exit : public int addBulkBudgetItems(BudgetDetailsVO budgetVO)");
								return success;
							}
							
							
							//================ Get Budgeted profitLoss report=========//
							public ReportVO getBudgetedProfitLossReport(int orgID,String fromDate,String toDate){
								ReportVO rptVO =new ReportVO();
								log.debug("Entry : public ReportVO getBudgetedProfitLossReport(int orgID,String fromDate,String toDate)");
								
								
								rptVO=budgetDomain.getBudgetedProfitLossReport(orgID, fromDate, toDate);
								
								
								log.debug("Exit : public ReportVO getBudgetedProfitLossReport(int orgID,String fromDate,String toDate)");
								return rptVO;
							}
							
							//================ Get Month wise Budget report=========//
							public ReportVO getBudgetedReport(BudgetDetailsVO budgetVO){
								ReportVO rptVO =new ReportVO();
								log.debug("Entry : public ReportVO getBudgetedReport(int orgID,String fromDate,String toDate)");
								
								
								rptVO=budgetDomain.getBudgetedReport(budgetVO);
								
								
								log.debug("Exit : public ReportVO getBudgetedReport(int orgID,String fromDate,String toDate)");
								return rptVO;
							}
							
							//================ Get Budget report for group =========//
							public BudgetDetailsVO getBudgetedReportForGroup(BudgetDetailsVO budgetVO){
								BudgetDetailsVO rptVO =new BudgetDetailsVO();
								log.debug("Entry : public ReportVO getBudgetedReportForGroup(int orgID,String fromDate,String toDate)");
								
								
								rptVO=budgetDomain.getBudgetDetailsForGroup(budgetVO);
								
								
								log.debug("Exit : public ReportVO getBudgetedReportForGroup(int orgID,String fromDate,String toDate)");
								return rptVO;
							}
							
							
							//================ Get Budgeted project/cost center report=========//
							public List getBudgetedProjectListWithDetails(int orgID,String fromDate,String toDate){
								log.debug("Entry :  public List getBudgetedProjectListWithDetails(int orgID,String fromDate,String toDate)");
								List projectList=null;
								
								projectList=budgetDomain.getBudgetedProjectListWithDetails(orgID,fromDate,toDate);
								
								log.debug("Exit : public List getBudgetedProjectListWithDetails(int orgID,String fromDate,String toDate)");
								return projectList;
								
							}
							
							
							//================ Get Budgeted project/cost center report=========//
							public ProjectDetailsVO getBudgetedProjectWithDetails(int orgID,String fromDate,String toDate,String projectAcronyms){
								log.debug("Entry :  public ProjectDetailsVO getBudgetedProjectWithDetails(int orgID,String fromDate,String toDate,String projectAcronyms)");
								ProjectDetailsVO projectVO=new ProjectDetailsVO();
								
								projectVO=budgetDomain.getBudgetedProjectWithDetails(orgID,fromDate,toDate,projectAcronyms);
								
								log.debug("Exit : public ProjectDetailsVO getBudgetedProjectWithDetails(int orgID,String fromDate,String toDate,String projectAcronyms)");
								return projectVO;
								
							}
							
							//================ Get Budgeted lineitem report=========//
							public List getBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate,String frequency){
								ReportVO rptVO =new ReportVO();
								List lineItemList=null;
								log.debug("Entry : public List getBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate,String frequency)");
								
								
								lineItemList=budgetDomain.getBudgetLineItemList(orgID, budgetID, groupID,fromDate,toDate,frequency);
								
								
								log.debug("Exit :public List getBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate,String frequency)");
								return lineItemList;
		

	                	}
	/**
	 * @param budgetDomain the budgetDomain to set
	 */
	public void setBudgetDomain(BudgetDomain budgetDomain) {
		this.budgetDomain = budgetDomain;
	}
	
	
	
	
	
	
	
	
	
}
