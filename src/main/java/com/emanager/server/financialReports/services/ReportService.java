package com.emanager.server.financialReports.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.BadSqlGrammarException;

import com.emanager.server.adminReports.valueObject.RptMorgageVO;
import com.emanager.server.adminReports.valueObject.RptVehicleVO;
import com.emanager.server.financialReports.domainObject.MMCContibutionDomain;
import com.emanager.server.financialReports.domainObject.MemberDueReportDomain;
import com.emanager.server.financialReports.domainObject.ReportDomain;
import com.emanager.server.financialReports.domainObject.TotalChargesReportDomain;
import com.emanager.server.financialReports.valueObject.MonthwiseChartVO;
import com.emanager.server.financialReports.valueObject.ReportDetailsVO;
import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.financialReports.valueObject.RptMonthYearVO;
import com.emanager.server.financialReports.valueObject.RptTransactionVO;
//import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.emanager.server.society.valueObject.SocietyVO;

public class ReportService 
{
	List reportList=null;
	Logger logger=Logger.getLogger(ReportService.class);
	ReportDomain reportDomain;
	MemberDueReportDomain memberDueReportDomain;
	MMCContibutionDomain mmcContributionDomain;
	TotalChargesReportDomain memberTotalChargesReportDomain;
	public List YearlyExpenseReport(int year,int intSocietyID) {
		try {
			logger.debug("Entry : public List YearlyExpenseReport(int year,int intSocietyID)");
			reportList = reportDomain.YearlyExpenseReport(year,intSocietyID);
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("Exception in  YearlyExpenseReport : " + ex);
		}
		logger.debug("Exit : public List YearlyExpenseReport(int year,int intSocietyID)");
		return reportList;
	}
	public List monthwiseExpenseReport(int year,int intSocietyID) {
		try {
			logger.debug("Entry:inside public List monthwiseExpenseReport(int year,int intSocietyID)");
			reportList = reportDomain.monthwiseExpenseReport(year, intSocietyID);
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("Exception" + ex);
		}
		logger.debug("Exit: public List monthwiseExpenseReport(int year,int intSocietyID)"+reportList.size());
		return reportList;
	}

	public List getMonthlyExpense(String month,int cmbyear,int monthindex,int intSocietyID)
	{
		try
		{
			logger.debug("Entry : public List getMonthlyExpense(String month,int cmbyear,int monthindex,int intSocietyID)");
			 reportList=reportDomain.getMonthlyReport(month, cmbyear, monthindex,intSocietyID);
		    logger.debug("Exit : public List getMonthlyExpense(String month,int cmbyear,int monthindex,int intSocietyID)");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.error("Exception in getMonthlyExpense : "+ex);
		}
		
		return reportList;
	}

	
	public List getMemberDue(int month,int year, int intSocietyID,String buildingId)
	{
		try
		{
			logger.debug("Entry : public List getMemberDue(int intSocietyID)");
			if((intSocietyID==15)||(intSocietyID==10)){
				reportList=memberTotalChargesReportDomain.getMemberDue(month, year, intSocietyID, buildingId);
			}else
		    reportList=memberDueReportDomain.getMemberDue(month,year,intSocietyID,buildingId);
		    logger.debug("Exit : public List getMemberDue(int intSocietyID)");
		}
		catch(NullPointerException e){
			logger.info("No Data available in memberdue");
		}
		catch(IllegalArgumentException ex)
		{
			reportList=null;
		
			logger.info("Exception in  : "+ex);
		}
		catch(Exception ex)
		{
			//ex.printStackTrace();
			logger.error("Exception in getMemberDue : "+ex);
		}
	
		return reportList;
		
	}
	
	
	public List getMemberDueNew(String uptoDate,int societyID,String buildingId,String groupType)
	{  List reportList=null;
		try
		{
			logger.debug("Entry : public List getMemberDue(int intSocietyID)");
			
		    reportList=memberDueReportDomain.getMemberDueNew(uptoDate,societyID,buildingId,groupType);
		    logger.debug("Exit : public List getMemberDue(int intSocietyID)");
		}
		catch(NullPointerException e){
			logger.info("No Data available in memberdue");
		}
		catch(IllegalArgumentException ex)
		{
			reportList=null;
		
			logger.info("Exception in  : "+ex);
		}
		catch(Exception ex)
		{
			//ex.printStackTrace();
			logger.error("Exception in getMemberDue : "+ex);
		}
	
		return reportList;
		
	}
	public List getMemberDueWithClosingBal(String uptoDate,int societyID,int buildingId)
	{   List dueMemberList=null;
		try
		{
			logger.debug("Entry : public List getMemberDue(int intSocietyID)");
			
			dueMemberList=memberDueReportDomain.getMemberDueWithClosingBal(uptoDate,societyID,buildingId);
		    
		    logger.debug("Exit : public List getMemberDue(int intSocietyID)");
		}
		catch(NullPointerException e){
			logger.info("No Data available in memberdue");
		}
		catch(IllegalArgumentException ex)
		{
			dueMemberList=null;
		
			logger.info("Exception in  : "+ex);
		}
		catch(Exception ex)
		{
			//ex.printStackTrace();
			logger.error("Exception in getMemberDue : "+ex);
		}
	
		return dueMemberList;
		
	}
	public List tenantDetailsRpt(int intSocietyID)
	{ 	try
		{
		logger.debug("Entry : public List tenantDetailsRpt(int intSocietyID) ");
		reportList=reportDomain.tenantDetailsRpt(intSocietyID);
			
		}catch(Exception ex)
		{
			logger.error("Exception in tenantDetailsRpt : "+ex);
		}
		
		logger.debug("Exit : public List tenantDetailsRpt(int intSocietyID) ");
		return reportList;
	}
	
	
	/*
	public List getCashInHand(int societyID,int year, String firstDate,String toDate){
		BigDecimal cashInHand = new BigDecimal("0.0");
		List accList=null;
		try {
		
		
			logger.debug("Entry :public BigDecimal getCashInHand(int id,String societyID,int year)");
			
		
			accList=reportDomain.getCashInHand(societyID, year,firstDate,toDate);
			
			
			
			logger.debug("Exit :public BigDecimal getCashInHand(int id,String societyID,int year)");
		} catch (Exception e) {
			
			logger.error("Exception : "+e);
		}
		return accList;
	}
	
	public List getBankBalance(int societyID,int year,String firstDate,String toDate){
		BigDecimal cashInHand = new BigDecimal("0.0");
		List accList=null;
		try {
		
		
			logger.debug("Entry :public BigDecimal getBankBalance(int id,String societyID,int year)");
			
		
			accList=reportDomain.getBankDetails(societyID, year,firstDate,toDate);
			
			
			
			logger.debug("Exit :public BigDecimal getBankBalance(int id,String societyID,int year)");
		} catch (Exception e) {
			
			logger.error("Exception : "+e);
		}
		return accList;
	}
	
	public List getCurrentCashBalNewSys(String societyID,String firstDate,String toDate,int groupID){
		BigDecimal cashInHand = new BigDecimal("0.0");
		List accList=null;
		try {
		
		
			logger.debug("Entry :public BigDecimal getCurrentCashBalNewSys(societyID, firstDate, toDate, groupID)");
			
		
			//accList=reportDomain.getCurrentBalNewSys(societyID, firstDate, toDate, groupID);
			
			
			
			logger.debug("Exit :public BigDecimal getCurrentBalNewSys(societyID, firstDate, toDate, groupID)"+accList.size());
		} catch (Exception e) {
			
			logger.error("Exception : "+e);
		}
		return accList;
	}
	
	public List getCurrentBankBalNewSys(String societyID,String firstDate,String toDate,int groupID){
		BigDecimal cashInHand = new BigDecimal("0.0");
		List accList=null;
		try {
		
		
			logger.debug("Entry :public BigDecimal getCurrentBankBalNewSys(societyID, firstDate, toDate, groupID)");
			
		
		//	accList=reportDomain.getCurrentBalNewSys(societyID, firstDate, toDate, groupID);
			
			
			
			logger.debug("Exit :public BigDecimal getCurrentBalNewSys(societyID, firstDate, toDate, groupID)"+accList.size());
		} catch (Exception e) {
			
			logger.error("Exception : "+e);
		}
		return accList;
	}*/
	
	public ReportVO getExpenseReport(int societyID,String fromDate,String uptoDate){
		ReportVO rptVO =new ReportVO();
		logger.debug("Entry : public ReportVO getExpenseReport(int societyID,String fromDate,String uptoDate)");
		
		
		rptVO=reportDomain.getExpenseReport(societyID, fromDate, uptoDate,0);
		
		
		logger.debug("Exit : public ReportVO getExpenseReport(int societyID,String fromDate,String uptoDate)");
		return rptVO;
	}
	
	
	
	public ReportVO getIncomeExpenseReport(int societyID,String fromDate,String uptoDate,int isConsolidated,String formatType){
		ReportVO rptVO =new ReportVO();
		logger.debug("Entry : public ReportVO getIncomeExpenseReport(int societyID,String fromDate,String uptoDate,int isConsolidated,String formatType)"+fromDate+uptoDate);
		
		
		rptVO=reportDomain.getIncomeExpenseReport(societyID, fromDate, uptoDate,isConsolidated,formatType);
		
		
		logger.debug("Exit : public ReportVO getIncomeExpenseReport(int societyID,String fromDate,String uptoDate,int isConsolidated,String formatType)");
		return rptVO;
	}
	
	public ReportVO getConsolidatedIncomeExpenseReport(int societyID,String fromDate,String uptoDate,int isConsolidated,String formatType){
		ReportVO rptVO =new ReportVO();
		logger.debug("Entry : public ReportVO getConsolidatedIncomeExpenseReport(int societyID,String fromDate,String uptoDate,int isConsolidated,String formatType)"+fromDate+uptoDate);
		
		
		rptVO=reportDomain.getIncomeExpenseReport(societyID, fromDate, uptoDate,1,formatType);
		
		
		logger.debug("Exit : public ReportVO getConsolidatedIncomeExpenseReport(int societyID,String fromDate,String uptoDate,int isConsolidated,String formatType)");
		return rptVO;
	}
	
	public ReportVO getProfitLossReport(int societyID,String fromDate,String uptoDate,String formatType){
		ReportVO rptVO =new ReportVO();
		logger.debug("Entry : public ReportVO getProfitLossReport(int societyID,String fromDate,String uptoDate,String formatType)"+fromDate+uptoDate);
		
		
		rptVO=reportDomain.getProfitLossReport(societyID, fromDate, uptoDate,0,formatType);
		
		
		logger.debug("Exit : public ReportVO getProfitLossReport(int societyID,String fromDate,String uptoDate,String formatType)");
		return rptVO;
	}
	
	//=-==========Print Profit Loss Report=================//
	public ReportVO getProfitLossReportForPrint(int societyID,String fromDate,String uptoDate,String formatType){
		ReportVO rptVO =new ReportVO();
		logger.debug("Entry : public ReportVO getProfitLossReportForPrint(int societyID,String fromDate,String uptoDate,String formatType)"+fromDate+uptoDate);
		
		
		rptVO=reportDomain.getProfitLossReportForPrint(societyID, fromDate, uptoDate,0,formatType);
		
		
		logger.debug("Exit : public ReportVO getProfitLossReportForPrint(int societyID,String fromDate,String uptoDate,String formatType)");
		return rptVO;
	}
	
	public ReportVO getConsolidatedBalanceSheetReport(int societyID,String fromDate,String uptoDate,String formatType){
		ReportVO rptVO =new ReportVO();
		logger.debug("Entry : public ReportVO getConsolidatedBalanceSheetReport(int societyID,String fromDate,String uptoDate,String formatType)"+fromDate+uptoDate);
		
		
		rptVO=reportDomain.getProfitLossReport(societyID, fromDate, uptoDate,1,formatType);
		
		
		logger.debug("Exit : public ReportVO getConsolidatedBalanceSheetReport(int societyID,String fromDate,String uptoDate,String formatType)");
		return rptVO;
	}

	public ReportVO getCashBasedReceiptPaymentReport(int societyID,String fromDate,String uptoDate,String rptType){
		ReportVO rptVO =new ReportVO();
		logger.debug("Entry : public ReportVO getCashBasedReceiptPaymentReport(int societyID,String fromDate,String uptoDate)"+fromDate+uptoDate);
		
		
		rptVO=reportDomain.getCashBasedReceiptPaymentReport(societyID, fromDate, uptoDate,rptType,0);
		
		
		logger.debug("Exit : public ReportVO getCashBasedReceiptPaymentReport(int societyID,String fromDate,String uptoDate)");
		return rptVO;
	}
	
	public ReportVO getIndASProfitLossReport(int societyID,String fromDate,String uptoDate,String formatType,int reportID){
		ReportVO rptVO =new ReportVO();
		logger.debug("Entry : public ReportVO getIndASProfitLossReport(int societyID,String fromDate,String uptoDate,String formatType)"+fromDate+uptoDate);
		
		
		rptVO=reportDomain.getIndASProfitLossReport(societyID, fromDate, uptoDate,0,formatType,reportID);
		
		
		logger.debug("Exit : public ReportVO getProfitLossReport(int societyID,String fromDate,String uptoDate,String formatType)");
		return rptVO;
	}
	
	
	public List getProfitLossDetailsForLabelReport(int orgID,String fromDate, String uptoDate,int labelID,int isConsolidated){
		List ledgerList=null;
		List accHeadListList=new ArrayList();
		try{
		logger.debug("Entry : public List getProfitLossDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated)"+labelID);
		
		accHeadListList=reportDomain.getProfitLossDetailsForLabelReport(orgID, fromDate, uptoDate, labelID,isConsolidated);
		
		
		logger.debug("Exit : public List getProfitLossDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated)");
		}catch (Exception e) {
			logger.error("Exception: public List getBalanceSheetDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated) "+e );
		}
		return accHeadListList;
	}
	
	public List getProfitLossDetailsForLabelReportPrint(int orgID,String fromDate, String uptoDate,int labelID,String labelName,int isConsolidated){
		List ledgerList=null;
		List accHeadListList=new ArrayList();
		try{
		logger.debug("Entry : public List getProfitLossDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated)"+labelID);
		
		accHeadListList=reportDomain.getProfitLossDetailsForLabelReportPrint(orgID, fromDate, uptoDate, labelID,labelName,isConsolidated);
		
		
		logger.debug("Exit : public List getProfitLossDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated)");
		}catch (Exception e) {
			logger.error("Exception: public List getBalanceSheetDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated) "+e );
		}
		return accHeadListList;
	}
	public List getCashBasedLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType){
			List accHeadListList=new ArrayList();
		
		try{
		logger.debug("Entry : public List getLedgerReport(int societyID,String fromDate, String uptoDate)"+fromDate+uptoDate);

		accHeadListList=reportDomain.getCashBasedLedgerReport(societyID, fromDate, uptoDate, groupID, txType,0);
		
		logger.debug("Exit : public List getLedgerReport(int societyID,String fromDate, String uptoDate)"+accHeadListList.size());
		}catch (Exception e) {
			logger.error("Exception: public List getLedgerReport(int societyID,String fromDate, String uptoDate) "+e );
		}
		return accHeadListList;
	}
	
	
	
	
	public List getConsolidatedCashBasedLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType){
		List accHeadListList=new ArrayList();
	
	try{
	logger.debug("Entry : public List getConsolidatedCashBasedLedgerReport(int societyID,String fromDate, String uptoDate)"+fromDate+uptoDate);

	accHeadListList=reportDomain.getCashBasedLedgerReport(societyID, fromDate, uptoDate, groupID, txType,1);
	
	logger.debug("Exit : public List getConsolidatedCashBasedLedgerReport(int societyID,String fromDate, String uptoDate)"+accHeadListList.size());
	}catch (Exception e) {
		logger.error("Exception: public List getConsolidatedCashBasedLedgerReport(int societyID,String fromDate, String uptoDate) "+e );
	}
	return accHeadListList;
}
	
	public ReportVO getProfitLossBalance(ReportVO reportVO){
		
		logger.debug("Entry : public ReportVO getProfitLossBalance(ReportVO reportVO)");
			

			try {
				
				reportVO=reportDomain.getProfitLossBalance(reportVO,"IE","S");


				
			} catch (Exception e) {
				logger.error("Exception occured while preparing getProfitLossBalance Report "+e);
			}
				
			
			logger.debug("Exit : public ReportVO getProfitLossBalance(ReportVO reportVO)");

			return reportVO;
		}
	
	
	 public ReportDetailsVO calculateProfitLossDetails(List incomeList,List expenseList,int societyID,String fromDate,String uptoDate,String bsOrTb,String formatType){
		 ReportDetailsVO rptVO=new ReportDetailsVO();
		logger.debug("Entry : public RptMonthYearVO calculateProfitLossDetails(List incomeList,List expenseList,int societyID,String fromDate,String uptoDate,String formatType)");
			

			try {
				if(bsOrTb.equalsIgnoreCase("bs")){
					rptVO=reportDomain.calculateProfitLossDetailsForBalanceSheet(incomeList, expenseList, societyID, fromDate, uptoDate,formatType);	
				}else if(bsOrTb.equalsIgnoreCase("tb")){
				rptVO=reportDomain.calculateProfitLossDetailsForTrialBalance(incomeList, expenseList, societyID, fromDate, uptoDate);
				}

				
			} catch (Exception e) {
				logger.error("Exception occured RptMonthYearVO calculateProfitLossDetails(List incomeList,List expenseList,int societyID,String fromDate,String uptoDate,String formatType) "+e);
			}
				
			
			logger.debug("Exit : public RptMonthYearVO calculateProfitLossDetails(List incomeList,List expenseList,int societyID,String fromDate,String uptoDate,String formatType)");

			return rptVO;
		}
		
	 public ReportVO convertReportObjectForPrinting(ReportVO reportVO){
		
		logger.debug("Entry : public ReportVO convertReportObjectForPrinting(ReportDetailsVO reportVO)");
			

			try {
				reportVO=reportDomain.convertReportObjectForPrinting(reportVO);

				
			} catch (Exception e) {
				logger.error("Exception occured public ReportVO convertReportObjectForPrinting(ReportDetailsVO reportVO) "+e);
			}
				
			
			logger.debug("Exit : public ReportDetailsVO convertReportObjectForPrinting(ReportDetailsVO reportVO)");

			return reportVO;
		}
	
	 
	 
	
	/*
	
	public List IncomeExpenditureReportCredit(int year, int societyID) {
		List incExpList=new ArrayList();
		try {
			logger.debug("Entry:Public List IncomeExpenditureReportCredit(String year, String id) and ReportService" + year );
			
			incExpList=reportDomain.IncomeExpenditureReportCredit(year, societyID);
		}
		catch (Exception ex) {
			logger.error("Exception in IncomeExpenditureReportCredit : " + ex);
		}
		logger.debug("Exit:Public List IncomeExpenditureReportCredit(String year, String id) and ReportService");
	return incExpList;

	}
	public List IncomeExpenditureReportDebit(int year, int id) {
		List incExpList=new ArrayList();
		try {
			logger.debug("Entry:Public List IncomeExpenditureReportDebit(String year, String id) and ReportService" + year );
			
			incExpList=reportDomain.IncomeExpenditureReportDebit(year, id);
		}
		catch (Exception ex) {
			logger.error("Exception in IncomeExpenditureReportDebit : " + ex);
		}
		logger.debug("Exit:Public List IncomeExpeIncomeExpenditureReportDebitnditureReport(String year, String id) and ReportService");
	return incExpList;

	}
	
	
	
	public List categoryWiseReportDetails(int year, int societyID,int categoryID,String type) {
		List incExpList=new ArrayList();
		try {
			logger.debug("Entry:Public List categoryWiseReportDetails(String year, String id) and ReportService" + year + categoryID);
			
			//incExpList=reportDomain.categoryWiseReportDetails(year, societyID,categoryID,type);
		}
		catch (Exception ex) {
			logger.error("Exception in categoryWiseReportDetails : " + ex);
		}
		logger.debug("Exit:Public List categoryWiseReportDetails(String year, String id) ");
	return incExpList;

	}
	
	public List getIncomeExpenditureReportCredit(String fromDate,String uptoDate,String societyID) {
		List incExpList=new ArrayList();
		try {
			logger.debug("Entry:public List monthlyIncomeExpenditureReportCredit(int year, int id, int month,String period)");
			
			//incExpList=reportDomain.monthlyIncomeExpenditureReportCredit(fromDate, uptoDate,societyID);
		}
		catch (Exception ex) {
			logger.error("Exception in monthlyIncomeExpenditureReportCredit : " + ex);
		}
		logger.debug("Exit:public List monthlyIncomeExpenditureReportCredit(int year, int id, int month,String period)");
	return incExpList;

	}
	
	public List getIncomeExpenditureReportDebit(String fromDate,String uptoDate,String societyID) {
		List incExpList=new ArrayList();
		try {
			logger.debug("Entry:public List monthlyIncomeExpenditureReportDebit(int year, int id, int month, String period)");
			
			//incExpList=reportDomain.monthlyIncomeExpenditureReportDebit(fromDate, uptoDate,societyID);
		}
		catch (Exception ex) {
			logger.error("Exception in monthlyIncomeExpenditureReportDebit : " + ex);
		}
		logger.debug("Exit:public List monthlyIncomeExpenditureReportDebit(int year, int id, int month)");
	return incExpList;

	}
	
	public List getMonthlyIncomeExpenditureReportDetailed(String firstDate,String lastDate ,int societyId,int groupID) {
		List incExpList=new ArrayList();
		try {
			logger.debug("Entry:public List getMonthlyIncomeExpenditureReportDetailed(String firstDate,String lastDate ,int societyId,int groupID)");
			
				incExpList = reportDomain.getMonthlyIncomeExpenditureReportDetailed(firstDate, lastDate, societyId, groupID);
			//	logger.info("###############################"+incExpList.size()+incExpList);
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getMonthlyIncExpDeb : " + ex);	
		}
		catch (Exception ex) {
			logger.error("Exception in getMonthlyIncomeExpenditureReportDetailed : " + ex);
		}
		logger.debug("Exit:public List getMonthlyIncomeExpenditureReportDetailed(String firstDate,String lastDate ,int societyId,int groupID)"+incExpList.size());
	return incExpList;

	}
	
	public List monthlycategoryWiseReportDetails(int year, int societyID, int categoryID,int monthIndex,String type,String period) {
		List incExpList=new ArrayList();
		try {
			logger.debug("Entry:	public List monthlycategoryWiseReportDetails(int year, int societyID, int categoryID,int monthIndex)" + year + categoryID);
			
			incExpList=reportDomain.monthlycategoryWiseReportDetails(year, societyID, categoryID, monthIndex,type,period);
		}
		catch (Exception ex) {
			logger.error("Exception in monthlycategoryWiseReportDetails : " + ex);
		}
		logger.debug("Exit:	public List monthlycategoryWiseReportDetails(int year, int societyID, int categoryID,int monthIndex) ");
	return incExpList;

	}*/
	
	/*public List getyearlyMaintainanceContibution(int year, int societyId,int buildingId,int month) {
		List incExpList = new ArrayList();
		try {
			logger.debug("Entry: public List getyearlyMaintainanceContibution(int year, int societyId,int buildingId)");
				
				incExpList = mmcContributionDomain.getyearlyMaintainanceContibution(year, societyId,buildingId,month);
			
		} catch (Exception ex) {
			logger.error("Exception in getyearlyMaintainanceContibution : " + ex);
		}
		logger
				.debug("Exit:public List getyearlyMaintainanceContibution(int year, int societyId,int buildingId)" +incExpList.size());
		return incExpList;
	}*/
	
	/*
	public List getFilteredTx(int year,String strSocietyID,String primaryID,int month){
		
		List listTransaction = null;

		try {
			logger.debug("Entry : public List getFilteredTx(String strSocietyID,String tableRelation,String primaryID)");
			
		

			listTransaction = mmcContributionDomain.getFilteredTx(year, strSocietyID, primaryID,month);
			
			logger.debug("outside query : " + listTransaction.size());
			logger.debug("Exit : public List getFilteredTx(String strSocietyID,String tableRelation,String primaryID)");
		} catch (Exception ex) {
			logger.error("Exception in getFilteredTx : "+ex);

		}
		return listTransaction;

	}*/
	/*
	public List expenseCategorywiseDetails(int year, int societyId, int categoryID,int monthIndex,String type,String period) {
		List expenseCategoryList = null;
		try {
			logger.debug("Entry:public List expenseCategorywiseDetails(int year, int societyId, int categoryID,int monthIndex)" + year + categoryID);
			
			expenseCategoryList=reportDomain.expenseCategorywiseDetails(year, societyId, categoryID, monthIndex, type, period);
		}
		catch (Exception ex) {
			logger.error("Exception in expenseCategorywiseDetails : " + ex);
		}
		logger.debug("Exit:	public List expenseCategorywiseDetails(int year, int societyId, int categoryID,int monthIndex) ");
	return expenseCategoryList;

	}*/
	
	public List getMembersTransactionSummary(int year, String memberID,String societyID) {
		List memberTxSummary = new ArrayList();
		try {
			logger.debug("Entry:public List getMembersTransactionSummary(int year, String memberID,String societyID)"
							+ year + memberID);
			
			//memberTxSummary = reportDomain.getMembersTransactionSummary(year, memberID, societyID);
			
		} catch (Exception ex) {
			logger.error("Exception in getMembersTransactionSummary : " + ex);
		}
		logger.debug("Exit:public List getMembersTransactionSummary(int year, String memberID,String societyID) " +memberTxSummary.size());
		return memberTxSummary;
	}
   /*
	public RptTransactionVO getLastTrasaction(String memberID, int societyID )
	{	 List rptMemberList=new ArrayList();
		 RptTransactionVO transactionVO=new RptTransactionVO();;
		try
		{
			logger.debug("Entry : public RptTransactionVO gelLastTrasaction(String memberID, String societyID )");
			
			
			transactionVO=memberDueReportDomain.getLastTrasaction(memberID, societyID);
		
			
		
			logger.debug("here "+transactionVO.getDueForMMC());
		}
		catch(Exception ex)
		{
			logger.error("Exception RptTransactionVO gelLastTrasaction(String memberID, String societyID ) : "+ex);
			
		}
		
		logger.debug("Exit :RptTransactionVO gelLastTrasaction(String memberID, String societyID )");
		return transactionVO;
	}
	*/
	/*public List getMemberListForContribution(String societyId,String buildingId,int year,String accountHeadId) {
		List memberList = new ArrayList();
		
		try {
			logger.debug("Entry:public List getMemberListForContribution(int societyId,int buildingId)"	 + societyId);
		
			memberList=reportDomain.getMemberListForContribution(societyId, buildingId,year,accountHeadId);
			
		
			
		} catch (Exception ex) {
			logger.error("Exception in getMemberListForContribution(int societyId,int buildingId) : " + ex);
		}
		logger
				.debug("Exit:Public List getMemberListForContribution(int societyId,int buildingId)" +memberList.size());
		return memberList;
	}*/
	
	public List getMemberDocs(String buildingID) {
		List memDocLst=new ArrayList();
		try {
			logger.debug("Entry:getMemberDocs(String buildingID)");
			
			memDocLst=reportDomain.getMemberDocs( buildingID);
		}
		catch (Exception ex) {
			logger.error("Exception in getMemberDocs(String buildingID) : " + ex);
		}
		logger.debug("Exit:Public List getMemberDocs(String buildingID)");
	return memDocLst;

	}
	
	

	public List getBankList(){
		
	    List bankList = null;

		try{		
		    logger.debug("Entry :  public List getBankList()" );
				    
			    bankList = reportDomain.getBankList();		
		    		
		    logger.debug("Exit :  public List getBankList()" );
		}catch(Exception Ex){			
			logger.error("Exception in getBankList() : "+Ex);
			
		}
		return bankList;
	}	
   public List searchMemberList(String strbuildingId){
		
	    List memberList = null;

		try{		
		    logger.debug("Entry : public List searchMemberList(String strbuildingId)" );
				    
			    memberList = reportDomain.searchMemberList(strbuildingId);		
		    		
		    logger.debug("Exit : public List searchMemberList(String strbuildingId)" );
		}catch(Exception Ex){			
			logger.error("Exception in searchMemberList : "+Ex);
			
		}
		return memberList;
	}
	
	
	
	public List getMorgageDetails(int societyId){
		List morgageFlatlst=new ArrayList();
		try {
			logger.debug("Entry:public List public List getMorgageDetails(String societyId)");
			
			morgageFlatlst=reportDomain.getMorgageDetails(societyId);
		}
		catch (Exception ex) {
			logger.error("Exception in getMorgageDetails : " + ex);
		}
		logger.debug("Exit:public List public List getMorgageDetails(String societyId)");
	return morgageFlatlst;

	}
	public List getMorgageDetailsForMember(int societyId,int memberID){
		List morgageFlatlst=new ArrayList();
		try {
			logger.debug("Entry:public List public List getMorgageDetailsForMember(String societyId,int memberID)");
			
			morgageFlatlst=reportDomain.getMorgageDetailsForMember(societyId, memberID);
		}
		catch (Exception ex) {
			logger.error("Exception in getMorgageDetailsForMember(String societyId,int memberID : " + ex);
		}
		logger.debug("Exit:public List public List getMorgageDetailsForMember(String societyId,int memberID)");
	return morgageFlatlst;

	}
	
	public int insertMoragageDetails(RptMorgageVO morgageVO) {

		int sucess = 0;  
		
		try{	
		    logger.debug("Entry : public int insertMoragageDetails(RptMorgageVO morgageVO)");
		
		    sucess = reportDomain.insertMoragageDetails(morgageVO);
						
		    logger.debug("Exit : public int insertMoragageDetails(RptMorgageVO morgageVO) ");

		}catch(Exception Ex){		
			Ex.printStackTrace();
			logger.error("Exception in insertMoragageDetails : "+Ex);		
		}
		return sucess;
	}
	
	public int updateMorgage(RptMorgageVO morgageVO,String morgId){
	      
		int sucess = 0;  
		
		try{	
		    logger.debug("Entry : public int updateMorgage(RptMorgageVO morgageVO,String morgId)");
		 
		     sucess = reportDomain.updateMorgage(morgageVO, morgId);
		    	
		    logger.debug("Exit : public int updateMorgage(RptMorgageVO morgageVO,String morgId) ");

		}catch(Exception Ex){				
			Ex.printStackTrace();
			logger.error("Exception in updateMorgage : "+Ex);		
		}
		return sucess;
	}
	
	  public int removMorgage(String morgageId){
			
			int sucess = 0;
			try{	
			    logger.debug("Entry : public int removMorgage(String morgageId)");
			 
			     sucess = reportDomain.removMorgage(morgageId);
			    	
			    logger.debug("Exit : public int removMorgage(String morgageId) ");

			}catch(Exception Ex){				
				Ex.printStackTrace();
				logger.error("Exception in removMorgage : "+Ex);		
			}
			return sucess;
		
	     }
	
	  public List getMorgageHistory(String apartmentID){
	 		
	    	 List morgageHistory=null;
	 		try{	
	 		    logger.debug("Entry : public List getMorgageHistory(String apartmentID)");
	 		 
	 		   morgageHistory = reportDomain.getMorgageHistory(apartmentID);
	 		    	
	 		    logger.debug("Exit : public List getMorgageHistory(String apartmentID) ");

	 		}catch(Exception Ex){				
	 			Ex.printStackTrace();
	 			logger.error("Exception in getMorgageHistory : "+Ex);		
	 		}
	 		return morgageHistory;
	 	
	      }
	
	  public int updateArrears(String arrears,String paidArrears,String arrearsComment,String aptID )
	  {
	  	int sucess=0;
	  	try
	  	{
	  		logger.debug("Entry : public int updateArrears(String paidArrears,String arrearsComment,String aptID )");
	  		
	  		sucess=reportDomain.updateArrears(arrears,paidArrears,arrearsComment,aptID );
	  		
	  		
	  	}
	  	catch(Exception ex)
	  	{
	  		logger.error("Exception in updateCreditDebitTx : "+ex);
	  		
	  	}
	  	
	  	logger.debug("Exit :public int updateArrears(String paidArrears,String arrearsComment,String aptID )");
	  	return sucess;
	  }
	  
	/*
	  public List getMemberLedgerList(String strbuildingId,int year,String strSoceityID){
          
          List memberList = null;

          try{       
              logger.debug("Entry : public List getMemberLedgerList(String strbuildingId,int year)" );
                     
                  memberList = reportDomain.getMemberLedgerList(strbuildingId,year,strSoceityID);       
                     
              logger.debug("Exit : public List getMemberLedgerList(String strbuildingId,int year)" );
          }catch(Exception Ex){           
              logger.error("Exception in getMemberLedgerList : "+Ex);
             
          }
          return memberList;
      }
   */
	  
	  public List getLedgerDueDetails(int societyID,String buildingID,String uptoDate)
		{
			try
			{
				logger.debug("Entry :  public List getLedgerDueDetails(String societyID,String buildingID,String uptoDate)");
				
			    reportList=memberDueReportDomain.getLedgerDueDetails(societyID, buildingID, uptoDate);
			    logger.debug("Exit :  public List getLedgerDueDetails(String societyID,String buildingID,String uptoDate)");
			}
			catch(NullPointerException e){
				logger.info("No Data available in getLedgerDueDetails");
			}
			catch(IllegalArgumentException ex)
			{
				reportList=null;
			
				logger.info("Exception in getLedgerDueDetails  : "+ex);
			}
			catch(Exception ex)
			{
				//ex.printStackTrace();
				logger.error("Exception in getLedgerDueDetails : "+ex);
			}
		
			return reportList;
			
		}
	    
	  public List getDocumentReport(int societyID,String buildingID){
			try
			{
				logger.debug("Entry :   public List getDocumentReport(String societyID,String buildingID)");
				
			    reportList=reportDomain.getDocumentReport(societyID, buildingID);
			    logger.debug("Exit :    public List getDocumentReport(String societyID,String buildingID)");
			}
			catch(NullPointerException e){
				logger.info("No Data available in getDocumentReport");
			}
			catch(IllegalArgumentException ex){
				reportList=null;			
				logger.info("Exception in getDocumentReport  : "+ex);
			}
			catch(Exception ex){
				//ex.printStackTrace();
				logger.error("Exception in getDocumentReport : "+ex);
			}
		
			return reportList;
			
		}
	  

	     public List getSingleDocumentReport(int societyID, int documentID){
	   			try	{
	   				logger.debug("Entry :  public List getSingleDocumentReport(int societyID, int documentID)");
	   				
	   			    reportList=reportDomain.getSingleDocumentReport(societyID, documentID);
	   			    logger.debug("Exit :  public List getSingleDocumentReport(int societyID, int documentID)");
	   			}
	   			catch(NullPointerException e){
	   				logger.info("No Data available in getSingleDocumentReport");
	   			}
	   			catch(IllegalArgumentException ex){
	   				reportList=null;   			
	   				logger.info("Exception in getSingleDocumentReport  : "+ex);
	   			}
	   			catch(Exception ex){
	   				//ex.printStackTrace();
	   				logger.error("Exception in getSingleDocumentReport : "+ex);
	   			}
	   		
	   			return reportList;
	   			
	   		}
	  
	  public List getDocumentSummaryReport(int societyID,String buildingID, int documentID){
			try
			{
				logger.debug("Entry : public List getDocumentSummaryReport(String societyID,String buildingID)");
				
			    reportList=reportDomain.getDocumentSummaryReport(societyID, buildingID, documentID);
			    logger.debug("Exit : public List getDocumentSummaryReport(String societyID,String buildingID)");
			}
			catch(NullPointerException e){
				logger.info("No Data available in getDocumentSummaryReport");
			}
			catch(IllegalArgumentException ex){
				reportList=null;			
				logger.info("Exception in getDocumentSummaryReport  : "+ex);
			}
			catch(Exception ex){//ex.printStackTrace();
				logger.error("Exception in getDocumentSummaryReport : "+ex);
			}
		
			return reportList;
			
		}
	  
	  public List getDocumentList(){
			try
			{
				logger.debug("Entry :  public List getDocumentList()");
				
			    reportList=reportDomain.getDocumentList();
			    logger.debug("Exit :  public List getDocumentList()");
			}
			catch(NullPointerException e){
				logger.info("No Data available in getDocumentList");
			}
			catch(IllegalArgumentException ex){
				reportList=null;			
				logger.info("Exception in getDocumentList  : "+ex);
			}
			catch(Exception ex){//ex.printStackTrace();
				logger.error("Exception in getDocumentList : "+ex);
			}
		
			return reportList;
			
		}
	  
	  
	  public List getDocumentList(String type){
			try
			{
				logger.debug("Entry :  public List getDocumentList(String type)");
				
			    reportList=reportDomain.getDocumentList(type);
			    logger.debug("Exit :  public List getDocumentList(String type)");
			}
			catch(NullPointerException e){
				logger.info("No Data available in getDocumentList");
			}
			catch(IllegalArgumentException ex){
				reportList=null;			
				logger.info("Exception in getDocumentList  : "+ex);
			}
			catch(Exception ex){//ex.printStackTrace();
				logger.error("Exception in getDocumentList : "+ex);
			}
		
			return reportList;
			
		}
	  
	  public List getDocumentStatusList(int documentID){
			try
			{
				logger.debug("Entry :  public List getDocumentStatusList(int documentID)");
				
			    reportList=reportDomain.getDocumentStatusList(documentID);
			    logger.debug("Exit :  public List getDocumentStatusList(int documentID)");
			}
			catch(NullPointerException e){
				logger.info("No Data available in getDocumentStatusList");
			}
			catch(IllegalArgumentException ex){
				reportList=null;			
				logger.info("Exception in getDocumentStatusList  : "+ex);
			}
			catch(Exception ex){//ex.printStackTrace();
				logger.error("Exception in getDocumentStatusList : "+ex);
			}
		
			return reportList;
			
		}
	  
	  public List getVehicleReport(int societyID,String buildingID){
			try
			{
				logger.debug("Entry :   public List getVehicleReport(String societyID,String buildingID)");
				
			    reportList=reportDomain.getVehicleReport(societyID, buildingID);
			    logger.debug("Exit :    public List getVehicleReport(String societyID,String buildingID)");
			}
			catch(NullPointerException e){
				logger.info("No Data available in getVehicleReport");
			}
			catch(IllegalArgumentException ex){
				reportList=null;			
				logger.info("Exception in getVehicleReport  : "+ex);
			}
			catch(Exception ex){
				//ex.printStackTrace();
				logger.error("Exception in getVehicleReport : "+ex);
			}
		
			return reportList;
			
		}
      
   public List getVehicleReportSummary(int societyID,String buildingID){
			try
			{
				logger.debug("Entry :   public List getVehicleReportSummary(String societyID,String buildingID)");
				
			    reportList=reportDomain.getVehicleReportSummary(societyID, buildingID);
			    logger.debug("Exit :    public List getVehicleReportSummary(String societyID,String buildingID)");
			}
			catch(NullPointerException e){
				logger.info("No Data available in getVehicleReportSummary");
			}
			catch(IllegalArgumentException ex){
				reportList=null;			
				logger.info("Exception in getVehicleReportSummary  : "+ex);
			}
			catch(Exception ex){
				//ex.printStackTrace();
				logger.error("Exception in getVehicleReportSummary : "+ex);
			}
		
			return reportList;
			
		}
   
   public RptVehicleVO getVehicleReportForAPI(int societyID)
	{   RptVehicleVO vehicleVO=new RptVehicleVO();
		try
		{
			logger.debug("Entry : public RptVehicleVO getVehicleReportForAPI(int societyID)");
			
			vehicleVO=reportDomain.getVehicleReportForAPI(societyID);
		    
		    logger.debug("Exit : public RptVehicleVO getVehicleReportForAPI(int intSocietyID)");
		}
		catch(Exception e){
			logger.info("Exception ");
		}
		return vehicleVO;
		}
   
   public List getReceivableList(String uptoDate,int societyID,int buildingId)
	{   List dueMemberList=null;
		try
		{
			logger.debug("Entry : public List getReceivableList(int intSocietyID)");
			
			dueMemberList=memberDueReportDomain.getReceivableList(uptoDate,societyID,buildingId);
		    
		    logger.debug("Exit : public List getReceivableList(int intSocietyID)");
		}
		catch(NullPointerException e){
			logger.info("No Data available in getReceivableList");
		}
		catch(IllegalArgumentException ex)
		{
			dueMemberList=null;
		
			logger.info("Exception in  : "+ex);
		}
		catch(Exception ex)
		{
			//ex.printStackTrace();
			logger.error("Exception in getMemberDue : "+ex);
		}
	
		return dueMemberList;
		
	}
   
   public List getReceivableListInBillFormat(int societyID,String fromDate,String uptoDate)
	{   List dueMemberList=null;
		try
		{
			logger.debug("Entry : public List getReceivableListInBillFormat(int intSocietyID)");
			
			dueMemberList=memberDueReportDomain.getReceivableListInBillFormat(societyID,fromDate,uptoDate);
		    
		    logger.debug("Exit : public List getReceivableListInBillFormat(int intSocietyID)");
		}
		catch(NullPointerException e){
			logger.info("No Data available in getReceivableList");
		}
		catch(IllegalArgumentException ex)
		{
			dueMemberList=null;
		
			logger.info("Exception in  : "+ex);
		}
		catch(Exception ex)
		{
			//ex.printStackTrace();
			logger.error("Exception in getMemberDue : "+ex);
		}
	
		return dueMemberList;
		
	}
   
   public List getReceivableListForAllTags(int societyID,String uptoDate)
  	{   List receivablesList=null;
  		
  			logger.debug("Entry : public List getReceivableListForAllTags(int intSocietyID)");
  			
  			receivablesList=reportDomain.getReceivableListForAllTags(societyID,uptoDate);
  		    
  		    logger.debug("Exit : public List getReceivableListForAllTags(int intSocietyID)");
  		  	
  		return receivablesList;
  		
  	}
   
   public List getReceivableListForSingleTags(int societyID,String uptoDate,String ledgerMeta)
 	{   List receivablesList=null;
 		
 			logger.debug("Entry : public List getReceivableListForSingleTags(int intSocietyID)");
 			
 			receivablesList=reportDomain.getReceivableListForSingleTags(societyID,uptoDate,ledgerMeta);
 		    
 		    logger.debug("Exit : public List getReceivableListForSingleTags(int intSocietyID)");
 		  	
 		return receivablesList;
 		
 	}
   
   public List getMonthWiseGroupReportForChart(int societyID,String fromDate,String uptoDate,String type)
 	{   
 	   List groupNameList=new ArrayList();
 		try
 		{
 			logger.debug("Entry : public List getMonthWiseGroupReportForChart(int intSocietyID)");
 			
 			groupNameList=reportDomain.getMonthWiseGroupReportForChart(societyID,fromDate,uptoDate,type);
 		    
 		    logger.debug("Exit : public List getMonthWiseGroupReportForChart(int intSocietyID)");
 		}
 		catch(Exception ex)
 		{
 			//ex.printStackTrace();
 			logger.error("Exception in getMonthWiseGroupReportForChart : "+ex);
 		}
 	
 		return groupNameList;
 		
 	}
   
   public List getMonthWiseReceiptPaymentGroupReportForChart(int societyID,String fromDate,String uptoDate,String type)
 	{   
 	   List groupNameList=new ArrayList();
 		try
 		{
 			logger.debug("Entry : public List getMonthWiseReceiptPaymentGroupReportForChart(int intSocietyID)");
 			
 			groupNameList=reportDomain.getMonthWiseReceiptPaymentGroupReportForChart(societyID,fromDate,uptoDate,type,0);
 		    
 		    logger.debug("Exit : public List getMonthWiseReceiptPaymentGroupReportForChart(int intSocietyID)");
 		}
 		catch(Exception ex)
 		{
 			//ex.printStackTrace();
 			logger.error("Exception in getMonthWiseReceiptPaymentGroupReportForChart : "+ex);
 		}
 	
 		return groupNameList;
 		
 	}
   
   public MonthwiseChartVO getIncomeExpenseReportForChart(int societyID,String fromDate,String uptoDate)
	{   
	   MonthwiseChartVO reportVO=null;
		try
		{
			logger.debug("Entry : public List getIncomeExpenseReportForChart(int intSocietyID)");
			
			reportVO=reportDomain.getIncomeExpenseReportForChart(societyID,fromDate,uptoDate);
		    
		    logger.debug("Exit : public List getIncomeExpenseReportForChart(int intSocietyID)");
		}
		catch(Exception ex)
		{
			//ex.printStackTrace();
			logger.error("Exception in getIncomeExpenseReportForChart : "+ex);
		}
	
		return reportVO;
		
	}
   
   public MonthwiseChartVO getReceiptPaymentReportForChart(int societyID,String fromDate,String uptoDate,String rptType)
  	{   
  	   MonthwiseChartVO reportVO=null;
  		try
  		{
  			logger.debug("Entry : public List getReceiptPaymentReportForChart(int intSocietyID)");
  			
  			reportVO=reportDomain.getReceiptPaymentReportForChart(societyID,fromDate,uptoDate,rptType,0);
  		    
  		    logger.debug("Exit : public List getReceiptPaymentReportForChart(int intSocietyID)");
  		}
  		catch(Exception ex)
  		{
  			//ex.printStackTrace();
  			logger.error("Exception in getReceiptPaymentReportForChart : "+ex);
  		}
  	
  		return reportVO;
  		
  	}
   
   public MonthwiseChartVO getMonthWiseBreakUpReportForReceivables(int societyID,String fromDate,String uptoDate)
  	{   
  	   MonthwiseChartVO reportVO=null;
  		
  			logger.debug("Entry : public List getMonthWiseBreakUpReportForReceivables(int intSocietyID)");
  			
  			reportVO=reportDomain.getMonthWiseBreakUpReportForReceivables(societyID,fromDate,uptoDate);
  		    
  		    logger.debug("Exit : public List getMonthWiseBreakUpReportForReceivables(int intSocietyID)");
  		
  	
  		return reportVO;
  		
  	}
   
   public MonthwiseChartVO getConsolidatedReceiptPaymentReportForChart(int societyID,String fromDate,String uptoDate,String rptType)
 	{   
 	   MonthwiseChartVO reportVO=null;
 		try
 		{
 			logger.debug("Entry : public List getConsolidatedReceiptPaymentReportForChart(int intSocietyID)");
 			
 			reportVO=reportDomain.getReceiptPaymentReportForChart(societyID,fromDate,uptoDate,rptType,1);
 		    
 		    logger.debug("Exit : public List getConsolidatedReceiptPaymentReportForChart(int intSocietyID)");
 		}
 		catch(Exception ex)
 		{
 			//ex.printStackTrace();
 			logger.error("Exception in getConsolidatedReceiptPaymentReportForChart : "+ex);
 		}
 	
 		return reportVO;
 		
 	}
   
	public ReportDomain getReportDomain() {
		return reportDomain;
	}

	public void setReportDomain(ReportDomain reportDomain) {
		this.reportDomain = reportDomain;
	}
	public MemberDueReportDomain getMemberDueReportDomain() {
		return memberDueReportDomain;
	}
	public void setMemberDueReportDomain(MemberDueReportDomain memberDueReportDomain) {
		this.memberDueReportDomain = memberDueReportDomain;
	}
	public MMCContibutionDomain getMmcContributionDomain() {
		return mmcContributionDomain;
	}
	public void setMmcContributionDomain(MMCContibutionDomain mmcContributionDomain) {
		this.mmcContributionDomain = mmcContributionDomain;
	}
	public TotalChargesReportDomain getMemberTotalChargesReportDomain() {
		return memberTotalChargesReportDomain;
	}
	public void setMemberTotalChargesReportDomain(
			TotalChargesReportDomain memberTotalChargesReportDomain) {
		this.memberTotalChargesReportDomain = memberTotalChargesReportDomain;
	}
	

}
