package com.emanager.server.financialReports.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.dashBoard.dataAccessObject.AccountStatusVO;
import com.emanager.server.financialReports.domainObject.AccountingStatusDomain;

public class AccountingStatusService {

	private static final Logger log=Logger.getLogger(AccountingStatusService.class);
	AccountingStatusDomain accountingStatusRptDomain;
	
	
	public List getAccountingStatusReport(int userID,int appID,String fromDate,String toDate){
		log.debug("Entry : public List getAccountingStatusReport(int userID,List societyList,String fromDate,String uptoDate)");
		List accountingStsList=null;
		
		accountingStsList=accountingStatusRptDomain.getAccountingStatusReport( userID,appID, fromDate, toDate);
		
		
		log.debug("Exit : public List getAccountingStatusReport(int userID,List societyList,String fromDate,String uptoDate)");
		return accountingStsList;
		
	}
	
	public int sendAccountStatusReport(AccountStatusVO acStsVO){
		log.debug("Entry : public int sendAccountStatusReport(AccountStatusVO acStsVO)");
		int success=0;
		
		success=accountingStatusRptDomain.sendAccountStatusReport( acStsVO);
		
		
		log.debug("Exit : public int sendAccountStatusReport(AccountStatusVO acStsVO)");
		return success;
		
	}

	
	//==================Get pending transaction report for all orgs Created 06-05-21===========//
	
	public List getPendingApprovalTxReport(){
		log.debug("Entry : public int getPendingApprovalTxReport()");
		List societyList=new ArrayList<>();
		
		societyList=accountingStatusRptDomain.getPendingApprovalTxReport();
		
		
		log.debug("Exit : public int getPendingApprovalTxReport()");
		return societyList;
		
	}

	public void setAccountingStatusRptDomain(
			AccountingStatusDomain accountingStatusRptDomain) {
		this.accountingStatusRptDomain = accountingStatusRptDomain;
	}


	
	
	
}
