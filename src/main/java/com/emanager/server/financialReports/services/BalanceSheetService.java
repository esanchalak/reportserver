package com.emanager.server.financialReports.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.financialReports.domainObject.BalanceSheetDomain;
import com.emanager.server.financialReports.valueObject.ReportVO;

public class BalanceSheetService {
	BalanceSheetDomain balanceSheetDomain;
	Logger logger=Logger.getLogger(BalanceSheetService.class);

	
	public ReportVO getBalanceSheet(int societyID,String fromDate,String uptoDate){
		ReportVO rptVO =new ReportVO();
		List balanceSheetList=new ArrayList();
		logger.debug("Entry : public ReportVO getBalanceSheet(int societyID,String fromDate,String uptoDate)");
		
		
		rptVO=balanceSheetDomain.getBalanceSheet(societyID, fromDate, uptoDate);
		
		
		logger.debug("Exit : public ReportVO getBalanceSheet(int societyID,String fromDate,String uptoDate)"+balanceSheetList.size());
		return rptVO;
	}
	
	public ReportVO getConsolidatedBalanceSheetReport(int societyID,String fromDate,String uptoDate,String formatType){
		ReportVO rptVO =new ReportVO();
		List balanceSheetList=new ArrayList();
		logger.debug("Entry : public ReportVO getConsolidatedBalanceSheetReport(int societyID,String fromDate,String uptoDate,String formatType)");
		
		
		rptVO=balanceSheetDomain.getConsolidatedBalanceSheetReport(societyID, fromDate, uptoDate,formatType);
		
		
		logger.debug("Exit : public ReportVO getConsolidatedBalanceSheetReport(int societyID,String fromDate,String uptoDate,String formatType)"+balanceSheetList.size());
		return rptVO;
	}

	
	public ReportVO printBalanceSheet(int societyID,String fromDate,String uptoDate,String formatType){
		ReportVO rptVO =new ReportVO();
		List balanceSheetList=new ArrayList();
		logger.debug("Entry : public ReportVO printBalanceSheet(int societyID,String fromDate,String uptoDate,String formatType)");
		
		
		rptVO=balanceSheetDomain.printBalanceSheet(societyID, fromDate, uptoDate,formatType);
		
		
		logger.debug("Exit : public ReportVO printBalanceSheet(int societyID,String fromDate,String uptoDate,String formatType)"+balanceSheetList.size());
		return rptVO;
	}
	
	
	public ReportVO getIndASBalanceSheetReport(int societyID,String fromDate,String uptoDate,int reportID){
		ReportVO rptVO =new ReportVO();
		List balanceSheetList=new ArrayList();
		logger.debug("Entry : public ReportVO getIndASBalanceSheetReport(int societyID,String fromDate,String uptoDate)");
		
		
		rptVO=balanceSheetDomain.getIndASBalanceSheetReport(societyID, fromDate, uptoDate,reportID);
		
		
		logger.debug("Exit : public ReportVO getIndASBalanceSheetReport(int societyID,String fromDate,String uptoDate)"+balanceSheetList.size());
		return rptVO;
	}
	
	public List getBalanceSheetDetailsForLabelReport(int orgID,String fromDate, String uptoDate,int labelID,int isConsolidated){
		List ledgerList=null;
		List accHeadListList=new ArrayList();
		try{
		logger.debug("Entry : public List getBalanceSheetDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated)"+labelID);
		
		accHeadListList=balanceSheetDomain.getBalanceSheetDetailsForLabelReport(orgID, fromDate, uptoDate, labelID,isConsolidated);
		
		
		logger.debug("Exit : public List getBalanceSheetDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated)");
		}catch (Exception e) {
			logger.error("Exception: public List getBalanceSheetDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated) "+e );
		}
		return accHeadListList;
	}

	public BalanceSheetDomain getBalanceSheetDomain() {
		return balanceSheetDomain;
	}


	public void setBalanceSheetDomain(BalanceSheetDomain balanceSheetDomain) {
		this.balanceSheetDomain = balanceSheetDomain;
	}


	
	
	
}
