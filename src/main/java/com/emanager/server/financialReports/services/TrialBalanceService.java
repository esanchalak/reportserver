package com.emanager.server.financialReports.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.financialReports.domainObject.ReportDomain;
import com.emanager.server.financialReports.domainObject.TrialBalanceDomain;

public class TrialBalanceService {
	
	ReportDomain reportDomain;
	TrialBalanceDomain trialBalanceDomain;
	Logger log=Logger.getLogger(TrialBalanceService.class);
	
	
	/*This method is used to get trialBalnce report*/
	public List getTrialBalanceReport(int societyID,String fromDate, String uptoDate){
		
		List<AccountHeadVO> accHeadListList=new ArrayList<AccountHeadVO>();
		try{
		log.debug("Entry : public List getTrialBalanceReport(String societyID,String fromDate, String uptoDate)");
		
		accHeadListList=trialBalanceDomain.getTrialBalanceReport(societyID, fromDate, uptoDate);
		
		log.debug("Exit : public List getTrialBalanceReport(String societyID,String fromDate, String uptoDate)"+accHeadListList.size());
		}catch (Exception e) {
			log.error("Exception: public List getTrialBalanceReport(String societyID,String fromDate, String uptoDate) "+e );
		}
		return accHeadListList;
	}
	
	
	/*This method is used to get consolidated trialBalnce report*/
	public List getConsolidatedTrialBalanceReport(int societyID,String fromDate, String uptoDate){
		
		List<AccountHeadVO> accHeadListList=new ArrayList<AccountHeadVO>();
		try{
		log.debug("Entry : public List getConsolidatedTrialBalanceReport(String societyID,String fromDate, String uptoDate)");
		
		accHeadListList=trialBalanceDomain.getConsolidatedTrialBalanceReport(societyID, fromDate, uptoDate);
		
		log.debug("Exit : public List getConsolidatedTrialBalanceReport(String societyID,String fromDate, String uptoDate)"+accHeadListList.size());
		}catch (Exception e) {
			log.error("Exception: public List getConsolidatedTrialBalanceReport(String societyID,String fromDate, String uptoDate) "+e );
		}
		return accHeadListList;
	}
	
	
	
	public List getLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,int isConsolidated){
		List ledgerList=null;
		List accHeadListList=new ArrayList();
		try{
		log.debug("Entry : public List getLedgerReport(String societyID,String fromDate, String uptoDate,int isConsolidated)"+groupID);
		
		accHeadListList=trialBalanceDomain.getLedgerReport(societyID, fromDate, uptoDate, groupID,isConsolidated);
		
		
		log.debug("Exit : public List getLedgerReport(String societyID,String fromDate, String uptoDate,int isConsolidated)");
		}catch (Exception e) {
			log.error("Exception: public List getLedgerReport(String societyID,String fromDate, String uptoDate,int isConsolidated) "+e );
		}
		return accHeadListList;
	}
	
	public List getGroupLedgerReport(int societyID,String fromDate, String uptoDate,int categoryID){
		List ledgerList=null;
		List accHeadListList=new ArrayList();
		try{
		log.debug("Entry : public List getGroupLedgerReport(String societyID,String fromDate, String uptoDate)");
		
		accHeadListList=trialBalanceDomain.getGroupLedgerReport(societyID, fromDate, uptoDate, categoryID);
		
		
		log.debug("Exit : public List getGroupLedgerReport(String societyID,String fromDate, String uptoDate)");
		}catch (Exception e) {
			log.error("Exception: public List getGroupLedgerReport(String societyID,String fromDate, String uptoDate) "+e );
		}
		return accHeadListList;
	}
	
	public List getLedgerReportFromCategory(int societyID,String fromDate, String uptoDate,int categoryID,int isConsolidated){
		List ledgerList=null;
		List accHeadListList=new ArrayList();
		try{
		log.debug("Entry : public List getLedgerReport(String societyID,String fromDate, String uptoDate,int isConsolidated)"+categoryID);
		
		accHeadListList=trialBalanceDomain.getLedgerReportFromCategory(societyID, fromDate, uptoDate, categoryID,isConsolidated);
		
		
		log.debug("Exit : public List getLedgerReport(String societyID,String fromDate, String uptoDate,int isConsolidated)");
		}catch (Exception e) {
			log.error("Exception: public List getLedgerReport(String societyID,String fromDate, String uptoDate,int isConsolidated) "+e );
		}
		return accHeadListList;
	}
	
	public List getTrialBalanceLedgerReport(int societyID,String fromDate, String uptoDate){
		List ledgerList=null;
		List accHeadListList=new ArrayList();
		try{
		log.debug("Entry : public List getTrialBalanceLedgerReport(String societyID,String fromDate, String uptoDate)");
		
		accHeadListList=trialBalanceDomain.getTrialBalanceLedgerReport(societyID, fromDate, uptoDate);
		
		
		log.debug("Exit : public List getTrialBalanceLedgerReport(String societyID,String fromDate, String uptoDate)");
		}catch (Exception e) {
			log.error("Exception: public List getTrialBalanceLedgerReport(String societyID,String fromDate, String uptoDate) "+e );
		}
		return accHeadListList;
	}

	
	/*This method is used to get category wise summary report*/
	public List getCategoryWiseSummary(int societyID,String fromDate, String uptoDate,int rootID){
		
		List<AccountHeadVO> accHeadListList=new ArrayList<AccountHeadVO>();
		try{
		log.debug("Entry : public List getCategoryWiseSummary(String societyID,String fromDate, String uptoDate)");
		
		accHeadListList=trialBalanceDomain.getCategoryWiseSummary(societyID, fromDate, uptoDate,rootID);
		
		log.debug("Exit : public List getCategoryWiseSummary(String societyID,String fromDate, String uptoDate)"+accHeadListList.size());
		}catch (Exception e) {
			log.error("Exception: public List getCategoryWiseSummary(String societyID,String fromDate, String uptoDate) "+e );
		}
		return accHeadListList;
	}
	

	public void setReportDomain(ReportDomain reportDomain) {
		this.reportDomain = reportDomain;
	}


	public void setTrialBalanceDomain(TrialBalanceDomain trialBalanceDomain) {
		this.trialBalanceDomain = trialBalanceDomain;
	}
	
	
	
	
}
