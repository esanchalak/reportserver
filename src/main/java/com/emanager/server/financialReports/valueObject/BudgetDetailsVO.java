package com.emanager.server.financialReports.valueObject;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


public class BudgetDetailsVO 
{

	
	private int budgetID; 
	private String budgetName;
	private String description; // Description of budget
	private String budgetType; //type- Profit loss,cost center
	private String frequency;
	private String fromDate;
	private String toDate;
	private String budgetDate;
	private int isDeleted;
	private int createdBy;
	private int updatedBy;
	private String creatorName;
	private String updatorName;
	private Timestamp createDate;
	private Timestamp updateDate;
	
	//Budget item info details
	private int budgetItemID; // Budget Item ID- items,ledger,groups
	private int orgID;
	private int ledgerID;
	private String ledgerName;
	private BigDecimal budgetAmt=BigDecimal.ZERO; // budgeted amount for given ledger
	private BigDecimal budgetPercent; //budget in percent
	private List<BudgetDetailsVO> itemList;
	private int projectID;
	private String projectName;
	private String projectAcronyms;
	private int groupID;
	private String groupName;
	private int ccCategoryID;
	private int categoryID;
	private String categoryName;
	private int rootID;
	private BigDecimal janAmt=BigDecimal.ZERO;
	private BigDecimal febAmt=BigDecimal.ZERO;
	private BigDecimal marAmt=BigDecimal.ZERO;
	private BigDecimal aprAmt=BigDecimal.ZERO;
	private BigDecimal mayAmt=BigDecimal.ZERO;
	private BigDecimal junAmt=BigDecimal.ZERO;
	private BigDecimal julAmt=BigDecimal.ZERO;
	private BigDecimal augAmt=BigDecimal.ZERO;
	private BigDecimal septAmt=BigDecimal.ZERO;
	private BigDecimal octAmt=BigDecimal.ZERO;
	private BigDecimal novAmt=BigDecimal.ZERO;
	private BigDecimal decAmt=BigDecimal.ZERO;
	private BudgetDetailsVO janObj;// January Months Details
	private BudgetDetailsVO febObj;// February Months Details
	private BudgetDetailsVO marObj;// March Months Details
	private BudgetDetailsVO aprObj;// April Months Details
	private BudgetDetailsVO mayObj;// May Months Details
	private BudgetDetailsVO junObj;// June Months Details
	private BudgetDetailsVO julObj;// July Months Details
	private BudgetDetailsVO augObj;// August Months Details
	private BudgetDetailsVO septObj;// September Months Details
	private BudgetDetailsVO octObj;// October Months Details
	private BudgetDetailsVO novObj;// November Months Details
	private BudgetDetailsVO decObj;// December Months Details
	private BudgetDetailsVO q1Obj;// First Quarter  Details
	private BudgetDetailsVO q2Obj;// Second Quarter  Details
	private BudgetDetailsVO q3Obj;// Third Quarter  Details
	private BudgetDetailsVO q4Obj;// Fourth Quarter  Details
	private BudgetDetailsVO h1Obj;// First Half Year Details
	private BudgetDetailsVO h2Obj;// Second Half Year Details
	private BudgetDetailsVO yearObj;// Yearly  Details
	private BudgetDetailsVO monthlyObj;// 12 Months Details
	private BudgetDetailsVO quaterlyObj;// 4 Quater Details
	private BigDecimal q1Amt=BigDecimal.ZERO;
	private BigDecimal q2Amt=BigDecimal.ZERO;
	private BigDecimal q3Amt=BigDecimal.ZERO;
	private BigDecimal q4Amt=BigDecimal.ZERO;
	private BigDecimal h1Amt=BigDecimal.ZERO;
	private BigDecimal h2Amt=BigDecimal.ZERO;
	private List chartList;// Charts data 
	
	private BigDecimal janActual=BigDecimal.ZERO;
	private BigDecimal febActual=BigDecimal.ZERO;
	private BigDecimal marActual=BigDecimal.ZERO;
	private BigDecimal aprActual=BigDecimal.ZERO;
	private BigDecimal mayActual=BigDecimal.ZERO;
	private BigDecimal junActual=BigDecimal.ZERO;
	private BigDecimal julActual=BigDecimal.ZERO;
	private BigDecimal augActual=BigDecimal.ZERO;
	private BigDecimal septActual=BigDecimal.ZERO;
	private BigDecimal octActual=BigDecimal.ZERO;
	private BigDecimal novActual=BigDecimal.ZERO;
	private BigDecimal decActual=BigDecimal.ZERO;
	
	private BigDecimal q1Actual=BigDecimal.ZERO;
	private BigDecimal q2Actual=BigDecimal.ZERO;
	private BigDecimal q3Actual=BigDecimal.ZERO;
	private BigDecimal q4Actual=BigDecimal.ZERO;
	
	private BigDecimal h1Actual=BigDecimal.ZERO;
	private BigDecimal h2Actual=BigDecimal.ZERO;
	
	private BigDecimal actualAmount;
	private BigDecimal totalBudgetAmount;
	private BigDecimal totalActualAmount;
	private int parentID;
	private String inOutType;
	
	
	/**
	 * @return the budgetID
	 */
	public int getBudgetID() {
		return budgetID;
	}
	/**
	 * @return the budgetName
	 */
	public String getBudgetName() {
		return budgetName;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @return the budgetType
	 */
	public String getBudgetType() {
		return budgetType;
	}
	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @return the budgetItemID
	 */
	public int getBudgetItemID() {
		return budgetItemID;
	}
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @return the ledgerID
	 */
	public int getLedgerID() {
		return ledgerID;
	}
	/**
	 * @return the budgetedAmt
	 */
	public BigDecimal getBudgetAmt() {
		return budgetAmt;
	}
	/**
	 * @return the budgetPercent
	 */
	public BigDecimal getBudgetPercent() {
		return budgetPercent;
	}
	/**
	 * @param budgetID the budgetID to set
	 */
	public void setBudgetID(int budgetID) {
		this.budgetID = budgetID;
	}
	/**
	 * @param budgetName the budgetName to set
	 */
	public void setBudgetName(String budgetName) {
		this.budgetName = budgetName;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @param budgetType the budgetType to set
	 */
	public void setBudgetType(String budgetType) {
		this.budgetType = budgetType;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	/**
	 * @param budgetItemID the budgetItemID to set
	 */
	public void setBudgetItemID(int budgetItemID) {
		this.budgetItemID = budgetItemID;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	/**
	 * @param ledgerID the ledgerID to set
	 */
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	/**
	 * @param budgetedAmt the budgetedAmt to set
	 */
	public void setBudgetAmt(BigDecimal budgetAmt) {
		this.budgetAmt = budgetAmt;
	}
	/**
	 * @param budgetPercent the budgetPercent to set
	 */
	public void setBudgetPercent(BigDecimal budgetPercent) {
		this.budgetPercent = budgetPercent;
	}
	/**
	 * @return the isDeleted
	 */
	public int getIsDeleted() {
		return isDeleted;
	}
	/**
	 * @return the createdBy
	 */
	public int getCreatedBy() {
		return createdBy;
	}
	/**
	 * @return the updatedBy
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @return the creatorName
	 */
	public String getCreatorName() {
		return creatorName;
	}
	/**
	 * @return the updatorName
	 */
	public String getUpdatorName() {
		return updatorName;
	}

	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * @param creatorName the creatorName to set
	 */
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	/**
	 * @param updatorName the updatorName to set
	 */
	public void setUpdatorName(String updatorName) {
		this.updatorName = updatorName;
	}
	/**
	 * @return the createDate
	 */
	public Timestamp getCreateDate() {
		return createDate;
	}
	/**
	 * @return the updateDate
	 */
	public Timestamp getUpdateDate() {
		return updateDate;
	}
	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
	/**
	 * @return the ledgerName
	 */
	public String getLedgerName() {
		return ledgerName;
	}
	/**
	 * @param ledgerName the ledgerName to set
	 */
	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}
	/**
	 * @return the itemList
	 */
	public List<BudgetDetailsVO> getItemList() {
		return itemList;
	}
	/**
	 * @param itemList the itemList to set
	 */
	public void setItemList(List<BudgetDetailsVO> itemList) {
		this.itemList = itemList;
	}
	/**
	 * @return the projectID
	 */
	public int getProjectID() {
		return projectID;
	}
	/**
	 * @param projectID the projectID to set
	 */
	public void setProjectID(int projectID) {
		this.projectID = projectID;
	}
	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}
	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	/**
	 * @return the projectAcronyms
	 */
	public String getProjectAcronyms() {
		return projectAcronyms;
	}
	/**
	 * @param projectAcronyms the projectAcronyms to set
	 */
	public void setProjectAcronyms(String projectAcronyms) {
		this.projectAcronyms = projectAcronyms;
	}
	/**
	 * @return the frequency
	 */
	public String getFrequency() {
		return frequency;
	}
	/**
	 * @param frequency the frequency to set
	 */
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	/**
	 * @return the groupID
	 */
	public int getGroupID() {
		return groupID;
	}
	/**
	 * @param groupID the groupID to set
	 */
	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}
	/**
	 * @return the ccCategoryID
	 */
	public int getCcCategoryID() {
		return ccCategoryID;
	}
	/**
	 * @param ccCategoryID the ccCategoryID to set
	 */
	public void setCcCategoryID(int ccCategoryID) {
		this.ccCategoryID = ccCategoryID;
	}
	/**
	 * @return the janAmt
	 */
	public BigDecimal getJanAmt() {
		return janAmt;
	}
	/**
	 * @param janAmt the janAmt to set
	 */
	public void setJanAmt(BigDecimal janAmt) {
		this.janAmt = janAmt;
	}
	/**
	 * @return the febAmt
	 */
	public BigDecimal getFebAmt() {
		return febAmt;
	}
	/**
	 * @param febAmt the febAmt to set
	 */
	public void setFebAmt(BigDecimal febAmt) {
		this.febAmt = febAmt;
	}
	/**
	 * @return the marAmt
	 */
	public BigDecimal getMarAmt() {
		return marAmt;
	}
	/**
	 * @param marAmt the marAmt to set
	 */
	public void setMarAmt(BigDecimal marAmt) {
		this.marAmt = marAmt;
	}
	/**
	 * @return the aprAmt
	 */
	public BigDecimal getAprAmt() {
		return aprAmt;
	}
	/**
	 * @param aprAmt the aprAmt to set
	 */
	public void setAprAmt(BigDecimal aprAmt) {
		this.aprAmt = aprAmt;
	}
	/**
	 * @return the mayAmt
	 */
	public BigDecimal getMayAmt() {
		return mayAmt;
	}
	/**
	 * @param mayAmt the mayAmt to set
	 */
	public void setMayAmt(BigDecimal mayAmt) {
		this.mayAmt = mayAmt;
	}
	/**
	 * @return the junAmt
	 */
	public BigDecimal getJunAmt() {
		return junAmt;
	}
	/**
	 * @param junAmt the junAmt to set
	 */
	public void setJunAmt(BigDecimal junAmt) {
		this.junAmt = junAmt;
	}
	/**
	 * @return the julAmt
	 */
	public BigDecimal getJulAmt() {
		return julAmt;
	}
	/**
	 * @param julAmt the julAmt to set
	 */
	public void setJulAmt(BigDecimal julAmt) {
		this.julAmt = julAmt;
	}
	/**
	 * @return the augAmt
	 */
	public BigDecimal getAugAmt() {
		return augAmt;
	}
	/**
	 * @param augAmt the augAmt to set
	 */
	public void setAugAmt(BigDecimal augAmt) {
		this.augAmt = augAmt;
	}
	/**
	 * @return the septAmt
	 */
	public BigDecimal getSeptAmt() {
		return septAmt;
	}
	/**
	 * @param septAmt the septAmt to set
	 */
	public void setSeptAmt(BigDecimal septAmt) {
		this.septAmt = septAmt;
	}
	/**
	 * @return the octAmt
	 */
	public BigDecimal getOctAmt() {
		return octAmt;
	}
	/**
	 * @param octAmt the octAmt to set
	 */
	public void setOctAmt(BigDecimal octAmt) {
		this.octAmt = octAmt;
	}
	/**
	 * @return the novAmt
	 */
	public BigDecimal getNovAmt() {
		return novAmt;
	}
	/**
	 * @param novAmt the novAmt to set
	 */
	public void setNovAmt(BigDecimal novAmt) {
		this.novAmt = novAmt;
	}
	/**
	 * @return the decAmt
	 */
	public BigDecimal getDecAmt() {
		return decAmt;
	}
	/**
	 * @param decAmt the decAmt to set
	 */
	public void setDecAmt(BigDecimal decAmt) {
		this.decAmt = decAmt;
	}
	/**
	 * @return the janObj
	 */
	public BudgetDetailsVO getJanObj() {
		return janObj;
	}
	/**
	 * @param janObj the janObj to set
	 */
	public void setJanObj(BudgetDetailsVO janObj) {
		this.janObj = janObj;
	}
	/**
	 * @return the febObj
	 */
	public BudgetDetailsVO getFebObj() {
		return febObj;
	}
	/**
	 * @param febObj the febObj to set
	 */
	public void setFebObj(BudgetDetailsVO febObj) {
		this.febObj = febObj;
	}
	/**
	 * @return the marObj
	 */
	public BudgetDetailsVO getMarObj() {
		return marObj;
	}
	/**
	 * @param marObj the marObj to set
	 */
	public void setMarObj(BudgetDetailsVO marObj) {
		this.marObj = marObj;
	}
	/**
	 * @return the aprObj
	 */
	public BudgetDetailsVO getAprObj() {
		return aprObj;
	}
	/**
	 * @param aprObj the aprObj to set
	 */
	public void setAprObj(BudgetDetailsVO aprObj) {
		this.aprObj = aprObj;
	}
	/**
	 * @return the mayObj
	 */
	public BudgetDetailsVO getMayObj() {
		return mayObj;
	}
	/**
	 * @param mayObj the mayObj to set
	 */
	public void setMayObj(BudgetDetailsVO mayObj) {
		this.mayObj = mayObj;
	}
	/**
	 * @return the junObj
	 */
	public BudgetDetailsVO getJunObj() {
		return junObj;
	}
	/**
	 * @param junObj the junObj to set
	 */
	public void setJunObj(BudgetDetailsVO junObj) {
		this.junObj = junObj;
	}
	/**
	 * @return the julObj
	 */
	public BudgetDetailsVO getJulObj() {
		return julObj;
	}
	/**
	 * @param julObj the julObj to set
	 */
	public void setJulObj(BudgetDetailsVO julObj) {
		this.julObj = julObj;
	}
	/**
	 * @return the augObj
	 */
	public BudgetDetailsVO getAugObj() {
		return augObj;
	}
	/**
	 * @param augObj the augObj to set
	 */
	public void setAugObj(BudgetDetailsVO augObj) {
		this.augObj = augObj;
	}
	/**
	 * @return the septObj
	 */
	public BudgetDetailsVO getSeptObj() {
		return septObj;
	}
	/**
	 * @param septObj the septObj to set
	 */
	public void setSeptObj(BudgetDetailsVO septObj) {
		this.septObj = septObj;
	}
	/**
	 * @return the octObj
	 */
	public BudgetDetailsVO getOctObj() {
		return octObj;
	}
	/**
	 * @param octObj the octObj to set
	 */
	public void setOctObj(BudgetDetailsVO octObj) {
		this.octObj = octObj;
	}
	/**
	 * @return the novObj
	 */
	public BudgetDetailsVO getNovObj() {
		return novObj;
	}
	/**
	 * @param novObj the novObj to set
	 */
	public void setNovObj(BudgetDetailsVO novObj) {
		this.novObj = novObj;
	}
	/**
	 * @return the decObj
	 */
	public BudgetDetailsVO getDecObj() {
		return decObj;
	}
	/**
	 * @param decObj the decObj to set
	 */
	public void setDecObj(BudgetDetailsVO decObj) {
		this.decObj = decObj;
	}
	/**
	 * @return the q1Obj
	 */
	public BudgetDetailsVO getQ1Obj() {
		return q1Obj;
	}
	/**
	 * @param q1Obj the q1Obj to set
	 */
	public void setQ1Obj(BudgetDetailsVO q1Obj) {
		this.q1Obj = q1Obj;
	}
	/**
	 * @return the q2Obj
	 */
	public BudgetDetailsVO getQ2Obj() {
		return q2Obj;
	}
	/**
	 * @param q2Obj the q2Obj to set
	 */
	public void setQ2Obj(BudgetDetailsVO q2Obj) {
		this.q2Obj = q2Obj;
	}
	/**
	 * @return the q3Obj
	 */
	public BudgetDetailsVO getQ3Obj() {
		return q3Obj;
	}
	/**
	 * @param q3Obj the q3Obj to set
	 */
	public void setQ3Obj(BudgetDetailsVO q3Obj) {
		this.q3Obj = q3Obj;
	}
	/**
	 * @return the q4Obj
	 */
	public BudgetDetailsVO getQ4Obj() {
		return q4Obj;
	}
	/**
	 * @param q4Obj the q4Obj to set
	 */
	public void setQ4Obj(BudgetDetailsVO q4Obj) {
		this.q4Obj = q4Obj;
	}
	/**
	 * @return the h1Obj
	 */
	public BudgetDetailsVO getH1Obj() {
		return h1Obj;
	}
	/**
	 * @param h1Obj the h1Obj to set
	 */
	public void setH1Obj(BudgetDetailsVO h1Obj) {
		this.h1Obj = h1Obj;
	}
	/**
	 * @return the h2Obj
	 */
	public BudgetDetailsVO getH2Obj() {
		return h2Obj;
	}
	/**
	 * @param h2Obj the h2Obj to set
	 */
	public void setH2Obj(BudgetDetailsVO h2Obj) {
		this.h2Obj = h2Obj;
	}
	/**
	 * @return the yearObj
	 */
	public BudgetDetailsVO getYearObj() {
		return yearObj;
	}
	/**
	 * @param yearObj the yearObj to set
	 */
	public void setYearObj(BudgetDetailsVO yearObj) {
		this.yearObj = yearObj;
	}
	/**
	 * @return the monthlyObj
	 */
	public BudgetDetailsVO getMonthlyObj() {
		return monthlyObj;
	}
	/**
	 * @param monthlyObj the monthlyObj to set
	 */
	public void setMonthlyObj(BudgetDetailsVO monthlyObj) {
		this.monthlyObj = monthlyObj;
	}
	/**
	 * @return the quaterlyObj
	 */
	public BudgetDetailsVO getQuaterlyObj() {
		return quaterlyObj;
	}
	/**
	 * @param quaterlyObj the quaterlyObj to set
	 */
	public void setQuaterlyObj(BudgetDetailsVO quaterlyObj) {
		this.quaterlyObj = quaterlyObj;
	}
	/**
	 * @return the q1Amt
	 */
	public BigDecimal getQ1Amt() {
		return q1Amt;
	}
	/**
	 * @param q1Amt the q1Amt to set
	 */
	public void setQ1Amt(BigDecimal q1Amt) {
		this.q1Amt = q1Amt;
	}
	/**
	 * @return the q2Amt
	 */
	public BigDecimal getQ2Amt() {
		return q2Amt;
	}
	/**
	 * @param q2Amt the q2Amt to set
	 */
	public void setQ2Amt(BigDecimal q2Amt) {
		this.q2Amt = q2Amt;
	}
	/**
	 * @return the q3Amt
	 */
	public BigDecimal getQ3Amt() {
		return q3Amt;
	}
	/**
	 * @param q3Amt the q3Amt to set
	 */
	public void setQ3Amt(BigDecimal q3Amt) {
		this.q3Amt = q3Amt;
	}
	/**
	 * @return the q4Amt
	 */
	public BigDecimal getQ4Amt() {
		return q4Amt;
	}
	/**
	 * @param q4Amt the q4Amt to set
	 */
	public void setQ4Amt(BigDecimal q4Amt) {
		this.q4Amt = q4Amt;
	}
	/**
	 * @return the h1Amt
	 */
	public BigDecimal getH1Amt() {
		return h1Amt;
	}
	/**
	 * @param h1Amt the h1Amt to set
	 */
	public void setH1Amt(BigDecimal h1Amt) {
		this.h1Amt = h1Amt;
	}
	/**
	 * @return the h2Amt
	 */
	public BigDecimal getH2Amt() {
		return h2Amt;
	}
	/**
	 * @param h2Amt the h2Amt to set
	 */
	public void setH2Amt(BigDecimal h2Amt) {
		this.h2Amt = h2Amt;
	}
	/**
	 * @return the chartList
	 */
	public List getChartList() {
		return chartList;
	}
	/**
	 * @param chartList the chartList to set
	 */
	public void setChartList(List chartList) {
		this.chartList = chartList;
	}
	/**
	 * @return the janActual
	 */
	public BigDecimal getJanActual() {
		return janActual;
	}
	/**
	 * @param janActual the janActual to set
	 */
	public void setJanActual(BigDecimal janActual) {
		this.janActual = janActual;
	}
	/**
	 * @return the febActual
	 */
	public BigDecimal getFebActual() {
		return febActual;
	}
	/**
	 * @param febActual the febActual to set
	 */
	public void setFebActual(BigDecimal febActual) {
		this.febActual = febActual;
	}
	/**
	 * @return the marActual
	 */
	public BigDecimal getMarActual() {
		return marActual;
	}
	/**
	 * @param marActual the marActual to set
	 */
	public void setMarActual(BigDecimal marActual) {
		this.marActual = marActual;
	}
	/**
	 * @return the aprActual
	 */
	public BigDecimal getAprActual() {
		return aprActual;
	}
	/**
	 * @param aprActual the aprActual to set
	 */
	public void setAprActual(BigDecimal aprActual) {
		this.aprActual = aprActual;
	}
	/**
	 * @return the mayActual
	 */
	public BigDecimal getMayActual() {
		return mayActual;
	}
	/**
	 * @param mayActual the mayActual to set
	 */
	public void setMayActual(BigDecimal mayActual) {
		this.mayActual = mayActual;
	}
	/**
	 * @return the junActual
	 */
	public BigDecimal getJunActual() {
		return junActual;
	}
	/**
	 * @param junActual the junActual to set
	 */
	public void setJunActual(BigDecimal junActual) {
		this.junActual = junActual;
	}
	/**
	 * @return the julActual
	 */
	public BigDecimal getJulActual() {
		return julActual;
	}
	/**
	 * @param julActual the julActual to set
	 */
	public void setJulActual(BigDecimal julActual) {
		this.julActual = julActual;
	}
	/**
	 * @return the augActual
	 */
	public BigDecimal getAugActual() {
		return augActual;
	}
	/**
	 * @param augActual the augActual to set
	 */
	public void setAugActual(BigDecimal augActual) {
		this.augActual = augActual;
	}
	/**
	 * @return the septActual
	 */
	public BigDecimal getSeptActual() {
		return septActual;
	}
	/**
	 * @param septActual the septActual to set
	 */
	public void setSeptActual(BigDecimal septActual) {
		this.septActual = septActual;
	}
	/**
	 * @return the octActual
	 */
	public BigDecimal getOctActual() {
		return octActual;
	}
	/**
	 * @param octActual the octActual to set
	 */
	public void setOctActual(BigDecimal octActual) {
		this.octActual = octActual;
	}
	/**
	 * @return the novActual
	 */
	public BigDecimal getNovActual() {
		return novActual;
	}
	/**
	 * @param novActual the novActual to set
	 */
	public void setNovActual(BigDecimal novActual) {
		this.novActual = novActual;
	}
	/**
	 * @return the decActual
	 */
	public BigDecimal getDecActual() {
		return decActual;
	}
	/**
	 * @param decActual the decActual to set
	 */
	public void setDecActual(BigDecimal decActual) {
		this.decActual = decActual;
	}
	/**
	 * @return the q1Actual
	 */
	public BigDecimal getQ1Actual() {
		return q1Actual;
	}
	/**
	 * @param q1Actual the q1Actual to set
	 */
	public void setQ1Actual(BigDecimal q1Actual) {
		this.q1Actual = q1Actual;
	}
	/**
	 * @return the q2Actual
	 */
	public BigDecimal getQ2Actual() {
		return q2Actual;
	}
	/**
	 * @param q2Actual the q2Actual to set
	 */
	public void setQ2Actual(BigDecimal q2Actual) {
		this.q2Actual = q2Actual;
	}
	/**
	 * @return the q3Actual
	 */
	public BigDecimal getQ3Actual() {
		return q3Actual;
	}
	/**
	 * @param q3Actual the q3Actual to set
	 */
	public void setQ3Actual(BigDecimal q3Actual) {
		this.q3Actual = q3Actual;
	}
	/**
	 * @return the q4Actual
	 */
	public BigDecimal getQ4Actual() {
		return q4Actual;
	}
	/**
	 * @param q4Actual the q4Actual to set
	 */
	public void setQ4Actual(BigDecimal q4Actual) {
		this.q4Actual = q4Actual;
	}
	/**
	 * @return the h1Actual
	 */
	public BigDecimal getH1Actual() {
		return h1Actual;
	}
	/**
	 * @param h1Actual the h1Actual to set
	 */
	public void setH1Actual(BigDecimal h1Actual) {
		this.h1Actual = h1Actual;
	}
	/**
	 * @return the h2Actual
	 */
	public BigDecimal getH2Actual() {
		return h2Actual;
	}
	/**
	 * @param h2Actual the h2Actual to set
	 */
	public void setH2Actual(BigDecimal h2Actual) {
		this.h2Actual = h2Actual;
	}
	/**
	 * @return the actualAmount
	 */
	public BigDecimal getActualAmount() {
		return actualAmount;
	}
	/**
	 * @param actualAmount the actualAmount to set
	 */
	public void setActualAmount(BigDecimal actualAmount) {
		this.actualAmount = actualAmount;
	}
	/**
	 * @return the totalBudgetAmount
	 */
	public BigDecimal getTotalBudgetAmount() {
		return totalBudgetAmount;
	}
	/**
	 * @param totalBudgetAmount the totalBudgetAmount to set
	 */
	public void setTotalBudgetAmount(BigDecimal totalBudgetAmount) {
		this.totalBudgetAmount = totalBudgetAmount;
	}
	/**
	 * @return the totalActualAmount
	 */
	public BigDecimal getTotalActualAmount() {
		return totalActualAmount;
	}
	/**
	 * @param totalActualAmount the totalActualAmount to set
	 */
	public void setTotalActualAmount(BigDecimal totalActualAmount) {
		this.totalActualAmount = totalActualAmount;
	}
	/**
	 * @return the parentID
	 */
	public int getParentID() {
		return parentID;
	}
	/**
	 * @param parentID the parentID to set
	 */
	public void setParentID(int parentID) {
		this.parentID = parentID;
	}
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	/**
	 * @return the categoryID
	 */
	public int getCategoryID() {
		return categoryID;
	}
	/**
	 * @param categoryID the categoryID to set
	 */
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}
	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}
	/**
	 * @param categoryName the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	/**
	 * @return the rootID
	 */
	public int getRootID() {
		return rootID;
	}
	/**
	 * @param rootID the rootID to set
	 */
	public void setRootID(int rootID) {
		this.rootID = rootID;
	}
	/**
	 * @return the inOutType
	 */
	public String getInOutType() {
		return inOutType;
	}
	/**
	 * @param inOutType the inOutType to set
	 */
	public void setInOutType(String inOutType) {
		this.inOutType = inOutType;
	}
	/**
	 * @return the budgetDate
	 */
	public String getBudgetDate() {
		return budgetDate;
	}
	/**
	 * @param budgetDate the budgetDate to set
	 */
	public void setBudgetDate(String budgetDate) {
		this.budgetDate = budgetDate;
	}
	
	
	
	
}
