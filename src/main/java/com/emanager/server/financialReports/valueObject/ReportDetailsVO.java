package com.emanager.server.financialReports.valueObject;

import java.math.BigDecimal;
import java.util.List;

import com.emanager.server.society.valueObject.SocietyVO;

public class ReportDetailsVO {

	    private int orgID;
	    
	    private int categoryID;

	    private String categoryName;

	    private int groupID;

	    private String groupName;

	    private int ledgerID;

	    private String ledgerName;
	    
	    private String txType;
	         
	    private String fromDate;
	    
	    private String uptoDate;
	    
   	    private String comments;
	     	   	    
	   private BigDecimal amount;
	   
	   private BigDecimal totalAmount;
	   
	   private List objectList;
	   
	   private BigDecimal openingBalance;
	   
	   private BigDecimal closingBalance;
	   
	   private BigDecimal difference;
	   
	   private BigDecimal differenceInCurrentBal;
	   
	   private BigDecimal creditBalance;
	   
	   private BigDecimal debitBalance;
	   
	   private String txDate;
	   
	   private String monthName;
	   
	   private ReportVO profitLossVO;
	   
	   private ReportVO balanceSheetVO;
	   
	   private SocietyVO societyVO;
	   
	   private BigDecimal budgetAmt;
	   
	   private BigDecimal budgetPercent;
	   
	   private BigDecimal budgetVarience;
	   
	   private int rootID;
	   
	   private int rptID;
	   private int id;
	   private int parentID;
	   private String rptGrpType;
	   private BigDecimal prevTotalAmount;

	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}

	/**
	 * @return the categoryID
	 */
	public int getCategoryID() {
		return categoryID;
	}

	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * @return the groupID
	 */
	public int getGroupID() {
		return groupID;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @return the ledgerID
	 */
	public int getLedgerID() {
		return ledgerID;
	}

	/**
	 * @return the ledgerName
	 */
	public String getLedgerName() {
		return ledgerName;
	}

	/**
	 * @return the openingBalance
	 */
	public BigDecimal getOpeningBalance() {
		return openingBalance;
	}

	/**
	 * @return the txType
	 */
	public String getTxType() {
		return txType;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @return the uptoDate
	 */
	public String getUptoDate() {
		return uptoDate;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @return the totalAmount
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}

	/**
	 * @param categoryID the categoryID to set
	 */
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}

	/**
	 * @param categoryName the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * @param groupID the groupID to set
	 */
	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @param ledgerID the ledgerID to set
	 */
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}

	/**
	 * @param ledgerName the ledgerName to set
	 */
	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}

	/**
	 * @param openingBalance the openingBalance to set
	 */
	public void setOpeningBalance(BigDecimal openingBalance) {
		this.openingBalance = openingBalance;
	}

	/**
	 * @param txType the txType to set
	 */
	public void setTxType(String txType) {
		this.txType = txType;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @param uptoDate the uptoDate to set
	 */
	public void setUptoDate(String uptoDate) {
		this.uptoDate = uptoDate;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the objectList
	 */
	public List getObjectList() {
		return objectList;
	}

	/**
	 * @param objectList the objectList to set
	 */
	public void setObjectList(List objectList) {
		this.objectList = objectList;
	}

	/**
	 * @return the closingBalance
	 */
	public BigDecimal getClosingBalance() {
		return closingBalance;
	}

	/**
	 * @param closingBalance the closingBalance to set
	 */
	public void setClosingBalance(BigDecimal closingBalance) {
		this.closingBalance = closingBalance;
	}

	/**
	 * @return the difference
	 */
	public BigDecimal getDifference() {
		return difference;
	}

	/**
	 * @param difference the difference to set
	 */
	public void setDifference(BigDecimal difference) {
		this.difference = difference;
	}

	/**
	 * @return the creditBalance
	 */
	public BigDecimal getCreditBalance() {
		return creditBalance;
	}

	/**
	 * @return the debitBalance
	 */
	public BigDecimal getDebitBalance() {
		return debitBalance;
	}

	/**
	 * @param creditBalance the creditBalance to set
	 */
	public void setCreditBalance(BigDecimal creditBalance) {
		this.creditBalance = creditBalance;
	}

	/**
	 * @param debitBalance the debitBalance to set
	 */
	public void setDebitBalance(BigDecimal debitBalance) {
		this.debitBalance = debitBalance;
	}

	/**
	 * @return the differenceInCurrentBal
	 */
	public BigDecimal getDifferenceInCurrentBal() {
		return differenceInCurrentBal;
	}

	/**
	 * @param differenceInCurrentBal the differenceInCurrentBal to set
	 */
	public void setDifferenceInCurrentBal(BigDecimal differenceInCurrentBal) {
		this.differenceInCurrentBal = differenceInCurrentBal;
	}

	/**
	 * @return the txDate
	 */
	public String getTxDate() {
		return txDate;
	}

	/**
	 * @return the monthName
	 */
	public String getMonthName() {
		return monthName;
	}

	/**
	 * @param txDate the txDate to set
	 */
	public void setTxDate(String txDate) {
		this.txDate = txDate;
	}

	/**
	 * @param monthName the monthName to set
	 */
	public void setMonthName(String monthName) {
		this.monthName = monthName;
	}

	/**
	 * @return the profitLossVO
	 */
	public ReportVO getProfitLossVO() {
		return profitLossVO;
	}

	/**
	 * @return the balanceSheetVO
	 */
	public ReportVO getBalanceSheetVO() {
		return balanceSheetVO;
	}

	/**
	 * @return the societyVO
	 */
	public SocietyVO getSocietyVO() {
		return societyVO;
	}

	/**
	 * @param profitLossVO the profitLossVO to set
	 */
	public void setProfitLossVO(ReportVO profitLossVO) {
		this.profitLossVO = profitLossVO;
	}

	/**
	 * @param balanceSheetVO the balanceSheetVO to set
	 */
	public void setBalanceSheetVO(ReportVO balanceSheetVO) {
		this.balanceSheetVO = balanceSheetVO;
	}

	/**
	 * @param societyVO the societyVO to set
	 */
	public void setSocietyVO(SocietyVO societyVO) {
		this.societyVO = societyVO;
	}

	/**
	 * @return the budgetAmt
	 */
	public BigDecimal getBudgetAmt() {
		return budgetAmt;
	}

	/**
	 * @return the budgetPercent
	 */
	public BigDecimal getBudgetPercent() {
		return budgetPercent;
	}

	/**
	 * @return the budgetVarience
	 */
	public BigDecimal getBudgetVarience() {
		return budgetVarience;
	}

	/**
	 * @param budgetAmt the budgetAmt to set
	 */
	public void setBudgetAmt(BigDecimal budgetAmt) {
		this.budgetAmt = budgetAmt;
	}

	/**
	 * @param budgetPercent the budgetPercent to set
	 */
	public void setBudgetPercent(BigDecimal budgetPercent) {
		this.budgetPercent = budgetPercent;
	}

	/**
	 * @param budgetVarience the budgetVarience to set
	 */
	public void setBudgetVarience(BigDecimal budgetVarience) {
		this.budgetVarience = budgetVarience;
	}

	/**
	 * @return the rootID
	 */
	public int getRootID() {
		return rootID;
	}

	/**
	 * @param rootID the rootID to set
	 */
	public void setRootID(int rootID) {
		this.rootID = rootID;
	}

	/**
	 * @return the rptID
	 */
	public int getRptID() {
		return rptID;
	}

	/**
	 * @param rptID the rptID to set
	 */
	public void setRptID(int rptID) {
		this.rptID = rptID;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the parentID
	 */
	public int getParentID() {
		return parentID;
	}

	/**
	 * @param parentID the parentID to set
	 */
	public void setParentID(int parentID) {
		this.parentID = parentID;
	}

	/**
	 * @return the rptGrpType
	 */
	public String getRptGrpType() {
		return rptGrpType;
	}

	/**
	 * @param rptGrpType the rptGrpType to set
	 */
	public void setRptGrpType(String rptGrpType) {
		this.rptGrpType = rptGrpType;
	}

	/**
	 * @return the prevTotalAmount
	 */
	public BigDecimal getPrevTotalAmount() {
		return prevTotalAmount;
	}

	/**
	 * @param prevTotalAmount the prevTotalAmount to set
	 */
	public void setPrevTotalAmount(BigDecimal prevTotalAmount) {
		this.prevTotalAmount = prevTotalAmount;
	}
	   
	      
	 

	  

	    

	}