package com.emanager.server.financialReports.valueObject;

import java.math.BigDecimal;
import java.util.List;

public class MonthwiseChartVO
{

	private int societyID;
	private int ledgerID;
	private int groupID;
	private int rootGroupID;
	private String groupName;
	private String ledgerName;
	private String rootGroupName;
	private BigDecimal totalAmount;
	private BigDecimal janAmt;
	private BigDecimal febAmt;
	private BigDecimal marAmt;
	private BigDecimal aprAmt;
	private BigDecimal mayAmt;
	private BigDecimal junAmt;
	private BigDecimal julAmt;
	private BigDecimal augAmt;
	private BigDecimal septAmt;
	private BigDecimal octAmt;
	private BigDecimal novAmt;
	private BigDecimal decAmt;
	private ReportVO janObj;// January Months Details
	private ReportVO febObj;// February Months Details
	private ReportVO marObj;// March Months Details
	private ReportVO aprObj;// April Months Details
	private ReportVO mayObj;// May Months Details
	private ReportVO junObj;// June Months Details
	private ReportVO julObj;// July Months Details
	private ReportVO augObj;// August Months Details
	private ReportVO septObj;// September Months Details
	private ReportVO octObj;// October Months Details
	private ReportVO novObj;// November Months Details
	private ReportVO decObj;// December Months Details
	private ReportVO q1Obj;// First Quarter  Details
	private ReportVO q2Obj;// Second Quarter  Details
	private ReportVO q3Obj;// Third Quarter  Details
	private ReportVO q4Obj;// Fourth Quarter  Details
	private ReportVO h1Obj;// First Half Year Details
	private ReportVO h2Obj;// Second Half Year Details
	private ReportVO yearObj;// Yearly  Details
	private ReportVO monthlyObj;// 12 Months Details
	private ReportVO quaterlyObj;// 4 Quater Details
	private BigDecimal q1Amt;
	private BigDecimal q2Amt;
	private BigDecimal q3Amt;
	private BigDecimal q4Amt;
	private List chartList;// Charts data 
	private String txType;	
	private BigDecimal openingBalance;
	private String transactionType;
	
	private BigDecimal janCr;
	private BigDecimal febCr;
	private BigDecimal marCr;
	private BigDecimal aprCr;
	private BigDecimal mayCr;
	private BigDecimal junCr;
	private BigDecimal julCr;
	private BigDecimal augCr;
	private BigDecimal septCr;
	private BigDecimal octCr;
	private BigDecimal novCr;
	private BigDecimal decCr;
	
	private BigDecimal janDb;
	private BigDecimal febDb;
	private BigDecimal marDb;
	private BigDecimal aprDb;
	private BigDecimal mayDb;
	private BigDecimal junDb;
	private BigDecimal julDb;
	private BigDecimal augDb;
	private BigDecimal septDb;
	private BigDecimal octDb;
	private BigDecimal novDb;
	private BigDecimal decDb;
	
	private BigDecimal q1Cr;
	private BigDecimal q2Cr;
	private BigDecimal q3Cr;
	private BigDecimal q4Cr;
	
	private BigDecimal q1Db;
	private BigDecimal q2Db;
	private BigDecimal q3Db;
	private BigDecimal q4Db;
	
	private BigDecimal janQty;
	private BigDecimal febQty;
	private BigDecimal marQty;
	private BigDecimal aprQty;
	private BigDecimal mayQty;
	private BigDecimal junQty;
	private BigDecimal julQty;
	private BigDecimal augQty;
	private BigDecimal septQty;
	private BigDecimal octQty;
	private BigDecimal novQty;
	private BigDecimal decQty;
	
	private int productID;
	
	
	
	public ReportVO getMonthlyObj() {
		return monthlyObj;
	}
	public void setMonthlyObj(ReportVO monthlyObj) {
		this.monthlyObj = monthlyObj;
	}
	public ReportVO getQuaterlyObj() {
		return quaterlyObj;
	}
	public void setQuaterlyObj(ReportVO quaterlyObj) {
		this.quaterlyObj = quaterlyObj;
	}
	public String getTxType() {
		return txType;
	}
	public void setTxType(String txType) {
		this.txType = txType;
	}
	public List getChartList() {
		return chartList;
	}
	public void setChartList(List chartList) {
		this.chartList = chartList;
	}	
	public BigDecimal getJanAmt() {
		return janAmt;
	}
	public void setJanAmt(BigDecimal janAmt) {
		this.janAmt = janAmt;
	}
	public BigDecimal getFebAmt() {
		return febAmt;
	}
	public void setFebAmt(BigDecimal febAmt) {
		this.febAmt = febAmt;
	}
	public BigDecimal getMarAmt() {
		return marAmt;
	}
	public void setMarAmt(BigDecimal marAmt) {
		this.marAmt = marAmt;
	}
	public BigDecimal getAprAmt() {
		return aprAmt;
	}
	public void setAprAmt(BigDecimal aprAmt) {
		this.aprAmt = aprAmt;
	}
	public BigDecimal getMayAmt() {
		return mayAmt;
	}
	public void setMayAmt(BigDecimal mayAmt) {
		this.mayAmt = mayAmt;
	}
	public BigDecimal getJunAmt() {
		return junAmt;
	}
	public void setJunAmt(BigDecimal junAmt) {
		this.junAmt = junAmt;
	}
	public BigDecimal getJulAmt() {
		return julAmt;
	}
	public void setJulAmt(BigDecimal julAmt) {
		this.julAmt = julAmt;
	}
	public BigDecimal getAugAmt() {
		return augAmt;
	}
	public void setAugAmt(BigDecimal augAmt) {
		this.augAmt = augAmt;
	}
	public BigDecimal getSeptAmt() {
		return septAmt;
	}
	public void setSeptAmt(BigDecimal septAmt) {
		this.septAmt = septAmt;
	}
	public BigDecimal getOctAmt() {
		return octAmt;
	}
	public void setOctAmt(BigDecimal octAmt) {
		this.octAmt = octAmt;
	}
	public BigDecimal getNovAmt() {
		return novAmt;
	}
	public void setNovAmt(BigDecimal novAmt) {
		this.novAmt = novAmt;
	}
	public BigDecimal getDecAmt() {
		return decAmt;
	}
	public void setDecAmt(BigDecimal decAmt) {
		this.decAmt = decAmt;
	}
	public ReportVO getJanObj() {
		return janObj;
	}
	public void setJanObj(ReportVO janObj) {
		this.janObj = janObj;
	}
	public ReportVO getFebObj() {
		return febObj;
	}
	public void setFebObj(ReportVO febObj) {
		this.febObj = febObj;
	}
	public ReportVO getMarObj() {
		return marObj;
	}
	public void setMarObj(ReportVO marObj) {
		this.marObj = marObj;
	}
	public ReportVO getAprObj() {
		return aprObj;
	}
	public void setAprObj(ReportVO aprObj) {
		this.aprObj = aprObj;
	}
	public ReportVO getMayObj() {
		return mayObj;
	}
	public void setMayObj(ReportVO mayObj) {
		this.mayObj = mayObj;
	}
	public ReportVO getJunObj() {
		return junObj;
	}
	public void setJunObj(ReportVO junObj) {
		this.junObj = junObj;
	}
	public ReportVO getJulObj() {
		return julObj;
	}
	public void setJulObj(ReportVO julObj) {
		this.julObj = julObj;
	}
	public ReportVO getAugObj() {
		return augObj;
	}
	public void setAugObj(ReportVO augObj) {
		this.augObj = augObj;
	}
	public ReportVO getSeptObj() {
		return septObj;
	}
	public void setSeptObj(ReportVO septObj) {
		this.septObj = septObj;
	}
	public ReportVO getOctObj() {
		return octObj;
	}
	public void setOctObj(ReportVO octObj) {
		this.octObj = octObj;
	}
	public ReportVO getNovObj() {
		return novObj;
	}
	public void setNovObj(ReportVO novObj) {
		this.novObj = novObj;
	}
	public ReportVO getDecObj() {
		return decObj;
	}
	public void setDecObj(ReportVO decObj) {
		this.decObj = decObj;
	}
	public ReportVO getQ1Obj() {
		return q1Obj;
	}
	public void setQ1Obj(ReportVO q1Obj) {
		this.q1Obj = q1Obj;
	}
	public ReportVO getQ2Obj() {
		return q2Obj;
	}
	public void setQ2Obj(ReportVO q2Obj) {
		this.q2Obj = q2Obj;
	}
	public ReportVO getQ3Obj() {
		return q3Obj;
	}
	public void setQ3Obj(ReportVO q3Obj) {
		this.q3Obj = q3Obj;
	}
	public ReportVO getQ4Obj() {
		return q4Obj;
	}
	public void setQ4Obj(ReportVO q4Obj) {
		this.q4Obj = q4Obj;
	}
	public ReportVO getH1Obj() {
		return h1Obj;
	}
	public void setH1Obj(ReportVO h1Obj) {
		this.h1Obj = h1Obj;
	}
	public ReportVO getH2Obj() {
		return h2Obj;
	}
	public void setH2Obj(ReportVO h2Obj) {
		this.h2Obj = h2Obj;
	}
	public BigDecimal getQ1Amt() {
		return q1Amt;
	}
	public void setQ1Amt(BigDecimal q1Amt) {
		this.q1Amt = q1Amt;
	}
	public BigDecimal getQ2Amt() {
		return q2Amt;
	}
	public void setQ2Amt(BigDecimal q2Amt) {
		this.q2Amt = q2Amt;
	}
	public BigDecimal getQ3Amt() {
		return q3Amt;
	}
	public void setQ3Amt(BigDecimal q3Amt) {
		this.q3Amt = q3Amt;
	}
	public BigDecimal getQ4Amt() {
		return q4Amt;
	}
	public void setQ4Amt(BigDecimal q4Amt) {
		this.q4Amt = q4Amt;
	}
	
		
	/**
	 * @return the societyID
	 */
	public int getSocietyID() {
		return societyID;
	}
	/**
	 * @return the ledgerID
	 */
	public int getLedgerID() {
		return ledgerID;
	}
	/**
	 * @return the groupID
	 */
	public int getGroupID() {
		return groupID;
	}
	/**
	 * @return the rootGroupID
	 */
	public int getRootGroupID() {
		return rootGroupID;
	}
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * @return the ledgerName
	 */
	public String getLedgerName() {
		return ledgerName;
	}
	/**
	 * @return the rootGroupName
	 */
	public String getRootGroupName() {
		return rootGroupName;
	}
	/**
	 * @return the totalAmount
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	/**
	 * @return the janAmt
	 */
	
	/**
	 * @param societyID the societyID to set
	 */
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	/**
	 * @param ledgerID the ledgerID to set
	 */
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	/**
	 * @param groupID the groupID to set
	 */
	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}
	/**
	 * @param rootGroupID the rootGroupID to set
	 */
	public void setRootGroupID(int rootGroupID) {
		this.rootGroupID = rootGroupID;
	}
	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	/**
	 * @param ledgerName the ledgerName to set
	 */
	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}
	/**
	 * @param rootGroupName the rootGroupName to set
	 */
	public void setRootGroupName(String rootGroupName) {
		this.rootGroupName = rootGroupName;
	}
	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	/**
	 * @return the yearObj
	 */
	public ReportVO getYearObj() {
		return yearObj;
	}
	
	/**
	 * @param yearObj the yearObj to set
	 */
	public void setYearObj(ReportVO yearObj) {
		this.yearObj = yearObj;
	}
	/**
	 * @return the openingBalance
	 */
	public BigDecimal getOpeningBalance() {
		return openingBalance;
	}
	/**
	 * @param openingBalance the openingBalance to set
	 */
	public void setOpeningBalance(BigDecimal openingBalance) {
		this.openingBalance = openingBalance;
	}
	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return transactionType;
	}
	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	/**
	 * @return the janCr
	 */
	public BigDecimal getJanCr() {
		return janCr;
	}
	/**
	 * @param janCr the janCr to set
	 */
	public void setJanCr(BigDecimal janCr) {
		this.janCr = janCr;
	}
	/**
	 * @return the febCr
	 */
	public BigDecimal getFebCr() {
		return febCr;
	}
	/**
	 * @param febCr the febCr to set
	 */
	public void setFebCr(BigDecimal febCr) {
		this.febCr = febCr;
	}
	/**
	 * @return the marCr
	 */
	public BigDecimal getMarCr() {
		return marCr;
	}
	/**
	 * @param marCr the marCr to set
	 */
	public void setMarCr(BigDecimal marCr) {
		this.marCr = marCr;
	}
	/**
	 * @return the aprCr
	 */
	public BigDecimal getAprCr() {
		return aprCr;
	}
	/**
	 * @param aprCr the aprCr to set
	 */
	public void setAprCr(BigDecimal aprCr) {
		this.aprCr = aprCr;
	}
	/**
	 * @return the mayCr
	 */
	public BigDecimal getMayCr() {
		return mayCr;
	}
	/**
	 * @param mayCr the mayCr to set
	 */
	public void setMayCr(BigDecimal mayCr) {
		this.mayCr = mayCr;
	}
	/**
	 * @return the junCr
	 */
	public BigDecimal getJunCr() {
		return junCr;
	}
	/**
	 * @param junCr the junCr to set
	 */
	public void setJunCr(BigDecimal junCr) {
		this.junCr = junCr;
	}
	/**
	 * @return the julCr
	 */
	public BigDecimal getJulCr() {
		return julCr;
	}
	/**
	 * @param julCr the julCr to set
	 */
	public void setJulCr(BigDecimal julCr) {
		this.julCr = julCr;
	}
	/**
	 * @return the augCr
	 */
	public BigDecimal getAugCr() {
		return augCr;
	}
	/**
	 * @param augCr the augCr to set
	 */
	public void setAugCr(BigDecimal augCr) {
		this.augCr = augCr;
	}
	/**
	 * @return the septCr
	 */
	public BigDecimal getSeptCr() {
		return septCr;
	}
	/**
	 * @param septCr the septCr to set
	 */
	public void setSeptCr(BigDecimal septCr) {
		this.septCr = septCr;
	}
	/**
	 * @return the octCr
	 */
	public BigDecimal getOctCr() {
		return octCr;
	}
	/**
	 * @param octCr the octCr to set
	 */
	public void setOctCr(BigDecimal octCr) {
		this.octCr = octCr;
	}
	/**
	 * @return the novCr
	 */
	public BigDecimal getNovCr() {
		return novCr;
	}
	/**
	 * @param novCr the novCr to set
	 */
	public void setNovCr(BigDecimal novCr) {
		this.novCr = novCr;
	}
	/**
	 * @return the decCr
	 */
	public BigDecimal getDecCr() {
		return decCr;
	}
	/**
	 * @param decCr the decCr to set
	 */
	public void setDecCr(BigDecimal decCr) {
		this.decCr = decCr;
	}
	/**
	 * @return the janDb
	 */
	public BigDecimal getJanDb() {
		return janDb;
	}
	/**
	 * @param janDb the janDb to set
	 */
	public void setJanDb(BigDecimal janDb) {
		this.janDb = janDb;
	}
	/**
	 * @return the febDb
	 */
	public BigDecimal getFebDb() {
		return febDb;
	}
	/**
	 * @param febDb the febDb to set
	 */
	public void setFebDb(BigDecimal febDb) {
		this.febDb = febDb;
	}
	/**
	 * @return the marDb
	 */
	public BigDecimal getMarDb() {
		return marDb;
	}
	/**
	 * @param marDb the marDb to set
	 */
	public void setMarDb(BigDecimal marDb) {
		this.marDb = marDb;
	}
	/**
	 * @return the aprDb
	 */
	public BigDecimal getAprDb() {
		return aprDb;
	}
	/**
	 * @param aprDb the aprDb to set
	 */
	public void setAprDb(BigDecimal aprDb) {
		this.aprDb = aprDb;
	}
	/**
	 * @return the mayDb
	 */
	public BigDecimal getMayDb() {
		return mayDb;
	}
	/**
	 * @param mayDb the mayDb to set
	 */
	public void setMayDb(BigDecimal mayDb) {
		this.mayDb = mayDb;
	}
	/**
	 * @return the junDb
	 */
	public BigDecimal getJunDb() {
		return junDb;
	}
	/**
	 * @param junDb the junDb to set
	 */
	public void setJunDb(BigDecimal junDb) {
		this.junDb = junDb;
	}
	/**
	 * @return the julDb
	 */
	public BigDecimal getJulDb() {
		return julDb;
	}
	/**
	 * @param julDb the julDb to set
	 */
	public void setJulDb(BigDecimal julDb) {
		this.julDb = julDb;
	}
	/**
	 * @return the augDb
	 */
	public BigDecimal getAugDb() {
		return augDb;
	}
	/**
	 * @param augDb the augDb to set
	 */
	public void setAugDb(BigDecimal augDb) {
		this.augDb = augDb;
	}
	/**
	 * @return the septDb
	 */
	public BigDecimal getSeptDb() {
		return septDb;
	}
	/**
	 * @param septDb the septDb to set
	 */
	public void setSeptDb(BigDecimal septDb) {
		this.septDb = septDb;
	}
	/**
	 * @return the octDb
	 */
	public BigDecimal getOctDb() {
		return octDb;
	}
	/**
	 * @param octDb the octDb to set
	 */
	public void setOctDb(BigDecimal octDb) {
		this.octDb = octDb;
	}
	/**
	 * @return the novDb
	 */
	public BigDecimal getNovDb() {
		return novDb;
	}
	/**
	 * @param novDb the novDb to set
	 */
	public void setNovDb(BigDecimal novDb) {
		this.novDb = novDb;
	}
	/**
	 * @return the decDb
	 */
	public BigDecimal getDecDb() {
		return decDb;
	}
	/**
	 * @param decDb the decDb to set
	 */
	public void setDecDb(BigDecimal decDb) {
		this.decDb = decDb;
	}
	/**
	 * @return the q1Cr
	 */
	public BigDecimal getQ1Cr() {
		return q1Cr;
	}
	/**
	 * @param q1Cr the q1Cr to set
	 */
	public void setQ1Cr(BigDecimal q1Cr) {
		this.q1Cr = q1Cr;
	}
	/**
	 * @return the q2Cr
	 */
	public BigDecimal getQ2Cr() {
		return q2Cr;
	}
	/**
	 * @param q2Cr the q2Cr to set
	 */
	public void setQ2Cr(BigDecimal q2Cr) {
		this.q2Cr = q2Cr;
	}
	/**
	 * @return the q3Cr
	 */
	public BigDecimal getQ3Cr() {
		return q3Cr;
	}
	/**
	 * @param q3Cr the q3Cr to set
	 */
	public void setQ3Cr(BigDecimal q3Cr) {
		this.q3Cr = q3Cr;
	}
	/**
	 * @return the q4Cr
	 */
	public BigDecimal getQ4Cr() {
		return q4Cr;
	}
	/**
	 * @param q4Cr the q4Cr to set
	 */
	public void setQ4Cr(BigDecimal q4Cr) {
		this.q4Cr = q4Cr;
	}
	/**
	 * @return the q1Db
	 */
	public BigDecimal getQ1Db() {
		return q1Db;
	}
	/**
	 * @param q1Db the q1Db to set
	 */
	public void setQ1Db(BigDecimal q1Db) {
		this.q1Db = q1Db;
	}
	/**
	 * @return the q2Db
	 */
	public BigDecimal getQ2Db() {
		return q2Db;
	}
	/**
	 * @param q2Db the q2Db to set
	 */
	public void setQ2Db(BigDecimal q2Db) {
		this.q2Db = q2Db;
	}
	/**
	 * @return the q3Db
	 */
	public BigDecimal getQ3Db() {
		return q3Db;
	}
	/**
	 * @param q3Db the q3Db to set
	 */
	public void setQ3Db(BigDecimal q3Db) {
		this.q3Db = q3Db;
	}
	/**
	 * @return the q4Db
	 */
	public BigDecimal getQ4Db() {
		return q4Db;
	}
	/**
	 * @param q4Db the q4Db to set
	 */
	public void setQ4Db(BigDecimal q4Db) {
		this.q4Db = q4Db;
	}
	/**
	 * @return the productID
	 */
	public int getProductID() {
		return productID;
	}
	/**
	 * @param productID the productID to set
	 */
	public void setProductID(int productID) {
		this.productID = productID;
	}
	/**
	 * @return the janQty
	 */
	public BigDecimal getJanQty() {
		return janQty;
	}
	/**
	 * @param janQty the janQty to set
	 */
	public void setJanQty(BigDecimal janQty) {
		this.janQty = janQty;
	}
	/**
	 * @return the febQty
	 */
	public BigDecimal getFebQty() {
		return febQty;
	}
	/**
	 * @param febQty the febQty to set
	 */
	public void setFebQty(BigDecimal febQty) {
		this.febQty = febQty;
	}
	/**
	 * @return the marQty
	 */
	public BigDecimal getMarQty() {
		return marQty;
	}
	/**
	 * @param marQty the marQty to set
	 */
	public void setMarQty(BigDecimal marQty) {
		this.marQty = marQty;
	}
	/**
	 * @return the aprQty
	 */
	public BigDecimal getAprQty() {
		return aprQty;
	}
	/**
	 * @param aprQty the aprQty to set
	 */
	public void setAprQty(BigDecimal aprQty) {
		this.aprQty = aprQty;
	}
	/**
	 * @return the mayQty
	 */
	public BigDecimal getMayQty() {
		return mayQty;
	}
	/**
	 * @param mayQty the mayQty to set
	 */
	public void setMayQty(BigDecimal mayQty) {
		this.mayQty = mayQty;
	}
	/**
	 * @return the junQty
	 */
	public BigDecimal getJunQty() {
		return junQty;
	}
	/**
	 * @param junQty the junQty to set
	 */
	public void setJunQty(BigDecimal junQty) {
		this.junQty = junQty;
	}
	/**
	 * @return the julQty
	 */
	public BigDecimal getJulQty() {
		return julQty;
	}
	/**
	 * @param julQty the julQty to set
	 */
	public void setJulQty(BigDecimal julQty) {
		this.julQty = julQty;
	}
	/**
	 * @return the augQty
	 */
	public BigDecimal getAugQty() {
		return augQty;
	}
	/**
	 * @param augQty the augQty to set
	 */
	public void setAugQty(BigDecimal augQty) {
		this.augQty = augQty;
	}
	/**
	 * @return the septQty
	 */
	public BigDecimal getSeptQty() {
		return septQty;
	}
	/**
	 * @param septQty the septQty to set
	 */
	public void setSeptQty(BigDecimal septQty) {
		this.septQty = septQty;
	}
	/**
	 * @return the octQty
	 */
	public BigDecimal getOctQty() {
		return octQty;
	}
	/**
	 * @param octQty the octQty to set
	 */
	public void setOctQty(BigDecimal octQty) {
		this.octQty = octQty;
	}
	/**
	 * @return the novQty
	 */
	public BigDecimal getNovQty() {
		return novQty;
	}
	/**
	 * @param novQty the novQty to set
	 */
	public void setNovQty(BigDecimal novQty) {
		this.novQty = novQty;
	}
	/**
	 * @return the decQty
	 */
	public BigDecimal getDecQty() {
		return decQty;
	}
	/**
	 * @param decQty the decQty to set
	 */
	public void setDecQty(BigDecimal decQty) {
		this.decQty = decQty;
	}
	
	
	
	
	
}
