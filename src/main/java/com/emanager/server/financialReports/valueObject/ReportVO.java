package com.emanager.server.financialReports.valueObject;

import java.math.BigDecimal;
import java.util.List;

import com.emanager.server.adminReports.valueObject.PrintReportVO;
public class ReportVO extends PrintReportVO
{

	private int societyID;
	private int memberID;
	private int ledgerID;
	private int groupID;
	private List incomeTxList;
	private List expenseTxList;
	private List accountBalanceList;
	private List assetsList;
	private List liabilityList;
	private List balanceSheetList;
	private String fromDate;
	private String uptDate;
	private String excessType;
	private String prevExcessType;
	private BigDecimal excessAmount;
	private BigDecimal prevExcessAmount;
	private List incomeDetailsTxList;
	private List expenseDetailsTxList;
	private BigDecimal incomeTotal;
	private BigDecimal expenseTotal;
	private BigDecimal prevIncTotal;
	private BigDecimal prevExpTotal;
	private BigDecimal incOverExp;
	private BigDecimal expOverInc;
	private BigDecimal prevIncOverExp;
	private BigDecimal prevExpOverInc;
	private String rptFromDate;
	private String rptUptoDate;
	private List liabilityDetailsList;
	private List assetsDetailsList;
	private int categoryID;
	private int rptID;
	private int id;
	private int parentID;
	
	public List getAccountBalanceList() {
		return accountBalanceList;
	}
	public void setAccountBalanceList(List accountBalanceList) {
		this.accountBalanceList = accountBalanceList;
	}
	public List getAssetsList() {
		return assetsList;
	}
	public void setAssetsList(List assetsList) {
		this.assetsList = assetsList;
	}
	public List getExpenseTxList() {
		return expenseTxList;
	}
	public void setExpenseTxList(List expenseTxList) {
		this.expenseTxList = expenseTxList;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public int getGroupID() {
		return groupID;
	}
	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}
	public List getIncomeTxList() {
		return incomeTxList;
	}
	public void setIncomeTxList(List incomeTxList) {
		this.incomeTxList = incomeTxList;
	}
	public int getLedgerID() {
		return ledgerID;
	}
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	public List getLiabilityList() {
		return liabilityList;
	}
	public void setLiabilityList(List liabilityList) {
		this.liabilityList = liabilityList;
	}
	public int getMemberID() {
		return memberID;
	}
	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	public String getUptDate() {
		return uptDate;
	}
	public void setUptDate(String uptDate) {
		this.uptDate = uptDate;
	}
	public List getBalanceSheetList() {
		return balanceSheetList;
	}
	public void setBalanceSheetList(List balanceSheetList) {
		this.balanceSheetList = balanceSheetList;
	}
	public BigDecimal getExcessAmount() {
		return excessAmount;
	}
	public void setExcessAmount(BigDecimal excessAmount) {
		this.excessAmount = excessAmount;
	}
	public String getExcessType() {
		return excessType;
	}
	public void setExcessType(String excessType) {
		this.excessType = excessType;
	}
	public List getExpenseDetailsTxList() {
		return expenseDetailsTxList;
	}
	public void setExpenseDetailsTxList(List expenseDetailsTxList) {
		this.expenseDetailsTxList = expenseDetailsTxList;
	}
	public List getIncomeDetailsTxList() {
		return incomeDetailsTxList;
	}
	public void setIncomeDetailsTxList(List incomeDetailsTxList) {
		this.incomeDetailsTxList = incomeDetailsTxList;
	}

	public BigDecimal getExpenseTotal() {
		return expenseTotal;
	}
	public void setExpenseTotal(BigDecimal expenseTotal) {
		this.expenseTotal = expenseTotal;
	}
	public BigDecimal getExpOverInc() {
		return expOverInc;
	}
	public void setExpOverInc(BigDecimal expOverInc) {
		this.expOverInc = expOverInc;
	}
	public BigDecimal getIncomeTotal() {
		return incomeTotal;
	}
	public void setIncomeTotal(BigDecimal incomeTotal) {
		this.incomeTotal = incomeTotal;
	}
	public BigDecimal getIncOverExp() {
		return incOverExp;
	}
	public void setIncOverExp(BigDecimal incOverExp) {
		this.incOverExp = incOverExp;
	}
	public String getRptFromDate() {
		return rptFromDate;
	}
	public void setRptFromDate(String rptFromDate) {
		this.rptFromDate = rptFromDate;
	}
	public String getRptUptoDate() {
		return rptUptoDate;
	}
	public void setRptUptoDate(String rptUptoDate) {
		this.rptUptoDate = rptUptoDate;
	}
	public List getLiabilityDetailsList() {
		return liabilityDetailsList;
	}
	public void setLiabilityDetailsList(List liabilityDetailsList) {
		this.liabilityDetailsList = liabilityDetailsList;
	}
	public List getAssetsDetailsList() {
		return assetsDetailsList;
	}
	public void setAssetsDetailsList(List assetsDetailsList) {
		this.assetsDetailsList = assetsDetailsList;
	}
	/**
	 * @return the categoryID
	 */
	public int getCategoryID() {
		return categoryID;
	}
	/**
	 * @param categoryID the categoryID to set
	 */
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}
	/**
	 * @return the rptID
	 */
	public int getRptID() {
		return rptID;
	}
	/**
	 * @param rptID the rptID to set
	 */
	public void setRptID(int rptID) {
		this.rptID = rptID;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the parentID
	 */
	public int getParentID() {
		return parentID;
	}
	/**
	 * @param parentID the parentID to set
	 */
	public void setParentID(int parentID) {
		this.parentID = parentID;
	}
	/**
	 * @return the prevExcessType
	 */
	public String getPrevExcessType() {
		return prevExcessType;
	}
	/**
	 * @param prevExcessType the prevExcessType to set
	 */
	public void setPrevExcessType(String prevExcessType) {
		this.prevExcessType = prevExcessType;
	}
	/**
	 * @return the prevIncTotal
	 */
	public BigDecimal getPrevIncTotal() {
		return prevIncTotal;
	}
	/**
	 * @param prevIncTotal the prevIncTotal to set
	 */
	public void setPrevIncTotal(BigDecimal prevIncTotal) {
		this.prevIncTotal = prevIncTotal;
	}
	/**
	 * @return the prevExpTotal
	 */
	public BigDecimal getPrevExpTotal() {
		return prevExpTotal;
	}
	/**
	 * @param prevExpTotal the prevExpTotal to set
	 */
	public void setPrevExpTotal(BigDecimal prevExpTotal) {
		this.prevExpTotal = prevExpTotal;
	}
	/**
	 * @return the prevExcessAmount
	 */
	public BigDecimal getPrevExcessAmount() {
		return prevExcessAmount;
	}
	/**
	 * @param prevExcessAmount the prevExcessAmount to set
	 */
	public void setPrevExcessAmount(BigDecimal prevExcessAmount) {
		this.prevExcessAmount = prevExcessAmount;
	}
	/**
	 * @return the prevIncOverExp
	 */
	public BigDecimal getPrevIncOverExp() {
		return prevIncOverExp;
	}
	/**
	 * @param prevIncOverExp the prevIncOverExp to set
	 */
	public void setPrevIncOverExp(BigDecimal prevIncOverExp) {
		this.prevIncOverExp = prevIncOverExp;
	}
	/**
	 * @return the prevExpOverInc
	 */
	public BigDecimal getPrevExpOverInc() {
		return prevExpOverInc;
	}
	/**
	 * @param prevExpOverInc the prevExpOverInc to set
	 */
	public void setPrevExpOverInc(BigDecimal prevExpOverInc) {
		this.prevExpOverInc = prevExpOverInc;
	}
	
}
