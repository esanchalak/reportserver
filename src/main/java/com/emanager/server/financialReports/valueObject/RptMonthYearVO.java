package com.emanager.server.financialReports.valueObject;

import java.math.BigDecimal;

public class RptMonthYearVO {
   

    private BigDecimal txAmount;

    private String txDate;

    private String societyID;

    private String categoryName;

    private String c_head_name;

    private BigDecimal total;

    private String monthName;

    private int counter;

    private BigDecimal openingBalance;

    private BigDecimal cashInHand;

    private String txType;
    
    private String categoryID;
    
    private String unitType;
    
    private String apartmentName;
    
    private BigDecimal totalBalance;
    
    private String memberName;
    
    private String invoice;
    
    private BigDecimal monthlyMmc;
    
    private BigDecimal outstandBal;
    
    private BigDecimal differnce;
    
    private BigDecimal expectedMMC;
    
    private int isChecked;
    
    private int memberID;
    
    private String fromDate;
    
    private String uptoDate;
    
    private BigDecimal discount;
    
    private String refNo;
    
    private String category;
    
    private String comments;
    
    private BigDecimal credit;
    
    private BigDecimal debit;
    
    private BigDecimal creditPrev;
    
    private BigDecimal debitPrev;
    
    private BigDecimal refund;
    
    private BigDecimal sinkingFunds;
    
    private BigDecimal rMCharges;
    
    private String accName;
    
    private int aptID;
    
    private BigDecimal otherCharges;
    
    private java.sql.Date startDate;    
    
    private java.sql.Date EndDate;    
    
    private BigDecimal totalPaidCharges;
    
    private String expAmntComments;
    
    private String fullName;
    private int isRented;
    private int unitID;
    
    private int ledgerID;
    
    private BigDecimal currentBalance;
    
    private BigDecimal prevBalance;
    
    private BigDecimal diffInCurrent;


   /* public String getExpAmntComments() {
        return expAmntComments;
    }

    public void setExpAmntComments(String expAmntComments) {
        this.expAmntComments = expAmntComments;
    }

    public BigDecimal getTotalPaidCharges() {
        return totalPaidCharges;
    }

    public void setTotalPaidCharges(BigDecimal totalPaidCharges) {
        this.totalPaidCharges = totalPaidCharges;
    }
*/
    public java.sql.Date getEndDate() {
        return EndDate;
    }

    public void setEndDate(java.sql.Date endDate) {
        EndDate = endDate;
    }

    public java.sql.Date getStartDate() {
        return startDate;
    }

    public void setStartDate(java.sql.Date startDate) {
        this.startDate = startDate;
    }

    public int getAptID() {
        return aptID;
    }

    public void setAptID(int aptID) {
        this.aptID = aptID;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getTxType() {
        return txType;
    }

    public void setTxType(String txType) {
        this.txType = txType;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

   
    public String getSocietyID() {
        return societyID;
    }

    public void setSocietyID(String societyID) {
        this.societyID = societyID;
    }

    public BigDecimal getTxAmount() {
        return txAmount;
    }

    public void setTxAmount(BigDecimal txAmount) {
        this.txAmount = txAmount;
    }

    public String getTxDate() {
        return txDate;
    }

    public void setTxDate(String txDate) {
        this.txDate = txDate;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;

    }

    public String getC_head_name() {
        return c_head_name;
    }

    public void setC_head_name(String c_head_name) {
        this.c_head_name = c_head_name;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

  /*  public BigDecimal getCashInHand() {
        return cashInHand;
    }

    public void setCashInHand(BigDecimal cashInHand) {
        this.cashInHand = cashInHand;
    }*/

    public BigDecimal getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(BigDecimal openingBalance) {
        this.openingBalance = openingBalance;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }
/*
    public String getApartmentName() {
        return apartmentName;
    }

    public void setApartmentName(String apartmentName) {
        this.apartmentName = apartmentName;
    }

    

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }
*/
    public BigDecimal getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(BigDecimal totalBalance) {
        this.totalBalance = totalBalance;
    }
    
    public BigDecimal getDiffernce() {
        return differnce;
    }

    public void setDiffernce(BigDecimal differnce) {
        this.differnce = differnce;
    }
    
/*
    public BigDecimal getMonthlyMmc() {
        return monthlyMmc;
    }

    

  

    public BigDecimal getOutstandBal() {
        return outstandBal;
    }

    public void setOutstandBal(BigDecimal outstandBal) {
        this.outstandBal = outstandBal;
    }

    public void setMonthlyMmc(BigDecimal monthlyMmc) {
        this.monthlyMmc = monthlyMmc;
    }

    public BigDecimal getExpectedMMC() {
        return expectedMMC;
    }

    public void setExpectedMMC(BigDecimal expectedMMC) {
        this.expectedMMC = expectedMMC;
    }
*/
    public int getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(int isChecked) {
        this.isChecked = isChecked;
    }

    public int getMemberID() {
        return memberID;
    }

    public void setMemberID(int memberID) {
        this.memberID = memberID;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getUptoDate() {
        return uptoDate;
    }

    public void setUptoDate(String uptoDate) {
        this.uptoDate = uptoDate;
    }
/*
    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }
*/
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        this.credit = credit;
    }

    public BigDecimal getDebit() {
        return debit;
    }

    public void setDebit(BigDecimal debit) {
        this.debit = debit;
    }

    /*public BigDecimal getRefund() {
        return refund;
    }

    public void setRefund(BigDecimal refund) {
        this.refund = refund;
    }

    public BigDecimal getRMCharges() {
        return rMCharges;
    }

    public void setRMCharges(BigDecimal charges) {
        rMCharges = charges;
    }

    public BigDecimal getSinkingFunds() {
        return sinkingFunds;
    }

    public void setSinkingFunds(BigDecimal sinkingFunds) {
        this.sinkingFunds = sinkingFunds;
    }*/

    public String getAccName() {
        return accName;
    }

    public void setAccName(String accName) {
        this.accName = accName;
    }
/*
    public BigDecimal getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(BigDecimal otherCharges) {
        this.otherCharges = otherCharges;
    }

	

	public int getIsRented() {
		return isRented;
	}

	public void setIsRented(int isRented) {
		this.isRented = isRented;
	}

	public int getUnitID() {
		return unitID;
	}

	public void setUnitID(int unitID) {
		this.unitID = unitID;
	}
*/
	public int getLedgerID() {
		return ledgerID;
	}

	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}

	public BigDecimal getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(BigDecimal currentBalance) {
		this.currentBalance = currentBalance;
	}

	public BigDecimal getPrevBalance() {
		return prevBalance;
	}

	public void setPrevBalance(BigDecimal prevBalance) {
		this.prevBalance = prevBalance;
	}

	public BigDecimal getCreditPrev() {
		return creditPrev;
	}

	public void setCreditPrev(BigDecimal creditPrev) {
		this.creditPrev = creditPrev;
	}

	public BigDecimal getDebitPrev() {
		return debitPrev;
	}

	public void setDebitPrev(BigDecimal debitPrev) {
		this.debitPrev = debitPrev;
	}

	/**
	 * @return the diffInCurrent
	 */
	public BigDecimal getDiffInCurrent() {
		return diffInCurrent;
	}

	/**
	 * @param diffInCurrent the diffInCurrent to set
	 */
	public void setDiffInCurrent(BigDecimal diffInCurrent) {
		this.diffInCurrent = diffInCurrent;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
    

}

