package com.emanager.server.financialReports.valueObject;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import com.emanager.server.adminReports.valueObject.PrintReportVO;
import com.emanager.server.society.valueObject.BankInfoVO;
public class RptTransactionVO extends PrintReportVO
{


	private String member_name;
	private String ledgerName;
	private Date tx_date;
	private BigDecimal tx_ammount;
	private BigDecimal societyMonthlyCharges;
	private String mobile;
	private Date current_date;
	private BigDecimal due;
	private java.util.Date from_date;
	private java.util.Date tx_to_date;	
	private java.util.Date display_date;
	private String mmcPaidUptodate;
	private String SFPaidUptodate;
	private String RMPaidUptodate;
	private BigDecimal total;
	private String address;
	private BigDecimal lateFees;
	private BigDecimal rentalFees;
	private BigDecimal actualTotal;
	private int is_rented;
	private int aptID;
	private int unitID;
	private int isChecked;
	private int id;
	private String commentsForRentFee;
	private String commentForLateFee;
	private String commentForDue;
	private String calculationMode;
	private int member_id;
	private String email;
	private String actualToatalInString;
	private String displayDate;
	private int categoryID;
	private String arrearsComment;
	private int monthParam;
	private int dayParam;
	private BigDecimal sinkingFunds;
	private BigDecimal sFRate;
	private BigDecimal rMCharges;
	private BigDecimal rMRate;
	private BigDecimal serviceTax;
	private BigDecimal sqFeets;
	private BigDecimal balance;
	private BigDecimal arrears;
	private BigDecimal arrearsPaid;
	private BigDecimal lateFeeRate;
	private int isLateFee;
	private BigDecimal lateFeeForMMC;
	private BigDecimal lateFeeForSF;
	private BigDecimal lateFeeForRM;
	private BigDecimal lateFeeForOther;
	private BigDecimal dueForMMC;
	private BigDecimal dueForSF;
	private BigDecimal dueForRM;
	private BigDecimal dueForOther;
	private BigDecimal dueForLegalFee;
	private BigDecimal dueForInfra;
	private BigDecimal dueForMeter;
	private BigDecimal dueForContigency;
	private BigDecimal dueForCarParking;
	private BigDecimal dueForTransfer;
	private BigDecimal dueForCorpus;
	private BigDecimal dueForSocietyFrm;
	private BigDecimal dueForWaterCharges;
	private BigDecimal dueForLift;
	private BigDecimal unitCost;
	private String lateFeeType;
	private String frequency;
	private int ledgerID;
	private BigDecimal openingBalance;
	private BigDecimal closingBalance;
	private String paymentDate;
	private int statusCode;
	private BigDecimal creditAmt;
	private BigDecimal debitAmt;
	private List dueDetails;
	private String buildingId;
	private String buildingName;
	private int aptCount;
	private List dueSummary;
	private BankInfoVO bankInfoVO;

	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public java.util.Date getDisplay_date() {
		return display_date;
	}
	public void setDisplay_date(java.util.Date Date) {
		this.display_date = Date;
	
	}
	public BigDecimal getDue() {
		return due;
	}
	public void setDue(BigDecimal due) {
		this.due = due;
	}
	
	public Date getCurrent_date() {
		return current_date;
	}
	public void setCurrent_date(Date current_date) {
		this.current_date = current_date;
	
	}
	public String getMember_name() {
		return member_name;
	}
	public void setMember_name(String member_name) {
		this.member_name = member_name;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	public BigDecimal getSocietyMonthlyCharges() {
		return societyMonthlyCharges;
	}
	public void setSocietyMonthlyCharges(BigDecimal societyMonthlyCharges) {
		this.societyMonthlyCharges = societyMonthlyCharges;
	}
	public BigDecimal getTx_ammount() {
		return tx_ammount;
	}
	public void setTx_ammount(BigDecimal tx_ammount) {
		this.tx_ammount = tx_ammount;
	}
	public Date getTx_date() {
		return tx_date;
	}
	public void setTx_date(Date tx_date) {
		this.tx_date = tx_date;
	}
	public java.util.Date getTx_to_date() {
		return tx_to_date;
	}
	public void setTx_to_date(java.util.Date tx_to_date) {
		this.tx_to_date = tx_to_date;
	}
	public BigDecimal getActualTotal() {
		return actualTotal;
	}
	public void setActualTotal(BigDecimal actualTotal) {
		this.actualTotal = actualTotal;
	}
	public int getIs_rented() {
		return is_rented;
	}
	public void setIs_rented(int is_rented) {
		this.is_rented = is_rented;
	}
	public BigDecimal getLateFees() {
		return lateFees;
	}
	public void setLateFees(BigDecimal lateFees) {
		this.lateFees = lateFees;
	}
	public BigDecimal getRentalFees() {
		return rentalFees;
	}
	public void setRentalFees(BigDecimal rentalFees) {
		this.rentalFees = rentalFees;
	}
	public int getAptID() {
		return aptID;
	}
	public void setAptID(int aptID) {
		this.aptID = aptID;
	}
	public int getIsChecked() {
		return isChecked;
	}
	public void setIsChecked(int isChecked) {
		this.isChecked = isChecked;
	}
	
	public String getCommentForLateFee() {
		return commentForLateFee;
	}
	public void setCommentForLateFee(String commentForLateFee) {
		this.commentForLateFee = commentForLateFee;
	}
	public String getCommentsForRentFee() {
		return commentsForRentFee;
	}
	public void setCommentsForRentFee(String commentsForRentFee) {
		this.commentsForRentFee = commentsForRentFee;
	}
	public String getCommentForDue() {
		return commentForDue;
	}
	public void setCommentForDue(String commentForDue) {
		this.commentForDue = commentForDue;
	}
	public String getCalculationMode() {
		return calculationMode;
	}
	public void setCalculationMode(String calculationMode) {
		this.calculationMode = calculationMode;
	}
	public int getMember_id() {
		return member_id;
	}
	public void setMember_id(int member_id) {
		this.member_id = member_id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getActualToatalInString() {
		return actualToatalInString;
	}
	public void setActualToatalInString(String actualToatalInString) {
		this.actualToatalInString = actualToatalInString;
	}
	public String getDisplayDate() {
		return displayDate;
	}
	public void setDisplayDate(String displayDate) {
		this.displayDate = displayDate;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public BigDecimal getArrears() {
		return arrears;
	}
	public void setArrears(BigDecimal arrears) {
		this.arrears = arrears;
	}
	public BigDecimal getArrearsPaid() {
		return arrearsPaid;
	}
	public void setArrearsPaid(BigDecimal arrearsPaid) {
		this.arrearsPaid = arrearsPaid;
	}
	public int getDayParam() {
		return dayParam;
	}
	public void setDayParam(int dayParam) {
		this.dayParam = dayParam;
	}
	public int getMonthParam() {
		return monthParam;
	}
	public void setMonthParam(int monthParam) {
		this.monthParam = monthParam;
	}
	public BigDecimal getRMCharges() {
		return rMCharges;
	}
	public void setRMCharges(BigDecimal charges) {
		rMCharges = charges;
	}
	public BigDecimal getSinkingFunds() {
		return sinkingFunds;
	}
	public void setSinkingFunds(BigDecimal sinkingFunds) {
		this.sinkingFunds = sinkingFunds;
	}
	public String getArrearsComment() {
		return arrearsComment;
	}
	public void setArrearsComment(String arrearsComment) {
		this.arrearsComment = arrearsComment;
	}
	public BigDecimal getServiceTax() {
		return serviceTax;
	}
	public void setServiceTax(BigDecimal serviceTax) {
		this.serviceTax = serviceTax;
	}
	public int getUnitID() {
		return unitID;
	}
	public void setUnitID(int unitID) {
		this.unitID = unitID;
	}
	public BigDecimal getSqFeets() {
		return sqFeets;
	}
	public void setSqFeets(BigDecimal sqFeets) {
		this.sqFeets = sqFeets;
	}
	public int getCategoryID() {
		return categoryID;
	}
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}
	public int getIsLateFee() {
		return isLateFee;
	}
	public void setIsLateFee(int isLateFee) {
		this.isLateFee = isLateFee;
	}
	public BigDecimal getLateFeeForMMC() {
		return lateFeeForMMC;
	}
	public void setLateFeeForMMC(BigDecimal lateFeeForMMC) {
		this.lateFeeForMMC = lateFeeForMMC;
	}
	public BigDecimal getLateFeeForRM() {
		return lateFeeForRM;
	}
	public void setLateFeeForRM(BigDecimal lateFeeForRM) {
		this.lateFeeForRM = lateFeeForRM;
	}
	public BigDecimal getLateFeeForSF() {
		return lateFeeForSF;
	}
	public void setLateFeeForSF(BigDecimal lateFeeForSF) {
		this.lateFeeForSF = lateFeeForSF;
	}
	public BigDecimal getLateFeeRate() {
		return lateFeeRate;
	}
	public void setLateFeeRate(BigDecimal lateFeeRate) {
		this.lateFeeRate = lateFeeRate;
	}
	public String getLateFeeType() {
		return lateFeeType;
	}
	public void setLateFeeType(String lateFeeType) {
		this.lateFeeType = lateFeeType;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public java.util.Date getFrom_date() {
		return from_date;
	}
	public void setFrom_date(java.util.Date from_date) {
		this.from_date = from_date;
	}
	public BigDecimal getDueForMMC() {
		return dueForMMC;
	}
	public void setDueForMMC(BigDecimal dueForMMC) {
		this.dueForMMC = dueForMMC;
	}
	public BigDecimal getDueForOther() {
		return dueForOther;
	}
	public void setDueForOther(BigDecimal dueForOther) {
		this.dueForOther = dueForOther;
	}
	public BigDecimal getDueForRM() {
		return dueForRM;
	}
	public void setDueForRM(BigDecimal dueForRM) {
		this.dueForRM = dueForRM;
	}
	public BigDecimal getDueForSF() {
		return dueForSF;
	}
	public void setDueForSF(BigDecimal dueForSF) {
		this.dueForSF = dueForSF;
	}
	public BigDecimal getLateFeeForOther() {
		return lateFeeForOther;
	}
	public void setLateFeeForOther(BigDecimal lateFeeForOther) {
		this.lateFeeForOther = lateFeeForOther;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMmcPaidUptodate() {
		return mmcPaidUptodate;
	}
	public void setMmcPaidUptodate(String mmcPaidUptodate) {
		this.mmcPaidUptodate = mmcPaidUptodate;
	}
	public String getRMPaidUptodate() {
		return RMPaidUptodate;
	}
	public void setRMPaidUptodate(String paidUptodate) {
		RMPaidUptodate = paidUptodate;
	}
	public String getSFPaidUptodate() {
		return SFPaidUptodate;
	}
	public void setSFPaidUptodate(String paidUptodate) {
		SFPaidUptodate = paidUptodate;
	}
	public BigDecimal getUnitCost() {
		return unitCost;
	}
	public void setUnitCost(BigDecimal unitCost) {
		this.unitCost = unitCost;
	}
	public BigDecimal getRMRate() {
		return rMRate;
	}
	public void setRMRate(BigDecimal rate) {
		this.rMRate = rate;
	}
	public BigDecimal getSFRate() {
		return sFRate;
	}
	public void setSFRate(BigDecimal rate) {
		this.sFRate = rate;
	}
	public int getLedgerID() {
		return ledgerID;
	}
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	public BigDecimal getClosingBalance() {
		return closingBalance;
	}
	public void setClosingBalance(BigDecimal closingBalance) {
		this.closingBalance = closingBalance;
	}
	public BigDecimal getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(BigDecimal openingBalance) {
		this.openingBalance = openingBalance;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public BigDecimal getDueForCarParking() {
		return dueForCarParking;
	}
	public void setDueForCarParking(BigDecimal dueForCarParking) {
		this.dueForCarParking = dueForCarParking;
	}
	public BigDecimal getDueForContigency() {
		return dueForContigency;
	}
	public void setDueForContigency(BigDecimal dueForContigency) {
		this.dueForContigency = dueForContigency;
	}
	public BigDecimal getDueForInfra() {
		return dueForInfra;
	}
	public void setDueForInfra(BigDecimal dueForInfra) {
		this.dueForInfra = dueForInfra;
	}
	public BigDecimal getDueForLegalFee() {
		return dueForLegalFee;
	}
	public void setDueForLegalFee(BigDecimal dueForLegalFee) {
		this.dueForLegalFee = dueForLegalFee;
	}
	public BigDecimal getDueForMeter() {
		return dueForMeter;
	}
	public void setDueForMeter(BigDecimal dueForMeter) {
		this.dueForMeter = dueForMeter;
	}
	public BigDecimal getDueForTransfer() {
		return dueForTransfer;
	}
	public void setDueForTransfer(BigDecimal dueForTransfer) {
		this.dueForTransfer = dueForTransfer;
	}
	public String getLedgerName() {
		return ledgerName;
	}
	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}
	public BigDecimal getCreditAmt() {
		return creditAmt;
	}
	public void setCreditAmt(BigDecimal creditAmt) {
		this.creditAmt = creditAmt;
	}
	public BigDecimal getDebitAmt() {
		return debitAmt;
	}
	public void setDebitAmt(BigDecimal debitAmt) {
		this.debitAmt = debitAmt;
	}
	public List getDueDetails() {
		return dueDetails;
	}
	public void setDueDetails(List dueDetails) {
		this.dueDetails = dueDetails;
	}
	public String getBuildingId() {
		return buildingId;
	}
	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}
	public BigDecimal getDueForCorpus() {
		return dueForCorpus;
	}
	public void setDueForCorpus(BigDecimal dueForCorpus) {
		this.dueForCorpus = dueForCorpus;
	}
	public BigDecimal getDueForSocietyFrm() {
		return dueForSocietyFrm;
	}
	public void setDueForSocietyFrm(BigDecimal dueForSocietyFrm) {
		this.dueForSocietyFrm = dueForSocietyFrm;
	}
	public BigDecimal getDueForWaterCharges() {
		return dueForWaterCharges;
	}
	public void setDueForWaterCharges(BigDecimal dueForWaterCharges) {
		this.dueForWaterCharges = dueForWaterCharges;
	}
	
	public List getDueSummary() {
		return dueSummary;
	}
	public void setDueSummary(List dueSummary) {
		this.dueSummary = dueSummary;
	}
	
	public String getBuildingName() {
		return buildingName;
	}
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	public int getAptCount() {
		return aptCount;
	}
	public void setAptCount(int aptCount) {
		this.aptCount = aptCount;
	}
	public BankInfoVO getBankInfoVO() {
		return bankInfoVO;
	}
	public void setBankInfoVO(BankInfoVO bankInfoVO) {
		this.bankInfoVO = bankInfoVO;
	}
	public BigDecimal getDueForLift() {
		return dueForLift;
	}
	public void setDueForLift(BigDecimal dueForLift) {
		this.dueForLift = dueForLift;
	}



	
}
