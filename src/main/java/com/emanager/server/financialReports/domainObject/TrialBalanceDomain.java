package com.emanager.server.financialReports.domainObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.financialReports.dataaccessObject.ReportDAO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;

public class TrialBalanceDomain {
	ReportDAO reportDAO;
	LedgerService ledgerService;
	SocietyService societyService;
	DateUtility dateUtil=new DateUtility();
	Logger logger=Logger.getLogger(TrialBalanceDomain.class);
	
	/*This method is used to get trialBalnce report*/
	public List getTrialBalanceReport(int societyID,String fromDate, String uptoDate){
		
		List<AccountHeadVO> accHeadListList=new ArrayList<AccountHeadVO>();
		List<AccountHeadVO> assetLiabilityList=new ArrayList<AccountHeadVO>();
		List<AccountHeadVO> incExpList=new ArrayList<AccountHeadVO>();
		List<AccountHeadVO> formatedList=new ArrayList<AccountHeadVO>();
		BigDecimal diffInOpening=BigDecimal.ZERO;
		try{
		logger.debug("Entry : public List getTrialBalanceReport(int societyID,String fromDate, String uptoDate)");
		SocietyVO societyVO=new SocietyVO();
		List ledgerList=ledgerService.getLedgerList(societyID, 0);
		List  groupList=new ArrayList();
		AccountHeadVO acVO=(AccountHeadVO) ledgerList.get(0);
		diffInOpening=acVO.getSumOfcreditBal().subtract(acVO.getSumOfDebitBal());
		societyVO=societyService.getSocietyDetails("0", societyID);
		assetLiabilityList=reportDAO.getTrialBalanceReport(societyID, societyVO.getEffectiveDate(), uptoDate,societyVO.getEffectiveDate(),dateUtil.getPrevOrNextDate(fromDate, 1),0,"AL");
		incExpList=reportDAO.getTrialBalanceReport(societyID, fromDate, uptoDate,societyVO.getEffectiveDate(),dateUtil.getPrevOrNextDate(fromDate, 1),0,"IE");
		//groupList=reportDAO.getTrialBalanceReport(societyID, dateUtil.getPrevOrNextDate(fromDate, 1), uptoDate,societyVO.getEffectiveDate());
		
		
		for(int i=0;assetLiabilityList.size()>i;i++){
			List tempGroupList=new ArrayList();
			AccountHeadVO trialBalanceVO=assetLiabilityList.get(i);
			AccountHeadVO temp=new AccountHeadVO();
		
			if((trialBalanceVO.getRootID()==3)||(trialBalanceVO.getRootID()==4)){
				
			
			
			temp=ledgerService.getBalancesForTrialBal(trialBalanceVO);
			
			trialBalanceVO.setDrClsBal(temp.getDrClsBal());
			trialBalanceVO.setCrClsBal(temp.getCrClsBal());
			trialBalanceVO.setDrOpnBal(temp.getDrOpnBal());
			trialBalanceVO.setCrOpnBal(temp.getCrOpnBal());
			
			trialBalanceVO.setObjectList(null);
			BigDecimal drOpnBal=BigDecimal.ZERO;
			BigDecimal crOpnBal=BigDecimal.ZERO;
			BigDecimal drClsBal=BigDecimal.ZERO;
			BigDecimal crClsBal=BigDecimal.ZERO;
		
					
			if((trialBalanceVO.getCrClsBal().compareTo(BigDecimal.ZERO)==0) && ( trialBalanceVO.getCrOpnBal().compareTo(BigDecimal.ZERO)==0) && (trialBalanceVO.getDrClsBal().compareTo(BigDecimal.ZERO)==0) && ( trialBalanceVO.getDrOpnBal().compareTo(BigDecimal.ZERO)==0)){
				assetLiabilityList.remove(i);
				--i;
				}else 
					formatedList.add(trialBalanceVO);
		}
		}

		for(int i=0;incExpList.size()>i;i++){
			List tempGroupList=new ArrayList();
			AccountHeadVO trialBalanceVO=incExpList.get(i);
			AccountHeadVO temp=new AccountHeadVO();
			if((trialBalanceVO.getRootID()==1)||(trialBalanceVO.getRootID()==2)){
			temp=ledgerService.getBalancesForTrialBal(trialBalanceVO);
			
			trialBalanceVO.setDrClsBal(temp.getDrClsBal());
			trialBalanceVO.setCrClsBal(temp.getCrClsBal());
			trialBalanceVO.setDrOpnBal(temp.getDrOpnBal());
			trialBalanceVO.setCrOpnBal(temp.getCrOpnBal());
			//logger.debug("==opneg "+trialBalanceVO.getDrOpnBal()+"   opPos "+trialBalanceVO.getCrOpnBal()+" clNeg "+trialBalanceVO.getDrClsBal()+" clPos "+trialBalanceVO.getCrClsBal());
			trialBalanceVO.setObjectList(null);
			BigDecimal drOpnBal=BigDecimal.ZERO;
			BigDecimal crOpnBal=BigDecimal.ZERO;
			BigDecimal drClsBal=BigDecimal.ZERO;
			BigDecimal crClsBal=BigDecimal.ZERO;
		
			
			if((trialBalanceVO.getCrClsBal().compareTo(BigDecimal.ZERO)==0)  && (trialBalanceVO.getDrClsBal().compareTo(BigDecimal.ZERO)==0) ){
			incExpList.remove(i);
			--i;
			}else 
				formatedList.add(trialBalanceVO);
			}
		}
		
		AccountHeadVO profitLossVO=new AccountHeadVO();
		profitLossVO=ledgerService.getProffitLossBalance(societyID, fromDate, uptoDate,"tb",0,"S");
		formatedList.add(profitLossVO);
		if(diffInOpening.compareTo(BigDecimal.ZERO)!=0){
			AccountHeadVO diffInBalVO=new AccountHeadVO();
			diffInBalVO.setStrAccountPrimaryHead("DIFF IN OPENING BALANCE");
			if(diffInOpening.compareTo(BigDecimal.ZERO)<0){
				diffInBalVO.setCrOpnBal(diffInOpening.abs());
				diffInBalVO.setCrClsBal(diffInOpening.abs());
			}else {
				diffInBalVO.setDrClsBal(diffInOpening);
				diffInBalVO.setDrOpnBal(diffInOpening);
			}
			formatedList.add(diffInBalVO);
		}
		
		logger.debug("Exit : public List getTrialBalanceReport(int societyID,String fromDate, String uptoDate)"+accHeadListList.size());
		}catch (Exception e) {
			logger.error("Exception: public List getTrialBalanceReport(int societyID,String fromDate, String uptoDate) "+e );
		}
		return formatedList;
	}
	
	/*This method is used to get consolidated trialBalnce report*/
	public List getConsolidatedTrialBalanceReport(int societyID,String fromDate, String uptoDate){
		
		List<AccountHeadVO> accHeadListList=new ArrayList<AccountHeadVO>();
		List<AccountHeadVO> formatedList=new ArrayList<AccountHeadVO>();
		List<AccountHeadVO> assetLiabilityList=new ArrayList<AccountHeadVO>();
		List<AccountHeadVO> incExpList=new ArrayList<AccountHeadVO>();
		BigDecimal diffInOpening=BigDecimal.ZERO;
		try{
		logger.debug("Entry : public List getConsolidatedTrialBalanceReport(int societyID,String fromDate, String uptoDate)");
		SocietyVO societyVO=new SocietyVO();
		
		societyVO=societyService.getSocietyDetails("0", societyID);
		assetLiabilityList=reportDAO.getTrialBalanceReport(societyID, societyVO.getEffectiveDate(), uptoDate,societyVO.getEffectiveDate(),dateUtil.getPrevOrNextDate(fromDate, 1),1,"AL");
		incExpList=reportDAO.getTrialBalanceReport(societyID, fromDate, uptoDate,societyVO.getEffectiveDate(),dateUtil.getPrevOrNextDate(fromDate, 1),1,"IE");
		//groupList=reportDAO.getTrialBalanceReport(societyID, dateUtil.getPrevOrNextDate(fromDate, 1), uptoDate,societyVO.getEffectiveDate());
		
		
		for(int i=0;assetLiabilityList.size()>i;i++){
			List tempGroupList=new ArrayList();
			AccountHeadVO trialBalanceVO=assetLiabilityList.get(i);
			AccountHeadVO temp=new AccountHeadVO();
		
			if((trialBalanceVO.getRootID()==3)||(trialBalanceVO.getRootID()==4)){
				
			
			
			temp=ledgerService.getBalancesForTrialBal(trialBalanceVO);
			
			trialBalanceVO.setDrClsBal(temp.getDrClsBal());
			trialBalanceVO.setCrClsBal(temp.getCrClsBal());
			trialBalanceVO.setDrOpnBal(temp.getDrOpnBal());
			trialBalanceVO.setCrOpnBal(temp.getCrOpnBal());
			
			trialBalanceVO.setObjectList(null);
			BigDecimal drOpnBal=BigDecimal.ZERO;
			BigDecimal crOpnBal=BigDecimal.ZERO;
			BigDecimal drClsBal=BigDecimal.ZERO;
			BigDecimal crClsBal=BigDecimal.ZERO;
		
					
			if((trialBalanceVO.getCrClsBal().compareTo(BigDecimal.ZERO)==0) && ( trialBalanceVO.getCrOpnBal().compareTo(BigDecimal.ZERO)==0) && (trialBalanceVO.getDrClsBal().compareTo(BigDecimal.ZERO)==0) && ( trialBalanceVO.getDrOpnBal().compareTo(BigDecimal.ZERO)==0)){
				assetLiabilityList.remove(i);
				--i;
				}else 
					formatedList.add(trialBalanceVO);
		}
		}

		for(int i=0;incExpList.size()>i;i++){
			List tempGroupList=new ArrayList();
			AccountHeadVO trialBalanceVO=incExpList.get(i);
			AccountHeadVO temp=new AccountHeadVO();
			if((trialBalanceVO.getRootID()==1)||(trialBalanceVO.getRootID()==2)){
			temp=ledgerService.getBalancesForTrialBal(trialBalanceVO);
			
			trialBalanceVO.setDrClsBal(temp.getDrClsBal());
			trialBalanceVO.setCrClsBal(temp.getCrClsBal());
			trialBalanceVO.setDrOpnBal(temp.getDrOpnBal());
			trialBalanceVO.setCrOpnBal(temp.getCrOpnBal());
			//logger.debug("==opneg "+trialBalanceVO.getDrOpnBal()+"   opPos "+trialBalanceVO.getCrOpnBal()+" clNeg "+trialBalanceVO.getDrClsBal()+" clPos "+trialBalanceVO.getCrClsBal());
			trialBalanceVO.setObjectList(null);
			BigDecimal drOpnBal=BigDecimal.ZERO;
			BigDecimal crOpnBal=BigDecimal.ZERO;
			BigDecimal drClsBal=BigDecimal.ZERO;
			BigDecimal crClsBal=BigDecimal.ZERO;
		
			
			if((trialBalanceVO.getCrClsBal().compareTo(BigDecimal.ZERO)==0)  && (trialBalanceVO.getDrClsBal().compareTo(BigDecimal.ZERO)==0) ){
			incExpList.remove(i);
			--i;
			}else 
				formatedList.add(trialBalanceVO);
			}
		}
		
		AccountHeadVO diffInBalVO=new AccountHeadVO();
		SocietyVO societyVo=societyService.getSocietyDetails(societyID);
		List societyList=societyService.getSocietyListInDatazone(societyVo.getDataZoneID());
		AccountHeadVO profitLossVO=new AccountHeadVO();
		profitLossVO.setClosingBalance(BigDecimal.ZERO);
		profitLossVO.setOpeningBalance(BigDecimal.ZERO);
		profitLossVO.setCrClsBal(BigDecimal.ZERO);
		profitLossVO.setCrOpnBal(BigDecimal.ZERO);
		profitLossVO.setDrClsBal(BigDecimal.ZERO);
		profitLossVO.setDrOpnBal(BigDecimal.ZERO);
		for(int i=0;societyList.size()>i;i++){
		List ledgerList=ledgerService.getLedgerList(societyID, 0);
		SocietyVO orgVO=(SocietyVO) societyList.get(i);
		List  groupList=new ArrayList();
		AccountHeadVO acVO=(AccountHeadVO) ledgerList.get(0);
		diffInOpening=acVO.getSumOfcreditBal().subtract(acVO.getSumOfDebitBal());
		
		AccountHeadVO acountsVO=ledgerService.getProffitLossBalance(orgVO.getSocietyID(), fromDate, uptoDate,"tb",0,"S");
		profitLossVO.setClosingBalance(profitLossVO.getClosingBalance().add(acountsVO.getClosingBalance()));
		profitLossVO.setOpeningBalance(profitLossVO.getOpeningBalance().add(acountsVO.getOpeningBalance()));
		profitLossVO.setCrClsBal(profitLossVO.getCrClsBal().add(acountsVO.getCrClsBal()));
		profitLossVO.setCrOpnBal(profitLossVO.getCrOpnBal().add(acountsVO.getCrOpnBal()));
		profitLossVO.setDrClsBal(profitLossVO.getDrClsBal().add(acountsVO.getDrClsBal()));
		profitLossVO.setDrOpnBal(profitLossVO.getDrOpnBal().add(acountsVO.getDrOpnBal()));
		profitLossVO.setStrAccountPrimaryHead(acountsVO.getStrAccountPrimaryHead());
		
		
		
		}
		formatedList.add(profitLossVO);
if(diffInOpening.compareTo(BigDecimal.ZERO)!=0){
			
			diffInBalVO.setStrAccountPrimaryHead("DIFF IN OPENING BALANCE");
			if(diffInOpening.compareTo(BigDecimal.ZERO)<0){
				diffInBalVO.setCrOpnBal(diffInBalVO.getCrOpnBal().add(diffInOpening.abs()));
				diffInBalVO.setCrClsBal(diffInBalVO.getCrClsBal().add(diffInOpening.abs()));
			}else {
				diffInBalVO.setDrClsBal(diffInBalVO.getDrClsBal().add(diffInOpening));
				diffInBalVO.setDrOpnBal(diffInBalVO.getDrOpnBal().add(diffInOpening));
			}
			formatedList.add(diffInBalVO);
		}
		
		logger.debug("Exit : public List getConsolidatedTrialBalanceReport(int societyID,String fromDate, String uptoDate)"+accHeadListList.size());
		}catch (Exception e) {
			logger.error("Exception: public List getConsolidatedTrialBalanceReport(int societyID,String fromDate, String uptoDate) "+e );
		}
		return formatedList;
	}
	
			
	public List getLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,int isConsolidated){
		List ledgerList=new ArrayList();
		List accHeadListList=new ArrayList();
		
		try{
		logger.debug("Entry : public List getLedgerReport(int societyID,String fromDate, String uptoDate,int isConsolidated)"+fromDate+uptoDate);
		SocietyVO societyVO=new SocietyVO();
		
		
		
		
		societyVO=societyService.getSocietyDetails("0", societyID);
		accHeadListList=reportDAO.getLedgerReport(societyID, fromDate, uptoDate,groupID,societyVO.getEffectiveDate(),isConsolidated);
		
		for(int i=0;accHeadListList.size()>i;i++){
			AccountHeadVO achVO=(AccountHeadVO) accHeadListList.get(i);
			
			achVO=ledgerService.unsignedOpeningClosingBalance(achVO);
			
			ledgerList.add(achVO);
		}
		
				
		logger.debug("Exit : public List getLedgerReport(int societyID,String fromDate, String uptoDate,int isConsolidated)"+ledgerList.size());
		}catch (Exception e) {
			logger.error("Exception: public List getLedgerReport(int societyID,String fromDate, String uptoDate,int isConsolidated) "+e );
		}
		return ledgerList;
	}
	
	
	public List getGroupLedgerReport(int societyID,String fromDate, String uptoDate,int categoryID){
		List ledgerList=null;
		List accHeadListList=new ArrayList();
		try{
		logger.debug("Entry : public List getGroupLedgerReport(String societyID,String fromDate, String uptoDate)");
		SocietyVO societyVO=new SocietyVO();
		
		
		
		
		societyVO=societyService.getSocietyDetails("0", societyID);
		accHeadListList=reportDAO.getGroupLedgerReport(societyID, fromDate, uptoDate, categoryID,societyVO.getEffectiveDate());
		
		
		logger.debug("Exit : public List getGroupLedgerReport(String societyID,String fromDate, String uptoDate)");
		}catch (Exception e) {
			logger.error("Exception: public List getGroupLedgerReport(String societyID,String fromDate, String uptoDate) "+e );
		}
		return accHeadListList;
	}
	
	public List getTrialBalanceLedgerReport(int societyID,String fromDate, String uptoDate){
		List ledgerList=new ArrayList();
		List accHeadListList=new ArrayList();
		
		try{
		logger.debug("Entry : public List getLedgerReport(int societyID,String fromDate, String uptoDate)"+fromDate+uptoDate);
		SocietyVO societyVO=new SocietyVO();	
		
		
		societyVO=societyService.getSocietyDetails("0", societyID);
		accHeadListList=reportDAO.getTrialBalanceLedgerReport(societyID, fromDate, uptoDate,societyVO.getEffectiveDate());
		
		if(accHeadListList.size()>0){
			for(int i=0;accHeadListList.size()>i;i++){
				AccountHeadVO achVO=(AccountHeadVO) accHeadListList.get(i);
				
				achVO=ledgerService.unsignedOpeningClosingBalance(achVO);
				
				ledgerList.add(achVO);
			}
		}
		
				
		logger.debug("Exit : public List getTrialBalanceLedgerReport(int societyID,String fromDate, String uptoDate)"+ledgerList.size());
		}catch (Exception e) {
			logger.error("Exception: public List getTrialBalanceLedgerReport(int societyID,String fromDate, String uptoDate) "+e );
		}
		return ledgerList;
	}
	
	public List getLedgerReportFromCategory(int societyID,String fromDate, String uptoDate,int categoryID,int isConsolidated){
		List ledgerList=new ArrayList();
		List accHeadListList=new ArrayList();
		
		try{
		logger.debug("Entry : public List getLedgerReport(int societyID,String fromDate, String uptoDate,int isConsolidated)"+fromDate+uptoDate);
		SocietyVO societyVO=new SocietyVO();
		
		
		
		
		societyVO=societyService.getSocietyDetails("0", societyID);
		accHeadListList=reportDAO.getLedgerReportFromCategory(societyID, fromDate, uptoDate,categoryID,societyVO.getEffectiveDate(),isConsolidated);
		
		for(int i=0;accHeadListList.size()>i;i++){
			AccountHeadVO achVO=(AccountHeadVO) accHeadListList.get(i);
			
			achVO=ledgerService.unsignedOpeningClosingBalance(achVO);
			
			ledgerList.add(achVO);
		}
		
				
		logger.debug("Exit : public List getLedgerReport(int societyID,String fromDate, String uptoDate,int isConsolidated)"+ledgerList.size());
		}catch (Exception e) {
			logger.error("Exception: public List getLedgerReport(int societyID,String fromDate, String uptoDate,int isConsolidated) "+e );
		}
		return ledgerList;
	}
	
	
	/*This method is used to get category wise summary report*/
	public List getCategoryWiseSummary(int societyID,String fromDate, String uptoDate,int rootID)		{
		List<AccountHeadVO> accHeadListList=new ArrayList<AccountHeadVO>();
		List<AccountHeadVO> categoryList=new ArrayList<AccountHeadVO>();
		List<AccountHeadVO> groupList=new ArrayList<AccountHeadVO>();
		List<AccountHeadVO> formatedList=new ArrayList<AccountHeadVO>();
		BigDecimal diffInOpening=BigDecimal.ZERO;
		try{
		logger.debug("Entry : public List getCategoryWiseSummary(int societyID,String fromDate, String uptoDate,int rootID)");
		SocietyVO societyVO=new SocietyVO();
		List ledgerList=ledgerService.getLedgerList(societyID, 0);
		//List  groupList=new ArrayList();
		AccountHeadVO acVO=(AccountHeadVO) ledgerList.get(0);
		diffInOpening=acVO.getSumOfcreditBal().subtract(acVO.getSumOfDebitBal());
		societyVO=societyService.getSocietyDetails("0", societyID);
		categoryList=reportDAO.getCategoryWiseSummary(societyID, fromDate, uptoDate,societyVO.getEffectiveDate(),dateUtil.getPrevOrNextDate(fromDate, 1),rootID,"C");
		groupList=reportDAO.getCategoryWiseSummary(societyID, fromDate, uptoDate,societyVO.getEffectiveDate(),dateUtil.getPrevOrNextDate(fromDate, 1),rootID,"G");
		//groupList=reportDAO.getTrialBalanceReport(societyID, fromDate, uptoDate,societyVO.getEffectiveDate(),dateUtil.getPrevOrNextDate(fromDate, 1),0,"AL");
		
		
		for(int i=0;categoryList.size()>i;i++){
			List tempGroupList=new ArrayList();
			AccountHeadVO temp=categoryList.get(i);
			
			if((temp.getRootID()==1)||(temp.getRootID()==2)){
				temp.setClosingBalance(temp.getClosingBalance().subtract(temp.getOpeningBalance()));
				temp.setOpeningBalance(BigDecimal.ZERO);
			}
			
			List tempList=new ArrayList();
			for(int j=0;groupList.size()>j;j++){
				
				AccountHeadVO tempVO=(AccountHeadVO) groupList.get(j);
				
				if(temp.getCategoryID()==tempVO.getCategoryID()){
					tempList.add(tempVO);
			}
				if((tempVO.getRootID()==1)||(tempVO.getRootID()==2)){
					tempVO.setClosingBalance(tempVO.getClosingBalance().subtract(tempVO.getOpeningBalance()));
					tempVO.setOpeningBalance(BigDecimal.ZERO);
				}
				
			}
			
			temp.setObjectList(tempList);
		
			
		}
		 
		

		
		
		
		
		logger.debug("Exit : public List getCategoryWiseSummary(int societyID,String fromDate, String uptoDate,int rootID)"+accHeadListList.size());
		}catch (Exception e) {
			logger.error("Exception: public List getCategoryWiseSummary(int societyID,String fromDate, String uptoDate,int rootID) "+e );
		}
		return categoryList;
	}
	
	
	public ReportDAO getReportDAO() {
		return reportDAO;
	}

	public void setReportDAO(ReportDAO reportDAO) {
		this.reportDAO = reportDAO;
	}

	public LedgerService getLedgerService() {
		return ledgerService;
	}

	public void setLedgerService(LedgerService ledgerService) {
		this.ledgerService = ledgerService;
	}


	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}
}
