package com.emanager.server.financialReports.domainObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.kahadb.util.DiskBenchmark.Report;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.BadSqlGrammarException;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.Domain.TransactionsDomain;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.adminReports.valueObject.RptMorgageVO;
import com.emanager.server.adminReports.valueObject.RptTenantVO;
import com.emanager.server.adminReports.valueObject.RptVehicleVO;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.financialReports.dataaccessObject.ReportDAO;
import com.emanager.server.financialReports.valueObject.MonthwiseChartVO;
import com.emanager.server.financialReports.valueObject.ReportDetailsVO;
import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.financialReports.valueObject.RptMonthYearVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;

public class ReportDomain {
	List reportList = null;

	Logger logger = Logger.getLogger(ReportDomain.class);

	ReportDAO reportDAO;
	LedgerService ledgerService;
	
	DateUtility dateutility = new DateUtility();
	
	
	
	TransactionsDomain transactionsDomain;
	
	SocietyService societyService;

	
	public List YearlyExpenseReport(int year, int intSocietyID) {
		try {
			logger
					.debug("Entry : public List YearlyExpenseReport(int year,int intSocietyID) "
							+ year);
			/*
			 * c=new config_manager(); String
			 * start=c.getLoadPropertiesValue("startYear"); String
			 * end=c.getLoadPropertiesValue("endYear"); logger.info("Property
			 * date is"+start); logger.info("Property date is"+end);
			 */

			//reportList = reportDAO.YearlyExpenseReport(year, intSocietyID);

			logger
					.debug("Exit : public List YearlyExpenseReport(int year,int intSocietyID)");
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("Exception in YearlyExpenseReport : " + ex);
		}
		return reportList;

	}

	public List monthwiseExpenseReport(int year, int intSocietyID) {

		try {
			logger
					.debug("Entry:inside public List monthwiseExpenseReport(int year,int intSocietyID) and ReportDomain "
							+ year);
			reportList = reportDAO.monthwiseExpenseReport(year, intSocietyID);

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("Error:" + ex);
		}

		return reportList;

	}

	public List getMonthlyReport(String month, int cmbyear, int monthindex,
			int intSocietyID) {
		try {

			logger
					.debug("Entry : public List getMonthlyReport(String month,int cmbyear,int monthindex,int intSocietyID) ");

			// hmap=dateutility.getMonthDate(month, cmbyear);
			// logger.info("dateutility.getMonthFirstDate(monthindex,cmbyear)
			// called");
			Date Firstdate = dateutility.getMonthFirstDate(monthindex, cmbyear);
			Date Lastdate = dateutility.getMonthLastDate(monthindex, cmbyear);

			//reportList = reportDAO.getMonthlyReport(Firstdate, Lastdate,	intSocietyID);
			logger
					.debug("Exit : public List getMonthlyReport(String month,int cmbyear,int monthindex,int intSocietyID) ");
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("Exception in getMonthlyReport : " + ex);
		}
		return reportList;

	}

	public List tenantDetailsRpt(int intSocietyID) {
		try {
			logger.debug("Entry : public List tenantDetailsRpt(int intSocietyID) ");
			List tenantList=new ArrayList();
			reportList = reportDAO.tenantDetailsRpt(intSocietyID);
			tenantList=reportDAO.countTenants(intSocietyID);
			int docsNtRecv=0,ntVerified=0,NRI=0;
			int docsRecvd=0,Verify=0,RI=0;
			int missingInfo=0;
			
			for(int i=0;i<reportList.size();i++){
			  RptTenantVO rpnTenant = new RptTenantVO();
			  rpnTenant = (RptTenantVO) reportList.get(i);
			  
			  if(rpnTenant.isDocsReceved.equalsIgnoreCase("0")){
				  docsNtRecv++;				  
				  
			  }else if(rpnTenant.isDocsReceved.equalsIgnoreCase("1")){
				  docsRecvd++;
				  
			  }
			  if(rpnTenant.isVerified.equalsIgnoreCase("0")){
				  ntVerified++;
			  }else if(rpnTenant.isVerified.equalsIgnoreCase("1")){
				  Verify++;
				  
			  }
			  if(rpnTenant.isNRI.equalsIgnoreCase("0")){
				 RI++;
				  
			  }else if(rpnTenant.isNRI.equalsIgnoreCase("1")){
				  NRI++;
				  
			  }
			  if(rpnTenant.getName().equalsIgnoreCase("Info Missing")){
				  missingInfo++;
			  }
		
			}
			int noOfMissing=tenantList.size()-reportList.size();
			RptTenantVO rpnTnt = (RptTenantVO) reportList.get(0);
			rpnTnt.setCntNtDocsRecvd(docsNtRecv);
			rpnTnt.setCntNtVerify(ntVerified);
			rpnTnt.setCntNRI(NRI);	
			rpnTnt.setCntDocsRecvd(docsRecvd);
			rpnTnt.setCntVerify(Verify);
			rpnTnt.setCntRI(RI);
			rpnTnt.setNoOfMissingInfo(missingInfo);
		}catch(EmptyResultDataAccessException e){
			logger.info("No tenant details available for this society");
		}catch (IndexOutOfBoundsException ex) {
			logger.info("No data available for this society : " + ex);
		
		} catch (Exception ex) {
			logger.error("Exception in tenantDetailsRpt :" + ex);
		}

		logger.debug("Exit : public List tenantDetailsRpt(int intSocietyID) ");
		return reportList;
	}
	
	public ReportVO getExpenseReport(int societyID,String fromDate,String uptoDate,int isConsolidated){
		ReportVO rptVO =new ReportVO();
		logger.debug("Entry : public ReportVO getExpenseReport(int societyID,String fromDate,String uptoDate,int isConsolidated)");
		try {
			List expenseList=new ArrayList();
			
			expenseList=reportDAO.getIncomeExpenditureReport(fromDate, uptoDate, societyID, 2, "E",isConsolidated,"S");
			
			rptVO.setExpenseTxList(expenseList);
		} catch (Exception e) {
			logger.info("Exception occured in getExpenseReport : "+e);
		}
		
		logger.debug("Exit : public ReportVO getExpenseReport(int societyID,String fromDate,String uptoDate,int isConsolidated)");
		return rptVO;
	}
	
	
	public ReportVO getIncomeExpenseReport(int societyID,String fromDate,String uptoDate,int isConsolidated,String formatType){
		ReportVO rptVO =new ReportVO();
		SocietyVO societyVO=new SocietyVO();
		String convertedFrmDate=null;
		String convertedToDate=null;
		logger.debug("Entry : public ReportVO getIncomeExpenseReport(int societyID,String fromDate,String uptoDate,int isConsolidated,String formatType)"+societyID);
		
		try {

			List incomeList=this.getIncomeExpenditureReportCredit(fromDate, uptoDate, societyID,"IE",isConsolidated,formatType);
		    List expeList=this.getIncomeExpenditureReportDebit(fromDate, uptoDate, societyID,"IE",isConsolidated,formatType);			
			//List bankBalanceList=this.getCurrentBalNewSys(societyID, fromDate, uptoDate, 4);
			//List cashInHandList=this.getCurrentBalNewSys(societyID, fromDate, uptoDate, 5);
			List incomeDetailList=this.getDetailIncomeExpenditureReport(fromDate, uptoDate, societyID,1,"E",isConsolidated,formatType);
		    List expenseDetailList=this.getDetailIncomeExpenditureReport(fromDate, uptoDate, societyID,2,"E",isConsolidated,formatType);	
		    
			societyVO= societyService.getSocietyDetails("0", societyID);
				
			/*	for(int i=0;cashInHandList.size()>i;i++){
					RptMonthYearVO achVO=(RptMonthYearVO) cashInHandList.get(i);
					bankBalanceList.add(achVO);
					
				}*/
				rptVO.setSocietyVO(societyVO);
				logger.debug("Society Name: "+societyVO.getSocietyName()+" Address: "+societyVO.getAddress());
				
			   
				rptVO.setIncomeTxList(incomeList);
				rptVO.setExpenseTxList(expeList);
				//rptVO.setAccountBalanceList(bankBalanceList);
				rptVO.setIncomeDetailsTxList(incomeDetailList);
				rptVO.setExpenseDetailsTxList(expenseDetailList);
				rptVO=this.getProfitLossBalance(rptVO,"IE","S");
				
				
				ReportVO dateVO=dateutility.getReportDate(fromDate, uptoDate);
				
				rptVO.setRptFromDate(dateVO.getFromDate());
				rptVO.setRptUptoDate(dateVO.getUptDate());
				rptVO.setFromDate(fromDate);
				rptVO.setUptDate(uptoDate);
				
			
				
		        logger.debug("Converted From Date Format: "+rptVO.getFromDate()+" To date: "+ rptVO.getUptDate());
		} catch (Exception e) {
			logger.error("Exception occured while preparing IncomeExpense Report "+e);
		}
			
		
		logger.debug("Exit : public ReportVO getIncomeExpenseReport(int societyID,String fromDate,String uptoDate,String formatType)");
		return rptVO;
	}
	
	public ReportVO getProfitLossReport(int societyID,String fromDate,String uptoDate,int isConsolidated,String formatType){
		ReportVO rptVO =new ReportVO();
		SocietyVO societyVO=new SocietyVO();
		String convertedFrmDate=null;
		String convertedToDate=null;
		logger.debug("Entry : public ReportVO getProfitLossReport(int societyID,String fromDate,String uptoDate,String formatType)"+societyID);
		
		try {

			List incomeList=this.getProfitLossCredit(fromDate, uptoDate, societyID,isConsolidated,formatType);
		    List expeList=this.getProfitLossDebit(fromDate, uptoDate, societyID,isConsolidated,formatType);	
			//List bankBalanceList=this.getCurrentBalNewSys(societyID, fromDate, uptoDate, 4);
			//List cashInHandList=this.getCurrentBalNewSys(societyID, fromDate, uptoDate, 5);
			List incomeDetailList=this.getDetailIncomeExpenditureReport(fromDate, uptoDate, societyID,1,"P",isConsolidated,formatType);
		    List expenseDetailList=this.getDetailIncomeExpenditureReport(fromDate, uptoDate, societyID,2,"P",isConsolidated,formatType);	
		    
			societyVO= societyService.getSocietyDetails("0", societyID);
				
			/*	for(int i=0;cashInHandList.size()>i;i++){
					RptMonthYearVO achVO=(RptMonthYearVO) cashInHandList.get(i);
					bankBalanceList.add(achVO);
					
				}*/
				rptVO.setSocietyVO(societyVO);
				logger.debug("Organization Name: "+societyVO.getSocietyName()+" Address: "+societyVO.getAddress());
				
			   
				rptVO.setIncomeTxList(incomeList);
				rptVO.setExpenseTxList(expeList);
				//rptVO.setAccountBalanceList(bankBalanceList);
				rptVO.setIncomeDetailsTxList(incomeDetailList);
				rptVO.setExpenseDetailsTxList(expenseDetailList);
				rptVO=this.getProfitLossBalance(rptVO,"PL","S");
				
				
				ReportVO dateVO=dateutility.getReportDate(fromDate, uptoDate);
				
				rptVO.setRptFromDate(dateVO.getFromDate());
				rptVO.setRptUptoDate(dateVO.getUptDate());
				rptVO.setFromDate(fromDate);
				rptVO.setUptDate(uptoDate);
				
			
				
		        logger.debug("Converted From Date Format: "+rptVO.getFromDate()+" To date: "+ rptVO.getUptDate());
		} catch (Exception e) {
			logger.error("Exception occured while preparing getProfitLossReport Report "+e);
		}
			
		
		logger.debug("Exit : public ReportVO getIncomeExpenseReport(int societyID,String fromDate,String uptoDate)");
		return rptVO;
	}
	
	public ReportVO getIndASProfitLossReport(int orgID,String fromDate,String uptoDate,int isConsolidated,String formatType,int reportID){
		ReportVO rptVO =new ReportVO();
		SocietyVO societyVO=new SocietyVO();
		String convertedFrmDate=null;
		String convertedToDate=null;
		List labelList=new ArrayList();
		List incCategoryList=new ArrayList();
		List expCategoryList=new ArrayList();
		logger.debug("Entry : public ReportVO getProfitLossReport(int societyID,String fromDate,String uptoDate,String formatType)"+orgID);
		
		try {
			
			rptVO=dateutility.getReportDate(fromDate, uptoDate);

			List incomeList=reportDAO.getProfitLossIndASReport(fromDate, uptoDate, orgID, 1, "P", isConsolidated, formatType, rptVO.getRptFromDate(), rptVO.getRptUptoDate());
			List incomeDetailList=reportDAO.getProfitLossIndASReport(fromDate, uptoDate, orgID, 1, "PL", isConsolidated, formatType, rptVO.getRptFromDate(), rptVO.getRptUptoDate());
			
			if(incomeDetailList.size()>0){
				for(int i=0;incomeDetailList.size()>i;i++){
					ReportDetailsVO incVO=(ReportDetailsVO) incomeDetailList.get(i);
					
					if((incVO.getTotalAmount().compareTo(BigDecimal.ZERO)==0)&&(incVO.getPrevTotalAmount().compareTo(BigDecimal.ZERO)==0)){
						incomeDetailList.remove(i);
						--i;
					}
				}
			}
			
			
			incomeList=convertProfitLossRpt(incomeList, incomeDetailList);
			
			
			
			List expeList=reportDAO.getProfitLossIndASReport(fromDate, uptoDate, orgID, 2, "P", isConsolidated, formatType, rptVO.getRptFromDate(), rptVO.getRptUptoDate());
			List expenseDetailList=reportDAO.getProfitLossIndASReport(fromDate, uptoDate, orgID, 2, "PL", isConsolidated, formatType, rptVO.getRptFromDate(), rptVO.getRptUptoDate());
			
			if(expenseDetailList.size()>0){
				for(int i=0;expenseDetailList.size()>i;i++){
					ReportDetailsVO incVO=(ReportDetailsVO) expenseDetailList.get(i);
					
					if((incVO.getTotalAmount().compareTo(BigDecimal.ZERO)==0)&&(incVO.getPrevTotalAmount().compareTo(BigDecimal.ZERO)==0)){
						expenseDetailList.remove(i);
						--i;
					}
				}
			}
			
			expeList=convertProfitLossRpt(expeList, expenseDetailList);
			
			
		    Map<Integer,ReportDetailsVO> incCatMap=converListToMap(incomeList,"P");
		    Map<Integer,ReportDetailsVO> expCatMap=converListToMap(expeList,"P");
		    
			societyVO= societyService.getSocietyDetails(orgID);
				
			
			incCategoryList=reportDAO.getIndASReportStructure(orgID, reportID, "C",1);
			expCategoryList=reportDAO.getIndASReportStructure(orgID, reportID, "C",2);
			
			
			for(int i=0;incCategoryList.size()>i;i++){
				List objectList=new ArrayList<>();
				BigDecimal totalAmt=BigDecimal.ZERO;
				BigDecimal prevTotalAmt=BigDecimal.ZERO;
				ReportDetailsVO cateVO=(ReportDetailsVO) incCategoryList.get(i);	
				
				List cateList=reportDAO.getIndASReportStructureDetails(cateVO.getId());
				
				if(cateList.size()>0){
					for(int j=0;cateList.size()>j;j++){
						ReportDetailsVO catlinkingVO=(ReportDetailsVO) cateList.get(j);
						 
						if(catlinkingVO.getRptGrpType().equalsIgnoreCase("C")){
							ReportDetailsVO obj=incCatMap.get(catlinkingVO.getCategoryID());
							if(obj!=null){
							ReportDetailsVO rptVo=new ReportDetailsVO();
					 		rptVo.setCategoryID(obj.getCategoryID());
							rptVo.setCategoryName(obj.getCategoryName());
							rptVo.setTotalAmount(obj.getTotalAmount());
							rptVo.setPrevTotalAmount(obj.getPrevTotalAmount());
							rptVo.setGroupID(obj.getGroupID());
							rptVo.setId(catlinkingVO.getId());
							totalAmt=totalAmt.add(obj.getTotalAmount());
							prevTotalAmt=prevTotalAmt.add(obj.getPrevTotalAmount());
							
							
							objectList.add(rptVo);
							}
						}else if(catlinkingVO.getRptGrpType().equalsIgnoreCase("G")){
							
						}
					}
				}
				cateVO.setObjectList(objectList);
				cateVO.setTotalAmount(totalAmt);;
				cateVO.setPrevTotalAmount(prevTotalAmt);
				cateVO.setSocietyVO(null);				
				
				
			}
			
			
			for(int i=0;expCategoryList.size()>i;i++){
				List objectList=new ArrayList<>();
				BigDecimal totalAmt=BigDecimal.ZERO;
				BigDecimal prevTotalAmt=BigDecimal.ZERO;
				ReportDetailsVO cateVO=(ReportDetailsVO) expCategoryList.get(i);	
				
				List cateList=reportDAO.getIndASReportStructureDetails(cateVO.getId());
				
				if(cateList.size()>0){
					for(int j=0;cateList.size()>j;j++){
						ReportDetailsVO catlinkingVO=(ReportDetailsVO) cateList.get(j);
						 
						if(catlinkingVO.getRptGrpType().equalsIgnoreCase("C")){
							ReportDetailsVO obj=expCatMap.get(catlinkingVO.getCategoryID());
							if(obj!=null){
							ReportDetailsVO rptVo=new ReportDetailsVO();
					 		rptVo.setCategoryID(obj.getCategoryID());
							rptVo.setCategoryName(obj.getCategoryName());
							rptVo.setTotalAmount(obj.getTotalAmount());
							rptVo.setPrevTotalAmount(obj.getPrevTotalAmount());
							rptVo.setGroupID(obj.getGroupID());
							rptVo.setId(catlinkingVO.getId());
							totalAmt=totalAmt.add(obj.getTotalAmount());
							prevTotalAmt=prevTotalAmt.add(obj.getPrevTotalAmount());
														
							objectList.add(rptVo);
							}
						}else if(catlinkingVO.getRptGrpType().equalsIgnoreCase("G")){
							
						}
					}
				}
				cateVO.setObjectList(objectList);
				cateVO.setTotalAmount(totalAmt);
				cateVO.setPrevTotalAmount(prevTotalAmt);
				cateVO.setSocietyVO(null);
						
			}
			
			
			
				rptVO.setSocietyVO(societyVO);
				logger.debug("Organization Name: "+societyVO.getSocietyName()+" Address: "+societyVO.getAddress());
				
			   
				rptVO.setIncomeTxList(incomeList);
				rptVO.setExpenseTxList(expeList);
				//rptVO.setAccountBalanceList(bankBalanceList);
				rptVO.setIncomeDetailsTxList(incomeDetailList);
				rptVO.setExpenseDetailsTxList(expenseDetailList);
				rptVO=this.getProfitLossBalance(rptVO,"PL","I");
				rptVO.setIncomeTxList(incCategoryList);
				rptVO.setExpenseTxList(expCategoryList);
				
				
				ReportVO dateVO=dateutility.getReportDate(fromDate, uptoDate);
				
				rptVO.setRptFromDate(dateVO.getFromDate());
				rptVO.setRptUptoDate(dateVO.getUptDate());
				rptVO.setFromDate(fromDate);
				rptVO.setUptDate(uptoDate);
				
			
				
		        logger.debug("Converted From Date Format: "+rptVO.getFromDate()+" To date: "+ rptVO.getUptDate());
		} catch (Exception e) {
			logger.error("Exception occured while preparing getProfitLossReport Report "+e);
		}
			
		
		logger.debug("Exit : public ReportVO getIncomeExpenseReport(int societyID,String fromDate,String uptoDate)");
		return rptVO;
		
		
	}
	
	
	public  Map<Integer, ReportDetailsVO> converListToMap(List<ReportDetailsVO> incExpList,String rptType)

	 {

	     Map<Integer, ReportDetailsVO> incExptMap = new HashMap<Integer, ReportDetailsVO>();

	     if(rptType.equalsIgnoreCase("P")){
	     
	     for(ReportDetailsVO incExpVO : incExpList)

	     {

	         incExptMap.put(incExpVO.getCategoryID(), incExpVO);

	     }
	     }else if(rptType.equalsIgnoreCase("PL")){
	    	 
	    	 for(ReportDetailsVO incExpVO : incExpList)

		     {

		         incExptMap.put(incExpVO.getGroupID(), incExpVO);

		     }
	    	 
	     }else if(rptType.equalsIgnoreCase("L")){
	    	 
	    	 for(ReportDetailsVO incExpVO : incExpList)

		     {

		         incExptMap.put(incExpVO.getLedgerID(), incExpVO);

		     }
	    	 
	     }

	     return incExptMap;

	 }
	
	private List convertProfitLossRpt(List cateList,List groupList){
		
		for(int i=0;cateList.size()>i;i++){
			ReportDetailsVO incVO=(ReportDetailsVO) cateList.get(i);
			List tempIncExpList=new ArrayList<>();
			for(int j=0;groupList.size()>j;j++){
				ReportDetailsVO incDtlVO=(ReportDetailsVO) groupList.get(j);
				
				if(incVO.getCategoryID()==incDtlVO.getCategoryID()){
					tempIncExpList.add(incDtlVO);
				}
				
				
			}
			if((incVO.getTotalAmount().compareTo(BigDecimal.ZERO)==0)&&(incVO.getPrevTotalAmount().compareTo(BigDecimal.ZERO)==0)){
				cateList.remove(i);
				--i;
			}
			if(tempIncExpList.size()>0){
				incVO.setObjectList(tempIncExpList);
			}
			
			
		}
		
		
		return cateList;
	}
	
	 
	public List getProfitLossDetailsForLabelReport(int orgID,String fromDate, String uptoDate,int labelID,int isConsolidated){
		List ledgerList=new ArrayList();
		List cateObjList=new ArrayList();
		
		try{
		logger.debug("Entry : public List getProfitLossDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated)"+labelID);
		ReportVO rptVO=dateutility.getReportDate(fromDate, uptoDate);
		SocietyVO societyVo=societyService.getSocietyDetails(orgID);
		List expeList=reportDAO.getProfitLossIndASReport(fromDate, uptoDate, orgID, 0, "P", isConsolidated, "S", rptVO.getRptFromDate(), rptVO.getRptUptoDate());
		List expenseDetailList=reportDAO.getProfitLossIndASReport(fromDate, uptoDate, orgID, 0, "PL", isConsolidated, "S", rptVO.getRptFromDate(), rptVO.getRptUptoDate());
	    List ledgersList=reportDAO.getProfitLossIndASReport(fromDate, uptoDate, orgID, 0, "L", isConsolidated, "S", rptVO.getRptFromDate(), rptVO.getRptUptoDate());
		expeList=convertProfitLossRpt(expeList, expenseDetailList);
		
		
	    Map<Integer,ReportDetailsVO> incCatMap=converListToMap(expeList,"P");
		List cateList=reportDAO.getIndASReportStructureDetails(labelID);
		Map<Integer,ReportDetailsVO> groupMap=converListToMap(expenseDetailList,"PL");
		Map<Integer,ReportDetailsVO> ledgerMap=converListToMap(ledgersList, "L");
		
		if(cateList.size()>0){
			for(int i=0;cateList.size()>i;i++){
				ReportDetailsVO catlinkingVO=(ReportDetailsVO) cateList.get(i);
				 
				if(catlinkingVO.getRptGrpType().equalsIgnoreCase("C")){
					ReportDetailsVO obj=incCatMap.get(catlinkingVO.getCategoryID());
					if(obj!=null){
					ReportDetailsVO rptVo=new ReportDetailsVO();
					catlinkingVO.setCategoryID(obj.getCategoryID());
					catlinkingVO.setCategoryName(obj.getCategoryName());
					catlinkingVO.setTotalAmount(obj.getTotalAmount());
					catlinkingVO.setPrevTotalAmount(obj.getPrevTotalAmount());
					
					List groupList=ledgerService.getGroupListByCategory(obj.getCategoryID(), orgID, 0);
					List grpObjList=new ArrayList();
					if(groupList.size()>0){
						for(int j=0;groupList.size()>j;j++){
							AccountHeadVO tempGrpVO=(AccountHeadVO) groupList.get(j);
							ReportDetailsVO group=groupMap.get(tempGrpVO.getAccountGroupID());
							if(group!=null){
							ReportDetailsVO grpVO=new ReportDetailsVO();
							grpVO.setCategoryID(group.getCategoryID());
							grpVO.setCategoryName(obj.getCategoryName());
							grpVO.setGroupName(group.getGroupName());
							grpVO.setGroupID(group.getGroupID());
							grpVO.setTotalAmount(group.getTotalAmount());
							grpVO.setPrevTotalAmount(group.getPrevTotalAmount());
							
							ledgerList=reportDAO.getLedgerReport(orgID, fromDate, uptoDate, grpVO.getGroupID(),societyVo.getEffectiveDate(), isConsolidated);
							List ledgerObjList=new ArrayList();
							if(ledgerList.size()>0){
								
								for(int k=0;ledgerList.size()>k;k++){
									
									AccountHeadVO ledger=(AccountHeadVO) ledgerList.get(k);
									ReportDetailsVO ledgerVO=ledgerMap.get(ledger.getLedgerID());
									if(ledgerVO!=null){
									
									ledgerVO.setCategoryID(grpVO.getCategoryID());
									ledgerVO.setCategoryName(grpVO.getCategoryName());
									ledgerVO.setLedgerName(ledgerVO.getLedgerName());
									ledgerVO.setGroupID(grpVO.getGroupID());
									ledgerVO.setPrevTotalAmount(ledgerVO.getPrevTotalAmount());
									ledgerVO.setTotalAmount(ledgerVO.getTotalAmount());
									ledgerVO.setLedgerID(ledgerVO.getLedgerID());
								ledgerObjList.add(ledgerVO);
									}
							}
							
							
						} grpVO.setObjectList(ledgerObjList);
						grpObjList.add(grpVO);
						
					}
							
					}catlinkingVO.setObjectList(grpObjList);
					
					
					}
					}
				}else if(catlinkingVO.getRptGrpType().equalsIgnoreCase("G")){
					
				}
				if((catlinkingVO.getObjectList()!=null)&&(catlinkingVO.getObjectList().size()>0))
				cateObjList.add(catlinkingVO);
			}
			
			
		}
		
		
		logger.debug("Exit : public List getProfitLossDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated)");
		}catch (Exception e) {
			logger.error("Exception: public List getProfitLossDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated) "+e );
		}
		return cateObjList;
	}
	

	public ReportVO getProfitLossReportForPrint(int societyID,String fromDate,String uptoDate,int isConsolidated,String formatType){
		ReportVO rptVO =new ReportVO();
		SocietyVO societyVO=new SocietyVO();
		String convertedFrmDate=null;
		String convertedToDate=null;
		logger.debug("Entry : public ReportVO getProfitLossReport(int societyID,String fromDate,String uptoDate,String formatType)"+societyID);
		
		try {
			
			List incomeList=this.getIncomeExpenditureReportCredit(fromDate, uptoDate, societyID,"IE",isConsolidated,formatType);
		    List expeList=this.getIncomeExpenditureReportDebit(fromDate, uptoDate, societyID,"IE",isConsolidated,formatType);		

		//	List incomeList=this.getProfitLossCredit(fromDate, uptoDate, societyID,isConsolidated,formatType);
		   // List expeList=this.getProfitLossDebit(fromDate, uptoDate, societyID,isConsolidated,formatType);	
			//List bankBalanceList=this.getCurrentBalNewSys(societyID, fromDate, uptoDate, 4);
			//List cashInHandList=this.getCurrentBalNewSys(societyID, fromDate, uptoDate, 5);
			List incomeDetailList=this.getDetailIncomeExpenditureReport(fromDate, uptoDate, societyID,1,"E",isConsolidated,formatType);
		    List expenseDetailList=this.getDetailIncomeExpenditureReport(fromDate, uptoDate, societyID,2,"E",isConsolidated,formatType);	
		    
			societyVO= societyService.getSocietyDetails("0", societyID);
				
			/*	for(int i=0;cashInHandList.size()>i;i++){
					RptMonthYearVO achVO=(RptMonthYearVO) cashInHandList.get(i);
					bankBalanceList.add(achVO);
					
				}*/
				rptVO.setSocietyVO(societyVO);
				logger.debug("Organization Name: "+societyVO.getSocietyName()+" Address: "+societyVO.getAddress());
				
			   
				rptVO.setIncomeTxList(incomeList);
				rptVO.setExpenseTxList(expeList);
				//rptVO.setAccountBalanceList(bankBalanceList);
				rptVO.setIncomeDetailsTxList(incomeDetailList);
				rptVO.setExpenseDetailsTxList(expenseDetailList);
				rptVO=this.getProfitLossBalance(rptVO,"PL","S");
				
				
				ReportVO dateVO=dateutility.getReportDate(fromDate, uptoDate);
				
				rptVO.setRptFromDate(dateVO.getFromDate());
				rptVO.setRptUptoDate(dateVO.getUptDate());
				rptVO.setFromDate(fromDate);
				rptVO.setUptDate(uptoDate);
				
			
				
		        logger.debug("Converted From Date Format: "+rptVO.getFromDate()+" To date: "+ rptVO.getUptDate());
		} catch (Exception e) {
			logger.error("Exception occured while preparing getProfitLossReport Report "+e);
		}
			
		
		logger.debug("Exit : public ReportVO getIncomeExpenseReport(int societyID,String fromDate,String uptoDate)");
		return rptVO;
	}
	
	public List getProfitLossDetailsForLabelReportPrint(int orgID,String fromDate, String uptoDate,int labelID,String labelName,int isConsolidated){
		List ledgerList=new ArrayList();
		List cateObjList=new ArrayList();
		
		try{
		logger.debug("Entry : public List getProfitLossDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated)"+labelID);
		ReportVO rptVO=dateutility.getReportDate(fromDate, uptoDate);
		SocietyVO societyVo=societyService.getSocietyDetails(orgID);
		List expeList=reportDAO.getProfitLossIndASReport(fromDate, uptoDate, orgID, 0, "P", isConsolidated, "S", rptVO.getRptFromDate(), rptVO.getRptUptoDate());
		List expenseDetailList=reportDAO.getProfitLossIndASReport(fromDate, uptoDate, orgID, 0, "PL", isConsolidated, "S", rptVO.getRptFromDate(), rptVO.getRptUptoDate());
	   // List ledgersList=reportDAO.getProfitLossIndASReport(fromDate, uptoDate, orgID, 0, "L", isConsolidated, "S", rptVO.getRptFromDate(), rptVO.getRptUptoDate());
		expeList=convertProfitLossRpt(expeList, expenseDetailList);
		
		
	    Map<Integer,ReportDetailsVO> incCatMap=converListToMap(expeList,"P");
		List cateList=reportDAO.getIndASReportStructureDetails(labelID);
		Map<Integer,ReportDetailsVO> groupMap=converListToMap(expenseDetailList,"PL");
		//Map<Integer,ReportDetailsVO> ledgerMap=converListToMap(ledgersList, "L");
		
		if(cateList.size()>0){
			ReportDetailsVO rptVo=new ReportDetailsVO();
			rptVo.setId(labelID);
			rptVo.setLedgerName(labelName);
			rptVo.setRptGrpType("L");
			//cateObjList.add(rptVo);
			
			for(int i=0;cateList.size()>i;i++){
				ReportDetailsVO catlinkingVO=(ReportDetailsVO) cateList.get(i);
				 
				if(catlinkingVO.getRptGrpType().equalsIgnoreCase("C")){
					ReportDetailsVO obj=incCatMap.get(catlinkingVO.getCategoryID());
					if(obj!=null){
					
					catlinkingVO.setCategoryID(obj.getCategoryID());
					catlinkingVO.setCategoryName(obj.getCategoryName());
					catlinkingVO.setTotalAmount(obj.getTotalAmount());
					catlinkingVO.setPrevTotalAmount(obj.getPrevTotalAmount());
					catlinkingVO.setRptGrpType("C");
					catlinkingVO.setId(labelID);
					catlinkingVO.setLedgerName(labelName);
					cateObjList.add(catlinkingVO);
					
					List groupList=ledgerService.getGroupListByCategory(obj.getCategoryID(), orgID, 0);
				
					if(groupList.size()>0){
						for(int j=0;groupList.size()>j;j++){
							AccountHeadVO tempGrpVO=(AccountHeadVO) groupList.get(j);
							ReportDetailsVO group=groupMap.get(tempGrpVO.getAccountGroupID());
						 if(group!=null){
							ReportDetailsVO grpVO=new ReportDetailsVO();
							grpVO.setCategoryID(group.getCategoryID());
							grpVO.setCategoryName(obj.getCategoryName());
							grpVO.setGroupName(group.getGroupName());
							grpVO.setGroupID(group.getGroupID());
							grpVO.setTotalAmount(group.getTotalAmount());
							grpVO.setPrevTotalAmount(group.getPrevTotalAmount());
							grpVO.setId(labelID);
							grpVO.setRptGrpType("G");
				
						  cateObjList.add(grpVO);
						
					     }
							
					   }
					
					
					   }
				   }
				}
				
			}
			
			
		}
		
		
		logger.debug("Exit : public List getProfitLossDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated)");
		}catch (Exception e) {
			logger.error("Exception: public List getProfitLossDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated) "+e );
		}
		return cateObjList;
	}
	
	public ReportVO getCashBasedReceiptPaymentReport(int societyID,String fromDate,String uptoDate,String rptType,int isConsolidated){
		ReportVO rptVO =new ReportVO();
		SocietyVO societyVO=new SocietyVO();
		String convertedFrmDate=null;
		String convertedToDate=null;
		logger.debug("Entry : public ReportVO getCashBasedIncomeExpenseReport(int societyID,String fromDate,String uptoDate,int isConsolidated)"+societyID);
		
		try {

			List rcptPymtList=reportDAO.getCashBasedReceiptPaymentReport(fromDate, uptoDate, societyID, isConsolidated);
		    ReportVO rcptVo=processCashBasedReceiptPaymentObject(rcptPymtList);
		    List incomeList=rcptVo.getIncomeTxList();
			List expeList=rcptVo.getExpenseTxList();		
			List rcptPymtDtList=reportDAO.getCashBasedReceiptPaymentDetailsReport(fromDate, uptoDate, societyID, isConsolidated);
			ReportVO rcptDetVO=processCashBasedReceiptPaymentObject(rcptPymtDtList);
;			List incomeDetailList=rcptDetVO.getIncomeTxList();
		    List expenseDetailList=rcptDetVO.getExpenseTxList();
		    
			societyVO= societyService.getSocietyDetails("0", societyID);
				
			/*	for(int i=0;cashInHandList.size()>i;i++){
					RptMonthYearVO achVO=(RptMonthYearVO) cashInHandList.get(i);
					bankBalanceList.add(achVO);
					
				}*/
				rptVO.setSocietyVO(societyVO);
				logger.debug("Society Name: "+societyVO.getSocietyName()+" Address: "+societyVO.getAddress());
				
			   
				rptVO.setIncomeTxList(incomeList);
				rptVO.setExpenseTxList(expeList);
				//rptVO.setAccountBalanceList(bankBalanceList);
				rptVO.setIncomeDetailsTxList(incomeDetailList);
				rptVO.setExpenseDetailsTxList(expenseDetailList);
				rptVO=this.getProfitLossBalance(rptVO,rptType,"S");
				String type = rptVO.getExcessType();
				if(type.equalsIgnoreCase("Excess of Expenditure Over Income")){
					rptVO.setExcessType("Excess of Payment Over Receipt");
				}else if(type.equalsIgnoreCase("Excess of Income Over Expenditure")){
					rptVO.setExcessType("Excess of Receipt Over Payment");
				}
				ReportVO dateVO=dateutility.getReportDate(fromDate, uptoDate);
				
				rptVO.setRptFromDate(dateVO.getFromDate());
				rptVO.setRptUptoDate(dateVO.getUptDate());
				rptVO.setFromDate(fromDate);
				rptVO.setUptDate(uptoDate);
				
				
				
		        logger.debug("Converted From Date Format: "+rptVO.getFromDate()+" To date: "+ rptVO.getUptDate());
		} catch (NullPointerException ex) {
			logger.info("getCashBasedIncomeExpenseReport No details found for societyID:  "+societyID);

		} catch (Exception e) {
			logger.error("Exception occured while preparing getCashBasedIncomeExpenseReport Report "+e);
		}
			
		
		logger.debug("Exit : public ReportVO getCashBasedIncomeExpenseReport(int societyID,String fromDate,String uptoDate,int isConsolidated)");
		return rptVO;
	}
	
	
	private ReportVO processCashBasedReceiptPaymentObject(List rcptPymtList){
		logger.debug("Entry : private ReportVO processCashBasedReceiptPaymentObject(List rcptPymtList)");
		ReportVO reportVO=new ReportVO();
		List incomeList=new ArrayList<>();
		List expenseList=new ArrayList<>();
		try{
			if(rcptPymtList.size()>0){
				
				for(int i=0;rcptPymtList.size()>i;i++){
					ReportDetailsVO rptVO=(ReportDetailsVO) rcptPymtList.get(i);
					
					if(rptVO.getRootID()==1){
						if(rptVO.getTxType().equalsIgnoreCase("C")){
							rptVO.setTotalAmount(rptVO.getDebitBalance());
							incomeList.add(rptVO);
						}else{
							rptVO.setTotalAmount(rptVO.getCreditBalance());
							expenseList.add(rptVO);
						}
						
					}else if(rptVO.getRootID()==2){
						if(rptVO.getTxType().equalsIgnoreCase("D")){
							rptVO.setTotalAmount(rptVO.getDebitBalance());
							expenseList.add(rptVO);
						}else{
							rptVO.setTotalAmount(rptVO.getCreditBalance());
							incomeList.add(rptVO);
						}
					}else if(rptVO.getRootID()==3){
						if(rptVO.getTxType().equalsIgnoreCase("C")){
							rptVO.setTotalAmount(rptVO.getCreditBalance());
							incomeList.add(rptVO);
						}else{
							rptVO.setTotalAmount(rptVO.getDebitBalance());
							expenseList.add(rptVO);
						}
						
					}else if(rptVO.getRootID()==4){
						if(rptVO.getTxType().equalsIgnoreCase("D")){
						rptVO.setTotalAmount(rptVO.getCreditBalance());
						expenseList.add(rptVO);
					}else{
						rptVO.setTotalAmount(rptVO.getDebitBalance());
						incomeList.add(rptVO);
					}
						
					}
					
					
					
					
					
				}
				
				
			}
			reportVO.setIncomeTxList(incomeList);
			reportVO.setExpenseTxList(expenseList);
			
			
		}catch(Exception e){
			logger.error("Exception : private ReportVO processCashBasedReceiptPaymentObject(List rcptPymtList)"+e);
		}
		
		logger.debug("Entry : private ReportVO processCashBasedReceiptPaymentObject(List rcptPymtList)");
		
		return reportVO;
	}
	
	private ReportVO processMonthlyCashBasedReceiptPaymentObject(List rcptPymtList){
		logger.debug("Entry : private ReportVO processMonthlyCashBasedReceiptPaymentObject(List rcptPymtList)");
		ReportVO reportVO=new ReportVO();
		List incomeList=new ArrayList<>();
		List expenseList=new ArrayList<>();
		try{
			if(rcptPymtList.size()>0){
				
				for(int i=0;rcptPymtList.size()>i;i++){
					MonthwiseChartVO rptVO=(MonthwiseChartVO) rcptPymtList.get(i);
					if(rptVO.getRootGroupID()==1){
						if(rptVO.getTxType().equalsIgnoreCase("C")){
							//rptVO.setTotalAmount(rptVO.getDebitBalance());
							rptVO.setAprAmt(rptVO.getAprDb());
							rptVO.setMayAmt(rptVO.getMayDb());
							rptVO.setJunAmt(rptVO.getJunDb());
							rptVO.setJulAmt(rptVO.getJulDb());
							rptVO.setAugAmt(rptVO.getAugDb());
							rptVO.setSeptAmt(rptVO.getSeptDb());
							rptVO.setOctAmt(rptVO.getOctDb());
							rptVO.setNovAmt(rptVO.getNovDb());
							rptVO.setDecAmt(rptVO.getDecDb());
							rptVO.setJanAmt(rptVO.getJanDb());
							rptVO.setFebAmt(rptVO.getFebDb());
							rptVO.setMarAmt(rptVO.getMarDb());
							rptVO.setQ1Amt(rptVO.getQ1Db());
							rptVO.setQ2Amt(rptVO.getQ2Db());
							rptVO.setQ3Amt(rptVO.getQ3Db());
							rptVO.setQ4Amt(rptVO.getQ4Db());
							
							incomeList.add(rptVO);
						}else{
							//rptVO.setTotalAmount(rptVO.getCreditBalance());
							rptVO.setAprAmt(rptVO.getAprCr());
							rptVO.setMayAmt(rptVO.getMayCr());
							rptVO.setJunAmt(rptVO.getJunCr());
							rptVO.setJulAmt(rptVO.getJulCr());
							rptVO.setAugAmt(rptVO.getAugCr());
							rptVO.setSeptAmt(rptVO.getSeptCr());
							rptVO.setOctAmt(rptVO.getOctCr());
							rptVO.setNovAmt(rptVO.getNovCr());
							rptVO.setDecAmt(rptVO.getDecCr());
							rptVO.setJanAmt(rptVO.getJanCr());
							rptVO.setFebAmt(rptVO.getFebCr());
							rptVO.setMarAmt(rptVO.getMarCr());
							expenseList.add(rptVO);
						}
						
					}else if(rptVO.getRootGroupID()==2){
						if(rptVO.getTxType().equalsIgnoreCase("D")){
							//rptVO.setTotalAmount(rptVO.getDebitBalance());
							rptVO.setAprAmt(rptVO.getAprDb());
							rptVO.setMayAmt(rptVO.getMayDb());
							rptVO.setJunAmt(rptVO.getJunDb());
							rptVO.setJulAmt(rptVO.getJulDb());
							rptVO.setAugAmt(rptVO.getAugDb());
							rptVO.setSeptAmt(rptVO.getSeptDb());
							rptVO.setOctAmt(rptVO.getOctDb());
							rptVO.setNovAmt(rptVO.getNovDb());
							rptVO.setDecAmt(rptVO.getDecDb());
							rptVO.setJanAmt(rptVO.getJanDb());
							rptVO.setFebAmt(rptVO.getFebDb());
							rptVO.setMarAmt(rptVO.getMarDb());
							rptVO.setQ1Amt(rptVO.getQ1Db());
							rptVO.setQ2Amt(rptVO.getQ2Db());
							rptVO.setQ3Amt(rptVO.getQ3Db());
							rptVO.setQ4Amt(rptVO.getQ4Db());
							expenseList.add(rptVO);
						}else{
							//rptVO.setTotalAmount(rptVO.getCreditBalance());
							rptVO.setAprAmt(rptVO.getAprCr());
							rptVO.setMayAmt(rptVO.getMayCr());
							rptVO.setJunAmt(rptVO.getJunCr());
							rptVO.setJulAmt(rptVO.getJulCr());
							rptVO.setAugAmt(rptVO.getAugCr());
							rptVO.setSeptAmt(rptVO.getSeptCr());
							rptVO.setOctAmt(rptVO.getOctCr());
							rptVO.setNovAmt(rptVO.getNovCr());
							rptVO.setDecAmt(rptVO.getDecCr());
							rptVO.setJanAmt(rptVO.getJanCr());
							rptVO.setFebAmt(rptVO.getFebCr());
							rptVO.setMarAmt(rptVO.getMarCr());
							rptVO.setQ1Amt(rptVO.getQ1Cr());
							rptVO.setQ2Amt(rptVO.getQ2Cr());
							rptVO.setQ3Amt(rptVO.getQ3Cr());
							rptVO.setQ4Amt(rptVO.getQ4Cr());
							incomeList.add(rptVO);
						}
					}else if(rptVO.getRootGroupID()==3){
						if(rptVO.getTxType().equalsIgnoreCase("C")){
							//rptVO.setTotalAmount(rptVO.getCreditBalance());
							rptVO.setAprAmt(rptVO.getAprCr());
							rptVO.setMayAmt(rptVO.getMayCr());
							rptVO.setJunAmt(rptVO.getJunCr());
							rptVO.setJulAmt(rptVO.getJulCr());
							rptVO.setAugAmt(rptVO.getAugCr());
							rptVO.setSeptAmt(rptVO.getSeptCr());
							rptVO.setOctAmt(rptVO.getOctCr());
							rptVO.setNovAmt(rptVO.getNovCr());
							rptVO.setDecAmt(rptVO.getDecCr());
							rptVO.setJanAmt(rptVO.getJanCr());
							rptVO.setFebAmt(rptVO.getFebCr());
							rptVO.setMarAmt(rptVO.getMarCr());
							rptVO.setQ1Amt(rptVO.getQ1Cr());
							rptVO.setQ2Amt(rptVO.getQ2Cr());
							rptVO.setQ3Amt(rptVO.getQ3Cr());
							rptVO.setQ4Amt(rptVO.getQ4Cr());
							incomeList.add(rptVO);
						}else{
							//rptVO.setTotalAmount(rptVO.getDebitBalance());
							rptVO.setAprAmt(rptVO.getAprDb());
							rptVO.setMayAmt(rptVO.getMayDb());
							rptVO.setJunAmt(rptVO.getJunDb());
							rptVO.setJulAmt(rptVO.getJulDb());
							rptVO.setAugAmt(rptVO.getAugDb());
							rptVO.setSeptAmt(rptVO.getSeptDb());
							rptVO.setOctAmt(rptVO.getOctDb());
							rptVO.setNovAmt(rptVO.getNovDb());
							rptVO.setDecAmt(rptVO.getDecDb());
							rptVO.setJanAmt(rptVO.getJanDb());
							rptVO.setFebAmt(rptVO.getFebDb());
							rptVO.setMarAmt(rptVO.getMarDb());
							rptVO.setQ1Amt(rptVO.getQ1Db());
							rptVO.setQ2Amt(rptVO.getQ2Db());
							rptVO.setQ3Amt(rptVO.getQ3Db());
							rptVO.setQ4Amt(rptVO.getQ4Db());
							expenseList.add(rptVO);
						}
						
					}else if(rptVO.getRootGroupID()==4){
						if(rptVO.getTxType().equalsIgnoreCase("D")){
						//rptVO.setTotalAmount(rptVO.getCreditBalance());
						rptVO.setAprAmt(rptVO.getAprCr());
						rptVO.setMayAmt(rptVO.getMayCr());
						rptVO.setJunAmt(rptVO.getJunCr());
						rptVO.setJulAmt(rptVO.getJulCr());
						rptVO.setAugAmt(rptVO.getAugCr());
						rptVO.setSeptAmt(rptVO.getSeptCr());
						rptVO.setOctAmt(rptVO.getOctCr());
						rptVO.setNovAmt(rptVO.getNovCr());
						rptVO.setDecAmt(rptVO.getDecCr());
						rptVO.setJanAmt(rptVO.getJanCr());
						rptVO.setFebAmt(rptVO.getFebCr());
						rptVO.setMarAmt(rptVO.getMarCr());
						rptVO.setQ1Amt(rptVO.getQ1Cr());
						rptVO.setQ2Amt(rptVO.getQ2Cr());
						rptVO.setQ3Amt(rptVO.getQ3Cr());
						rptVO.setQ4Amt(rptVO.getQ4Cr());
						expenseList.add(rptVO);
					}else{
					//	rptVO.setTotalAmount(rptVO.getDebitBalance());
						rptVO.setAprAmt(rptVO.getAprDb());
						rptVO.setMayAmt(rptVO.getMayDb());
						rptVO.setJunAmt(rptVO.getJunDb());
						rptVO.setJulAmt(rptVO.getJulDb());
						rptVO.setAugAmt(rptVO.getAugDb());
						rptVO.setSeptAmt(rptVO.getSeptDb());
						rptVO.setOctAmt(rptVO.getOctDb());
						rptVO.setNovAmt(rptVO.getNovDb());
						rptVO.setDecAmt(rptVO.getDecDb());
						rptVO.setJanAmt(rptVO.getJanDb());
						rptVO.setFebAmt(rptVO.getFebDb());
						rptVO.setMarAmt(rptVO.getMarDb());
						rptVO.setQ1Amt(rptVO.getQ1Db());
						rptVO.setQ2Amt(rptVO.getQ2Db());
						rptVO.setQ3Amt(rptVO.getQ3Db());
						rptVO.setQ4Amt(rptVO.getQ4Db());
						incomeList.add(rptVO);
					}
					
					
					
				}
				
				}
			}
			reportVO.setIncomeTxList(incomeList);
			reportVO.setExpenseTxList(expenseList);
			
			
		}catch(Exception e){
			logger.error("Exception : private ReportVO processMonthlyCashBasedReceiptPaymentObject(List rcptPymtList)"+e);
		}
		
		logger.debug("Entry : private ReportVO processMonthlyCashBasedReceiptPaymentObject(List rcptPymtList)");
		
		return reportVO;
	}
	
	public List getCashBasedLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType,int isConsolidated){
		List accHeadListList=new ArrayList();
		
		try{
		logger.debug("Entry : public List getLedgerReport(int societyID,String fromDate, String uptoDate,int isConsolidated)"+fromDate+uptoDate);
		SocietyVO societyVO=new SocietyVO();
		
		societyVO=societyService.getSocietyDetails("0", societyID);
		accHeadListList=reportDAO.getCashBasedLedgerReport(societyID, fromDate, uptoDate, groupID,txType,isConsolidated);
		accHeadListList=processCashBasedReceiptPaymentLedgerObject(accHeadListList);
			
		logger.debug("Exit : public List getLedgerReport(int societyID,String fromDate, String uptoDate,int isConsolidated)"+accHeadListList.size());
		}catch (Exception e) {
			logger.error("Exception: public List getLedgerReport(int societyID,String fromDate, String uptoDate,int isConsolidated) "+e );
		}
		return accHeadListList;
	}
	
	private List processCashBasedReceiptPaymentLedgerObject(List rcptPymtList){
		logger.debug("Entry : private ReportVO processCashBasedReceiptPaymentLedgerObject(List rcptPymtList)");
		ReportVO reportVO=new ReportVO();
		
		try{
			if(rcptPymtList.size()>0){
				
				for(int i=0;rcptPymtList.size()>i;i++){
					AccountHeadVO rptVO=(AccountHeadVO) rcptPymtList.get(i);
					
					if(rptVO.getRootID()==1){
						if(rptVO.getAccType().equalsIgnoreCase("C")){
							rptVO.setClosingBalance(rptVO.getDrClsBal());
							
						}else{
							rptVO.setClosingBalance(rptVO.getCrClsBal());
							
						}
						
					}else if(rptVO.getRootID()==2){
						if(rptVO.getAccType().equalsIgnoreCase("D")){
							rptVO.setClosingBalance(rptVO.getDrClsBal());
							
						}else{
							rptVO.setClosingBalance(rptVO.getCrClsBal());
							
						}
					}else if(rptVO.getRootID()==3){
						if(rptVO.getAccType().equalsIgnoreCase("C")){
							rptVO.setClosingBalance(rptVO.getCrClsBal());
							
						}else{
							rptVO.setClosingBalance(rptVO.getDrClsBal());
							
						}
						
					}else if(rptVO.getRootID()==4){
						if(rptVO.getAccType().equalsIgnoreCase("D")){
						rptVO.setClosingBalance(rptVO.getCrClsBal());
						
					}else{
						rptVO.setClosingBalance(rptVO.getDrClsBal());
						
					}
						
					}
					
					
					
					
					
				}
				
				
			}
			
			
			
		}catch(Exception e){
			logger.error("Exception : private ReportVO processCashBasedReceiptPaymentObject(List rcptPymtList)"+e);
		}
		
		logger.debug("Entry : private ReportVO processCashBasedReceiptPaymentObject(List rcptPymtList)");
		
		return rcptPymtList;
	}
	
	
public ReportVO getProfitLossBalance(ReportVO reportVO,String rptType,String rptFrmt){
		
	logger.debug("Entry : public ReportVO getProfitLossBalance(ReportVO reportVO)");
		

		BigDecimal incomeTotal = BigDecimal.ZERO;
		BigDecimal prevIncTotal=BigDecimal.ZERO;
		BigDecimal expenseTotal =  BigDecimal.ZERO;
		BigDecimal prevExpTotal=BigDecimal.ZERO;
		BigDecimal incomeTempTotal = BigDecimal.ZERO;
		BigDecimal expenseTempTotal =  BigDecimal.ZERO;
		BigDecimal prevIncTempTotal=BigDecimal.ZERO;
		BigDecimal prevExpTempTotal=BigDecimal.ZERO;
		String condition="before";
		logger.debug("Income : "+incomeTotal);
		try {
			 //total income
			 for(int i=0;reportVO.getIncomeTxList().size()>i;i++){
					ReportDetailsVO incomeVO=(ReportDetailsVO) reportVO.getIncomeTxList().get(i);
					incomeTotal=incomeTotal.add(incomeVO.getTotalAmount());
					if(incomeVO.getPrevTotalAmount()!=null)
						prevIncTotal=prevIncTotal.add(incomeVO.getPrevTotalAmount());
					
			 }


			 //total expense
			 for(int i=0;reportVO.getExpenseTxList().size()>i;i++){
				 ReportDetailsVO expenseVO=(ReportDetailsVO) reportVO.getExpenseTxList().get(i);
					expenseTotal=expenseTotal.add(expenseVO.getTotalAmount());
					if(expenseVO.getPrevTotalAmount()!=null)
						prevExpTotal=prevExpTotal.add(expenseVO.getPrevTotalAmount());
			 }
			logger.debug("Total Income: "+incomeTotal+" Total Expense: "+expenseTotal);
			int resultCur=incomeTotal.compareTo(expenseTotal);
			if(resultCur==1){				
				expenseTempTotal=incomeTotal.subtract(expenseTotal);				
			}else{
				incomeTempTotal=expenseTotal.subtract(incomeTotal);				
			}
			
			int resultPrev=prevIncTotal.compareTo(prevExpTotal);
			if(resultPrev==1){
				prevExpTempTotal=prevIncTotal.subtract(prevExpTotal);
			}else{
				prevIncTempTotal=prevExpTotal.subtract(prevIncTotal);
			}
			
			if(rptFrmt.equalsIgnoreCase("I")){
				condition="after";
			}
			
			if(!(incomeTempTotal.toString().equalsIgnoreCase("0"))){
				if(rptType.equalsIgnoreCase("IE")){
				reportVO.setExcessType("Excess of Expenditure Over Income");
				}else if(rptType.equalsIgnoreCase("PL")){
				reportVO.setExcessType("Net Loss "+condition+" tax");
			}else if(rptType.equalsIgnoreCase("CF"))
					reportVO.setExcessType("Net cash out flow");
				
				reportVO.setExcessAmount(incomeTempTotal);
				reportVO.setExpOverInc(incomeTempTotal);
				
			}else if(!(expenseTempTotal.toString().equalsIgnoreCase("0"))){
				if(rptType.equalsIgnoreCase("IE")){
					reportVO.setExcessType("Excess of Income Over Expenditure");
					}else if(rptType.equalsIgnoreCase("PL")){
					reportVO.setExcessType("Net Profit "+condition+" tax");
			}else if(rptType.equalsIgnoreCase("CF"))
				reportVO.setExcessType("Net cash in flow");
				
				reportVO.setExcessAmount(expenseTempTotal);
				reportVO.setIncOverExp(expenseTempTotal);
			}
			reportVO.setIncomeTotal(incomeTotal);
			reportVO.setExpenseTotal(expenseTotal);
			
			if(!(prevIncTempTotal.toString().equalsIgnoreCase("0"))){
				if(rptType.equalsIgnoreCase("IE")){
				reportVO.setPrevExcessType("Excess of Expenditure Over Income");
				}else if(rptType.equalsIgnoreCase("PL")){
				reportVO.setPrevExcessType("Net Loss "+condition+" tax");
			}else if(rptType.equalsIgnoreCase("CF"))
					reportVO.setPrevExcessType("Net cash out flow");
				
				reportVO.setPrevExcessAmount(prevIncTempTotal);
				reportVO.setPrevExpOverInc(prevIncTempTotal);
				
			}else if(!(prevExpTempTotal.toString().equalsIgnoreCase("0"))){
				if(rptType.equalsIgnoreCase("IE")){
					reportVO.setPrevExcessType("Excess of Income Over Expenditure");
					}else if(rptType.equalsIgnoreCase("PL")){
					reportVO.setPrevExcessType("Net Profit "+condition+" tax");
			}else if(rptType.equalsIgnoreCase("CF"))
				reportVO.setPrevExcessType("Net cash in flow");
				
				reportVO.setPrevExcessAmount(prevExpTempTotal);
				reportVO.setPrevIncOverExp(prevExpTempTotal);
			}
			
			reportVO.setPrevIncTotal(prevIncTotal);
			reportVO.setPrevExpTotal(prevExpTotal);
			logger.debug("Expense: "+expenseTempTotal+" Income: "+incomeTempTotal+" Type: "+reportVO.getExcessType()+" Amount Expense: "+reportVO.getExcessAmount());
			
		} catch (Exception e) {
			logger.error("Exception occured while preparing getProfitLossBalance Report "+e);
		}
			
		
		logger.debug("Exit : public ReportVO getProfitLossBalance(ReportVO reportVO)");

		return reportVO;
	}
	
	
	private AccountHeadVO getIncExpBalance(int societyID, String fromDate,String uptoDate,String incExp) {
		List<AccountHeadVO> incExpList = new ArrayList<AccountHeadVO>();
		List groupList = new ArrayList();
		BigDecimal totalCur=BigDecimal.ZERO;
		BigDecimal totalPrev=BigDecimal.ZERO;
		AccountHeadVO incExpVO=new AccountHeadVO();
		try {

			logger.debug("Entry: private List getIncExpList(int societyID, String fromDate,String uptoDate,String incExp)"+ societyID );
			if(incExp.equalsIgnoreCase("I"))
				groupList=ledgerService.getAllGroupList(1,societyID);
			else 
				groupList=ledgerService.getAllGroupList(2,societyID);
			String tempCategoryName = "";
			
			for(int j=0;groupList.size()>j;j++){
			BigDecimal crOpnBal=BigDecimal.ZERO;
			BigDecimal drOpnBal=BigDecimal.ZERO;
			BigDecimal crClsBal=BigDecimal.ZERO;
			BigDecimal drClsBal=BigDecimal.ZERO;
			BigDecimal openingBal=BigDecimal.ZERO;
			BigDecimal closingBal=BigDecimal.ZERO;
			AccountHeadVO achVO=(AccountHeadVO) groupList.get(j);
			
			RptMonthYearVO rptVO=new RptMonthYearVO();
			incExpList=ledgerService.getLedgerList(societyID, achVO.getAccountGroupID(),"G");
			
			//acHvO=getOpeningClosingBalance(societyID, groupID, fromDate, uptoDate, "G");
			
		
		for(AccountHeadVO acHVO:incExpList){
			
			acHVO=ledgerService.getOpeningClosingBalance(societyID, acHVO.getLedgerID(), fromDate, uptoDate,"L");
			
			crClsBal=crClsBal.add(acHVO.getCrClsBal());
			drClsBal=drClsBal.add(acHVO.getDrClsBal());
			crOpnBal=crOpnBal.add(acHVO.getCrOpnBal());
			drOpnBal=drOpnBal.add(acHVO.getDrOpnBal());
			openingBal=openingBal.add(acHVO.getOpeningBalance());
			closingBal=closingBal.add(acHVO.getClosingBalance());
			
			//logger.info(acHVO.getOpeningBalance()+"======="+acHVO.getClosingBalance()+"=======?"+openingBal+"  "+closingBal+" "+acHVO.getStrAccountHeadName());
		}
		
		
		
		achVO.setAccountGroupID(achVO.getAccountGroupID());
		achVO.setStrAccountHeadName(achVO.getStrAccountHeadName());
		achVO.setClosingBalance(closingBal);
		achVO.setOpeningBalance(openingBal);
		achVO.setCrOpnBal(crOpnBal);
		achVO.setDrOpnBal(drOpnBal);
		achVO.setCrClsBal(crClsBal);
		achVO.setDrClsBal(drClsBal);
			
			totalCur=totalCur.add(achVO.getClosingBalance());
			totalPrev=totalPrev.add(achVO.getOpeningBalance());
			//logger.info(totalCur+"==============?"+achVO.getOpeningBalance()+"  "+achVO.getClosingBalance()+" "+achVO.getStrAccountHeadName());
			//logger.info("Here     "+totalCur+"    "+totalPrev);
			
			
		
	
			
			
		}
			incExpVO.setOpeningBalance(totalPrev);
			incExpVO.setClosingBalance(totalCur);
			
		} catch (Exception ex) {
			logger.error("Exception in IncomeExpenditureReportCredit : " + ex);
		}
		logger
				.debug("Exit:Public List IncomeExpenditureReportCredit(String year, String id) and ReportDomain");
		return incExpVO;
	}
	
	public List getProfitLossDebit(String fromDate, String uptoDate, int SocietyID,int isConsolidated,String formatType) {
		List incExpList=new ArrayList();
		List incExpListDetails=new ArrayList<>();
		String firstDate=null;	
		String lastDate=null;
		try {
			logger.debug("Entry:public List getProfitLossDebit(String fromDate, String uptoDate, int SocietyID)");
			
			
			
			incExpList=reportDAO.getIncomeExpenditureReport(fromDate, uptoDate, SocietyID,2,"PL",isConsolidated,formatType);
			incExpListDetails=getDetailIncomeExpenditureReport(fromDate, uptoDate, SocietyID, 2,"P",isConsolidated,formatType);
			for(int i=0;incExpList.size()>i;i++){
				ReportDetailsVO expVO=(ReportDetailsVO) incExpList.get(i);
				List tempIncExpList=new ArrayList<>();
				for(int j=0;incExpListDetails.size()>j;j++){
					ReportDetailsVO expDtlVO=(ReportDetailsVO) incExpListDetails.get(j);
					
					if(expVO.getCategoryID()==expDtlVO.getCategoryID()){
						tempIncExpList.add(expDtlVO);
					}
					
					
				}
				if(expVO.getTotalAmount().compareTo(BigDecimal.ZERO)==0){
					incExpList.remove(i);
					--i;
				}
				if(tempIncExpList.size()>0){
					expVO.setObjectList(tempIncExpList);
				}
				
				
			}
			
		}
		catch (Exception ex) {
			logger.error("Exception in public List getProfitLossDebit(String fromDate, String uptoDate, int SocietyID) : " + ex);
		}
		logger.debug("Exit:public List getProfitLossDebit(String fromDate, String uptoDate, int SocietyID)");
	return incExpList;

	}
	public List getProfitLossCredit(String fromDate, String uptoDate, int SocietyID,int isConsolidated,String formatType) {
		List incExpList=new ArrayList();
		List incExpListDetails=new ArrayList<>();
		
		String firstDate = null;;
		String lastDate=null;
		
		try {
			logger.debug("Entry:public List getProfitLossCredit(String fromDate, String uptoDate, int SocietyID,int isConsolidated,String formatType)");
		
			
			incExpList=reportDAO.getIncomeExpenditureReport(fromDate, uptoDate, SocietyID,1,"PL",isConsolidated,formatType);
			incExpListDetails=getDetailIncomeExpenditureReport(fromDate, uptoDate, SocietyID, 1,"P",isConsolidated,formatType);
			
			for(int i=0;incExpList.size()>i;i++){
				ReportDetailsVO incVO=(ReportDetailsVO) incExpList.get(i);
				List tempIncExpList=new ArrayList<>();
				for(int j=0;incExpListDetails.size()>j;j++){
					ReportDetailsVO incDtlVO=(ReportDetailsVO) incExpListDetails.get(j);
					
					if(incVO.getCategoryID()==incDtlVO.getCategoryID()){
						tempIncExpList.add(incDtlVO);
					}
					
					
				}
				if(incVO.getTotalAmount().compareTo(BigDecimal.ZERO)==0){
					incExpList.remove(i);
					--i;
				}
				if(tempIncExpList.size()>0){
					incVO.setObjectList(tempIncExpList);
				}
				
				
			}
			
		}
		catch (Exception ex) {
			logger.error("Exception in public List getProfitLossCredit(String fromDate, String uptoDate, int SocietyID,int isConsolidated,String formatType) : " + ex);
		}
		logger.debug("Exit:public List public List getProfitLossCredit(String fromDate, String uptoDate, int SocietyID,int isConsolidated,String formatType)");
	return incExpList;

	}
	
	
	/*public List IncomeExpenditureReportCredit(int year, int id) {
		List incExpList = new ArrayList();
		List incList = new ArrayList();
		try {

			logger
					.debug("Entry:Public List IncomeExpenditureReportCredit(String year, String id) and ReportDomain"
							+ year + id);
		
			incExpList = reportDAO.IncomeExpenditureReportCredit(year, id);

		} catch (Exception ex) {
			logger.error("Exception in IncomeExpenditureReportCredit : " + ex);
		}
		logger
				.debug("Exit:Public List IncomeExpenditureReportCredit(String year, String id) and ReportDomain");
		return incExpList;
	}
	
	public List IncomeExpenditureReportDebit(int year, int id) {
		List incExpList = new ArrayList();
		List incList = new ArrayList();
		try {

			logger
					.debug("Entry:Public List IncomeExpenditureReportDebit(String year, String id) and ReportDomain"
							+ year + id);
		
			incExpList = reportDAO.IncomeExpenditureReportDebit(year, id);

		} catch (Exception ex) {
			logger.error("Exception in IncomeExpenditureReportDebit : " + ex);
		}
		logger
				.debug("Exit:Public List IncomeExpenditureReport(String year, String id) and ReportDomain");
		return incExpList;
	}
*/
	public List getIncomeExpenditureReportDebit(String fromDate, String uptoDate, int SocietyID,String type,int isConsolidated,String formatType) {
		List incExpList=new ArrayList();
		String firstDate=null;	
		String lastDate=null;
		try {
			logger.debug("Entry:public List monthlyIncomeExpenditureReportDebit(int year, int id, int month, String period,int isConsolidated,String formatType)");
			
			
			
			incExpList=reportDAO.getIncomeExpenditureReport(fromDate, uptoDate, SocietyID,2,type,isConsolidated,formatType);
			
			for(int i=0;incExpList.size()>i;i++){
				ReportDetailsVO incVO=(ReportDetailsVO) incExpList.get(i);
				if(incVO.getTotalAmount().compareTo(BigDecimal.ZERO)==0){
					incExpList.remove(i);
					--i;
				}
				
			}
			
		}
		catch (Exception ex) {
			logger.error("Exception in monthlyIncomeExpenditureReportDebit : " + ex);
		}
		logger.debug("Exit:public List monthlyIncomeExpenditureReportDebit(int year, int id, int month, String period,int isConsolidated)");
	return incExpList;

	}
	public List getIncomeExpenditureReportCredit(String fromDate, String uptoDate, int SocietyID,String type,int isConsolidated,String formatType) {
		List incExpList=new ArrayList();
		String firstDate = null;;
		String lastDate=null;
		
		try {
			logger.debug("Entry:public List monthlyIncomeExpenditureReportCredit(int year, int id, int month, String period,String formatType)");
		
			
			incExpList=reportDAO.getIncomeExpenditureReport(fromDate, uptoDate, SocietyID,1,type,isConsolidated,formatType);
			
			for(int i=0;incExpList.size()>i;i++){
				ReportDetailsVO incVO=(ReportDetailsVO) incExpList.get(i);
				if(incVO.getTotalAmount().compareTo(BigDecimal.ZERO)==0){
					incExpList.remove(i);
					--i;
				}
				
			}
			
		}
		catch (Exception ex) {
			logger.error("Exception in monthlyIncomeExpenditureReportCredit : " + ex);
		}
		logger.debug("Exit:public List monthlyIncomeExpenditureReportCredit(int year, int id, int month, String period,String formatType)");
	return incExpList;

	}
	
	public List getDetailIncomeExpenditureReport(String fromDate, String uptoDate, int SocietyID,int groupId,String type,int isConsolidated,String formatType) {
		List incExpList=new ArrayList();
				
		try {




			logger.debug("Entry:public List getDetailIncomeExpenditureReport(String fromDate, String uptoDate, int SocietyID,int groupId,int isConsolidated,String formatType)");
		
			

			incExpList=reportDAO.getIncomeExpenditureReport(fromDate, uptoDate, SocietyID,groupId,type,isConsolidated,formatType);
			for(int i=0;incExpList.size()>i;i++){
				ReportDetailsVO incVO=(ReportDetailsVO) incExpList.get(i);
				if(incVO.getTotalAmount().compareTo(BigDecimal.ZERO)==0){
					incExpList.remove(i);
					--i;
				}
				
			}
			
		}
		catch (Exception ex) {
			logger.error("Exception in getDetailIncomeExpenditureReport : " + ex);
		}
		logger.debug("Exit:public List getDetailIncomeExpenditureReport(String fromDate, String uptoDate, int SocietyID,int groupId,String formatType)");
	return incExpList;
	}
	
	/*public ReportVO getBalanceSheet(int societyID,String fromDate,String uptoDate){
		ReportVO balanceSheetVO =new ReportVO();
		logger.debug("Entry : public ReportVO getBlanceSheet(int societyID,String fromDate,String uptoDate)" +fromDate+"  "+uptoDate);
		
		try {
			List balanceSheetList=new ArrayList();
			
			List incomeList=reportDAO.getIncomeExpenditureReport(fromDate, uptoDate, societyID,1,"IE");
			List expenseList=reportDAO.getIncomeExpenditureReport(fromDate, uptoDate, societyID,2,"IE");
		
			
			RptMonthYearVO profitLossVO=calculateProfitLossDetails(incomeList, expenseList, societyID,fromDate,uptoDate);
			
			List assetList=getAssetsDetails(fromDate, uptoDate, societyID,profitLossVO);
			List liabilitiesList=getLiabilitiesDetails(fromDate, uptoDate, societyID,profitLossVO);
			
		    List templiabilitiesList=getLiabilitiesDetails(fromDate, uptoDate, societyID,profitLossVO);
			    balanceSheetVO.setIncomeTxList(incomeList);
			    balanceSheetVO.setExpenseTxList(expenseList);
				balanceSheetVO.setAssetsList(assetList);
				
				balanceSheetVO.setLiabilityList(templiabilitiesList);
				
				for(int i=0;assetList.size()>i;i++){
					
					RptMonthYearVO achVO=(RptMonthYearVO) assetList.get(i);
					
					liabilitiesList.add(achVO);
					
				}
				
               ReportVO dateVO=dateutility.getReportDate(fromDate, uptoDate);
				
                balanceSheetVO.setFromDate(dateVO.getFromDate());
                balanceSheetVO.setUptDate(dateVO.getUptDate());	
				balanceSheetVO.setBalanceSheetList(liabilitiesList);
				
		} catch (Exception e) {
			logger.error("Exception occured while preparing IncomeExpense Report "+e);
		}
			
		
		logger.debug("Exit : public ReportVO getIncomeExpenseReport(int societyID,String fromDate,String uptoDate)"+balanceSheetVO.getBalanceSheetList().size());
		return balanceSheetVO;
	}*/
	
	
	  
	   
	   
	//private List getLiabilities(String fromDate,String uptoDate,int societyId,RptMonthYearVO profitLossVO){
		
	//}
	   
	
	private List getLiabilitiesDetails(String fromDate, String uptoDate, int societyID,RptMonthYearVO profitLossVO) {
		List liabilitiesList=new ArrayList();
		List<AccountHeadVO> liabilitiesLedgerList=new ArrayList<AccountHeadVO>();
		List groupList=new ArrayList();
		String firstDate=null;	
		String lastDate=null;
		BigDecimal totalCur=BigDecimal.ZERO;
		BigDecimal totalPrev=BigDecimal.ZERO;
		try {
			logger.debug("Entry: public List getLiabilitiesDetails(String fromDate, String uptoDate, int SocietyID)");
			groupList=ledgerService.getAllGroupList(4,societyID);
			String tempCategoryName = "";
			RptMonthYearVO rptNameVO=new RptMonthYearVO();
			rptNameVO.setC_head_name("LIABILITIES");
			liabilitiesList.add(0, rptNameVO);
			for(int j=0;groupList.size()>j;j++){
			BigDecimal crOpnBal=BigDecimal.ZERO;
			BigDecimal drOpnBal=BigDecimal.ZERO;
			BigDecimal crClsBal=BigDecimal.ZERO;
			BigDecimal drClsBal=BigDecimal.ZERO;
			BigDecimal openingBal=BigDecimal.ZERO;
			BigDecimal closingBal=BigDecimal.ZERO;
			AccountHeadVO achVO=(AccountHeadVO) groupList.get(j);
			
			RptMonthYearVO rptVO=new RptMonthYearVO();
			logger.debug("here "+achVO.getStrAccountHeadName());
			liabilitiesLedgerList=ledgerService.getLedgerList(societyID, achVO.getAccountGroupID(),"G");
			
			//acHvO=getOpeningClosingBalance(societyID, groupID, fromDate, uptoDate, "G");
			
		
		for(AccountHeadVO acHVO:liabilitiesLedgerList){
					
			acHVO=ledgerService.getOpeningClosingBalance(societyID, acHVO.getLedgerID(), fromDate, uptoDate,"L");
			
			crClsBal=crClsBal.add(acHVO.getCrClsBal());
			drClsBal=drClsBal.add(acHVO.getDrClsBal());
			crOpnBal=crOpnBal.add(acHVO.getCrOpnBal());
			drOpnBal=drOpnBal.add(acHVO.getDrOpnBal());
			openingBal=openingBal.add(acHVO.getOpeningBalance());
			closingBal=closingBal.add(acHVO.getClosingBalance());
			
			logger.debug("==============================="+closingBal);
		}
		
		
		
		achVO.setAccountGroupID(achVO.getAccountGroupID());
		achVO.setStrAccountHeadName(achVO.getStrAccountHeadName());
		achVO.setClosingBalance(closingBal);
		achVO.setOpeningBalance(openingBal);
		achVO.setCrOpnBal(crOpnBal);
		achVO.setDrOpnBal(drOpnBal);
		achVO.setCrClsBal(crClsBal);
		achVO.setDrClsBal(drClsBal);
			
			totalCur=totalCur.add(achVO.getClosingBalance());
			totalPrev=totalPrev.add(achVO.getOpeningBalance());
			//logger.info(totalCur+"==============?"+achVO.getOpeningBalance()+"  "+achVO.getClosingBalance()+" "+achVO.getStrAccountHeadName());
			logger.debug("Here     "+totalCur+"    "+totalPrev+"  "+achVO.getClosingBalance()+"  "+achVO.getOpeningBalance());
			
			
	
		if((achVO.getClosingBalance().signum()==0)&&(achVO.getOpeningBalance().signum()==0)){
				groupList.remove(j);
				--j;
				continue;
			}
			
			String categoryName=achVO.getStrAccountHeadName().trim();
			if (!tempCategoryName.equalsIgnoreCase(categoryName)) {
				rptVO.setC_head_name(categoryName);
				tempCategoryName = categoryName;
			} else {
				rptVO.setC_head_name(" ");
			}
			rptVO.setCategoryID(achVO.getAccountGroupID()+"");
			rptVO.setCategoryName(achVO.getStrAccountHeadName());
			rptVO.setCurrentBalance(achVO.getClosingBalance());
			rptVO.setPrevBalance(achVO.getOpeningBalance());
			logger.debug("r------------"+rptVO.getCategoryName());
			liabilitiesList.add(rptVO);
			
			
		}
			logger.info(profitLossVO.getCredit()+"here incExpP debuit "+profitLossVO.getDebit());
			//if(profitLossVO.getCredit().signum()!=0){
				
				profitLossVO.setCurrentBalance(profitLossVO.getDiffernce().add(profitLossVO.getOpeningBalance()));
				profitLossVO.setPrevBalance(profitLossVO.getOpeningBalance());
				
				if(profitLossVO.getCurrentBalance().signum()!=0){
					
				liabilitiesList.add(liabilitiesList.size(), profitLossVO);
				totalCur=totalCur.add(profitLossVO.getCurrentBalance());
				totalPrev=totalPrev.add(profitLossVO.getPrevBalance());
				}
				
			//}
			//BigDecimal totalCurrent=totalCur.add(profitLossVO.getCurrentBalance());
			RptMonthYearVO rptVO=new RptMonthYearVO();
			rptVO.setC_head_name("TOTAL");
			rptVO.setCategoryName("TOTAL");
			rptVO.setCurrentBalance(totalCur);
			rptVO.setPrevBalance(totalPrev);
			liabilitiesList.add(liabilitiesList.size(), rptVO);
		}
		catch (Exception ex) {
			logger.error("Exception in getLiabilitiesDetails : " + ex);
		}
		logger.debug("Exit: public List getLiabilitiesDetails(String fromDate, String uptoDate, int SocietyID)"+liabilitiesList.size());
	return liabilitiesList;

	}
	private List getAssetsDetails(String fromDate, String uptoDate, int societyID,RptMonthYearVO profitLossVO) {
		List<AccountHeadVO> assetsLedgersList=new ArrayList<AccountHeadVO>();
		List assetList=new ArrayList();
		List assetsGroupList=new ArrayList();
		String firstDate = null;;
		String lastDate=null;
		BigDecimal totalCur=BigDecimal.ZERO;
		BigDecimal totalPrev=BigDecimal.ZERO;
		
		try {
			logger.debug("Entry: public List getAssetsDetails(String fromDate, String uptoDate, int SocietyID)");
		
			
			assetsGroupList=ledgerService.getAllGroupList(3,societyID);
			String tempCategoryName = "";
			RptMonthYearVO rptNameVO=new RptMonthYearVO();
			rptNameVO.setC_head_name("ASSETS");
			assetList.add(0, rptNameVO);
			for(int i=0;assetsGroupList.size()>i;i++){
				BigDecimal crOpnBal=BigDecimal.ZERO;
				BigDecimal drOpnBal=BigDecimal.ZERO;
				BigDecimal crClsBal=BigDecimal.ZERO;
				BigDecimal drClsBal=BigDecimal.ZERO;
				BigDecimal openingBal=BigDecimal.ZERO;
				BigDecimal closingBal=BigDecimal.ZERO;
				AccountHeadVO achVO=(AccountHeadVO) assetsGroupList.get(i);
				
				RptMonthYearVO rptVO=new RptMonthYearVO();
				assetsLedgersList=ledgerService.getLedgerList(societyID, achVO.getAccountGroupID(),"G");
				
				//acHvO=getOpeningClosingBalance(societyID, groupID, fromDate, uptoDate, "G");
				
			
			for(AccountHeadVO acHVO:assetsLedgersList){
						
				acHVO=ledgerService.getOpeningClosingBalance(societyID, acHVO.getLedgerID(), fromDate, uptoDate,"L");
				
				crClsBal=crClsBal.add(acHVO.getCrClsBal());
				drClsBal=drClsBal.add(acHVO.getDrClsBal());
				crOpnBal=crOpnBal.add(acHVO.getCrOpnBal());
				drOpnBal=drOpnBal.add(acHVO.getDrOpnBal());
				openingBal=openingBal.add(acHVO.getOpeningBalance());
				closingBal=closingBal.add(acHVO.getClosingBalance());
				
				
			}
			
			
			
			achVO.setAccountGroupID(achVO.getAccountGroupID());
			achVO.setStrAccountHeadName(achVO.getStrAccountHeadName());
			achVO.setClosingBalance(closingBal);
			achVO.setOpeningBalance(openingBal);
			achVO.setCrOpnBal(crOpnBal);
			achVO.setDrOpnBal(drOpnBal);
			achVO.setCrClsBal(crClsBal);
			achVO.setDrClsBal(drClsBal);
				
				totalCur=totalCur.add(achVO.getClosingBalance());
				totalPrev=totalPrev.add(achVO.getOpeningBalance());
				//logger.info(totalCur+"==============?"+achVO.getOpeningBalance()+"  "+achVO.getClosingBalance()+" "+achVO.getStrAccountHeadName());
				//logger.info("Here     "+totalCur+"    "+totalPrev);
				
				
			
			if((achVO.getClosingBalance().signum()==0)&&(achVO.getOpeningBalance().signum()==0)){
					assetsGroupList.remove(i);
					--i;
					continue;
				}
				
				String categoryName=achVO.getStrAccountHeadName().trim();
				if (!tempCategoryName.equalsIgnoreCase(categoryName)) {
					rptVO.setC_head_name(categoryName);
					tempCategoryName = categoryName;
				} else {
					rptVO.setC_head_name(" ");
				}
				rptVO.setCategoryID(achVO.getAccountGroupID()+"");
				rptVO.setCategoryName(achVO.getStrAccountHeadName());
				rptVO.setCurrentBalance(achVO.getClosingBalance());
				rptVO.setPrevBalance(achVO.getOpeningBalance());
				//logger.info("Here "+rptVO.getC_head_name()+" "+rptVO.getCategoryName()+" "+rptVO.getCurrentBalance());
				assetList.add(rptVO);
				
				
			}
			//logger.info(profitLossVO.getDebit()+"here incExpP credit "+profitLossVO.getCredit());
			/*if(profitLossVO.getDebit().signum()!=0){
				profitLossVO.setCurrentBalance(profitLossVO.getDebit().add(profitLossVO.getOpeningBalance()));
				profitLossVO.setPrevBalance(profitLossVO.getOpeningBalance());
				if(profitLossVO.getCurrentBalance().signum()!=0){
				assetList.add(assetList.size(), profitLossVO);
				totalCur=totalCur.add(profitLossVO.getCurrentBalance());
				totalPrev=totalPrev.add(profitLossVO.getPrevBalance());
				}
				
			}*/
			//BigDecimal totalCurrent=totalCur.add(profitLossVO.getCurrentBalance());
			RptMonthYearVO rptVO=new RptMonthYearVO();
			rptVO.setC_head_name("TOTAL");
			rptVO.setCategoryName("TOTAL");
			rptVO.setCurrentBalance(totalCur);
			rptVO.setPrevBalance(totalPrev);
			assetList.add(assetList.size(), rptVO);
			
			
		}
		catch (Exception ex) {
			logger.error("Exception in getAssetsDetails : " + ex);
		}
		logger.debug("Exit: public List getAssetsDetails(String fromDate, String uptoDate, int SocietyID)"+assetList.size());
	return assetList;

	}
	
	public ReportDetailsVO calculateProfitLossDetailsForBalanceSheet(List incomeList,List expenseList,int societyID,String fromDate,String uptoDate,String formatType){
		logger.debug("Entry : private RptMonthYearVO calculateProfitLossDetails(AccountHeadVO incomeVO,AccountHeadVO expenseVO,int societyID,String formatType) ");
		ReportDetailsVO rptVO=new ReportDetailsVO();
		BigDecimal totalIncCur=BigDecimal.ZERO;
		BigDecimal totalExpCur=BigDecimal.ZERO;
		BigDecimal totalIncPrev=BigDecimal.ZERO;
		BigDecimal totalExpPrev=BigDecimal.ZERO;
		BigDecimal differenceIncCur=BigDecimal.ZERO;
		BigDecimal incExpPlusPLPrev=BigDecimal.ZERO;
		BigDecimal differenceIncPrev=BigDecimal.ZERO;
		BigDecimal differenceExpCur=BigDecimal.ZERO;
		BigDecimal incExpPlusPLCur=BigDecimal.ZERO;
		BigDecimal openingBal=BigDecimal.ZERO;
		BigDecimal closingBal=BigDecimal.ZERO;
		List incListForPrev=new ArrayList();
		List expListForPrev=new ArrayList();
		try {
		
		
		AccountHeadVO profitLossVO=ledgerService.getProfitLossLedger(societyID);
		incListForPrev=reportDAO.getIncomeExpenditureReport(profitLossVO.getEffectiveDate(), dateutility.getPrevOrNextDate(fromDate, 1), societyID, 1, "IE",0,formatType);
		expListForPrev=reportDAO.getIncomeExpenditureReport(profitLossVO.getEffectiveDate(), dateutility.getPrevOrNextDate(fromDate, 1), societyID, 2, "IE",0,formatType);
		totalExpPrev=calculateTotalForList(expListForPrev);
		totalIncPrev=calculateTotalForList(incListForPrev);
		
		logger.debug(totalExpPrev+"<------------------------------>"+totalIncPrev);
		totalIncCur=calculateTotalForList(incomeList);
		totalExpCur=calculateTotalForList(expenseList);
		logger.debug(totalExpCur+"<------------------------------>"+totalIncCur);
		AccountHeadVO tempVO=new AccountHeadVO();
		AccountHeadVO tempCurVO=new AccountHeadVO();
			tempVO = ledgerService.getOpeningClosingBalance(societyID, profitLossVO.getLedgerID(), profitLossVO.getEffectiveDate(), dateutility.getPrevOrNextDate(fromDate, 1), "L");
			tempCurVO=ledgerService.getOpeningClosingBalance(societyID, profitLossVO.getLedgerID(), fromDate, uptoDate, "L");
			logger.debug("Opening Bal "+profitLossVO.getOpeningBalance()+"ProfitLoss Vo Closing Bal "+profitLossVO.getClosingBalance());
			logger.debug("Opening Bal "+tempVO.getOpeningBalance()+" tempVO Closing Balance "+tempVO.getClosingBalance());
			logger.debug("Opening Bal "+tempVO.getOpeningBalance()+" tempCurVO Closing Balance "+tempVO.getClosingBalance()+" ----- "+tempCurVO.getClPosBal());
		BigDecimal pLTransactionTotalPrev=tempVO.getClosingBalance().subtract(tempVO.getOpeningBalance());
		BigDecimal plTransactionTotalCur=tempCurVO.getClosingBalance().subtract(tempCurVO.getOpeningBalance());
		
		int resultCur=totalIncCur.compareTo(totalExpCur);
		int resultPrev=totalIncPrev.compareTo(totalExpPrev);
			
			differenceIncCur=totalIncCur.subtract(totalExpCur);
			incExpPlusPLCur=differenceIncCur.add(plTransactionTotalCur);
		
			
	
			differenceIncPrev=totalIncPrev.subtract(totalExpPrev);
		
		    incExpPlusPLPrev=differenceIncPrev.add(pLTransactionTotalPrev); 
		    //If no previous year transactions are available
			if(totalExpPrev.compareTo(totalIncPrev)==0){
			//If ProfitLossVO's opening balance is non zero
			if(profitLossVO.getOpeningBalance().compareTo(BigDecimal.ZERO)!=0){
				openingBal=profitLossVO.getOpeningBalance();
			}else
		openingBal=differenceIncPrev;
		}else
		openingBal=profitLossVO.getOpeningBalance().add(incExpPlusPLPrev);
		if(differenceIncCur.compareTo(BigDecimal.ZERO)==0)
			closingBal=openingBal;
		else
		closingBal=openingBal.add(incExpPlusPLCur);
		logger.debug("Here DifferenceCur "+differenceIncCur+" "+ incExpPlusPLCur+" "+ openingBal+" And DifferencePrev "+differenceIncPrev+" "+ incExpPlusPLPrev+" "+ closingBal);
		rptVO.setCategoryName("Profit / Loss ");
		rptVO.setOpeningBalance(openingBal);
		rptVO.setClosingBalance(closingBal);
		rptVO.setDifference(differenceIncCur);
		if(closingBal.signum()>0){
			rptVO.setCreditBalance(differenceIncCur);
			rptVO.setDebitBalance(BigDecimal.ZERO);
		}else{
			
		rptVO.setCreditBalance(BigDecimal.ZERO);
		rptVO.setDebitBalance(differenceIncCur);
		
		logger.debug(differenceExpCur+"here in00000000000000--------------> "+closingBal+" "+rptVO.getDebitBalance());
		}
		rptVO.setDifferenceInCurrentBal(tempCurVO.getClPosBal());
		logger.debug(rptVO.getCreditBalance()+" here incExpP credit "+rptVO.getDebitBalance());
		//rptVO.setCurrentBalance(BigDecimal.ZERO);
		logger.debug("Profit Loss amount :=============== Opening Balance "+rptVO.getOpeningBalance()+" Here closing Balance  "+rptVO.getClosingBalance()+" difference in inc Exp "+differenceIncPrev+" Profit Loss tx "+pLTransactionTotalPrev+" difference "+differenceExpCur+" "+differenceIncCur);
		
		} catch (Exception e) {
			logger.error("Exception  in calculateProfitLoss : "+e);
		}
		
		logger.debug("Exit : private RptMonthYearVO calculateProfitLossDetails(AccountHeadVO incomeVO,AccountHeadVO expenseVO,int societyID,String formatType) ");
		return rptVO;
	}
	
	public ReportDetailsVO calculateConsolidatedProfitLossDetailsForBalanceSheet(List incomeList,List expenseList,int societyID,String fromDate,String uptoDate,String formatType){
		logger.debug("Entry : private RptMonthYearVO calculateConsolidatedProfitLossDetailsForBalanceSheet(AccountHeadVO incomeVO,AccountHeadVO expenseVO,int societyID,String formatType) ");
		ReportDetailsVO rptVO=new ReportDetailsVO();
		BigDecimal totalIncCur=BigDecimal.ZERO;
		BigDecimal totalExpCur=BigDecimal.ZERO;
		BigDecimal totalIncPrev=BigDecimal.ZERO;
		BigDecimal totalExpPrev=BigDecimal.ZERO;
		BigDecimal differenceIncCur=BigDecimal.ZERO;
		BigDecimal incExpPlusPLPrev=BigDecimal.ZERO;
		BigDecimal differenceIncPrev=BigDecimal.ZERO;
		BigDecimal differenceExpCur=BigDecimal.ZERO;
		BigDecimal incExpPlusPLCur=BigDecimal.ZERO;
		BigDecimal openingBal=BigDecimal.ZERO;
		BigDecimal closingBal=BigDecimal.ZERO;
		List incListForPrev=new ArrayList();
		List expListForPrev=new ArrayList();
		try {
		SocietyVO societyVo=societyService.getSocietyDetails(societyID);
		List societyList=societyService.getSocietyListInDatazone(societyVo.getDataZoneID());
		
		for(int i=0;societyList.size()>i;i++){
		
			SocietyVO societyVO=(SocietyVO) societyList.get(i);
			
		AccountHeadVO profitLossVO=ledgerService.getProfitLossLedger(societyVO.getSocietyID());
		incListForPrev=reportDAO.getIncomeExpenditureReport(profitLossVO.getEffectiveDate(), dateutility.getPrevOrNextDate(fromDate, 1), societyID, 1, "IE",0,formatType);
		expListForPrev=reportDAO.getIncomeExpenditureReport(profitLossVO.getEffectiveDate(), dateutility.getPrevOrNextDate(fromDate, 1), societyID, 2, "IE",0,formatType);
		totalExpPrev=calculateTotalForList(expListForPrev);
		totalIncPrev=calculateTotalForList(incListForPrev);
		
		logger.debug(totalExpPrev+"<------------------------------>"+totalIncPrev);
		totalIncCur=calculateTotalForList(incomeList);
		totalExpCur=calculateTotalForList(expenseList);
		logger.debug(totalExpCur+"<------------------------------>"+totalIncCur);
		AccountHeadVO tempVO=new AccountHeadVO();
		AccountHeadVO tempCurVO=new AccountHeadVO();
			tempVO = ledgerService.getOpeningClosingBalance(societyID, profitLossVO.getLedgerID(), profitLossVO.getEffectiveDate(), dateutility.getPrevOrNextDate(fromDate, 1), "L");
			tempCurVO=ledgerService.getOpeningClosingBalance(societyID, profitLossVO.getLedgerID(), fromDate, uptoDate, "L");
			logger.debug("Opening Bal "+profitLossVO.getOpeningBalance()+"ProfitLoss Vo Closing Bal "+profitLossVO.getClosingBalance());
			logger.debug("Opening Bal "+tempVO.getOpeningBalance()+" tempVO Closing Balance "+tempVO.getClosingBalance());
			logger.debug("Opening Bal "+tempVO.getOpeningBalance()+" tempCurVO Closing Balance "+tempVO.getClosingBalance()+" ----- "+tempCurVO.getClPosBal());
		BigDecimal pLTransactionTotalPrev=tempVO.getClosingBalance().subtract(tempVO.getOpeningBalance());
		BigDecimal plTransactionTotalCur=tempCurVO.getClosingBalance().subtract(tempCurVO.getOpeningBalance());
		
		int resultCur=totalIncCur.compareTo(totalExpCur);
		int resultPrev=totalIncPrev.compareTo(totalExpPrev);
			
			differenceIncCur=totalIncCur.subtract(totalExpCur);
			incExpPlusPLCur=differenceIncCur.add(plTransactionTotalCur);
		
			
	
			differenceIncPrev=totalIncPrev.subtract(totalExpPrev);
		
		    incExpPlusPLPrev=differenceIncPrev.add(pLTransactionTotalPrev); 
		    //If no previous year transactions are available
			if(totalExpPrev.compareTo(totalIncPrev)==0){
			//If ProfitLossVO's opening balance is non zero
			if(profitLossVO.getOpeningBalance().compareTo(BigDecimal.ZERO)!=0){
				openingBal=profitLossVO.getOpeningBalance();
			}else
		openingBal=differenceIncPrev;
		}else
		openingBal=profitLossVO.getOpeningBalance().add(incExpPlusPLPrev);
		if(differenceIncCur.compareTo(BigDecimal.ZERO)==0)
			closingBal=openingBal;
		else
		closingBal=openingBal.add(incExpPlusPLCur);
		logger.debug("Here DifferenceCur "+differenceIncCur+" "+ incExpPlusPLCur+" "+ openingBal+" And DifferencePrev "+differenceIncPrev+" "+ incExpPlusPLPrev+" "+ closingBal);
		rptVO.setCategoryName("Profit / Loss ");
		rptVO.setOpeningBalance(rptVO.getOpeningBalance().add(openingBal));
		rptVO.setClosingBalance(rptVO.getClosingBalance().add(closingBal));
		rptVO.setDifference(rptVO.getDifference().add(differenceIncCur));
		if(closingBal.signum()>0){
			rptVO.setCreditBalance(rptVO.getCreditBalance().add(differenceIncCur));
			rptVO.setDebitBalance(BigDecimal.ZERO);
		}else{
			
		rptVO.setCreditBalance(BigDecimal.ZERO);
		rptVO.setDebitBalance(rptVO.getDebitBalance().add(differenceIncCur));
		
		logger.debug(differenceExpCur+"here in00000000000000--------------> "+closingBal+" "+rptVO.getDebitBalance());
		}
		rptVO.setDifferenceInCurrentBal(rptVO.getDifferenceInCurrentBal().add(tempCurVO.getClPosBal()));
		logger.debug(rptVO.getCreditBalance()+" here incExpP credit "+rptVO.getDebitBalance());
		//rptVO.setCurrentBalance(BigDecimal.ZERO);
		logger.debug("Profit Loss amount :=============== Opening Balance "+rptVO.getOpeningBalance()+" Here closing Balance  "+rptVO.getClosingBalance()+" difference in inc Exp "+differenceIncPrev+" Profit Loss tx "+pLTransactionTotalPrev+" difference "+differenceExpCur+" "+differenceIncCur);
		}
		} catch (Exception e) {
			logger.error("Exception  in calculateProfitLoss : "+e);
		}
		
		logger.debug("Exit : private RptMonthYearVO calculateConsolidatedProfitLossDetailsForBalanceSheet(AccountHeadVO incomeVO,AccountHeadVO expenseVO,int societyID) ");
		return rptVO;
	}
	
	public ReportDetailsVO calculateProfitLossDetailsForTrialBalance(List incomeList,List expenseList,int societyID,String fromDate,String uptoDate){
		logger.debug("Entry : private RptMonthYearVO calculateProfitLossDetails(AccountHeadVO incomeVO,AccountHeadVO expenseVO,int societyID) ");
		ReportDetailsVO rptVO=new ReportDetailsVO();
		BigDecimal totalIncCur=BigDecimal.ZERO;
		BigDecimal totalExpCur=BigDecimal.ZERO;
		BigDecimal totalIncPrev=BigDecimal.ZERO;
		BigDecimal totalExpPrev=BigDecimal.ZERO;
		BigDecimal differenceIncCur=BigDecimal.ZERO;
		BigDecimal incExpPlusPLPrev=BigDecimal.ZERO;
		BigDecimal differenceIncPrev=BigDecimal.ZERO;
		BigDecimal differenceExpCur=BigDecimal.ZERO;
		BigDecimal incExpPlusPLCur=BigDecimal.ZERO;
		BigDecimal openingBal=BigDecimal.ZERO;
		BigDecimal closingBal=BigDecimal.ZERO;
		List incListForPrev=new ArrayList();
		List expListForPrev=new ArrayList();
		try {
		
		
		AccountHeadVO profitLossVO=ledgerService.getProfitLossLedger(societyID);
		incListForPrev=reportDAO.getIncomeExpenditureReport(profitLossVO.getEffectiveDate(), dateutility.getPrevOrNextDate(fromDate, 1), societyID, 1, "IE",0,"S");
		expListForPrev=reportDAO.getIncomeExpenditureReport(profitLossVO.getEffectiveDate(), dateutility.getPrevOrNextDate(fromDate, 1), societyID, 2, "IE",0,"S");
		totalExpPrev=calculateTotalForList(expListForPrev);
		totalIncPrev=calculateTotalForList(incListForPrev);
		
		logger.debug(totalExpPrev+"<------------------------------>"+totalIncPrev);
		
		logger.debug(totalExpCur+"<---------------Cur--------------->"+totalIncCur);
		AccountHeadVO tempVO=new AccountHeadVO();
		AccountHeadVO tempCurVO=new AccountHeadVO();
			tempVO = ledgerService.getOpeningClosingBalance(societyID, profitLossVO.getLedgerID(), profitLossVO.getEffectiveDate(), dateutility.getPrevOrNextDate(fromDate, 1), "L");
			tempCurVO=ledgerService.getOpeningClosingBalance(societyID, profitLossVO.getLedgerID(), fromDate, uptoDate, "L");
			logger.debug("Opening Bal "+profitLossVO.getOpeningBalance()+"ProfitLoss Vo Closing Bal "+profitLossVO.getClosingBalance());
			logger.debug("Opening Bal "+tempVO.getOpeningBalance()+" tempVO Closing Balance "+tempVO.getClosingBalance());
			logger.debug("Opening Bal "+tempVO.getOpeningBalance()+" tempCurVO Closing Balance "+tempVO.getClosingBalance()+" ----- "+tempCurVO.getClPosBal());
		BigDecimal pLTransactionTotalPrev=tempVO.getClosingBalance().subtract(tempVO.getOpeningBalance());
		BigDecimal plTransactionTotalCur=tempCurVO.getClosingBalance().subtract(tempCurVO.getOpeningBalance());
		
		int resultCur=totalIncCur.compareTo(totalExpCur);
		int resultPrev=totalIncPrev.compareTo(totalExpPrev);
			
			differenceIncCur=totalIncCur.subtract(totalExpCur);
			incExpPlusPLCur=differenceIncCur.add(plTransactionTotalCur);
		
			
	
			differenceIncPrev=totalIncPrev.subtract(totalExpPrev);
		
		    incExpPlusPLPrev=differenceIncPrev.add(pLTransactionTotalPrev); 
		    //If no previous year transactions are available
		if(totalExpPrev.compareTo(totalIncPrev)==0){
			//If ProfitLossVO's opening balance is non zero
			if(profitLossVO.getOpeningBalance().compareTo(BigDecimal.ZERO)!=0){
			openingBal=profitLossVO.getOpeningBalance();
			}else
		openingBal=differenceIncPrev;
		}else
		openingBal=profitLossVO.getOpeningBalance().add(incExpPlusPLPrev);
		if(differenceIncCur.compareTo(BigDecimal.ZERO)==0)
			closingBal=openingBal;
		else
		closingBal=openingBal.add(incExpPlusPLCur);
		logger.debug("Here DifferenceCur "+differenceIncCur+" "+ incExpPlusPLCur+" "+ openingBal+" And DifferencePrev "+differenceIncPrev+" "+ incExpPlusPLPrev+" "+ closingBal);
		rptVO.setCategoryName("Profit / Loss ");
		rptVO.setOpeningBalance(openingBal);
		rptVO.setClosingBalance(closingBal);
		rptVO.setDifference(differenceIncCur);
		if(closingBal.signum()>0){
			rptVO.setCreditBalance(differenceIncCur);
			rptVO.setDebitBalance(BigDecimal.ZERO);
		}else{
			
		rptVO.setCreditBalance(BigDecimal.ZERO);
		rptVO.setDebitBalance(differenceIncCur);
		
		logger.debug(differenceExpCur+"here in00000000000000--------------> "+closingBal+" "+rptVO.getDebitBalance());
		}
		if(tempCurVO.getClPosBal().compareTo(BigDecimal.ZERO)==0){
		rptVO.setDifferenceInCurrentBal(tempCurVO.getClPosBal());
		}else
		rptVO.setDifferenceInCurrentBal(tempCurVO.getClPosBal().subtract(tempVO.getClPosBal()));
		logger.debug(rptVO.getCreditBalance()+" here incExpP credit "+rptVO.getDebitBalance()+" Difference in current Bal "+tempCurVO.getClPosBal());
		//rptVO.setCurrentBalance(BigDecimal.ZERO);
		logger.debug("Profit Loss amount :=============== Opening Balance "+rptVO.getOpeningBalance()+" Here closing Balance  "+rptVO.getClosingBalance()+" difference in inc Exp "+differenceIncPrev+" Profit Loss tx "+pLTransactionTotalPrev+" difference "+differenceExpCur+" "+differenceIncCur);
		
		} catch (Exception e) {
			logger.error("Exception  in calculateProfitLoss : "+e);
		}
		
		logger.debug("Exit : private RptMonthYearVO calculateProfitLossDetails(AccountHeadVO incomeVO,AccountHeadVO expenseVO,int societyID) ");
		return rptVO;
	}
	
	private BigDecimal calculateTotalForList(List inputList){
		logger.debug("Entry : private BigDecimal calculateToatlForList(List inputList) ");
		BigDecimal total=BigDecimal.ZERO;
		
		if(inputList.size()>0){
			for(int i=0;inputList.size()>i;i++){
				ReportDetailsVO rptVO=(ReportDetailsVO) inputList.get(i);
				total=total.add(rptVO.getTotalAmount());
				
			}
		}
		
		
		logger.debug("Exit : private BigDecimal calculateToatlForList(List inputList) ");
		return total;
	}
	
	/*
	public List getMonthlyIncomeExpenditureReportDetailed(String firstDate,String lastDate ,int societyId,int groupID) {
		List incExpList=new ArrayList();
		try {
			logger.debug("Entry:public List getMonthlyIncomeExpenditureReportDetailed(String firstDate,String lastDate ,int societyId,int groupID)");
			
				incExpList = reportDAO.getMonthlyIncomeExpenditureReportDetailed(firstDate, lastDate, societyId, groupID);
			//	logger.info("###############################"+incExpList.size()+incExpList);
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getMonthlyIncExpDeb : " + ex);	
		}
		catch (Exception ex) {
			logger.error("Exception in getMonthlyIncomeExpenditureReportDetailed : " + ex);
		}
		logger.debug("Exit:public List getMonthlyIncomeExpenditureReportDetailed(String firstDate,String lastDate ,int societyId,int groupID)"+incExpList.size());
	return incExpList;

	}
	
	
	public List getCashInHand(int societyID,int year,String firstDate,String toDate){
		BigDecimal cashInHand = new BigDecimal("0.0");
		List accList=null;
		try {
		    
			logger.debug("Entry :public BigDecimal getCashInHand(int id,int societyID,int year)");
			
			String str="Cash";
			accList=reportDAO.getAccDetails(year,societyID,str);
			for(int i=0;accList.size()>i;i++){
				RptMonthYearVO rpMonthYearVO=(RptMonthYearVO) accList.get(i);
			BigDecimal totalCrdit=reportDAO.totalAmountIncomeAndExpense(societyID, firstDate, toDate, "Cash_Cr", rpMonthYearVO.getCategoryID());
			BigDecimal totalDebit=reportDAO.totalAmountIncomeAndExpense(societyID, firstDate, toDate, "Cash_Db", rpMonthYearVO.getCategoryID());
			
			BigDecimal balance=totalCrdit.subtract(totalDebit).setScale(2);
			cashInHand=balance.add(rpMonthYearVO.getOpeningBalance()).setScale(2);
			rpMonthYearVO.setTotalBalance(cashInHand);
			
			
			
			}	
			
			logger.debug("Exit :public BigDecimal getCashInHand(int id,int societyID,int year)");
		} catch (EmptyResultDataAccessException e) {
			cashInHand=new BigDecimal("0.00");
			logger.info("Cash in hand is not available for  opening balance for societyId "+societyID+" for the year "+year +e);
		
	} catch (NullPointerException e) {
		cashInHand=new BigDecimal("0.00");
		logger.info("Cash in hand is not available for  opening balance for societyId "+societyID+" for the year "+year +e);
	
} 
		catch (Exception e) {
		cashInHand=new BigDecimal("0.00");
		logger.error("Exception : "+e);
	}
		return accList;
	}
	
	
	public List getCurrentBalNewSys(int societyID,String firstDate,String toDate,int groupID){
		BigDecimal cashInHand = new BigDecimal("0.0");
		List accList=null;
		try {
		    
			logger.debug("Entry :public BigDecimal getCashInHand(int societyID,int year)");
			
			
			accList=reportDAO.getNewSysAccDetails(groupID, societyID);
			for(int i=0;accList.size()>i;i++){
				RptMonthYearVO rpMonthYearVO=(RptMonthYearVO) accList.get(i);
				AccountHeadVO achVO=new AccountHeadVO();
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				 DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
		            

		        LocalDate date = formatter.parseLocalDate(toDate);

		       
		        int month=date.getMonthOfYear();
		        int year=date.getYear();
		        Date uptoDate=dateutility.getMonthLastDate(month, year);
		        
		        
		        achVO=ledgerService.getOpeningClosingBalance(societyID, rpMonthYearVO.getLedgerID(), firstDate, sdf.format(uptoDate), "L");
			
			
			rpMonthYearVO.setTotalBalance(achVO.getClosingBalance());
			logger.debug("here closing bal is "+rpMonthYearVO.getAccName());
			
			
			}	
			
			logger.debug("Exit :public BigDecimal getCashInHand(int id,int societyID,int year)");
		} catch (EmptyResultDataAccessException e) {
			cashInHand=new BigDecimal("0.00");
			logger.info("Cash in hand is not available for  opening balance for societyId "+societyID+" for the year ");
		
	} catch (NullPointerException e) {
		cashInHand=new BigDecimal("0.00");
		logger.info("Cash in hand is not available for  opening balance for societyId "+societyID+" for the year ");
	
} 
		catch (Exception e) {
		cashInHand=new BigDecimal("0.00");
		logger.error("Exception : "+e);
	}
		return accList;
	}
	public List getBankDetails(int societyID,int year,String firstDate, String lastDate){
		BigDecimal bankBalance = new BigDecimal("0.0");
		List accList=null;
		try {
			
			
		
			logger.debug("Entry :public BigDecimal getBankDetails(int id,int societyID,int year)");
			
			String str="Bank";
			accList=reportDAO.getAccDetails(year,societyID,str);
			for(int i=0;accList.size()>i;i++){
				RptMonthYearVO rpMonthYearVO=(RptMonthYearVO) accList.get(i);
			BigDecimal totalCrdit=reportDAO.totalAmountIncomeAndExpense(societyID, firstDate, lastDate, "Bank_Cr", rpMonthYearVO.getCategoryID());
			BigDecimal totalDebit=reportDAO.totalAmountIncomeAndExpense(societyID, firstDate, lastDate, "Bank_Db", rpMonthYearVO.getCategoryID());
			
			BigDecimal balance=totalCrdit.subtract(totalDebit).setScale(2);
			bankBalance=balance.add(rpMonthYearVO.getOpeningBalance()).setScale(2);
			rpMonthYearVO.setTotalBalance(bankBalance);
			
			
			
			}	
			logger.debug("Exit :public BigDecimal getCashInHand(int id,int societyID,int year)");
		} catch (EmptyResultDataAccessException e) {
			bankBalance=new BigDecimal("0.00");
			logger.info("Bank account details is not available for  opening balance for societyId "+societyID+" for the year "+year +e);
		}
		 catch (NullPointerException e) {
				bankBalance=new BigDecimal("0.00");
				logger.info("Bank account details is not available for  opening balance for societyId "+societyID+" for the year "+year +e);
			}
		catch (Exception e) {
			bankBalance=new BigDecimal("0.00");
			logger.error("Exception : "+e);
		}
		return accList;
	}
	
	public RptMonthYearVO getAccDetails(int year,int id,String str){
		RptMonthYearVO rptVo=new RptMonthYearVO();
		logger.debug("Entry :public RptMonthYearVO getAccDetails(int year,String strSocietyID,String str) ");
		
		try {
			//rptVo=reportDAO.getAccDetails(year, id, str);
		} catch (Exception e) {
		logger.error("Exception "+e);
		}
		
		logger.debug("Exit : public RptMonthYearVO getAccDetails(int year,String strSocietyID, String str)");
		
		return rptVo;
	}
	
	public BigDecimal totalAmountIncomeAndExpense(int id,String fromTxDate, String toTxDate, String searchTxType) {
		BigDecimal totalAmount = new BigDecimal("0.00");
		
		try{
		
		logger.debug("Entry : public BigDecimal totalAmountIncomeAndExpense(String strSocietyID,String fromTxDate, String toTxDate, String searchTxType)");
		
		totalAmount = reportDAO.totalAmountIncomeAndExpense(id, fromTxDate, toTxDate, searchTxType,"");
				
		logger.debug("Exit : public BigDecimal totalAmountIncomeAndExpense(String strSocietyID,String fromTxDate, String toTxDate, String searchTxType)");
		
		}catch(Exception Ex){	
			
			logger.error("Exception in totalAmountIncomeAndExpense : "+Ex);
			
		}
		return totalAmount;
	
	}
	
	public List monthlycategoryWiseReportDetails(int year, int societyID, int categoryID,int month,String type, String period) {
		List incExpList=new ArrayList();
		String firstDate=null;
		String lastDate=null;
		try {
			logger.debug("Entry:	public List monthlycategoryWiseReportDetails(int year, int societyID, int categoryID,int monthIndex)" + year + categoryID);
			if(month!=0){
				if(period.equalsIgnoreCase("quarter")){
					if(month==1){
						firstDate=(year+1)+"-"+month+"-"+01;
						lastDate=(year+1)+"-"+(month+2)+"-"+31;
					}else{
					firstDate=year+"-"+month+"-"+01;
					lastDate=year+"-"+(month+2)+"-"+31;
				}
				}
					else if(period.equalsIgnoreCase("half")){
						if(month==10){
							firstDate=(year)+"-"+month+"-"+01;
							lastDate=(year+1)+"-"+03+"-"+31;
						}else{
						firstDate=year+"-"+month+"-"+01;
						lastDate=year+"-"+(month+5)+"-"+31;
					}
					}
				else{
				 firstDate = year + "-" + month + "-" + 01;
				 lastDate = year  + "-" + (month) + "-" + 31;
				}
				}else {
					firstDate = year + "-" + 04 + "-" + 01;
				     lastDate = (year + 1) + "-" + 03 + "-" + 31;
				}
			
			
			if(categoryID==18){
				incExpList=reportDAO.getComunityHallDetails(year, societyID, categoryID, type,firstDate,lastDate);
			}else{
				
		
				incExpList=reportDAO.monthlycategoryWiseReportDetails(year, societyID, categoryID, firstDate,lastDate,type);
		}
		}
		catch (Exception ex) {
			logger.error("Exception in monthlycategoryWiseReportDetails : " + ex);
		}
		logger.debug("Exit:	public List monthlycategoryWiseReportDetails(int year, int societyID, int categoryID,int monthIndex) ");
	return incExpList;

	}
	
	public List expenseCategorywiseDetails(int year, int societyId, int categoryID,int month,String type, String period) {
		List expenseCategoryList = null;
		String firstDate=null;
		String lastDate=null;
		try {
			logger.debug("Entry:public List expenseCategorywiseDetails(int year, int societyId, int categoryID,int monthIndex)" + year + categoryID);
			if(month!=0){
				if(period.equalsIgnoreCase("quarter")){
					if(month==1){
						firstDate=(year+1)+"-"+month+"-"+01;
						lastDate=(year+1)+"-"+(month+2)+"-"+31;
					}else{
					firstDate=year+"-"+month+"-"+01;
					lastDate=year+"-"+(month+2)+"-"+31;
				}
				}
					else if(period.equalsIgnoreCase("half")){
						if(month==10){
							firstDate=(year)+"-"+month+"-"+01;
							lastDate=(year+1)+"-"+03+"-"+31;
						}else{
						firstDate=year+"-"+month+"-"+01;
						lastDate=year+"-"+(month+5)+"-"+31;
					}
					}
				else{
				 firstDate = year + "-" + month + "-" + 01;
				 lastDate = year  + "-" + (month) + "-" + 31;
				}
				}else {
					firstDate = year + "-" + 04 + "-" + 01;
				     lastDate = (year + 1) + "-" + 03 + "-" + 31;
				}
			
			expenseCategoryList=reportDAO.expenseCategorywiseDetails(societyId, categoryID, firstDate, lastDate,type);
		}
		catch (Exception ex) {
			logger.error("Exception in expenseCategorywiseDetails : " + ex);
		}
		logger.debug("Exit:	public List expenseCategorywiseDetails(int year, int societyId, int categoryID,int monthIndex) ");
	return expenseCategoryList;

	}
	*/
	
	
	
	

	
	public List getMemberDocs(String buildingID) {
		List memDocLst=new ArrayList();
		try {
			logger.debug("Entry:getMemberDocs(String buildingID)");
			
			memDocLst=reportDAO.getMemberDocs( buildingID);
		}
		catch (Exception ex) {
			logger.error("Exception in getMemberDocs(String buildingID) : " + ex);
		}
		logger.debug("Exit:Public List getMemberDocs(String buildingID)");
	return memDocLst;

	}
	
	
	 public List getBankList(){
		
	    List bankList = null;

		try{		
		    logger.debug("Entry :  public List getBankList()" );
				    
			    bankList = reportDAO.getBankList();		
		    		
		    logger.debug("Exit :  public List getBankList()" );
		}catch(Exception Ex){			
			logger.error("Exception in getBankList(): "+Ex);
			
		}
		return bankList;
	}
	
	public List searchMemberList(String strbuildingId){
		
	    List memberList = null;

		try{		
		    logger.debug("Entry : public List searchMemberList(String strbuildingId)" );
				    
			    memberList = reportDAO.searchMemberList(strbuildingId);		
		    		
		    logger.debug("Exit : public List searchMemberList(String strbuildingId)" );
		}catch(Exception Ex){			
			logger.error("Exception in searchMemberList : "+Ex);
			
		}
		return memberList;
	}
	
	public List getMorgageDetails(int societyID){
		List morgageFlatlst=new ArrayList();
		try {
			logger.debug("Entry:public List public List getMorgageDetails(int societyID)");
			
			morgageFlatlst=reportDAO.getMorgageDetails(societyID);
		}
		catch (Exception ex) {
			logger.error("Exception in getMorgageDetails : " + ex);
		}
		logger.debug("Exit:public List public List getMorgageDetails(int societyID)");
	return morgageFlatlst;

	}
	
	public List getMorgageDetailsForMember(int societyID,int memberID){
		List morgageFlatlst=new ArrayList();
		try {
			logger.debug("Entry:public List public List getMorgageDetailsForMember(int societyID,int memberID)");
			
			morgageFlatlst=reportDAO.getMorgageDetailsForMember(societyID,memberID);
		}
		catch (Exception ex) {
			logger.error("Exception in getMorgageDetailsForMember(int societyID,int memberID : " + ex);
		}
		logger.debug("Exit:public List public List getMorgageDetailsForMember(int societyID,int memberID)");
	return morgageFlatlst;

	}
	
	public int insertMoragageDetails(RptMorgageVO morgageVO) {
        String aptId=null; 
		int sucess = 0;  
		
		try{	
		    logger.debug("Entry : public int insertMoragageDetails(RptMorgageVO morgageVO)");
		    aptId=reportDAO.getApartmentID(String.valueOf(morgageVO.getMemberId()));
		
		    if(aptId!=null){
		      sucess = reportDAO.insertMoragageDetails(morgageVO,aptId);
		    }		
		    logger.debug("Exit : public int insertMoragageDetails(RptMorgageVO morgageVO) ");

		}catch(Exception Ex){		
			Ex.printStackTrace();
			logger.error("Exception in insertMoragageDetails : "+Ex);		
		}
		return sucess;
	}
	
	public int updateMorgage(RptMorgageVO morgageVO,String morgId){
      
		int sucess = 0;  
		
		try{	
		    logger.debug("Entry : public int updateMorgage(RptMorgageVO morgageVO,String morgId)");
		 
		     sucess = reportDAO.updateMorgage(morgageVO, morgId);
		    	
		    logger.debug("Exit : public int updateMorgage(RptMorgageVO morgageVO,String morgId) ");

		}catch(Exception Ex){				
			Ex.printStackTrace();
			logger.error("Exception in updateMorgage : "+Ex);		
		}
		return sucess;
	}
	
     public int removMorgage(String morgageId){
		
		int sucess = 0;
		try{	
		    logger.debug("Entry : public int removMorgage(String morgageId)");
		 
		     sucess = reportDAO.removMorgage(morgageId);
		    	
		    logger.debug("Exit : public int removMorgage(String morgageId) ");

		}catch(Exception Ex){				
			Ex.printStackTrace();
			logger.error("Exception in removMorgage : "+Ex);		
		}
		return sucess;
	
     }
     
     public List getMorgageHistory(String apartmentID){
 		
    	 List morgageHistory=null;
 		try{	
 		    logger.debug("Entry : public List getMorgageHistory(String apartmentID)");
 		 
 		   morgageHistory = reportDAO.getMorgageHistory(apartmentID);
 		    	
 		    logger.debug("Exit : public List getMorgageHistory(String apartmentID) ");

 		}catch(Exception Ex){				
 			Ex.printStackTrace();
 			logger.error("Exception in getMorgageHistory : "+Ex);		
 		}
 		return morgageHistory;
 	
      }
	
     public int updateArrears(String arrears,String paidArrears,String arrearsComment,String aptID )
	  {
	  	int sucess=0;
	  	try
	  	{
	  		logger.debug("Entry : public int updateArrears(String paidArrears,String arrearsComment,String aptID )");
	  		
	  		sucess=reportDAO.updateArrears(arrears,paidArrears,arrearsComment,aptID );
	  		
	  		
	  	}
	  	catch(Exception ex)
	  	{
	  		logger.error("Exception in updateCreditDebitTx : "+ex);
	  		
	  	}
	  	
	  	logger.debug("Exit :public int updateArrears(String paidArrears,String arrearsComment,String aptID )");
	  	return sucess;
	  }
     /*
     public List getMemberLedgerList(String strbuildingId,int year,String strSoceityID){
         
         List memberList = null;

         try{       
             logger.debug("Entry : public List getMemberLedgerList(String strbuildingId,int year)" );
                    
                 memberList = reportDAO.getMemberLedgerList(strbuildingId,year,strSoceityID);       
                    
             logger.debug("Exit : public List getMemberLedgerList(String strbuildingId,int year)" );
         }catch(Exception Ex){           
             logger.error("Exception in getMemberLedgerList : "+Ex);
            
         }
         return memberList;
     }*/
     
     public List getDocumentReport(int societyID,String buildingID)
		{
			try
			{
				logger.debug("Entry :   public List getDocumentReport(int societyID,String buildingID)");
				
			    reportList=reportDAO.getDocumentReport(societyID, buildingID);
			    logger.debug("Exit :    public List getDocumentReport(int societyID,String buildingID)");
			}
			catch(NullPointerException e){
				logger.info("No Data available in getDocumentReport");
			}
			catch(IllegalArgumentException ex)
			{
				reportList=null;
			
				logger.info("Exception in getDocumentReport  : "+ex);
			}
			catch(Exception ex)
			{
				//ex.printStackTrace();
				logger.error("Exception in getDocumentReport : "+ex);
			}
		
			return reportList;
			
		}
     
      public List getSingleDocumentReport(int societyID, int documentID){
   			try	{
   				logger.debug("Entry :  public List getSingleDocumentReport(int societyID, int documentID)");
   				
   			    reportList=reportDAO.getSingleDocumentReport(societyID, documentID);
   			    logger.debug("Exit :  public List getSingleDocumentReport(int societyID, int documentID)");
   			}
   			catch(NullPointerException e){
   				logger.info("No Data available in getSingleDocumentReport");
   			}
   			catch(IllegalArgumentException ex){
   				reportList=null;   			
   				logger.info("Exception in getSingleDocumentReport  : "+ex);
   			}
   			catch(Exception ex){
   				//ex.printStackTrace();
   				logger.error("Exception in getSingleDocumentReport : "+ex);
   			}
   		
   			return reportList;
   			
   		}
     
     
     public List getDocumentSummaryReport(int societyID,String buildingID, int documentID)
		{
			try
			{
				logger.debug("Entry :   public List getDocumentSummaryReport(int societyID,String buildingID)");
				
			    reportList=reportDAO.getDocumentSummaryReport(societyID, buildingID, documentID);
			    logger.debug("Exit :    public List getDocumentSummaryReport(int societyID,String buildingID)");
			}
			catch(NullPointerException e){
				logger.info("No Data available in getDocumentSummaryReport");
			}
			catch(IllegalArgumentException ex)
			{
				reportList=null;
			
				logger.info("Exception in getDocumentSummaryReport  : "+ex);
			}
			catch(Exception ex)
			{
				//ex.printStackTrace();
				logger.error("Exception in getDocumentReport : "+ex);
			}
		
			return reportList;
			
		}
     
     public List getDocumentList(){
			try
			{
				logger.debug("Entry :  public List getDocumentList()");
				
			    reportList=reportDAO.getDocumentList();
			    logger.debug("Exit :  public List getDocumentList()");
			}
			catch(NullPointerException e){
				logger.info("No Data available in getDocumentList");
			}
			catch(IllegalArgumentException ex){
				reportList=null;			
				logger.info("Exception in getDocumentList  : "+ex);
			}
			catch(Exception ex){//ex.printStackTrace();
				logger.error("Exception in getDocumentList : "+ex);
			}
		
			return reportList;
			
		}
     
    public List getDocumentList(String type){
			try
			{
				logger.debug("Entry :  public List getDocumentList(String type)");
				
			    reportList=reportDAO.getDocumentList(type);
			    logger.debug("Exit :  public List getDocumentList(String type)");
			}
			catch(NullPointerException e){
				logger.info("No Data available in getDocumentList");
			}
			catch(IllegalArgumentException ex){
				reportList=null;			
				logger.info("Exception in getDocumentList  : "+ex);
			}
			catch(Exception ex){//ex.printStackTrace();
				logger.error("Exception in getDocumentList : "+ex);
			}
		
			return reportList;
			
		}
     
     public List getDocumentStatusList(int documentID){
			try
			{
				logger.debug("Entry :  public List getDocumentStatusList(int documentID)");
				
			    reportList=reportDAO.getDocumentStatusList(documentID);
			    logger.debug("Exit :  public List getDocumentStatusList(int documentID)");
			}
			catch(NullPointerException e){
				logger.info("No Data available in getDocumentStatusList");
			}
			catch(IllegalArgumentException ex){
				reportList=null;			
				logger.info("Exception in getDocumentStatusList  : "+ex);
			}
			catch(Exception ex){//ex.printStackTrace();
				logger.error("Exception in getDocumentStatusList : "+ex);
			}
		
			return reportList;
			
		}
    
     public List getVehicleReport(int societyID,String buildingID)
		{
			try
			{
				logger.debug("Entry :   public List getVehicleReport(int societyID,String buildingID)");
				
			    reportList=reportDAO.getVehicleReport(societyID, buildingID);
			    logger.debug("Exit :    public List getVehicleReport(int societyID,String buildingID)");
			}
			catch(NullPointerException e){
				logger.info("No Data available in getVehicleReport");
			}
			catch(IllegalArgumentException ex){
				reportList=null;			
				logger.info("Exception in getVehicleReport  : "+ex);
			}
			catch(Exception ex){
				//ex.printStackTrace();
				logger.error("Exception in getVehicleReport : "+ex);
			}
		
			return reportList;
			
		}
        
     public List getVehicleReportSummary(int societyID,String buildingID)
		{
			try
			{
				logger.debug("Entry :   public List getVehicleReportSummary(int societyID,String buildingID)");
				
			    reportList=reportDAO.getVehicleReportSummary(societyID, buildingID);
			    logger.debug("Exit :    public List getVehicleReportSummary(int societyID,String buildingID)");
			}
			catch(NullPointerException e){
				logger.info("No Data available in getVehicleReportSummary");
			}
			catch(IllegalArgumentException ex){
				reportList=null;			
				logger.info("Exception in getVehicleReportSummary  : "+ex);
			}
			catch(Exception ex){
				//ex.printStackTrace();
				logger.error("Exception in getVehicleReportSummary : "+ex);
			}
		
			return reportList;
			
		}

     public RptVehicleVO getVehicleReportForAPI(int societyID)
 	{   RptVehicleVO vehicleVO=new RptVehicleVO();
 		List summaryList=new ArrayList();
 		List vehiList=new ArrayList();
 		int stickers=0;
 		int twoWheeler=0;
 		int fourWheeler=0;
 		int commercial=0;
 		try
 		{
 			logger.debug("Entry : public RptVehicleVO getVehicleReportForAPI(int societyID)");
 			
 			summaryList=getVehicleReportSummary(societyID, "0");
 			
 			if(summaryList.size()>1){
 				for(int i=0;summaryList.size()>i;i++){
 				 RptVehicleVO tempVo=(RptVehicleVO) summaryList.get(i);
 				 stickers=stickers+Integer.valueOf(tempVo.getStickerId());
 				 twoWheeler=twoWheeler+tempVo.getTwoWheeler();
 				 fourWheeler=fourWheeler+tempVo.getFourwheeler();
 				 commercial=commercial+tempVo.getCommercial();
 				
 				}
 				RptVehicleVO vhclVO=new RptVehicleVO();
 				vhclVO.setStickerId(Integer.toString(stickers));
 				vhclVO.setTwoWheeler(twoWheeler);
 				vhclVO.setFourwheeler(fourWheeler);
 				vhclVO.setCommercial(commercial);
 				summaryList.clear();
 				summaryList.add(vhclVO);
 				
 			}
 			
 			vehiList=getVehicleReport(societyID, "0");
 			
 			
 			vehicleVO.setSummaryVehicles(summaryList);
 			if(vehiList.size()>0){
 				vehicleVO.setStatusCode(1);
 			}
 			vehicleVO.setVehiclesList(vehiList);
 			vehicleVO.setSocietyID(societyID);
 			
 		    
 		    logger.debug("Exit : public RptVehicleVO getVehicleReportForAPI(int intSocietyID)");
 		}
 		catch(Exception e){
 			logger.info("Exception ");
 		}
 		return vehicleVO;
 		}
     
     public List getReceivableListForAllTags(int societyID,String uptoDate)
    	{   List receivablesList=null;
    		
    			logger.debug("Entry : public List getReceivableListForAllTags(int intSocietyID)");
    			
    			receivablesList=reportDAO.getReceivableListForAllTags(societyID,uptoDate);
    			if(receivablesList.size()>0){
    			for(int i=0;receivablesList.size()>i;i++){
    				AccountHeadVO accVO=(AccountHeadVO) receivablesList.get(i);
    				
    				if(accVO.getAdditionalProps()==null){
    					receivablesList.remove(i);
    					--i;
    					continue;
    				}
    				
    				if(accVO.getClosingBalance().intValue()<=0){
    					receivablesList.remove(i);
    					--i;
    					continue;
    				}
    				
    			}
    			}
    		    
    		    logger.debug("Exit : public List getReceivableListForAllTags(int intSocietyID)");
    		  	
    		return receivablesList;
    		
    	}
      
      public List getReceivableListForSingleTags(int societyID,String uptoDate,String ledgerMeta)
   	{   List receivablesList=null;
   		
   			logger.debug("Entry : public List getReceivableListForSingleTags(int intSocietyID)");
   			
   			receivablesList=reportDAO.getReceivableListForSingleTags(societyID,uptoDate, ledgerMeta);
   			
   			for(int i=0;receivablesList.size()>i;i++){
    				AccountHeadVO accVO=(AccountHeadVO) receivablesList.get(i);
    				
    				if(accVO.getAdditionalProps()==null){
    					receivablesList.remove(i);
    					--i;
    					continue;
    				}
    				
    				if(accVO.getClosingBalance().intValue()<=0){
    					receivablesList.remove(i);
    					--i;
    					continue;
    				}
   			}
   		    logger.debug("Exit : public List getReceivableListForSingleTags(int intSocietyID)");
   		  	
   		return receivablesList;
   		
   	}
     
     public List getMonthWiseGroupReportForChart(int societyID,String fromDate,String uptoDate,String type)
  	{   
  	   List groupNameList=new ArrayList();
  		try
  		{
  			logger.debug("Entry : public List getMonthWiseGroupReportForChart(int intSocietyID)");
  			
  			groupNameList=reportDAO.getMonthWiseGroupReportForChart(societyID,fromDate,uptoDate,type);
  		    
  		    logger.debug("Exit : public List getMonthWiseGroupReportForChart(int intSocietyID)");
  		}
  		catch(Exception ex)
  		{
  			//ex.printStackTrace();
  			logger.error("Exception in getMonthWiseGroupReportForChart : "+ex);
  		}
  	
  		return groupNameList;
  		
  	}
      
   
 
     
     public List getMonthWiseReceiptPaymentGroupReportForChart(int societyID,String fromDate,String uptoDate,String type, int isConsolidated)
  	{   
  	   List groupNameList=new ArrayList();
  		try
  		{
  			logger.debug("Entry : public List getMonthWiseReceiptPaymentGroupReportForChart(int intSocietyID,int isConsolidated)");
  			
  			groupNameList=reportDAO.getMonthWiseReceiptPaymentGroupReportForChart(societyID,fromDate,uptoDate,type,isConsolidated);
  		    
  		    logger.debug("Exit : public List getMonthWiseReceiptPaymentGroupReportForChart(int intSocietyID,int isConsolidated)");
  		}
  		catch(Exception ex)
  		{
  			//ex.printStackTrace();
  			logger.error("Exception in getMonthWiseReceiptPaymentGroupReportForChart : "+ex);
  		}
  	
  		return groupNameList;
  		
  	}
     
     public MonthwiseChartVO getIncomeExpenseReportForChart(int societyID,String fromDate,String uptoDate)
 	{   
 	   MonthwiseChartVO reportVO=new MonthwiseChartVO();
 	   ReportVO monthlyObj=new ReportVO();
 	  ReportVO quaterlyObj=new ReportVO();
 		try
 		{
 			logger.debug("Entry : public List getIncomeExpenseReportForChart(int intSocietyID)");
 			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
 			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
 			java.util.Date parse =  sdf.parse(fromDate);
 			Calendar c = Calendar.getInstance();
 			c.setTime(parse);
 			int month=c.get(Calendar.MONTH+1);
 			int year=c.get(Calendar.YEAR);
 			
 			//Fill year object
 			reportVO.setYearObj(getIncomeExpenseReport(societyID,year+"-04-01" ,sdf.format(dateutility.getMonthLastDate(3,(year+1))),0,"S"));
 			
 			//Fill monthwise breakup object
 			List monthlyList=getMonthWiseGroupReportForChart(societyID,year+"-04-01" ,sdf.format(dateutility.getMonthLastDate(3,(year+1))),"G");
 			monthlyObj.setIncomeTxList(monthlyList);
 			reportVO.setMonthlyObj(monthlyObj);
 			
 			//Fill quaterwise breakup object
 			List quaterlyList=reportDAO.getQuaterWiseGroupReportForChart(societyID,year+"-04-01" ,sdf.format(dateutility.getMonthLastDate(3,(year+1))),"G");
 			quaterlyObj.setIncomeTxList(quaterlyList);
 			reportVO.setQuaterlyObj(quaterlyObj);
 			
 			//Fill chart monthwise object
 			reportVO.setChartList(getMonthWiseGroupReportForChart(societyID,year+"-04-01" ,sdf.format(dateutility.getMonthLastDate(3,(year+1))),"R"));
 			
 			/*reportVO.setJanObj(getIncomeExpenseReport(societyID,(year+1)+"-01-01" ,sdf.format(dateutility.getMonthLastDate(1, (year+1)))));
 			
 			reportVO.setFebObj(getIncomeExpenseReport(societyID,(year+1)+"-02-01" ,sdf.format(dateutility.getMonthLastDate(2, (year+1)))));
 			
 			reportVO.setMarObj(getIncomeExpenseReport(societyID,(year+1)+"-03-01" ,sdf.format(dateutility.getMonthLastDate(3, (year+1)))));
 			
 			reportVO.setAprObj(getIncomeExpenseReport(societyID,year+"-04-01" ,sdf.format(dateutility.getMonthLastDate(4, year))));
 			
 			reportVO.setMayObj(getIncomeExpenseReport(societyID,year+"-05-01" ,sdf.format(dateutility.getMonthLastDate(5, year))));
 			
 			reportVO.setJunObj(getIncomeExpenseReport(societyID,year+"-06-01" ,sdf.format(dateutility.getMonthLastDate(6, year))));
 			
 			reportVO.setJulObj(getIncomeExpenseReport(societyID,year+"-07-01" ,sdf.format(dateutility.getMonthLastDate(7, year))));
 			
 			reportVO.setAugObj(getIncomeExpenseReport(societyID,year+"-08-01" ,sdf.format(dateutility.getMonthLastDate(8, year))));
 			
 			reportVO.setSeptObj(getIncomeExpenseReport(societyID,year+"-09-01" ,sdf.format(dateutility.getMonthLastDate(9, year))));
 			
 			reportVO.setOctObj(getIncomeExpenseReport(societyID,year+"-10-01" ,sdf.format(dateutility.getMonthLastDate(10, year))));
 			
 			reportVO.setNovObj(getIncomeExpenseReport(societyID,year+"-11-01" ,sdf.format(dateutility.getMonthLastDate(11, year))));
 			
 			reportVO.setDecObj(getIncomeExpenseReport(societyID,year+"-12-01" ,sdf.format(dateutility.getMonthLastDate(12, year))));
 			
 			reportVO.setQ1Obj(getIncomeExpenseReport(societyID,year+"-04-01" ,sdf.format(dateutility.getMonthLastDate(6, year))));
 			
 			reportVO.setQ2Obj(getIncomeExpenseReport(societyID,year+"-07-01" ,sdf.format(dateutility.getMonthLastDate(9,year))));
 			
 			reportVO.setQ3Obj(getIncomeExpenseReport(societyID,year+"-10-01" ,sdf.format(dateutility.getMonthLastDate(12, year))));
 			
 			reportVO.setQ4Obj(getIncomeExpenseReport(societyID,(year+1)+"-01-01" ,sdf.format(dateutility.getMonthLastDate(3, (year+1)))));
 			
 			reportVO.setH1Obj(getIncomeExpenseReport(societyID,year+"-04-01" ,sdf.format(dateutility.getMonthLastDate(9, year))));
 			
 			reportVO.setH2Obj(getIncomeExpenseReport(societyID,year+"-10-01" ,sdf.format(dateutility.getMonthLastDate(3,(year+1)))));*/
 			
 			
 		    
 		    logger.debug("Exit : public List getIncomeExpenseReportForChart(int intSocietyID)");
 		}
 		catch(Exception ex)
 		{
 			//ex.printStackTrace();
 			logger.error("Exception in getIncomeExpenseReportForChart : "+ex);
 		}
 	
 		return reportVO;
 		
 	}
     
     
     
     
     public MonthwiseChartVO getReceiptPaymentReportForChart(int societyID,String fromDate,String uptoDate,String rptType,int isConsolidated)
  	{   
  	   MonthwiseChartVO reportVO=new MonthwiseChartVO();
  	  ReportVO monthlyObj=new ReportVO();
	  ReportVO quaterlyObj=new ReportVO();
  		try
  		{
  			logger.debug("Entry : public List getReceiptPaymentReportForChart(int intSocietyID)");
  			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
  			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
  			java.util.Date parse =  sdf.parse(fromDate);
  			Calendar c = Calendar.getInstance();
  			c.setTime(parse);
  			int month=c.get(Calendar.MONTH+1);
  			int year=c.get(Calendar.YEAR);
  			
  			//Fill year object
  			reportVO.setYearObj(getCashBasedReceiptPaymentReport(societyID,year+"-04-01" ,sdf.format(dateutility.getMonthLastDate(3,(year+1))),rptType,isConsolidated));
  			
  			//Fill monthwise breakup object
  			List monthwiseRcptPymtList=this.getMonthWiseReceiptPaymentGroupReportForChart(societyID,year+"-04-01" ,sdf.format(dateutility.getMonthLastDate(3,(year+1))),"G",isConsolidated);
  			ReportVO mntRcptVo=processMonthlyCashBasedReceiptPaymentObject(monthwiseRcptPymtList);
  			List incomeMonthwiseList=mntRcptVo.getIncomeTxList();
		    List expenseMonthwiseList=mntRcptVo.getExpenseTxList();
		    monthlyObj.setIncomeTxList(incomeMonthwiseList);
		    monthlyObj.setExpenseTxList(expenseMonthwiseList);
		    reportVO.setMonthlyObj(monthlyObj);
 			
		   //Fill quaterwise breakup object
		    List qtrWiseList=reportDAO.getQuaterWiseReceiptPaymentGroupReportForChart(societyID,year+"-04-01" ,sdf.format(dateutility.getMonthLastDate(3,(year+1))),"G");
  			ReportVO qtrWiseVO=processMonthlyCashBasedReceiptPaymentObject(qtrWiseList);
		    List incomeQuaterwiseList=qtrWiseVO.getIncomeTxList();
		    List expenseQuaterwiseList=qtrWiseVO.getExpenseTxList();
		    quaterlyObj.setIncomeTxList(incomeQuaterwiseList);
		    quaterlyObj.setExpenseTxList(expenseQuaterwiseList);
		    reportVO.setQuaterlyObj(quaterlyObj);
		    		   
 			// Fill chart object
		    List chartList=new ArrayList<>();
		    MonthwiseChartVO incomeVO=convertMonthlyChartObject(incomeMonthwiseList);
		    if(incomeVO.getGroupName()!=null){
		    chartList.add(convertMonthlyChartObject(incomeMonthwiseList));
		    }
		    MonthwiseChartVO expenseVO=convertMonthlyChartObject(expenseMonthwiseList);
		    if(expenseVO.getGroupName()!=null){
		    chartList.add(convertMonthlyChartObject(expenseMonthwiseList));
		    }
		    if(chartList.size()>0)
		    reportVO.setChartList(chartList);
 			
  			/*reportVO.setJanObj(getCashBasedReceiptPaymentReport(societyID,(year+1)+"-01-01" ,sdf.format(dateutility.getMonthLastDate(1, (year+1)))));
  			
  			reportVO.setFebObj(getCashBasedReceiptPaymentReport(societyID,(year+1)+"-02-01" ,sdf.format(dateutility.getMonthLastDate(2, (year+1)))));
  			
  			reportVO.setMarObj(getCashBasedReceiptPaymentReport(societyID,(year+1)+"-03-01" ,sdf.format(dateutility.getMonthLastDate(3, (year+1)))));
  			
  			reportVO.setAprObj(getCashBasedReceiptPaymentReport(societyID,year+"-04-01" ,sdf.format(dateutility.getMonthLastDate(4, year))));
  			
  			reportVO.setMayObj(getCashBasedReceiptPaymentReport(societyID,year+"-05-01" ,sdf.format(dateutility.getMonthLastDate(5, year))));
  			
  			reportVO.setJunObj(getCashBasedReceiptPaymentReport(societyID,year+"-06-01" ,sdf.format(dateutility.getMonthLastDate(6, year))));
  			
  			reportVO.setJulObj(getCashBasedReceiptPaymentReport(societyID,year+"-07-01" ,sdf.format(dateutility.getMonthLastDate(7, year))));
  			
  			reportVO.setAugObj(getCashBasedReceiptPaymentReport(societyID,year+"-08-01" ,sdf.format(dateutility.getMonthLastDate(8, year))));
  			
  			reportVO.setSeptObj(getCashBasedReceiptPaymentReport(societyID,year+"-09-01" ,sdf.format(dateutility.getMonthLastDate(9, year))));
  			
  			reportVO.setOctObj(getCashBasedReceiptPaymentReport(societyID,year+"-10-01" ,sdf.format(dateutility.getMonthLastDate(10, year))));
  			
  			reportVO.setNovObj(getCashBasedReceiptPaymentReport(societyID,year+"-11-01" ,sdf.format(dateutility.getMonthLastDate(11, year))));
  			
  			reportVO.setDecObj(getCashBasedReceiptPaymentReport(societyID,year+"-12-01" ,sdf.format(dateutility.getMonthLastDate(12, year))));
  			
  			reportVO.setQ1Obj(getCashBasedReceiptPaymentReport(societyID,year+"-04-01" ,sdf.format(dateutility.getMonthLastDate(6, year))));
  			
  			reportVO.setQ2Obj(getCashBasedReceiptPaymentReport(societyID,year+"-07-01" ,sdf.format(dateutility.getMonthLastDate(9,year))));
  			
  			reportVO.setQ3Obj(getCashBasedReceiptPaymentReport(societyID,year+"-10-01" ,sdf.format(dateutility.getMonthLastDate(12, year))));
  			
  			reportVO.setQ4Obj(getCashBasedReceiptPaymentReport(societyID,(year+1)+"-01-01" ,sdf.format(dateutility.getMonthLastDate(3, (year+1)))));
  			
  			reportVO.setH1Obj(getCashBasedReceiptPaymentReport(societyID,year+"-04-01" ,sdf.format(dateutility.getMonthLastDate(9, year))));
  			
  			reportVO.setH2Obj(getCashBasedReceiptPaymentReport(societyID,year+"-10-01" ,sdf.format(dateutility.getMonthLastDate(3,(year+1)))));*/
  			
  			
  		    
  		    logger.debug("Exit : public List getReceiptPaymentReportForChart(int intSocietyID)");
  		}
  		catch(Exception ex)
  		{
  			//ex.printStackTrace();
  			logger.error("Exception in getReceiptPaymentReportForChart : "+ex);
  		}
  	
  		return reportVO;
  		
  	}
     
     public MonthwiseChartVO getMonthWiseBreakUpReportForReceivables(int societyID,String fromDate,String uptoDate)
  	{   
  	   MonthwiseChartVO reportVO=new MonthwiseChartVO();
  	   ReportVO monthlyObj=new ReportVO();
  	  ReportVO quaterlyObj=new ReportVO();
  		try
  		{
  			logger.debug("Entry : public List getMonthWiseBreakUpReportForReceivables(int intSocietyID)");
  			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
  			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
  			java.util.Date parse =  sdf.parse(fromDate);
  			Calendar c = Calendar.getInstance();
  			c.setTime(parse);
  			int month=c.get(Calendar.MONTH+1);
  			int year=c.get(Calendar.YEAR);
  			
  			
  			//Fill monthwise breakup object
  			List monthlyList=processReceivablesList(reportDAO.getMonthWiseBreakUpReportForReceivables(societyID,year+"-04-01" ,sdf.format(dateutility.getMonthLastDate(3,(year+1))),"I"));
  			
  			monthlyObj.setIncomeTxList(monthlyList);
  			reportVO.setMonthlyObj(monthlyObj);
  			
  			reportVO.setChartList(processReceivablesList(reportDAO.getMonthWiseBreakUpReportForReceivables(societyID,year+"-04-01" ,sdf.format(dateutility.getMonthLastDate(3,(year+1))),"G")));
  		    
  		    logger.debug("Exit : public List getMonthWiseBreakUpReportForReceivables(int intSocietyID)");
  		}
  		catch(Exception ex)
  		{
  			//ex.printStackTrace();
  			logger.error("Exception in getMonthWiseBreakUpReportForReceivables : "+ex);
  		}
  	
  		return reportVO;
  		
  	}
     
     private List processReceivablesList(List monthWiseReceivablesList){
    	 List updatedList=new ArrayList();
    	 BigDecimal baseOpBal=BigDecimal.ZERO;
    	 for(int i=0;monthWiseReceivablesList.size()>i;i++){
    		 MonthwiseChartVO monthwiseChartVO=(MonthwiseChartVO) monthWiseReceivablesList.get(i);
    		 MonthwiseChartVO newObjectVO=new MonthwiseChartVO();
    		 newObjectVO.setSocietyID(monthwiseChartVO.getSocietyID());
    		 newObjectVO.setGroupID(monthwiseChartVO.getGroupID());
    		 newObjectVO.setLedgerID(monthwiseChartVO.getLedgerID());
    		 newObjectVO.setRootGroupID(monthwiseChartVO.getRootGroupID());
    		 newObjectVO.setGroupName(monthwiseChartVO.getGroupName());
    		 newObjectVO.setLedgerName(monthwiseChartVO.getLedgerName());
    		 newObjectVO.setRootGroupName(monthwiseChartVO.getRootGroupName());
    		 newObjectVO.setOpeningBalance(monthwiseChartVO.getOpeningBalance());
    		 
    		 if(monthwiseChartVO.getOpeningBalance()!=null){
    		  monthwiseChartVO.setAprAmt(monthwiseChartVO.getAprAmt().add(monthwiseChartVO.getOpeningBalance()));
    		 newObjectVO.setAprAmt(monthwiseChartVO.getAprAmt());
    		 }
    		 if(monthwiseChartVO.getAprAmt().compareTo(BigDecimal.ZERO)<0){
    			 newObjectVO.setAprAmt(BigDecimal.ZERO);
    		 }
    		  monthwiseChartVO.setMayAmt(monthwiseChartVO.getMayAmt().add(monthwiseChartVO.getAprAmt()));
    		  newObjectVO.setMayAmt(monthwiseChartVO.getMayAmt());
    		  if(monthwiseChartVO.getMayAmt().compareTo(BigDecimal.ZERO)<0){
     			 newObjectVO.setMayAmt(BigDecimal.ZERO);
     		 }
    		  monthwiseChartVO.setJunAmt(monthwiseChartVO.getJunAmt().add(monthwiseChartVO.getMayAmt()));
    		  newObjectVO.setJunAmt(monthwiseChartVO.getJunAmt());
    		  if(monthwiseChartVO.getJunAmt().compareTo(BigDecimal.ZERO)<0){
      			 newObjectVO.setJunAmt(BigDecimal.ZERO);
      		 }
    		  monthwiseChartVO.setJulAmt(monthwiseChartVO.getJulAmt().add(monthwiseChartVO.getJunAmt()));
    		  newObjectVO.setJulAmt(monthwiseChartVO.getJulAmt());
    		  if(monthwiseChartVO.getJulAmt().compareTo(BigDecimal.ZERO)<0){
      			 newObjectVO.setJulAmt(BigDecimal.ZERO);
      		 }
    		  monthwiseChartVO.setAugAmt(monthwiseChartVO.getAugAmt().add(monthwiseChartVO.getJulAmt()));
    		  newObjectVO.setAugAmt(monthwiseChartVO.getAugAmt());
    		  if(monthwiseChartVO.getAugAmt().compareTo(BigDecimal.ZERO)<0){
      			 newObjectVO.setAugAmt(BigDecimal.ZERO);
      		 }
    		  monthwiseChartVO.setSeptAmt(monthwiseChartVO.getSeptAmt().add(monthwiseChartVO.getAugAmt()));
    		  newObjectVO.setSeptAmt(monthwiseChartVO.getAugAmt());
    		  if(monthwiseChartVO.getSeptAmt().compareTo(BigDecimal.ZERO)<0){
      			 newObjectVO.setSeptAmt(BigDecimal.ZERO);
      		 }
    		  monthwiseChartVO.setOctAmt(monthwiseChartVO.getOctAmt().add(monthwiseChartVO.getSeptAmt()));
    		  newObjectVO.setOctAmt(monthwiseChartVO.getOctAmt());
    		  if(monthwiseChartVO.getOctAmt().compareTo(BigDecimal.ZERO)<0){
      			 newObjectVO.setOctAmt(BigDecimal.ZERO);
      		 }
    		  monthwiseChartVO.setNovAmt(monthwiseChartVO.getNovAmt().add(monthwiseChartVO.getOctAmt()));
    		  newObjectVO.setNovAmt(monthwiseChartVO.getNovAmt());
    		  if(monthwiseChartVO.getNovAmt().compareTo(BigDecimal.ZERO)<0){
      			 newObjectVO.setNovAmt(BigDecimal.ZERO);
      		 }
    		  monthwiseChartVO.setDecAmt(monthwiseChartVO.getDecAmt().add(monthwiseChartVO.getNovAmt()));
    		  newObjectVO.setDecAmt(monthwiseChartVO.getDecAmt());
    		  if(monthwiseChartVO.getDecAmt().compareTo(BigDecimal.ZERO)<0){
    			  newObjectVO.setDecAmt(BigDecimal.ZERO);
      		 }
    		  monthwiseChartVO.setJanAmt(monthwiseChartVO.getJanAmt().add(monthwiseChartVO.getDecAmt()));
    		  newObjectVO.setJanAmt(monthwiseChartVO.getJanAmt());
    		  if(monthwiseChartVO.getJanAmt().compareTo(BigDecimal.ZERO)<0){
    			  newObjectVO.setJanAmt(BigDecimal.ZERO);
      		 }
    		  monthwiseChartVO.setFebAmt(monthwiseChartVO.getFebAmt().add(monthwiseChartVO.getJanAmt()));
    		  newObjectVO.setFebAmt(monthwiseChartVO.getFebAmt());
    		  if(monthwiseChartVO.getFebAmt().compareTo(BigDecimal.ZERO)<0){
    			  newObjectVO.setFebAmt(BigDecimal.ZERO);
      		 }
    		  monthwiseChartVO.setMarAmt(monthwiseChartVO.getMarAmt().add(monthwiseChartVO.getFebAmt()));
    		  newObjectVO.setMarAmt(monthwiseChartVO.getMarAmt());
    		  if(monthwiseChartVO.getMarAmt().compareTo(BigDecimal.ZERO)<0){
    			  newObjectVO.setMarAmt(BigDecimal.ZERO);
      		 }
    		  
    		  updatedList.add(newObjectVO);
    	 }
    	 
    	 return updatedList;
     }
     
     private MonthwiseChartVO convertMonthlyChartObject(List chartList){
    	 logger.debug("Entry : private MonthwiseChartVO convertMonthlyChartObject(List chartList)");
    	 MonthwiseChartVO outputObject=new MonthwiseChartVO();
    	 String txType="";
    	 String rootGroupName="";
    	 String groupName="";
    	 BigDecimal aprBal=BigDecimal.ZERO;
    	 BigDecimal mayBal=BigDecimal.ZERO;
    	 BigDecimal junBal=BigDecimal.ZERO;
    	 BigDecimal julBal=BigDecimal.ZERO;
    	 BigDecimal augBal=BigDecimal.ZERO;
    	 BigDecimal sepBal=BigDecimal.ZERO;
    	 BigDecimal octBal=BigDecimal.ZERO;
    	 BigDecimal novBal=BigDecimal.ZERO;
    	 BigDecimal decBal=BigDecimal.ZERO;
    	 BigDecimal janBal=BigDecimal.ZERO;
    	 BigDecimal febBal=BigDecimal.ZERO;
    	 BigDecimal marBal=BigDecimal.ZERO;
    	 
    	 if(chartList.size()>0){
    		 
    		 for(int i=0;chartList.size()>i;i++){
    			 MonthwiseChartVO monthVO=(MonthwiseChartVO) chartList.get(i);
    			 outputObject.setTxType(monthVO.getTxType());
    			 outputObject.setRootGroupName(monthVO.getRootGroupName());
    			 outputObject.setGroupName(monthVO.getGroupName());
    			 aprBal=aprBal.add(monthVO.getAprAmt()).setScale(2,RoundingMode.HALF_UP);
    			 mayBal=mayBal.add(monthVO.getMayAmt()).setScale(2,RoundingMode.HALF_UP);
    			 junBal=junBal.add(monthVO.getJunAmt()).setScale(2,RoundingMode.HALF_UP);
    			 julBal=julBal.add(monthVO.getJulAmt()).setScale(2,RoundingMode.HALF_UP);
    			 augBal=augBal.add(monthVO.getAugAmt()).setScale(2,RoundingMode.HALF_UP);
    			 sepBal=sepBal.add(monthVO.getSeptAmt()).setScale(2,RoundingMode.HALF_UP);
    			 octBal=octBal.add(monthVO.getOctAmt()).setScale(2,RoundingMode.HALF_UP);
    			 novBal=novBal.add(monthVO.getNovAmt()).setScale(2,RoundingMode.HALF_UP);
    			 decBal=decBal.add(monthVO.getDecAmt()).setScale(2,RoundingMode.HALF_UP);
    			 janBal=janBal.add(monthVO.getJanAmt()).setScale(2,RoundingMode.HALF_UP);
    			 febBal=febBal.add(monthVO.getFebAmt()).setScale(2,RoundingMode.HALF_UP);
    			 marBal=marBal.add(monthVO.getMarAmt()).setScale(2,RoundingMode.HALF_UP);
    			 outputObject.setAprAmt(aprBal);
    			 outputObject.setMayAmt(mayBal);
    			 outputObject.setJunAmt(junBal);
    			 outputObject.setJulAmt(julBal);
    			 outputObject.setAugAmt(augBal);
    			 outputObject.setSeptAmt(sepBal);
    			 outputObject.setOctAmt(octBal);
    			 outputObject.setNovAmt(novBal);
    			 outputObject.setDecAmt(decBal);
    			 outputObject.setJanAmt(janBal);
    			 outputObject.setFebAmt(febBal);
    			 outputObject.setMarAmt(marBal);
    			 
    		 }
    		 
    		 
    	 }
    	 
    	 
    	 
    	 logger.debug("Exit : private MonthwiseChartVO convertMonthlyChartObject(List chartList)");
    	 return outputObject;
     }
     
     
     public ReportVO convertReportObjectForPrinting(ReportVO reportVO){
    		
    		logger.debug("Entry : public ReportDetailsVO convertReportObjectForPrinting(ReportDetailsVO reportVO)");
    			

    			try {
    				List newIncomeList=new ArrayList();
    				List newExpenseList=new ArrayList();
    				List newAssetsList=new ArrayList();
    				List newLiabilityList=new ArrayList();
    				
    				if((reportVO.getIncomeTxList()!=null) && (reportVO.getIncomeTxList().size()>0)){
    					newIncomeList=convertReportListForPrinting(reportVO.getIncomeTxList());
    				}
    				
    				
    				if((reportVO.getExpenseTxList()!=null) && (reportVO.getExpenseTxList().size()>0)){
    					newExpenseList=convertReportListForPrinting(reportVO.getExpenseTxList());
    				}
    				
    				
    				if((reportVO.getAssetsList()!=null) && (reportVO.getAssetsList().size()>0)){
    					newAssetsList=convertReportListForPrinting(reportVO.getAssetsList());   					
    				}
    				
    				if((reportVO.getLiabilityList()!=null) && (reportVO.getLiabilityList().size()>0)){
    					newLiabilityList=convertReportListForPrinting(reportVO.getLiabilityList());
    				}
    				
    				
    				
    				reportVO.setIncomeTxList(newIncomeList);
    				reportVO.setExpenseTxList(newExpenseList);
    				reportVO.setAssetsList(newAssetsList);
    				reportVO.setLiabilityList(newLiabilityList);
    				
    			} catch (Exception e) {
    				logger.error("Exception occured public ReportDetailsVO convertReportObjectForPrinting(ReportDetailsVO reportVO) "+e);
    			}
    				
    			
    			logger.debug("Exit : public ReportDetailsVO convertReportObjectForPrinting(ReportDetailsVO reportVO)");

    			return reportVO;
    		}
        
        
        public List convertReportListForPrinting(List reportList){
    		
    		logger.debug("Entry : public List convertReportListForPrinting(List reportList)"+reportList.size());
    		List newReportList=new ArrayList();	
    		
    			try {
    				
    				if(reportList!=null && reportList.size()>0){
    					
    					for(int i=0;reportList.size()>i;i++){
    						ReportDetailsVO rptLblVO=(ReportDetailsVO) reportList.get(i);
    						
    						newReportList.add(rptLblVO);
    						
    						if((rptLblVO.getObjectList()!=null) && (rptLblVO.getObjectList().size()>0)){
    							List subLablList=rptLblVO.getObjectList();
    							for(int j=0;subLablList.size()>j;j++){
    								
    								ReportDetailsVO subLblVO=(ReportDetailsVO) subLablList.get(j);
    								
    								newReportList.add(subLblVO);
    								
    								/*if(subLblVO.getObjectList().size()>0){
    									List categoryList=subLblVO.getObjectList();
    									
    									for(int k=0;categoryList.size()>k;k++){
    										
    										ReportDetailsVO categoryVO=(ReportDetailsVO) categoryList.get(k);
    										
    										newReportList.add(categoryVO);
    										
    									}
    								}
    								*/
    								
    							}
    							
    							
    						}
    						
    						
    					}
    					
    					
    				}
    				

    				
    			} catch (Exception e) {
    				logger.error("Exception occured public List convertReportListForPrinting(List reportList) "+e);
    			}
    				
    			
    			logger.debug("Exit : public ReportDetailsVO convertReportObjectForPrinting(ReportDetailsVO reportVO)");

    			return newReportList;
    		}
     
     
     
	public ReportDAO getReportDAO() {
		return reportDAO;
	}

	public void setReportDAO(ReportDAO reportDAO) {
		this.reportDAO = reportDAO;
	}
	

	public TransactionsDomain getTransactionsDomain() {
		return transactionsDomain;
	}

	public void setTransactionsDomain(TransactionsDomain transactionsDomain) {
		this.transactionsDomain = transactionsDomain;
	}

	public SocietyService getSocietyService() {
		return societyService;
	}

	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}

	public LedgerService getLedgerService() {
		return ledgerService;
	}

	public void setLedgerService(LedgerService ledgerService) {
		this.ledgerService = ledgerService;
	}

}
