package com.emanager.server.financialReports.domainObject;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Calendar;

import org.apache.log4j.Logger;

import com.emanager.server.commonUtils.domainObject.CalculateLateFeeForTransactions;
import com.emanager.server.financialReports.valueObject.RptTransactionVO;
import com.emanager.server.invoice.Domain.InvoiceDomain;
import com.emanager.server.invoice.dataAccessObject.InvoiceDetailsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceLineItemsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceMemberChargesVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;

public class CalculateLateFee {
	Logger logger = Logger.getLogger(CalculateLateFee.class);
	BigDecimal hundred = new BigDecimal("100");
	CalculateLateFeeForTransactions custom=new CalculateLateFeeForTransactions();
	InvoiceDomain invoiceDomain;
	SocietyService societyService;
	
 
 
 	public BigDecimal getLateFeeForInvoice(InvoiceMemberChargesVO invLineVO,int societyID,InvoiceDetailsVO invoiceVO){
 		BigDecimal lateFees=BigDecimal.ZERO;
 		logger.debug("Entry : Public BigDecimal getLateFeeForInvoice(InvoiceLineItemsVO invLineVO)"+invoiceVO.getEntityID());
 		try {
			
			SocietyVO societyVO=societyService.getSocietyDetails("0", societyID);
			invoiceVO=invoiceDomain.getInvoiceBalanceForLateFee(invoiceVO, societyID, invLineVO.getInvoiceTypeID(), invLineVO.getLateFeeFor());
			BigDecimal balance=invoiceVO.getBalance();
			BigDecimal currentBalance=invoiceVO.getCurrentBalance();
			if(balance.compareTo(societyVO.getThresholdBalance()) < 0 ){
				logger.info("The late fee should be waived off since balance is "+balance+"  Threshold Amount should be  "+societyVO.getThresholdBalance());
			}
			else{
			
			if(invLineVO.getFrequency().equalsIgnoreCase("OneTime")){
				lateFees=calculateLateFeeFixedAmtWise(balance, invLineVO);
			}
			else if(balance.signum()>0){
			
			if(invLineVO.getLateFeeType().equalsIgnoreCase("P")){
				lateFees=calculateLateFeePercentWise(currentBalance, invLineVO);
				
			}else if(invLineVO.getLateFeeType().equalsIgnoreCase("F")){
				lateFees=calculateLateFeeFixedAmtWise(currentBalance, invLineVO);
				
			}else if(invLineVO.getLateFeeType().equalsIgnoreCase("C")){
				lateFees=calculateLateFeeCummulative(balance, invLineVO);
			}
			
			
			}
			}
		} catch (Exception e) {
			logger.error("Exception occured in getLateFeeForInvoice "+e);
		}
 		
 		
 		
 		logger.debug("Exit : Public BigDecimal getLateFeeForInvoice(InvoiceLineItemsVO invoiceVO)"+lateFees);
 		return lateFees;
 	}
 	
 	
 	public BigDecimal getLateFeeForInvoiceSingle(InvoiceMemberChargesVO invLineVO,int societyID,InvoiceDetailsVO invoiceVO){
 		BigDecimal lateFees=BigDecimal.ZERO;
 		logger.debug("Entry : Public BigDecimal getLateFeeForInvoice(InvoiceLineItemsVO invLineVO)"+invoiceVO.getEntityID());
 		try {
			invoiceVO.setInvoiceTypeID(invoiceVO.getEntityID());
			invoiceVO=invoiceDomain.getInvoiceBalance(invoiceVO, societyID, invoiceVO.getEntityID());
			BigDecimal balance=invoiceVO.getBalance();
			BigDecimal currentBalance=invoiceVO.getCurrentBalance();
			
			
			if(invLineVO.getFrequency().equalsIgnoreCase("OneTime")){
				lateFees=calculateLateFeeFixedAmtWise(balance, invLineVO);
			}
			else if(balance.signum()>0){
			
			if(invLineVO.getLateFeeType().equalsIgnoreCase("P")){
				lateFees=calculateLateFeePercentWise(currentBalance, invLineVO);
				
			}else if(invLineVO.getLateFeeType().equalsIgnoreCase("F")){
				lateFees=calculateLateFeeFixedAmtWise(currentBalance, invLineVO);
				
			}else if(invLineVO.getLateFeeType().equalsIgnoreCase("C")){
				lateFees=calculateLateFeeCummulative(balance, invLineVO);
			}
			
			
			}
			
		} catch (Exception e) {
			logger.error("Exception occured in getLateFeeForInvoice "+e);
		}
 		
 		
 		
 		logger.debug("Exit : Public BigDecimal getLateFeeForInvoice(InvoiceLineItemsVO invoiceVO)"+lateFees);
 		return lateFees;
 	}
 	
 	private BigDecimal calculateLateFeePercentWise(BigDecimal balance,InvoiceMemberChargesVO invLineVO){
 		logger.debug("Entry : private BigDecimal calculateLateFeePercentWise(BigDecimal balance,InvoiceLineItemsVO invLineVO)");
 		BigDecimal lateFees=BigDecimal.ZERO;
 		MathContext mc = new MathContext(4);
 		BigDecimal latefeesInHundred=invLineVO.getTotalAmount().divide(hundred);
 		
 		lateFees =balance.multiply(latefeesInHundred).setScale(0,RoundingMode.HALF_UP).setScale(2);
 		
 		logger.debug("Exit : private BigDecimal calculateLateFeePercentWise(BigDecimal balance,InvoiceLineItemsVO invLineVO)");
 		return lateFees;
 	}

 	private BigDecimal calculateLateFeeFixedAmtWise(BigDecimal balance,InvoiceMemberChargesVO invLineVO){
 		logger.debug("Entry : private BigDecimal calculateLateFeeFixedAmtWise(BigDecimal balance,InvoiceLineItemsVO invLineVO)");
 		BigDecimal lateFees=BigDecimal.ZERO;
 		 		
 		lateFees = invLineVO.getTotalAmount();
 		
 		logger.debug("Exit : private BigDecimal calculateLateFeeFixedAmtWise(BigDecimal balance,InvoiceLineItemsVO invLineVO)");
 		return lateFees;
 	}
 	
 	
 	private BigDecimal calculateLateFeeCummulative(BigDecimal balance,InvoiceMemberChargesVO invLineVO){
 		logger.debug("Entry : private BigDecimal calculateLateFeePercentWise(BigDecimal balance,InvoiceLineItemsVO invLineVO)");
 		BigDecimal lateFees=BigDecimal.ZERO;
 		MathContext mc = new MathContext(4);
 		BigDecimal latefeesInHundred=invLineVO.getTotalAmount().divide(hundred);
 		
 		lateFees = balance.multiply(latefeesInHundred).setScale(0,RoundingMode.HALF_UP).setScale(2);
 		
 		logger.debug("Exit : private BigDecimal calculateLateFeePercentWise(BigDecimal balance,InvoiceLineItemsVO invLineVO)");
 		return lateFees;
 	}
 	
	public InvoiceDomain getInvoiceDomain() {
		return invoiceDomain;
	}

	public void setInvoiceDomain(InvoiceDomain invoiceDomain) {
		this.invoiceDomain = invoiceDomain;
	}

	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}
 
 	
 
 
}
