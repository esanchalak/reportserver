package com.emanager.server.financialReports.domainObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.financialReports.dataaccessObject.MemberFeeDAO;
import com.emanager.server.financialReports.dataaccessObject.ReportDAO;
import com.emanager.server.financialReports.valueObject.RptMonthYearVO;
import com.emanager.server.financialReports.valueObject.RptTransactionVO;




public class MMCContibutionDomain {
    ReportDAO reportDAO;
    MemberFeeDAO memberFeeDAO;
    DateUtility dateutility = new DateUtility();

    BigDecimal latefeesInpercent = new BigDecimal("0.00");

    BigDecimal rentFeesInpercent = new BigDecimal("0.00");

    BigDecimal hundred = new BigDecimal("100");

    BigDecimal due = new BigDecimal("0.00");

    BigDecimal total = new BigDecimal(0.00);
    String Comments=null;
    String financialYear=null;

    Logger logger = Logger.getLogger(MMCContibutionDomain.class);
    String firstDate="";
    String lastDate="";
    Date firstDateSQL=null;
    Date lastDateSQL=null;
    Date thirtyFirst=null;
    RptTransactionVO rptTransVO=new RptTransactionVO();

   /* public List getyearlyMaintainanceContibution(int year, int societyId,int buildingId,int month) {
        List MMCContriList = new ArrayList();
        try {
            logger.debug("Entry:public List getyearlyMaintainanceContibution(int year, int societyId,int buildingId)"
                            + year + societyId);
            String expMMCCmnts=null;
             firstDate = year + "-" + 04 + "-" + 01;
            String thirtyFirstMarch=year+"-"+03+"-"+31;
            // lastDate = (year + 1) + "-" + 03 + "-" + 31;
             
             if(month!=0){
     			
     				if(month==1){
     					firstDate=(year+1)+"-"+month+"-"+01;
     					lastDate=(year+1)+"-"+(month+2)+"-"+31;
     					
     				}else{
     				//firstDate=year+"-"+month+"-"+01;
     				lastDate=year+"-"+(month+2)+"-"+31;
     				
     				}
     				}else {
     				//firstDate = year + "-" + 04 + "-" + 01;
     			    lastDate = (year + 1) + "-" + 03 + "-" + 31;
     			   
     			}
            
             firstDateSQL = java.sql.Date.valueOf(firstDate);
             lastDateSQL = java.sql.Date.valueOf(lastDate);
             thirtyFirst=java.sql.Date.valueOf(thirtyFirstMarch);
             
                        
            MMCContriList = reportDAO.getyearlyMaintainanceContibution(year,societyId,buildingId,firstDate,lastDate);
                        
            List membersNotTransactedInperodList = getyearlyMaintainanceContriNotPresentInFY(
                    year, societyId, MMCContriList,buildingId,firstDate,lastDate);            
            List nonTrasactedMembersList = getyearlyNonContibutionMembers(year,societyId, MMCContriList,buildingId,firstDate,lastDate);
    
            for (int i = 0; MMCContriList.size() > i; i++) {

                RptMonthYearVO rptVO = (RptMonthYearVO) MMCContriList.get(i);
                if (rptVO.getIsChecked() != 1) {

                    Calendar cal = Calendar.getInstance();
                    int CmYyear = cal.get(Calendar.YEAR);
                    int months =cal.get(Calendar.MONTH);
                    if(months<3){
                    CmYyear = CmYyear - 1;
                    }
                    //logger.info(year+" ***|||||||** "+CmYyear);
                    rptTransVO=calculateFees(rptVO,""+societyId, firstDate,lastDate);
                   
                    BigDecimal totalExp=calculateTotalinBigDecimal(rptTransVO);
                    BigDecimal otherChargesAmount=reportDAO.getTxOtherChanrges(year, societyId, rptVO.getMemberID(),firstDate,lastDate);    
                    BigDecimal rentFees=calculateRentFees(rptVO.getAptID(),rptVO.getMonthlyMmc(), firstDateSQL,lastDateSQL);
                    
                     
                    
                    expMMCCmnts= "TotalMMC="+rptTransVO.getDueForMMC()+"\n Total Tenant Fee="+rptTransVO.getRentalFees()+"\n Total Sinking Fund="+rptTransVO.getDueForSF()+"\n Total RM="+rptTransVO.getDueForRM()+"\n Other Dues="+rptTransVO.getDueForOther()+"\n Total Exp="+totalExp;
                    
                    rptVO.setExpectedMMC(totalExp);
                  
                    rptVO.setOtherCharges(otherChargesAmount);
                                        
                  //  rptVO.setExpAmntComments(expMMCCmnts);
                    
                    //  total paid amount
                    BigDecimal totalPaidAmount=rptVO.getTxAmount().add(rptVO.getOtherCharges()).setScale(2);     
                    rptVO.setTotalPaidCharges(totalPaidAmount);
                    
                
                    if(rptVO.getTxAmount().intValue()==0){
                        rptVO.setDiscount(new BigDecimal("0.00"));
                    }
                    if(rptVO.getOutstandBal().intValue()==0){
                        rptVO.setComments("Arrears Carried Forward");
                    }
                    
                                    
                    BigDecimal tempArrears=rptVO.getOutstandBal().add(rptVO.getRefund()).setScale(2);
                    BigDecimal tempActualTotal = rptVO.getTotalPaidCharges().subtract(
                            tempArrears).setScale(2);
                    BigDecimal diference = totalExp.subtract(
                            (tempActualTotal)).setScale(2);
                    BigDecimal balance=diference.subtract(rptVO.getDiscount()).setScale(2);
                    rptVO.setDiffernce(balance);
                    //logger.info("Total Exp "+totalExp+" + "+rptVO.getOutstandBal()+" + "+rptVO.getRefund()+" - "+tempActualTotal+"="+balance);
                    
                    
                                    
                    if(balance.intValue()>0){
                        rptVO.setDebit(rptVO.getDiffernce());
                        rptVO.setCredit(new BigDecimal("0.00"));
                    
                        
                    }else if(balance.intValue()<=0){
                        int bal=Math.abs(balance.intValue());
                        
                        rptVO.setCredit(new BigDecimal(bal).setScale(2));
                        
                        rptVO.setDebit(new BigDecimal("0.00"));
                        
                    }

                }
            }

        } catch (Exception ex) {
            logger.error("Exception in getyearlyMaintainanceContibution : "
                    + ex);
        }
        logger.debug("Exit:public List getyearlyMaintainanceContibution(int year, int societyId,int buildingId)"
                        + MMCContriList.size());
        return MMCContriList;
    }*/
    /*
    public BigDecimal calculateRentFees(int aptId,BigDecimal mmc,Date firstDate,Date lastDate){
        logger.debug("Entry :private BigDecimal calculateRentFees(int aptID,Date memberDueDate,Date lastDateOfMonth )");
        
        int totalDiffernceInMonth=0;
        
        BigDecimal rentfee=new BigDecimal("0.00");
             
        if(aptId !=0){
            List renterList = reportDAO.getRenterYealryHistory(aptId, firstDate, lastDate);
            
            
            for(int i=0;renterList.size()>i;i++){
            	int differneceInMonth=0;
                RptMonthYearVO rptTenantVO  = new RptMonthYearVO();
                rptTenantVO = (RptMonthYearVO) renterList.get(i);    
            
                if(rptTenantVO.getFullName()!=null){
                    
                Calendar lastDateCal = Calendar.getInstance();
                Calendar firstDateCal = Calendar.getInstance();
                Calendar startAggDate=Calendar.getInstance();
                Calendar endAggDate=Calendar.getInstance();
                
                lastDateCal.setTime(lastDate);
                firstDateCal.setTime(firstDate);
                
                endAggDate.setTime(rptTenantVO.getEndDate());    
                startAggDate.setTime(rptTenantVO.getStartDate());
                
                //Date Extend by month
                Calendar cal1 = Calendar.getInstance();  
                cal1.setTime(rptTenantVO.getEndDate());  
                cal1.add(Calendar.MONTH,1);
               
                Calendar cal2 = Calendar.getInstance();  
                cal2.setTime(rptTenantVO.getStartDate());  
                cal2.add(Calendar.MONTH,-1);
                             
                Calendar cal3 = Calendar.getInstance();  
                cal3.setTime(firstDate);  
                cal3.add(Calendar.MONTH, -1);              
                              
                java.util.Date endAggrExt = cal1.getTime();
                java.util.Date startAggrExt = cal2.getTime();
                java.util.Date firstDtExt = cal3.getTime();
                
                               
                java.sql.Date endAgrDt = new java.sql.Date(endAggrExt.getTime());
                java.sql.Date startAgrDt = new java.sql.Date(startAggrExt.getTime());
                java.sql.Date  firstDt = new java.sql.Date(firstDtExt.getTime());
              
                                
                 // Agreement start and ends in FY.                                                                      
                if((startAggDate.after(firstDateCal)||startAggDate.equals(firstDateCal)) && (endAggDate.before(lastDateCal)|| endAggDate.equals(lastDateCal))){
                                    
                        differneceInMonth=Math.abs((dateutility.getDateDifference(rptTenantVO.getStartDate(), endAgrDt)));
                         logger.debug(aptId+" 1.After FY start to before  End FY "+differneceInMonth+"  here start and end dates are "+rptTenantVO.getStartDate()+" & "+endAgrDt);
                                                        
                }
                // Agrement starts before FY and ends after FY
                else   if((startAggDate.before(firstDateCal)||startAggDate.equals(firstDateCal))&& (endAggDate.after(lastDateCal)|| endAggDate.equals(lastDateCal))){
                    
                    differneceInMonth=Math.abs((dateutility.getDateDifference(firstDt, lastDate)));
                     logger.debug(aptId+" 2 .Before FY start  and  End after FY "+differneceInMonth+" here start and end dates are "+firstDate+" & "+lastDate);
                                                    
                }
                // Agrement starts before startDate of FY and ends before lastDate of FY
                else   if((startAggDate.before(firstDateCal)||startAggDate.equals(firstDateCal))&& (endAggDate.after(firstDateCal)|| endAggDate.equals(firstDateCal))){
                    
                    differneceInMonth=Math.abs((dateutility.getDateDifference(firstDate, endAgrDt)));
                     logger.debug(aptId+" 3 .Starts before FY start to end before End FY "+differneceInMonth+" here start and end dates are "+firstDt+" & "+endAgrDt);
                                                    
                }
                // Agrement starts after startDate of FY and ends after lastDate of FY
                else   if((startAggDate.before(lastDateCal)||startAggDate.equals(lastDateCal))&& (endAggDate.after(lastDateCal)|| endAggDate.equals(lastDateCal ))){
                    
                    differneceInMonth=Math.abs((dateutility.getDateDifference(startAgrDt, lastDate)));
                     logger.debug(aptId+" 4 .After FY start and ends after End FY "+differneceInMonth+" here start and end dates are "+startAgrDt+" & "+lastDate);
                                                    
            }        
                
                totalDiffernceInMonth=totalDiffernceInMonth+differneceInMonth;
                
                 logger.debug(differneceInMonth+"Aprt id-"+aptId+" Difference --"+totalDiffernceInMonth);
                 
            }
            }
            BigDecimal rentPeriodInBigdecimal=new BigDecimal(totalDiffernceInMonth);
            BigDecimal rentFeeRate=mmc.multiply(rentPeriodInBigdecimal);
            rentfee=rentFeeRate.multiply(new BigDecimal("0.10")).setScale(2);
            
            
            
        }
        logger.debug("Exit :private BigDecimal calculateRentFees(int aptID,Date memberDueDate,Date lastDateOfMonth )");
        return rentfee;
    }*/
           
  /*  public List getyearlyMaintainanceContriNotPresentInFY(int year,int societyId, List memberTxlist,int buildingId,String firstDate,String lastDate) {
        List incExpList = new ArrayList();
        String expMMCCmnts=null;
        try {
            logger.debug("Entry:public List getyearlyMaintainanceContriNotPresentInFY(int year,int societyId, List memberTxlist,int buildingId)"
                            + year + societyId);

            incExpList = reportDAO.getyearlyMaintainanceContriNotPresentInFY(
                    year, societyId,buildingId,firstDate,lastDate);
            for (int j = 0; memberTxlist.size() > j; j++) {
                RptMonthYearVO rptmntVo = (RptMonthYearVO) memberTxlist.get(j);
                for (int i = 0; incExpList.size() > i; i++) {
                    RptMonthYearVO rptMonthYearVO = (RptMonthYearVO) incExpList
                            .get(i);
                    if (rptmntVo.getMemberID() == rptMonthYearVO.getMemberID()) {

                        incExpList.remove(i);

                        --i;
                    }
                }
            }

            for (int i = 0; incExpList.size() > i; i++) {
            	logger.debug("Here list "+incExpList.size());
                RptMonthYearVO rptYearVO = (RptMonthYearVO) incExpList.get(i);
                Calendar cal = Calendar.getInstance();
                int CmYyear = cal.get(Calendar.YEAR);
                int month =cal.get(Calendar.MONTH);
                if(month<3){
                CmYyear = CmYyear - 1;
                }
                rptTransVO=calculateFees(rptYearVO,""+societyId, firstDate,lastDate);
                
                BigDecimal totalExp=calculateTotalinBigDecimal(rptTransVO);
                expMMCCmnts="TotalMMC="+rptTransVO.getDueForMMC()+"\n Total Tenant Fee="+rptTransVO.getRentalFees()+"\n Total Sinking Fund="+rptTransVO.getDueForSF()+"\n Total RM="+rptTransVO.getDueForRM()+"\n Other Dues="+rptTransVO.getDueForOther()+"\n Total Exp="+totalExp;
                
                  BigDecimal otherChargesAmount=reportDAO.getTxOtherChanrges(year, societyId, rptYearVO.getMemberID(),firstDate,lastDate);                                 
                  rptYearVO.setOtherCharges(otherChargesAmount);
                  rptYearVO.setComments(expMMCCmnts);
                  BigDecimal totalPaidAmount=rptYearVO.getTxAmount().add(rptYearVO.getOtherCharges()).setScale(2);                
                    rptYearVO.setTotalPaidCharges(totalPaidAmount);
                
                if(rptYearVO.getTxAmount().intValue()==0){
                    rptYearVO.setDiscount(new BigDecimal("0.00"));
                }
                if(rptYearVO.getOutstandBal().intValue()==0){
                    rptYearVO.setComments("Arrears Carried Forward");
                }
                BigDecimal tempArrears=rptYearVO.getOutstandBal().add(rptYearVO.getRefund()).setScale(2);
                BigDecimal tempActualTotal = rptYearVO.getTxAmount().subtract(
                        tempArrears).setScale(2);
                BigDecimal diference = totalExp.subtract(
                        tempActualTotal).setScale(2);
                BigDecimal balance=diference.subtract(rptYearVO.getDiscount()).setScale(2);
                
                rptYearVO.setDiffernce(balance);
                if(balance.intValue()>0){
                    rptYearVO.setDebit(rptYearVO.getDiffernce());
                    rptYearVO.setCredit(new BigDecimal("0.00"));
                    
                    
                }else if(balance.intValue()<=0){
                    int bal=Math.abs(balance.intValue());
                    
                    rptYearVO.setCredit(new BigDecimal(bal).setScale(2));
                    rptYearVO.setDebit(new BigDecimal("0.00"));
                
                }
                
                rptYearVO.setExpectedMMC(totalExp);
                
                rptYearVO.setOtherCharges(otherChargesAmount);
                                    
             //   rptYearVO.setExpAmntComments(expMMCCmnts);
                    
                    
                rptYearVO.setIsChecked(1);
                
                memberTxlist.add(rptYearVO);
            }

        } catch (Exception ex) {
            logger.error("Exception in getyearlyMaintainanceContriNotPresentInFY : "
                            + ex);
        }
        logger.debug("Exit:public List getyearlyMaintainanceContriNotPresentInFY(int year,int societyId, List memberTxlist,int buildingId)"
                        + incExpList.size());
        return memberTxlist;
    }

    public List getyearlyNonContibutionMembers(int year, int societyId,List memberTxlist,int buildingId,String firstDate, String lastDate) {
        List incExpList = new ArrayList();
        String expMMCCmnts=null;
        try {
            logger.debug("Entry:public List getyearlyNonContibutionMembers(int year, int societyId,List memberTxlist,int buildingId) "
                            + year + societyId);

            incExpList = reportDAO.getyearlyNonContibutionMembers(year,
                    societyId,buildingId,firstDate,lastDate);

            for (int i = 0; incExpList.size() > i; i++) {
                RptMonthYearVO rptYearVO = (RptMonthYearVO) incExpList.get(i);
                rptYearVO.setIsChecked(1);
               rptTransVO=calculateFees(rptYearVO,""+societyId, firstDate,lastDate);
                
                BigDecimal totalExp=calculateTotalinBigDecimal(rptTransVO);
                expMMCCmnts= "TotalMMC="+rptTransVO.getDueForMMC()+"\n Total Tenant Fee="+rptTransVO.getRentalFees()+"\n Total Sinking Fund="+rptTransVO.getDueForSF()+"\n Total RM="+rptTransVO.getDueForRM()+"\n Other Dues="+rptTransVO.getDueForOther()+"\n Total Exp="+totalExp;
                
                rptYearVO.setComments(expMMCCmnts);
                
                //other Charges paid amount            
                BigDecimal otherChargesAmount=reportDAO.getTxOtherChanrges(year, societyId, rptYearVO.getMemberID(),firstDate,lastDate);                      
                rptYearVO.setOtherCharges(otherChargesAmount);
               
                
                //total paid amount
                BigDecimal totalPaidAmount=rptYearVO.getTxAmount().add(rptYearVO.getOtherCharges()).setScale(2);                
                  rptYearVO.setTotalPaidCharges(totalPaidAmount);
                
                if(rptYearVO.getOutstandBal().intValue()==0){
                    rptYearVO.setComments("Arrears Carried Forward");
                }
                BigDecimal tempArrears=rptYearVO.getOutstandBal().add(rptYearVO.getRefund()).setScale(2);
                BigDecimal tempActualTotal = rptYearVO.getTotalPaidCharges().subtract(
                        tempArrears).setScale(2);

                BigDecimal diference = totalExp.subtract(
                        tempActualTotal).setScale(2);
                rptYearVO.setTxAmount(new BigDecimal("0.00").setScale(2));
                rptYearVO.setDiscount(new BigDecimal("0.00").setScale(2));
                BigDecimal balance=diference.subtract(rptYearVO.getDiscount()).setScale(2);
                rptYearVO.setDiffernce(balance);
                
                //logger.info(rptYearVO.getMemberID()+"Total Exp "+totalExp+" + "+rptYearVO.getOutstandBal()+" + "+rptYearVO.getRefund()+" - "+tempActualTotal+" "+rptYearVO.getTotalPaidCharges()+"="+balance);
                
                if(balance.intValue()>0){
                    rptYearVO.setDebit(rptYearVO.getDiffernce());
                    rptYearVO.setCredit(new BigDecimal("0.00"));
                    
                    
                }else if(balance.intValue()<=0){
                    int bal=Math.abs(balance.intValue());
                    
                    rptYearVO.setCredit(new BigDecimal(bal).setScale(2));
                    rptYearVO.setDebit(new BigDecimal("0.00"));
                    
                }    
                
                rptYearVO.setIsChecked(1);
                rptYearVO.setTxDate(null);
                memberTxlist.add(rptYearVO);
            }

        } catch (Exception ex) {
            logger.error("Exception in getyearlyNonContibutionMembers : " + ex);
        }
        logger.debug("Exit:public List getyearlyNonContibutionMembers(int year, int societyId,List memberTxlist,int buildingId) "
                        + incExpList.size());
        return memberTxlist;
    }
    */
    
    /*
    public List getFilteredTx(int year, String strSocietyID, String primaryID,int month) {

        List listTransaction = null;

        try {
            logger.debug("Entry : public List getFilteredTx(String strSocietyID,String tableRelation,String primaryID)");
            if(month!=0){
     			
 				if(month==1){
 					firstDate=(year+1)+"-"+month+"-"+01;
 					lastDate=(year+1)+"-"+(month+2)+"-"+31;
 					
 				}else{
 				firstDate=year+"-"+04+"-"+01;
 				lastDate=year+"-"+(month+2)+"-"+31;
 				
 				}
 				}else {
 				firstDate = year + "-" + 04 + "-" + 01;
 			    lastDate = (year + 1) + "-" + 03 + "-" + 31;
 			   
 			}
                
                listTransaction = reportDAO.getFilteredTx(year, strSocietyID, primaryID,firstDate,lastDate);
            

            logger.debug("outside query : " + listTransaction.size());
            logger
                    .debug("Exit : public List getFilteredTx(String strSocietyID,String tableRelation,String primaryID)");
        } catch (Exception ex) {
            logger.error("Exception in getFilteredTx : " + ex);

        }
        return listTransaction;

    }
*/
/*    private RptTransactionVO calculateFees(RptMonthYearVO rptMember,String societyID,String frmDate,String lastDate) throws ParseException{
		List memberFeeList=null;
		logger.debug("Entry : private List getMemberChargeDetails(RptTransactionVO rptMember,int societyID)");
		BigDecimal totalDueOther=BigDecimal.ZERO;
		String voucherXML = "";
		String voucherTag = ""; 
		RptTransactionVO rptTrans=new RptTransactionVO();
		rptTrans.setMember_id(rptMember.getMemberID());
		rptTrans.setUnitID(rptMember.getUnitID());
		rptTrans.setDueForMMC(BigDecimal.ZERO);
		rptTrans.setDueForSF(BigDecimal.ZERO);
		rptTrans.setDueForRM(BigDecimal.ZERO);
		rptTrans.setDueForOther(BigDecimal.ZERO);
		rptTrans.setRentalFees(BigDecimal.ZERO);
		memberFeeList=memberFeeDAO.getMemberChargeDetails(rptTrans,Integer.parseInt(societyID));
		
		for (int i = 0; i < memberFeeList.size(); i++) {
			BigDecimal due=BigDecimal.ZERO;
			BigDecimal dueOther=BigDecimal.ZERO;
			BigDecimal rentFee=BigDecimal.ZERO;
			BigDecimal lateFee=BigDecimal.ZERO;
			
			
			RptTransactionVO member=(RptTransactionVO) memberFeeList.get(i);
			
			int category=member.getCategoryID();
			
			 switch (category) {
	            case 16:  //For MMC
	            	due=calculateDue(member, societyID, category);
	            	if(due.signum() != 0){
	            	rptTrans.setDueForMMC(due);	            	
	            	
	            	   	if(rptMember.getIsRented()==1){
	            	   		
	            	  
						rentFee=due.multiply(new BigDecimal("0.10")).setScale(2);
						rptTrans.setRentalFees(rentFee);
					}}	            	
	            
	                     break;
	           
	            	case 24:  // For Calaulating Sinking Fund
	            		
	            	due=calculateDue(member, societyID, category);
	            	if(due.signum() != 0){
	            		rptTrans.setDueForSF(due);
		            	
	            	}
	                     break;
	            case 41:  // For Calculating Repairs and Maintenance
	            	
	            	due=calculateDue(member, societyID,category);
	            	if(due.signum() != 0){
	            		rptTrans.setDueForRM(due);
	            		
	            	}
	            	     break;
	           
	            default: // For calculating Any other fees
	            
	            	dueOther=calculateAllOther(member, societyID,category);
	            if(dueOther.signum() != 0){
	            	totalDueOther=totalDueOther.add(dueOther);
	        		rptTrans.setDueForOther(totalDueOther);
	            }
	                  	
	            break;
	        } 
			
			
			
			
			
			
		}
		
		
		logger.debug("Exit : private List getMemberChargeDetails(RptTransactionVO rptMember,int societyID)");
		return rptTrans;
	}
	
	*/
	
	private BigDecimal calculateDue(RptTransactionVO rptMember,String societyID,int category){
		   RptTransactionVO rptMeberObj=new RptTransactionVO();
		   Date frmDate=thirtyFirst;
		   BigDecimal totalDue=BigDecimal.ZERO;
		   BigDecimal paidAmt=BigDecimal.ZERO;
		   java.sql.Date today = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		   List memberFeeList=null;
		   logger.debug("Entry :  private BigDecimal calculateMMC(RptTransactionVO rptMember,int societyID)");
		   
		try {
			//For getting payment details		
			rptMeberObj=memberFeeDAO.getPaidChargesDetails(rptMember, Integer.parseInt(societyID), lastDateSQL,category);
			paidAmt=rptMeberObj.getTotal();
			if(rptMeberObj.getTx_to_date()==null){
				rptMeberObj=memberFeeDAO.getUnPaidChargesDetails(rptMember, Integer.parseInt(societyID), lastDateSQL,category);
			}
			
			// To get the applicable fees
			memberFeeList=memberFeeDAO.getAppllicableFees(rptMember, Integer.parseInt(societyID), firstDateSQL,lastDateSQL,category);
			int differneceInMonth=0;
			
			
			for(int i=0;memberFeeList.size()>i;i++){
				BigDecimal due=BigDecimal.ZERO;
				RptTransactionVO member=(RptTransactionVO) memberFeeList.get(i);	
				if(rptMember.getFrequency().equalsIgnoreCase("O")){
					
						due=rptMember.getTx_ammount().setScale(2,RoundingMode.HALF_UP);
					
					
				}else{
				if(i==0){//Default
					
					if(frmDate.compareTo(member.getFrom_date())<0){
						differneceInMonth=dateutility.getDateDifference(lastDateSQL, (Date) member.getFrom_date());
						logger.debug(frmDate+"  ****   "+member.getFrom_date()+" -"+lastDateSQL+""+frmDate.compareTo(member.getFrom_date())+"- "+differneceInMonth);
					}else{
						differneceInMonth=dateutility.getDateDifference(lastDateSQL, frmDate);
						logger.debug(frmDate+"  ****   "+member.getFrom_date()+" -- "+differneceInMonth);
					}}else{// To calculate previous dues
					
					if(frmDate.compareTo(member.getFrom_date())<0){
						differneceInMonth=dateutility.getDateDifference((Date) member.getTx_to_date(), (Date) member.getFrom_date());
					}else
						differneceInMonth=dateutility.getDateDifference((Date) member.getTx_to_date(), frmDate);
				}
				
				if(differneceInMonth>0){
					logger.debug("Due "+due+" "+member.getCalculationMode());
					if(member.getCalculationMode().equalsIgnoreCase("C")){
						BigDecimal percentage=member.getTx_ammount().multiply(new BigDecimal("0.01")).setScale(6);
						logger.debug("Due "+due+" "+member.getCalculationMode()+"         "+percentage);
						due=member.getUnitCost().multiply(percentage).setScale(0,RoundingMode.HALF_UP);
						logger.debug("Due "+due+" ="+member.getUnitCost()+"X"+percentage);
						due=due.multiply(new BigDecimal(differneceInMonth)).setScale(2).setScale(0,RoundingMode.HALF_UP);
						
					}else{
					
						logger.debug("Due "+due+" "+member.getCalculationMode());
					due=member.getTx_ammount().multiply(new BigDecimal(differneceInMonth)).setScale(2).setScale(0,RoundingMode.HALF_UP);
					logger.debug(member.getTx_ammount()+" X "+differneceInMonth+" = "+due);
					}}
				}
				totalDue=totalDue.add(due);
			}
				
			
					
					
			
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("The exception at calculateSF "+e+"  "+category);
			}
		   
		   
		   logger.debug("total Dues "+totalDue);
		   logger.debug("Exit : private BigDecimal calculateSF(RptTransactionVO rptMember,int societyID)");
		   return totalDue;
	   }
	
	
	
	
	
	
	
	private BigDecimal calculateAllOther(RptTransactionVO rptMember,String societyID,int category){
		  RptTransactionVO rptMeberObj=new RptTransactionVO();
		   BigDecimal otherDue=BigDecimal.ZERO;
		   BigDecimal paidAmt=BigDecimal.ZERO;
		   logger.debug("Entry :  private BigDecimal calculateOther(RptTransactionVO rptMember,int societyID)");
		  
		try {
					
			rptMeberObj=memberFeeDAO.getPaidChargesDetails(rptMember, Integer.parseInt(societyID), lastDateSQL,category);
			paidAmt=rptMeberObj.getTotal();
			if(rptMeberObj.getTx_to_date()==null){
				rptMeberObj=memberFeeDAO.getUnPaidChargesDetails(rptMember, Integer.parseInt(societyID), lastDateSQL,category);
			}
			
			
			if(rptMember.getFrequency().equalsIgnoreCase("O")){
				
					otherDue=rptMember.getTx_ammount().setScale(2,RoundingMode.HALF_UP);
				}else{
				int differneceInMonth=0;
				
				differneceInMonth = dateutility.getDateDifference(lastDateSQL, (Date) rptMeberObj.getTx_to_date());
			otherDue=rptMember.getTx_ammount().multiply(new BigDecimal(differneceInMonth)).setScale(2,RoundingMode.HALF_UP);
			}
			
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("The exception at calculateALL "+e);
			}
			
		   
		   
		   logger.debug("Exit : private BigDecimal calculateOther(RptTransactionVO rptMember,int societyID)");
		   return otherDue;
	   }
	
	
	/*private BigDecimal calculateLateFee(RptTransactionVO rptMember,int societyID,Date uptoDate,int categoryID){
		   RptTransactionVO rptMeberObj=new RptTransactionVO();
		   BigDecimal lateFee=BigDecimal.ZERO;
		   Date frmDate=null;
		   Calendar txDateCal = Calendar.getInstance();
		   Calendar fstDayOfFY=Calendar.getInstance();
		   logger.debug("Entry :  private BigDecimal calculateMMC(RptTransactionVO rptMember,int societyID)");
		   
		try {
					
			rptMeberObj=memberFeeDAO.getPaidChargesDetails(rptMember, societyID, uptoDate,categoryID);
			if(rptMeberObj.getTx_to_date()==null){
				rptMeberObj=memberFeeDAO.getUnPaidChargesDetails(rptMember, societyID, uptoDate,categoryID);
				}	   
			int differneceInMonth=0;
			txDateCal.setTime(rptMeberObj.getTx_to_date());
			
			frmDate=(Date) rptMeberObj.getTx_to_date();
			if(frmDate.compareTo(sdfSource.parse(firstDateofFY))<0){ // compare if the paid upto date is greater than 1st of Apr
				frmDate=new java.sql.Date( sdfSource.parse(firstDateofFY).getTime());
				
			}
			fstDayOfFY.setTime(java.sql.Date.valueOf(firstDateofFY));
		
				differneceInMonth = dateutility.getDateDifference(uptoDate, frmDate);	
			
			
						
			if(rptMember.getIsLateFee()==1){
			lateFee=getLateFee.calculateLatefees(rptMember, differneceInMonth);
			
			logger.debug(rptMember.getCalculationMode()+"Late fee for "+categoryID+" is --"+differneceInMonth+" == "+lateFee);
			}
			
				
			} catch (Exception e) {
				// 
				logger.error("The exception at calculateLateFee "+e);
			}
		   
		   
		   
		   logger.debug("Exit : private BigDecimal calculateMMC(RptTransactionVO rptMember,int societyID)");
		   return lateFee;
	   }
	*/
		
	private BigDecimal calculateServiceTax(BigDecimal due,BigDecimal servTax){
		   BigDecimal serviceTax=null;
		   logger.debug("Entry :  private BigDecimal calculateServiceTax(BigDecimal due,BigDecimal servTax)");
		   
		   
			
			try {
					 serviceTax=due.multiply(servTax).setScale(2);
				   
				
				
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("The exception at calculateServiceTax "+e);
			}
		   
		   
		   
		   logger.debug(" private BigDecimal calculateServiceTax(BigDecimal due,BigDecimal servTax)");
		   return serviceTax;
	   }
	
	private BigDecimal calculateTotalinBigDecimal(RptTransactionVO rptMember){
		logger.debug("Entry :private BigDecimal calculateTotalinBigDecimal(RptTransactionVO rptMember)");
		BigDecimal totalAmtAndLF=BigDecimal.ZERO;
		BigDecimal totalAmt=BigDecimal.ZERO;
		BigDecimal SF=rptMember.getDueForSF();
		BigDecimal RM=rptMember.getDueForRM();
		BigDecimal Other=rptMember.getDueForOther();
		BigDecimal MMC=rptMember.getDueForMMC();
		
		BigDecimal totalAmtArrears=BigDecimal.ZERO;
		
		
		if(RM==null){
			RM=BigDecimal.ZERO;
		}if(SF==null){
			SF=BigDecimal.ZERO;
		}if(Other==null){
			Other=BigDecimal.ZERO;
		}if(MMC==null){
			MMC=BigDecimal.ZERO;
		
		}
		try {
		
			
			
			
			BigDecimal sumSFRM = RM.add(SF);
			BigDecimal sumSFRMOther=sumSFRM.add(Other);
			 totalAmt = MMC.add(sumSFRMOther);
			 
		
			 totalAmtAndLF=totalAmt.add(rptMember.getRentalFees()).setScale(2, RoundingMode.HALF_UP);
			 logger.debug("The RM is "+RM+" The Other "+Other+" The MMC is "+MMC+" The Tenant Fee "+rptMember.getRentalFees());
			 
			logger.debug("The MMC :"+totalAmtAndLF+" SF: "+rptMember.getArrears()+" RM: "+rptMember.getArrearsPaid()+" Other "+Other); 
		} catch (Exception e) {
			logger.error("Exception at calculate total method"+e.getMessage());
		}
		 
		
		 logger.debug("Exit :private BigDecimal calculateTotalinBigDecimal(RptTransactionVO rptMember)");
		return totalAmtAndLF;
	}
	
	private BigDecimal calculatteBalance(BigDecimal arrears, BigDecimal arrears_paid){
		logger.debug("Entry :private BigDecimal calculatteBalance(BigDecimal arrears, BigDecimal arrears_paid)");
		BigDecimal totalAmount=null;
		
	     totalAmount=arrears.subtract(arrears_paid);
		
		 
		
		 logger.debug(" Exit :private BigDecimal calculatteBalance(BigDecimal arrears, BigDecimal arrears_paid)");
		return totalAmount;
	}
	/*
	private BigDecimal calculateRentFees(RptTransactionVO rptTransVO,Date lastDateOfMonth ){
		logger.debug("Entry :private BigDecimal calculateRentFees(int aptID,Date memberDueDate,Date lastDateOfMonth )");
		int differneceInMonth=0;
		int totalDiffernceInMonth=0;
		
		BigDecimal rentfee=BigDecimal.ZERO;
	   BigDecimal	rentFeesInpercent = BigDecimal.TEN
		.divide(hundred);
    	
    	try {
			if(rptTransVO.getAptID() !=0){
				List renterList = reportDAO.getRenterHistoryDetails(rptTransVO.getAptID(),paidUptoThisDateMMC,lastDateOfMonth);
				for(int i=0;renterList.size()>i;i++){
					RptTransactionVO rptTransactionVO = new RptTransactionVO();
					rptTransactionVO = (RptTransactionVO) renterList.get(i);	
				
				if(rptTransactionVO.getAddress()!=null){
					Calendar lastDateCal = Calendar.getInstance();
					Calendar txDateCal = Calendar.getInstance();
					Calendar txAggDateCal=Calendar.getInstance();
					Calendar paidUptoThisDateCal=Calendar.getInstance();
					lastDateCal.setTime(lastDateOfMonth);
					txDateCal.setTime(rptTransactionVO.getCurrent_date());
					txAggDateCal.setTime(rptTransactionVO.getTx_to_date());	
					paidUptoThisDateCal.setTime(paidUptoThisDateMMC);
					
					if(lastDateCal.after(txDateCal)&&paidUptoThisDateCal.before(txAggDateCal)){
					
						if(rptTransactionVO.getIs_rented()==1){
							differneceInMonth=Math.abs((dateutility.getDateDifference(rptTransactionVO.getCurrent_date(), paidUptoThisDateMMC)));
							
					}
					}
					else	if(lastDateCal.before(txDateCal)){
						
						if(rptTransactionVO.getIs_rented()==1){
							 differneceInMonth =Math.abs( dateutility.getDateDifference(
									lastDateOfMonth,  paidUptoThisDateMMC));
							
							
						}
					}else if(paidUptoThisDateCal.before(txDateCal)){
						
						if(rptTransactionVO.getIs_rented()==1){
							differneceInMonth=Math.abs(dateutility.getDateDifference(rptTransactionVO.getCurrent_date(), paidUptoThisDateMMC));
							
						}
				
					}
						
					
					totalDiffernceInMonth=totalDiffernceInMonth+differneceInMonth;
					
				logger.debug(totalDiffernceInMonth);
				
				}
				}
				BigDecimal rentPeriodInBigdecimal=new BigDecimal(totalDiffernceInMonth);
				BigDecimal rentFeeRate=rptTransVO.getTx_ammount().multiply(rentPeriodInBigdecimal);
				rentfee=rentFeeRate.multiply(rentFeesInpercent).setScale(2);
				if(totalDiffernceInMonth!=0){
					String Comments=totalDiffernceInMonth+" months * "+rptTransVO.getRentalFees()+"% = "+rentfee;
				
				rptTransVO.setCommentsForRentFee(Comments);}
			}else {
				rentfee=BigDecimal.ZERO;
			}
			
		
		} catch (NullPointerException e) {
			logger.error("Exception in calculating tenant Fee"+e.getMessage());
		}
    		
    	
    	logger.debug("Exit :private BigDecimal calculateRentFees(int aptID,Date memberDueDate,Date lastDateOfMonth )");
		return rentfee;
	}*/

    
    public ReportDAO getReportDAO() {
        return reportDAO;
    }

    public void setReportDAO(ReportDAO reportDAO) {
        this.reportDAO = reportDAO;
    }

	public MemberFeeDAO getMemberFeeDAO() {
		return memberFeeDAO;
	}

	public void setMemberFeeDAO(MemberFeeDAO memberFeeDAO) {
		this.memberFeeDAO = memberFeeDAO;
	}
}

