package com.emanager.server.financialReports.domainObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.financialReports.dataaccessObject.MemberFeeDAO;
import com.emanager.server.financialReports.dataaccessObject.ReportDAO;
import com.emanager.server.financialReports.valueObject.RptTransactionVO;

public class TotalChargesReportDomain {
	List reportList = null;

	Logger logger = Logger.getLogger(TotalChargesReportDomain.class);

	ReportDAO reportDAO;
	MemberFeeDAO memberFeeDAO;
	
	CalculateLateFee getLateFee=new CalculateLateFee();

	DateUtility dateutility = new DateUtility();

	BigDecimal latefeesInpercent = BigDecimal.ZERO;

	BigDecimal rentFeesInpercent = BigDecimal.ZERO;

	BigDecimal hundred = new BigDecimal("100");
	SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd");
	BigDecimal due = BigDecimal.ZERO;
	Date paidUptoThisDateMMC=null;
	Date paidUptoThisDateSF=null;
	String firstDateofFY=null;
    Date fromDate=null;
    Date toDate=null;
	BigDecimal total = new BigDecimal(0.00);
	String Comments=null;
	String financialYear=null;
	BigDecimal societyMMC;
	BigDecimal societySF;
	SimpleDateFormat dateFormat = new SimpleDateFormat("MMM-yy");
	
	
	public List getMemberDue(int month,int year, int intSocietyID,String buildingID) {
		logger.debug("Entry : public List getMemberDue(int month,int year, int intSocietyID,String buildingID)");
		List memberList=null;
		int count=0;
		try {
			Date uptoDate=dateutility.getMonthLastDate(month, year);
			if(month<4){
				year=year-1;
			}
		    firstDateofFY=year+"-03-31";
			
			memberList=getMemberList(intSocietyID,uptoDate,buildingID);
			for(int i=0;memberList.size()>i;i++){

			RptTransactionVO member=(RptTransactionVO) memberList.get(i);
			member=calculateFees(member, intSocietyID, uptoDate);
			BigDecimal balance=calculatteBalance(member.getArrears(), member.getArrearsPaid());
			BigDecimal actualTotal=calculateTotalinBigDecimal(member);
			member.setActualTotal(actualTotal);
			member.setActualToatalInString(""+actualTotal);
			member.setBalance(balance);
			
			if(member.getActualTotal().signum()<=0){
				
				memberList.remove(i);
				--i;
				count++;
			}
			
				//logger.info(actualTotal+" "+member.getMember_name()+" Here are the details : MMC due ="+member.getDueForMMC()+", Late fee="+member.getLateFeeForMMC()+", SF="+member.getDueForSF()+",Late fee="+member.getLateFeeForSF()+", Arrears :"+member.getArrears());
			}
		} catch (Exception e) {
			logger.error("Exception in getMemberDue "+e);
		}
		
		
		
		return memberList;
	}
	
	private List getMemberList(int societyID,Date uptDate,String buildingID){
		List memberList=null;
		logger.debug("Entry : private List getMemberList(int societyID)");
		
		memberList=memberFeeDAO.getMemberList(societyID,buildingID);
		
		logger.debug("Exit : private List getMemberList(int societyID)");
		return memberList;
	}
	
	private RptTransactionVO calculateFees(RptTransactionVO rptMember,int societyID,Date uptoDate){
		List memberFeeList=null;
		logger.debug("Entry : private List getMemberChargeDetails(RptTransactionVO rptMember,int societyID)");
		BigDecimal totalDueOther=BigDecimal.ZERO;
		societyMMC=BigDecimal.ZERO;
		societySF=BigDecimal.ZERO;
		memberFeeList=memberFeeDAO.getMemberChargeDetails(rptMember,societyID);
		
		for (int i = 0; i < memberFeeList.size(); i++) {
			BigDecimal dueForMMC=BigDecimal.ZERO;
			BigDecimal dueForSF=BigDecimal.ZERO;
			BigDecimal dueForRM=BigDecimal.ZERO;
			BigDecimal dueOther=BigDecimal.ZERO;
			BigDecimal rentFee=BigDecimal.ZERO;
			BigDecimal lateFee=BigDecimal.ZERO;
			
			RptTransactionVO member=(RptTransactionVO) memberFeeList.get(i);
			fromDate=(Date) member.getFrom_date();
			toDate=(Date) member.getTx_to_date();
			int category=member.getCategoryID();
			 switch (category) {
	            case 16:  //For MMC
	               	dueForMMC=calculateDue(member, societyID, uptoDate,category);
	            	if(dueForMMC.signum() != 0){
	            	lateFee=calculateLateFee(member, societyID, uptoDate, category);
	            	   	if(rptMember.getIs_rented()==1){
	            	   	//Updated as per discussion with Niranjan temporary removed
						rentFee=dueForMMC.multiply(new BigDecimal("0.10"));									
					}}	            	
	            	rptMember.setRentalFees(rentFee);
	            	rptMember.setDueForMMC(dueForMMC);
	            	rptMember.setMmcPaidUptodate(dateFormat.format(paidUptoThisDateMMC));
	            	rptMember.setLateFeeForMMC(lateFee);
	                     break;
	            case 24:  // For Calaulating Sinking Fund
	            	dueForSF=calculateDue(member, societyID, uptoDate,category);
	            	if(dueForSF.signum() != 0){
	            	lateFee=calculateLateFee(member, societyID, uptoDate, category);
	            	}rptMember.setDueForSF(dueForSF);
	            	rptMember.setSFPaidUptodate(dateFormat.format(paidUptoThisDateSF));
	            	rptMember.setLateFeeForSF(lateFee);
	                     break;
	            case 41:  // For Calculating Repairs and Maintenance
	            	dueForRM=calculateDue(member, societyID, uptoDate,category);
	            	if(dueForRM.signum() != 0){
	            	lateFee=calculateLateFee(member, societyID, uptoDate, category);
	            	}rptMember.setDueForRM(dueForRM);
	            	rptMember.setRMPaidUptodate(dateFormat.format(paidUptoThisDateMMC));
	            	rptMember.setLateFeeForRM(lateFee);
	                     break;
	           
	            default: // For calculating Any other fees
	            	dueOther=calculateAllOther(member, societyID, uptoDate,category);
	            	if(dueOther.signum() != 0){
	            	lateFee=calculateLateFee(member, societyID, uptoDate, category);
	            	}
	            	totalDueOther=totalDueOther.add(dueOther);
	        		rptMember.setDueForOther(totalDueOther);
	             	rptMember.setLateFeeForOther(lateFee);
	            	  break;
	        } 
			
			
			
			
			
			
		}
		
		
		logger.debug("Exit : private List getMemberChargeDetails(RptTransactionVO rptMember,int societyID)");
		return rptMember;
	}
	
	
	
	private BigDecimal calculateDue(RptTransactionVO rptMember,int societyID,Date uptoDate,int category){
		   RptTransactionVO rptMeberObj=new RptTransactionVO();
		   Date frmDate=null;
		   BigDecimal totalDue=BigDecimal.ZERO;
		   BigDecimal paidAmt=BigDecimal.ZERO;
		   BigDecimal societyMMC=BigDecimal.ZERO;
		   
		   List memberFeeList=null;
		   logger.debug("Entry :  private BigDecimal calculateMMC(RptTransactionVO rptMember,int societyID)");
		   
		try {
			//For getting payment details		
			rptMeberObj=memberFeeDAO.getPaidChargesDetails(rptMember, societyID, uptoDate,category);
			paidAmt=rptMeberObj.getTotal();
			if(rptMeberObj.getTx_to_date()==null){
			//To get the details of charges which are unpaid	
			rptMeberObj=memberFeeDAO.getUnPaidChargesDetails(rptMember, societyID, uptoDate,category);
			}
			frmDate=(Date) rptMeberObj.getTx_to_date();
			
			if(frmDate.compareTo(sdfSource.parse(firstDateofFY))<0){ // compare if the paid upto date is greater than 1st of Apr
				frmDate=new java.sql.Date( sdfSource.parse(firstDateofFY).getTime());
				
			}
			// To get the applicable fees
			memberFeeList=memberFeeDAO.getAppllicableFees(rptMember, societyID, uptoDate, frmDate,category);
			int differneceInMonth=0;
			
			
			if(rptMember.getFrequency().equalsIgnoreCase("O")){
				if(paidAmt==null){
					totalDue=rptMember.getTx_ammount();
						
				}else
				totalDue=rptMember.getTx_ammount().subtract(paidAmt);
				
				
			}else{
			for(int i=0;memberFeeList.size()>i;i++){
				BigDecimal due=BigDecimal.ZERO;
				RptTransactionVO member=(RptTransactionVO) memberFeeList.get(i);	
				if(i==0){//Default
					if(frmDate.compareTo(member.getFrom_date())<0){
						differneceInMonth=dateutility.getDateDifference(uptoDate, (Date) member.getFrom_date());
						logger.debug(frmDate+"  ****   "+member.getFrom_date()+" -- "+differneceInMonth);
					}else{
						differneceInMonth=dateutility.getDateDifference(uptoDate, frmDate);
						logger.debug(frmDate+"  ****   "+member.getFrom_date()+" -- "+differneceInMonth);
					}}else{// To calculate previous dues
					
					if(frmDate.compareTo(member.getFrom_date())<0){
						differneceInMonth=dateutility.getDateDifference((Date) member.getTx_to_date(), (Date) member.getFrom_date());
					}else
						differneceInMonth=dateutility.getDateDifference((Date) member.getTx_to_date(), frmDate);
				}
				
				if(differneceInMonth>0){
					
					
					due=member.getTx_ammount().multiply(new BigDecimal(differneceInMonth)).setScale(2).setScale(0,RoundingMode.HALF_UP);
					
					}
				logger.debug(member.getTx_ammount()+" X "+differneceInMonth+" = "+due);
				societyMMC=member.getTx_ammount();
				totalDue=totalDue.add(due);
			}
			}
				
			
			
			if(category==16){ // MMC paid upto date for calculating Tenant Fee
				paidUptoThisDateMMC=frmDate;
				this.societyMMC=societyMMC.setScale(0);
			}else if(category==24){ // Sinking Fund paid upto date for calculating Tenant Fee
				paidUptoThisDateSF=frmDate;
				this.societySF=societyMMC.setScale(2,RoundingMode.HALF_UP);
			}
			
					
			
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("The exception at CalculateDue "+e);
			}
		   
		   
		   
		   logger.debug(category+"Exit : private BigDecimal CalculateDue(RptTransactionVO rptMember,int societyID)"+totalDue);
		   return totalDue;
	   }
	
	
	// Old code
	/*private BigDecimal calculateDue(RptTransactionVO rptMember,int societyID,Date uptoDate,int category){
		   RptTransactionVO rptMeberObj=new RptTransactionVO();
		   BigDecimal due=BigDecimal.ZERO;
		   Date frmDate=null;
		   Calendar txDateCal = Calendar.getInstance();
		   Calendar fstDayOfFY=Calendar.getInstance();
		   logger.debug("Entry :  private BigDecimal calculateMMC(RptTransactionVO rptMember,int societyID)");
		   
		try {
					
			rptMeberObj=memberFeeDAO.getPaidChargesDetails(rptMember, societyID, uptoDate,category);
			if(rptMeberObj.getTx_to_date()==null){
			rptMeberObj=memberFeeDAO.getUnPaidChargesDetails(rptMember, societyID, uptoDate,category);
			}
			int differneceInMonth=0;
			txDateCal.setTime(rptMeberObj.getTx_to_date());
			
			fstDayOfFY.setTime(java.sql.Date.valueOf(firstDateofFY));
			if(fstDayOfFY.before(txDateCal)){
				if(rptMeberObj.getTx_to_date().compareTo(fromDate) > 0){
					if(uptoDate.compareTo(fromDate)>0){
						differneceInMonth = dateutility.getDateDifference(uptoDate, (Date) rptMeberObj.getTx_to_date());
					}else
					differneceInMonth = dateutility.getDateDifference(fromDate, (Date) rptMeberObj.getTx_to_date());
					
					
					logger.info(rptMember.getMember_id()+"--------->"+differneceInMonth+" "+rptMember.getId()+"  ******** "+fromDate+" *****"+rptMeberObj.getTx_to_date());
				}else{
				frmDate=(Date) rptMeberObj.getTx_to_date();
				differneceInMonth = dateutility.getDateDifference(uptoDate, fromDate);	
				logger.info(rptMember.getMember_id()+"-----2---->"+differneceInMonth+" "+rptMember.getId()+"  ******** "+fromDate+" *****"+rptMeberObj.getTx_to_date());
				}}else{
				frmDate=java.sql.Date.valueOf(firstDateofFY);
				differneceInMonth = dateutility.getDateDifference(uptoDate, java.sql.Date.valueOf(firstDateofFY));
				logger.info(rptMember.getMember_id()+"-----3---->"+differneceInMonth+" "+rptMember.getId()+"  ******** "+fromDate+" *****"+rptMeberObj.getTx_to_date());
			}if(category==16){
				paidUptoThisDate=frmDate;
			}
			
					
			
			if(differneceInMonth>0){
			
			
			due=rptMember.getTx_ammount().multiply(new BigDecimal(differneceInMonth)).setScale(2).setScale(0,RoundingMode.HALF_UP);
			logger.info(rptMember.getTx_ammount()+" X "+differneceInMonth+" = "+due);
			}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("The exception at calculateSF "+e);
			}
		   
		   
		   
		   logger.debug("Exit : private BigDecimal calculateSF(RptTransactionVO rptMember,int societyID)");
		   return due;
	   }*/
	
	
	
	
	
	private BigDecimal calculateAllOther(RptTransactionVO rptMember,int societyID,Date uptoDate,int category){
		   RptTransactionVO rptMeberObj=new RptTransactionVO();
		   BigDecimal otherDue=BigDecimal.ZERO;
		   BigDecimal paidAmt=BigDecimal.ZERO;
		   logger.debug("Entry :  private BigDecimal calculateMMC(RptTransactionVO rptMember,int societyID)");
		  
		try {
					
			rptMeberObj=memberFeeDAO.getPaidChargesDetails(rptMember, societyID, uptoDate,category);
			paidAmt=rptMeberObj.getTotal();
			if(rptMeberObj.getTx_to_date()==null){
				rptMeberObj=memberFeeDAO.getUnPaidChargesDetails(rptMember, societyID, uptoDate,category);
			}
			
			
			if(rptMember.getFrequency().equalsIgnoreCase("O")){
				if(paidAmt==null){
					otherDue=rptMember.getTx_ammount();
				}else
				otherDue=rptMember.getTx_ammount().subtract(paidAmt);
				
				
			}else{
				int differneceInMonth=0;
				
				differneceInMonth = dateutility.getDateDifference(uptoDate, (Date) rptMeberObj.getTx_to_date());
			otherDue=rptMember.getTx_ammount().multiply(new BigDecimal(differneceInMonth)).setScale(2).setScale(0,RoundingMode.HALF_UP);
			logger.info(rptMember.getTx_ammount()+" X "+differneceInMonth+" = "+otherDue);
			}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("The exception at calculateALL "+e);
			}
		   
		   
		   
		   logger.debug(category+"Exit : private BigDecimal calculateALL(RptTransactionVO rptMember,int societyID)"+otherDue);
		   return otherDue;
	   }
	
	
	private BigDecimal calculateLateFee(RptTransactionVO rptMember,int societyID,Date uptoDate,int categoryID){
		   RptTransactionVO rptMeberObj=new RptTransactionVO();
		   BigDecimal lateFee=BigDecimal.ZERO;
		   Date frmDate=null;
		   Calendar txDateCal = Calendar.getInstance();
		   Calendar fstDayOfFY=Calendar.getInstance();
		   logger.debug("Entry :  private BigDecimal calculateMMC(RptTransactionVO rptMember,int societyID)");
		   
		try {
					
			rptMeberObj=memberFeeDAO.getPaidChargesDetails(rptMember, societyID, uptoDate,categoryID);
			if(rptMeberObj.getTx_to_date()==null){
				rptMeberObj=memberFeeDAO.getUnPaidChargesDetails(rptMember, societyID, uptoDate,categoryID);
				}	   
			int differneceInMonth=0;
			txDateCal.setTime(rptMeberObj.getTx_to_date());
			
			frmDate=(Date) rptMeberObj.getTx_to_date();
			if(frmDate.compareTo(sdfSource.parse(firstDateofFY))<0){ // compare if the paid upto date is greater than 1st of Apr
				frmDate=new java.sql.Date( sdfSource.parse(firstDateofFY).getTime());
				
			}
			fstDayOfFY.setTime(java.sql.Date.valueOf(firstDateofFY));
		
				differneceInMonth = dateutility.getDateDifference(uptoDate, frmDate);	
			
			
						
			if(rptMember.getIsLateFee()==1){
			//lateFee=getLateFee.calculateLatefees(rptMember, differneceInMonth);
			
			logger.debug(rptMember.getCalculationMode()+"Late fee for "+categoryID+" is --"+differneceInMonth+" == "+lateFee);
			}
			
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("The exception at calculateLateFee "+e);
			}
		   
		   
		   
		   logger.debug("Exit : private BigDecimal calculateMMC(RptTransactionVO rptMember,int societyID)");
		   return lateFee;
	   }
	
		
	private BigDecimal calculateServiceTax(BigDecimal due,BigDecimal servTax){
		   BigDecimal serviceTax=null;
		   logger.debug("Entry :  private BigDecimal calculateServiceTax(BigDecimal due,BigDecimal servTax)");
		   
		   
			
			try {
					 serviceTax=due.multiply(servTax).setScale(2);
				   
				
				
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("The exception at calculateServiceTax "+e);
			}
		   
		   
		   
		   logger.debug(" private BigDecimal calculateServiceTax(BigDecimal due,BigDecimal servTax)");
		   return serviceTax;
	   }
	
	private BigDecimal calculateTotalinBigDecimal(RptTransactionVO rptMember){
		logger.debug("Entry :private BigDecimal calculateTotalinBigDecimal(RptTransactionVO rptMember)");
		BigDecimal totalAmtAndLF=BigDecimal.ZERO;
		BigDecimal totalAmt=BigDecimal.ZERO;
		BigDecimal SF=rptMember.getDueForSF();
		BigDecimal RM=rptMember.getDueForRM();
		BigDecimal Other=rptMember.getDueForOther();
		BigDecimal MMC=rptMember.getDueForMMC();
		BigDecimal totalLateFee=BigDecimal.ZERO;
		BigDecimal LFMMC=rptMember.getLateFeeForMMC();
		BigDecimal LFSF=rptMember.getLateFeeForSF();
		BigDecimal LFRM=rptMember.getLateFeeForRM();
		BigDecimal LFOther=rptMember.getLateFeeForOther();
		BigDecimal totalAmtArrears=BigDecimal.ZERO;
		
		
		if(RM==null){
			RM=BigDecimal.ZERO;
		}if(SF==null){
			SF=BigDecimal.ZERO;
		}if(Other==null){
			Other=BigDecimal.ZERO;
		}if(MMC==null){
			MMC=BigDecimal.ZERO;
		}if(LFMMC==null){
			LFMMC=BigDecimal.ZERO;
		}if(LFSF==null){
			LFSF=BigDecimal.ZERO;
		}if(LFOther==null){
			LFOther=BigDecimal.ZERO;
		}if(LFRM==null){
			LFRM=BigDecimal.ZERO;
		}
		try {
			BigDecimal sumLFSFRM=LFRM.add(LFSF);
			BigDecimal sumLFSFRMOther=sumLFSFRM.add(LFOther);
			totalLateFee=sumLFSFRMOther.add(LFMMC);
			
			BigDecimal sumSFRM = RM.add(SF);
			BigDecimal sumSFRMOther=sumSFRM.add(Other);
			 totalAmt = MMC.add(sumSFRMOther);
			 
			 totalAmtAndLF=totalAmt.add(totalLateFee);
			 totalAmtAndLF=totalAmtAndLF.add(rptMember.getRentalFees());
			 BigDecimal balance=calculatteBalance(rptMember.getArrears(), rptMember.getArrearsPaid());
			 totalAmtArrears=totalAmtAndLF.add(balance);
			logger.debug("The MMC :"+balance+" SF: "+rptMember.getArrears()+" RM: "+rptMember.getArrearsPaid()+" Other "+Other); 
		} catch (Exception e) {
			logger.error("Exception at calculate total method"+e.getMessage());
		}
		 
		
		 logger.debug("Exit :private BigDecimal calculateTotalinBigDecimal(RptTransactionVO rptMember)");
		return totalAmtArrears;
	}
	
	private BigDecimal calculatteBalance(BigDecimal arrears, BigDecimal arrears_paid){
		logger.debug("Entry :private BigDecimal calculatteBalance(BigDecimal arrears, BigDecimal arrears_paid)");
		BigDecimal totalAmount=null;
		
	     totalAmount=arrears.subtract(arrears_paid);
		
		 
		
		 logger.debug(" Exit :private BigDecimal calculatteBalance(BigDecimal arrears, BigDecimal arrears_paid)");
		return totalAmount;
	}
	
	private BigDecimal calculateRentFees(RptTransactionVO rptTransVO,Date lastDateOfMonth ){
		logger.debug("Entry :private BigDecimal calculateRentFees(int aptID,Date memberDueDate,Date lastDateOfMonth )");
		int differneceInMonth=0;
		int totalDiffernceInMonth=0;
		
		BigDecimal rentfee=BigDecimal.ZERO;
	   BigDecimal	rentFeesInpercent = BigDecimal.TEN
		.divide(hundred);
    	
    	try {
			if(rptTransVO.getAptID() !=0){
				List renterList = reportDAO.getRenterHistoryDetails(rptTransVO.getAptID(),paidUptoThisDateMMC,lastDateOfMonth);
				for(int i=0;renterList.size()>i;i++){
					RptTransactionVO rptTransactionVO = new RptTransactionVO();
					rptTransactionVO = (RptTransactionVO) renterList.get(i);	
				
				if(rptTransactionVO.getAddress()!=null){
					Calendar lastDateCal = Calendar.getInstance();
					Calendar txDateCal = Calendar.getInstance();
					Calendar txAggDateCal=Calendar.getInstance();
					Calendar paidUptoThisDateCal=Calendar.getInstance();
					lastDateCal.setTime(lastDateOfMonth);
					txDateCal.setTime(rptTransactionVO.getCurrent_date());
					txAggDateCal.setTime(rptTransactionVO.getTx_to_date());	
					paidUptoThisDateCal.setTime(paidUptoThisDateMMC);
					
					if(lastDateCal.after(txDateCal)&&paidUptoThisDateCal.before(txAggDateCal)){
					
						if(rptTransactionVO.getIs_rented()==1){
							differneceInMonth=Math.abs((dateutility.getDateDifference(rptTransactionVO.getCurrent_date(), paidUptoThisDateMMC)));
							
					}
					}
					else	if(lastDateCal.before(txDateCal)){
						
						if(rptTransactionVO.getIs_rented()==1){
							 differneceInMonth =Math.abs( dateutility.getDateDifference(
									lastDateOfMonth,  paidUptoThisDateMMC));
							
							
						}
					}else if(paidUptoThisDateCal.before(txDateCal)){
						
						if(rptTransactionVO.getIs_rented()==1){
							differneceInMonth=Math.abs(dateutility.getDateDifference(rptTransactionVO.getCurrent_date(), paidUptoThisDateMMC));
							
						}
				
					}
						
					
					totalDiffernceInMonth=totalDiffernceInMonth+differneceInMonth;
					
				logger.debug(totalDiffernceInMonth);
				
				}
				}
				BigDecimal rentPeriodInBigdecimal=new BigDecimal(totalDiffernceInMonth);
				BigDecimal rentFeeRate=rptTransVO.getTx_ammount().multiply(rentPeriodInBigdecimal);
				rentfee=rentFeeRate.multiply(rentFeesInpercent).setScale(2);
				if(totalDiffernceInMonth!=0){
					String Comments=totalDiffernceInMonth+" months * "+rptTransVO.getRentalFees()+"% = "+rentfee;
				
				rptTransVO.setCommentsForRentFee(Comments);}
			}else {
				rentfee=BigDecimal.ZERO;
			}
			
		
		} catch (NullPointerException e) {
			logger.error("Exception in calculating tenant Fee"+e.getMessage());
		}
    		
    	
    	logger.debug("Exit :private BigDecimal calculateRentFees(int aptID,Date memberDueDate,Date lastDateOfMonth )");
		return rentfee;
	}
	
	
	
	
	public RptTransactionVO getLastTrasaction(int memberID, int societyID )
	{	 BigDecimal rentFees=null;
		 RptTransactionVO rptMember=new RptTransactionVO();;
			try
			{
				logger.debug("Entry : public RptTransactionVO gelLastTrasaction(String memberID, String societyID )");
				
				rptMember=memberFeeDAO.getMemberDetails(memberID, societyID);
				java.util.Date today = new java.util.Date();
				java.sql.Date curDate = new java.sql.Date(today.getTime());
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(curDate);
				int month=calendar.get(Calendar.MONTH);
				int year=calendar.get(Calendar.YEAR);
				
				
				if(month<3){
					year=year-1;
				}
				firstDateofFY= year + "-" + 03 + "-" + 31;
				calendar.add(Calendar.MONTH, 1);
				calendar.set(Calendar.DAY_OF_MONTH, 1);
				calendar.add(Calendar.DATE, -1);

				java.util.Date lastDayOfMonth = calendar.getTime();

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String lastDay = sdf.format(lastDayOfMonth);
				
				
				rptMember=calculateFees(rptMember, societyID, java.sql.Date.valueOf(lastDay));
				
				BigDecimal balance=calculatteBalance(rptMember.getArrears(), rptMember.getArrearsPaid());
				BigDecimal actualTotal=calculateTotalinBigDecimal(rptMember);
				rptMember.setActualTotal(actualTotal);
				rptMember.setBalance(balance);
				rptMember.setSocietyMonthlyCharges(societyMMC);
				rptMember.setTx_ammount(societySF);
				
				
			
			}catch(NullPointerException ex)
			{
				
				logger.info("No Data found for the member  : "+memberID+" "+ex);
				
			}
			
			catch(Exception ex)
			{
				
				logger.error("Exception RptTransactionVO gelLastTrasaction(String memberID, String societyID ) : "+ex);
				
			}
			
			logger.debug("Exit :RptTransactionVO gelLastTrasaction(String memberID, String societyID )");
			
	
		return rptMember;
	}
	
	
	
	
	public ReportDAO getReportDAO() {
		return reportDAO;
	}

	public void setReportDAO(ReportDAO reportDAO) {
		this.reportDAO = reportDAO;
	}

	public MemberFeeDAO getMemberFeeDAO() {
		return memberFeeDAO;
	}

	public void setMemberFeeDAO(MemberFeeDAO memberFeeDAO) {
		this.memberFeeDAO = memberFeeDAO;
	}
}
