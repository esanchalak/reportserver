package com.emanager.server.financialReports.domainObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.financialReports.dataaccessObject.ReportDAO;
import com.emanager.server.financialReports.valueObject.RptTransactionVO;
import com.emanager.server.invoice.Services.InvoiceService;
import com.emanager.webservice.WrapperDataAccessObject.MemberLedgerVO;

public class MemberDueReportDomain {
	List reportList = null;

	Logger logger = Logger.getLogger(MemberDueReportDomain.class);

	ReportDAO reportDAO;
	
	TransactionService transactionService;
	
	InvoiceService invoiceService;
	
	CalculateLateFee getLateFee=new CalculateLateFee();

	DateUtility dateutility = new DateUtility();

	BigDecimal latefeesInpercent = new BigDecimal("0.00");

	BigDecimal rentFeesInpercent = new BigDecimal("0.00");

	BigDecimal hundred = new BigDecimal("100");

	BigDecimal due = new BigDecimal("0.00");

	BigDecimal total = new BigDecimal("0.00");

	String Comments=null;
	String financialYear=null;
	
	public List getMemberDue(int month,int year, int intSocietyID,String buildingID) {
		List nonTransactedMeberList=null;
		java.sql.Date curDate;
		String date=month+"/"+year;
		SimpleDateFormat sdfSource = new SimpleDateFormat("MM/yyyy");
		
		java.util.Date uptoDate = null;
		try {
			uptoDate = sdfSource.parse(date);
			
		} catch (ParseException e) {
			logger.error("Exception" + e);
		}

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(uptoDate);

		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.add(Calendar.DATE, -1);
		if(month<4){
			year=year-1;
		}
		
		java.util.Date today = new java.util.Date();  

        Calendar calendara = Calendar.getInstance();  
        calendara.setTime(today);  

        calendara.add(Calendar.MONTH, 1);  
        calendara.set(Calendar.DAY_OF_MONTH, 1);  
        calendara.add(Calendar.DATE, -1);  

        java.util.Date lastDayOfMontha = (java.util.Date) calendara.getTime();  

          
		String startOfFY= year + "-" + 03 + "-" + 31;
		String frstJan=(year+1)+"-"+01+"-"+01;
		java.util.Date lastDayOfMonth = calendar.getTime();
		logger.debug("Last date of month +"+lastDayOfMonth);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		String lastDay = sdf.format(lastDayOfMonth);
		String lastDayMonth=sdf.format(lastDayOfMontha);
		Calendar lastDateCal = Calendar.getInstance();
		Calendar fstJan=Calendar.getInstance();
		fstJan.setTime(java.sql.Date.valueOf(startOfFY));
		lastDateCal.setTime(lastDayOfMonth);
		
		int fyear=(year-2000)+1;
		financialYear=year+"-"+fyear;
		
		
		logger.debug("Entry : public List getMemberDue(int intSocietyID ) ");
		curDate = dateutility.findCurrentDate();
		
		//reportList = reportDAO.getMemberDue(intSocietyID, date,buildingID);
		nonTransactedMeberList=getMemberDueOfNonTransacted(intSocietyID, lastDay,reportList,buildingID,startOfFY);
		try { // date difference calulation

			for (int j = 0; j < reportList.size(); j++) {
				BigDecimal rentFees=null;
				RptTransactionVO rptTransactionVO = new RptTransactionVO();
				rptTransactionVO = (RptTransactionVO) reportList.get(j);
				if(rptTransactionVO.getIsChecked() !=1){
				Date compareDate = (Date) rptTransactionVO.getTx_to_date();
				
				Calendar txDateCal = Calendar.getInstance();
				Calendar fstDayOfFY=Calendar.getInstance();
				
				txDateCal.setTime(compareDate);
				
				fstDayOfFY.setTime(java.sql.Date.valueOf(startOfFY));
				
				
				if ((lastDateCal.before(txDateCal))||(lastDateCal.equals(txDateCal))) {
					reportList.remove(j);
					--j;
				} else {
					Date lastDateOfMonthSQl=java.sql.Date.valueOf(lastDayMonth);
					Date lastDateSQL = java.sql.Date.valueOf(lastDay);
					int differneceInMonth=0;
					int diffenceForLate=0;
					if(fstDayOfFY.before(txDateCal)){
					differneceInMonth = dateutility.getDateDifference(
							lastDateSQL, (Date) rptTransactionVO.getTx_to_date());
					diffenceForLate = dateutility.getDateDifference(
							lastDateOfMonthSQl, (Date) rptTransactionVO.getTx_to_date());
					}else{
						differneceInMonth=dateutility.getDateDifference(lastDateSQL,java.sql.Date.valueOf(startOfFY) );
					diffenceForLate = dateutility.getDateDifference(
							lastDateOfMonthSQl, java.sql.Date.valueOf(startOfFY));
					}
						BigDecimal convertInt = new BigDecimal(
								differneceInMonth);
						BigDecimal convertIntLate=new BigDecimal(diffenceForLate);
						BigDecimal tempSF=rptTransactionVO.getSinkingFunds().add(rptTransactionVO.getRMCharges());
						
						BigDecimal serviceTax=rptTransactionVO.getSocietyMonthlyCharges().multiply(rptTransactionVO.getServiceTax()).setScale(2);
						
						BigDecimal tempMMC=rptTransactionVO.getSocietyMonthlyCharges().add(tempSF);
						
						BigDecimal tempMMCServ=tempMMC.add(serviceTax);
						
						BigDecimal mmcDue=rptTransactionVO.getSocietyMonthlyCharges().multiply(convertInt).setScale(2);
						
						
						
						BigDecimal due = tempMMCServ.multiply(convertInt).setScale(0,RoundingMode.HALF_UP).setScale(2);
						
						
						rentFeesInpercent = rptTransactionVO.getRentalFees()
								.divide(hundred);
						
								
						// logger.info("Pending :--"+due);
						
							
						BigDecimal lateFees=null;
							if(dateutility.isLateFeeApplicable(rptTransactionVO)){
							// lateFees=getLateFee.calculateLatefeesInBigdecimal(convertIntLate, rptTransactionVO,diffenceForLate);
							 if(rptTransactionVO.getCalculationMode().equalsIgnoreCase("FIX"))
								 Comments=differneceInMonth+" months * "+rptTransactionVO.getLateFees()+" = "+lateFees;
							else
							 Comments=due+" * "+rptTransactionVO.getLateFees()+"% = "+lateFees;
							 
							}else{
								lateFees=new BigDecimal("0.00");
								Comments=" = 0.00";
							}
							if(rptTransactionVO.getIs_rented()==1){
								rentFees=mmcDue.multiply(new BigDecimal("0.10")).setScale(2);
								String Comments=due+"  * "+rptTransactionVO.getRentalFees()+"% = "+rentFees;
								
								rptTransactionVO.setCommentsForRentFee(Comments);
							}else
								 rentFees=new BigDecimal("0.00");
							BigDecimal balance=calculatteBalance(rptTransactionVO.getArrears(), rptTransactionVO.getArrearsPaid());
							
							BigDecimal actualTotal=calculateTotalinBigDecimal(due, lateFees, rentFees,balance);
							
							String commentsForDue="For FY "+financialYear+" :"+tempMMCServ+" * "+differneceInMonth+" months = "+due;
							rptTransactionVO.setLateFees(lateFees);
							rptTransactionVO.setRentalFees(rentFees);
							rptTransactionVO.setBalance(balance);
							rptTransactionVO.setActualTotal(actualTotal);
							rptTransactionVO.setCommentForLateFee(Comments);
							rptTransactionVO.setCommentForDue(commentsForDue);
							rptTransactionVO.setActualToatalInString(""+actualTotal);
							
						
						
						rptTransactionVO.setDue(due);
					
						DateFormat df = new SimpleDateFormat("MMM-yyyy");
						String sa = df.format(rptTransactionVO.getTx_to_date());
						
						
						rptTransactionVO.setDisplay_date(df.parse(sa));
						rptTransactionVO.setDisplayDate(df.format(rptTransactionVO.getTx_to_date()));
						
						
						
					
				}
			}
			}
			logger.debug("Exit : public List getMemberDue(int intSocietyID)");
		} catch (Exception ex) {

			logger.error("Exception in getMemberDue : " + ex);

		}
		
		return reportList;
	}
	
	public List getMemberDueOfNonTransacted(int societyID,String lastDayOfMonth,List reportList,String buildingID ,String startOfFY ){
		List nonTransactedMeberList=null;
		logger.debug("Entry : public List getMemberDueOfNonTransacted(int SocietyID,Date lastDayOfMonth ) ");
		
		//nonTransactedMeberList=reportDAO.getMemberDueOfNonTransacted(societyID,buildingID);
		

			try {
				for (int j = 0; j < nonTransactedMeberList.size(); j++) {
					BigDecimal rentFees=null;
					RptTransactionVO rptTransactionVO = new RptTransactionVO();
					rptTransactionVO = (RptTransactionVO) nonTransactedMeberList.get(j);

					Calendar fstDayOfFY=Calendar.getInstance();
					Calendar txDateCal = Calendar.getInstance();
					fstDayOfFY.setTime(java.sql.Date.valueOf(startOfFY));
					txDateCal.setTime(rptTransactionVO.getTx_to_date());

						Date lastDateSQL = java.sql.Date.valueOf(lastDayOfMonth);
						
						int differneceInMonth=0;
						if(fstDayOfFY.before(txDateCal)){
						differneceInMonth = dateutility.getDateDifference(
								lastDateSQL, (Date) rptTransactionVO.getTx_to_date());
						}else
							differneceInMonth=dateutility.getDateDifference(lastDateSQL,java.sql.Date.valueOf(startOfFY) );
						
						
						if (differneceInMonth <= 0) {
							nonTransactedMeberList.remove(j);
							--j;

						} else {
							BigDecimal convertInt = new BigDecimal(
									differneceInMonth);
							
							BigDecimal tempSF=rptTransactionVO.getSinkingFunds().add(rptTransactionVO.getRMCharges());
														
							BigDecimal serviceTax=rptTransactionVO.getSocietyMonthlyCharges().multiply(rptTransactionVO.getServiceTax()).setScale(2);
							
							BigDecimal tempMMC=rptTransactionVO.getSocietyMonthlyCharges().add(tempSF);
							
							BigDecimal tempMMCServ=tempMMC.add(serviceTax);
							
							BigDecimal mmcDue=rptTransactionVO.getSocietyMonthlyCharges().multiply(convertInt).setScale(2);
														
							BigDecimal due = tempMMCServ.multiply(convertInt).setScale(0,RoundingMode.HALF_UP).setScale(2);
							BigDecimal lateFees=null;
							if(dateutility.isLateFeeApplicable(rptTransactionVO)){
							// lateFees=getLateFee.calculateLatefeesInBigdecimal(convertInt, rptTransactionVO,differneceInMonth);
							 if(rptTransactionVO.getCalculationMode().equalsIgnoreCase("FIX"))
								 Comments=differneceInMonth+" months * "+rptTransactionVO.getLateFees()+" = "+lateFees;
							else
							 Comments=due+" * "+rptTransactionVO.getLateFees()+"% = "+lateFees;
							 
							}else{
								lateFees=new BigDecimal("0.00");
								Comments=" = 0.00";
							}
							
							if(rptTransactionVO.getIs_rented()==1){
								rentFees=mmcDue.multiply(new BigDecimal("0.10")).setScale(2);
								String Comments=due+"  * "+rptTransactionVO.getRentalFees()+"% = "+rentFees;
								
								rptTransactionVO.setCommentsForRentFee(Comments);
							}else
								 rentFees=new BigDecimal("0.00");
							//BigDecimal rentFees=calculateRentFees(rptTransactionVO, lastDateSQL);
							BigDecimal balance=calculatteBalance(rptTransactionVO.getArrears(), rptTransactionVO.getArrearsPaid());
							BigDecimal actualTotal=calculateTotalinBigDecimal(due, lateFees, rentFees,balance);
							
							
							String commentsForDue="For FY "+financialYear+" :"+tempMMCServ+" * "+differneceInMonth+" months = "+due;
								rptTransactionVO.setLateFees(lateFees);
								rptTransactionVO.setRentalFees(rentFees);
								rptTransactionVO.setBalance(balance);
								rptTransactionVO.setActualTotal(actualTotal);
								rptTransactionVO.setCommentForLateFee(Comments);
								rptTransactionVO.setCommentForDue(commentsForDue);
								rptTransactionVO.setActualToatalInString(""+actualTotal);
							
							rptTransactionVO.setDue(due);
						

							DateFormat df = new SimpleDateFormat("MMM-yyyy");
							String sa = df.format(rptTransactionVO.getTx_to_date());
							SimpleDateFormat sdfsource = new SimpleDateFormat("MMM-yyyy");
							try {
								rptTransactionVO.setDisplay_date(sdfsource.parse(sa));
								rptTransactionVO.setDisplayDate(sdfsource.format(rptTransactionVO.getTx_to_date()));
							} catch (ParseException e) {
								
								logger.error("Exception "+e);
							}
							rptTransactionVO.setIsChecked(1);
							
							reportList.add(rptTransactionVO);
							lateFees=null;
							actualTotal=null;
						}
				}
			} catch (NullPointerException e) {
				logger.info("Here no data found "+e);
			}
		
		
		
		logger.debug("Exit : public List getMemberDueOfNonTransacted(int SocietyID,Date lastDayOfMonth ) ");
		return reportList;
	}
	
		
	private BigDecimal calculateServiceTax(BigDecimal due,BigDecimal servTax){
		   BigDecimal serviceTax=null;
		   logger.debug("Entry :  private BigDecimal calculateServiceTax(BigDecimal due,BigDecimal servTax)");
		   
		   
			
			try {
					 serviceTax=due.multiply(servTax).setScale(2);
				   
				
				
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("The exception at calculateServiceTax "+e);
			}
		   
		   
		   
		   logger.debug(" private BigDecimal calculateServiceTax(BigDecimal due,BigDecimal servTax)");
		   return serviceTax;
	   }
	
	private BigDecimal calculateTotalinBigDecimal(BigDecimal due, BigDecimal lateFee,BigDecimal rentFee ,BigDecimal balance){
		logger.debug("Entry :private BigDecimal calculateTotalinBigDecimal(BigDecimal due, BigDecimal lateFee,BigDecimal rentFee)"+lateFee);
		BigDecimal totalAmount=null;
		
		BigDecimal lateFeePlsRent = lateFee.add(rentFee);
		BigDecimal addBal=lateFeePlsRent.add(balance);
		 totalAmount = due.add(addBal);
		 
		
		 logger.debug("Exit :private BigDecimal calculateTotalinBigDecimal(BigDecimal due, BigDecimal lateFee,BigDecimal rentFee)");
		return totalAmount;
	}
	
	private BigDecimal calculatteBalance(BigDecimal arrears, BigDecimal arrears_paid){
		logger.debug("Entry :private BigDecimal calculatteBalance(BigDecimal arrears, BigDecimal arrears_paid)");
		BigDecimal totalAmount=null;
		
	     totalAmount=arrears.subtract(arrears_paid);
		
		 
		
		 logger.debug(" Exit :private BigDecimal calculatteBalance(BigDecimal arrears, BigDecimal arrears_paid)");
		return totalAmount;
	}
	
	private BigDecimal calculateRentFees(RptTransactionVO rptTransVO,Date lastDateOfMonth ){
		logger.debug("Entry :private BigDecimal calculateRentFees(int aptID,Date memberDueDate,Date lastDateOfMonth )");
		int differneceInMonth=0;
		int totalDiffernceInMonth=0;
		Date paidUptoThisDate=(Date) rptTransVO.getTx_to_date();
		BigDecimal rentfee=new BigDecimal("0.00");
	   BigDecimal	rentFeesInpercent = rptTransVO.getRentalFees()
		.divide(hundred);
    	
    	if(rptTransVO.getAptID() !=0){
			List renterList = reportDAO.getRenterHistoryDetails(rptTransVO.getAptID(),paidUptoThisDate,lastDateOfMonth);
			for(int i=0;renterList.size()>i;i++){
				RptTransactionVO rptTransactionVO = new RptTransactionVO();
				rptTransactionVO = (RptTransactionVO) renterList.get(i);	
			
			if(rptTransactionVO.getAddress()!=null){
				Calendar lastDateCal = Calendar.getInstance();
				Calendar txDateCal = Calendar.getInstance();
				Calendar txAggDateCal=Calendar.getInstance();
				Calendar paidUptoThisDateCal=Calendar.getInstance();
				lastDateCal.setTime(lastDateOfMonth);
				txDateCal.setTime(rptTransactionVO.getCurrent_date());
				txAggDateCal.setTime(rptTransactionVO.getTx_to_date());	
				paidUptoThisDateCal.setTime(paidUptoThisDate);
				
				if(lastDateCal.after(txDateCal)&&paidUptoThisDateCal.before(txAggDateCal)){
				
					if(rptTransactionVO.getIs_rented()==1){
						differneceInMonth=Math.abs((dateutility.getDateDifference(rptTransactionVO.getCurrent_date(), (Date) rptTransactionVO.getTx_to_date())));
						
				}
				}
				else	if(lastDateCal.before(txDateCal)){
					
					if(rptTransactionVO.getIs_rented()==1){
						 differneceInMonth =Math.abs( dateutility.getDateDifference(
								lastDateOfMonth, (Date) rptTransVO.getTx_to_date()));
						
					}
				}else if(paidUptoThisDateCal.before(txDateCal)){
					
					if(rptTransactionVO.getIs_rented()==1){
						differneceInMonth=Math.abs(dateutility.getDateDifference(rptTransactionVO.getCurrent_date(), paidUptoThisDate));
						
					}
			
				}
					
				
				totalDiffernceInMonth=totalDiffernceInMonth+differneceInMonth;
				
			logger.debug(totalDiffernceInMonth);
			
			}
			}
			BigDecimal rentPeriodInBigdecimal=new BigDecimal(totalDiffernceInMonth);
			BigDecimal rentFeeRate=rptTransVO.getSocietyMonthlyCharges().multiply(rentPeriodInBigdecimal);
			rentfee=rentFeeRate.multiply(rentFeesInpercent).setScale(2);
			if(totalDiffernceInMonth!=0){
				String Comments=totalDiffernceInMonth+" months * "+rptTransVO.getRentalFees()+"% = "+rentfee;
			
			rptTransVO.setCommentsForRentFee(Comments);}
    	}else {
    		rentfee=new BigDecimal("0.00");
    	}
    		
    	
    	logger.debug("Exit :private BigDecimal calculateRentFees(int aptID,Date memberDueDate,Date lastDateOfMonth )");
		return rentfee;
	}
	/*
	public RptTransactionVO getLastTrasaction(String memberID, int societyID )
	{	 BigDecimal rentFees=null;
		 RptTransactionVO transactionVO=new RptTransactionVO();;
		try
		{
			logger.debug("Entry : public RptTransactionVO getLastTrasaction(String memberID, String societyID )");
			java.util.Date today = new java.util.Date();
			java.sql.Date curDate = new java.sql.Date(today.getTime());
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(curDate);
			int month=calendar.get(Calendar.MONTH);
			int year=calendar.get(Calendar.YEAR);
			
			
			if(month<3){
				year=year-1;
			}
			String startOfFY= year + "-" + 03 + "-" + 31;
			calendar.add(Calendar.MONTH, 1);
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			calendar.add(Calendar.DATE, -1);

			java.util.Date lastDayOfMonth = calendar.getTime();

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String lastDay = sdf.format(lastDayOfMonth);

			
			transactionVO=reportDAO.getLastTrasaction(memberID, societyID);
			if(transactionVO.getTx_to_date()==null){
				transactionVO=reportDAO.getLastTrasactionWhoNotPaid(memberID,societyID);
			}
	
			if(transactionVO.getIsChecked() !=1){
			Date compareDate = (Date) transactionVO.getTx_to_date();
			Calendar lastDateCal = Calendar.getInstance();
			Calendar txDateCal = Calendar.getInstance();
			Calendar fstDayOfFY=Calendar.getInstance();
			lastDateCal.setTime(lastDayOfMonth);
			fstDayOfFY.setTime(java.sql.Date.valueOf(startOfFY));
			txDateCal.setTime(compareDate);

			if (lastDateCal.before(txDateCal)) {
				DateFormat df = new SimpleDateFormat("MMM-yyyy");
				String sa = df.format(transactionVO.getTx_to_date());
				SimpleDateFormat sdfsource = new SimpleDateFormat("MMM-yyyy");
				transactionVO.setDisplay_date(sdfsource.parse(sa));
			} else {

				Date lastDateSQL = java.sql.Date.valueOf(lastDay);

				int differneceInMonth=0;
				if(fstDayOfFY.before(txDateCal)){
				  differneceInMonth= dateutility.getDateDifference(
						lastDateSQL, (Date) transactionVO.getTx_to_date());
				}else
					differneceInMonth=dateutility.getDateDifference(lastDateSQL,java.sql.Date.valueOf(startOfFY) );
					
				if (differneceInMonth <= 0) {	
					DateFormat df = new SimpleDateFormat("MMM-yyyy");
					String sa = df.format(transactionVO.getTx_to_date());
					
					SimpleDateFormat sdfsource = new SimpleDateFormat("MMM-yyyy");
					transactionVO.setDisplay_date(sdfsource.parse(sa));
					
				} else {
					BigDecimal convertInt = new BigDecimal(
							differneceInMonth);
					BigDecimal serviceTax=transactionVO.getSocietyMonthlyCharges().multiply(transactionVO.getServiceTax()).setScale(2);
					BigDecimal tempSF=transactionVO.getSinkingFunds().add(transactionVO.getRMCharges());
					BigDecimal sfAndST=serviceTax.add(tempSF);
					BigDecimal tempDue=transactionVO.getSocietyMonthlyCharges().add(sfAndST);
					BigDecimal mmcDue=transactionVO.getSocietyMonthlyCharges().multiply(convertInt).setScale(2);
					BigDecimal due = tempDue.multiply(convertInt).setScale(0,RoundingMode.HALF_UP).setScale(2);
					rentFeesInpercent = transactionVO.getRentalFees()
							.divide(hundred);
					BigDecimal lateFees=null;
					
					if(dateutility.isLateFeeApplicable(transactionVO)){
					// lateFees=getLateFee.calculateLatefeesInBigdecimal(convertInt, transactionVO,differneceInMonth);
					
					 if(transactionVO.getCalculationMode().equalsIgnoreCase("FIX"))
						 Comments=differneceInMonth+" months * "+transactionVO.getLateFees()+" = "+lateFees;
					else
					 Comments=due+" * "+transactionVO.getLateFees()+"% = "+lateFees;
					 
					}else{
						lateFees=new BigDecimal("0.00");
						Comments=" = 0.00";
					}
					if(transactionVO.getIs_rented()==1){
						rentFees=mmcDue.multiply(new BigDecimal("0.10")).setScale(2);
					}else
						 rentFees=new BigDecimal("0.00");
					//logger.info(month+"************"+startOfFY);
					
					//BigDecimal rentFees=calculateRentFees(transactionVO, lastDateSQL);
					BigDecimal balance=calculatteBalance(transactionVO.getArrears(), transactionVO.getArrearsPaid());
					BigDecimal actualTotal=calculateTotalinBigDecimal(due, lateFees, rentFees,balance);
					String commentsForDue=transactionVO.getSocietyMonthlyCharges()+" X "+differneceInMonth+" months = "+due;
					transactionVO.setLateFees(lateFees);
					transactionVO.setBalance(balance);
					transactionVO.setRentalFees(rentFees);
					transactionVO.setActualTotal(actualTotal);
					transactionVO.setCommentForDue(commentsForDue);
					transactionVO.setActualToatalInString(""+actualTotal);
					transactionVO.setSocietyMonthlyCharges(tempDue);
					transactionVO.setDue(due);
					transactionVO.setMonthParam(differneceInMonth);
					
					
					DateFormat df = new SimpleDateFormat("MMM-yyyy");
					String sa = df.format(transactionVO.getTx_to_date());
					SimpleDateFormat sdfsource = new SimpleDateFormat("MMM-yyyy");
					transactionVO.setDisplay_date(sdfsource.parse(sa));
					
					
					
			
		}
			}
			// else for member last transaction is null
			}else{
				transactionVO=reportDAO.getMemberMMC(memberID, societyID);
			}			
			if(transactionVO.getActualTotal()==null){
				transactionVO.setLateFees((BigDecimal.ZERO).setScale(2));
				transactionVO.setRentalFees((BigDecimal.ZERO).setScale(2));
				transactionVO.setCommentForDue("0");
				transactionVO.setDue((BigDecimal.ZERO).setScale(2));
				transactionVO.setActualTotal((BigDecimal.ZERO).setScale(2));
			}
		
		}catch(NullPointerException ex)
		{
			
			logger.info("No Data found for the member  : "+memberID+" "+ex);
			
		}
		
		catch(Exception ex)
		{
			
			logger.error("Exception RptTransactionVO gelLastTrasaction(String memberID, String societyID ) : "+ex);
			
		}
		
		logger.debug("Exit :RptTransactionVO gelLastTrasaction(String memberID, String societyID )");
		return transactionVO;
	}*/
	
	public List getMemberDueNew(String uptoDate, int societyID,String buildingID,String groupType) {
		List dueMemberList=null;
		java.sql.Date curDate;
		String fromDate="2014-04-01";
		DateFormat df=new SimpleDateFormat("yyyy-MM-dd");		
		SimpleDateFormat sdf=new SimpleDateFormat("dd-MMM-yy");	
		
		logger.debug("Entry : public List getMemberDue(int intSocietyID ) ");
		curDate = dateutility.findCurrentDate();
		if(groupType.equalsIgnoreCase("APT")){
		//	dueMemberList = reportDAO.getClosingBalances(societyID, buildingID, uptoDate) ;
			logger.info("Group By Apartment result: "+dueMemberList.size());
			
		}else if(groupType.equalsIgnoreCase("BLDG")){
			//dueMemberList = reportDAO.getClosingBalanceSummary(societyID, buildingID, uptoDate) ;
			logger.info("Group By Building result: "+dueMemberList.size());
		}
		
		
		try { // date difference calulation

			for (int j = 0; j < dueMemberList.size(); j++) {
				BigDecimal due=BigDecimal.ZERO;
				BigDecimal total=BigDecimal.ZERO;
				BigDecimal otherFee=BigDecimal.ZERO;
				int aptCount=0;
				List transactionList=null;
				RptTransactionVO rptTransactionVO = new RptTransactionVO();
				RptTransactionVO tempRptTxVo=new RptTransactionVO();
				rptTransactionVO = (RptTransactionVO) dueMemberList.get(j);
				
				 due=rptTransactionVO.getDue().add(rptTransactionVO.getDueForSF());
				 due=due.add(rptTransactionVO.getDueForMMC());
				 due=due.add(rptTransactionVO.getDueForInfra());
				 due=due.add(rptTransactionVO.getDueForLegalFee());
				 due=due.add(rptTransactionVO.getDueForMeter());
				 due=due.add(rptTransactionVO.getDueForTransfer());
				 due=due.add(rptTransactionVO.getDueForContigency());
				 due=due.add(rptTransactionVO.getDueForCarParking());
				 due=due.add(rptTransactionVO.getDueForCorpus());
				 due=due.add(rptTransactionVO.getDueForSocietyFrm());
				 due=due.add(rptTransactionVO.getDueForWaterCharges());
				 due=due.add(rptTransactionVO.getDueForRM());
				 due=due.add(rptTransactionVO.getDueForLift());
				 due=due.add(rptTransactionVO.getDueForOther()).setScale(2);
				 
				
				//achVO=transactionService.getOpeningClosingBalance(societyID, rptTransactionVO.getLedgerID(), uptoDate, uptoDate, "L");
				total=due.add(rptTransactionVO.getLateFees()).setScale(2);
				
				rptTransactionVO.setDue(due);
				
				rptTransactionVO.setActualTotal(total);
				
				
					
				}
			
			
			logger.debug("Exit : public List getMemberDue(int intSocietyID)");
		} catch (Exception ex) {

			logger.error("Exception in getMemberDue : " + ex);

		}
		
		return dueMemberList;
	}
	
	public List getMemberDueWithClosingBal(String uptoDate, int societyID,int buildingID) {
		List dueMemberList=null;
		java.sql.Date curDate;
		String fromDate="2014-04-01";
		DateFormat df=new SimpleDateFormat("yyyy-MM-dd");		
		SimpleDateFormat sdf=new SimpleDateFormat("dd-MMM-yy");
		
		logger.debug("Entry : public List getMemberDueWithClosingBal(String uptoDate, String societyID,String buildingID) ");
		
		try { 
			dueMemberList = reportDAO.getMemberDueWithClosingBal(societyID, buildingID, uptoDate) ;
						
			logger.debug("Exit : public List getMemberDueWithClosingBal(String uptoDate, String societyID,String buildingID)");
		} catch (Exception ex) {

			logger.error("Exception in getMemberDueWithClosingBal : " + ex);

		}
		
		return dueMemberList;
	}
	
	
	 public List getLedgerDueDetails(int societyID,String buildingID,String uptoDate)
		{
		
			try
			{
				logger.debug("Entry :  public List getLedgerDueDetails(String societyID,String aptID,String uptoDate)");
				
			    //eportList=reportDAO.getLedgerDueDetails(societyID, buildingID, uptoDate);
			   
			    for(int i=0;i<reportList.size();i++){
			    	RptTransactionVO rptTransactionVo= new RptTransactionVO();
			    	
			    	rptTransactionVo=(RptTransactionVO) reportList.get(i);
			    	
			    	if(rptTransactionVo.getDue().signum()==0){
			    		reportList.remove(i);
			    		--i;
			    	}
			    	
			    }
				   		    
			    logger.debug("Exit :  public List getLedgerDueDetails(String societyID,String aptID,String uptoDate)");
			}
			catch(NullPointerException e){
				logger.info("No Data available in getLedgerDueDetails");
			}
			catch(IllegalArgumentException ex)
			{
				reportList=null;
				
				logger.info("Exception in getLedgerDueDetails  : "+ex);
			}
			catch(Exception ex)
			{
				//ex.printStackTrace();
				logger.error("Exception in getLedgerDueDetails : "+ex);
			}
		
			return reportList;
			
		}
	  

	 public List getReceivableList(String uptoDate,int societyID,int buildingId)
		{   List dueMemberList=null;
			List convertedList=new ArrayList();
			try
			{
				logger.debug("Entry : public List getReceivableList(int intSocietyID)");
				
				dueMemberList=getMemberDueWithClosingBal(uptoDate,societyID,buildingId);
			    
				if(dueMemberList.size()>0){
				convertedList=ConvertReceivableList(societyID, dueMemberList);
				}
				
			    logger.debug("Exit : public List getReceivableList(int intSocietyID)");
			}
			catch(NullPointerException e){
				logger.info("No Data available in memberdue");
			}
			catch(IllegalArgumentException ex)
			{
				convertedList=null;
			
				logger.info("Exception in  : "+ex);
			}
			catch(Exception ex)
			{
				//ex.printStackTrace();
				logger.error("Exception in getMemberDue : "+ex);
			}
		
			return convertedList;
			
		}
	 
	 public List getReceivableListInBillFormat(int societyID,String fromDate,String uptoDate)
		{   List dueMemberList=null;
			
			try
			{
				logger.debug("Entry : public List getReceivableListInBillFormat(int societyID,String fromDate,String uptoDate)");
				
				dueMemberList=reportDAO.getReceivableListInBillFormat(societyID, fromDate, uptoDate);
			    
				
				
			    logger.debug("Exit : public List getReceivableListInBillFormat(int societyID,String fromDate,String uptoDate)");
			}
			catch(NullPointerException e){
				logger.info("No Data available in memberdue");
			}
			catch(IllegalArgumentException ex)
			{
				
			
				logger.info("Exception in  : "+ex);
			}
			catch(Exception ex)
			{
				//ex.printStackTrace();
				logger.error("Exception in getMemberDue : "+ex);
			}
		
			return dueMemberList;
			
		}
	 
	 // Formatting receivable list for API
	 public List ConvertReceivableList(int societyID,List memberList)
		{   List receivableList=new ArrayList();
			try
			{
				logger.debug("Entry : public List ConvertReceivableList(int societyID,List memberList)");
				
				for(int i=0;memberList.size()>i;i++){
					RptTransactionVO rptTxVO=(RptTransactionVO) memberList.get(i);
					MemberLedgerVO memberVO=new MemberLedgerVO();
					memberVO.setAptID(rptTxVO.getAptID());
					memberVO.setMemberID(rptTxVO.getMember_id());
					memberVO.setBuildingID(Integer.parseInt(rptTxVO.getBuildingId()));
					memberVO.setLedgerID(rptTxVO.getLedgerID());
					memberVO.setLedgerName(rptTxVO.getLedgerName());
					memberVO.setSocietyID(societyID);
					memberVO.setMobile(rptTxVO.getMobile());
					memberVO.setClosingBalance(rptTxVO.getClosingBalance());
					memberVO.setCrClosingBalance(rptTxVO.getCreditAmt());
					memberVO.setDrClosingBalance(rptTxVO.getDebitAmt());
					memberVO.setOpeningBalance(rptTxVO.getOpeningBalance());
					
					
					receivableList.add(memberVO);
					
				}
				
				
				
			    
			    logger.debug("Exit : public List ConvertReceivableList(int societyID,List memberList)");
			}
			catch(NullPointerException e){
				logger.info("No Data available in memberdue");
			}
			catch(IllegalArgumentException ex)
			{
				receivableList=null;
			
				logger.info("Exception in  : "+ex);
			}
			catch(Exception ex)
			{
				//ex.printStackTrace();
				logger.error("Exception in getMemberDue : "+ex);
			}
		
			return receivableList;
			
		}
	 
	public InvoiceService getInvoiceService() {
		return invoiceService;
	}

	public void setInvoiceService(InvoiceService invoiceService) {
		this.invoiceService = invoiceService;
	}

	public ReportDAO getReportDAO() {
		return reportDAO;
	}

	public void setReportDAO(ReportDAO reportDAO) {
		this.reportDAO = reportDAO;
	}

	public TransactionService getTransactionService() {
		return transactionService;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}
	
	
	
}
