package com.emanager.server.financialReports.domainObject;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.dashBoard.Services.DashBoardService;
import com.emanager.server.dashBoard.dataAccessObject.AccountStatusVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardAccountVO;
import com.emanager.server.financialReports.dataaccessObject.AccountStatusDAO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;

public class AccountingStatusDomain {

	private static final Logger log=Logger.getLogger(AccountingStatusDomain.class);
	DashBoardService dashBoardService;
	AccountStatusDAO accountStatusDAO;
	SocietyService societyService;
	DateUtility dateUtil=new DateUtility();
	
	
	public List getAccountingStatusReport(int userID, int appID,String fromDate, String toDate){
		log.debug("Entry : public List getAccountingStatusReport(int userID,List societyList)");
		List accountingStsList=new ArrayList();
		List societyList=new ArrayList();
		try {
			
			societyList=societyService.getSocietyListForUser(userID, appID);
			for(int i=0;societyList.size()>i;i++){
				SocietyVO societyVO=(SocietyVO) societyList.get(i);
				DashBoardAccountVO daVO=dashBoardService.getSocietyAccountStatus(societyVO.getSocietyID(),fromDate,toDate);
				daVO.setCommitteeMail(societyVO.getMailListA());
				daVO.setSocietyId(societyVO.getSocietyID()+"");
				DashBoardAccountVO dsVO=accountStatusDAO.getAccountsStatusReportDetails(societyVO.getSocietyID());
				
				if(dsVO.getSocietyId()!=null){
				daVO.setStatusPeriod(dsVO.getStatusPeriod());
				daVO.setStatusSendDate(dateUtil.getConvetedDateTimeStamptoDate(dsVO.getStatusSendDate()));
					
				}
				
				
				accountingStsList.add(daVO);
			}
		} catch (Exception e) {
			log.error("Exception : getAccountingStatusReport "+e);
		}
		
		
		log.debug("Exit : public List getAccountingStatusReport(int userID,List societyList)"+accountingStsList.size());
		return accountingStsList;
		
	}

	public int sendAccountStatusReport(AccountStatusVO acStsVO){
		log.debug("Entry : public int sendAccountStatusReport(AccountStatusVO acStsVO)");
		int success=0;
		
		success=accountStatusDAO.sendAccountStatusReport(acStsVO);
		
		
		log.debug("Exit : public int sendAccountStatusReport(AccountStatusVO acStsVO)");
		return success;
		
	}
	
	//==================Get pending transaction report for all orgs Created 06-05-21===========//
	
			public List getPendingApprovalTxReport(){
				log.debug("Entry : public int getPendingApprovalTxReport()");
				List societyList=new ArrayList<>();
				
				societyList=accountStatusDAO.getPendingApprovalTxReport();
				
				
				log.debug("Exit : public int getPendingApprovalTxReport()");
				return societyList;
				
			}


	public void setDashBoardService(DashBoardService dashBoardService) {
		this.dashBoardService = dashBoardService;
	}

	public void setAccountStatusDAO(AccountStatusDAO accountStatusDAO) {
		this.accountStatusDAO = accountStatusDAO;
	}

	/**
	 * @param societyService the societyService to set
	 */
	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}
	
	
}
