package com.emanager.server.financialReports.domainObject;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.financialReports.dataaccessObject.BudgetDAO;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.financialReports.valueObject.BudgetDetailsVO;
import com.emanager.server.financialReports.valueObject.ReportDetailsVO;
import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.projects.service.ProjectDetailsService;
import com.emanager.server.projects.valueObjects.ProjectDetailsVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;

public class BudgetDomain {
	
	BudgetDAO budgetDAO;
	ReportService reportService;
	SocietyService societyService;
	ProjectDetailsService projectDetailsService;
	
	Logger log=Logger.getLogger(BudgetDomain.class);
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	DateUtility dateUtil=new DateUtility();
	
	//-----Add Budget Details -------//
		public int addBudgetDetails(BudgetDetailsVO budgetVO){
			log.debug("Entry : public int addBudgetDetails(BudgetDetailsVO budgetVO)");
			int success=0;
			int budgetSuccess=0;
			try{
				
				success=budgetDAO.addBudgetDetails(budgetVO);		
				
						
			}catch(Exception e){
				log.error("Exception in adding budget details "+e);
			}
			
			
			log.debug("Exit : public int addBudgetDetails(BudgetDetailsVO budgetVO)");
			return success;
		}
		
		//-----Update Budget Details -------//
			public int updateBudgetDetails(BudgetDetailsVO budgetVO){
				log.debug("Entry : public int updateBudgetDetails(BudgetDetailsVO budgetVO)");
				int success=0;
				try{
					
					success=budgetDAO.updateBudgetDetails(budgetVO);			
					
				}catch(Exception e){
					log.error("Exception in updating budget details "+e);
				}
				
				
				log.debug("Exit : public int updateBudgetDetails(BudgetDetailsVO budgetVO)");
				return success;
			}
		
			//-----Delete Budget Details -------//
					public int deleteBudgetDetails(BudgetDetailsVO budgetVO){
						log.debug("Entry : public int deleteBudgetDetails(BudgetDetailsVO budgetVO)");
						int success=0;
						try{
							
							success=budgetDAO.deleteBudgetDetails(budgetVO);			
							
						}catch(Exception e){
							log.error("Exception in deleting budget details "+e);
						}
						
						
						log.debug("Exit : public int deleteBudgetDetails(BudgetDetailsVO budgetVO)");
						return success;
					}
			
					//-----Get Budget Details  List-------//
					public List getBudgetDetailsList(BudgetDetailsVO budgetVO){
						log.debug("Entry : public List getBudgetDetailsList(BudgetDetailsVO budgetVO)");
						List budgetList=null;
						try{
							
							budgetList=budgetDAO.getBudgetDetailsList(budgetVO);	
							
							if(budgetList.size()>0){
								for(int i=0;budgetList.size()>i;i++){
									BudgetDetailsVO budgetDetailsVO=(BudgetDetailsVO) budgetList.get(i);
									budgetDetailsVO.setItemList(getBudgetItemDetailsList(budgetDetailsVO));
								}
							}
							
							
						}catch(Exception e){
							log.error("Exception in getting budget details "+e);
						}
						
						
						log.debug("Exit : public int getBudgetDetailsList(BudgetDetailsVO budgetVO)");
						return budgetList;
					}
					
					
					//-----Get Budget Details  -------//
					public BudgetDetailsVO getBudgetDetails(BudgetDetailsVO budgetVO){
						log.debug("Entry : public BudgetDetailsVO getBudgetDetails(BudgetDetailsVO budgetVO)");
						BudgetDetailsVO budgetDetailsVO=new BudgetDetailsVO();
						try{
							
							budgetDetailsVO=budgetDAO.getBudgetDetails(budgetVO);			
							
						}catch(Exception e){
							log.error("Exception in getting budget details "+e);
						}
						
						
						log.debug("Exit : public BudgetDetailsVO getBudgetDetails(BudgetDetailsVO budgetVO)");
						return budgetDetailsVO;
					}
	
	
	
					//-----Add Budget Item Details -------//
					public int addBudgetItemDetails(BudgetDetailsVO budgetVO){
						log.debug("Entry : public int addBudgetItemDetails(BudgetDetailsVO budgetVO)");
						int success=0;
						try{
							
							success=budgetDAO.addBudgetItemDetails(budgetVO);			
							
						}catch(Exception e){
							log.error("Exception in adding budget details "+e);
						}
						
						
						log.debug("Exit : public int addBudgetItemDetails(BudgetDetailsVO budgetVO)");
						return success;
					}
					
					//-----Add bulk Budget Item Details -------//
					public int addBulkBudgetItemDetails(List budgetList){
						log.debug("Entry : public int addBulkBudgetItemDetails(List budgetList)");
						int success=0;
						int budgetSuccess=0;
						
						try{
							
							
							for(int i=0;budgetList.size()>i;i++){
								BudgetDetailsVO budgetDetailsVO=(BudgetDetailsVO) budgetList.get(i);
								budgetSuccess=budgetDAO.addBudgetItemDetails(budgetDetailsVO);
								if(budgetSuccess>0)
								success=success+1;
							}
							
							
						}catch(Exception e){
							log.error("Exception in adding budget details "+e);
						}
						
						
						log.debug("Exit : public int addBulkBudgetItemDetails(List budgetList)");
						return success;
					}
					
					//-----Update Budget Item Details -------//
					public int updateBudgetItemDetails(BudgetDetailsVO budgetVO){
						log.debug("Entry : public int updateBudgetItemDetails(BudgetDetailsVO budgetVO)");
						int delSuccess=0;
						int success=0;
						try{
							
							delSuccess=budgetDAO.deleteBudgetItemDetails(budgetVO);		
							
							if(delSuccess==0 || delSuccess>0){
								success=addBulkBudgetItems(budgetVO);
								
							}
							
						}catch(Exception e){
							log.error("Exception in updating budget details "+e);
						}
						
						
						log.debug("Exit : public int updateBudgetItemDetails(BudgetDetailsVO budgetVO)");
						return success;
					}
					
						//-----Delete Budget Item Details -------//
						public int deleteBudgetItemDetails(BudgetDetailsVO budgetVO){
									log.debug("Entry : public int deleteBudgetItemDetails(BudgetDetailsVO budgetVO)");
									int success=0;
									try{
										
										success=budgetDAO.deleteBudgetItemDetails(budgetVO);			
										
									}catch(Exception e){
										log.error("Exception in deleting budget details "+e);
									}
									
									
									log.debug("Exit : public int deleteBudgetItemDetails(BudgetDetailsVO budgetVO)");
									return success;
								}
						
								//-----Get Budget Item Details  List-------//
								public List getBudgetItemDetailsList(BudgetDetailsVO budgetVO){
									log.debug("Entry : public List getBudgetItemDetailsList(BudgetDetailsVO budgetVO)");
									List budgetList=null;
									try{
										
										budgetList=budgetDAO.getBudgetItemDetailsList(budgetVO);			
										
									}catch(Exception e){
										log.error("Exception in getting budget details "+e);
									}
									
									
									log.debug("Exit : public int getBudgetItemDetailsList(BudgetDetailsVO budgetVO)");
									return budgetList;
								}
								
								
								//-----Get Budget Details  -------//
								public BudgetDetailsVO getBudgetItemDetails(BudgetDetailsVO budgetVO){
									log.debug("Entry : public BudgetDetailsVO getBudgetItemDetails(BudgetDetailsVO budgetVO)");
									BudgetDetailsVO budgetDetailsVO=new BudgetDetailsVO();
									try{
										
										budgetDetailsVO=budgetDAO.getBudgetItemDetails(budgetVO);			
										
									}catch(Exception e){
										log.error("Exception in getting budget details "+e);
									}
									
									
									log.debug("Exit : public BudgetDetailsVO getBudgetItemDetails(BudgetDetailsVO budgetVO)");
									return budgetDetailsVO;
								}
								
								//-----Add bulk Budget Item Details -------//
								public int addBulkBudgetItems(BudgetDetailsVO budgetVO){
									log.debug("Entry : public int addBulkBudgetItems(BudgetDetailsVO budgetVO)");
									int success=0;
									try{
										
										
										List lineItemsList=budgetVO.getItemList();
										
										if(lineItemsList.size()>0){
											
											for(int i=0;lineItemsList.size()>i;i++){
												BudgetDetailsVO budgetDetailsVO=(BudgetDetailsVO) lineItemsList.get(i);
												budgetDetailsVO.setBudgetID(budgetVO.getBudgetID());
												budgetDetailsVO.setFromDate(budgetVO.getFromDate());
												List budgetList=new ArrayList();
												
												switch (budgetVO.getFrequency()) {
												case "M":
													budgetList=convertBudgetObjectIntoMonthly(budgetDetailsVO);
													break;
													
												case "Q":
													budgetList=convertBudgetObjectIntoQuarterly(budgetDetailsVO);
													break;
												
												case "H":
													budgetList=convertBudgetObjectIntoHalfYearly(budgetDetailsVO);
													break;
													
												case "Y":
													budgetList=convertBudgetObjectIntoYearly(budgetDetailsVO);
													break;
												default:
													break;
												}
												
												
												if(budgetList.size()>0){
													for(int j=0;budgetList.size()>j;j++){
														
														BudgetDetailsVO budgetEntryVO=(BudgetDetailsVO) budgetList.get(j);		
														
														success=budgetDAO.addBudgetItemDetails(budgetEntryVO);
														
														success=success+1;
													}
													
													
												}
												
											}
											
										}
										
										
												
										
									}catch(Exception e){
										log.error("Exception in public int addBulkBudgetItems(BudgetDetailsVO budgetVO) "+e);
									}
									
									
									log.debug("Exit : public int addBulkBudgetItems(BudgetDetailsVO budgetVO)");
									return success;
								}
								
								
		private List convertBudgetObjectIntoMonthly(BudgetDetailsVO budgetVO){
			List budgetItemList=new ArrayList();
			int year=0;
			try {
			java.util.Date newDate=sdf.parse(budgetVO.getFromDate());
										
			Calendar cal = Calendar.getInstance();
			cal.setTime(newDate);
									
			year  = cal.get(Calendar.YEAR);
			} catch (ParseException e) {
										 
			log.error("Exception : convertBudgetObjectIntoMonthly "+e);
			}
			BudgetDetailsVO budgetJanVO=new BudgetDetailsVO();
			if(budgetVO.getJanAmt()!=null){						
			budgetJanVO.setBudgetAmt(budgetVO.getJanAmt());
			}else budgetJanVO.setBudgetAmt(BigDecimal.ZERO);
			budgetJanVO.setFromDate(year+1+"-01-01");
			budgetJanVO.setToDate(dateUtil.getLastDayOfMonth(budgetJanVO.getFromDate()));
			budgetJanVO.setGroupID(budgetVO.getGroupID());
			budgetJanVO.setOrgID(budgetVO.getOrgID());
			budgetJanVO.setBudgetID(budgetVO.getBudgetID());
			budgetJanVO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetJanVO);
									
			BudgetDetailsVO budgetFebVO=new BudgetDetailsVO();
									
			if(budgetVO.getFebAmt()!=null){						
				budgetFebVO.setBudgetAmt(budgetVO.getFebAmt());
				}else budgetFebVO.setBudgetAmt(BigDecimal.ZERO);
			budgetFebVO.setFromDate(year+1+"-02-01");
			budgetFebVO.setToDate(dateUtil.getLastDayOfMonth(budgetFebVO.getFromDate()));
			budgetFebVO.setGroupID(budgetVO.getGroupID());
			budgetFebVO.setOrgID(budgetVO.getOrgID());
			budgetFebVO.setBudgetID(budgetVO.getBudgetID());
			budgetFebVO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetFebVO);
			
			BudgetDetailsVO budgetMarVO=new BudgetDetailsVO();
			
			if(budgetVO.getMarAmt()!=null){						
				budgetMarVO.setBudgetAmt(budgetVO.getMarAmt());
				}else budgetMarVO.setBudgetAmt(BigDecimal.ZERO);
			budgetMarVO.setBudgetAmt(budgetVO.getMarAmt());
			budgetMarVO.setFromDate(year+1+"-03-01");
			budgetMarVO.setToDate(dateUtil.getLastDayOfMonth(budgetMarVO.getFromDate()));
			budgetMarVO.setGroupID(budgetVO.getGroupID());
			budgetMarVO.setOrgID(budgetVO.getOrgID());
			budgetMarVO.setBudgetID(budgetVO.getBudgetID());
			budgetMarVO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetMarVO);
			
			BudgetDetailsVO budgetAprVO=new BudgetDetailsVO();
			
			if(budgetVO.getAprAmt()!=null){						
				budgetAprVO.setBudgetAmt(budgetVO.getAprAmt());
				}else budgetAprVO.setBudgetAmt(BigDecimal.ZERO);
			budgetAprVO.setFromDate(year+"-04-01");
			budgetAprVO.setToDate(dateUtil.getLastDayOfMonth(budgetAprVO.getFromDate()));
			budgetAprVO.setGroupID(budgetVO.getGroupID());
			budgetAprVO.setOrgID(budgetVO.getOrgID());
			budgetAprVO.setBudgetID(budgetVO.getBudgetID());
			budgetAprVO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetAprVO);
			
			BudgetDetailsVO budgetMayVO=new BudgetDetailsVO();
			
			if(budgetVO.getMayAmt()!=null){						
				budgetMayVO.setBudgetAmt(budgetVO.getMayAmt());
				}else budgetMayVO.setBudgetAmt(BigDecimal.ZERO);
			budgetMayVO.setFromDate(year+"-05-01");
			budgetMayVO.setToDate(dateUtil.getLastDayOfMonth(budgetMayVO.getFromDate()));
			budgetMayVO.setGroupID(budgetVO.getGroupID());
			budgetMayVO.setOrgID(budgetVO.getOrgID());
			budgetMayVO.setBudgetID(budgetVO.getBudgetID());
			budgetMayVO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetMayVO);
			
			BudgetDetailsVO budgetJunVO=new BudgetDetailsVO();
			
			if(budgetVO.getJunAmt()!=null){						
				budgetJunVO.setBudgetAmt(budgetVO.getJunAmt());
				}else budgetJunVO.setBudgetAmt(BigDecimal.ZERO);
			budgetJunVO.setFromDate(year+"-06-01");
			budgetJunVO.setToDate(dateUtil.getLastDayOfMonth(budgetJunVO.getFromDate()));
			budgetJunVO.setGroupID(budgetVO.getGroupID());
			budgetJunVO.setOrgID(budgetVO.getOrgID());
			budgetJunVO.setBudgetID(budgetVO.getBudgetID());
			budgetJunVO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetJunVO);
			
			BudgetDetailsVO budgetJulVO=new BudgetDetailsVO();
			
			if(budgetVO.getJulAmt()!=null){						
				budgetJulVO.setBudgetAmt(budgetVO.getJulAmt());
				}else budgetJulVO.setBudgetAmt(BigDecimal.ZERO);
			budgetJulVO.setFromDate(year+"-07-01");
			budgetJulVO.setToDate(dateUtil.getLastDayOfMonth(budgetJulVO.getFromDate()));
			budgetJulVO.setGroupID(budgetVO.getGroupID());
			budgetJulVO.setOrgID(budgetVO.getOrgID());
			budgetJulVO.setBudgetID(budgetVO.getBudgetID());
			budgetJulVO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetJulVO);
			
			BudgetDetailsVO budgetAugVO=new BudgetDetailsVO();
			
			if(budgetVO.getAugAmt()!=null){						
				budgetAugVO.setBudgetAmt(budgetVO.getAugAmt());
				}else budgetAugVO.setBudgetAmt(BigDecimal.ZERO);
			budgetAugVO.setFromDate(year+"-08-01");
			budgetAugVO.setToDate(dateUtil.getLastDayOfMonth(budgetAugVO.getFromDate()));
			budgetAugVO.setGroupID(budgetVO.getGroupID());
			budgetAugVO.setOrgID(budgetVO.getOrgID());
			budgetAugVO.setBudgetID(budgetVO.getBudgetID());
			budgetAugVO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetAugVO);
			
			BudgetDetailsVO budgetSepVO=new BudgetDetailsVO();
			
			if(budgetVO.getSeptAmt()!=null){						
				budgetSepVO.setBudgetAmt(budgetVO.getSeptAmt());
				}else budgetSepVO.setBudgetAmt(BigDecimal.ZERO);
			budgetSepVO.setFromDate(year+"-09-01");
			budgetSepVO.setToDate(dateUtil.getLastDayOfMonth(budgetSepVO.getFromDate()));
			budgetSepVO.setGroupID(budgetVO.getGroupID());
			budgetSepVO.setOrgID(budgetVO.getOrgID());
			budgetSepVO.setBudgetID(budgetVO.getBudgetID());
			budgetSepVO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetSepVO);
			
			BudgetDetailsVO budgetOctVO=new BudgetDetailsVO();
			
			if(budgetVO.getOctAmt()!=null){						
				budgetOctVO.setBudgetAmt(budgetVO.getOctAmt());
				}else budgetOctVO.setBudgetAmt(BigDecimal.ZERO);
			budgetOctVO.setFromDate(year+"-10-01");
			budgetOctVO.setToDate(dateUtil.getLastDayOfMonth(budgetOctVO.getFromDate()));
			budgetOctVO.setGroupID(budgetVO.getGroupID());
			budgetOctVO.setOrgID(budgetVO.getOrgID());
			budgetOctVO.setBudgetID(budgetVO.getBudgetID());
			budgetOctVO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetOctVO);
			
			BudgetDetailsVO budgetNovVO=new BudgetDetailsVO();
			
			if(budgetVO.getNovAmt()!=null){						
				budgetNovVO.setBudgetAmt(budgetVO.getNovAmt());
				}else budgetNovVO.setBudgetAmt(BigDecimal.ZERO);
			budgetNovVO.setFromDate(year+"-11-01");
			budgetNovVO.setToDate(dateUtil.getLastDayOfMonth(budgetNovVO.getFromDate()));
			budgetNovVO.setGroupID(budgetVO.getGroupID());
			budgetNovVO.setOrgID(budgetVO.getOrgID());
			budgetNovVO.setBudgetID(budgetVO.getBudgetID());
			budgetNovVO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetNovVO);
			
			BudgetDetailsVO budgetDecVO=new BudgetDetailsVO();
			
			if(budgetVO.getDecAmt()!=null){						
				budgetDecVO.setBudgetAmt(budgetVO.getDecAmt());
				}else budgetDecVO.setBudgetAmt(BigDecimal.ZERO);
			budgetDecVO.setFromDate(year+"-12-01");
			budgetDecVO.setToDate(dateUtil.getLastDayOfMonth(budgetDecVO.getFromDate()));
			budgetDecVO.setGroupID(budgetVO.getGroupID());
			budgetDecVO.setOrgID(budgetVO.getOrgID());
			budgetDecVO.setBudgetID(budgetVO.getBudgetID());
			budgetDecVO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetDecVO);
									
									
									
									
									
			return budgetItemList;
		}
		
		
		private List convertBudgetObjectIntoQuarterly(BudgetDetailsVO budgetVO){
			List budgetItemList=new ArrayList();
			int year=0;
			String tempDate="";
			try {
			java.util.Date newDate=sdf.parse(budgetVO.getFromDate());
										
			Calendar cal = Calendar.getInstance();
			cal.setTime(newDate);
									
			year  = cal.get(Calendar.YEAR);
			} catch (ParseException e) {
										 
			log.error("Exception : convertBudgetObjectIntoQuarterly "+e);
			}
			BudgetDetailsVO budgetQ1VO=new BudgetDetailsVO();
									
			if(budgetVO.getQ1Amt()!=null){						
				budgetQ1VO.setBudgetAmt(budgetVO.getQ1Amt());
				}else budgetQ1VO.setBudgetAmt(BigDecimal.ZERO);
			budgetQ1VO.setFromDate(year+"-04-01");
			tempDate=year+"-06-01";
			budgetQ1VO.setToDate(dateUtil.getLastDayOfMonth(tempDate));
			budgetQ1VO.setGroupID(budgetVO.getGroupID());
			budgetQ1VO.setOrgID(budgetVO.getOrgID());
			budgetQ1VO.setBudgetID(budgetVO.getBudgetID());
			budgetQ1VO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetQ1VO);
									
			BudgetDetailsVO budgetQ2VO=new BudgetDetailsVO();
			
			if(budgetVO.getQ2Amt()!=null){						
				budgetQ2VO.setBudgetAmt(budgetVO.getQ2Amt());
				}else budgetQ2VO.setBudgetAmt(BigDecimal.ZERO);
			budgetQ2VO.setFromDate(year+"-07-01");
			tempDate=year+"-09-01";
			budgetQ2VO.setToDate(dateUtil.getLastDayOfMonth(tempDate));
			budgetQ2VO.setGroupID(budgetVO.getGroupID());
			budgetQ2VO.setOrgID(budgetVO.getOrgID());
			budgetQ2VO.setBudgetID(budgetVO.getBudgetID());
			budgetQ2VO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetQ2VO);
			
			BudgetDetailsVO budgetQ3VO=new BudgetDetailsVO();
			
			if(budgetVO.getQ3Amt()!=null){						
				budgetQ3VO.setBudgetAmt(budgetVO.getQ3Amt());
				}else budgetQ3VO.setBudgetAmt(BigDecimal.ZERO);
			budgetQ3VO.setFromDate(year+"-10-01");
			tempDate=year+"-12-01";
			budgetQ3VO.setToDate(dateUtil.getLastDayOfMonth(tempDate));
			budgetQ3VO.setGroupID(budgetVO.getGroupID());
			budgetQ3VO.setOrgID(budgetVO.getOrgID());
			budgetQ3VO.setBudgetID(budgetVO.getBudgetID());
			budgetQ3VO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetQ3VO);
			
			BudgetDetailsVO budgetQ4VO=new BudgetDetailsVO();
			
			if(budgetVO.getQ4Amt()!=null){						
				budgetQ4VO.setBudgetAmt(budgetVO.getQ4Amt());
				}else budgetQ4VO.setBudgetAmt(BigDecimal.ZERO);
			budgetQ4VO.setFromDate(year+1+"-01-01");
			tempDate=year+1+"-03-01";
			budgetQ4VO.setToDate(dateUtil.getLastDayOfMonth(tempDate));
			budgetQ4VO.setGroupID(budgetVO.getGroupID());
			budgetQ4VO.setOrgID(budgetVO.getOrgID());
			budgetQ4VO.setBudgetID(budgetVO.getBudgetID());
			budgetQ4VO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetQ4VO);
			
					
									
			return budgetItemList;
		}
		
		
		private List convertBudgetObjectIntoHalfYearly(BudgetDetailsVO budgetVO){
			List budgetItemList=new ArrayList();
			int year=0;
			String tempDate="";
			try {
			java.util.Date newDate=sdf.parse(budgetVO.getFromDate());
										
			Calendar cal = Calendar.getInstance();
			cal.setTime(newDate);
									
			year  = cal.get(Calendar.YEAR);
			} catch (ParseException e) {
										 
			log.error("Exception : convertBudgetObjectIntoQuarterly "+e);
			}
			BudgetDetailsVO budgetH1VO=new BudgetDetailsVO();
									
			if(budgetVO.getH1Amt()!=null){						
				budgetH1VO.setBudgetAmt(budgetVO.getH1Amt());
				}else budgetH1VO.setBudgetAmt(BigDecimal.ZERO);
			budgetH1VO.setFromDate(year+"-04-01");
			tempDate=year+"-09-01";
			budgetH1VO.setToDate(dateUtil.getLastDayOfMonth(tempDate));
			budgetH1VO.setGroupID(budgetVO.getGroupID());
			budgetH1VO.setOrgID(budgetVO.getOrgID());
			budgetH1VO.setBudgetID(budgetVO.getBudgetID());
			budgetH1VO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetH1VO);
									
			BudgetDetailsVO budgetH2VO=new BudgetDetailsVO();
			
			if(budgetVO.getH2Amt()!=null){						
				budgetH2VO.setBudgetAmt(budgetVO.getH2Amt());
				}else budgetH2VO.setBudgetAmt(BigDecimal.ZERO);
			budgetH2VO.setFromDate(year+"-10-01");
			tempDate=year+1+"-03-01";
			budgetH2VO.setToDate(dateUtil.getLastDayOfMonth(tempDate));
			budgetH2VO.setGroupID(budgetVO.getGroupID());
			budgetH2VO.setOrgID(budgetVO.getOrgID());
			budgetH2VO.setBudgetID(budgetVO.getBudgetID());
			budgetH2VO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetH2VO);
			
			return budgetItemList;
		}
		
		private List convertBudgetObjectIntoYearly(BudgetDetailsVO budgetVO){
			List budgetItemList=new ArrayList();
			int year=0;
			String tempDate="";
			try {
			java.util.Date newDate=sdf.parse(budgetVO.getFromDate());
										
			Calendar cal = Calendar.getInstance();
			cal.setTime(newDate);
									
			year  = cal.get(Calendar.YEAR);
			} catch (ParseException e) {
										 
			log.error("Exception : convertBudgetObjectIntoQuarterly "+e);
			}
			BudgetDetailsVO budgetYVO=new BudgetDetailsVO();
			
			if(budgetVO.getBudgetAmt()!=null){						
				budgetYVO.setBudgetAmt(budgetVO.getBudgetAmt());
			}else budgetYVO.setBudgetAmt(BigDecimal.ZERO);
			
			budgetYVO.setFromDate(year+"-04-01");
			tempDate=year+1+"-03-01";
			budgetYVO.setToDate(dateUtil.getLastDayOfMonth(tempDate));
			budgetYVO.setGroupID(budgetVO.getGroupID());
			budgetYVO.setOrgID(budgetVO.getOrgID());
			budgetYVO.setBudgetID(budgetVO.getBudgetID());
			budgetYVO.setInOutType(budgetVO.getInOutType());
									
			budgetItemList.add(budgetYVO);
									
						
			return budgetItemList;
		}
								
					
		//===========Get Budgeted profitLoss report=========//
		public ReportVO getBudgetedProfitLossReport(int orgID,String fromDate,String toDate){
				ReportVO rptVO =new ReportVO();
				SocietyVO societyVO=new SocietyVO();
				String convertedFrmDate=null;
				String convertedToDate=null;
				DateUtility dateUtility=new DateUtility();
				List incomeList=new ArrayList();
				List expenseList=new ArrayList();
				List asstetsList=new ArrayList();
				List liailityList=new ArrayList();
				log.debug("Entry : public ReportVO getBudgetedProfitLossReport(int orgID,String fromDate,String toDate)");
									
				try {

				List budgetList=this.getBudgetReport(fromDate, toDate, orgID);
					
				List incomeDetailList=this.getDetailIncomeExpenditureReport(fromDate, toDate, orgID,1,"P");
				List expenseDetailList=this.getDetailIncomeExpenditureReport(fromDate, toDate, orgID,2,"P");
				List liabilityDetailList=this.getDetailIncomeExpenditureReport(fromDate, toDate, orgID,4,"P");
				List assetsDetailList=this.getDetailIncomeExpenditureReport(fromDate, toDate, orgID,3,"P");					    
				societyVO= societyService.getSocietyDetails("0", orgID);
				
				if(budgetList.size()>0){
					for(int i=0;budgetList.size()>i;i++){
						ReportDetailsVO budgetVO=(ReportDetailsVO) budgetList.get(i);
						
						if(budgetVO.getRootID()==1){
							incomeList.add(budgetVO);
						}else if(budgetVO.getRootID()==2){
							expenseList.add(budgetVO);
						}else if(budgetVO.getRootID()==3){
							asstetsList.add(budgetVO);
						}else if(budgetVO.getRootID()==4){
							liailityList.add(budgetVO);
						}
						
						
					}
					
					
				}
				
				
									
				rptVO.setSocietyVO(societyVO);
				log.debug("Organization Name: "+societyVO.getSocietyName()+" Address: "+societyVO.getAddress());
											
										   
				rptVO.setIncomeTxList(incomeList);
				rptVO.setExpenseTxList(expenseList);
				rptVO.setAssetsList(asstetsList);
				rptVO.setLiabilityList(liailityList);
				//rptVO.setAccountBalanceList(bankBalanceList);
				rptVO.setIncomeDetailsTxList(incomeDetailList);
				rptVO.setExpenseDetailsTxList(expenseDetailList);
				rptVO.setAssetsDetailsList(assetsDetailList);
				rptVO.setLiabilityDetailsList(liabilityDetailList);
				rptVO=this.getProfitLossBalance(rptVO,"PL");
											
											
				ReportVO dateVO=dateUtility.getReportDate(fromDate, toDate);
											
				rptVO.setRptFromDate(dateVO.getFromDate());
				rptVO.setRptUptoDate(dateVO.getUptDate());
				rptVO.setFromDate(fromDate);
				rptVO.setUptDate(toDate);
												
											
				log.debug("Converted From Date Format: "+rptVO.getFromDate()+" To date: "+ rptVO.getUptDate());
				} catch (Exception e) {
					log.error("Exception occured while preparing getProfitLossReport Report "+e);
				}
						
				log.debug("Exit : public ReportVO getBudgetedProfitLossReport(int orgID,String fromDate,String toDate)");
				return rptVO;
		}
								
	
								public List getBudgetReport(String fromDate, String toDate, int orgID) {
									List incExpList=new ArrayList();
									List incExpListDetails=new ArrayList<>();
									int rootID=0;
									String firstDate = null;;
									String lastDate=null;
									
									try {
										log.debug("Entry: public List getProfitLossCredit(String fromDate, String toDate, int orgID)");
									
										
										incExpList=budgetDAO.getIncomeExpenditureReport(fromDate, toDate, orgID,rootID,"PL");
										incExpListDetails=getDetailIncomeExpenditureReport(fromDate, toDate, orgID, rootID,"P");
										
										for(int i=0;incExpList.size()>i;i++){
											ReportDetailsVO incVO=(ReportDetailsVO) incExpList.get(i);
											List tempIncExpList=new ArrayList<>();
											for(int j=0;incExpListDetails.size()>j;j++){
												ReportDetailsVO incDtlVO=(ReportDetailsVO) incExpListDetails.get(j);
												
												if(incVO.getCategoryID()==incDtlVO.getCategoryID()){
													tempIncExpList.add(incDtlVO);
												}
												
												
											}
											if(incVO.getTotalAmount().compareTo(BigDecimal.ZERO)==0){
												incExpList.remove(i);
												--i;
											}
											if(tempIncExpList.size()>0){
												incVO.setObjectList(tempIncExpList);
											}
											
											
										}
										
									}
									catch (Exception ex) {
										log.error("Exception in public List getProfitLossCredit(String fromDate, String uptoDate, int SocietyID) : " + ex);
									}
									log.debug("Exit:public List public List getProfitLossCredit(String fromDate, String uptoDate, int SocietyID)");
								return incExpList;

								}
								
	
								public List getDetailIncomeExpenditureReport(String fromDate, String uptoDate, int SocietyID,int groupId,String type) {
									List incExpList=new ArrayList();
											
									try {




										log.debug("Entry:public List getDetailIncomeExpenditureReport(String fromDate, String uptoDate, int SocietyID,int groupId)");
									
										

										incExpList=budgetDAO.getIncomeExpenditureReport(fromDate, uptoDate, SocietyID,groupId,type);
										for(int i=0;incExpList.size()>i;i++){
											ReportDetailsVO incVO=(ReportDetailsVO) incExpList.get(i);
											if(incVO.getTotalAmount().compareTo(BigDecimal.ZERO)==0){
												incExpList.remove(i);
												--i;
											}
											
										}
										
									}
									catch (Exception ex) {
										log.error("Exception in getDetailIncomeExpenditureReport : " + ex);
									}
									log.debug("Exit:public List getDetailIncomeExpenditureReport(String fromDate, String uptoDate, int SocietyID,int groupId)");
								return incExpList;
								}
								
		
								
							
								public ReportVO getProfitLossBalance(ReportVO reportVO,String rptType){
									
									log.debug("Entry : public ReportVO getProfitLossBalance(ReportVO reportVO)");
										

										BigDecimal incomeTotal = BigDecimal.ZERO;
										BigDecimal expenseTotal =  BigDecimal.ZERO;
										BigDecimal incomeTempTotal = BigDecimal.ZERO;
										BigDecimal expenseTempTotal =  BigDecimal.ZERO;
										log.debug("Income : "+incomeTotal);
										try {
											 //total income
											 for(int i=0;reportVO.getIncomeTxList().size()>i;i++){
													ReportDetailsVO incomeVO=(ReportDetailsVO) reportVO.getIncomeTxList().get(i);
													incomeTotal=incomeTotal.add(incomeVO.getTotalAmount());
											 }


											 //total expense
											 for(int i=0;reportVO.getExpenseTxList().size()>i;i++){
												 ReportDetailsVO expenseVO=(ReportDetailsVO) reportVO.getExpenseTxList().get(i);
													expenseTotal=expenseTotal.add(expenseVO.getTotalAmount());
											 }
											log.debug("Total Income: "+incomeTotal+" Total Expense: "+expenseTotal);
											int resultCur=incomeTotal.compareTo(expenseTotal);
											if(resultCur==1){				
												expenseTempTotal=incomeTotal.subtract(expenseTotal);				
											}else{
												incomeTempTotal=expenseTotal.subtract(incomeTotal);				
											}
											
											if(!(incomeTempTotal.toString().equalsIgnoreCase("0"))){
												if(rptType.equalsIgnoreCase("IE")){
												reportVO.setExcessType("Excess of Expenditure Over Income");
												}else if(rptType.equalsIgnoreCase("PL")){
												reportVO.setExcessType("Net Loss before tax");
											}else if(rptType.equalsIgnoreCase("CF"))
													reportVO.setExcessType("Net cash out flow ");
												
												reportVO.setExcessAmount(incomeTempTotal);
												reportVO.setExpOverInc(incomeTempTotal);
												
											}else if(!(expenseTempTotal.toString().equalsIgnoreCase("0"))){
												if(rptType.equalsIgnoreCase("IE")){
													reportVO.setExcessType("Excess of Income Over Expenditure");
													}else if(rptType.equalsIgnoreCase("PL")){
													reportVO.setExcessType("Net Profit before tax");
											}else if(rptType.equalsIgnoreCase("CF"))
												reportVO.setExcessType("Net cash in flow");
												
												reportVO.setExcessAmount(expenseTempTotal);
												reportVO.setIncOverExp(expenseTempTotal);
											}
											reportVO.setIncomeTotal(incomeTotal);
											reportVO.setExpenseTotal(expenseTotal);

											log.debug("Expense: "+expenseTempTotal+" Income: "+incomeTempTotal+" Type: "+reportVO.getExcessType()+" Amount Expense: "+reportVO.getExcessAmount());
											
										} catch (Exception e) {
											log.error("Exception occured while preparing getProfitLossBalance Report "+e);
										}
											
										
										log.debug("Exit : public ReportVO getProfitLossBalance(ReportVO reportVO)");

										return reportVO;
									}
								
								
						public ReportVO getBudgetedReport(BudgetDetailsVO budgetVO){
							ReportVO rptVO =new ReportVO();
							log.debug("Entry : public ReportVO getBudgetedReport(int orgID,String fromDate,String toDate,int budgetID,String frequency)");
							
							
							switch (budgetVO.getFrequency()) {
							case "M":
								rptVO=getMonthWiseBudgetReport(budgetVO);
								break;
							
							case "Q":
								rptVO=getQuarterWiseBudgetReport(budgetVO);
								break;
								
							case "H":
								rptVO=getHalfYearWiseBudgetReport(budgetVO);
								break;
								
							case "Y":
								rptVO=getYearWiseBudgetReport(budgetVO);
								break;
							default:
								break;
							}
							
							
							log.debug("Exit : public ReportVO getBudgetedReport(int orgID,String fromDate,String toDate,int budgetID,String frequency)");

							return rptVO;
						}
								
								
								//================ Get Month wise Budget report=========//
								public ReportVO getMonthWiseBudgetReport(BudgetDetailsVO budgetsVO){
									ReportVO rptVO =new ReportVO();
									log.debug("Entry : public ReportVO getMonthWiseBudgetReport(int orgID,String fromDate,String toDate)");
									SocietyVO societyVO=societyService.getSocietyDetails(budgetsVO.getOrgID());
									List incExpList=new ArrayList();
									List incExpDetailsList=new ArrayList();
									List incomeList=new ArrayList();
									List expenseList=new ArrayList();
									List asstetsList=new ArrayList();
									List liailityList=new ArrayList();
									DateUtility dateUtility=new DateUtility();
									List incomeDetailsList=new ArrayList();
									List expenseDetailsList=new ArrayList();
									List asstetsDetailsList=new ArrayList();
									List liailityDetailsList=new ArrayList();
									if(budgetsVO.getBudgetType().equalsIgnoreCase("PL")){
										incExpDetailsList=budgetDAO.getMonthWiseBudgetReport(budgetsVO.getFromDate(), budgetsVO.getToDate(),budgetsVO.getBudgetID(), budgetsVO.getOrgID(), "P");
									}else if(budgetsVO.getBudgetType().equalsIgnoreCase("PC")){
										List categoryList=projectDetailsService.getCostCeneterCategoryLinkingList(budgetsVO.getCcCategoryID());
										incExpDetailsList=budgetDAO.getMonthWiseBudgetReportWithCCCategory(budgetsVO.getFromDate(), budgetsVO.getToDate(),budgetsVO.getBudgetID(), budgetsVO.getOrgID(), "P",categoryList, budgetsVO.getProjectAcronyms(),budgetsVO.getBudgetType());
									}else if(budgetsVO.getBudgetType().equalsIgnoreCase("CC")){
										if(budgetsVO.getCcCategoryID()>0){
											List categoryList=projectDetailsService.getCostCeneterCategoryLinkingList(budgetsVO.getCcCategoryID());
											
											incExpDetailsList=budgetDAO.getMonthWiseBudgetReportWithCCCategory(budgetsVO.getFromDate(), budgetsVO.getToDate(),budgetsVO.getBudgetID(), budgetsVO.getOrgID(), "P",categoryList, budgetsVO.getProjectAcronyms(),budgetsVO.getBudgetType());
									}
									}
									BudgetDetailsVO budgetVO=new BudgetDetailsVO();
									BudgetDetailsVO budgetOutVO=new BudgetDetailsVO();
									List tempIncExpList=new ArrayList<>();
									List tempExList=new ArrayList<>();
									for(int j=0;incExpDetailsList.size()>j;j++){
										
										BudgetDetailsVO incDtlVO=(BudgetDetailsVO) incExpDetailsList.get(j);
										
									
										if((incDtlVO.getInOutType()!=null)&&(Integer.parseInt(incDtlVO.getInOutType())==0)){
											tempIncExpList.add(incDtlVO);
										}else if((incDtlVO.getInOutType()!=null)&&(Integer.parseInt(incDtlVO.getInOutType())==1)){
											tempExList.add(incDtlVO);
										}
										
									}
									budgetVO.setItemList(tempIncExpList);
									budgetOutVO.setItemList(tempExList);
									
									
									
										incomeList.add(budgetVO);
									
										expenseList.add(budgetOutVO);
									
									rptVO.setSocietyVO(societyVO);
									log.debug("Organization Name: "+societyVO.getSocietyName()+" Address: "+societyVO.getAddress());
																
															   
									rptVO.setIncomeTxList(incomeList);
									rptVO.setExpenseTxList(expenseList);
									rptVO.setAssetsList(asstetsList);
									rptVO.setLiabilityList(liailityList);
									//rptVO.setAccountBalanceList(bankBalanceList);
									rptVO.setIncomeDetailsTxList(incomeDetailsList);
									rptVO.setExpenseDetailsTxList(expenseDetailsList);
									rptVO.setAssetsDetailsList(asstetsDetailsList);
									rptVO.setLiabilityDetailsList(liailityDetailsList);
									
																
																
									ReportVO dateVO=dateUtility.getReportDate(budgetsVO.getFromDate(), budgetsVO.getToDate());
																
									rptVO.setRptFromDate(dateVO.getFromDate());
									rptVO.setRptUptoDate(dateVO.getUptDate());
									rptVO.setFromDate(budgetsVO.getFromDate());
									rptVO.setUptDate(budgetsVO.getToDate());
									
									log.debug("Exit : public ReportVO getMonthWiseBudgetReport(int orgID,String fromDate,String toDate)");
									return rptVO;
								}
								
								
								//================ Get Quarter wise Budget report=========//
								public ReportVO getQuarterWiseBudgetReport(BudgetDetailsVO budgetsVO){
									ReportVO rptVO =new ReportVO();
									log.debug("Entry : public ReportVO getQuarterWiseBudgetReport(int orgID,String fromDate,String toDate)");
									SocietyVO societyVO=societyService.getSocietyDetails(budgetsVO.getOrgID());
									List incExpList=new ArrayList();
									List incExpDetailsList=new ArrayList();
									List incomeList=new ArrayList();
									List expenseList=new ArrayList();
									List asstetsList=new ArrayList();
									List liailityList=new ArrayList();
									DateUtility dateUtility=new DateUtility();
									List incomeDetailsList=new ArrayList();
									List expenseDetailsList=new ArrayList();
									List asstetsDetailsList=new ArrayList();
									List liailityDetailsList=new ArrayList();
									
									if(budgetsVO.getBudgetType().equalsIgnoreCase("PL")){
										incExpDetailsList=budgetDAO.getQuarterWiseBudgetReport(budgetsVO.getFromDate(), budgetsVO.getToDate(),budgetsVO.getBudgetID(), budgetsVO.getOrgID(), "P");
									}else if(budgetsVO.getBudgetType().equalsIgnoreCase("PC")){
										List categoryList=projectDetailsService.getCostCeneterCategoryLinkingList(budgetsVO.getCcCategoryID());
										incExpDetailsList=budgetDAO.getQuarterWiseBudgetReportWithCCCategory(budgetsVO.getFromDate(), budgetsVO.getToDate(),budgetsVO.getBudgetID(), budgetsVO.getOrgID(), "P",categoryList, budgetsVO.getProjectAcronyms(),budgetsVO.getBudgetType());
									}else if(budgetsVO.getBudgetType().equalsIgnoreCase("CC")){
										if(budgetsVO.getCcCategoryID()>0){
											List categoryList=projectDetailsService.getCostCeneterCategoryLinkingList(budgetsVO.getCcCategoryID());
											
											incExpDetailsList=budgetDAO.getQuarterWiseBudgetReportWithCCCategory(budgetsVO.getFromDate(), budgetsVO.getToDate(),budgetsVO.getBudgetID(), budgetsVO.getOrgID(), "P",categoryList, budgetsVO.getProjectAcronyms(),budgetsVO.getBudgetType());
									}
									}
									BudgetDetailsVO budgetVO=new BudgetDetailsVO();
									BudgetDetailsVO budgetOutVO=new BudgetDetailsVO();
									List tempIncExpList=new ArrayList<>();
									List tempExList=new ArrayList<>();
									for(int j=0;incExpDetailsList.size()>j;j++){
										
										BudgetDetailsVO incDtlVO=(BudgetDetailsVO) incExpDetailsList.get(j);
										
									
										if((incDtlVO.getInOutType()!=null)&&(Integer.parseInt(incDtlVO.getInOutType())==0)){
											tempIncExpList.add(incDtlVO);
										}else if((incDtlVO.getInOutType()!=null)&&(Integer.parseInt(incDtlVO.getInOutType())==1)){
											tempExList.add(incDtlVO);
										}
										
									}
									budgetVO.setItemList(tempIncExpList);
									budgetOutVO.setItemList(tempExList);
									
									
									
										incomeList.add(budgetVO);
									
										expenseList.add(budgetOutVO);
									
									rptVO.setSocietyVO(societyVO);
									log.debug("Organization Name: "+societyVO.getSocietyName()+" Address: "+societyVO.getAddress());
																
															   
									rptVO.setIncomeTxList(incomeList);
									rptVO.setExpenseTxList(expenseList);
									rptVO.setAssetsList(asstetsList);
									rptVO.setLiabilityList(liailityList);
									//rptVO.setAccountBalanceList(bankBalanceList);
									rptVO.setIncomeDetailsTxList(incomeDetailsList);
									rptVO.setExpenseDetailsTxList(expenseDetailsList);
									rptVO.setAssetsDetailsList(asstetsDetailsList);
									rptVO.setLiabilityDetailsList(liailityDetailsList);
									
																
																
									ReportVO dateVO=dateUtility.getReportDate(budgetsVO.getFromDate(), budgetsVO.getToDate());
																
									rptVO.setRptFromDate(dateVO.getFromDate());
									rptVO.setRptUptoDate(dateVO.getUptDate());
									rptVO.setFromDate(budgetsVO.getFromDate());
									rptVO.setUptDate(budgetsVO.getToDate());
									
									log.debug("Exit : public ReportVO getQuarterWiseBudgetReport(int orgID,String fromDate,String toDate)");
									return rptVO;
								}
								
								
				//================ Get Quarter wise Budget report=========//
				public ReportVO getHalfYearWiseBudgetReport(BudgetDetailsVO budgetsVO){
					ReportVO rptVO =new ReportVO();
									log.debug("Entry : public ReportVO getHalfYearWiseBudgetReport(int orgID,String fromDate,String toDate)");
									SocietyVO societyVO=societyService.getSocietyDetails(budgetsVO.getOrgID());
									List incExpList=new ArrayList();
									List incExpDetailsList=new ArrayList();
									List incomeList=new ArrayList();
									List expenseList=new ArrayList();
									List asstetsList=new ArrayList();
									List liailityList=new ArrayList();
									DateUtility dateUtility=new DateUtility();
									List incomeDetailsList=new ArrayList();
									List expenseDetailsList=new ArrayList();
									List asstetsDetailsList=new ArrayList();
									List liailityDetailsList=new ArrayList();
									
									if(budgetsVO.getBudgetType().equalsIgnoreCase("PL")){
										incExpDetailsList=budgetDAO.getHalfYearWiseBudgetReport(budgetsVO.getFromDate(), budgetsVO.getToDate(),budgetsVO.getBudgetID(), budgetsVO.getOrgID(), "P");
									}else if(budgetsVO.getBudgetType().equalsIgnoreCase("PC")){
										List categoryList=projectDetailsService.getCostCeneterCategoryLinkingList(budgetsVO.getCcCategoryID());
										incExpDetailsList=budgetDAO.getHalfYearWiseBudgetReportWithCCCategory(budgetsVO.getFromDate(), budgetsVO.getToDate(),budgetsVO.getBudgetID(), budgetsVO.getOrgID(), "P",categoryList, budgetsVO.getProjectAcronyms(),budgetsVO.getBudgetType());
									}else if(budgetsVO.getBudgetType().equalsIgnoreCase("CC")){
										if(budgetsVO.getCcCategoryID()>0){
											List categoryList=projectDetailsService.getCostCeneterCategoryLinkingList(budgetsVO.getCcCategoryID());
											
											incExpDetailsList=budgetDAO.getHalfYearWiseBudgetReportWithCCCategory(budgetsVO.getFromDate(), budgetsVO.getToDate(),budgetsVO.getBudgetID(), budgetsVO.getOrgID(), "P",categoryList, budgetsVO.getProjectAcronyms(),budgetsVO.getBudgetType());
									}
									}
									BudgetDetailsVO budgetVO=new BudgetDetailsVO();
									BudgetDetailsVO budgetOutVO=new BudgetDetailsVO();
									List tempIncExpList=new ArrayList<>();
									List tempExList=new ArrayList<>();
									for(int j=0;incExpDetailsList.size()>j;j++){
										
										BudgetDetailsVO incDtlVO=(BudgetDetailsVO) incExpDetailsList.get(j);
										
									
										if((incDtlVO.getInOutType()!=null)&&(Integer.parseInt(incDtlVO.getInOutType())==0)){
											tempIncExpList.add(incDtlVO);
										}else if((incDtlVO.getInOutType()!=null)&&(Integer.parseInt(incDtlVO.getInOutType())==1)){
											tempExList.add(incDtlVO);
										}
										
									}
									budgetVO.setItemList(tempIncExpList);
									budgetOutVO.setItemList(tempExList);
									
									
									
										incomeList.add(budgetVO);
									
										expenseList.add(budgetOutVO);
									
									rptVO.setSocietyVO(societyVO);
									log.debug("Organization Name: "+societyVO.getSocietyName()+" Address: "+societyVO.getAddress());
																
															   
									rptVO.setIncomeTxList(incomeList);
									rptVO.setExpenseTxList(expenseList);
									rptVO.setAssetsList(asstetsList);
									rptVO.setLiabilityList(liailityList);
									//rptVO.setAccountBalanceList(bankBalanceList);
									rptVO.setIncomeDetailsTxList(incomeDetailsList);
									rptVO.setExpenseDetailsTxList(expenseDetailsList);
									rptVO.setAssetsDetailsList(asstetsDetailsList);
									rptVO.setLiabilityDetailsList(liailityDetailsList);
									
																
																
									ReportVO dateVO=dateUtility.getReportDate(budgetsVO.getFromDate(), budgetsVO.getToDate());
																
									rptVO.setRptFromDate(dateVO.getFromDate());
									rptVO.setRptUptoDate(dateVO.getUptDate());
									rptVO.setFromDate(budgetsVO.getFromDate());
									rptVO.setUptDate(budgetsVO.getToDate());
									
									log.debug("Exit : public ReportVO getHalfYearWiseBudgetReport(int orgID,String fromDate,String toDate)");
									return rptVO;
		}
								
				
				//================ Get Year wise Budget report=========//
				public ReportVO getYearWiseBudgetReport(BudgetDetailsVO budgetsVO){
					ReportVO rptVO =new ReportVO();
									log.debug("Entry : public ReportVO getYearWiseBudgetReport(int orgID,String fromDate,String toDate)");
									SocietyVO societyVO=societyService.getSocietyDetails(budgetsVO.getOrgID());
									List incExpList=new ArrayList();
									List incExpDetailsList=new ArrayList();
									List incomeList=new ArrayList();
									List expenseList=new ArrayList();
									List asstetsList=new ArrayList();
									List liailityList=new ArrayList();
									DateUtility dateUtility=new DateUtility();
									List incomeDetailsList=new ArrayList();
									List expenseDetailsList=new ArrayList();
									List asstetsDetailsList=new ArrayList();
									List liailityDetailsList=new ArrayList();
									
									if(budgetsVO.getBudgetType().equalsIgnoreCase("PL")){
										incExpDetailsList=budgetDAO.getYearWiseBudgetReport(budgetsVO.getFromDate(), budgetsVO.getToDate(),budgetsVO.getBudgetID(), budgetsVO.getOrgID(), "P");
									}else if(budgetsVO.getBudgetType().equalsIgnoreCase("PC")){
										List categoryList=projectDetailsService.getCostCeneterCategoryLinkingList(budgetsVO.getCcCategoryID());
										incExpDetailsList=budgetDAO.getYearWiseBudgetReportWithCCCategory(budgetsVO.getFromDate(), budgetsVO.getToDate(),budgetsVO.getBudgetID(), budgetsVO.getOrgID(), "P",categoryList, budgetsVO.getProjectAcronyms(),budgetsVO.getBudgetType());
									}else if(budgetsVO.getBudgetType().equalsIgnoreCase("CC")){
										if(budgetsVO.getCcCategoryID()>0){
											List categoryList=projectDetailsService.getCostCeneterCategoryLinkingList(budgetsVO.getCcCategoryID());
											
											incExpDetailsList=budgetDAO.getYearWiseBudgetReportWithCCCategory(budgetsVO.getFromDate(), budgetsVO.getToDate(),budgetsVO.getBudgetID(), budgetsVO.getOrgID(), "P",categoryList, budgetsVO.getProjectAcronyms(),budgetsVO.getBudgetType());
									}
									}
										BudgetDetailsVO budgetVO=new BudgetDetailsVO();
										BudgetDetailsVO budgetOutVO=new BudgetDetailsVO();
										List tempIncExpList=new ArrayList<>();
										List tempExList=new ArrayList<>();
										for(int j=0;incExpDetailsList.size()>j;j++){
											
											BudgetDetailsVO incDtlVO=(BudgetDetailsVO) incExpDetailsList.get(j);
											
										
											if((incDtlVO.getInOutType()!=null)&&(Integer.parseInt(incDtlVO.getInOutType())==0)){
												tempIncExpList.add(incDtlVO);
											}else if((incDtlVO.getInOutType()!=null)&&(Integer.parseInt(incDtlVO.getInOutType())==1)){
												tempExList.add(incDtlVO);
											}
											
										}
										budgetVO.setItemList(tempIncExpList);
										budgetOutVO.setItemList(tempExList);
										
										
										
											incomeList.add(budgetVO);
										
											expenseList.add(budgetOutVO);
									
									
									
									rptVO.setSocietyVO(societyVO);
									log.debug("Organization Name: "+societyVO.getSocietyName()+" Address: "+societyVO.getAddress());
																
															   
									rptVO.setIncomeTxList(incomeList);
									rptVO.setExpenseTxList(expenseList);
									rptVO.setAssetsList(asstetsList);
									rptVO.setLiabilityList(liailityList);
									//rptVO.setAccountBalanceList(bankBalanceList);
									rptVO.setIncomeDetailsTxList(incomeDetailsList);
									rptVO.setExpenseDetailsTxList(expenseDetailsList);
									rptVO.setAssetsDetailsList(asstetsDetailsList);
									rptVO.setLiabilityDetailsList(liailityDetailsList);
									
																
																
									ReportVO dateVO=dateUtility.getReportDate(budgetsVO.getFromDate(), budgetsVO.getToDate());
																
									rptVO.setRptFromDate(dateVO.getFromDate());
									rptVO.setRptUptoDate(dateVO.getUptDate());
									rptVO.setFromDate(budgetsVO.getFromDate());
									rptVO.setUptDate(budgetsVO.getToDate());
									
									log.debug("Exit : public ReportVO getYearWiseBudgetReport(int orgID,String fromDate,String toDate)");
									return rptVO;
					}
								
				
						
				
				//================ Get Budget details report for group =========//
				public BudgetDetailsVO getBudgetDetailsForGroup(BudgetDetailsVO budgetVO){
					BudgetDetailsVO rptVO =new BudgetDetailsVO();
					BudgetDetailsVO bgdtVO=new BudgetDetailsVO();
					log.debug("Entry : public ReportVO getBudgetDetailsForGroup(int orgID,String fromDate,String toDate)");
					List budgetList=new ArrayList();
					
					budgetList=budgetDAO.getBudgetDetailsForGroup(budgetVO);
					if(budgetList.size()>0){
						rptVO=(BudgetDetailsVO) budgetList.get(0);
						
						rptVO.setGroupID(budgetVO.getGroupID());
						
						bgdtVO=getBudgetedReports(rptVO);
					}
					
					
					
					log.debug("Exit : public ReportVO getBudgetDetailsForGroup(int orgID,String fromDate,String toDate)");
					return bgdtVO;
				}
				
				//================ Get Budget report for group =========//
				public BudgetDetailsVO getBudgetedReports(BudgetDetailsVO budgetsVO){
					BudgetDetailsVO rptVO =new BudgetDetailsVO();
					log.debug("Entry : public ReportVO getBudgetedBudgetReportForGroup(int orgID,String fromDate,String toDate)");
					SocietyVO societyVO=societyService.getSocietyDetails(budgetsVO.getOrgID());
					List budgetList=new ArrayList();
					
					if(budgetsVO.getBudgetType().equalsIgnoreCase("PL")){
						budgetList=budgetDAO.getYearWiseBudgetReportForGroup(budgetsVO.getFromDate(), budgetsVO.getToDate(),budgetsVO.getBudgetID(), budgetsVO.getOrgID(), "P",budgetsVO.getGroupID());
					}else if(budgetsVO.getBudgetType().equalsIgnoreCase("PC")){
						List categoryList=projectDetailsService.getCostCeneterCategoryLinkingList(budgetsVO.getCcCategoryID());
						budgetList=budgetDAO.getYearWiseBudgetReportWithCCCategoryForGroup(budgetsVO.getFromDate(), budgetsVO.getToDate(),budgetsVO.getBudgetID(), budgetsVO.getOrgID(), "P",categoryList, budgetsVO.getProjectAcronyms(),budgetsVO.getBudgetType(),budgetsVO.getGroupID());
					}else if(budgetsVO.getBudgetType().equalsIgnoreCase("CC")){
						if(budgetsVO.getCcCategoryID()>0){
							List categoryList=projectDetailsService.getCostCeneterCategoryLinkingList(budgetsVO.getCcCategoryID());
							
							budgetList=budgetDAO.getYearWiseBudgetReportWithCCCategoryForGroup(budgetsVO.getFromDate(), budgetsVO.getToDate(),budgetsVO.getBudgetID(), budgetsVO.getOrgID(), "P",categoryList, budgetsVO.getProjectAcronyms(),budgetsVO.getBudgetType(),budgetsVO.getGroupID());
					}
					}
					
					budgetsVO.setItemList(budgetList);
					
					
					log.debug("Exit : public ReportVO getBudgetedBudgetReportForGroup(int orgID,String fromDate,String toDate)");
					return budgetsVO;
				}
				
								//================ Get Budgeted project/cost center report=========//
								public List getBudgetedProjectListWithDetails(int orgID,String fromDate,String toDate){
									log.debug("Entry :  public List getBudgetedProjectListWithDetails(int orgID,String fromDate,String toDate)");
									List projectList=null;
									
									projectList=budgetDAO.getBudgetedProjectListWithDetails(orgID,fromDate,toDate);
									
									if(projectList.size()>0){
										
										for(int i=0;projectList.size()>i;i++){
											BigDecimal incomeTotal=BigDecimal.ZERO;
											BigDecimal expenseTotal=BigDecimal.ZERO;
											BigDecimal totalAmount=BigDecimal.ZERO;
											ProjectDetailsVO projectVO=(ProjectDetailsVO) projectList.get(i);
											List objectList=budgetDAO.getLedgersListInProject(orgID, projectVO.getProjectID(), fromDate, toDate);
											for(int j=0;objectList.size()>j;j++){
												ProjectDetailsVO projectDetVO=(ProjectDetailsVO) objectList.get(j);
												if((projectDetVO.getRootID()==1)||(projectDetVO.getRootID()==4)){
													incomeTotal=incomeTotal.add(projectDetVO.getActualAmount());
												}else if((projectDetVO.getRootID()==2)||(projectDetVO.getRootID()==3)){
													expenseTotal=expenseTotal.add(projectDetVO.getActualAmount());
												}
												
											}
												totalAmount=incomeTotal.subtract(expenseTotal);
												projectVO.setTotalAmount(totalAmount);
												projectVO.setIncomeTotal(incomeTotal);
												projectVO.setExpenseTotal(expenseTotal);
							     				projectVO.setObjectList(objectList);
										}
										
										
									}
									
									
									log.debug("Exit : public List getBudgetedProjectListWithDetails(int orgID,String fromDate,String toDate)");
									return projectList;
									
								}
								
								
								//================ Get Budgeted project/cost center report=========//
								public ProjectDetailsVO getBudgetedProjectWithDetails(int orgID,String fromDate,String toDate,String projectAcronyms){
									log.debug("Entry :  public ProjectDetailsVO getBudgetedProjectWithDetails(int orgID,String fromDate,String toDate,String projectAcronyms)");
									
									
									BigDecimal incomeTotal=BigDecimal.ZERO;
									BigDecimal expenseTotal=BigDecimal.ZERO;
									BigDecimal liabilityTotal=BigDecimal.ZERO;
									BigDecimal assetsTotal=BigDecimal.ZERO;
									BigDecimal totalAmount=BigDecimal.ZERO;
									BigDecimal IncExpTotal=BigDecimal.ZERO;
									BigDecimal liaAssetTotal=BigDecimal.ZERO;
									ProjectDetailsVO projectVO=new ProjectDetailsVO();
									List objectList=budgetDAO.getLedgersListInProjectTagWise(orgID, projectAcronyms, fromDate, toDate,0,1);
									for(int j=0;objectList.size()>j;j++){
										ProjectDetailsVO projectDetVO=(ProjectDetailsVO) objectList.get(j);
										if(projectDetVO.getRootID()==1){
											incomeTotal=incomeTotal.add(projectDetVO.getActualAmount());
										}else if(projectDetVO.getRootID()==2){
											expenseTotal=expenseTotal.add(projectDetVO.getActualAmount());
										}else if(projectDetVO.getRootID()==3){
											assetsTotal=assetsTotal.add(projectDetVO.getActualAmount());
										}else if(projectDetVO.getRootID()==4){
											liabilityTotal=liabilityTotal.add(projectDetVO.getActualAmount());
										}
									List ledgerList=budgetDAO.getLedgersListInProjectTagWise(orgID, projectAcronyms, fromDate, toDate, projectDetVO.getGroupID(), 0);
									projectDetVO.setObjectList(ledgerList);
										
									}
									IncExpTotal=incomeTotal.subtract(expenseTotal);
									liaAssetTotal=assetsTotal.subtract(liabilityTotal);
									totalAmount=IncExpTotal.add(liaAssetTotal);
									projectVO.setTotalAmount(totalAmount);
									projectVO.setIncomeTotal(incomeTotal);
									projectVO.setExpenseTotal(expenseTotal);
									projectVO.setAssetsTotal(assetsTotal);
									projectVO.setLiabilityTotal(liabilityTotal);
				     				projectVO.setObjectList(objectList);
										
										
									
									
									
									log.debug("Exit : public ProjectDetailsVO getBudgetedProjectWithDetails(int orgID,String fromDate,String toDate,String projectAcronyms)");
									return projectVO;
									
								}
								
								public List getBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate,String frequency){
									ReportVO rptVO =new ReportVO();
									log.debug("Entry : public List getBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate,String frequency)");
									List lineItemList=new ArrayList();
									
									switch (frequency) {
									case "M":
										lineItemList=budgetDAO.getMonthWiseBudgetLineItemList(orgID,budgetID,groupID,fromDate, toDate);
										break;
									
									case "Q":
										lineItemList=budgetDAO.getQuarterWiseBudgetLineItemList(orgID,budgetID,groupID,fromDate, toDate);
										break;
										
									case "H":
										lineItemList=budgetDAO.getHalfYearWiseBudgetLineItemList(orgID,budgetID,groupID,fromDate, toDate);
										break;
										
									case "Y":
										lineItemList=budgetDAO.getYearWiseBudgetLineItemList(orgID,budgetID,groupID,fromDate, toDate);
										break;
									default:
										break;
									}
									
									
									log.debug("Exit : public List getBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate,String frequency)");

									return lineItemList;
								}
								
	/**
	 * @param budgetDAO the budgetDAO to set
	 */
	public void setBudgetDAO(BudgetDAO budgetDAO) {
		this.budgetDAO = budgetDAO;
	}
	/**
	 * @param reportService the reportService to set
	 */
	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}
	/**
	 * @param societyService the societyService to set
	 */
	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}
	/**
	 * @param projectDetailsService the projectDetailsService to set
	 */
	public void setProjectDetailsService(ProjectDetailsService projectDetailsService) {
		this.projectDetailsService = projectDetailsService;
	}

	
	
	
	
	
	
	
	
}
