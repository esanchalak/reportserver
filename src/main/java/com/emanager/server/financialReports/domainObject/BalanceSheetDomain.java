package com.emanager.server.financialReports.domainObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

//import com.caucho.xtpdoc.Summary;
import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.financialReports.dataaccessObject.ReportDAO;
import com.emanager.server.financialReports.services.TrialBalanceService;
import com.emanager.server.financialReports.valueObject.ReportDetailsVO;
import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.financialReports.valueObject.RptMonthYearVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;

public class BalanceSheetDomain {
	SocietyService societyService;
	LedgerService ledgerService;
	TrialBalanceService trialBalanceService;
	ReportDAO reportDAO;
	Logger logger=Logger.getLogger(BalanceSheetDomain.class);
	DateUtility dateUtil=new DateUtility();
	
	
	 public ReportVO getBalanceSheet(int societyID,String fromDate,String uptoDate){
			ReportVO balanceSheetVO =new ReportVO();
			logger.debug("Entry : public ReportVO getBlanceSheet(int societyID,String fromDate,String uptoDate)" +fromDate+"  "+uptoDate);
			List balanceSheetList=new ArrayList();
			List balanceSheetLedgerDetailList=new ArrayList();
			List liabilityList=new ArrayList();
			List assetsList=new ArrayList();
		
			try {
								
				SocietyVO societyVo=societyService.getSocietyDetails("0", societyID);
				AccountHeadVO profitLossVO=ledgerService.getProffitLossBalance(societyID, fromDate, uptoDate,"bs",0,"S");
				balanceSheetList=reportDAO.getTreeWiseBalanceSheet(societyID, dateUtil.getPrevOrNextDate(fromDate, 1), uptoDate, societyVo.getEffectiveDate());
				balanceSheetLedgerDetailList=reportDAO.getNormalBalanceSheet(societyID, dateUtil.getPrevOrNextDate(fromDate, 1), uptoDate, societyVo.getEffectiveDate(),"S");
				balanceSheetVO=proccessBalanceSheet(balanceSheetList,profitLossVO,societyID,fromDate,uptoDate,balanceSheetLedgerDetailList);
				ReportVO dateVO=dateUtil.getReportDate(fromDate, uptoDate);
				balanceSheetVO.setFromDate(fromDate);
				balanceSheetVO.setUptDate(uptoDate);
				balanceSheetVO.setRptFromDate(dateVO.getFromDate());
				balanceSheetVO.setRptUptoDate(dateVO.getUptDate());
				balanceSheetVO.setSocietyVO(societyVo);				
				balanceSheetVO.setLiabilityDetailsList(liabilityList);
				balanceSheetVO.setAssetsDetailsList(assetsList);
				
				logger.debug("Exit : public ReportVO getBlanceSheet(int societyID,String fromDate,String uptoDate)" +balanceSheetVO.getBalanceSheetList().size());
			}catch (Exception e) {
				logger.error("Exception occurred in public ReportVO getBlanceSheet "+e);
			}
			return balanceSheetVO;
		   }

	 private ReportVO proccessBalanceSheet(List balanceSheetList,AccountHeadVO profiLossVO,int societyID,String fromDate,String toDate,List balanceSheetDetailsList){
		 logger.debug("Entry : private List proccessBalanceSheet(List balanceSheetList)");
		 ReportVO balanceSheetVO=new ReportVO();
		 List tempLiabilityList=new ArrayList();
		 SocietyVO societyVO=societyService.getSocietyDetails(societyID);
		 List liabilityList=new ArrayList();
		 List assetsList=new ArrayList();
		 BigDecimal liabilitySumCur=BigDecimal.ZERO;
		 BigDecimal liabilitySumPrev=BigDecimal.ZERO;
		 BigDecimal assetSumCur=BigDecimal.ZERO;
		 BigDecimal assetSumPrev=BigDecimal.ZERO;
		 BigDecimal differenceInOpening=BigDecimal.ZERO;
		 List ledgerList=ledgerService.getLedgerList(societyID, 0);
		 AccountHeadVO acchVO=(AccountHeadVO) ledgerList.get(0);
		 differenceInOpening=acchVO.getSumOfcreditBal().subtract(acchVO.getSumOfDebitBal());
		 ReportDetailsVO liabilityVO=new ReportDetailsVO();
		 liabilityVO.setCategoryName("LIABILITIES");
		 liabilityList.add(liabilityVO);
		 tempLiabilityList.add(liabilityVO);
		 ReportDetailsVO assetVO=new ReportDetailsVO();
		 assetVO.setCategoryName("ASSETS");
		 assetsList.add(assetVO);
		 ReportDetailsVO prtLssVO=new ReportDetailsVO();
		 if(societyVO.getOrgType().equalsIgnoreCase("S")){
		 prtLssVO.setCategoryName("Income and Expenditure A/c");
		 }else if(societyVO.getOrgType().equalsIgnoreCase("C")){
			 prtLssVO.setCategoryName("Profit Loss A/c");
		 }
		 prtLssVO.setOpeningBalance(profiLossVO.getClosingBalance());
		 prtLssVO.setClosingBalance(profiLossVO.getOpeningBalance());
		 
		
		 	
		 	for(int i=0;balanceSheetList.size()>i;i++){
		 		List tempLedgersList=new ArrayList();
		 		AccountHeadVO balancesheetVO=(AccountHeadVO) balanceSheetList.get(i);
		 		ReportDetailsVO rptVO=new ReportDetailsVO();
		 		rptVO.setCategoryID(balancesheetVO.getCategoryID());
				rptVO.setCategoryName(balancesheetVO.getCategoryName());
				rptVO.setOpeningBalance(balancesheetVO.getOpeningBalance());
				rptVO.setClosingBalance(balancesheetVO.getClosingBalance());
				
				List ledgersList=balanceSheetDetailsList;
				
				if(ledgersList.size()>0){
					for (int j = 0; ledgersList.size()>j; j++) {
						AccountHeadVO ledgerVO=(AccountHeadVO) ledgersList.get(j);
						if(rptVO.getCategoryID()==ledgerVO.getCategoryID()){
						
						ReportDetailsVO rptLdgrVO=new ReportDetailsVO();
						rptLdgrVO.setCategoryID(ledgerVO.getCategoryID());
						rptLdgrVO.setGroupID(ledgerVO.getAccountGroupID());
						rptLdgrVO.setCategoryName(ledgerVO.getStrAccountPrimaryHead());
						rptLdgrVO.setOpeningBalance(ledgerVO.getOpeningBalance());
						rptLdgrVO.setClosingBalance(ledgerVO.getClosingBalance());
						rptLdgrVO.setLedgerID(ledgerVO.getLedgerID());
						rptLdgrVO.setLedgerName(ledgerVO.getStrAccountHeadName());
						 if((ledgerVO.getOpeningBalance().signum()!=0)||(ledgerVO.getClosingBalance().signum()!=0)){
						 		
							 tempLedgersList.add(rptLdgrVO);
						 	 }

						}			

					}
					
					
				}
				
				if(tempLedgersList.size()>0)
					rptVO.setObjectList(tempLedgersList);
				
		 		
		 		
		 		if((balancesheetVO.getOpeningBalance().signum()!=0)||(balancesheetVO.getClosingBalance().signum()!=0)){
		 		if(balancesheetVO.getRootID()==4){
		 			liabilityList.add(rptVO);
		 			liabilitySumPrev=liabilitySumPrev.add(balancesheetVO.getOpeningBalance());
		 			liabilitySumCur=liabilitySumCur.add(balancesheetVO.getClosingBalance());
		 			tempLiabilityList.add(rptVO);
		 			
		 			
		 		}else if(balancesheetVO.getRootID()==3){
		 			assetsList.add(rptVO);
		 			assetSumPrev=assetSumPrev.add(balancesheetVO.getOpeningBalance());
		 			assetSumCur=assetSumCur.add(balancesheetVO.getClosingBalance());
		 			
		 			
		 			
		 		}
		 		
		 		}
		 	}
		 	
		
		 	liabilitySumCur=liabilitySumCur.add(profiLossVO.getOpeningBalance());
		 	liabilitySumPrev=liabilitySumPrev.add(profiLossVO.getClosingBalance());
		 	
		 	liabilityList.add(prtLssVO);
		 	tempLiabilityList.add(prtLssVO);
		 	
		 	ReportDetailsVO diffOpening=new ReportDetailsVO();
		 	diffOpening.setCategoryName("DIFF IN OPENING BALANCE");
			if(differenceInOpening.compareTo(BigDecimal.ZERO)!=0){
				if(differenceInOpening.compareTo(BigDecimal.ZERO)<0){
					diffOpening.setOpeningBalance(differenceInOpening.abs());
		 			diffOpening.setClosingBalance(differenceInOpening.abs());
		 			liabilitySumCur=liabilitySumCur.add(differenceInOpening.abs());
		 			liabilitySumPrev=liabilitySumPrev.add(differenceInOpening.abs());
		 			liabilityList.add(diffOpening);
		 			tempLiabilityList.add(diffOpening);
		 			
		 		}
		 	}
		 	
		 	ReportDetailsVO sumLiability=new ReportDetailsVO();
		 	sumLiability.setCategoryName("TOTAL");
		 	sumLiability.setClosingBalance(liabilitySumCur);
		 	sumLiability.setOpeningBalance(liabilitySumPrev);
		 	
		 	liabilityList.add(sumLiability);
		 	tempLiabilityList.add(sumLiability);
		 	balanceSheetVO.setLiabilityList(liabilityList);
			if(differenceInOpening.compareTo(BigDecimal.ZERO)!=0){
				if(differenceInOpening.compareTo(BigDecimal.ZERO)>0){
		 			diffOpening.setOpeningBalance(differenceInOpening.abs());
		 			diffOpening.setClosingBalance(differenceInOpening.abs());
		 			assetSumPrev=assetSumPrev.add(differenceInOpening.abs());
		 			assetSumCur=assetSumCur.add(differenceInOpening.abs());
		 			assetsList.add(diffOpening);
		 		}
		 	}
		 	
		 	ReportDetailsVO sumAssets=new ReportDetailsVO();
		 	sumAssets.setCategoryName("TOTAL");
		 	sumAssets.setClosingBalance(assetSumCur);
		 	sumAssets.setOpeningBalance(assetSumPrev);
		 	assetsList.add(sumAssets);
		 	
		 	balanceSheetVO.setAssetsList(assetsList);
		 	
		 	for(int i=0;assetsList.size()>i;i++){
		 		ReportDetailsVO rpt=(ReportDetailsVO) assetsList.get(i);
		 		tempLiabilityList.add(rpt);
		 	}
		   balanceSheetVO.setBalanceSheetList(tempLiabilityList);
		   		   
		 
		 logger.debug("Entry : private List proccessBalanceSheet(List balanceSheetList)"+liabilityList.size()+" "+balanceSheetVO.getBalanceSheetList().size());
		 return balanceSheetVO;
	 }
	 
	 public ReportVO printBalanceSheet(int societyID,String fromDate,String uptoDate,String formatType){
			ReportVO balanceSheetVO =new ReportVO();
			logger.debug("Entry : public ReportVO printBalanceSheet(int societyID,String fromDate,String uptoDate,String formatType)" +fromDate+"  "+uptoDate);
			List balanceSheetList=new ArrayList();
			List balanceSheetLedgerDetailList=new ArrayList();
			List liabilityList=new ArrayList();
			List assetsList=new ArrayList();
			SocietyVO societyVO=societyService.getSocietyDetails(societyID);
		
			try {
								
				SocietyVO societyVo=societyService.getSocietyDetails("0", societyID);
				ReportVO dateVO=dateUtil.getReportDate(fromDate, uptoDate);
				balanceSheetVO.setFromDate(fromDate);
				balanceSheetVO.setUptDate(uptoDate);
				balanceSheetVO.setRptFromDate(dateVO.getFromDate());
				balanceSheetVO.setRptUptoDate(dateVO.getUptDate());
				balanceSheetVO.setSocietyVO(societyVo);
				
          if((!formatType.equalsIgnoreCase("I"))){
         	 logger.info(formatType);
				AccountHeadVO profitLossVO=ledgerService.getProffitLossBalance(societyID, fromDate, uptoDate,"bs",0,formatType);
				balanceSheetList=reportDAO.getNormalBalanceSheet(societyID, dateUtil.getPrevOrNextDate(fromDate, 1), uptoDate, societyVo.getEffectiveDate(),formatType);
				balanceSheetVO=proccessPrintBalanceSheet(balanceSheetList,profitLossVO,societyID,fromDate,uptoDate);
							
				//ledger details
				balanceSheetLedgerDetailList=reportDAO.getBalanceSheetLedgerDetails(societyID, dateUtil.getPrevOrNextDate(fromDate, 1), uptoDate, societyVo.getEffectiveDate());
				for(int i=0;balanceSheetLedgerDetailList.size()>i;i++){
			 		AccountHeadVO balancesheetVO=(AccountHeadVO) balanceSheetLedgerDetailList.get(i);
			 		ReportDetailsVO rptVO=new ReportDetailsVO();
			 		rptVO.setCategoryID(balancesheetVO.getAccountGroupID());
					rptVO.setCategoryName(balancesheetVO.getStrAccountPrimaryHead());
					rptVO.setOpeningBalance(balancesheetVO.getOpeningBalance());
					rptVO.setClosingBalance(balancesheetVO.getClosingBalance());
					rptVO.setLedgerName(balancesheetVO.getLedgerName());
					rptVO.setLedgerID(balancesheetVO.getLedgerID());
			 		
			 	 if((balancesheetVO.getOpeningBalance().signum()!=0)||(balancesheetVO.getClosingBalance().signum()!=0)){
			 		if(balancesheetVO.getRootID()==4){
			 			liabilityList.add(rptVO);			 		
			 			 			
			 		}else if(balancesheetVO.getRootID()==3){
			 			assetsList.add(rptVO);
			 						 			
			 		}
			 		
			 	 }
			 	}
				
				 BigDecimal differenceInOpening=BigDecimal.ZERO;
				 List ledgerList=ledgerService.getLedgerList(societyID, 0);
				 AccountHeadVO acchVO=(AccountHeadVO) ledgerList.get(0);
				 differenceInOpening=acchVO.getSumOfcreditBal().subtract(acchVO.getSumOfDebitBal());
				
			 	ReportDetailsVO diffOpening=new ReportDetailsVO();
			 	diffOpening.setCategoryName("DIFF IN OPENING BALANCE");
			 	diffOpening.setLedgerName("DIFF IN OPENING BALANCE");
			 	if(differenceInOpening.intValue()!=0){
			 		if(differenceInOpening.intValue()<0){
			 			diffOpening.setOpeningBalance(differenceInOpening.abs());
			 			diffOpening.setClosingBalance(differenceInOpening.abs());
			 			liabilityList.add(diffOpening);		 			
			 		}
			 	}
				
				 ReportDetailsVO prtLssVO=new ReportDetailsVO();
				 if(societyVO.getOrgType().equalsIgnoreCase("S")){
					 prtLssVO.setCategoryName("Income and Expenditure A/c");
					 prtLssVO.setLedgerName("Income and Expenditure A/c");
					 }else if(societyVO.getOrgType().equalsIgnoreCase("C")){
						 prtLssVO.setCategoryName("Profit Loss A/c");
						 prtLssVO.setLedgerName("Profit Loss A/c");
					 }
				
				 prtLssVO.setOpeningBalance(profitLossVO.getClosingBalance());
				 prtLssVO.setClosingBalance(profitLossVO.getOpeningBalance());
				 liabilityList.add(prtLssVO);
				 
				balanceSheetVO.setLiabilityDetailsList(liabilityList);
				balanceSheetVO.setAssetsDetailsList(assetsList);
          }
				
				logger.debug("Exit : public ReportVO printBalanceSheet(int societyID,String fromDate,String uptoDate,String formatType)");
			}catch (Exception e) {
				logger.error("Exception occurred in public ReportVO printBalanceSheet "+e);
			}
			return balanceSheetVO;
		   }

	 private ReportVO proccessPrintBalanceSheet(List balanceSheetList,AccountHeadVO profiLossVO,int societyID,String fromDate,String toDate){
		 logger.debug("Entry : private List proccessPrintBalanceSheet(List balanceSheetList)");
		 ReportVO balanceSheetVO=new ReportVO();
		 List tempLiabilityList=new ArrayList();
		 List liabilityList=new ArrayList();
		 List assetsList=new ArrayList();
		 BigDecimal liabilitySumCur=BigDecimal.ZERO;
		 BigDecimal liabilitySumPrev=BigDecimal.ZERO;
		 BigDecimal assetSumCur=BigDecimal.ZERO;
		 BigDecimal assetSumPrev=BigDecimal.ZERO;
		 BigDecimal differenceInOpening=BigDecimal.ZERO;
		 SocietyVO societyVO=societyService.getSocietyDetails(societyID);
		 List ledgerList=ledgerService.getLedgerList(societyID, 0);
		 AccountHeadVO acchVO=(AccountHeadVO) ledgerList.get(0);
		 differenceInOpening=acchVO.getSumOfcreditBal().subtract(acchVO.getSumOfDebitBal());
		 ReportDetailsVO liabilityVO=new ReportDetailsVO();
		 liabilityVO.setCategoryName("LIABILITIES");
		 liabilityList.add(liabilityVO);
		 tempLiabilityList.add(liabilityVO);
		 ReportDetailsVO assetVO=new ReportDetailsVO();
		 assetVO.setCategoryName("ASSETS");
		 assetsList.add(assetVO);
		 ReportDetailsVO prtLssVO=new ReportDetailsVO();
		 if(societyVO.getOrgType().equalsIgnoreCase("S")){
		 prtLssVO.setCategoryName("Income and Expenditure A/c");
		 }else if(societyVO.getOrgType().equalsIgnoreCase("C")){
			 prtLssVO.setCategoryName("Profit Loss A/c");
		 }
		 prtLssVO.setOpeningBalance(profiLossVO.getClosingBalance());
		 prtLssVO.setClosingBalance(profiLossVO.getOpeningBalance());
		 
		
		 	
		 	for(int i=0;balanceSheetList.size()>i;i++){
		 		List tempLedgersList=new ArrayList();
		 		AccountHeadVO balancesheetVO=(AccountHeadVO) balanceSheetList.get(i);
		 		ReportDetailsVO rptVO=new ReportDetailsVO();
		 		rptVO.setCategoryID(balancesheetVO.getAccountGroupID());
				rptVO.setCategoryName(balancesheetVO.getStrAccountPrimaryHead());
				rptVO.setOpeningBalance(balancesheetVO.getOpeningBalance());
				rptVO.setClosingBalance(balancesheetVO.getClosingBalance());
				
				List ledgersList=trialBalanceService.getLedgerReport(societyID, fromDate, toDate, balancesheetVO.getAccountGroupID(),0);
				
				if(ledgersList.size()>0){
					for (int j = 0; ledgersList.size()>j; j++) {
						AccountHeadVO ledgerVO=(AccountHeadVO) ledgersList.get(j);
						ReportDetailsVO rptLdgrVO=new ReportDetailsVO();
						rptLdgrVO.setCategoryID(ledgerVO.getAccountGroupID());
						rptLdgrVO.setCategoryName(ledgerVO.getStrAccountPrimaryHead());
						rptLdgrVO.setOpeningBalance(ledgerVO.getOpeningBalance());
						rptLdgrVO.setClosingBalance(ledgerVO.getClosingBalance());
						rptLdgrVO.setLedgerID(ledgerVO.getLedgerID());
						rptLdgrVO.setLedgerName(ledgerVO.getStrAccountHeadName());
						tempLedgersList.add(rptLdgrVO);
					}
					
					
				}
				
				if(tempLedgersList.size()>0)
					rptVO.setObjectList(tempLedgersList);
				
		 		
		 		
		 		if((balancesheetVO.getOpeningBalance().signum()!=0)||(balancesheetVO.getClosingBalance().signum()!=0)){
		 		if(balancesheetVO.getRootID()==4){
		 			liabilityList.add(rptVO);
		 			liabilitySumPrev=liabilitySumPrev.add(balancesheetVO.getOpeningBalance());
		 			liabilitySumCur=liabilitySumCur.add(balancesheetVO.getClosingBalance());
		 			tempLiabilityList.add(rptVO);
		 			
		 			
		 		}else if(balancesheetVO.getRootID()==3){
		 			assetsList.add(rptVO);
		 			assetSumPrev=assetSumPrev.add(balancesheetVO.getOpeningBalance());
		 			assetSumCur=assetSumCur.add(balancesheetVO.getClosingBalance());
		 			
		 			
		 			
		 		}
		 		
		 		}
		 	}
		 	
		
		 	liabilitySumCur=liabilitySumCur.add(profiLossVO.getOpeningBalance());
		 	liabilitySumPrev=liabilitySumPrev.add(profiLossVO.getClosingBalance());
		 	
		 	liabilityList.add(prtLssVO);
		 	tempLiabilityList.add(prtLssVO);
		 	
		 	ReportDetailsVO diffOpening=new ReportDetailsVO();
		 	diffOpening.setCategoryName("DIFF IN OPENING BALANCE");
			if(differenceInOpening.compareTo(BigDecimal.ZERO)!=0){
				if(differenceInOpening.compareTo(BigDecimal.ZERO)<0){
		 			diffOpening.setOpeningBalance(differenceInOpening.abs());
		 			diffOpening.setClosingBalance(differenceInOpening.abs());
		 			liabilitySumCur=liabilitySumCur.add(differenceInOpening.abs());
		 			liabilitySumPrev=liabilitySumPrev.add(differenceInOpening.abs());
		 			liabilityList.add(diffOpening);
		 			tempLiabilityList.add(diffOpening);
		 			
		 		}
		 	}
		 	
		 	ReportDetailsVO sumLiability=new ReportDetailsVO();
		 	sumLiability.setCategoryName("TOTAL");
		 	sumLiability.setClosingBalance(liabilitySumCur);
		 	sumLiability.setOpeningBalance(liabilitySumPrev);
		 	
		 	liabilityList.add(sumLiability);
		 	tempLiabilityList.add(sumLiability);
		 	balanceSheetVO.setLiabilityList(liabilityList);
			if(differenceInOpening.compareTo(BigDecimal.ZERO)!=0){
				if(differenceInOpening.compareTo(BigDecimal.ZERO)>0){
		 			diffOpening.setOpeningBalance(differenceInOpening.abs());
		 			diffOpening.setClosingBalance(differenceInOpening.abs());
		 			assetSumPrev=assetSumPrev.add(differenceInOpening.abs());
		 			assetSumCur=assetSumCur.add(differenceInOpening.abs());
		 			assetsList.add(diffOpening);
		 		}
		 	}
		 	
		 	ReportDetailsVO sumAssets=new ReportDetailsVO();
		 	sumAssets.setCategoryName("TOTAL");
		 	sumAssets.setClosingBalance(assetSumCur);
		 	sumAssets.setOpeningBalance(assetSumPrev);
		 	assetsList.add(sumAssets);
		 	
		 	balanceSheetVO.setAssetsList(assetsList);
		 	
		 	for(int i=0;assetsList.size()>i;i++){
		 		ReportDetailsVO rpt=(ReportDetailsVO) assetsList.get(i);
		 		tempLiabilityList.add(rpt);
		 	}
		   balanceSheetVO.setBalanceSheetList(tempLiabilityList);
		   		   
		 
		 logger.debug("Entry : private List proccessPrintBalanceSheet(List balanceSheetList)"+liabilityList.size()+" "+balanceSheetVO.getBalanceSheetList().size());
		 return balanceSheetVO;
	 }
	 
	 public ReportVO getConsolidatedBalanceSheetReport(int societyID,String fromDate,String uptoDate,String formatType){
			ReportVO balanceSheetVO =new ReportVO();
			logger.debug("Entry : public ReportVO getConsolidatedBalanceSheetReport(int societyID,String fromDate,String uptoDate)" +fromDate+"  "+uptoDate);
			List balanceSheetList=new ArrayList();
			List balanceSheetLedgerDetailList=new ArrayList();
			List liabilityList=new ArrayList();
			List assetsList=new ArrayList();
		
			try {
								
				SocietyVO societyVo=societyService.getSocietyDetails("0", societyID);
				AccountHeadVO profitLossVO=ledgerService.getConsolidatedProffitLossBalance(societyID, fromDate, uptoDate,"bs",formatType);
				balanceSheetList=reportDAO.getConsolidatedTreeWiseBalanceSheet(societyID, dateUtil.getPrevOrNextDate(fromDate, 1), uptoDate, societyVo.getEffectiveDate());
				balanceSheetLedgerDetailList=reportDAO.getConsolidatedNormalBalanceSheet(societyID, dateUtil.getPrevOrNextDate(fromDate, 1), uptoDate, societyVo.getEffectiveDate());
				balanceSheetVO=proccessBalanceSheet(balanceSheetList,profitLossVO,societyID,fromDate,uptoDate,balanceSheetLedgerDetailList);
				ReportVO dateVO=dateUtil.getReportDate(fromDate, uptoDate);
				balanceSheetVO.setFromDate(fromDate);
				balanceSheetVO.setUptDate(uptoDate);
				balanceSheetVO.setRptFromDate(dateVO.getFromDate());
				balanceSheetVO.setRptUptoDate(dateVO.getUptDate());
				balanceSheetVO.setSocietyVO(societyVo);				
				balanceSheetVO.setLiabilityDetailsList(liabilityList);
				balanceSheetVO.setAssetsDetailsList(assetsList);
				
				logger.debug("Exit : public ReportVO getConsolidatedBalanceSheetReport(int societyID,String fromDate,String uptoDate)" +balanceSheetVO.getBalanceSheetList().size());
			}catch (Exception e) {
				logger.error("Exception occurred in public ReportVO getConsolidatedBalanceSheetReport "+e);
			}
			return balanceSheetVO;
		   }
	 
	 public ReportVO getIndASBalanceSheetReport(int orgID,String fromDate,String uptoDate,int reportID){
			ReportVO balanceSheetVO =new ReportVO();
			logger.debug("Entry : public ReportVO getIndASBalanceSheetReport(int societyID,String fromDate,String uptoDate)" +fromDate+"  "+uptoDate);
			List balanceSheetList=new ArrayList();
			List balanceSheetLedgerDetailList=new ArrayList();
			List liabilityList=new ArrayList();
			List assetsList=new ArrayList();
			List labelList=new ArrayList();
			List categoryList=new ArrayList();
			 
			
		
			try {
								
				SocietyVO societyVo=societyService.getSocietyDetails("0", orgID);
				AccountHeadVO profitLossVO=ledgerService.getProffitLossBalance(orgID, fromDate, uptoDate,"bs",0,"S");
			
				balanceSheetList=reportDAO.getTreeWiseBalanceSheet(orgID, dateUtil.getPrevOrNextDate(fromDate, 1), uptoDate, societyVo.getEffectiveDate());
				

				Map<Integer,AccountHeadVO> categoryMap=converListToMapBalanceSheet(balanceSheetList);
				
				
				labelList=reportDAO.getIndASReportStructure(orgID, reportID, "L",0);
				categoryList=reportDAO.getIndASReportStructure(orgID, reportID, "C",0);
				
				
				
				for(int i=0;categoryList.size()>i;i++){
					List objectList=new ArrayList<>();
					BigDecimal opnBal=BigDecimal.ZERO;
					BigDecimal clsBal=BigDecimal.ZERO;
					ReportDetailsVO cateVO=(ReportDetailsVO) categoryList.get(i);	
					
					List cateList=reportDAO.getIndASReportStructureDetails(cateVO.getId());
					
					if(cateList.size()>0){
						for(int j=0;cateList.size()>j;j++){
							ReportDetailsVO catlinkingVO=(ReportDetailsVO) cateList.get(j);
							 
							if(catlinkingVO.getRptGrpType().equalsIgnoreCase("C")){
								AccountHeadVO obj=categoryMap.get(catlinkingVO.getCategoryID());
								if(obj!=null){
								ReportDetailsVO rptVO=new ReportDetailsVO();
						 		rptVO.setCategoryID(obj.getCategoryID());
								rptVO.setCategoryName(obj.getCategoryName());
								rptVO.setOpeningBalance(obj.getOpeningBalance());
								rptVO.setClosingBalance(obj.getClosingBalance());
								opnBal=opnBal.add(obj.getOpeningBalance());
								clsBal=clsBal.add(obj.getClosingBalance());
								
								objectList.add(rptVO);
								}
							}else if(catlinkingVO.getRptGrpType().equalsIgnoreCase("G")){
								
							}
						}
					}
					cateVO.setObjectList(objectList);
					cateVO.setOpeningBalance(opnBal);
					cateVO.setClosingBalance(clsBal);
					
					
					
				}
				
				
				if(labelList.size()>0){
					
					for(int i=0;labelList.size()>i;i++){
						List objectList=new ArrayList<>();
						ReportDetailsVO labelObj=(ReportDetailsVO) labelList.get(i);
						BigDecimal opnBal=BigDecimal.ZERO;
						BigDecimal clsBal=BigDecimal.ZERO;
						
						for(int j=0;categoryList.size()>j;j++){
							
							ReportDetailsVO cateVO=(ReportDetailsVO) categoryList.get(j);	
							
							if (labelObj.getId()==cateVO.getParentID()) {
								opnBal=opnBal.add(cateVO.getOpeningBalance());
								clsBal=clsBal.add(cateVO.getClosingBalance());
								objectList.add(cateVO);
								
							}	
							
							
							
							
							
							
						}
						labelObj.setObjectList(objectList);
						labelObj.setOpeningBalance(opnBal);
						labelObj.setClosingBalance(clsBal);
					}
					
					
				}
				
				
				
				
				
				balanceSheetVO=proccessIndASBalanceSheet(labelList,profitLossVO,societyVo,fromDate,uptoDate);
				
				ReportVO dateVO=dateUtil.getReportDate(fromDate, uptoDate);
				balanceSheetVO.setFromDate(fromDate);
				balanceSheetVO.setUptDate(uptoDate);
				balanceSheetVO.setRptFromDate(dateVO.getFromDate());
				balanceSheetVO.setRptUptoDate(dateVO.getUptDate());
				balanceSheetVO.setSocietyVO(societyVo);				
				balanceSheetVO.setBalanceSheetList(labelList);
				
				
				logger.debug("Exit : public ReportVO getIndASBalanceSheetReport(int societyID,String fromDate,String uptoDate)" +balanceSheetVO.getBalanceSheetList().size());
			}catch (Exception e) {
				logger.error("Exception occurred in public ReportVO getIndASBalanceSheetReport "+e);
			}
			return balanceSheetVO;
		   }
	 
	 
	 public  Map<Integer,AccountHeadVO> converListToMapBalanceSheet(List<AccountHeadVO> balanceSheetList)

	 {

	     Map<Integer, AccountHeadVO> balanceSheetMap = new HashMap<Integer, AccountHeadVO>();

	     for(AccountHeadVO balanceSheetVO : balanceSheetList)

	     {

	         balanceSheetMap.put(balanceSheetVO.getCategoryID(), balanceSheetVO);

	     }

	     return balanceSheetMap;

	 }
	 
	 public  Map<Integer,AccountHeadVO> converListToMapBalanceSheetDetails(List<AccountHeadVO> balanceSheetList)

	 {

	     Map<Integer, AccountHeadVO> balanceSheetMap = new HashMap<Integer, AccountHeadVO>();

	     for(AccountHeadVO balanceSheetVO : balanceSheetList)

	     {

	         balanceSheetMap.put(balanceSheetVO.getAccountGroupID(), balanceSheetVO);

	     }

	     return balanceSheetMap;

	 }
	 
	 
	 private ReportVO proccessIndASBalanceSheet(List balanceSheetList,AccountHeadVO profiLossVO,SocietyVO societyVO,String fromDate,String toDate){
		 logger.debug("Entry : private List proccessBalanceSheet(List balanceSheetList)");
		 ReportVO balanceSheetVO=new ReportVO();
		 List tempLiabilityList=new ArrayList();
		 List liabilityList=new ArrayList();
		 List assetsList=new ArrayList();
		 BigDecimal liabilitySumCur=BigDecimal.ZERO;
		 BigDecimal liabilitySumPrev=BigDecimal.ZERO;
		 BigDecimal assetSumCur=BigDecimal.ZERO;
		 BigDecimal assetSumPrev=BigDecimal.ZERO;
		 BigDecimal differenceInOpening=BigDecimal.ZERO;
		 List ledgerList=ledgerService.getLedgerList(societyVO.getSocietyID(), 0);
		 AccountHeadVO acchVO=(AccountHeadVO) ledgerList.get(0);
		 differenceInOpening=acchVO.getSumOfcreditBal().subtract(acchVO.getSumOfDebitBal());
		
		 ReportDetailsVO prtLssVO=new ReportDetailsVO();
		 if(societyVO.getOrgType().equalsIgnoreCase("C")){
		 prtLssVO.setCategoryName("Profit Loss A/c");
		 }else if(societyVO.getOrgType().equalsIgnoreCase("S")){
			 prtLssVO.setCategoryName("Income and Expenditure A/c");
		 }
		 prtLssVO.setOpeningBalance(profiLossVO.getClosingBalance());
		 prtLssVO.setClosingBalance(profiLossVO.getOpeningBalance());
		 prtLssVO.setRptGrpType("L");
		 
		
		 	
		 	for(int i=0;balanceSheetList.size()>i;i++){
		 		List tempLedgersList=new ArrayList();
		 		ReportDetailsVO balanceshtVO=(ReportDetailsVO) balanceSheetList.get(i);
		 	
		 		
		 		
		 	
		 		if(balanceshtVO.getRootID()==4){
		 			
		 			liabilitySumPrev=liabilitySumPrev.add(balanceshtVO.getOpeningBalance());
		 			liabilitySumCur=liabilitySumCur.add(balanceshtVO.getClosingBalance());
		 			
		 			liabilityList.add(balanceshtVO);
		 			tempLiabilityList.add(balanceshtVO);
		 			
		 			
		 		}else if(balanceshtVO.getRootID()==3){
		 			
		 			assetSumPrev=assetSumPrev.add(balanceshtVO.getOpeningBalance());
		 			assetSumCur=assetSumCur.add(balanceshtVO.getClosingBalance());
		 			
		 			assetsList.add(balanceshtVO);
		 			
		 			
		 		}
		 		
		 	
		 	}
		 	
		
		 	liabilitySumCur=liabilitySumCur.add(profiLossVO.getOpeningBalance());
		 	liabilitySumPrev=liabilitySumPrev.add(profiLossVO.getClosingBalance());
		 	
		 	liabilityList.add(prtLssVO);
		 	tempLiabilityList.add(prtLssVO);
		 	
		 	ReportDetailsVO diffOpening=new ReportDetailsVO();
		 	diffOpening.setRptGrpType("L");
		 	diffOpening.setCategoryName("DIFF IN OPENING BALANCE");
			if(differenceInOpening.compareTo(BigDecimal.ZERO)!=0){
				if(differenceInOpening.compareTo(BigDecimal.ZERO)<0){
					diffOpening.setOpeningBalance(differenceInOpening.abs());
		 			diffOpening.setClosingBalance(differenceInOpening.abs());
		 			liabilitySumCur=liabilitySumCur.add(differenceInOpening.abs());
		 			liabilitySumPrev=liabilitySumPrev.add(differenceInOpening.abs());
		 			liabilityList.add(diffOpening);
		 			tempLiabilityList.add(diffOpening);
		 			
		 		}
		 	}
		 	
		 	ReportDetailsVO sumLiability=new ReportDetailsVO();
		 	sumLiability.setCategoryName("TOTAL");
		 	sumLiability.setClosingBalance(liabilitySumCur);
		 	sumLiability.setOpeningBalance(liabilitySumPrev);
		 	sumLiability.setRptGrpType("L");
		 	
		 	liabilityList.add(sumLiability);
		 	tempLiabilityList.add(sumLiability);
		 	balanceSheetVO.setLiabilityList(liabilityList);
			if(differenceInOpening.compareTo(BigDecimal.ZERO)!=0){
				if(differenceInOpening.compareTo(BigDecimal.ZERO)>0){
		 			diffOpening.setOpeningBalance(differenceInOpening.abs());
		 			diffOpening.setClosingBalance(differenceInOpening.abs());
		 			assetSumPrev=assetSumPrev.add(differenceInOpening.abs());
		 			assetSumCur=assetSumCur.add(differenceInOpening.abs());
		 			assetsList.add(diffOpening);
		 		}
		 	}
		 	
		 	ReportDetailsVO sumAssets=new ReportDetailsVO();
		 	sumAssets.setCategoryName("TOTAL");
		 	sumAssets.setClosingBalance(assetSumCur);
		 	sumAssets.setOpeningBalance(assetSumPrev);
		 	sumAssets.setRptGrpType("L");
		 	assetsList.add(sumAssets);
		 	
		 	balanceSheetVO.setAssetsList(assetsList);
		 	
		 	for(int i=0;assetsList.size()>i;i++){
		 		ReportDetailsVO rpt=(ReportDetailsVO) assetsList.get(i);
		 		tempLiabilityList.add(rpt);
		 	}
		   balanceSheetVO.setBalanceSheetList(tempLiabilityList);
		   		   
		 
		 logger.debug("Entry : private List proccessBalanceSheet(List balanceSheetList)"+liabilityList.size()+" "+balanceSheetVO.getBalanceSheetList().size());
		 return balanceSheetVO;
	 }
	 
	 public List getBalanceSheetDetailsForLabelReport(int orgID,String fromDate, String uptoDate,int labelID,int isConsolidated){
			List ledgerList=new ArrayList();
			List cateObjList=new ArrayList();
			
			try{
			logger.debug("Entry : public List getBalanceSheetDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated)"+labelID);
			SocietyVO societyVo=societyService.getSocietyDetails("0", orgID);
			List balanceSheetList=reportDAO.getTreeWiseBalanceSheet(orgID, dateUtil.getPrevOrNextDate(fromDate, 1), uptoDate, societyVo.getEffectiveDate());
			List balanceSheetLedgerDetailList=reportDAO.getNormalBalanceSheet(orgID, dateUtil.getPrevOrNextDate(fromDate, 1), uptoDate, societyVo.getEffectiveDate(),"S");
			List cateList=reportDAO.getIndASReportStructureDetails(labelID);
			Map<Integer,AccountHeadVO> categoryMap=converListToMapBalanceSheet(balanceSheetList);
			Map<Integer,AccountHeadVO> groupMap=converListToMapBalanceSheetDetails(balanceSheetLedgerDetailList);
			
			if(cateList.size()>0){
				for(int i=0;cateList.size()>i;i++){
					ReportDetailsVO catlinkingVO=(ReportDetailsVO) cateList.get(i);
					 
					if(catlinkingVO.getRptGrpType().equalsIgnoreCase("C")){
						AccountHeadVO obj=categoryMap.get(catlinkingVO.getCategoryID());
						if(obj!=null){
						ReportDetailsVO rptVO=new ReportDetailsVO();
						catlinkingVO.setCategoryID(obj.getCategoryID());
						catlinkingVO.setCategoryName(obj.getCategoryName());
						catlinkingVO.setOpeningBalance(obj.getOpeningBalance());
						catlinkingVO.setClosingBalance(obj.getClosingBalance());
						
						List groupList=ledgerService.getGroupListByCategory(obj.getCategoryID(), orgID, 0);
						List grpObjList=new ArrayList();
						if(groupList.size()>0){
							for(int j=0;groupList.size()>j;j++){
								AccountHeadVO tempGrpVO=(AccountHeadVO) groupList.get(j);
								AccountHeadVO group=groupMap.get(tempGrpVO.getAccountGroupID());
								if(group!=null){
								ReportDetailsVO grpVO=new ReportDetailsVO();
								grpVO.setCategoryID(group.getCategoryID());
								grpVO.setCategoryName(obj.getCategoryName());
								grpVO.setGroupName(group.getStrAccountPrimaryHead());
								grpVO.setGroupID(group.getAccountGroupID());
								grpVO.setOpeningBalance(group.getOpeningBalance());
								grpVO.setClosingBalance(group.getClosingBalance());
								
								ledgerList=trialBalanceService.getLedgerReport(orgID, fromDate, uptoDate, grpVO.getGroupID(), isConsolidated);
								List ledgerObjList=new ArrayList();
								if(ledgerList.size()>0){
									
									for(int k=0;ledgerList.size()>k;k++){
										
										AccountHeadVO ledger=(AccountHeadVO) ledgerList.get(k);
										if(ledger!=null){
										ReportDetailsVO ledgerVO=new ReportDetailsVO();
										ledgerVO.setCategoryID(grpVO.getCategoryID());
										ledgerVO.setCategoryName(grpVO.getCategoryName());
										ledgerVO.setLedgerName(ledger.getStrAccountHeadName());
										ledgerVO.setGroupID(grpVO.getGroupID());
										ledgerVO.setOpeningBalance(ledger.getOpeningBalance());
										ledgerVO.setClosingBalance(ledger.getClosingBalance());
										ledgerVO.setLedgerID(ledger.getLedgerID());
									ledgerObjList.add(ledgerVO);
										}
								}
								
								
							} grpVO.setObjectList(ledgerObjList);
							grpObjList.add(grpVO);
							
						}
								
						}catlinkingVO.setObjectList(grpObjList);
						
						
						}
						}
					}else if(catlinkingVO.getRptGrpType().equalsIgnoreCase("G")){
						
					}
					if((catlinkingVO.getObjectList()!=null)&&(catlinkingVO.getObjectList().size()>0))
					cateObjList.add(catlinkingVO);
				}
				
				
			}
			
			
			logger.debug("Exit : public List getBalanceSheetDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated)");
			}catch (Exception e) {
				logger.error("Exception: public List getBalanceSheetDetailsForLabelReport(String societyID,String fromDate, String uptoDate,int isConsolidated) "+e );
			}
			return cateObjList;
		}
	 
	 
	 
	public ReportDAO getReportDAO() {
		return reportDAO;
	}


	public void setReportDAO(ReportDAO reportDAO) {
		this.reportDAO = reportDAO;
	}


	public SocietyService getSocietyService() {
		return societyService;
	}


	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}


	public void setLedgerService(LedgerService ledgerService) {
		this.ledgerService = ledgerService;
	}

	/**
	 * @param trialBalanceService the trialBalanceService to set
	 */
	public void setTrialBalanceService(TrialBalanceService trialBalanceService) {
		this.trialBalanceService = trialBalanceService;
	}

}
