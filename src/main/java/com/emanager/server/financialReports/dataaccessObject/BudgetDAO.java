package com.emanager.server.financialReports.dataaccessObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.emanager.server.financialReports.valueObject.BudgetDetailsVO;
import com.emanager.server.financialReports.valueObject.ReportDetailsVO;
import com.emanager.server.projects.valueObjects.ProjectDetailsVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;

public class BudgetDAO {

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	private SocietyService societyService;
	private static final Logger log = Logger.getLogger(BudgetDAO.class);

	// -----Add Budget Details -------//
	public int addBudgetDetails(BudgetDetailsVO budgetVO) {
		log.debug("Entry : public int addBudgetDetails(BudgetDetailsVO budgetVO)");
		int success = 0;
		try {
			java.util.Date today = new java.util.Date();
			final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			budgetVO.setCreateDate(currentTimestamp);
			budgetVO.setUpdateDate(currentTimestamp);

			String sql = "INSERT INTO budget_details "
					+ "(org_id,budget_name,description ,from_date,to_date,created_by,updated_by,create_date,update_date,project_id,cc_category_id, parent_id, frequency,budget_type,project_acronyms )  "
					+ "VALUES (:orgID,:budgetName,:description,:fromDate,:toDate,:createdBy,:updatedBy,:createDate,:updateDate,:projectID,:ccCategoryID,:parentID,:frequency,:budgetType,:projectAcronyms)";
			log.debug("Sql: " + sql);

			SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(budgetVO);
			KeyHolder keyHolder = new GeneratedKeyHolder();
			getNamedParameterJdbcTemplate().update(sql, fileParameters, keyHolder);
			success = keyHolder.getKey().intValue();

		} catch (DuplicateKeyException e) {
			success = -1;
			log.info("Duplicate budget Name can not be added");

		} catch (Exception e) {
			log.error("Exception in adding budget details " + e);
		}
		log.debug("Exit : public int addBudgetDetails(BudgetDetailsVO budgetVO)");
		return success;
	}

	// -----Update Budget Details -------//
	public int updateBudgetDetails(BudgetDetailsVO budgetVO) {
		log.debug("Entry : public int updateBudgetDetails(BudgetDetailsVO budgetVO)");
		int success = 0;
		try {
			java.util.Date today = new java.util.Date();
			final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());

			String str = "UPDATE budget_details SET budget_name=:budgetName,"
					+ " description=:description, from_date=:fromDate, to_date=:toDate, updated_by=:updatedBy, update_date=:updateDate,cc_category_id=:ccCategoryID,parent_id=:parentID,frequency=:frequency, budget_type=:budgetType,project_acronyms=:projectAcronyms  WHERE budget_id=:budgetID and org_id=:orgID ";

			Map hMap = new HashMap();
			hMap.put("budgetID", budgetVO.getBudgetID());
			hMap.put("orgID", budgetVO.getOrgID());
			hMap.put("budgetName", budgetVO.getBudgetName());
			hMap.put("description", budgetVO.getDescription());
			hMap.put("fromDate", budgetVO.getFromDate());
			hMap.put("toDate", budgetVO.getToDate());
			hMap.put("updatedBy", budgetVO.getUpdatedBy());
			hMap.put("updateDate", currentTimestamp);
			hMap.put("ccCategoryID", budgetVO.getCcCategoryID());
			hMap.put("parentID", budgetVO.getProjectID());
			hMap.put("frequency", budgetVO.getFrequency());
			hMap.put("budgetType",budgetVO.getBudgetType());
			hMap.put("projectAcronyms", budgetVO.getProjectAcronyms());

			success = namedParameterJdbcTemplate.update(str, hMap);

		} catch (Exception e) {
			log.error("Exception in updating budget details " + e);
		}

		log.debug("Exit : public int updateBudgetDetails(BudgetDetailsVO budgetVO)");
		return success;
	}

	// -----Delete Budget Details -------//
	public int deleteBudgetDetails(BudgetDetailsVO budgetVO) {
		log.debug("Entry : public int deleteBudgetDetails(BudgetDetailsVO budgetVO)");
		int success = 0;
		try {

			java.util.Date today = new java.util.Date();
			final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());

			String str = "UPDATE budget_details SET is_deleted=1,  updated_by=:updatedBy, update_date=:updateDate WHERE budget_id=:budgetID and org_id=:orgID ";

			Map hMap = new HashMap();
			hMap.put("budgetID", budgetVO.getBudgetID());
			hMap.put("orgID", budgetVO.getOrgID());
			hMap.put("updatedBy", budgetVO.getUpdatedBy());
			hMap.put("updateDate", currentTimestamp);

			success = namedParameterJdbcTemplate.update(str, hMap);

		} catch (Exception e) {
			log.error("Exception in deleting budget details " + e);
		}

		log.debug("Exit : public int deleteBudgetDetails(BudgetDetailsVO budgetVO)");
		return success;
	}

	// -----Get Budget Details List-------//
	public List getBudgetDetailsList(BudgetDetailsVO budgetVO) {
		log.debug("Entry : public List getBudgetDetailsList(BudgetDetailsVO budgetVO)");
		List budgetList = null;
		try {

			String str = "SELECT b.*,p.category_name FROM(SELECT b.*,u.full_name AS creatorName,um.full_name AS updatorName FROM budget_details b,um_users u,um_users um WHERE b.created_by=u.id AND b.updated_by=um.id AND org_id=:orgID AND (from_date BETWEEN :fromDate AND :toDate) and b.is_deleted=0 ) "
					+ " AS b LEFT JOIN (SELECT * FROM cc_category_details WHERE org_id=:orgID AND is_deleted=0) AS p ON b.cc_category_id=p.id  ;";

			Map hMap = new HashMap();
			hMap.put("orgID", budgetVO.getOrgID());
			hMap.put("fromDate", budgetVO.getFromDate());
			hMap.put("toDate", budgetVO.getToDate());

			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					List accheadVOList = null;
					BudgetDetailsVO budgetVO = new BudgetDetailsVO();
					budgetVO.setBudgetID(rs.getInt("budget_id"));
					budgetVO.setOrgID(rs.getInt("org_id"));
					budgetVO.setBudgetName(rs.getString("budget_name"));
					budgetVO.setDescription(rs.getString("description"));
					budgetVO.setBudgetType(rs.getString("budget_type"));
					budgetVO.setIsDeleted(rs.getInt("is_deleted"));
					budgetVO.setCreatedBy(rs.getInt("created_by"));
					budgetVO.setUpdatedBy(rs.getInt("updated_by"));
					budgetVO.setFromDate(rs.getString("from_date"));
					budgetVO.setToDate(rs.getString("to_date"));
					budgetVO.setUpdatorName(rs.getString("updatorName"));
					budgetVO.setCreatorName(rs.getString("creatorName"));
					budgetVO.setUpdateDate(rs.getTimestamp("update_date"));
					budgetVO.setCreateDate(rs.getTimestamp("create_date"));
					budgetVO.setCcCategoryID(rs.getInt("cc_category_id"));
					budgetVO.setProjectID(rs.getInt("parent_id"));
					budgetVO.setFrequency(rs.getString("frequency"));
					budgetVO.setCategoryName(rs.getString("category_name"));
					budgetVO.setProjectAcronyms(rs.getString("project_acronyms"));
					return budgetVO;

				}

			};

			budgetList = namedParameterJdbcTemplate.query(str, hMap, Rmapper);

		} catch (Exception e) {
			log.error("Exception in getting budget details " + e);
		}

		log.debug("Exit : public int getBudgetDetailsList(BudgetDetailsVO budgetVO)");
		return budgetList;
	}

	// -----Get Budget Details -------//
	public BudgetDetailsVO getBudgetDetails(BudgetDetailsVO budgetVO) {
		log.debug("Entry : public BudgetDetailsVO getBudgetDetails(BudgetDetailsVO budgetVO)");
		BudgetDetailsVO budgetDetailsVO = new BudgetDetailsVO();
		try {

			String str = "SELECT b.*,p.category_name FROM(SELECT b.*,u.full_name AS creatorName,um.full_name AS updatorName FROM budget_details b,um_users u,um_users um WHERE b.created_by=u.id AND b.updated_by=um.id AND b.budget_id=:budgetID AND org_id=:orgID  AND b.is_deleted=0 ) AS b LEFT JOIN (SELECT * FROM cc_category_details WHERE org_id=:orgID AND is_deleted=0) AS p ON b.cc_category_id=p.id";

			Map hMap = new HashMap();
			hMap.put("orgID", budgetVO.getOrgID());
			hMap.put("budgetID", budgetVO.getBudgetID());
			

			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					List accheadVOList = null;
					BudgetDetailsVO budgetVO = new BudgetDetailsVO();
					budgetVO.setBudgetID(rs.getInt("budget_id"));
					budgetVO.setOrgID(rs.getInt("org_id"));
					budgetVO.setBudgetName(rs.getString("budget_name"));
					budgetVO.setDescription(rs.getString("description"));
					budgetVO.setBudgetType(rs.getString("budget_type"));
					budgetVO.setIsDeleted(rs.getInt("is_deleted"));
					budgetVO.setCreatedBy(rs.getInt("created_by"));
					budgetVO.setUpdatedBy(rs.getInt("updated_by"));
					budgetVO.setFromDate(rs.getString("from_date"));
					budgetVO.setToDate(rs.getString("to_date"));
					budgetVO.setUpdatorName(rs.getString("updatorName"));
					budgetVO.setCreatorName(rs.getString("creatorName"));
					budgetVO.setUpdateDate(rs.getTimestamp("update_date"));
					budgetVO.setCreateDate(rs.getTimestamp("create_date"));
					budgetVO.setCcCategoryID(rs.getInt("cc_category_id"));
					budgetVO.setProjectID(rs.getInt("parent_id"));
					budgetVO.setFrequency(rs.getString("frequency"));
					budgetVO.setCategoryName(rs.getString("category_name"));
					budgetVO.setProjectAcronyms(rs.getString("project_acronyms"));
					return budgetVO;

				}

			};

			budgetDetailsVO = (BudgetDetailsVO) namedParameterJdbcTemplate.queryForObject(str, hMap, Rmapper);

		} catch (Exception e) {
			log.error("Exception in getting budget details " + e);
		}

		log.debug("Exit : public BudgetDetailsVO getBudgetDetails(BudgetDetailsVO budgetVO)");
		return budgetDetailsVO;
	}

	// -----Get Budget Details for group -------//
		public List getBudgetDetailsForGroup(BudgetDetailsVO budgetVO) {
			log.debug("Entry : public List getBudgetDetailsForGroup(BudgetDetailsVO budgetVO)");
			List budgetList=new ArrayList();
			String condition="";
			try {
				if(budgetVO.getProjectAcronyms()!=null){
					condition=" and b.project_acronyms=:projectAccronyms ";
				}
				String str = "SELECT b.*,u.full_name AS creatorName,um.full_name AS updatorName,bb.group_id,bb.from_date as fromDate,bb.to_date as toDate FROM budget_details b,um_users u,um_users um,budget_item_details bb WHERE b.budget_id=bb.budget_id AND b.created_by=u.id AND b.updated_by=um.id "+condition+" AND bb.group_id=:groupID AND b.org_id=:orgID  AND b.is_deleted=0 AND  :txDate BETWEEN bb.from_date AND bb.to_date ;";

				Map hMap = new HashMap();
				hMap.put("orgID", budgetVO.getOrgID());
				hMap.put("budgetID", budgetVO.getBudgetID());
				hMap.put("groupID", budgetVO.getGroupID());
				hMap.put("txDate", budgetVO.getBudgetDate());
				hMap.put("projectAccronyms", budgetVO.getProjectAcronyms());
				

				RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						List accheadVOList = null;
						BudgetDetailsVO budgetVO = new BudgetDetailsVO();
						budgetVO.setBudgetID(rs.getInt("budget_id"));
						budgetVO.setOrgID(rs.getInt("org_id"));
						budgetVO.setBudgetName(rs.getString("budget_name"));
						budgetVO.setDescription(rs.getString("description"));
						budgetVO.setBudgetType(rs.getString("budget_type"));
						budgetVO.setIsDeleted(rs.getInt("is_deleted"));
						budgetVO.setCreatedBy(rs.getInt("created_by"));
						budgetVO.setUpdatedBy(rs.getInt("updated_by"));
						budgetVO.setFromDate(rs.getString("fromDate"));
						budgetVO.setToDate(rs.getString("toDate"));
						budgetVO.setUpdatorName(rs.getString("updatorName"));
						budgetVO.setCreatorName(rs.getString("creatorName"));
						budgetVO.setUpdateDate(rs.getTimestamp("update_date"));
						budgetVO.setCreateDate(rs.getTimestamp("create_date"));
						budgetVO.setCcCategoryID(rs.getInt("cc_category_id"));
						budgetVO.setProjectID(rs.getInt("parent_id"));
						budgetVO.setFrequency(rs.getString("frequency"));
						budgetVO.setProjectAcronyms(rs.getString("project_acronyms"));
						return budgetVO;

					}

				};

				budgetList = namedParameterJdbcTemplate.query(str, hMap, Rmapper);

			} catch (Exception e) {
				log.error("Exception in getting budget details " + e);
			}

			log.debug("Exit : public List getBudgetDetailsForGroup(BudgetDetailsVO budgetVO)");
			return budgetList;
		}
	
	// -----Add Budget Item Details -------//
	public int addBudgetItemDetails(BudgetDetailsVO budgetVO) {
		log.debug("Entry : public int addBudgetItemDetails(BudgetDetailsVO budgetVO)");
		int success = 0;
		try {
			java.util.Date today = new java.util.Date();
			final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			budgetVO.setCreateDate(currentTimestamp);
			budgetVO.setUpdateDate(currentTimestamp);

			String sql = "INSERT INTO budget_item_details "
					+ "(org_id,budget_id,ledger_id ,budget_amount,budget_percent,from_date,to_date,group_id,in_out_type )  "
					+ "VALUES (:orgID,:budgetID,:ledgerID,:budgetAmt,:budgetPercent,:fromDate,:toDate,:groupID,:inOutType)";
			log.debug("Sql: " + sql);

			SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(budgetVO);
			KeyHolder keyHolder = new GeneratedKeyHolder();
			getNamedParameterJdbcTemplate().update(sql, fileParameters, keyHolder);
			success = keyHolder.getKey().intValue();

			// getNamedParameterJdbcTemplate().batchUpdate(

		} catch (Exception e) {
			log.error("Exception in adding budget details " + e);
		}

		log.debug("Exit : public int addBudgetItemDetails(BudgetDetailsVO budgetVO)");
		return success;
	}

	// -----Add bulk Budget Item Details -------//
	public int addBulkBudgetItemDetails(List budgetItemList) {
		log.debug("Entry : public int addBulkBudgetItemDetails(BudgetDetailsVO budgetVO)");
		int success = 0;
		try {

			String sql = "INSERT INTO budget_item_details "
					+ "(org_id,budget_id,ledger_id ,budget_amount,budget_percent, from_date, to_date, group_id,in_out_type )  "
					+ "VALUES (:orgID,:budgetID,:ledgerID,:budgetAmt,:budgetPercent,:fromDate,:toDate,:groupID,:inOutType)";
			log.debug("Sql: " + sql);

			// Map array
			Map<String, Object>[] paramMap = new HashMap[budgetItemList.size()];
			for (int i = 0; i < budgetItemList.size(); i++) {
				BudgetDetailsVO budgetVO = (BudgetDetailsVO) budgetItemList.get(i);
				Map<String, Object> tempMap = new HashMap<String, Object>();
				tempMap.put("orgID", budgetVO.getOrgID());
				tempMap.put("budgetID", budgetVO.getBudgetID());
				tempMap.put("ledgerID", budgetVO.getLedgerID());
				tempMap.put("budgetAmt", budgetVO.getBudgetAmt());
				tempMap.put("budgetPercent", budgetVO.getBudgetPercent());
				tempMap.put("fromDate", budgetVO.getFromDate());
				tempMap.put("toDate", budgetVO.getToDate());
				tempMap.put("groupID", budgetVO.getGroupID());
				paramMap[i] = tempMap;
			}

			namedParameterJdbcTemplate.batchUpdate(sql, paramMap);

		} catch (Exception e) {
			log.error("Exception in adding budget details " + e);
		}

		log.debug("Exit : public int addBulkBudgetItemDetails(BudgetDetailsVO budgetVO)");
		return success;
	}

	// -----Update Budget Item Details -------//
	public int updateBudgetItemDetails(BudgetDetailsVO budgetVO) {
		log.debug("Entry : public int updateBudgetItemDetails(BudgetDetailsVO budgetVO)");
		int success = 0;
		try {
			java.util.Date today = new java.util.Date();
			final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());

			String str = "UPDATE budget_item_details SET budget_amount=:budgetAmount,"
					+ " budget_percent=:budgetPercent,from_date=:fromDate,to_date=:toDate,group_id=:groupID,in_out_type=:inOutType WHERE item_id=:itemID and org_id=:orgID and ledger_id=:ledgerID ;";

			Map hMap = new HashMap();
			hMap.put("itemID", budgetVO.getBudgetItemID());
			hMap.put("orgID", budgetVO.getOrgID());
			hMap.put("ledgerID", budgetVO.getLedgerID());
			hMap.put("budgetPercent", budgetVO.getBudgetPercent());
			hMap.put("budgetAmount", budgetVO.getBudgetAmt());
			hMap.put("fromDate", budgetVO.getFromDate());
			hMap.put("toDate", budgetVO.getToDate());
			hMap.put("groupID", budgetVO.getGroupID());
			hMap.put("inOutType", budgetVO.getInOutType());

			success = namedParameterJdbcTemplate.update(str, hMap);

		} catch (Exception e) {
			log.error("Exception in updating budget details " + e);
		}

		log.debug("Exit : public int updateBudgetItemDetails(BudgetDetailsVO budgetVO)");
		return success;
	}

	// -----Delete Budget Item Details -------//
		public int deleteBudgetItemDetails(BudgetDetailsVO budgetVO) {
			log.debug("Entry : public int deleteBudgetItemDetails(BudgetDetailsVO budgetVO)");
			int success = 0;
			try {

				java.util.Date today = new java.util.Date();
				final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());

				String str = "DELETE from budget_item_details WHERE budget_id=:budgetID and group_id=:groupID and org_id=:orgID ";

				Map hMap = new HashMap();
				hMap.put("budgetID", budgetVO.getBudgetID());
				hMap.put("orgID", budgetVO.getOrgID());
				hMap.put("groupID", budgetVO.getGroupID());

				success = namedParameterJdbcTemplate.update(str, hMap);

			} catch (Exception e) {
				log.error("Exception in deleting budget details " + e);
			}

			log.debug("Exit : public int deleteBudgetItemDetails(BudgetDetailsVO budgetVO)");
			return success;
		}

	// -----Get Budget Item Details List-------//
	public List getBudgetItemDetailsList(BudgetDetailsVO budgetVO) {
		log.debug("Entry : public List getBudgetItemDetailsList(BudgetDetailsVO budgetVO)");
		List budgetList = null;
		try {
			SocietyVO societyVO = societyService.getSocietyDetails(budgetVO.getOrgID());
			String str = "SELECT bi.*,a.ledger_name FROM budget_item_details bi,account_ledgers_"
					+ societyVO.getDataZoneID()
					+ " a WHERE a.org_id=bi.org_id AND a.id=bi.ledger_id AND bi.org_id=:orgID AND bi.budget_id=:budgetID  AND bi.is_deleted=0 ORDER BY item_id;";

			Map hMap = new HashMap();
			hMap.put("orgID", budgetVO.getOrgID());
			hMap.put("budgetID", budgetVO.getBudgetID());

			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					List accheadVOList = null;
					BudgetDetailsVO budgetVO = new BudgetDetailsVO();
					budgetVO.setBudgetItemID(rs.getInt("item_id"));
					budgetVO.setBudgetID(rs.getInt("budget_id"));
					budgetVO.setOrgID(rs.getInt("org_id"));
					budgetVO.setLedgerID(rs.getInt("ledger_id"));
					budgetVO.setLedgerName(rs.getString("ledger_name"));
					budgetVO.setBudgetAmt(rs.getBigDecimal("budget_amount"));
					budgetVO.setBudgetPercent(rs.getBigDecimal("budget_percent"));
					return budgetVO;

				}

			};

			budgetList = namedParameterJdbcTemplate.query(str, hMap, Rmapper);

		} catch (Exception e) {
			log.error("Exception in getting budget details " + e);
		}

		log.debug("Exit : public int getBudgetItemDetailsList(BudgetDetailsVO budgetVO)");
		return budgetList;
	}

	// -----Get Budget Details -------//
	public BudgetDetailsVO getBudgetItemDetails(BudgetDetailsVO budgetVO) {
		log.debug("Entry : public BudgetDetailsVO getBudgetItemDetails(BudgetDetailsVO budgetVO)");
		BudgetDetailsVO budgetDetailsVO = new BudgetDetailsVO();
		try {

			SocietyVO societyVO = societyService.getSocietyDetails(budgetVO.getOrgID());
			String str = "SELECT bi.*,a.ledger_name FROM budget_item_details bi,account_ledgers_"
					+ societyVO.getDataZoneID()
					+ " a WHERE a.org_id=bi.org_id AND a.id=bi.ledger_id AND bi.org_id=:orgID AND bi.budget_id=:budgetID  AND bi.is_deleted=0 ORDER BY item_id;";

			Map hMap = new HashMap();
			hMap.put("orgID", budgetVO.getOrgID());
			hMap.put("budgetID", budgetVO.getBudgetID());

			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					List accheadVOList = null;
					BudgetDetailsVO budgetVO = new BudgetDetailsVO();
					budgetVO.setBudgetItemID(rs.getInt("item_id"));
					budgetVO.setBudgetID(rs.getInt("budget_id"));
					budgetVO.setOrgID(rs.getInt("org_id"));
					budgetVO.setLedgerID(rs.getInt("ledger_id"));
					budgetVO.setLedgerName(rs.getString("ledger_name"));
					budgetVO.setBudgetAmt(rs.getBigDecimal("budget_amount"));
					budgetVO.setBudgetPercent(rs.getBigDecimal("budget_percent"));
					return budgetVO;

				}

			};

			budgetDetailsVO = (BudgetDetailsVO) namedParameterJdbcTemplate.queryForObject(str, hMap, Rmapper);

		} catch (Exception e) {
			log.error("Exception in getting budget details " + e);
		}

		log.debug("Exit : public BudgetDetailsVO getBudgetItemDetails(BudgetDetailsVO budgetVO)");
		return budgetDetailsVO;
	}

	public List getIncomeExpenditureReport(String fromDate, String toDate, int orgID, int incExpGroupID,
			String rptType) {
		List incExpList = new ArrayList();
		try {
			log.debug("Entry:public List monthlyIncomeExpenditureReportCredit(int year, int id, int month)" + fromDate
					+ toDate);

			String strWhereClause = "";
			String orderbyClause = "";
			if (rptType.equalsIgnoreCase("P")) {
				strWhereClause = "groupID";
				orderbyClause = "seq_no";
			} else if (rptType.equalsIgnoreCase("PL")) {
				strWhereClause = "category_id";
				orderbyClause = "category_id";
			} else if (rptType.equalsIgnoreCase("E")) {
				strWhereClause = "ledger_id";
				orderbyClause = "seq_no";
			} else if (rptType.equalsIgnoreCase("IE")) {
				strWhereClause = "groupID";
				orderbyClause = "category_id";
			}
			SocietyVO societyVO = societyService.getSocietyDetails(orgID);
			String sql = " SELECT s.*,SUM(tx_amount) AS txAmt,SUM(budget_amount) AS budgetAmount,budget_id,root_id FROM(SELECT tb.*,SUM(b.budget_amount) as budget_amount,b.budget_id,SUM(tb.txAmount) AS tx_amount FROM (SELECT ag.root_id, ag.category_name,ac.category_id,ac.group_name,al.ledger_name AS ledgerName,ale.*,SUM(ale.amount) AS txAmount ,ac.id AS groupID,ac.seq_no  "
					+ " FROM account_transactions_" + societyVO.getDataZoneID() + " ats,account_ledger_entry_"
					+ societyVO.getDataZoneID() + " ale,account_ledgers_" + societyVO.getDataZoneID()
					+ " al,account_groups ac ,account_category ag "
					+ " WHERE ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:orgID and ats.tx_id=ale.tx_id AND ale.ledger_id=al.id AND al.sub_group_id=ac.id AND ats.is_deleted=0 AND  ac.category_id=ag.category_id and "
					+ " ( ats.tx_date >= :fromDate AND ats.tx_date <= :toDate) GROUP BY al.sub_group_id) AS tb LEFT JOIN (SELECT bb.* FROM budget_details b,budget_item_details bb WHERE b.org_id=:orgID AND b.from_date=:fromDate AND b.to_date=:toDate AND b.budget_id=bb.budget_id AND b.is_deleted=0 and bb.is_deleted=0 AND b.budget_type='P') AS b ON b.group_id=tb.groupID  GROUP BY tb.groupID) AS s  group by "
					+ strWhereClause + " order by " + orderbyClause;

			log.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("fromDate", fromDate);
			hashMap.put("toDate", toDate);
			// hashMap.put("rootID", incExpGroupID);
			hashMap.put("orgID", orgID);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					ReportDetailsVO rptVO = new ReportDetailsVO();
					rptVO.setLedgerName(rs.getString("ledgerName"));
					rptVO.setCategoryName(rs.getString("category_name"));
					rptVO.setGroupName(rs.getString("group_name"));
					rptVO.setTxType(rs.getString("type"));
					rptVO.setTotalAmount(rs.getBigDecimal("txAmt"));
					rptVO.setCategoryID(rs.getInt("category_id"));
					rptVO.setGroupID(rs.getInt("groupID"));
					rptVO.setLedgerID(rs.getInt("ledger_id"));
					rptVO.setBudgetAmt(rs.getBigDecimal("budgetAmount"));
					rptVO.setRootID(rs.getInt("root_id"));
					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);
			// logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			log.info("Exception in getMonthyIncExpCr: " + ex);
		} catch (Exception ex) {
			log.error("Exception in monthlyIncomeExpenditureReportCredit : " + ex);
		}
		log.debug("Exit:public List monthlyIncomeExpenditureReport(int year, int id, int month)" + incExpList.size());
		return incExpList;

	}

	public List getMonthWiseBudgetReport(String fromDate, String toDate, int budgetID, int orgID, String rptType) {
		List incExpList = new ArrayList();
		try {
			log.debug(
					"Entry: public List getMonthWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
							+ fromDate + toDate);

			String strWhereClause = "";
			String orderbyClause = "";
			String budgetOrderClause="";
			if (rptType.equalsIgnoreCase("P")) {
				strWhereClause = "groupID";
				orderbyClause = "seq_no";
				budgetOrderClause="g.id ) AS b ON b.group_id=tx.groupID GROUP BY b.group_id;";
			} else if (rptType.equalsIgnoreCase("PL")) {
				strWhereClause = "category_id";
				orderbyClause = "category_id";
				budgetOrderClause="g.category_id ) AS b ON b.category_id=tx.category_id GROUP BY b.category_id;";
			}
			SocietyVO societyVO = societyService.getSocietyDetails(orgID);
			String sql = " SELECT root_group_name,root_id,b.group_name,groupID,b.category_id,b.category_name,IFNULL(SUM(Apr),0) AS Apr,IFNULL(SUM(May),0) AS May,IFNULL(SUM(Jun),0) AS Jun,IFNULL(SUM(Jul),0) AS Jul,IFNULL(SUM(Aug),0) AS Aug,IFNULL(SUM(Sept),0) AS Sept,IFNULL(SUM(OCT),0) AS Oct,IFNULL(SUM(Nov),0) AS Nov, IFNULL(SUM(Decs),0) AS Decs,IFNULL(SUM(Jan),0) AS Jan,IFNULL(SUM(Feb),0) AS Feb,IFNULL(SUM(Mar),0) AS Mar,item_id,IFNULL(SUM(budget_amount),0) AS budgetAmount,"
					+ " IFNULL(SUM(bdgtApr),0) AS bdgtApr,IFNULL(SUM(bdgtMay),0) AS bdgtMay,IFNULL(SUM(bdgtJun),0) AS bdgtJun,IFNULL(SUM(bdgtJul),0) AS bdgtJul,IFNULL(SUM(bdgtAug),0) AS bdgtAug,IFNULL(SUM(bdgtSept),0) AS bdgtSept,IFNULL(SUM(bdgtOct),0) AS bdgtOct,IFNULL(SUM(bdgtNov),0) AS bdgtNov,IFNULL(SUM(bdgtDec),0) AS bdgtDec,IFNULL(SUM(bdgtJan),0) AS bdgtJan,IFNULL(SUM(bdgtFeb),0) AS bdgtFeb,IFNULL(SUM(bdgtMar),0) AS bdgtMar , in_out_type, b.group_name as groupName,b.category_name as categoryName,b.category_id as categoryID,b.group_id "
					+ " FROM(SELECT account_root_group.root_group_name,account_root_group.root_id, account_groups.group_name,SUM(al.opening_balance) AS opening_balance,account_groups.id AS groupID,c.category_name,c.category_id,SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'April' THEN ale.amount ELSE '0.00' END) AS Apr,SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'May' THEN ale.amount ELSE '0.00' END) AS May,"
					+ " SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'June' THEN ale.amount ELSE '0.00' END) AS Jun,SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'July' THEN ale.amount ELSE '0.00' END) AS Jul,SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'August' THEN ale.amount ELSE '0.00' END) AS Aug,SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'September' THEN ale.amount ELSE '0.00' END) AS Sept, "
					+ " SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'October' THEN ale.amount ELSE '0.00' END) AS OCT,SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'November' THEN ale.amount ELSE '0.00' END) AS Nov,SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'December' THEN ale.amount ELSE '0.00' END) AS Decs,SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'January' THEN ale.amount ELSE '0.00' END) AS Jan,"
					+ " SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'February' THEN ale.amount ELSE '0.00' END) AS Feb,SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'March' THEN ale.amount ELSE '0.00' END) AS Mar FROM account_transactions_"
					+ societyVO.getDataZoneID() + " ats,account_ledger_entry_" + societyVO.getDataZoneID()
					+ " ale,account_ledgers_" + societyVO.getDataZoneID()
					+ " al ,account_groups,account_root_group ,account_category c "
					+ " WHERE ats.tx_date BETWEEN :fromDate AND :toDate AND ats.is_deleted=0  AND ats.org_id=ale.org_id AND ale.org_id=al.org_id AND account_groups.category_id=c.category_id AND al.org_id=:orgID AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND al.sub_group_id = account_groups.id AND account_groups.root_id = account_root_group.root_id GROUP BY  account_groups.id ORDER BY account_root_group.root_id,account_groups.id) AS tx Right JOIN "
					+ " (SELECT bb.*,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'April' THEN budget_amount ELSE '0.00' END) AS bdgtApr,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'May' THEN budget_amount ELSE '0.00' END) AS bdgtMay,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'June' THEN budget_amount ELSE '0.00' END) AS bdgtJun,"
					+ " SUM(CASE WHEN MONTHNAME(bb.from_date) = 'July' THEN budget_amount ELSE '0.00' END) AS bdgtJul,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'August' THEN budget_amount ELSE '0.00' END) AS bdgtAug,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'September' THEN budget_amount ELSE '0.00' END) AS bdgtSept,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'October' THEN budget_amount ELSE '0.00' END) AS bdgtOCT,"
					+ " SUM(CASE WHEN MONTHNAME(bb.from_date) = 'November' THEN budget_amount ELSE '0.00' END) AS bdgtNov,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'December' THEN budget_amount ELSE '0.00' END) AS bdgtDec,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'January' THEN budget_amount ELSE '0.00' END) AS bdgtJan,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'February' THEN budget_amount ELSE '0.00' END) AS bdgtFeb,"
					+ " SUM(CASE WHEN MONTHNAME(bb.from_date) = 'March' THEN budget_amount ELSE '0.00' END) AS bdgtMar ,g.group_name,g.category_id,c.category_name  FROM budget_details b,budget_item_details bb,account_groups g,account_category c WHERE g.id=bb.group_id AND g.category_id=c.category_id AND b.org_id=:orgID AND b.from_date=:fromDate AND b.to_date=:toDate AND b.budget_id=bb.budget_id AND b.budget_id=:budgetID AND b.is_deleted=0 AND bb.is_deleted=0  GROUP BY "+budgetOrderClause+") AS b ON b.group_id=tx.groupID GROUP BY tx."
					+ strWhereClause + "; ";

			log.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("fromDate", fromDate);
			hashMap.put("toDate", toDate);
			hashMap.put("budgetID", budgetID);
			hashMap.put("orgID", orgID);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					BudgetDetailsVO rptVO = new BudgetDetailsVO();

					rptVO.setCategoryName(rs.getString("categoryName"));
					rptVO.setGroupName(rs.getString("groupName"));

					rptVO.setCategoryID(rs.getInt("categoryID"));
					rptVO.setGroupID(rs.getInt("group_id"));

					rptVO.setBudgetAmt(rs.getBigDecimal("budgetAmount"));
					rptVO.setRootID(rs.getInt("root_id"));
					rptVO.setJanActual(rs.getBigDecimal("Jan"));
					rptVO.setFebActual(rs.getBigDecimal("Feb"));
					rptVO.setMarActual(rs.getBigDecimal("Mar"));
					rptVO.setAprActual(rs.getBigDecimal("Apr"));
					rptVO.setMayActual(rs.getBigDecimal("May"));
					rptVO.setJunActual(rs.getBigDecimal("Jun"));
					rptVO.setJulActual(rs.getBigDecimal("Jul"));
					rptVO.setAugActual(rs.getBigDecimal("Aug"));
					rptVO.setSeptActual(rs.getBigDecimal("Sept"));
					rptVO.setOctActual(rs.getBigDecimal("Oct"));
					rptVO.setNovActual(rs.getBigDecimal("Nov"));
					rptVO.setDecActual(rs.getBigDecimal("Decs"));

					rptVO.setJanAmt(rs.getBigDecimal("bdgtJan"));
					rptVO.setFebAmt(rs.getBigDecimal("bdgtFeb"));
					rptVO.setMarAmt(rs.getBigDecimal("bdgtMar"));
					rptVO.setAprAmt(rs.getBigDecimal("bdgtApr"));
					rptVO.setMayAmt(rs.getBigDecimal("bdgtMay"));
					rptVO.setJunAmt(rs.getBigDecimal("bdgtJun"));
					rptVO.setJulAmt(rs.getBigDecimal("bdgtJul"));
					rptVO.setAugAmt(rs.getBigDecimal("bdgtAug"));
					rptVO.setSeptAmt(rs.getBigDecimal("bdgtSept"));
					rptVO.setOctAmt(rs.getBigDecimal("bdgtOct"));
					rptVO.setNovAmt(rs.getBigDecimal("bdgtNov"));
					rptVO.setDecAmt(rs.getBigDecimal("bdgtDec"));
					rptVO.setInOutType(rs.getString("in_out_type"));
					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);
			// logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			log.info(
					"Exception in public List getMonthWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType): "
							+ ex);
		} catch (Exception ex) {
			log.error(
					"Exception in public List getMonthWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType) : "
							+ ex);
		}
		log.debug(
				"Exit: public List getMonthWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
						+ incExpList.size());
		return incExpList;

	}

	public List getQuarterWiseBudgetReport(String fromDate, String toDate, int budgetID, int orgID, String rptType) {
		List incExpList = new ArrayList();
		try {
			log.debug(
					"Entry: public List getQuarterWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
							+ fromDate + toDate);

			String strWhereClause = "";
			String orderbyClause = "";
			String budgetOrderClause="";
			if (rptType.equalsIgnoreCase("P")) {
				strWhereClause = "groupID";
				orderbyClause = "seq_no";
				budgetOrderClause="g.id ) AS b ON b.group_id=tx.groupID GROUP BY b.group_id;";
			} else if (rptType.equalsIgnoreCase("PL")) {
				strWhereClause = "category_id";
				orderbyClause = "category_id";
				budgetOrderClause="g.category_id ) AS b ON b.category_id=tx.category_id GROUP BY b.category_id;";
			}
			SocietyVO societyVO = societyService.getSocietyDetails(orgID);
			String sql = " SELECT root_group_name,root_id,b.group_name,groupID,b.category_id,b.category_name,IFNULL(SUM(Q1),0) AS q1,IFNULL(SUM(Q2),0) AS q2,IFNULL(SUM(Q3),0) AS q3,IFNULL(SUM(Q4),0) AS q4,item_id,IFNULL(SUM(budget_amount),0) AS budgetAmount,"
					+ " IFNULL(SUM(bdgtQ1),0) AS bdgtQ1,IFNULL(SUM(bdgtQ2),0) AS bdgtQ2,IFNULL(SUM(bdgtQ3),0) AS bdgtQ3,IFNULL(SUM(bdgtQ4),0) AS bdgtQ4 , in_out_type, b.group_name as groupName,b.category_name as categoryName,b.category_id as categoryID,b.group_id "
					+ " FROM(SELECT account_root_group.root_group_name,account_root_group.root_id, account_groups.group_name,SUM(al.opening_balance) AS opening_balance,account_groups.id AS groupID,c.category_name,c.category_id,"
					+ " SUM(CASE WHEN QUARTER(ats.tx_date) = '2' THEN ale.amount ELSE '0.00' END) AS Q1,"
					+ " SUM(CASE WHEN QUARTER(ats.tx_date) = '3' THEN ale.amount ELSE '0.00' END) AS Q2,"
					+ " SUM(CASE WHEN QUARTER(ats.tx_date) = '4' THEN ale.amount ELSE '0.00' END) AS Q3,"
					+ " SUM(CASE WHEN QUARTER(ats.tx_date) = '1' THEN ale.amount ELSE '0.00' END) AS Q4"
					+ " FROM account_transactions_" + societyVO.getDataZoneID() + " ats,account_ledger_entry_"
					+ societyVO.getDataZoneID() + " ale,account_ledgers_" + societyVO.getDataZoneID()
					+ " al ,account_groups,account_root_group ,account_category c "
					+ " WHERE ats.tx_date BETWEEN :fromDate AND :toDate AND ats.is_deleted=0  AND ats.org_id=ale.org_id AND ale.org_id=al.org_id AND account_groups.category_id=c.category_id AND al.org_id=:orgID AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND al.sub_group_id = account_groups.id AND account_groups.root_id = account_root_group.root_id GROUP BY  account_groups.id ORDER BY account_root_group.root_id,account_groups.id) AS tx Right JOIN "
					+ " (SELECT bb.*,SUM(CASE WHEN QUARTER(bb.from_date) = '2' THEN budget_amount ELSE '0.00' END) AS bdgtQ1,"
					+ " SUM(CASE WHEN QUARTER(bb.from_date) = '3' THEN budget_amount ELSE '0.00' END) AS bdgtQ2,"
					+ " SUM(CASE WHEN QUARTER(bb.from_date) = '4' THEN budget_amount ELSE '0.00' END) AS bdgtQ3,"
					+ " SUM(CASE WHEN QUARTER(bb.from_date) = '1' THEN budget_amount ELSE '0.00' END) AS bdgtQ4 ,g.group_name,g.category_id,c.category_name  FROM budget_details b,budget_item_details bb,account_groups g,account_category c WHERE g.id=bb.group_id AND g.category_id=c.category_id AND b.org_id=:orgID AND b.from_date=:fromDate AND b.to_date=:toDate AND b.budget_id=bb.budget_id AND b.budget_id=:budgetID AND b.is_deleted=0 AND bb.is_deleted=0  GROUP BY "+budgetOrderClause;

			log.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("fromDate", fromDate);
			hashMap.put("toDate", toDate);
			hashMap.put("budgetID", budgetID);
			hashMap.put("orgID", orgID);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					BudgetDetailsVO rptVO = new BudgetDetailsVO();

					rptVO.setCategoryName(rs.getString("categoryName"));
					rptVO.setGroupName(rs.getString("groupName"));

					rptVO.setCategoryID(rs.getInt("categoryID"));
					rptVO.setGroupID(rs.getInt("group_id"));

					rptVO.setBudgetAmt(rs.getBigDecimal("budgetAmount"));
					rptVO.setRootID(rs.getInt("root_id"));
					rptVO.setQ1Actual(rs.getBigDecimal("q1"));
					rptVO.setQ2Actual(rs.getBigDecimal("q2"));
					rptVO.setQ3Actual(rs.getBigDecimal("q3"));
					rptVO.setQ4Actual(rs.getBigDecimal("q4"));

					rptVO.setQ1Amt(rs.getBigDecimal("bdgtQ1"));
					rptVO.setQ2Amt(rs.getBigDecimal("bdgtQ2"));
					rptVO.setQ3Amt(rs.getBigDecimal("bdgtQ3"));
					rptVO.setQ4Amt(rs.getBigDecimal("bdgtQ4"));
					
					rptVO.setInOutType(rs.getString("in_out_type"));
					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);
			// logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			log.info(
					"Exception in public List getQuarterWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType): "
							+ ex);
		} catch (Exception ex) {
			log.error(
					"Exception in public List getQuarterWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType) : "
							+ ex);
		}
		log.debug(
				"Exit: public List getQuarterWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
						+ incExpList.size());
		return incExpList;

	}

	public List getHalfYearWiseBudgetReport(String fromDate, String toDate, int budgetID, int orgID, String rptType) {
		List incExpList = new ArrayList();
		try {
			log.debug(
					"Entry: public List getHalfYearWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
							+ fromDate + toDate);

			String strWhereClause = "";
			String orderbyClause = "";
			String budgetOrderClause="";
			if (rptType.equalsIgnoreCase("P")) {
				strWhereClause = "groupID";
				orderbyClause = "seq_no";
				budgetOrderClause="g.id ) AS b ON b.group_id=tx.groupID GROUP BY b.group_id;";
			} else if (rptType.equalsIgnoreCase("PL")) {
				strWhereClause = "category_id";
				orderbyClause = "category_id";
				budgetOrderClause="g.category_id ) AS b ON b.category_id=tx.category_id GROUP BY b.category_id;";
			}
			SocietyVO societyVO = societyService.getSocietyDetails(orgID);
			String sql = " SELECT root_group_name,root_id,b.group_name,groupID,b.category_id,b.category_name,IFNULL(SUM(H1),0) AS h1,IFNULL(SUM(H2),0) AS h2,item_id,IFNULL(SUM(budget_amount),0) AS budgetAmount,"
					+ " IFNULL(SUM(bdgtH1),0) AS bdgtH1,IFNULL(SUM(bdgtH2),0) AS bdgtH2 , in_out_type, b.group_name as groupName,b.category_name as categoryName,b.category_id as categoryID,b.group_id "
					+ " FROM(SELECT account_root_group.root_group_name,account_root_group.root_id, account_groups.group_name,SUM(al.opening_balance) AS opening_balance,account_groups.id AS groupID,c.category_name,c.category_id,"
					+ " SUM(CASE WHEN ( QUARTER(ats.tx_date) = '2' or quarter(ats.tx_date)='3' ) THEN ale.amount ELSE '0.00' END) AS H1,"
					+ " SUM(CASE WHEN ( QUARTER(ats.tx_date) = '1' or quarter(ats.tx_date)='4' ) THEN ale.amount ELSE '0.00' END) AS H2"
					+ " FROM account_transactions_" + societyVO.getDataZoneID() + " ats,account_ledger_entry_"
					+ societyVO.getDataZoneID() + " ale,account_ledgers_" + societyVO.getDataZoneID()
					+ " al ,account_groups,account_root_group ,account_category c "
					+ " WHERE ats.tx_date BETWEEN :fromDate AND :toDate AND ats.is_deleted=0  AND ats.org_id=ale.org_id AND ale.org_id=al.org_id AND account_groups.category_id=c.category_id AND al.org_id=:orgID AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND al.sub_group_id = account_groups.id AND account_groups.root_id = account_root_group.root_id GROUP BY  account_groups.id ORDER BY account_root_group.root_id,account_groups.id) AS tx Right JOIN "
					+ " (SELECT bb.*,"
					+ " SUM(CASE WHEN ( QUARTER(bb.from_date) = '2' or quarter(bb.from_date)='3' ) THEN budget_amount ELSE '0.00' END) AS bdgtH1,"
					+ " SUM(CASE WHEN ( QUARTER(bb.from_date) = '1' or quarter(bb.from_date)='4' ) THEN budget_amount ELSE '0.00' END) AS bdgtH2 "
					+ " ,g.group_name,g.category_id,c.category_name  FROM budget_details b,budget_item_details bb,account_groups g,account_category c WHERE g.id=bb.group_id AND g.category_id=c.category_id AND b.org_id=:orgID AND b.from_date=:fromDate AND b.to_date=:toDate AND b.budget_id=bb.budget_id AND b.budget_id=:budgetID AND b.is_deleted=0 AND bb.is_deleted=0  GROUP BY "+budgetOrderClause;

			log.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("fromDate", fromDate);
			hashMap.put("toDate", toDate);
			hashMap.put("budgetID", budgetID);
			hashMap.put("orgID", orgID);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					BudgetDetailsVO rptVO = new BudgetDetailsVO();

					rptVO.setCategoryName(rs.getString("categoryName"));
					rptVO.setGroupName(rs.getString("groupName"));

					rptVO.setCategoryID(rs.getInt("categoryID"));
					rptVO.setGroupID(rs.getInt("group_id"));

					rptVO.setBudgetAmt(rs.getBigDecimal("budgetAmount"));
					rptVO.setRootID(rs.getInt("root_id"));
					rptVO.setH1Actual(rs.getBigDecimal("h1"));
					rptVO.setH2Actual(rs.getBigDecimal("h2"));

					rptVO.setH1Amt(rs.getBigDecimal("bdgtH1"));
					rptVO.setH2Amt(rs.getBigDecimal("bdgtH2"));
					
					rptVO.setInOutType(rs.getString("in_out_type"));
					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);
			// logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			log.info(
					"Exception in public List getHalfYearWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType): "
							+ ex);
		} catch (Exception ex) {
			log.error(
					"Exception in public List getHalfYearWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType) : "
							+ ex);
		}
		log.debug(
				"Exit: public List getHalfYearWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
						+ incExpList.size());
		return incExpList;

	}

	public List getYearWiseBudgetReport(String fromDate, String toDate, int budgetID, int orgID, String rptType) {
		List incExpList = new ArrayList();
		try {
			log.debug(
					"Entry: public List getYearWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
							+ fromDate + toDate);

			String strWhereClause = "";
			String orderbyClause = "";
			String budgetOrderClause="";
			if (rptType.equalsIgnoreCase("P")) {
				strWhereClause = "groupID";
				orderbyClause = "seq_no";
				budgetOrderClause="g.id ) AS b ON b.group_id=tx.groupID GROUP BY b.group_id;";
			} else if (rptType.equalsIgnoreCase("PL")) {
				strWhereClause = "category_id";
				orderbyClause = "category_id";
				budgetOrderClause="g.category_id ) AS b ON b.category_id=tx.category_id GROUP BY b.category_id;";
			}
			SocietyVO societyVO = societyService.getSocietyDetails(orgID);
			String sql = " SELECT root_group_name,root_id,b.group_name,groupID,b.category_id,b.category_name,IFNULL(SUM(Y),0) AS y,item_id,IFNULL(SUM(budget_amount),0) AS budgetAmount,"
					+ " IFNULL(SUM(bdgtY),0) AS bdgtY , in_out_type , b.group_name as groupName,b.category_name as categoryName,b.category_id as categoryID,b.group_id"
					+ " FROM(SELECT account_root_group.root_group_name,account_root_group.root_id, account_groups.group_name,SUM(al.opening_balance) AS opening_balance,account_groups.id AS groupID,c.category_name,c.category_id,"
					+ " SUM(ale.amount) AS Y " +
					" FROM account_transactions_" + societyVO.getDataZoneID() + " ats,account_ledger_entry_"
					+ societyVO.getDataZoneID() + " ale,account_ledgers_" + societyVO.getDataZoneID()
					+ " al ,account_groups,account_root_group ,account_category c "
					+ " WHERE ats.tx_date BETWEEN :fromDate AND :toDate AND ats.is_deleted=0  AND ats.org_id=ale.org_id AND ale.org_id=al.org_id AND account_groups.category_id=c.category_id AND al.org_id=:orgID AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND al.sub_group_id = account_groups.id AND account_groups.root_id = account_root_group.root_id GROUP BY  account_groups.id ORDER BY account_root_group.root_id,account_groups.id) AS tx Right JOIN "
					+ " (SELECT bb.*," + " SUM(budget_amount) AS bdgtY "
					+ " ,g.group_name,g.category_id,c.category_name  FROM budget_details b,budget_item_details bb,account_groups g,account_category c WHERE g.id=bb.group_id AND g.category_id=c.category_id AND b.org_id=:orgID AND b.from_date=:fromDate AND b.to_date=:toDate AND b.budget_id=bb.budget_id AND b.budget_id=:budgetID AND b.is_deleted=0 AND bb.is_deleted=0  GROUP BY "+budgetOrderClause ;

			log.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("fromDate", fromDate);
			hashMap.put("toDate", toDate);
			hashMap.put("budgetID", budgetID);
			hashMap.put("orgID", orgID);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					BudgetDetailsVO rptVO = new BudgetDetailsVO();

					rptVO.setCategoryName(rs.getString("categoryName"));
					rptVO.setGroupName(rs.getString("groupName"));

					rptVO.setCategoryID(rs.getInt("categoryID"));
					rptVO.setGroupID(rs.getInt("group_id"));

					rptVO.setBudgetAmt(rs.getBigDecimal("budgetAmount"));
					rptVO.setRootID(rs.getInt("root_id"));
					rptVO.setActualAmount(rs.getBigDecimal("y"));
					rptVO.setBudgetAmt(rs.getBigDecimal("bdgtY"));
					
					rptVO.setInOutType(rs.getString("in_out_type"));

					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);
			// logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			log.info(
					"Exception in public List getYearWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType): "
							+ ex);
		} catch (Exception ex) {
			log.error(
					"Exception in public List getYearWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType) : "
							+ ex);
		}
		log.debug(
				"Exit: public List getYearWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
						+ incExpList.size());
		return incExpList;

	}

	public List getMonthWiseBudgetReportWithCCCategory(String fromDate, String toDate, int budgetID, int orgID,
			String rptType, List categoryList,String projectAcronyms,String budgetType) {
		List incExpList = new ArrayList();
		try {
			log.debug(
					"Entry: public List getMonthWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
							+ fromDate + toDate);

			String strWhereClause = "";
			String orderbyClause = "";
			String budgetOrderClause="";
			String ccCondition="";
			if (rptType.equalsIgnoreCase("P")) {
				strWhereClause = "groupID";
				orderbyClause = "seq_no";
				budgetOrderClause="g.id ) AS b ON b.group_id=project.groupID GROUP BY b.group_id;";
			} else if (rptType.equalsIgnoreCase("PL")) {
				strWhereClause = "category_id";
				orderbyClause = "category_id";
				budgetOrderClause="g.category_id ) AS b ON b.category_id=project.category_id GROUP BY b.category_id;";
			}

			String orCondition = "";
			String andCondition = "";
			if(budgetType.equalsIgnoreCase("CC")){
				ccCondition="";
			for (int i = 0; categoryList.size() > i; i++) {
				ProjectDetailsVO projVO = (ProjectDetailsVO) categoryList.get(i);
				if (categoryList.size() == 1) {
					orCondition = orCondition + " pm.project_acronyms='" + projVO.getProjectAcronyms().trim() + "'";
					andCondition = andCondition + " ((al.tx_le_tags LIKE '%#" + projVO.getProjectAcronyms().trim()
							+ "%')OR(l.project_tags LIKE '%#" + projVO.getProjectAcronyms().trim() + "%'))";

				} else {

					if (i == 0) {
						orCondition = orCondition + " pm.project_acronyms='" + projVO.getProjectAcronyms().trim() + "'";
						andCondition = andCondition + " ((al.tx_le_tags LIKE '%#" + projVO.getProjectAcronyms().trim()
								+ "%')OR(l.project_tags LIKE '%#" + projVO.getProjectAcronyms().trim() + "%'))";
					} else {
						orCondition = orCondition + " or pm.project_acronyms='" + projVO.getProjectAcronyms().trim()
								+ "'";
						andCondition = andCondition + " or ((al.tx_le_tags LIKE '%#"
								+ projVO.getProjectAcronyms().trim() + "%')OR(l.project_tags LIKE '%#"
								+ projVO.getProjectAcronyms().trim() + "%'))";
					}
				}

			}
			}else if(budgetType.equalsIgnoreCase("PC")){
				ccCondition="SUM";
				List<String> result = Arrays.asList(projectAcronyms.split("\\s*,\\s*"));
		        if(result.size()==1){
		        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronyms.trim()+"'";
		        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(l.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
		        	
		        }else{
		        for(int i=0;result.size()>i;i++){
		        	String projectAcronym=result.get(i);
		        	if(i==0){
		        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronym.trim()+"'";
		        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projectAcronym.trim()+"%')OR(l.project_tags LIKE '%#"+projectAcronym.trim()+"%'))";
		        	}else{
		        	orCondition=orCondition+" or pm.project_acronyms='"+projectAcronym.trim()+"'";
		        	andCondition=andCondition+" and ((al.tx_le_tags LIKE '%#"+projectAcronym.trim()+"%')OR(l.project_tags LIKE '%#"+projectAcronym.trim()+"%'))";
		        	}}}
			}

			SocietyVO societyVO = societyService.getSocietyDetails(orgID);
			String sql = " SELECT project.*,IFNULL(SUM(Apr),0) AS Apr,IFNULL(SUM(May),0) AS May,IFNULL(SUM(Jun),0) AS Jun,IFNULL(SUM(Jul),0) AS Jul,IFNULL(SUM(Aug),0) AS Aug,IFNULL(SUM(Sept),0) AS Sept,IFNULL(SUM(OCT),0) AS Oct,IFNULL(SUM(Nov),0) AS Nov, IFNULL(SUM(Decs),0) AS Decs,IFNULL(SUM(Jan),0) AS Jan,IFNULL(SUM(Feb),0) AS Feb,IFNULL(SUM(Mar),0) AS Mar,item_id,IFNULL(SUM(budget_amount),0) AS budgetAmount,"
					+ " IFNULL(SUM(bdgtApr),0) AS bdgtApr,IFNULL(SUM(bdgtMay),0) AS bdgtMay,IFNULL(SUM(bdgtJun),0) AS bdgtJun,IFNULL(SUM(bdgtJul),0) AS bdgtJul,IFNULL(SUM(bdgtAug),0) AS bdgtAug,IFNULL(SUM(bdgtSept),0) AS bdgtSept,IFNULL(SUM(bdgtOct),0) AS bdgtOct,IFNULL(SUM(bdgtNov),0) AS bdgtNov,IFNULL(SUM(bdgtDec),0) AS bdgtDec,IFNULL(SUM(bdgtJan),0) AS bdgtJan,IFNULL(SUM(bdgtFeb),0) AS bdgtFeb,IFNULL(SUM(bdgtMar),0) AS bdgtMar , in_out_type, b.group_name as groupName,b.category_name as categoryName,b.category_id as categoryID,b.group_id FROM(SELECT projects.*,SUM(actualAmount) AS actualAmt ,SUM(txAmt) AS txAmount,SUM(CASE WHEN MONTHNAME(tx_date) = 'April' THEN actualAmount ELSE '0.00' END) AS Apr, "
					+ " SUM(CASE WHEN MONTHNAME(tx_date) = 'May' THEN actualAmount ELSE '0.00' END) AS May,SUM(CASE WHEN MONTHNAME(tx_date) = 'June' THEN actualAmount ELSE '0.00' END) AS Jun, "
					+ " SUM(CASE WHEN MONTHNAME(tx_date) = 'July' THEN actualAmount ELSE '0.00' END) AS Jul,SUM(CASE WHEN MONTHNAME(tx_date) = 'August' THEN actualAmount ELSE '0.00' END) AS Aug, "
					+ " SUM(CASE WHEN MONTHNAME(tx_date) = 'September' THEN actualAmount ELSE '0.00' END) AS Sept,SUM(CASE WHEN MONTHNAME(tx_date) = 'October' THEN actualAmount ELSE '0.00' END) AS OCT, "
					+ " SUM(CASE WHEN MONTHNAME(tx_date) = 'November' THEN actualAmount ELSE '0.00' END) AS Nov,SUM(CASE WHEN MONTHNAME(tx_date) = 'December' THEN actualAmount ELSE '0.00' END) AS Decs,"
					+ " SUM(CASE WHEN MONTHNAME(tx_date) = 'January' THEN actualAmount ELSE '0.00' END) AS Jan,SUM(CASE WHEN MONTHNAME(tx_date) = 'February' THEN actualAmount ELSE '0.00' END) AS Feb,SUM(CASE WHEN MONTHNAME(tx_date) = 'March' THEN actualAmount ELSE '0.00' END) AS Mar  "
					+ " FROM(SELECT h.org_id,h.project_id,h.project_name,h.project_acronyms,h.ledgerID,h.ledgerName,SUM(h.txAmt) AS txAmt, (SUM(h.actualAmount)) AS actualAmount ,h.groupID ,h.root_id ,h.project_cost,h.group_name, h.seq_no,category_id,category_name,tx_date FROM (SELECT a.org_id,pm.project_id,pm.project_name,pm.project_acronyms,l.id AS ledgerID,l.ledger_name AS ledgerName,"+ccCondition+"(al.amount) AS txAmt, "+ccCondition+"((al.amount)) AS actualAmount ,ac.id AS groupID ,ac.root_id ,pm.project_cost,ac.group_name, ac.seq_no,ag.category_id,ag.category_name ,a.tx_date "
					+ " FROM account_ledgers_" + societyVO.getDataZoneID() + " l,account_transactions_"
					+ societyVO.getDataZoneID() + " a,account_ledger_entry_" + societyVO.getDataZoneID()
					+ " al,account_groups ac,account_category ag ,project_master_details pm WHERE a.tx_id=al.tx_id "
					+ " AND a.org_id=:orgID AND a.org_id=pm.org_id AND l.sub_group_id=ac.id AND ac.category_id=ag.category_id AND a.is_deleted=0 AND al.ledger_id=l.id "
					+ " AND ( a.tx_date >=:fromDate AND a.tx_date <=:toDate) AND (" + orCondition + ") AND ("
					+ andCondition + " ) GROUP BY al.id,l.id) AS h GROUP BY tx_date,ledgerID) AS projects GROUP BY "
					+ strWhereClause + " ORDER BY " + orderbyClause + ") AS project Right JOIN "
					+ " (SELECT bb.*,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'April' THEN budget_amount ELSE '0.00' END) AS bdgtApr,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'May' THEN budget_amount ELSE '0.00' END) AS bdgtMay,"
					+ " SUM(CASE WHEN MONTHNAME(bb.from_date) = 'June' THEN budget_amount ELSE '0.00' END) AS bdgtJun,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'July' THEN budget_amount ELSE '0.00' END) AS bdgtJul,"
					+ " SUM(CASE WHEN MONTHNAME(bb.from_date) = 'August' THEN budget_amount ELSE '0.00' END) AS bdgtAug,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'September' THEN budget_amount ELSE '0.00' END) AS bdgtSept,"
					+ " SUM(CASE WHEN MONTHNAME(bb.from_date) = 'October' THEN budget_amount ELSE '0.00' END) AS bdgtOct,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'November' THEN budget_amount ELSE '0.00' END) AS bdgtNov,"
					+ " SUM(CASE WHEN MONTHNAME(bb.from_date) = 'December' THEN budget_amount ELSE '0.00' END) AS bdgtDec,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'January' THEN budget_amount ELSE '0.00' END) AS bdgtJan,"
					+ " SUM(CASE WHEN MONTHNAME(bb.from_date) = 'February' THEN budget_amount ELSE '0.00' END) AS bdgtFeb,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'March' THEN budget_amount ELSE '0.00' END) AS bdgtMar ,g.group_name,g.category_id,c.category_name  FROM budget_details b,budget_item_details bb,account_groups g,account_category c WHERE g.id=bb.group_id AND g.category_id=c.category_id AND b.org_id=:orgID AND b.from_date=:fromDate AND b.to_date=:toDate AND b.budget_id=bb.budget_id AND b.budget_id=:budgetID AND b.is_deleted=0 AND bb.is_deleted=0  GROUP BY "+budgetOrderClause;

			log.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("fromDate", fromDate);
			hashMap.put("toDate", toDate);
			hashMap.put("budgetID", budgetID);
			hashMap.put("orgID", orgID);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					BudgetDetailsVO rptVO = new BudgetDetailsVO();

					rptVO.setCategoryName(rs.getString("categoryName"));
					rptVO.setGroupName(rs.getString("groupName"));

					rptVO.setCategoryID(rs.getInt("categoryID"));
					rptVO.setGroupID(rs.getInt("group_id"));

					rptVO.setBudgetAmt(rs.getBigDecimal("budgetAmount"));
					rptVO.setRootID(rs.getInt("root_id"));
					rptVO.setJanActual(rs.getBigDecimal("Jan"));
					rptVO.setFebActual(rs.getBigDecimal("Feb"));
					rptVO.setMarActual(rs.getBigDecimal("Mar"));
					rptVO.setAprActual(rs.getBigDecimal("Apr"));
					rptVO.setMayActual(rs.getBigDecimal("May"));
					rptVO.setJunActual(rs.getBigDecimal("Jun"));
					rptVO.setJulActual(rs.getBigDecimal("Jul"));
					rptVO.setAugActual(rs.getBigDecimal("Aug"));
					rptVO.setSeptActual(rs.getBigDecimal("Sept"));
					rptVO.setOctActual(rs.getBigDecimal("Oct"));
					rptVO.setNovActual(rs.getBigDecimal("Nov"));
					rptVO.setDecActual(rs.getBigDecimal("Decs"));

					rptVO.setJanAmt(rs.getBigDecimal("bdgtJan"));
					rptVO.setFebAmt(rs.getBigDecimal("bdgtFeb"));
					rptVO.setMarAmt(rs.getBigDecimal("bdgtMar"));
					rptVO.setAprAmt(rs.getBigDecimal("bdgtApr"));
					rptVO.setMayAmt(rs.getBigDecimal("bdgtMay"));
					rptVO.setJunAmt(rs.getBigDecimal("bdgtJun"));
					rptVO.setJulAmt(rs.getBigDecimal("bdgtJul"));
					rptVO.setAugAmt(rs.getBigDecimal("bdgtAug"));
					rptVO.setSeptAmt(rs.getBigDecimal("bdgtSept"));
					rptVO.setOctAmt(rs.getBigDecimal("bdgtOct"));
					rptVO.setNovAmt(rs.getBigDecimal("bdgtNov"));
					rptVO.setDecAmt(rs.getBigDecimal("bdgtDec"));
					
					rptVO.setInOutType(rs.getString("in_out_type"));
					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);
			// logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			log.info(
					"Exception in public List getMonthWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType): "
							+ ex);
		} catch (Exception ex) {
			log.error(
					"Exception in public List getMonthWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType) : "
							+ ex);
		}
		log.debug(
				"Exit: public List getMonthWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
						+ incExpList.size());
		return incExpList;

	}

	public List getQuarterWiseBudgetReportWithCCCategory(String fromDate, String toDate, int budgetID, int orgID,
			String rptType, List categoryList,String projectAcronyms,String budgetType) {
		List incExpList = new ArrayList();
		try {
			log.debug(
					"Entry: public List getQuarterWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
							+ fromDate + toDate);

			String strWhereClause = "";
			String orderbyClause = "";
			String budgetOrderClause="";
			String ccCondition="";
			if (rptType.equalsIgnoreCase("P")) {
				strWhereClause = "groupID";
				orderbyClause = "seq_no";
				budgetOrderClause="g.id ) AS b ON b.group_id=project.groupID GROUP BY b.group_id;";
			} else if (rptType.equalsIgnoreCase("PL")) {
				strWhereClause = "category_id";
				orderbyClause = "category_id";
				budgetOrderClause="g.category_id ) AS b ON b.category_id=project.category_id GROUP BY b.category_id;";
			}

			String orCondition = "";
			String andCondition = "";

			if(budgetType.equalsIgnoreCase("CC")){
				ccCondition="";
				for (int i = 0; categoryList.size() > i; i++) {
					ProjectDetailsVO projVO = (ProjectDetailsVO) categoryList.get(i);
					if (categoryList.size() == 1) {
						orCondition = orCondition + " pm.project_acronyms='" + projVO.getProjectAcronyms().trim() + "'";
						andCondition = andCondition + " ((al.tx_le_tags LIKE '%#" + projVO.getProjectAcronyms().trim()
								+ "%')OR(l.project_tags LIKE '%#" + projVO.getProjectAcronyms().trim() + "%'))";

					} else {

						if (i == 0) {
							orCondition = orCondition + " pm.project_acronyms='" + projVO.getProjectAcronyms().trim() + "'";
							andCondition = andCondition + " ((al.tx_le_tags LIKE '%#" + projVO.getProjectAcronyms().trim()
									+ "%')OR(l.project_tags LIKE '%#" + projVO.getProjectAcronyms().trim() + "%'))";
						} else {
							orCondition = orCondition + " or pm.project_acronyms='" + projVO.getProjectAcronyms().trim()
									+ "'";
							andCondition = andCondition + " or ((al.tx_le_tags LIKE '%#"
									+ projVO.getProjectAcronyms().trim() + "%')OR(l.project_tags LIKE '%#"
									+ projVO.getProjectAcronyms().trim() + "%'))";
						}
					}

				}
				}else if(budgetType.equalsIgnoreCase("PC")){
					ccCondition="SUM";
					List<String> result = Arrays.asList(projectAcronyms.split("\\s*,\\s*"));
			        if(result.size()==1){
			        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronyms.trim()+"'";
			        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(l.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
			        	
			        }else{
			        for(int i=0;result.size()>i;i++){
			        	String projectAcronym=result.get(i);
			        	if(i==0){
			        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronym.trim()+"'";
			        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projectAcronym.trim()+"%')OR(l.project_tags LIKE '%#"+projectAcronym.trim()+"%'))";
			        	}else{
			        	orCondition=orCondition+" or pm.project_acronyms='"+projectAcronym.trim()+"'";
			        	andCondition=andCondition+" and ((al.tx_le_tags LIKE '%#"+projectAcronym.trim()+"%')OR(l.project_tags LIKE '%#"+projectAcronym.trim()+"%'))";
			        	}}}
				}

			SocietyVO societyVO = societyService.getSocietyDetails(orgID);
			String sql = " SELECT project.*,IFNULL(SUM(Q1),0) AS q1,IFNULL(SUM(Q2),0) AS q2,IFNULL(SUM(Q3),0) AS q3,IFNULL(SUM(Q4),0) AS q4,item_id,IFNULL(SUM(budget_amount),0) AS budgetAmount,"
					+ " IFNULL(SUM(bdgtQ1),0) AS bdgtQ1,IFNULL(SUM(bdgtQ2),0) AS bdgtQ2,IFNULL(SUM(bdgtQ3),0) AS bdgtQ3,IFNULL(SUM(bdgtQ4),0) AS bdgtQ4 , in_out_type, b.group_name as groupName,b.category_name as categoryName,b.category_id as categoryID,b.group_id FROM(SELECT projects.*,SUM(actualAmount) AS actualAmt ,SUM(txAmt) AS txAmount,SUM(CASE WHEN Quarter(tx_date) = '2' THEN actualAmount ELSE '0.00' END) AS Q1, "
					+ " SUM(CASE WHEN QUARTER(tx_date) = '3' THEN actualAmount ELSE '0.00' END) AS Q2,SUM(CASE WHEN Quarter(tx_date) = '4' THEN actualAmount ELSE '0.00' END) AS Q3, "
					+ " SUM(CASE WHEN QUARTER(tx_date) = '1' THEN actualAmount ELSE '0.00' END) AS Q4  "
					+ " FROM(SELECT h.org_id,h.project_id,h.project_name,h.project_acronyms,h.ledgerID,h.ledgerName,SUM(h.txAmt) AS txAmt, (SUM(h.actualAmount)) AS actualAmount ,h.groupID ,h.root_id ,h.project_cost,h.group_name, h.seq_no,category_id,category_name,tx_date FROM (SELECT a.org_id,pm.project_id,pm.project_name,pm.project_acronyms,l.id AS ledgerID,l.ledger_name AS ledgerName,"+ccCondition+"(al.amount) AS txAmt, "+ccCondition+"((al.amount)) AS actualAmount ,ac.id AS groupID ,ac.root_id ,pm.project_cost,ac.group_name, ac.seq_no,ag.category_id,ag.category_name ,a.tx_date "
					+ " FROM account_ledgers_" + societyVO.getDataZoneID() + " l,account_transactions_"
					+ societyVO.getDataZoneID() + " a,account_ledger_entry_" + societyVO.getDataZoneID()
					+ " al,account_groups ac,account_category ag ,project_master_details pm WHERE a.tx_id=al.tx_id "
					+ " AND a.org_id=:orgID AND a.org_id=pm.org_id AND l.sub_group_id=ac.id AND ac.category_id=ag.category_id AND a.is_deleted=0 AND al.ledger_id=l.id "
					+ " AND ( a.tx_date >=:fromDate AND a.tx_date <=:toDate) AND (" + orCondition + ") AND ("
					+ andCondition + " ) GROUP BY al.id,l.id) AS h GROUP BY tx_date,ledgerID) AS projects GROUP BY "
					+ strWhereClause + " ORDER BY " + orderbyClause + ") AS project Right JOIN "
					+ " (SELECT bb.*,SUM(CASE WHEN QUARTER(bb.from_date) = '2' THEN budget_amount ELSE '0.00' END) AS bdgtQ1,SUM(CASE WHEN QUARTER(bb.from_date) = '3' THEN budget_amount ELSE '0.00' END) AS bdgtQ2,"
					+ " SUM(CASE WHEN QUARTER(bb.from_date) = '4' THEN budget_amount ELSE '0.00' END) AS bdgtQ3,SUM(CASE WHEN QUARTER(bb.from_date) = '1' THEN budget_amount ELSE '0.00' END) AS bdgtQ4 "
					+ " ,g.group_name,g.category_id,c.category_name  FROM budget_details b,budget_item_details bb,account_groups g,account_category c WHERE g.id=bb.group_id AND g.category_id=c.category_id AND b.org_id=:orgID AND b.from_date=:fromDate AND b.to_date=:toDate AND b.budget_id=bb.budget_id AND b.budget_id=:budgetID AND b.is_deleted=0 AND bb.is_deleted=0  GROUP BY "+budgetOrderClause;

			log.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("fromDate", fromDate);
			hashMap.put("toDate", toDate);
			hashMap.put("budgetID", budgetID);
			hashMap.put("orgID", orgID);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					BudgetDetailsVO rptVO = new BudgetDetailsVO();

					rptVO.setCategoryName(rs.getString("categoryName"));
					rptVO.setGroupName(rs.getString("groupName"));

					rptVO.setCategoryID(rs.getInt("categoryID"));
					rptVO.setGroupID(rs.getInt("group_id"));

					rptVO.setBudgetAmt(rs.getBigDecimal("budgetAmount"));
					rptVO.setRootID(rs.getInt("root_id"));
					rptVO.setQ1Actual(rs.getBigDecimal("q1"));
					rptVO.setQ2Actual(rs.getBigDecimal("q2"));
					rptVO.setQ3Actual(rs.getBigDecimal("q3"));
					rptVO.setQ4Actual(rs.getBigDecimal("q4"));

					rptVO.setQ1Amt(rs.getBigDecimal("bdgtQ1"));
					rptVO.setQ2Amt(rs.getBigDecimal("bdgtQ2"));
					rptVO.setQ3Amt(rs.getBigDecimal("bdgtQ3"));
					rptVO.setQ4Amt(rs.getBigDecimal("bdgtQ4"));
					
					rptVO.setInOutType(rs.getString("in_out_type"));
					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);
			// logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			log.info(
					"Exception in public List getQuarterWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType): "
							+ ex);
		} catch (Exception ex) {
			log.error(
					"Exception in public List getQuarterWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType) : "
							+ ex);
		}
		log.debug(
				"Exit: public List getQuarterWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
						+ incExpList.size());
		return incExpList;

	}

	public List getHalfYearWiseBudgetReportWithCCCategory(String fromDate, String toDate, int budgetID, int orgID,
			String rptType, List categoryList,String projectAcronyms,String budgetType) {
		List incExpList = new ArrayList();
		try {
			log.debug(
					"Entry: public List getHalfYearWiseBudgetReportWithCCCategory(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
							+ fromDate + toDate);

			String strWhereClause = "";
			String orderbyClause = "";
			String budgetOrderClause="";
			String ccCondition="";
			if (rptType.equalsIgnoreCase("P")) {
				strWhereClause = "groupID";
				orderbyClause = "seq_no";
				budgetOrderClause="g.id ) AS b ON b.group_id=project.groupID GROUP BY b.group_id;";
			} else if (rptType.equalsIgnoreCase("PL")) {
				strWhereClause = "category_id";
				orderbyClause = "category_id";
				budgetOrderClause="g.category_id ) AS b ON b.category_id=project.category_id GROUP BY b.category_id;";
			}

			String orCondition = "";
			String andCondition = "";

			if(budgetType.equalsIgnoreCase("CC")){
				ccCondition="";
				for (int i = 0; categoryList.size() > i; i++) {
					ProjectDetailsVO projVO = (ProjectDetailsVO) categoryList.get(i);
					if (categoryList.size() == 1) {
						orCondition = orCondition + " pm.project_acronyms='" + projVO.getProjectAcronyms().trim() + "'";
						andCondition = andCondition + " ((al.tx_le_tags LIKE '%#" + projVO.getProjectAcronyms().trim()
								+ "%')OR(l.project_tags LIKE '%#" + projVO.getProjectAcronyms().trim() + "%'))";

					} else {

						if (i == 0) {
							orCondition = orCondition + " pm.project_acronyms='" + projVO.getProjectAcronyms().trim() + "'";
							andCondition = andCondition + " ((al.tx_le_tags LIKE '%#" + projVO.getProjectAcronyms().trim()
									+ "%')OR(l.project_tags LIKE '%#" + projVO.getProjectAcronyms().trim() + "%'))";
						} else {
							orCondition = orCondition + " or pm.project_acronyms='" + projVO.getProjectAcronyms().trim()
									+ "'";
							andCondition = andCondition + " or ((al.tx_le_tags LIKE '%#"
									+ projVO.getProjectAcronyms().trim() + "%')OR(l.project_tags LIKE '%#"
									+ projVO.getProjectAcronyms().trim() + "%'))";
						}
					}

				}
				}else if(budgetType.equalsIgnoreCase("PC")){
					ccCondition="SUM";
					List<String> result = Arrays.asList(projectAcronyms.split("\\s*,\\s*"));
			        if(result.size()==1){
			        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronyms.trim()+"'";
			        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(l.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
			        	
			        }else{
			        for(int i=0;result.size()>i;i++){
			        	String projectAcronym=result.get(i);
			        	if(i==0){
			        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronym.trim()+"'";
			        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projectAcronym.trim()+"%')OR(l.project_tags LIKE '%#"+projectAcronym.trim()+"%'))";
			        	}else{
			        	orCondition=orCondition+" or pm.project_acronyms='"+projectAcronym.trim()+"'";
			        	andCondition=andCondition+" and ((al.tx_le_tags LIKE '%#"+projectAcronym.trim()+"%')OR(l.project_tags LIKE '%#"+projectAcronym.trim()+"%'))";
			        	}}}
				}

			SocietyVO societyVO = societyService.getSocietyDetails(orgID);
			String sql = " SELECT project.*,IFNULL(SUM(H1),0) AS h1,IFNULL(SUM(H2),0) AS h2,item_id,IFNULL(SUM(budget_amount),0) AS budgetAmount,"
					+ " IFNULL(SUM(bdgtH1),0) AS bdgtH1,IFNULL(SUM(bdgtH2),0) AS bdgtH2 , in_out_type, b.group_name as groupName,b.category_name as categoryName,b.category_id as categoryID,b.group_id FROM(SELECT projects.*,SUM(actualAmount) AS actualAmt ,SUM(txAmt) AS txAmount,SUM(CASE WHEN( Quarter(tx_date) = '1' or QUARTER(tx_date)='4') THEN actualAmount ELSE '0.00' END) AS H2, "
					+ " SUM(CASE WHEN (QUARTER(tx_date)='2' or QUARTER(tx_date) = '3') THEN actualAmount ELSE '0.00' END) AS H1  "
					+ " FROM(SELECT h.org_id,h.project_id,h.project_name,h.project_acronyms,h.ledgerID,h.ledgerName,SUM(h.txAmt) AS txAmt, (SUM(h.actualAmount)) AS actualAmount ,h.groupID ,h.root_id ,h.project_cost,h.group_name, h.seq_no,category_id,category_name,tx_date FROM (SELECT a.org_id,pm.project_id,pm.project_name,pm.project_acronyms,l.id AS ledgerID,l.ledger_name AS ledgerName,"+ccCondition+"(al.amount) AS txAmt, "+ccCondition+"((al.amount)) AS actualAmount ,ac.id AS groupID ,ac.root_id ,pm.project_cost,ac.group_name, ac.seq_no,ag.category_id,ag.category_name ,a.tx_date "
					+ " FROM account_ledgers_" + societyVO.getDataZoneID() + " l,account_transactions_"
					+ societyVO.getDataZoneID() + " a,account_ledger_entry_" + societyVO.getDataZoneID()
					+ " al,account_groups ac,account_category ag ,project_master_details pm WHERE a.tx_id=al.tx_id "
					+ " AND a.org_id=:orgID AND a.org_id=pm.org_id AND l.sub_group_id=ac.id AND ac.category_id=ag.category_id AND a.is_deleted=0 AND al.ledger_id=l.id "
					+ " AND ( a.tx_date >=:fromDate AND a.tx_date <=:toDate) AND (" + orCondition + ") AND ("
					+ andCondition + " ) GROUP BY al.id,l.id) AS h GROUP BY tx_date,ledgerID) AS projects GROUP BY "
					+ strWhereClause + " ORDER BY " + orderbyClause + ") AS project Right JOIN "
					+ " (SELECT bb.*,SUM(CASE WHEN ( QUARTER(bb.from_date) = '2' or QUARTER(bb.from_date)='3') THEN budget_amount ELSE '0.00' END) AS bdgtH1,SUM(CASE WHEN ( QUARTER(bb.from_date) = '4' or QUARTER(bb.from_date)='1') THEN budget_amount ELSE '0.00' END) AS bdgtH2 "
					+

					" ,g.group_name,g.category_id,c.category_name  FROM budget_details b,budget_item_details bb,account_groups g,account_category c WHERE g.id=bb.group_id AND g.category_id=c.category_id AND b.org_id=:orgID AND b.from_date=:fromDate AND b.to_date=:toDate AND b.budget_id=bb.budget_id AND b.budget_id=:budgetID AND b.is_deleted=0 AND bb.is_deleted=0  GROUP BY "+budgetOrderClause;

			log.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("fromDate", fromDate);
			hashMap.put("toDate", toDate);
			hashMap.put("budgetID", budgetID);
			hashMap.put("orgID", orgID);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					BudgetDetailsVO rptVO = new BudgetDetailsVO();

					rptVO.setCategoryName(rs.getString("categoryName"));
					rptVO.setGroupName(rs.getString("groupName"));

					rptVO.setCategoryID(rs.getInt("categoryID"));
					rptVO.setGroupID(rs.getInt("group_id"));

					rptVO.setBudgetAmt(rs.getBigDecimal("budgetAmount"));
					rptVO.setRootID(rs.getInt("root_id"));
					rptVO.setH1Actual(rs.getBigDecimal("h1"));
					rptVO.setH2Actual(rs.getBigDecimal("h2"));

					rptVO.setH1Amt(rs.getBigDecimal("bdgtH1"));
					rptVO.setH2Amt(rs.getBigDecimal("bdgtH2"));
					
					rptVO.setInOutType(rs.getString("in_out_type"));
					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);
			// logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			log.info(
					"Exception in public List getHalfYearWiseBudgetReportWithCCCategory(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType): "
							+ ex);
		} catch (Exception ex) {
			log.error(
					"Exception in public List getHalfYearWiseBudgetReportWithCCCategory(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType) : "
							+ ex);
		}
		log.debug(
				"Exit: public List getHalfYearWiseBudgetReportWithCCCategory(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
						+ incExpList.size());
		return incExpList;

	}

	public List getYearWiseBudgetReportWithCCCategory(String fromDate, String toDate, int budgetID, int orgID,
			String rptType, List categoryList,String projectAcronyms,String budgetType) {
		List incExpList = new ArrayList();
		try {
			log.debug(
					"Entry: public List getYearWiseBudgetReportWithCCCategory(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
							+ fromDate + toDate);

			String strWhereClause = "";
			String orderbyClause = "";
			String budgetOrderClause="";
			String ccCondition="";
			if (rptType.equalsIgnoreCase("P")) {
				strWhereClause = "groupID";
				orderbyClause = "seq_no";
				budgetOrderClause="g.id ) AS b ON b.group_id=project.groupID GROUP BY b.group_id;";
			} else if (rptType.equalsIgnoreCase("PL")) {
				strWhereClause = "category_id";
				orderbyClause = "category_id";
				budgetOrderClause="g.category_id ) AS b ON b.category_id=project.category_id GROUP BY b.category_id;";
			}

			String orCondition = "";
			String andCondition = "";

			if(budgetType.equalsIgnoreCase("CC")){
				ccCondition="";
				for (int i = 0; categoryList.size() > i; i++) {
					ProjectDetailsVO projVO = (ProjectDetailsVO) categoryList.get(i);
					if (categoryList.size() == 1) {
						orCondition = orCondition + " pm.project_acronyms='" + projVO.getProjectAcronyms().trim() + "'";
						andCondition = andCondition + " ((al.tx_le_tags LIKE '%#" + projVO.getProjectAcronyms().trim()
								+ "%')OR(l.project_tags LIKE '%#" + projVO.getProjectAcronyms().trim() + "%'))";

					} else {

						if (i == 0) {
							orCondition = orCondition + " pm.project_acronyms='" + projVO.getProjectAcronyms().trim() + "'";
							andCondition = andCondition + " ((al.tx_le_tags LIKE '%#" + projVO.getProjectAcronyms().trim()
									+ "%')OR(l.project_tags LIKE '%#" + projVO.getProjectAcronyms().trim() + "%'))";
						} else {
							orCondition = orCondition + " or pm.project_acronyms='" + projVO.getProjectAcronyms().trim()
									+ "'";
							andCondition = andCondition + " or ((al.tx_le_tags LIKE '%#"
									+ projVO.getProjectAcronyms().trim() + "%')OR(l.project_tags LIKE '%#"
									+ projVO.getProjectAcronyms().trim() + "%'))";
						}
					}

				}
				}else if(budgetType.equalsIgnoreCase("PC")){
					ccCondition="SUM";
					List<String> result = Arrays.asList(projectAcronyms.split("\\s*,\\s*"));
			        if(result.size()==1){
			        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronyms.trim()+"'";
			        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(l.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
			        	
			        }else{
			        for(int i=0;result.size()>i;i++){
			        	String projectAcronym=result.get(i);
			        	if(i==0){
			        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronym.trim()+"'";
			        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projectAcronym.trim()+"%')OR(l.project_tags LIKE '%#"+projectAcronym.trim()+"%'))";
			        	}else{
			        	orCondition=orCondition+" or pm.project_acronyms='"+projectAcronym.trim()+"'";
			        	andCondition=andCondition+" and ((al.tx_le_tags LIKE '%#"+projectAcronym.trim()+"%')OR(l.project_tags LIKE '%#"+projectAcronym.trim()+"%'))";
			        	}}}
				}

			SocietyVO societyVO = societyService.getSocietyDetails(orgID);
			String sql = " SELECT project.*,IFNULL(SUM(Y),0) AS y,item_id,IFNULL(SUM(budget_amount),0) AS budgetAmount,"
					+ " IFNULL(SUM(bdgtY),0) AS bdgtY , in_out_type, b.group_name as groupName,b.category_name as categoryName,b.category_id as categoryID,b.group_id FROM(SELECT projects.*,SUM(actualAmount) AS actualAmt ,SUM(txAmt) AS txAmount,SUM( actualAmount ) AS Y "
					+ " FROM(SELECT h.org_id,h.project_id,h.project_name,h.project_acronyms,h.ledgerID,h.ledgerName,SUM(h.txAmt) AS txAmt, (SUM(h.actualAmount)) AS actualAmount ,h.groupID ,h.root_id ,h.project_cost,h.group_name, h.seq_no,category_id,category_name,tx_date FROM (SELECT a.org_id,pm.project_id,pm.project_name,pm.project_acronyms,l.id AS ledgerID,l.ledger_name AS ledgerName,"+ccCondition+"(al.amount) AS txAmt, "+ccCondition+"((al.amount)) AS actualAmount ,ac.id AS groupID ,ac.root_id ,pm.project_cost,ac.group_name, ac.seq_no,ag.category_id,ag.category_name ,a.tx_date "
					+ " FROM account_ledgers_" + societyVO.getDataZoneID() + " l,account_transactions_"
					+ societyVO.getDataZoneID() + " a,account_ledger_entry_" + societyVO.getDataZoneID()
					+ " al,account_groups ac,account_category ag ,project_master_details pm WHERE a.tx_id=al.tx_id "
					+ " AND a.org_id=:orgID AND a.org_id=pm.org_id AND l.sub_group_id=ac.id AND ac.category_id=ag.category_id AND a.is_deleted=0 AND al.ledger_id=l.id "
					+ " AND ( a.tx_date >=:fromDate AND a.tx_date <=:toDate) AND (" + orCondition + ") AND ("
					+ andCondition + " ) GROUP BY al.id,l.id) AS h GROUP BY tx_date,ledgerID) AS projects GROUP BY "
					+ strWhereClause + " ORDER BY " + orderbyClause + ") AS project Right JOIN "
					+ " (SELECT bb.*,SUM( budget_amount ) AS bdgtY "
					+ " ,g.group_name,g.category_id,c.category_name  FROM budget_details b,budget_item_details bb,account_groups g,account_category c WHERE g.id=bb.group_id AND g.category_id=c.category_id AND b.org_id=:orgID AND b.from_date=:fromDate AND b.to_date=:toDate AND b.budget_id=bb.budget_id AND b.budget_id=:budgetID AND b.is_deleted=0 AND bb.is_deleted=0  GROUP BY "+budgetOrderClause;

			
			log.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("fromDate", fromDate);
			hashMap.put("toDate", toDate);
			hashMap.put("budgetID", budgetID);
			hashMap.put("orgID", orgID);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					BudgetDetailsVO rptVO = new BudgetDetailsVO();

					rptVO.setCategoryName(rs.getString("categoryName"));
					rptVO.setGroupName(rs.getString("groupName"));

					rptVO.setCategoryID(rs.getInt("categoryID"));
					rptVO.setGroupID(rs.getInt("group_id"));

					rptVO.setBudgetAmt(rs.getBigDecimal("budgetAmount"));
					rptVO.setRootID(rs.getInt("root_id"));
					rptVO.setActualAmount(rs.getBigDecimal("y"));

					rptVO.setBudgetAmt(rs.getBigDecimal("bdgtY"));
					rptVO.setInOutType(rs.getString("in_out_type"));
					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);
			// logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			log.info(
					"Exception in public List getYearWiseBudgetReportWithCCCategory(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType): "
							+ ex);
		} catch (Exception ex) {
			log.error(
					"Exception in public List getYearWiseBudgetReportWithCCCategory(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType) : "
							+ ex);
		}
		log.debug(
				"Exit: public List getYearWiseBudgetReportWithCCCategory(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
						+ incExpList.size());
		return incExpList;

	}
	
	public List getYearWiseBudgetReportForGroup(String fromDate, String toDate, int budgetID, int orgID, String rptType,int groupID) {
		List incExpList = new ArrayList();
		try {
			log.debug(
					"Entry: public List getYearWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
							+ fromDate + toDate);

			String strWhereClause = "";
			String orderbyClause = "";
			String budgetOrderClause="";
			if (rptType.equalsIgnoreCase("P")) {
				strWhereClause = "groupID";
				orderbyClause = "seq_no";
				budgetOrderClause="g.id ) AS b ON b.group_id=tx.groupID  GROUP BY b.group_id;";
			} else if (rptType.equalsIgnoreCase("PL")) {
				strWhereClause = "category_id";
				orderbyClause = "category_id";
				budgetOrderClause="g.category_id ) AS b ON b.category_id=tx.category_id GROUP BY b.category_id;";
			}
			SocietyVO societyVO = societyService.getSocietyDetails(orgID);
			String sql = " SELECT root_group_name,root_id,b.group_name,groupID,b.category_id,b.category_name,IFNULL(SUM(Y),0) AS y,item_id,IFNULL(SUM(budget_amount),0) AS budgetAmount,"
					+ " IFNULL(SUM(bdgtY),0) AS bdgtY , in_out_type , b.group_name as groupName,b.category_name as categoryName,b.category_id as categoryID,b.group_id"
					+ " FROM(SELECT account_root_group.root_group_name,account_root_group.root_id, account_groups.group_name,SUM(al.opening_balance) AS opening_balance,account_groups.id AS groupID,c.category_name,c.category_id,"
					+ " SUM(ale.amount) AS Y " +
					" FROM account_transactions_" + societyVO.getDataZoneID() + " ats,account_ledger_entry_"
					+ societyVO.getDataZoneID() + " ale,account_ledgers_" + societyVO.getDataZoneID()
					+ " al ,account_groups,account_root_group ,account_category c "
					+ " WHERE ats.tx_date BETWEEN :fromDate AND :toDate AND ats.is_deleted=0  AND ats.org_id=ale.org_id AND ale.org_id=al.org_id AND account_groups.category_id=c.category_id AND al.org_id=:orgID AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND al.sub_group_id = account_groups.id AND account_groups.root_id = account_root_group.root_id GROUP BY  account_groups.id ORDER BY account_root_group.root_id,account_groups.id) AS tx Right JOIN "
					+ " (SELECT bb.*," + " SUM(budget_amount) AS bdgtY "
					+ " ,g.group_name,g.category_id,c.category_name  FROM budget_details b,budget_item_details bb,account_groups g,account_category c WHERE g.id=bb.group_id AND g.category_id=c.category_id AND b.org_id=:orgID AND bb.from_date=:fromDate AND bb.to_date=:toDate AND b.budget_id=bb.budget_id AND b.budget_id=:budgetID AND b.is_deleted=0 AND bb.is_deleted=0 and bb.group_id=:groupID GROUP BY "+budgetOrderClause ;

			log.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("fromDate", fromDate);
			hashMap.put("toDate", toDate);
			hashMap.put("budgetID", budgetID);
			hashMap.put("orgID", orgID);
			hashMap.put("groupID", groupID);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					BudgetDetailsVO rptVO = new BudgetDetailsVO();

					rptVO.setCategoryName(rs.getString("categoryName"));
					rptVO.setGroupName(rs.getString("groupName"));

					rptVO.setCategoryID(rs.getInt("categoryID"));
					rptVO.setGroupID(rs.getInt("group_id"));

					rptVO.setBudgetAmt(rs.getBigDecimal("budgetAmount"));
					rptVO.setRootID(rs.getInt("root_id"));
					rptVO.setActualAmount(rs.getBigDecimal("y"));
					rptVO.setBudgetAmt(rs.getBigDecimal("bdgtY"));
					
					rptVO.setInOutType(rs.getString("in_out_type"));

					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);
			// logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			log.info(
					"Exception in public List getYearWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType): "
							+ ex);
		} catch (Exception ex) {
			log.error(
					"Exception in public List getYearWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType) : "
							+ ex);
		}
		log.debug(
				"Exit: public List getYearWiseBudgetReport(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
						+ incExpList.size());
		return incExpList;

	}
	
	public List getYearWiseBudgetReportWithCCCategoryForGroup(String fromDate, String toDate, int budgetID, int orgID,
			String rptType, List categoryList,String projectAcronyms,String budgetType,int groupID) {
		List incExpList = new ArrayList();
		try {
			log.debug(
					"Entry: public List getYearWiseBudgetReportWithCCCategory(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
							+ fromDate + toDate);

			String strWhereClause = "";
			String orderbyClause = "";
			String budgetOrderClause="";
			String ccCondition="";
			if (rptType.equalsIgnoreCase("P")) {
				strWhereClause = "groupID";
				orderbyClause = "seq_no";
				budgetOrderClause="g.id ) AS b ON b.group_id=project.groupID  GROUP BY b.group_id;";
			} else if (rptType.equalsIgnoreCase("PL")) {
				strWhereClause = "category_id";
				orderbyClause = "category_id";
				budgetOrderClause="g.category_id ) AS b ON b.category_id=project.category_id GROUP BY b.category_id;";
			}

			String orCondition = "";
			String andCondition = "";

			if(budgetType.equalsIgnoreCase("CC")){
				ccCondition="";
				for (int i = 0; categoryList.size() > i; i++) {
					ProjectDetailsVO projVO = (ProjectDetailsVO) categoryList.get(i);
					if (categoryList.size() == 1) {
						orCondition = orCondition + " pm.project_acronyms='" + projVO.getProjectAcronyms().trim() + "'";
						andCondition = andCondition + " ((al.tx_le_tags LIKE '%#" + projVO.getProjectAcronyms().trim()
								+ "%')OR(l.project_tags LIKE '%#" + projVO.getProjectAcronyms().trim() + "%'))";

					} else {

						if (i == 0) {
							orCondition = orCondition + " pm.project_acronyms='" + projVO.getProjectAcronyms().trim() + "'";
							andCondition = andCondition + " ((al.tx_le_tags LIKE '%#" + projVO.getProjectAcronyms().trim()
									+ "%')OR(l.project_tags LIKE '%#" + projVO.getProjectAcronyms().trim() + "%'))";
						} else {
							orCondition = orCondition + " or pm.project_acronyms='" + projVO.getProjectAcronyms().trim()
									+ "'";
							andCondition = andCondition + " or ((al.tx_le_tags LIKE '%#"
									+ projVO.getProjectAcronyms().trim() + "%')OR(l.project_tags LIKE '%#"
									+ projVO.getProjectAcronyms().trim() + "%'))";
						}
					}

				}
				}else if(budgetType.equalsIgnoreCase("PC")){
					ccCondition="SUM";
					List<String> result = Arrays.asList(projectAcronyms.split("\\s*#\\s*"));
			        if(result.size()==1){
			        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronyms.trim()+"'";
			        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(l.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
			        	
			        }else{
			        for(int i=0;result.size()>i;i++){
			        	String projectAcronym=result.get(i);
			        	if(projectAcronym.trim().length()>0){
			        	if(i==1){
			        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronym.trim()+"'";
			        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projectAcronym.trim()+"%')OR(l.project_tags LIKE '%#"+projectAcronym.trim()+"%'))";
			        	}else{
			        	orCondition=orCondition+" or pm.project_acronyms='"+projectAcronym.trim()+"'";
			        	andCondition=andCondition+" and ((al.tx_le_tags LIKE '%#"+projectAcronym.trim()+"%')OR(l.project_tags LIKE '%#"+projectAcronym.trim()+"%'))";
			        	}
			        	}
			        }}
				}

			SocietyVO societyVO = societyService.getSocietyDetails(orgID);
			String sql = " SELECT project.*,IFNULL(SUM(Y),0) AS y,item_id,IFNULL(SUM(budget_amount),0) AS budgetAmount,"
					+ " IFNULL(SUM(bdgtY),0) AS bdgtY , in_out_type, b.group_name as groupName,b.category_name as categoryName,b.category_id as categoryID,b.group_id FROM(SELECT projects.*,SUM(actualAmount) AS actualAmt ,SUM(txAmt) AS txAmount,SUM( actualAmount ) AS Y "
					+ " FROM(SELECT h.org_id,h.project_id,h.project_name,h.project_acronyms,h.ledgerID,h.ledgerName,SUM(h.txAmt) AS txAmt, (SUM(h.actualAmount)) AS actualAmount ,h.groupID ,h.root_id ,h.project_cost,h.group_name, h.seq_no,category_id,category_name,tx_date FROM (SELECT a.org_id,pm.project_id,pm.project_name,pm.project_acronyms,l.id AS ledgerID,l.ledger_name AS ledgerName,"+ccCondition+"(al.amount) AS txAmt, "+ccCondition+"((al.amount)) AS actualAmount ,ac.id AS groupID ,ac.root_id ,pm.project_cost,ac.group_name, ac.seq_no,ag.category_id,ag.category_name ,a.tx_date "
					+ " FROM account_ledgers_" + societyVO.getDataZoneID() + " l,account_transactions_"
					+ societyVO.getDataZoneID() + " a,account_ledger_entry_" + societyVO.getDataZoneID()
					+ " al,account_groups ac,account_category ag ,project_master_details pm WHERE a.tx_id=al.tx_id "
					+ " AND a.org_id=:orgID AND a.org_id=pm.org_id AND l.sub_group_id=ac.id AND ac.category_id=ag.category_id AND a.is_deleted=0 AND al.ledger_id=l.id "
					+ " AND ( a.tx_date >=:fromDate AND a.tx_date <=:toDate) AND (" + orCondition + ") AND ("
					+ andCondition + " ) GROUP BY al.id,l.id) AS h GROUP BY tx_date,ledgerID) AS projects GROUP BY "
					+ strWhereClause + " ORDER BY " + orderbyClause + ") AS project Right JOIN "
					+ " (SELECT bb.*,SUM( budget_amount ) AS bdgtY "
					+ " ,g.group_name,g.category_id,c.category_name  FROM budget_details b,budget_item_details bb,account_groups g,account_category c WHERE g.id=bb.group_id AND g.category_id=c.category_id AND b.org_id=:orgID AND bb.from_date=:fromDate AND bb.to_date=:toDate AND b.budget_id=bb.budget_id AND b.budget_id=:budgetID AND b.is_deleted=0 AND bb.is_deleted=0 and bb.group_id=:groupID GROUP BY "+budgetOrderClause;

			
			log.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("fromDate", fromDate);
			hashMap.put("toDate", toDate);
			hashMap.put("budgetID", budgetID);
			hashMap.put("orgID", orgID);
			hashMap.put("groupID", groupID);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					BudgetDetailsVO rptVO = new BudgetDetailsVO();

					rptVO.setCategoryName(rs.getString("categoryName"));
					rptVO.setGroupName(rs.getString("groupName"));

					rptVO.setCategoryID(rs.getInt("categoryID"));
					rptVO.setGroupID(rs.getInt("group_id"));

					rptVO.setBudgetAmt(rs.getBigDecimal("budgetAmount"));
					rptVO.setRootID(rs.getInt("root_id"));
					rptVO.setActualAmount(rs.getBigDecimal("y"));

					rptVO.setBudgetAmt(rs.getBigDecimal("bdgtY"));
					rptVO.setInOutType(rs.getString("in_out_type"));
					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);
			// logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			log.info(
					"Exception in public List getYearWiseBudgetReportWithCCCategory(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType): "
							+ ex);
		} catch (Exception ex) {
			log.error(
					"Exception in public List getYearWiseBudgetReportWithCCCategory(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType) : "
							+ ex);
		}
		log.debug(
				"Exit: public List getYearWiseBudgetReportWithCCCategory(String fromDate, String toDate,int orgID, int incExpGroupID, String rptType)"
						+ incExpList.size());
		return incExpList;

	}

	// ================ Get Budgeted project/cost center report=========//
	public List getBudgetedProjectListWithDetails(int orgID, String fromDate, String toDate) {
		log.debug("Entry : public List getBudgetedProjectListWithDetails(int orgID,String fromDate,String toDate)");
		List projectList = null;

		try {
			SocietyVO societyVO = societyService.getSocietyDetails(orgID);
			String query = "SELECT pro.project_id,pro.org_id,pro.project_name,pro.description,pro.project_cost,p.amount,p.actualAmt,budgetAmount AS budget_amount,budgetPercent AS budget_percent, in_out_type FROM (SELECT * FROM project_master_details WHERE org_id=:orgID) AS  pro LEFT JOIN  "
					+ "(SELECT projects.*,SUM(actualAmount) AS actualAmt,SUM(budget_amount) as budgetAmount,SUM(budget_percent) as budgetPercent FROM (SELECT proj.*,b.budget_amount,b.budget_percent FROM(SELECT pa.org_id,  pm.project_id,pm.project_name,pm.description,pm.project_cost,pa.id,pa.amount,pa.ledger_id ,ag.category_name,ac.category_id,ac.group_name,al.ledger_name AS ledgerName,SUM(ale.amount) AS txAmount, (SUM(ale.amount)*pa.amount/100) AS actualAmount ,ac.id AS groupID "
					+ " FROM account_transactions_" + societyVO.getDataZoneID() + " ats,account_ledger_entry_"
					+ societyVO.getDataZoneID() + " ale,account_ledgers_" + societyVO.getDataZoneID()
					+ " al,account_groups ac ,account_category ag ,project_master_details pm ,project_allocation_relation pa "
					+ " WHERE ats.org_id=ale.org_id AND pm.org_id=pa.org_id AND pm.project_id=pa.project_id AND pa.ledger_id=ale.ledger_id AND ale.org_id=al.org_id AND pm.org_id=:orgID AND ats.tx_id=ale.tx_id AND ale.ledger_id=al.id AND al.sub_group_id=ac.id  AND ats.is_deleted=0 AND  ac.category_id=ag.category_id "
					+ " AND ( ats.tx_date >= :fromDate AND ats.tx_date <= :toDate) GROUP BY pa.ledger_id ORDER BY ac.seq_no ) AS proj LEFT JOIN (SELECT bb.* FROM budget_details b,budget_item_details bb WHERE b.org_id=:orgID AND b.from_date=:fromDate AND b.to_date=:toDate AND b.budget_id=bb.budget_id AND b.is_deleted=0 AND b.budget_type='C') AS b ON b.ledger_id=proj.ledger_id) AS projects GROUP BY project_id ) AS p ON p.project_id=pro.project_id;";

			Map hMap = new HashMap();
			hMap.put("orgID", orgID);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", toDate);

			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					ProjectDetailsVO projectVO = new ProjectDetailsVO();
					projectVO.setProjectID(rs.getInt("project_id"));
					projectVO.setDescription(rs.getString("description"));
					projectVO.setOrgID(rs.getInt("org_id"));
					projectVO.setAllocationAmount(rs.getBigDecimal("amount"));
					projectVO.setActualAmount(rs.getBigDecimal("actualAmt"));
					projectVO.setProjectCost(rs.getBigDecimal("project_cost"));
					projectVO.setBudgetAmt(rs.getBigDecimal("budget_amount"));
					projectVO.setBudgetPercent(rs.getBigDecimal("budget_percent"));
					return projectVO;
				}
			};

			projectList = namedParameterJdbcTemplate.query(query, hMap, RMapper);
		} catch (DataAccessException e) {

			log.error("Exception : public int getProjectDetailsListWithDetails " + e);
		} catch (Exception e) {

			log.error("Exception : public int getProjectDetailsListWithDetails " + e);
		}

		log.debug("Exit : public List getBudgetedProjectListWithDetails(int orgID,String fromDate,String toDate)");
		return projectList;

	}

	public List getLedgersListInProject(int orgID, int projectID, String fromDate, String toDate) {
		log.debug(
				"Entry : public List getLedgersListInProject(int orgID, int projectID,String fromDate,String toDate)");
		List projectList = null;

		try {
			SocietyVO societyVO = societyService.getSocietyDetails(orgID);
			String query = "SELECT projects.*,SUM(actualAmount) AS actualAmt ,SUM(txAmount) AS txAmount,root_id,SUM(budget_amount) AS budget_amount,SUM(budget_percent) AS budget_precent  FROM (SELECT proj.*,b.budget_amount,b.budget_percent FROM(SELECT pa.org_id,  pm.project_id,pm.project_name,pm.description,pm.project_cost,pa.id,pa.amount,pa.ledger_id ,ag.category_name,ac.category_id,ac.group_name,al.ledger_name AS ledgerName,SUM(ale.amount) AS txAmount, (SUM(ale.amount)*pa.amount/100) AS actualAmount ,ac.id AS groupID,ac.root_id "
					+ " FROM account_transactions_" + societyVO.getDataZoneID() + " ats,account_ledger_entry_"
					+ societyVO.getDataZoneID() + " ale,account_ledgers_" + societyVO.getDataZoneID()
					+ " al,account_groups ac ,account_category ag ,project_master_details pm ,project_allocation_relation pa "
					+ " WHERE ats.org_id=ale.org_id AND pm.org_id=pa.org_id AND pm.project_id=pa.project_id and pa.project_id=:projectID AND pa.ledger_id=ale.ledger_id AND ale.org_id=al.org_id AND pm.org_id=:orgID AND ats.tx_id=ale.tx_id AND ale.ledger_id=al.id AND al.sub_group_id=ac.id  AND ats.is_deleted=0 AND  ac.category_id=ag.category_id "
					+ " AND ( ats.tx_date >= :fromDate AND ats.tx_date <= :toDate) GROUP BY pa.ledger_id ORDER BY ac.seq_no ) AS proj LEFT JOIN (SELECT bb.* FROM budget_details b,budget_item_details bb WHERE b.org_id=:orgID AND b.from_date=:fromDate AND b.to_date=:toDate AND b.budget_id=bb.budget_id AND b.is_deleted=0 AND b.budget_type='C') AS b ON b.ledger_id=proj.ledger_id) AS projects GROUP BY id ;";

			Map hMap = new HashMap();
			hMap.put("orgID", orgID);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", toDate);
			hMap.put("projectID", projectID);

			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					ProjectDetailsVO projectVO = new ProjectDetailsVO();
					projectVO.setProjectID(rs.getInt("project_id"));
					projectVO.setProjectName(rs.getString("project_name"));
					projectVO.setDescription(rs.getString("description"));
					projectVO.setOrgID(rs.getInt("org_id"));
					projectVO.setAllocationAmount(rs.getBigDecimal("amount"));
					projectVO.setActualAmount(rs.getBigDecimal("actualAmt"));
					projectVO.setProjectCost(rs.getBigDecimal("project_cost"));
					projectVO.setLedgerName(rs.getString("ledgerName"));
					projectVO.setCategoryName(rs.getString("category_name"));
					projectVO.setGroupName(rs.getString("group_name"));
					projectVO.setTxAmount(rs.getBigDecimal("txAmount"));
					projectVO.setGroupID(rs.getInt("groupID"));
					projectVO.setLedgerID(rs.getInt("ledger_id"));
					projectVO.setRootID(rs.getInt("root_id"));
					projectVO.setBudgetAmt(rs.getBigDecimal("budget_amount"));
					projectVO.setBudgetPercent(rs.getBigDecimal("budget_percent"));
					return projectVO;
				}
			};

			projectList = namedParameterJdbcTemplate.query(query, hMap, RMapper);
		} catch (DataAccessException e) {

			log.error(
					"Exception : public List getLedgersListInProject(int orgID, int projectID,String fromDate,String toDate) "
							+ e);
		} catch (Exception e) {

			log.error(
					"Exception : public List getLedgersListInProject(int orgID, int projectID,String fromDate,String toDate) "
							+ e);
		}

		log.debug("Exit : public List getLedgersListInProject(int orgID, int projectID,String fromDate,String toDate)");
		return projectList;

	}

	// ===========New Cost Center ledger list===================//
	public List getLedgersListInProjectTagWise(int orgID, String projectName, String fromDate, String toDate,
			int groupID, int summary) {
		log.debug(
				"Entry : public List getLedgersListInProjectTagWise(int orgID, int projectID,String fromDate,String toDate)");
		List projectList = null;

		try {

			SocietyVO societyVO = societyService.getSocietyDetails(orgID);
			String orgTypeCondition = "";
			String strGroupCondition = "";
			String strCondition = "";
			if (summary == 1) {
				strGroupCondition = " groupID ";
			} else
				strGroupCondition = " ledgerID ";
			if (groupID > 0) {
				strCondition = " AND ac.id=:groupID ";
			}

			if (societyVO.getOrgType().equalsIgnoreCase("S")) {
				orgTypeCondition = "seq_for_soc";
			} else
				orgTypeCondition = "category_id";
			String query = "SELECT s.*,SUM(tx_amount) AS txAmt,SUM(budget_amount) AS budgetAmount,budget_id FROM(SELECT tb.*,SUM(b.budget_amount) AS budget_amount,b.budget_id,SUM(tb.txAmount) AS tx_amount FROM (SELECT projects.*,SUM(actualAmount) AS actualAmt ,SUM(txAmt) AS txAmount FROM(SELECT a.org_id,pm.project_id,pm.project_name,pm.project_acronyms,l.id AS ledgerID,l.ledger_name AS ledgerName,SUM(al.amount) AS txAmt, (SUM(al.amount)) AS actualAmount ,ac.id AS groupID ,ac.root_id ,pm.project_cost,ag.seq_for_soc, ag.category_id,ac.group_name"
					+ " FROM account_ledgers_" + societyVO.getSocietyID() + " l,account_transactions_"
					+ societyVO.getSocietyID() + " a,account_ledger_entry_" + societyVO.getSocietyID()
					+ " al,account_groups ac,account_category ag ,project_master_details pm WHERE a.tx_id=al.tx_id "
					+ " AND a.org_id=:orgID AND a.org_id=pm.org_id AND l.sub_group_id=ac.id AND ac.category_id=ag.category_id AND al.ledger_id=l.id "
					+ " AND ( a.tx_date >= :fromDate AND a.tx_date <= :toDate) AND pm.project_acronyms=:projectName AND ((al.tx_le_tags LIKE '%#"
					+ projectName + "%')OR(l.project_tags LIKE '%#" + projectName + "')) " + strCondition
					+ " GROUP BY l.id) AS projects GROUP BY ledgerID ) as tb"
					+ " LEFT JOIN (SELECT bb.* FROM budget_details b,budget_item_details bb,project_master_details p WHERE b.org_id=:orgID AND b.from_date=:fromDate AND b.to_date=:toDate AND b.budget_id=bb.budget_id AND b.is_deleted=0 AND b.budget_type='C' AND p.project_id=b.project_id AND p.project_acronyms=:projectName )AS b ON b.ledger_id=tb.ledgerID  GROUP BY tb.ledgerID) AS s GROUP BY "
					+ strGroupCondition + " ORDER BY " + orgTypeCondition + " ;";

			log.debug("Query : " + query);

			Map hMap = new HashMap();
			hMap.put("orgID", orgID);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", toDate);
			hMap.put("projectName", projectName);
			hMap.put("groupID", groupID);

			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					ProjectDetailsVO projectVO = new ProjectDetailsVO();
					projectVO.setProjectID(rs.getInt("project_id"));
					projectVO.setProjectName(rs.getString("project_name"));
					// projectVO.setDescription(rs.getString("description"));
					projectVO.setOrgID(rs.getInt("org_id"));
					// projectVO.setAllocationAmount(rs.getBigDecimal("amount"));
					projectVO.setActualAmount(rs.getBigDecimal("actualAmt"));
					projectVO.setProjectCost(rs.getBigDecimal("project_cost"));
					projectVO.setLedgerName(rs.getString("ledgerName"));
					projectVO.setBudgetAmt(rs.getBigDecimal("budgetAmount"));
					projectVO.setGroupName(rs.getString("group_name"));
					projectVO.setTxAmount(rs.getBigDecimal("txAmount"));
					projectVO.setGroupID(rs.getInt("groupID"));
					projectVO.setLedgerID(rs.getInt("ledgerID"));
					projectVO.setRootID(rs.getInt("root_id"));
					projectVO.setProjectAcronyms(rs.getString("project_acronyms"));
					return projectVO;
				}
			};

			projectList = namedParameterJdbcTemplate.query(query, hMap, RMapper);
		} catch (DataAccessException e) {

			log.error(
					"Exception : public List getLedgersListInProjectTagWise(int orgID, int projectID,String fromDate,String toDate,int groupID) "
							+ e);
		} catch (Exception e) {

			log.error(
					"Exception : public List getLedgersListInProjectTagWise(int orgID, int projectID,String fromDate,String toDate,int groupID) "
							+ e);
		}

		log.debug(
				"Exit : public List getLedgersListInProjectTagWise(int orgID, int projectID,String fromDate,String toDate,int groupID)");
		return projectList;

	}
	
	public List getYearWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate) {
		List incExpList = new ArrayList();
		try {
			log.debug(
					"Entry: public List getYearWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate) "
							+ fromDate + toDate);
			String condition=" ";
			if(groupID!=0){
				condition=" bb.group_id=:groupID ";
			}
			
			
			String sql = " SELECT bb.*,SUM( budget_amount ) AS bdgtY,g.root_id,g.group_name  FROM budget_details b,budget_item_details bb,account_groups g WHERE b.org_id=:orgID AND b.from_date=:fromDate AND b.to_date=:toDate "
					   + " AND b.budget_id=bb.budget_id AND b.budget_id=:budgetID AND b.is_deleted=0 AND bb.is_deleted=0 "+condition+" AND g.id=bb.group_id GROUP BY bb.group_id;";

			log.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("fromDate", fromDate);
			hashMap.put("toDate", toDate);
			hashMap.put("budgetID", budgetID);
			hashMap.put("orgID", orgID);
			hashMap.put("groupID", groupID);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					BudgetDetailsVO rptVO = new BudgetDetailsVO();
					rptVO.setGroupID(rs.getInt("group_id"));
					rptVO.setOrgID(rs.getInt("org_id"));
					rptVO.setBudgetID(rs.getInt("budget_id"));
					rptVO.setGroupName(rs.getString("group_name"));						
					rptVO.setRootID(rs.getInt("root_id"));
					rptVO.setBudgetAmt(rs.getBigDecimal("bdgtY"));
					rptVO.setInOutType(rs.getString("in_out_type"));
					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);
			// logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			log.info(
					"Exception in public List getYearWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate)  "
							+ ex);
		} catch (Exception ex) {
			log.error(
					"Exception in public List getYearWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate)  "
							+ ex);
		}
		log.debug(
				"Exit: public List getYearWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate) "
						+ incExpList.size());
		return incExpList;

	}
	
	
	public List getQuarterWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate) {
		List incExpList = new ArrayList();
		try {
			log.debug(
					"Entry: public List getQuarterWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate) "
							+ fromDate + toDate);
			String condition=" ";
			if(groupID!=0){
				condition=" bb.group_id=:groupID ";
			}
			
			String sql = " SELECT bb.*,g.root_id,g.group_name ,SUM(CASE WHEN QUARTER(bb.from_date) = '2' THEN budget_amount ELSE '0.00' END) AS bdgtQ1,SUM(CASE WHEN QUARTER(bb.from_date) = '3' THEN budget_amount ELSE '0.00' END) AS bdgtQ2,"
					+ " SUM(CASE WHEN QUARTER(bb.from_date) = '4' THEN budget_amount ELSE '0.00' END) AS bdgtQ3,SUM(CASE WHEN QUARTER(bb.from_date) = '1' THEN budget_amount ELSE '0.00' END) AS bdgtQ4 "
					+ " FROM budget_details b,budget_item_details bb ,account_groups g WHERE b.org_id=:orgID AND b.from_date=:fromDate AND b.to_date=:toDate AND b.budget_id=bb.budget_id AND b.budget_id=:budgetID "
					+ "AND b.is_deleted=0 AND bb.is_deleted=0 "+condition+" AND g.id=bb.group_id   GROUP BY bb.group_id ";
			
			log.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("fromDate", fromDate);
			hashMap.put("toDate", toDate);
			hashMap.put("budgetID", budgetID);
			hashMap.put("orgID", orgID);
			hashMap.put("groupID", groupID);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					BudgetDetailsVO rptVO = new BudgetDetailsVO();
					rptVO.setGroupID(rs.getInt("group_id"));
					rptVO.setGroupName(rs.getString("group_name"));			
					rptVO.setRootID(rs.getInt("root_id"));
					rptVO.setOrgID(rs.getInt("org_id"));
					rptVO.setBudgetID(rs.getInt("budget_id"));
					rptVO.setQ1Amt(rs.getBigDecimal("bdgtQ1"));
					rptVO.setQ2Amt(rs.getBigDecimal("bdgtQ2"));
					rptVO.setQ3Amt(rs.getBigDecimal("bdgtQ3"));
					rptVO.setQ4Amt(rs.getBigDecimal("bdgtQ4"));
					
					rptVO.setInOutType(rs.getString("in_out_type"));
					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);
			// logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			log.info(
					"Exception in public List getQuarterWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate)  "
							+ ex);
		} catch (Exception ex) {
			log.error(
					"Exception in public List getQuarterWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate)  "
							+ ex);
		}
		log.debug(
				"Exit: public List getQuarterWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate) "
						+ incExpList.size());
		return incExpList;

	}
	
	
	public List getHalfYearWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate) {
		List incExpList = new ArrayList();
		try {
			log.debug(
					"Entry: public List getHalfYearWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate) "
							+ fromDate + toDate);
			String condition=" ";
			if(groupID!=0){
				condition=" bb.group_id=:groupID ";
			}
			
		
		    String sql = " SELECT bb.*,g.root_id,g.group_name ,SUM(CASE WHEN ( QUARTER(bb.from_date) = '2' or QUARTER(bb.from_date)='3') THEN budget_amount ELSE '0.00' END) AS bdgtH1,SUM(CASE WHEN ( QUARTER(bb.from_date) = '4' or QUARTER(bb.from_date)='1') THEN budget_amount ELSE '0.00' END) AS bdgtH2 "
			          + " FROM budget_details b,budget_item_details bb,account_groups g WHERE b.org_id=:orgID AND b.from_date=:fromDate AND b.to_date=:toDate AND b.budget_id=bb.budget_id"
			          + " AND b.budget_id=:budgetID AND b.is_deleted=0 AND bb.is_deleted=0 "+condition+" AND g.id=bb.group_id  GROUP BY bb.group_id";
			
			log.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("fromDate", fromDate);
			hashMap.put("toDate", toDate);
			hashMap.put("budgetID", budgetID);
			hashMap.put("orgID", orgID);
			hashMap.put("groupID", groupID);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					BudgetDetailsVO rptVO = new BudgetDetailsVO();
					rptVO.setGroupID(rs.getInt("group_id"));
					rptVO.setGroupName(rs.getString("group_name"));			
					rptVO.setOrgID(rs.getInt("org_id"));
					rptVO.setBudgetID(rs.getInt("budget_id"));
					rptVO.setRootID(rs.getInt("root_id"));
					rptVO.setH1Amt(rs.getBigDecimal("bdgtH1"));
					rptVO.setH2Amt(rs.getBigDecimal("bdgtH2"));
					rptVO.setInOutType(rs.getString("in_out_type"));
					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);
			// logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			log.info(
					"Exception in public List getHalfYearWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate)  "
							+ ex);
		} catch (Exception ex) {
			log.error(
					"Exception in public List getHalfYearWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate)  "
							+ ex);
		}
		log.debug(
				"Exit: public List getHalfYearWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate) "
						+ incExpList.size());
		return incExpList;

	}
	
	
	public List getMonthWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate) {
		List incExpList = new ArrayList();
		try {
			log.debug(
					"Entry: public List getMonthWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate) "
							+ fromDate + toDate);
			String condition=" ";
			if(groupID!=0){
				condition=" bb.group_id=:groupID ";
			}
			
		  String sql = " SELECT bb.*,g.root_id,g.group_name,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'April' THEN budget_amount ELSE '0.00' END) AS bdgtApr,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'May' THEN budget_amount ELSE '0.00' END) AS bdgtMay,"
			+ " SUM(CASE WHEN MONTHNAME(bb.from_date) = 'June' THEN budget_amount ELSE '0.00' END) AS bdgtJun,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'July' THEN budget_amount ELSE '0.00' END) AS bdgtJul,"
			+ " SUM(CASE WHEN MONTHNAME(bb.from_date) = 'August' THEN budget_amount ELSE '0.00' END) AS bdgtAug,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'September' THEN budget_amount ELSE '0.00' END) AS bdgtSept,"
			+ " SUM(CASE WHEN MONTHNAME(bb.from_date) = 'October' THEN budget_amount ELSE '0.00' END) AS bdgtOct,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'November' THEN budget_amount ELSE '0.00' END) AS bdgtNov,"
			+ " SUM(CASE WHEN MONTHNAME(bb.from_date) = 'December' THEN budget_amount ELSE '0.00' END) AS bdgtDec,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'January' THEN budget_amount ELSE '0.00' END) AS bdgtJan,"
			+ " SUM(CASE WHEN MONTHNAME(bb.from_date) = 'February' THEN budget_amount ELSE '0.00' END) AS bdgtFeb,SUM(CASE WHEN MONTHNAME(bb.from_date) = 'March' THEN budget_amount ELSE '0.00' END) AS bdgtMar  FROM budget_details b,budget_item_details bb,account_groups g"
			+ " WHERE b.org_id=:orgID AND b.from_date=:fromDate AND b.to_date=:toDate AND b.budget_id=bb.budget_id AND b.budget_id=:budgetID AND b.is_deleted=0 AND bb.is_deleted=0 "+condition+" AND g.id=bb.group_id  GROUP BY bb.group_id";

			
			log.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("fromDate", fromDate);
			hashMap.put("toDate", toDate);
			hashMap.put("budgetID", budgetID);
			hashMap.put("orgID", orgID);
			hashMap.put("groupID", groupID);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					BudgetDetailsVO rptVO = new BudgetDetailsVO();
					rptVO.setGroupID(rs.getInt("group_id"));
					rptVO.setGroupName(rs.getString("group_name"));			
					rptVO.setOrgID(rs.getInt("org_id"));
					rptVO.setBudgetID(rs.getInt("budget_id"));
					rptVO.setRootID(rs.getInt("root_id"));
					rptVO.setJanAmt(rs.getBigDecimal("bdgtJan"));
					rptVO.setFebAmt(rs.getBigDecimal("bdgtFeb"));
					rptVO.setMarAmt(rs.getBigDecimal("bdgtMar"));
					rptVO.setAprAmt(rs.getBigDecimal("bdgtApr"));
					rptVO.setMayAmt(rs.getBigDecimal("bdgtMay"));
					rptVO.setJunAmt(rs.getBigDecimal("bdgtJun"));
					rptVO.setJulAmt(rs.getBigDecimal("bdgtJul"));
					rptVO.setAugAmt(rs.getBigDecimal("bdgtAug"));
					rptVO.setSeptAmt(rs.getBigDecimal("bdgtSept"));
					rptVO.setOctAmt(rs.getBigDecimal("bdgtOct"));
					rptVO.setNovAmt(rs.getBigDecimal("bdgtNov"));
					rptVO.setDecAmt(rs.getBigDecimal("bdgtDec"));
					
					rptVO.setInOutType(rs.getString("in_out_type"));
					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);
			// logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			log.info(
					"Exception in public List getMonthWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate)  "
							+ ex);
		} catch (Exception ex) {
			log.error(
					"Exception in public List getMonthWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate)  "
							+ ex);
		}
		log.debug(
				"Exit: public List getMonthWiseBudgetLineItemList(int orgID,int budgetID,int groupID,String fromDate, String toDate) "
						+ incExpList.size());
		return incExpList;

	}
	
	

	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	/**
	 * @return the namedParameterJdbcTemplate
	 */
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	/**
	 * @param societyService
	 *            the societyService to set
	 */
	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}

}
