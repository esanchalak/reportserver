package com.emanager.server.financialReports.dataaccessObject;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

//import sun.util.logging.resources.logging;

import com.emanager.server.dashBoard.dataAccessObject.AccountStatusVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardAccountVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardFacilityVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardMemberVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardSocietyVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardTenantVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardVehicleVO;


public class AccountStatusDAO {
	
	   
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	private static final Logger log = Logger.getLogger(AccountStatusDAO.class);

	public int sendAccountStatusReport(AccountStatusVO acStsVO){
		log.debug("Entry : public int sendAccountStatusReport(AccountStatusVO acStsVO)");
		int success=0;
		Date currentDatetime = new Date(System.currentTimeMillis());
		
		try {
			String query="INSERT INTO committee_status_report_details(society_id ,user_id ,send_date ,report_id ,to_email ,bcc_email , note, period, from_date, to_date, is_test )VALUES " +
						   "(:societyID, :userID, :sendDate, :reportID, :toMail, :bccMail, :note, :period, :fromDate, :toDate ,:isTest );";
			
			Map namedParameters = new HashMap();
			namedParameters.put("societyID", acStsVO.getSocietyID());
			namedParameters.put("userID", acStsVO.getUserID());
			namedParameters.put("sendDate", currentDatetime);
			namedParameters.put("reportID", acStsVO.getReportID());
			namedParameters.put("toMail", acStsVO.getToEmail());
			namedParameters.put("bccMail", acStsVO.getBccEmail());
			namedParameters.put("note", acStsVO.getNote());
			namedParameters.put("period", acStsVO.getPeriod());
			namedParameters.put("fromDate", acStsVO.getFromDate());
			namedParameters.put("toDate", acStsVO.getToDate());
			namedParameters.put("isTest", acStsVO.getIsTest());
			
			
			
			success=namedParameterJdbcTemplate.update(query, namedParameters);
		} catch (DataAccessException e) {
			
			log.error("Exception : public int sendAccountStatusReport "+e);
		} catch (Exception e) {
			
			log.error("Exception : public int sendAccountStatusReport "+e);
		}
		
		log.debug("Exit : public int sendAccountStatusReport(int userID,List societyList,String fromDate,String uptoDate)");
		return success;
		
	}
	
	
	public DashBoardAccountVO getAccountsStatusReportDetails(int societyId) {

		DashBoardAccountVO dbAcVO=new DashBoardAccountVO();
		
				
		
		try {
			log.debug("Entry : public DashBoardAccountVO getAccountsStatusReportDetails(int societyId)");
		
		String strSQL = "SELECT * FROM committee_status_report_details WHERE society_id=:societyID AND is_test=0 ORDER BY to_date desc,sent_on DESC LIMIT 1;";
		
			//1. SQL Query
			log.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyID", societyId);
			
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  DashBoardAccountVO dbVO = new DashBoardAccountVO();
			            dbVO.setSocietyId(rs.getString("society_id"));
			            dbVO.setStatusPeriod(rs.getString("period"));
			            dbVO.setStatusSendDate(rs.getString("sent_on"));
			                       
			            return dbVO;
				}
			};
			

			dbAcVO =  (DashBoardAccountVO) namedParameterJdbcTemplate.queryForObject(strSQL, namedParameters, RMapper);

			
			log.debug("Exit:	public DashBoardAccountVO getAccountsDetails(int societyId)");
		} catch (NullPointerException ex) {
			log.info("No details found : "+ex);

		} catch (BadSqlGrammarException ex) {
			log.info("No details found : "+ex);
		}catch (EmptyResultDataAccessException ex) {
			log.info("No details found : "+ex);
		}
		catch (Exception ex) {
			log.error("Exception in getAccountsDetails : "+ex);

		}
		return dbAcVO;

	}
	
	//==================Get pending transaction report for all orgs Created 06-05-21===========//
	
		public List getPendingApprovalTxReport(){
			log.debug("Entry : public List getPendingApprovalTxReport()");
			List societyList=new ArrayList<>();
			
			
			try {
				//1. Sql Query
				String query="SELECT society_id,COUNT(*) AS COUNT,society_name FROM society_details d, transaction_details t WHERE d.society_id=t.org_id AND t.is_deleted=4 GROUP BY t.org_id order by t.org_id;";
				
				//2. Parameters.
				Map namedParameters = new HashMap();
				
				//3. RowMapper.
				RowMapper RMapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						  DashBoardAccountVO dbVO = new DashBoardAccountVO();
				            dbVO.setSocietyId(rs.getString("society_id"));
				            dbVO.setPendingTxCount(rs.getInt("count"));
				            dbVO.setSocietyName(rs.getString("society_name"));
				                       
				            return dbVO;
					}
				};
				
				
				
				societyList=namedParameterJdbcTemplate.query(query, namedParameters, RMapper);
			} catch (DataAccessException e) {
				
				log.error("Exception : public List getPendingApprovalTxReport() "+e);
			} catch (Exception e) {
				
				log.error("Exception : public List getPendingApprovalTxReport() "+e);
			}
			
			log.debug("Exit : public List getPendingApprovalTxReport()");
			return societyList;
			
		}
	
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	
	
}
