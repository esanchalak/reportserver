package com.emanager.server.financialReports.dataaccessObject;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.emanager.server.financialReports.valueObject.RptTransactionVO;

public class MemberFeeDAO {
	List reportList = null;

	private static final Logger logger = Logger.getLogger(MemberFeeDAO.class);

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	
	
	 public List getMemberList(int societyId,String buildingID) {
			List incExpList = new ArrayList();
			try {
				logger.debug("Entry: public List getMemberList(String societyId)"+ societyId);
				String strWhereClause=null;
				if(buildingID.equalsIgnoreCase("0"))
					strWhereClause=" ";
					else
						strWhereClause="  AND b.building_id="+buildingID+"";
				
				Map hashMap = new HashMap();
				
				hashMap.put("societyID", societyId);
				
						
				
				
				String sql = "SELECT a.*,m.*,concat(b.building_name,' ',a.apt_name) as flat_no,lu.* FROM apartment_details a,building_details b,member_details m,ledger_user_mapping_"+societyId+" lu WHERE m.apt_id=a.apt_id AND a.building_id=b.building_id and m.member_id=lu.user_id " +
						" AND m.type='P' AND m.is_current='1' AND b.society_id=:societyID "+strWhereClause+" Order By seq_no";
					
					
				RowMapper rMapper=new RowMapper()
				{

					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						// TODO Auto-generated method stub
						RptTransactionVO rptVO=new RptTransactionVO();
						rptVO.setAptID(rs.getInt("apt_id"));
						
						rptVO.setUnitID(rs.getInt("unit_id"));
						rptVO.setAddress(rs.getString("flat_no"));
						rptVO.setMobile(rs.getString("mobile"));
						rptVO.setEmail(rs.getString("email"));
						rptVO.setArrears(rs.getBigDecimal("arrears"));
						rptVO.setArrearsPaid(rs.getBigDecimal("arrears_paid"));
						rptVO.setArrearsComment(rs.getString("arrears_comment"));
						rptVO.setMember_name(rs.getString("full_name"));
						rptVO.setSqFeets(rs.getBigDecimal("sq_feet"));
						rptVO.setMember_id(rs.getInt("member_id"));
						rptVO.setIs_rented(rs.getInt("is_Rented"));
						rptVO.setLateFeeType(rs.getString("ledger_id"));
						
						return rptVO;
					}};
					incExpList = namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
				
			} catch (Exception ex) {
				logger.error("Exception in public List getMemberList(String societyId) : " + ex);
			}
			logger
					.debug("Exit: public List getMemberList(String societyId)" +incExpList.size());
			return incExpList;
		} 
	 
	 
	 public RptTransactionVO getMemberDetails(int memberID,int societyId) {
			List incExpList = new ArrayList();
			RptTransactionVO rptMember=new RptTransactionVO();
			try {
				logger.debug("Entry: public List getMemberDetails(String societyId)"+ societyId);
				String strWhereClause=null;
				
				
				Map hashMap = new HashMap();
				
				hashMap.put("societyID", societyId);
				hashMap.put("memberID", memberID);
				
						
				
				
				String sql = "SELECT a.*,m.*,concat(b.building_name,' ',a.apt_name) as flat_no FROM apartment_details a,building_details b,member_details m WHERE m.apt_id=a.apt_id AND a.building_id=b.building_id " +
						" AND m.type='P' AND m.is_current='1' AND b.society_id=:societyID and m.member_id=:memberID";
					
					
				RowMapper rMapper=new RowMapper()
				{

					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						// TODO Auto-generated method stub
						RptTransactionVO rptVO=new RptTransactionVO();
						rptVO.setAptID(rs.getInt("apt_id"));
						rptVO.setUnitID(rs.getInt("unit_id"));
						rptVO.setAddress(rs.getString("flat_no"));
						rptVO.setMobile(rs.getString("mobile"));
						rptVO.setEmail(rs.getString("email"));
						rptVO.setArrears(rs.getBigDecimal("arrears"));
						rptVO.setArrearsPaid(rs.getBigDecimal("arrears_paid"));
						rptVO.setArrearsComment(rs.getString("arrears_comment"));
						rptVO.setMember_name(rs.getString("full_name"));
						rptVO.setSqFeets(rs.getBigDecimal("sq_feet"));
						rptVO.setMember_id(rs.getInt("member_id"));
						rptVO.setIs_rented(rs.getInt("is_Rented"));
						
						return rptVO;
					}};
					rptMember = (RptTransactionVO) namedParameterJdbcTemplate.queryForObject(sql, hashMap,rMapper);
				
			} catch (Exception ex) {
				logger.error("Exception in public List getMemberList(String societyId) : " + ex);
			}
			logger
					.debug("Exit: public List getMemberList(String societyId)" +incExpList.size());
			return rptMember;
		} 
	 
	 public List getMemberChargeDetails(RptTransactionVO rptMember,int societyID) {
			List incExpList = new ArrayList();
			try {
				logger.debug("Entry: private List getMemberChargeDetails(RptTransactionVO rptMember,int societyID)"+ societyID+" "+rptMember.getMember_id()+" "+rptMember.getUnitID());
			
				String condition="AND s.unit_id="+rptMember.getUnitID();
				
				Map hashMap = new HashMap();
				
				hashMap.put("societyID", societyID);
				hashMap.put("memberID", rptMember.getMember_id());
				hashMap.put("unitID", rptMember.getUnitID());
						
				
				
				String sql = "SELECT * FROM member_details m,soc_charge_details s ,apartment_details a,building_details b ,society_settings ss "+
							" WHERE m.apt_id=a.apt_id AND a.building_id=b.building_id and s.society_id=ss.society_id " +
							"AND m.type='P' AND m.is_current='1' AND b.society_id=s.society_id AND m.member_id=:memberID" +
							" AND ( s.unit_id=:unitID OR s.unit_id IS NULL) group by charge_id ";
					
					
				RowMapper rMapper=new RowMapper()
				{

					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						// TODO Auto-generated method stub
						RptTransactionVO rptVO=new RptTransactionVO();
						rptVO.setId(rs.getInt("id"));
						rptVO.setAptID(rs.getInt("apt_id"));
						rptVO.setUnitID(rs.getInt("unit_id"));
						rptVO.setSqFeets(rs.getBigDecimal("sq_feet"));
						rptVO.setMember_id(rs.getInt("member_id"));
						rptVO.setCalculationMode(rs.getString("cal_method"));
						rptVO.setTx_ammount(rs.getBigDecimal("amount"));
						rptVO.setCategoryID(rs.getInt("charge_id"));
						rptVO.setIsLateFee(rs.getInt("is_late_fee"));
						rptVO.setLateFees(rs.getBigDecimal("late_fee_rate"));
						rptVO.setLateFeeType(rs.getString("late_fee_type"));
						rptVO.setFrequency(rs.getString("frequency"));
						rptVO.setFrom_date(rs.getDate("from_date"));
						rptVO.setTx_to_date(rs.getDate("to_date"));
						rptVO.setDayParam(rs.getInt("day_param"));
						rptVO.setMonthParam(rs.getInt("month_param"));
						rptVO.setCalculationMode(rs.getString("mode"));
						
						
						return rptVO;
					}};
					incExpList = namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
				
			} catch (Exception ex) {
				logger.error("Exception in public getMemberChargeDetails(RptTransactionVO rptMember,int societyID) : " + ex);
			}
			logger
					.debug("Exit: public getMemberChargeDetails(RptTransactionVO rptMember,int societyID)" +incExpList.size());
			return incExpList;
		} 
	 
	 
	 public RptTransactionVO getPaidChargesDetails(RptTransactionVO rptMember,int societyId,Date uptoDate,int categoryID) {
			RptTransactionVO rptMemberObj = new RptTransactionVO();
			try {
				logger.debug("Entry: public RptTransactionVO getPaidChargesDetails(RptTransactionVO rptMember,int societyId,String uptoDate)"+ societyId);
			
				
				Map hashMap = new HashMap();
				
				hashMap.put("societyID", societyId);
				hashMap.put("memberID", rptMember.getMember_id());
				hashMap.put("categoryID", categoryID);
				hashMap.put("uptoDate", uptoDate);
								
				String sql = "SELECT s.*,MAX(tx_to_date) AS max_dt,sum(tx_ammount) as amt FROM soc_tx_"+societyId+" s,member_details m WHERE s.table_primary_id=m.member_id AND  m.member_id=:memberID AND acc_head_id=:categoryID and s.is_arrears=0;  ";
									
				RowMapper rMapper=new RowMapper()
				{

					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						// TODO Auto-generated method stub
						RptTransactionVO rptVO=new RptTransactionVO();
						rptVO.setTx_to_date(rs.getDate("max_dt"));
						rptVO.setMember_id(rs.getInt("table_primary_id"));
						rptVO.setTotal(rs.getBigDecimal("amt"));
						
						return rptVO;
					}};
					rptMemberObj = (RptTransactionVO) namedParameterJdbcTemplate.queryForObject(sql, hashMap, rMapper);
				
				
			} catch (Exception ex) {
				logger.error("Exception in RptTransactionVO getPaidChargesDetails(RptTransactionVO rptMember,int societyId,String uptoDate) : "+ ex);
			}
			logger
					.debug("Exit: public RptTransactionVO getPaidChargesDetails(RptTransactionVO rptMember,int societyId,String uptoDate)" );
			return rptMemberObj;
		} 
	 
	 public RptTransactionVO getUnPaidChargesDetails(RptTransactionVO rptMember,int societyId,Date uptoDate,final int catgoryID) {
			RptTransactionVO rptMemberObj = new RptTransactionVO();
			try {
				logger.debug("Entry: public RptTransactionVO getUnPaidChargesDetails(RptTransactionVO rptMember,int societyId,String uptoDate)"+ societyId);
			
				
				Map hashMap = new HashMap();
				
				hashMap.put("societyID", societyId);
				hashMap.put("memberID", rptMember.getMember_id());
				hashMap.put("categoryID", catgoryID);
				hashMap.put("unitID", rptMember.getUnitID());
				hashMap.put("uptoDate", uptoDate);
				hashMap.put("ID", rptMember.getId());
						
				
				
				String sql = "SELECT * FROM member_details m,soc_charge_details s ,apartment_details a,building_details b ,society_settings ss "+
							 " WHERE m.apt_id=a.apt_id AND a.building_id=b.building_id AND s.society_id=ss.society_id "+ 
							 " AND m.type='P' AND m.is_current='1' AND b.society_id=s.society_id AND m.member_id=:memberID "+
						     " AND ( s.unit_id=:unitID OR s.unit_id IS NULL) AND s.charge_id=:categoryID and id=:ID; ";
					
					
				RowMapper rMapper=new RowMapper()
				{

					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						// TODO Auto-generated method stub
						RptTransactionVO rptVO=new RptTransactionVO();
						if(catgoryID==16){
						rptVO.setTx_to_date(rs.getDate("soc_form_date"));	
						}else
						rptVO.setTx_to_date(rs.getDate("from_date"));
						rptVO.setMember_id(rs.getInt("member_id"));
						rptVO.setTotal(rs.getBigDecimal("amount"));
						
						return rptVO;
					}};
					rptMemberObj = (RptTransactionVO) namedParameterJdbcTemplate.queryForObject(sql, hashMap, rMapper);
				
				
			} catch (Exception ex) {
				logger.error("Exception in RptTransactionVO getUnPaidChargesDetails(RptTransactionVO rptMember,int societyId,String uptoDate) : " + ex+"  "+rptMember.getMember_id());
			}
			logger
					.debug("Exit: public RptTransactionVO getUnPaidChargesDetails(RptTransactionVO rptMember,int societyId,String uptoDate)" );
			return rptMemberObj;
		}  
	 
	 
	 public List getAppllicableFees(RptTransactionVO rptMember,int societyID,Date uptoDate,Date paidUpto,int category) {
			List memberFeeList = new ArrayList();
			try {
				logger.debug("Entry: private List getAppllicableFees(RptTransactionVO rptMember,int societyID)"+ societyID);
			
				String condition="AND s.unit_id="+rptMember.getUnitID();
				
				Map hashMap = new HashMap();
				
				hashMap.put("societyID", societyID);
				hashMap.put("memberID", rptMember.getMember_id());
				hashMap.put("unitID", rptMember.getUnitID());
				hashMap.put("uptoDate", uptoDate);
				hashMap.put("paidUpto", paidUpto);
				hashMap.put("categoryID", category);
						
				
				
				String sql = "SELECT * FROM member_details m,soc_charge_details s ,apartment_details a,building_details b ,society_settings ss "+
							" WHERE m.apt_id=a.apt_id AND a.building_id=b.building_id and s.society_id=ss.society_id " +
							"AND m.type='P' AND m.is_current='1' AND b.society_id=s.society_id AND m.member_id=:memberID and charge_id=:categoryID" +
							" AND ( s.unit_id=:unitID OR s.unit_id IS NULL) AND (( :paidUpto BETWEEN from_date AND to_date ) OR (:uptoDate BETWEEN from_date AND to_date)) ";
					
					
				RowMapper rMapper=new RowMapper()
				{

					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						// TODO Auto-generated method stub
						RptTransactionVO rptVO=new RptTransactionVO();
						rptVO.setId(rs.getInt("id"));
						rptVO.setAptID(rs.getInt("apt_id"));
						rptVO.setUnitID(rs.getInt("unit_id"));
						rptVO.setSqFeets(rs.getBigDecimal("sq_feet"));
						rptVO.setMember_id(rs.getInt("member_id"));
						rptVO.setCalculationMode(rs.getString("cal_method"));
						rptVO.setTx_ammount(rs.getBigDecimal("amount"));
						rptVO.setUnitCost(rs.getBigDecimal("cost"));
						rptVO.setCategoryID(rs.getInt("charge_id"));
						rptVO.setIsLateFee(rs.getInt("is_late_fee"));
						rptVO.setLateFees(rs.getBigDecimal("late_fee_rate"));
						rptVO.setLateFeeType(rs.getString("late_fee_type"));
						rptVO.setFrequency(rs.getString("frequency"));
						rptVO.setFrom_date(rs.getDate("from_date"));
						rptVO.setTx_to_date(rs.getDate("to_date"));
						rptVO.setDayParam(rs.getInt("day_param"));
						rptVO.setMonthParam(rs.getInt("month_param"));
						//rptVO.setCalculationMode(rs.getString("mode"));
						
						
						return rptVO;
					}};
					memberFeeList = namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
					logger.debug(rptMember.getMember_id()+" paid upto date "+paidUpto+"  Upto Date "+uptoDate+"  "+memberFeeList.size());
					
			} catch (Exception ex) {
				logger.error("Exception in public getMemberChargeDetails(RptTransactionVO rptMember,int societyID) : " + ex);
			}
			logger
					.debug("Exit: public getMemberChargeDetails(RptTransactionVO rptMember,int societyID)" +memberFeeList.size());
			return memberFeeList;
		} 
	 
	 
	
	 
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

}
