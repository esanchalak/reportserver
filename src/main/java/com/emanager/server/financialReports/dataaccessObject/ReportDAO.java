package com.emanager.server.financialReports.dataaccessObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.adminReports.valueObject.RptDocumentVO;
import com.emanager.server.adminReports.valueObject.RptMorgageVO;
import com.emanager.server.adminReports.valueObject.RptTenantVO;
import com.emanager.server.adminReports.valueObject.RptVehicleVO;
import com.emanager.server.commonUtils.valueObject.DropDownVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardTenantVO;
import com.emanager.server.financialReports.valueObject.MonthwiseChartVO;
import com.emanager.server.financialReports.valueObject.ReportDetailsVO;
import com.emanager.server.financialReports.valueObject.RptMonthYearVO;
import com.emanager.server.financialReports.valueObject.RptTransactionVO;
import com.emanager.server.invoice.dataAccessObject.BillDetailsVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;

public class ReportDAO {
	List reportList = null;

	private static final Logger logger = Logger.getLogger(ReportDAO.class);

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	SocietyService societyService;

	/*public List getClosingBalances(int societyID, String buildingID,
			String uptoDate) {

		List reportList = null;
		try {
			logger
					.debug("Entry : public List getClosingBalances(String societyID,String buildingID))"
							+ societyID);
			String strWhereClause = null;
			if (buildingID.equalsIgnoreCase("0"))
				strWhereClause = " ";
			else
				strWhereClause = "  AND a.building_id=" + buildingID + "";

			//String sqlDue="SELECT MAX(tx_to_date) , soc_tx_"+intSocietyID+".*, MAX(tx_to_date) AS maxdt,member_details.* FROM soc_tx_"+intSocietyID+" ,member_details WHERE member_details.member_id=soc_tx_"+intSocietyID+".table_primary_id AND charge_type = 'Monthly maintenance Charges' GROUP BY soc_tx_"+intSocietyID+".table_primary_id;";
			String sqlDue = "SELECT b.building_id,m.apt_id,m.member_id,m.full_name,m.ledger_name,m.mobile,m.title,m.full_name,m.email,mu.ledger_id,building_name,apt_name ,SUM(CASE WHEN invoice_type_id=1 THEN current_balance ELSE 0 END) AS regular, "
					+ " SUM(CASE WHEN invoice_type_id=2 THEN current_balance ELSE 0 END) AS sinkingFund, "
					+ "SUM(CASE WHEN invoice_type_id=3 THEN current_balance ELSE 0 END) AS serviceCharge, "
					+ "SUM(CASE WHEN invoice_type_id=4 THEN current_balance ELSE 0 END) AS infraFund, "
					+ "SUM(CASE WHEN invoice_type_id=5 THEN current_balance ELSE 0 END) AS legalFee, "
					+ "SUM(CASE WHEN invoice_type_id=6 THEN current_balance ELSE 0 END) AS meter , "
					+ "SUM(CASE WHEN invoice_type_id=7 THEN current_balance ELSE 0 END) AS memberTransfer, "
					+ "SUM(CASE WHEN invoice_type_id=8 THEN current_balance ELSE 0 END) AS contegency, "
					+ "SUM(CASE WHEN invoice_type_id=9 THEN current_balance ELSE 0 END) AS openCar, "
					+ "SUM(CASE WHEN invoice_type_id=10 THEN current_balance ELSE 0 END) AS lateFee, "
					+ "SUM(CASE WHEN invoice_type_id=11 THEN current_balance ELSE 0 END) AS other, "
					+ "SUM(CASE WHEN invoice_type_id=12 THEN current_balance ELSE 0 END) AS corpus,"
					+ "SUM(CASE WHEN invoice_type_id=13 THEN current_balance ELSE 0 END) AS societyFrm,"
					+ "SUM(CASE WHEN invoice_type_id=14 THEN current_balance ELSE 0 END) AS water,"
					+ "SUM(CASE WHEN invoice_type_id=15 THEN current_balance ELSE 0 END) AS repair,"
					+ "SUM(CASE WHEN invoice_type_id=16 THEN current_balance ELSE 0 END) AS lift FROM in_invoice_"
					+ societyID
					+ " i,member_details m ,apartment_details a ,ledger_user_mapping_"
					+ societyID
					+ " mu ,building_details b "
					+ "WHERE m.apt_id=i.apt_id AND m.apt_id=a.apt_id AND a.building_id=b.building_id  AND m.member_id=mu.user_id and (status_id=0 OR status_id=1) AND i.is_deleted=0   AND i.invoice_date<=:uptoDate "
					+ strWhereClause + " GROUP BY m.apt_id ORDER BY seq_no;";

			String sqlDue = "SELECT * FROM member_details m,apartment_details a,building_details b,ledger_user_mapping_"+societyID+" lu "
			 + " WHERE m.apt_id=a.apt_id   "+strWhereClause+" AND a.building_id=b.building_id AND m.member_id=lu.user_id AND lu.ledger_type='M' AND b.society_id=:societyID order by seq_no;";

			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("uptoDate", uptoDate);

			RowMapper rMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int num) throws SQLException {
					// TODO Auto-generated method stub
					RptTransactionVO reportVO = new RptTransactionVO();
					reportVO.setAptID(rs.getInt("apt_id"));
					reportVO.setMember_id(rs.getInt("member_id"));
					reportVO.setBuildingId(rs.getString("building_id"));
					reportVO.setLedgerName(rs.getString("building_name") + " "
							+ rs.getString("apt_name") + " - "
							+ rs.getString("title") + " "
							+ rs.getString("full_name"));
					reportVO.setMember_name(rs.getString("title") + " "
							+ rs.getString("full_name"));
					reportVO.setLedgerID(rs.getInt("ledger_id"));
					reportVO.setEmail(rs.getString("email"));
					reportVO.setMobile(rs.getString("mobile"));
					reportVO.setDue(rs.getBigDecimal("regular"));
					reportVO.setDueForSF(rs.getBigDecimal("sinkingFund"));
					reportVO.setDueForMMC(rs.getBigDecimal("serviceCharge"));
					reportVO.setDueForInfra(rs.getBigDecimal("infraFund"));
					reportVO.setDueForLegalFee(rs.getBigDecimal("legalFee"));
					reportVO.setDueForMeter(rs.getBigDecimal("meter"));
					reportVO.setDueForTransfer(rs
							.getBigDecimal("memberTransfer"));
					reportVO
							.setDueForContigency(rs.getBigDecimal("contegency"));
					reportVO.setDueForCarParking(rs.getBigDecimal("openCar"));
					reportVO.setLateFees(rs.getBigDecimal("lateFee"));
					reportVO.setDueForOther(rs.getBigDecimal("other"));
					reportVO.setDueForCorpus(rs.getBigDecimal("corpus"));
					reportVO
							.setDueForSocietyFrm(rs.getBigDecimal("societyFrm"));
					reportVO.setDueForWaterCharges(rs.getBigDecimal("water"));
					reportVO.setDueForRM(rs.getBigDecimal("repair"));
					reportVO.setDueForLift(rs.getBigDecimal("lift"));

					return reportVO;
				}
			};
			reportList = namedParameterJdbcTemplate
					.query(sqlDue, hMap, rMapper);

			logger.debug("Outside Query : " + reportList.size());
			logger
					.debug("Exit : public getClosingBalances(String societyID,String buildingID)");
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getClosingBalances : " + ex);
		} catch (IllegalArgumentException ex) {
			//ex.printStackTrace();
			reportList = null;
			logger.info("Exception in  : " + ex);
		}

		catch (Exception ex) {
			logger.error("Exception in getClosingBalances : " + ex);
		}
		return reportList;
	}

	public List getClosingBalanceSummary(int societyID, String buildingID,
			String uptoDate) {
		List reportList = null;
		try {
			logger
					.debug("Entry : public List getClosingBalances(String societyID,String buildingID))"
							+ societyID);
			String strWhereClause = null;

			if (buildingID.equalsIgnoreCase("0"))
				strWhereClause = " ";
			else
				strWhereClause = "  AND a.building_id=" + buildingID + "";

			String sqlDue = "SELECT b.building_id,b.building_name,COUNT(DISTINCT a.apt_name) AS apt_count,SUM(CASE WHEN invoice_type_id=1 THEN current_balance ELSE 0 END) AS regular, "
					+ " SUM(CASE WHEN invoice_type_id=2 THEN current_balance ELSE 0 END) AS sinkingFund, "
					+ "SUM(CASE WHEN invoice_type_id=3 THEN current_balance ELSE 0 END) AS serviceCharge, "
					+ "SUM(CASE WHEN invoice_type_id=4 THEN current_balance ELSE 0 END) AS infraFund, "
					+ "SUM(CASE WHEN invoice_type_id=5 THEN current_balance ELSE 0 END) AS legalFee, "
					+ "SUM(CASE WHEN invoice_type_id=6 THEN current_balance ELSE 0 END) AS meter , "
					+ "SUM(CASE WHEN invoice_type_id=7 THEN current_balance ELSE 0 END) AS memberTransfer, "
					+ "SUM(CASE WHEN invoice_type_id=8 THEN current_balance ELSE 0 END) AS contegency, "
					+ "SUM(CASE WHEN invoice_type_id=9 THEN current_balance ELSE 0 END) AS openCar, "
					+ "SUM(CASE WHEN invoice_type_id=10 THEN current_balance ELSE 0 END) AS lateFee, "
					+ "SUM(CASE WHEN invoice_type_id=11 THEN current_balance ELSE 0 END) AS other, "
					+ "SUM(CASE WHEN invoice_type_id=12 THEN current_balance ELSE 0 END) AS corpus,"
					+ "SUM(CASE WHEN invoice_type_id=13 THEN current_balance ELSE 0 END) AS societyFrm,"
					+ "SUM(CASE WHEN invoice_type_id=14 THEN current_balance ELSE 0 END) AS water,"
					+ "SUM(CASE WHEN invoice_type_id=15 THEN current_balance ELSE 0 END) AS repair,"
					+ "SUM(CASE WHEN invoice_type_id=16 THEN current_balance ELSE 0 END) AS lift  FROM in_invoice_"
					+ societyID
					+ " i,member_details m ,apartment_details a ,ledger_user_mapping_"
					+ societyID
					+ " mu ,building_details b "
					+ "WHERE m.apt_id=i.apt_id AND m.apt_id=a.apt_id AND a.building_id=b.building_id  AND m.member_id=mu.user_id and (status_id=0 OR status_id=1)  AND i.invoice_date<=:uptoDate AND i.is_deleted=0 AND b.society_id=:societyID "
					+ strWhereClause
					+ " GROUP BY b.building_id ORDER BY seq_no;";

			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("uptoDate", uptoDate);

			RowMapper rMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int num) throws SQLException {
					// TODO Auto-generated method stub
					RptTransactionVO reportVO = new RptTransactionVO();
					reportVO.setBuildingId(rs.getString("building_id"));
					reportVO.setBuildingName(rs.getString("building_name"));
					reportVO.setAptCount(rs.getInt("apt_count"));
					reportVO.setDue(rs.getBigDecimal("regular"));
					reportVO.setDueForSF(rs.getBigDecimal("sinkingFund"));
					reportVO.setDueForMMC(rs.getBigDecimal("serviceCharge"));
					reportVO.setDueForInfra(rs.getBigDecimal("infraFund"));
					reportVO.setDueForLegalFee(rs.getBigDecimal("legalFee"));
					reportVO.setDueForMeter(rs.getBigDecimal("meter"));
					reportVO.setDueForTransfer(rs
							.getBigDecimal("memberTransfer"));
					reportVO
							.setDueForContigency(rs.getBigDecimal("contegency"));
					reportVO.setDueForCarParking(rs.getBigDecimal("openCar"));
					reportVO.setLateFees(rs.getBigDecimal("lateFee"));
					reportVO.setDueForOther(rs.getBigDecimal("other"));
					reportVO.setDueForCorpus(rs.getBigDecimal("corpus"));
					reportVO
							.setDueForSocietyFrm(rs.getBigDecimal("societyFrm"));
					reportVO.setDueForWaterCharges(rs.getBigDecimal("water"));
					reportVO.setDueForRM(rs.getBigDecimal("repair"));
					reportVO.setDueForLift(rs.getBigDecimal("lift"));

					return reportVO;
				}
			};
			reportList = namedParameterJdbcTemplate
					.query(sqlDue, hMap, rMapper);

			logger.debug("Outside Query : " + reportList.size());
			logger
					.debug("Exit : public getClosingBalanceSummary(String societyID,String buildingID)");
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getClosingBalanceSummary : " + ex);
		} catch (IllegalArgumentException ex) {
			//ex.printStackTrace();
			reportList = null;
			logger.info("Exception in  : " + ex);
		}

		catch (Exception ex) {
			logger.error("Exception in getClosingBalanceSummary : " + ex);
		}
		return reportList;
	}*/

	public List getMemberDueWithClosingBal(int societyID, int buildingID,
			String uptoDate) {

		try {
			logger
					.debug("Entry : public List getClosingBalances(String societyID,String buildingID))"
							+ societyID);
			final SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			String strWhereClause = null;
			if (buildingID == 0)
				strWhereClause = " ";
			else
				strWhereClause = "  AND a.building_id=" + buildingID + "";
			//String sqlDue="SELECT MAX(tx_to_date) , soc_tx_"+intSocietyID+".*, MAX(tx_to_date) AS maxdt,member_details.* FROM soc_tx_"+intSocietyID+" ,member_details WHERE member_details.member_id=soc_tx_"+intSocietyID+".table_primary_id AND charge_type = 'Monthly maintenance Charges' GROUP BY soc_tx_"+intSocietyID+".table_primary_id;";
			/*String sql = " SELECT a.building_id,unit_name,full_name, ale.ledger_id,al.ledger_name,al.opening_balance,mobile,email,m.apt_id,m.member_id,m.title, "+
			 " IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount)) AS sumAmt, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS closing_balance "+
			 " FROM account_ledgers_"+societyID+" al, account_transactions_"+societyID+" ats,account_ledger_entry_"+societyID+" ale ,members_info m,ledger_user_mapping_"+societyID+" lu ,apartment_details a"+
			 " WHERE ats.tx_id=ale.tx_id AND m.society_id=:societyID AND ats.tx_date <= :uptoDate AND ats.is_deleted=0 AND m.member_id=lu.user_id AND lu.ledger_id=al.id AND m.type='P'  AND m.is_current=1 "+strWhereClause+" "+
			 " AND ale.ledger_id = al.id AND m.apt_id=a.apt_id"+
			 " GROUP BY ale.ledger_id ORDER BY seq_no ;";*/

			String sql = "SELECT * FROM (SELECT ale.ledger_id,al.ledger_name,al.opening_balance ,"
					+ " IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount)) AS sumAmt, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS closing_balance "
					+ " FROM account_ledgers_"
					+ societyVO.getDataZoneID()
					+ " al, account_transactions_"
					+ societyVO.getDataZoneID()
					+ " ats,account_ledger_entry_"
					+ societyVO.getDataZoneID()
					+ " ale "
					+ " WHERE ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID and ats.tx_id=ale.tx_id AND ats.tx_date <= :uptoDate AND al.sub_group_id=1 AND ats.is_deleted=0 AND ale.ledger_id = al.id "
					+ " GROUP BY al.id  ) AS ledger LEFT JOIN ( SELECT a.building_id,unit_name,full_name,mobile,email,m.apt_id,m.member_id,m.title,ledger_id,seq_no FROM members_info m,ledger_user_mapping_"
					+ societyVO.getDataZoneID()
					+ " l,apartment_details a WHERE m.society_id=l.org_id and m.member_id=l.user_id AND m.society_id="
					+ societyID
					+ " "
					+ strWhereClause
					+ " AND m.apt_id=a.apt_id AND m.type='P') AS member ON ledger.ledger_id=member.ledger_id ORDER BY ISNULL(seq_no), seq_no ASC ;";

			/*String sqlDue = "SELECT * FROM member_details m,apartment_details a,building_details b,ledger_user_mapping_"+societyID+" lu "
			 + " WHERE m.apt_id=a.apt_id   "+strWhereClause+" AND a.building_id=b.building_id AND m.member_id=lu.user_id AND lu.ledger_type='M' AND b.society_id=:societyID order by seq_no;";*/

			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("uptoDate", uptoDate);

			RowMapper rMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int num) throws SQLException {
					// TODO Auto-generated method stub
					RptTransactionVO reportVO = new RptTransactionVO();
					reportVO.setSocietyVO(societyVO);
					reportVO.setMember_id(rs.getInt("member_id"));
					if ((rs.getInt("apt_id") == 0)) {
						reportVO.setAptID(0);
						reportVO.setBuildingId("0");
						reportVO.setEmail("NA");
						reportVO.setMobile("NA");
						reportVO.setMember_name(rs.getString("ledger_name"));
						reportVO.setLedgerName(rs.getString("ledger_name"));
					} else {
						reportVO.setBuildingId(rs.getString("building_id"));
						reportVO.setAptID(rs.getInt("apt_id"));
						reportVO.setEmail(rs.getString("email"));
						reportVO.setMobile(rs.getString("mobile"));
						reportVO.setLedgerName(rs.getString("unit_name")
								+ " - " + rs.getString("title") + " "
								+ rs.getString("full_name"));
						reportVO.setMember_name(rs.getString("title") + " "
								+ rs.getString("full_name"));
					}
					reportVO.setAptID(rs.getInt("apt_id"));

					reportVO.setLedgerID(rs.getInt("ledger_id"));

					reportVO.setDue(rs.getBigDecimal("closing_balance"));
					if (reportVO.getDue().compareTo(BigDecimal.ZERO) > 0) {
						reportVO.setDebitAmt(reportVO.getDue());
					} else
						reportVO.setCreditAmt(reportVO.getDue().abs());

					return reportVO;
				}
			};
			reportList = namedParameterJdbcTemplate.query(sql, hMap, rMapper);

			logger.debug("Outside Query : " + reportList.size());
			logger
					.debug("Exit : public getClosingBalances(String societyID,String buildingID)");
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getClosingBalances : " + ex);
		} catch (IllegalArgumentException ex) {
			//ex.printStackTrace();
			reportList = null;
			logger.info("Exception in  : " + ex);
		}

		catch (Exception ex) {
			logger.error("Exception in getClosingBalances : " + ex);
		}
		return reportList;
	}

	public List getReceivableListInBillFormat(int orgID,final String fromDate,
			final String uptoDate) {

			try {
				logger
						.debug("Entry : public List getReceivableListInBillFormat(int societyID,String fromDate,String uptoDate)"
								+ orgID);
				SocietyVO societyVO=societyService.getSocietyDetails(orgID);

				 String sql="SELECT member.member_id,member.id AS ledgerId ,txTable.member_name,IF(txTable.closing_balance IS NULL,0,txTable.closing_balance) AS closing_balance,txTable.opening_balance,txTable.intClosingBalance,(txTable.closing_balance-txTable.intClosingBalance) AS principleBalance,member.opening_balance AS openingBalWhenNoTx,member.sub_group_id AS group_id,member.ledger_name AS ledger_name,IF(sumCredit IS NULL,0,sumCredit) AS sumCredit,IF(sumDebit IS NULL,0,sumDebit) AS sumDebit FROM ( Select a.*,lu.user_id AS member_id from  account_ledgers_"+societyVO.getDataZoneID()+" a ,ledger_user_mapping_"+societyVO.getDataZoneID()+" lu WHERE a.org_id=lu.org_id and  a.org_id=:orgID AND a.id=lu.ledger_id) AS member "+
				            " LEFT JOIN (SELECT closingBal.member_name, closingBal.member_id,closingBal.ledger_id AS ledger_id, closingBal.closing_balance AS closing_balance,openingBal.opening_balance, closingBal.int_closing_balance as intClosingBalance ,(closingBal.sumCredit-IF(openingBal.sumCredit IS NULL,0,openingBal.sumCredit)) AS sumCredit ,(closingBal.sumDebit-IF(openingBal.sumDebit IS NULL,0,openingBal.sumDebit)) AS sumDebit,closingBal.org_id as society_id FROM( SELECT txCls.*,l.member_id,l.member_name FROM (SELECT ale.org_id,ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0,"+
				            " SUM(ale.amount) + al.opening_balance) AS closing_balance,IF(SUM(ale.int_amount) IS NULL, 0, SUM(ale.int_amount) + al.int_balance) AS int_closing_balance,SUM(CASE WHEN ale.type = 'C' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumCredit,SUM(CASE WHEN ale.type = 'D' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumDebit,al.is_deleted "+
				            " FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale WHERE ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:orgID and  ats.tx_id=ale.tx_id AND ats.tx_date <=:toDate AND ats.is_deleted=0 AND ale.ledger_id = al.id AND sub_group_id=1 "+
				            " GROUP BY ale.ledger_id  ORDER BY ale.ledger_id ) AS txCls right JOIN (SELECT user_id AS member_id,ledger_id,CONCAT(m.unit_name,' - ',m.title,' ',m.full_name) AS member_name FROM ledger_user_mapping_"+societyVO.getDataZoneID()+" l,members_info m WHERE l.org_id=m.society_id and m.member_id=l.user_id AND m.society_id="+orgID+") AS l ON txCls.ledger_id=l.ledger_id) AS closingBal "+
				            " LEFT JOIN (SELECT txOpn.*,l.member_id,l.member_name FROM (SELECT ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS opening_balance,SUM(CASE WHEN ale.type = 'C' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumCredit,SUM(CASE WHEN ale.type = 'D' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumDebit,al.is_deleted  "+
				            " FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale WHERE  ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:orgID and  ats.tx_id=ale.tx_id AND ats.tx_date < :fromDate AND ats.is_deleted=0 AND ale.ledger_id = al.id AND sub_group_id=1 "+
				            " GROUP BY ale.ledger_id  ORDER BY ale.ledger_id ) AS txOpn right JOIN (SELECT user_id AS member_id,ledger_id ,CONCAT(m.unit_name,' - ',m.title,' ',m.full_name) AS member_name FROM ledger_user_mapping_"+societyVO.getDataZoneID()+" l,members_info m WHERE l.org_id=m.society_id and m.member_id=l.user_id AND m.society_id="+orgID+") AS l ON txOpn.ledger_id=l.ledger_id) AS openingBal ON closingBal.ledger_id=openingBal.ledger_id ) AS txTable ON member.member_id=txTable.member_id WHERE  member.org_id=txTable.society_id AND txTable.ledger_id=member.id and member.sub_group_id=1 AND member.is_deleted=0 ;";
				logger.debug("Query is " + sql);

				Map hashMap = new HashMap();

				hashMap.put("orgID", orgID);
				hashMap.put("fromDate", fromDate);
				hashMap.put("toDate", uptoDate);

				RowMapper rMapper = new RowMapper() {

					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						BillDetailsVO billVO = new BillDetailsVO();
						billVO.setMemberID(rs.getInt("member_id"));
						billVO.setLedgerName(rs.getString("ledger_name"));
						if(billVO.getMemberID()==0){
							billVO.setMemberName(rs.getString("ledger_name"));
						}else
						billVO.setMemberName(rs.getString("member_name"));
						
						billVO.setLedgerID(rs.getInt("ledgerId"));
						if (rs.getBigDecimal("opening_balance") == null) {
							billVO.setOpeningBalance(rs
									.getBigDecimal("openingBalWhenNoTx"));
						} else
							billVO.setOpeningBalance(rs
									.getBigDecimal("opening_balance"));
						if (rs.getBigDecimal("closing_balance") == null) {
							billVO.setClosingBalance(BigDecimal.ZERO);
						} else
							billVO.setClosingBalance(rs
									.getBigDecimal("closing_balance"));
						//logger.info(rs.getInt("ledgerId")+" "+rs.getBigDecimal("sumDebit")+" Here "+rs.getBigDecimal("opening_balance"));
						billVO.setTotalAmount(billVO.getOpeningBalance().add(
								rs.getBigDecimal("sumDebit").abs()));
						billVO.setChargedAmount(rs.getBigDecimal("sumDebit").abs());
						billVO.setPaidAmount(rs.getBigDecimal("sumCredit").abs());
						billVO.setIntBalance(rs.getBigDecimal("intClosingBalance"));
						billVO.setPrincipleBalance(rs.getBigDecimal("principleBalance"));
						billVO.setFromDate(fromDate);
						billVO.setUptoDate(uptoDate);
						return billVO;
					}
				};

				reportList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);

				logger.debug("Outside Query : " + reportList.size());
				logger
						.debug("Exit : public List getReceivableListInBillFormat(int societyID,String fromDate,String uptoDate)");
			} catch (BadSqlGrammarException ex) {
				logger.info("Exception in getClosingBalances : " + ex);
			} catch (IllegalArgumentException ex) {
				//ex.printStackTrace();
				reportList = null;
				logger.info("Exception in  : " + ex);
			}

			catch (Exception ex) {
				logger.error("Exception in getClosingBalances : " + ex);
			}
			return reportList;
	}

	public List getReceivableListForAllTags(int orgID,
			final String uptoDate) {

			try {
				logger
						.debug("Entry : public List getReceivableListForAllTags(int societyID,String fromDate,String uptoDate)"
								+ orgID);
				SocietyVO societyVO=societyService.getSocietyDetails(orgID);

				 String sql="SELECT ledgerID,ledger_name,ledger_meta,SUM(opening_balance) as opening_balance,SUM(closing_balance) as closing_balance "+
				            " FROM (SELECT m.ledgerID,m.ledger_name,m.ledger_meta,m.opening_balance,SUM(ledger.closing_balance)  AS closing_balance "+
				            " FROM (SELECT ale.ledger_id,al.ledger_name,al.opening_balance , IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount)) AS sumAmt, IF(IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance)>0,IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance),0) AS closing_balance "+
				            " FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale WHERE ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:orgID and  ats.tx_id=ale.tx_id AND ats.tx_date <=:toDate AND ats.is_deleted=0 AND ale.ledger_id = al.id AND sub_group_id=1 "+
				            " GROUP BY al.id ) AS ledger Left JOIN ( SELECT a.id AS ledgerID,a.opening_balance,a.int_balance,l.ledger_meta,a.ledger_name FROM account_ledgers_"+societyVO.getDataZoneID()+" a,account_ledger_profile_details l  WHERE a.id=l.ledger_id AND a.org_id=l.org_id AND l.org_id=:orgID "+
				            " AND a.sub_group_id=1 ) AS m ON ledger.ledger_id=m.ledgerID GROUP BY m.ledgerID) AS receivables GROUP BY ledger_meta  ";
				           
				logger.debug("Query is " + sql);

				Map hashMap = new HashMap();

				hashMap.put("orgID", orgID);
				hashMap.put("toDate", uptoDate);

				RowMapper rMapper = new RowMapper() {

					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						AccountHeadVO accVO=new AccountHeadVO();
						accVO.setLedgerID(rs.getInt("ledgerID"));
						accVO.setLedgerName(rs.getString("ledger_name"));
						accVO.setAdditionalProps(rs.getString("ledger_meta"));
						accVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
						accVO.setClosingBalance(rs.getBigDecimal("closing_balance"));
						
						return accVO;
					}
				};

				reportList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);

				logger.debug("Outside Query : " + reportList.size());
				logger
						.debug("Exit : public List getReceivableListForAllTags(int societyID,String fromDate,String uptoDate)");
			} catch (BadSqlGrammarException ex) {
				logger.info("Exception in getReceivableListForAllTags : " + ex);
			} catch (ArrayIndexOutOfBoundsException ex) {
				logger.info("Exception in getReceivableListForAllTags : " + ex);
			} catch (IllegalArgumentException ex) {
				//ex.printStackTrace();
				reportList = null;
				logger.info("Exception in getReceivableListForAllTags : " + ex);
			}

			catch (Exception ex) {
				logger.error("Exception in getReceivableListForAllTags : " + ex);
			}
			return reportList;
	}
	
	public List getReceivableListForSingleTags(int orgID,
			final String uptoDate, String ledgerMeta) {

			try {
				logger
						.debug("Entry : public List getReceivableListForSingleTags(int societyID,String fromDate,String uptoDate)"
								+ orgID);
				SocietyVO societyVO=societyService.getSocietyDetails(orgID);

				 String sql="SELECT ledgerID,ledger_name,ledger_meta,SUM(opening_balance) as opening_balance,SUM(closing_balance) as closing_balance "+
				            " FROM (SELECT m.ledgerID,m.ledger_name,m.ledger_meta,m.opening_balance,SUM(ledger.closing_balance) AS closing_balance "+
				            " FROM (SELECT ale.ledger_id,al.ledger_name,al.opening_balance , IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount)) AS sumAmt, IF(IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance)>0,IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance),0) AS closing_balance "+
				            " FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale WHERE ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:orgID and  ats.tx_id=ale.tx_id AND ats.tx_date <=:toDate AND ats.is_deleted=0 AND ale.ledger_id = al.id AND sub_group_id=1 "+
				            " GROUP BY al.id ) AS ledger left JOIN ( SELECT a.id AS ledgerID,a.opening_balance,a.int_balance,l.ledger_meta,a.ledger_name FROM account_ledgers_"+societyVO.getDataZoneID()+" a,account_ledger_profile_details l  WHERE a.id=l.ledger_id AND a.org_id=l.org_id AND l.org_id=:orgID "+
				            " AND a.sub_group_id=1 ) AS m ON ledger.ledger_id=m.ledgerID GROUP BY m.ledgerID) AS receivables WHERE ledger_meta LIKE '%"+ledgerMeta+"%' GROUP BY ledgerID   ";
				           
				logger.debug("Query is " + sql);

				Map hashMap = new HashMap();

				hashMap.put("orgID", orgID);
				hashMap.put("toDate", uptoDate);

				RowMapper rMapper = new RowMapper() {

					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						AccountHeadVO accVO=new AccountHeadVO();
						accVO.setLedgerID(rs.getInt("ledgerID"));
						accVO.setLedgerName(rs.getString("ledger_name"));
						accVO.setAdditionalProps(rs.getString("ledger_meta"));
						accVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
						accVO.setClosingBalance(rs.getBigDecimal("closing_balance"));
						
						return accVO;
					}
				};

				reportList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);

				logger.debug("Outside Query : " + reportList.size());
				logger
						.debug("Exit : public List getReceivableListForSingleTags(int societyID,String fromDate,String uptoDate)");
			} catch (BadSqlGrammarException ex) {
				logger.info("Exception in getReceivableListForSingleTags : " + ex);
			} catch (IllegalArgumentException ex) {
				//ex.printStackTrace();
				reportList = null;
				logger.info("Exception in getReceivableListForSingleTags : " + ex);
			}

			catch (Exception ex) {
				logger.error("Exception in getReceivableListForSingleTags : " + ex);
			}
			return reportList;
	}
	
	public List getTrialBalanceReport(int societyID, String fromDate,
			String uptoDate, String effectiveDate,String dayBeforeFromDate,int isConsolidated,String rootType) {

		List<AccountHeadVO> groupList = new ArrayList<AccountHeadVO>();
		try {
			logger
					.debug("Entry : List getTrialBalanceReport(int societyID,String fromDate, String uptoDate,String effectiveDate)");

			String query = "SELECT opening_balance.report_group_name as report_group_name,opening_balance.id AS report_group_id,opening_balance.root_name,opening_balance.normal_balance,opening_balance.root_id,opening_balance.cl_pos as op_pos,opening_balance.cl_neg as op_neg,closing_balance.cl_pos as cl_pos ,closing_balance.cl_neg as cl_neg ,opening_balance.category_name,opening_balance.category_id,opening_balance.seq_no "
					+ " from ( "
					+ createTrialBalanceQuery(societyID, effectiveDate,
							dayBeforeFromDate,isConsolidated,rootType)
					+ " ) as opening_balance, ( "
					+ createTrialBalanceQuery(societyID, fromDate,
							uptoDate,isConsolidated,rootType)
					+ " ) as closing_balance "
					+ " where opening_balance.id=closing_balance.id  order by opening_balance.seq_no ";
			logger.debug(query);

			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", uptoDate);

			RowMapper Rmapper = new RowMapper() {
				String tempCategoryName="";
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					AccountHeadVO acHVO = new AccountHeadVO();
					acHVO.setCategoryID(rs.getInt("category_id"));
					acHVO.setCategoryName(rs.getString("category_name"));
					acHVO.setOpPosBal(rs.getBigDecimal("op_pos"));
					acHVO.setOpNegBal(rs.getBigDecimal("op_neg"));
					acHVO.setClPosBal(rs.getBigDecimal("cl_pos"));
					acHVO.setClNegBal(rs.getBigDecimal("cl_neg"));
					acHVO.setRootID(rs.getInt("root_id"));
					acHVO.setStrAccountPrimaryHead(rs
							.getString("report_group_name"));
					acHVO.setAccountGroupID(rs.getInt("report_group_id"));
					acHVO.setRootGroupName(rs.getString("root_name"));

					acHVO.setNormalBalance(rs.getString("normal_balance"));
					return acHVO;
				}

			};
			groupList = namedParameterJdbcTemplate.query(query, hMap, Rmapper);

			logger
					.debug("Exit : List getTrialBalanceReport(int societyID,String fromDate, String uptoDate,String effectiveDate)"
							+ groupList.size());

		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in Renter History : " + ex);
		} catch (EmptyResultDataAccessException ex) {
			logger.debug("No data available for this apartment : ");
		} catch (Exception e) {
			logger.error("Exception : " + e);
		}

		return groupList;
	}

	public List getLedgerReport(final int societyID, final String fromDate,
			String uptoDate, int groupID, String effectiveDate,int isConsolidated) {

		List<AccountHeadVO> ledgerList = new ArrayList<AccountHeadVO>();
		String orgCondition="";
		String orgClause="";
		if(isConsolidated==0){
			orgClause=" and al.org_id=:societyID ";
			orgCondition=" a.org_id=:societyID AND ";
		}
		try {

			logger
					.debug("Entry : public List getLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String effectiveDate)"
							+ groupID);
			SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			String query = " SELECT a.id AS ledger_id,txTable.closing_balance,txTable.opening_balance,a.opening_balance AS openingBalWhenNoTx,ag.group_name,a.sub_group_id AS group_id,a.ledger_name AS ledger_name,ag.root_id,ar.normal_balance FROM account_groups ag,account_root_group ar,account_ledgers_"
					+  societyVO.getDataZoneID()
					+ " a LEFT JOIN "
					+ " (SELECT closingBal.ledger_id AS ledger_id, closingBal.closing_balance AS closing_balance,openingBal.opening_balance,group_name,root_id,normal_balance,closingBal.org_id  FROM(SELECT ale.org_id,ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS closing_balance,al.is_deleted ,r.root_id,r.normal_balance,g.group_name   "
					+ "  FROM account_ledgers_"
					+ societyVO.getDataZoneID()
					+ " al, account_transactions_"
					+  societyVO.getDataZoneID()
					+ " ats,account_ledger_entry_"
					+  societyVO.getDataZoneID()
					+ " ale,account_groups g,account_root_group r WHERE ats.org_id=ale.org_id and ale.org_id=al.org_id "+orgClause+" and ats.tx_id=ale.tx_id AND al.sub_group_id=g.id AND g.root_id=r.root_id AND ats.tx_date <= :toDate AND ats.is_deleted=0 "
					+ " AND ale.ledger_id = al.id AND sub_group_id=:groupID  GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS closingBal LEFT JOIN (SELECT ale.org_id, ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS opening_balance,al.is_deleted  "
					+ " FROM account_ledgers_"
					+  societyVO.getDataZoneID()
					+ " al, account_transactions_"
					+  societyVO.getDataZoneID()
					+ " ats,account_ledger_entry_"
					+  societyVO.getDataZoneID()
					+ " ale WHERE  ats.org_id=ale.org_id and ale.org_id=al.org_id "+orgClause+" and ats.tx_id=ale.tx_id  AND ats.tx_date <:fromDate AND ats.is_deleted=0 AND ale.ledger_id = al.id AND sub_group_id=:groupID  GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS openingBal ON closingBal.ledger_id=openingBal.ledger_id ) AS txTable ON a.id=txTable.ledger_id WHERE "+orgCondition+"  a.sub_group_id=:groupID and a.is_deleted=0 AND a.sub_group_id=ag.id AND ag.root_id=ar.root_id;";
			logger.debug(query);
			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", uptoDate);
			hMap.put("groupID", groupID);

			RowMapper Rmapper = new RowMapper() {
				String normalBalance = "";

				int rootID = 0;

				String groupName = "";

				int groupID = 0;

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					AccountHeadVO acHVO = new AccountHeadVO();

					acHVO
							.setOpeningBalance(rs
									.getBigDecimal("opening_balance"));
					acHVO
							.setClosingBalance(rs
									.getBigDecimal("closing_balance"));

					acHVO.setRootID(rs.getInt("root_id"));
					acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
					acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
					acHVO.setAccountGroupID(rs.getInt("group_id"));
					acHVO.setLedgerName(rs.getString("ledger_name"));
					//acHVO.setCategoryID(rs.getInt("group_id"));
					acHVO.setCategoryName(rs.getString("group_name"));
					acHVO.setLedgerID(rs.getInt("ledger_id"));
					if (rs.getString("normal_balance") != null) {
						acHVO.setNormalBalance(rs.getString("normal_balance"));
						normalBalance = rs.getString("normal_balance");
						rootID = rs.getInt("root_id");
						groupName = rs.getString("group_name");
						groupID = rs.getInt("group_id");

					}
					if ((rs.getBigDecimal("opening_balance") == null)
							&& (rs.getBigDecimal("closing_balance") == null)) {

						acHVO.setClosingBalance(rs
								.getBigDecimal("openingBalWhenNoTx"));
						acHVO.setOpeningBalance(rs
								.getBigDecimal("openingBalWhenNoTx"));
						acHVO.setNormalBalance(normalBalance);
						acHVO.setRootID(rootID);
						acHVO.setAccountGroupID(groupID);
						acHVO.setStrAccountPrimaryHead(groupName);
					} else if (rs.getBigDecimal("opening_balance") == null) {
						acHVO.setNormalBalance(normalBalance);
						acHVO.setRootID(rootID);
						acHVO.setAccountGroupID(groupID);
						acHVO.setStrAccountPrimaryHead(groupName);
						acHVO.setOpeningBalance(rs
								.getBigDecimal("openingBalWhenNoTx"));
					}

					return acHVO;
				}

			};
			ledgerList = namedParameterJdbcTemplate.query(query, hMap, Rmapper);

			logger
					.debug("Exit : public List getLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String effectiveDate)"
							+ ledgerList.size());
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getLedgerReport : " + ex);
		} catch (Exception e) {
			logger.error("Exception : " + e);
		}

		return ledgerList;
	}
	
	public List getGroupLedgerReport(final int societyID, final String fromDate,
			String uptoDate, int categoryID, String effectiveDate) {

		List<AccountHeadVO> ledgerList = new ArrayList<AccountHeadVO>();

		try {

			logger
					.debug("Entry : public List getGroupLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String effectiveDate)"
							+ categoryID);
			SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			String query = " SELECT txTable.ledger_id,g.id as sub_group_id,txTable.closing_balance AS closing_balance,txTable.opening_balance AS opening_balance,g.group_name,g.org_id,g.category_id,g.balance as openingBalWhenNoTx "
					+ " from (SELECT closingBal.ledger_id AS ledger_id,closingBal.sub_group_id,closingBal.category_id, closingBal.closing_balance AS closing_balance,openingBal.opening_balance,closingBal.group_name,root_id,normal_balance,closingBal.org_id  FROM(SELECT txCls.org_id,txCls.ledger_id,txCls.ledger_name,txCls.sub_group_id,g.group_name,g.category_id,SUM(txCls.cls_balance) AS closing_balance,txCls.root_id,txCls.normal_balance FROM(SELECT ale.org_id,ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS cls_balance,al.is_deleted ,r.root_id,r.normal_balance,g.group_name   "
					+ "  FROM account_ledgers_"
					+ societyVO.getDataZoneID()
					+ " al, account_transactions_"
					+  societyVO.getDataZoneID()
					+ " ats,account_ledger_entry_"
					+  societyVO.getDataZoneID()
					+ " ale,account_groups g,account_root_group r WHERE ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID and ats.tx_id=ale.tx_id AND al.sub_group_id=g.id AND g.root_id=r.root_id AND ats.tx_date <= :toDate AND ats.is_deleted=0 "
					+ " AND ale.ledger_id = al.id   GROUP BY ale.ledger_id  ORDER BY ale.ledger_id)AS txCls,account_groups g WHERE txCls.sub_group_id=g.id GROUP BY g.id ) AS closingBal LEFT JOIN (SELECT tx.org_id,tx.ledger_id,tx.ledger_name,tx.sub_group_id,g.group_name,g.category_id,SUM(tx.opn_balance) AS opening_balance FROM(SELECT ale.org_id, ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS opn_balance,al.is_deleted  "
					+ " FROM account_ledgers_"
					+  societyVO.getDataZoneID()
					+ " al, account_transactions_"
					+  societyVO.getDataZoneID()
					+ " ats,account_ledger_entry_"
					+  societyVO.getDataZoneID()
					+ " ale WHERE  ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID and ats.tx_id=ale.tx_id  AND ats.tx_date <:fromDate AND ats.is_deleted=0 AND ale.ledger_id = al.id   GROUP BY ale.ledger_id  ORDER BY ale.ledger_id)AS tx,account_groups g WHERE tx.sub_group_id=g.id GROUP BY g.id ) AS openingBal ON closingBal.sub_group_id=openingBal.sub_group_id ) AS txTable RIGHT JOIN ( SELECT SUM(l.opening_balance) AS balance,ag.group_name,ag.category_id,ag.id,l.org_id FROM account_ledgers_"+societyVO.getDataZoneID()+" l,account_groups ag WHERE l.sub_group_id=ag.id AND l.is_deleted=0 AND l.org_id=:societyID GROUP BY ag.id) AS g  ON g.id=txTable.sub_group_id  WHERE g.category_id=:categoryID ; ";
					
			logger.debug(query);
			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", uptoDate);
			hMap.put("categoryID", categoryID);

			RowMapper Rmapper = new RowMapper() {
				String normalBalance = "";

				int rootID = 0;

				String groupName = "";

				int groupID = 0;

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					AccountHeadVO acHVO = new AccountHeadVO();

					acHVO
							.setOpeningBalance(rs
									.getBigDecimal("opening_balance"));
					acHVO
							.setClosingBalance(rs
									.getBigDecimal("closing_balance"));

					
					acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
					
					acHVO.setAccountGroupID(rs.getInt("sub_group_id"));
					//acHVO.setCategoryID(rs.getInt("group_id"));
					acHVO.setCategoryName(rs.getString("group_name"));
					acHVO.setLedgerID(rs.getInt("ledger_id"));
		
					if ((rs.getBigDecimal("opening_balance") == null)
							&& (rs.getBigDecimal("closing_balance") == null)) {

						acHVO.setClosingBalance(rs
								.getBigDecimal("openingBalWhenNoTx"));
						acHVO.setOpeningBalance(rs
								.getBigDecimal("openingBalWhenNoTx"));
						acHVO.setNormalBalance(normalBalance);
						
						acHVO.setCategoryName(groupName);
					} else if (rs.getBigDecimal("opening_balance") == null) {
						acHVO.setNormalBalance(normalBalance);
					
						acHVO.setCategoryName(groupName);
						acHVO.setOpeningBalance(rs
								.getBigDecimal("openingBalWhenNoTx"));
					}


					return acHVO;
				}

			};
			ledgerList = namedParameterJdbcTemplate.query(query, hMap, Rmapper);

			logger
					.debug("Exit : public List getGroupLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String effectiveDate)"
							+ ledgerList.size());
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getGroupLedgerReport : " + ex);
		} catch (Exception e) {
			logger.error("Exception : " + e);
		}

		return ledgerList;
	}
	
	public List getLedgerReportFromCategory(final int societyID, final String fromDate,
			String uptoDate, int categoryID, String effectiveDate,int isConsolidated) {

		List<AccountHeadVO> ledgerList = new ArrayList<AccountHeadVO>();
		String orgCondition="";
		String orgClause="";
		if(isConsolidated==0){
			orgClause=" and al.org_id=:societyID ";
			orgCondition=" a.org_id=:societyID AND ";
		}
		try {

			logger
					.debug("Entry : public List getLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String effectiveDate)"
							+ categoryID);
			SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			String query = " SELECT a.id AS ledger_id,txTable.closing_balance,txTable.opening_balance,a.opening_balance AS openingBalWhenNoTx,ag.group_name,a.sub_group_id AS group_id,a.ledger_name AS ledger_name,ag.root_id,ar.normal_balance FROM account_groups ag,account_root_group ar, account_category c,account_ledgers_"
					+  societyVO.getDataZoneID()
					+ " a LEFT JOIN "
					+ " (SELECT closingBal.ledger_id AS ledger_id, closingBal.closing_balance AS closing_balance,openingBal.opening_balance,group_name,root_id,normal_balance,closingBal.org_id  FROM(SELECT ale.org_id,ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS closing_balance,al.is_deleted ,r.root_id,r.normal_balance,g.group_name   "
					+ "  FROM account_ledgers_"
					+ societyVO.getDataZoneID()
					+ " al, account_transactions_"
					+  societyVO.getDataZoneID()
					+ " ats,account_ledger_entry_"
					+  societyVO.getDataZoneID()
					+ " ale,account_groups g,account_root_group r WHERE ats.org_id=ale.org_id and ale.org_id=al.org_id "+orgClause+" and ats.tx_id=ale.tx_id AND al.sub_group_id=g.id AND g.root_id=r.root_id AND ats.tx_date <= :toDate AND ats.is_deleted=0 "
					+ " AND ale.ledger_id = al.id  GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS closingBal LEFT JOIN (SELECT ale.org_id, ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS opening_balance,al.is_deleted  "
					+ " FROM account_ledgers_"
					+  societyVO.getDataZoneID()
					+ " al, account_transactions_"
					+  societyVO.getDataZoneID()
					+ " ats,account_ledger_entry_"
					+  societyVO.getDataZoneID()
					+ " ale WHERE  ats.org_id=ale.org_id and ale.org_id=al.org_id "+orgClause+" and ats.tx_id=ale.tx_id  AND ats.tx_date <:fromDate AND ats.is_deleted=0 AND ale.ledger_id = al.id  GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS openingBal ON closingBal.ledger_id=openingBal.ledger_id ) AS txTable ON a.id=txTable.ledger_id WHERE "+orgCondition+"  ag.category_id=c.category_id AND c.category_id=:groupID and a.is_deleted=0 AND a.sub_group_id=ag.id AND ag.root_id=ar.root_id;";
			logger.debug(query);
			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", uptoDate);
			hMap.put("groupID", categoryID);

			RowMapper Rmapper = new RowMapper() {
				String normalBalance = "";

				int rootID = 0;

				String groupName = "";

				int groupID = 0;

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					AccountHeadVO acHVO = new AccountHeadVO();

					acHVO
							.setOpeningBalance(rs
									.getBigDecimal("opening_balance"));
					acHVO
							.setClosingBalance(rs
									.getBigDecimal("closing_balance"));

					acHVO.setRootID(rs.getInt("root_id"));
					acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
					acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
					acHVO.setAccountGroupID(rs.getInt("group_id"));
					acHVO.setLedgerName(rs.getString("ledger_name"));
					//acHVO.setCategoryID(rs.getInt("group_id"));
					acHVO.setCategoryName(rs.getString("group_name"));
					acHVO.setLedgerID(rs.getInt("ledger_id"));
					if (rs.getString("normal_balance") != null) {
						acHVO.setNormalBalance(rs.getString("normal_balance"));
						normalBalance = rs.getString("normal_balance");
						rootID = rs.getInt("root_id");
						groupName = rs.getString("group_name");
						groupID = rs.getInt("group_id");

					}
					if ((rs.getBigDecimal("opening_balance") == null)
							&& (rs.getBigDecimal("closing_balance") == null)) {

						acHVO.setClosingBalance(rs
								.getBigDecimal("openingBalWhenNoTx"));
						acHVO.setOpeningBalance(rs
								.getBigDecimal("openingBalWhenNoTx"));
						acHVO.setNormalBalance(normalBalance);
						acHVO.setRootID(rootID);
						acHVO.setAccountGroupID(groupID);
						acHVO.setStrAccountPrimaryHead(groupName);
					} else if (rs.getBigDecimal("opening_balance") == null) {
						acHVO.setNormalBalance(normalBalance);
						acHVO.setRootID(rootID);
						acHVO.setAccountGroupID(groupID);
						acHVO.setStrAccountPrimaryHead(groupName);
						acHVO.setOpeningBalance(rs
								.getBigDecimal("openingBalWhenNoTx"));
					}

					return acHVO;
				}

			};
			ledgerList = namedParameterJdbcTemplate.query(query, hMap, Rmapper);

			logger
					.debug("Exit : public List getLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String effectiveDate)"
							+ ledgerList.size());
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getLedgerReport : " + ex);
		} catch (Exception e) {
			logger.error("Exception : " + e);
		}

		return ledgerList;
	}
	
	private String createTrialBalanceQuery(int societyID, String effectiveDate,
			String toDate,int isConsolidated,String rootType) {
		String orgClause="";
		String orgCondition="";
		String rootCondition="";
		if(rootType.equalsIgnoreCase("AL")){//Asset liability
			rootCondition=" account_groups.root_id = 3 or account_groups.root_id= 4 ";
		}else{
			rootCondition=" account_groups.root_id = 1 or account_groups.root_id= 2 ";
		}
		
		if(isConsolidated==0){
			orgClause="and al.org_id=:societyID";
			orgCondition="and alle.org_id="+societyID+"";
		}
		SocietyVO societyVO=societyService.getSocietyDetails(societyID);
		String query = "SELECT  account_category.category_id,account_category.category_name,account_groups.group_name AS report_group_name,account_groups.id,seq_no,ars.normal_balance,ars.root_id, ars.root_group_name AS root_name,  "
				+ " abs(SUM(report_balance.op_positive)) AS op_pos,abs(SUM(report_balance.op_negative)) AS op_neg, "
				+ " abs(SUM(report_balance.closing_positive)) AS cl_pos,abs(SUM(report_balance.closing_negative)) AS cl_neg FROM "
				+ " (SELECT alle.sub_group_id,transaction_sum.group_name,alle.ledger_name,SUM(CASE WHEN alle.opening_balance<0 THEN alle.opening_balance ELSE 0 END) AS op_negative, "
				+ " SUM(CASE WHEN alle.opening_balance>0 THEN alle.opening_balance ELSE 0 END) AS op_positive, "
				+ " SUM(alle.opening_balance) AS opening_balance_sum,IFNULL(SUM(transaction_sum.tx_sum),0) AS trans_sum, "
				+ " (CASE WHEN (SUM(alle.opening_balance)+ IFNULL(SUM(transaction_sum.tx_sum),0))<0 THEN (SUM( alle.opening_balance)+ IFNULL(SUM(transaction_sum.tx_sum),0)) ELSE 0 END) AS closing_negative,"
				+ " (CASE WHEN (SUM( alle.opening_balance)+ IFNULL(SUM(transaction_sum.tx_sum),0))>0 THEN (SUM( alle.opening_balance)+ IFNULL(SUM(transaction_sum.tx_sum),0)) ELSE 0 END) AS closing_positive "
				+ " FROM account_ledgers_"
				+  societyVO.getDataZoneID()
				+ " alle  LEFT JOIN "
				+ "(SELECT account_groups.id,account_groups.group_name, al.id AS tx_ledger_id,al.ledger_name ,"
				+ "SUM(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END) AS tx_negative, "
				+ "SUM(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END) AS tx_positive, "
				+ "(SUM( ale.amount)) AS tx_sum "
				+ "FROM account_transactions_"
				+  societyVO.getDataZoneID()
				+ " ats,account_ledger_entry_"
				+  societyVO.getDataZoneID()
				+ " ale,account_ledgers_"
				+  societyVO.getDataZoneID()
				+ " al,account_groups WHERE "
				+ "ats.tx_date BETWEEN '"
				+ effectiveDate
				+ "' AND '"
				+ toDate
				+ "' AND ats.is_deleted = 0"
				+ " AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id "
				+" AND ats.org_id=ale.org_id and ale.org_id=al.org_id  "+orgClause
				+ " AND al.sub_group_id = account_groups.id GROUP BY ale.ledger_id)AS transaction_sum "
				+ "ON alle.id = transaction_sum.tx_ledger_id  WHERE alle.is_deleted=0 "+orgCondition+" GROUP BY alle.id) AS report_balance , account_groups,"
				+ "account_root_group AS ars ,account_category WHERE account_groups.id = report_balance.sub_group_id AND account_groups.category_id=account_category.category_id AND ( "+rootCondition+" ) AND account_groups.root_id=ars.root_id GROUP BY account_groups.id ORDER BY seq_no";

		return query;

	}
	
	public List getTrialBalanceLedgerReport(final int societyID, final String fromDate,
			String uptoDate, String effectiveDate) {

		List<AccountHeadVO> ledgerList = new ArrayList<AccountHeadVO>();

		try {

			logger
					.debug("Entry : public List getTrialBalanceLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String effectiveDate)");
			SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			String query = " SELECT a.id AS ledger_id,a.org_id,txTable.closing_balance,txTable.opening_balance,a.opening_balance AS openingBalWhenNoTx,ag.group_name,a.sub_group_id AS group_id,a.ledger_name AS ledger_name,ag.root_id,ar.normal_balance,ar.root_group_name  FROM account_groups ag,account_root_group ar,account_ledgers_"
					+ societyVO.getDataZoneID()
					+ " a LEFT JOIN "
					+ " (SELECT closingBal.ledger_id AS ledger_id, closingBal.closing_balance AS closing_balance,openingBal.opening_balance,group_name,root_id,normal_balance,closingBal.org_id  FROM(SELECT ale.org_id,ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS closing_balance,al.is_deleted ,r.root_id,r.normal_balance,g.group_name "
					+ "  FROM account_ledgers_"
					+ societyVO.getDataZoneID()
					+ " al, account_transactions_"
					+ societyVO.getDataZoneID()
					+ " ats,account_ledger_entry_"
					+  societyVO.getDataZoneID()
					+ " ale,account_groups g,account_root_group r WHERE ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID and ats.tx_id=ale.tx_id AND al.sub_group_id=g.id AND g.root_id=r.root_id AND ats.tx_date <= :toDate AND ats.is_deleted=0 "
					+ " AND ale.ledger_id = al.id GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS closingBal LEFT JOIN (SELECT ale.org_id, ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS opening_balance,al.is_deleted  "
					+ " FROM account_ledgers_"
					+ societyVO.getDataZoneID()
					+ " al, account_transactions_"
					+ societyVO.getDataZoneID()
					+ " ats,account_ledger_entry_"
					+  societyVO.getDataZoneID()
					+ " ale WHERE  ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID and ats.tx_id=ale.tx_id  AND ats.tx_date <:fromDate AND ats.is_deleted=0 AND ale.ledger_id = al.id  GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS openingBal ON closingBal.ledger_id=openingBal.ledger_id ) AS txTable ON a.id=txTable.ledger_id WHERE  a.org_id=:societyID   and a.is_deleted=0 AND a.sub_group_id=ag.id AND ag.root_id=ar.root_id AND (ar.root_id!='5') ORDER BY seq_no; ";
			logger.debug(query);
			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", uptoDate);
			
			RowMapper Rmapper = new RowMapper() {
				String normalBalance = "";

				int rootID = 0;

				String groupName = "";

				int groupID = 0;

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					AccountHeadVO acHVO = new AccountHeadVO();

					acHVO
							.setOpeningBalance(rs
									.getBigDecimal("opening_balance"));
					acHVO
							.setClosingBalance(rs
									.getBigDecimal("closing_balance"));

					acHVO.setRootID(rs.getInt("root_id"));
					acHVO.setRootGroupName(rs.getString("root_group_name"));
					acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
					acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
					acHVO.setAccountGroupID(rs.getInt("group_id"));
					//acHVO.setCategoryID(rs.getInt("group_id"));
					acHVO.setCategoryName(rs.getString("group_name"));
					acHVO.setLedgerID(rs.getInt("ledger_id"));
					if (rs.getString("normal_balance") != null) {
						acHVO.setNormalBalance(rs.getString("normal_balance"));
						normalBalance = rs.getString("normal_balance");
						rootID = rs.getInt("root_id");
						groupName = rs.getString("group_name");
						groupID = rs.getInt("group_id");

					}
					if ((rs.getBigDecimal("opening_balance") == null)
							&& (rs.getBigDecimal("closing_balance") == null)) {

						acHVO.setClosingBalance(rs
								.getBigDecimal("openingBalWhenNoTx"));
						acHVO.setOpeningBalance(rs
								.getBigDecimal("openingBalWhenNoTx"));
						acHVO.setNormalBalance(normalBalance);
						acHVO.setRootID(rootID);
						acHVO.setAccountGroupID(groupID);
						acHVO.setStrAccountPrimaryHead(groupName);
					} else if (rs.getBigDecimal("opening_balance") == null) {
						acHVO.setNormalBalance(normalBalance);
						acHVO.setRootID(rootID);
						acHVO.setAccountGroupID(groupID);
						acHVO.setStrAccountPrimaryHead(groupName);
						acHVO.setOpeningBalance(rs
								.getBigDecimal("openingBalWhenNoTx"));
					}

					return acHVO;
				}

			};
			ledgerList = namedParameterJdbcTemplate.query(query, hMap, Rmapper);

			logger
					.debug("Exit : public List getTrialBalanceLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String effectiveDate)"
							+ ledgerList.size());
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getTrialBalanceLedgerReport : " + ex);
		} catch (Exception e) {
			logger.error("Exception : " + e);
		}

		return ledgerList;
	}

	
	public List getNormalBalanceSheet(int societyID, String fromDate,
			String uptoDate, String effectiveDate,String formatType) {
		List balanceSheetList = new ArrayList();
		logger
				.debug("Entry : public ReportVO getNormalBalanceSheet(int societyID,String fromDate,String uptoDate,String formatType)"
						+ fromDate + "  " + uptoDate);

		try {

			String str = "SELECT balance_sheet_current.id, balance_sheet_current.category_id,balance_sheet_current.root_id, balance_sheet_current.report_group_name, balance_sheet_current.closing_balance AS current_balance, balance_sheet_previous.closing_balance AS previous_balance , balance_sheet_previous.seq_no "
					+ "FROM ("
					+ createNormalBalanceSheetQuery(societyID, effectiveDate,
							fromDate,formatType)
					+ ") AS balance_sheet_previous, ("
					+ createNormalBalanceSheetQuery(societyID, effectiveDate,
							uptoDate,formatType)
					+ ") AS balance_sheet_current"
					+ " WHERE balance_sheet_current.report_group_name = balance_sheet_previous.report_group_name order by balance_sheet_previous.seq_no";

			logger.debug(str);
			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("effectiveDate", effectiveDate);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", uptoDate);

			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					AccountHeadVO acHVO = new AccountHeadVO();
					acHVO.setAccountGroupID(rs.getInt("id"));
					acHVO.setCategoryID(rs.getInt("category_id"));
					acHVO.setRootID(rs.getInt("root_id"));
					acHVO.setOpeningBalance(rs
							.getBigDecimal("previous_balance"));
					acHVO
							.setClosingBalance(rs
									.getBigDecimal("current_balance"));
					acHVO.setStrAccountPrimaryHead(rs.getString(
							"report_group_name").trim());
					return acHVO;
				}

			};
			balanceSheetList = namedParameterJdbcTemplate.query(str, hMap,
					Rmapper);

			logger
					.debug("Entry : public ReportVO getNormalBalanceSheet(int societyID,String fromDate,String uptoDate,String formatType)"
							+ balanceSheetList.size());
		} catch (Exception e) {
			logger
					.error("Exception occurred in public ReportVO getNormalBalanceSheet "
							+ e);
		}
		return balanceSheetList;
	}
	
	private String createNormalBalanceSheetQuery(int societyID, String effectiveDate,
			String toDate,String formatType) {
		String strCondition="";
		if(formatType.equalsIgnoreCase("N")||formatType.equalsIgnoreCase("T")){
			strCondition="account_groups.n_seq_no";
		}else if(formatType.equalsIgnoreCase("S")){
			strCondition="account_groups.seq_no";
		}
		
		SocietyVO societyVO=societyService.getSocietyDetails(societyID);
		String query = "SELECT account_groups.id,account_groups.category_id,account_groups.root_id,account_groups.group_name AS report_group_name, report_balance.opening_balance,report_balance.trans_sum, (report_balance.opening_balance + (report_balance.trans_sum))AS closing_balance,"+strCondition+" as seq_no "
				+ "FROM (SELECT al.sub_group_id,report_balances.group_name,SUM(al.opening_balance) AS  opening_balance,IFNULL(SUM(report_balances.trans_sum),0) AS trans_sum FROM(SELECT al.id, al.sub_group_id,transaction_sum.group_name,"
				+ "SUM(al.opening_balance) AS  opening_balance,IFNULL(SUM(transaction_sum.tx_sum),0) AS trans_sum "
				+ "FROM account_ledgers_"
				+ societyVO.getDataZoneID()
				+ " al LEFT JOIN(SELECT account_groups.id,account_groups.group_name,al.id AS tx_ledger_id ,al.ledger_name, IFNULL((SUM( ale.amount)),0) AS tx_sum "
				+ "FROM account_transactions_"
				+ societyVO.getDataZoneID()
				+ " ats,account_ledger_entry_"
				+ societyVO.getDataZoneID()
				+ " ale ,account_ledgers_"
				+ societyVO.getDataZoneID()
				+ " al,account_groups "
				+ "WHERE ats.tx_date BETWEEN '"
				+ effectiveDate
				+ "' AND '"
				+ toDate
				+ "' AND ats.is_deleted = 0 "
				+" AND ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID "
				+ " AND ale.tx_id = ats.tx_id   AND ale.ledger_id = al.id "
				+ "AND al.sub_group_id = account_groups.id GROUP BY ale.ledger_id ) AS transaction_sum ON al.id = transaction_sum.tx_ledger_id WHERE al.org_id=:societyID  "
				+ " GROUP BY al.id) AS report_balances,account_ledgers_"
				+ societyVO.getDataZoneID()
				+ " al  WHERE al.id=report_balances.id AND al.is_deleted=0 GROUP BY al.sub_group_id) AS report_balance"
				+ " , account_groups WHERE account_groups.id = report_balance.sub_group_id  AND (account_groups.root_id = '3' OR account_groups.root_id='4')   ORDER BY "+strCondition;

		return query;
	}

	public List getConsolidatedNormalBalanceSheet(int societyID, String fromDate,
			String uptoDate, String effectiveDate) {
		List balanceSheetList = new ArrayList();
		logger
				.debug("Entry : public ReportVO getConsolidatedNormalBalanceSheet(int societyID,String fromDate,String uptoDate)"
						+ fromDate + "  " + uptoDate);

		try {

			String str = "SELECT balance_sheet_current.id, balance_sheet_current.category_id,balance_sheet_current.root_id, balance_sheet_current.report_group_name, balance_sheet_current.closing_balance AS current_balance, balance_sheet_previous.closing_balance AS previous_balance , balance_sheet_previous.seq_no "
					+ "FROM ("
					+ createConsolidatedNormalBalanceSheetQuery(societyID, effectiveDate,
							fromDate)
					+ ") AS balance_sheet_previous, ("
					+ createConsolidatedNormalBalanceSheetQuery(societyID, effectiveDate,
							uptoDate)
					+ ") AS balance_sheet_current"
					+ " WHERE balance_sheet_current.report_group_name = balance_sheet_previous.report_group_name order by balance_sheet_previous.seq_no";

			logger.debug(str);
			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("effectiveDate", effectiveDate);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", uptoDate);

			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					AccountHeadVO acHVO = new AccountHeadVO();
					acHVO.setAccountGroupID(rs.getInt("id"));
					acHVO.setCategoryID(rs.getInt("category_id"));
					acHVO.setRootID(rs.getInt("root_id"));
					acHVO.setOpeningBalance(rs
							.getBigDecimal("previous_balance"));
					acHVO
							.setClosingBalance(rs
									.getBigDecimal("current_balance"));
					acHVO.setStrAccountPrimaryHead(rs.getString(
							"report_group_name").trim());
					return acHVO;
				}

			};
			balanceSheetList = namedParameterJdbcTemplate.query(str, hMap,
					Rmapper);

			logger
					.debug("Entry : public ReportVO getConsolidatedNormalBalanceSheet(int societyID,String fromDate,String uptoDate)"
							+ balanceSheetList.size());
		} catch (Exception e) {
			logger
					.error("Exception occurred in public ReportVO getConsolidatedNormalBalanceSheet "
							+ e);
		}
		return balanceSheetList;
	}
	
	private String createConsolidatedNormalBalanceSheetQuery(int societyID, String effectiveDate,
			String toDate) {
		SocietyVO societyVO=societyService.getSocietyDetails(societyID);
		String query = "SELECT account_groups.id,account_groups.category_id,account_groups.root_id,account_groups.group_name AS report_group_name, report_balance.opening_balance,report_balance.trans_sum, (report_balance.opening_balance + (report_balance.trans_sum))AS closing_balance,account_groups.seq_no "
				+ "FROM (SELECT al.sub_group_id,report_balances.group_name,SUM(al.opening_balance) AS  opening_balance,IFNULL(SUM(report_balances.trans_sum),0) AS trans_sum FROM(SELECT al.id, al.sub_group_id,transaction_sum.group_name,"
				+ "SUM(al.opening_balance) AS  opening_balance,IFNULL(SUM(transaction_sum.tx_sum),0) AS trans_sum "
				+ "FROM account_ledgers_"
				+ societyVO.getDataZoneID()
				+ " al LEFT JOIN(SELECT account_groups.id,account_groups.group_name,al.id AS tx_ledger_id ,al.ledger_name, IFNULL((SUM( ale.amount)),0) AS tx_sum "
				+ "FROM account_transactions_"
				+ societyVO.getDataZoneID()
				+ " ats,account_ledger_entry_"
				+ societyVO.getDataZoneID()
				+ " ale ,account_ledgers_"
				+ societyVO.getDataZoneID()
				+ " al,account_groups "
				+ "WHERE ats.tx_date BETWEEN '"
				+ effectiveDate
				+ "' AND '"
				+ toDate
				+ "' AND ats.is_deleted = 0 "
				+" AND ats.org_id=ale.org_id and ale.org_id=al.org_id "
				+ " AND ale.tx_id = ats.tx_id   AND ale.ledger_id = al.id "
				+ "AND al.sub_group_id = account_groups.id GROUP BY ale.ledger_id ) AS transaction_sum ON al.id = transaction_sum.tx_ledger_id   "
				+ " GROUP BY al.id) AS report_balances,account_ledgers_"
				+ societyVO.getDataZoneID()
				+ " al  WHERE al.id=report_balances.id AND al.is_deleted=0 GROUP BY al.sub_group_id) AS report_balance"
				+ " , account_groups WHERE account_groups.id = report_balance.sub_group_id  AND (account_groups.root_id = '3' OR account_groups.root_id='4')   ORDER BY account_groups.seq_no ";

		return query;
	}
	
	public List getTreeWiseBalanceSheet(int societyID, String fromDate,
			String uptoDate, String effectiveDate) {
		List balanceSheetList = new ArrayList();
		logger
				.debug("Entry : public ReportVO getTreeWiseBalanceSheet(int societyID,String fromDate,String uptoDate)"
						+ fromDate + "  " + uptoDate);

		try {

			String str = "SELECT balance_sheet_current.id,balance_sheet_current.category_id,balance_sheet_current.category_name,balance_sheet_current.root_id, balance_sheet_current.report_group_name, balance_sheet_current.closing_balance AS current_balance, balance_sheet_previous.closing_balance AS previous_balance , balance_sheet_previous.seq_no "
					+ "FROM ("
					+ createTreeWiseBalanceSheetQuery(societyID, effectiveDate,
							fromDate)
					+ ") AS balance_sheet_previous, ("
					+ createTreeWiseBalanceSheetQuery(societyID, effectiveDate,
							uptoDate)
					+ ") AS balance_sheet_current"
					+ " WHERE balance_sheet_current.report_group_name = balance_sheet_previous.report_group_name order by balance_sheet_previous.seq_no";

			logger.debug(str);
			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("effectiveDate", effectiveDate);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", uptoDate);

			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					AccountHeadVO acHVO = new AccountHeadVO();
					acHVO.setAccountGroupID(rs.getInt("id"));
					acHVO.setCategoryID(rs.getInt("category_id"));
					acHVO.setCategoryName(rs.getString("category_name"));
					acHVO.setRootID(rs.getInt("root_id"));
					acHVO.setOpeningBalance(rs
							.getBigDecimal("previous_balance"));
					acHVO
							.setClosingBalance(rs
									.getBigDecimal("current_balance"));
					acHVO.setStrAccountPrimaryHead(rs.getString(
							"report_group_name").trim());
					return acHVO;
				}

			};
			balanceSheetList = namedParameterJdbcTemplate.query(str, hMap,
					Rmapper);

			logger
					.debug("Entry : public ReportVO getTreeWiseBalanceSheet(int societyID,String fromDate,String uptoDate)"
							+ balanceSheetList.size());
		} catch (Exception e) {
			logger
					.error("Exception occurred in public ReportVO getTreeWiseBalanceSheet for  Org ID :"+societyID+"  "+ e);
		}
		return balanceSheetList;
	}
	
	private String createTreeWiseBalanceSheetQuery(int societyID, String effectiveDate,
			String toDate) {
		SocietyVO societyVO=societyService.getSocietyDetails(societyID);
		String query = "SELECT account_groups.id,account_groups.category_id,ac.category_name,account_groups.root_id,account_groups.group_name AS report_group_name, report_balance.opening_balance,report_balance.trans_sum, (report_balance.opening_balance + (report_balance.trans_sum))AS closing_balance,account_groups.seq_no "
				+ "FROM (SELECT al.sub_group_id,report_balances.group_name,report_balances.category_id,report_balances.category_name,SUM(al.opening_balance) AS  opening_balance,IFNULL(SUM(report_balances.trans_sum),0) AS trans_sum FROM(SELECT al.id,ac.category_id,ac.category_name, al.sub_group_id,transaction_sum.group_name,"
				+ "SUM(al.opening_balance) AS  opening_balance,IFNULL(SUM(transaction_sum.tx_sum),0) AS trans_sum "
				+ "FROM ( account_ledgers_"
				+ societyVO.getDataZoneID()
				+ " al ,account_groups ag,account_category ac) LEFT JOIN(SELECT account_groups.id,account_groups.group_name,al.id AS tx_ledger_id ,al.ledger_name, IFNULL((SUM( ale.amount)),0) AS tx_sum "
				+ "FROM account_transactions_"
				+ societyVO.getDataZoneID()
				+ " ats,account_ledger_entry_"
				+ societyVO.getDataZoneID()
				+ " ale ,account_ledgers_"
				+ societyVO.getDataZoneID()
				+ " al,account_groups "
				+ "WHERE ats.tx_date BETWEEN '"
				+ effectiveDate
				+ "' AND '"
				+ toDate
				+ "' AND ats.is_deleted = 0 "
				+" AND ats.org_id=ale.org_id and ale.org_id=al.org_id  and al.org_id=:societyID"
				+ " AND ale.tx_id = ats.tx_id   AND ale.ledger_id = al.id "
				+ "AND al.sub_group_id = account_groups.id GROUP BY ale.ledger_id ) AS transaction_sum ON al.id = transaction_sum.tx_ledger_id WHERE al.org_id=:societyID  AND ac.category_id=ag.category_id AND al.sub_group_id=ag.id "
				+ " GROUP BY al.id) AS report_balances,account_ledgers_"
				+ societyVO.getDataZoneID()
				+ " al  WHERE al.id=report_balances.id AND al.is_deleted=0 GROUP BY report_balances.category_id) AS report_balance"
				+ " , account_groups,account_category ac WHERE account_groups.category_id = report_balance.category_id AND account_groups.category_id=ac.category_id  AND (account_groups.root_id = '3' OR account_groups.root_id='4') GROUP BY ac.category_id  ORDER BY account_groups.seq_no ";

		return query;
	}

	
	public List getConsolidatedTreeWiseBalanceSheet(int societyID, String fromDate,
			String uptoDate, String effectiveDate) {
		List balanceSheetList = new ArrayList();
		logger
				.debug("Entry : public ReportVO getConsolidatedTreeWiseBalanceSheet(int societyID,String fromDate,String uptoDate)"
						+ fromDate + "  " + uptoDate);

		try {

			String str = "SELECT balance_sheet_current.id,balance_sheet_current.category_id,balance_sheet_current.category_name,balance_sheet_current.root_id, balance_sheet_current.report_group_name, balance_sheet_current.closing_balance AS current_balance, balance_sheet_previous.closing_balance AS previous_balance , balance_sheet_previous.seq_no "
					+ "FROM ("
					+ createConsolidatedTreeWiseBalanceSheetQuery(societyID, effectiveDate,
							fromDate)
					+ ") AS balance_sheet_previous, ("
					+ createConsolidatedTreeWiseBalanceSheetQuery(societyID, effectiveDate,
							uptoDate)
					+ ") AS balance_sheet_current"
					+ " WHERE balance_sheet_current.report_group_name = balance_sheet_previous.report_group_name order by balance_sheet_previous.seq_no";

			logger.debug(str);
			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("effectiveDate", effectiveDate);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", uptoDate);

			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					AccountHeadVO acHVO = new AccountHeadVO();
					acHVO.setAccountGroupID(rs.getInt("id"));
					acHVO.setCategoryID(rs.getInt("category_id"));
					acHVO.setCategoryName(rs.getString("category_name"));
					acHVO.setRootID(rs.getInt("root_id"));
					acHVO.setOpeningBalance(rs
							.getBigDecimal("previous_balance"));
					acHVO
							.setClosingBalance(rs
									.getBigDecimal("current_balance"));
					acHVO.setStrAccountPrimaryHead(rs.getString(
							"report_group_name").trim());
					return acHVO;
				}

			};
			balanceSheetList = namedParameterJdbcTemplate.query(str, hMap,
					Rmapper);

			logger
					.debug("Entry : public ReportVO getConsolidatedTreeWiseBalanceSheet(int societyID,String fromDate,String uptoDate)"
							+ balanceSheetList.size());
		} catch (Exception e) {
			logger
					.error("Exception occurred in public ReportVO getConsolidatedTreeWiseBalanceSheet for  Org ID :"+societyID+"  "+ e);
		}
		return balanceSheetList;
	}
	
	private String createConsolidatedTreeWiseBalanceSheetQuery(int societyID, String effectiveDate,
			String toDate) {
		SocietyVO societyVO=societyService.getSocietyDetails(societyID);
		String query = "SELECT account_groups.id,account_groups.category_id,ac.category_name,account_groups.root_id,account_groups.group_name AS report_group_name, report_balance.opening_balance,report_balance.trans_sum, (report_balance.opening_balance + (report_balance.trans_sum))AS closing_balance,account_groups.seq_no "
				+ "FROM (SELECT al.sub_group_id,report_balances.group_name,report_balances.category_id,report_balances.category_name,SUM(al.opening_balance) AS  opening_balance,IFNULL(SUM(report_balances.trans_sum),0) AS trans_sum FROM(SELECT al.id,ac.category_id,ac.category_name, al.sub_group_id,transaction_sum.group_name,"
				+ "SUM(al.opening_balance) AS  opening_balance,IFNULL(SUM(transaction_sum.tx_sum),0) AS trans_sum "
				+ "FROM ( account_ledgers_"
				+ societyVO.getDataZoneID()
				+ " al ,account_groups ag,account_category ac) LEFT JOIN(SELECT account_groups.id,account_groups.group_name,al.id AS tx_ledger_id ,al.ledger_name, IFNULL((SUM( ale.amount)),0) AS tx_sum "
				+ "FROM account_transactions_"
				+ societyVO.getDataZoneID()
				+ " ats,account_ledger_entry_"
				+ societyVO.getDataZoneID()
				+ " ale ,account_ledgers_"
				+ societyVO.getDataZoneID()
				+ " al,account_groups "
				+ "WHERE ats.tx_date BETWEEN '"
				+ effectiveDate
				+ "' AND '"
				+ toDate
				+ "' AND ats.is_deleted = 0 "
				+" AND ats.org_id=ale.org_id and ale.org_id=al.org_id  "
				+ " AND ale.tx_id = ats.tx_id   AND ale.ledger_id = al.id "
				+ "AND al.sub_group_id = account_groups.id GROUP BY ale.ledger_id ) AS transaction_sum ON al.id = transaction_sum.tx_ledger_id WHERE ac.category_id=ag.category_id AND al.sub_group_id=ag.id "
				+ " GROUP BY al.id) AS report_balances,account_ledgers_"
				+ societyVO.getDataZoneID()
				+ " al  WHERE al.id=report_balances.id AND al.is_deleted=0 GROUP BY report_balances.category_id) AS report_balance"
				+ " , account_groups,account_category ac WHERE account_groups.category_id = report_balance.category_id AND account_groups.category_id=ac.category_id  AND (account_groups.root_id = '3' OR account_groups.root_id='4') GROUP BY ac.category_id  ORDER BY account_groups.seq_no ";

		return query;
	}
	
	public List getBalanceSheetLedgerDetails(int societyID, String fromDate,
			String uptoDate, String effectiveDate) {
		List balanceSheetList = new ArrayList();
		logger
				.debug("Entry : public ReportVO getBalanceSheetLedgerDetails(int societyID,String fromDate,String uptoDate)"
						+ fromDate + "  " + uptoDate);

		try {

			String str = "SELECT balance_sheet_current.orgID, balance_sheet_current.id,balance_sheet_current.root_id, balance_sheet_current.report_group_name, balance_sheet_current.closing_balance AS current_balance, balance_sheet_previous.closing_balance AS previous_balance , balance_sheet_previous.seq_no,balance_sheet_current.report_ledger_name,balance_sheet_current.report_ledgerID "
					+ "FROM ("
					+ createBalanceSheetLedgersQuery(societyID, effectiveDate,
							fromDate)
					+ ") AS balance_sheet_previous, ("
					+ createBalanceSheetLedgersQuery(societyID, effectiveDate,
							uptoDate)
					+ ") AS balance_sheet_current"
					+ " WHERE balance_sheet_current.report_ledgerID= balance_sheet_previous.report_ledgerID and balance_sheet_current.orgID=balance_sheet_previous.orgID   order by balance_sheet_previous.seq_no";

			logger.debug(str);
			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("effectiveDate", effectiveDate);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", uptoDate);

			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					AccountHeadVO acHVO = new AccountHeadVO();
					acHVO.setAccountGroupID(rs.getInt("id"));
					acHVO.setRootID(rs.getInt("root_id"));
					acHVO.setOrgID(rs.getInt("orgID"));
					acHVO.setOpeningBalance(rs
							.getBigDecimal("previous_balance"));
					acHVO
							.setClosingBalance(rs
									.getBigDecimal("current_balance"));
					acHVO.setStrAccountPrimaryHead(rs.getString(
							"report_group_name").trim());
					acHVO.setLedgerName(rs.getString("report_ledger_name"));
					acHVO.setLedgerID(rs.getInt("report_ledgerID"));
					return acHVO;
				}

			};
			balanceSheetList = namedParameterJdbcTemplate.query(str, hMap,
					Rmapper);

			logger
					.debug("Entry : public ReportVO getBalanceSheetLedgerDetails(int societyID,String fromDate,String uptoDate)"
							+ balanceSheetList.size());
		} catch (Exception e) {
			logger
					.error("Exception occurred in public ReportVO getBalanceSheetLedgerDetails "
							+ e);
		}
		return balanceSheetList;
	}
	
	private String createBalanceSheetLedgersQuery(int societyID, String effectiveDate,
			String toDate) {
		SocietyVO societyVO=societyService.getSocietyDetails(societyID);
		String query = "SELECT ledgerID AS report_ledgerID,orgID,ledgerName AS report_ledger_name,account_groups.id,account_groups.root_id,account_groups.group_name AS report_group_name, report_balance.opening_balance,report_balance.trans_sum, (report_balance.opening_balance + (report_balance.trans_sum))AS closing_balance,account_groups.seq_no "
				+ "FROM (SELECT al.id AS ledgerID, report_balances.orgID, al.ledger_name  AS ledgerName,al.sub_group_id,report_balances.group_name,SUM(al.opening_balance) AS  opening_balance,IFNULL(SUM(report_balances.trans_sum),0) AS trans_sum FROM(SELECT al.id, al.org_id AS orgID,al.sub_group_id,transaction_sum.group_name,"
				+ "SUM(al.opening_balance) AS  opening_balance,IFNULL(SUM(transaction_sum.tx_sum),0) AS trans_sum "
				+ "FROM account_ledgers_"
				+ societyVO.getDataZoneID()
				+ " al LEFT JOIN(SELECT account_groups.id,account_groups.group_name,al.id AS tx_ledger_id ,al.id AS ledgerID, al.ledger_name  AS ledgerName, IFNULL((SUM( ale.amount)),0) AS tx_sum "
				+ "FROM account_transactions_"
				+ societyVO.getDataZoneID()
				+ " ats,account_ledger_entry_"
				+ societyVO.getDataZoneID()
				+ " ale ,account_ledgers_"
				+ societyVO.getDataZoneID()
				+ " al,account_groups "
				+ "WHERE ats.tx_date BETWEEN '"
				+ effectiveDate
				+ "' AND '"
				+ toDate
				+ "' AND ats.is_deleted = 0 "
				+" AND ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID "
				+ " AND ale.tx_id = ats.tx_id   AND ale.ledger_id = al.id "
				+ "AND al.sub_group_id = account_groups.id GROUP BY ale.ledger_id ) AS transaction_sum ON al.id = transaction_sum.tx_ledger_id  "
				+ " where al.org_id=:societyID   GROUP BY al.id) AS report_balances,account_ledgers_"
				+ societyVO.getDataZoneID()
				+ " al  WHERE al.id=report_balances.id AND report_balances.orgID=al.org_id AND al.is_deleted=0 GROUP BY al.id) AS report_balance"
				+ " , account_groups WHERE account_groups.id = report_balance.sub_group_id  AND (account_groups.root_id = '3' OR account_groups.root_id='4')   ORDER BY account_groups.seq_no ";

		return query;
	}
	
	
	public List getIndASReportStructure(int orgID, int rptID,String type,int rootID) {
		List balanceSheetList = new ArrayList();
		String strCondition="";
		logger.debug("Entry : public List getIndASReportStructure(int orgID, int rptID,String type)");
		if(rootID!=0){
			strCondition=" and r.root_id=:rootID ";
		}

		try {

			String str = " SELECT r.* FROM report_groups_details r,report_details rd WHERE r.report_id=rd.report_id AND r.report_id=:reportID AND r.reporting_group_type=:type AND (rd.org_id=0 OR rd.org_id=:orgID) and r.is_deleted=0 "+strCondition+" ORDER BY r.id,r.parent_id ;";
			Map hMap = new HashMap();
			hMap.put("orgID", orgID);
			hMap.put("reportID", rptID);
			hMap.put("type", type);
			hMap.put("rootID",rootID);
			

			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					ReportDetailsVO rptVO = new ReportDetailsVO();
					rptVO.setId(rs.getInt("id"));
					rptVO.setCategoryName(rs.getString("reporting_group_name"));
					rptVO.setRptGrpType(rs.getString("reporting_group_type"));
					rptVO.setParentID(rs.getInt("parent_id"));
					rptVO.setRootID(rs.getInt("root_id"));
					return rptVO;
				}

			};
			balanceSheetList = namedParameterJdbcTemplate.query(str, hMap,
					Rmapper);

			logger
					.debug("Entry : public List getIndASReportStructure(int orgID, int rptID,String type)"
							+ balanceSheetList.size());
		} catch (Exception e) {
			logger
					.error("Exception occurred in public List getIndASReportStructure(int orgID, int rptID,String type) for  Org ID :"+orgID+"  "+ e);
		}
		return balanceSheetList;
	}
	
	
	public List getIndASReportStructureDetails(int rptID) {
		List balanceSheetList = new ArrayList();
		logger.debug("Entry : public List getIndASReportStructureDetails(int rptID)");

		try {

			String str = " SELECT ld.* FROM report_groups_details r,report_groups_linking_details ld WHERE r.id=ld.reporting_group_id AND r.id=:reportID AND   r.is_deleted=0 ORDER BY r.id,r.parent_id ;";
				

			logger.debug(str);
			Map hMap = new HashMap();
			hMap.put("reportID", rptID);
			
			

			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					ReportDetailsVO rptVO = new ReportDetailsVO();
					rptVO.setId(rs.getInt("reporting_group_id"));
					rptVO.setCategoryID(rs.getInt("acc_category_id"));
					rptVO.setRptGrpType(rs.getString("type"));
					return rptVO;
				}

			};
			balanceSheetList = namedParameterJdbcTemplate.query(str, hMap,
					Rmapper);

			logger
					.debug("Entry : public List getIndASReportStructureDetails(int rptID)"
							+ balanceSheetList.size());
		} catch (Exception e) {
			logger
					.error("Exception occurred in public List getIndASReportStructureDetails(int rptID)  "+ e);
		}
		return balanceSheetList;
	}
	

	public List getRenterHistoryDetails(int aptID, Date startDate, Date endDate) {
		List renterList = null;
		RptTransactionVO rptTransactionVO = new RptTransactionVO();
		try {
			logger
					.debug("Entry : public List getRenterHistoryDetails(int aptID)"
							+ aptID + "and " + startDate + "  to" + endDate);
			String sqlDue = "SELECT * FROM renter_details WHERE is_deleted=0 AND apt_id="
					+ aptID + " AND is_current_renter!=0";
			//String sqlDue = "SELECT * FROM renter_details WHERE is_deleted=0 AND apt_id="+aptID+" AND ( ((agg_start_date <=:startDate ) OR (agg_end_date >= :endDate)))" ;
			//	" SELECT * FROM renter_details WHERE is_deleted=0 AND apt_id="+aptID+" AND agg_start_date > :startDate AND agg_end_date < :endDate ";

			Map hmap = new HashMap();
			hmap.put("startDate", startDate);
			hmap.put("endDate", endDate);

			RowMapper rMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int num) throws SQLException {
					// TODO Auto-generated method stub
					RptTransactionVO reportVO = new RptTransactionVO();
					reportVO.setAddress(rs.getString("renter_full_name"));
					reportVO.setTx_to_date(rs.getDate("agg_start_date"));
					reportVO.setIs_rented(rs.getInt("is_current_renter"));
					reportVO.setCurrent_date(rs.getDate("agg_end_date"));

					return reportVO;
				}
			};
			renterList = namedParameterJdbcTemplate
					.query(sqlDue, hmap, rMapper);

			logger
					.debug("Exit :public List getRenterHistoryDetails(int aptID)");
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in Renter History : " + ex);
		} catch (EmptyResultDataAccessException ex) {
			logger.debug("No data available for this apartment : ");
		} catch (Exception e) {
			logger.error("Exception : " + e);
		}
		return renterList;
	}

	public List tenantDetailsRpt(int intSocietyID) {
		String sql = null;
		RowMapper rMapper = null;
		SqlParameterSource namedParameters = null;
		try {
			logger
					.debug("Entry : public List tenantDetailsRpt(int intSocietyID) ");

			/*sql = "SELECT title,renter_first_name,renter_last_name,renter_full_name, b.building_name, a.apt_id,a.apt_name,mobile,agg_start_date,agg_end_date,is_document_received,is_nri,is_verified "
			 + " FROM renter_details, apartment_details a, building_details b "
			 + " WHERE  a.apt_id=renter_details.apt_id AND b.building_id=a.building_id "
			 + " AND b.society_id="
			 + intSocietyID
			 + " AND is_current_renter=1 and a.is_Rented=1 GROUP BY apt_id";*/

			sql = "SELECT a.*,building_name, m.* FROM (apartment_details  a,building_details b) LEFT  JOIN renter_details m ON "
					+ "a.apt_id=m.apt_id AND is_current_renter=1 WHERE  a.building_id=b.building_id  "
					+ "AND b.society_id=:intSocietyID  AND a.is_Rented=1 ORDER BY a.seq_no;";

			namedParameters = new MapSqlParameterSource("intSocietyID",
					intSocietyID);

			rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int arg1)
						throws SQLException {
					RptTenantVO rptTenantVO = new RptTenantVO();
					rptTenantVO.setTitle(rs.getString("title"));
					rptTenantVO.setAptID(rs.getInt("a.apt_id"));
					rptTenantVO.setRenterID(rs.getInt("m.renter_id"));
					rptTenantVO.setApt_name(rs.getString("b.building_name")
							+ " - " + rs.getString("a.apt_name"));
					rptTenantVO.setMobile(rs.getString("mobile"));
					rptTenantVO.setName(rs.getString("renter_full_name"));
					rptTenantVO.setAgg_start_date(rs
							.getString("agg_start_date"));
					rptTenantVO.setAgg_end_date(rs.getString("agg_end_date"));
					rptTenantVO.setBuilding_name(rs
							.getString("b.building_name"));
					rptTenantVO.setIsDocsReceved(rs
							.getString("is_document_received"));
					rptTenantVO.setIsNRI(rs.getString("is_nri"));
					rptTenantVO.setIsVerified(rs.getString("is_verified"));
					if (rs.getString("renter_full_name") == null) {
						rptTenantVO.setTitle("");
						rptTenantVO.setName("INFO MISSING");
						rptTenantVO.setIsNRI("0");
						rptTenantVO.setIsDocsReceved("0");
						rptTenantVO.setIsVerified("0");
					}
					return rptTenantVO;
				}

			};
			reportList = namedParameterJdbcTemplate.query(sql, namedParameters,
					rMapper);

		} catch (EmptyResultDataAccessException e) {
			logger.info("No tenant details available for this society");
		} catch (IndexOutOfBoundsException ex) {
			logger.info("No data available for this society : " + ex);
		}

		catch (Exception ex) {
			logger.error("Exception in tenantDetailsRpt : " + ex);
		}

		logger.debug("Exit : public List tenantDetailsRpt(int intSocietyID) "
				+ reportList.size());
		return reportList;
	}

	public List countTenants(int societyId) {

		List tenantList = null;

		try {
			logger.debug("Entry : public int countTenants(int societyId)"
					+ societyId);

			/*String strSQL = "SELECT * FROM renter_details r,member_details m,apartment_details a,building_details b WHERE a.apt_id=m.apt_id AND m.apt_id=r.apt_id "+
			 "AND a.building_id=b.building_id AND b.society_id=:societyId AND r.is_current_renter=1 AND r.is_deleted=0  AND m.type='P' group by r.apt_id; "; */
			String strSQL = " SELECT * FROM apartment_details a,building_details b WHERE a.building_id=b.building_id and a.is_Rented=1 AND b.society_id=:societyId;";
			//1. SQL Query
			logger.debug("query : " + strSQL);

			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyId", societyId);

			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					DashBoardTenantVO dbVO = new DashBoardTenantVO();

					return dbVO;
				}
			};

			tenantList = namedParameterJdbcTemplate.query(strSQL,
					namedParameters, RMapper);

			logger.debug("Exit:	public int countTenants(int societyId)"
					+ tenantList.size());
		} catch (NullPointerException ex) {
			logger.info("No details found : " + ex);
		} catch (BadSqlGrammarException ex) {
			logger.info("No details found : " + ex);

		} catch (Exception ex) {
			logger.error("Exception in countTenants : " + ex);

		}
		return tenantList;

	}

	
	public List getIncomeExpenditureReport(String firstDate, String lastDate,
			int societyId, int incExpGroupID, String rptType,int isConsolidated,String formatType) {
		List incExpList = new ArrayList();
		try {
			logger
					.debug("Entry:public List monthlyIncomeExpenditureReportCredit(int year, int id, int month,String formatType)"
							+ firstDate + lastDate);


          SocietyVO societyVO=societyService.getSocietyDetails(societyId);
			String strWhereClause = "";
			String orderbyClause="";
			String orgClause="";
			String orgTypeCondition="";
			
			if(societyVO.getOrgType().equalsIgnoreCase("S")){
				orgTypeCondition="ag.seq_for_soc";
			}else orgTypeCondition="ac.category_id";
			
			
			if (rptType.equalsIgnoreCase("P")) {
				strWhereClause = "ac.id";
				if(formatType.equalsIgnoreCase("N")||formatType.equalsIgnoreCase("T"))
					orderbyClause="ac.n_seq_no";
				else
				orderbyClause="ac.seq_no";
			} else if (rptType.equalsIgnoreCase("PL")) {
				strWhereClause = orgTypeCondition;
				orderbyClause=orgTypeCondition;
			}else 	if (rptType.equalsIgnoreCase("E")) {
				strWhereClause = "al.id";
				if(formatType.equalsIgnoreCase("N")||formatType.equalsIgnoreCase("T"))
					orderbyClause="ac.n_seq_no";
				else
				orderbyClause="ac.seq_no";
			} else if (rptType.equalsIgnoreCase("IE")) {
				strWhereClause = "ac.id";
				orderbyClause=orgTypeCondition;
			}
			
			if(isConsolidated==1){
				orgClause="";
			}else orgClause="and al.org_id=:societyID" ;
			
			
			String sql = "SELECT  ag.category_name,ac.category_id,ac.group_name,al.ledger_name AS ledgerName,ale.*,SUM(ale.amount) AS txAmount ,ac.id AS groupID "
					+ " FROM account_transactions_"
					+ societyVO.getDataZoneID()
					+ " ats,account_ledger_entry_"
					+ societyVO.getDataZoneID()
					+ " ale,account_ledgers_"
					+ societyVO.getDataZoneID()
					+ " al,account_groups ac ,account_category ag "
					+ " WHERE ats.org_id=ale.org_id and ale.org_id=al.org_id "+orgClause+" and ats.tx_id=ale.tx_id AND ale.ledger_id=al.id AND al.sub_group_id=ac.id AND ac.root_id=:rootID AND ats.is_deleted=0 AND  ac.category_id=ag.category_id and "
					+ " ( ats.tx_date >= :firstDate AND ats.tx_date <= :lastDate) GROUP BY "
					+ strWhereClause + " order by "+orderbyClause;

			logger.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("firstDate", firstDate);
			hashMap.put("lastDate", lastDate);
			hashMap.put("rootID", incExpGroupID);
			hashMap.put("societyID",societyId);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					// TODO Auto-generated method stub
					ReportDetailsVO rptVO = new ReportDetailsVO();
					rptVO.setLedgerName(rs.getString("ledgerName"));
					rptVO.setCategoryName(rs.getString("category_name"));
					rptVO.setGroupName(rs.getString("group_name"));
					rptVO.setTxType(rs.getString("type"));
					rptVO.setTotalAmount(rs.getBigDecimal("txAmount"));
					rptVO.setCategoryID(rs.getInt("category_id"));
					rptVO.setGroupID(rs.getInt("groupID"));
					rptVO.setLedgerID(rs.getInt("ledger_id"));
					String groupName = rs.getString("group_name");
				/*	if (!tempGroupName.equalsIgnoreCase(groupName)) {
						rptVO.setCategoryName(groupName);
						tempGroupName = groupName;
					} else {
						rptVO.setCategoryName(" ");
					}*/
					//rptVO.setCategory(rs.getString("category"));

					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate
					.query(sql, hashMap, rMapper);
			//	logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getMonthyIncExpCr: " + ex);
		} catch (Exception ex) {
			logger.error("Exception in monthlyIncomeExpenditureReportCredit : "
					+ ex);
		}
		logger
				.debug("Exit:public List monthlyIncomeExpenditureReport(int year, int id, int month,String formatType)"
						+ incExpList.size());
		return incExpList;

	}

	public List getProfitLossIndASReport(String firstDate, String lastDate,
			int societyId, int incExpGroupID, String rptType,int isConsolidated,String formatType,String prevFromDate,String prevToDate) {
		List incExpList = new ArrayList();
		try {
			logger
					.debug("Entry:public List getProfitLossIndASReport(int year, int id, int month,String formatType)"
							+ firstDate + lastDate);


          SocietyVO societyVO=societyService.getSocietyDetails(societyId);
			String strWhereClause = "";
			String orderbyClause="";
			String orgClause="";
			String onCondition="";
			String strRootCondition="";
			String ledgerGroupBy="";
			String categoryGroupBy="";
			
			if(incExpGroupID!=0){
				strRootCondition=" AND ac.root_id=:rootID ";
			}
			
			
			
			if (rptType.equalsIgnoreCase("P")) {
				strWhereClause = "ac.category_id";
				onCondition=" current.category_id=previous.category_id ";
				ledgerGroupBy=" g.category_id ";
				categoryGroupBy=" c.category_id=t.category_id ";
				if(formatType.equalsIgnoreCase("N")||formatType.equalsIgnoreCase("T"))
					orderbyClause="ac.n_seq_no";
				else
				orderbyClause="ac.seq_no";
			} else if (rptType.equalsIgnoreCase("PL")) {
				strWhereClause =" ac.id ";
				orderbyClause=" ac.seq_no ";
				onCondition=" current.groupID=previous.groupID ";
				ledgerGroupBy=" g.id ";
				categoryGroupBy=" c.group_id=t.groupID ";
			} else if(rptType.equalsIgnoreCase("L")){
				strWhereClause =" al.id ";
				orderbyClause=" ac.seq_no ";
				onCondition=" current.ledger_id=previous.ledger_id ";
				ledgerGroupBy=" l.id ";
				categoryGroupBy=" c.ledger_id=t.ledger_id ";
			}
			
			if(isConsolidated==1){
				orgClause="";
			}else orgClause="and al.org_id=:societyID" ;
			
			
			String sql = "SELECT current.type,current.category_name,current.category_id,current.group_name,current.ledgerName,current.org_id,current.ledger_id,current.groupID,IFNULL( curAmt,0) AS curAmt,IFNULL( prevAmt,0) AS prevAmt FROM ( SELECT c.group_id AS groupID,c.group_name,c.category_id,c.category_name,c.ledger_name as ledgerName,t.type,t.curAmt,c.org_id,c.ledger_id FROM(SELECT  ag.category_name,ac.category_id,ac.group_name,al.ledger_name AS ledgerName,ale.*,SUM(ale.amount) AS curAmt ,ac.id AS groupID "
					+ " FROM account_transactions_"
					+ societyVO.getDataZoneID()
					+ " ats,account_ledger_entry_"
					+ societyVO.getDataZoneID()
					+ " ale,account_ledgers_"
					+ societyVO.getDataZoneID()
					+ " al,account_groups ac ,account_category ag "
					+ " WHERE ats.org_id=ale.org_id and ale.org_id=al.org_id "+orgClause+" and ats.tx_id=ale.tx_id AND ale.ledger_id=al.id AND al.sub_group_id=ac.id "+strRootCondition+" AND ats.is_deleted=0 AND  ac.category_id=ag.category_id and "
					+ " ( ats.tx_date >= :firstDate AND ats.tx_date <= :lastDate) GROUP BY "
					+ strWhereClause + " order by "+orderbyClause+")  t RIGHT JOIN "
					+" (SELECT g.id AS group_id,g.group_name,c.category_id,c.category_name,l.id as ledger_id,l.ledger_name,l.org_id FROM account_groups g,account_category c,account_ledgers_"+societyVO.getDataZoneID()+" l WHERE l.sub_group_id=g.id and l.org_id=:orgID and g.category_id=c.category_id AND (g.org_id=0 OR g.org_id=:orgID ) GROUP BY "+ledgerGroupBy+" ) c ON "+categoryGroupBy+") AS current LEFT JOIN(SELECT  ag.category_name,ac.category_id,SUM(ale.amount) AS prevAmt ,ac.id AS groupID ,al.id as ledger_id "
					+" FROM account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups ac ,account_category ag " 
					+" WHERE ats.org_id=ale.org_id AND ale.org_id=al.org_id "+orgClause+" AND ats.tx_id=ale.tx_id AND ale.ledger_id=al.id AND al.sub_group_id=ac.id "+strRootCondition+" AND ats.is_deleted=0 AND  ac.category_id=ag.category_id AND "
					+" ( ats.tx_date >= :prevFromDate AND ats.tx_date <= :prevToDate) GROUP BY "+strWhereClause+" ORDER BY "+orderbyClause+") AS previous ON "+onCondition+";" ;

			logger.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("firstDate", firstDate);
			hashMap.put("lastDate", lastDate);
			hashMap.put("rootID", incExpGroupID);
			hashMap.put("societyID",societyId);
			hashMap.put("prevFromDate", prevFromDate);
			hashMap.put("prevToDate", prevToDate);
			hashMap.put("orgID", societyId);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					// TODO Auto-generated method stub
					ReportDetailsVO rptVO = new ReportDetailsVO();
					rptVO.setLedgerName(rs.getString("ledgerName"));
					rptVO.setCategoryName(rs.getString("category_name"));
					rptVO.setGroupName(rs.getString("group_name"));
					rptVO.setTxType(rs.getString("type"));
					rptVO.setTotalAmount(rs.getBigDecimal("curAmt"));
					rptVO.setPrevTotalAmount(rs.getBigDecimal("prevAmt"));
					rptVO.setCategoryID(rs.getInt("category_id"));
					rptVO.setGroupID(rs.getInt("groupID"));
					rptVO.setLedgerID(rs.getInt("ledger_id"));
					String groupName = rs.getString("group_name");
				/*	if (!tempGroupName.equalsIgnoreCase(groupName)) {
						rptVO.setCategoryName(groupName);
						tempGroupName = groupName;
					} else {
						rptVO.setCategoryName(" ");
					}*/
					//rptVO.setCategory(rs.getString("category"));

					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate
					.query(sql, hashMap, rMapper);
			//	logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getMonthyIncExpCr: " + ex);
		} catch (Exception ex) {
			logger.error("Exception in monthlyIncomeExpenditureReportCredit : "
					+ ex);
		}
		logger
				.debug("Exit:public List monthlyIncomeExpenditureReport(int year, int id, int month,String formatType)"
						+ incExpList.size());
		return incExpList;

	}
	
	public List monthwiseExpenseReport(int year, int intSocietyID) {
		try {
			logger
					.debug("Entry:inside public List monthwiseExpenseReport(int year,int intSocietyID) and ReportDAO"
							+ year + intSocietyID);
			String firstDate = year + "-" + 04 + "-" + 01;
			String lastDate = (year + 1) + "-" + 03 + "-" + 31;
			logger.debug(firstDate);
			logger.debug(lastDate);
			SocietyVO societyVO=societyService.getSocietyDetails(intSocietyID);
			String sqls = " SELECT ag.category_id,ag.category_name ,ac.group_name,al.ledger_name AS ledgerName,tx_date,MONTHNAME(tx_date) AS MONTH,ale.*,SUM(ale.amount) AS txAmount ,ac.id AS groupID "
					+ " FROM account_transactions_"
					+ societyVO.getDataZoneID()
					+ " ats,account_ledger_entry_"
					+ societyVO.getDataZoneID()
					+ " ale,account_ledgers_"
					+ societyVO.getDataZoneID()
					+ " al,account_groups ac ,account_category ag"
					+ " WHERE  ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID and ats.tx_id=ale.tx_id  AND ale.ledger_id=al.id AND al.sub_group_id=ac.id AND ac.root_id=2 AND ats.is_deleted=0 AND  ac.category_id=ag.category_id and"
					+ " ( ats.tx_date BETWEEN :firstDate AND :lastDate) GROUP BY MONTH,al.id ORDER BY tx_date ASC;";

			Map hashMap = new HashMap();
			hashMap.put("firstDate", firstDate);
			hashMap.put("lastDate", lastDate);
			hashMap.put("societyID", intSocietyID);
			
			logger.debug("HashMap contains :--" + hashMap);
			final RowMapper rMapper = new RowMapper() {
				String tempCategoryName = "";

				BigDecimal total = new BigDecimal(0.00);

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					RptMonthYearVO monthYearVO = new RptMonthYearVO();

				
					
					ReportDetailsVO rptVO = new ReportDetailsVO();
					rptVO.setLedgerName(rs.getString("ledgerName"));
					rptVO.setCategoryName(rs.getString("category_name"));
					rptVO.setTxDate(rs.getString("tx_date"));
					rptVO.setMonthName(rs.getString("month"));
					rptVO.setGroupName(rs.getString("group_name"));
					rptVO.setTotalAmount(rs.getBigDecimal("txAmount"));
					rptVO.setGroupID(rs.getInt("groupID"));
			

					return monthYearVO;
				}

			};
			reportList = namedParameterJdbcTemplate.query(sqls, hashMap,
					rMapper);
			logger
					.debug("Exit:public List monthwiseExpenseReport(int year,int intSocietyID) and ReportDAO"
							+ reportList);
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in Monthly Expense : " + ex);
		} catch (Exception ex) {
			logger.error("Exception:" + ex);
		}
		return reportList;

	}

	public List getIncomeExpenditureReportDebit(String firstDate,
			String lastDate, int societyId) {
		List incExpList = new ArrayList();
		try {
			logger
					.debug("Entry:public List monthlyIncomeExpenditureReportDebit(int year, int id, int month)");

			Map hashMap = new HashMap();
			hashMap.put("firstDate", firstDate);
			hashMap.put("lastDate", lastDate);
			hashMap.put("societyID", societyId);
			SocietyVO societyVO=societyService.getSocietyDetails(societyId);
			String sql = "SELECT ag.category_id,ag.category_name ,ac.group_name,ale.*,SUM(ale.amount) AS txAmount ,ac.id AS groupID "
					+ " FROM account_transactions_"
					+ societyVO.getDataZoneID()
					+ " ats,account_ledger_entry_"
					+ societyVO.getDataZoneID()
					+ " ale,account_ledgers_"
					+ societyVO.getDataZoneID()
					+ " al,account_groups ac ,account_category ag"
					+ " WHERE  ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID and ats.tx_id=ale.tx_id AND ale.ledger_id=al.id AND al.sub_group_id=ac.id AND ac.root_id=2 AND ats.is_deleted=0 AND ac.category_id=ag.category_id AND "
					+ " ( ats.tx_date BETWEEN :firstDate AND :lastDate) GROUP BY ac.id;";

			logger.debug("query : " + sql);
			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					// TODO Auto-generated method stub
					ReportDetailsVO rptVO = new ReportDetailsVO();
					rptVO.setLedgerName(rs.getString("ledger_name"));
					rptVO.setCategoryName(rs.getString("category_name"));
					rptVO.setGroupName(rs.getString("group_name"));
					rptVO.setTxType(rs.getString("type"));
					rptVO.setTotalAmount(rs.getBigDecimal("txAmount"));
					rptVO.setGroupID(rs.getInt("groupID"));
			
					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate
					.query(sql, hashMap, rMapper);
			//	logger.info("###############################"+incExpList.size()+incExpList);
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getMonthlyIncExpDeb : " + ex);
		} catch (Exception ex) {
			logger.error("Exception in monthlyIncomeExpenditureReportDebit : "
					+ ex);
		}
		logger
				.debug("Exit:public List monthlyIncomeExpenditureReportDebit(int year, int id, int month)"
						+ incExpList.size());
		return incExpList;

	}

	public List getMonthlyIncomeExpenditureReportDetailed(String firstDate,
			String lastDate, int societyId, int groupID) {
		List incExpList = new ArrayList();
		try {
			logger
					.debug("Entry:public List getMonthlyIncomeExpenditureReportDetailed(String firstDate,String lastDate ,int societyId,int groupID)"
							+ groupID);

			Map hashMap = new HashMap();
			hashMap.put("firstDate", firstDate);
			hashMap.put("lastDate", lastDate);
			hashMap.put("groupID", groupID);
			hashMap.put("societyID",societyId);
			
			SocietyVO societyVO=societyService.getSocietyDetails(societyId);
			
			String sql = "SELECT ag.category_id,ag.category_name ,ac.group_name,ale.*,SUM(ale.amount) AS txAmount ,ac.id AS groupID ,al.ledger_name AS ledgerName "
					+ " FROM account_transactions_"
					+ societyVO.getDataZoneID()
					+ " ats,account_ledger_entry_"
					+ societyVO.getDataZoneID()
					+ " ale,account_ledgers_"
					+ societyVO.getDataZoneID()
					+ " al,account_groups ac ,account_category ag "
					+ " WHERE  ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID and ats.tx_id=ale.tx_id AND ale.ledger_id=al.id AND al.sub_group_id=ac.id AND ac.id=:groupID AND ats.is_deleted=0 AND ac.category_id=ag.category_id and "
					+ " ( ats.tx_date BETWEEN :firstDate AND :lastDate) GROUP BY al.id;";

			logger.debug("query : " + sql);
			RowMapper rMapper = new RowMapper()

			{

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					// TODO Auto-generated method stub
					
					ReportDetailsVO rptVO = new ReportDetailsVO();
					rptVO.setLedgerName(rs.getString("ledgerName"));
					rptVO.setCategoryName(rs.getString("category_name"));
					rptVO.setGroupName(rs.getString("group_name"));
					rptVO.setTxType(rs.getString("type"));
					rptVO.setTotalAmount(rs.getBigDecimal("txAmount"));
					rptVO.setGroupID(rs.getInt("groupID"));
				

					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate
					.query(sql, hashMap, rMapper);
			//	logger.info("###############################"+incExpList.size()+incExpList);
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getMonthlyIncExpDeb : " + ex);
		} catch (Exception ex) {
			logger
					.error("Exception in getMonthlyIncomeExpenditureReportDetailed : "
							+ ex);
		}
		logger
				.debug("Exit:public List getMonthlyIncomeExpenditureReportDetailed(String firstDate,String lastDate ,int societyId,int groupID)"
						+ incExpList.size());
		return incExpList;

	}
	
	
	
	public List getCashBasedReceiptPaymentReport(String fromDate, String uptoDate,
			int societyId, int isConsolidated) {
		List incExpList = new ArrayList();
		try {
			logger
					.debug("Entry: public List getCashBasedReceiptPaymentReport(String fromDate, String uptoDate,int societyId, String txType,int isConsolidated) "+ fromDate + uptoDate);
			String orgCondition="";
			String orgClause="";
			String strTxTypeClause="";
			if(isConsolidated==0){
				orgCondition=" AND l.org_id=:orgID ";
				orgClause=" And al.org_id=:orgID ";
			}
			
			
			
			SocietyVO societyVO=societyService.getSocietyDetails(societyId);
			String sql = "SELECT alg.category_name, alg.ledger_name AS ledgerName,alg.sub_group_id as groupID,alg.group_name , txTable.*,SUM(amount) AS txAmount FROM (SELECT  ats.tx_id AS TXID,ats.tx_date,ats.ref_no,ats.create_date,ats.bank_date,ats.description,ats.tx_number,ats.tx_type,tx_mode,ats.created_by,ats.updated_by,SUM(CASE WHEN (ABS(table_primary.amount)>ABS(table_secondary.amount)) THEN ABS(table_secondary.amount) ELSE ABS(table_primary.amount) END) AS amount, table_primary.type, table_primary.tx_id,table_secondary.ledger_id,a.ledger_name ,sub_group_id,amt_in_word "+ 
			           "  FROM account_ledger_entry_"+societyVO.getDataZoneID()+" table_primary,account_ledger_entry_"+societyVO.getDataZoneID()+" table_secondary,account_ledgers_"+societyVO.getDataZoneID()+" a,account_transactions_"+societyVO.getDataZoneID()+" ats "+
			           "  WHERE table_primary.tx_id = table_secondary.tx_id AND  "+
			           " ats.org_id=a.org_id and a.org_id=table_primary.org_id and table_primary.org_id=table_secondary.org_id "+orgCondition+
			           "  AND table_primary.ledger_id=a.id "+
			           "  AND table_primary.type!=table_secondary.type  "+
			  		  "  AND ats.tx_id = table_primary.tx_id AND (tx_date BETWEEN :fromDate AND :toDate) and (ats.tx_type!='Contra' and ats.tx_type!='Journal' AND ats.tx_type!='Sales' AND ats.tx_type!='Purchase') "+  
		              "   AND ats.is_deleted=0  GROUP BY table_secondary.ledger_id,ats.tx_type,table_secondary.type ORDER BY ats.tx_date ) AS txTable LEFT JOIN (SELECT al.id AS ledger_id,al.sub_group_id,al.ledger_name,ag.group_name,ac.category_name FROM account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups ag,account_category ac WHERE "+orgClause+" al.sub_group_id=ag.id and ac.category_id=ag.category_id ) AS alg ON txTable.ledger_id=alg.ledger_id WHERE ( alg.sub_group_id!=4 AND alg.sub_group_id!=5 and alg.sub_group_id!=95)  GROUP BY tx_type,groupID ORDER BY alg.group_name ;";
 
			String sqlQuery ="SELECT SUM(ABS(tx_negative)) AS txNegative,SUM(ABS(tx_positive)) AS txPositive,SUM(tx_sum) AS txSum,tabl.* fROM(SELECT root_id,g.id AS groupID,g.category_id,g.group_name,ale.type,ale.tx_id,ats.tx_type, al.id AS tx_ledger_id,al.ledger_name "+
						" ,(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END) AS tx_negative, (CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END) AS tx_positive, (SUM( ale.amount)) AS tx_sum FROM (SELECT a.* FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_ledgers_"+societyVO.getDataZoneID()+" l ,account_groups g"+
					    " WHERE a.is_deleted=0 AND a.tx_id=al.tx_id AND a.org_id=l.org_id "+orgCondition+" AND a.tx_date BETWEEN :fromDate AND :toDate AND al.ledger_id=l.id AND l.sub_group_id=g.id AND a.tx_type!='Contra' AND (g.category_id=3 OR g.category_id=101 OR g.category_id=64 OR g.category_id=65 )) AS ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups g WHERE ats.tx_date "+
                        " BETWEEN :fromDate AND :toDate AND ats.is_deleted = 0 AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND ats.org_id=ale.org_id AND ale.org_id=al.org_id "+orgClause+ 
                        " AND al.sub_group_id = g.id AND   ats.tx_type!='Contra'  GROUP BY ale.id) AS tabl WHERE  tabl.category_id!=3 AND tabl.category_id!=101 AND tabl.category_id!=64 AND tabl.category_id!=65 GROUP BY tabl.groupID,TYPE ORDER BY group_name;";
			logger.debug("query : " + sql);

			Map hashMap = new HashMap();
			hashMap.put("fromDate", fromDate);
			hashMap.put("toDate", uptoDate);
			hashMap.put("orgID", societyId);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					// TODO Auto-generated method stub
					ReportDetailsVO rptVO = new ReportDetailsVO();
					rptVO.setLedgerName(rs.getString("ledger_name"));
					rptVO.setLedgerID(rs.getInt("tx_ledger_id"));
					rptVO.setGroupName(rs.getString("group_name"));
					rptVO.setTxType(rs.getString("type"));
					rptVO.setCreditBalance(rs.getBigDecimal("txNegative").abs().setScale(2, RoundingMode.HALF_UP));
					rptVO.setDebitBalance(rs.getBigDecimal("txPositive").abs().setScale(2, RoundingMode.HALF_UP));
					rptVO.setGroupID(rs.getInt("groupID"));
					rptVO.setRootID(rs.getInt("root_id"));
					
					
				

					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate
					.query(sqlQuery, hashMap, rMapper);
			//	logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getCashBasedReceiptPaymentReport: " + ex);
		} catch (Exception ex) {
			logger.error("Exception in getCashBasedReceiptPaymentReport : "
					+ ex);
		}
		logger
				.debug("Exit:public List getCashBasedReceiptPaymentReport(String fromDate, String uptoDate,int societyId, String txType,int isConsolidated)"
						+ incExpList.size());
		return incExpList;

	}
	public List getCashBasedReceiptPaymentDetailsReport(String fromDate, String uptoDate,
			int societyId,int isConsolidated) {
		List incExpList = new ArrayList();
		try {
			logger
					.debug("Entry: public List getCashBasedReceiptPaymentDetailsReport(String fromDate, String uptoDate,int societyId, String txType,int isConsolidated)"
							+ fromDate + uptoDate);
			String strTxTypeClause="";
			String orgCondition="";
			String orgClause="";
			if(isConsolidated==0){
				orgCondition=" AND l.org_id=:orgID ";
				orgClause=" And al.org_id=:orgID ";
			}
			 
		
			SocietyVO societyVO=societyService.getSocietyDetails(societyId);
			String sqlQuery ="SELECT SUM(ABS(tx_negative)) AS txNegative,SUM(ABS(tx_positive)) AS txPositive,SUM(tx_sum) AS txSum,tabl.* fROM(SELECT root_id,g.id AS groupID,g.group_name,g.category_id,ale.type,ale.tx_id,ats.tx_type, al.id AS tx_ledger_id,al.ledger_name "+
					" ,(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END) AS tx_negative, (CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END) AS tx_positive, (SUM( ale.amount)) AS tx_sum FROM (SELECT a.* FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_ledgers_"+societyVO.getDataZoneID()+" l ,account_groups g"+
				    " WHERE a.is_deleted=0 AND a.tx_id=al.tx_id AND a.org_id=l.org_id "+orgCondition+" AND a.tx_date BETWEEN :fromDate AND :toDate AND al.ledger_id=l.id and l.sub_group_id=g.id AND a.tx_type!='Contra' AND (g.category_id=3 OR g.category_id=101 OR g.category_id=64 OR g.category_id=65 )) AS ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups g WHERE ats.tx_date "+
                    " BETWEEN :fromDate AND :toDate AND ats.is_deleted = 0 AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND ats.org_id=ale.org_id AND ale.org_id=al.org_id "+orgClause+ 
                    " AND al.sub_group_id = g.id AND  ats.tx_type!='Contra'  GROUP BY ale.id) AS tabl WHERE  tabl.category_id!=3 AND tabl.category_id!=101 AND tabl.category_id!=64 AND tabl.category_id!=65 GROUP BY tabl.tx_ledger_id,TYPE ORDER BY group_name;";
		logger.debug("query : " + sqlQuery);

			Map hashMap = new HashMap();
			hashMap.put("fromDate", fromDate);
			hashMap.put("toDate", uptoDate);
			hashMap.put("orgID",societyId);

			RowMapper rMapper = new RowMapper() {
				String tempGroupName = "";

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					// TODO Auto-generated method stub
					ReportDetailsVO rptVO = new ReportDetailsVO();
					rptVO.setLedgerName(rs.getString("ledger_name"));
					rptVO.setLedgerID(rs.getInt("tx_ledger_id"));
					rptVO.setGroupName(rs.getString("group_name"));
					rptVO.setTxType(rs.getString("type"));
					rptVO.setCreditBalance(rs.getBigDecimal("txNegative").abs().setScale(2, RoundingMode.HALF_UP));
					rptVO.setDebitBalance(rs.getBigDecimal("txPositive").abs().setScale(2, RoundingMode.HALF_UP));
					rptVO.setGroupID(rs.getInt("groupID"));
					rptVO.setRootID(rs.getInt("root_id"));
					
									
					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate
					.query(sqlQuery, hashMap, rMapper);
			//	logger.info("###############################"+incExpList.size()+incExpList);

		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getCashBasedReceiptPaymentDetailsReport: " + ex);
		} catch (Exception ex) {
			logger.error("Exception in getCashBasedReceiptPaymentDetailsReport : "
					+ ex);
		}
		logger
				.debug("Exit: public List getCashBasedReceiptPaymentDetailsReport(String fromDate, String uptoDate,	int societyId, String txType,int isConsolidated)"
						+ incExpList.size());
		return incExpList;

	}
	
	public List getCashBasedLedgerReport(final int societyID, final String fromDate,
			String uptoDate, int groupID,String txType,int isConsolidated) {

		List<AccountHeadVO> ledgerList = new ArrayList<AccountHeadVO>();

		try {
			String strCondition="";
			if(txType.equalsIgnoreCase("D")){
				strCondition=" and ale.type='D' ";
			}else if(txType.equalsIgnoreCase("C")){
				strCondition="  AND ale.type='C' ";
			}else if(txType.equalsIgnoreCase("Receipt")){
				strCondition=" AND ats.tx_type='Receipt' ";
			}else if(txType.equalsIgnoreCase("Payment")){
				strCondition=" AND ats.tx_type='Payment' ";
			}else strCondition="";
			
			String orgCondition="";
			String orgClause="";
			if(isConsolidated==0){
				orgCondition=" AND al.org_id=:orgID ";
				orgClause=" And l.org_id=:orgID ";
			}

			logger
					.debug("Entry : public List getLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String effectiveDate,int isConsolidated)"
							+ groupID);
			SocietyVO societyVO=societyService.getSocietyDetails(societyID);
		
			String sqlQuery ="SELECT SUM(ABS(tx_negative)) AS txNegative,SUM(ABS(tx_positive)) AS txPositive,SUM(tx_sum) AS txSum,tabl.* fROM(SELECT root_id,account_groups.id AS groupID,account_groups.group_name,ale.type,ale.tx_id,ats.tx_type, al.id AS tx_ledger_id,al.ledger_name "+
					" ,(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END) AS tx_negative, (CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END) AS tx_positive, (SUM( ale.amount)) AS tx_sum ,category_id FROM (SELECT a.* FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_ledgers_"+societyVO.getDataZoneID()+" l ,account_groups g "+
				    " WHERE a.is_deleted=0 AND a.tx_id=al.tx_id AND a.org_id=l.org_id "+orgClause+" AND a.tx_date BETWEEN :fromDate AND :toDate AND al.ledger_id=l.id AND a.tx_type!='Contra' and l.sub_group_id=g.id AND (g.category_id=3 OR g.category_id=101 OR g.category_id=64 OR g.category_id=65 )) AS ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups WHERE ats.tx_date "+
                    " BETWEEN :fromDate AND :toDate AND ats.is_deleted = 0 AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND ats.org_id=ale.org_id AND ale.org_id=al.org_id "+orgCondition+strCondition+
                    " AND al.sub_group_id = account_groups.id AND  ats.tx_type!='Contra' GROUP BY ale.id) AS tabl WHERE tabl.category_id!=3 AND tabl.category_id!=101 AND tabl.category_id!=64 AND tabl.category_id!=65 and tabl.groupID=:groupID  GROUP BY tabl.tx_ledger_id,TYPE ORDER BY ledger_name;";

			logger.debug(sqlQuery);
			Map hMap = new HashMap();
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", uptoDate);
			hMap.put("groupID", groupID);
			hMap.put("orgID",societyID);

			RowMapper Rmapper = new RowMapper() {
				String normalBalance = "";

				int rootID = 0;

				String groupName = "";

				int groupID = 0;

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					AccountHeadVO acHVO = new AccountHeadVO();

				
					
					acHVO.setCrClsBal(rs.getBigDecimal("txNegative").abs().setScale(2, RoundingMode.HALF_UP));
					acHVO.setDrClsBal(rs.getBigDecimal("txPositive").abs().setScale(2, RoundingMode.HALF_UP));
					acHVO.setRootID(rs.getInt("root_id"));
					acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
					acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
					acHVO.setAccountGroupID(rs.getInt("groupID"));
					acHVO.setLedgerID(rs.getInt("tx_ledger_id"));
					acHVO.setAccType(rs.getString("type"));
				

					return acHVO;
				}

			};
			ledgerList = namedParameterJdbcTemplate.query(sqlQuery, hMap, Rmapper);

			logger
					.debug("Exit : public List getLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String effectiveDate,int isConsolidated)"
							+ ledgerList.size());
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getLedgerReport : " + ex);
		} catch (Exception e) {
			logger.error("Exception : " + e);
		}

		return ledgerList;
	}

	public List getAccDetails(int year, int strSocietyID, String searchTxType) {
		List accList = null;
		RptMonthYearVO rptVo = new RptMonthYearVO();
		String strWhereClause = null;
		try {
			logger
					.debug("Entry :public RptMonthYearVO getCashAccDetails(int year,String strSocietyID) ");
			if (searchTxType.equalsIgnoreCase("Cash"))
				strWhereClause = " AND type = 'Cash'";
			if (searchTxType.equalsIgnoreCase("Bank"))
				strWhereClause = " AND type = 'Bank'";
			String strSQL = "SELECT sa.acc_id,description,opening_bal FROM soc_account_details sa ,account_balance_details ab WHERE sa.acc_id=ab.acc_id AND sa.society_id=ab.society_id AND is_primary='Y' "
					+ strWhereClause
					+ " AND sa.society_id=:societyID AND YEAR=:year; ";
			//1. SQL Query" "
			logger.debug("query : " + strSQL);
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyID", strSocietyID);
			namedParameters.put("year", year);
			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					// TODO Auto-generated method stub
					RptMonthYearVO rptVO = new RptMonthYearVO();
					rptVO.setOpeningBalance(rs.getBigDecimal("opening_bal"));
					rptVO.setCategoryID(rs.getString("acc_id"));
					rptVO.setAccName(rs.getString("Description"));

					return rptVO;
				}
			};

			accList = namedParameterJdbcTemplate.query(strSQL, namedParameters,
					rMapper);

			logger
					.debug("Exit : public RptMonthYearVO getCashAccDetails(int year,String strSocietyID)");
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in geetAcc: " + ex);
		} catch (EmptyResultDataAccessException e) {
			logger
					.info("No details available for opening balance for societyId "
							+ strSocietyID + " for the year " + year + e);
		} catch (Exception e) {
			logger.error("Exception : " + e);
		}

		return accList;
	}

	public List getNewSysAccDetails(int groupID, int societyID) {
		List accList = null;
		RptMonthYearVO rptVo = new RptMonthYearVO();

		try {
			logger
					.debug("Entry : public List getNewSysAccDetails(int groupID,int societyID)");
			SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			String strSQL = "SELECT * FROM account_ledgers_" + societyVO.getDataZoneID()
					+ " WHERE sub_group_id=:groupID AND is_deleted=0 and org_id=:societyID; ";
			//1. SQL Query" "
			logger.debug("query : " + strSQL);
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("groupID", groupID);
			namedParameters.put("societyID", societyID);

			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					// TODO Auto-generated method stub
					RptMonthYearVO rptVO = new RptMonthYearVO();
					rptVO
							.setOpeningBalance(rs
									.getBigDecimal("opening_balance"));
					rptVO.setLedgerID(rs.getInt("id"));
					rptVO.setAccName(rs.getString("ledger_name"));

					return rptVO;
				}
			};

			accList = namedParameterJdbcTemplate.query(strSQL, namedParameters,
					rMapper);

			logger
					.debug("Exit : public List getNewSysAccDetails(int groupID,int societyID)");
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in geetAcc: " + ex);
		} catch (EmptyResultDataAccessException e) {
			logger
					.info("No details available for opening balance for societyId "
							+ societyID + " for the year ");
		} catch (Exception e) {
			logger.error("Exception : " + e);
		}

		return accList;
	}

	//=========Document Reports==================//
	public List getMemberDocs(String buildingID) {
		List memDocLst = new ArrayList();
		try {
			logger.debug("Entry:getMemberDocs(String buildingID)");
			String sql = "SELECT COUNT(d.doc_id) AS no_of_docs,a.apt_id,mobile,CONCAT(title,' ',full_name) AS memberName, CONCAT(b.building_name,' ',a.apt_name) AS building_name , IFNULL( SUM(r.is_doc),0) AS counter "
					+ " FROM (apartment_details a ,document_details d,building_details b, member_details m) "
					+ " LEFT JOIN documents_relation r ON d.doc_id =r.doc_id WHERE  a.building_id=b.building_id AND a.apt_id=m.apt_id and m.is_current!=0  "
					+ " AND b.building_id=:bldgID AND (a.apt_id=r.apt_id OR r.apt_id IS NULL) GROUP BY a.apt_id Order by seq_no;";

			Map hashMap = new HashMap();
			hashMap.put("bldgID", buildingID);

			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					// TODO Auto-generated method stub
					MemberVO memberVO = new MemberVO();
					memberVO.setAptID(rs.getInt("apt_id"));
					memberVO.setFullName(rs.getString("memberName"));
					memberVO.setFlatNo(rs.getString("building_name"));
					memberVO.setNoOfDocs(rs.getInt("no_of_docs"));
					memberVO.setMobile(rs.getString("mobile"));
					memberVO.setAvailableNoOfDocs(rs.getInt("counter"));

					if (memberVO.getNoOfDocs() > memberVO
							.getAvailableNoOfDocs()) {
						memberVO.setIsDoc(false);
					} else
						memberVO.setIsDoc(true);

					return memberVO;
				}
			};

			memDocLst = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception getMemberDocs) " + ex);
		} catch (Exception ex) {
			logger.error("Exception in getMemberDocs(String buildingID) : "
					+ ex);
		}
		logger.debug("Exit:Public List getMemberDocs(String buildingID)");
		return memDocLst;

	}

	public List getBankList() {

		List listBank = null;
		String strSQL = "";

		try {
			logger.debug("Entry : public List getBankList()");
			//1. SQL Query
			strSQL = " SELECT bank_id,bank_name FROM master_bank; ";

			Map namedParameters = new HashMap();
			logger.debug("query : " + strSQL);
			// 2. Parameters.

			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					DropDownVO bank = new DropDownVO();
					bank.setData(rs.getString("bank_id"));
					bank.setLabel(rs.getString("bank_name"));

					return bank;
				}
			};

			listBank = namedParameterJdbcTemplate.query(strSQL,
					namedParameters, RMapper);

			logger.debug("outside query : " + listBank.size());
			logger.debug("Exit : public List getBankList");
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception getBankList) " + ex);
		} catch (Exception ex) {
			logger.error("Exception in getBankList : " + ex);

		}
		return listBank;

	}

	public List searchMemberList(String strbuildingId) {

		List listMembers = null;
		String strSQL = "";

		try {
			logger
					.debug("Entry :  public List searchMemberList(String strbuildingId)");
			//1. SQL Query
			strSQL = "select apartment_details.*,member_details.* from apartment_details,member_details "
					+ "where apartment_details.building_id = :buildingId AND apartment_details.apt_id = member_details.apt_id AND member_details.is_current!=0 "
					+ "AND type='P' ORDER BY apartment_details.seq_no;";

			logger.debug("query : " + strSQL);
			// 2. Parameters.
			SqlParameterSource namedParameters = new MapSqlParameterSource(
					"buildingId", strbuildingId);

			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					DropDownVO membrs = new DropDownVO();
					membrs.setData(rs.getString("member_id"));
					membrs.setLabel(rs.getString("apt_name") + " - "
							+ rs.getString("full_name"));

					return membrs;
				}
			};

			listMembers = namedParameterJdbcTemplate.query(strSQL,
					namedParameters, RMapper);

			logger.debug("outside query : " + listMembers.size());
			logger
					.debug("Exit :  public List searchMemberList(String strbuildingId)");
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception searchMemberList) " + ex);
		} catch (Exception ex) {
			logger.error("Exception in searchMemberList : " + ex);

		}
		return listMembers;

	}

	// get Apartment Id
	public String getApartmentID(String strMemberID) {

		String aptID = null;
		String strSQL = "";

		try {
			logger
					.debug("Entry : public String getApartmentID(String strMemberID)");
			//1. SQL Query
			strSQL = "select apartment_details.apt_id from building_details,member_details,apartment_details "
					+ "where member_details.member_id = :member_ID "
					+ "and member_details.apt_id = apartment_details.apt_id and apartment_details.building_id = building_details.building_id ";
			logger.debug("query : " + strSQL);

			// 2. Parameters.
			SqlParameterSource namedParameters = new MapSqlParameterSource(
					"member_ID", strMemberID);

			aptID = namedParameterJdbcTemplate.queryForObject(strSQL,
					namedParameters, String.class).toString();

			logger
					.debug("Exit : public String getApartmentID(String strMemberID)");

		} catch (BadSqlGrammarException ex) {
			logger.info("Exception getSocietYID) " + ex);
		} catch (Exception ex) {
			logger.error("Exception in getSocietyID : " + ex);
		}

		return aptID;

	}

	public List getMorgageDetails(int societyID) {
		List morgFlatLst = new ArrayList();
		try {
			logger.debug("Entry:public List getMorgageDetails(int societyID)");
			String sql = " SELECT CONCAT(title,' ',full_name) AS memberName,CONCAT(building_details.building_name,' ',apartment_details.apt_name) AS buildingName,mobile,morgage_details.*,bank_name "
					+ " FROM morgage_details,apartment_details,member_details,building_details,master_bank "
					+ " WHERE apartment_details.apt_id=morgage_details.apt_id "
					+ " AND member_details.member_id=morgage_details.member_id "
					+ " AND morgage_details.bank_id=master_bank.bank_id "
					+ " AND apartment_details.building_id=building_details.building_id "
					+ " AND building_details.society_id=:socID AND morgage_details.is_deleted=0 AND morg_status='M' AND member_details.type='P' and is_current!=0 Order by seq_no ";

			Map hashMap = new HashMap();
			hashMap.put("socID", societyID);

			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					// TODO Auto-generated method stub
					RptMorgageVO morgVO = new RptMorgageVO();
					morgVO.setMorgId(rs.getString("morg_id"));
					morgVO.setAptId(rs.getInt("apt_id"));
					morgVO.setMemberName(rs.getString("memberName"));
					morgVO.setMobile(rs.getString("mobile"));
					morgVO.setFlatNo(rs.getString("buildingName"));
					morgVO.setBankId(rs.getString("bank_id"));
					morgVO.setBankName(rs.getString("bank_name"));
					morgVO.setBranchCode(rs.getString("branch_code"));
					morgVO.setFromDate(rs.getString("morg_from_date"));
					morgVO.setToDate(rs.getString("morg_to_date"));
					morgVO.setMorgAmount(rs.getBigDecimal("morg_amount"));
					morgVO.setMorgStatus(rs.getString("morg_status"));
					morgVO.setNotes(rs.getString("description"));
					morgVO.setIsDeleted(rs.getString("is_deleted"));
					return morgVO;
				}
			};

			morgFlatLst = namedParameterJdbcTemplate.query(sql, hashMap,
					rMapper);
			logger.debug("Exit : getMorgageDetails Size()--"
					+ morgFlatLst.size());
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception getMorgarge) " + ex);
		} catch (Exception ex) {
			logger.error("Exception in getMorgageDetails(int societyID) : "
					+ ex);
		}
		logger.debug("Exit:public List getMorgageDetails(int societyID)");
		return morgFlatLst;

	}

	public List getMorgageDetailsForMember(int societyID, int memberID) {
		List morgFlatLst = new ArrayList();
		try {
			logger
					.debug("Entry:public List getMorgageDetailsForMember(int societyID,int memberID)");
			String sql = " SELECT CONCAT(title,' ',full_name) AS memberName,CONCAT(building_details.building_name,' ',apartment_details.apt_name) AS buildingName,mobile,morgage_details.*,bank_name "
					+ " FROM morgage_details,apartment_details,member_details,building_details,master_bank "
					+ " WHERE apartment_details.apt_id=morgage_details.apt_id "
					+ " AND member_details.member_id=morgage_details.member_id "
					+ " AND morgage_details.bank_id=master_bank.bank_id "
					+ " AND apartment_details.building_id=building_details.building_id "
					+ " AND building_details.society_id=:socID AND member_details.member_id=:memberID and morgage_details.is_deleted=0 AND morg_status='M' AND member_details.type='P' and is_current!=0 Order by seq_no ";

			Map hashMap = new HashMap();
			hashMap.put("socID", societyID);
			hashMap.put("memberID", memberID);

			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					// TODO Auto-generated method stub
					RptMorgageVO morgVO = new RptMorgageVO();
					morgVO.setMorgId(rs.getString("morg_id"));
					morgVO.setAptId(rs.getInt("apt_id"));
					morgVO.setMemberName(rs.getString("memberName"));
					morgVO.setMobile(rs.getString("mobile"));
					morgVO.setFlatNo(rs.getString("buildingName"));
					morgVO.setBankId(rs.getString("bank_id"));
					morgVO.setBankName(rs.getString("bank_name"));
					morgVO.setBranchCode(rs.getString("branch_code"));
					morgVO.setFromDate(rs.getString("morg_from_date"));
					morgVO.setToDate(rs.getString("morg_to_date"));
					morgVO.setMorgAmount(rs.getBigDecimal("morg_amount"));
					morgVO.setMorgStatus(rs.getString("morg_status"));
					morgVO.setNotes(rs.getString("description"));
					morgVO.setIsDeleted(rs.getString("is_deleted"));
					return morgVO;
				}
			};

			morgFlatLst = namedParameterJdbcTemplate.query(sql, hashMap,
					rMapper);
			logger.debug("Exit : getMorgageDetails Size()--"
					+ morgFlatLst.size());
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception getMorgarge) " + ex);
		} catch (Exception ex) {
			logger
					.error("Exception in getMorgageDetailsForMember(int societyID,int memberID) : "
							+ ex);
		}
		logger
				.debug("Exit:public List getMorgageDetailsForMember(int societyID,int memberID)");
		return morgFlatLst;

	}

	public int insertMoragageDetails(RptMorgageVO morgageVO, String aptId) {
		int insertFlag = 0;

		logger
				.debug("Entry : public int insertMoragageDetails(RptMorgageVO morgageVO,String aptId)");
		try {

			String insertFileString = " INSERT INTO morgage_details "
					+ " ( apt_id, member_id, bank_id, branch_code, morg_from_date, morg_to_date, morg_amount, morg_status, description ) "
					+ " SELECT * FROM (SELECT :aptId, :memberId, :bankId, :branchCode, :fromDate, :toDate,:morgAmount, :morgStatus, :notes ) AS tmp "
					+ " WHERE NOT EXISTS (SELECT morg_status,member_id FROM morgage_details WHERE member_id="
					+ morgageVO.getMemberId()
					+ " AND morg_status='M' AND is_deleted='0' ) LIMIT 1 ";

			logger.debug("query : " + insertFileString);

			Map hmap = new HashMap();
			hmap.put("aptId", aptId);
			hmap.put("memberId", morgageVO.getMemberId());
			hmap.put("bankId", morgageVO.getBankId());
			hmap.put("branchCode", morgageVO.getBranchCode());
			hmap.put("fromDate", morgageVO.getFromDate());
			hmap.put("toDate", morgageVO.getToDate());
			hmap.put("morgAmount", morgageVO.getMorgAmount());
			hmap.put("morgStatus", morgageVO.getMorgStatus());
			hmap.put("notes", morgageVO.getNotes());

			insertFlag = namedParameterJdbcTemplate.update(insertFileString,
					hmap);

			logger
					.debug("Exit : public int insertMoragageDetails(RptMorgageVO morgageVO,String aptId))"
							+ insertFlag);

		} catch (Exception e) {
			insertFlag = 0;
			logger.error("Exception in insertMoragageDetails : " + e);
		}

		return insertFlag;
	}

	public int updateMorgage(RptMorgageVO morgageVO, String morgId) {

		int flag = 0;

		try {
			logger
					.debug("Entry : public int updateMorgage(RptMorgageVO morgageVO,String morgId)");
			Date currentDatetime = new Date(System.currentTimeMillis());
			java.sql.Date sqlDate = new java.sql.Date(currentDatetime.getTime());
			java.sql.Timestamp timestamp = new java.sql.Timestamp(
					currentDatetime.getTime());

			SimpleDateFormat sdf = new SimpleDateFormat(
					"dd - MMM - yyyy h:mm:ss a");
			String formattedDate = sdf.format(currentDatetime);

			String query = "UPDATE morgage_details SET morg_amount= :morgAmount, morg_status= :morgStatus, morg_from_date= :fromDate, morg_to_date= :toDate,description= :notes  "
					+ "  WHERE morg_id=" + morgId + "";
			logger.debug("query : " + query);

			Map hmap = new HashMap();
			hmap.put("fromDate", morgageVO.getFromDate());
			hmap.put("toDate", morgageVO.getToDate());
			hmap.put("morgAmount", morgageVO.getMorgAmount());
			hmap.put("morgStatus", morgageVO.getMorgStatus());
			hmap.put("notes",morgageVO.getNotes());
			//hmap.put("notes", formattedDate + "- Updated By:-"
					//+ morgageVO.getMemberName() + "<br>"
					//+ morgageVO.getComments() + "<br>---------------------<br>"
					//+ morgageVO.getNotes());

			flag = namedParameterJdbcTemplate.update(query, hmap);

			logger
					.debug("Exit : public int updateMorgage(RptMorgageVO morgageVO,String morgId) "
							+ flag);

		} catch (DataAccessException exc) {
			flag = 0;
			logger.error("DataAccessException in updateMorgage : " + exc);

		} catch (Exception e) {
			logger.error("Exception in updateMorgage : " + e);
		}
		return flag;
	}

	public int removMorgage(String morgageId) {

		int sucess = 0;
		String strSQL = "";
		try {
			logger.debug("Entry : public int removMorgage(String morgageId");
			strSQL = "Update morgage_details SET is_deleted='1' WHERE morg_id=:morgageId;";
			logger.debug("query : " + strSQL);
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("morgageId", morgageId);

			sucess = namedParameterJdbcTemplate.update(strSQL, namedParameters);

			logger.debug("Exit : public int removMorgage(String morgageId"
					+ sucess);

		} catch (Exception Ex) {
			Ex.printStackTrace();
			logger.error("Exception in removMorgage : " + Ex);
		}
		return sucess;
	}

	public List getMorgageHistory(String apartmentID) {
		List morgageHistory = null;

		try {
			logger
					.debug("Entry : public List getMorgageHistory(String apartmentID)");

			String query = "SELECT CONCAT(title,' ',full_name) AS memberName,bank_name,CONCAT(building_details.building_name,' ',apartment_details.apt_name) AS buildingName,mobile,morgage_details.* "
					+ " FROM morgage_details,apartment_details,member_details,building_details,master_bank "
					+ " WHERE morgage_details.apt_id="
					+ apartmentID
					+ " AND is_deleted='0' AND morg_status='MC' "
					+ " AND apartment_details.apt_id=morgage_details.apt_id "
					+ " AND member_details.member_id=morgage_details.member_id "
					+ " AND morgage_details.bank_id=master_bank.bank_id "
					+ " AND apartment_details.building_id=building_details.building_id ";

			logger.debug("query : " + query);
			Map namedParameters = new HashMap();
			//namedParameters.put("aptID", apartmentID);

			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					RptMorgageVO morgVO = new RptMorgageVO();
					morgVO.setMorgId(rs.getString("morg_id"));
					morgVO.setMemberName(rs.getString("memberName"));
					morgVO.setMobile(rs.getString("mobile"));
					morgVO.setFlatNo(rs.getString("buildingName"));
					morgVO.setBankId(rs.getString("bank_id"));
					morgVO.setBankName(rs.getString("bank_name"));
					morgVO.setBranchCode(rs.getString("branch_code"));
					morgVO.setFromDate(rs.getString("morg_from_date"));
					morgVO.setToDate(rs.getString("morg_to_date"));
					morgVO.setMorgAmount(rs.getBigDecimal("morg_amount"));
					morgVO.setMorgStatus(rs.getString("morg_status"));
					morgVO.setNotes(rs.getString("description"));
					morgVO.setIsDeleted(rs.getString("is_deleted"));
					return morgVO;

				}
			};

			morgageHistory = namedParameterJdbcTemplate.query(query,
					namedParameters, RMapper);

		} catch (DataAccessException e) {
			logger.error("DataAccessException in getMorgageHistory : " + e);
		} catch (Exception ex) {
			logger.error("Exception in getMorgageHistory : " + ex);
		}

		logger.debug("Exit :public List getMorgageHistory(String apartmentID)");
		return morgageHistory;
	}

	public int updateArrears(String arrears, String paidArrears,
			String arrearsComment, String aptID) {
		int sucess = 0;
		try {
			logger
					.debug("Entry : public int updateArrears(String paidArrears,String arrearsComment,String aptID )");
			String sqlStirng = " UPDATE apartment_details  SET arrears=:arrears, arrears_paid=:paidArrears, arrears_comment=:arrearsComment   WHERE apt_id="
					+ aptID + " ;";

			Map hmap = new HashMap();
			hmap.put("arrears", arrears);
			hmap.put("paidArrears", paidArrears);
			hmap.put("arrearsComment", arrearsComment);

			sucess = namedParameterJdbcTemplate.update(sqlStirng, hmap);

		} catch (Exception ex) {
			logger.error("Exception in updateCreditDebitTx : " + ex);

		}

		logger
				.debug("Exit :public int updateArrears(String paidArrears,String arrearsComment,String aptID )");
		return sucess;
	}

	public List getMemberList(int societyId, String buildingID) {
		List incExpList = new ArrayList();
		try {
			logger.debug("Entry: public List getMemberList(int societyID)"
					+ societyId);
			String strWhereClause = null;
			if (buildingID.equalsIgnoreCase("0"))
				strWhereClause = " ";
			else
				strWhereClause = "  AND b.building_id=" + buildingID + "";

			Map hashMap = new HashMap();

			hashMap.put("societyID", societyId);

			String sql = "SELECT a.*,m.*,concat(b.building_name,' ',a.apt_name) as flat_no FROM apartment_details a,building_details b,member_details m WHERE m.apt_id=a.apt_id AND a.building_id=b.building_id "
					+ " AND m.type='P' AND m.is_current='1' AND b.society_id=:societyID "
					+ strWhereClause + " order by seq_no ";

			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					// TODO Auto-generated method stub
					RptTransactionVO rptVO = new RptTransactionVO();
					rptVO.setAptID(rs.getInt("apt_id"));
					rptVO.setUnitID(rs.getInt("unit_id"));
					rptVO.setAddress(rs.getString("flat_no"));
					rptVO.setMobile(rs.getString("mobile"));
					rptVO.setEmail(rs.getString("email"));
					rptVO.setArrears(rs.getBigDecimal("arrears"));
					rptVO.setMember_name(rs.getString("full_name"));
					rptVO.setSqFeets(rs.getBigDecimal("sq_feet"));
					rptVO.setMember_id(rs.getInt("member_id"));
					rptVO.setIs_rented(rs.getInt("is_Rented"));

					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate
					.query(sql, hashMap, rMapper);
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception getMemberList) " + ex);
		} catch (Exception ex) {
			logger
					.error("Exception in public List getMemberList(int societyID) : "
							+ ex);
		}
		logger.debug("Exit: public List getMemberList(int societyID)"
				+ incExpList.size());
		return incExpList;
	}

	public List getMemberChargeDetails(RptTransactionVO rptMember, int societyID) {
		List incExpList = new ArrayList();
		try {
			logger
					.debug("Entry: private List getMemberChargeDetails(RptTransactionVO rptMember,int societyID)"
							+ societyID);

			String condition = "AND s.unit_id=" + rptMember.getUnitID();

			Map hashMap = new HashMap();

			hashMap.put("societyID", societyID);
			hashMap.put("memberID", rptMember.getMember_id());
			hashMap.put("unitID", rptMember.getUnitID());

			String sql = "SELECT * FROM member_details m,soc_charge_details s ,apartment_details a,building_details b ,society_settings ss "
					+ " WHERE m.apt_id=a.apt_id AND a.building_id=b.building_id and s.society_id=ss.society_id "
					+ "AND m.type='P' AND m.is_current='1' AND b.society_id=s.society_id AND m.member_id=:memberID"
					+ " AND ( s.unit_id=:unitID OR s.unit_id IS NULL) Order by seq_no ";

			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					// TODO Auto-generated method stub
					RptTransactionVO rptVO = new RptTransactionVO();
					rptVO.setAptID(rs.getInt("apt_id"));
					rptVO.setUnitID(rs.getInt("unit_id"));
					rptVO.setSqFeets(rs.getBigDecimal("sq_feet"));
					rptVO.setMember_id(rs.getInt("member_id"));
					rptVO.setCalculationMode(rs.getString("cal_method"));
					rptVO.setTx_ammount(rs.getBigDecimal("amount"));
					rptVO.setCategoryID(rs.getInt("charge_id"));
					rptVO.setIsLateFee(rs.getInt("is_late_fee"));
					rptVO.setLateFees(rs.getBigDecimal("late_fee_rate"));
					rptVO.setLateFeeType(rs.getString("late_fee_type"));
					rptVO.setFrequency(rs.getString("frequency"));
					rptVO.setFrom_date(rs.getDate("from_date"));
					rptVO.setTx_to_date(rs.getDate("to_date"));
					rptVO.setDayParam(rs.getInt("day_param"));
					rptVO.setMonthParam(rs.getInt("month_param"));
					rptVO.setCalculationMode(rs.getString("mode"));

					return rptVO;
				}
			};
			incExpList = namedParameterJdbcTemplate
					.query(sql, hashMap, rMapper);
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception getMemberChargeDetails) " + ex);
		} catch (Exception ex) {
			logger
					.error("Exception in public getMemberChargeDetails(RptTransactionVO rptMember,int societyID) : "
							+ ex);
		}
		logger
				.debug("Exit: public getMemberChargeDetails(RptTransactionVO rptMember,int societyID)"
						+ incExpList.size());
		return incExpList;
	}


	public List getRenterYealryHistory(int aptID, Date startDate, Date endDate) {
		List renterList = null;

		try {
			logger
					.debug("Entry : public List getRenterYealryHistory(int aptID,Date startDate,Date endDate) "
							+ aptID + "and " + startDate + "  to" + endDate);
			String sqlDue = " SELECT renter_details.apt_id,renter_full_name,is_current_renter,agg_start_date,agg_end_date FROM renter_details,apartment_details "
					+ " WHERE apartment_details.apt_id=renter_details.apt_id AND  renter_details.apt_id="
					+ aptID + " ";

			Map hmap = new HashMap();
			hmap.put("startDate", startDate);
			hmap.put("endDate", endDate);

			RowMapper rMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int num) throws SQLException {
					// TODO Auto-generated method stub
					RptMonthYearVO rptMYVO = new RptMonthYearVO();
					rptMYVO.setFullName(rs.getString("renter_full_name"));
					rptMYVO.setStartDate(rs.getDate("agg_start_date"));
					rptMYVO.setEndDate(rs.getDate("agg_end_date"));

					return rptMYVO;
				}
			};
			renterList = namedParameterJdbcTemplate
					.query(sqlDue, hmap, rMapper);

			logger.debug("Query renter Apt Id---" + aptID + "-Renter size--"
					+ renterList.size());
			logger
					.debug("Exit :public List getRenterYealryHistory(int aptID,Date startDate,Date endDate) ");
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception  " + ex);
		} catch (EmptyResultDataAccessException ex) {
			logger.debug("No data available for this apartment : ");
		} catch (Exception e) {
			logger.error("Exception : " + e);
		}
		return renterList;
	}
	
	public List getDocumentReport(int societyID, String buildingID) {

		try {
			logger
					.debug("Entry : public List getDocumentReport(int societyID,String buildingID))"
							+ societyID);
			String strWhereClause = null;
			if (buildingID.equalsIgnoreCase("0"))
				strWhereClause = " AND b.society_id=" +societyID;
			else
				strWhereClause = " AND b.society_id=" + societyID+"  AND b.building_id=" + buildingID + "";

			String sqlDue = " SELECT r.entity_id,m.member_id,b.building_id,b.building_name,a.apt_name,m.full_name, "
					+ " (CASE WHEN r.doc_id = '5'  THEN s.name ELSE 'Not Available' END) index2, "
					+ " (CASE WHEN r.doc_id = '19'  THEN s.name ELSE 'Not Available'  END) sale_deed,"
					+ " (CASE WHEN r.doc_id = '12' THEN s.name ELSE 'Not Available'  END) primary_form,"
					+ " (CASE WHEN r.doc_id = '13' THEN s.name ELSE 'Not Available'  END) associate_form,"
					+ " (CASE WHEN r.doc_id = '14' THEN s.name ELSE 'Not Available'  END) nomination,"
					+ " (CASE WHEN r.doc_id = '15' THEN s.name ELSE 'Not Available'  END) parking_letter,"
					+ " (CASE WHEN r.doc_id = '16' THEN s.name ELSE 'Not Available'  END) possesion_letter,"
					+ " (CASE WHEN r.doc_id = '18' THEN s.name ELSE 'Not Available'  END) share_certificate "
					+ " FROM (apartment_details a ,document_details d,building_details b, member_details m) "
					+ " LEFT JOIN documents_relation r ON d.doc_id =r.doc_id  "
					+ " LEFT JOIN status s ON s.id=r.status_id "					
					+ " WHERE  a.building_id=b.building_id AND a.apt_id=m.apt_id AND m.is_current!=0 AND m.type='P' "
					+ "AND (m.member_id=r.entity_id OR r.entity_id IS NULL) "+ strWhereClause+" GROUP BY a.apt_id ORDER BY seq_no;";

			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("buildingID", buildingID);

			RowMapper rMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int num) throws SQLException {
					// TODO Auto-generated method stub
					RptDocumentVO documentVO = new RptDocumentVO();
					documentVO.setMemberID(rs.getInt("member_id"));
					documentVO.setBuildingID(rs.getInt("building_id"));
					documentVO.setUnitName(rs.getString("building_name")
							+ " - " + rs.getString("apt_name"));
					documentVO.setMemberName(rs.getString("full_name"));
					documentVO.setIndex2(rs.getString("index2"));
					documentVO.setSaleDeed(rs.getString("sale_deed"));
					documentVO.setPrimaryForm(rs.getString("primary_form"));
					documentVO.setAssociateForm(rs.getString("associate_form"));
					documentVO.setNominationForm(rs.getString("nomination"));
					documentVO.setParkingLetter(rs.getString("parking_letter"));
					documentVO
							.setPossessioLetter(rs.getString("possesion_letter"));
					documentVO.setShareCertificate(rs
							.getString("share_certificate"));

					return documentVO;
				}
			};
			reportList = namedParameterJdbcTemplate
					.query(sqlDue, hMap, rMapper);

			logger.debug("Outside Query : " + reportList.size());
			logger
					.debug("Exit : public getDocumentReport(int societyID,String buildingID)");
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getDocumentReport : " + ex);
		} catch (IllegalArgumentException ex) {
			//ex.printStackTrace();
			reportList = null;
			logger.info("Exception in  : " + ex);
		}

		catch (Exception ex) {
			logger.error("Exception in getDocumentReport : " + ex);
		}
		return reportList;
	}
	
	public List getSingleDocumentReport(int societyID, int documentID) {

		try {
			logger.debug("Entry : public List getSingleDocumentReport(int societyID, int documentID) "
							+ societyID+documentID);
			String strWhereClause = null;
			
			String sqlDue = " SELECT m.unit_name,m.full_name,m.member_id,m.building_id,r.entity_id, r.id, d.doc_id ,d.document_name,d.type,IFNULL(r.id, 0) AS docMapID, IFNULL(r.status_id, 0) AS statusId,IFNULL(s.name, 'Not Available') AS statusName,r.status_description "
					+ " FROM (document_details d,members_info m)"
					+ " LEFT JOIN documents_relation r ON r.entity_id=m.member_id AND d.doc_id =r.doc_id "
					+ " LEFT JOIN status s ON s.id=r.status_id WHERE d.type='M' AND m.is_current!=0 AND m.type='P' AND d.doc_id=:documentID AND m.society_id=:societyID  "; 

			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("documentID", documentID);
			
			RowMapper rMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int num) throws SQLException {
					// TODO Auto-generated method stub
					RptDocumentVO documentVO = new RptDocumentVO();
					documentVO.setMemberID(rs.getInt("member_id"));
					documentVO.setDocumentMapID(rs.getInt("docMapID"));			
					documentVO.setUnitName(rs.getString("unit_name"));
					documentVO.setMemberName(rs.getString("full_name"));
					documentVO.setDocumentID(rs.getInt("doc_id"));
					documentVO.setDocumentName(rs.getString("document_name"));
					documentVO.setDocumentStatusID(rs.getInt("statusId"));
					documentVO.setDocumentStatusName(rs.getString("statusName"));
					documentVO.setDescriptionHistory(rs.getString("status_description"));
					documentVO.setBuildingID(rs.getInt("building_id"));
					return documentVO;
				}
			};
			reportList = namedParameterJdbcTemplate
					.query(sqlDue, hMap, rMapper);

			logger.debug("Outside Query : " + reportList.size());
			logger
					.debug("Exit :public List getSingleDocumentReport(int societyID, int documentID)");
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getSingleDocumentReport : " + ex);
		} catch (IllegalArgumentException ex) {
			//ex.printStackTrace();
			reportList = null;
			logger.info("Exception in  : " + ex);
		}

		catch (Exception ex) {
			logger.error("Exception in getSingleDocumentReport : " + ex);
		}
		return reportList;
	}
	
	public List getDocumentSummaryReport(int societyID, String buildingID, int documentID) {

		try {
			logger
					.debug("Entry : public List getDocumentSummaryReport(int societyID,String buildingID))"
							+ societyID);
			String strWhereClause = null;
			if (buildingID.equalsIgnoreCase("0"))
				strWhereClause = " ";
			else
				strWhereClause = "  AND r.building_id=" + buildingID + "";

		
	
			String sqlDue = " SELECT  r.building_id,r.building_name,sold_apt,d.doc_id,d.document_name, "
					+ " SUM(CASE WHEN r.doc_id =:documentID  THEN 1 ELSE 0 END) totalDoc "					
					+ " FROM received_documents r,apt_count apt,document_details d "
					+ " WHERE r.building_id=apt.building_id AND r.doc_id =:documentID "
                    + " AND r.doc_id=d.doc_id  AND r.society_id =:societyID "
					+ strWhereClause + " GROUP BY r.building_id ";

			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("buildingID", buildingID);
			hMap.put("documentID", documentID);

			RowMapper rMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int num) throws SQLException {
					// TODO Auto-generated method stub
					RptDocumentVO documentVO = new RptDocumentVO();
					documentVO.setBuildingID(rs.getInt("building_id"));
					documentVO.setBuilidingName(rs.getString("building_name"));
					documentVO.setTotal(rs.getInt("sold_apt"));
					documentVO.setDocumentID(rs.getInt("doc_id"));
					documentVO.setDocumentName(rs.getString("document_name"));
					documentVO.setDocumentMapID(rs.getInt("totalDoc"));
					return documentVO;
				}
			};
			reportList = namedParameterJdbcTemplate
					.query(sqlDue, hMap, rMapper);

			logger.debug("Outside Query : " + reportList.size());
			logger
					.debug("Exit : public getDocumentSummaryReport(int societyID,String buildingID)");
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getDocumentSummaryReport : " + ex);
		} catch (IllegalArgumentException ex) {
			//ex.printStackTrace();
			reportList = null;
			logger.info("Exception in  : " + ex);
		}

		catch (Exception ex) {
			logger.error("Exception in getDocumentSummaryReport : " + ex);
		}
		return reportList;
	}
	
	public List getDocumentList() {

		try {
			logger.debug("Entry : public List getDocumentList() ");
			String strWhereClause = null;
			
			String sqlDue = " SELECT * FROM document_details  "; 

			Map hMap = new HashMap();
				
			RowMapper rMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int num) throws SQLException {
					// TODO Auto-generated method stub
					RptDocumentVO documentVO = new RptDocumentVO();
					documentVO.setDocumentID(rs.getInt("doc_id"));
					documentVO.setDocumentName(rs.getString("document_name"));
				    documentVO.setType(rs.getString("type"));
					return documentVO;
				}
			};
			reportList = namedParameterJdbcTemplate
					.query(sqlDue, hMap, rMapper);

			logger.debug("Outside Query : " + reportList.size());
			logger
					.debug("Exit :public List getDocumentList()");
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getDocumentList : " + ex);
		} catch (IllegalArgumentException ex) {
			//ex.printStackTrace();
			reportList = null;
			logger.info("Exception in getDocumentList : " + ex);
		}

		catch (Exception ex) {
			logger.error("Exception in getDocumentList : " + ex);
		}
		return reportList;
	}

	
	public List getDocumentList(String type) {

		try {
			logger.debug("Entry : public List getDocumentList(String type) ");
			String strWhereClause = null;
			if(type.equalsIgnoreCase("0")){
				strWhereClause="";
			}else{
				strWhereClause = " where type=:type";
			}
			
			String sqlDue = " SELECT * FROM document_details "+strWhereClause; 
			logger.debug("Query: "+sqlDue);
			
			Map hMap = new HashMap();
			hMap.put("type", type);
				
			RowMapper rMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int num) throws SQLException {
					// TODO Auto-generated method stub
					RptDocumentVO documentVO = new RptDocumentVO();
					documentVO.setDocumentID(rs.getInt("doc_id"));
					documentVO.setDocumentName(rs.getString("document_name"));
				    documentVO.setType(rs.getString("type"));
					return documentVO;
				}
			};
			reportList = namedParameterJdbcTemplate
					.query(sqlDue, hMap, rMapper);

			logger.debug("Outside Query : " + reportList.size());
			logger
					.debug("Exit :public List getDocumentList(String type)");
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getDocumentList : " + ex);
		} catch (IllegalArgumentException ex) {
			//ex.printStackTrace();
			reportList = null;
			logger.info("Exception in getDocumentList : " + ex);
		}

		catch (Exception ex) {
			logger.error("Exception in getDocumentList : " + ex);
		}
		return reportList;
	}

	public List getDocumentStatusList(int documentID) {

		try {
			logger.debug("Entry : public List getDocumentStatusList(int documentID) ");
			String strWhereClause = null;
			if(documentID==0){
				strWhereClause="";
			}else{
				strWhereClause=",document_details d ,document_status_relation dr WHERE s.id=dr.status_id AND d.doc_id=dr.document_id AND dr.document_id=:documentID";
			}
						
			String sqlDue = " SELECT s.* FROM status s "+strWhereClause; 
			logger.debug("Query: "+sqlDue);
			
			Map hMap = new HashMap();
			hMap.put("documentID", documentID);
			
			RowMapper rMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int num) throws SQLException {
					// TODO Auto-generated method stub
					RptDocumentVO documentVO = new RptDocumentVO();
					documentVO.setDocumentStatusID(rs.getInt("id"));
					documentVO.setDocumentStatusName(rs.getString("name"));
				    
					return documentVO;
				}
			};
			reportList = namedParameterJdbcTemplate
					.query(sqlDue, hMap, rMapper);

			logger.debug("Outside Query : " + reportList.size());
			logger
					.debug("Exit :public List getDocumentStatusList(int documentID)");
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getDocumentStatusList : " + ex);
		} catch (IllegalArgumentException ex) {
			//ex.printStackTrace();
			reportList = null;
			logger.info("Exception in getDocumentStatusList : " + ex);
		}

		catch (Exception ex) {
			logger.error("Exception in getDocumentStatusList : " + ex);
		}
		return reportList;
	}


	public List getVehicleReport(int societyID, String buildingID) {

		try {
			logger
					.debug("Entry : public List getVehicleReport(int societyID,String buildingID))"
							+ societyID);
			String strWhereClause = null;
			if (buildingID.equalsIgnoreCase("0"))
				strWhereClause = " ";
			else
				strWhereClause = "  AND b.building_id=" + buildingID + "";

			String sqlDue = " SELECT v.member_id,v.vehicle_id,b.building_id,CONCAT(building_name,' - ',apt_name)AS unitName,full_name,sticker_id,vehicle_type,CONCAT(reg_number,' ',vehicle_number)AS vehNumber,vehicle_description,commercial,status_code"
					+ " FROM vehicle_details v,apartment_details a, building_details b,member_details m "
					+ " WHERE b.building_id=a.building_id AND a.apt_id=m.apt_id AND m.member_id=v.member_id AND m.is_current!=0  "
					+ strWhereClause
					+ " AND v.soc_id=:societyID ORDER BY seq_no  ";

			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("buildingID", buildingID);

			RowMapper rMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int num) throws SQLException {
					// TODO Auto-generated method stub
					RptVehicleVO vehicleVO = new RptVehicleVO();
					vehicleVO.setBuildingID(rs.getInt("building_id"));
					vehicleVO.setUnitName(rs.getString("unitName"));
					vehicleVO.setMemberID(rs.getInt("member_id"));
					vehicleVO.setMemberName(rs.getString("full_name"));
					vehicleVO.setStickerId(rs.getString("sticker_id"));
					vehicleVO.setVehicleType(rs.getString("vehicle_type"));
					vehicleVO.setVehicleNumber(rs.getString("vehNumber"));
					vehicleVO.setDescription(rs
							.getString("vehicle_description"));
					vehicleVO.setCommercial(rs.getInt("commercial"));
					vehicleVO.setVehicleStatus(rs.getInt("status_code"));
					vehicleVO.setVehicleID(rs.getInt("vehicle_id"));
					return vehicleVO;
				}
			};
			reportList = namedParameterJdbcTemplate
					.query(sqlDue, hMap, rMapper);

			logger.debug("Outside Query : " + reportList.size());
			logger
					.debug("Exit : public getVehicleReport(int societyID,String buildingID)");
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getVehicleReport : " + ex);
		} catch (IllegalArgumentException ex) {
			//ex.printStackTrace();
			reportList = null;
			logger.info("Exception in getVehicleReport : " + ex);
		}

		catch (Exception ex) {
			logger.error("Exception in getVehicleReport : " + ex);
		}
		return reportList;
	}

	public List getVehicleReportSummary(int societyID, String buildingID) {

		try {
			logger
					.debug("Entry : public List getVehicleReportSummary(int societyID,String buildingID))"
							+ societyID);
			String strWhereClause = null;
			if (buildingID.equalsIgnoreCase("0"))
				strWhereClause = " ";
			else
				strWhereClause = "  AND b.building_id=" + buildingID + "";

			String sqlDue = " SELECT b.building_id,building_name,COUNT(DISTINCT v.member_id)AS memberCount,SUM(CASE WHEN sticker_id !='' THEN 1 ELSE 0 END) AS countStickers, "
					+ " SUM(CASE WHEN commercial = 1 THEN 1 ELSE 0 END) AS countCommercial, "
					+ " SUM(CASE WHEN vehicle_type = 0 THEN 1 ELSE 0 END) AS twoWheeler,"
					+ " SUM(CASE WHEN vehicle_type = 1 THEN 1 ELSE 0 END) AS fourWheeler "
					+ " FROM vehicle_details v,apartment_details a, building_details b,member_details m "
					+ " WHERE b.building_id=a.building_id AND a.apt_id=m.apt_id AND m.member_id=v.member_id "
					+ " AND v.soc_id=:societyID GROUP BY b.building_id ORDER BY building_id ";

			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("buildingID", buildingID);

			RowMapper rMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int num) throws SQLException {
					// TODO Auto-generated method stub
					RptVehicleVO vehicleVO = new RptVehicleVO();
					vehicleVO.setBuildingID(rs.getInt("building_id"));
					vehicleVO.setBuilidingName(rs.getString("building_name"));
					vehicleVO.setTotal(rs.getInt("memberCount"));
					vehicleVO.setStickerId(rs.getString("countStickers"));
					vehicleVO.setTwoWheeler(rs.getInt("twoWheeler"));
					vehicleVO.setFourwheeler(rs.getInt("fourWheeler"));
					vehicleVO.setCommercial(rs.getInt("countCommercial"));
					return vehicleVO;
				}
			};
			reportList = namedParameterJdbcTemplate
					.query(sqlDue, hMap, rMapper);

			logger.debug("Outside Query : " + reportList.size());
			logger
					.debug("Exit : public getVehicleReportSummary(int societyID,String buildingID)");
		} catch (BadSqlGrammarException ex) {
			logger.info("Exception in getVehicleReportSummary : " + ex);
		} catch (IllegalArgumentException ex) {
			//ex.printStackTrace();
			reportList = null;
			logger.info("Exception in getVehicleReportSummary  : " + ex);
		}

		catch (Exception ex) {
			logger.error("Exception in getVehicleReportSummary : " + ex);
		}
		return reportList;
	}
	public List getMonthWiseGroupReportForChart(int societyID,String fromDate,String uptoDate,String type)
 	{   
 	   List groupNameList=new ArrayList();
 		try
 		{
 			logger.debug("Entry : public List getMonthWiseGroupReportForChart(int intSocietyID)");
 			String strWhereClause = null;
			if (type.equalsIgnoreCase("R"))//Root
				strWhereClause = " GROUP BY  account_groups.root_id ORDER BY account_root_group.root_id ";
			else if(type.equalsIgnoreCase("G")){//Account Group
				strWhereClause = " GROUP BY  account_groups.id ORDER BY account_root_group.root_id,account_groups.id ";
			}
			SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			String sqlDue = "SELECT account_root_group.root_group_name,account_root_group.root_id, account_groups.group_name,SUM(al.opening_balance) AS opening_balance,account_groups.id AS groupID,"+
											" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'April' THEN ale.amount ELSE '0.00' END) AS Apr, "+
											" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'May' THEN ale.amount ELSE '0.00' END) AS May,"+
											" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'June' THEN ale.amount ELSE '0.00' END) AS Jun,"+
											" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'July' THEN ale.amount ELSE '0.00' END) AS Jul,"+
											" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'August' THEN ale.amount ELSE '0.00' END) AS Aug,"+
											" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'September' THEN ale.amount ELSE '0.00' END) AS Sept,"+
											" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'October' THEN ale.amount ELSE '0.00' END) AS Oct,"+
											" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'November' THEN ale.amount ELSE '0.00' END) AS Nov,"+
											"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'December' THEN ale.amount ELSE '0.00' END) AS Decs,"+
											"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'January' THEN ale.amount ELSE '0.00' END) AS Jan,"+
											"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'February' THEN ale.amount ELSE '0.00' END) AS Feb,"+
											"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'March' THEN ale.amount ELSE '0.00' END) AS Mar "+
											"  FROM account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al ,account_groups,account_root_group "+
											" WHERE ats.tx_date BETWEEN :fromDate AND :toDate AND ats.is_deleted=0  and ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID  "+
											" AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND al.sub_group_id = account_groups.id AND account_groups.root_id = account_root_group.root_id "+strWhereClause ;
			
			logger.debug("sql "+sqlDue);
			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate",uptoDate);

			RowMapper rMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int num) throws SQLException {
					// TODO Auto-generated method stub
					MonthwiseChartVO reportVO=new MonthwiseChartVO();
					reportVO.setRootGroupName(rs.getString("root_group_name"));
					reportVO.setRootGroupID(rs.getInt("root_id"));
					reportVO.setGroupName(rs.getString("group_name"));
					reportVO.setGroupID(rs.getInt("groupID"));
					reportVO.setJanAmt(rs.getBigDecimal("Jan"));
					reportVO.setFebAmt(rs.getBigDecimal("Feb"));
					reportVO.setMarAmt(rs.getBigDecimal("Mar"));
					reportVO.setAprAmt(rs.getBigDecimal("Apr"));
					reportVO.setMayAmt(rs.getBigDecimal("May"));
					reportVO.setJunAmt(rs.getBigDecimal("Jun"));
					reportVO.setJulAmt(rs.getBigDecimal("Jul"));
					reportVO.setAugAmt(rs.getBigDecimal("Aug"));
					reportVO.setSeptAmt(rs.getBigDecimal("Sept"));
					reportVO.setOctAmt(rs.getBigDecimal("Oct"));
					reportVO.setNovAmt(rs.getBigDecimal("Nov"));
					reportVO.setDecAmt(rs.getBigDecimal("Decs"));
					return reportVO;
				}
			};
			groupNameList = namedParameterJdbcTemplate
					.query(sqlDue, hMap, rMapper);
 		    
 		    logger.debug("Exit : public List getMonthWiseGroupReportForChart(int intSocietyID)");
 		}
 		catch(Exception ex)
 		{
 			//ex.printStackTrace();
 			logger.error("Exception in getMonthWiseGroupReportForChart : "+ex);
 		}
 	
 		return groupNameList;
 		
 	}
   
	public List getQuaterWiseGroupReportForChart(int societyID,String fromDate,String uptoDate,String type)
 	{   
 	   List groupNameList=new ArrayList();
 		try
 		{
 			logger.debug("Entry : public List getQuaterWiseGroupReportForChart(int intSocietyID)");
 			String strWhereClause = null;
			if (type.equalsIgnoreCase("R"))//Root
				strWhereClause = " GROUP BY  account_groups.root_id ORDER BY account_root_group.root_id ";
			else if(type.equalsIgnoreCase("G")){//Account Group
				strWhereClause = " GROUP BY  account_groups.id ORDER BY account_root_group.root_id,account_groups.id ";
			}
			SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			String sqlDue = "SELECT account_root_group.root_group_name,account_root_group.root_id, account_groups.group_name,account_groups.id AS groupID,SUM(al.opening_balance) AS opening_balance,"+
											" SUM(CASE WHEN QUARTER(ats.tx_date) = '2' THEN ale.amount ELSE '0.00' END) AS Q1, "+
											" SUM(CASE WHEN QUARTER(ats.tx_date) = '3' THEN ale.amount ELSE '0.00' END) AS  Q2,"+
											" SUM(CASE WHEN QUARTER(ats.tx_date) = '4' THEN ale.amount ELSE '0.00' END) AS  Q3,"+
											" SUM(CASE WHEN QUARTER(ats.tx_date) = '1' THEN ale.amount ELSE '0.00' END) AS  Q4 "+											
											"  FROM account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+"  al,account_groups,account_root_group "+
											" WHERE ats.tx_date BETWEEN :fromDate AND :toDate AND ats.is_deleted=0  and ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID "+
											" AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id "+ 
											" AND al.sub_group_id = account_groups.id AND account_groups.root_id = account_root_group.root_id "+strWhereClause ;
			logger.debug("sql "+sqlDue);
			Map hMap = new HashMap();
			hMap.put("societyID", societyID);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate",uptoDate);

			RowMapper rMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int num) throws SQLException {
					// TODO Auto-generated method stub
					MonthwiseChartVO reportVO=new MonthwiseChartVO();
					reportVO.setRootGroupName(rs.getString("root_group_name"));
					reportVO.setRootGroupID(rs.getInt("root_id"));
					reportVO.setGroupName(rs.getString("group_name"));
					reportVO.setGroupID(rs.getInt("groupID"));
					reportVO.setQ1Amt(rs.getBigDecimal("Q1"));
					reportVO.setQ2Amt(rs.getBigDecimal("Q2"));
					reportVO.setQ3Amt(rs.getBigDecimal("Q3"));
					reportVO.setQ4Amt(rs.getBigDecimal("Q4"));
					return reportVO;
				}
			};
			groupNameList = namedParameterJdbcTemplate
					.query(sqlDue, hMap, rMapper);
 		    
 		    logger.debug("Exit : public List getQuaterWiseGroupReportForChart(int intSocietyID)");
 		}
 		catch(Exception ex)
 		{
 			//ex.printStackTrace();
 			logger.error("Exception in getQuaterWiseGroupReportForChart : "+ex);
 		}
 	
 		return groupNameList;
 		
 	}
	  
	  
	  public List getMonthWiseReceiptPaymentGroupReportForChart(int societyID,String fromDate,String uptoDate,String type, int isConsolidated)
	 	{   
	 	   List groupNameList=new ArrayList();
	 		try
	 		{
	 			logger.debug("Entry : public List getMonthWiseReceiptPaymentGroupReportForChart(int intSocietyID)");
	 			String strWhereClause = "";
	 			String strGroupClause = "";
	 			String strColumn="";
	 			String strTxTypeClause=" ";
	 			
	 			String orgCondition="";
				String orgClause="";
				if(isConsolidated==0){
					orgCondition=" AND l.org_id=:orgID ";
					orgClause=" And al.org_id=:orgID ";
				}
	 			
				if (type.equalsIgnoreCase("R")){//Root
					strWhereClause = "  WHERE ( alg.sub_group_id!=4 and alg.sub_group_id!=5 and alg.sub_group_id!=95 ) GROUP BY txTable.type ORDER BY alg.group_name ";
				    strGroupClause=" table_secondary.type, ats.tx_type ";
				    strColumn="";
	 		     }else if(type.equalsIgnoreCase("G")){//Account Group
					
					strGroupClause=" table_secondary.ledger_id, ats.tx_type ";
					strColumn=" ifnull(SUM(AprNeg),0) AS AprNeg,ifnull(SUM(AprPos),0) AS AprPos,ifnull(SUM(MayNeg),0) AS MayNeg,ifnull(SUM(MayPos),0) AS MayPos,ifnull(SUM(JunNeg),0) AS JunNeg,ifnull(SUM(JunPos),0) AS JunPos,ifnull(SUM(JulNeg),0) AS JulNeg,ifnull(SUM(JulPos),0) AS JulPos,ifnull(SUM(AugNeg),0) AS AugNeg,ifnull(SUM(AugPos),0) AS AugPos,"+
                               " ifnull(SUM(SepNeg),0) AS SepNeg,ifnull(SUM(SepPos),0) AS SepPos, "+
                              " ifnull(SUM(OctNeg),0) AS OctNeg,ifnull(SUM(OctPos),0) AS OctPos,ifnull(SUM(NovNeg),0) AS NovNeg,ifnull(SUM(NovPos),0) AS NovPos,ifnull(SUM(DecNeg),0) AS DecNeg,ifnull(SUM(DecPos),0) AS DecPos,ifnull(SUM(JanNeg),0) AS JanNeg,ifnull(SUM(JanPos),0) AS JanPos,ifnull(SUM(FebNeg),0) AS FebNeg,ifnull(SUM(FebPos),0) AS FebPos,"+
                               " ifnull(SUM(MarNeg),0) AS MarNeg,ifnull(SUM(MarPos),0) AS MarPos,";
				}
				
				
				 SocietyVO societyVO=societyService.getSocietyDetails(societyID);
				String sqlDue = "SELECT alg.group_name,alg.sub_group_id AS groupID,txTable.tx_type,SUM(amount) AS txAmount, "+strColumn+" txTable.* FROM ( SELECT "+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'April' THEN ABS(CASE WHEN (ABS(table_primary.amount)>ABS(table_secondary.amount)) THEN ABS(table_secondary.amount) ELSE ABS(table_primary.amount) END) ELSE '0.00' END) AS Apr, "+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'May' THEN ABS(CASE WHEN (ABS(table_primary.amount)>ABS(table_secondary.amount)) THEN ABS(table_secondary.amount) ELSE ABS(table_primary.amount) END) ELSE '0.00' END) AS May,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'June' THEN ABS(CASE WHEN (ABS(table_primary.amount)>ABS(table_secondary.amount)) THEN ABS(table_secondary.amount) ELSE ABS(table_primary.amount) END) ELSE '0.00' END) AS Jun,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'July' THEN ABS(CASE WHEN (ABS(table_primary.amount)>ABS(table_secondary.amount)) THEN ABS(table_secondary.amount) ELSE ABS(table_primary.amount) END) ELSE '0.00' END) AS Jul, "+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'August' THEN ABS(CASE WHEN (ABS(table_primary.amount)>ABS(table_secondary.amount)) THEN ABS(table_secondary.amount) ELSE ABS(table_primary.amount) END) ELSE '0.00' END) AS Aug ,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'September' THEN ABS(CASE WHEN (ABS(table_primary.amount)>ABS(table_secondary.amount)) THEN ABS(table_secondary.amount) ELSE ABS(table_primary.amount) END) ELSE '0.00' END) AS Sept,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'October' THEN ABS(CASE WHEN (ABS(table_primary.amount)>ABS(table_secondary.amount)) THEN ABS(table_secondary.amount) ELSE ABS(table_primary.amount) END) ELSE '0.00' END) AS Oct,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'November' THEN ABS(CASE WHEN (ABS(table_primary.amount)>ABS(table_secondary.amount)) THEN ABS(table_secondary.amount) ELSE ABS(table_primary.amount) END) ELSE '0.00' END) AS Nov,"+
												"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'December' THEN ABS(CASE WHEN (ABS(table_primary.amount)>ABS(table_secondary.amount)) THEN ABS(table_secondary.amount) ELSE ABS(table_primary.amount) END) ELSE '0.00' END) AS Decs,"+
												"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'January' THEN ABS(CASE WHEN (ABS(table_primary.amount)>ABS(table_secondary.amount)) THEN ABS(table_secondary.amount) ELSE ABS(table_primary.amount) END) ELSE '0.00' END) AS Jan,"+
												"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'February' THEN ABS(CASE WHEN (ABS(table_primary.amount)>ABS(table_secondary.amount)) THEN ABS(table_secondary.amount) ELSE ABS(table_primary.amount) END) ELSE '0.00' END) AS Feb,"+
												"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'March' THEN ABS(CASE WHEN (ABS(table_primary.amount)>ABS(table_secondary.amount)) THEN ABS(table_secondary.amount) ELSE ABS(table_primary.amount) END) ELSE '0.00' END) AS Mar, "+
												"  ats.tx_id AS TXID,ats.tx_date,ats.ref_no,ats.create_date,ats.bank_date,ats.description,ats.tx_number,ats.tx_type,tx_mode,ats.created_by,ats.updated_by,SUM(table_secondary.amount) AS amount, table_primary.type, table_primary.tx_id,table_secondary.ledger_id,a.ledger_name ,sub_group_id "+
												" FROM account_ledger_entry_"+societyVO.getDataZoneID()+" table_primary,account_ledger_entry_"+societyVO.getDataZoneID()+" table_secondary,account_ledgers_"+societyVO.getDataZoneID()+" a,account_transactions_"+societyVO.getDataZoneID()+" ats "+
												" WHERE table_primary.tx_id = table_secondary.tx_id  AND table_primary.ledger_id=a.id  "+
												" AND table_primary.type!=table_secondary.type  AND (ats.tx_type!='Contra' AND ats.tx_type!='Journal' AND ats.tx_type!='Sales' AND ats.tx_type!='Purchase') "+
												" AND (ats.tx_type!='Contra' or ats.tx_type!='Journal') AND ats.org_id=a.org_id and a.org_id=table_primary.org_id and table_primary.org_id=table_secondary.org_id "+orgCondition+
												" AND ats.tx_id = table_primary.tx_id AND (tx_date BETWEEN :fromDate AND :toDate) AND ats.is_deleted=0  GROUP BY "+strGroupClause+" ORDER BY ats.tx_date ) as txTable "+ 
												" LEFT JOIN (SELECT al.id AS ledger_id,al.sub_group_id,al.ledger_name,ag.group_name FROM account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups ag WHERE "+orgClause+" al.sub_group_id=ag.id) AS alg ON txTable.ledger_id=alg.ledger_id  "+strWhereClause ;
			
				
				String sql="SELECT SUM(ABS(tx_negative)) AS txNegative,SUM(ABS(tx_positive)) AS txPositive,SUM(tx_sum) AS txSum,"+strColumn+" tabl.* fROM(SELECT "+
					     "CASE WHEN MONTHNAME(ats.tx_date) = 'April' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS AprNeg, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'April' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS AprPos, "+												
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'May' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS MayNeg, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'May' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS MayPos, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'June' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS JunNeg, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'June' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS JunPos,"+												
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'July' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS JulNeg,"+ 
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'July' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS JulPos, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'August' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS AugNeg,"+ 
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'August' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS AugPos, "+												
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'September' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS SepNeg, "+ 
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'September' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS SepPos, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'October' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS OctNeg, "+ 
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'October' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS OctPos, "+													
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'November' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS NovNeg, "+ 
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'November' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS NovPos, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'December' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS DecNeg, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'December' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS DecPos, "+												
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'January' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS JanNeg, "+ 
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'January' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS JanPos, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'February' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS FebNeg, "+ 
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'February' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS FebPos, "+												
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'March' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS MarNeg, "+ 
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'March' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS MarPos,"+
						" root_id,g.id AS groupID,g.group_name,g.category_id,ale.type,ale.tx_id,ats.tx_type, al.id AS tx_ledger_id,al.ledger_name "+
						" ,(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END) AS tx_negative, (CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END) AS tx_positive, (SUM( ale.amount)) AS tx_sum FROM (SELECT a.* FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_ledgers_"+societyVO.getDataZoneID()+" l ,account_groups g"+
					    " WHERE a.is_deleted=0 AND a.tx_id=al.tx_id AND a.org_id=l.org_id "+orgCondition+" AND a.tx_date BETWEEN :fromDate AND :toDate AND al.ledger_id=l.id and l.sub_group_id=g.id AND a.tx_type!='Contra' AND (g.category_id=3 OR g.category_id=101 OR g.category_id=64 OR g.category_id=65 )) AS ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups g WHERE ats.tx_date "+
	                    " BETWEEN :fromDate AND :toDate AND ats.is_deleted = 0 AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND ats.org_id=ale.org_id AND ale.org_id=al.org_id "+orgClause+ 
	                    " AND al.sub_group_id = g.id AND  ats.tx_type!='Contra'  GROUP BY ale.id) AS tabl WHERE  tabl.category_id!=3 AND tabl.category_id!=101 AND tabl.category_id!=64 AND tabl.category_id!=65 GROUP BY tabl.groupID,TYPE ORDER BY group_name;";
				
				
				
				logger.debug("Query: "+sqlDue);
				Map hMap = new HashMap();
				hMap.put("orgID", societyID);
				hMap.put("fromDate", fromDate);
				hMap.put("toDate",uptoDate);

				RowMapper rMapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int num) throws SQLException {
						// TODO Auto-generated method stub
						MonthwiseChartVO reportVO=new MonthwiseChartVO();
						reportVO.setTransactionType(rs.getString("tx_type"));
						reportVO.setRootGroupID(rs.getInt("root_id"));
						reportVO.setTxType(rs.getString("type"));
						reportVO.setGroupName(rs.getString("group_name"));
						reportVO.setGroupID(rs.getInt("groupID"));
						reportVO.setJanCr(rs.getBigDecimal("JanNeg").setScale(2, RoundingMode.HALF_UP));
						reportVO.setFebCr(rs.getBigDecimal("FebNeg").setScale(2, RoundingMode.HALF_UP));
						reportVO.setMarCr(rs.getBigDecimal("MarNeg").setScale(2, RoundingMode.HALF_UP));
						reportVO.setAprCr(rs.getBigDecimal("AprNeg").setScale(2, RoundingMode.HALF_UP));
						reportVO.setMayCr(rs.getBigDecimal("MayNeg").setScale(2, RoundingMode.HALF_UP));
						reportVO.setJunCr(rs.getBigDecimal("JunNeg").setScale(2, RoundingMode.HALF_UP));
						reportVO.setJulCr(rs.getBigDecimal("JulNeg").setScale(2, RoundingMode.HALF_UP));
						reportVO.setAugCr(rs.getBigDecimal("AugNeg").setScale(2, RoundingMode.HALF_UP));
						reportVO.setSeptCr(rs.getBigDecimal("SepNeg").setScale(2, RoundingMode.HALF_UP));
						reportVO.setOctCr(rs.getBigDecimal("OctNeg").setScale(2, RoundingMode.HALF_UP));
						reportVO.setNovCr(rs.getBigDecimal("NovNeg").setScale(2, RoundingMode.HALF_UP));
						reportVO.setDecCr(rs.getBigDecimal("DecNeg").setScale(2, RoundingMode.HALF_UP));
						reportVO.setJanDb(rs.getBigDecimal("JanPos").setScale(2, RoundingMode.HALF_UP));
						reportVO.setFebDb(rs.getBigDecimal("FebPos").setScale(2, RoundingMode.HALF_UP));
						reportVO.setMarDb(rs.getBigDecimal("MarPos").setScale(2, RoundingMode.HALF_UP));
						reportVO.setAprDb(rs.getBigDecimal("AprPos").setScale(2, RoundingMode.HALF_UP));
						reportVO.setMayDb(rs.getBigDecimal("MayPos").setScale(2, RoundingMode.HALF_UP));
						reportVO.setJunDb(rs.getBigDecimal("JunPos").setScale(2, RoundingMode.HALF_UP));
						reportVO.setJulDb(rs.getBigDecimal("JulPos").setScale(2, RoundingMode.HALF_UP));
						reportVO.setAugDb(rs.getBigDecimal("AugPos").setScale(2, RoundingMode.HALF_UP));
						reportVO.setSeptDb(rs.getBigDecimal("SepPos").setScale(2, RoundingMode.HALF_UP));
						reportVO.setOctDb(rs.getBigDecimal("OctPos").setScale(2, RoundingMode.HALF_UP));
						reportVO.setNovDb(rs.getBigDecimal("NovPos").setScale(2, RoundingMode.HALF_UP));
						reportVO.setDecDb(rs.getBigDecimal("DecPos").setScale(2, RoundingMode.HALF_UP));
						return reportVO;
					}
				};
				groupNameList = namedParameterJdbcTemplate
						.query(sql, hMap, rMapper);
	 		    
	 		    logger.debug("Exit : public List getMonthWiseReceiptPaymentGroupReportForChart(int intSocietyID)");
	 		}
	 		catch(Exception ex)
	 		{
	 			//ex.printStackTrace();
	 			logger.error("Exception in getMonthWiseReceiptPaymentGroupReportForChart : "+ex);
	 		}
	 	
	 		return groupNameList;
	 		
	 	}
	  
	  
	  public List getQuaterWiseReceiptPaymentGroupReportForChart(int societyID,String fromDate,String uptoDate,String type)
	 	{   
	 	   List groupNameList=new ArrayList();
	 		try
	 		{
	 			logger.debug("Entry : public List getQuaterWiseReceiptPaymentGroupReportForChart(int intSocietyID)");
	 			String strWhereClause = "";
	 			String strGroupClause = "";
	 			String strColumn="";
	 			String strTxTypeClause="";
	 			
	 		String orgCondition=" AND l.org_id=:orgID ";
	 		String orgClause=" And al.org_id=:orgID ";
	 			
				if (type.equalsIgnoreCase("R")){//Root
					strWhereClause = " WHERE ( alg.sub_group_id!=4 AND alg.sub_group_id!=5 and alg.sub_group_id!=95) GROUP BY txTable.type ORDER BY alg.group_name ";
				    strGroupClause=" ats.tx_type ";
				    strColumn="";
	 		     }else if(type.equalsIgnoreCase("G")){//Account Group
					strWhereClause = " WHERE (alg.sub_group_id!=4 AND alg.sub_group_id!=5 and alg.sub_group_id!=95) GROUP BY  alg.sub_group_id,tx_type ORDER BY alg.group_name ";
					strGroupClause=" table_secondary.ledger_id, ats.tx_type ";
					strColumn=" ifnull(SUM(Q1Neg),0) AS Q1Neg,ifnull(SUM(Q1Pos),0) AS Q1Pos,ifnull(SUM(Q2Neg),0) AS Q2Neg,ifnull(SUM(Q2Pos),0) AS Q2Pos, ifnull(SUM(Q3Neg),0) AS Q3Neg,ifnull(SUM(Q3Pos),0) AS Q3Pos,ifnull(SUM(Q4Neg),0) AS Q4Neg,ifnull(SUM(Q4Pos),0) AS Q4Pos,";
				}
				
				//transaction type :Receipt or Payment							    
	 		   
	 		    SocietyVO societyVO=societyService.getSocietyDetails(societyID);
                 String sqlDue=" SELECT alg.group_name,alg.sub_group_id AS groupID, txTable.tx_type,SUM(amount) AS txAmount,"+strColumn+" txTable.* FROM ( SELECT "+
				                		       " SUM(CASE WHEN QUARTER(ats.tx_date) = '2' THEN ABS(CASE WHEN (ABS(table_primary.amount)>ABS(table_secondary.amount)) THEN ABS(table_secondary.amount) ELSE ABS(table_primary.amount) END) ELSE '0.00' END) AS Q1, "+
											   " SUM(CASE WHEN QUARTER(ats.tx_date) = '3' THEN ABS(CASE WHEN (ABS(table_primary.amount)>ABS(table_secondary.amount)) THEN ABS(table_secondary.amount) ELSE ABS(table_primary.amount) END) ELSE '0.00' END) AS  Q2,"+
											   " SUM(CASE WHEN QUARTER(ats.tx_date) = '4' THEN ABS(CASE WHEN (ABS(table_primary.amount)>ABS(table_secondary.amount)) THEN ABS(table_secondary.amount) ELSE ABS(table_primary.amount) END) ELSE '0.00' END) AS  Q3,"+
											   " SUM(CASE WHEN QUARTER(ats.tx_date) = '1' THEN ABS(CASE WHEN (ABS(table_primary.amount)>ABS(table_secondary.amount)) THEN ABS(table_secondary.amount) ELSE ABS(table_primary.amount) END) ELSE '0.00' END) AS  Q4, "+											
												"  ats.tx_id AS TXID,ats.tx_date,ats.ref_no,ats.create_date,ats.bank_date,ats.description,ats.tx_number,ats.tx_type,tx_mode,ats.created_by,ats.updated_by,SUM(table_secondary.amount) AS amount, table_primary.type, table_primary.tx_id,table_secondary.ledger_id,a.ledger_name ,sub_group_id "+
												" FROM account_ledger_entry_"+societyVO.getDataZoneID()+" table_primary,account_ledger_entry_"+societyVO.getDataZoneID()+" table_secondary,account_ledgers_"+societyVO.getDataZoneID()+" a,account_transactions_"+societyVO.getDataZoneID()+" ats "+
												" WHERE table_primary.tx_id = table_secondary.tx_id  AND table_primary.ledger_id=a.id  "+
												" AND table_primary.type!=table_secondary.type  AND (ats.tx_type!='Contra' AND ats.tx_type!='Journal' AND ats.tx_type!='Sales' AND ats.tx_type!='Purchase') "+
       											" AND (ats.tx_type!='Contra' or ats.tx_type!='Journal') AND ats.org_id=a.org_id and a.org_id=table_primary.org_id and table_primary.org_id=table_secondary.org_id and a.org_id="+societyID+" "+
												" AND ats.tx_id = table_primary.tx_id AND (tx_date BETWEEN :fromDate AND :toDate) AND ats.is_deleted=0  GROUP BY "+strGroupClause+" ORDER BY ats.tx_date ) as txTable "+ 
												" LEFT JOIN (SELECT al.id AS ledger_id,al.sub_group_id,al.ledger_name,ag.group_name FROM account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups ag WHERE al.org_id=:societyID and  al.sub_group_id=ag.id) AS alg ON txTable.ledger_id=alg.ledger_id  "+strWhereClause ;
				
                 String sql="SELECT SUM(ABS(tx_negative)) AS txNegative,SUM(ABS(tx_positive)) AS txPositive,SUM(tx_sum) AS txSum,"+strColumn+" tabl.* fROM(SELECT "+
					     "CASE WHEN QUARTER(ats.tx_date) = '2' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS Q1Neg, "+
						 " CASE WHEN QUARTER(ats.tx_date) = '2' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS Q1Pos, "+												
						 " CASE WHEN QUARTER(ats.tx_date) = '3' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS Q2Neg, "+
						 " CASE WHEN QUARTER(ats.tx_date) = '3' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS Q2Pos, "+
						 " CASE WHEN QUARTER(ats.tx_date) = '4' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS Q3Neg, "+
						 " CASE WHEN QUARTER(ats.tx_date) = '4' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS Q3Pos,"+												
						 " CASE WHEN QUARTER(ats.tx_date) = '1' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS Q4Neg,"+ 
						 " CASE WHEN QUARTER(ats.tx_date) = '1' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS Q4Pos, "+
						" root_id,g.id AS groupID,g.group_name,g.category_id,ale.type,ale.tx_id,ats.tx_type, al.id AS tx_ledger_id,al.ledger_name "+
						" ,(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END) AS tx_negative, (CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END) AS tx_positive, (SUM( ale.amount)) AS tx_sum FROM (SELECT a.* FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_ledgers_"+societyVO.getDataZoneID()+" l, account_groups g "+
					  " WHERE a.is_deleted=0 AND a.tx_id=al.tx_id AND a.org_id=l.org_id "+orgCondition+" AND a.tx_date BETWEEN :fromDate AND :toDate AND al.ledger_id=l.id AND l.sub_group_id=g.id and a.tx_type!='Contra' AND (g.category_id=3 OR g.category_id=101 or g.category_id=64 or g.category_id=65 )) AS ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups g WHERE ats.tx_date "+
		                 " BETWEEN :fromDate AND :toDate AND ats.is_deleted = 0 AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND ats.org_id=ale.org_id AND ale.org_id=al.org_id "+orgClause+ 
		                 " AND al.sub_group_id = g.id AND  ats.tx_type!='Contra' GROUP BY ale.id) AS tabl WHERE tabl.category_id!=3 AND tabl.category_id!=101 AND tabl.category_id!=64 AND tabl.category_id!=65 GROUP BY tabl.groupID,TYPE ORDER BY group_name;";
                 
				logger.debug("sql "+sqlDue);
				Map hMap = new HashMap();
				hMap.put("orgID", societyID);
				hMap.put("fromDate", fromDate);
				hMap.put("toDate",uptoDate);

				RowMapper rMapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int num) throws SQLException {
						// TODO Auto-generated method stub
						MonthwiseChartVO reportVO=new MonthwiseChartVO();
						reportVO.setTransactionType(rs.getString("tx_type"));
						reportVO.setRootGroupID(rs.getInt("root_id"));
						reportVO.setTxType(rs.getString("type"));
						reportVO.setGroupName(rs.getString("group_name"));
						reportVO.setGroupID(rs.getInt("groupID"));
						reportVO.setQ1Cr(rs.getBigDecimal("Q1Neg").setScale(2, RoundingMode.HALF_UP));
						reportVO.setQ2Cr(rs.getBigDecimal("Q2Neg").setScale(2, RoundingMode.HALF_UP));
						reportVO.setQ3Cr(rs.getBigDecimal("Q3Neg").setScale(2, RoundingMode.HALF_UP));
						reportVO.setQ4Cr(rs.getBigDecimal("Q4Neg").setScale(2, RoundingMode.HALF_UP));
						reportVO.setQ1Db(rs.getBigDecimal("Q1Pos").setScale(2, RoundingMode.HALF_UP));
						reportVO.setQ2Db(rs.getBigDecimal("Q2Pos").setScale(2, RoundingMode.HALF_UP));
						reportVO.setQ3Db(rs.getBigDecimal("Q3Pos").setScale(2, RoundingMode.HALF_UP));
						reportVO.setQ4Db(rs.getBigDecimal("Q4Pos").setScale(2, RoundingMode.HALF_UP));
						return reportVO;
					}
				};
				groupNameList = namedParameterJdbcTemplate
						.query(sql, hMap, rMapper);
	 		    
	 		    logger.debug("Exit : public List getQuaterWiseReceiptPaymentGroupReportForChart(int intSocietyID)");
	 		}
	 		catch(Exception ex)
	 		{
	 			//ex.printStackTrace();
	 			logger.error("Exception in getQuaterWiseReceiptPaymentGroupReportForChart : "+ex);
	 		}
	 	
	 		return groupNameList;
	 		
	 	}
	
	  public List getMonthWiseBreakUpReportForReceivables(int societyID,String fromDate,String uptoDate,String groupName)
	 	{   
	 	   List groupNameList=new ArrayList();
	 		try
	 		{
	 			logger.debug("Entry : public List getMonthWiseBreakUpReportForReceivables(int intSocietyID)");
	 			String strWhereClause = null;
	 			String strConditions=null;
	 			String strClause=null;
	 			if(groupName.equalsIgnoreCase("G"))// Grouped
	 			{
	 				strWhereClause="c.category_id ";
	 				strConditions="";
	 				strClause="category_id ";
	 			}else if(groupName.equalsIgnoreCase("I"))// Individual ledger wise
	 			{
	 				strWhereClause="al.id ";
	 				strConditions=" chart.ledgerID=op_balance.id AND ";
	 				strClause="ledger.id ";
	 			}
				SocietyVO societyVO=societyService.getSocietyDetails(societyID);
				String sqlDue = "SELECT op_balance.id,op_balance.ledger_name AS ledgerName,op_balance.sub_group_id,op_balance.category_id AS categoryID,ifnull(Aprs,0) as Apr,ifnull(Mays,0) as May,ifnull(Juns,0) as Jun,ifnull(Juls,0) as Jul,ifnull(Augs,0) as Aug,ifnull(Septs,0) as Sept, "
						+ " ifnull(Octs,0) as Oct,ifnull(Novs,0) as Nov, ifnull(Decs,0) as Decss, ifnull(Jans,0) as Jan, ifnull(Febs,0) as Feb, ifnull(Mars,0) as Mar , chart.*,op_balance.opening_balance FROM(  SELECT al.id AS ledgerID,al.ledger_name, account_root_group.root_group_name,account_root_group.root_id, account_groups.group_name,account_groups.id AS groupID, c.category_id ,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'April' THEN ale.amount ELSE '0.00' END) AS Aprs, "+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'May' THEN ale.amount ELSE '0.00' END) AS Mays,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'June' THEN ale.amount ELSE '0.00' END) AS Juns,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'July' THEN ale.amount ELSE '0.00' END) AS Juls,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'August' THEN ale.amount ELSE '0.00' END) AS Augs,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'September' THEN ale.amount ELSE '0.00' END) AS Septs,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'October' THEN ale.amount ELSE '0.00' END) AS Octs,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'November' THEN ale.amount ELSE '0.00' END) AS Novs,"+
												"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'December' THEN ale.amount ELSE '0.00' END) AS Decs,"+
												"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'January' THEN ale.amount ELSE '0.00' END) AS Jans,"+
												"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'February' THEN ale.amount ELSE '0.00' END) AS Febs,"+
												"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'March' THEN ale.amount ELSE '0.00' END) AS Mars "+
												"  FROM account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al ,account_groups,account_root_group ,account_category c "+
												" WHERE ats.tx_date BETWEEN :fromDate AND :toDate AND ats.is_deleted=0  and ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID  "+
												" AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND al.sub_group_id = account_groups.id AND account_groups.root_id = account_root_group.root_id AND account_groups.category_id=c.category_id and c.category_id=1 GROUP BY "+strWhereClause+"  ORDER BY al.id ) as chart right join "+
												 " (SELECT  ledger.id,ledger.ledger_name,SUM(ledger.opening_balance + iFNULL(s.closing_balance,0) ) AS opening_balance,ledger.sub_group_id,ledger.category_id FROM (SELECT al.id,ale.ledger_id,al.ledger_name,al.opening_balance ,sub_group_id, "+
												" IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount)) AS sumAmt, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount)) AS closing_balance, c.category_id "+
												" FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale ,account_groups g,account_category c  WHERE ats.org_id=ale.org_id AND ale.org_id=al.org_id AND al.org_id=:societyID AND ats.tx_id=ale.tx_id AND ats.tx_date < :fromDate AND al.sub_group_id=g.id AND g.category_id=c.category_id AND ats.is_deleted=0 AND ale.ledger_id = al.id and c.category_id=1 "+ 
												" GROUP BY al.id ) s  RIGHT JOIN (SELECT a.*,g.category_id FROM account_ledgers_"+societyVO.getDataZoneID()+" a,account_groups g  WHERE a.sub_group_id=g.id AND g.category_id=1 AND a.org_id=:societyID AND a.is_deleted=0) ledger ON s.id=ledger.id GROUP BY "+strClause+" ) AS op_balance on "+strConditions+"  chart.category_id=op_balance.category_id  ;" ;
				
				logger.debug("sql "+sqlDue);
				Map hMap = new HashMap();
				hMap.put("societyID", societyID);
				hMap.put("fromDate", fromDate);
				hMap.put("toDate",uptoDate);

				RowMapper rMapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int num) throws SQLException {
						// TODO Auto-generated method stub
						MonthwiseChartVO reportVO=new MonthwiseChartVO();
						reportVO.setLedgerName(rs.getString("ledgerName"));
						reportVO.setLedgerID(rs.getInt("id"));
						reportVO.setRootGroupName(rs.getString("root_group_name"));
						reportVO.setRootGroupID(rs.getInt("root_id"));
						reportVO.setGroupName(rs.getString("group_name"));
						reportVO.setGroupID(rs.getInt("sub_group_id"));
						reportVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
						reportVO.setJanAmt(rs.getBigDecimal("Jan").setScale(2, RoundingMode.HALF_UP));
						reportVO.setFebAmt(rs.getBigDecimal("Feb").setScale(2, RoundingMode.HALF_UP));
						reportVO.setMarAmt(rs.getBigDecimal("Mar").setScale(2, RoundingMode.HALF_UP));
						reportVO.setAprAmt(rs.getBigDecimal("Apr").setScale(2, RoundingMode.HALF_UP));
						reportVO.setMayAmt(rs.getBigDecimal("May").setScale(2, RoundingMode.HALF_UP));
						reportVO.setJunAmt(rs.getBigDecimal("Jun").setScale(2, RoundingMode.HALF_UP));
						reportVO.setJulAmt(rs.getBigDecimal("Jul").setScale(2, RoundingMode.HALF_UP));
						reportVO.setAugAmt(rs.getBigDecimal("Aug").setScale(2, RoundingMode.HALF_UP));
						reportVO.setSeptAmt(rs.getBigDecimal("Sept").setScale(2, RoundingMode.HALF_UP));
						reportVO.setOctAmt(rs.getBigDecimal("Oct").setScale(2, RoundingMode.HALF_UP));
						reportVO.setNovAmt(rs.getBigDecimal("Nov").setScale(2, RoundingMode.HALF_UP));
						reportVO.setDecAmt(rs.getBigDecimal("Decss").setScale(2, RoundingMode.HALF_UP));
						return reportVO;
					}
				};
				groupNameList = namedParameterJdbcTemplate
						.query(sqlDue, hMap, rMapper);
	 		    
	 		    logger.debug("Exit : public List getMonthWiseBreakUpReportForReceivables(int intSocietyID)");
	 		}
	 		catch(Exception ex)
	 		{
	 			//ex.printStackTrace();
	 			logger.error("Exception in getMonthWiseBreakUpReportForReceivables : "+ex);
	 		}
	 	
	 		return groupNameList;
	 		
	 	}
	  
	  public List getCategoryWiseSummary(int societyID, String fromDate,
				String uptoDate, String effectiveDate,String dayBeforeFromDate,int rootID,String categoryGroup) {

			List<AccountHeadVO> groupList = new ArrayList<AccountHeadVO>();
			try {
				String categoryCondition="";
				
				if(categoryGroup.equalsIgnoreCase("C")){
					categoryCondition="balance_sheet_current.category_id = balance_sheet_previous.category_id";
				}else if(categoryGroup.equalsIgnoreCase("G")){
					categoryCondition="balance_sheet_current.report_group_name = balance_sheet_previous.report_group_name";
				}
				
				logger
						.debug("Entry : List getTrialBalanceReport(int societyID,String fromDate, String uptoDate,String effectiveDate)");
				
				String query = "SELECT balance_sheet_current.id, balance_sheet_current.category_id,balance_sheet_current.root_id, balance_sheet_current.report_group_name,balance_sheet_current.report_category_name, balance_sheet_current.closing_balance AS current_balance, balance_sheet_previous.closing_balance AS previous_balance , balance_sheet_previous.seq_no "
					+ "FROM ("
					+ getCategoryWiseSummaryQuery(societyID, effectiveDate,
							dayBeforeFromDate,rootID,categoryGroup)
					+ ") AS balance_sheet_previous, ("
					+ getCategoryWiseSummaryQuery(societyID, effectiveDate,
							uptoDate,rootID,categoryGroup)
					+ ") AS balance_sheet_current"
					+ " WHERE "+categoryCondition+" order by balance_sheet_previous.seq_no";

				logger.debug(query);

				Map hMap = new HashMap();
				hMap.put("societyID", societyID);
				hMap.put("fromDate", fromDate);
				hMap.put("toDate", uptoDate);
				hMap.put("rootID", rootID);

				RowMapper Rmapper = new RowMapper() {
					String tempCategoryName="";
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {

						AccountHeadVO acHVO = new AccountHeadVO();
						acHVO.setAccountGroupID(rs.getInt("id"));
						acHVO.setCategoryID(rs.getInt("category_id"));
						acHVO.setRootID(rs.getInt("root_id"));
						acHVO.setOpeningBalance(rs
								.getBigDecimal("previous_balance"));
						acHVO
								.setClosingBalance(rs
										.getBigDecimal("current_balance"));
						acHVO.setStrAccountPrimaryHead(rs.getString(
								"report_group_name").trim());
						acHVO.setCategoryName(rs.getString("report_category_name"));
						return acHVO;
					}

				};
				groupList = namedParameterJdbcTemplate.query(query, hMap, Rmapper);

				logger
						.debug("Exit : List getTrialBalanceReport(int societyID,String fromDate, String uptoDate,String effectiveDate)"
								+ groupList.size());

			} catch (BadSqlGrammarException ex) {
				logger.info("Exception in Renter History : " + ex);
			} catch (EmptyResultDataAccessException ex) {
				logger.debug("No data available for this apartment : ");
			} catch (Exception e) {
				logger.error("Exception : " + e);
			}

			return groupList;
		}
	  
	  private String getCategoryWiseSummaryQuery(int societyID, String effectiveDate,
				String toDate,int rootID,String categoryGroup) {
			String orgClause="";
			String orgCondition="";
			String rootCondition="";
			String categoryCondition="";
			
			if(categoryGroup.equalsIgnoreCase("C")){
				categoryCondition="category_id";
			}else if(categoryGroup.equalsIgnoreCase("G")){
				categoryCondition="al.sub_group_id";
			}
			
			SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			String query = "SELECT account_groups.id,account_groups.category_id,account_groups.root_id,account_groups.group_name AS report_group_name ,account_category.category_name AS report_category_name, report_balance.opening_balance,report_balance.trans_sum, (report_balance.opening_balance + (report_balance.trans_sum))AS closing_balance,account_groups.seq_no as seq_no "
					+ "FROM (SELECT al.sub_group_id,report_balances.group_name,category_id,SUM(al.opening_balance) AS  opening_balance,IFNULL(SUM(report_balances.trans_sum),0) AS trans_sum FROM(SELECT al.id, al.sub_group_id,transaction_sum.group_name,g.category_id,"
					+ "SUM(al.opening_balance) AS  opening_balance,IFNULL(SUM(transaction_sum.tx_sum),0) AS trans_sum "
					+ "FROM ( account_ledgers_"
					+ societyVO.getDataZoneID()
					+ " al,account_groups g) LEFT JOIN(SELECT account_groups.id,account_groups.category_id,account_groups.group_name,al.id AS tx_ledger_id ,al.ledger_name, IFNULL((SUM( ale.amount)),0) AS tx_sum "
					+ "FROM account_transactions_"
					+ societyVO.getDataZoneID()
					+ " ats,account_ledger_entry_"
					+ societyVO.getDataZoneID()
					+ " ale ,account_ledgers_"
					+ societyVO.getDataZoneID()
					+ " al,account_groups "
					+ "WHERE ats.tx_date BETWEEN '"
					+ effectiveDate
					+ "' AND '"
					+ toDate
					+ "' AND ats.is_deleted = 0 "
					+" AND ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID "
					+ " AND ale.tx_id = ats.tx_id   AND ale.ledger_id = al.id "
					+ "AND al.sub_group_id = account_groups.id GROUP BY ale.ledger_id ) AS transaction_sum ON al.id = transaction_sum.tx_ledger_id WHERE al.org_id=:societyID AND al.sub_group_id=g.id "
					+ " GROUP BY al.id) AS report_balances,account_ledgers_"
					+ societyVO.getDataZoneID()
					+ " al  WHERE al.id=report_balances.id AND al.is_deleted=0 GROUP BY "+categoryCondition+") AS report_balance"
					+ " , account_groups ,account_category WHERE account_groups.id = report_balance.sub_group_id  AND (account_groups.root_id = :rootID) AND account_groups.category_id=account_category.category_id  ORDER BY account_groups.seq_no ";

		
			return query;

		}
	  
 NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	/**
	 * @param societyService the societyService to set
	 */
	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}

}
