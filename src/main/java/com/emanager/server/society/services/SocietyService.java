package com.emanager.server.society.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.society.domainObject.SocietyDomain;
import com.emanager.server.society.valueObject.ApartmentVO;
import com.emanager.server.society.valueObject.BankInfoVO;
import com.emanager.server.society.valueObject.BookingVO;
import com.emanager.server.society.valueObject.InsertMemberVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;

public class SocietyService {
	SocietyDomain societyDomain;
	TransactionService transactionService;
	LedgerService ledgerService;
	private static final Logger logger = Logger.getLogger(SocietyService.class);
	DateUtility dateUtil=new DateUtility();
	
	public int insertSociety(SocietyVO societyVO){
		int flag=0;
		
		try{
		logger.debug("Entry : public int insertSociety(SocietyVO societyVO)");
		
		flag=societyDomain.insertSociety(societyVO);
		
		if(flag>2){
			logger.info("Society '"+societyVO.getSocietyName()+"' has been added sucessfully.");
		}
		logger.debug("Exit : public int insertSociety(SocietyVO societyVO)");	
		}catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in insertSociety : "+Ex);
		}
	return flag;

    }

	public List searchSocietyList(){

		List societyList = null;
		try{	
		logger.debug("Entry : public List searchSocietyList()" );

		societyList=societyDomain.searchSocietyList();
		
		logger.debug("Exit : public List searchSocietyList()");
		}catch(Exception Ex){
			logger.error("Exception in searchSocietyList : "+Ex);
		}
		return societyList;
		
		
	}
	
public List searchSocietyListForBuilder(String builderID){
		
		List societyList = null;
	    try{
		logger.debug("Entry : public List searchSocietyListForBuilder" );

		societyList=societyDomain.searchSocietyListForBuilder(builderID);
		
		logger.debug("Exit : public List searchSocietyListForBuilder");
	    }catch(Exception Ex){
			logger.error("Exception in searchSocietyListForBuilder : "+Ex);
		}
		return societyList;
		
		
	}
    
	public int insertBuildingNames(List listOfBuilding,int societyID){

		int flag=0;
		try{		
		logger.debug("Entry : public int insertBuildingNames(List listOfBuilding,int societyID)" );

		flag=societyDomain.insertBuildingNames(listOfBuilding, societyID);
		
		if(flag==1){
			logger.info("Society ID'"+societyID+"' has '"+listOfBuilding.size()+"' building added sucessfully.");
		}
		
		}catch(Exception Ex){
			logger.error("Exception in insertBuildingNames : "+Ex);
		}
		logger.debug("Exit public int insertBuildingNames(List listOfBuilding,int societyID)");
		
		return flag;
		
		
	}
	
	public int importMemberData(int orgID){

		int flag=0;
		try{		
		logger.debug("Entry : public int importMemberData(int societyID)" );

		flag=societyDomain.importMemberData( orgID);
		
	
		}catch(Exception Ex){
			logger.error("Exception in importMemberData : "+Ex);
		}
		logger.debug("Exit public int importMemberData(int societyID)");
		
		return flag;
		
		
	}
	
     public int deleteBuilding(int buildingID){
		
		int sucess = 0;
		try{		
		logger.debug("Entry : public int deleteBuilding(int buildingID)");
		
		sucess = societyDomain.deleteBuilding(buildingID);
				
		logger.debug("Exit : public int deleteBuilding(int buildingID)");

		}catch(Exception Ex){			
			logger.error("Exception in deleteBuilding : "+Ex);
		}
		return sucess;
	}
     
     public List searchApartmentsList(String buildingID,int societyID) {

 		List apartmentMemberList = null;
        try{
 		logger.debug("Entry public List searchApartmentsList(String buildingID)");

 		apartmentMemberList = societyDomain.searchApartmentsList(buildingID,societyID);

 		logger.debug("Exit :  public List searchApartmentsList(String buildingID)");
        }catch(Exception Ex){			
			logger.error("Exception in searchApartmentsList : "+Ex);
		}
 		return apartmentMemberList;

 	}

     public List searchApartmentsListDropDown(String buildingID){

 		List memberList = null;
 		
 		String strSQL = "";
 		try{	
 		logger.debug("Entry : public List searchApartmentsListDropDown(String buildingID)");
 		//1. SQL Query
 		
 		
 		        memberList = societyDomain.searchApartmentsListDropDown(buildingID);
 		        
 		      
 		        logger.debug("Exit : public List searchApartmentsListDropDown(String buildingID)");
 		}catch (Exception ex){
 			logger.error("Exception in searchApartmentsListDropDown : "+ex);
 			
 		}
 		return memberList;
 		
 		
 	}
 	public int insertApartmentNames(List listOfApartment, int buildingID) {

 		int flag = 0;
         try{
 		logger.debug("Entry : public int insertApartNames(List listOfApartment,String buildingID)");

 		flag = societyDomain.insertApartmentNames(listOfApartment, buildingID);
 		
 		if(flag==1){
			logger.info("Building ID'"+buildingID+"' has '"+listOfApartment.size()+"' apartment added sucessfully.");
		}
 		
        }catch(Exception Ex){			
 			logger.error("Exception in insertApartmentNames : "+Ex);
 		}
 		logger.debug("Exit : public int insertApartNames(List listOfApartment,String buildingID)");

 		return flag;

 	}

 	public int updateMember(MemberVO insertMemberVO, int apartmentID){
 		int insertFlag = 0;
 		try {
 			logger.debug("Entry : public int updateMember(InsertMemberVO insertMemberVO,int apartmentID)");

 			insertFlag = societyDomain.updateMember(insertMemberVO, apartmentID);
 			
 			if(insertFlag==1){
 				logger.info("Apartment ID '"+apartmentID+"' has member details updated sucessfully.");
 			}
 		}catch (Exception Ex) {
 			Ex.printStackTrace();
 			logger.error("Exception in updateIMember : "+Ex);

 		}
 		logger.debug("Exit : public int updateMember(InsertMemberVO insertMemberVO,int apartmentID)");
 		return insertFlag;
 	}

 	public int insertNewMember(MemberVO insertMemberVO, int apartmentID) {
 		int insertFlag = 0;
 		int generatedID=0;
 		int ledgerID=0;
 		try {
 			logger.debug("Entry : public int insertNewMember(InsertMemberVO insMemVO,int apartmentID)");

 			generatedID = societyDomain.insertNewMember(insertMemberVO, apartmentID);
 			
 			if((generatedID!=0)&&(generatedID>2)&& (insertMemberVO.getType().equalsIgnoreCase("P"))){
 			
 			AccountHeadVO achVO=new AccountHeadVO();
 			achVO.setOpeningBalance(BigDecimal.ZERO);
 			achVO.setLedgerType("C");
 			achVO.setAccountGroupID(1);
 			achVO.setOrgID(insertMemberVO.getSocietyID());
 			achVO.setStrAccountHeadName(insertMemberVO.getFlatNo()+" - "+insertMemberVO.getTitle()+" "+insertMemberVO.getFullName());
 			achVO.setEffectiveDate(insertMemberVO.getEffectiveDate());
 			achVO.setCreateDate(dateUtil.findCurrentDate().toString());
 			achVO.setAccType("Regular");
 			
 			
 			achVO=ledgerService.insertLedger(achVO, insertMemberVO.getSocietyID());
 			
 			if((generatedID!=0)&&(generatedID>2)&&(achVO.getStatusCode()==200)){
 				insertFlag=societyDomain.insertLedgerUserMapper(generatedID, achVO.getLedgerID(), "M", insertMemberVO.getSocietyID());
 			}
 			
 			}
 			
 			if(generatedID!=0&&(generatedID>2)){
 				logger.info("Apartment ID '"+apartmentID+"' has member details added sucessfully.");
 				
 			}
 			
 			insertFlag=generatedID;
 			
 		} catch (Exception Ex) {
 			Ex.printStackTrace();
 			logger.error("Exception in insertNewMember : "+Ex);
 		}
 		logger.debug("Exit : public int insertNewMember(InsertMemberVO insertMemberVO,int apartmentID)");
 		return insertFlag;
 	}

 	public int deleteApartment(int apartmentID) {

 		int sucess = 0;
 		try {
 			logger.debug("Entry : public int deleteApartment(int apartmentID)");

 			sucess = societyDomain.deleteApartment(apartmentID);

 			logger.debug("Exit : public int deleteApartment(int apartmentID)");

 		}catch (Exception Ex) {
 			logger.error("Exception in deleteApartment : "+Ex);
 		}
 		return sucess;
 	}

 	public int checkAvailabilty(int aptID) {
 		logger.debug("Entry : public int checkAvailabilty(int aptID)");
 		int flag = 0;

 		flag = societyDomain.checkAvailabilty(aptID);
 		logger.debug("Exit : public int checkAvailabilty(int aptID)");
 		return flag;
 	}

 	public int updateApartmentDetails(ApartmentVO aptVO, int apartmentID) {

 		int flag = 0;
        try{
 		logger.debug("Entry : public int updateApartmentDetails(ApartmentVO aptVO,int apartmentID)");

 		flag = societyDomain.updateApartmentDetails(aptVO, apartmentID);
 		
 		if(flag==1){
			logger.info("Apartment ID '"+apartmentID+"' has been updated sucessfully.");
 		} 
 		logger.debug("Exit : public int updateApartmentDetails(ApartmentVO aptVO,int apartmentID)");
        }catch (Exception Ex) {
 			logger.error("Exception in updateApartmentDetails : "+Ex);
 		}
 		return flag;

 	}
 	
 	public InsertMemberVO getAssociateMember(int aptID,String type){

		InsertMemberVO memberVO = null;
		try{	
		logger.debug("Entry InsertMemberVO getAssociateMember(int aptID)" );
		
	
		        memberVO = societyDomain.getAssociateMember(aptID,type);
		        
	
		       
		        logger.debug("Exit : public InsertMemberVO getAssociateMember(int aptID)");
		}catch (Exception ex){
			logger.error("Exception InsertMemberVO getAssociateMember(int aptID) : "+ex);
			
		}
		return memberVO;
		
		
	}
 	
 	
 	
 	
 	public int delMember(InsertMemberVO member,int apartmentID) {

 		int sucess = 0;
 		try {
 			logger.debug("Entry : public int delMember(InsertMemberVO member,int apartmentID)");

 			sucess = societyDomain.delMember(member,apartmentID);

 			logger.debug("Exit : public int delMember(InsertMemberVO member,int apartmentID)");

 		}catch (Exception Ex) {
 			logger.error("Exception in public int delMember(InsertMemberVO member,int apartmentID): "+Ex);
 		}
 		return sucess;
 	}
 	


 	public SocietyVO getSocietyDetails(String aptID,int societyID)
    {	
    	SocietyVO societyVO=new SocietyVO();
    	try
    	{
    		logger.debug("Entry : public SocietyVO getSocietyDetails(String aptID,int societyID)");
    		   		   		
    		
    		societyVO=societyDomain.getSocietyDetails(aptID,societyID);
    		logger.debug("Society Name: "+societyVO.getSocietyName());
       		
    	}
    	catch(Exception Ex){
    		logger.error("Exception in public SocietyVO getSocietyDetails(String aptID,int societyID)) : "+Ex);
    	}
    	logger.debug("Exit : public SocietyVO getSocietyDetails(String aptID)");
    return societyVO;	
    }
 	
    public BankInfoVO getBankDetails(int societyID)
    {	
    	BankInfoVO bankVO=new BankInfoVO();
    	try
    	{
    		logger.debug("Entry : public BankInfoVO getBankDetails(int societyID)");
    		   		   		
    		
    		bankVO=societyDomain.getBankDetails(societyID);
       		
    	}
    	catch(Exception Ex){
    		logger.error("Exception in public BankInfoVO getBankDetails(int societyID) : "+Ex);
    	}
    	logger.debug("Exit : public BankInfoVO getBankDetails(int societyID)"+bankVO.getMicrNo()+""+bankVO.getContact());
    return bankVO;	
    }

    
    public int insertBooking(BookingVO bookingVO)
	{	
		int insertFlag=0;
		try{
		
		logger.debug("Entry : public int insertBooking(BookingVO bookingVO)");
		
		
			
			insertFlag=societyDomain.insertBooking(bookingVO);
		
          			
		}
		
		catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in public int insertBooking(BookingVO bookingVO) : "+Ex);
		
		}
		logger.debug("Exit :public int insertBooking(BookingVO bookingVO)"+insertFlag);
	return insertFlag;	
	}
 	
    
    public List bookings(String memberID){

		List bookingList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry public List bookings(String memberID)" );
		//1. SQL Query
		
		        bookingList = societyDomain.bookings(memberID);
		        
	
		       
		        logger.debug("Exit :public List bookings(String memberID)"+bookingList.size());
		}catch (Exception ex){
			logger.error("Exception public List bookings(String memberID) : "+ex);
			
		}
		return bookingList;
		
		
	}
    
    public int checkAvailability(BookingVO bookingVO)
	{	
		int insertFlag=0;
		try{
		
		logger.debug("Entry : public int checkAvailability(BookingVO bookingVO)");
		
			
			
			
			insertFlag=societyDomain.checkAvailability(bookingVO);
		
          			
		}
		
		catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in public int checkAvailability(BookingVO bookingVO) : "+Ex);
		
		}
		logger.debug("Exit :public int checkAvailability(BookingVO bookingVO)"+insertFlag);
	return insertFlag;	
	}
    
    
    public List getUnitList(){

		List unitList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry public List getUnitList" );
		
	
		        unitList = societyDomain.getUnitList();
		        
	
		       
		        logger.debug("Exit : public List getUnitList()"+unitList.size());
		}catch (Exception ex){
			logger.error("Exception in getUnitList : "+ex);
			
		}
		return unitList;
		
		
	}
    
  public List getJRegisterList(int socID, int buildingId){
		
	    List memberList = null;
		try{
			logger.debug("Entry : public List getJRegisterList(String socID, String buildingId)");
	
			memberList = societyDomain.getJRegisterList(socID, buildingId);  
			
		 logger.debug("Exit : public List getJRegisterList(String socID, String buildingId)");
				
		}catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in getJRegisterList : "+Ex);
		}
		return memberList;
	}
    
  public List getIRegisterList(String aptID){
		
	    List memberList=null;
	   
	
		try{
			logger.debug("Entry :public List getIRegisterList(String aptID)");
		  
			memberList = societyDomain.getIRegisterList(aptID);
		
			
			
		 logger.debug("Exit :public List getIRegisterList(String aptID)");
				
		}catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in getIRegisterList : "+Ex);
		}
		return memberList;
	}

   public List getNomineeList(String memberId ){
		
	    List nomineeList=null;
	   
	
		try{
			logger.debug("Entry : public List getNomineeList(String memberId )");
		  
			nomineeList = societyDomain.getNomineeList(memberId);
		
			
			
		 logger.debug("Exit : public List getNomineeList(String memberId )");
				
		}catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in getNomineeList : "+Ex);
		}
		return nomineeList;
	} 
    
   public List getFlatHistory(String aptID ){
		
	    List memberHistoryList=null;	   
	
		try{
			logger.debug("Entry : public List getFlatHistory(String aptID )");
		  
			memberHistoryList = societyDomain.getFlatHistory(aptID);				
			
		 logger.debug("Exit : public List getFlatHistory(String aptID )");
				
		}catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in getFlatHistory : "+Ex);
		}
		return memberHistoryList;
	}
 
   public int getLatestMembershipNo(int societyID){
		int membershipNo=0;
		try
		{
			logger.debug("Entry :public int getLatestMembershipNo(int societyID)");
			
			
				membershipNo=societyDomain.getLatestMembershipNo(societyID);
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
			logger.error("Exception in getLatestMembershipNo : "+ex);
			
		}
			logger.debug("Exit : public int getLatestMembershipNo(int societyID)");
		return membershipNo;
		}
   
   public int getLatestNominationNo(int societyID){
		int nominationNo=0;
		try
		{
			logger.debug("Entry :public int getLatestNominationNo(int societyID)");
			
			
			nominationNo=societyDomain.getLatestNominationNo(societyID);
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
			logger.error("Exception in getLatestNominationNo : "+ex);
			
		}
			logger.debug("Exit : public int getLatestNominationNo(int societyID)");
		return nominationNo;
		}
  
   public int insertLedgerUserMapper(int userID,int ledgerID, String userType,int societyID)
	{	
		int insertFlag=0;
		try{
		
		logger.debug("Entry : public int insertLedgerUserMapper(int userID,int ledgerID, String userType)");
	
			insertFlag=societyDomain.insertLedgerUserMapper(userID, ledgerID, userType, societyID);        			
		}
		
		catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in public int insertLedgerUserMapper(int userID,int ledgerID, String userType) for SocietyID "+societyID+" MemberID "+userID+" and ledgerID "+ledgerID+" : "+Ex);
		
		}
		logger.debug("Exit :public int insertLedgerUserMapper(int userID,int ledgerID, String userType)"+insertFlag);
	return insertFlag;	
	}
   
   public List getSocietyList(){

		List societyList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry public List getSocietyList" );
		
		 societyList = societyDomain.getSocietyList(0);
		        
	
		       
		        logger.debug("Exit : public List getSocietyList()"+societyList.size());
		}catch (Exception ex){
			logger.error("Exception in getSocietyList : "+ex);
			
		}
		return societyList;
		
		
	}
   public List getSocietyList(int userID){

		List societyList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry : public List getSocietyList(int userID)" );
		
		 societyList = societyDomain.getSocietyList(userID);
		        
	
		       
		        logger.debug("Exit : public List getSocietyList(int userID)"+societyList.size());
		}catch (Exception ex){
			logger.error("Exception in getSocietyList : "+ex);
			
		}
		return societyList;
		
		
	}
   public List getSocietyListInDatazone(String dataZoneiD){

		List societyList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry : public List getSocietyListInDatazone(int dataZoneiD)" );
		
		 societyList = societyDomain.getSocietyListInDatazone(dataZoneiD);
		        
	
		       
		        logger.debug("Exit : public List getSocietyListInDatazone(int dataZoneiD)"+societyList.size());
		}catch (Exception ex){
			logger.error("Exception in public List getSocietyListInDatazone(int dataZoneiD) : "+ex);
			
		}
		return societyList;
		
		
	}
   
   public List getSocietyListForUser(int userID,int appID){

		List societyList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry public List getSocietyListForUser" );
		
		 societyList = societyDomain.getSocietyListForUser(userID, appID);
		        
	
		       
		        logger.debug("Exit : public List getSocietyListForUser()"+societyList.size());
		}catch (Exception ex){
			logger.error("Exception in getSocietyListForUser : "+ex);
			
		}
		return societyList;
		
		
	}
   
   public int updateLockDateForSociety(int societyID,String lockDate)
	{	
		int insertFlag=0;
		try{
		
		logger.debug("Entry :  public int updateLockDateForSociety(int societyID,String lockDate)");
		
			
			
			
			insertFlag=societyDomain.updateLockDateForSociety(societyID, lockDate);
		
         			
		}
		
		catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in  public int updateLockDateForSociety(int societyID,String lockDate) : "+Ex);
		
		}
		logger.debug("Exit : public int updateLockDateForSociety(int societyID,String lockDate)"+insertFlag);
	return insertFlag;	
	}
   
   public int updateLockDateForGSTTx(int societyID,String lockDate)
  	{	
  		int insertFlag=0;
  		try{
  		
  		logger.debug("Entry :  public int updateLockDateForGSTTx(int societyID,String lockDate)");
  		
  			
  			
  			
  			insertFlag=societyDomain.updateLockDateForGSTTx(societyID, lockDate);
  		
           			
  		}
  		
  		catch(Exception Ex){
  			Ex.printStackTrace();
  			logger.error("Exception in  public int updateLockDateForGSTTx(int societyID,String lockDate) : "+Ex);
  		
  		}
  		logger.debug("Exit : public int updateLockDateForGSTTx(int societyID,String lockDate)"+insertFlag);
  	return insertFlag;	
  	}
   
   public int updateLockDateForAuditTx(int societyID,String lockDate)
  	{	
  		int insertFlag=0;
  		try{
  		
  		logger.debug("Entry :  public int updateLockDateForAuditTx(int societyID,String lockDate)");
  		
  			
  			
  			
  			insertFlag=societyDomain.updateLockDateForAuditTx(societyID, lockDate);
  		
           			
  		}
  		
  		catch(Exception Ex){
  			Ex.printStackTrace();
  			logger.error("Exception in  public int updateLockDateForAuditTx(int societyID,String lockDate) : "+Ex);
  		
  		}
  		logger.debug("Exit : public int updateLockDateForAuditTx(int societyID,String lockDate)"+insertFlag);
  	return insertFlag;	
  	}
   
   
   public SocietyVO getSocietyDetails(int societyID)
   {	
	   SocietyVO societyVO=new SocietyVO();
   	try
   	{
   		logger.debug("Entry :  public SocietyVO getSocietyDetails(String societyID)");
   		   		   		
   		
   		societyVO=societyDomain.getSocietyDetails(societyID);
      		
   	}
   	catch(Exception Ex){
   		logger.error("Exception in  public SocietyVO getSocietyDetails(String societyID) : "+Ex);
   	}
   	logger.debug("Exit :  public SocietyVO getSocietyDetails(String societyID)");
   return societyVO;	
   }
   
   public SocietyVO getOrgDetailsByOrgName(String orgName)
   {	
   	SocietyVO societyVO=new SocietyVO();
   	try
   	{
   		logger.debug("Entry :     public SocietyVO getOrgDetailsByOrgName(String orgName)");

   		    		
   		societyVO=societyDomain.getOrgDetailsByOrgName(orgName);
   	
   	}
   	catch(Exception Ex){
   		logger.error("Exception in     public SocietyVO getOrgDetailsByOrgName(String orgName) : for SocietyNam "+orgName+" : "+Ex);
   	}
   	logger.debug("Exit :    public SocietyVO getOrgDetailsByOrgName(String orgName)");
   return societyVO;	
   }
   
  public List getSubscriptionExpiringOrgList(String appID,int period){	
   	   List orgList=null;
   	 
  	  	try
  	  	{
  	  		logger.debug("Entry : public List getSubscriptionExpiringOrgList(int appID,int periodID)");
  	  	 	  		   		
  	  		
  	  		orgList= societyDomain.getSubscriptionExpiringOrgList(appID,period);
  	     		
  	  	}
  	   	catch(Exception Ex){
  	  		logger.error("Exception in public List getSubscriptionExpiringOrgList(int appID,int periodID) : "+Ex);
  	  	}
  	  	logger.debug("Exit :  public List getSubscriptionExpiringOrgList(int appID,int periodID)");
  	 
      return orgList;	
      }
   
   public List getSubscriptionExpiredOrgList(int appID,int gracePeriod){	
   	   List orgList=null;
   	 
  	  	try
  	  	{
  	  		logger.debug("Entry : public List getSubscriptionExpiredOrgList(int appID)");
  	  	 	  		   		
  	  		
  	  		orgList= societyDomain.getSubscriptionExpiredOrgList(appID,gracePeriod);
  	     		
  	  	}
  	   	catch(Exception Ex){
  	  		logger.error("Exception in public List getSubscriptionExpiredOrgList(int appID) : "+Ex);
  	  	}
  	  	logger.debug("Exit :  public List getSubscriptionExpiredOrgList(int appID)");
  	 
      return orgList;	
      }
   
   
   public int restrictSubscriptionExpiredOrgs(){	
   	  int success=0;
   	 
  	  	try
  	  	{
  	  		logger.debug("Entry : public  int restrictSubscriptionExpiredOrgs()");
  	  	 	  		   		
  	  		
  	  		success= societyDomain.restrictSubscriptionExpiredOrgs();
  	     		
  	  	}
  	   	catch(Exception Ex){
  	  		logger.error("Exception in public  int restrictSubscriptionExpiredOrgs() : "+Ex);
  	  	}
  	  	logger.debug("Exit :  public  int restrictSubscriptionExpiredOrgs()");
  	 
      return success;	
      }
   
   public List getFinancialYearListForOrg(int orgID){	
	   	  List fyList=new ArrayList();
	   	 
	  	  	try
	  	  	{
	  	  		logger.debug("Entry : public List getFinancialYearListForOrg(int orgID)");
	  	  	 	  		   		
	  	  		
	  	  		fyList= societyDomain.getFinancialYearListForOrg(orgID);
	  	     		
	  	  	}
	  	   	catch(Exception Ex){
	  	  		logger.error("Exception in public List getFinancialYearListForOrg(int orgID): "+Ex);
	  	  	}
	  	  	logger.debug("Exit :  public List getFinancialYearListForOrg(int orgID)");
	  	 
	      return fyList;	
	}
   
   public SocietyVO getSocietyConfigurationDetails(int societyID)
   {	
	   SocietyVO societyVO=new SocietyVO();
   	try
   	{
   		logger.debug("Entry :  public SocietyVO getSocietyConfigurationDetails(int societyID)");
   		   		   		
   		
   		societyVO=societyDomain.getSocietyConfigurationDetails(societyID);
      		
   	}
   	catch(Exception Ex){
   		logger.error("Exception in  public SocietyVO getSocietyConfigurationDetails(int societyID) : "+Ex);
   	}
   	logger.debug("Exit :  public SocietyVO getSocietyConfigurationDetails(int societyID)");
   return societyVO;	
   }
    
	public SocietyDomain getSocietyDomain() {
		return societyDomain;
	}

	public void setSocietyDomain(SocietyDomain societyDomain) {
		this.societyDomain = societyDomain;
	}

	public TransactionService getTransactionService() {
		return transactionService;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	public void setLedgerService(LedgerService ledgerService) {
		this.ledgerService = ledgerService;
	}

	

	
}