package com.emanager.server.society.services;

import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.society.domainObject.EmployeeDomain;
import com.emanager.server.society.valueObject.EmergencyVO;
import com.emanager.server.society.valueObject.EmployeeVO;

public class EmployeeService {
		
	EmployeeDomain employeeDomain;
	private static final Logger logger = Logger.getLogger(EmployeeService.class);
	
public List getEmployeeList(String strSocietyID){
		
	    List employeeList = null;
		
		try{
		
		logger.debug("Entry : public List getEmployeeList(String strSocietyID)");
		
		employeeList = employeeDomain.getEmployeeList(strSocietyID);
				
		logger.debug("Exit : public List getEmployeeList(String strSocietyID)");
	
		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in getEmployeeList : "+Ex);
			
		}
		return employeeList;
}

public int addEmployeeDetails(EmployeeVO employeeVO)
{	int sucess=0;
	try
	{
	logger.debug("Entry : public int addEmployeeDetails(EmployeeVO employeeVO)");
	
	sucess=employeeDomain.addEmployeeDetails(employeeVO);
	
	if(sucess==1){
		logger.info("Employee Name '"+employeeVO.getFirstName()+" "+employeeVO.getLastName()+"' has been added sucessfully.");
	}
		
	}
	catch(Exception Ex)
	{
		Ex.printStackTrace();
		logger.error("Exception in addEmployeeDetails : "+Ex);
	
	}
	logger.debug("Exit : public int addEmployeeDetails(EmployeeVO employeeVO)");
	return sucess;	
}

public int updateEmployeeDetails(EmployeeVO employeeVO,String employeeID,String societyID)
{
	int sucess=0;
	try
	{
		logger.debug("Entry : public int updateEmployeeDetails(EmployeeVO employeeVO,String employeeID,String societyID)");
		sucess=employeeDomain.updateEmployeeDetails(employeeVO, employeeID,societyID);
		
		if(sucess==1){
			logger.info("Employee ID '"+employeeVO.getEmployeeId()+"' has been updated sucessfully.");
		}
			
	}
	catch(Exception ex)
	{
		logger.error("Exception in updateEmployeeDetails : "+ex);
		
	}
	logger.debug("Exit : public int updateEmployeeDetails(EmployeeVO employeeVO,String employeeID,String societyID)");
	return sucess;
}

public List getEmployeeSalaryList(String strSocietyID,int month,int year) {
	List employeeList=null;
		logger.debug("Entry : public List getEmployeeSalaryList(String strSocietyID)");
		
		employeeList = employeeDomain.getEmployeeSalaryList(strSocietyID, month, year);

		logger.debug("outside query : " + employeeList.size());
		logger.debug("Exit : public List getEmployeeSalaryList(String strSocietyID)");
	
	return employeeList;

}
public List getEmployeeWiseDetailsList(String employeeID,int month,int year) throws Exception{
	List employeeList=null;
	logger.debug("Entry : public List getEmployeeWiseDetailsList(String strSocietyID,int month,int year)");
	
	employeeList = employeeDomain.getEmployeeWiseDetailsList(employeeID, month, year);

	logger.debug("outside query : " + employeeList.size());
	logger.debug("Exit : public List getEmployeeWiseDetailsList(String strSocietyID,int month,int year)");

return employeeList;
}

public List searchEmployers(){
	
	List listEmployers = null;
	String strSQL = "";
	
	try{	
		logger.debug("Entry : public List searchEmployers()" );
	//1. SQL Query
	

	        listEmployers = employeeDomain.searchEmployers();
	        

	        logger.debug("outside query : " + listEmployers.size() );
	        logger.debug("Exit : public List searchEmployers");
	}catch (Exception ex){
		logger.error("Exception in searchEmployers : "+ex);
		
	}
	return listEmployers;
	
}
public List searchEmployersForUpdates(){
	
	List listEmployers = null;
	String strSQL = "";
	
	try{	
		logger.debug("Entry : public List searchEmployersForUpdates()" );
	//1. SQL Query
	

	        listEmployers = employeeDomain.searchEmployers();
	        

	        logger.debug("outside query : " + listEmployers.size() );
	        logger.debug("Exit : public List searchEmployersForUpdates()");
	}catch (Exception ex){
		logger.error("Exception in searchEmployersForUpdates() : "+ex);
		
	}
	return listEmployers;
	
}

public List getEmergencyNoList(String strSocietyID){
	
    List emergencyList = null;
	
	try{
	
	logger.debug("Entry : public List getEmergencyNoList(String strSocietyID)");
	
	emergencyList = employeeDomain.getEmergencyNoList(strSocietyID);
			
	logger.debug("Exit : public List getEmergencyNoList(String strSocietyID)");

	}catch(Exception Ex){
		
		Ex.printStackTrace();
		logger.error("Exception in getEmergencyNoList : "+Ex);
		
	}
	return emergencyList;
}

public int addEmergencyContact(EmergencyVO emergencyVO)
{	int sucess=0;
	try
	{
	logger.debug("Entry : public int addEmergencyContact(EmergencyVO emergencyVO)");
	
	sucess=employeeDomain.addEmergencyContact(emergencyVO);
	
	if(sucess==1){
		logger.info("Emergency Type-"+emergencyVO.getType()+" Contact No -"+emergencyVO.getMobile()+"' has been added sucessfully.");
	}
		
	}
	catch(Exception Ex)
	{
		Ex.printStackTrace();
		logger.error("Exception in addEmployeeDetails : "+Ex);
	
	}
	logger.debug("Exit :public int addEmergencyContact(EmergencyVO emergencyVO)");
	return sucess;	
}

public int deleteEmergencyDetails(String emergencyId){
	
	int sucess = 0;
	try{	
	logger.debug("Entry : public int deleteEmergencyDetails(String emergencyId)");
	
	sucess = employeeDomain.deleteEmergencyDetails(emergencyId);
			
	logger.debug("Exit : public int deleteEmergencyDetails(String emergencyId)");

	}catch(Exception Ex){		
		Ex.printStackTrace();
		logger.error("Exception in deleteEmergencyDetails : "+Ex);		
	}
	return sucess;
}


public EmployeeDomain getEmployeeDomain() {
	return employeeDomain;
}

public void setEmployeeDomain(EmployeeDomain employeeDomain) {
	this.employeeDomain = employeeDomain;
}
		
	

}
