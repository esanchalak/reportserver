package com.emanager.server.society.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;

import com.emanager.server.adminReports.valueObject.RptDocumentVO;
import com.emanager.server.commonUtils.valueObject.DropDownVO;
import com.emanager.server.society.domainObject.MemberDomain;
import com.emanager.server.society.valueObject.InsertMemberVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.MemberWSVO;
import com.emanager.server.society.valueObject.RenterVO;
import com.emanager.server.society.valueObject.VehicleVO;
import com.emanager.server.taskAndEventManagement.valueObject.EmailMessage;
import com.emanager.server.taskAndEventManagement.valueObject.NotificationVO;
import com.emanager.server.taskAndEventManagement.valueObject.SMSMessage;
import com.emanager.server.taskAndEventManagement.valueObject.TicketVO;

public class MemberService {

	MemberDomain memberDomain;

	private static final Logger logger = Logger.getLogger(MemberService.class);

	public List getMemberList(String strSocietyID, String listEvent) {

		List memberList = null;
		
		try{
			logger.debug("Entry : public List getMemberList(String strSocietyID, String listEvent) ");
			
			memberList = memberDomain.getMemberList(strSocietyID, listEvent);

			logger.debug("Exit : public List getMemberList(String strSocietyID, String listEvent) ");

		}catch(Exception Ex){
			logger.error("Exception in  getMemberList : "+Ex);
		}
		return memberList;
	}

	public List getSocietyMemberList(String strSocietyID) {

		List societyMemeberList = null;
		
		try {			
			logger.debug("Entry : public List getSocietyMemberList(String strSocietyID)");

			societyMemeberList = memberDomain.getSocietyMemberList(strSocietyID);

			logger.debug("Exit : public List getSocietyMemberList(String strSocietyID)");

		}catch(Exception Ex){
			logger.error("Exception in getSocietyMemberList : "+Ex);
		}
		return societyMemeberList;
	}
	
	public List getActivePrimaryMemberList(int societyID) {

		List societyMemeberList = null;
		
		try {			
			logger.debug("Entry : public List getActivePrimaryMemberList(int societyID)");

			societyMemeberList = memberDomain.getActivePrimaryMemberList(societyID);

			logger.debug("Exit : public List getActivePrimaryMemberList(int societyID)");

		}catch(Exception Ex){
			logger.error("Exception in getActivePrimaryMemberList : "+Ex);
		}
		return societyMemeberList;
	}

	public MemberVO searchMembersInfo(String memberID){
		
	    MemberVO memberVO=new MemberVO();;

		try{
		
		logger.debug("Entry: public MemberVO searchMembersInfo(String memberID)" );
		
	   	memberVO = memberDomain.searchMembersInfo(memberID);
		
		logger.debug("Exit: public MemberVO searchMembersInfo(String memberID)" );
				
		}catch(Exception Ex){			
			
			logger.error("Exception in public MemberVO searchMembersInfo(String memberID) : "+Ex);
		}
		return memberVO;
	}
	
	public List getBuildingList(int strSocietyID,String lastName) {

		List buildingList = null;
	
		try{
			logger.debug("Entry : public List getBuildingList(String strSocietyID)");

			DropDownVO drpDwnVO = new DropDownVO();

			buildingList = memberDomain.getBuildingList(strSocietyID,"");

			logger.debug("Exit : public List getBuildingList(String strSocietyID)");

		}catch(Exception Ex){
			logger.error("Exception in getBuildingList : "+Ex);
		}
		return buildingList;
	}

	public List getVehicleList(int strMemberID) {

		List vechicleList = null;	
		try {
			logger.debug("Entry : public List getVehicleList(String strMemberID) ");

			vechicleList = memberDomain.getVehicleList(strMemberID);

			logger.debug("Exit : public List getVehicleList(String strMemberID) ");

		}catch(Exception Ex){
			logger.error("Exception in getVehicleList : "+Ex);
		}
		return vechicleList;
	}
	
	public MemberVO getAssociateMembersAndShareDetails(int aptID){
		MemberVO memberVO=new MemberVO();
		logger.debug("Entry : public MemberVO getAssociateMembersAndShareDetails(int aptID)");
		
		memberVO=memberDomain.getAssociateMembersAndShareDetails(aptID);
		
		logger.debug("Exit : public MemberVO getAssociateMembersAndShareDetails(int aptID)");
		return memberVO;
	}
	
	public int updateMemberDetails(MemberVO memberVO, int memberId) {
		int flag = 0;
		try {
			logger.debug("Entry : public int updateMemberDetails(MemberVO memberVO,String memberId)");

			flag = memberDomain.updateMemberDetails(memberVO, memberId);
			
			if(flag == 1){
				logger.info("Member ID '"+memberId+"' details has been updated sucessfully.");
			}
			logger.debug("Exit : public int updateMemberDetails(MemberVO memberVO,String memberId)"+flag);
		}catch(Exception e){
			logger.error("Exception in updateMemberDetails : "+e);
		}
		return flag;
	}

	public int addVehicleDetails(VehicleVO vehicleVO, int memberId,int societyId) {
		int flag = 0;
         
		try{
		logger.debug("Entry : public int addVehicleDetails(VehicleVO vehicleVO,String memberId,String societyId )");
	
		flag = memberDomain.addVehicleDetails(vehicleVO, memberId, societyId);
		
		if(flag == 1){
			logger.info("Member ID '"+memberId+"' has vehicle details added sucessfully.");
		}
		logger.debug("Exit : public int addVehicleDetails(VehicleVO vehicleVO,String memberId,String societyId )");
		}catch(Exception e){
			logger.error("Exception in addVehicleDetails : "+e);
		}
		return flag;
	}

	public int deleteVehicleDetails(int vehicleId) {

		int sucess = 0;
		try {
			logger.debug("Entry : public int int deleteVehicleDetails(String vehicleId)");

			sucess = memberDomain.deleteVehicleDetails(vehicleId);

			logger.debug("Exit : public int deleteVehicleDetails(String vehicleId)");

		}catch(Exception Ex){
			logger.error("Exception in deleteVehicleDetails : "+Ex);
		}
		return sucess;
	}
	
	public VehicleVO getVehicleDetails(int vehicleID){
		
		VehicleVO vehicleVO = new VehicleVO();
		try{	
		logger.debug("Entry : public VehicleVO getVehicleDetails(String vehicleID)" );
		
		vehicleVO = memberDomain.getVehicleDetails(vehicleID);
				
		logger.debug("Exit : public VehicleVO getVehicleDetails(String vehicleID)");

		}catch(Exception Ex){		
			logger.error("Exception in getVehicleDetails : "+Ex);
		}
		return vehicleVO;
	}
	
	public int updateVehicleDetails(VehicleVO vehicleVO,int vehicleID){
		int success=0;
		try {
			logger.debug("Entry :public int updateVehicleDetails(VehicleVO vehicleVO,int vehicleID)");
			
			success=memberDomain.updateVehicleDetails(vehicleVO, vehicleID);		
			
			logger.debug("Exit :public int updateVehicleDetails(VehicleVO vehicleVO,int vehicleID))"+success);
		}catch(Exception e){		
			logger.error("Exception in updateVehicleDetails : "+e);
		}
		return success;
	}

	public int updateRenterDetails(RenterVO renterVO, int renterId) {
		int flag = 0;
		try {
			logger.debug("Entry :public int updateRenterDetails(RenterVO renterVO,String renterId)");

			flag = memberDomain.updateRenterDetails(renterVO, renterId);
            
			if(flag == 1){
				logger.info("Renter ID '"+renterId+"' details has been updated sucessfully.");
			} 
			logger.debug("Exit :public int updateRenterDetails(RenterVO renterVO,String renterId)"+ flag);
		}catch(Exception e){
			logger.error("Exception in updaterenterDetails : "+e);
		}
		return flag;
	}

	public int addRenterDetails(RenterVO renterVO) {
		int flag = 0;
		
        try{
		    logger.debug("Entry :public int addRenterDetails(RenterVO renterVO )");

		flag = memberDomain.addRenterDetails(renterVO);
		
		if(flag == 1){
			logger.info("Apartment ID '"+renterVO.getAptID()+"' has renter details added sucessfully.");
		}
		   logger.debug("Exit :public int addRenterDetails(RenterVO renterVO )");
        }catch(Exception e){
			logger.error("Exception in addRenterDetails : "+e);
		}
		return flag;
	}

	public int addRenterMemberDetails(RenterVO renterVO) {
		int flag = 0;
		
        try{
		    logger.debug("Entry :public int addRenterMemberDetails(RenterVO renterVO )");

		flag = memberDomain.addRenterMemberDetails(renterVO);
		
		if(flag == 1){
			logger.info("Apartment ID '"+renterVO.getAptID()+"' has renter details added sucessfully.");
		}
		   logger.debug("Exit :public int addRenterDetails(RenterVO renterVO )");
        }catch(Exception e){
			logger.error("Exception in addRenterDetails : "+e);
		}
		return flag;
	}
	
	public int updateRenterMemberDetails(RenterVO renterVO, String renterId) {
		int flag = 0;
		try {
			logger.debug("Entry :public int updateRenterMemberDetails(RenterVO renterVO,String renterId)");

			flag = memberDomain.updateRenterMemberDetails(renterVO, renterId);
            
			if(flag == 1){
				logger.info("Renter ID '"+renterId+"' details has been updated sucessfully.");
			} 
			logger.debug("Exit :public int updateRenterMemberDetails(RenterVO renterVO,String renterId)"+ flag);
		}catch(Exception e){
			logger.error("Exception in updateRenterMemberDetails : "+e);
		}
		return flag;
	}
	public RenterVO getRenterDetails(int aptID , int renterID) {
		RenterVO rentrVO = new RenterVO();
		try{
		logger.debug("Entry : public RenterVO getRenterDetails(String apartmentID)");

		rentrVO = memberDomain.getRenterDetails(aptID,renterID);

		logger.debug("Exit : public RenterVO getRenterDetails(String apartmentID)");
		}catch(Exception e){
			logger.error("Exception in getRenterDetails : "+e);
		}
		return rentrVO;
	}

	public List getRenterMemberDetails(int renterID){
		
		List rentrVOList=null;
			
		try {
			logger.debug("Entry : public RenterVO getRenterMemberDetails(String renterID)");
			
			
			rentrVOList = memberDomain.getRenterMemberDetails(renterID);
		}
		catch (Exception ex){
			logger.error("Exception in getRenterMemberDetails : "+ex);
		}
		
		logger.debug("Exit : public RenterVO getRenterMemberDetails(String apartmentID)"+rentrVOList.size());
		return rentrVOList;
	}
	
	public List getRenterHistoryDetails(int apartmentID){
		List rentrVOList=null; 
		try{
		logger.debug("Entry : public List getRenterHistoryDetails(String apartmentID)");

		rentrVOList = memberDomain.getRenterHistoryDetails(apartmentID);

		logger.debug("Exit : public List getRenterHistoryDetails(String apartmentID)");
		}catch(Exception e){
			logger.error("Exception in getRenterHistoryDetails : "+e);
		}
		return rentrVOList;
	}
	
	public int updateRenterNOCStatus(String renterId, String apartmentID,int nocStatus) {

		int sucess = 0;
		try {
			logger.debug("Entry : public int updateRenterNOCStatus(String renterId, String apartmentID)");

			sucess = memberDomain.updateRenterNOCStatus(renterId, apartmentID,nocStatus);

			logger.debug("Exit : public int updateRenterNOCStatus(String renterId, String apartmentID)");

		}catch(Exception Ex){
			logger.error("Exception in updateRenterNOCStatus : "+Ex);
		}
		return sucess;
	}

	public int deleteRenterDetails(int renterId, int apartmentID) {

		int sucess = 0;
		try {
			logger.debug("Entry : public int deleteRenterDetails(String renterId, String apartmentID)");

			sucess = memberDomain.deleteRenterDetails(renterId, apartmentID);

			logger.debug("Exit : public int deleteRenterDetails(String renterId, String apartmentID)");

		}catch(Exception Ex){
			logger.error("Exception in deleteRenterDetails : "+Ex);
		}
		return sucess;
	}

	public int deleteRenterMember(int renterMemberId) {

		int sucess = 0;
		try {
			logger.debug("Entry : public int deleteRenterMember(String renterMemberId)");

			sucess = memberDomain.deleteRenterMember(renterMemberId);

			logger.debug("Exit : public int deleteRenterMember(String renterMemberId)");

		}catch(Exception Ex){
			logger.error("Exception in deleteRenterMember : "+Ex);
		}
		return sucess;
	}
	
	public MemberVO getMemberNameDetails(String memeberID){
		MemberVO memberVo=new MemberVO();
		
		try {
			logger.debug("Entry : public MemberVO getMemberNameDetails(String memeberID)");
			
			
			memberVo = memberDomain.getMemberNameDetails(memeberID);
			
		} catch (DataAccessException e) {
		    logger.error("DataAccessException in getMemberNameDetails : "+e);
		}
		catch (Exception ex) {
			logger.error("Exception in getMemberNameDetails : "+ex);
		}
		
		logger.debug("Exit : public MemberVO getMemberNameDetails(String memeberID)");
		return memberVo;
	}
    
	public List getMemberListForSearchByName(String strSocietyID){
		
	    List memberList = null;

		try{		
		    logger.debug("Entry : public List getMemberListForSearchByName(String strSocietyID)" );
		
		 
			    memberList = memberDomain.getMemberListForSearchByName(strSocietyID);	
		    	
		
		    logger.debug("Exit : public List getMemberListForSearchByName(String strSocietyID)" );
		}catch (Exception ex) {
			logger.error("Exception in getMemberListForSearchByName : "+ex);
		}
		return memberList;
		}
	
	
public List getMemberListForMobileApp(String strSocietyID){
		
	    List memberList = null;

		try{		
		    logger.debug("Entry : public List getMemberListForMobileApp(String buildingID)" );
		
		 
		    memberList = memberDomain.getMemberListForSearchByName(strSocietyID);	
		    	
		
		    logger.debug("Exit : public List getMemberListForMobileApp(String buildingID)" );
		}catch (Exception ex) {
			logger.error("Exception in getMemberListForMobileApp : "+ex);
		}
		return memberList;
		}
	
	public List getMemberListBuildingWise(int buildingID,int societyID){
	
    List memberList = null;

	try{		
	    logger.debug("Entry : public List getMemberListBuildingWise(String buildingID)" );
	
	 
		    memberList = memberDomain.getMemberListBuildingWise(buildingID,societyID);	
	    	
	
	    logger.debug("Exit : public List getMemberListBuildingWise(String buildingID)" );
	}catch (Exception ex) {
		logger.error("Exception in getMemberListForMobileApp : "+ex);
	}
	return memberList;
	}
	public List getMemberListOfApartmentsBuilding(int aptID){
		
	    List memberList = null;

		try{		
		    logger.debug("Entry : public List getMemberListOfApartmentsBuilding(String aptID)" );
		
		 
			    memberList = memberDomain.getMemberListOfApartmentsBuilding(aptID);	
		    	
		
		    logger.debug("Exit : public List getMemberListOfApartmentsBuilding(String buildingID)" );
		}catch (Exception ex) {
			logger.error("Exception in getMemberListOfApartmentsBuilding : "+ex);
		}
		return memberList;
		}
	
	public List getVehicleListForSearch(String societyId){
		
	    List vechicleList = null;	
		try{	
		logger.debug("Entry : public List getVehicleListForSearch(String societyId)" );
		
		vechicleList = memberDomain.getVehicleListForSearch(societyId);
				
		logger.debug("Exit : public List getVehicleListForSearch(String societyId)");

		}catch(Exception Ex){		
			Ex.printStackTrace();
			logger.error("Exception in getVehicleListForSearch : "+Ex);
		}
		return vechicleList;
	}
	
	public MemberVO getMemberInfo(int aptID){
		MemberVO memberVo=new MemberVO();
		
		try {
			logger.debug("Entry :public MemberVO getMemberInfo(String memberID)");
			
			
			memberVo = memberDomain.getMemberInfo(aptID);
			
		} catch (DataAccessException e) {
		    logger.error("DataAccessException in getMemberInfo : "+e);
		}
		catch (Exception ex) {
			logger.error("Exception in getMemberInfo : "+ex);
		}
		
		logger.debug("Exit : public MemberVO getMemberInfo(String memberID)");
		return memberVo;
	}
	
	public MemberWSVO getMembersInfoWithMail(String emailID){
		MemberWSVO memberVo=new MemberWSVO();
		
		try {
			logger.debug("Entry : public MemberWSVO getMembersInfoWithMail(String emailID)");
			
			
			memberVo = memberDomain.getMembersInfoWithMail(emailID);
			
		} catch (DataAccessException e) {
		    logger.error("DataAccessException in getMembersInfoWithMail : "+e);
		}
		catch (Exception ex) {
			logger.error("Exception in getMembersInfoWithMail : "+ex);
		}
		
		logger.debug("Exit : public MemberVO getMembersInfoWithMail(String memberID)");
		return memberVo;
	}
	
	public MemberVO getMemberInfoThroughLedgerID(int societyID,String ledgerID){
		MemberVO memberVo=new MemberVO();
		
		try {
			logger.debug("Entry :public MemberVO getMemberInfoThroughLedgerID(String ledgerID");
			
			
			memberVo = memberDomain.getMemberInfoThroughLedgerID(societyID,ledgerID);
			
		} catch (DataAccessException e) {
		    logger.error("DataAccessException in getMemberInfoThroughLedgerID(String ledgerID : "+e);
		}
		catch (Exception ex) {
			logger.error("Exception in getMemberInfoThroughLedgerID(String ledgerID : "+ex);
		}
		
		logger.debug("Exit : public MemberVO getMemberInfoThroughLedgerID(String ledgerID");
		return memberVo;
	}
	
	
	public List categoryWiseMemberTxDetails(int year, String societyId, String categoryID,int memberId) {
		List txCategorywise=null;
		try {
			logger.debug("Entry:public List categoryWiseMemberTxDetails(int year, String societyId, String categoryID,int memberId)" + year + categoryID+memberId);
			
			txCategorywise=memberDomain.categoryWiseMemberTxDetails(year, societyId, categoryID, memberId);
		}
		catch (Exception ex) {
			logger.error("Exception in categoryWiseMemberTxDetails : " + ex);
		}
		logger.debug("Exit:public List categoryWiseMemberTxDetails(int year, String societyId, String categoryID,int memberId)");
	return txCategorywise;

	}
	
	
	public List getMemberDocumentList(int memberID){
		
	    List memberDocsList = null;

		try{		
		    logger.debug("Entry : public List getMemberDocumentList(String memberID)" );
		
		    
		    memberDocsList = memberDomain.getMemberDocumentList(memberID);
		        		
		    logger.debug("Exit : public List getMemberDocumentList(String memberID)" );
		
		}catch(Exception Ex){			
			logger.error("Exception in getMemberDocumentList : "+Ex);
			
		}
		return memberDocsList;
	}
	
	public List getMemberMissingDocument(int memberID,int documentID){
		
	    List memberDocsList = null;

		try{		
		    logger.debug("Entry : public List getMemberMissingDocument(int memberID,int documentID)" );
		
		    
		    memberDocsList = memberDomain.getMemberMissingDocument(memberID,documentID);
		        		
		    logger.debug("Exit :public List getMemberMissingDocument(int memberID,int documentID)" );
		}catch (ClassCastException ex) {
		
		
		}catch(Exception Ex){			
			logger.error("Exception in public List getMemberMissingDocument(int memberID,int documentID) : "+Ex);
			
		}
		return memberDocsList;
	}

	
	
	public List getDocumentMissingList(int buildingID,int societyId,int documentID) {

		List memberList=null;
		
		
		try {
			logger.debug("Entry : public List getDocumentMissingList(int buildingID,int societyId,int documentID)"+societyId);

		
			memberList=  memberDomain.getDocumentMissingList(buildingID, societyId, documentID);

			
			logger.debug("Exit:	public List getDocumentMissingList(int buildingID,int societyId,int documentID)");
		} catch (NullPointerException ex) {
			logger.error("Exception in public List getDocumentMissingList(int buildingID,int societyId,int documentID) : "+ex);

			
		} catch (Exception ex) {
			logger.error("Exception in public List getDocumentMissingList(int buildingID,int societyId,int documentID) : "+ex);

		}
		return memberList;

	}

		public int updateDocument(RptDocumentVO memberVO){
			
			   int flag=0 ;

				try{		
				    logger.debug("Entry : public int updateDocument(RptDocumentVO memberVO)" );
				
				    
				    flag = memberDomain.updateDocument(memberVO);
				        		
				    logger.debug("Exit : public int updateDocument(RptDocumentVO memberVO)" );
					
				}catch(Exception Ex){			
					logger.error("Exception in updateDocument : "+Ex);
					
				}
				return flag;
			}
     
		public int updateDocumentList(MemberVO memberVO){
			
			   int flag=0 ;

				try{		
				    logger.debug("Entry : public int updateDocumentList(MemberVO memberVO)" );
				
				    
				    flag = memberDomain.updateDocumentList(memberVO);
				        		
				    logger.debug("Exit : public int updateDocumentList(MemberVO memberVO)" );
					
				}catch(Exception Ex){			
					logger.error("Exception in updateDocumentList : "+Ex);
					
				}
				return flag;
			}
	
		
		public int transferMember(InsertMemberVO memberVO,int aptID) {

	 		int sucess = 0;
	 		try {
	 			logger.debug("Entry : public int transferMember(InsertMemberVO memberVO,int aptID)");

	 			sucess = memberDomain.transferMember(memberVO,aptID);

	 			logger.debug("Exit : public int transferMember(InsertMemberVO memberVO,int aptID)");

	 		}catch (Exception Ex) {
	 			logger.error("Exception in public int transferMember(InsertMemberVO memberVO,int aptID): "+Ex);
	 		}
	 		return sucess;
	 	}	
		
		public DropDownVO checkTransferMember(String txID,String memberID,String societyID,String type) {

	 		DropDownVO downVO=null;
	 		try {
	 			logger.debug("Entry : public int checkTransferMember(String txID,String memberID)");

	 			downVO = memberDomain.checkTransferMember(txID,memberID,societyID,type);

	 			logger.debug("Exit : public int checkTransferMember(String txID,String memberID)");

	 		}catch (Exception Ex) {
	 			logger.error("Exception in public int checkTransferMember(String txID,String memberID): "+Ex);
	 		}
	 		return downVO;
	 	}	
		
		
		public int confirmMemberTransfer(int apartmentID,InsertMemberVO memberVO) {

	 		int sucess = 0;
	 		try {
	 			logger.debug("Entry : confirmMemberTransfer(String apartmentID)");

	 			sucess = memberDomain.confirmMemberTransfer(apartmentID, memberVO);

	 			logger.debug("Exit : public int confirmMemberTransfer(String apartmentID)");

	 		}catch (Exception Ex) {
	 			logger.error("Exception in public int confirmMemberTransfer(String apartmentID): "+Ex);
	 		}
	 		return sucess;
	 	}	
		
		public List getMembershipNoList(String societyID){
	    	List memberList=null;
	    	
	    	try {
	    		    	
	    	memberList =  memberDomain.getMembershipNoList(societyID);
	    		
	    	logger.debug("Exit : public List getMembershipNoList(String soceityID)"+memberList.size());
	    	} catch (DataAccessException e) {
	    	    logger.error("DataAccessException in public List getMembershipNoList(String soceityID) : "+e);
	    	}
	    	catch (Exception ex) {
	    		logger.error("Exception in public List getMembershipNoList(String soceityID) : "+ex);
	    	}
	    	return memberList;
	    }
		
		public List convertMemberListForWebservice(List memberList){
			List WsMemberList=new ArrayList();
			logger.debug("Entry : public List convertMemberListForWebservice(List MemberList)");
			
			WsMemberList=memberDomain.convertMemberListForWebservice(memberList);
			
			logger.debug("Exit : public List convertMemberListForWebservice(List MemberList)");
			return WsMemberList;
		}
		
		
		public int insertTenantForWebservice(RenterVO renterVO) {
			int flag = 0;
			
	        try{
			 logger.debug("Entry :public int insertTenantForWebservice(RenterVO renterVO)");

			flag = memberDomain.insertTenantForWebservice(renterVO);
			
			if(flag >0){
				logger.info("Apartment ID '"+renterVO.getAptID()+"' has renter details added sucessfully.");
			}
			   logger.debug("Exit :	public int insertTenantForWebservice(RenterVO renterVO)");
	        }catch(Exception e){
				logger.error("Exception in insertTenantForWebservice : "+e);
			}
			return flag;
		}
	    
		 public List getExpiredtenantDetails(int societyID,String uptoDate,Boolean aboutToExpire) {
				List tenantList=new ArrayList();
				try {
					logger
							.debug("Entry : public List getExpiredtenantDetails(int intSocietyID) ");

					tenantList = memberDomain.getExpiredtenantDetails(societyID, uptoDate,aboutToExpire);

				} catch (EmptyResultDataAccessException e) {
					logger.info("No tenant details available for this society");
				} catch (IndexOutOfBoundsException ex) {
					logger.info("No data available for this society : " + ex);
				}

				catch (Exception ex) {
					logger.error("Exception in getExpiredtenantDetails : " + ex);
				}

				logger.debug("Exit : public List getExpiredtenantDetails(int intSocietyID) "
						+ tenantList.size());
				return tenantList;
			}
		
		public List getMembersContactList(String societyID, String memberType,String infoMissingType) {

			List societyMemeberList = null;
			
			try {			
				logger.debug("Entry : public List getMembersContactList(String societyID, String memberType,String infoMissingType)");

				societyMemeberList = memberDomain.getMembersContactList(societyID, memberType, infoMissingType);

				logger.debug("Exit : public List getMembersContactList(String societyID, String memberType,String infoMissingType)");

			}catch(Exception Ex){
				logger.error("Exception in getMembersContactList : "+Ex);
			}
			return societyMemeberList;
		}
		
		public TicketVO getTicketList(int orgID,int userID,String statusType){
			
			String apiURL="";
			String apiStatus="";
			TicketVO ticketsVO=new TicketVO();
			logger.debug("Entry : private TicketVO getTicketList(String societID,int orgID,String statusType)");
			
			try {
				ticketsVO=memberDomain.getTicketList(orgID,userID, statusType);
				
			 }catch(Exception e) {
			    	logger.error("Exception in getTicketList "+e);
			 }   
				
				
			logger.debug("Exit : private TicketVO getTicketList(String societID,int orgID,String statusType)");
			return ticketsVO;
		}
		
       public TicketVO getTicketDetails(int ticketNO,String statusType){
			
			TicketVO ticketsVO=new TicketVO();
			logger.debug("Entry : private TicketVO getTicketDetails(int ticketNO,String statusType)");
			
			try {
				ticketsVO=memberDomain.getTicketDetails(ticketNO, statusType);
				
			 }catch(Exception e) {
			    	logger.error("Exception in getTicketDetails "+e);
			 }   
				
				
			logger.debug("Exit : private TicketVO getTicketDetails(int ticketNO,String statusType)");
			return ticketsVO;
		} 
       
       public int createTicket(TicketVO requestVO){
    	   int success = 0;
			logger.debug("Entry :public TicketVO createTicket(TicketVO requestVO)");
			
			try {
				success=memberDomain.createTicket(requestVO);
				
			 }catch(Exception e) {
			    	logger.error("Exception in createTicket "+e);
			 }   
				
				
			logger.debug("Exit : public TicketVO createTicket(TicketVO requestVO)");
			return success;
		} 
       
       public TicketVO getTicketHelpTopics(String userType){
			
			TicketVO ticketsVO=new TicketVO();
			logger.debug("Entry :  public TicketVO getTicketHelpTopics(String userType)");
			
			try {
				ticketsVO=memberDomain.getTicketHelpTopics(userType);
				
			 }catch(Exception e) {
			    	logger.error("Exception in getTicketHelpTopics "+e);
			 }   
				
				
			logger.debug("Exit : public TicketVO getTicketHelpTopics(String userType)");
			return ticketsVO;
		} 
       
       public int createSupportTicket(TicketVO requestVO){
    	   int success = 0;
			logger.debug("Entry :public TicketVO createSupportTicket(TicketVO requestVO)");
			
			try {
				success=memberDomain.createSupportTicket(requestVO);
				
			 }catch(Exception e) {
			    	logger.error("Exception in createSupportTicket "+e);
			 }   
				
				
			logger.debug("Exit : public TicketVO createSupportTicket(TicketVO requestVO)");
			return success;
		} 
       
       public TicketVO getOrgRulesAndRegulations(int orgID,String type){
			
			TicketVO ticketsVO=new TicketVO();
			logger.debug("Entry :  public TicketVO getOrgRulesAndRegulations(int orgID)");
			
			try {
				ticketsVO=memberDomain.getOrgRulesAndRegulations(orgID,type);
				
			 }catch(Exception e) {
			    	logger.error("Exception in getOrgRulesAndRegulations "+e);
			 }   
				
				
			logger.debug("Exit : public TicketVO getOrgRulesAndRegulations(int orgID)");
			return ticketsVO;
		} 
       
       public int createUserInTicketPortal(TicketVO requestVO){
    	   int success = 0;
			logger.debug("Entry :public TicketVO createUserInTicketPortal(TicketVO requestVO)");
			
			try {
				success=memberDomain.createUserInTicketPortal(requestVO);
				
			 }catch(Exception e) {
			    	logger.error("Exception in createUserInTicketPortal "+e);
			 }   
				
				
			logger.debug("Exit : public TicketVO createUserInTicketPortal(TicketVO requestVO)");
			return success;
		} 
       
       public NotificationVO sendSMSandPushNotificationsToAGroup(SMSMessage smsVO){
			
			NotificationVO notificationVO=new NotificationVO();
			logger.debug("Entry : public NotificationVO sendSMSandPushNotificationsToAGroup(SMSMessage smsVO)");
			
			try {
				notificationVO=memberDomain.sendSMSandPushNotificationsToAGroup(smsVO);
				
			 }catch(Exception e) {
			    	logger.error("Exception in public NotificationVO sendSMSandPushNotificationsToAGroup(SMSMessage smsVO) "+e);
			 }   
				
				
			logger.debug("Exit : public NotificationVO sendSMSNotificationsToAGroup(SMSMessage smsVO)");
			return notificationVO;
		} 
       
       public NotificationVO sendEmailandPushNotificationsToAGroup(EmailMessage emailVO){
			
			NotificationVO notificationVO=new NotificationVO();
			logger.debug("Entry : public NotificationVO sendEmailandPushNotificationsToAGroup(EmailMessage emailVO)");
			
			try {
				notificationVO=memberDomain.sendEmailandPushNotificationsToAGroup(emailVO);
				
			 }catch(Exception e) {
			    	logger.error("Exception in public NotificationVO sendEmailandPushNotificationsToAGroup(EmailMessage emailVO) "+e);
			 }   
				
				
			logger.debug("Exit : public NotificationVO sendEmailandPushNotificationsToAGroup(EmailMessage emailVO)");
			return notificationVO;
		} 
		
     //============ Get Member list of groups like, member/ committee/ tenant etc =========// 
  	 public List getMembersListFromGroup(int societyID, String groupName){
  		 	List memberList=new ArrayList();
  		 	logger.debug("Entry : public List getMembersListFromGroup(int societyID, String groupName)"+societyID+groupName);
  		 	
  		try{	
  		 		memberList = memberDomain.getMembersListFromGroup(societyID, groupName);
  		 	}catch (Exception ex){
  		 		logger.error("Exception in public List getMembersListFromGroup(int societyID, String groupName) : "+ex);
  		 		
  		 	}
  		 	
  		 	logger.debug("Exit : public List getMembersListFromGroup(int societyID, String groupName)");
  		 	return memberList;
  		 }

       
  	public MemberVO getIRegister(int orgID,int aptID){
		
		MemberVO memberVO=new MemberVO();
		logger.debug("Entry : public MemberVO getIRegister(int orgID,int aptID");
		
		try {
			memberVO=memberDomain.getIRegister(orgID, aptID);
			
		 }catch(Exception e) {
		    	logger.error("Exception in getIRegister "+e);
		 }   
			
			
		logger.debug("Exit : public MemberVO getIRegister(int orgID,int aptID");
		return memberVO;
	} 
  	 
  	
  	public List getNominationRegisterForApt(int orgID,int aptID){
		
		List nominationList=new ArrayList<>();
		logger.debug("Entry : public List getNominationRegisterForApt(int orgID,int aptID");
		
		try {
			nominationList=memberDomain.getNominationRegisterForApt(orgID, aptID);
			
		 }catch(Exception e) {
		    	logger.error("Exception in getNominationRegisterForApt "+e);
		 }   
			
			
		logger.debug("Exit : public List getNominationRegisterForApt(int orgID,int aptID");
		return nominationList;
	} 
       
	public MemberDomain getMemberDomain() {
		return memberDomain;
	}

	public void setMemberDomain(MemberDomain memberDomain) {
		this.memberDomain = memberDomain;
	}

}
