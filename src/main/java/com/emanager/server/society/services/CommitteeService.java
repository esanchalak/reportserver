package com.emanager.server.society.services;

import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.society.domainObject.CommitteeDomain;
import com.emanager.server.society.domainObject.MemberDomain;
import com.emanager.server.society.valueObject.CommitteeVO;

public class CommitteeService {
	private static final Logger logger = Logger.getLogger(CommitteeService.class);
	
	CommitteeDomain committeeDomain;
	MemberDomain memberDomain; 


public List getSocietyMemberList(String societyID){
	
    List societyMemberList = null;
    
	try{
		
	logger.debug("Entry : public List getSocietyMemberList(String societyID)" );
	
	societyMemberList = memberDomain.getSocietyMemberList(societyID);
			
	logger.debug("Exit : public List getSocietyMemberList(String societyID)" );

	}catch(Exception Ex){
		
		logger.error("Exception in getSocietyMemberList : "+Ex);
		
	}
	return societyMemberList;
}

  public List searchCommityMembers(String committeeId){
	
    List commityMemberList = null;

	logger.debug("Entry :   public List searchCommityMembers(String committeeId)" );
	
	commityMemberList = committeeDomain.searchCommityMembers(committeeId);
	
	logger.debug("Exit :   public List searchCommityMembers(String committeeId)" );
	

	return commityMemberList;
}

	public List getCommityList(String societyID){
		
	    List committeeList = null;

		try{		

		logger.debug("Entry : public List getCommityList(String societyID)" );
		
		committeeList = committeeDomain.getCommityList(societyID);
		
		logger.debug("Exit : public List getCommityList(String societyID)" );
					
		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in getCommityList : "+Ex);
			
		}
		return committeeList;
	}
	
	public List getCommityMemberList(String committeeId){
		
	    List committeeMemberList = null;
		
		logger.debug("Entry : public List getCommityMemberList(String committeeId)");
		
		committeeMemberList = committeeDomain.getCommityMemberList(committeeId);
		
		logger.debug("Exit : public List getCommityMemberList(String committeeId)");
			
		
		return committeeMemberList;
	}
	
	public int insertCommiteeDetails(CommitteeVO committeeVO){
		
		int sucess = 0;
		
		try{
		
		logger.debug("Entry : public int insertCommiteeDetails(CommitteeVO committeeVO)");
		
		sucess = committeeDomain.insertCommiteeDetails(committeeVO);
		
		if(sucess == 1){
			logger.info("Committee '"+committeeVO.getCommitteeName()+"' has been added sucessfully.");
		}
		logger.debug("Exit : public int insertCommiteeDetails(CommitteeVO committeeVO)"+sucess);

		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in insertCommiteeDetails : "+Ex);
			
		}
		return sucess;
	}
	public int insertCommityWorklog(CommitteeVO committeeVO,String committeeId){
		
		int sucess = 0;
		
	    logger.debug("Entry : public int insertCommityWorklog(CommitteeVO committeeVO,String committeeId)");
		
		sucess = committeeDomain.insertCommityWorklog(committeeVO,committeeId);
		
		if(sucess == 1){
			logger.info("Committee ID '"+committeeId+"' has worklog added sucessfully.");
		}
		
		logger.debug("Exit : public int insertCommityWorklog(CommitteeVO committeeVO,String committeeId)"+sucess);

		
		return sucess;
	}

	public List searchCommityMembersForUpdate(CommitteeVO committeeVO){
		
	    List memberList = null;

		try{	

		logger.debug("Entry : public List searchCommityMembersForUpdate(committeeVO)" );
		
		memberList = committeeDomain.searchCommityMembersForUpdate(committeeVO);
			
		logger.debug("Exit : public List searchCommityMembersForUpdate(committeeVO)" );
				
		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in searchCommityMembersForUpdate : "+Ex);
			
		}
		return memberList;
	}
	
public List listCommityMemberUpdate(String societyId){
		
	    List memberList = null;

		try{	

		logger.debug("Entry : public List listCommityMemberUpdate(String societyId)" );
		
		memberList = committeeDomain.listCommityMemberUpdate(societyId);
		
		logger.debug("Exit : public List listCommityMemberUpdate(String societyId)" );
				
		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in listCommityMemberUpdate : "+Ex);
			
		}
		return memberList;
	}
	

   public int removeCommity(String committeeId){
	
	int sucess = 0;
	
	try{
	
	logger.debug("Entry : public int removeCommity(String committeeId)");
	
	sucess = committeeDomain.removeCommity(committeeId);
			
	logger.debug("Exit : public int removeCommity(String committeeId)");

	}catch(Exception Ex){
		
		Ex.printStackTrace();
		logger.error("Exception in removeCommity : "+Ex);
	}
	return sucess;
}
   public int updateCommityDetails(CommitteeVO committeeVO,String committeeId){
		
		int sucess = 0;
		
		try{
		
		logger.debug("Entry :public int updateCommityDetails(CommitteeVO committeeVO,String committeeId)");
	
		sucess = committeeDomain.updateCommityDetails(committeeVO,committeeId);
		
		if(sucess == 1){
			logger.info("Committee ID '"+committeeId+"' has been updated sucessfully.");
		}		
		
		logger.debug("Exit : public int updateCommityDetails(CommitteeVO committeeVO,String committeeId)");

		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in updateCommityDetails : "+Ex);
			
		}
		return sucess;
	}
   
   public List searchCommittee(String strSocietyID){
		
	    List committeeList = null;

		try{		
		logger.debug("Entry :  public List searchCommittee(String strSocietyID)" );
		
		committeeList = committeeDomain.searchCommittee(strSocietyID);  		
		
		logger.debug("Exit :  public List searchCommittee(String strSocietyID)" );
				
		}catch(Exception Ex){			
			Ex.printStackTrace();
		    logger.error("Exception in searchCommittee : "+Ex);
		}
		return committeeList;
	}
   
	public MemberDomain getMemberDomain() {
		return memberDomain;
	}


	public void setMemberDomain(MemberDomain memberDomain) {
		this.memberDomain = memberDomain;
	}

	public CommitteeDomain getCommitteeDomain() {
		return committeeDomain;
	}

	public void setCommitteeDomain(CommitteeDomain committeeDomain) {
		this.committeeDomain = committeeDomain;
	}
}
