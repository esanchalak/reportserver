package com.emanager.server.society.domainObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.adminReports.Services.ShareRegService;
import com.emanager.server.adminReports.valueObject.RptDocumentVO;
import com.emanager.server.adminReports.valueObject.RptTenantVO;
import com.emanager.server.adminReports.valueObject.ShareRegisterVO;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.services.AddressService;
import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.commonUtils.valueObject.DropDownVO;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.society.dataAccessObject.MemberDAO;
import com.emanager.server.society.dataAccessObject.SocietyDAO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.InsertMemberVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.MemberWSVO;
import com.emanager.server.society.valueObject.RenterVO;
import com.emanager.server.society.valueObject.VehicleVO;
import com.emanager.server.taskAndEventManagement.Service.NotificationService;
import com.emanager.server.taskAndEventManagement.valueObject.EmailMessage;
import com.emanager.server.taskAndEventManagement.valueObject.NotificationVO;
import com.emanager.server.taskAndEventManagement.valueObject.PushNotificationVO;
import com.emanager.server.taskAndEventManagement.valueObject.SMSMessage;
import com.emanager.server.taskAndEventManagement.valueObject.TicketVO;
import com.emanager.server.userLogin.services.LoginService;
import com.emanager.server.userManagement.service.UserService;
import com.emanager.server.userManagement.valueObject.UserVO;
import com.emanager.webservice.GoogleGroupUpdater;
import com.google.gson.Gson;



public class MemberDomain {
	MemberDAO memberDAO;
	
	LoginService loginService;
	GoogleGroupUpdater googleGroupUpdater;
	AddressService addressServiceBean;
	ShareRegService shareRegisterService;
	LedgerService ledgerService;
	UserService userService;
	NotificationService notificationService;
	ReportService reportServiceBean;
	SocietyService societyServiceBean;
	
	private static final Logger logger = Logger.getLogger(MemberDomain.class);
	
	public List getMemberList(String strSocietyID, String listEvent){
		
	    List memberList = null;

		try{		
		    logger.info(strSocietyID+"Entry : public List getMemberList(String strSocietyID, String listEvent)" );
		
		    if (listEvent.equalsIgnoreCase("comboList"))
			    memberList = memberDAO.searchMembers(strSocietyID,listEvent);
		    else
			    memberList = memberDAO.searchMembers(strSocietyID);
		
		    		
		    logger.debug("Exit : public List getMemberList(String strSocietyID, String listEvent)" );
				
		}catch(Exception Ex){			
			logger.error("Exception in getMemberList : "+Ex);
			
		}
		return memberList;
	}
	
public List getSocietyMemberList(String strSocietyID){
		
	    List societyMemeberList = null;

		try{
		
		logger.debug("Entry: public List getSocietyMemberList(String strSocietyID)" );
		
	   	societyMemeberList = memberDAO.searchSocietyMembers(strSocietyID);
		
		logger.debug("Exit: public List getSocietyMemberList(String strSocietyID)" );
				
		}catch(Exception Ex){			
			Ex.printStackTrace();
			logger.error("Exception in getSocietyMemberList : "+Ex);
		}
		return societyMemeberList;
	}
	

public List getActivePrimaryMemberList(int strSocietyID){
	
    List societyMemeberList = null;

	try{
	
	logger.debug("Entry: public List getActivePrimaryMemberList(int strSocietyID)" );
	
   	societyMemeberList = memberDAO.getActivePrimaryMemberList(strSocietyID);
	
	logger.debug("Exit: public List getActivePrimaryMemberList(int strSocietyID)" );
			
	}catch(Exception Ex){			
		Ex.printStackTrace();
		logger.error("Exception in getActivePrimaryMemberList : "+Ex);
	}
	return societyMemeberList;
}

public MemberVO searchMembersInfo(String memberID){
	
    MemberVO memberVO=new MemberVO();;

	try{
	
	logger.debug("Entry: public MemberVO searchMembersInfo(String memberID)" );
	
   	memberVO = memberDAO.searchMembersInfo(memberID);
	
	logger.debug("Exit: public MemberVO searchMembersInfo(String memberID)" );
			
	}catch(Exception Ex){			
		
		logger.error("Exception in public MemberVO searchMembersInfo(String memberID) : "+Ex);
	}
	return memberVO;
}
public List getBuildingList(int strSocietyID, String lastName){
		
	    List buildingList = null;

		try{		
		logger.debug("Entry : public List getBuildingList(String strSocietyID, String lastName)" );
		
		buildingList = memberDAO.searchBuildings(strSocietyID,"");  		
		
		logger.debug("Exit : public List getBuildingList(String strSocietyID, String lastName)" );
				
		}catch(Exception Ex){			
			Ex.printStackTrace();
		    logger.error("Exception in getBuildingList : "+Ex);
		}
		return buildingList;
	}
	
public List getVehicleList(int strMemberID){
	
    List vechicleList = null;	
	try{	
	logger.debug("Entry : public List getVehicleList(String strMemberID)" );
	
	vechicleList = memberDAO.getVehicleList(strMemberID);
			
	logger.debug("Exit : public List getVehicleList(String strMemberID)");

	}catch(Exception Ex){		
		Ex.printStackTrace();
		logger.error("Exception in getVehicleList : "+Ex);
	}
	return vechicleList;
}


public MemberVO getAssociateMembersAndShareDetails(int aptID){
	MemberVO memberVO=new MemberVO();
	logger.debug("Entry : public MemberVO getAssociateMembersAndShareDetails(int aptID)");
	
	try {
		List assoMemberList=new ArrayList();
		ShareRegisterVO shareCertVO=new ShareRegisterVO();
		assoMemberList=getAssociateMembers(aptID);
		
		shareCertVO=shareRegisterService.getShareDetails(aptID);
		
		memberVO.setAssociateMembers(assoMemberList);
		memberVO.setShareCertVO(shareCertVO);
	} catch (Exception e) {
		logger.error("Execption : public MemberVO getAssociateMembersAndShareDetails(int aptID)"+e);
	}
	
	
	logger.debug("Exit : public MemberVO getAssociateMembersAndShareDetails(int aptID)");
	return memberVO;
}


public List getAssociateMembers(int aptID){
	List memberList=new ArrayList();
	logger.debug("Entry : public MemberVO getAssociateMembers(int aptID)");
	
	memberList=memberDAO.getAssociateMembers(aptID);
	
	logger.debug("Exit : public MemberVO getAssociateMembers(int aptID)");
	return memberList;
}


public int updateMemberDetails(MemberVO memberVO,int memberId){
	int flag=0;
	int delCommiteeMember=0;
	int addCommitteeMember=0;
	int delMember=0;
	int addMember=0;
	try {
		logger.debug("Entry : public int updateMemberDetails(MemberVO memberVO,String memberId)");
		
		flag=memberDAO.updateMemberDetails(memberVO, memberId);
		if(flag==1){
		if(memberVO.getIsCommitteeMember()==1){
			if(memberVO.getOldEmailID()==""){
				   delCommiteeMember=1;
			   }else
			delCommiteeMember=googleGroupUpdater.deleteMember(memberVO.getComitteeGroupEmail(), memberVO.getOldEmailID());
		    addCommitteeMember=googleGroupUpdater.createMember(memberVO.getComitteeGroupEmail(), memberVO.getEmail(), "Member");
		   
		    
		   /* if((delCommiteeMember==0)||(addCommitteeMember==0)){
		    	revertBackMemberUpdate(memberVO, memberId);
		    }*/
		}
		if(memberVO.getOldEmailID()==""){
			delMember=1;
		}else
		delMember=googleGroupUpdater.deleteMember(memberVO.getMemberGroupEmail(), memberVO.getOldEmailID());
		addMember=googleGroupUpdater.createMember(memberVO.getMemberGroupEmail(), memberVO.getEmail(), "member");
		
		/*if((delMember==0)||(addMember==0))
				revertBackMemberUpdate(memberVO, memberId);*/
		}
		if(memberVO.getAutoIncrement()){
			memberDAO.incrementMembershipNumber(memberVO.getSocietyID());
		}
		logger.debug("Exit : public int updateMemberDetails(MemberVO memberVO,String memberId)"+flag);
	}catch(Exception e){		
		logger.error("Exception in updateMemberDetails : "+e);
	}
	return flag;
}
	private int revertBackMemberUpdate(MemberVO memberVO,int memberId){
		int flag=0;
		
		try {
			logger.debug("Entry : public int revertBackMemberUpdate(MemberVO memberVO,String memberId)");
			logger.info("Here Old Email= "+memberVO.getOldEmailID()+" New EmailID "+memberVO.getEmail());
			memberVO.setEmail(memberVO.getOldEmailID());
			flag=memberDAO.updateMemberDetails(memberVO, memberId);
			
			
			logger.debug("Exit : public int revertBackMemberUpdate(MemberVO memberVO,String memberId)"+flag);
		}catch(Exception e){		
			logger.error("Exception in revertBackMemberUpdate : "+e);
		}
		return flag;	
		
	}
public int addVehicleDetails(VehicleVO vechicleVO,int memberId,int societyId ){
	int flag=0;
	try{
	    logger.debug("Entry : public int addVehicleDetails(VehicleVO vechicleVO,String memberId,String societyId )");
	    
	     flag=memberDAO.addVehicleDetails(vechicleVO,memberId,societyId);
	     
	    logger.debug("Exit : public int addVehicleDetails(VehicleVO vechicleVO,String memberId,String societyId )");
	
	}catch(Exception Ex){
		Ex.printStackTrace();
		logger.error("Exception in addVehicleDetails : "+Ex);
	}
	return flag;
}
public int deleteVehicleDetails(int vehicleId){
	
	int sucess = 0;
	try{	
	logger.debug("Entry : public int deleteVehicleDetails(String vehicleId)");
	
	sucess = memberDAO.deleteVehicleDetails(vehicleId);
			
	logger.debug("Exit : public int deleteVehicleDetails(String vehicleId)");

	}catch(Exception Ex){		
		Ex.printStackTrace();
		logger.error("Exception in deleteVehicleDetails : "+Ex);		
	}
	return sucess;
}

public VehicleVO getVehicleDetails(int vehicleID){
	
	VehicleVO vehicleVO = new VehicleVO();
	try{	
	logger.debug("Entry : public VehicleVO getVehicleDetails(String vehicleID)" );
	
	vehicleVO = memberDAO.getVehicleDetails(vehicleID);
			
	logger.debug("Exit : public VehicleVO getVehicleDetails(String vehicleID)");

	}catch(Exception Ex){		
		logger.error("Exception in getVehicleDetails : "+Ex);
	}
	return vehicleVO;
}

public int updateVehicleDetails(VehicleVO vehicleVO,int vehicleID){
	int success=0;
	try {
		logger.debug("Entry :public int updateVehicleDetails(VehicleVO vehicleVO,int vehicleID)");
		
		success=memberDAO.updateVehicleDetails(vehicleVO, vehicleID);		
		
		logger.debug("Exit :public int updateVehicleDetails(VehicleVO vehicleVO,int vehicleID))"+success);
	}catch(Exception e){		
		logger.error("Exception in updateVehicleDetails : "+e);
	}
	return success;
}

public int updateRenterDetails(RenterVO renterVO,int renterId){
	int flag=0;
	int agentflag=0;
	int addressflag=0;
	try {
		logger.info("Entry :public int updateRenterDetails(RenterVO renterVO,String renterId)"+renterVO.getRenterID());
		     
        if((renterVO.getAgentName().length() == 0)   && ((renterVO.getAgentName().length() == 0) )) {
        	renterVO.setAgentID(agentflag);	
			logger.debug("Agent ID: 0 ");        	
        }else if((renterVO.getAgentID()==0)  && ((renterVO.getAgentName().length() >0) ) ){
        	agentflag=memberDAO.addAgentDetails(renterVO);
        	logger.debug("Agent Added Id: "+agentflag);   
        	renterVO.setAgentID(agentflag);	
        }else if((renterVO.getAgentID()> 0) && (renterVO.getAgentName().length() > 0)  ){
			agentflag=memberDAO.updateAgentDetails(renterVO, renterVO.getAgentID());	
			logger.debug("Agent Updated Id: "+renterVO.getAgentID());  			
		}
        if(renterVO.getAddressID()==0 ){
        	renterVO.setAddressID(0);
        }
        AddressVO addressVO=new AddressVO();
		addressVO.setAddID(renterVO.getAddressID());
		addressVO.setAddLine1(renterVO.getAddress());
		addressVO.setCity(renterVO.getCity());
		addressVO.setState(renterVO.getState());
		addressVO.setZip(renterVO.getZip());
		addressVO.setCountryCode(renterVO.getCountryCode());
		addressVO.setCategoryID("1");
		addressVO.setPerson_category("R");
		addressVO.setMobile("0000000000");
		addressVO.setIsSocietyAddress(renterVO.getIsSocietyAddress());;
		//add renter address
		addressflag=addressServiceBean.addUpdateAddressDetails(addressVO, renterId);		
		logger.debug("Address Updated Status: "+addressflag); 
		
		flag=memberDAO.updateRenterDetails(renterVO, renterId);
		
		
		logger.debug("Exit :public int updateRenterDetails(RenterVO renterVO,String renterId)"+flag);
	}catch(Exception e){		
		logger.error("Exception in updateRenterDetails : "+e);
	}
	return flag;
}

public int updateAgentDetails(RenterVO renterVO,int agentId){
	int flag=0;
	try {
		logger.debug("Entry :public int updateAgentDetails(RenterVO renterVO,String agentId)");
		
		flag=memberDAO.updateAgentDetails(renterVO, agentId);		
		
		logger.debug("Exit :public int updateAgentDetails(RenterVO renterVO,String agentId)"+flag);
	}catch(Exception e){		
		logger.error("Exception in updateAgentDetails : "+e);
	}
	return flag;
}

public int addRenterDetails(RenterVO renterVO ){
	int flag=0;
	int agentID=0;
	try{
	logger.debug("Entry :public int addRenterDetails(RenterVO renterVO )");
	 if(renterVO.getAgentName()== null || renterVO.getAgentName().length() ==0 || renterVO.getAgentName().equalsIgnoreCase(" ")){
		 renterVO.setAgentID(agentID);			
	 }else{
		 agentID=memberDAO.addAgentDetails(renterVO);	
		 renterVO.setAgentID(agentID);
	 } 
	
	flag=memberDAO.addRenterDetails(renterVO);
	
	logger.debug("Exit :public int addRenterDetails(RenterVO renterVO )");
	}catch(Exception e){		
		logger.error("Exception in addRenterDetails : "+e);
	}
	return flag;
}

public int addRenterMemberDetails(RenterVO renterVO ){
	int flag=0;
	try{
	logger.debug("Entry :public int addRenterDetails(RenterVO renterVO )");
	
	flag=memberDAO.addRenterMemberDetails(renterVO);
	
	logger.debug("Exit :public int addRenterDetails(RenterVO renterVO )");
	}catch(Exception e){		
		logger.error("Exception in addRenterDetails : "+e);
	}
	return flag;
}

public int updateRenterMemberDetails(RenterVO renterVO,String renterId){
	int flag=0;
	try {
		logger.debug("Entry :public int updateRenterMemberDetails(RenterVO renterVO,String renterId)");
		
		flag=memberDAO.updateRenterMemberDetails(renterVO, renterId);
		
		logger.debug("Exit :public int updateRenterMemberDetails(RenterVO renterVO,String renterId)"+flag);
	}catch(Exception e){		
		logger.error("Exception in updateRenterMemberDetails : "+e);
	}
	return flag;
}


public List getRenterMemberDetails(int renterID){
	
	List rentrVOList=null;
		
	try {
		logger.debug("Entry : public RenterVO getRenterMemberDetails(String renterID)");
		
		
		rentrVOList = memberDAO.getRenterMemberDetails(renterID);
	}
	catch (Exception ex){
		logger.error("Exception in getRenterMemberDetails : "+ex);
	}
	
	logger.debug("Exit : public RenterVO getRenterMemberDetails(String apartmentID)");
	return rentrVOList;
}

public RenterVO getRenterDetails(int aptID , int renterID){
	
	RenterVO rentrVO=new RenterVO();
		
 try {
	 logger.debug("Entry: public RenterVO getRenterDetails(int aptID , int renterID)");
	 
	List rentrVOList=memberDAO.getRenterDetails(aptID,renterID);
	
	 	rentrVO=(RenterVO) rentrVOList.get(0);
		 
	logger.debug("Exit: public RenterVO getRenterDetails(String apartmentID)");
	
    }catch (IndexOutOfBoundsException e) {
	        logger.info("Sorry no details for renter are available at this time.");
    }catch (Exception ex) {
	       logger.error("Exception in getRenterDetails : "+ex);
    }
	return rentrVO;
}

public List getRenterHistoryDetails(int apartmentID){
	List rentrVOList=null;
 try {
	 
	 logger.debug("Entry: public List getRenterHistoryDetails(String apartmentID)");
	 
	 rentrVOList=memberDAO.getRenterHistoryDetails(apartmentID);
	 
	logger.debug("Exit: public List getRenterHistoryDetails(String apartmentID)");
	
    }catch (IndexOutOfBoundsException e) {
	     logger.info("Sorry no details for renter history are available at this time.");
    }catch (Exception ex) {
	     logger.error("Exception in getRenterHistoryDetails : "+ex);
    }

	return rentrVOList;
}



public int updateRenterNOCStatus(String renterId,String apartmentID,int nocStatus){
	
	int sucess = 0;
	try{	
	logger.debug("Entry : public int updateRenterNOCStatus(String renterId,String apartmentID)");
	
	sucess = memberDAO.updateRenterNOCStatus(renterId,apartmentID,nocStatus);
			
	logger.debug("Exit : public int updateRenterNOCStatus(String renterId,String apartmentID)");

	}catch(Exception Ex){		
		logger.error("Exception in updateRenterNOCStatus : "+Ex);		
	}
	return sucess;
}

public int deleteRenterDetails(int renterId,int apartmentID){
	
	int sucess = 0;
	try{	
	logger.debug("Entry : public int deleteRenterDetails(String renterId,String apartmentID)");
	
	sucess = memberDAO.deleteRenterDetails(renterId,apartmentID);
			
	logger.debug("Exit : public int deleteRenterDetails(String renterId,String apartmentID)");

	}catch(Exception Ex){		
		logger.error("Exception in deleteRenterDetails : "+Ex);		
	}
	return sucess;
}

public int deleteRenterMember(int renterMemberId) {

	int sucess = 0;
	try {
		logger.debug("Entry : public int deleteRenterMember(String renterMemberId)");

		sucess = memberDAO.deleteRenterMember(renterMemberId);

		logger.debug("Exit : public int deleteRenterMember(String renterMemberId)");

	}catch(Exception Ex){
		logger.error("Exception in deleteRenterMember : "+Ex);
	}
	return sucess;
}


public MemberVO getMemberNameDetails(String memeberID){
	MemberVO memberVo=new MemberVO();
	
	try {
		logger.debug("Entry : public MemberVO getMemberNameDetails(String memeberID)");
		
		
		memberVo = memberDAO.getMemberNameDetails(memeberID);
		
	} catch (DataAccessException e) {
	    logger.error("DataAccessException in getMemberNameDetails : "+e);
	}
	catch (Exception ex) {
		logger.error("Exception in getMemberNameDetails : "+ex);
	}
	
	logger.debug("Exit : public MemberVO getMemberNameDetails(String memeberID)");
	return memberVo;
}


public List getMemberListForSearchByName(String strSocietyID){
	
    List memberList = null;

	try{		
	    logger.debug("Entry : public List getMemberListForSearchByName(String strSocietyID)" );
	
	 
		    memberList = memberDAO.getMemberListForSearchByName(strSocietyID);
		   
	
	    logger.debug("Exit : public List getMemberListForSearchByName(String strSocietyID)" );
	}catch (Exception ex) {
		logger.error("Exception in getMemberListForSearchByName : "+ex);
	}
	return memberList;
	}

public List getMemberListOfApartmentsBuilding(int aptID){
	
    List memberList = null;

	try{		
	    logger.debug("Entry : public List getMemberListOfApartmentsBuilding(String aptID)" );
	    	
	    	MemberVO meberVo=memberDAO.getMemberInfo(aptID);
	 
		    memberList =getMemberListBuildingWise(meberVo.getBuildingID(), meberVo.getSocietyID());
		
	    	
	
	    logger.debug("Exit : public List getMemberListForSearchByName(String strSocietyID)" );
	}catch (Exception ex) {
		logger.error("Exception in getMemberListForSearchByName : "+ex);
	}
	return memberList;
	}

public List getMemberListBuildingWise(int buildingID,int societyID){
	
    List memberList = null;

	try{		
	    logger.debug("Entry : public List getMemberListBuildingWise(String buildingID)" );
	
	 
		    memberList = memberDAO.getMemberListBuildingWise(buildingID,societyID);	
	    	
	
	    logger.debug("Exit : public List getMemberListBuildingWise(String buildingID)" );
	}catch (Exception ex) {
		logger.error("Exception in getMemberListForMobileApp : "+ex);
	}
	return memberList;
	}




public List getVehicleListForSearch(String societyId){
	
    List vechicleList = null;	
	try{	
	logger.debug("Entry : public List getVehicleListForSearch(String societyId)" );
	
	vechicleList = memberDAO.getVehicleListForSearch(societyId);
			
	logger.debug("Exit : public List getVehicleListForSearch(String societyId)");

	}catch(Exception Ex){		
		Ex.printStackTrace();
		logger.error("Exception in getVehicleListForSearch : "+Ex);
	}
	return vechicleList;
}

public MemberVO getMemberInfo(int aptID){
	MemberVO memberVo=new MemberVO();
	AccountHeadVO achVO=new AccountHeadVO();
	try {
		logger.debug("Entry :public MemberVO getMemberInfo(String memberID)");
				
		memberVo = memberDAO.getMemberInfo(aptID);
		achVO=ledgerService.getLedgerID(aptID, memberVo.getSocietyID(), "M","APTID");
		memberVo.setLedgerID(achVO.getLedgerID());
		
	} catch (DataAccessException e) {
	    logger.error("DataAccessException in getMemberInfo : "+e);
	}
	catch (Exception ex) {
		logger.error("Exception in getMemberInfo : "+ex);
	}
	
	logger.debug("Exit : public MemberVO getMemberInfo(String memberID)");
	return memberVo;
}
public MemberWSVO getMembersInfoWithMail(String emailID){
	MemberWSVO memberVo=new MemberWSVO();
	
	try {
		logger.debug("Entry : public MemberWSVO getMembersInfoWithMail(String emailID)");
		
		
		memberVo = memberDAO.getMembersInfoWithMail(emailID);
		
	} catch (DataAccessException e) {
	    logger.error("DataAccessException in getMembersInfoWithMail : "+e);
	}
	catch (Exception ex) {
		logger.error("Exception in getMembersInfoWithMail : "+ex);
	}
	
	logger.debug("Exit : public MemberVO getMembersInfoWithMail(String memberID)");
	return memberVo;
}

public MemberVO getMemberInfoThroughLedgerID(int societyID,String ledgerID){
	MemberVO memberVo=new MemberVO();
	
	try {
		logger.debug("Entry :public MemberVO getMemberInfoThroughLedgerID(String ledgerID");
		
		
		memberVo = memberDAO.getMemberInfoThroughLedgerID(societyID,ledgerID);
		
	} catch (DataAccessException e) {
	    logger.error("DataAccessException in getMemberInfoThroughLedgerID(String ledgerID : "+e);
	}
	catch (Exception ex) {
		logger.error("Exception in getMemberInfoThroughLedgerID(String ledgerID : "+ex);
	}
	
	logger.debug("Exit : public MemberVO getMemberInfoThroughLedgerID(String ledgerID");
	return memberVo;
}

public List categoryWiseMemberTxDetails(int year, String societyId, String categoryID,int memberId) {
	List txCategorywise=null;
	try {
		logger.debug("Entry:public List categoryWiseMemberTxDetails(int year, String societyId, String categoryID,int memberId)" + year + categoryID);
		
		txCategorywise=memberDAO.categoryWiseMemberTxDetails(year, societyId, categoryID, memberId);
	}
	catch (Exception ex) {
		logger.error("Exception in categoryWiseMemberTxDetails : " + ex);
	}
	logger.debug("Exit:public List categoryWiseMemberTxDetails(int year, String societyId, String categoryID,int memberId)");
return txCategorywise;

}



public List getMemberDocumentList(int memberID){
	
    List memberDocsList = null;

	try{		
	    logger.debug("Entry : public List getMemberDocumentList(String memberID)" );
	
	    
	    memberDocsList = memberDAO.getMemberDocumentList(memberID);
	        		
	    logger.debug("Exit : public List getMemberDocumentList(String memberID)" );
	}catch (ClassCastException ex) {
	
	
	}catch(Exception Ex){			
		logger.error("Exception in getMemberDocumentList : "+Ex);
		
	}
	return memberDocsList;
}

public int addMemberDocument(RptDocumentVO memberVO){
	
   int flag=0 ;

	try{		
	    logger.debug("Entry : public int addMemberDocument(RptDocumentVO memberVO)" );
	
	    
	    flag = memberDAO.addMemberDocument(memberVO);
	        		
	    logger.debug("Exit : public int addMemberDocument(RptDocumentVO memberVO)" );
	}catch(Exception Ex){			
		logger.error("Exception in addMemberDocument : "+Ex);
		
	}
	return flag;
}

public int updateDocument(RptDocumentVO memberVO){
	
	   int flag=0 ;

		try{		
		    logger.debug("Entry : public int updateDocument(RptDocumentVO memberVO)" );
		    
		    if(memberVO.getDocumentMapID()>0){
		        flag = memberDAO.updateMemberDocument(memberVO);
		    }else{
		    	flag=memberDAO.addMemberDocument(memberVO);
		    }
		    	
		        		
		    logger.debug("Exit : public int updateDocument(RptDocumentVO memberVO)" );
			
		}catch(Exception Ex){			
			logger.error("Exception in updateDocument : "+Ex);
			
		}
		return flag;
	}

public int updateDocumentList(MemberVO memberVO){
	
	   int flag=0 ;

		try{		
		    logger.debug("Entry : public int updateDocumentList(MemberVO memberVO)" );
		    
		    for(int i=0;i<memberVO.getDocumentList().size();i++){
		    	MemberVO documentVO =(MemberVO)memberVO.getDocumentList().get(i);
		    
			    if((documentVO.getDocCategoryValue().equalsIgnoreCase("1")) && (documentVO.getMapId().length()==0) ){		    
			    //   flag = addMemberDocument(documentVO);
			    }else if(documentVO.getDocCategoryValue().equalsIgnoreCase("0")){
			    	//flag=memberDAO.deleteDocuments(documentVO);
			    }
			    	
		    }   		
		    logger.debug("Exit : public int updateDocumentList(MemberVO memberVO)" );
			
		}catch(Exception Ex){			
			logger.error("Exception in updateDocumentList : "+Ex);
			
		}
		return flag;
	}

public List getMemberMissingDocument(int memberID,int documentID){
	
    List memberDocsList = null;

	try{		
	    logger.debug("Entry : public List getMemberMissingDocument(int memberID,int documentID)" );
	
	    
	    memberDocsList = memberDAO.getMemberMissingDocument(memberID, documentID);
	        		
	    logger.debug("Exit :public List getMemberMissingDocument(int memberID,int documentID)" );
	}catch (ClassCastException ex) {
	
	
	}catch(Exception Ex){			
		logger.error("Exception in public List getMemberMissingDocument(int memberID,int documentID) : "+Ex);
		
	}
	return memberDocsList;
}


public List getDocumentMissingList(int buildingID,int societyId,int documentID) {

	List memberList=null;
	
	
	try {
		logger.debug("Entry : public List getDocumentMissingList(int buildingID,int societyId,int documentID)"+societyId);

	
		memberList=  memberDAO.getDocumentMissingList(buildingID, societyId, documentID);

		
		logger.debug("Exit:	public List getDocumentMissingList(int buildingID,int societyId,int documentID)");
	} catch (NullPointerException ex) {
		logger.error("Exception in public List getDocumentMissingList(int buildingID,int societyId,int documentID) : "+ex);

		
	} catch (Exception ex) {
		logger.error("Exception in public List getDocumentMissingList(int buildingID,int societyId,int documentID) : "+ex);

	}
	return memberList;

}

public int transferMember(InsertMemberVO memberVO,int aptID) {

		int sucess = 0;
		try {
			logger.debug("Entry : public int transferMember(InsertMemberVO memberVO,int aptID)");

			sucess = addOldMembers(aptID, memberVO);

			logger.debug("Exit : public int transferMember(InsertMemberVO memberVO,int aptID)");

		}catch (Exception Ex) {
			logger.error("Exception in public int transferMember(InsertMemberVO memberVO,int aptID): "+Ex);
		}
		return sucess;
	}


public DropDownVO checkTransferMember(String txID,String memberID,String societyID,String type) {

		DropDownVO dropDownVO = null;
	
		try {
			logger.debug("Entry : public int checkTransferMember(String txID,String memberID)");
			
			dropDownVO = memberDAO.checkTransferMember(txID,memberID, societyID);
			
			
			
			logger.debug("Exit : public int checkTransferMember(String txID,String memberID)");

		}catch (Exception Ex) {
			logger.error("Exception in public int checkTransferMember(String txID,String memberID): "+Ex);
		}
		return dropDownVO;
	}


	public int confirmMemberTransfer(int apartmentID,InsertMemberVO memberVO) {

		int sucess = 0;
		try {
			logger.debug("Entry : confirmMemberTransfer(String apartmentID)");

			sucess=addOldMembers(apartmentID,memberVO);

			logger.debug("Exit : public int confirmMemberTransfer(String apartmentID)");

		}catch (Exception Ex) {
			logger.error("Exception in public int confirmMemberTransfer(String apartmentID): "+Ex);
		}
		return sucess;
	}	

	public int addOldMembers(int apartmentID,InsertMemberVO memberVO) {

		int success = 0;
		int addOldMembers=0;
		int deleteMember=1;
		int addNewMember=0;
		int deleteMemberFromGroup=0;
		int addMemberToGroup=0;
		int membershipNo=0;
		List memberList=null;
		DateUtility dateUtil=new DateUtility();
		try {
			logger.debug("Entry : addOldMembers(String apartmentID)");

			memberList=memberDAO.getOldMembers(apartmentID);
			
			
			if(memberList.size()>0){
				addOldMembers=memberDAO.insertOldMembersToHistory(memberList, apartmentID,memberVO);
				//for loop for deleteing members from the groups
				for(int i=0;memberList.size()>i;i++){
					InsertMemberVO delMemberVO= (InsertMemberVO) memberList.get(i);
					/*
					if(delMemberVO.getIsCommitteeMember()==1){
						//deleteMemberFromGroup=googleGroupUpdater.deleteMember(memberVO.getCommitteeGroupMail(), delMemberVO.getEmail());
					}*/
					if(delMemberVO.getEmail().length()>0)
					  deleteMemberFromGroup=googleGroupUpdater.deleteMember(memberVO.getMemberGroupMail(), delMemberVO.getEmail());
				}
			}
			
			if(addOldMembers!=0){
				deleteMember=memberDAO.delMember(apartmentID);
			}
			
			if(deleteMember!=0){
				 if(memberVO.getAutoIncrement()){
		    		   membershipNo=memberDAO.getIncrementMembershipNumber(Integer.parseInt(memberVO.getSocietyID()));
		    		   memberVO.setMembershipID(membershipNo+"");
		           	 }
				 
				addNewMember=memberDAO.insertNewMember(memberVO, apartmentID);
				
			if((addNewMember!=0)&& (memberVO.getType().equalsIgnoreCase("P"))){	
				AccountHeadVO achVO=new AccountHeadVO();
	 			achVO.setOpeningBalance(BigDecimal.ZERO);
	 			achVO.setLedgerType("C");
	 			achVO.setAccountGroupID(1);
	 			achVO.setStrAccountHeadName(memberVO.getUnitName()+" - "+memberVO.getTitle()+" "+memberVO.getFullName());
	 			Date today = new Date();
			    Calendar cal = Calendar.getInstance();
			    cal.setTime(today); // don't forget this if date is arbitrary
			    int month = cal.get(Calendar.MONTH); // 0 being January
			    int year = cal.get(Calendar.YEAR);
			    
			    if(month<=2){
			    	year=year-1;
			    }
			    
			    String date=year+"-03-31";
			    achVO.setEffectiveDate(date);
	 			achVO.setCreateDate(dateUtil.findCurrentDate().toString());
	 			achVO.setOrgID(Integer.parseInt(memberVO.getSocietyID()));
	 			achVO.setAccType("Regular");	 			
	 			
	 			achVO=ledgerService.insertLedger(achVO, Integer.parseInt(memberVO.getSocietyID()));
	 			if(achVO.getStatusCode()==200){
	 			  societyServiceBean.insertLedgerUserMapper(addNewMember, achVO.getLedgerID(), "M",Integer.parseInt(memberVO.getSocietyID()));
	 			}
			}
	 			if((addNewMember!=0) && ((memberVO.getEmail().length()>0) || (memberVO.getMemberGroupMail().length()>0)) ){
				addMemberToGroup=googleGroupUpdater.createMember(memberVO.getMemberGroupMail(), memberVO.getEmail(), "member");
	 			}
	 			
				if((addNewMember!=0) && (memberVO.getAutoIncrement())){
				  memberDAO.incrementMembershipNumber(Integer.parseInt(memberVO.getSocietyID()));
				}
			  
			}
			if((memberList.size()!=0)&&(addOldMembers!=0)&&(deleteMember!=0)&&(addNewMember!=0)){
				success=1;
			}
			
			logger.debug("Exit : public int addOldMembers(String apartmentID)");

		}catch (Exception Ex) {
			logger.error("Exception in public int addOldMembers(String apartmentID): "+Ex);
		}
		return success;
	}	
	
	public List getMembershipNoList(String societyID){
    	List memberList=null;
    	
    	try {
    	
    	
    	memberList =  memberDAO.getMembershipNoList(societyID);
    		
    	logger.debug("Exit : public List getMembershipNoList(String soceityID)"+memberList.size());
    	} catch (DataAccessException e) {
    	    logger.error("DataAccessException in public List getMembershipNoList(String soceityID) : "+e);
    	}
    	catch (Exception ex) {
    		logger.error("Exception in public List getMembershipNoList(String soceityID) : "+ex);
    	}
    	return memberList;
    }
	
	public List convertMemberListForWebservice(List memberList){
		List WsMemberList=new ArrayList();
		logger.debug("Entry : public List convertMemberListForWebservice(List MemberList)");
		
		if(memberList.size()>0){
			for(int i=0;memberList.size()>i;i++){
				MemberVO memberVO=(MemberVO) memberList.get(i);
				MemberWSVO wsMemberVO=new MemberWSVO();
				wsMemberVO.setAge(memberVO.getAge());
				wsMemberVO.setAptID(memberVO.getAptID());
				wsMemberVO.setAptType(memberVO.getAptType());
				wsMemberVO.setArea(memberVO.getArea());
				wsMemberVO.setBuildingID(memberVO.getBuildingID());
				wsMemberVO.setComitteeGroupEmail(memberVO.getComitteeGroupEmail());
				wsMemberVO.setDateOfBirth(memberVO.getDateOfBirth());
				wsMemberVO.setEmail(memberVO.getEmail());
				wsMemberVO.setFlatNo(memberVO.getFlatNo());
				wsMemberVO.setFullName(memberVO.getFullName());
				wsMemberVO.setIntercom(memberVO.getIntercom());
				wsMemberVO.setIs_Rented(memberVO.getIsRented());
				wsMemberVO.setMemberGroupEmail(memberVO.getMemberGroupEmail());
				wsMemberVO.setLedgerID(memberVO.getLedgerID());
				wsMemberVO.setMemberID(memberVO.getMemberID());
				wsMemberVO.setMembershipDate(memberVO.getMembershipDate());
				wsMemberVO.setMembershipID(memberVO.getMembershipID());
				wsMemberVO.setMobile(memberVO.getMobile());
				wsMemberVO.setOccupation(memberVO.getOccupation());
				wsMemberVO.setPhone(memberVO.getPhone());
				wsMemberVO.setRenterDetails(memberVO.getRenterDetails());
				wsMemberVO.setSocietyID(memberVO.getSocietyID());
				wsMemberVO.setSocietyMMC(memberVO.getSocietyMMC());
				wsMemberVO.setTitle(memberVO.getTitle());
				WsMemberList.add(i, wsMemberVO);				
				
			}
			
		}
		
		logger.debug("Exit : public List convertMemberListForWebservice(List MemberList)");
		return WsMemberList;
	}
	
	 public List getExpiredtenantDetails(int societyID,String uptoDate,Boolean aboutToExpire) {
			List tenantList=new ArrayList();
			try {
				logger
						.debug("Entry : public List getExpiredtenantDetails(int intSocietyID) ");

				tenantList = memberDAO.getExpiredtenantDetails(societyID, uptoDate,aboutToExpire);

			} catch (EmptyResultDataAccessException e) {
				logger.info("No tenant details available for this society");
			} catch (IndexOutOfBoundsException ex) {
				logger.info("No data available for this society : " + ex);
			}

			catch (Exception ex) {
				logger.error("Exception in getExpiredtenantDetails : " + ex);
			}

			logger.debug("Exit : public List getExpiredtenantDetails(int intSocietyID) "
					+ tenantList.size());
			return tenantList;
		}

	public int insertTenantForWebservice(RenterVO renterVO) {
		int flag = 0;
		int addressflag=0;
		int checkRenterFlag=0;
		int agentID=0;
		int renterID=0;
		RenterVO renterDetails=new RenterVO();
        try{
		 logger.debug("Entry :public int insertTenantForWebservice(RenterVO renterVO)");
		 
		 //check renter present or not
		 checkRenterFlag=memberDAO.checkRenterDetails(renterVO.aptID);      
		
		 renterVO.setIsDeleted("0");
				 
		 // add renter details
		 flag = addRenterDetails(renterVO);		
		 
		if(flag > 0){
			renterID=flag;
			logger.info("insertTenantForWebservice Apartment ID "+renterVO.getAptID()+" has tenant details added sucessfully.");
			
			// get added renter info
			//renterDetails= getRenterDetails(renterVO.getAptID(),renterVO.getRenterID());
			
			if(renterID !=0){
				
				logger.info("Inserted Renter ID: +"+renterID);
						   
				AddressVO addressVO=new AddressVO();
				addressVO.setAddID(0);
				addressVO.setAddLine1(renterVO.getAddress());
				addressVO.setCity(renterVO.getCity());
				addressVO.setState(renterVO.getState());
				addressVO.setZip(renterVO.getZip());
				addressVO.setCountryCode(renterVO.getCountryCode());
				addressVO.setCategoryID("1");
				addressVO.setPerson_category("R");
				addressVO.setMobile("0000000000");
				addressVO.setIsSocietyAddress(false);
				
				//add renter address
				addressflag=addressServiceBean.addUpdateAddressDetails(addressVO, renterID);
				
				if(addressflag == 0){
					logger.info("Renter ID: +"+renterID+" Unable to add address to renter");
					//flag=0;
				}else{
					//flag=1;
					logger.info("Renter ID: +"+renterID+" has been added address successfully");
				}
			}
			
			
		}else{
			flag=0;
		}
		
		
		   logger.debug("Exit :	public int insertTenantForWebservice(RenterVO renterVO)");
        }catch(Exception e){
			logger.error("Exception in insertTenantForWebservice : "+e);
		}
		return flag;
	}
	
	public List getMembersContactList(String societyID, String memberType,String infoMissingType) {

		List societyMemeberList = null;
		
		try {			
			logger.debug("Entry : public List getMembersContactList(String societyID, String memberType,String infoMissingType)");

			societyMemeberList = memberDAO.getMembersContactList(societyID, memberType, infoMissingType);

			logger.debug("Exit : public List getMembersContactList(String societyID, String memberType,String infoMissingType)");

		}catch(Exception Ex){
			logger.error("Exception in getMembersContactList : "+Ex);
		}
		return societyMemeberList;
	}
	
	
	public TicketVO getTicketList(int orgID,int userID,String statusType){
		
		String apiURL="";
		String apiStatus="";
		TicketVO ticketsVO=new TicketVO();
		ConfigManager c=new ConfigManager();
		UserVO userDetailsVO =new UserVO();
		logger.debug("Entry : private TicketVO getTicketStatus(String societID,int orgID,String statusType)"+userID+statusType);
		
		try {
			
			if(orgID==0){
				userDetailsVO=userService.getUserDetailsByID(Integer.toString(userID));
				logger.debug("Email: "+userDetailsVO.getLoginName());
			}
						
			char quotes ='"';
			 URL url;			
			 apiURL=c.getPropertiesValue("ticket.apiURL")+"/tickets";
	       
	        String data ="{"+quotes+URLEncoder.encode("Status", "UTF-8") +quotes+ ":" +quotes+ URLEncoder.encode(statusType, "UTF-8")+quotes;
		    data += "," +quotes+ URLEncoder.encode("OrgID", "UTF-8")+quotes + ":" +quotes+ URLEncoder.encode(""+orgID, "UTF-8")+quotes;
		    data += "," +quotes+ URLEncoder.encode("EmailID", "UTF-8")+quotes + ":" +quotes+ userDetailsVO.getLoginName()+quotes+"}";
		    logger.debug("Post parameters : " + data);
		   
			
				url = new URL(apiURL);
			
				//HttpURLConnection con = (HttpURLConnection) url.openConnection();
		    	HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

				//add reuqest header
				con.setRequestMethod("POST");
				con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				
				//data={"Status":"mgmt","OrgID":"2"};
				// Send post request
				con.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(con.getOutputStream());
				wr.writeBytes(data);
				wr.flush();
				wr.close();
				
				int responseCode = con.getResponseCode();
				logger.debug("\nSending 'POST' request to URL : " + url);
				logger.debug("Post parameters : " + data);
				logger.debug("Response Code : " + responseCode);
				
				if(responseCode==200){
				    BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
				    String line;
				    while ((line = rd.readLine()) != null) {
				    	 JSONParser j = new JSONParser();
			             JSONObject o = (JSONObject)j.parse(line);
			             String json=o.toString();			          
			             
			             // Now do the magic.
			             ticketsVO = new Gson().fromJson(json, TicketVO.class);		
			        }
		   
				}
		
		   
	    }catch(Exception e) {
	    	logger.error("Exception occured in getTicketList "+e);
	    }   
		
		
		logger.debug("Exit : private TicketVO getTicketList(String societID,int orgID,String statusType)");
		return ticketsVO;
	}
	
    public TicketVO getTicketDetails(int ticketNO,String statusType){
		
		String apiURL="";
		ConfigManager c=new ConfigManager();
		TicketVO ticketsVO=new TicketVO();
		logger.debug("Entry :  public TicketVO getTicketDetails(int ticketNO,String statusType)"+ticketNO+statusType);
		
		try {
			
			char quotes ='"';
			 URL url;			
			 apiURL=c.getPropertiesValue("ticket.apiURL")+"/ticket";
	        
	        String data ="{"+quotes+URLEncoder.encode("Status", "UTF-8") +quotes+ ":" +quotes+ URLEncoder.encode(statusType, "UTF-8")+quotes;
		    data += "," +quotes+ URLEncoder.encode("TicketNo", "UTF-8")+quotes + ":" +quotes+ URLEncoder.encode(""+ticketNO, "UTF-8")+quotes+"}";
		    logger.debug("Post parameters : " + data);
		   
			
				url = new URL(apiURL);
			
				//HttpURLConnection con = (HttpURLConnection) url.openConnection();
		    	HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

				//add reuqest header
				con.setRequestMethod("POST");
				con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				
				//data={"Status":"mgmt","OrgID":"2"};
				// Send post request
				con.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(con.getOutputStream());
				wr.writeBytes(data);
				wr.flush();
				wr.close();
				
				int responseCode = con.getResponseCode();
				logger.debug("\nSending 'POST' request to URL : " + url);
				logger.debug("Post parameters : " + data);
				logger.debug("Response Code : " + responseCode);
				
				if(responseCode==200){
				    BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
				    
				    logger.info("rd "+rd);
				    String line;
				    while ((line = rd.readLine()) != null) {
				    	 JSONParser j = new JSONParser();
			             JSONObject o = (JSONObject)j.parse(line);
			             String json=o.toString();			          
			             
			             // Now do the magic.
			             ticketsVO = new Gson().fromJson(json, TicketVO.class);		
			        }
		   
				}
		
		   
	    }catch(Exception e) {
	    	logger.error("Exception occured in getTicketDetails "+e);
	    }   
		
		
		logger.debug("Exit : public TicketVO getTicketDetails(int ticketNO,String statusType)");
		return ticketsVO;
	}
    
    
public int createTicket(TicketVO requestVO){
		
		String apiURL="";
		int success = 0;
		TicketVO ticketsVO=new TicketVO();
		ConfigManager c=new ConfigManager();
		logger.debug("Entry :  public int createTicket(TicketVO requestVO)");
		
		try {
			UserVO userDetailsVO=userService.getUserDetailsByID(Integer.toString(requestVO.getUser_id()));
			char quotes ='"';
			 URL url;			
			 apiURL=c.getPropertiesValue("ticket.apiURL")+"/tickets/create";
			
	        String data ="{"+quotes+URLEncoder.encode("name", "UTF-8") +quotes+ ":" +quotes+ URLEncoder.encode(userDetailsVO.getFullName(), "UTF-8")+quotes;
		    data += "," +quotes+ URLEncoder.encode("email", "UTF-8")+quotes + ":" +quotes+ userDetailsVO.getLoginName()+quotes;
		    data += "," +quotes+ URLEncoder.encode("subject", "UTF-8")+quotes + ":" +quotes+ URLEncoder.encode(requestVO.getSubject(), "UTF-8")+quotes;
		    data += "," +quotes+ URLEncoder.encode("message", "UTF-8")+quotes + ":" +quotes+ URLEncoder.encode(requestVO.getMessage(), "UTF-8")+quotes;
		    data += "," +quotes+ URLEncoder.encode("topicId", "UTF-8")+quotes + ":" +quotes+ requestVO.getTopic_id()+quotes+"}";
		    logger.debug("Post parameters : " + data);
		   
			
				url = new URL(apiURL);
			
				//HttpURLConnection con = (HttpURLConnection) url.openConnection();
		    	HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

				//add reuqest header
				con.setRequestMethod("POST");
				con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				
				//data={"name":"Amit","email":"abc@e.com","subject":"abc","message":"abc","topicId":"1"};
				// Send post request
				con.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(con.getOutputStream());
				wr.writeBytes(data);
				wr.flush();
				wr.close();
				
				int responseCode = con.getResponseCode();
				logger.debug("\nSending 'POST' request to URL : " + url);
				logger.debug("Post parameters : " + data);
				logger.debug("Response Code : " + responseCode);
				
				if(responseCode==200){
				    BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
				    success=1;
				    
				    String line;
				    while ((line = rd.readLine()) != null) {
				    	 JSONParser j = new JSONParser();
			             JSONObject o = (JSONObject)j.parse(line);
			             String json=o.toString();			          
			             
			             // Now do the magic.
			             ticketsVO = new Gson().fromJson(json, TicketVO.class);		
			        }
		   
				}else{
					success=0;
				}
		
		   
	    }catch(Exception e) {
	    	logger.error("Exception occured in createTicket "+e);
	    }   
		
		
		logger.debug("Exit : public int createTicket(TicketVO requestVO)");
		return success;
	}


public int createSupportTicket(TicketVO requestVO){
	
	String apiURL="";
	int success = 0;
	TicketVO ticketsVO=new TicketVO();
	ConfigManager c=new ConfigManager();
	logger.debug("Entry :  public int createSupportTicket(TicketVO requestVO)");
	NotificationVO notificationVO=new NotificationVO();
	EmailMessage emailVO=new EmailMessage();
	List emailList=new ArrayList<EmailMessage>();
	NotificationVO userNotificationVO=new NotificationVO();
	EmailMessage userEmailVO=new EmailMessage();
	List userEmailList=new ArrayList<EmailMessage>();
	try {
		
		
		logger.info("New Support Ticket Org: "+requestVO.getOrgName()+" Email: "+requestVO.getEmail());
		
	 if(requestVO.getType().equalsIgnoreCase("mobile")){
		 //Create Support Ticket
		 emailVO.setDefaultSender("Helpdesk");
		 emailVO.setEmailTO("helpdesk@esanchalak.com");
		 emailVO.setSubject("["+requestVO.getOrgName()+"] New Support Request");
		 String message="";
		 message="<html><head></head>"
            +"<body >"
            +"<table  align='left' border='0'  style='font-family:OpenSans-Light, Helvetica Neue, Helvetica,Calibri, Arial, sans-serif;line-height:1.6;'> "
		    +"<tr>"
		    +"<td  >"
		    
		 	+" <table  align='center' cellpadding='0' cellspacing='0' style='font-family:OpenSans-Light,Calibri, Arial, sans-serif;font-size:14px;'  bgcolor='#FFFFFF'>"
			  +"<tbody>"
				+"<tr>"
				+"<td style='padding-top:5px;padding-left:15px;padding-right:15px;padding-bottom:15px'>"
				+" <p ><strong>Dear Support Team, </strong></p>"

				+" <p >Request Details:-</p>"
				+" <p >Name: "+requestVO.getName()+" <br>"
				+ " E-Mail: "+requestVO.getEmail()+" <br>"
				+ " Mobile: "+requestVO.getMobile()+" <br>"
				+ " Organization: "+requestVO.getOrgName()+" <br> "				
				+ " Flat No: "+requestVO.getUnitName()+" <br> "
				+ " Message: "+requestVO.getMessage()+" <br>"
				+ "</p>"

				+"	<p style='font-size:13px;'>Regards,<br />"
				+"	Support Team Esanchalak</p>"
				+"</td>"
				+"</tr>"			
			+"</tbody>"		
			+"</table>"
			
            +"<td>"				
            +"<tr>"
		   +"</table>"
		+"</body>"
		+"</html>"	;
		 
		 emailVO.setMessage(message);	
	     emailList.add(emailVO);	    
	     
	     notificationVO.setEmailList(emailList);
	 	 notificationVO.setSendEmail(true);
	 	 notificationVO.setSendSMS(false);	 	 
	 	 notificationVO=notificationService.SendNotifications(notificationVO);
	 	 
	 }else if(requestVO.getType().equalsIgnoreCase("web")){
		 
		 emailVO.setDefaultSender("Helpdesk");
		 emailVO.setEmailTO("helpdesk@esanchalak.com");
		 emailVO.setSubject("["+requestVO.getOrgName()+"] New Organization Enquiry");
		 String message="";
		 message="<html><head></head>"
            +"<body >"
            +"<table  align='left' border='0'  style='font-family:OpenSans-Light, Helvetica Neue, Helvetica,Calibri, Arial, sans-serif;line-height:1.6;'> "
		    +"<tr>"
		    +"<td  >"
		    
		 	+" <table  align='center' cellpadding='0' cellspacing='0' style='font-family:OpenSans-Light,Calibri, Arial, sans-serif;font-size:14px;'  bgcolor='#FFFFFF'>"
			  +"<tbody>"
				+"<tr>"
				+"<td style='padding-top:5px;padding-left:15px;padding-right:15px;padding-bottom:15px'>"
				+" <p ><strong>Dear Sales Team, </strong></p>"

				+" <p >Enquiry Details:-</p>"
				+" <p >Name: "+requestVO.getName()+" <br>"
				+ " E-Mail: "+requestVO.getEmail()+" <br>"
				+ " Mobile: "+requestVO.getMobile()+" <br>"
				+ " Organization: "+requestVO.getOrgName()+" <br> "	
				+ " No. of Units: "+requestVO.getNoOfUnits()+" <br> "
				+ " City: "+requestVO.getCity()+" <br> "
				+ " Message: "+requestVO.getMessage()+" <br>"
				+ "</p>"

				+"	<p style='font-size:13px;'>Regards,<br />"
				+"	Marketing Team Esanchalak</p>"
				+"</td>"
				+"</tr>"				
			+"</tbody>"		
			+"</table>"
			
            +"<td>"				
            +"<tr>"
		   +"</table>"
		+"</body>"
		+"</html>"	;
		 
		 emailVO.setMessage(message);	
	     emailList.add(emailVO);	    
	     
	     notificationVO.setEmailList(emailList);
	 	 notificationVO.setSendEmail(true);
	 	 notificationVO.setSendSMS(false);	 	 
	 	 notificationVO=notificationService.SendNotifications(notificationVO);	 
		 
	 }
	 	
	 
	 //Send notification to user	 	
	 	userEmailVO.setDefaultSender("Helpdesk");
	 	userEmailVO.setEmailTO(requestVO.getEmail());
	 	userEmailVO.setSubject("Helpdesk Ticket Opened ");
	 	
		String message1="";
		 message1="<html><head></head>"
            +"<body width='600' >"
            +"<table  align='center' style='min-width:300px;max-width:600px;font-family:OpenSans-Light, Helvetica Neue, Helvetica,Calibri, Arial, sans-serif;line-height:1.6;'> "
		    +"<tr>"
		    +"<td  style='border:1px solid #f0f0f0;' >"
		    
		 	+" <table  align='center' cellpadding='0' cellspacing='0' style='font-family:OpenSans-Light,Calibri, Arial, sans-serif;font-size:14px;'  bgcolor='#FFFFFF'>"
			  +"<tbody>"
				+"<tr>"
				+"<td style='padding-top:5px;padding-left:15px;padding-right:15px;padding-bottom:15px'>"
				+" <p ><strong>Dear "+requestVO.getName()+", </strong></p>"

                +" <p >Thank for contacting helpdesk. A request for support has been created. A representative will follow-up with you as soon as possible.</p>"
                
				+"	<p style='font-size:13px;'>Sincerely,<br />"
				+"	Team Esanchalak</p>"
				+"</td>"
				+"</tr>"
				+"<tr>"
				+  "<td align='center' style='padding:10px;vertical-align:top;display:block;font-size:11px; font-weight:300; font-family: OpenSans, helvetica, sans-serif;' bgcolor='#F7F7F7'>&nbsp;&copy; 2018. All rights reserved</td>"
			    +"</tr>"
			+"</tbody>"		
			+"</table>"
			
            +"<td>"				
            +"<tr>"
		   +"</table>"
		+"</body>"
		+"</html>";
		 
		 userEmailVO.setMessage(message1);	
	     userEmailList.add(userEmailVO);	  
	     
	     userNotificationVO.setEmailList(userEmailList);
	     userNotificationVO.setSendEmail(true);
	     userNotificationVO.setSendSMS(false);	 	 
	     userNotificationVO=notificationService.SendNotifications(userNotificationVO);
	
	     success=1;
	     
    }catch(Exception e) {
    	logger.error("Exception occured in createSupportTicket "+e);
    }   
	
	
	logger.debug("Exit : public int createSupportTicket(TicketVO requestVO)");
	return success;
}

public TicketVO getTicketHelpTopics(String userType){
	
	String apiURL="";
	
	TicketVO ticketsVO=new TicketVO();
	ConfigManager c=new ConfigManager();
	logger.debug("Entry :  public TicketVO getTicketHelpTopics(String userType)");
	
	try {
	//userType: 0:member topic ,1: all topic
		char quotes ='"';
		 URL url;			
		apiURL=c.getPropertiesValue("ticket.apiURL")+"/tickets/topics";
        
        String data ="{"+quotes+URLEncoder.encode("UserType", "UTF-8") +quotes+ ":" +quotes+ URLEncoder.encode(userType, "UTF-8")+quotes+"}";
	    logger.debug("Post parameters : " + data);
	   
		
			url = new URL(apiURL);
		
			//HttpURLConnection con = (HttpURLConnection) url.openConnection();
	    	HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			//add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			
			
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(data);
			wr.flush();
			wr.close();
			
			int responseCode = con.getResponseCode();
			logger.debug("\nSending 'POST' request to URL : " + url);
			logger.debug("Post parameters : " + data);
			logger.debug("Response Code : " + responseCode);
			
			if(responseCode==200){
			    BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));			    
			    
			    String line;
			    while ((line = rd.readLine()) != null) {
			    	 JSONParser j = new JSONParser();
		             JSONObject o = (JSONObject)j.parse(line);
		             String json=o.toString();			          
		             
		             // Now do the magic.
		             ticketsVO = new Gson().fromJson(json, TicketVO.class);		
		        }
	   
			}
	
	   
    }catch(Exception e) {
    	logger.error("Exception occured in getTicketHelpTopics "+e);
    }   
	
	
	logger.debug("Exit : public TicketVO getTicketHelpTopics()");
	return ticketsVO;
}


public TicketVO getOrgRulesAndRegulations(int orgID,String type){
	
	String apiURL="";
	ConfigManager c=new ConfigManager();
	TicketVO ticketsVO=new TicketVO();
	logger.debug("Entry :  public TicketVO getOrgRulesAndRegulations(int orgID,String type)");
	
	try {
		
		char quotes ='"';
		 URL url;			
		 
		 apiURL=c.getPropertiesValue("ticket.apiURL")+"/posts/rules"; 
		        
        String data ="{"+quotes+URLEncoder.encode("OrgId", "UTF-8") +quotes+ ":" +quotes+ orgID+quotes;
	    data += "," +quotes+ URLEncoder.encode("Type", "UTF-8")+quotes + ":" +quotes+ URLEncoder.encode(type, "UTF-8")+quotes+"}";
	    logger.debug("Post parameters : " + data);
	   
		
			url = new URL(apiURL);
		
			//HttpURLConnection con = (HttpURLConnection) url.openConnection();
	    	HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			//add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			
			//data={"Status":"mgmt","OrgID":"2"};
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(data);
			wr.flush();
			wr.close();
			
			int responseCode = con.getResponseCode();
			logger.debug("\nSending 'POST' request to URL : " + url);
			logger.debug("Post parameters : " + data);
			logger.debug("Response Code : " + responseCode);
			
			if(responseCode==200){
			    BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
			    			    
			    String line;
			    while ((line = rd.readLine()) != null) {
			    	 JSONParser j = new JSONParser();
		             JSONObject o = (JSONObject)j.parse(line);
		             String json=o.toString();			          
		             
		             // Now do the magic.
		             ticketsVO = new Gson().fromJson(json, TicketVO.class);	
		             logger.debug("json "+json);
		        }
	   
			}
	
	   
    }catch(Exception e) {
    	logger.error("Exception occured in getOrgRulesAndRegulations "+e);
    }   
	
	
	logger.debug("Exit : public TicketVO getOrgRulesAndRegulations(int orgID,String type)");
	return ticketsVO;
}



public int createUserInTicketPortal(TicketVO requestVO){
	
	String apiURL="";
	int success = 0;
	TicketVO ticketsVO=new TicketVO();
	ConfigManager c=new ConfigManager();
	logger.debug("Entry :  public int createUserInTicketPortal(TicketVO requestVO)");
	
	try {
		
		char quotes ='"';
		 URL url;			
		 apiURL=c.getPropertiesValue("ticket.apiURL")+"/tickets/create/user";
		
        String data ="{"+quotes+URLEncoder.encode("name", "UTF-8") +quotes+ ":" +quotes+ URLEncoder.encode(requestVO.getName(), "UTF-8")+quotes;
	    data += "," +quotes+ URLEncoder.encode("email", "UTF-8")+quotes + ":" +quotes+ requestVO.getEmail()+quotes;
	    data += "," +quotes+ URLEncoder.encode("orgId", "UTF-8")+quotes + ":" +quotes+ requestVO.getOrg_id()+quotes+"}";
	    logger.debug("Post parameters : " + data);
	   
		
			url = new URL(apiURL);
		
			//HttpURLConnection con = (HttpURLConnection) url.openConnection();
	    	HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			//add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			
			//data={"name":"Amit","email":"abc@e.com","orgId":"11"};
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(data);
			wr.flush();
			wr.close();
			
			int responseCode = con.getResponseCode();
			logger.debug("\nSending 'POST' request to URL : " + url);
			logger.debug("Post parameters : " + data);
			logger.debug("Response Code : " + responseCode);
			
			if(responseCode==200){
			    BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
			    success=1;
			    
			    String line;
			    while ((line = rd.readLine()) != null) {
			    	 JSONParser j = new JSONParser();
		             JSONObject o = (JSONObject)j.parse(line);
		             String json=o.toString();			          
		             
		             // Now do the magic.
		             ticketsVO = new Gson().fromJson(json, TicketVO.class);		
		        }
	   
			}else{
				success=0;
			}
	
	   
    }catch(Exception e) {
    	success=0;
    	logger.error("Exception occured in createUserInTicketPortal "+e);
    }   
	
	
	logger.debug("Exit : public int createSupportTicket(TicketVO requestVO)");
	return success;
}

public NotificationVO sendSMSandPushNotificationsToAGroup(SMSMessage smsVO){
	
	NotificationVO notificationVO=new NotificationVO();
	logger.debug("Entry : public NotificationVO sendSMSandPushNotificationsToAGroup(SMSMessage smsVO)");
	
	try {
		notificationVO.setOrgID(smsVO.getOrgID());
		if(smsVO.getGroupName().equalsIgnoreCase("tenant")){
			List tenantList=reportServiceBean.tenantDetailsRpt(smsVO.getOrgID());
			List smsList=new ArrayList();
			
			
			for(int i=0;tenantList.size()>i;i++){
				RptTenantVO tenantVO=(RptTenantVO) tenantList.get(i);
				if((tenantVO.getMobile()!=null)&&(!tenantVO.getMobile().equalsIgnoreCase("0000000000"))){
				SMSMessage smsObjVO=new SMSMessage();
				smsObjVO.setMessage(smsVO.getMessage());
				smsObjVO.setMobileNumber(tenantVO.getMobile());
				smsList.add(smsObjVO);
				}
			}
			notificationVO.setSmsList(smsList);
			
		}else{
			
			List memberList=memberDAO.getMembersListFromGroup(smsVO.getOrgID(), smsVO.getGroupName());
			List smsList=new ArrayList();
			List pushNotifications=new ArrayList();
			
			for(int i=0;memberList.size()>i;i++){
				MemberVO memberVO= (MemberVO) memberList.get(i);
				if((memberVO.getMobile()!=null)&&(!memberVO.getMobile().equalsIgnoreCase("0000000000"))){
				SMSMessage smsObjVO=new SMSMessage();
				smsObjVO.setMessage(smsVO.getMessage());
				smsObjVO.setMobileNumber(memberVO.getMobile());
				smsList.add(smsObjVO);
				if(smsVO.isSendPushNotifications()){
				UserVO userVO=userService.getUserDeviceDetailsFromMemberID(memberVO.getMemberID(), smsVO.getAppID()+"","M");
				if(userVO.getAndriodDeviceID()!=null){
					PushNotificationVO pushVO=new PushNotificationVO();
					pushVO.setTo(userVO.getAndriodDeviceID());
					pushVO.setTitle("");
					pushVO.setBody(smsVO.getMessage());
					pushNotifications.add(pushVO);
				}
					
				}
				
				}
			}
			notificationVO.setPushNotificationList(pushNotifications);
			notificationVO.setSmsList(smsList);
			
		}
		logger.info("Sender UserID: "+smsVO.getUserID()+", OrgID: "+smsVO.getOrgID()+", Group: "+smsVO.getGroupName()+", SMS List size: "+notificationVO.getSmsList().size()+", Message: "+smsVO.getMessage());
		if(notificationVO.getSmsList().size()>0 && smsVO.getMessage().length()>0 ){
			notificationVO.setSendSMS(true);
			
			notificationVO.setSendEmail(false);
			if(notificationVO.getPushNotificationList().size()>0){
			
			notificationVO.setSendPushNotifications(true);
			}
			notificationVO.setOrgID(smsVO.getOrgID());
			notificationVO.setAppID(smsVO.getAppID()+"");
			notificationVO=notificationService.SendNotifications(notificationVO);
			logger.info("SMS has been sent successfully ");
			notificationVO.setStatusCode(200);
		}else{
			logger.info("Unable to send SMS ");			
			notificationVO.setStatusCode(404);
		}
		
		
		
		
		
	 }catch(Exception e) {
	    	logger.error("Exception in public NotificationVO sendSMSandPushNotificationsToAGroup(SMSMessage smsVO) "+e);
	 }   
		
		
	logger.debug("Exit : public NotificationVO sendSMSandPushNotificationsToAGroup(SMSMessage smsVO)");
	return notificationVO;
} 

public NotificationVO sendEmailandPushNotificationsToAGroup(EmailMessage emailVO){
	
	NotificationVO notificationVO=new NotificationVO();
	logger.debug("Entry : public NotificationVO sendEmailandPushNotificationsToAGroup(EmailMessage emailVO)");
	
	try {
		notificationVO.setOrgID(emailVO.getOrgID());
		if(emailVO.getGroupName().equalsIgnoreCase("tenant")){
			List tenantList=reportServiceBean.tenantDetailsRpt(emailVO.getOrgID());
			List emailList=new ArrayList();
			
			
			for(int i=0;tenantList.size()>i;i++){
				RptTenantVO tenantVO=(RptTenantVO) tenantList.get(i);
				if((tenantVO.getEmail()!=null)&&(tenantVO.getEmail().length()>0)){
				EmailMessage emailObj=new EmailMessage();
				emailObj.setMessage(emailVO.getMessage());
				emailObj.setEmailTO(tenantVO.getEmail());
				emailObj.setDefaultSender(emailVO.getDefaultSender());
				if(emailVO.getEmailCc()!=null)
				emailObj.setEmailCc(emailVO.getEmailCc());
				emailObj.setSubject(emailVO.getSubject());
				emailList.add(emailObj);
				}
			}
			notificationVO.setEmailList(emailList);
			
		}else{
			
			List memberList=memberDAO.getMembersListFromGroup(emailVO.getOrgID(), emailVO.getGroupName());
			List emailList=new ArrayList();
			List pushNotifications=new ArrayList();
			
			for(int i=0;memberList.size()>i;i++){
				MemberVO memberVO= (MemberVO) memberList.get(i);
				if((memberVO.getEmail()!=null)&&(memberVO.getEmail().length()>0)){
				EmailMessage emailObj=new EmailMessage();
				emailObj.setMessage(emailVO.getMessage());
				emailObj.setEmailTO(memberVO.getEmail());
				emailObj.setDefaultSender(emailVO.getDefaultSender());
				if(emailVO.getEmailCc()!=null)
				emailObj.setEmailCc(emailVO.getEmailCc());
				emailObj.setSubject(emailVO.getSubject());
				emailList.add(emailObj);
				if(emailVO.isSendPushNotifications()){
				UserVO userVO=userService.getUserDeviceDetailsFromMemberID(memberVO.getMemberID(), emailVO.getAppID()+"","M");
				if(userVO.getAndriodDeviceID()!=null){
					PushNotificationVO pushVO=new PushNotificationVO();
					pushVO.setTo(userVO.getAndriodDeviceID());
					pushVO.setTitle("");
					pushVO.setBody(emailVO.getSubject());
					pushNotifications.add(pushVO);
				}
					
				}
				
				}
			}
			notificationVO.setPushNotificationList(pushNotifications);
			notificationVO.setEmailList(emailList);
			
		}
		logger.info("Sender UserID: "+emailVO.getUserID()+", OrgID: "+emailVO.getOrgID()+", Group: "+emailVO.getGroupName()+", Email List size: "+notificationVO.getEmailList().size()+", Subject: "+emailVO.getSubject());
		if(notificationVO.getEmailList().size()>0 && emailVO.getMessage().length()>0 ){
			notificationVO.setSendEmail(true);
			
			notificationVO.setSendSMS(false);
			if(notificationVO.getPushNotificationList().size()>0){
			
			notificationVO.setSendPushNotifications(true);
			}
			notificationVO.setOrgID(emailVO.getOrgID());
			notificationVO.setAppID(emailVO.getAppID()+"");
			notificationVO=notificationService.SendNotifications(notificationVO);
			logger.info("Email has been sent successfully ");
			notificationVO.setStatusCode(200);
		}else{
			logger.info("Unable to send Emails ");			
			notificationVO.setStatusCode(404);
		}
		
		
		
		
		
	 }catch(Exception e) {
	    	logger.error("Exception in public NotificationVO sendEmailandPushNotificationsToAGroup(EmailMessage emailVO) "+e);
	 }   
		
		
	logger.debug("Exit : public NotificationVO sendEmailandPushNotificationsToAGroup(EmailMessage emailVO)");
	return notificationVO;
}

//============ Get Member list of groups like, member/ committee/ tenant etc =========// 
	 public List getMembersListFromGroup(int societyID, String groupName){
		 	List memberList=new ArrayList();
		 	logger.debug("Entry : public List getMembersListFromGroup(int societyID, String groupName)"+societyID+groupName);
		 	
		try{	
		 		memberList = memberDAO.getMembersListFromGroup(societyID, groupName);
		 	}catch (Exception ex){
		 		logger.error("Exception in public List getMembersListFromGroup(int societyID, String groupName) : "+ex);
		 		
		 	}
		 	
		 	logger.debug("Exit : public List getMembersListFromGroup(int societyID, String groupName)");
		 	return memberList;
		 }

	 public MemberVO getIRegister(int orgID,int aptID){
			
			MemberVO memberVO=new MemberVO();
			logger.debug("Entry : public MemberVO getIRegister(int orgID,int aptID");
			List nominationList=new ArrayList();
			List historyList=new ArrayList();
			try {
				memberVO=memberDAO.getMemberInfo(aptID);
				memberVO.setAssociateMembers(memberDAO.getMemberListforApartment(aptID));
				
				historyList=memberDAO.getMembershipHistory(aptID,orgID);
				memberVO.setMemberHistoryList(historyList);
				
				nominationList=societyServiceBean.getNomineeList(memberVO.getMemberID()+"");
				memberVO.setNominationList(nominationList);
				
				
			 }catch(Exception e) {
			    	logger.error("Exception in getIRegister "+e);
			 }   
				
				
			logger.debug("Exit : public MemberVO getIRegister(int orgID,int aptID");
			return memberVO;
		} 
	 
	 
	 	public List getNominationRegisterForApt(int orgID,int aptID){
			
			MemberVO memberVO=new MemberVO();
			logger.debug("Entry : public List getNominationRegisterForApt(int orgID,int aptID");
			List nominationList=new ArrayList();
			
			try {
				memberVO=memberDAO.getMemberInfo(aptID);
				
				
				
				
				nominationList=societyServiceBean.getNomineeList(memberVO.getMemberID()+"");
				
				
				
			 }catch(Exception e) {
			    	logger.error("Exception in getNominationRegisterForApt "+e);
			 }   
				
				
			logger.debug("Exit : public List getNominationRegisterForApt(int orgID,int aptID");
			return nominationList;
		} 

public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

public MemberDAO getMemberDAO() {
	return memberDAO;
}

public void setMemberDAO(MemberDAO memberDAO) {
	this.memberDAO = memberDAO;
}

public LoginService getLoginService() {
	return loginService;
}

public void setLoginService(LoginService loginService) {
	this.loginService = loginService;
}



public GoogleGroupUpdater getGoogleGroupUpdater() {
	return googleGroupUpdater;
}

public void setGoogleGroupUpdater(GoogleGroupUpdater googleGroupUpdater) {
	this.googleGroupUpdater = googleGroupUpdater;
}

public AddressService getAddressServiceBean() {
	return addressServiceBean;
}

public void setAddressServiceBean(AddressService addressServiceBean) {
	this.addressServiceBean = addressServiceBean;
}

public ShareRegService getShareRegisterService() {
	return shareRegisterService;
}

public void setShareRegisterService(ShareRegService shareRegisterService) {
	this.shareRegisterService = shareRegisterService;
}

public LedgerService getLedgerService() {
	return ledgerService;
}

public void setLedgerService(LedgerService ledgerService) {
	this.ledgerService = ledgerService;
}

public NotificationService getNotificationService() {
	return notificationService;
}

public void setNotificationService(NotificationService notificationService) {
	this.notificationService = notificationService;
}

/**
 * @param reportServiceBean the reportServiceBean to set
 */
public void setReportServiceBean(ReportService reportServiceBean) {
	this.reportServiceBean = reportServiceBean;
}

public SocietyService getSocietyServiceBean() {
	return societyServiceBean;
}

public void setSocietyServiceBean(SocietyService societyServiceBean) {
	this.societyServiceBean = societyServiceBean;
}



}
