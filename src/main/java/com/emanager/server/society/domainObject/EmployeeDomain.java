package com.emanager.server.society.domainObject;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.society.dataAccessObject.EmployeeDAO;
import com.emanager.server.society.valueObject.EmergencyVO;
import com.emanager.server.society.valueObject.EmployeeVO;

public class EmployeeDomain {
	
	EmployeeDAO employeeDAO;
	private static final Logger logger = Logger.getLogger(EmployeeDomain.class);
	
	public List getEmployeeList(String strSocietyID){
		
	    List employeeList = null;

		try{
		logger.debug("Entry : public List getEmployeeList(String strSocietyID)");
		
		employeeList = employeeDAO.searchEmployees(strSocietyID);  		
		
		logger.debug("Exit : public List getEmployeeList(String strSocietyID)");
			
		
		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in getEmployeeList :"+Ex);
			
		}
		return employeeList;
	}
	
	
	public int addEmployeeDetails(EmployeeVO employeeVO)
	{	int sucess=0;
		try
		{
		logger.debug("Entry : public int addEmployeeDetails(EmployeeVO employeeVO)");
		
		sucess=employeeDAO.addEmployeeDetails(employeeVO);
			
		}
		catch(Exception Ex)
		{
			Ex.printStackTrace();
			logger.error("Exception in addEmployeeDetails :"+Ex);
		
		}
		logger.debug("Exit : public int addEmployeeDetails(EmployeeVO employeeVO)");
		
	return sucess;	
	}
	
	public int updateEmployeeDetails(EmployeeVO employeeVO,String employeeID,String societyID)
	{
		int sucess=0;
		try
		{
			logger.debug("Entry : public int updateEmployeeDetails(EmployeeVO employeeVO,String employeeID,String societyID)");
			sucess=employeeDAO.updateEmployeeDetails(employeeVO, employeeID,societyID);
		}
		catch(Exception ex)
		{
			logger.error("Exception in updateEmployeeDetails : "+ex);
			
		}
		logger.debug("Exit : public int updateEmployeeDetails(EmployeeVO employeeVO,String employeeID,String societyID)");
		return sucess;
	}
	
	public List getEmployeeSalaryList(String strSocietyID,int month,int year) {
		List employeeList=null;
		DateUtility dateutility = new DateUtility();
		
		Date fromDate = dateutility.getMonthFirstDate(month, year);
		Date uptoDate = dateutility.getMonthLastDate(month, year);
		int totalLateDays=0;
		BigDecimal totalSalary=new BigDecimal("0.00");
			logger.debug("Entry : public List getEmployeeSalaryList(String strSocietyID)");
			
			employeeList = employeeDAO.getEmployeeSalaryList(strSocietyID, fromDate, uptoDate);
			for(int i=0;employeeList.size()>i;i++){
				EmployeeVO employee=(EmployeeVO) employeeList.get(i);
				int presentDays=0;
				//int lateMark=calculateLateMarks
				//totalLateDays=calculateLateDays(employee.getLateDays(),employee.getLateMarks());
				presentDays=employee.getPresentDays();//-totalLateDays;
				totalSalary=calculateSalary(totalLateDays,employee);
				String commentForPresentDays="Total Present Days - "+employee.getPresentDays();
				String commentForLateDays="Total Days with late marks "+employee.getLateDays();
				String commentForSalary=null;
				if((employee.getAvailableLeaves()>=employee.getTakenLeaves())&&(employee.getTakenLeaves()!=0)){
				 commentForSalary="("+presentDays+" + "+employee.getTakenLeaves()+") * "+employee.getSalaryPerDay()+" Rs per day = Rs "+totalSalary;
				}else{
				 commentForSalary=presentDays+" * "+employee.getSalaryPerDay()+" Rs per day = Rs "+totalSalary;}
				employee.setCommentForPresentDays(commentForPresentDays);
				employee.setCommentForLateDays(commentForLateDays);
				employee.setCommentForSalary(commentForSalary);
				employee.setSalaryPerDay(totalSalary);
			}

			logger.debug("outside query : " + employeeList.size());
			logger.debug("Exit : public List getEmployeeSalaryList(String strSocietyID)");
		
		return employeeList;

	}
	private BigDecimal calculateSalary(int totalLateDays,EmployeeVO employee) {
		BigDecimal totalSalary=new BigDecimal("0.00");
		 int availabledays =0;
		try {
			
			
			if(employee.getAvailableLeaves()>=employee.getTakenLeaves()){
				availabledays=(employee.getPresentDays()-totalLateDays)+employee.getTakenLeaves();
			
			}
			else{
			availabledays= employee.getPresentDays()-totalLateDays;
			}
			
			totalSalary=employee.getSalaryPerDay().multiply(new BigDecimal(availabledays)).setScale(2);
			
		} catch (Exception e) {
		logger.error("Exception in Caculate Salary"+e);
		}
		
		return totalSalary;
	}


	private int calculateLateDays(int lateDays, int lateMarks) {
	int totalLateDays=0;
	
	try {
		totalLateDays=lateDays/lateMarks;
		
	} catch (RuntimeException e) {
		logger.error("Exception at calculateLateDays"+e);
	}
	
	return totalLateDays;	
	}
	
	public List getEmployeeWiseDetailsList(String employeeID,int month,int year) throws Exception{
		List employeeList=null;
		DateUtility dateutility = new DateUtility();
		
		Date fromDate = dateutility.getMonthFirstDate(month, year);
		Date uptoDate = dateutility.getMonthLastDate(month, year);
		logger.debug("Entry : public List getEmployeeWiseDetailsList(String strSocietyID,int month,int year)");
		
		employeeList = employeeDAO.getEmployeeWiseDetailsList(employeeID, fromDate, uptoDate);
		
		for(int i=0;employeeList.size()>i;i++){
			EmployeeVO employee=(EmployeeVO) employeeList.get(i);
			if(employee.getInTime().equalsIgnoreCase(" - ")||employee.getOutTime().equalsIgnoreCase(" - "))
			{
				
			}else{
			SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date today=sdfSource.parse(employee.getPresentDate());
			Calendar calendar = Calendar.getInstance();
	        calendar.setTime(today);

	        
	        calendar.set(Calendar.HOUR, employee.getStartTime());

	       java.util.Date lastDayOfMonth = calendar.getTime();
	       
	            // Custom date format
	            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	        
	           
	         
	        java.util.Date d1 = null;
	        java.util.Date d2 = null;
	        try {
	            d1 =  format.parse(employee.getInTime());
	            d2 =  format.parse(employee.getOutTime());
	           
	        } catch (Exception e) {
	            logger.error("Error in differnce", e);
	        }    

	        // Get msec from each, and subtract.
	        long diff = d1.getTime()-lastDayOfMonth.getTime();
	        long diffSeconds = diff / 1000;         
	        long diffMinutes = diff / (60 * 1000);     
	        long diffHours=d2.getTime()-d1.getTime();
	        long workADay = diffHours / (60 * 60 * 1000);  
	       
	        int workHours=employee.getWorkingHours()-((int)(long)workADay);
	        if(workHours>0){
	        	
	        	employee.setTakenLeaves(1);
	        	employee.setInTime(" - ");
	        	employee.setOutTime(" - ");
	        }
	        else{
	        Integer lates=((int)(long)diffMinutes);
	        int lateMinutes=lates-employee.getLateMinutes();
	       
	       if(lateMinutes>0){
	    	   
	    	  employee.setLateMarks(1); 
	       }else
	    	   employee.setLateMarks(0);
	    	   
	        SimpleDateFormat  simple=new SimpleDateFormat("HH:mm:ss a");
	        employee.setInTime(simple.format(d1));
	        employee.setOutTime(simple.format(d2));
	        
	        
			}
			}
		}
		
		
		logger.debug("outside query : " + employeeList.size());
		logger.debug("Exit : public List getEmployeeWiseDetailsList(String strSocietyID,int month,int year)");

	return employeeList;
	}
	
	
public List searchEmployers(){
		
		List listEmployers = null;
		String strSQL = "";
		
		try{	
			logger.debug("Entry : public List searchEmployers()" );
		//1. SQL Query
		
	
		        listEmployers = employeeDAO.searchEmployers();
		        
	
		        logger.debug("outside query : " + listEmployers.size() );
		        logger.debug("Exit : public List searchMembers(String strSocietyID,String lastName)");
		}catch (Exception ex){
			logger.error("Exception in searchMembers(lastName) : "+ex);
			
		}
		return listEmployers;
		
	}
	public List getEmergencyNoList(String strSocietyID){
		
	    List emergencyList = null;
		
		try{
		
		logger.debug("Entry : public List getEmergencyNoList(String strSocietyID)");
		
		emergencyList = employeeDAO.getEmergencyNoList(strSocietyID);
				
		logger.debug("Exit : public List getEmergencyNoList(String strSocietyID)");

		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in getEmergencyNoList : "+Ex);
			
		}
		return emergencyList;
	}
	
	public int addEmergencyContact(EmergencyVO emergencyVO)
	{	int sucess=0;
		try
		{
		logger.debug("Entry : public int addEmergencyContact(EmergencyVO emergencyVO)");
		
		sucess=employeeDAO.addEmergencyContact(emergencyVO);
		
					
		}
		catch(Exception Ex)
		{
			Ex.printStackTrace();
			logger.error("Exception in addEmergencyContact : "+Ex);
		
		}
		logger.debug("Exit :public int addEmergencyContact(EmergencyVO emergencyVO)");
		return sucess;	
	}
    
	public int deleteEmergencyDetails(String emergencyId){
		
		int sucess = 0;
		try{	
		logger.debug("Entry : public int deleteEmergencyDetails(String emergencyId)");
		
		sucess = employeeDAO.deleteEmergencyDetails(emergencyId);
				
		logger.debug("Exit : public int deleteEmergencyDetails(String emergencyId)");

		}catch(Exception Ex){		
			Ex.printStackTrace();
			logger.error("Exception in deleteEmergencyDetails : "+Ex);		
		}
		return sucess;
	}
	
	public EmployeeDAO getEmployeeDAO() {
		return employeeDAO;
	}

	public void setEmployeeDAO(EmployeeDAO employeeDAO) {
		this.employeeDAO = employeeDAO;
	}
	
	
	

}
