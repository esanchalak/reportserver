package com.emanager.server.society.domainObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.emanager.server.accounts.DataAccessObjects.PenaltyChargesVO;
import com.emanager.server.commonUtils.services.AddressService;
import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.commonUtils.valueObject.ImportMemberVO;
import com.emanager.server.invoice.Services.InvoiceService;
import com.emanager.server.invoice.dataAccessObject.InvoiceBillingCycleVO;
import com.emanager.server.society.dataAccessObject.MemberDAO;
import com.emanager.server.society.dataAccessObject.SocietyDAO;
import com.emanager.server.society.valueObject.ApartmentVO;
import com.emanager.server.society.valueObject.BankInfoVO;
import com.emanager.server.society.valueObject.BookingVO;
import com.emanager.server.society.valueObject.InsertMemberVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.userManagement.service.AppDetailsService;
import com.emanager.webservice.GoogleGroupUpdater;

public class SocietyDomain {

	MemberDAO memberDAO;
	SocietyDAO societyDAO;
	GoogleGroupUpdater googleGroupUpdater;
	AddressService addressService;
	AppDetailsService appDetailsService;
	InvoiceService invoiceService;
	private static final Logger logger = Logger.getLogger(SocietyDomain.class);
	
	public List getMemberList(String strSocietyID, String lastName){
		
	    List memberList = null;
		try{
			logger.debug("Entry :public List getMemberList(String strSocietyID, String lastName)");
		/*if(lastName != null)
		  lstMemeberList = memberDAO.searchMembers(strSocietyID,lastName);	
		else*/
			memberList = memberDAO.searchMembers(strSocietyID);  
			
		 logger.debug("Exit :public List getMemberList(String strSocietyID, String lastName)");
				
		}catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in getMemberList : for SocietyID "+strSocietyID+" LastName"+lastName+" "+Ex);
		}
		return memberList;
	}
	
	public int insertSociety(SocietyVO societyVO)
	{	int generatedOrgID=0;
		try{
			
		logger.debug("Entry : public int insertSociety(SocietyVO societyVO)");
		
		generatedOrgID=societyDAO.insertSociety(societyVO);
		
		if(generatedOrgID>2){
		
		if(societyVO.getIsEnterprise()==1){
			//create tables for enterprise
			societyDAO.createAccLdgrTable(generatedOrgID);
			societyDAO.createAccTransactionsTable(generatedOrgID);
			societyDAO.createAccLedgerEntryTable(generatedOrgID);
			societyDAO.createAccLedgerMappingTable(generatedOrgID);
			societyDAO.createReceiptInvoiceMappingTable(generatedOrgID);
		}
		
	
			societyDAO.createSampleLedgers(generatedOrgID,societyVO.getOrgType());
		}
		
			
		}catch(Exception Ex){
			logger.error("Exception in insertSociety : for SocietyID "+societyVO.getSocietyID()+""+Ex);
		}
		logger.debug("Exit : public int insertSociety(SocietyVO societyVO)");
	return generatedOrgID;

    }
	
	public List searchSocietyList(){
		
		List listSociety = null;	
		try{
		logger.debug("Entry : public List searchSocietyList" );

		listSociety=societyDAO.searchSocietyList();
		
		logger.debug("Exit : public List ssearchSocietyList");
		}catch(Exception Ex){
			 logger.error("Exception in searchSocietyList : "+Ex);
		}
		return listSociety;
		
		
	}
public List searchSocietyListForBuilder(String builderID){
	
		List listSociety = null;
		try{		
		logger.debug("Entry : public List searchSocietyListForBuilder" );

		listSociety=societyDAO.searchSocietyListForBuilder(builderID);
		
		logger.debug("Exit : public List searchSocietyListForBuilder");
        }catch(Exception Ex){
		   logger.error("Exception in searchSocietyListForBuilder : "+Ex);
		}
		return listSociety;
		
		
	}
	
	public int insertBuildingNames(List listOfBuilding,int societyID){

		int flag=0;
		try{
		logger.debug("Entry : public int insertBuildingNames(List listOfBuilding,int societyID)" );

		flag=societyDAO.insertBuildingNames(listOfBuilding, societyID);
		
		logger.debug("Exit : public int insertBuildingNames(List listOfBuilding,int societyID)");
		}catch(Exception Ex){			
			logger.error("Exception in insertBuildingNames : for SocietyID "+societyID+""+Ex);
				
		}
		return flag;
		
		
	}
	
	public int importMemberData(int orgID){

		int flag=0;
		try{		
		/*logger.debug("Entry : public int importMemberData(int societyID)" );
		ConfigManager conf=new ConfigManager();
		  File folder = new File(conf.getPropertiesValue("import.memberData"));
		    File[] listOfFiles = folder.listFiles();
		    for (int j = 0; j < listOfFiles.length; j++)
		        if (listOfFiles[j].isFile()) {
		          logger.info("File " + listOfFiles[j].getName());
		          String csvFile = folder + "\\" + listOfFiles[j].getName();
		PoijiOptions options = PoijiOptionsBuilder.settings().headerStart(1).build(); // header starts at 1 (zero-based).
		List<ImportMemberVO> groups = Poiji.fromExcel(new File(csvFile), ImportMemberVO.class, options);
	
		Set<String> building=uniqueBuildings(groups);
		Iterator iter = building.iterator();
		for (String s : building) {
		    System.out.println(s);
		    for(int i=0;groups.size()>i;i++){
				ImportMemberVO group=groups.get(i);
				String str=s;
				String strs=group.buildingName;
				if(str.equalsIgnoreCase(strs)){
				System.out.println(group.title);
				System.out.println(group.primaryName);
				}
			}
		    
		    
		}
		for(int i=0;building.size()>i;i++){
		//	Groups group=groups.get(i);
			System.out.println(building);
		//	System.out.println(group.groupA.primaryName);
			
		}
		
		
		
		for(int i=0;groups.size()>i;i++){
			ImportMemberVO group=groups.get(i);
			System.out.println(group.title);
			System.out.println(group.primaryName);
			
		}

		
		        }*/
		}catch(Exception Ex){
			logger.error("Exception in importMemberData : "+Ex);
		}
		logger.debug("Exit public int importMemberData(int societyID)");
		
		return flag;
		
		
	}
	
	public Set<String> uniqueBuildings(final List<ImportMemberVO> membersList) {
	    Set<String> buildingNames = new HashSet<>();
	    for(final ImportMemberVO member: membersList) {
	        buildingNames.add(member.buildingName);
	    }
	    return buildingNames;
	}
	
	public Set<String> uniqueUnits(final List<ImportMemberVO> memberList) {
	    Set<String> unitNames = new HashSet<>();
	    for(final ImportMemberVO member: memberList) {
	        unitNames.add(member.unitName+member.buildingName);
	    }
	    return unitNames;
	}
	
    public int deleteBuilding(int buildingID){
		
		int sucess = 0;
		try{
		
		logger.debug("Entry : public int delBuilding(int buildingID)");
		
		sucess = societyDAO.deleteBuilding(buildingID);
				
		logger.debug("Exit : public int delBuilding(int buildingID)");

		}catch(Exception Ex){			
		   logger.error("Exception in deleteBuilding : for BuildingID"+buildingID+" "+Ex);
			
		}
		return sucess;
	}
    
    public List searchApartmentsList(String buildingID,int societyID){

       	List membersList = null;
    	try{
    	logger.debug("Entry  : public List searchApartmentsList(String buildingID)" );
    	
    	membersList=societyDAO.searchApartmentsList(buildingID,societyID);
    	
        logger.debug("Exit : public List searchMembers(String buildingID)");
    	}catch(Exception Ex){
 			
    	   logger.error("Exception in searchApartmentsList : for SocietyID "+societyID+" BuildingID"+buildingID+"   "+Ex);
    				
    	 } 
    	return membersList;
    	
    	
    }
    
    public List searchApartmentsListDropDown(String buildingID){

		List memberList = null;
		
		String strSQL = "";
		try{	
		logger.debug("Entry : public List searchApartmentsListDropDown(String buildingID)");
		//1. SQL Query
		
		
		        memberList = societyDAO.searchApartmentsListDropDown(buildingID);
		        
		      
		        logger.debug("Exit : public List searchApartmentsListDropDown(String buildingID)");
		}catch (Exception ex){
			logger.error("Exception in searchApartmentsListDropDown : for  BuildingID"+buildingID+""+ex);
			
		}
		return memberList;
		
		
	}

    public int insertApartmentNames(List listOfApartment,int buildingID){

      int flag=0;
      try{   
      logger.debug("Entry : public int insertApartmentNames(List listOfApartment,String buildingID)" );

      // flag=societyDAO.insertApartmentNames(listOfApartment, buildingID);
       
       if(listOfApartment.size()>0){
    	   
    	   for(int i=0;listOfApartment.size()>i;i++){
    		   InsertMemberVO apartmentVO = (InsertMemberVO) listOfApartment.get(i);
    		   
    		   flag=societyDAO.insertApartment(apartmentVO);
    		   
    		   
    	   }
    	   
    	   
       }

      logger.debug("Exit : public int insertApartmentNames(List listOfApartment,String buildingID)");
      }catch(Exception Ex){
			
		logger.error("Exception in insertApartmentNames :for  BuildingID"+buildingID+" "+Ex);
			
	  }
    return flag;


    }
    
    public int updateMember(MemberVO insertMemberVO,int apartmentID){	
    	int insertFlag=0;
    	int generatedId=0;
    	int sendFlag=0;
    	int flag=0;
    	int delCommiteeMember=0;
    	int addCommitteeMember=0;
    	int delMember=0;
    	int addMember=0;
    	MemberVO memberVO=new MemberVO();
    	try{
    		
    		logger.debug("Entry : public int updateMember(InsertMemberVO insertMemberVO,int apartmentID)");
    					
         	
    			insertFlag=societyDAO.updateMember(insertMemberVO,apartmentID);
    			memberVO.setOldEmailID(insertMemberVO.getOldEmailID());
    			memberVO.setMemberID(insertMemberVO.getMemberID());
    			memberVO.setAptID(insertMemberVO.getAptID());
    			memberVO.setMembershipDate(insertMemberVO.getMembershipDate());
    			memberVO.setMembershipID(insertMemberVO.getMembershipID());
    			memberVO.setOccupation(insertMemberVO.getOccupation());
    			memberVO.setTitle(insertMemberVO.getTitle());
    			memberVO.setFullName(insertMemberVO.getFullName());
    			memberVO.setMobile(insertMemberVO.getMobile());
    			memberVO.setPhone(insertMemberVO.getPhone());
    			memberVO.setAge(insertMemberVO.getAge());
    			
    		if(insertMemberVO.getAutoIncrement()){
    			memberDAO.incrementMembershipNumber(insertMemberVO.getSocietyID());
    		}
    		
    		if(insertFlag==1){
    			if(insertMemberVO.getIsCommitteeMember()==1){
    				if(insertMemberVO.getOldEmailID()==""){
    					delCommiteeMember=1;
    				}else
    				delCommiteeMember=googleGroupUpdater.deleteMember(insertMemberVO.getComitteeGroupEmail(), insertMemberVO.getOldEmailID());
    			    addCommitteeMember=googleGroupUpdater.createMember(insertMemberVO.getComitteeGroupEmail(), insertMemberVO.getEmail(), "Member");
    			   /* if((delCommiteeMember==0)||(addCommitteeMember==0)){
    			    	revertBackMemberUpdate(memberVO, ""+memberVO.getMemberId());
    			    }*/
    			}
    			if(insertMemberVO.getOldEmailID()==""){
    				delMember=1;
    			}else
    			delMember=googleGroupUpdater.deleteMember(insertMemberVO.getMemberGroupEmail(), insertMemberVO.getOldEmailID());
    			addMember=googleGroupUpdater.createMember(insertMemberVO.getMemberGroupEmail(), insertMemberVO.getEmail(), "member");
    			/*if((delMember==0)||(addMember==0))
    				revertBackMemberUpdate(memberVO, ""+memberVO.getMemberId());*/
    			}
    			
    	}catch(Exception Ex){
    		logger.error("Exception in updateMember : "+Ex);
    	
    	}
    	logger.debug("Exit : public int updateMember(InsertMemberVO insertMemberVO,int apartmentID)");
    return insertFlag;	
    }

    private int revertBackMemberUpdate(MemberVO memberVO,int memberId){
		int flag=0;
		
		try {
			logger.debug("Entry : public int revertBackMemberUpdate(MemberVO memberVO,String memberId)");
			logger.info("Here Old Email= "+memberVO.getOldEmailID()+" New EmailID "+memberVO.getEmail());
			memberVO.setEmail(memberVO.getOldEmailID());
			flag=memberDAO.updateMemberDetails(memberVO, memberId);
			
			
			logger.debug("Exit : public int revertBackMemberUpdate(MemberVO memberVO,String memberId)"+flag);
		}catch(Exception e){		
			logger.error("Exception in revertBackMemberUpdate : "+e);
		}
		return flag;	
		
	}
    
    /*public int sendLoginVO(InsertMemberVO insertMemberVO,int memberId){
    	logger.debug("Entry: public int sendLoginVO(InsertMemberVO insertMemberVO,int memberId)");
    	int sendMail = 0;
    	try {
    		
    		LoginVO loginVO=new LoginVO();
    		loginVO.setSocietyID(insertMemberVO.getSocietyID());
    		loginVO.setLoginUserId(insertMemberVO.getEmail());
    		loginVO.setLoginUserFullName(insertMemberVO.getFirstName()+" "+insertMemberVO.getLastName());
    		String memID=new Integer(memberId).toString();;
    		loginVO.setUserID(memID);
    		loginVO.setUserType(insertMemberVO.getType());
    		loginVO.setSocietyName(insertMemberVO.getBuildingName());
    		//LoginService loginService=new LoginService();
    		sendMail=loginService.createNewLogin(loginVO);
    	} catch (Exception e) {
    		logger.error("Exception in MemberDomain"+e);
    	}
    	
    	logger.debug("Entry: public int sendLoginVO(InsertMemberVO insertMemberVO,int memberId)");
    	return sendMail;
    }*/

    public int insertNewMember(MemberVO insertMemberVO,int apartmentID)
    {	
    	int insertFlag=0;
    	int generatedID=0;
    	int groupFlag=0;
    	int membershipNo=0;
    	int primaryAvailable=0;

    	try{
    		logger.debug("Entry : public int insertNewMember(InsertMemberVO insMemVO,int apartmentID)");
    		 if(insertMemberVO.getAutoIncrement()){
    		   membershipNo=memberDAO.getIncrementMembershipNumber((insertMemberVO.getSocietyID()));
    		   insertMemberVO.setMembershipID(membershipNo);
           	 }
    	    
    		 if(insertMemberVO.getType().equalsIgnoreCase("P")){
    			 primaryAvailable=societyDAO.checkAvailibilityForPrimaryMember(apartmentID);
    		 }
    		 
    		 if(primaryAvailable<1){

    	      generatedID=societyDAO.insertNewMember(insertMemberVO, apartmentID);
    	     
    	      if((generatedID!=0) && ((insertMemberVO.getEmail().length()>0) || (insertMemberVO.getMemberGroupEmail().length()>0)) ){
    	        groupFlag=googleGroupUpdater.createMember(insertMemberVO.getMemberGroupEmail(), insertMemberVO.getEmail(), "member");
    	      }
    	     
    	      if((generatedID!=0)&& (insertMemberVO.getAutoIncrement())){
    	    	 
    	    	  memberDAO.incrementMembershipNumber((insertMemberVO.getSocietyID()));
    	      }
    	      
    	      if(generatedID==0){
    	    	  insertFlag=2;
    	      }
    		 }else generatedID=2;
    		
    	}catch(Exception Ex){
    		logger.error("Exception in insertNewMember : for  apartmetnID "+apartmentID+""+Ex);
    		
    	
    	}
    	logger.debug("Exit : public int insertNewMember(InsertMemberVO insertMemberVO,int apartmentID)"+insertFlag);
    return generatedID;	
    }


    public int deleteApartment(int apartmentID){
    	
    	int sucess = 0;
    	try{    	
    	logger.debug("Entry : public int deleteApartment(int apartmentID)");
    	
    	sucess = societyDAO.deleteApartment(apartmentID);
    			
    	logger.debug("Exit : public int deleteApartment(int apartmentID)");

    	}catch(Exception Ex){    		
    	  logger.error("Exception in deleteApartment : "+Ex);
    		
    	}
    	return sucess;
    }

    public int checkAvailabilty(int aptID){
    	logger.debug("Entry : public int checkAvailabilty(int aptID)");
    	int flag=0;
    	
    	flag=societyDAO.checkAptAvailablilty(aptID);
    	logger.debug("Exit : public int checkAvailabilty(int aptID)");
    	return flag;
    }

    public int updateApartmentDetails(ApartmentVO aptVO,int apartmentID){

    	int flag=0;
    	try{    	    	
    	logger.debug("Entry : public int updateApartmentDetails(ApartmentVO aptVO,int apartmentID)" );

    	flag=societyDAO.updateApartmentDetails(aptVO, apartmentID);
    	
    	logger.debug("Exit : public int updateApartmentDetails(ApartmentVO aptVO,int apartmentID)");
    	}catch(Exception Ex){    		
      	  logger.error("Exception in updateApartmentDetails : for Apartment "+apartmentID+" "+Ex);
      		
      	}
    	return flag;
        	
    }
    
    public InsertMemberVO getAssociateMember(int aptID, String type){

		InsertMemberVO memberVO = null;
		try{	
		logger.debug("Entry InsertMemberVO getAssociateMember(int aptID)" );
		
	
		        memberVO = societyDAO.getAssociateMember(aptID ,type);
		      
		       
		        logger.debug("Exit : public InsertMemberVO getAssociateMember(int aptID)");
		}catch (Exception ex){
			logger.error("Exception InsertMemberVO getAssociateMember(int aptID) : "+ex);
			
		}
		return memberVO;
		
		
	}
    
    
   
    
    
    
    public int delMember(InsertMemberVO member,int apartmentID) {

 		int sucess = 0;
 		try {
 			logger.debug("Entry : public int delMember(InsertMemberVO member,int apartmentID)");

 			sucess = societyDAO.delMember(member);

 			logger.debug("Exit : public int delMember(InsertMemberVO member,int apartmentID)");

 		}catch (Exception Ex) {
 			logger.error("Exception in public int delMember(InsertMemberVO member,int apartmentID): "+Ex);
 		}
 		return sucess;
 	}    
    
    public SocietyVO getSocietyDetails(String aptID,int societyID)
    {	
    	SocietyVO societyVO=new SocietyVO();
    	try
    	{
    		logger.debug("Entry : public SocietyVO getSocietyDetails(String aptID,int societyID)");

    		    		
    		societyVO=societyDAO.getSocietyDetails(aptID,societyID);
    		List address=addressService.getSocietyAddressDetails(societyID,"S", "s");
			AddressVO addressVO= (AddressVO) address.get(0);
			societyVO.setAddress(addressVO.getConcatAddr());
       		societyVO.setSocietyAddress(addressVO);
    	}
    	catch(Exception Ex){
    		logger.error("Exception in public SocietyVO getSocietyDetails(String aptID,int societyID)) : for SocietyID "+societyID+" Apartment"+aptID+" "+Ex);
    	}
    	logger.debug("Exit : public SocietyVO getSocietyDetails(String aptID)");
    return societyVO;	
    }
    
    public SocietyVO getOrgDetailsByOrgName(String orgName)
    {	
    	SocietyVO societyVO=new SocietyVO();
    	try
    	{
    		logger.debug("Entry :     public SocietyVO getOrgDetailsByOrgName(String orgName)");

    		    		
    		societyVO=societyDAO.getOrgDetailsByOrgName(orgName);
    	
    	}
    	catch(Exception Ex){
    		logger.error("Exception in     public SocietyVO getOrgDetailsByOrgName(String orgName) : for SocietyNam "+orgName+" : "+Ex);
    	}
    	logger.debug("Exit :    public SocietyVO getOrgDetailsByOrgName(String orgName)");
    return societyVO;	
    }
    
    
    public BankInfoVO getBankDetails(int societyID)
    {	
    	BankInfoVO bankVO=new BankInfoVO();
    	try
    	{
    		logger.debug("Entry : public BankInfoVO getBankDetails(int societyID)");
    		   		   		
    		
    		bankVO= societyDAO.getBankDetails(societyID);
       		
    	}
    	catch(Exception Ex){
    		logger.error("Exception in public BankInfoVO getBankDetails(int societyID) : "+Ex);
    	}
    	logger.debug("Exit : public BankInfoVO getBankDetails(int societyID)"+bankVO.getFavour()+""+bankVO.getIfscNo());
    return bankVO;	
    }
    
    public int insertBooking(BookingVO bookingVO)
	{	
		int insertFlag=0;
		try{
		
		logger.debug("Entry : public int insertBooking(BookingVO bookingVO)");
		
		
			
			insertFlag=societyDAO.insertBooking(bookingVO);
		
          			
		}
		
		catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in public int insertBooking(BookingVO bookingVO) : "+Ex);
		
		}
		logger.debug("Exit :public int insertBooking(BookingVO bookingVO)"+insertFlag);
	return insertFlag;	
	}
    
    public List bookings(String memberID){

		List bookingList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry public List bookings(String memberID)" );
		//1. SQL Query
		
		        bookingList = societyDAO.bookings(memberID);
		        
	
		       
		        logger.debug("Exit :public List bookings(String memberID)"+bookingList.size());
		}catch (Exception ex){
			logger.error("Exception public List bookings(String memberID) : "+ex);
			
		}
		return bookingList;
		
		
	}
    
    
    public int checkAvailability(BookingVO bookingVO)
	{	
		int insertFlag=0;
		try{
		
		logger.debug("Entry : public int checkAvailability(BookingVO bookingVO)");
		
			
			
			
			insertFlag=societyDAO.checkAvailability(bookingVO);
		
          			
		}
		
		catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in public int checkAvailability(BookingVO bookingVO) : "+Ex);
		
		}
		logger.debug("Exit :public int checkAvailability(BookingVO bookingVO)"+insertFlag);
	return insertFlag;	
	}
    
    public List getUnitList(){

		List unitList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry public List getUnitList" );
		
	
		        unitList = societyDAO.getUnitList();
		        
	
		       
		        logger.debug("Exit : public List getUnitList()"+unitList.size());
		}catch (Exception ex){
			logger.error("Exception in getUnitList : "+ex);
			
		}
		return unitList;
		
		
	}
    
  public List getJRegisterList(int socID, int buildingId){
		
	    List memberList = null;
		try{
			logger.debug("Entry : public List getJRegisterList(String socID, String buildingId)");
	
			memberList = societyDAO.getJRegisterList(socID, buildingId);  		
	
			
		 logger.debug("Exit : public List getJRegisterList(String socID, String buildingId)");
				
		}catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in getJRegisterList : for SocietyID "+socID+" BuildingID"+buildingId+" "+Ex);
		}
		return memberList;
	}
    
    
  public List getIRegisterList(String aptID){
		
	    List memberList=null;	   
	
		try{
			logger.debug("Entry :public List getIRegisterList(String aptID)");
		  
			memberList = societyDAO.getIRegisterList(aptID);				
			
		 logger.debug("Exit :public List getIRegisterList(String aptID)");
				
		}catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in getMemberList : for apartmentID"+aptID+""+Ex);
		}
		return memberList;
	}
  
  public List getNomineeList(String memberId ){
		
	    List nomineeList=null;   
	
		try{
			logger.debug("Entry : public List getNomineeList(String memberId )");
		  
			nomineeList = societyDAO.getNomineeList(memberId);			
			
		 logger.debug("Exit : public List getNomineeList(String memberId )");
				
		}catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in getNomineeList : for memberID "+memberId+" "+Ex);
		}
		return nomineeList;
	} 
  
   public List getFlatHistory(String aptID ){
		
	    List memberHistoryList=null;	   
	
		try{
			logger.debug("Entry : public List getFlatHistory(String aptID )");
		  
			memberHistoryList = societyDAO.getFlatHistory(aptID);				
			
		 logger.debug("Exit : public List getFlatHistory(String aptID )");
				
		}catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in getFlatHistory : "+Ex);
		}
		return memberHistoryList;
	}
   
   public int getLatestMembershipNo(int societyID){
		int membershipNo=0;
		try
		{
			logger.debug("Entry :public int getLatestMembershipNo(int societyID)");
			
			
				membershipNo=societyDAO.getLatestMembershipNo(societyID);
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
			logger.error("Exception in getLatestMembershipNo : "+ex);
			
		}
			logger.debug("Exit : public int getLatestMembershipNo(int societyID)");
		return membershipNo;
		}
   
   public int getLatestNominationNo(int societyID){
		int nominationNo=0;
		try
		{
			logger.debug("Entry :public int getLatestNominationNo(int societyID)");
			
			
			nominationNo=societyDAO.getLatestNominationNo(societyID);
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
			logger.error("Exception in getLatestNominationNo : "+ex);
			
		}
			logger.debug("Exit : public int getLatestNominationNo(int societyID)");
		return nominationNo;
		}
  
   
   public int insertLedgerUserMapper(int userID,int ledgerID, String userType,int societyID)
	{	
		int insertFlag=0;
		try{
		
		logger.debug("Entry : public int insertLedgerUserMapper(int userID,int ledgerID, String userType)");
		
			
			
			insertFlag=societyDAO.insertLedgerUserMapper(userID, ledgerID, userType, societyID);
		
         			
		}
		
		catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in public int insertLedgerUserMapper(int userID,int ledgerID, String userType) for SocietyID "+societyID+" MemberID "+userID+" and ledgerID "+ledgerID+" : "+Ex);
		
		}
		logger.debug("Exit :public int insertLedgerUserMapper(int userID,int ledgerID, String userType)"+insertFlag);
	return insertFlag;	
	}
   
   
   public List getSocietyList(int userID){

		List societyList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry public List getSocietyList" );
		
		 societyList = societyDAO.getSocietyList(userID);
		        
	
		       
		        logger.debug("Exit : public List getSocietyList()"+societyList.size());
		}catch (Exception ex){
			logger.error("Exception in getSocietyList : "+ex);
			
		}
		return societyList;
		
		
	}
   
   public List getSocietyListInDatazone(String dataZoneiD){

		List societyList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry : public List getSocietyListInDatazone(int dataZoneiD)" );
		
		 societyList = societyDAO.getSocietyListInDatazone(dataZoneiD);
		        
	
		       
		        logger.debug("Exit : public List getSocietyListInDatazone(int dataZoneiD)"+societyList.size());
		}catch (Exception ex){
			logger.error("Exception in public List getSocietyListInDatazone(int dataZoneiD) : "+ex);
			
		}
		return societyList;
		
		
	}
   
   public List getSocietyListForUser(int userID,int appID){

		List societyList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry public List getSocietyListForUser" );
		
		 societyList = societyDAO.getSocietyListForUser(userID, appID);
		        
	
		       
		        logger.debug("Exit : public List getSocietyListForUser()"+societyList.size());
		}catch (Exception ex){
			logger.error("Exception in getSocietyListForUser : "+ex);
			
		}
		return societyList;
		
		
	}
   
   public int updateLockDateForGSTTx(int societyID,String lockDate)
	{	
		int insertFlag=0;
		try{
		
		logger.debug("Entry :  public int updateLockDateForGSTTx(int societyID,String lockDate)");
		
			
			
			
			insertFlag=societyDAO.updateLockDateForGSTTx(societyID, lockDate);
		
        			
		}
		
		catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in  public int updateLockDateForGSTTx(int societyID,String lockDate) :for SocietyID "+societyID+" "+Ex);
		
		}
		logger.debug("Exit : public int updateLockDateForGSTTx(int societyID,String lockDate)"+insertFlag);
	return insertFlag;	
	}
   
   public int updateLockDateForAuditTx(int societyID,String lockDate)
  	{	
  		int insertFlag=0;
  		try{
  		
  		logger.debug("Entry :  public int updateLockDateForAuditTx(int societyID,String lockDate)");
  		
  			
  			
  			
  			insertFlag=societyDAO.updateLockDateForAuditTx(societyID, lockDate);
  		
          			
  		}
  		
  		catch(Exception Ex){
  			Ex.printStackTrace();
  			logger.error("Exception in  public int updateLockDateForAuditTx(int societyID,String lockDate) :for SocietyID "+societyID+" "+Ex);
  		
  		}
  		logger.debug("Exit : public int updateLockDateForAuditTx(int societyID,String lockDate)"+insertFlag);
  	return insertFlag;	
  	}
   public int updateLockDateForSociety(int societyID,String lockDate)
  	{	
  		int insertFlag=0;
  		try{
  		
  		logger.debug("Entry :  public int updateLockDateForSociety(int societyID,String lockDate)");
  		
  			
  			
  			
  			insertFlag=societyDAO.updateLockDateForSociety(societyID, lockDate);
  		
          			
  		}
  		
  		catch(Exception Ex){
  			Ex.printStackTrace();
  			logger.error("Exception in  public int updateLockDateForSociety(int societyID,String lockDate) :for SocietyID "+societyID+" "+Ex);
  		
  		}
  		logger.debug("Exit : public int updateLockDateForSociety(int societyID,String lockDate)"+insertFlag);
  	return insertFlag;	
  	}
   
   public SocietyVO getSocietyDetails(int societyID)
   {	
	   SocietyVO societyVO=new SocietyVO();
   	try
   	{
   		logger.debug("Entry :  public SocietyVO getSocietyDetails(String societyID)");
   		   		   		
   		
   		societyVO=societyDAO.getSocietyDetails(societyID);
      		
   	}
   	catch(Exception Ex){
   		logger.error("Exception in  public SocietyVO getSocietyDetails(String societyID) : "+Ex);
   	}
   	logger.debug("Exit :  public SocietyVO getSocietyDetails(String societyID)");
   return societyVO;	
   }
   
   public List getSubscriptionExpiringOrgList(String appID,int period){	
   	   List orgList=null;
   	 
  	  	try
  	  	{
  	  		logger.debug("Entry : public List getSubscriptionExpiringOrgList(int appID,int periodID)");
  	  	 	  		   		
  	  		
  	  		orgList= societyDAO.getSubscriptionExpiringOrgList(appID,period);
  	     		
  	  	}
  	   	catch(Exception Ex){
  	  		logger.error("Exception in public List getSubscriptionExpiringOrgList(int appID,int periodID) : "+Ex);
  	  	}
  	  	logger.debug("Exit :  public List getSubscriptionExpiringOrgList(int appID,int periodID)");
  	 
      return orgList;	
      }
   
   public List getSubscriptionExpiredOrgList(int appID,int gracePeriod){	
   	   List orgList=null;
   	 
  	  	try
  	  	{
  	  		logger.debug("Entry : public List getSubscriptionExpiredOrgList(int appID)");
  	  	 	  		   		
  	  		
  	  		orgList= societyDAO.getSubscriptionExpiredOrgList(appID,gracePeriod);
  	     		
  	  	}
  	   	catch(Exception Ex){
  	  		logger.error("Exception in public List getSubscriptionExpiredOrgList(int appID) : "+Ex);
  	  	}
  	  	logger.debug("Exit :  public List getSubscriptionExpiredOrgList(int appID)");
  	 
      return orgList;	
      }
   
   public int restrictSubscriptionExpiredOrgs(){	
	   	  int success=0;
	   	 
	  	  	try
	  	  	{
	  	  		logger.debug("Entry : public  int restrictSubscriptionExpiredOrgs()");
	  	  		
	  	  		List orgList=societyDAO.getSubscriptionExpiredOrgList(2,0);
	  	  		
	  	  		for(int i=0;orgList.size()>i;i++){
	  	  			
	  	  			SocietyVO societyVO= (SocietyVO) orgList.get(i);
	  	  			 success=appDetailsService.updateSubscriptionToGracePeriod(societyVO.getSocietyID());
	  	  			
	  	  			if(success!=0){
	  	  				
	  	  				success=appDetailsService.restrictLoginAccessInGracePeriod(societyVO.getSocietyID());
	  	  				
	  	  			}
	  	  			
	  	  			
	  	  			
	  	  		}
	  	  		
	  	  	 	  		   		
	  	  		
	  	  		
	  	     		
	  	  	}
	  	   	catch(Exception Ex){
	  	  		logger.error("Exception in public  int restrictSubscriptionExpiredOrgs() : "+Ex);
	  	  	}
	  	  	logger.debug("Exit :  public  int restrictSubscriptionExpiredOrgs()");
	  	 
	      return success;	
	      }
   
   
   public List getFinancialYearListForOrg(int orgID){	
	   	  List fyList=new ArrayList();
	   	 
	  	  	try
	  	  	{
	  	  		logger.debug("Entry : public List getFinancialYearListForOrg(int orgID)");
	  	  	 	  		   		
	  	  		
	  	  		fyList= societyDAO.getFinancialYearListForOrg(orgID);
	  	     		
	  	  	}
	  	   	catch(Exception Ex){
	  	  		logger.error("Exception in public List getFinancialYearListForOrg(int orgID): "+Ex);
	  	  	}
	  	  	logger.debug("Exit :  public List getFinancialYearListForOrg(int orgID)");
	  	 
	      return fyList;	
	}

   public SocietyVO getSocietyConfigurationDetails(int societyID)
   {	
	   SocietyVO societyVO=new SocietyVO();
	try
	{
		logger.debug("Entry :  public SocietyVO getSocietyConfigurationDetails(int societyID)");
		   		   		
		societyVO=getSocietyDetails(societyID);
		List billingCycleList=invoiceService.getBillingCycleList(societyID);
		if(billingCycleList.size()>0){
		InvoiceBillingCycleVO billingCycleVO=(InvoiceBillingCycleVO) billingCycleList.get(billingCycleList.size()-1);
		societyVO.setBillingCycleVO(billingCycleVO);
		}
		
		List penaltyChrgeList=societyDAO.getPenaltyCharges(societyID);
		PenaltyChargesVO penaltyChargeVO=(PenaltyChargesVO) penaltyChrgeList.get(penaltyChrgeList.size()-1);
		
		societyVO.setPenaltyChargeVO(penaltyChargeVO);
		
		List chargesList=societyDAO.getApplicableCharges(societyID);
		societyVO.setChargesList(chargesList);
		
   		
	}
	catch(ArrayIndexOutOfBoundsException Ex){
		logger.info("Exception in  public SocietyVO getSocietyConfigurationDetails(int societyID) : "+Ex);
	}
	catch(Exception Ex){
		logger.error("Exception in  public SocietyVO getSocietyConfigurationDetails(int societyID) : "+Ex);
	}
	logger.debug("Exit :  public SocietyVO getSocietyConfigurationDetails(int societyID)");
	return societyVO;	
   }
     
	public SocietyDAO getSocietyDAO() {
		return societyDAO;
	}

	public void setSocietyDAO(SocietyDAO societyDAO) {
		this.societyDAO = societyDAO;
	}

	public MemberDAO getMemberDAO() {
		return memberDAO;
	}

	public void setMemberDAO(MemberDAO memberDAO) {
		this.memberDAO = memberDAO;
	}

	public GoogleGroupUpdater getGoogleGroupUpdater() {
		return googleGroupUpdater;
	}

	public void setGoogleGroupUpdater(GoogleGroupUpdater googleGroupUpdater) {
		this.googleGroupUpdater = googleGroupUpdater;
	}

	public AddressService getAddressService() {
		return addressService;
	}

	public void setAddressService(AddressService addressService) {
		this.addressService = addressService;
	}

	/**
	 * @param appDetailsService the appDetailsService to set
	 */
	public void setAppDetailsService(AppDetailsService appDetailsService) {
		this.appDetailsService = appDetailsService;
	}

	/**
	 * @param invoiceService the invoiceService to set
	 */
	public void setInvoiceService(InvoiceService invoiceService) {
		this.invoiceService = invoiceService;
	}

	
	
	
}
