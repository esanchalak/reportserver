package com.emanager.server.society.domainObject;

import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.commonUtils.valueObject.DropDownVO;
import com.emanager.server.society.dataAccessObject.CommitteeDAO;
import com.emanager.server.society.valueObject.CommitteeVO;


public class CommitteeDomain {
	
	private static final Logger logger = Logger.getLogger(CommitteeDomain.class);
	CommitteeDAO committeeDAO;
	public DropDownVO drpDwn=new DropDownVO();
	public DropDownVO drpDwn1=new DropDownVO();
	
    public List searchCommityMembersForUpdate(CommitteeVO committeeVO){
		
	    List memberList = null;
	    String societyID = committeeVO.getSocietyId();
	    List listSelectedMember = committeeVO.getCommityMemberList();
		try{	

		logger.debug("Entry : public List searchCommityMembersForUpdate(committeeVO)" );
		 
		memberList = committeeDAO.searchSocietyMembers(societyID);
		   logger.debug("Outside query of Society Member list   : "+memberList.size() );
		   for(int i=0;memberList.size()>i;i++){
		   		drpDwn=(DropDownVO)memberList.get(i);	
		   		
				for(int j=0;listSelectedMember.size()>j;j++){
				 	drpDwn1=(DropDownVO)listSelectedMember.get(j);
						
					  if(drpDwn.getData().equals(drpDwn1.getData()) ){
						  memberList.remove(i);
						 --i;
								
				 	  }
					
				}	
					
				
				  
		   }
		   logger.debug(" Updated member list for committee  : "+memberList.size() );
		logger.debug("Exit : public List searchCommityMembersForUpdate(committeeVO)" );
		
		
		
		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in searchCommityMembersForUpdate : "+Ex);
			
		}
		return memberList;
	}
    
    public List searchCommityMembers(String committeeId){
		
	    List commityMemberList = null;

		logger.debug("Entry :   public List searchCommityMembers(String committeeId)" );
		
		commityMemberList = committeeDAO.searchCommityMembers(committeeId);
		
		logger.debug("Exit :   public List searchCommityMembers(String committeeId)" );
		

		return commityMemberList;
	}
   
	public List listCommityMemberUpdate(String societyId ){
		
	    List memberList = null;

		logger.debug("Entry : public List listCommityMemberUpdate(String societyId )" );
		
		memberList = committeeDAO.searchSocietyMembers(societyId);
		
		logger.debug("Exit : public List listCommityMemberUpdate(String societyId )" );
		

		return memberList;
	}

	public List getCommityList(String societyID){
		
	    List committeeList = null;

		try{
		
		logger.debug("Entry : public List getCommityList(String societyID)" );
		
		committeeList = committeeDAO.getCommityList(societyID);
		
		logger.debug("Exit : public List getCommityList(String societyID)" );
			
		
		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in getCommityList : "+Ex);
			
		}
		return committeeList;
	}
	
	public List getCommityMemberList(String committeeId){
		
	    List committeeMemberList = null;

		try{
		logger.debug("Entry : public List getCommityMemberList(String committeeId)" );
		
		committeeMemberList = committeeDAO.getCommityMemberList(committeeId);
		
		logger.debug("Exit : public List getCommityMemberList(String committeeId)" );
			
		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in getCommityMemberList : "+Ex);
			
		}
		return committeeMemberList;
	}
	
	public int insertCommiteeDetails(CommitteeVO committeeVO){
		
		int sucess = 0;
		
		try{
		
		logger.debug("Entry : public int insertCommiteeDetails(CommitteeVO committeeVO)");
		
		sucess = committeeDAO.insertCommiteeDetails(committeeVO);
				
		logger.debug("Exit : public int insertCommiteeDetails(CommitteeVO committeeVO)");

		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in insertCommiteeDetails :"+Ex);
			
		}
		return sucess;
	}
	
	public int insertCommityWorklog(CommitteeVO committeeVO,String committeeId){
		
		int sucess = 0;
		
	    logger.debug("Entry : public int insertCommityWorklog(CommitteeVO committeeVO,String committeeId)");
		
		sucess = committeeDAO.insertCommityWorklog(committeeVO,committeeId);
				
		logger.debug("Exit : public int insertCommityWorklog(CommitteeVO committeeVO,String committeeId)");

		
		return sucess;
	}
	
	
	public int removeCommity(String committeeId){
		
		int sucess = 0;
		
		try{
		
		logger.debug("Entry : public int removeCommity(String committeeId)");
		
		sucess = committeeDAO.removeCommity(committeeId);
				
		logger.debug("Exit : public int removeCommity(String committeeId)");

		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in removeCommity :"+Ex);
			
		}
		return sucess;
	}
	
	 public int updateCommityDetails(CommitteeVO committeeVO,String committeeId){
			
			int sucess = 0;
			
			try{
			
			logger.debug("Entry :public int updateCommityDetails(CommitteeVO committeeVO,String committeeId)");
			
			sucess = committeeDAO.updateCommityDetails(committeeVO,committeeId);
					
			logger.debug("Exit : public int updateCommityDetails(CommitteeVO committeeVO,String committeeId)");

			}catch(Exception Ex){
				
				Ex.printStackTrace();
				logger.error("Exception in updateCommityDetails :"+Ex);
				
			}
			return sucess;
		}
	 
	 public List searchCommittee(String strSocietyID){
			
		    List committeeList = null;

			try{		
			logger.debug("Entry :  public List searchCommittee(String strSocietyID)" );
			
			committeeList = committeeDAO.searchCommittee(strSocietyID);  		
			
			logger.debug("Exit :  public List searchCommittee(String strSocietyID)" );
					
			}catch(Exception Ex){			
				Ex.printStackTrace();
			    logger.error("Exception in searchCommittee : "+Ex);
			}
			return committeeList;
		}
	 
	public CommitteeDAO getCommitteeDAO() {
		return committeeDAO;
	}

	public void setCommitteeDAO(CommitteeDAO committeeDAO) {
		this.committeeDAO = committeeDAO;
	}
}
