package com.emanager.server.society.valueObject;

import java.util.List;

public class RenterVO {
	public String title;
	public int renterID;
	public int aptID;
	public String fullName;
	public String firstName;
	public String lastName;
	public String mobile;
	public String email;
	public String isCurrentRenter;
	public String aggSrtDate;
	public String aggEndDate;
	public String note;
	public String userName;
	public String comment;
	public String isDeleted;
    public int isDocsReceved;
	public int isNRI;
	public int isVerified;
	public int isNOCGiven;
	public int age;
	public String gender;
	public String group;
	public List renterMembers;
	public List addressList;
	public int statusCode;
	public String moveInDate;
	public String moveOutDate;
	public String address;
	public String city;
	public String state;
	public String zip;
	public String countryCode;
	public int agentID;
	public String agentName;
	public String agentMobile;
	public String agentEmail;
	public String adhaarCardNo;
	public String memberName;
	public int addressID;
	public Boolean isSocietyAddress;
	public int orgID;
	
	public Boolean getIsSocietyAddress() {
		return isSocietyAddress;
	}
	public void setIsSocietyAddress(Boolean isSocietyAddress) {
		this.isSocietyAddress = isSocietyAddress;
	}
	public int getAddressID() {
		return addressID;
	}
	public void setAddressID(int addressID) {
		this.addressID = addressID;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public List getAddressList() {
		return addressList;
	}
	public void setAddressList(List addressList) {
		this.addressList = addressList;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getMoveInDate() {
		return moveInDate;
	}
	public void setMoveInDate(String moveInDate) {
		this.moveInDate = moveInDate;
	}
	public String getMoveOutDate() {
		return moveOutDate;
	}
	public void setMoveOutDate(String moveOutDate) {
		this.moveOutDate = moveOutDate;
	}
	public List getRenterMembers() {
		return renterMembers;
	}
	public void setRenterMembers(List renterMembers) {
		this.renterMembers = renterMembers;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public int getIsVerified() {
		return isVerified;
	}
	public int getIsDocsReceved() {
		return isDocsReceved;
	}
	public void setIsDocsReceved(int isDocsReceved) {
		this.isDocsReceved = isDocsReceved;
	}
	public int getIsNRI() {
		return isNRI;
	}
	public void setIsNRI(int isNRI) {
		this.isNRI = isNRI;
	}
	public void setIsVerified(int isVerified) {
		this.isVerified = isVerified;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getAggEndDate() {
		return aggEndDate;
	}
	public void setAggEndDate(String aggEndDate) {
		this.aggEndDate = aggEndDate;
	}
	public String getAggSrtDate() {
		return aggSrtDate;
	}
	public void setAggSrtDate(String aggSrtDate) {
		this.aggSrtDate = aggSrtDate;
	}
	public int getAptID() {
		return aptID;
	}
	public void setAptID(int aptID) {
		this.aptID = aptID;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getIsCurrentRenter() {
		return isCurrentRenter;
	}
	public void setIsCurrentRenter(String isCurrentRenter) {
		this.isCurrentRenter = isCurrentRenter;
	}

	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public int getRenterID() {
		return renterID;
	}
	public void setRenterID(int renterID) {
		this.renterID = renterID;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getIsNOCGiven() {
		return isNOCGiven;
	}
	public void setIsNOCGiven(int isNOCGiven) {
		this.isNOCGiven = isNOCGiven;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getAgentEmail() {
		return agentEmail;
	}
	public void setAgentEmail(String agentEmail) {
		this.agentEmail = agentEmail;
	}
	
	public String getAgentMobile() {
		return agentMobile;
	}
	public void setAgentMobile(String agentMobile) {
		this.agentMobile = agentMobile;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getAdhaarCardNo() {
		return adhaarCardNo;
	}
	public void setAdhaarCardNo(String adhaarCardNo) {
		this.adhaarCardNo = adhaarCardNo;
	}
	public int getAgentID() {
		return agentID;
	}
	public void setAgentID(int agentID) {
		this.agentID = agentID;
	}
	public int getOrgID() {
		return orgID;
	}
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	
	
	

	
}
