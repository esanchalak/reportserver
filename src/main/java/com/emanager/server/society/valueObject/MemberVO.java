package com.emanager.server.society.valueObject;

import java.math.BigDecimal;
import java.util.List;

import com.emanager.server.adminReports.valueObject.ShareRegisterVO;


/**
 * @author Sudarshan Sathe
 * Date  July 24, 2012
 */
public class MemberVO 
{

	private int memberID;
	private int membershipID;
	private List objectList;
	private String title;
	private String fullName;
	private String fullNameRev;
	private String flatNo;
	private String occupation;
	private String mobile;
	private String email;
	private int aptID;
	private String dateOfBirth;
	private RenterVO renterDetails;
	private String aptType;
	private BigDecimal mmcDetails;
	private String lastDate;
	private int societyID;
	private String message;
	private String societyMMC;
	private String mmcDue;
	private String phone;
	private String intercom;
	private BigDecimal area;
	private String mapId;
	private String docCategoryId;
	private String docCategoryValue;
	private String docName;
	private Boolean isDoc;
	private String docType;
	private int noOfDocs;
	private int availableNoOfDocs;
	private int isRented;
	private BigDecimal rentFess;
	private BigDecimal totalDue;
	private String memberGroupEmail;
	private String comitteeGroupEmail;
	private int isCommitteeMember;
	private String oldEmailID;
	private	Boolean autoIncrement;
	private String membershipDate;
	private int age;
	private int ledgerID;
	private int buildingID;
	private BigDecimal costOfUnit;
	private List<MemberVO> associateMembers;
	private ShareRegisterVO shareCertVO;
	private String type;
	private int unitID;
	private String invoiceGroups;
	private int statusCode;
	private String statusMessage;
	private List<MemberVO> documentList;
	private int tenantFeeOnFullAmt;
	private int tenantLedgerID;
	private int documentStatusId;
	private String documentStatusName;
	private String societyName;
	private String societyShortName;//Used while sending SMS
	private String societyStateName;
	private String memberStateName;
	private String buildingName;
	private String apartmentName;
	private String gstin;
	private String supplyState;
	private String tags;
	private String effectiveDate;
	private String shareTransferDate;
	private List<MemberVO> nominationList;
	private List<MemberVO> memberHistoryList;
	private String panNo;
	
	public int getTenantFeeOnFullAmt() {
		return tenantFeeOnFullAmt;
	}
	public void setTenantFeeOnFullAmt(int tenantFeeOnFullAmt) {
		this.tenantFeeOnFullAmt = tenantFeeOnFullAmt;
	}
	public int getTenantLedgerID() {
		return tenantLedgerID;
	}
	public void setTenantLedgerID(int tenantLedgerID) {
		this.tenantLedgerID = tenantLedgerID;
	}
	public int getDocumentStatusId() {
		return documentStatusId;
	}
	public void setDocumentStatusId(int documentStatusId) {
		this.documentStatusId = documentStatusId;
	}
	public String getDocumentStatusName() {
		return documentStatusName;
	}
	public void setDocumentStatusName(String documentStatusName) {
		this.documentStatusName = documentStatusName;
	}
	public List<MemberVO> getDocumentList() {
		return documentList;
	}
	public void setDocumentList(List<MemberVO> documentList) {
		this.documentList = documentList;
	}
	/**
	 * @return the memberId
	 */
	public int getMemberID() {
		return memberID;
	}
	/**
	 * @return the membershipId
	 */
	public int getMembershipID() {
		return membershipID;
	}
	
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}
	/**
	 * @return the fullNameRev
	 */
	public String getFullNameRev() {
		return fullNameRev;
	}
	/**
	 * @return the flatNo
	 */
	public String getFlatNo() {
		return flatNo;
	}
	/**
	 * @return the occupation
	 */
	public String getOccupation() {
		return occupation;
	}
	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * @return the aptID
	 */
	public int getAptID() {
		return aptID;
	}
	/**
	 * @return the dateOfBirth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	/**
	 * @return the renterDetails
	 */
	public RenterVO getRenterDetails() {
		return renterDetails;
	}
	/**
	 * @return the aptType
	 */
	public String getAptType() {
		return aptType;
	}
	/**
	 * @return the mmcDetails
	 */
	public BigDecimal getMmcDetails() {
		return mmcDetails;
	}
	/**
	 * @return the lastDate
	 */
	public String getLastDate() {
		return lastDate;
	}
	/**
	 * @return the societyID
	 */
	public int getSocietyID() {
		return societyID;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @return the societyMMC
	 */
	public String getSocietyMMC() {
		return societyMMC;
	}
	/**
	 * @return the mmcDue
	 */
	public String getMmcDue() {
		return mmcDue;
	}
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @return the intercom
	 */
	public String getIntercom() {
		return intercom;
	}
	/**
	 * @return the area
	 */
	public BigDecimal getArea() {
		return area;
	}
	/**
	 * @return the mapId
	 */
	public String getMapId() {
		return mapId;
	}
	/**
	 * @return the docCategoryId
	 */
	public String getDocCategoryId() {
		return docCategoryId;
	}
	/**
	 * @return the docCategoryValue
	 */
	public String getDocCategoryValue() {
		return docCategoryValue;
	}
	/**
	 * @return the docName
	 */
	public String getDocName() {
		return docName;
	}
	/**
	 * @return the isDoc
	 */
	public Boolean getIsDoc() {
		return isDoc;
	}
	/**
	 * @return the docType
	 */
	public String getDocType() {
		return docType;
	}
	/**
	 * @return the noOfDocs
	 */
	public int getNoOfDocs() {
		return noOfDocs;
	}
	/**
	 * @return the availableNoOfDocs
	 */
	public int getAvailableNoOfDocs() {
		return availableNoOfDocs;
	}
	/**
	 * @return the rentFess
	 */
	public BigDecimal getRentFess() {
		return rentFess;
	}
	/**
	 * @return the totalDue
	 */
	public BigDecimal getTotalDue() {
		return totalDue;
	}
	/**
	 * @return the memberGroupEmail
	 */
	public String getMemberGroupEmail() {
		return memberGroupEmail;
	}
	/**
	 * @return the comitteeGroupEmail
	 */
	public String getComitteeGroupEmail() {
		return comitteeGroupEmail;
	}
	/**
	 * @return the isCommitteeMember
	 */
	public int getIsCommitteeMember() {
		return isCommitteeMember;
	}
	/**
	 * @return the oldEmailID
	 */
	public String getOldEmailID() {
		return oldEmailID;
	}
	/**
	 * @return the autoIncrement
	 */
	public Boolean getAutoIncrement() {
		return autoIncrement;
	}
	/**
	 * @return the membershipDate
	 */
	public String getMembershipDate() {
		return membershipDate;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @return the ledgerID
	 */
	public int getLedgerID() {
		return ledgerID;
	}
	/**
	 * @return the buildingID
	 */
	public int getBuildingID() {
		return buildingID;
	}
	/**
	 * @return the costOfUnit
	 */
	public BigDecimal getCostOfUnit() {
		return costOfUnit;
	}
	/**
	 * @return the associateMembers
	 */
	public List<MemberVO> getAssociateMembers() {
		return associateMembers;
	}
	/**
	 * @return the shareCertVO
	 */
	public ShareRegisterVO getShareCertVO() {
		return shareCertVO;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @return the unitID
	 */
	public int getUnitID() {
		return unitID;
	}
	/**
	 * @return the invoiceGroups
	 */
	public String getInvoiceGroups() {
		return invoiceGroups;
	}
	/**
	 * @param memberId the memberId to set
	 */
	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}
	/**
	 * @param membershipId the membershipId to set
	 */
	public void setMembershipID(int membershipID) {
		this.membershipID = membershipID;
	}
	
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	/**
	 * @param fullNameRev the fullNameRev to set
	 */
	public void setFullNameRev(String fullNameRev) {
		this.fullNameRev = fullNameRev;
	}
	/**
	 * @param flatNo the flatNo to set
	 */
	public void setFlatNo(String flatNo) {
		this.flatNo = flatNo;
	}
	/**
	 * @param occupation the occupation to set
	 */
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * @param aptID the aptID to set
	 */
	public void setAptID(int aptID) {
		this.aptID = aptID;
	}
	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	/**
	 * @param renterDetails the renterDetails to set
	 */
	public void setRenterDetails(RenterVO renterDetails) {
		this.renterDetails = renterDetails;
	}
	/**
	 * @param aptType the aptType to set
	 */
	public void setAptType(String aptType) {
		this.aptType = aptType;
	}
	/**
	 * @param mmcDetails the mmcDetails to set
	 */
	public void setMmcDetails(BigDecimal mmcDetails) {
		this.mmcDetails = mmcDetails;
	}
	/**
	 * @param lastDate the lastDate to set
	 */
	public void setLastDate(String lastDate) {
		this.lastDate = lastDate;
	}
	/**
	 * @param societyID the societyID to set
	 */
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @param societyMMC the societyMMC to set
	 */
	public void setSocietyMMC(String societyMMC) {
		this.societyMMC = societyMMC;
	}
	/**
	 * @param mmcDue the mmcDue to set
	 */
	public void setMmcDue(String mmcDue) {
		this.mmcDue = mmcDue;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @param intercom the intercom to set
	 */
	public void setIntercom(String intercom) {
		this.intercom = intercom;
	}
	/**
	 * @param area the area to set
	 */
	public void setArea(BigDecimal area) {
		this.area = area;
	}
	/**
	 * @param mapId the mapId to set
	 */
	public void setMapId(String mapId) {
		this.mapId = mapId;
	}
	/**
	 * @param docCategoryId the docCategoryId to set
	 */
	public void setDocCategoryId(String docCategoryId) {
		this.docCategoryId = docCategoryId;
	}
	/**
	 * @param docCategoryValue the docCategoryValue to set
	 */
	public void setDocCategoryValue(String docCategoryValue) {
		this.docCategoryValue = docCategoryValue;
	}
	/**
	 * @param docName the docName to set
	 */
	public void setDocName(String docName) {
		this.docName = docName;
	}
	/**
	 * @param isDoc the isDoc to set
	 */
	public void setIsDoc(Boolean isDoc) {
		this.isDoc = isDoc;
	}
	/**
	 * @param docType the docType to set
	 */
	public void setDocType(String docType) {
		this.docType = docType;
	}
	/**
	 * @param noOfDocs the noOfDocs to set
	 */
	public void setNoOfDocs(int noOfDocs) {
		this.noOfDocs = noOfDocs;
	}
	/**
	 * @param availableNoOfDocs the availableNoOfDocs to set
	 */
	public void setAvailableNoOfDocs(int availableNoOfDocs) {
		this.availableNoOfDocs = availableNoOfDocs;
	}
	/**
	 * @param rentFess the rentFess to set
	 */
	public void setRentFess(BigDecimal rentFess) {
		this.rentFess = rentFess;
	}
	/**
	 * @param totalDue the totalDue to set
	 */
	public void setTotalDue(BigDecimal totalDue) {
		this.totalDue = totalDue;
	}
	/**
	 * @param memberGroupEmail the memberGroupEmail to set
	 */
	public void setMemberGroupEmail(String memberGroupEmail) {
		this.memberGroupEmail = memberGroupEmail;
	}
	/**
	 * @param comitteeGroupEmail the comitteeGroupEmail to set
	 */
	public void setComitteeGroupEmail(String comitteeGroupEmail) {
		this.comitteeGroupEmail = comitteeGroupEmail;
	}
	/**
	 * @param isCommitteeMember the isCommitteeMember to set
	 */
	public void setIsCommitteeMember(int isCommitteeMember) {
		this.isCommitteeMember = isCommitteeMember;
	}
	/**
	 * @param oldEmailID the oldEmailID to set
	 */
	public void setOldEmailID(String oldEmailID) {
		this.oldEmailID = oldEmailID;
	}
	/**
	 * @param autoIncrement the autoIncrement to set
	 */
	public void setAutoIncrement(Boolean autoIncrement) {
		this.autoIncrement = autoIncrement;
	}
	/**
	 * @param membershipDate the membershipDate to set
	 */
	public void setMembershipDate(String membershipDate) {
		this.membershipDate = membershipDate;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @param ledgerID the ledgerID to set
	 */
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	/**
	 * @param buildingID the buildingID to set
	 */
	public void setBuildingID(int buildingID) {
		this.buildingID = buildingID;
	}
	/**
	 * @param costOfUnit the costOfUnit to set
	 */
	public void setCostOfUnit(BigDecimal costOfUnit) {
		this.costOfUnit = costOfUnit;
	}
	/**
	 * @param associateMembers the associateMembers to set
	 */
	public void setAssociateMembers(List<MemberVO> associateMembers) {
		this.associateMembers = associateMembers;
	}
	/**
	 * @param shareCertVO the shareCertVO to set
	 */
	public void setShareCertVO(ShareRegisterVO shareCertVO) {
		this.shareCertVO = shareCertVO;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @param unitID the unitID to set
	 */
	public void setUnitID(int unitID) {
		this.unitID = unitID;
	}
	/**
	 * @param invoiceGroups the invoiceGroups to set
	 */
	public void setInvoiceGroups(String invoiceGroups) {
		this.invoiceGroups = invoiceGroups;
	}
	/**
	 * @return the isRented
	 */
	public int getIsRented() {
		return isRented;
	}
	/**
	 * @param isRented the isRented to set
	 */
	public void setIsRented(int isRented) {
		this.isRented = isRented;
	}
	/**
	 * @return the objectList
	 */
	public List getObjectList() {
		return objectList;
	}
	/**
	 * @return the statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}
	/**
	 * @return the statusMessage
	 */
	public String getStatusMessage() {
		return statusMessage;
	}
	/**
	 * @param objectList the objectList to set
	 */
	public void setObjectList(List objectList) {
		this.objectList = objectList;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @param statusMessage the statusMessage to set
	 */
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	/**
	 * @return the societyName
	 */
	public String getSocietyName() {
		return societyName;
	}
	/**
	 * @param societyName the societyName to set
	 */
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	/**
	 * @return the societyShortName
	 */
	public String getSocietyShortName() {
		return societyShortName;
	}
	/**
	 * @param societyShortName the societyShortName to set
	 */
	public void setSocietyShortName(String societyShortName) {
		this.societyShortName = societyShortName;
	}
	/**
	 * @return the societyStateName
	 */
	public String getSocietyStateName() {
		return societyStateName;
	}
	/**
	 * @return the memberStateName
	 */
	public String getMemberStateName() {
		return memberStateName;
	}
	/**
	 * @param societyStateName the societyStateName to set
	 */
	public void setSocietyStateName(String societyStateName) {
		this.societyStateName = societyStateName;
	}
	/**
	 * @param memberStateName the memberStateName to set
	 */
	public void setMemberStateName(String memberStateName) {
		this.memberStateName = memberStateName;
	}
	public String getBuildingName() {
		return buildingName;
	}
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	public String getApartmentName() {
		return apartmentName;
	}
	public void setApartmentName(String apartmentName) {
		this.apartmentName = apartmentName;
	}
	/**
	 * @return the gstin
	 */
	public String getGstin() {
		return gstin;
	}
	/**
	 * @return the supplyState
	 */
	public String getSupplyState() {
		return supplyState;
	}
	/**
	 * @param gstin the gstin to set
	 */
	public void setGstin(String gstin) {
		this.gstin = gstin;
	}
	/**
	 * @param supplyState the supplyState to set
	 */
	public void setSupplyState(String supplyState) {
		this.supplyState = supplyState;
	}
	/**
	 * @return the tags
	 */
	public String getTags() {
		return tags;
	}
	/**
	 * @param tags the tags to set
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	/**
	 * @return the nominationList
	 */
	public List<MemberVO> getNominationList() {
		return nominationList;
	}
	/**
	 * @param nominationList the nominationList to set
	 */
	public void setNominationList(List<MemberVO> nominationList) {
		this.nominationList = nominationList;
	}
	/**
	 * @return the memberHistoryList
	 */
	public List<MemberVO> getMemberHistoryList() {
		return memberHistoryList;
	}
	/**
	 * @param memberHistoryList the memberHistoryList to set
	 */
	public void setMemberHistoryList(List<MemberVO> memberHistoryList) {
		this.memberHistoryList = memberHistoryList;
	}
	/**
	 * @return the shareTransferDate
	 */
	public String getShareTransferDate() {
		return shareTransferDate;
	}
	/**
	 * @param shareTransferDate the shareTransferDate to set
	 */
	public void setShareTransferDate(String shareTransferDate) {
		this.shareTransferDate = shareTransferDate;
	}
	/**
	 * @return the panNo
	 */
	public String getPanNo() {
		return panNo;
	}
	/**
	 * @param panNo the panNo to set
	 */
	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	
  
	
	
}