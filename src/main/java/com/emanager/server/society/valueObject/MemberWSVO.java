package com.emanager.server.society.valueObject;

import java.math.BigDecimal;
import java.util.List;


/**
 * @author Sudarshan Sathe
 * Date  April 18, 2015
 */
public class MemberWSVO 
{

	private int memberID;
	private int membershipID;
	private String title;
	private String fullName;
	private String flatNo;
	private String occupation;
	private String mobile;
	private String email;
	private int is_Rented;
	private int aptID;
	private String dateOfBirth;
	private RenterVO renterDetails;
	private String aptType;
	private int societyID;
	private String societyName;
	private String societyMMC;
	private String phone;
	private String intercom;
	private BigDecimal area;
	private String memberGroupEmail;
	private String comitteeGroupEmail;
	private int isCommitteeMember;
	private String membershipDate;
	private int age;
	private int ledgerID;
	private int buildingID;
	private int statusCode;
	
	/**
	 * @return the memberID
	 */
	public int getMemberID() {
		return memberID;
	}
	/**
	 * @return the membershipID
	 */
	public int getMembershipID() {
		return membershipID;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}
	/**
	 * @return the flatNo
	 */
	public String getFlatNo() {
		return flatNo;
	}
	/**
	 * @return the occupation
	 */
	public String getOccupation() {
		return occupation;
	}
	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @return the is_Rented
	 */
	public int getIs_Rented() {
		return is_Rented;
	}
	/**
	 * @return the aptID
	 */
	public int getAptID() {
		return aptID;
	}
	/**
	 * @return the dateOfBirth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	/**
	 * @return the renterDetails
	 */
	public RenterVO getRenterDetails() {
		return renterDetails;
	}
	/**
	 * @return the aptType
	 */
	public String getAptType() {
		return aptType;
	}
	/**
	 * @return the societyID
	 */
	public int getSocietyID() {
		return societyID;
	}
	/**
	 * @return the societyName
	 */
	public String getSocietyName() {
		return societyName;
	}
	/**
	 * @return the societyMMC
	 */
	public String getSocietyMMC() {
		return societyMMC;
	}
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @return the intercom
	 */
	public String getIntercom() {
		return intercom;
	}
	/**
	 * @return the area
	 */
	public BigDecimal getArea() {
		return area;
	}
	/**
	 * @return the memberGroupEmail
	 */
	public String getMemberGroupEmail() {
		return memberGroupEmail;
	}
	/**
	 * @return the comitteeGroupEmail
	 */
	public String getComitteeGroupEmail() {
		return comitteeGroupEmail;
	}
	/**
	 * @return the isCommitteeMember
	 */
	public int getIsCommitteeMember() {
		return isCommitteeMember;
	}
	/**
	 * @return the membershipDate
	 */
	public String getMembershipDate() {
		return membershipDate;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @return the ledgerID
	 */
	public int getLedgerID() {
		return ledgerID;
	}
	/**
	 * @return the buildingID
	 */
	public int getBuildingID() {
		return buildingID;
	}
	/**
	 * @return the statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}
	/**
	 * @param memberID the memberID to set
	 */
	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}
	/**
	 * @param membershipID the membershipID to set
	 */
	public void setMembershipID(int membershipID) {
		this.membershipID = membershipID;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	/**
	 * @param flatNo the flatNo to set
	 */
	public void setFlatNo(String flatNo) {
		this.flatNo = flatNo;
	}
	/**
	 * @param occupation the occupation to set
	 */
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @param is_Rented the is_Rented to set
	 */
	public void setIs_Rented(int is_Rented) {
		this.is_Rented = is_Rented;
	}
	/**
	 * @param aptID the aptID to set
	 */
	public void setAptID(int aptID) {
		this.aptID = aptID;
	}
	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	/**
	 * @param renterDetails the renterDetails to set
	 */
	public void setRenterDetails(RenterVO renterDetails) {
		this.renterDetails = renterDetails;
	}
	/**
	 * @param aptType the aptType to set
	 */
	public void setAptType(String aptType) {
		this.aptType = aptType;
	}
	/**
	 * @param societyID the societyID to set
	 */
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	/**
	 * @param societyName the societyName to set
	 */
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	/**
	 * @param societyMMC the societyMMC to set
	 */
	public void setSocietyMMC(String societyMMC) {
		this.societyMMC = societyMMC;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @param intercom the intercom to set
	 */
	public void setIntercom(String intercom) {
		this.intercom = intercom;
	}
	/**
	 * @param area the area to set
	 */
	public void setArea(BigDecimal area) {
		this.area = area;
	}
	/**
	 * @param memberGroupEmail the memberGroupEmail to set
	 */
	public void setMemberGroupEmail(String memberGroupEmail) {
		this.memberGroupEmail = memberGroupEmail;
	}
	/**
	 * @param comitteeGroupEmail the comitteeGroupEmail to set
	 */
	public void setComitteeGroupEmail(String comitteeGroupEmail) {
		this.comitteeGroupEmail = comitteeGroupEmail;
	}
	/**
	 * @param isCommitteeMember the isCommitteeMember to set
	 */
	public void setIsCommitteeMember(int isCommitteeMember) {
		this.isCommitteeMember = isCommitteeMember;
	}
	/**
	 * @param membershipDate the membershipDate to set
	 */
	public void setMembershipDate(String membershipDate) {
		this.membershipDate = membershipDate;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @param ledgerID the ledgerID to set
	 */
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	/**
	 * @param buildingID the buildingID to set
	 */
	public void setBuildingID(int buildingID) {
		this.buildingID = buildingID;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	
	
	

	
}