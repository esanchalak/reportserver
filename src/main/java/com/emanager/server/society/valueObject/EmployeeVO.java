package com.emanager.server.society.valueObject;

import java.math.BigDecimal;

public class EmployeeVO {
	
	private String employeeId;
	private String title;
	private String firstName;
	private String lastName;
	private String fullName;
	private String designation;
	private BigDecimal salary;
	private String middleName;
	private String joingDate;
	private String leavingDate;
	private String description;
	private String societyID;
	private int presentDays;
	private int lateMarks;
	private int lateDays;
	private BigDecimal salaryPerDay;
	private String commentForPresentDays;
	private String commentForLateDays;
	private String commentForSalary;
	private int availableLeaves;
	private int takenLeaves;
	private String inTime;
	private String outTime;
	private String presentDate;
	private String vendorName;
	private String vendorID;
	private int lateMinutes;
	private int startTime;
	private int workingHours;
	private String mobile;
	
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getJoingDate() {
		return joingDate;
	}
	public void setJoingDate(String joingDate) {
		this.joingDate = joingDate;
	}
	public String getLeavingDate() {
		return leavingDate;
	}
	public void setLeavingDate(String leavingDate) {
		this.leavingDate = leavingDate;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	
	public BigDecimal getSalary() {
		return salary;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSocietyID() {
		return societyID;
	}
	public void setSocietyID(String societyID) {
		this.societyID = societyID;
	}
	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}
	public int getLateMarks() {
		return lateMarks;
	}
	public void setLateMarks(int lateMarks) {
		this.lateMarks = lateMarks;
	}
	public int getLateDays() {
		return lateDays;
	}
	public void setLateDays(int lateDays) {
		this.lateDays = lateDays;
	}
	public int getPresentDays() {
		return presentDays;
	}
	public void setPresentDays(int presentDays) {
		this.presentDays = presentDays;
	}
	public BigDecimal getSalaryPerDay() {
		return salaryPerDay;
	}
	public void setSalaryPerDay(BigDecimal salaryPerDay) {
		this.salaryPerDay = salaryPerDay;
	}
	public String getCommentForLateDays() {
		return commentForLateDays;
	}
	public void setCommentForLateDays(String commentForLateDays) {
		this.commentForLateDays = commentForLateDays;
	}
	public String getCommentForPresentDays() {
		return commentForPresentDays;
	}
	public void setCommentForPresentDays(String commentForPresentDays) {
		this.commentForPresentDays = commentForPresentDays;
	}
	public String getCommentForSalary() {
		return commentForSalary;
	}
	public void setCommentForSalary(String commentForSalary) {
		this.commentForSalary = commentForSalary;
	}
	public int getAvailableLeaves() {
		return availableLeaves;
	}
	public void setAvailableLeaves(int availableLeaves) {
		this.availableLeaves = availableLeaves;
	}
	public int getTakenLeaves() {
		return takenLeaves;
	}
	public void setTakenLeaves(int takenLeaves) {
		this.takenLeaves = takenLeaves;
	}
	public String getInTime() {
		return inTime;
	}
	public void setInTime(String inTime) {
		this.inTime = inTime;
	}
	public String getOutTime() {
		return outTime;
	}
	public void setOutTime(String outTime) {
		this.outTime = outTime;
	}
	public String getPresentDate() {
		return presentDate;
	}
	public void setPresentDate(String presentDate) {
		this.presentDate = presentDate;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVendorID() {
		return vendorID;
	}
	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}
	public int getLateMinutes() {
		return lateMinutes;
	}
	public void setLateMinutes(int lateMinutes) {
		this.lateMinutes = lateMinutes;
	}
	public int getStartTime() {
		return startTime;
	}
	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}
	public int getWorkingHours() {
		return workingHours;
	}
	public void setWorkingHours(int workingHours) {
		this.workingHours = workingHours;
	}
	
	
	
	

}
