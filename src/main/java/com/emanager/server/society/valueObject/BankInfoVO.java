package com.emanager.server.society.valueObject;

import java.util.ArrayList;

public class BankInfoVO {
	
	private String societyId;
	private String societyName;
	private String bankName;
	private String accountNo;
	private String ifscNo;
	private String micrNo;
	private String branchCode;
	private String contact;
	private String accType;
	private String favour;
	private String societyShortName;
	private String email;
	private String filePath;
	private int isDeleted;
	private ArrayList<BankInfoVO> bankList;
	private int statusCode;
	
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getFavour() {
		return favour;
	}
	public void setFavour(String favour) {
		this.favour = favour;
	}
	public String getIfscNo() {
		return ifscNo;
	}
	public void setIfscNo(String ifscNo) {
		this.ifscNo = ifscNo;
	}
	public String getMicrNo() {
		return micrNo;
	}
	public void setMicrNo(String micrNo) {
		this.micrNo = micrNo;
	}
	public String getSocietyId() {
		return societyId;
	}
	public void setSocietyId(String societyId) {
		this.societyId = societyId;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	public String getAccType() {
		return accType;
	}
	public void setAccType(String accType) {
		this.accType = accType;
	}
	/**
	 * @return the societyShortName
	 */
	public String getSocietyShortName() {
		return societyShortName;
	}
	/**
	 * @param societyShortName the societyShortName to set
	 */
	public void setSocietyShortName(String societyShortName) {
		this.societyShortName = societyShortName;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}
	/**
	 * @return the isDeleted
	 */
	public int getIsDeleted() {
		return isDeleted;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	/**
	 * @return the bankList
	 */
	public ArrayList<BankInfoVO> getBankList() {
		return bankList;
	}
	/**
	 * @param bankList the bankList to set
	 */
	public void setBankList(ArrayList<BankInfoVO> bankList) {
		this.bankList = bankList;
	}
	/**
	 * @return the statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	
	
	
	
}
