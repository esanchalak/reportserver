package com.emanager.server.society.valueObject;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import com.emanager.server.accounts.DataAccessObjects.PenaltyChargesVO;
import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceBillingCycleVO;

public class SocietyVO {

	private int societyID;
	private String societyName;
	private String loginStatus;
	private String societyRegNo;
	private String mailListA;
	private String mailListM;
	private String uri;
	private String socShortName;
	private String socFullName;
	private AddressVO societyAddress;
	private String address;
	private String txCloseDate;
	private String notes;
	private String effectiveDate;
	private BigDecimal thresholdBalance;
	private String accountant;
	private int counter;
	private String roleID;
	private String roleName;
	private String appID;
	private String appName;
	private String defaultOrgID;
	private int statusCode;
	private List objectList;
	private int orgID;
	private String ccEmailID;
	private BigDecimal serviceTax;
	private Timestamp appSubscriptionValidity;
	private int apartmentID;
	private String unitName;
	private String logoUrl;
	private String gstIN;
	private String serverName;
	private String schemaName;	
	private String template;
	private String auditCloseDate;
	private String gstCloseDate;
	private int reminderToAll;
	private String stateName;
	private int tdsLedgerID;
	private String driveName;
	private String dataZoneID;
	private String orgType;
	private int isGracePeriod;
	private String profileType;
	private int isEnterprise;
	private int isParent;
	private int isHubOrg;
	private int gstAsIncome;
	private int gstAsExpense;
	private String panNo;
	private String tanNo;
	private String billingFrequency;
	private String nextBillingDate;
	private PenaltyChargesVO penaltyChargeVO;
	private InvoiceBillingCycleVO billingCycleVO;
	private List chargesList;
	
	public String getAuditCloseDate() {
		return auditCloseDate;
	}
	public void setAuditCloseDate(String auditCloseDate) {
		this.auditCloseDate = auditCloseDate;
	}
	public String getGstCloseDate() {
		return gstCloseDate;
	}
	public void setGstCloseDate(String gstCloseDate) {
		this.gstCloseDate = gstCloseDate;
	}
	public String getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	public String getGstIN() {
		return gstIN;
	}
	public void setGstIN(String gstIN) {
		this.gstIN = gstIN;
	}
	public String getServerName() {
		return serverName;
	}
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	public String getSchemaName() {
		return schemaName;
	}
	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public int getOrgID() {
		return orgID;
	}
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	public String getCcEmailID() {
		return ccEmailID;
	}
	public void setCcEmailID(String ccEmailID) {
		this.ccEmailID = ccEmailID;
	}
	public int getApartmentID() {
		return apartmentID;
	}
	public void setApartmentID(int apartmentID) {
		this.apartmentID = apartmentID;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public Timestamp getAppSubscriptionValidity() {
		return appSubscriptionValidity;
	}
	public void setAppSubscriptionValidity(Timestamp appSubscriptionValidity) {
		this.appSubscriptionValidity = appSubscriptionValidity;
	}
	public BigDecimal getServiceTax() {
		return serviceTax;
	}
	public void setServiceTax(BigDecimal serviceTax) {
		this.serviceTax = serviceTax;
	}
	public String getRoleID() {
		return roleID;
	}
	public void setRoleID(String roleID) {
		this.roleID = roleID;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getAppID() {
		return appID;
	}
	public void setAppID(String appID) {
		this.appID = appID;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getDefaultOrgID() {
		return defaultOrgID;
	}
	public void setDefaultOrgID(String defaultOrgID) {
		this.defaultOrgID = defaultOrgID;
	}
	public String getMailListA() {
		return mailListA;
	}
	public void setMailListA(String mailListA) {
		this.mailListA = mailListA;
	}
	public String getMailListM() {
		return mailListM;
	}
	public void setMailListM(String mailListM) {
		this.mailListM = mailListM;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getSocietyRegNo() {
		return societyRegNo;
	}
	public void setSocietyRegNo(String societyRegNo) {
		this.societyRegNo = societyRegNo;
	}
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	public String getLoginStatus() {
		return loginStatus;
	}
	public void setLoginStatus(String loginStatus) {
		this.loginStatus = loginStatus;
	}
	public void setSocFullName(String socFullName) {
		this.socFullName = socFullName;
	}
	public void setSocShortName(String socShortName) {
		this.socShortName = socShortName;
	}
	public String getSocFullName() {
		return socFullName;
	}
	public String getSocShortName() {
		return socShortName;
	}
	public AddressVO getSocietyAddress() {
		return societyAddress;
	}
	public void setSocietyAddress(AddressVO societyAddress) {
		this.societyAddress = societyAddress;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTxCloseDate() {
		return txCloseDate;
	}
	public void setTxCloseDate(String txCloseDate) {
		this.txCloseDate = txCloseDate;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public BigDecimal getThresholdBalance() {
		return thresholdBalance;
	}
	public void setThresholdBalance(BigDecimal thresholdBalance) {
		this.thresholdBalance = thresholdBalance;
	}
	public String getAccountant() {
		return accountant;
	}
	public void setAccountant(String accountant) {
		this.accountant = accountant;
	}
	public int getCounter() {
		return counter;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
	/**
	 * @return the statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}
	/**
	 * @return the objectList
	 */
	public List getObjectList() {
		return objectList;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @param objectList the objectList to set
	 */
	public void setObjectList(List objectList) {
		this.objectList = objectList;
	}
	/**
	 * @return the reminderToAll
	 */
	public int getReminderToAll() {
		return reminderToAll;
	}
	/**
	 * @param reminderToAll the reminderToAll to set
	 */
	public void setReminderToAll(int reminderToAll) {
		this.reminderToAll = reminderToAll;
	}
	/**
	 * @return the stateName
	 */
	public String getStateName() {
		return stateName;
	}
	/**
	 * @param stateName the stateName to set
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public int getTdsLedgerID() {
		return tdsLedgerID;
	}
	public void setTdsLedgerID(int tdsLedgerID) {
		this.tdsLedgerID = tdsLedgerID;
	}
	public String getDriveName() {
		return driveName;
	}
	public void setDriveName(String driveName) {
		this.driveName = driveName;
	}
	/**
	 * @return the dataZoneID
	 */
	public String getDataZoneID() {
		return dataZoneID;
	}
	/**
	 * @param dataZoneID the dataZoneID to set
	 */
	public void setDataZoneID(String dataZoneID) {
		this.dataZoneID = dataZoneID;
	}
	public String getOrgType() {
		return orgType;
	}
	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}
	/**
	 * @return the isGracePeriod
	 */
	public int getIsGracePeriod() {
		return isGracePeriod;
	}
	/**
	 * @param isGracePeriod the isGracePeriod to set
	 */
	public void setIsGracePeriod(int isGracePeriod) {
		this.isGracePeriod = isGracePeriod;
	}
	public String getProfileType() {
		return profileType;
	}
	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}
	/**
	 * @return the isEnterprise
	 */
	public int getIsEnterprise() {
		return isEnterprise;
	}
	/**
	 * @param isEnterprise the isEnterprise to set
	 */
	public void setIsEnterprise(int isEnterprise) {
		this.isEnterprise = isEnterprise;
	}
	/**
	 * @return the isParent
	 */
	public int getIsParent() {
		return isParent;
	}
	/**
	 * @param isParent the isParent to set
	 */
	public void setIsParent(int isParent) {
		this.isParent = isParent;
	}
	/**
	 * @return the isHubOrg
	 */
	public int getIsHubOrg() {
		return isHubOrg;
	}
	/**
	 * @param isHubOrg the isHubOrg to set
	 */
	public void setIsHubOrg(int isHubOrg) {
		this.isHubOrg = isHubOrg;
	}
	/**
	 * @return the gstAsIncome
	 */
	public int getGstAsIncome() {
		return gstAsIncome;
	}
	/**
	 * @return the gstAsExpense
	 */
	public int getGstAsExpense() {
		return gstAsExpense;
	}
	/**
	 * @param gstAsIncome the gstAsIncome to set
	 */
	public void setGstAsIncome(int gstAsIncome) {
		this.gstAsIncome = gstAsIncome;
	}
	/**
	 * @param gstAsExpense the gstAsExpense to set
	 */
	public void setGstAsExpense(int gstAsExpense) {
		this.gstAsExpense = gstAsExpense;
	}
	public String getPanNo() {
		return panNo;
	}
	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}
	public String getTanNo() {
		return tanNo;
	}
	public void setTanNo(String tanNo) {
		this.tanNo = tanNo;
	}
	/**
	 * @return the billingFrequency
	 */
	public String getBillingFrequency() {
		return billingFrequency;
	}
	/**
	 * @param billingFrequency the billingFrequency to set
	 */
	public void setBillingFrequency(String billingFrequency) {
		this.billingFrequency = billingFrequency;
	}
	/**
	 * @return the nextBillingDate
	 */
	public String getNextBillingDate() {
		return nextBillingDate;
	}
	/**
	 * @param nextBillingDate the nextBillingDate to set
	 */
	public void setNextBillingDate(String nextBillingDate) {
		this.nextBillingDate = nextBillingDate;
	}
	/**
	 * @return the penaltyChargeVO
	 */
	public PenaltyChargesVO getPenaltyChargeVO() {
		return penaltyChargeVO;
	}
	/**
	 * @param penaltyChargeVO the penaltyChargeVO to set
	 */
	public void setPenaltyChargeVO(PenaltyChargesVO penaltyChargeVO) {
		this.penaltyChargeVO = penaltyChargeVO;
	}
	/**
	 * @return the billingCycleVO
	 */
	public InvoiceBillingCycleVO getBillingCycleVO() {
		return billingCycleVO;
	}
	/**
	 * @param billingCycleVO the billingCycleVO to set
	 */
	public void setBillingCycleVO(InvoiceBillingCycleVO billingCycleVO) {
		this.billingCycleVO = billingCycleVO;
	}
	/**
	 * @return the chargesList
	 */
	public List getChargesList() {
		return chargesList;
	}
	/**
	 * @param chargesList the chargesList to set
	 */
	public void setChargesList(List chargesList) {
		this.chargesList = chargesList;
	}
	
	
	
	
}
