package com.emanager.server.society.valueObject;

import java.util.List;

public class CommitteeVO {
	
	private String societyId;
	private String committeeId;
	private String committeeName;
	private String ownerId;
	private String ownerName;
	private String committeeDescription;
	private String insertDate;
	private String workLog;
	private List commityMemberList;
	private String comments;
	private String mobile;
	private String flatNo;
	
	public String getFlatNo() {
		return flatNo;
	}
	public void setFlatNo(String flatNo) {
		this.flatNo = flatNo;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public String getCommitteeDescription() {
		return committeeDescription;
	}
	public void setCommitteeDescription(String committeeDescription) {
		this.committeeDescription = committeeDescription;
	}
	public String getCommitteeId() {
		return committeeId;
	}
	public void setCommitteeId(String committeeId) {
		this.committeeId = committeeId;
	}
	public String getCommitteeName() {
		return committeeName;
	}
	public void setCommitteeName(String committeeName) {
		this.committeeName = committeeName;
	}
	public String getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(String insertDate) {
		this.insertDate = insertDate;
	}
	
	public String getWorkLog() {
		return workLog;
	}
	public void setWorkLog(String workLog) {
		this.workLog = workLog;
	}
	public String getSocietyId() {
		return societyId;
	}
	public void setSocietyId(String societyId) {
		this.societyId = societyId;
	}
	
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	
	public List getCommityMemberList() {
		return commityMemberList;
	}
	public void setCommityMemberList(List commityMemberList) {
		this.commityMemberList = commityMemberList;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	
	
}
