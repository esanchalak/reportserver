package com.emanager.server.society.valueObject;

public class FinancialYearVO {
	
	private int orgID;
	private String fyName;
	private String startDate;
	private String endDate;
	private String countryCode;
	private int fyID;
	
	
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	/**
	 * @return the fyName
	 */
	public String getFyName() {
		return fyName;
	}
	/**
	 * @param fyName the fyName to set
	 */
	public void setFyName(String fyName) {
		this.fyName = fyName;
	}
	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the closeDate
	 */
	public String getEndDate() {
		return endDate;
	}
	/**
	 * @param closeDate the closeDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}
	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	/**
	 * @return the fyID
	 */
	public int getFyID() {
		return fyID;
	}
	/**
	 * @param fyID the fyID to set
	 */
	public void setFyID(int fyID) {
		this.fyID = fyID;
	}

}
