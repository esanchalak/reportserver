package com.emanager.server.society.valueObject;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonInclude(Include.NON_NULL)
public class InsertMemberVO {
	
	private int aptId;
	private String aptName;
	private String title;
	private String firstName;
	private String lastName;
	private String email;
	private String mobile;
	private String buildingID;
	private String societyID;
	private String buildingName;
	private int is_rented;
	private BigDecimal societyMaintanaceCharge;
	private String type;
	private String description;
	private String memberId;
	private String unitType;
	private String unitID;
	private String soldStatus;
	private int is_sale;
	private String intercomm;
	private int counter;
	private int bookCounter;
	private int soldCounter;
	private String fullName ;
	private BigDecimal sinkingFund;
    private BigDecimal rmCharges;
    private BigDecimal sqFeet;
    private BigDecimal cost;
    private String phone;
    private String issueDate;
    private String expiryDate;
    private String shareSerialNo;
    private BigDecimal shareAmount;
    private int noOfShares;
    private Date birthDate;
    private String occupation;
    private String nomineeID;
    private String membershipID;
    private String oldEmail;
    private Boolean autoIncrement;
    private String membershipDate;
    private int age;
    private String dob;
    private String committeeGroupMail;
    private String memberGroupMail;
    private int isCommitteeMember;
    private BigDecimal openingBalance;
    private String ledgerType;
    private String effectiveDate;
    private String normalBalance;
    private int isOccupied;
    private String tags;
    private int statusCode;
    private List objectList;
    private String transferDate;
    private String transfereeName;
    private String transferReason;
    private List<InsertMemberVO> listOfApartments;
    private String unitName;
	private String gstin;
	private String supplyState;
	private String panNo;
	private int ledgerID;
	
	
	public String getNomineeID() {
		return nomineeID;
	}
	public void setNomineeID(String nomineeID) {
		this.nomineeID = nomineeID;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public int getNoOfShares() {
		return noOfShares;
	}
	public void setNoOfShares(int noOfShares) {
		this.noOfShares = noOfShares;
	}
	public BigDecimal getShareAmount() {
		return shareAmount;
	}
	public void setShareAmount(BigDecimal shareAmount) {
		this.shareAmount = shareAmount;
	}
	public String getShareSerialNo() {
		return shareSerialNo;
	}
	public void setShareSerialNo(String shareSerialNo) {
		this.shareSerialNo = shareSerialNo;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getIntercomm() {
		return intercomm;
	}
	public void setIntercomm(String intercomm) {
		this.intercomm = intercomm;
	}
	public int getIs_sale() {
		return is_sale;
	}
	public void setIs_sale(int is_sale) {
		this.is_sale = is_sale;
	}
	public String getSoldStatus() {
		return soldStatus;
	}
	public void setSoldStatus(String soldStatus) {
		this.soldStatus = soldStatus;
	}
	public String getUnitType() {
		return unitType;
	}
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	public String getBuildingName() {
		return buildingName;
	}
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	public int getAptId() {
		return aptId;
	}
	public void setAptId(int aptId) {
		this.aptId = aptId;
	}
	public String getAptName() {
		return aptName;
	}
	public void setAptName(String aptName) {
		this.aptName = aptName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBuildingID() {
		return buildingID;
	}
	public void setBuildingID(String buildingID) {
		this.buildingID = buildingID;
	}
	public String getSocietyID() {
		return societyID;
	}
	public void setSocietyID(String societyID) {
		this.societyID = societyID;
	}
	public int getIs_rented() {
		return is_rented;
	}
	public void setIs_rented(int is_rented) {
		this.is_rented = is_rented;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public int getCounter() {
		return counter;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
	public int getBookCounter() {
		return bookCounter;
	}
	public void setBookCounter(int bookCounter) {
		this.bookCounter = bookCounter;
	}
	public int getSoldCounter() {
		return soldCounter;
	}
	public void setSoldCounter(int soldCounter) {
		this.soldCounter = soldCounter;
	}
	
	public String getUnitID() {
		return unitID;
	}
	public void setUnitID(String unitID) {
		this.unitID = unitID;
	}
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	
	public String getOldEmail() {
		return oldEmail;
	}
	public void setOldEmail(String oldEmail) {
		this.oldEmail = oldEmail;
	}
	public Boolean getAutoIncrement() {
		return autoIncrement;
	}
	public void setAutoIncrement(Boolean autoIncrement) {
		this.autoIncrement = autoIncrement;
	}
	public String getMembershipDate() {
		return membershipDate;
	}
	public void setMembershipDate(String membershipDate) {
		this.membershipDate = membershipDate;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getCommitteeGroupMail() {
		return committeeGroupMail;
	}
	public void setCommitteeGroupMail(String committeeGroupMail) {
		this.committeeGroupMail = committeeGroupMail;
	}
	public String getMemberGroupMail() {
		return memberGroupMail;
	}
	public void setMemberGroupMail(String memberGroupMail) {
		this.memberGroupMail = memberGroupMail;
	}
	public int getIsCommitteeMember() {
		return isCommitteeMember;
	}
	public void setIsCommitteeMember(int isCommitteeMember) {
		this.isCommitteeMember = isCommitteeMember;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getLedgerType() {
		return ledgerType;
	}
	public void setLedgerType(String ledgerType) {
		this.ledgerType = ledgerType;
	}
	public String getNormalBalance() {
		return normalBalance;
	}
	public void setNormalBalance(String normalBalance) {
		this.normalBalance = normalBalance;
	}
	public BigDecimal getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(BigDecimal openingBalance) {
		this.openingBalance = openingBalance;
	}
	public BigDecimal getCost() {
		return cost;
	}
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	public BigDecimal getRmCharges() {
		return rmCharges;
	}
	public void setRmCharges(BigDecimal rmCharges) {
		this.rmCharges = rmCharges;
	}
	public BigDecimal getSinkingFund() {
		return sinkingFund;
	}
	public void setSinkingFund(BigDecimal sinkingFund) {
		this.sinkingFund = sinkingFund;
	}
	public BigDecimal getSocietyMaintanaceCharge() {
		return societyMaintanaceCharge;
	}
	public void setSocietyMaintanaceCharge(BigDecimal societyMaintanaceCharge) {
		this.societyMaintanaceCharge = societyMaintanaceCharge;
	}
	public BigDecimal getSqFeet() {
		return sqFeet;
	}
	public void setSqFeet(BigDecimal sqFeet) {
		this.sqFeet = sqFeet;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public int getIsOccupied() {
		return isOccupied;
	}
	public void setIsOccupied(int isOccupied) {
		this.isOccupied = isOccupied;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	/**
	 * @return the statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}
	/**
	 * @return the objectList
	 */
	public List getObjectList() {
		return objectList;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @param objectList the objectList to set
	 */
	public void setObjectList(List objectList) {
		this.objectList = objectList;
	}
	/**
	 * @return the transferDate
	 */
	public String getTransferDate() {
		return transferDate;
	}
	/**
	 * @param transferDate the transferDate to set
	 */
	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}
	/**
	 * @return the transfereeName
	 */
	public String getTransfereeName() {
		return transfereeName;
	}
	/**
	 * @param transfereeName the transfereeName to set
	 */
	public void setTransfereeName(String transfereeName) {
		this.transfereeName = transfereeName;
	}
	/**
	 * @return the transferReason
	 */
	public String getTransferReason() {
		return transferReason;
	}
	/**
	 * @param transferReason the transferReason to set
	 */
	public void setTransferReason(String transferReason) {
		this.transferReason = transferReason;
	}
	/**
	 * @return the listOfApartments
	 */
	public List<InsertMemberVO> getListOfApartments() {
		return listOfApartments;
	}
	/**
	 * @param listOfApartments the listOfApartments to set
	 */
	public void setListOfApartments(List<InsertMemberVO> listOfApartments) {
		this.listOfApartments = listOfApartments;
	}
	/**
	 * @return the unitName
	 */
	public String getUnitName() {
		return unitName;
	}
	/**
	 * @param unitName the unitName to set
	 */
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getMembershipID() {
		return membershipID;
	}
	public void setMembershipID(String membershipID) {
		this.membershipID = membershipID;
	}
	/**
	 * @return the gstin
	 */
	public String getGstin() {
		return gstin;
	}
	/**
	 * @param gstin the gstin to set
	 */
	public void setGstin(String gstin) {
		this.gstin = gstin;
	}
	/**
	 * @return the supplyState
	 */
	public String getSupplyState() {
		return supplyState;
	}
	/**
	 * @param supplyState the supplyState to set
	 */
	public void setSupplyState(String supplyState) {
		this.supplyState = supplyState;
	}
	/**
	 * @return the panNo
	 */
	public String getPanNo() {
		return panNo;
	}
	/**
	 * @param panNo the panNo to set
	 */
	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}
	/**
	 * @return the ledgerID
	 */
	public int getLedgerID() {
		return ledgerID;
	}
	/**
	 * @param ledgerID the ledgerID to set
	 */
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	


	
}
