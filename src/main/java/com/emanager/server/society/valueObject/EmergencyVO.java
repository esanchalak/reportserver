package com.emanager.server.society.valueObject;


public class EmergencyVO {
	
	private String emergencyId;
	private String type;
	private String name;
	private String societyID;
	private String mobile;
	private String landline;
	
	public String getLandline() {
		return landline;
	}
	public void setLandline(String landline) {
		this.landline = landline;
	}
	public String getEmergencyId() {
		return emergencyId;
	}
	public void setEmergencyId(String emergencyId) {
		this.emergencyId = emergencyId;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSocietyID() {
		return societyID;
	}
	public void setSocietyID(String societyID) {
		this.societyID = societyID;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

}
