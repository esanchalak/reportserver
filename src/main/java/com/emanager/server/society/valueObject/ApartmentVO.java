package com.emanager.server.society.valueObject;

import java.math.BigDecimal;
import java.sql.Date;

public class ApartmentVO {

	private int aptID;
	private String aptName;
	private String buildingID;
	private String societyID;
	private String buildingName;
	private int is_rented;
	private BigDecimal societyMaintanaceCharge;
	private String type;
	private String description;
	private String unitType;
	private String unitID;
	private String soldStatus;
	private int is_sale;
	private String intercomm;
	private int counter;
	private int bookCounter;
	private int soldCounter;
	private BigDecimal sinkingFund;
    private BigDecimal rmCharges;
    private BigDecimal sqFeet;
    private BigDecimal cost;
    private int isOccupied;
	/**
	 * @return the aptID
	 */
	public int getAptID() {
		return aptID;
	}
	/**
	 * @return the aptName
	 */
	public String getAptName() {
		return aptName;
	}
	/**
	 * @return the buildingID
	 */
	public String getBuildingID() {
		return buildingID;
	}
	/**
	 * @return the societyID
	 */
	public String getSocietyID() {
		return societyID;
	}
	/**
	 * @return the buildingName
	 */
	public String getBuildingName() {
		return buildingName;
	}
	/**
	 * @return the is_rented
	 */
	public int getIs_rented() {
		return is_rented;
	}
	/**
	 * @return the societyMaintanaceCharge
	 */
	public BigDecimal getSocietyMaintanaceCharge() {
		return societyMaintanaceCharge;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @return the unitType
	 */
	public String getUnitType() {
		return unitType;
	}
	/**
	 * @return the unitID
	 */
	public String getUnitID() {
		return unitID;
	}
	/**
	 * @return the soldStatus
	 */
	public String getSoldStatus() {
		return soldStatus;
	}
	/**
	 * @return the is_sale
	 */
	public int getIs_sale() {
		return is_sale;
	}
	/**
	 * @return the intercomm
	 */
	public String getIntercomm() {
		return intercomm;
	}
	/**
	 * @return the counter
	 */
	public int getCounter() {
		return counter;
	}
	/**
	 * @return the bookCounter
	 */
	public int getBookCounter() {
		return bookCounter;
	}
	/**
	 * @return the soldCounter
	 */
	public int getSoldCounter() {
		return soldCounter;
	}
	/**
	 * @return the sinkingFund
	 */
	public BigDecimal getSinkingFund() {
		return sinkingFund;
	}
	/**
	 * @return the rmCharges
	 */
	public BigDecimal getRmCharges() {
		return rmCharges;
	}
	/**
	 * @return the sqFeet
	 */
	public BigDecimal getSqFeet() {
		return sqFeet;
	}
	/**
	 * @return the cost
	 */
	public BigDecimal getCost() {
		return cost;
	}
	/**
	 * @return the isOccupied
	 */
	public int getIsOccupied() {
		return isOccupied;
	}
	/**
	 * @param aptID the aptID to set
	 */
	public void setAptID(int aptID) {
		this.aptID = aptID;
	}
	/**
	 * @param aptName the aptName to set
	 */
	public void setAptName(String aptName) {
		this.aptName = aptName;
	}
	/**
	 * @param buildingID the buildingID to set
	 */
	public void setBuildingID(String buildingID) {
		this.buildingID = buildingID;
	}
	/**
	 * @param societyID the societyID to set
	 */
	public void setSocietyID(String societyID) {
		this.societyID = societyID;
	}
	/**
	 * @param buildingName the buildingName to set
	 */
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	/**
	 * @param is_rented the is_rented to set
	 */
	public void setIs_rented(int is_rented) {
		this.is_rented = is_rented;
	}
	/**
	 * @param societyMaintanaceCharge the societyMaintanaceCharge to set
	 */
	public void setSocietyMaintanaceCharge(BigDecimal societyMaintanaceCharge) {
		this.societyMaintanaceCharge = societyMaintanaceCharge;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @param unitType the unitType to set
	 */
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	/**
	 * @param unitID the unitID to set
	 */
	public void setUnitID(String unitID) {
		this.unitID = unitID;
	}
	/**
	 * @param soldStatus the soldStatus to set
	 */
	public void setSoldStatus(String soldStatus) {
		this.soldStatus = soldStatus;
	}
	/**
	 * @param is_sale the is_sale to set
	 */
	public void setIs_sale(int is_sale) {
		this.is_sale = is_sale;
	}
	/**
	 * @param intercomm the intercomm to set
	 */
	public void setIntercomm(String intercomm) {
		this.intercomm = intercomm;
	}
	/**
	 * @param counter the counter to set
	 */
	public void setCounter(int counter) {
		this.counter = counter;
	}
	/**
	 * @param bookCounter the bookCounter to set
	 */
	public void setBookCounter(int bookCounter) {
		this.bookCounter = bookCounter;
	}
	/**
	 * @param soldCounter the soldCounter to set
	 */
	public void setSoldCounter(int soldCounter) {
		this.soldCounter = soldCounter;
	}
	/**
	 * @param sinkingFund the sinkingFund to set
	 */
	public void setSinkingFund(BigDecimal sinkingFund) {
		this.sinkingFund = sinkingFund;
	}
	/**
	 * @param rmCharges the rmCharges to set
	 */
	public void setRmCharges(BigDecimal rmCharges) {
		this.rmCharges = rmCharges;
	}
	/**
	 * @param sqFeet the sqFeet to set
	 */
	public void setSqFeet(BigDecimal sqFeet) {
		this.sqFeet = sqFeet;
	}
	/**
	 * @param cost the cost to set
	 */
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	/**
	 * @param isOccupied the isOccupied to set
	 */
	public void setIsOccupied(int isOccupied) {
		this.isOccupied = isOccupied;
	}
	
}
