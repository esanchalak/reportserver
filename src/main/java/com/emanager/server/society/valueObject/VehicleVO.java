package com.emanager.server.society.valueObject;

import java.math.BigDecimal;
import java.util.List;

public class VehicleVO {
	
	private int vehicleID;
	private String vehicleType;
	private String regNumber;
	private String description;
	private String stickerId;
	public  String title;
	public  int memberID;
    public  String fullName;
	public  String firstName;
	public  String lastName;
	public  String flatNo;
	public  int aptID;
	public  String aptType;
	public  String commercialType;
	public  String email;
	public  String mobile;
	private String vehicleNo;
	private int isRented;
	private int ledgerID;
	private BigDecimal cost;
	private int vehicleStatus;
	private int societyID;
	private String createDate;
	private int statusCode;
	private List objectList;
	
	/**
	 * @return the vehicleID
	 */
	public int getVehicleID() {
		return vehicleID;
	}
	/**
	 * @return the vehicleType
	 */
	public String getVehicleType() {
		return vehicleType;
	}
	/**
	 * @return the regNumber
	 */
	public String getRegNumber() {
		return regNumber;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @return the stickerId
	 */
	public String getStickerId() {
		return stickerId;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @return the memberID
	 */
	public int getMemberID() {
		return memberID;
	}
	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @return the flatNo
	 */
	public String getFlatNo() {
		return flatNo;
	}
	/**
	 * @return the aptID
	 */
	public int getAptID() {
		return aptID;
	}
	/**
	 * @return the aptType
	 */
	public String getAptType() {
		return aptType;
	}
	/**
	 * @return the commercialType
	 */
	public String getCommercialType() {
		return commercialType;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * @return the vehicleNo
	 */
	public String getVehicleNo() {
		return vehicleNo;
	}
	/**
	 * @return the isRented
	 */
	public int getIsRented() {
		return isRented;
	}
	/**
	 * @return the ledgerID
	 */
	public int getLedgerID() {
		return ledgerID;
	}
	/**
	 * @return the cost
	 */
	public BigDecimal getCost() {
		return cost;
	}
	/**
	 * @return the vehicleStatus
	 */
	public int getVehicleStatus() {
		return vehicleStatus;
	}
	/**
	 * @return the societyID
	 */
	public int getSocietyID() {
		return societyID;
	}
	/**
	 * @return the createDate
	 */
	public String getCreateDate() {
		return createDate;
	}
	/**
	 * @param vehicleID the vehicleID to set
	 */
	public void setVehicleID(int vehicleID) {
		this.vehicleID = vehicleID;
	}
	/**
	 * @param vehicleType the vehicleType to set
	 */
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	/**
	 * @param regNumber the regNumber to set
	 */
	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @param stickerId the stickerId to set
	 */
	public void setStickerId(String stickerId) {
		this.stickerId = stickerId;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @param memberID the memberID to set
	 */
	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}
	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @param flatNo the flatNo to set
	 */
	public void setFlatNo(String flatNo) {
		this.flatNo = flatNo;
	}
	/**
	 * @param aptID the aptID to set
	 */
	public void setAptID(int aptID) {
		this.aptID = aptID;
	}
	/**
	 * @param aptType the aptType to set
	 */
	public void setAptType(String aptType) {
		this.aptType = aptType;
	}
	/**
	 * @param commercialType the commercialType to set
	 */
	public void setCommercialType(String commercialType) {
		this.commercialType = commercialType;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/**
	 * @param vehicleNo the vehicleNo to set
	 */
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	/**
	 * @param isRented the isRented to set
	 */
	public void setIsRented(int isRented) {
		this.isRented = isRented;
	}
	/**
	 * @param ledgerID the ledgerID to set
	 */
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	/**
	 * @param cost the cost to set
	 */
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	/**
	 * @param vehicleStatus the vehicleStatus to set
	 */
	public void setVehicleStatus(int vehicleStatus) {
		this.vehicleStatus = vehicleStatus;
	}
	/**
	 * @param societyID the societyID to set
	 */
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	/**
	 * @return the statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}
	/**
	 * @return the objectList
	 */
	public List getObjectList() {
		return objectList;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @param objectList the objectList to set
	 */
	public void setObjectList(List objectList) {
		this.objectList = objectList;
	}
	
	
	
	
	
	

}
