package com.emanager.server.society.dataAccessObject;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.emanager.server.adminReports.valueObject.NominationRegisterVO;
import com.emanager.server.adminReports.valueObject.RptDocumentVO;
import com.emanager.server.adminReports.valueObject.RptTenantVO;
import com.emanager.server.adminReports.valueObject.ShareRegisterVO;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.valueObject.DropDownVO;
import com.emanager.server.financialReports.valueObject.RptMonthYearVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceMemberChargesVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.InsertMemberVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.MemberWSVO;
import com.emanager.server.society.valueObject.RenterVO;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.society.valueObject.VehicleVO;
import com.lowagie.text.Row;

public class MemberDAO {
		
	 private JdbcTemplate  jdbcTemplate;
	 private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	 private SocietyService societyService;
	   
	   private static final Logger logger = Logger.getLogger(MemberDAO.class);
	   Date currentDatetime = new Date(System.currentTimeMillis());   
	    java.sql.Date sqlDate = new java.sql.Date(currentDatetime.getTime());   
		java.sql.Timestamp timestamp = new java.sql.Timestamp(currentDatetime.getTime());
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
		String formattedDate = sdf.format(currentDatetime);
		
	
	
  public List searchMembers(String strSocietyID){

	    List listMembers = null;
		String strSQL = "";
		
		try{	
		logger.debug("Entry : public List searchMembers(String strSocietyID))" );
		//1. SQL Query
		strSQL = "select apartment_details.apt_id,type_of_unit,societymonthlyCharges,apartment_details.is_Rented, apt_name,apartment_details.inv_group,member_details.*,society_id from apartment_details ,member_details ,building_details " +
 		         "where apartment_details.apt_id = member_details.apt_id " +
 		         "AND apartment_details.building_id=building_details.building_id AND building_details.building_id=:society_ID" +
 		         " AND member_details.type='P' and is_current!=0  ;";
		logger.debug("query : " + strSQL);
		// 2. Parameters.
		 SqlParameterSource namedParameters = new MapSqlParameterSource("society_ID", strSocietyID);
       
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            MemberVO membrs = new MemberVO();
		            membrs.setMemberID(rs.getInt("member_id"));
		            membrs.setMembershipID(rs.getInt("membership_id"));
		            membrs.setOldEmailID(rs.getString("email"));
		            membrs.setAptID(rs.getInt("apt_id"));
		            membrs.setFlatNo(rs.getString("apt_name"));
		            membrs.setTitle(rs.getString("title"));
		            membrs.setFullName(rs.getString("full_name"));
		            membrs.setOccupation(rs.getString("occupation"));
		            membrs.setMobile(rs.getString("mobile"));
		            membrs.setEmail(rs.getString("email"));
		            membrs.setPhone(rs.getString("phone"));
		            membrs.setMembershipDate(rs.getString("issue_date"));
		            membrs.setDateOfBirth(rs.getString("dob"));
		            membrs.setIsRented(rs.getInt("is_Rented"));
		            membrs.setAptType(rs.getString("type_of_unit"));
		            membrs.setMmcDetails(rs.getBigDecimal("societymonthlyCharges"));
		            membrs.setSocietyID(rs.getInt("society_id"));
		            membrs.setInvoiceGroups(rs.getString("inv_group"));
		            
		           
		            return membrs;
		        }};
	
		        listMembers = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
		    		        		        
		        logger.debug("Exit : public List searchMembers(String strSocietyID)"+listMembers.size());
		}catch(Exception ex){
			logger.error("Exception in searchMembers : "+ex);
			
		}
		return listMembers;
		
		
	}	
  
  /* Get list of all active members of the given society */
	public List getActivePrimaryMemberList(int societyID) {
		List memberList = null;
		try {
			logger.debug("Entry : public List getActiveMemberList(String societyID)");
			SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			String sql =" SELECT b.building_name,a.apt_name,CONCAT(b.building_name,' ',a.apt_name,' - ',m.title,' ',m.full_name) AS member_name,m.*,l.*,lu.*,s.*,a.*,a.is_Rented as tenant ,m.state_of_supply AS member_state,s.state_of_supply AS society_state,m.gstin as memberGstin,m.pan_no as memberPan FROM apartment_details a,member_details m,building_details b,account_ledgers_"+societyVO.getDataZoneID()+" l,ledger_user_mapping_"+societyVO.getDataZoneID()+" lu, society_settings s "+
					  " WHERE a.apt_id=m.apt_id and s.society_id=b.society_id AND m.type='P'   AND a.building_id=b.building_id AND m.member_id=lu.user_id AND lu.ledger_id=l.id and l.org_id=lu.org_id and b.society_id=l.org_id AND b.society_id=:societyID ;";
			
			
			Map hashMap = new HashMap();

			hashMap.put("societyID", societyID);

			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					// TODO Auto-generated method stub
					MemberVO inMemberVO = new MemberVO();
					inMemberVO.setAptID(rs.getInt("apt_id"));
					inMemberVO.setEmail(rs.getString("email"));
					inMemberVO.setMobile(rs.getString("mobile"));
					inMemberVO.setFullName(rs.getString("member_name"));
					inMemberVO.setMemberID(rs.getInt("member_id"));
					inMemberVO.setSocietyID(rs.getInt("society_id"));
					inMemberVO.setLedgerID(rs.getInt("ledger_id"));
					inMemberVO.setBuildingID(rs.getInt("building_id"));
					inMemberVO.setUnitID(rs.getInt("unit_id"));
					inMemberVO.setArea(rs.getBigDecimal("sq_feet"));
					inMemberVO.setCostOfUnit(rs.getBigDecimal("cost"));
					inMemberVO.setIsRented(rs.getInt("tenant"));
					inMemberVO.setInvoiceGroups(rs.getString("inv_group"));
					inMemberVO.setTenantFeeOnFullAmt(rs.getInt("tenant_fee_on_full_amt"));
					inMemberVO.setTenantLedgerID(rs.getInt("tenant_ledger_Id"));
					inMemberVO.setFlatNo(rs.getString("building_name")+" "+rs.getString("apt_name"));
					inMemberVO.setMemberStateName(rs.getString("member_state"));
					inMemberVO.setSocietyStateName(rs.getString("society_state"));
					inMemberVO.setGstin(rs.getString("memberGstin"));
					inMemberVO.setPanNo(rs.getString("memberPan"));
					return inMemberVO;
				}
			};
			memberList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);

		} catch (Exception ex) {
			logger.error("Exception in public List getMemberList(String societyId) : "
							+ ex);
		}

		logger.debug("Exit : public List getActiveMemberList(String societyID)"+memberList.size());
		return memberList;
	}
	
	
  public MemberVO searchMembersInfo(String memberID){

	    MemberVO memberVO = new MemberVO();
		String strSQL = "";
		
		try{	
		logger.debug("Entry : public MemberVO searchMembers(String memberID)"+memberID );
		//1. SQL Query
		strSQL = "SELECT a.apt_id,type_of_unit,a.is_Rented,a.inv_group,m.*,s.society_id,s.society_name,s.short_name FROM apartment_details a,members_info m,society_details s " +
		         "WHERE a.apt_id = m.apt_id  " +
		         "AND m.society_id=s.society_id AND m.member_id=:memberID AND m.is_current!=0 ;" ;
		        
		logger.debug("query : " + strSQL);
		// 2. Parameters.
		 SqlParameterSource namedParameters = new MapSqlParameterSource("memberID", memberID);
     
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            MemberVO membrs = new MemberVO();
		            membrs.setMemberID(rs.getInt("member_id"));
		            membrs.setMembershipID(rs.getInt("membership_id"));
		            membrs.setOldEmailID(rs.getString("email"));
		            membrs.setAptID(rs.getInt("apt_id"));
		            membrs.setFlatNo(rs.getString("unit_name"));
		            membrs.setTitle(rs.getString("title"));
		            membrs.setFullName(rs.getString("full_name"));
		            membrs.setOccupation(rs.getString("occupation"));
		            membrs.setMobile(rs.getString("mobile"));
		            membrs.setPhone(rs.getString("phone"));
		            membrs.setEmail(rs.getString("email"));
		            membrs.setMembershipDate(rs.getString("issue_date"));
		            membrs.setDateOfBirth(rs.getString("dob"));
		            membrs.setIsRented(rs.getInt("is_Rented"));
		            membrs.setAptType(rs.getString("type_of_unit"));
		            membrs.setSocietyID(rs.getInt("society_id"));
		            membrs.setAge(rs.getInt("age"));
		            membrs.setInvoiceGroups(rs.getString("inv_group"));
		            membrs.setType(rs.getString("type"));
		            membrs.setSocietyName(rs.getString("society_name"));
		            membrs.setSocietyShortName(rs.getString("short_name"));
		            membrs.setTags(rs.getString("tags"));
		            membrs.setPanNo(rs.getString("pan_no"));
		            membrs.setGstin(rs.getString("gstin"));
		            membrs.setSupplyState(rs.getString("state_of_supply"));
		            return membrs;
		        }};
	
		        memberVO = (MemberVO) namedParameterJdbcTemplate.queryForObject(strSQL, namedParameters, RMapper);
		    		        		        
		        logger.debug("Exit : public MemberVO searchMembers(String memberID)");
		}catch(Exception ex){
			logger.error("Exception in searchMembers : "+ex);
			
		}
		return memberVO;
		
		
	}
  
  
	public List searchMembers(String buildingID,String lastName){
		
		List listMembers = null;
		String strSQL = "";
		
		try{	
			logger.debug("Entry : public List searchMembers(String strSocietyID,String lastName)" );
		//1. SQL Query
		strSQL = "select apartment_details.*,member_details.*,building_name from apartment_details,member_details ,building_details " +
 		         "where apartment_details.building_id = building_details.building_id AND apartment_details.apt_id = member_details.apt_id " +
 		         "AND member_details.type='P' and is_current!=0  AND building_details.building_id=:buildingID ORDER BY apartment_details.seq_no;" ;
 		         
		
		logger.debug("query : " + strSQL);
		// 2. Parameters.
		 SqlParameterSource namedParameters = new MapSqlParameterSource("buildingID", buildingID);
       
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            DropDownVO membrs = new DropDownVO();
		            membrs.setData(rs.getString("member_id"));
		            membrs.setLabel(rs.getString("building_name")+" "+rs.getString("apt_name") + " - " + rs.getString("full_name") );
		            
		            return membrs;
		        }};
	
		        listMembers = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
		        
	
		        logger.debug("outside query : " + listMembers.size() );
		        logger.debug("Exit : public List searchMembers(String strSocietyID,String lastName)");
		}catch (Exception ex){
			logger.error("Exception in searchMembers(lastName) : "+ex);
			
		}
		return listMembers;
		
	}

	
public List searchSocietyMembers(String strSocietyID){
		
		List listSocietyMembrs = null;		
		String strSQL = "";
		
		try{	
			logger.debug("inside query searchSocietyMembers(String strSocietyID)" );
		//1. SQL Query
		strSQL = "SELECT member_id,apt_name,first_name,last_name,full_name,building_name  FROM building_details,apartment_details,member_details " +
				"WHERE member_details.apt_id=apartment_details.apt_id AND apartment_details.building_id=building_details.building_id  AND building_details.society_id = :society_ID AND member_details.type='P' and is_current!=0 order by seq_no " ;
				
		logger.debug("query : " + strSQL);
		// 2. Parameters.
		 SqlParameterSource namedParameters = new MapSqlParameterSource("society_ID", strSocietyID);
       
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            DropDownVO societyMembrs = new DropDownVO();
		            societyMembrs.setData(rs.getString("member_id"));
		            societyMembrs.setLabel(rs.getString("building_name")+" - "+rs.getString("apt_name") + " - " + rs.getString("full_name") );
		            return societyMembrs;
		        }};
	
		        listSocietyMembrs = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
		        
	
		        logger.debug("outside query : " + listSocietyMembrs.size() );
		        logger.debug("Exit : public List searchMembers(String strSocietyID,String lastName)");
		        
		}catch (Exception ex){
			logger.error("Exception in searchSocietyMembers : "+ex);
			
		}
		return listSocietyMembrs;
		
	}

	
public List searchBuildings(int strSocietyID,String lastName){
		
		List listBuildings = null;		
		String strSQL = "";
		
		try{	
			logger.debug("Entry : public List searchBuildings(String strSocietyID,String lastName)" );
		//1. SQL Query
		strSQL = "select * from building_details where society_id= :society_id;";
		logger.debug("query : " + strSQL);
		// 2. Parameters.
		 SqlParameterSource namedParameters = new MapSqlParameterSource("society_id", strSocietyID);
       
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            DropDownVO drpDwn = new DropDownVO();
		            drpDwn.setData(rs.getString("building_id"));
		            drpDwn.setLabel(rs.getString("building_name"));
		            return drpDwn;
		        }};
	
		        listBuildings = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
		
		        logger.debug("outside query : " + listBuildings.size() );
		        logger.debug("Exit : public List searchBuildings(String strSocietyID,String lastName)" );
		}catch (Exception ex){
			logger.error("Exception in searchBuildings : "+ex);
			
		}
		return listBuildings;
	}

public List getVehicleList(int strMemberID){

	List listVechicles = null;	
	String strSQL = "";
	
	try{	
		logger.debug("Entry : public List getVehicleList(String strMemberID)" );
	//1. SQL Query
	strSQL = "select * from vehicle_details where member_id = :member_ID ";
	logger.debug("query : " + strSQL);
	// 2. Parameters.
	 SqlParameterSource namedParameters = new MapSqlParameterSource("member_ID", strMemberID);
   
	//3. RowMapper.
	 RowMapper RMapper = new RowMapper() {
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	            VehicleVO vehicle = new VehicleVO();
	            vehicle.setVehicleID(rs.getInt("vehicle_id"));
	            vehicle.setVehicleType(rs.getString("vehicle_type"));
	            vehicle.setRegNumber(rs.getString("reg_number")+" "+rs.getString("vehicle_number"));
	            vehicle.setDescription(rs.getString("vehicle_description"));
	            vehicle.setStickerId(rs.getString("sticker_id"));
	            vehicle.setCommercialType(rs.getString("commercial"));
	            vehicle.setVehicleStatus(rs.getInt("status_code"));
	            vehicle.setCreateDate(rs.getString("create_date"));
	            return vehicle;
	        }};

	        listVechicles = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
	        

	        logger.debug("outside query : " + listVechicles.size() );
			logger.debug("Exit : public List getVehicleList(String strMemberID)" );
	}catch (Exception ex){
		logger.error("Exception in getVehicleList : "+ex);
		
	}
	return listVechicles;
	
	
}


public String getSocietyID(String strMemberID){

	String societyID = null;	
	String strSQL = "";
	
	try{	
	     logger.debug("Entry : public String getSocietyID(String strMemberID)");
	//1. SQL Query
	strSQL = "select society_id from building_details,member_details,apartment_details " +
			"where member_details.member_id = :strMemberID " +
			"and member_details.apt_id = apartment_details.apt_id and apartment_details.building_id = building_details.building_id ";
	logger.debug("query : " + strSQL);
	
	// 2. Parameters.
	 SqlParameterSource namedParameters = new MapSqlParameterSource("member_ID", strMemberID);
   
	 societyID = namedParameterJdbcTemplate.queryForObject(strSQL,namedParameters,String.class).toString();	        

		logger.debug("Exit : public String getSocietyID(String strMemberID)");
	}catch (Exception ex){
		logger.error("Exception in getSocietyID : "+ex);
	}
	
	return societyID;
		
}


   public int addVehicleDetails(VehicleVO vehicleVO, int memberId, int societyId){
	   
	  int flag=0;
	 	try{
			logger.debug(" Entry : public int addVehicleDetails(VehicleVO vechicleVO,String memberId,String societyId )");
			 Date currentDatetime = new Date(System.currentTimeMillis());   
		    java.sql.Date sqlDate = new java.sql.Date(currentDatetime.getTime());   
			java.sql.Timestamp timestamp = new java.sql.Timestamp(currentDatetime.getTime());
			
	
			flag = this.jdbcTemplate.update("  INSERT INTO vehicle_details(member_id,soc_id,sticker_id,vehicle_type,reg_number,vehicle_number,vehicle_description,commercial,status_code,create_date) VALUES (?,?,?,?,?,?,?,?,?,?); ", 
					                                             new Object[] {memberId,societyId,vehicleVO.getStickerId(),vehicleVO.getVehicleType(),vehicleVO.getRegNumber(),vehicleVO.getVehicleNo(),vehicleVO.getDescription(),vehicleVO.getCommercialType(),vehicleVO.getVehicleStatus(),timestamp});
			
			logger.debug(" Exit :public int addVehicleDetails(VehicleVO vechicleVO,String memberId,String societyId )"+flag);
				
		}catch (DuplicateKeyException duplicate) {
			 flag=2;
			logger.info("DuplicateKeyException in addVehicleDetails : "+duplicate);
		
		}catch(Exception Ex){
			 Ex.printStackTrace();
			logger.error("Exception in addVehicleDetails : "+Ex);
			 
		 }
		return flag;
	
   }

public int deleteVehicleDetails(int vehicleId){
	
	int sucess = 0;
	String strSQL = "";
	try {
			logger.debug("Entry : public int deleteVehicleDetails(String vehicleType, String strSocietyID)");
			strSQL = "DELETE FROM vehicle_details WHERE vehicle_id=:vehicleId" ;
			logger.debug("query : " + strSQL);
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("vehicleId", vehicleId);
			
			sucess = namedParameterJdbcTemplate.update(strSQL,namedParameters);
			
			logger.debug("Exit : public int deleteVehicleDetails(String vehicleType, String strSocietyID)"+sucess);

	}catch(Exception Ex){		
		Ex.printStackTrace();
		logger.error("Exception in deleteVehicleDetails : "+Ex);		
	}
	return sucess;
}	

public VehicleVO getVehicleDetails(int vehicleID){

	VehicleVO vehicleVO = new VehicleVO();
	String strSQL = "";
	
	try{	
	logger.debug("Entry : public VehicleVO getVehicleDetails(String vehicleID)"+vehicleID );
	//1. SQL Query
	strSQL = " SELECT v.member_id,v.vehicle_id,v.reg_number,vehicle_number,create_date,b.building_id,CONCAT(building_name,' - ',apt_name)AS unitName,full_name,sticker_id,vehicle_type,CONCAT(reg_number,' ',vehicle_number)AS vehNumber,vehicle_description,commercial,status_code "
			+ " FROM vehicle_details v,apartment_details a, building_details b,member_details m "
			+ " WHERE b.building_id=a.building_id AND a.apt_id=m.apt_id AND m.member_id=v.member_id AND m.is_current!=0  "
			+ " AND v.vehicle_id=:vehicleID  ";
	logger.debug("query : " + strSQL);
	// 2. Parameters.
	Map hMap = new HashMap();
	hMap.put("vehicleID", vehicleID);
	
 
	//3. RowMapper.
	 RowMapper RMapper = new RowMapper() {
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	           VehicleVO vehicleVO = new VehicleVO();
	           
	            vehicleVO.setVehicleID(rs.getInt("vehicle_id"));
				vehicleVO.setFlatNo(rs.getString("unitName"));
				vehicleVO.setMemberID(rs.getInt("member_id"));
				vehicleVO.setFullName(rs.getString("full_name"));
				vehicleVO.setStickerId(rs.getString("sticker_id"));
				vehicleVO.setVehicleType(rs.getString("vehicle_type"));
				vehicleVO.setRegNumber(rs.getString("reg_number"));
				vehicleVO.setVehicleNo(rs.getString("vehicle_number"));
				vehicleVO.setDescription(rs.getString("vehicle_description"));
				vehicleVO.setCommercialType(rs.getString("commercial"));
				vehicleVO.setVehicleStatus(rs.getInt("status_code"));				
				vehicleVO.setCreateDate(rs.getString("create_date"));
	            return vehicleVO;
	        }};

	        vehicleVO = (VehicleVO) namedParameterJdbcTemplate.queryForObject(strSQL, hMap, RMapper);
	    		        		        
	        logger.debug("Exit : public VehicleVO getVehicleDetails(String vehicleID)");
	}catch(Exception ex){
		logger.error("Exception in getVehicleDetails : "+ex);
		
	}
	return vehicleVO;
	
	
}

public int updateVehicleDetails(VehicleVO vehicleVO,int vehicleID){
	int success=0;
	try {
		logger.debug("Entry : public int updateVehicleDetails(VehicleVO vehicleVO,int vehicleID)");
		
		String query=" UPDATE vehicle_details SET sticker_id=:stickerID, vehicle_type=:vehicleType, vehicle_description=:description, commercial=:commercial, status_code=:status " +
					"  WHERE vehicle_id="+vehicleID+"  ; ";
		logger.debug("query : " + query);
		
		Map hmap=new HashMap();
	
		hmap.put("stickerID", vehicleVO.getStickerId());
		hmap.put("vehicleType", vehicleVO.getVehicleType());
		hmap.put("description", vehicleVO.getDescription());
		hmap.put("commercial", vehicleVO.getCommercialType());
		hmap.put("status", vehicleVO.getVehicleStatus());
				
		success=namedParameterJdbcTemplate.update(query, hmap);
		
		logger.debug("Exit : public int updateVehicleDetails(VehicleVO vehicleVO,int vehicleID)"+success);
	}catch (Exception e){		
		logger.error("Exception in updateVehicleDetails : "+e);
		success=0;
	}
	return success;
}


public List getAssociateMembers(int aptID){
	List memberList=new ArrayList();
	logger.debug("Entry : public MemberVO getAssociateMembers(int aptID)"+aptID);
	
String strSQL = "";
	
	try{	
		
	//1. SQL Query
	strSQL = "select * from member_details where apt_id = :aptID and type='A' ";
	logger.debug("query : " + strSQL);
	// 2. Parameters.
	 SqlParameterSource namedParameters = new MapSqlParameterSource("aptID", aptID);
   
	//3. RowMapper.
	 RowMapper RMapper = new RowMapper() {
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	            MemberVO memberVO = new MemberVO();
	            memberVO.setTitle(rs.getString("title"));
	            memberVO.setMemberID(rs.getInt("member_id"));
	            memberVO.setAptID(rs.getInt("apt_id"));
	            memberVO.setFullName(rs.getString("full_name"));
	            memberVO.setEmail(rs.getString("email"));
	            memberVO.setMobile(rs.getString("mobile"));
	            memberVO.setMembershipID(rs.getInt("membership_id"));
	           
	            return memberVO;
	        }};

	        memberList = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
	        

	        logger.debug("outside query : " + memberList.size() );
	        logger.debug("Exit : public MemberVO getAssociateMembers(int aptID)");
	}catch (Exception ex){
		logger.error("Exception in getVehicleList : "+ex);
		
	}
	
	logger.debug("Exit : public MemberVO getAssociateMembers(int aptID)");
	return memberList;
}

public List getMemberListforApartment(int aptID){
	List memberList=new ArrayList();
	logger.debug("Entry : public MemberVO getMemberListforApartment(int aptID)"+aptID);
	
String strSQL = "";
	
	try{	
		
	//1. SQL Query
	strSQL = "select * from member_details where apt_id = :aptID order by type desc ";
	logger.debug("query : " + strSQL);
	// 2. Parameters.
	 SqlParameterSource namedParameters = new MapSqlParameterSource("aptID", aptID);
   
	//3. RowMapper.
	 RowMapper RMapper = new RowMapper() {
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	            MemberVO memberVO = new MemberVO();
	            memberVO.setTitle(rs.getString("title"));
	            memberVO.setMemberID(rs.getInt("member_id"));
	            memberVO.setAptID(rs.getInt("apt_id"));
	            memberVO.setFullName(rs.getString("full_name"));
	            memberVO.setEmail(rs.getString("email"));
	            memberVO.setMobile(rs.getString("mobile"));
	            memberVO.setMembershipID(rs.getInt("membership_id"));
	           
	            return memberVO;
	        }};

	        memberList = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
	        

	        logger.debug("outside query : " + memberList.size() );
	        logger.debug("Exit : public MemberVO getMemberListforApartment(int aptID)");
	}catch (Exception ex){
		logger.error("Exception in getMemberListforApartment : "+ex);
		
	}
	
	logger.debug("Exit : public MemberVO getMemberListforApartment(int aptID)");
	return memberList;
}

public int updateMemberDetails(MemberVO memberVO,int memberId){
	
	int flag=0;
	int success=0;
	
	try {
		logger.debug("Entry : public int updateMemberDetails(MemberVO memberVO,String memberId)");
		
		String query="UPDATE member_details SET  email= :email ,title=:title,mobile=:mobile, phone=:phone,full_name=:fullName," +
				" dob=:dateOfBirth, age=:age ,occupation=:occupation, issue_date=:membershipDate, tags=:tags ,gstin=:gstin, pan_no=:panNo" +
				    "  WHERE apt_id="+memberVO.getAptID()+" AND member_id="+memberId+"";
		logger.debug("query : " + query);
		
		Map hmap=new HashMap();
		hmap.put("title",memberVO.getTitle());
		//hmap.put("firstName", memberVO.getFirstName());
		//hmap.put("lastName", memberVO.getLastName());
		hmap.put("email", memberVO.getEmail());
		hmap.put("dateOfBirth", memberVO.getDateOfBirth());
		hmap.put("mobile", memberVO.getMobile());
		hmap.put("phone", memberVO.getPhone());
		hmap.put("fullName", memberVO.getFullName());
		hmap.put("membershipDate", memberVO.getMembershipDate());
		hmap.put("age", memberVO.getAge());
		hmap.put("occupation", memberVO.getOccupation());
		hmap.put("tags", memberVO.getTags());
		hmap.put("gstin", memberVO.getGstin());
		hmap.put("panNo", memberVO.getPanNo());
		
		flag=namedParameterJdbcTemplate.update(query, hmap);
		
		if(flag != 0){
			
		String sqlQuery=" UPDATE apartment_details SET is_Rented= :is_Rented WHERE apt_id="+memberVO.getAptID()+" ";
		logger.debug("query : " + sqlQuery);
		
		   Map map=new HashMap();
		   map.put("is_Rented", memberVO.getIsRented());
		   
		   success=namedParameterJdbcTemplate.update(sqlQuery, map);
		}
		if(flag != 0 && success == 1){
			flag = 1;
		}
	
		logger.debug("Exit : public int updateMemberDetails(MemberVO memberVO,String memberId)"+flag);
		
	}catch(DataAccessException exc){
		logger.error("DataAccessException in updateMemberDetails : "+exc);
		
	}catch(Exception e){		
		logger.error("Exception in updateMemberDetails : "+e);
	}
	return flag;
}
 

public int addRenterDetails(RenterVO renterVO ){
	int flag=0;

	try{
		logger.debug("Entry :public int addRenterDetails(RenterVO renterVO )");
				
		String insertFileString = "INSERT INTO renter_details "
			+ "( apt_id,title, renter_first_name, renter_last_name, mobile, email, is_current_renter, is_verified, agg_start_date, agg_end_date, description, is_deleted, is_document_received, is_nri,renter_full_name ,age ,gender ,groups, adhaar_card_no, move_in_date, move_out_date, agent_id) VALUES "
			+ "( :aptID, :title, :firstName, :lastName, :mobile, :email , :isCurrentRenter, :isVerified, :aggSrtDate, :aggEndDate, :note, :isDeleted, :isDocsReceved, :isNRI, :fullName, :age ,:gender ,:group ,:adhaarCardNo, :moveInDate ,:moveOutDate ,:agentID) ";
		logger.debug("query : " + insertFileString);
		
   SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(renterVO);
   KeyHolder keyHolder = new GeneratedKeyHolder();
	getNamedParameterJdbcTemplate().update(
			insertFileString, fileParameters, keyHolder);
   int	genratedID = keyHolder.getKey().intValue();
	logger.debug("key GEnereator ---" + genratedID);	
   
	flag=genratedID;  			
		
	}catch(DataAccessException e) {
		logger.error("DataAccessException in addRenterDetails : "+e);
	}
	
	catch(Exception ex){
		logger.error("Exception in addRenterDetails : "+ex);
	}
	logger.debug("Exit :public int addRenterDetails(RenterVO renterVO )"+flag);
	return flag;
}
	
public int addRenterMemberDetails(RenterVO renterVO ){
	int flag=0;

	try{
		logger.debug("Entry :public int addRenterMemberDetails(RenterVO renterVO )");
				
		String insertFileString = "INSERT INTO renter_members "
			+ "( renter_id,title, full_name,  mobile, email, gender, age ) VALUES "
			+ "( :renterID, :title, :fullName, :mobile, :email , :gender, :age) ";
		logger.debug("query : " + insertFileString);
		
   SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(renterVO);
   KeyHolder keyHolder = new GeneratedKeyHolder();
	flag = getNamedParameterJdbcTemplate().update(
			insertFileString, fileParameters, keyHolder);
   
				
		
	}catch(DataAccessException e) {
		logger.error("DataAccessException in addRenterMemberDetails : "+e);
	}
	
	catch(Exception ex){
		logger.error("Exception in addRenterMemberDetails : "+ex);
	}
	logger.debug("Exit :public int addRenterMemberDetails(RenterVO renterVO )"+flag);
	return flag;
}

public List getRenterDetails(int aptID , int renterID){
	
	List rentrVOList=null;
	String condition="";
   if(renterID==0){
		condition=" AND r.renter_id=:renterID";
	}else{
		condition=" AND r.apt_id=:aptID AND r.renter_id=:renterID ";		
	}
		
	try {
		logger.debug("Entry : public RenterVO getRenterDetails(int aptID , int renterID)");
		
		String query="SELECT r.*,a.* FROM renter_details r LEFT JOIN agent_details a ON r.agent_id=a.agent_id WHERE r.is_current_renter='1' "+condition+" ";
		
		logger.debug("query : " + query);
		Map namedParameters = new HashMap();
		namedParameters.put("aptID", aptID);
		namedParameters.put("renterID", renterID);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				RenterVO rentrVO=new RenterVO();
				rentrVO.setRenterID(rs.getInt("r.renter_id"));
				rentrVO.setAptID(rs.getInt("r.apt_id"));
				rentrVO.setTitle(rs.getString("title"));
			    rentrVO.setFullName(rs.getString("renter_full_name"));
				rentrVO.setMobile(rs.getString("mobile"));
				rentrVO.setEmail(rs.getString("email"));
				rentrVO.setIsCurrentRenter(rs.getString("is_current_renter"));
				rentrVO.setAggSrtDate(rs.getString("agg_start_date"));
				rentrVO.setAggEndDate(rs.getString("agg_end_date"));
				rentrVO.setIsVerified(rs.getInt("is_verified"));
				rentrVO.setIsDocsReceved(rs.getInt("is_document_received"));
				rentrVO.setIsNRI(rs.getInt("is_nri"));
				rentrVO.setNote(rs.getString("description"));
				rentrVO.setIsDeleted(rs.getString("is_deleted"));
				rentrVO.setIsNOCGiven(rs.getInt("is_noc_given"));
				rentrVO.setAge(rs.getInt("age"));
				rentrVO.setGender(rs.getString("gender"));
				rentrVO.setGroup(rs.getString("groups"));
				rentrVO.setAdhaarCardNo(rs.getString("adhaar_card_no"));
				rentrVO.setMoveInDate(rs.getString("move_in_date"));
				rentrVO.setMoveOutDate(rs.getString("move_out_date"));
				rentrVO.setAgentID(rs.getInt("agent_id"));
				rentrVO.setAgentName(rs.getString("a.agent_name"));
				rentrVO.setAgentMobile(rs.getString("a.mobile"));
				rentrVO.setAgentEmail(rs.getString("a.email"));
				return rentrVO;
			}
		};
		
		rentrVOList = namedParameterJdbcTemplate.query(query, namedParameters, RMapper);
	}catch (DataAccessException e){
	      logger.error("DataAccessException in getRenterDetails : "+e);
	}
	catch (Exception ex){
		logger.error("Exception in getRenterDetails : "+ex);
	}
	
	logger.debug("Exit : public RenterVO getRenterDetails(String apartmentID)");
	return rentrVOList;
}

public int updateRenterMemberDetails(RenterVO renterVO,String renterId){
	int flag=0;
	try {
		logger.debug("Entry : public int updateRenterMemberDetails(RenterVO renterVO,String renterId)");
		
		String query=" UPDATE renter_members SET title=:title, full_name=:fullName, mobile=:mobile, email=:email, age=:age, gender=:gender WHERE id="+renterId+"  ; ";
		logger.debug("query : " + query);
		
		Map hmap=new HashMap();
		hmap.put("title", renterVO.getTitle());
		hmap.put("fullName", renterVO.getFullName());
		hmap.put("mobile", renterVO.getMobile());
		hmap.put("email", renterVO.getEmail());
		hmap.put("age", renterVO.getAge());
		hmap.put("gender", renterVO.getGender());
		
		
		flag=namedParameterJdbcTemplate.update(query, hmap);
		
		logger.debug("Exit : public int updateRenterMemberDetails(RenterVO renterVO,String renterId)"+flag);
	}catch (Exception e){		
		logger.error("Exception in updateRenterMemberDetails : "+e);
	}
	return flag;
}

public List getRenterMemberDetails(int renterID){
	
	List rentrVOList=null;
		
	try {
		logger.debug("Entry : public RenterVO getRenterMemberDetails(String renterID)");
		
		String query="SELECT * FROM renter_members WHERE renter_id=:renterID;";
		logger.debug("query : " + query);
		Map namedParameters = new HashMap();
		namedParameters.put("renterID", renterID);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				RenterVO rentrVO=new RenterVO();
				rentrVO.setRenterID(rs.getInt("id"));
				rentrVO.setTitle(rs.getString("title"));
				rentrVO.setFullName(rs.getString("full_name"));
				rentrVO.setMobile(rs.getString("mobile"));
				rentrVO.setEmail(rs.getString("email"));
				rentrVO.setAge(rs.getInt("age"));
				rentrVO.setGender(rs.getString("gender"));
				return rentrVO;
			}
		};
		
		rentrVOList = namedParameterJdbcTemplate.query(query, namedParameters, RMapper);
	}catch (DataAccessException e){
	      logger.error("DataAccessException in getRenterMemberDetails : "+e);
	}
	catch (Exception ex){
		logger.error("Exception in getRenterMemberDetails : "+ex);
	}
	
	logger.debug("Exit : public RenterVO getRenterMemberDetails(String apartmentID)");
	return rentrVOList;
}

public List getRenterHistoryDetails(int apartmentID){
	List renterHistoryList=null;
	
	try {
		logger.debug("Entry : public List getRenterHistoryDetails(String apartmentID)");
		
		String query="SELECT r.*,a.* FROM renter_details r LEFT JOIN agent_details a ON r.agent_id=a.agent_id  WHERE r.apt_id=:aptID;";
		
		logger.debug("query : " + query);
		Map namedParameters = new HashMap();
		namedParameters.put("aptID", apartmentID);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				RenterVO rentrVO=new RenterVO();
				rentrVO.setRenterID(rs.getInt("renter_id"));
				rentrVO.setAptID(rs.getInt("apt_id"));
				rentrVO.setTitle(rs.getString("title"));
				rentrVO.setFirstName(rs.getString("renter_first_name"));
				rentrVO.setLastName(rs.getString("renter_last_name"));
				rentrVO.setMobile(rs.getString("mobile"));
				rentrVO.setEmail(rs.getString("email"));
				rentrVO.setFullName(rs.getString("renter_full_name"));
				rentrVO.setIsCurrentRenter(rs.getString("is_current_renter"));
				rentrVO.setAggSrtDate(rs.getString("agg_start_date"));
				rentrVO.setAggEndDate(rs.getString("agg_end_date"));
				rentrVO.setIsVerified(rs.getInt("is_verified"));
				rentrVO.setIsDocsReceved(rs.getInt("is_document_received"));
				rentrVO.setIsNRI(rs.getInt("is_nri"));
				rentrVO.setNote(rs.getString("description"));
				rentrVO.setIsDeleted(rs.getString("is_deleted"));
				rentrVO.setIsNOCGiven(rs.getInt("is_noc_given"));
				rentrVO.setAge(rs.getInt("age"));
				rentrVO.setGender(rs.getString("gender"));
				rentrVO.setGroup(rs.getString("groups"));
				rentrVO.setAdhaarCardNo(rs.getString("adhaar_card_no"));
				rentrVO.setMoveInDate(rs.getString("move_in_date"));
				rentrVO.setMoveOutDate(rs.getString("move_out_date"));
				rentrVO.setAgentID(rs.getInt("agent_id"));
				rentrVO.setAgentName(rs.getString("agent_name"));
				rentrVO.setAgentMobile(rs.getString("a.mobile"));
				rentrVO.setAgentEmail(rs.getString("a.email"));
				
				return rentrVO;
			}
		};
		
		renterHistoryList = namedParameterJdbcTemplate.query(query, namedParameters, RMapper);
		
	} catch (DataAccessException e) {
	    logger.error("DataAccessException in getRenterHistoryDetails : "+e);
	}
	catch (Exception ex) {
		logger.error("Exception in getRenterHistoryDetails : "+ex);
	}
	
	logger.debug("Exit : public List getRenterHistoryDetails(String apartmentID)");
	return renterHistoryList;
}



 public int updateRenterForApartment(int apartmentID,int genratedID){
	 int success=0;
		try {
			logger.debug("Entry : public int updateRenterForApartment(String apartmentID,int genratedID)");
			
			String query="UPDATE renter_details SET  WHERE renter_full_name=:fullName AND renter_id!=:genID ;";
			logger.debug("query : " + query);
			
			Map hmap=new HashMap();
			hmap.put("aptID", apartmentID);
			hmap.put("genID", genratedID);
			
			success=namedParameterJdbcTemplate.update(query, hmap);
			
			logger.debug("Exit : public int updateRenterForApartment(String apartmentID,int genratedID)"+success);
		}catch(Exception e){			
			logger.error("Exception in updateRenterForApartment : "+e);
		}
		return success;
 }

public int updateRenterDetails(RenterVO renterVO,int renterId){
	int flag=0;
	try {
		logger.debug("Entry : public int updateRenterDetails(RenterVO renterVO,String renterId)");
		
		String query=" UPDATE renter_details SET title=:title, renter_full_name=:fullName, mobile=:mobile, email=:email, is_verified=:isVerified, is_document_received=:docsRecvd, is_nri=:isNri, agg_start_date=:aggSrtDate, agg_end_date=:aggEndDate," +
				" description=:note,is_deleted=:isDeleted, age=:age, gender=:gender, groups=:group, adhaar_card_no=:adhaarNo, move_in_date=:moveInDate, move_out_date=:moveOutDate, agent_id=:agentId" +
				"  WHERE renter_id="+renterId+"  ; ";
		logger.debug("query : " + query);
		
		Map hmap=new HashMap();
		hmap.put("title", renterVO.getTitle());
		//hmap.put("firstName", renterVO.getFirstName());
		//hmap.put("lastName", renterVO.getLastName());
		hmap.put("fullName", renterVO.getFullName());
		hmap.put("mobile", renterVO.getMobile());
		hmap.put("email", renterVO.getEmail());
		hmap.put("docsRecvd", renterVO.getIsDocsReceved());
		hmap.put("isNri", renterVO.getIsNRI());
		hmap.put("isVerified", renterVO.getIsVerified());
		hmap.put("aggSrtDate", renterVO.getAggSrtDate());
		hmap.put("aggEndDate", renterVO.getAggEndDate());
		hmap.put("note", formattedDate+"- Updated By:-"+renterVO.getUserName()+"\n"+renterVO.getComment()+"\n---------------------\n"+renterVO.getNote());
		hmap.put("isDeleted", renterVO.getIsDeleted());
		hmap.put("age", renterVO.getAge());
		hmap.put("gender", renterVO.getGender());
		hmap.put("group", renterVO.getGroup());
		hmap.put("adhaarNo", renterVO.getAdhaarCardNo());
		hmap.put("moveInDate", renterVO.getMoveInDate());
		hmap.put("moveOutDate", renterVO.getMoveOutDate());
		hmap.put("agentId", renterVO.getAgentID());
		
		flag=namedParameterJdbcTemplate.update(query, hmap);
		
		logger.debug("Exit : public int updateRenterDetails(RenterVO renterVO,String renterId)"+flag);
	}catch (Exception e){		
		logger.error("Exception in updateRenterDetails : "+e);
	}
	return flag;
}

public int updateAgentDetails(RenterVO renterVO,int agentId){
	int flag=0;
	try {
		logger.debug("Entry : public int updateAgentDetails(RenterVO renterVO,String agentId)");
		
		String query=" UPDATE agent_details SET  agent_name=:agent_name, mobile=:mobile, email=:email where agent_id="+agentId+"  ; ";
		logger.debug("query : " + query);
		
		Map hmap=new HashMap();
		hmap.put("agent_name", renterVO.getAgentName());
		hmap.put("mobile", renterVO.getAgentMobile());
		hmap.put("email", renterVO.getAgentEmail());
		
		flag=namedParameterJdbcTemplate.update(query, hmap);
		
		logger.debug("Exit : public int updateAgentDetails(RenterVO renterVO,String agentId)"+flag);
	}catch (Exception e){		
		logger.error("Exception in updateAgentDetails : "+e);
	}
	return flag;
}

public int updateRenterNOCStatus(String renterId,String aptId,int nocStatus){
	
	int sucess = 0;
	int flag=0;
	String strSQL = "";
	try {
			logger.debug("Entry : public int updateRenterNOCStatus(String renterId)");
			strSQL = "Update renter_details SET is_noc_given=:nocStatus WHERE renter_id=:renterID and apt_id=:aptID ;" ;
			logger.debug("query : " + strSQL);
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("nocStatus", nocStatus);
			namedParameters.put("renterID", renterId);
			namedParameters.put("aptID",aptId );
					
			sucess = namedParameterJdbcTemplate.update(strSQL,namedParameters);
			
			
			
			logger.debug("Exit : public int updateRenterNOCStatus(String renterId)"+sucess);

	}catch(Exception Ex){
		
		Ex.printStackTrace();
		logger.error("Exception in deleteRenterDetails : "+Ex);
		
	}
	return sucess;
}	 

public int deleteRenterDetails(int renterId,int aptId){
	
	int sucess = 0;
	int flag=0;
	String strSQL = "";
	try {
			logger.debug("Entry : public int deleteRenterDetails(String renterId)");
			strSQL = "Update renter_details SET is_deleted='1',is_current_renter='0' WHERE renter_id="+renterId+";" ;
			logger.debug("query : " + strSQL);
			// 2. Parameters.
			Map namedParameters = new HashMap();
					
			sucess = namedParameterJdbcTemplate.update(strSQL,namedParameters);
			
			if(sucess == 1){
				String query = "UPDATE apartment_details SET is_Rented='0' WHERE apt_id="+aptId+" " ;
				logger.debug("query : " + query);
				// 2. Parameters.
				Map namedParameter = new HashMap();
								
				flag = namedParameterJdbcTemplate.update(query,namedParameter);
				
			}
			if(flag != 0 && sucess ==1){
				flag=1;
			}
			
			logger.debug("Exit : public int deleteRenterDetails(String renterId)"+flag);

	}catch(Exception Ex){
		
		Ex.printStackTrace();
		logger.error("Exception in deleteRenterDetails : "+Ex);
		
	}
	return flag;
}	 
 
public int deleteRenterMember(int renterMemberId){
	
	int sucess = 0;
	String strSQL = "";
	try {
			logger.debug("Entry : public int deleteRenterMember(String renterMemberId)");
			strSQL = "DELETE FROM renter_members WHERE id="+renterMemberId+" ";
			logger.debug("query : " + strSQL);
			// 2. Parameters.
			Map namedParameters = new HashMap();				
			
			sucess = namedParameterJdbcTemplate.update(strSQL,namedParameters);
			
			logger.debug("Exit : public int deleteRenterMember(String renterMemberId)"+sucess);

	}catch(Exception Ex){		
		Ex.printStackTrace();
		logger.error("Exception in deleteRenterMember : "+Ex);		
	}
	return sucess;
}	


public MemberVO getMemberNameDetails(String memeberID){
	MemberVO memberVo=new MemberVO();
	
	try {
		logger.debug("Entry : public MemberVO getMemberNameDetails(String memeberID)");
		
		String query="SELECT CONCAT(building_name,' ',apt_name)AS building_names,full_name, member_id,apt_name,first_name,last_name,building_name  FROM building_details,apartment_details,member_details "+
		"	WHERE member_details.apt_id=apartment_details.apt_id AND apartment_details.building_id=building_details.building_id  AND member_id=:memebrID AND member_details.type='P' and is_current!=0 ";
	logger.debug("query : " + query);
	Map namedParameters = new HashMap();
	namedParameters.put("memebrID", memeberID);
	
	RowMapper RMapper = new RowMapper() {
		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			MemberVO memberVO=new MemberVO();
			memberVO.setTitle(rs.getString("building_names"));
			memberVO.setFullName(rs.getString("full_name"));
			return memberVO;
		}
	};
	
	memberVo = (MemberVO) namedParameterJdbcTemplate.queryForObject(query, namedParameters, RMapper);
		
	} catch (DataAccessException e) {
	    logger.info("No data found : "+e);
	}
	catch (Exception ex) {
		logger.error("Exception in getMemberNameDetails : "+ex);
	}
	
	logger.debug("Exit : public MemberVO getMemberNameDetails(String memeberID)");
	return memberVo;
}


public List getMemberListForSearchByName(String strSocietyID){

    List listMembers = null;
	String strSQL = "";
	
	try{	
	logger.debug("Entry : public List getMemberListForSearchByName(String strSocietyID)" );
	SocietyVO societyVO=societyService.getSocietyDetails(Integer.parseInt(strSocietyID));
	//1. SQL Query
	strSQL = "SELECT apartment_details.apt_id,intercomm_no,type_of_unit,cost,societymonthlyCharges,apartment_details.is_Rented, apt_name,unit_id,apartment_details.inv_group,member_details.*,society_id,building_details.building_name,sq_feet,phone ,age ,issue_date,ledger_id,building_details.building_id FROM building_details,apartment_details,member_details,ledger_user_mapping_"+societyVO.getDataZoneID()+" lu " +
	"WHERE member_details.apt_id=apartment_details.apt_id AND apartment_details.building_id=building_details.building_id and lu.org_id=building_details.society_id  AND building_details.society_id = :society_ID AND lu.user_id=member_details.member_id and member_details.type='P' and is_current!=0 and unit_sold_status='SOLD' order by seq_no;";

	logger.debug("query : " + strSQL);
	// 2. Parameters.
	 SqlParameterSource namedParameters = new MapSqlParameterSource("society_ID", strSocietyID);
   
	//3. RowMapper.s
	 RowMapper RMapper = new RowMapper() {
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	            MemberVO membrs = new MemberVO();
	            membrs.setMemberID(rs.getInt("member_id"));
	            membrs.setMembershipID(rs.getInt("membership_id"));
	            membrs.setOldEmailID(rs.getString("email"));
	            membrs.setAptID(rs.getInt("apt_id"));
	            membrs.setBuildingID(rs.getInt("building_id"));
	            membrs.setFlatNo(rs.getString("building_name")+" "+rs.getString("apt_name"));
	            membrs.setTitle(rs.getString("title"));
	            membrs.setFullName( rs.getString("full_name"));
	            membrs.setFullNameRev(rs.getString("full_name")+" - "+rs.getString("building_name")+" "+rs.getString("apt_name") );
	            membrs.setMobile(rs.getString("mobile"));
	            membrs.setPhone(rs.getString("phone"));
	            membrs.setIntercom(rs.getString("intercomm_no"));
	            membrs.setEmail(rs.getString("email"));
	            membrs.setMembershipDate(rs.getString("issue_date"));
	            membrs.setIsRented(rs.getInt("is_Rented"));
	            membrs.setCostOfUnit(rs.getBigDecimal("cost"));
	            membrs.setAptType(rs.getString("type_of_unit"));
	            membrs.setMmcDetails(rs.getBigDecimal("societymonthlyCharges"));
	            membrs.setSocietyID(rs.getInt("society_id"));
	            membrs.setArea(rs.getBigDecimal("sq_feet"));
	            membrs.setPhone(rs.getString("phone"));
	            membrs.setAge(rs.getInt("age"));
	            membrs.setUnitID(rs.getInt("unit_id"));
	            membrs.setDateOfBirth(rs.getString("dob"));
	            membrs.setOccupation(rs.getString("occupation"));
	            membrs.setLedgerID(rs.getInt("ledger_id"));
	            membrs.setInvoiceGroups(rs.getString("inv_group"));
	            
	            return membrs;
	        }};

	        listMembers = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
	    		        		        
	        logger.debug("Exit :public List getMemberListForSearchByName(String strSocietyID)"+listMembers.size());
	}catch(Exception ex){
		logger.error("Exception in getMemberListForSearchByName : "+ex);
		
	}
	return listMembers;
	
	
}


public List getMemberListBuildingWise(int buildingID,int societyID){

    List listMembers = null;
	String strSQL = "";
	String condition="";
	try{	
	logger.debug("Entry : public List getMemberListBuildingWise(String buildingID,String societyID)" );
	if(buildingID==0){
		condition= " ";
	}else{
		condition= " AND building_details.building_id = :buildingID ";
	}
	
	//1. SQL Query
	SocietyVO societyVO=societyService.getSocietyDetails(societyID);
	strSQL = "SELECT apartment_details.apt_id,type_of_unit,societymonthlyCharges,apartment_details.is_Rented as rented,sq_feet,intercomm_no, apt_name,apartment_details.inv_group,member_details.*,society_id,building_details.building_name ,building_details.building_id,age,issue_date,ledger_id FROM building_details,apartment_details,member_details ,ledger_user_mapping_"+societyVO.getDataZoneID()+" lu " +
	"WHERE member_details.apt_id=apartment_details.apt_id AND member_details.member_id=lu.user_id and lu.ledger_type='M' AND apartment_details.building_id=building_details.building_id  "+condition+" AND member_details.type='P' and is_current!=0 and lu.org_id=:societyID  and unit_sold_status='SOLD' order by seq_no;";
	logger.debug("query : " + strSQL);
	// 2. Parameters.
	 Map hMap=new HashMap();
	 hMap.put("buildingID", buildingID);
	 hMap.put("societyID", societyID);
	//3. RowMapper.s
	 RowMapper RMapper = new RowMapper() {
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	            MemberVO membrs = new MemberVO();
	            membrs.setMemberID(rs.getInt("member_id"));
	            membrs.setMembershipID(rs.getInt("membership_id"));
	            membrs.setOldEmailID(rs.getString("email"));
	            membrs.setAptID(rs.getInt("apt_id"));
	            membrs.setBuildingID(rs.getInt("building_id"));
	            membrs.setFlatNo(rs.getString("building_name")+" "+rs.getString("apt_name"));
	            membrs.setTitle(rs.getString("title"));
	            membrs.setFullName(rs.getString("full_name") );
	            membrs.setFullNameRev(rs.getString("full_name")+" - "+rs.getString("building_name")+" "+rs.getString("apt_name") );
	            membrs.setOccupation(rs.getString("occupation"));
	            membrs.setMobile(rs.getString("mobile"));
	            membrs.setPhone(rs.getString("phone"));
	            membrs.setEmail(rs.getString("email"));
	            membrs.setMembershipDate(rs.getString("issue_date"));
	            membrs.setDateOfBirth(rs.getString("dob"));
	            membrs.setIsRented(rs.getInt("rented"));
	            membrs.setAptType(rs.getString("type_of_unit"));
	            membrs.setMmcDetails(rs.getBigDecimal("societymonthlyCharges"));
	            membrs.setSocietyID(rs.getInt("society_id"));
	            membrs.setAge(rs.getInt("age"));
	            membrs.setDateOfBirth(rs.getString("dob"));
	            membrs.setLedgerID(rs.getInt("ledger_id"));
	            membrs.setOccupation(rs.getString("occupation"));
	            membrs.setArea(rs.getBigDecimal("sq_feet"));
	            membrs.setSocietyMMC(rs.getString("societymonthlyCharges"));
	            membrs.setIntercom(rs.getString("intercomm_no"));
	            membrs.setMembershipID(rs.getInt("membership_id"));
	            membrs.setMembershipDate(rs.getString("issue_date"));
	            membrs.setBuildingID(rs.getInt("building_id"));
	            membrs.setInvoiceGroups(rs.getString("inv_group"));
	            return membrs;
	        }};

	        listMembers = namedParameterJdbcTemplate.query(strSQL, hMap, RMapper);
	    		        		        
	        logger.debug("Exit :public List getMemberListBuildingWise(String buildingID,String societyID)"+listMembers.size());
	}catch (BadSqlGrammarException e) {
		logger.info("No members found "+e);
	        
	}catch(Exception ex){
		logger.error("Exception in getMemberListBuildingWise(String buildingID,String societyID) : "+ex);
		
	}
	return listMembers;
	
	
}



public List getVehicleListForSearch(String societyId){

	List listVechicles = null;	
	String strSQL = "";
	
	try{	
		logger.debug("Entry : public List getVehicleListForSearch(String societyId)" );
	//1. SQL Query
		SocietyVO societyVO=societyService.getSocietyDetails(Integer.getInteger(societyId));
	strSQL = "SELECT apartment_details.apt_id,apt_name,apartment_details.is_Rented,type_of_unit,cost,building_details.building_name,vehicle_details.*,member_details.*,ledger_id FROM vehicle_details ,building_details,apartment_details,member_details ,ledger_user_mapping_"+societyVO.getDataZoneID()+" lu "
	         + "WHERE member_details.apt_id=apartment_details.apt_id AND apartment_details.building_id=building_details.building_id and member_details.member_id=lu.user_id "
	         + "AND member_details.member_id=vehicle_details.member_id AND member_details.type='P' and is_current!=0 and lu.org_id=building_details.society_id "
	         + "AND building_details.society_id = :society_ID; ";
	logger.debug("query : " + strSQL);
	// 2. Parameters.
	 SqlParameterSource namedParameters = new MapSqlParameterSource("society_ID", societyId);
   
	//3. RowMapper.
	 RowMapper RMapper = new RowMapper() {
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	            VehicleVO vehicle = new VehicleVO();
	            vehicle.setVehicleID(rs.getInt("vehicle_id"));
	            vehicle.setVehicleType(rs.getString("vehicle_type"));
	            vehicle.setRegNumber(rs.getString("reg_number")+" "+rs.getString("vehicle_number"));
	            vehicle.setVehicleNo(rs.getString("vehicle_number"));
	            
	            vehicle.setDescription(rs.getString("vehicle_description"));
	            vehicle.setCommercialType(rs.getString("commercial"));
	            vehicle.setStickerId(rs.getString("sticker_id"));
	            vehicle.setMemberID(rs.getInt("member_id"));
	            vehicle.setAptID(rs.getInt("apt_id"));
	            vehicle.setFlatNo(rs.getString("building_name")+" "+rs.getString("apt_name"));
	            vehicle.setTitle(rs.getString("title"));
	            vehicle.setFirstName(rs.getString("first_name"));
	            vehicle.setLastName(rs.getString("last_name"));
	            vehicle.setAptType(rs.getString("type_of_unit"));
	            vehicle.setFullName( rs.getString("full_name") );
	            vehicle.setMobile(rs.getString("mobile"));
	            vehicle.setEmail(rs.getString("email"));
	            vehicle.setCost(rs.getBigDecimal("cost"));
	            vehicle.setIsRented(rs.getInt("is_Rented"));
	            vehicle.setLedgerID(rs.getInt("ledger_id"));
	            return vehicle;
	        }};

	        listVechicles = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
	        

	        logger.debug("outside query : " + listVechicles.size() );
			logger.debug("Exit : public List getVehicleListForSearch(String societyId)" );
			
	}catch (BadSqlGrammarException e) {
		logger.info("No members found "+e);
	 
	}catch (Exception ex){
		logger.error("Exception in getVehicleListForSearch : "+ex);
		
	}
	return listVechicles;
	
	
}

public MemberVO getMemberInfo(int aptID){
	MemberVO memberVo=new MemberVO();
	
	try {
		logger.debug("Entry : public MemberVO getMemberInfo(String memeberID)");
		
		String query= "SELECT a.apt_id,m.apt_name,intercomm_no,societymonthlyCharges,a.is_Rented,a.inv_group,type_of_unit,m.*, s.short_name,cert.*,cert.share_cert_no As ShareCertNo  "
		             + "FROM ( apartment_details a,members_info m ,society_details s,member_details mi ) LEFT JOIN  share_cert_details cert "
			         + " ON m.apt_id=cert.apt_id WHERE m.apt_id=a.apt_id  and m.member_id=mi.member_id and mi.apt_id=m.apt_id AND m.society_id=s.society_id  AND m.apt_id=:aptID AND m.type='P' and m.is_current!=0 ;";
	logger.debug("query : " + query);
	Map namedParameters = new HashMap();
	namedParameters.put("aptID", aptID);
	
	RowMapper RMapper = new RowMapper() {
		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			MemberVO memberVO=new MemberVO();
			ShareRegisterVO shareCertVO =new ShareRegisterVO();
			
			    memberVO.setMemberID(rs.getInt("member_id"));
			    memberVO.setSocietyID(rs.getInt("society_id"));
			    memberVO.setMembershipID(rs.getInt("membership_id"));
	            memberVO.setOldEmailID(rs.getString("email"));
			    memberVO.setAptID(rs.getInt("apt_id"));
			    memberVO.setFlatNo(rs.getString("unit_name"));
			    memberVO.setTitle(rs.getString("title"));
			    memberVO.setFullName(rs.getString("full_name") );
			    memberVO.setMobile(rs.getString("mobile"));
	            memberVO.setMembershipDate(rs.getString("issue_date"));
	            memberVO.setPhone(rs.getString("phone"));
	            memberVO.setIntercom(rs.getString("intercomm_no"));
	            memberVO.setEmail(rs.getString("email"));
	            memberVO.setIsRented(rs.getInt("is_Rented"));
	            memberVO.setMmcDetails(rs.getBigDecimal("societymonthlyCharges"));
	            memberVO.setAptType(rs.getString("type_of_unit"));
	            memberVO.setAge(rs.getInt("age"));
	            memberVO.setDateOfBirth(rs.getString("dob"));
	            memberVO.setBuildingID(rs.getInt("building_id"));
	            memberVO.setOccupation(rs.getString("occupation"));
	            memberVO.setSocietyID(rs.getInt("society_id"));
	            memberVO.setInvoiceGroups(rs.getString("inv_group"));
	            memberVO.setSocietyName(rs.getString("short_name"));
	            memberVO.setArea(rs.getBigDecimal("sq_feet"));
	            memberVO.setApartmentName(rs.getString("apt_name"));    
	            memberVO.setBuildingName(rs.getString("building_name")); 
	            memberVO.setGstin(rs.getString("gstin"));
	            memberVO.setSupplyState(rs.getString("state_of_supply"));
	            memberVO.setPanNo(rs.getString("pan_no"));
	            shareCertVO.setAllotmentDate(rs.getString("share_date"));
	            shareCertVO.setShareCertNo(rs.getString("ShareCertNo"));
	            shareCertVO.setShareFrom(rs.getInt("share_from"));
	            shareCertVO.setShareTo(rs.getInt("share_upto"));
	            shareCertVO.setNoOfShares(rs.getInt("no_of_share"));
	            shareCertVO.setFaceValue(rs.getBigDecimal("face_value"));
	            shareCertVO.setTotalValue(rs.getBigDecimal("total_share_value"));
	            memberVO.setShareCertVO(shareCertVO);     
	                     
	        	           
			return memberVO;
		}
	};
	
	memberVo = (MemberVO) namedParameterJdbcTemplate.queryForObject(query, namedParameters, RMapper);
		
	} catch (DataAccessException e) {
	    logger.error("DataAccessException in getMemberInfo : "+e);
	}
	catch (Exception ex) {
		logger.error("Exception in getMemberInfo : "+ex);
	}
	return memberVo;
}

public MemberWSVO getMembersInfoWithMail(String email){
	MemberWSVO memberVo=new MemberWSVO();
	
	try {
		logger.debug("Entry : public MemberWSVO getMembersInfoWithMail(String aptID)"+email);
		
		String query= "SELECT a.apt_id,apt_name,intercomm_no,societymonthlyCharges,a.is_Rented,type_of_unit,b.building_name,b.building_id,b.society_id,m.* ,society_name "
		             + " FROM building_details b,apartment_details a,member_details m,society_details s "
			         + "WHERE m.apt_id=a.apt_id AND b.society_id=s.society_id AND a.building_id=b.building_id  AND m.email=:email  AND is_current!=0 LIMIT 1;";
	logger.debug("query : " + query);
	Map namedParameters = new HashMap();
	namedParameters.put("email", email);
	
	RowMapper RMapper = new RowMapper() {
		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			MemberWSVO memberVO=new MemberWSVO();
			    memberVO.setMemberID(rs.getInt("member_id"));
			    memberVO.setMembershipID(rs.getInt("membership_id"));
	            memberVO.setAptID(rs.getInt("apt_id"));
			    memberVO.setFlatNo(rs.getString("building_name")+" - "+rs.getString("apt_name"));
			    memberVO.setTitle(rs.getString("title"));
			    memberVO.setFullName(rs.getString("full_name") );
			    memberVO.setMobile(rs.getString("mobile"));
	            memberVO.setMembershipDate(rs.getString("issue_date"));
	            memberVO.setPhone(rs.getString("phone"));
	            memberVO.setIntercom(rs.getString("intercomm_no"));
	            memberVO.setEmail(rs.getString("email"));
	            memberVO.setIs_Rented(rs.getInt("is_Rented"));
	            memberVO.setAptType(rs.getString("type_of_unit"));
	            memberVO.setAge(rs.getInt("age"));
	            memberVO.setBuildingID(rs.getInt("building_id"));
	            memberVO.setOccupation(rs.getString("occupation"));
	            memberVO.setSocietyID(rs.getInt("society_id"));
	            memberVO.setSocietyName(rs.getString("society_name"));
	            memberVO.setStatusCode(1);
	                      
	        	           
			return memberVO;
		}
	};
	
	memberVo = (MemberWSVO) namedParameterJdbcTemplate.queryForObject(query, namedParameters, RMapper);
	}catch (EmptyResultDataAccessException e) {
		 logger.info("EmptyResultDataAccessException in MemberWSVO getMembersInfoWithMail(String aptID) : "+e);
		    memberVo.setStatusCode(0);
	
	} catch (DataAccessException e) {
	    logger.info("DataAccessException in MemberWSVO getMembersInfoWithMail(String aptID) : "+e);
	    memberVo.setStatusCode(0);
	}
	catch (Exception ex) {
		logger.error("Exception in MemberWSVO getMembersInfoWithMail(String aptID) : "+ex);
		memberVo.setStatusCode(0);
	}
	return memberVo;
}


public MemberVO getMemberInfoThroughLedgerID(int societyID,String ledgerID){
	MemberVO memberVo=new MemberVO();
	
	try {
		logger.debug("Entry : public MemberVO getMemberInfoThroughLedgerID(String ledgerID)");
		SocietyVO societyVO=societyService.getSocietyDetails(societyID);
		String query= "SELECT a.apt_id,apt_name,intercomm_no,societymonthlyCharges,a.is_Rented,a.inv_group,type_of_unit,b.building_name,b.building_id,b.society_id,m.*  "
		             + " FROM building_details b,apartment_details a,member_details m, ledger_user_mapping_"+societyVO.getDataZoneID()+" lu "
			         + " WHERE m.apt_id=a.apt_id AND a.building_id=b.building_id  AND m.member_id=lu.user_id AND lu.ledger_type='M'  and lu.org_id=:societyID "
			         + " AND lu.ledger_id=:ledgerID AND m.type='P' AND is_current!=0 ;";
	logger.debug("query : " + query);
	Map namedParameters = new HashMap();
	namedParameters.put("ledgerID", ledgerID);
	namedParameters.put("societyID", societyID);
	
	RowMapper RMapper = new RowMapper() {
		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			MemberVO memberVO=new MemberVO();
			    memberVO.setMemberID(rs.getInt("member_id"));
			    memberVO.setMembershipID(rs.getInt("membership_id"));
	            memberVO.setOldEmailID(rs.getString("email"));
			    memberVO.setAptID(rs.getInt("apt_id"));
			    memberVO.setFlatNo(rs.getString("building_name")+" "+rs.getString("apt_name"));
			    memberVO.setTitle(rs.getString("title"));
			    memberVO.setFullName(rs.getString("full_name") );
			    memberVO.setMobile(rs.getString("mobile"));
	            memberVO.setMembershipDate(rs.getString("issue_date"));
	            memberVO.setPhone(rs.getString("phone"));
	            memberVO.setIntercom(rs.getString("intercomm_no"));
	            memberVO.setEmail(rs.getString("email"));
	            memberVO.setIsRented(rs.getInt("is_Rented"));
	            memberVO.setMmcDetails(rs.getBigDecimal("societymonthlyCharges"));
	            memberVO.setAptType(rs.getString("type_of_unit"));
	            memberVO.setAge(rs.getInt("age"));
	            memberVO.setBuildingID(rs.getInt("building_id"));
	            memberVO.setOccupation(rs.getString("occupation"));
	            memberVO.setSocietyID(rs.getInt("society_id"));
	            memberVO.setInvoiceGroups(rs.getString("inv_group"));
	            
	                      
	        	           
			return memberVO;
		}
	};
	
	memberVo = (MemberVO) namedParameterJdbcTemplate.queryForObject(query, namedParameters, RMapper);
		
	} catch (DataAccessException e) {
	    logger.info("DataAccessException in getMemberInfoThroughLedgerID(String ledgerID) for ledgerID "+ledgerID+" and "+societyID+" : "+e);
	}
	catch (Exception ex) {
		logger.error("Exception in getMemberInfoThroughLedgerID(String ledgerID) : "+ex);
	}
	return memberVo;
}

public List categoryWiseMemberTxDetails(int year, String societyId, String categoryID,int memberId) {
	List txCategorywise=null;
	try {
		logger.debug("Entry:public List categoryWiseMemberTxDetails(int year, String societyId, String categoryID,int memberId)");
		String firstDate = year + "-" + 04 + "-" + 01;
		String lastDate = (year + 1) + "-" + 03 + "-" + 31;
		Map hashMap = new HashMap();
		hashMap.put("firstDate", firstDate);
		hashMap.put("lastDate", lastDate);
		hashMap.put("categoryID", categoryID);
		hashMap.put("memberId", memberId);
		
		String sql = "SELECT apartment_details.apt_id,tx_invoice_number,tx_from_date,tx_to_date,category_name,soc_tx_"+ societyId +".acc_head_id, category_head_name,tx_type,tx_ammount,tx_date " 
		            +" FROM soc_tx_"+ societyId +",account_head,account_category,member_details,apartment_details,building_details "
		            +" WHERE member_details.apt_id = apartment_details.apt_id "
		            +" AND  member_details.member_id=:memberId "
		            +" AND  member_details.member_id=soc_tx_"+ societyId +".table_primary_id "
		            +" AND  building_details.building_id=apartment_details.building_id "
		            +" AND  account_category.category_id=account_head.category_id AND account_head.acc_head_id=soc_tx_"+ societyId +".acc_head_id "
		            +" AND (tx_date BETWEEN :firstDate AND :lastDate) AND is_deleted=0 AND member_details.type='P' and is_current!=0 "
		            +" AND  soc_tx_"+ societyId +".acc_head_id="+categoryID+"; ";
		RowMapper rMapper=new RowMapper()
		{

			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				// TODO Auto-generated method stub
				RptMonthYearVO rptVO=new RptMonthYearVO();
				rptVO.setTxDate(rs.getString("tx_date"));
				rptVO.setInvoice(rs.getString("tx_invoice_number"));
				rptVO.setFromDate(rs.getString("tx_from_date"));
				rptVO.setUptoDate(rs.getString("tx_to_date"));			
				rptVO.setTxType(rs.getString("tx_type"));
				rptVO.setTxAmount(rs.getBigDecimal("tx_ammount"));
				rptVO.setC_head_name(rs.getString("category_head_name"));
				rptVO.setCategoryName(rs.getString("category_name"));
				rptVO.setCategoryID(rs.getString("acc_head_id"));
				
				return rptVO;
			}};
			txCategorywise = namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
	} catch (Exception ex) {
		logger.error("Exception in categoryWiseMemberTxDetails : " + ex);
	}
	logger.debug("Exit:public List categoryWiseMemberTxDetails(int year, String societyId, String categoryID,int memberId)"+txCategorywise.size());
	return txCategorywise;
}


	
 
	public int addMemberDocument(RptDocumentVO memberVO)
	{	
		int insertFlag=0;
		try
		{
			logger.debug("Entry : public int addMemberDocument(RptDocumentVO memberVO))");
			Date currentDatetime = new Date(System.currentTimeMillis());   
			java.sql.Date sqlDate = new java.sql.Date(currentDatetime.getTime());   
			java.sql.Timestamp timestamp = new java.sql.Timestamp(currentDatetime.getTime());
						
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy h:mm:ss a");
			String formattedDate = sdf.format(currentDatetime);
					
			String sql="insert into documents_relation (entity_id,doc_id,status_id,status_description)" +
					" values(:entityID,:documentID,:statusID,:description)";
			logger.debug("query : " + sql);
			
			Map hashMap=new HashMap();
			hashMap.put("entityID", memberVO.getMemberID());
			hashMap.put("documentID", memberVO.getDocumentID());
			hashMap.put("statusID", memberVO.getDocumentStatusID());	
			hashMap.put("description", formattedDate+"- Updated By:-"+memberVO.getUpdaterName()+" Status: "+memberVO.getDocumentStatusName()+"\n"+memberVO.getDescription()+"\n---------------------\n");;
			
			insertFlag=namedParameterJdbcTemplate.update(sql,hashMap);
				
		}catch(Exception Ex)
		{
			//Ex.printStackTrace();
			logger.error("Exception in addMemberDocument : "+Ex);
		
		}
		logger.debug("Exit : public int addMemberDocument(MemberVO memberVO)");
	return insertFlag;	
	}
	
	public int updateMemberDocument(RptDocumentVO memberVO)
	{	
		int insertFlag=0;
		try
		{
			logger.debug("Entry : public int updateMemberDocument(RptDocumentVO memberVO))");
			Date currentDatetime = new Date(System.currentTimeMillis());   
			java.sql.Date sqlDate = new java.sql.Date(currentDatetime.getTime());   
			java.sql.Timestamp timestamp = new java.sql.Timestamp(currentDatetime.getTime());
						
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy h:mm:ss a");
			String formattedDate = sdf.format(currentDatetime);
					
			
			String sql="Update documents_relation SET entity_id=:entityID,doc_id=:documentID,status_id=:statusID,status_description=:description Where id=:docMapID ";
					
			logger.debug("query : " + sql);
			
			Map hashMap=new HashMap();
			hashMap.put("entityID", memberVO.getMemberID());
			hashMap.put("documentID", memberVO.getDocumentID());
			hashMap.put("statusID", memberVO.getDocumentStatusID());	
			hashMap.put("description", formattedDate+"- Updated By:-"+memberVO.getUpdaterName()+" Status: "+memberVO.getDocumentStatusName()+"\n"+memberVO.getDescription()+"\n---------------------\n"+memberVO.getDescriptionHistory());;
			hashMap.put("docMapID", memberVO.getDocumentMapID());
			
			insertFlag=namedParameterJdbcTemplate.update(sql,hashMap);
				
		}catch(Exception Ex)
		{
			//Ex.printStackTrace();
			logger.error("Exception in updateMemberDocument : "+Ex);
		
		}
		logger.debug("Exit : public int updateMemberDocument(RptDocumentVO memberVO)");
	return insertFlag;	
	}

		
	public int deleteDocuments(RptDocumentVO memberVO)
	{	
		int insertFlag=0;
		try
		{
			logger.debug("Entry : public int deleteDocuments(RptDocumentVO memberVO)");
			
			String sql=" DELETE FROM `documents_relation` WHERE entity_id=:entityID " +
					   " and doc_id=:documentID ;";					   
				
			logger.debug("query : " + sql);
			
			Map hashMap=new HashMap();
			hashMap.put("entityID", memberVO.getMemberID());
			hashMap.put("documentID", memberVO.getDocumentID());		
			
			insertFlag=namedParameterJdbcTemplate.update(sql,hashMap);
				
		}
		catch(Exception Ex)
		{
			Ex.printStackTrace();
			logger.error("Exception in deleteDocuments : "+Ex);
		
		}
		logger.debug("Exit : public int deleteDocuments(RptDocumentVO memberVO)"+insertFlag);
	return insertFlag;	
	}
	
	
	public List getMemberDocumentList(int memberID){

		List document = null;	
		String strSQL = "";
		
		try{	
			logger.debug("Entry : public List getMemberDocumentList(int memberID)" );
		//1. SQL Query
		strSQL = " SELECT entity_id, r.id , d.doc_id ,d.document_name,type,IFNULL(r.id, 0) AS docMapId, IFNULL(r.status_id, 0) AS statusId,IFNULL(s.name, 'Not Available') AS statusName FROM document_details d "
				+" LEFT JOIN documents_relation r ON r.entity_id="+memberID+" AND d.doc_id =r.doc_id  "
				+" LEFT JOIN status s ON s.id=r.status_id WHERE d.type='M' ORDER BY d.TYPE,d.doc_id ";
		logger.debug("query : " + strSQL);
		// 2. Parameters.
		 SqlParameterSource namedParameters = new MapSqlParameterSource("entity_id", memberID);
	   
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		           RptDocumentVO docMember = new RptDocumentVO();
		           docMember.setDocumentMapID(rs.getInt("docMapId"));
		           docMember.setDocumentID(rs.getInt("doc_id"));
		           docMember.setDocumentName(rs.getString("document_name"));
		           docMember.setType(rs.getString("type"));
		           docMember.setDocumentStatusID(rs.getInt("statusId"));
		           docMember.setDocumentStatusName(rs.getString("statusName"));
		       
		            return docMember;
		        }};

		        document = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
		        

		        logger.debug("outside query : " + document.size() );
				logger.debug("Exit :public List getMemberDocumentList(int memberID)" );
				
		}catch (NullPointerException ex){
			logger.error("Exception in getMemberDocumentList : "+ex);
			
		}
		
		catch (Exception ex){
			logger.error("Exception in getMemberDocumentList : "+ex);
			
		}
		return document;
		
		
	}
	
	
	public List getMemberMissingDocument(int memberID,int documentID){

		List documentList = null;	
		String strSQL = "";
		
		try{	
			logger.debug("Entry : public List getMemberMissingDocument(int memberID)" );
		//1. SQL Query
		strSQL = " SELECT entity_id, r.id , d.doc_id ,d.document_name,TYPE, IFNULL(r.status_id, 0) AS statusId,IFNULL(s.name, 'Not Available') AS statusName FROM document_details d "+
							" LEFT JOIN documents_relation r ON r.entity_id=:memberID AND d.doc_id =r.doc_id "+  
							" LEFT JOIN `status` s ON s.id=r.status_id WHERE d.type='M' AND d.doc_id=:documentID AND (r.status_id!=0 OR r.status_id!=3) ORDER BY d.TYPE,d.doc_id ;";
		logger.debug("query : " + strSQL);
		// 2. Parameters.
		Map hMap=new HashMap<>();
		hMap.put("memberID",memberID);
		hMap.put("documentID",documentID);
	   
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		           MemberVO docMember = new MemberVO();
		           docMember.setMapId(rs.getString("id"));
		           docMember.setDocCategoryId(rs.getString("doc_id"));
		           docMember.setDocName(rs.getString("document_name"));
		           docMember.setDocType(rs.getString("type"));
		           docMember.setDocumentStatusId(rs.getInt("statusId"));
		           docMember.setDocumentStatusName(rs.getString("statusName"));
		          /* docMember.setDocCategoryValue(rs.getString("catValue"));
		           if(docMember.getDocCategoryValue().equalsIgnoreCase("1")){
		        	   docMember.setIsDoc(true);
		           }else
		        	   docMember.setIsDoc(false);*/
		            return docMember;
		        }};

		        documentList = namedParameterJdbcTemplate.query(strSQL,hMap,RMapper);
		        

		        logger.debug("outside query : " + documentList.size() );
				logger.debug("Exit :public List getMemberMissingDocument(int memberID)" );
				
		}catch (NullPointerException ex){
			logger.error("Exception in getMemberMissingDocument : "+ex);
			
		}
		
		catch (Exception ex){
			logger.error("Exception in getMemberMissingDocument : "+ex);
			
		}
		return documentList;
		
		
	}
	
	
	public List getDocumentMissingList(int buildingID,int societyId,int documentID) {

		List memberList=null;
		
		String strWhereClause="";
		
		if(buildingID==0)
			strWhereClause=" ";
			else
				strWhereClause="  AND m.building_id="+buildingID+"";

		try {
			logger.debug("Entry : public List getDocumentMissingList(int buildingID,int societyId,int documentID)"+societyId);
	
		//jdbcTemplate.execute(createNomineeAddressView);
		
		String strSQL = "SELECT m.unit_name,m.full_name,m.member_id,m.email,m.mobile,m.title,m.apt_Id,r.entity_id, r.id, d.doc_id ,d.document_name,d.type,IFNULL(r.id, 0) AS docMapID, IFNULL(r.status_id, 0) AS statusId,IFNULL(s.name, 'Not Available') AS statusName,r.status_description ,ss.* "+ 
                                      "   FROM (document_details d,members_info m,society_details ss)  LEFT JOIN documents_relation r ON r.entity_id=m.member_id AND d.doc_id =r.doc_id  "+
                                      "   LEFT JOIN `status` s ON s.id=r.status_id WHERE d.type='M' AND m.is_current!=0 AND m.type='P' AND d.doc_id=:docID AND (r.status_id IS NULL OR r.status_id=0 OR r.status_id=3)  AND m.society_id=:societyID   AND m.society_id=ss.society_id  "+strWhereClause+" "; 
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyID", societyId);
			namedParameters.put("docID", documentID);
			namedParameters.put("buildingID", buildingID);
			
			//3. RowMapper.
			
			RowMapper RMapper = new RowMapper() {
				String tempAptName = "";
				String tempMemberName="";
				int i=0;
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					
					  MemberVO membrs = new MemberVO();
			            membrs.setMemberID(rs.getInt("member_id"));
			            membrs.setOldEmailID(rs.getString("email"));
			            membrs.setAptID(rs.getInt("apt_id"));
			            membrs.setFlatNo(rs.getString("unit_name"));
			            membrs.setTitle(rs.getString("title"));
			            membrs.setFullName(rs.getString("full_name"));
			            
			            membrs.setMobile(rs.getString("mobile"));
			            membrs.setEmail(rs.getString("email"));
			             membrs.setSocietyID(rs.getInt("society_id"));
			            membrs.setComitteeGroupEmail(rs.getString("mailingList_committee"));
			            membrs.setSocietyName(rs.getString("society_name"));
			            membrs.setSocietyShortName(rs.getString("short_name"));
			            return membrs;
				}
			};
			memberList=  namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);

			
			logger.debug("Exit:	public List getDocumentMissingList(int buildingID,int societyId,int documentID)");
		} catch (NullPointerException ex) {
			logger.info("No document is missing for societyID : "+societyId+" Document ID: "+documentID);

			
		} catch (Exception ex) {
			logger.error("Exception in public List getDocumentMissingList(int buildingID,int societyId,int documentID) : "+ex);

		}
		return memberList;

	}
	
	 public DropDownVO checkTransferMember(String txID,String memberID,String societyID) {

	 		DropDownVO dropdownVO=new DropDownVO();
	 		try {
	 			logger.debug("Entry : public int checkTransferMember(String txID,String memberID)");
	 			
	 			String sql="SELECT count(tx_id) as count,tx_date FROM soc_tx_"+societyID+" WHERE tx_id=:txID AND acc_head_id=36 AND table_primary_id=:memberID;";
				
	 			logger.debug("query is"+sql+memberID+societyID);
	 			Map hashMap=new HashMap();
	 			hashMap.put("txID", txID);
	 			hashMap.put("memberID", memberID);
	 			RowMapper RMapper = new RowMapper() {
		    		public Object mapRow(ResultSet rs, int rowNum)
		    				throws SQLException {
		    			DropDownVO dropDownVO=new DropDownVO();
		    			   
		    	            dropDownVO.setData(rs.getString("count"));
		    	            dropDownVO.setLabel(rs.getString("tx_date"));
		    	                      
		    	        	           
		    			return dropDownVO;
		    		}
		    	};
			
	 			
	 			dropdownVO = (DropDownVO) namedParameterJdbcTemplate.queryForObject(sql, hashMap, RMapper);

	 			logger.debug("Exit : public int checkTransferMember(String txID,String memberID)");
	 		}catch (EmptyResultDataAccessException Ex) {
	 	 			logger.error("Exception in public int checkTransferMember(String txID,String memberID): "+Ex);
	 	 	
	 		}catch (Exception Ex) {
	 			logger.error("Exception in public int checkTransferMember(String txID,String memberID): "+Ex);
	 		}
	 		return dropdownVO;
	 	}
	    
	 public List getOldMembers(int apartmentID){
	    	List memberList=null;
	    	
	    	try {
	    		logger.debug("Entry : public List getOldMembers(String apartmentID)");
	    		
	    		String query= "SELECT * "
	    		             + "FROM member_details "
	    			         + "WHERE  apt_id=:aptID ;";
	    	logger.debug("query : " + query);
	    	Map namedParameters = new HashMap();
	    	namedParameters.put("aptID", apartmentID);
	    	
	    	
	    	RowMapper RMapper = new RowMapper() {
	    		public Object mapRow(ResultSet rs, int rowNum)
	    				throws SQLException {
	    			InsertMemberVO memberVO=new InsertMemberVO();
	    			    memberVO.setMemberId(rs.getString("member_id"));
	    			    memberVO.setTitle(rs.getString("title"));
	    			    memberVO.setMembershipID(rs.getString("membership_id"));
			            memberVO.setOldEmail(rs.getString("email"));
	    			    memberVO.setAptId(rs.getInt("apt_id"));
	    			    memberVO.setFullName(rs.getString("full_name") );
	    			    memberVO.setMobile(rs.getString("mobile"));
	    	            memberVO.setPhone(rs.getString("phone"));
	    	            memberVO.setPhone(rs.getString("phone"));
	    	            memberVO.setEmail(rs.getString("email"));
	    	            memberVO.setType(rs.getString("type"));
	    	            memberVO.setIssueDate(rs.getString("issue_date"));
	    	            
	    	           
	    	            
	    	                      
	    	        	           
	    			return memberVO;
	    		}
	    	};
	    	
	    	memberList =  namedParameterJdbcTemplate.query(query, namedParameters, RMapper);
	    		
	    	logger.debug("Exit : public List getOldMembers(String apartmentID)"+memberList.size());
	    	} catch (DataAccessException e) {
	    	    logger.error("DataAccessException in public List getOldMembers(String apartmentID) : "+e);
	    	}
	    	catch (Exception ex) {
	    		logger.error("Exception in public List getOldMembers(String apartmentID) : "+ex);
	    	}
	    	return memberList;
	    }

	 
	 
	 
	 
	 public int insertOldMembersToHistory( final List<InsertMemberVO>listOfMembers,final int apartment_id,final InsertMemberVO memberVO){

	    	int flag=0;
	    	
	    	logger.debug("Entry : public int insertOldMembersToHistory( final List<InsertMemberVO>listOfMembers,final String apartment_id)");

	    	try {
	    		
	    		String str="INSERT INTO  (member_id,membership_id,apt_id,title,full_name,email,mobile,phone,type,date_of_transfer,transferee_name,org_id,transfer_type,notes)" +
	    				" VALUES (?,?,?,?,?,?,?,?,? ,?, ?, ?, ?, ?) ;";
	    		logger.debug("Query: "+str);	
	    		getJdbcTemplate().batchUpdate(str, new BatchPreparedStatementSetter() {
	    			 
	    			//@Override
	    			public void setValues(PreparedStatement ps, int i) throws SQLException {
	    				InsertMemberVO insertMemberVO = listOfMembers.get(i);
	    				
	    				ps.setString(1, insertMemberVO.getMemberId());
	    				ps.setString(2, insertMemberVO.getMembershipID());
	    				ps.setInt(3, insertMemberVO.getAptId());
	    				ps.setString(4, insertMemberVO.getTitle());
	    				ps.setString(5, insertMemberVO.getFullName());
	    				ps.setString(6, insertMemberVO.getEmail());
	    				ps.setString(7, insertMemberVO.getMobile());
	    				ps.setString(8, insertMemberVO.getPhone());
	    				ps.setString(9, insertMemberVO.getType());
	    				ps.setString(10, memberVO.getTransferDate());
	    				ps.setString(11, memberVO.getFullName());	    			     
	    				ps.setInt(12,Integer.parseInt(memberVO.getSocietyID()));
	    				ps.setString(13, memberVO.getTransferReason());
	    				ps.setString(14, memberVO.getTransferReason());
	    					
	    			}
	    			//@Override
	    			public int getBatchSize() {
	    				return listOfMembers.size();
	    			}
	    			
	    			
	    		});
	    		flag=listOfMembers.size();
	    	}catch (DataAccessException e) {
	    		flag=0;
	    		logger.error("DataACCESSException in insertOldMembersToHistory : "+e);
	    	
	    	}catch (Exception ex) {
	    		flag =0;
	    		logger.error("Exception in insertOldMembersToHistory : "+ex);
	    	}
	    	
	    	logger.debug("Exit : public int insertOldMembersToHistory( final List<InsertMemberVO>listOfMembers,final String apartment_id)"+listOfMembers.size());
	    	
	    	return flag;
	    	
	    	
	    }
	
	 public int delMember(int apartmentID) {

	 		int sucess = 0;
	 		try {
	 			logger.debug("Entry : public int delMember(InsertMemberVO member,int apartmentID)");

	 			String sqlQuery="Delete from  member_details  where apt_id=:aptID";
	 			
	 			Map hashMap=new HashMap();
	 			hashMap.put("aptID", apartmentID);
	 			
	 			sucess=namedParameterJdbcTemplate.update(sqlQuery, hashMap);
	 			
	 			
	 			logger.debug("Exit : public int delMember(InsertMemberVO member,int apartmentID)");

	 		}catch (Exception Ex) {
	 			logger.error("Exception in public int delMember(InsertMemberVO member,int apartmentID): "+Ex);
	 		}
	 		return sucess;
	 	}
	 
	 
	 
	 public int insertNewMember(InsertMemberVO insertMemberVO,int apartmentID)
	    {	
	    	int insertFlag=0;
	    	int flag=0;
	    	int genratedId=0;
	    	String sql=null;
	    	try
	    	{logger.debug("Entry : public int insertNewMember(InsertMemberVO insertMemberVO,int apartmentID)"+insertMemberVO.getAptId()+" "+insertMemberVO.getMembershipID());
	    	    
	    	
	    	if(insertMemberVO.getType().equalsIgnoreCase("P")){
    	    	sql="INSERT INTO member_details (membership_id,apt_id,title,email,mobile,phone,full_name,type,issue_date,age,occupation,dob,tags) "+
    				" SELECT :membershipID,:aptId,:title,:email,:mobile,:phone,:fullName,:type,:membershipDate, :age, :occupation,:dob,:tags FROM member_details " +
    				" WHERE NOT EXISTS (SELECT * FROM member_details WHERE apt_id =:aptId  AND TYPE=:type) LIMIT 1; ";
    	    }else
    	    	sql="INSERT INTO member_details (membership_id,apt_id,title,email,mobile,phone,full_name,type,issue_date,age, occupation, dob,tags) "+
				" values (:membershipID,:aptId,:title,:email,:mobile,:phone,:fullName,:type, :membershipDate,:age ,:occupation ,:dob,:tags) ;";
    		
    	    
    	    SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(insertMemberVO);
			   KeyHolder keyHolder = new GeneratedKeyHolder();
			   getNamedParameterJdbcTemplate().update(sql, fileParameters, keyHolder);
			   genratedId=keyHolder.getKey().intValue();

	    	}catch(DuplicateKeyException duplicateEntry){
	    		duplicateEntry.printStackTrace();
	    		logger.error("DuplicateKeyException in insertNewMember : "+duplicateEntry);
	    		genratedId = 2;
	    		return genratedId;	
	       		
	    	}
	    	catch(Exception Ex){
	    		logger.error("Exception in insertNewMember : "+Ex);
	    	}
	    	logger.debug("Exit : public int insertNewMember(InsertMemberVO insertMemberVO,int apartmentID)"+insertFlag);
	    return genratedId;	
	    }
	 
	
	 
	 public List getMembershipNoList(String societyID){
	    	List memberList=null;
	    	
	    	try {
	    		logger.debug("Entry : public List getMembershipNoList(String soceityID)");
	    		
	    		String query= "SELECT membership_id FROM apartment_details a,member_details m,building_details b WHERE a.apt_id=m.apt_id AND b.building_id=a.building_id AND b.society_id=:societyID "+
	    					  "UNION "+
	    					  "SELECT membership_id FROM apartment_details a, m,building_details b WHERE a.apt_id=m.apt_id AND b.building_id=a.building_id AND b.society_id=:societyID "+
	    					  "ORDER BY membership_id;";
	    	logger.debug("query : " + query);
	    	Map namedParameters = new HashMap();
	    	namedParameters.put("societyID", societyID);
	    	
	    	
	    	RowMapper RMapper = new RowMapper() {
	    		public Object mapRow(ResultSet rs, int rowNum)
	    				throws SQLException {
	    			MemberVO memberVO=new MemberVO();
	    			    memberVO.setMembershipID(rs.getInt("membership_id"));
	    	      	return memberVO;
	    		}
	    	};
	    	
	    	memberList =  namedParameterJdbcTemplate.query(query, namedParameters, RMapper);
	    		
	    	logger.debug("Exit : public List getMembershipNoList(String soceityID)"+memberList.size());
	    	} catch (DataAccessException e) {
	    	    logger.error("DataAccessException in public List getMembershipNoList(String soceityID) : "+e);
	    	}
	    	catch (Exception ex) {
	    		logger.error("Exception in public List getMembershipNoList(String soceityID) : "+ex);
	    	}
	    	return memberList;
	    }
	 
	 public int incrementMembershipNumber(int societyiD){
			int invoiceNo=0;
			try
			{
				logger.debug("Entry :public int incrementMembershipNumber(String societyiD)");
				
				String sqlQry=" Update society_settings set membership_counter=membership_counter+1 where society_id=:societyID" ;
						
				Map hMap=new HashMap();
				hMap.put("societyID", societyiD);
				
				
				
					invoiceNo=namedParameterJdbcTemplate.update(sqlQry, hMap);
				
			}catch(Exception ex)
			{
				ex.printStackTrace();
				logger.error("Exception in incrementMembershipNumber : "+ex);
				
			}
				logger.debug("Exit : public int incrementMembershipNumber(String societyiD)");
			return invoiceNo;
			}
	 
	 public int getIncrementMembershipNumber(int societyiD){
			int membershipNo=0;
			try
			{
				logger.debug("Entry :public int getIncrementMembershipNumber(String societyiD)");
				
				String sqlQry=" select membership_counter from society_settings where society_id=:societyID" ;
						
				Map hMap=new HashMap();
				hMap.put("societyID", societyiD);
			
				membershipNo=namedParameterJdbcTemplate.queryForObject(sqlQry, hMap, Integer.class);
					
			
				
			}catch(Exception ex)
			{
				ex.printStackTrace();
				logger.error("Exception in getIncrementMembershipNumber : "+ex);
				
			}
				logger.debug("Exit : public int getIncrementMembershipNumber(String societyiD)");
			return membershipNo;
			}

	 
//	webservice check renter present or not
	 public int checkRenterDetails(int aptId){			
			int sucess = 0;
			int flag=0;
			String strSQL = "";
			try {
					logger.debug("Entry : public int checkRenterDetails(String aptId)");
					strSQL = "Update renter_details SET is_deleted='1',is_current_renter='0' WHERE apt_id="+aptId+";" ;
					logger.debug("query : " + strSQL);
					// 2. Parameters.
					Map namedParameters = new HashMap();
							
					sucess = namedParameterJdbcTemplate.update(strSQL,namedParameters);
					
					if(sucess==1){
						
						logger.debug("Delete old Tenant for adding new tenant");
						
					}else{
                      //update renter flag					
						String query = "UPDATE apartment_details SET is_Rented='1' WHERE apt_id="+aptId+" " ;
						logger.debug("query : " + query);
						// 2. Parameters.
						Map namedParameter = new HashMap();
										
						flag = namedParameterJdbcTemplate.update(query,namedParameter);
						
						logger.debug("Update Tenant flag for adding new tenant");
						
					}
											
					
					if(flag != 0 ){
						flag=1;
					}
					
					logger.debug("Exit : public int checkRenterDetails(String aptId)"+flag);

			}catch(Exception Ex){
				
				Ex.printStackTrace();
				logger.error("Exception in checkRenterDetails : "+Ex);
				
			}
			return flag;
		}	
	 
	 public List getExpiredtenantDetails(int societyID,String uptoDate,Boolean aboutToExpire) {
			List tenantList=new ArrayList();
			DateUtility dateUtil=new DateUtility();
			try {
				logger
						.debug("Entry : public List getExpiredtenantDetails(int intSocietyID) ");
				String strCondition="";
				if(aboutToExpire){
					strCondition=" AND r.agg_end_date>=' "+dateUtil.findCurrentDate()+"' ";
				}

			

				String  sql = "SELECT member_id,r.title as renter_title,m.title as member_title,unit_name,m.apt_id,m.full_name as member_name,m.email AS member_email,m.mobile AS member_mobile,r.renter_full_name,r.email AS renter_email,r.mobile AS renter_mobile, renter_id, agg_end_date "
						+ "FROM members_info m,renter_details r,apartment_details a "
						+ "WHERE a.is_Rented=1 AND m.apt_id=r.apt_id AND m.apt_id=a.apt_id AND r.agg_end_date<=:uptoDate "+strCondition
						+" AND r.is_current_renter=1 AND m.society_id=:societyID AND m.type='P' ORDER BY seq_no ;";
				logger.debug(uptoDate+"Query "+sql);
				Map hmap = new HashMap();
				hmap.put("societyID",societyID);
				hmap.put("uptoDate",uptoDate);

				  RowMapper  rMapper = new RowMapper() {

					public Object mapRow(ResultSet rs, int arg1)
							throws SQLException {
						RptTenantVO rptTenantVO = new RptTenantVO();
						rptTenantVO.setTitle(rs.getString("renter_title"));
						rptTenantVO.setAptID(rs.getInt("apt_id"));
						rptTenantVO.setRenterID(rs.getInt("renter_id"));
						rptTenantVO.setMemberID(rs.getInt("member_id"));
						rptTenantVO.setApt_name(rs.getString("unit_name"));
						rptTenantVO.setMobile(rs.getString("renter_mobile"));
						rptTenantVO.setOwnerMobile(rs.getString("member_mobile"));
						rptTenantVO.setName(rs.getString("renter_full_name"));
						rptTenantVO.setOwnerName(rs.getString("member_title")+" "+rs.getString("member_name"));
						rptTenantVO.setAgg_end_date(rs.getString("agg_end_date"));
						rptTenantVO.setEmail(rs.getString("renter_email"));
						rptTenantVO.setOwnerEmail(rs.getString("member_email"));
					
						return rptTenantVO;
					}

				};
				tenantList = namedParameterJdbcTemplate.query(sql, hmap,
						rMapper);

			} catch (EmptyResultDataAccessException e) {
				logger.info("No tenant details available for this society");
			} catch (IndexOutOfBoundsException ex) {
				logger.info("No data available for this society : " + ex);
			}

			catch (Exception ex) {
				logger.error("Exception in tenantDetailsRpt : " + ex);
			}

			logger.debug("Exit : public List tenantDetailsRpt(int intSocietyID) "
					+ tenantList.size());
			return tenantList;
		}

	 public List getMembersContactList(String societyID, String memberType,String infoMissingType){
	 	List memberList=new ArrayList();
	 	logger.debug("Entry : public List getMembersContactList(String societyID, String memberType)"+societyID+memberType+infoMissingType);
	 	
	    String strSQL = "";
	    String strCondition="";
	    String strContactCondition="";
	    
	    if(memberType.equalsIgnoreCase("P")){
			strCondition=" AND m.type='P' ";
		}else if(memberType.equalsIgnoreCase("A")){
			strCondition=" AND m.type='A' ";			
		}else if(memberType.equalsIgnoreCase("ALL")){
			strCondition="";			
		}
	    
	    //0: missing mobile no, 1: missing email, 2:no missing
	    if(infoMissingType.equalsIgnoreCase("0")){
	    	strContactCondition=" AND ((m.mobile = '0000000000' OR m.mobile IS NULL)) ";
	    }else if(infoMissingType.equalsIgnoreCase("1")){
	    	strContactCondition=" AND ((m.email = '' OR m.email IS NULL))  ";
	    }else if(infoMissingType.equalsIgnoreCase("2")){
	    	strContactCondition="";			
		}
	 	try{	
	 		
	 	strSQL = " SELECT b.building_id,b.building_name,a.apt_id,type_of_unit,a.is_Rented, a.apt_name,m.*,society_id "
	 	       + " FROM apartment_details a,member_details m,building_details b "
               + " WHERE a.apt_id = m.apt_id AND a.building_id=b.building_id AND b.society_id=:societyID "
               + strContactCondition +  strCondition +"  AND m.is_current!=0 AND a.unit_sold_status='Sold' order by seq_no ";
	logger.debug("query : " + strSQL);
	// 2. Parameters.
	 SqlParameterSource namedParameters = new MapSqlParameterSource("societyID", societyID);
  
	//3. RowMapper.
	 RowMapper RMapper = new RowMapper() {
	        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	            MemberVO membrs = new MemberVO();
	            membrs.setBuildingID(rs.getInt("building_id"));
	            membrs.setMemberID(rs.getInt("member_id"));
	            membrs.setMembershipID(rs.getInt("membership_id"));
	            membrs.setAptID(rs.getInt("apt_id"));
	            membrs.setType(rs.getString("type"));
	            membrs.setFlatNo(rs.getString("building_name")+" - "+rs.getString("apt_name"));
	            membrs.setTitle(rs.getString("title"));
	            membrs.setFullName(rs.getString("title")+" "+rs.getString("full_name"));
	            membrs.setOccupation(rs.getString("occupation"));
	            membrs.setMobile(rs.getString("mobile"));
	            membrs.setEmail(rs.getString("email"));
	            membrs.setPhone(rs.getString("phone"));
	            membrs.setMembershipDate(rs.getString("issue_date"));
	            membrs.setDateOfBirth(rs.getString("dob"));
	            membrs.setIsRented(rs.getInt("is_Rented"));
	            membrs.setAptType(rs.getString("type_of_unit"));
	            membrs.setSocietyID(rs.getInt("society_id"));	            
	           
	            return membrs;
	        }};

	        memberList = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
	 	}catch (Exception ex){
	 		logger.error("Exception in getMembersContactList : "+ex);
	 		
	 	}
	 	
	 	logger.debug("Exit : public List getMembersContactList(String societyID, String memberType)");
	 	return memberList;
	 }
	 
	 
	 
	 public int addAgentDetails(RenterVO renterVO){	
		int key=0;			
			try{
				   
				   String SQL = "INSERT INTO agent_details "
				   		+"(agent_name,mobile,email)  " +
				                "VALUES (:agentName, :agentMobile,:agentEmail)";
				   SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(renterVO);
				   KeyHolder keyHolder = new GeneratedKeyHolder();
				   getNamedParameterJdbcTemplate().update(SQL, fileParameters, keyHolder);
				   key=keyHolder.getKey().intValue();
				   				  	   
				   
				   logger.debug("Exit :public int addAgentDetails(RenterVO renterVO)"+key); 
				}catch(Exception se){
					logger.error("Exception in public public int addAgentDetails(RenterVO renterVO)"+se);
				   
				}
			return key;
		
		}

	 //============ Get Member list of groups like, member/ committee/ tenant etc =========// 
	 public List getMembersListFromGroup(int societyID, String groupName){
		 	List memberList=new ArrayList();
		 	logger.debug("Entry : public List getMembersListFromGroup(int societyID, String groupName)"+societyID+groupName);
		 	
		    String groupCondition = "";
		    String strSQL;
		    if(groupName.equalsIgnoreCase("primary")){		    	
		    	groupCondition = " AND m.type='P' ";
		    }else if(groupName.equalsIgnoreCase("associate")){
		    	groupCondition = " AND m.type='A' ";
	        }else if(groupName.equalsIgnoreCase("all")){
	        	groupCondition = " ";
	        }else{
	        	groupCondition = " AND m.tags LIKE '%"+groupName+"%' ";
	        }
		    
		 	try{	
		 		
		 	strSQL = "SELECT mi.*,m.tags FROM members_info mi,member_details m WHERE mi.member_id=m.member_id AND mi.society_id=:societyID "+groupCondition+"";;
		logger.debug("query : " + strSQL);
		// 2. Parameters.
		 SqlParameterSource namedParameters = new MapSqlParameterSource("societyID", societyID);
	  
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            MemberVO membrs = new MemberVO();
		            membrs.setBuildingID(rs.getInt("building_id"));
		            membrs.setMemberID(rs.getInt("member_id"));
		            membrs.setMembershipID(rs.getInt("membership_id"));
		            membrs.setAptID(rs.getInt("apt_id"));
		            membrs.setType(rs.getString("type"));
		            membrs.setFlatNo(rs.getString("unit_name"));
		            membrs.setTitle(rs.getString("title"));
		            membrs.setFullName(rs.getString("title")+" "+rs.getString("full_name"));
		            membrs.setOccupation(rs.getString("occupation"));
		            membrs.setMobile(rs.getString("mobile"));
		            membrs.setEmail(rs.getString("email"));
		            membrs.setPhone(rs.getString("phone"));
		            membrs.setMembershipDate(rs.getString("issue_date"));
		            membrs.setDateOfBirth(rs.getString("dob"));
		            membrs.setIsRented(rs.getInt("is_Rented"));
		            membrs.setSocietyID(rs.getInt("society_id"));	            
		           
		            return membrs;
		        }};

		        memberList = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
		 	}catch (Exception ex){
		 		logger.error("Exception in public List getMembersListFromGroup(int societyID, String groupName) : "+ex);
		 		
		 	}
		 	
		 	logger.debug("Exit : public List getMembersListFromGroup(int societyID, String groupName)");
		 	return memberList;
		 }
	 
	 
	 public List getMembershipHistory(int aptID,int orgID){
			List memberList=new ArrayList();
			logger.debug("Entry : public List getMembershipHistory(int aptID)"+aptID);
			
		String strSQL = "";
			
			try{	
				
			//1. SQL Query
			strSQL = " SELECT membr.membership_id,membr.full_name,membr.type,membr.issue_date, membr.date_of_transfer FROM member_history membr ,apartment_details apt, building_details bldg  WHERE membr.apt_id = apt.apt_id AND apt.building_id = bldg.building_id AND bldg.society_id = :orgID AND apt.apt_id = :aptID;";
			logger.debug("query : " + strSQL);
			// 2. Parameters.
			Map hmap = new HashMap();
			hmap.put("orgID",orgID);
			hmap.put("aptID",aptID);
		   
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			            MemberVO memberVO = new MemberVO();
			            
			            
			            memberVO.setType(rs.getString("type"));
			            memberVO.setFullName(rs.getString("full_name"));
			            memberVO.setMembershipDate(rs.getString("issue_date"));
			            memberVO.setShareTransferDate(rs.getString("date_of_transfer"));
			            memberVO.setMembershipID(rs.getInt("membership_id"));
			           
			            return memberVO;
			        }};

			        memberList = namedParameterJdbcTemplate.query(strSQL,hmap,RMapper);
			        

			        logger.debug("outside query : " + memberList.size() );
			        logger.debug("Exit : public List getMembershipHistory(int aptID)");
			}catch (Exception ex){
				logger.error("Exception in public List getMembershipHistory(int aptID) : "+ex);
				
			}
			
			logger.debug("Exit : public List getMembershipHistory(int aptID)");
			return memberList;
		}

	 
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

   
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}



	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	/**
	 * @param societyService the societyService to set
	 */
	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}

}
