package com.emanager.server.society.dataAccessObject;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.emanager.server.accounts.DataAccessObjects.ChargesVO;
import com.emanager.server.accounts.DataAccessObjects.PenaltyChargesVO;
import com.emanager.server.adminReports.valueObject.NominationRegisterVO;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.commonUtils.valueObject.DropDownVO;
import com.emanager.server.society.valueObject.ApartmentVO;
import com.emanager.server.society.valueObject.BankInfoVO;
import com.emanager.server.society.valueObject.BookingVO;
import com.emanager.server.society.valueObject.FinancialYearVO;
import com.emanager.server.society.valueObject.InsertMemberVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;




public class SocietyDAO {

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	private JdbcTemplate jdbcTemplate;
	private static final Logger logger = Logger.getLogger(SocietyDAO.class);
	ConfigManager conf=new ConfigManager();
	
	public int insertSociety(SocietyVO societyVO)
	{	
		int generatedOrgID=0;
		try{
		
		logger.debug("Entry : public int insertSociety(SocietyVO societyVO)");
		
			String sql="INSERT INTO society_details(society_name,register_no,mailingList_member,mailingList_committee,URI,org_type)VALUES" +
					   "(:societyName, :societyRegNo, :mailListM, :mailListA, :uri,:orgType); ";
			logger.debug("query : " + sql);
			
				
			   SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(societyVO);
			   KeyHolder keyHolder = new GeneratedKeyHolder();
			   getNamedParameterJdbcTemplate().update(sql, fileParameters, keyHolder);
			   generatedOrgID=keyHolder.getKey().intValue();
			   
			   if(generatedOrgID>0){
				   societyVO.setSocietyID(generatedOrgID);
				   int successFlage=insertOrgSettings(societyVO);
				   
			   }
			
		
		
          			
		}catch(DuplicateKeyException DEx){
			
			logger.info("Org already present with name :  "+societyVO.getSocietyName()+"  "+DEx);
			generatedOrgID=2;
		
	}catch(Exception Ex){
			
			logger.error("Exception in insertSociety : for "+societyVO.getSocietyName()+" "+Ex);
		
		}
		logger.debug("Exit : public int insertSociety(SocietyVO societyVO)"+generatedOrgID);
	return generatedOrgID;	
	}
	
	  public int insertOrgSettings(SocietyVO societyVO){

	    	int flag=0;
	    	ConfigManager conf=new ConfigManager();
	    	if(societyVO.getOrgType().equalsIgnoreCase("S")){
	    	societyVO.setTemplate(conf.getPropertiesValue("template.society"));
	    	}else if(societyVO.getOrgType().equalsIgnoreCase("C")){
	    	societyVO.setTemplate(conf.getPropertiesValue("template.organization"));
	    	}
	    	
	    	if(societyVO.getIsEnterprise()==1){
	    		societyVO.setDataZoneID(""+societyVO.getSocietyID());
	    	}else if(societyVO.getIsEnterprise()==0){
	    		societyVO.setDataZoneID(conf.getPropertiesValue("dataZoneID.default"));
	    	}
	    	
	    	try{
	    	logger.debug("Entry : public int insertOrgSettings(SocietyVO societyVO)"+societyVO.getSocietyID() );

	    	String sql="insert into society_settings (society_id,favour,template,datazone_id,is_parent,hub_org) values (:societyID,:societyFullName,:template,:dataZoneID,:isParent,:hubOrg);  ";
	    	
	    	Map hashMap=new HashMap();
	    	hashMap.put("societyID",societyVO.getSocietyID());
	    	hashMap.put("societyFullName",societyVO.getSocFullName());
	    	hashMap.put("template",societyVO.getTemplate());
	    	hashMap.put("dataZoneID", societyVO.getDataZoneID());
	    	hashMap.put("isParent", societyVO.getIsParent());
	    	hashMap.put("hubOrg", societyVO.getIsHubOrg());
	    
	    	
	    	flag=namedParameterJdbcTemplate.update(sql,hashMap);
	    	    	
	    	logger.debug("Exit : public int insertOrgSettings(SocietyVO societyVO)"+flag);
	    	}catch (Exception ex) {
	    		logger.error("Exception in  public int insertOrgSettings(SocietyVO societyVO) for Org ID "+societyVO.getSocietyID()+" : "+ex);
	    	}
	    	return flag;
	    	
	    	
	     }
	
	public List searchSocietyList(){

			List societyList = null;
			String strSQL = "";
			try{	
			logger.debug("Entry public List searchSocietyList" );
			//1. SQL Query
			strSQL =" select society_id,society_name from society_details; " ;
			logger.debug("query : " + strSQL);		
			// 2. Parameters.
			Map hsmap=new HashMap();
			
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			            DropDownVO drVo=new DropDownVO();
			            drVo.setData(rs.getString("society_id"));
			            drVo.setLabel(rs.getString("society_name"));	
			             return drVo;
			            
			        }};
		
			        societyList = namedParameterJdbcTemplate.query(strSQL,hsmap,RMapper);
			        
		
			       
			        logger.debug("Exit : public List searchSocietyList()"+societyList.size());
			}catch (Exception ex){
				logger.error("Exception in searchSocietyList : "+ex);
				
			}
			return societyList;
			
			
		}
	public List searchSocietyListForBuilder(String builderID){

		List societyList = null;
		
		String strSQL = "";
		try{	
		logger.debug("Entry : public searchSocietyListForBuilder(String builderID)");
		//1. SQL Query
		strSQL =" SELECT * FROM society_details WHERE builder_id=:builderID ; " ;
		logger.debug("query : " + strSQL);			
		
		// 2. Parameters.
		Map hsmap=new HashMap();
		hsmap.put("builderID", builderID);
		
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            SocietyVO members=new SocietyVO();
		            members.setSocietyID(rs.getInt("society_id"));
		            members.setSocietyName(rs.getString("society_name"));
		           
		             return members;
		            
		        }};
	
		        societyList = namedParameterJdbcTemplate.query(strSQL,hsmap,RMapper);
		        
	
		       
		        logger.debug("Exit : public List searchSocietyListForBuilder(String builderID)"+societyList.size());
		}catch (Exception ex){
			logger.error("Exception in searchSocietyListForBuilder : "+ex);
			
		}
		return societyList;
		
		
	}

	
	public int insertBuildingNames(final List<InsertMemberVO>listOfBuilding,final int societyID){

		int flag=0;
		
		logger.debug("Entry : public int insertBuildingNames(final List<InsertMemberVO>listOfBuilding,final int societyID)" );

		try {
			String str="INSERT INTO building_details (society_id,building_name) VALUES (?,?) ;";
				
			getJdbcTemplate().batchUpdate(str, new BatchPreparedStatementSetter(){
				
				public void setValues(PreparedStatement ps, int i) throws SQLException{
					InsertMemberVO imVO = listOfBuilding.get(i);
					ps.setString(1, imVO.getSocietyID());
					ps.setString(2, imVO.getBuildingName());
					
				}
				public int getBatchSize(){
					return listOfBuilding.size();
				}
				
			});
			
			
		} catch (DataAccessException e) {
			flag=0;
			logger.error("DataACCESSException in insertBuildingNames : "+e);
			return flag;
		}
		catch (Exception ex) {
			flag=0;
			logger.error("Exception in insertBuildingNames : "+ex);
			return flag;
		}
	    flag=1;
		logger.debug("Exit : public int insertBuildingNames(final List<InsertMemberVO>listOfBuilding,final int societyID)"+flag);
		
		return flag;
		
		
	}
    public int deleteBuilding(int buildingID){
		
		int sucess = 0;
		String strSQL = "";
		try {
				logger.debug("Entry : public int deleteBuilding(String buildingID)");
				strSQL = "DELETE FROM building_details WHERE building_id=:buildId;" ;
				logger.debug("query : " + strSQL);
				// 2. Parameters.
				Map namedParameters = new HashMap();
				namedParameters.put("buildId", buildingID);
				
				sucess = namedParameterJdbcTemplate.update(strSQL,namedParameters);
				
				logger.debug("Exit : public int deleteBuilding(String buildingID)"+sucess);

		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in deleteBuilding :"+Ex);
			
		}
		return sucess;
	}	
    
    public List searchApartmentsList(String buildingID,int societyID){

		List memberList = null;
		
		String strSQL = "";
		String strWhereClause="";
		SocietyVO societyVO=getSocietyDetails(societyID);
		
		if(buildingID.equalsIgnoreCase("0"))
			strWhereClause=" ";
			else
				strWhereClause="  AND a.building_id="+buildingID+"";
		
		try{	
		logger.debug("Entry : public List searchApartmentsList(String buildingID)"+societyID);
		//1. SQL Query
		
		strSQL="Select * from (SELECT a.*,building_name,b.building_id AS buildID, u.unit_type, m.member_id,m.membership_id,m.email,m.mobile,m.title,m.full_name,m.phone,m.age,m.occupation,m.type,m.dob,m.issue_date "+
		      "FROM (apartment_details  a,building_details b ,unit_type_mapper u) LEFT  JOIN member_details m ON "+
		      "a.apt_id=m.apt_id "+
		      " WHERE  a.building_id=b.building_id AND a.unit_id=u.unit_id "+strWhereClause+" AND b.society_id=:societyID  order by a.seq_no) AS apt LEFT JOIN (SELECT * FROM ledger_user_mapping_"+societyVO.getDataZoneID()+" WHERE org_id=:societyID AND ledger_type='M' ) AS lu ON apt.member_id=lu.user_id";
		 				
		
		// 2. Parameters.
		Map namedParameters = new HashMap();
		namedParameters.put("BuildingID", buildingID);
		namedParameters.put("societyID", societyID);
       
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            InsertMemberVO members=new InsertMemberVO();
		            members.setAptId(rs.getInt("apt_id"));
		            members.setAptName(rs.getString("apt_name"));
		            members.setUnitName(rs.getString("building_name")+" "+rs.getString("apt_name"));
		            members.setBuildingID(rs.getString("building_id"));
		            members.setBuildingName(rs.getString("building_name"));
		            members.setDescription(rs.getString("description"));
		            members.setUnitType(rs.getString("unit_type"));
		            members.setUnitID(rs.getString("unit_id"));
		            members.setMembershipDate(rs.getString("issue_date"));
		            members.setSoldStatus(rs.getString("unit_sold_status"));
		            members.setIs_rented(rs.getInt("is_Rented"));
		            members.setIs_sale(rs.getInt("available_for_sale"));
		            members.setIntercomm(rs.getString("intercomm_no"));
		            members.setMemberId(rs.getString("member_id"));
		            members.setMembershipID(rs.getString("membership_id"));
		            members.setOldEmail(rs.getString("email"));
		            members.setTitle(rs.getString("title"));
		            members.setEmail(rs.getString("email"));
		            members.setMobile(rs.getString("mobile"));
		            members.setPhone(rs.getString("phone"));
		            members.setSocietyMaintanaceCharge(rs.getBigDecimal("societyMonthlyCharges"));
		            members.setSinkingFund(rs.getBigDecimal("sinking_fund"));
		            members.setSqFeet(rs.getBigDecimal("sq_feet"));
		            members.setCost(rs.getBigDecimal("cost"));
		            members.setAge(rs.getInt("age"));
		            members.setOccupation(rs.getString("occupation"));
		            members.setRmCharges(rs.getBigDecimal("repair_maintance"));
		            members.setFullName(rs.getString("full_name"));
		            members.setType(rs.getString("type"));
		            members.setDob(rs.getString("dob"));
		            members.setIsOccupied(rs.getInt("is_occupied"));
		            members.setLedgerID(rs.getInt("ledger_id"));
		               		            
		            		          
		            return members;
		        }};
	
		        memberList = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
		        
		        //logger.debug("Size of memberList is  : " + memberList.size() );
		        logger.debug("Exit : public List searchApartmentsList(String buildingID)");
		}catch (Exception ex){
			logger.error("Exception in searchApartmentsList : "+ex);
			
		}
		return memberList;
		
		
	}
     
  
    public List searchApartmentsListDropDown(String buildingID){

		List memberList = null;
		
		String strSQL = "";
		try{	
		logger.debug("Entry : public List searchApartmentsListDropDown(String buildingID)");
		//1. SQL Query
		
		strSQL=" SELECT a.* FROM apartment_details  a,building_details b WHERE a.building_id=b.building_id and (a.building_id=:BuildingID)  order by a.seq_no;";
		 				
		
		// 2. Parameters.
		 SqlParameterSource namedParameters = new MapSqlParameterSource("BuildingID", buildingID);
       
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            DropDownVO dropdown=new DropDownVO();
		            dropdown.setData(rs.getString("apt_id"));
		            dropdown.setLabel(rs.getString("apt_name"));
		            
		            		          
		            return dropdown;
		        }};
	
		        memberList = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
		        
		        logger.debug("Size of memberList is  : " + memberList.size() );
		        logger.debug("Exit : public List searchApartmentsListDropDown(String buildingID)");
		}catch (Exception ex){
			logger.error("Exception in searchApartmentsListDropDown : "+ex);
			
		}
		return memberList;
		
		
	}
 
    

    public int insertApartmentNames( final List<InsertMemberVO>listOfApartment,final int buildingID){

    	int flag=0;
    	
    	logger.debug("Entry : public int insertApartmentNames( final List<InsertMemberVO>listOfApartment,final String buildingID)");

    	try {
    		String str="INSERT INTO apartment_details (building_id,apt_name,available_for_rent,societyMonthlyCharges,description,type_of_unit,unit_sold_status,available_for_sale,intercomm_no,sinking_fund,repair_maintance,sq_feet, cost, unit_id,is_occupied) VALUES (?,?,?,?,?,?,?,?,? ,?, ? ,? ,?,? ,?)  ;";
    			
    		getJdbcTemplate().batchUpdate(str, new BatchPreparedStatementSetter() {
    			 
    			//@Override
    			public void setValues(PreparedStatement ps, int i) throws SQLException {
    				InsertMemberVO insertMemberVO = listOfApartment.get(i);
    				ps.setString(1, insertMemberVO.getBuildingID());
    				ps.setString(2, insertMemberVO.getAptName());
    				ps.setInt(3, insertMemberVO.getIs_rented());
    				ps.setBigDecimal(4, insertMemberVO.getSocietyMaintanaceCharge());
    				ps.setString(5, insertMemberVO.getDescription());
    				ps.setString(6, insertMemberVO.getUnitType());
    				ps.setString(7, insertMemberVO.getSoldStatus());
    				ps.setInt(8, insertMemberVO.getIs_sale());
    				ps.setString(9, insertMemberVO.getIntercomm());
    				ps.setBigDecimal(10, insertMemberVO.getSinkingFund());
    				ps.setBigDecimal(11, insertMemberVO.getRmCharges());
    				ps.setBigDecimal(12, insertMemberVO.getSqFeet());
    				ps.setBigDecimal(13, insertMemberVO.getCost());
    				ps.setString(14, insertMemberVO.getUnitID());
    				ps.setInt(15, insertMemberVO.getIsOccupied());
    			}
    			//@Override
    			public int getBatchSize() {
    				return listOfApartment.size();
    			}
    			
    			
    		});
    	
    	}catch (DataAccessException e) {
    		flag=0;
    		logger.error("DataACCESSException in insertApartmentNames : "+e);
    	
    	}catch (Exception ex) {
    		logger.error("Exception in insertApartmentNames : "+ex);
    	}
    	flag=1;
    	logger.debug("Exit : public int insertApartmentNames( final List<InsertMemberVO>listOfApartment,final String buildingID)"+flag);
    	
    	return flag;
    	
    	
    }
    
    public int insertApartment(InsertMemberVO aptVO){

    	int flag=0;
    	
    	logger.debug("Entry : public int insertApartmentNames( final List<InsertMemberVO>listOfApartment,final String buildingID)");

    	try {
/*    		String str="INSERT INTO apartment_details (building_id,apt_name,available_for_rent,societyMonthlyCharges,description,type_of_unit,unit_sold_status,available_for_sale,intercomm_no,sinking_fund,repair_maintance,sq_feet, cost, unit_id,is_occupied) VALUES"
    				+ " (:building_id,:apt_name,:available_for_rent, :societyMonthlyCharges, :description, :type_of_unit, :unit_sold_status, :available_for_sale, :intercomm_no, :sinking_fund, :repair_maintance, :sq_feet, :cost, :unit_id, :is_occupied)  ;";
    				
    			
    		Map hashMap=new HashMap();
        	hashMap.put("building_id",aptVO.getBuildingID());
        	hashMap.put("apt_name",aptVO.getAptName());
        	hashMap.put("available_for_rent",aptVO.getIs_rented());
        	hashMap.put("societyMonthlyCharges",aptVO.getSocietyMaintanaceCharge());
        	hashMap.put("description",aptVO.getDescription());
        	hashMap.put("type_of_unit",aptVO.getUnitType());
        	hashMap.put("societyMaintanaceCharge",aptVO.getSocietyMaintanaceCharge());
        	hashMap.put("unit_sold_status",aptVO.getSoldStatus());
        	hashMap.put("available_for_sale", aptVO.getIs_sale());
        	hashMap.put("intercomm_no", aptVO.getIntercomm());
        	hashMap.put("sinking_fund", aptVO.getSinkingFund());
        	hashMap.put("repair_maintance", aptVO.getRmCharges());
        	hashMap.put("sq_feet", aptVO.getSqFeet());
        	hashMap.put("cost", aptVO.getCost());
        	hashMap.put("unit_id", aptVO.getUnitID());
        	hashMap.put("is_occupied", aptVO.getIsOccupied());
        	
        	flag=namedParameterJdbcTemplate.update(str,hashMap);*/
        	
        	
        	String sql="INSERT INTO apartment_details (building_id,apt_name,description,type_of_unit,unit_sold_status,sq_feet, cost, unit_id,is_occupied) VALUES" +
					   "(:buildingID, :aptName, :description, :unitType, :soldStatus,:sqFeet,:cost,:unitID,:isOccupied); ";
			logger.debug("query : " + sql);
			
				
			   SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(aptVO);
			   KeyHolder keyHolder = new GeneratedKeyHolder();
			   getNamedParameterJdbcTemplate().update(sql, fileParameters, keyHolder);
			   flag=keyHolder.getKey().intValue();
			   
			   if(flag>0){
				  String strs="UPDATE apartment_details SET seq_no=:aptID WHERE apt_id=:aptID;";
				  Map hashMaps=new HashMap();
		        	hashMaps.put("aptID",flag);
				   int successFlage=namedParameterJdbcTemplate.update(strs,hashMaps);
				   
			   }
        	
    	
    	}catch (DataAccessException e) {
    		flag=0;
    		logger.error("DataACCESSException in insertApartmentNames : "+e);
    	
    	}catch (Exception ex) {
    		logger.error("Exception in insertApartmentNames : "+ex);
    	}
    	flag=1;
    	logger.debug("Exit : public int insertApartmentNames( final List<InsertMemberVO>listOfApartment,final String buildingID)"+flag);
    	
    	return flag;
    	
    	
    }

    public int updateApartmentDetails(ApartmentVO aptVO,int apartmentID){

    	int flag=0;
    	
    	try{
    	logger.debug("Entry : public int updateApartmentDetails(ApartmentVO aptVO,int apartmentID)"+aptVO.getUnitID() );

    	String sql="UPDATE  apartment_details SET  description=:description,societyMonthlyCharges=:societyMaintanaceCharge,available_for_rent=:is_rented,type_of_unit=:unitType,unit_sold_status=:soldStatus," +
    			"available_for_sale=:is_sale,intercomm_no=:intercomm ,sinking_fund=:SF ,repair_maintance=:RM, sq_feet=:SQF, cost=:COST ,unit_id=:UNITID , is_occupied=:isOccupied " +
    			" WHERE apt_id="+apartmentID+"  ";
    	
    	Map hashMap=new HashMap();
    	hashMap.put("description",aptVO.getDescription());
    	hashMap.put("is_rented",aptVO.getIs_rented());
    	hashMap.put("unitType",aptVO.getUnitType());
    	hashMap.put("soldStatus",aptVO.getSoldStatus());
    	hashMap.put("is_sale",aptVO.getIs_sale());
    	hashMap.put("intercomm",aptVO.getIntercomm());
    	hashMap.put("societyMaintanaceCharge",aptVO.getSocietyMaintanaceCharge());
    	hashMap.put("SF",aptVO.getSinkingFund());
    	hashMap.put("RM", aptVO.getRmCharges());
    	hashMap.put("SQF", aptVO.getSqFeet());
    	hashMap.put("COST", aptVO.getCost());
    	hashMap.put("UNITID", aptVO.getUnitID());
    	hashMap.put("isOccupied", aptVO.getIsOccupied());
    	
    	flag=namedParameterJdbcTemplate.update(sql,hashMap);
    	    	
    	logger.debug("Exit : public int updateApartmentDetails(ApartmentVO aptVO,int apartmentID)"+flag);
    	}catch (Exception ex) {
    		logger.error("Exception in updateApartmentDetails : "+ex);
    	}
    	return flag;
    	
    	
     }
     public int deleteApartment(int apartmentID){
    	
    	int sucess = 0;
    	String strSQL = "";
    	try {
    			logger.debug("Entry : public int deleteApartment(int apartmentID)");
    			
    			strSQL = "DELETE FROM apartment_details WHERE apt_id=:apt_id;" ;
    			logger.debug("query : " + strSQL);
    			// 2. Parameters.
    			Map namedParameters = new HashMap();
    			namedParameters.put("apt_id", apartmentID);
    			
    			sucess = namedParameterJdbcTemplate.update(strSQL,namedParameters);
    			
    			logger.debug("Exit :  public int deleteApartment(int apartmentID)"+sucess);

    	}catch(Exception Ex){
    		
    		Ex.printStackTrace();
    		logger.error("Exception in deleteApartment : "+Ex);
    		
    	}
    	return sucess;
    }	

    public int checkAptAvailablilty(int aptID){
    	int checkFlag=0;
    	String sql="SELECT COUNT(member_id) FROM member_details WHERE apt_id=:aptID;";
    	logger.debug("in MemberDAO aptID "+aptID );
    	Map hmap=new HashMap();
    	hmap.put("aptID", aptID);
    	
    	checkFlag=namedParameterJdbcTemplate.queryForObject(sql, hmap,Integer.class);
    	//checkFlag=namedParameterJdbcTemplate.queryForInt(sql, hmap);
    	logger.info("The apartment contains "+checkFlag+" members.");
    	return checkFlag;
    }

    
    public int checkAvailibilityForPrimaryMember(int aptID){
    	int checkFlag=0;
    	List memberList=null;
    	logger.debug(" public int checkAvailibilityForPrimaryMember(int aptID) "+aptID );
    	try {
			String sql="SELECT * FROM member_details WHERE apt_id =:aptID  AND TYPE='P' ;";
			
			Map hmap=new HashMap();
			hmap.put("aptID", aptID);
			
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			            MemberVO memberVO=new MemberVO();
			            memberVO.setFullName(rs.getString("full_name"));
			            memberVO.setMemberID(rs.getInt("member_id"));
			             return memberVO;
			            
			        }};
			
			memberList=namedParameterJdbcTemplate.query(sql, hmap,RMapper);
			checkFlag=memberList.size();
		} catch (Exception e) {
		
			logger.error("The apartment contains "+checkFlag+" members."+e);
		}
    	//checkFlag=namedParameterJdbcTemplate.queryForInt(sql, hmap);
    	logger.info("The apartment contains "+checkFlag+" members.");
    	return checkFlag;
    }
 
    public int insertNewMember(MemberVO insertMemberVO,int apartmentID)
    {	
    	int insertFlag=0;
    	int flag=0;
    	int genratedId=0;
    	String sql=null;
    	try
    	{
    		logger.debug("Entry : public int insertNewMember(InsertMemberVO insertMemberVO,int apartmentID)"+insertMemberVO.getAptID()+" "+insertMemberVO.getType());
    	    
    		   	sql="INSERT INTO member_details (membership_id,apt_id,title,email,mobile,phone,full_name,type,issue_date,age, occupation, dob,tags) "+
				" values (:membershipID,:aptID,:title,:email,:mobile,:phone,:fullName,:type, :membershipDate,:age ,:occupation ,:dateOfBirth,:tags) ;";
    		
    	    
    	    SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(insertMemberVO);
			   KeyHolder keyHolder = new GeneratedKeyHolder();
			   getNamedParameterJdbcTemplate().update(sql, fileParameters, keyHolder);
			   genratedId=keyHolder.getKey().intValue();
    		
    	/*	logger.debug("query : " + sql);
    					
    		Map hashMap=new HashMap();
    		hashMap.put("aptId", apartmentID);
    		hashMap.put("title",insertMemberVO.getTitle());
    		hashMap.put("memberId", insertMemberVO.getMemberId());
    		hashMap.put("membershipId", insertMemberVO.getMemberShipID());
    		
    		hashMap.put("email",insertMemberVO.getEmail());
    		hashMap.put("mobile",insertMemberVO.getMobile());
    		hashMap.put("fullName", insertMemberVO.getFullName());
    		hashMap.put("type", insertMemberVO.getType());
    		hashMap.put("membershipDate", insertMemberVO.getMembershipDate());
    		hashMap.put("age", insertMemberVO.getAge());
    		hashMap.put("occupation", insertMemberVO.getOccupation());
    		   		
    		
    		insertFlag=namedParameterJdbcTemplate.update(sql,hashMap);
*/
    	}catch(DuplicateKeyException duplicateEntry){
    		duplicateEntry.printStackTrace();
    		logger.error("DuplicateKeyException in insertNewMember : "+duplicateEntry);
    		insertFlag = 2;
    		genratedId=0;
    		return genratedId;	
       		
    	}
    	catch(Exception Ex){
    		logger.error("Exception in insertNewMember : "+Ex);
    	}
    	logger.debug("Exit : public int insertNewMember(InsertMemberVO insertMemberVO,int apartmentID)"+genratedId);
    return genratedId;	
    }
     
    public int updateMember(MemberVO insertMemberVO,int apartmentID)
    {	
    	int insertFlag=0;
    	try
    	{	
    		logger.info("InsertVo "+insertMemberVO.getMembershipDate()+" "+insertMemberVO.getAge());
    		logger.debug("Entry : public int updateMember(InsertMemberVO insertMemberVO,int apartmentID)");
    		String sql="UPDATE  member_details SET title=:title,membership_id=:membershipID, full_name=:fullName, mobile=:mobile,phone=:phone, email=:email, full_name=:fullName ,issue_date=:membershipDate , type=:type, age=:age, occupation=:occupation, dob=:dob ,tags=:tags  WHERE type=:type and member_id=:memberId and  apt_id="+apartmentID+"  ";
    				
    		Map hashMap=new HashMap();
    		hashMap.put("title",insertMemberVO.getTitle());
    		hashMap.put("email",insertMemberVO.getEmail());
    		hashMap.put("mobile",insertMemberVO.getMobile());
    		hashMap.put("fullName", insertMemberVO.getFullName());
    		hashMap.put("type", insertMemberVO.getType());
    		hashMap.put("memberId", insertMemberVO.getMemberID());
    		hashMap.put("membershipID", insertMemberVO.getMembershipID());
    		hashMap.put("membershipDate", insertMemberVO.getMembershipDate());
    		hashMap.put("age", insertMemberVO.getAge());
    		hashMap.put("occupation", insertMemberVO.getOccupation());
    		hashMap.put("dob", insertMemberVO.getDateOfBirth());  	
    		hashMap.put("phone", insertMemberVO.getPhone());
    		hashMap.put("tags", insertMemberVO.getTags());
    		
    		insertFlag=namedParameterJdbcTemplate.update(sql,hashMap);
       		
    	}
    	catch(Exception Ex){
    		logger.error("Exception in updateMember : "+Ex);
    	}
    	logger.debug("Exit : public int updateMember(InsertMemberVO insertMemberVO,int apartmentID)"+insertFlag);
    return insertFlag;	
    }

    
    public InsertMemberVO getAssociateMember(int aptID,String type){

		InsertMemberVO memberVO = null;
		
		logger.debug("Entry InsertMemberVO getAssociateMember(int aptID)" );
		
		try{	
		
			//1. SQL Query
	        String	strSQL ="select full_name,email,mobile,phone, title,type from member_details where type=:type and apt_id=:aptID; " ;
			logger.debug("query : " + strSQL);		
			// 2. Parameters.
			Map hsmap=new HashMap();
			hsmap.put("aptID",aptID);
			hsmap.put("type", type);
			
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			            InsertMemberVO memberVO=new InsertMemberVO();
			            memberVO.setFullName(rs.getString("full_name"));
			            memberVO.setTitle(rs.getString("title"));
			            memberVO.setEmail(rs.getString("email"));
			            memberVO.setMobile(rs.getString("mobile"));
			            memberVO.setType(rs.getString("type"));
			            memberVO.setPhone(rs.getString("phone"));
			             return memberVO;
			            
			        }};
		
			        memberVO = (InsertMemberVO) namedParameterJdbcTemplate.queryForObject(strSQL,hsmap,RMapper);
			        
		
		    
		        
	
		       
		        logger.info("Exit : public InsertMemberVO getAssociateMember(int aptID)"+memberVO.getType());
		}catch(EmptyResultDataAccessException e){
			logger.info("No associate member avaialable for "+aptID);
			
		}catch (Exception ex){
			logger.error("Exception InsertMemberVO getAssociateMember(int aptID) : "+ex);
			
		}
		return memberVO;
		
		
	}
    
   
    
    
    
    
    public int insertMemberHistory(InsertMemberVO memberVO){
    	int success=0;
    	InsertMemberVO memberVo=new InsertMemberVO();
    	Date currentDatetime = new Date(System.currentTimeMillis());   
	    java.sql.Date sqlDate = new java.sql.Date(currentDatetime.getTime()); 
    	try {
    		logger.debug("Entry : public MemberVO getMemberInfo(String memeberID)");
    		
    		String query= "INSERT INTO member_history (member_id,membershi_id,apt_id,title,full_name,email,mobile,phone,type,share_cert_no,issue_date,expiry_date,age) "+
				" values (:memberId,:membershipId,:aptId,:title,:fullName,:email,:mobile,:phone,:type,:shNo,:issue,:expiry ,:age) ;";
    	logger.debug("query : " + query);
    	Map namedParameters = new HashMap();
    	namedParameters.put("memberId", memberVO.getMemberId());
    	namedParameters.put("membershipId", memberVO.getMembershipID());
    	namedParameters.put("aptId", memberVO.getAptId());
    	namedParameters.put("title", memberVO.getTitle());
    	namedParameters.put("fullName", memberVO.getFullName());
    	namedParameters.put("email", memberVO.getEmail());
    	namedParameters.put("mobile", memberVO.getMobile());
    	namedParameters.put("phone", memberVO.getPhone());
    	namedParameters.put("type", memberVO.getType());
    	namedParameters.put("shNo", memberVO.getShareSerialNo());
    	namedParameters.put("issue", memberVO.getIssueDate());
    	namedParameters.put("expiry", sqlDate);
    	namedParameters.put("age", memberVO.getAge());
    	
    	
    	success=namedParameterJdbcTemplate.update(query, namedParameters);
    		
    	} catch (DataAccessException e) {
    	    logger.error("DataAccessException in getMemberInfo : "+e);
    	}
    	catch (Exception ex) {
    		logger.error("Exception in getMemberInfo : "+ex);
    	}
    	return success;
    }
    
    public int delMember(InsertMemberVO member) {

 		int sucess = 0;
 		try {
 			logger.debug("Entry : public int delMember(InsertMemberVO member,int apartmentID)");

 			String sqlQuery="Delete from member_details where member_id=:memberID";
 			
 			Map hashMap=new HashMap();
 			hashMap.put("memberID", member.getMemberId());
 			
 			sucess=namedParameterJdbcTemplate.update(sqlQuery, hashMap);
 			
 			
 			logger.debug("Exit : public int delMember(InsertMemberVO member,int apartmentID)");

 		}catch (Exception Ex) {
 			logger.error("Exception in public int delMember(InsertMemberVO member,int apartmentID): "+Ex);
 		}
 		return sucess;
 	}
   
    public SocietyVO getSocietyDetails(String aptID,int societyID)
    {	
    	SocietyVO societyVO=new SocietyVO();
    	 String query = "";
    	  if(aptID=="0")
    		  query="select * from society_details s,society_settings ss WHERE s.society_id=ss.society_id and s.society_id="+societyID+"  ";
          else
        	  query="SELECT s.*,ss.* FROM society_settings ss, society_details s ,building_details b,apartment_details a WHERE b.society_id=s.society_id and s.society_id=ss.society_id AND a.building_id=b.building_id AND a.apt_id="+aptID+"";
                
    	try
    	{
    		logger.debug("Entry : public SocietyVO getSocietyDetails(String aptID)");
    		

    				
    		Map hashMap=new HashMap();
    		//hashMap.put("aptID", aptID);
    		 RowMapper RMapper = new RowMapper() {
 		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
 		            SocietyVO societyVO=new SocietyVO();
 		            societyVO.setSocietyID(rs.getInt("society_id"));
 		            societyVO.setSocietyName(rs.getString("society_name"));
 		            societyVO.setSocietyRegNo(rs.getString("register_no"));
 		            societyVO.setSocFullName(rs.getString("society_full_name"));
 		            societyVO.setMailListM(rs.getString("mailingList_member"));
 		            societyVO.setSocShortName(rs.getString("short_name"));
 		            societyVO.setNotes(rs.getString("notes"));
 		            societyVO.setEffectiveDate(rs.getString("effective_date"));
 		            societyVO.setThresholdBalance(rs.getBigDecimal("min_amount"));
 		            societyVO.setTxCloseDate(rs.getString("tx_close_date"));
 		            societyVO.setAccountant(rs.getString("accountant"));
 		            societyVO.setOrgID(rs.getInt("org_id"));
 		            societyVO.setServiceTax(rs.getBigDecimal("service_tax"));
 		            societyVO.setLogoUrl(rs.getString("logo_url"));
		            societyVO.setTemplate(rs.getString("template"));
		            societyVO.setGstIN(rs.getString("gstin"));
		            societyVO.setServerName(rs.getString("server_name"));
		            societyVO.setSchemaName(rs.getString("schema_name"));
		            societyVO.setAuditCloseDate(rs.getString("audit_close_date"));
		            societyVO.setGstCloseDate(rs.getString("gst_close_date"));
		            societyVO.setReminderToAll(rs.getInt("reminder_to_all"));
		            societyVO.setStateName(rs.getString("state_of_supply"));
		            societyVO.setTdsLedgerID(rs.getInt("tds_ledger_id"));
		            societyVO.setDriveName(rs.getString("drive_name"));
		            societyVO.setOrgType(rs.getString("org_type"));
		            societyVO.setIsParent(rs.getInt("is_parent"));
		            societyVO.setIsHubOrg(rs.getInt("hub_org"));
		            societyVO.setGstAsIncome(rs.getInt("gst_as_income"));
		            societyVO.setGstAsExpense(rs.getInt("gst_as_expense"));
		            societyVO.setPanNo(rs.getString("pan"));
		            societyVO.setTanNo(rs.getString("tan"));
 		            return societyVO;
 		        }};
 	
    		
    		   		
    		
    		societyVO=(SocietyVO) namedParameterJdbcTemplate.queryForObject(query, hashMap, RMapper);
       		
    	}
    	catch(EmptyResultDataAccessException e){
    		logger.info("No bank Details available"+e);
    	}
    	catch(NullPointerException Ex){
    		logger.info("No bank details available : "+Ex);
    	}
    	catch(Exception Ex){
    		logger.error("Exception in public SocietyVO getSocietyDetails(String aptID) : "+Ex);
    	}
    	logger.debug("Exit : public SocietyVO getSocietyDetails(String aptID)");
    return societyVO;	
    }

    
    public BankInfoVO getBankDetails(int societyID)
    {	
    	BankInfoVO bankVO=new BankInfoVO();
    	try
    	{
    		logger.debug("Entry : public BankInfoVO getBankDetails(int societyID)");
    		String sql="SELECT society_settings.favour,society_details.society_name,bank_name,account_no ,acc_type,ifsc,micr,branch_code,contact,bank_details.society_id  ,society_details.short_name " +
    				"FROM bank_details,society_details,society_settings " +
    				"WHERE society_settings.society_id=society_details.society_id " +
    				"AND society_details.society_id=bank_details.society_id " +
    				"AND bank_details.society_id=:societyID limit 1";
    				
    		Map hashMap=new HashMap();
    		hashMap.put("societyID", societyID);
    		 RowMapper RMapper = new RowMapper() {
 		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
 		            BankInfoVO bankVO=new BankInfoVO();
 		            bankVO.setAccountNo(rs.getString("account_no"));
 		            bankVO.setBankName(rs.getString("bank_name"));
 		            bankVO.setBranchCode(rs.getString("branch_code"));
 		            bankVO.setContact(rs.getString("contact"));
 		            bankVO.setFavour(rs.getString("favour"));
 		            bankVO.setIfscNo(rs.getString("ifsc"));
 		            bankVO.setMicrNo(rs.getString("micr"));
 		            bankVO.setSocietyId(rs.getString("society_id"));
 		            bankVO.setSocietyName(rs.getString("society_name"));
 		            bankVO.setAccType(rs.getString("acc_type"));
 		            bankVO.setSocietyShortName(rs.getString("short_name"));
 		           
 		            return bankVO;
 		        }};
 	
    		
    		   		
    		
    		bankVO=(BankInfoVO) namedParameterJdbcTemplate.queryForObject(sql, hashMap, RMapper);
       		
    	}
    	catch(EmptyResultDataAccessException e){
    		logger.info("No bank Details available"+e);
    	}
    	catch(NullPointerException Ex){
    		logger.info("No bank details available : "+Ex);
    	}
    	catch(Exception Ex){
    		logger.error("Exception in public BankInfoVO getBankDetails(int societyID) : "+Ex);
    	}
    	logger.debug("Exit : public BankInfoVO getBankDetails(int societyID)"+bankVO.getBankName()+""+bankVO.getBranchCode());
    return bankVO;	
    }

    
    public int insertBooking(BookingVO bookingVO)
	{	
		int insertFlag=0;
		try{
		
		logger.debug("Entry : public int insertBooking(BookingVO bookingVO)");
		
			String sql="INSERT INTO booking_details(booked_date,member_id,mobile,utility,from_time,upto_time,status)VALUES" +
					   "(:bookDate, :memberID, :mobile, :utility, :fromDate,:toDate,:status); ";
			logger.debug("query : " + sql);
			
			Map namedParam = new HashMap();
			namedParam.put("bookDate",bookingVO.getBookingDate());
			namedParam.put("memberID",bookingVO.getMemberID());
			namedParam.put("mobile",bookingVO.getMobile());			
			namedParam.put("utility",bookingVO.getUtilityName());	
			namedParam.put("fromDate",bookingVO.getFromTime());	
			namedParam.put("toDate",bookingVO.getToTime());	
			namedParam.put("status", bookingVO.getStatus());
			
			insertFlag=namedParameterJdbcTemplate.update(sql,namedParam);
		
          			
		}
		
		catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in public int insertBooking(BookingVO bookingVO) : "+Ex);
		
		}
		logger.debug("Exit :public int insertBooking(BookingVO bookingVO)"+insertFlag);
	return insertFlag;	
	}
    
    
    public List bookings(String memberID){

		List bookingList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry public List bookings(String memberID)" );
		//1. SQL Query
		strSQL =" select booking_details.* ,concat(first_name,' ',last_name) AS member_name  from booking_details,member_details where booking_details.member_id=member_details.member_id "+ 
		"AND booking_details.member_id=:memberID ORDER BY booked_date DESC ";
				
		logger.debug("query : " + strSQL);		
		// 2. Parameters.
		Map hsmap=new HashMap();
		hsmap.put("memberID", memberID);
		
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            BookingVO bookingVO=new BookingVO();
		            bookingVO.setBookingDate(rs.getString("booked_date"));
		            bookingVO.setBookingID(rs.getString("booking_id"));
		            bookingVO.setFromTime(rs.getString("from_time"));
		            bookingVO.setToTime(rs.getString("upto_time"));
		            bookingVO.setMemberName(rs.getString("member_name"));
		            bookingVO.setMobile(rs.getString("mobile"));
		            bookingVO.setStatus(rs.getString("status"));
		            bookingVO.setUtilityName(rs.getString("utility"));
		             return bookingVO;
		            
		        }};
	
		        bookingList = namedParameterJdbcTemplate.query(strSQL,hsmap,RMapper);
		        
	
		       
		        logger.debug("Exit :public List bookings(String memberID)"+bookingList.size());
		}catch (Exception ex){
			logger.error("Exception public List bookings(String memberID) : "+ex);
			
		}
		return bookingList;
		
		
	}
    
    
    public int checkAvailability(BookingVO bookingVO)
	{	
		int insertFlag=0;
		try{
		
		logger.debug("Entry : public int checkAvailability(BookingVO bookingVO)");
		
			String sql="SELECT COUNT(booking_id) FROM booking_details WHERE booked_date=:bookDate AND utility=:utility ";
			logger.debug("query : " + sql);
			
			Map namedParam = new HashMap();
			namedParam.put("bookDate",bookingVO.getBookingDate());
					
			namedParam.put("utility",bookingVO.getUtilityName());	
			
			insertFlag=namedParameterJdbcTemplate.queryForObject(sql,namedParam,Integer.class);
			//insertFlag=namedParameterJdbcTemplate.queryForInt(sql,namedParam);
		
          			
		}
		
		catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in public int checkAvailability(BookingVO bookingVO) : "+Ex);
		
		}
		logger.debug("Exit :public int checkAvailability(BookingVO bookingVO)"+insertFlag);
	return insertFlag;	
	}
    
    public List getUnitList(){

		List unitList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry public List getUnitList" );
		//1. SQL Query
		strSQL =" select unit_id,unit_type from unit_type_mapper; " ;
		logger.debug("query : " + strSQL);		
		// 2. Parameters.
		Map hsmap=new HashMap();
		
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            DropDownVO drVo=new DropDownVO();
		            drVo.setData(rs.getString("unit_id"));
		            drVo.setLabel(rs.getString("unit_type"));	
		             return drVo;
		            
		        }};
	
		        unitList = namedParameterJdbcTemplate.query(strSQL,hsmap,RMapper);
		        
	
		       
		        logger.debug("Exit : public List getUnitList()"+unitList.size());
		}catch (Exception ex){
			logger.error("Exception in getUnitList : "+ex);
			
		}
		return unitList;
		
		
	}
    
    public List getJRegisterList(int socID,int buildingId ){

		List memberList = null;
		String strWhereClause=null;
		String strSQL = "";
		SocietyVO societyVO=getSocietyDetails(socID);
		try{	
		logger.info("Entry : public List getJRegisterList(String socID,String buildingId )"+buildingId);
		//1. SQL Query
		
		if(buildingId==0)
			strWhereClause=" ";
			else
				strWhereClause="  AND b.building_id="+buildingId+"";
       	
  strSQL= "Select * From (SELECT s.share_date, s.share_cert_no, s.total_share_value, s.no_of_share, member_id,type_of_unit,b.society_id ,a.apt_id, email,a.apt_name,title, mobile, TYPE,CONCAT(title,' ',full_name)AS fname,CONCAT(building_name,' ',apt_name)AS bName,membership_id,age,occupation,a.is_rented, m.tags, m.pan_no, m.gstin, m.state_of_supply "  
	 	+ " FROM (apartment_details a, building_details b, member_details m) LEFT JOIN  share_cert_details s " 
   	 	+ " ON m.apt_id=s.apt_id WHERE m.apt_id=a.apt_id AND a.building_id=b.building_id "
        + " "+strWhereClause+" "
        + " AND b.society_id="+socID+" AND is_current!=0 AND unit_sold_status='Sold'  "
        + " ORDER BY a.seq_no ) AS apt LEFT JOIN (SELECT * FROM ledger_user_mapping_"+societyVO.getDataZoneID()+" WHERE org_id="+socID+" AND ledger_type='M' ) AS lu ON apt.member_id=lu.user_id;";
      
	logger.debug("query : " + strSQL);	
		// 2. Parameters.
		Map Hmap = new HashMap();
		
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            InsertMemberVO members=new InsertMemberVO();
		            members.setAptId(rs.getInt("apt_id"));
		            members.setMembershipID(rs.getString("membership_id"));
		            members.setOldEmail(rs.getString("email"));
		            members.setAptName(rs.getString("apt_name"));
		            members.setBuildingName(rs.getString("bName"));
		            members.setUnitType(rs.getString("type_of_unit"));
		            members.setMemberId(rs.getString("member_id"));
		            members.setTitle(rs.getString("title"));
		            members.setEmail(rs.getString("email"));
		            members.setMobile(rs.getString("mobile"));
		            members.setFullName(rs.getString("fname"));
		            members.setType(rs.getString("type"));
		            members.setIssueDate(rs.getString("share_date"));
		            members.setShareSerialNo(rs.getString("share_cert_no"));
		            members.setShareAmount(rs.getBigDecimal("total_share_value"));
		            members.setNoOfShares(rs.getInt("no_of_share"));
		            members.setAge(rs.getInt("age"));
		            members.setOccupation(rs.getString("occupation"));
		            members.setIs_rented(rs.getInt("is_rented"));     
		            members.setTags(rs.getString("tags"));
		            members.setPanNo(rs.getString("pan_no"));
		            members.setSupplyState(rs.getString("state_of_supply"));
		            members.setGstin(rs.getString("gstin"));
		            members.setLedgerID(rs.getInt("ledger_id"));
		            return members;
		        }};
	
		        memberList = namedParameterJdbcTemplate.query(strSQL,Hmap,RMapper);
		        
		        logger.info("Size of memberList is  : " + memberList.size() );
		        logger.debug("Exit : public List getJRegisterList(String socID,String buildingId )");
		}catch (Exception ex){
			logger.error("Exception in getJRegisterList : "+ex);
			
		}
		return memberList;
		
		
	}
    
    
    public List getIRegisterList(String aptID ){

		List memberList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry : public List getIRegisterList(String aptID )");
		//1. SQL Query
		
       	
  strSQL= " SELECT a.apt_id,unit_type,mobile, member_id ,TYPE,CONCAT(title,' ',full_name)AS fname,CONCAT (building_name,' ',apt_name)AS bName,s.share_cert_no,s.total_share_value,s.no_of_share,s.share_date ,membership_id "
	  	+ " FROM ( member_details m,apartment_details a,building_details b,unit_type_mapper u )  LEFT JOIN share_cert_details s ON m.apt_id=s.apt_id "	
	    + " WHERE  a.apt_id=m.apt_id AND a.building_id=b.building_id and is_current!=0  "
	    + " AND a.unit_id=u.unit_id AND a.apt_id="+aptID+" ";
        
	logger.debug("query : " + strSQL);	
		// 2. Parameters.
		Map Hmap = new HashMap();
		
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            InsertMemberVO members=new InsertMemberVO();
		            members.setAptId(rs.getInt("apt_id"));
		            members.setMemberId(rs.getString("member_id"));	
		            members.setMembershipID(rs.getString("membership_id"));
		            members.setUnitType(rs.getString("unit_type"));
		            members.setMobile(rs.getString("mobile"));
		            members.setFullName(rs.getString("fname"));
		            members.setAptName(rs.getString("bName"));
		            members.setType(rs.getString("type"));
		            members.setIssueDate(rs.getString("share_date"));
		            members.setShareSerialNo(rs.getString("share_cert_no"));
		            members.setShareAmount(rs.getBigDecimal("total_share_value"));
		            members.setNoOfShares(rs.getInt("no_of_share"));
		           
		            return members;
		        }};
	
		        memberList = namedParameterJdbcTemplate.query(strSQL,Hmap,RMapper);
		        
		        logger.debug("Size of memberList is  : " + memberList.size() );
		        logger.debug("Exit : public List getIRegisterList(String aptID )");
		}catch (Exception ex){
			logger.error("Exception in getIRegisterList : "+ex);
			
		}
		return memberList;
		
		
	}
    
    public List getNomineeList(String memberId ){

		List nomineeList = null;
		
		try{	
		logger.debug("Entry :  public List getNomineeList(String memberId )"+memberId);
		//1. SQL Query
		
       	
		String createAddressView="CREATE VIEW address AS SELECT apr.person_id,ab.* FROM address_book ab,address_person_relationship apr WHERE apr.address_id=ab.address_id AND apr.person_category='N';";	
		//jdbcTemplate.execute(createAddressView);
		String createNomineeAddressView="create view nominee_address as select * from (nominee_details n) left join address a on n.nominee_id=a.person_id ;";	
		//jdbcTemplate.execute(createNomineeAddressView);
		
		String strSQL = "SELECT CONCAT(b.building_name,' ',a.apt_name) AS aptName,m.full_name,n.*,a.apt_id ,m.member_id AS MemberID,s.id As statusId ,s.name As statusName "+
    		             " FROM (apartment_details  a,building_details b,member_details m, nominee_address n,documents_relation dr,status s) "+
             		     " WHERE  a.building_id=b.building_id AND m.apt_id=a.apt_id " +
             		     "  AND m.is_current=1 and a.unit_sold_status='Sold' AND m.member_id=n.member_id  AND dr.entity_id=m.member_id AND s.id=dr.status_id and m.member_id=:memberID GROUP BY n.nominee_id ORDER BY seq_no; "; 
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("memberID", memberId);
			
			//3. RowMapper.
			
			RowMapper RMapper = new RowMapper() {
				String tempAptName = "";
				String tempMemberName="";
				int i=0;
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					
					  NominationRegisterVO nomRegVO = new NominationRegisterVO();
			            nomRegVO.setAptId(rs.getInt("apt_id"));
			            nomRegVO.setDob(rs.getString("dob"));
			            String aptName =rs.getString("aptName");			           
			            if (!tempAptName.equalsIgnoreCase(aptName)) {
							nomRegVO.setAptName(aptName);
							tempAptName = aptName;
							++i;
							nomRegVO.setSrNo(""+i);
						} else {
							nomRegVO.setSrNo("");
							nomRegVO.setAptName("");
						}
			            nomRegVO.setMemberId(rs.getInt("MemberID"));
			            nomRegVO.setNominationId(rs.getInt("nominee_id"));
			            nomRegVO.setNominationRegisterID(rs.getInt("nomination_id"));
			            nomRegVO.setMemberName(rs.getString("full_name"));
			            String memberName=rs.getString("full_name");
			            if (!tempMemberName.equalsIgnoreCase(memberName)) {
							nomRegVO.setMemberName(memberName);
							tempMemberName = memberName;
						} else {
							nomRegVO.setMemberName(" ");
						}
			            nomRegVO.setNomineeName(rs.getString("nominee_name"));
			            nomRegVO.setRelation(rs.getString("relation"));
			            nomRegVO.setShare(rs.getBigDecimal("share"));
			            nomRegVO.setRemark(rs.getString("remark"));
			            if(rs.getString("city")==null){
			            	nomRegVO.setAddressString("N A");
			            }else
			            nomRegVO.setAddressString(rs.getString("line_1")+","+rs.getString("line_2")+","+rs.getString("city"));
			            nomRegVO.setNominationDate(rs.getString("issue_date"));
			            nomRegVO.setRecordingDate(rs.getString("recording_date"));
			            nomRegVO.setSameAddress(rs.getInt("has_address"));
			            nomRegVO.setRevocationDate(rs.getString("revocation_date"));       
			            nomRegVO.setDocumentStatusID(rs.getInt("statusId"));
			            nomRegVO.setIsMinor(rs.getInt("is_minor"));
			            nomRegVO.setDocumentStatusName(rs.getString("statusName"));
			            return nomRegVO;
				}
			};
			nomineeList=  namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);

		        
		        logger.debug("Size of Nominee List is  : " + nomineeList.size() );
		        logger.debug("Exit : public List getNomineeList(String memberId )");
		}catch (Exception ex){
			logger.error("Exception in getNomineeList : "+ex);
			
		}
		return nomineeList;
		
		
	}
    
    
    public List getFlatHistory(String aptID ){

		List memberHistoryList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry :  public List getFlatHistory(String aptID )");
		//1. SQL Query
		
       	
  strSQL= " SELECT a.apt_id,mobile, member_id ,TYPE,CONCAT(title,' ',full_name)AS fname,issue_date,expiry_date,membership_id "
	    + " FROM member_history m,apartment_details a	 "
	    + " WHERE  a.apt_id=m.apt_id  AND a.apt_id="+aptID+" ";
                  
		// 2. Parameters.
		Map Hmap = new HashMap();
		
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            InsertMemberVO members=new InsertMemberVO();
		            
		            members.setMemberId(rs.getString("member_id"));		           
		            members.setMobile(rs.getString("mobile"));
		            members.setFullName(rs.getString("fname"));
		            members.setType(rs.getString("type"));
		            members.setMembershipID(rs.getString("membership_id"));
		            members.setIssueDate(rs.getString("issue_date"));
		            members.setExpiryDate(rs.getString("expiry_date"));		    
		         		           
		            return members;
		        }};
	
		        memberHistoryList = namedParameterJdbcTemplate.query(strSQL,Hmap,RMapper);
		        
		        logger.debug("Size of memberList is  : " + memberHistoryList.size() );
		        logger.debug("Exit :  public List getIRegisterHistory(String aptID )");
		}catch (Exception ex){
			logger.error("Exception in getFlatHistory : "+ex);
			
		}
		return memberHistoryList;
		
		
	}
    public int getLatestMembershipNo(int societyID){
		int membershipNo=0;
		try
		{
			logger.debug("Entry :public int getLatestMembershipNo(int societyID)");
			
			String sqlQry=" SELECT membership_counter FROM society_settings WHERE society_id=:societyID" ;
					
			Map hMap=new HashMap();
			hMap.put("societyID", societyID);
			
			
			
				membershipNo=namedParameterJdbcTemplate.queryForObject(sqlQry, hMap, Integer.class);
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
			logger.error("Exception in getLatestMembershipNo : "+ex);
			
		}
			logger.debug("Exit : public int getLatestMembershipNo(int societyID)");
		return membershipNo;
		}
    
    public int getLatestNominationNo(int societyID){
		int nominationNo=0;
		try
		{
			logger.debug("Entry :public int getLatestNominationNo(int societyID)");
			
			String sqlQry=" SELECT nomination_counter FROM society_settings WHERE society_id=:societyID" ;
					
			Map hMap=new HashMap();
			hMap.put("societyID", societyID);
			
			
			
			nominationNo=namedParameterJdbcTemplate.queryForObject(sqlQry, hMap, Integer.class);
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
			logger.error("Exception in getLatestNominationNo : "+ex);
			
		}
			logger.debug("Exit : public int getLatestNominationNo(int societyID)");
		return nominationNo;
		}
    
    
    public int insertLedgerUserMapper(int userID,int ledgerID, String userType,int societyID)
	{	
		int insertFlag=0;
		try{
		
		logger.debug("Entry : public int insertLedgerUserMapper(int userID,int ledgerID, String userType)");
			SocietyVO societyVO=getSocietyDetails(societyID);
			String sql="INSERT INTO ledger_user_mapping_"+societyVO.getDataZoneID()+"(ledger_type,user_id,ledger_id,org_id) VALUES" +
					   "(:ledgerType, :userID, :ledgerID,:societyID); ";
			logger.debug("query : " + sql);
			
			Map namedParam = new HashMap();
			namedParam.put("ledgerType",userType);
			namedParam.put("userID",userID);
			namedParam.put("ledgerID",ledgerID);
			namedParam.put("societyID", societyVO.getSocietyID());
			
			
			insertFlag=namedParameterJdbcTemplate.update(sql,namedParam);
		
          			
		}
		
		catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in public int insertLedgerUserMapper(int userID,int ledgerID, String userType) : "+Ex);
		
		}
		logger.debug("Exit :public int insertLedgerUserMapper(int userID,int ledgerID, String userType)"+insertFlag);
	return insertFlag;	
	}
    
    
    public List getSocietyList(int userID){

		List societyList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry public List searchSocietyList" );
		//1. SQL Query
		if(userID==0){
		
		strSQL =" SELECT s.society_id,s.society_name,s.society_full_name,s.short_name,s.register_no,a.*,tx_close_date,ss.is_parent,ss.hub_org,ss.gst_as_income,ss.gst_as_expense FROM society_details s,society_address a,society_settings ss  WHERE s.society_id=a.society_id AND s.working=1 and ss.society_id=s.society_id; " ;
		}else{
		strSQL =" SELECT s.society_id,s.society_name,s.society_full_name,s.short_name,s.register_no,a.*,tx_close_datem,ss.is_parent,ss.hub_org,ss.gst_as_income,ss.gst_as_expense FROM society_details s,society_address a,user_society_mapping m,society_settings ss  WHERE s.society_id=a.society_id AND m.society_id=s.society_id AND ss.society_id=s.society_id AND m.user_id=:userID; ";	
		}
		logger.debug("query : " + strSQL);		
		// 2. Parameters.
		Map hsmap=new HashMap();
		hsmap.put("userID", userID);
		
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            SocietyVO soceityVo=new SocietyVO();
		            AddressVO addressVO=new AddressVO();
		            soceityVo.setSocietyID(rs.getInt("society_id"));
		            soceityVo.setSocFullName(rs.getString("society_full_name"));
		            soceityVo.setSocShortName(rs.getString("short_name"));
		            soceityVo.setSocietyRegNo(rs.getString("register_no"));
		            soceityVo.setMailListM(rs.getString("mailingList_member"));
		            soceityVo.setMailListA(rs.getString("mailingList_committee"));
		            soceityVo.setSocietyName(rs.getString("society_name"));
		            soceityVo.setTxCloseDate(rs.getString("tx_close_date"));
		            addressVO.setAddLine1(rs.getString("line_1"));
		            addressVO.setAddLine2(rs.getString("line_2"));
		            addressVO.setCity(rs.getString("city"));
		            addressVO.setZip(rs.getString("postal_code"));
		            addressVO.setState(rs.getString("state"));
		            soceityVo.setAddress(rs.getString("line_1")+", "+rs.getString("line_2")+", "+rs.getString("city")+", "+rs.getString("postal_code"));
		            soceityVo.setSocietyAddress(addressVO);
		            soceityVo.setIsParent(rs.getInt("is_parent"));
		            soceityVo.setIsHubOrg(rs.getInt("hub_org"));
		            soceityVo.setGstAsIncome(rs.getInt("gst_as_income"));
		            soceityVo.setGstAsExpense(rs.getInt("gst_as_expense"));
		             return soceityVo;
		            
		        }};
	
		        societyList = namedParameterJdbcTemplate.query(strSQL,hsmap,RMapper);
		        
	
		       
		        logger.info("Exit : public List searchSocietyList()"+societyList.size());
		}catch (Exception ex){
			logger.error("Exception in searchSocietyList : "+ex);
			
		}
		return societyList;
		
		
	}
    
    public List getSocietyListInDatazone(String dataZoneiD){

		List societyList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry  :  public List getSocietyListInDatazone(int dataZoneiD)" );
		//1. SQL Query
	
		
		strSQL =" SELECT s.society_id,s.society_name,s.society_full_name,s.short_name,s.register_no,a.*,tx_close_date,ss.is_parent,ss.hub_org,ss.gst_as_income,ss.gst_as_expense FROM society_details s,society_address a,society_settings ss  WHERE s.society_id=a.society_id AND s.working=1 and ss.society_id=s.society_id and ss.datazone_id=:datazoneID; " ;
	
		logger.debug("query : " + strSQL);		
		// 2. Parameters.
		Map hsmap=new HashMap();
		hsmap.put("datazoneID", dataZoneiD);
		
		
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            SocietyVO soceityVo=new SocietyVO();
		            AddressVO addressVO=new AddressVO();
		            soceityVo.setSocietyID(rs.getInt("society_id"));
		            soceityVo.setSocFullName(rs.getString("society_full_name"));
		            soceityVo.setSocShortName(rs.getString("short_name"));
		            soceityVo.setSocietyRegNo(rs.getString("register_no"));
		            soceityVo.setMailListM(rs.getString("mailingList_member"));
		            soceityVo.setMailListA(rs.getString("mailingList_committee"));
		            soceityVo.setSocietyName(rs.getString("society_name"));
		            soceityVo.setTxCloseDate(rs.getString("tx_close_date"));
		            addressVO.setAddLine1(rs.getString("line_1"));
		            addressVO.setAddLine2(rs.getString("line_2"));
		            addressVO.setCity(rs.getString("city"));
		            addressVO.setZip(rs.getString("postal_code"));
		            addressVO.setState(rs.getString("state"));
		            soceityVo.setAddress(rs.getString("line_1")+", "+rs.getString("line_2")+", "+rs.getString("city")+", "+rs.getString("postal_code"));
		            soceityVo.setSocietyAddress(addressVO);
		            soceityVo.setIsParent(rs.getInt("is_parent"));
		            soceityVo.setIsHubOrg(rs.getInt("hub_org"));
		            soceityVo.setGstAsIncome(rs.getInt("gst_as_income"));
		            soceityVo.setGstAsExpense(rs.getInt("gst_as_expense"));
		             return soceityVo;
		            
		        }};
	
		        societyList = namedParameterJdbcTemplate.query(strSQL,hsmap,RMapper);
		        
	
		       
		        logger.info("Exit :  public List getSocietyListInDatazone(int dataZoneiD)"+societyList.size());
		}catch (Exception ex){
			logger.error("Exception in  public List getSocietyListInDatazone(int dataZoneiD) : "+ex);
			
		}
		return societyList;
		
		
	}
    
    public List getSocietyListForUser(int userID, int appID){

		List societyList = null;
		String strSQL = "";
		try{	
		logger.debug("Entry public List getSocietyListForUser" );
		//1. SQL Query
		if(userID==0){
		
		strSQL =" SELECT s.society_id,s.society_name,s.society_full_name,s.short_name,s.register_no,a.*,tx_close_date,ss.is_parent, ss.hub_org FROM society_details s,society_address a,society_settings ss  WHERE s.society_id=a.society_id AND s.working=1 and ss.society_id=s.society_id; " ;
		}else{
		strSQL =" SELECT s.society_id,s.society_name,s.society_full_name,s.short_name,s.register_no,a.*,tx_close_date,ss.is_parent, ss.hub_org FROM society_details s,society_address a,um_user_app_role m,society_settings ss  WHERE s.society_id=a.society_id AND m.org_id=s.society_id AND ss.society_id=s.society_id AND m.user_id=:userID AND m.app_id=:appID; ";	
		}
		logger.debug("query : " + strSQL);		
		// 2. Parameters.
		Map hsmap=new HashMap();
		hsmap.put("userID", userID);
		hsmap.put("appID", appID);
		
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            SocietyVO soceityVo=new SocietyVO();
		            AddressVO addressVO=new AddressVO();
		            soceityVo.setSocietyID(rs.getInt("society_id"));
		            soceityVo.setSocFullName(rs.getString("society_full_name"));
		            soceityVo.setSocShortName(rs.getString("short_name"));
		            soceityVo.setSocietyRegNo(rs.getString("register_no"));
		            soceityVo.setMailListM(rs.getString("mailingList_member"));
		            soceityVo.setMailListA(rs.getString("mailingList_committee"));
		            soceityVo.setSocietyName(rs.getString("society_name"));
		            soceityVo.setTxCloseDate(rs.getString("tx_close_date"));
		            addressVO.setAddLine1(rs.getString("line_1"));
		            addressVO.setAddLine2(rs.getString("line_2"));
		            addressVO.setCity(rs.getString("city"));
		            addressVO.setZip(rs.getString("postal_code"));
		            addressVO.setState(rs.getString("state"));
		            soceityVo.setAddress(rs.getString("line_1")+", "+rs.getString("line_2")+", "+rs.getString("city")+", "+rs.getString("postal_code"));
		            soceityVo.setSocietyAddress(addressVO);
		            soceityVo.setIsParent(rs.getInt("is_parent"));
		            soceityVo.setIsHubOrg(rs.getInt("hub_org"));
		             return soceityVo;
		            
		        }};
	
		        societyList = namedParameterJdbcTemplate.query(strSQL,hsmap,RMapper);
		        
	
		       
		        logger.info("Exit : public List getSocietyListForUser()"+societyList.size());
		}catch (Exception ex){
			logger.error("Exception in getSocietyListForUser : "+ex);
			
		}
		return societyList;
		
		
	}
    
    public int updateLockDateForSociety(int societyID,String lockDate)
	{	
		int insertFlag=0;
		try{
		
		logger.debug("Entry :  public int updateLockDateForSociety(int societyID,String lockDate)");
		
		String sql="Update society_settings set tx_close_date=:lockDate where society_id=:societyID ";
		logger.debug("query : " + sql);

		Map namedParam = new HashMap();
		namedParam.put("societyID",societyID);
		namedParam.put("lockDate",lockDate);
		

		insertFlag=namedParameterJdbcTemplate.update(sql,namedParam);

		
         			
		}
		
		catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in  public int updateLockDateForSociety(int societyID,String lockDate) : "+Ex);
		
		}
		logger.debug("Exit : public int updateLockDateForSociety(int societyID,String lockDate)"+insertFlag);
	return insertFlag;	
	}
    
    public int updateLockDateForGSTTx(int societyID,String lockDate)
  	{	
  		int insertFlag=0;
  		try{
  		
  		logger.debug("Entry :  public int updateLockDateForGSTTx(int societyID,String lockDate)");
  		
  		String sql="Update society_settings set gst_close_date=:lockDate where society_id=:societyID ";
  		logger.debug("query : " + sql);

  		Map namedParam = new HashMap();
  		namedParam.put("societyID",societyID);
  		namedParam.put("lockDate",lockDate);
  		

  		insertFlag=namedParameterJdbcTemplate.update(sql,namedParam);

  		
           			
  		}
  		
  		catch(Exception Ex){
  			Ex.printStackTrace();
  			logger.error("Exception in  public int updateLockDateForGSTTx(int societyID,String lockDate) : "+Ex);
  		
  		}
  		logger.debug("Exit : public int updateLockDateForGSTTx(int societyID,String lockDate)"+insertFlag);
  	return insertFlag;	
  	}
    
    public int updateLockDateForAuditTx(int societyID,String lockDate)
  	{	
  		int insertFlag=0;
  		try{
  		
  		logger.debug("Entry :  public int updateLockDateForAuditTx(int societyID,String lockDate)");
  		
  		String sql="Update society_settings set audit_close_date=:lockDate where society_id=:societyID ";
  		logger.debug("query : " + sql);

  		Map namedParam = new HashMap();
  		namedParam.put("societyID",societyID);
  		namedParam.put("lockDate",lockDate);
  		

  		insertFlag=namedParameterJdbcTemplate.update(sql,namedParam);

  		
           			
  		}
  		
  		catch(Exception Ex){
  			Ex.printStackTrace();
  			logger.error("Exception in  public int updateLockDateForAuditTx(int societyID,String lockDate) : "+Ex);
  		
  		}
  		logger.debug("Exit : public int updateLockDateForAuditTx(int societyID,String lockDate)"+insertFlag);
  	return insertFlag;	
  	}
    
    public SocietyVO getSocietyDetails(int societyID){	
  	   SocietyVO societyVO=new SocietyVO();
  	 
 	  	try
 	  	{
 	  		logger.debug("Entry : public SocietyVO getSocietyDetails(String societyID)");
 	  		String sql="SELECT * FROM society_settings ss ,society_details sd, society_address sa WHERE ss.society_id=sa.society_id and ss.society_id=sd.society_id AND sd.society_id=:societyID";
 	  				
 	  		Map hashMap=new HashMap();
 	  		hashMap.put("societyID", societyID);
 	  		 RowMapper RMapper = new RowMapper() {
 			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
 			        	 SocietyVO societyVO=new SocietyVO(); 		
 			        	 AddressVO addressVO=new AddressVO();
 			             societyVO.setSocietyID(rs.getInt("sd.society_id"));
 			             societyVO.setSocietyName(rs.getString("society_name"));
 			             societyVO.setSocietyRegNo(rs.getString("register_no"));
 			             societyVO.setCounter(rs.getInt("counter"));
 			             societyVO.setMailListA(rs.getString("mailingList_committee"));
 			             societyVO.setMailListM(rs.getString("mailingList_member"));
 			             societyVO.setTxCloseDate(rs.getString("tx_close_date"));
 			             societyVO.setAddress(rs.getString("line_1")+", "+rs.getString("line_2")+", "+rs.getString("city")+", "+rs.getString("postal_code"));
 			             societyVO.setSocFullName(rs.getString("society_full_name"));
 			             societyVO.setOrgID(rs.getInt("org_id"));
 			             societyVO.setAuditCloseDate(rs.getString("audit_close_date"));
 			             societyVO.setGstCloseDate(rs.getString("gst_close_date"));
 			             societyVO.setNotes(rs.getString("notes"));
 			             societyVO.setServiceTax(rs.getBigDecimal("service_tax"));
 	 		             societyVO.setLogoUrl(rs.getString("logo_url"));
 			             societyVO.setTemplate(rs.getString("template"));
 			             societyVO.setGstIN(rs.getString("gstin"));
 			             societyVO.setServerName(rs.getString("server_name"));
 			             societyVO.setSchemaName(rs.getString("schema_name"));
 			             societyVO.setReminderToAll(rs.getInt("reminder_to_all"));
 			             societyVO.setStateName(rs.getString("state_of_supply"));
 			             societyVO.setTdsLedgerID(rs.getInt("tds_ledger_id"));
 			             societyVO.setDriveName(rs.getString("drive_name"));
 			             societyVO.setDataZoneID(rs.getString("datazone_id"));
 			            societyVO.setIsParent(rs.getInt("is_parent"));
 			            societyVO.setIsHubOrg(rs.getInt("hub_org"));
 			            societyVO.setGstAsIncome(rs.getInt("gst_as_income"));
 			            societyVO.setGstAsExpense(rs.getInt("gst_as_expense"));
 			            societyVO.setPanNo(rs.getString("pan"));
 			            societyVO.setTanNo(rs.getString("tan"));
 			            societyVO.setBillingFrequency(rs.getString("invoice_period"));
			            societyVO.setNextBillingDate(rs.getString("next_billing_date"));
 			            societyVO.setOrgType(rs.getString("org_type"));
 			            addressVO.setAddLine1(rs.getString("line_1"));
 			            addressVO.setAddLine2(rs.getString("line_2"));
 			            addressVO.setCity(rs.getString("city"));
 			            addressVO.setZip(rs.getString("postal_code"));
 			            addressVO.setState(rs.getString("state"));
 			            addressVO.setCountryCode(rs.getString("country_code"));
 			            societyVO.setSocietyAddress(addressVO);
 			            return societyVO;
 			        }};
 	  		   		
 	  		
 	  		societyVO= (SocietyVO) namedParameterJdbcTemplate.queryForObject(sql, hashMap, RMapper);
 	     		
 	  	}
 	  	catch(EmptyResultDataAccessException e){
 	  		logger.info("No society Details available"+e);
 	  	}
 	  	catch(NullPointerException Ex){
 	  		logger.info("No society details available : "+Ex);
 	  	}
 	  	catch(Exception Ex){
 	  		logger.error("Exception in public SocietyVO getSocietyDetails(String societyID) : "+Ex);
 	  	}
 	  	logger.debug("Exit :  public SocietyVO getSocietyDetails(String societyID)");
 	 
     return societyVO;	
     }
    
    public SocietyVO getOrgDetailsByOrgName(String orgName){	
   	   SocietyVO societyVO=new SocietyVO();
   	 
  	  	try
  	  	{
  	  		logger.debug("Entry : public SocietyVO getSocietyDetails(String societyID)");
  	  		String sql="SELECT * FROM society_settings ss ,society_details sd WHERE  ss.society_id=sd.society_id AND sd.society_name=:orgName";
  	  				
  	  		Map hashMap=new HashMap();
  	  		hashMap.put("orgName", orgName);
  	  		 RowMapper RMapper = new RowMapper() {
  			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
  			        	 SocietyVO societyVO=new SocietyVO(); 			             
  			             societyVO.setSocietyID(rs.getInt("sd.society_id"));
  			             societyVO.setSocietyName(rs.getString("society_name"));
  			             societyVO.setSocietyRegNo(rs.getString("register_no"));
  			             societyVO.setCounter(rs.getInt("counter"));
  			             societyVO.setMailListA(rs.getString("mailingList_committee"));
  			             societyVO.setTxCloseDate(rs.getString("tx_close_date"));
  			             societyVO.setSocFullName(rs.getString("society_full_name"));
  			             societyVO.setOrgID(rs.getInt("org_id"));
  			             societyVO.setAuditCloseDate(rs.getString("audit_close_date"));
  			             societyVO.setGstCloseDate(rs.getString("gst_close_date"));
  			             societyVO.setNotes(rs.getString("notes"));
  			             societyVO.setServiceTax(rs.getBigDecimal("service_tax"));
  	 		             societyVO.setLogoUrl(rs.getString("logo_url"));
  			             societyVO.setTemplate(rs.getString("template"));
  			             societyVO.setGstIN(rs.getString("gstin"));
  			             societyVO.setServerName(rs.getString("server_name"));
  			             societyVO.setSchemaName(rs.getString("schema_name"));
  			             societyVO.setReminderToAll(rs.getInt("reminder_to_all"));
  			             societyVO.setStateName(rs.getString("state_of_supply"));
  			             societyVO.setTdsLedgerID(rs.getInt("tds_ledger_id"));
  			             societyVO.setDriveName(rs.getString("drive_name"));
  			             societyVO.setDataZoneID(rs.getString("datazone_id"));
  			            societyVO.setIsParent(rs.getInt("is_parent"));
  			            societyVO.setIsHubOrg(rs.getInt("hub_org"));
  			            societyVO.setGstAsIncome(rs.getInt("gst_as_income"));
  			            societyVO.setGstAsExpense(rs.getInt("gst_as_expense"));
  			            return societyVO;
  			        }};
  	  		   		
  	  		
  	  		societyVO= (SocietyVO) namedParameterJdbcTemplate.queryForObject(sql, hashMap, RMapper);
  	     		
  	  	}
  	  	catch(EmptyResultDataAccessException e){
  	  		logger.info("No society Details available"+e);
  	  	}
  	  	catch(NullPointerException Ex){
  	  		logger.info("No society details available : "+Ex);
  	  	}
  	  	catch(Exception Ex){
  	  		logger.error("Exception in public SocietyVO getSocietyDetails(String societyID) : "+Ex);
  	  	}
  	  	logger.debug("Exit :  public SocietyVO getSocietyDetails(String societyID)");
  	 
      return societyVO;	
      }
    
     public List getSubscriptionExpiringOrgList(String appID,int period){	
     	  List orgList=null;
     	 
    	  	try
    	  	{
    	  		logger.debug("Entry : public List getSubscriptionExpiringOrgList(int appID,int period)");
    	  		String sql="SELECT * FROM society_settings ss ,society_details sd, society_address sa,org_app_subscription o WHERE ss.society_id=o.org_id AND o.app_id=:appID AND ss.society_id=sa.society_id AND ss.society_id=sd.society_id  AND DATE(expired_date)=(CURDATE()+INTERVAL :period DAY) and o.subscription_id=0;";
    	  				
    	  		Map hashMap=new HashMap();
    	  		hashMap.put("appID", appID);
    	  		hashMap.put("period", period);
    	  		 RowMapper RMapper = new RowMapper() {
    			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    			        	 SocietyVO societyVO=new SocietyVO(); 			             
   			             societyVO.setSocietyID(rs.getInt("sd.society_id"));
   			             societyVO.setSocietyName(rs.getString("society_name"));
   			             societyVO.setSocietyRegNo(rs.getString("register_no"));
   			             societyVO.setCounter(rs.getInt("counter"));
   			             societyVO.setMailListA(rs.getString("email_address"));
   			             societyVO.setTxCloseDate(rs.getString("tx_close_date"));
   			             societyVO.setAddress(rs.getString("line_1")+", "+rs.getString("line_2")+", "+rs.getString("city")+", "+rs.getString("postal_code"));
   			             societyVO.setSocFullName(rs.getString("society_full_name"));
   			             societyVO.setOrgID(rs.getInt("org_id"));
   			             societyVO.setAuditCloseDate(rs.getString("audit_close_date"));
   			             societyVO.setGstCloseDate(rs.getString("gst_close_date"));
   			             societyVO.setNotes(rs.getString("notes"));
   			             societyVO.setServiceTax(rs.getBigDecimal("service_tax"));
   	 		             societyVO.setLogoUrl(rs.getString("logo_url"));
   			             societyVO.setTemplate(rs.getString("template"));
   			             societyVO.setGstIN(rs.getString("gstin"));
   			             societyVO.setServerName(rs.getString("server_name"));
   			             societyVO.setSchemaName(rs.getString("schema_name"));
   			             societyVO.setReminderToAll(rs.getInt("reminder_to_all"));
   			             societyVO.setStateName(rs.getString("state_of_supply"));
   			             societyVO.setTdsLedgerID(rs.getInt("tds_ledger_id"));
   			             societyVO.setDriveName(rs.getString("drive_name"));
   			             societyVO.setDataZoneID(rs.getString("datazone_id"));	             
    			             societyVO.setAppSubscriptionValidity(rs.getTimestamp("expired_date"));
    			            return societyVO;
    			        }};
    	  		   		
    	  		
    	  		orgList= namedParameterJdbcTemplate.query(sql, hashMap, RMapper);
    	     		
    	  	}
    	  	catch(EmptyResultDataAccessException e){
    	  		logger.info("No society Details available"+e);
    	  	}
    	  	catch(NullPointerException Ex){
    	  		logger.info("No society details available : "+Ex);
    	  	}
    	  	catch(Exception Ex){
    	  		logger.error("Exception in public List getSubscriptionExpiringOrgList(int appID,int period) : "+Ex);
    	  	}
    	  	logger.debug("Exit :  public List getSubscriptionExpiringOrgList(int appID,int period)");
    	 
        return orgList;	
        }
       
    public List getSubscriptionExpiredOrgList(int appID,int gracePeriod){	
     	  List orgList=null;
     	  String condition="";
     	  
    	  	try
    	  	{
    	  		logger.debug("Entry : public List getSubscriptionExpiredOrgList(int appID)");
    	  		if(gracePeriod==0){
    	  			condition="(CURDATE()-1)";
    	  		}else if(gracePeriod==1){
    	  			condition="(CURDATE())";
    	  		}
    	  		
    	  		String sql="SELECT * FROM society_settings ss ,society_details sd, society_address sa,org_app_subscription o WHERE ss.society_id=o.org_id AND (o.app_id=2 OR o.app_id=4) AND ss.society_id=sa.society_id AND ss.society_id=sd.society_id  AND (DATE(expired_date) BETWEEN (CURDATE()-INTERVAL 30 DAY) AND "+condition+") AND o.subscription_id=:gracePeriod ;";
    	  				
    	  		Map hashMap=new HashMap();
    	  		hashMap.put("appID", appID);
    	  		hashMap.put("gracePeriod",gracePeriod);
    	  		
    	  		 RowMapper RMapper = new RowMapper() {
    			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    			        	 SocietyVO societyVO=new SocietyVO(); 			             
   			             societyVO.setSocietyID(rs.getInt("sd.society_id"));
   			             societyVO.setSocietyName(rs.getString("society_name"));
   			             societyVO.setSocietyRegNo(rs.getString("register_no"));
   			             societyVO.setCounter(rs.getInt("counter"));
   			             societyVO.setMailListA(rs.getString("email_address"));
   			             societyVO.setTxCloseDate(rs.getString("tx_close_date"));
   			             societyVO.setAddress(rs.getString("line_1")+", "+rs.getString("line_2")+", "+rs.getString("city")+", "+rs.getString("postal_code"));
   			             societyVO.setSocFullName(rs.getString("society_full_name"));
   			             societyVO.setOrgID(rs.getInt("org_id"));
   			             societyVO.setAuditCloseDate(rs.getString("audit_close_date"));
   			             societyVO.setGstCloseDate(rs.getString("gst_close_date"));
   			             societyVO.setNotes(rs.getString("notes"));
   			             societyVO.setServiceTax(rs.getBigDecimal("service_tax"));
   	 		             societyVO.setLogoUrl(rs.getString("logo_url"));
   			             societyVO.setTemplate(rs.getString("template"));
   			             societyVO.setGstIN(rs.getString("gstin"));
   			             societyVO.setServerName(rs.getString("server_name"));
   			             societyVO.setSchemaName(rs.getString("schema_name"));
   			             societyVO.setReminderToAll(rs.getInt("reminder_to_all"));
   			             societyVO.setStateName(rs.getString("state_of_supply"));
   			             societyVO.setTdsLedgerID(rs.getInt("tds_ledger_id"));
   			             societyVO.setDriveName(rs.getString("drive_name"));
   			             societyVO.setDataZoneID(rs.getString("datazone_id"));	             
    			             societyVO.setAppSubscriptionValidity(rs.getTimestamp("expired_date"));
    			            return societyVO;
    			        }};
    	  		   		
    	  		
    	  		orgList= namedParameterJdbcTemplate.query(sql, hashMap, RMapper);
    	     		
    	  	}
    	  	catch(EmptyResultDataAccessException e){
    	  		logger.info("No society Details available"+e);
    	  	}
    	  	catch(NullPointerException Ex){
    	  		logger.info("No society details available : "+Ex);
    	  	}
    	  	catch(Exception Ex){
    	  		logger.error("Exception in public List getSubscriptionExpiredOrgList(int appID) : "+Ex);
    	  	}
    	  	logger.debug("Exit :  public List getSubscriptionExpiredOrgList(int appID)");
    	 
        return orgList;	
        }
   
    public void createAccLdgrTable(int orgID)  {
  		logger.debug("Entry : public void createAccLdgrTable(int orgID)");
  		try{
    	String query=conf.getPropertiesValue("dbTable.acountLedger");
    	query=query.replace("$orgID", ""+orgID);
    	
         jdbcTemplate.execute(query);
        
  		}catch(Exception e){
  			logger.error("Excepion in createAccLdgrTable(int orgID) for Org ID"+orgID+"  "+e);
  		}
	  		logger.debug("Exit : public void createAccLdgrTable(int orgID)");

    }
    
    public void createAccTransactionsTable(int orgID)  {
  		logger.debug("Entry : public void createAccTransactionsTable(int orgID)");
  		try{
    	String query=conf.getPropertiesValue("dbTable.accountTransaction");
    	query=query.replace("$orgID", ""+orgID);
    	
         jdbcTemplate.execute(query);
        
  		}catch(Exception e){
  			logger.error("Excepion in createAccTransactionsTable(int orgID) for Org ID"+orgID+"  "+e);
  		}
	  		logger.debug("Exit : public void createAccTransactionsTable(int orgID)");

    }
    
    public void createAccLedgerEntryTable(int orgID)  {
  		logger.debug("Entry : public void createAccLedgerEntryTable(int orgID)");
  		try{
    	String query=conf.getPropertiesValue("dbTable.accountLedgerEntry");
    	query=query.replace("$orgID", ""+orgID);
    	
         jdbcTemplate.execute(query);
        
  		}catch(Exception e){
  			logger.error("Excepion in createAccLedgerEntryTable(int orgID) for Org ID"+orgID+"  "+e);
  		}
	  		logger.debug("Exit : public void createAccLedgerEntryTable(int orgID)");

    }
    
    public void createAccLedgerMappingTable(int orgID)  {
  		logger.debug("Entry : public void createAccLedgerMappingTable(int orgID)");
  		try{
    	String query=conf.getPropertiesValue("dbTable.ledgerMapping");
    	query=query.replace("$orgID", ""+orgID);
    	
         jdbcTemplate.execute(query);
        
  		}catch(Exception e){
  			logger.error("Excepion in createAccLedgerMappingTable(int orgID) for Org ID"+orgID+"  "+e);
  		}
	  		logger.debug("Exit : public void createAccLedgerMappingTable(int orgID)");

    }
    
    public void createReceiptInvoiceMappingTable(int orgID)  {
  		logger.debug("Entry : public void createReceiptInvoiceMappingTable(int orgID)");
  		try{
    	String query=conf.getPropertiesValue("dbTable.receiptInvoiceMapping");
    	query=query.replace("$orgID", ""+orgID);
    	
         jdbcTemplate.execute(query);
        
  		}catch(Exception e){
  			logger.error("Excepion in createReceiptInvoiceMappingTable(int orgID) for Org ID"+orgID+"  "+e);
  		}
	  		logger.debug("Exit : public void createReceiptInvoiceMappingTable(int orgID)");

    }
    
    public void createSampleLedgers(int orgID,String orgType)  {
  		logger.debug("Entry : public void createSampleLedgers(int orgID,String orgType)");
  		try{
   		SocietyVO societyVO=getSocietyDetails(orgID);
  		java.sql.Date curdate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
  		String query="";
  		if(orgType.equalsIgnoreCase("C")){
    	query=conf.getPropertiesValue("dbTable.ledgersForCompany");
  		}else if(orgType.equalsIgnoreCase("S")){
  		query=conf.getPropertiesValue("dbTable.ledgersForSoc");
  		}
    	query=query.replace("$orgID", ""+orgID);
    	query=query.replace("$dataZoneID", societyVO.getDataZoneID());
    	query=query.replace("$curdate",""+curdate);
    	
         jdbcTemplate.execute(query);
        
  		}catch(Exception e){
  			logger.error("Excepion in createSampleLedgers(int orgID,String orgType) for Org ID"+orgID+"  "+e);
  		}
	  		logger.debug("Exit : public void createSampleLedgers(int orgID,String orgType)"+orgID);

    }
    
    public List getFinancialYearListForOrg(int orgID){	
	   	  List fyList=new ArrayList();
	   	 
	  	  	try
	  	  	{
	  	  		logger.debug("Entry : public List getFinancialYearListForOrg(int orgID)");
	  	  	 	  		   		
	  	  	String sql="SELECT f.*,s.society_id FROM society_settings s,fy_master_details f WHERE s.society_id=:orgID AND s.close_fy_id<=f.id ORDER BY start_date;";
				
	  		Map hashMap=new HashMap();
	  		hashMap.put("orgID", orgID);
	  			  		
	  		 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			        	 FinancialYearVO fyVO=new FinancialYearVO(); 			             
			             fyVO.setFyID(rs.getInt("id"));
			             fyVO.setFyName(rs.getString("name"));
			             fyVO.setStartDate(rs.getString("start_date"));
			             fyVO.setEndDate(rs.getString("end_date"));
			             fyVO.setCountryCode(rs.getString("country_code"));
			             fyVO.setOrgID(rs.getInt("society_id"));
			            return fyVO;
			        }};
	  		   		
	  		
	  		fyList= namedParameterJdbcTemplate.query(sql, hashMap, RMapper);
	  	     		
	  	  	}
	  	   	catch(Exception Ex){
	  	  		logger.error("Exception in public List getFinancialYearListForOrg(int orgID): "+Ex);
	  	  	}
	  	  	logger.debug("Exit :  public List getFinancialYearListForOrg(int orgID)");
	  	 
	      return fyList;	
	}
  
  /* Get list of all penalty charges for the given society */
	public List getPenaltyCharges(int societyID) {
		logger.debug("Entry : public List getApplicablePenaltyCharges(int societyID)");
		List chargeList = null;
		
		
		
		
		
		try {
			String sql = "SELECT m.*,s.precision ,s.gstin"
					+ " FROM penalty_charge_details m ,society_settings s WHERE  m.society_id=s.society_id AND (m.society_id=:societyID )" +
							" AND m.status='Active'" ;
		logger.debug(sql+"  "+societyID);
			Map hashMap = new HashMap();

			hashMap.put("societyID", societyID);
			
			
			
			

			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					PenaltyChargesVO chargeVO = new PenaltyChargesVO();
					chargeVO.setPenaltyChargeID(rs.getInt("id"));
					chargeVO.setCalMethod(rs.getString("cal_method"));
					chargeVO.setCalMode(rs.getString("cal_mode"));
					chargeVO.setLedgerID(rs.getInt("ledger_id"));					
					chargeVO.setAmount(rs.getBigDecimal("amount"));					
					chargeVO.setStartDate(rs.getString("start_date"));
					chargeVO.setEndDate(rs.getString("end_date"));
					chargeVO.setChargeTypeID(rs.getInt("charge_type_id"));
					chargeVO.setDescription(rs.getString("description"));
					chargeVO.setNextChargeDate(rs.getString("next_charge_date"));
					chargeVO.setPrevChargeDate(rs.getString("prev_charge_date"));
					chargeVO.setPenaltyChargeDate(rs.getString("penalty_charge_date"));
					chargeVO.setChargeFrequency(rs.getString("charge_frequency"));
					chargeVO.setCreateInvoices(rs.getInt("generate_invoice"));
					chargeVO.setTxDate(rs.getString("tx_date"));
					chargeVO.setPrecison(rs.getInt("precision"));
					chargeVO.setItemID(rs.getInt("item_id"));
					chargeVO.setSocGstIn(rs.getString("gstin"));
					chargeVO.setIsApplicable(false);
					
					return chargeVO;
				}
			};
			chargeList = namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
		} catch (EmptyResultDataAccessException e) {
			logger.info("No Penalty charges available for Society "+societyID+"  " + e);
		
		
		} catch (Exception e) {
			logger.error("Exception at public List getApplicablePenaltyCharges(int societyID) " + e);
		}
		logger.debug("Exit : public List getApplicablePenaltyCharges(int societyID)"+chargeList.size());
		return chargeList;
	}
  
	/* Get list of all applicable fees for the given member */
	public List getApplicableCharges(int societyID) {
		logger.debug("Entry : public List List getApplicableCharges(int societyID,String fromDate,String toDate,int chargeTypeID)");
		List chargeList = null;
		String strCondition="";
		
		
		
		
		try {
			String sql = "SELECT m.*,i.invoice_type AS chargeType,s.precision ,s.gstin "
					+ " FROM scheduled_charges m,in_invoice_type_details i ,society_settings s WHERE m.charge_type_id=i.id  AND m.org_id=s.society_id AND (m.org_id=:societyID )" +
							" AND m.status='Active' ORDER BY charge_sequence; ";
			
			Map hashMap = new HashMap();

			hashMap.put("societyID", societyID);
			
			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					ChargesVO chargeVO = new ChargesVO();
					chargeVO.setChargeID(rs.getInt("id"));
					chargeVO.setCalMethod(rs.getString("cal_method"));
					chargeVO.setGroupName(rs.getString("group_name"));
					chargeVO.setGroupTypeID(rs.getString("group_type_id"));
					chargeVO.setDebitLedgerID(rs.getInt("debit_ledger_id"));
					chargeVO.setCreditLedgerID(rs.getInt("credit_ledger_id"));
					chargeVO.setAmount(rs.getBigDecimal("amount"));
					chargeVO.setChargeSequence(rs.getInt("charge_sequence"));
					chargeVO.setStartDate(rs.getString("start_date"));
					chargeVO.setEndDate(rs.getString("end_date"));
					chargeVO.setChargeType(rs.getString("chargeType"));
					chargeVO.setChargeTypeID(rs.getInt("charge_type_id"));
					chargeVO.setDescription(rs.getString("description"));
					chargeVO.setNextChargeDate(rs.getString("next_charge_date"));
					chargeVO.setTxDate(rs.getString("tx_date"));
					chargeVO.setChargeFrequency(rs.getString("charge_frequency"));
					chargeVO.setCreateInvoices(rs.getInt("generate_invoice"));
					chargeVO.setPrecison(rs.getInt("precision"));
					chargeVO.setItemID(rs.getInt("item_id"));
					chargeVO.setGstRate(rs.getBigDecimal("gst"));
					chargeVO.setSocGstIN(rs.getString("gstin"));
					chargeVO.setIsApplicable(false);
					
					return chargeVO;
				}
			};
			chargeList = namedParameterJdbcTemplate.query(sql, hashMap,rMapper);

		} catch (Exception e) {
			logger.error("Exception at List getApplicableCharges(int societyID,String fromDate,String toDate,int chargeTypeID) " + e);
		}
		logger.debug("Exit : public List List getApplicableCharges(int societyID,String fromDate,String toDate,int chargeTypeID))"+chargeList.size());
		return chargeList;
	}

    
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

}
