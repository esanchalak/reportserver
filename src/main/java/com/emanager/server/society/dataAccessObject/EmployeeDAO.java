package com.emanager.server.society.dataAccessObject;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.emanager.server.commonUtils.valueObject.DropDownVO;
import com.emanager.server.society.valueObject.EmergencyVO;
import com.emanager.server.society.valueObject.EmployeeVO;

public class EmployeeDAO {

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	  private static final Logger logger = Logger.getLogger(EmployeeDAO.class);

	public List searchEmployees(String strSocietyID) {

		List employeeList = null;

		try {
			logger.debug("Entry : public List searchEmployees(String strSocietyID)");
			//1. SQL Query
			String strSQL = "SELECT employee_details.*,vendor_name FROM employee_details,employee_vendor WHERE employee_details.vendor_id=employee_vendor.vendor_id AND society_id= :society_ID ";
			logger.debug("query : " + strSQL);
			// 2. Parameters.
			SqlParameterSource namedParameters = new MapSqlParameterSource(
					"society_ID", strSocietyID);

			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					EmployeeVO employee = new EmployeeVO();
					employee.setEmployeeId(rs.getString("emp_id"));
					employee.setTitle(rs.getString("title"));
					employee.setFirstName(rs.getString("first_name"));
					employee.setMiddleName(rs.getString("middle_name"));
					employee.setLastName(rs.getString("last_name"));
					employee.setDesignation(rs.getString("designation"));
					employee.setDescription(rs.getString("verification_details"));
					employee.setJoingDate(rs.getString("join_date"));
					employee.setLeavingDate(rs.getString("leave_date"));
					employee.setSalary(rs.getBigDecimal("salary"));
					employee.setVendorID(rs.getString("vendor_id"));
					employee.setVendorName(rs.getString("vendor_name"));
					
					employee.setFullName(rs.getString("full_name"));
					
					return employee;
				}
			};

			employeeList = namedParameterJdbcTemplate.query(strSQL,namedParameters, RMapper);

			logger.debug("outside query : " + employeeList.size());
			logger.debug("Exit : public List searchEmployees(String strSocietyID)");
		} catch (Exception ex) {
			logger.error("Exception in searchEmployees :"+ex);

		}
		return employeeList;

	}
	public int addEmployeeDetails(EmployeeVO employeeVO)
	{	
		int insertFlag=0;
		try
		{
			logger.debug("Entry : public int addEmployeeDetails(EmployeeVO employeeVO)");
			
			String sql="insert into employee_details (society_id,title,first_name,last_name,designation,salary,middle_name,join_date,leave_date,verification_details,vendor_id,full_name)" +
					"values(:soc_id,:title,:first_name,:last_name,:designation,:salary,:middle_name,:join_date,:leave_date,:verification_details,:vendorID,:fullName)";
			logger.debug("query : " + sql);
			
			Map hashMap=new HashMap();
			hashMap.put("soc_id",employeeVO.getSocietyID());
			hashMap.put("title",employeeVO.getTitle());
			hashMap.put("first_name",employeeVO.getFirstName());
			hashMap.put("last_name",employeeVO.getLastName());
			hashMap.put("designation",employeeVO.getDesignation());
			hashMap.put("salary",employeeVO.getSalary());
			hashMap.put("middle_name",employeeVO.getMiddleName());
			hashMap.put("join_date",employeeVO.getJoingDate());
			hashMap.put("leave_date",employeeVO.getLeavingDate());
			hashMap.put("verification_details",employeeVO.getDescription());
			hashMap.put("vendorID",employeeVO.getVendorID());
			hashMap.put("fullName", employeeVO.getFullName());
			
			
			insertFlag=namedParameterJdbcTemplate.update(sql,hashMap);
				
		}
		catch(Exception Ex)
		{
			Ex.printStackTrace();
			logger.error("Exception in addEmployeeDetails : "+Ex);
		
		}
		logger.debug("Exit : public int addEmployeeDetails(EmployeeVO employeeVO)");
	return insertFlag;	
	}
	
	public int updateEmployeeDetails(EmployeeVO employeeVO,String employeeID,String societyID)
	{
		int insertFlag=0;
		try
		{
			logger.debug("Entry : public int updateEmployeeDetails(EmployeeVO employeeVO,String employeeID,String societyID)");
			
			String sql=" update employee_details SET title=:title,first_name=:f_name,middle_name=:m_name," +
					" last_name=:l_name,designation=:desig,salary=:sal,join_date=:j_date,leave_date=:l_date,verification_details=:veri_details,vendor_id=:vendorID , full_name=:fullName" +
					"  where emp_id="+employeeID+ " AND "+ " society_id="+societyID+";";
			logger.debug("query : " + sql);
						
			Map hashmap=new HashMap();
			hashmap.put("title",employeeVO.getTitle());
			hashmap.put("f_name",employeeVO.getFirstName());
			hashmap.put("m_name",employeeVO.getMiddleName());
			hashmap.put("l_name",employeeVO.getLastName());			
			hashmap.put("desig",employeeVO.getDesignation());
			hashmap.put("sal",employeeVO.getSalary());
			hashmap.put("j_date",employeeVO.getJoingDate());
			hashmap.put("l_date",employeeVO.getLeavingDate());
		    hashmap.put("veri_details",employeeVO.getDescription());
		    hashmap.put("vendorID", employeeVO.getVendorID());
		    hashmap.put("fullName", employeeVO.getFullName());
			
			insertFlag=namedParameterJdbcTemplate.update(sql, hashmap);
		}
		catch(Exception ex)
		{
			logger.error("Exception in updateEmployeeDetails : "+ex);
			
		}
		logger.debug("Exit : public int updateEmployeeDetails(EmployeeVO employeeVO,String employeeID,String societyID)"+insertFlag);
		return insertFlag;
	}
	
	public List getEmployeeSalaryList(String strSocietyID,Date fromDate,Date uptoDate) {
		List employeeList = null;
		
		try {
			logger.debug("Entry : public List getEmployeeSalaryList(String strSocietyID)");
			//1. SQL Query
			String strSQL = "SELECT employee_details.*,employee_salary_setting.*,employee_attendance_records.*,vendor_name, SUM(IF(on_leave= '0', is_present, 0)) AS present_days,SUM(is_late) AS late_days,SUM(on_leave) AS LEAVES" +
					" FROM employee_details,employee_salary_setting,employee_attendance_records,employee_vendor" +
					" WHERE employee_attendance_records.employee_id=employee_salary_setting.employee_id" +
					" AND employee_salary_setting.employee_id=employee_details.emp_id " +
					" AND employee_details.vendor_id=employee_vendor.vendor_id" +
					" AND (present_date BETWEEN :fromDate AND :uptoDate)" +
					" AND employee_details.society_id=:society_ID GROUP BY emp_id" ;
			logger.debug("query : " + strSQL);
			// 2. Parameters.
			logger.info(fromDate+" --Date--> "+uptoDate);
			Map keyMap=new HashMap();
			keyMap.put("society_ID", strSocietyID);
			
			keyMap.put("fromDate", fromDate);
			keyMap.put("uptoDate", uptoDate);

			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					EmployeeVO employee = new EmployeeVO();
					employee.setEmployeeId(rs.getString("emp_id"));
					employee.setTitle(rs.getString("title"));
					employee.setPresentDays(rs.getInt("present_days"));
					employee.setLateMarks(rs.getInt("late_mark"));
					employee.setLateDays(rs.getInt("late_days"));
					employee.setDesignation(rs.getString("designation"));
					employee.setDescription(rs.getString("verification_details"));
					employee.setJoingDate(rs.getString("join_date"));
					employee.setLeavingDate(rs.getString("leave_date"));
					employee.setSalary(rs.getBigDecimal("salary"));
					employee.setSalaryPerDay(rs.getBigDecimal("salary_per_day"));
					employee.setFullName(rs.getString("full_name"));
					employee.setAvailableLeaves(rs.getInt("leave_limit"));
					employee.setTakenLeaves(rs.getInt("leaves"));
					employee.setVendorName(rs.getString("vendor_name"));
					return employee;
				}
			};

			employeeList = namedParameterJdbcTemplate.query(strSQL,keyMap, RMapper);

			logger.debug("outside query : " + employeeList.size());
			logger.debug("Exit : public List getEmployeeSalaryList(String strSocietyID)");
		} catch (Exception ex) {
			logger.error("Exception in getEmployeeSalaryList :"+ex);

		}
		return employeeList;

	}
	
	
	
	public List getEmployeeWiseDetailsList(String employeeID,Date fromDate,Date uptoDate){
		List employeeList=null;
		try{
		logger.debug("Entry : public List getEmployeeWiseDetailsList(String strSocietyID,int month,int year)");
		String strSQL = "SELECT employee_details.*,employee_salary_setting.*,employee_attendance_records.*" +
		" FROM employee_details,employee_salary_setting,employee_attendance_records" +
		" WHERE employee_attendance_records.employee_id=employee_salary_setting.employee_id" +
		" AND employee_salary_setting.employee_id=employee_details.emp_id " +
		" AND (present_date BETWEEN :fromDate AND :uptoDate)" +
		" AND emp_id=:employeeID ORDER BY present_date" ;
		logger.debug("query : " + strSQL);
		// 2. Parameters.
		Map keyMap=new HashMap();
		keyMap.put("employeeID", employeeID);

		keyMap.put("fromDate", fromDate);
		keyMap.put("uptoDate", uptoDate);

		//3. RowMapper.
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
			throws SQLException {
		EmployeeVO employee = new EmployeeVO();
		employee.setEmployeeId(rs.getString("emp_id"));
		employee.setTitle(rs.getString("title"));
		employee.setPresentDate(rs.getString("present_date"));
		//employee.setLateMarks(rs.getInt("is_late"));
		//employee.setLateDays(rs.getInt("late_days"));
		employee.setDesignation(rs.getString("designation"));
		employee.setDescription(rs.getString("verification_details"));
		employee.setJoingDate(rs.getString("join_date"));
		if(rs.getInt("on_leave")==1){
			employee.setInTime(" - ");
			employee.setOutTime(" - ");
		}else{
		employee.setInTime(rs.getString("in_time"));
		employee.setOutTime(rs.getString("out_time"));
		}
		employee.setLeavingDate(rs.getString("leave_date"));
		employee.setSalary(rs.getBigDecimal("salary"));
		employee.setSalaryPerDay(rs.getBigDecimal("salary_per_day"));
		employee.setFullName(rs.getString("full_name"));
		employee.setAvailableLeaves(rs.getInt("leave_limit"));
		employee.setTakenLeaves(rs.getInt("on_leave"));
		employee.setLateMinutes(rs.getInt("late_minutes"));
		employee.setStartTime(rs.getInt("start_time"));
		employee.setWorkingHours(rs.getInt("working_hours"));
		return employee;
		
	}
		};

		employeeList = namedParameterJdbcTemplate.query(strSQL,keyMap, RMapper);

		logger.debug("outside query : " + employeeList.size());
		logger.debug("Exit : public List getEmployeeSalaryList(String strSocietyID)");
		} catch (Exception ex) {
			logger.error("Exception in getEmployeeSalaryList :"+ex);

		}
		return employeeList;

	}
	
	
	public List searchEmployers(){
		
		List listEmployers = null;
		String strSQL = "";
		
		try{	
			logger.debug("Entry : public List searchEmployers()" );
		//1. SQL Query
		strSQL = "SELECT * FROM employee_vendor;";
		
		logger.debug("query : " + strSQL);
		// 2. Parameters.
		 SqlParameterSource namedParameters = new MapSqlParameterSource();
       
		//3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		            DropDownVO membrs = new DropDownVO();
		            membrs.setData(rs.getString("vendor_id"));
		            membrs.setLabel(rs.getString("vendor_name"));
		            
		            return membrs;
		        }};
	
		        listEmployers = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
		        
	
		        logger.debug("outside query : " + listEmployers.size() );
		        logger.debug("Exit : public List searchMembers(String strSocietyID,String lastName)");
		}catch (Exception ex){
			logger.error("Exception in searchMembers(lastName) : "+ex);
			
		}
		return listEmployers;
		
	}
	public List getEmergencyNoList(String strSocietyID) {

		List emergencyList = null;

		try {
			logger.debug("Entry : public List getEmergencyNoList(String strSocietyID)");
			//1. SQL Query
			String strSQL = "(SELECT * FROM emergency_contact_no WHERE emergency_id = 0 LIMIT 1)"
                           + "UNION"
                           +"(SELECT * FROM emergency_contact_no WHERE society_id=:society_ID ORDER BY emergency_id);";
			logger.debug("query : " + strSQL);
			// 2. Parameters.
			SqlParameterSource namedParameters = new MapSqlParameterSource(
					"society_ID", strSocietyID);

			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					EmergencyVO emergency = new EmergencyVO();
					emergency.setEmergencyId(rs.getString("emergency_id"));
					emergency.setSocietyID(rs.getString("society_id"));
					emergency.setType(rs.getString("type"));
					emergency.setMobile(rs.getString("mobile"));
					emergency.setLandline(rs.getString("landline_no"));
				
					
					return emergency;
				}
			};

			emergencyList = namedParameterJdbcTemplate.query(strSQL,namedParameters, RMapper);

			logger.debug("outside query : " + emergencyList.size());
			logger.debug("Exit : public List getEmergencyNoList(String strSocietyID)");
		} catch (Exception ex) {
			logger.error("Exception in getEmergencyNoList :"+ex);

		}
		return emergencyList;

	}
	public int addEmergencyContact(EmergencyVO emergencyVO)
	{	
		int insertFlag=0;
		try
		{
			logger.debug("Entry : public int addEmergencyContact(EmergencyVO emergencyVO)");
			
			String sql="insert into emergency_contact_no (society_id,type,mobile,landline_no)values(:societyId,:type,:mobile,:landline)";
			logger.debug("query : " + sql);
			
			Map hashMap=new HashMap();
			hashMap.put("societyId",emergencyVO.getSocietyID());
			hashMap.put("type",emergencyVO.getType());
			hashMap.put("mobile",emergencyVO.getMobile());
			hashMap.put("landline",emergencyVO.getLandline());
			
			insertFlag=namedParameterJdbcTemplate.update(sql,hashMap);
				
		}
		catch(Exception Ex)
		{
			Ex.printStackTrace();
			logger.error("Exception in addEmergencyContact : "+Ex);
		
		}
		logger.debug("Exit : public int addEmergencyContact(EmergencyVO emergencyVO)");
	return insertFlag;	
	}
	
	public int deleteEmergencyDetails(String emergencyId){
		
		int sucess = 0;
		String strSQL = "";
		try {
				logger.debug("Entry : public int deleteEmergencyDetails(String emergencyId)");
				strSQL = "DELETE FROM emergency_contact_no WHERE emergency_id=:emergencyId" ;
				logger.debug("query : " + strSQL);
				// 2. Parameters.
				Map namedParameters = new HashMap();
				namedParameters.put("emergencyId", emergencyId);
				
				sucess = namedParameterJdbcTemplate.update(strSQL,namedParameters);
				
				logger.debug("Exit : public int deleteEmergencyDetails(String emergencyId)"+sucess);

		}catch(Exception Ex){		
			Ex.printStackTrace();
			logger.error("Exception in deleteEmergencyDetails : "+Ex);		
		}
		return sucess;
	}	
	
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

}
