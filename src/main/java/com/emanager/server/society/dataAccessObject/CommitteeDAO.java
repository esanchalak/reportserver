package com.emanager.server.society.dataAccessObject;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.emanager.server.commonUtils.valueObject.DropDownVO;
import com.emanager.server.society.valueObject.CommitteeVO;


public class CommitteeDAO {
	
	private static final Logger logger = Logger.getLogger(CommitteeDAO.class);
	
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	
		
		public List searchSocietyMembers(String strSocietyID){
			
			List societyMemberList = null;
			String strSQL = "";
			
			try{	
				logger.debug("Entry : public List searchSocietyMembers(String strSocietyID)" );
				
			//1. SQL Query
			strSQL = "SELECT member_id,apt_name,title,first_name,last_name,full_name,building_name,mobile  FROM building_details,apartment_details,member_details " +
					"WHERE member_details.apt_id=apartment_details.apt_id AND apartment_details.building_id=building_details.building_id  AND building_details.society_id = :society_ID AND member_details.type='P' and is_current!=0 order by apartment_details.seq_no  ;";
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			 SqlParameterSource namedParameters = new MapSqlParameterSource("society_ID", strSocietyID);
	       
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			            DropDownVO societyMembrs = new DropDownVO();
			            societyMembrs.setData(rs.getString("member_id"));
			            societyMembrs.setLabel(rs.getString("building_name")+" - "+rs.getString("apt_name") + " - "+ rs.getString("title")+" " + rs.getString("full_name") );
			            return societyMembrs;
			        }};
		
			        societyMemberList = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
			        
		
			        logger.debug("Outside query of society Member list : " + societyMemberList.size() );
			        logger.debug("Exit :public List searchSocietyMembers(String strSocietyID)");
			        
			}catch (Exception ex){
				logger.error("Exception in searchSocietyMembers : "+ex);
				
			}
			return societyMemberList;
			
		}
		
    public List searchCommityMembers(String committeeId){
			
			List commityMemberList = null;
			String strSQL = "";
			
			try{	
				logger.debug("Entry :  public List searchCommityMembers(String committeeId)" );
				
			//1. SQL Query
			strSQL = "SELECT committee_member_relation.*,title,apt_name,first_name,last_name ,full_name ,building_name,mobile FROM building_details,apartment_details,committee_member_relation,member_details "
		           +"WHERE committee_member_relation.member_list_id = member_details.member_id "
		           +"AND member_details.apt_id=apartment_details.apt_id "
		           +"AND apartment_details.building_id=building_details.building_id "
		           +"AND committee_id =:committeeId AND member_details.type='P' and is_current!=0  order by apartment_details.seq_no;";
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			 SqlParameterSource namedParameters = new MapSqlParameterSource("committeeId", committeeId);
	       
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			            CommitteeVO committeeVO = new CommitteeVO();
			           
		                committeeVO.setOwnerName(rs.getString("title")+". " + rs.getString("full_name") ); 
			            committeeVO.setFlatNo(rs.getString("building_name")+" - "+rs.getString("apt_name"));
		                committeeVO.setMobile(rs.getString("mobile"));
			           
			            return committeeVO;
			        }};
		
			        commityMemberList = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
			        
		
			        logger.debug("Outside query of Commity Member list : " + commityMemberList.size() );
			        logger.debug("Exit : public List searchCommityMembers(String committeeId)");
			        
			}catch (Exception ex){
				logger.error("Exception in searchCommityMembers : "+ex);
				
			}
			return commityMemberList;
			
		}
		public List getCommityMemberList(String committeeId){
			 List committeeMember=null;
			 String strSQL = "";
			 
			 try{
			 logger.debug("Entry : public List getCommityMemberList(String commityId)");
			 
			 strSQL = "SELECT committee_member_relation.*,apt_name,first_name,last_name, full_name,building_name,mobile FROM building_details,apartment_details,committee_member_relation,member_details "
			           +"WHERE committee_member_relation.member_list_id = member_details.member_id "
			           +"AND member_details.apt_id=apartment_details.apt_id AND member_details.type='P' and is_current!=0 "
			           +" AND apartment_details.building_id=building_details.building_id "
			           +" AND committee_id =:commityId;";
			 logger.debug("query : " + strSQL);       
			     
		     // 2. Parameters.
		      Map namedParameters = new HashMap();
		      namedParameters.put("commityId", committeeId);

		    //3. RowMapper.
		    RowMapper RMapper = new RowMapper() {
		      public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		    	  DropDownVO drpDwn = new DropDownVO();
		          drpDwn.setData(rs.getString("member_list_id"));
		          drpDwn.setLabel(rs.getString("building_name")+" - "+rs.getString("apt_name") + " - " + rs.getString("full_name"));
		        return drpDwn;
		    }};

		    committeeMember = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
		    
		    logger.debug("Exit : public List getCommityMemberList(String commityId)"+committeeMember.size());
			}catch (Exception ex){
				logger.error("Exception in getCommityMemberList : "+ex);
					
			}
			return committeeMember;
			 
		 }
		public List getCommityList(String strSocietyID){

			List committeeList = null;
			final List societyMember ; 
			final List commityMember;
			String strSQL = "";
			
			try{
							    		
			logger.debug("Entry : public List getCommityList(String strSocietyID)");
			
			//1. SQL Query
			strSQL = "SELECT committee_details.*,title,first_name,last_name, full_name ,member_id,mobile FROM committee_details,member_details,society_details "+
		          	 "WHERE committee_details.committee_owner = member_details.member_id  " +
		          	 "AND society_details.society_id = committee_details.society_id  "+
			         "AND committee_details.society_id = :society_ID AND member_details.type='P' and is_current!=0 "+
			         "GROUP BY committee_details.committee_id; ";
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("society_ID", strSocietyID);
			
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			            CommitteeVO commitee = new CommitteeVO();
			            commitee.setCommitteeId(rs.getString("committee_id"));
			            commitee.setSocietyId(rs.getString("society_id"));
			            commitee.setCommitteeName(rs.getString("committee_name"));
			            commitee.setOwnerName( rs.getString("title")+". "+rs.getString("full_name"));
			            commitee.setCommitteeDescription(rs.getString("description"));
			            commitee.setInsertDate(rs.getString("insert_date"));
			            commitee.setWorkLog(rs.getString("committee_worklog"));
			            commitee.setOwnerId(rs.getString("member_id"));
			            commitee.setMobile(rs.getString("mobile"));
			            return commitee;
			        }};

			        committeeList = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
			        
			        
			        logger.debug("Size of commity List is  : " + committeeList.size() );
			        logger.debug("Exit : public List getCommityList(String strSocietyID)");
			}catch (Exception ex){
				logger.error("Exception in getCommityList : "+ex);
				
			}
			return committeeList;
			
			
		}


		public int insertCommiteeDetails(CommitteeVO committeeVO){
			int genratedID=0;
			int sucess = 0;
			int insertFlag=0;
			
			try{
			
			logger.debug("Entry : public int insertCommiteeDetails(CommitteeVO committeeVO)");
						
			String insertFileString = "INSERT INTO committee_details (society_id,committee_name, committee_owner,description) VALUES "
				+ "( :societyId, :committeeName, :ownerId, :committeeDescription); ";
			logger.debug("query : " + insertFileString);
			
			SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(
					committeeVO);

		    KeyHolder keyHolder = new GeneratedKeyHolder();
		    insertFlag = getNamedParameterJdbcTemplate().update(
				    insertFileString, fileParameters, keyHolder);
		    genratedID = keyHolder.getKey().intValue();
		    logger.debug("key GEnereator ---" + genratedID);
			
		    	
		    if(insertFlag != 0){
		    	sucess = committeeMemberRelation(committeeVO, genratedID);
							
		    }
		   
		    if(sucess != 0 && insertFlag != 0 ){
			    insertFlag = 1;
		    }
					
			logger.debug("Exit : public int insertCommiteeDetails(CommitteeVO committeeVO)"+insertFlag);

			}catch(Exception Ex){
				
				Ex.printStackTrace();
				logger.error("Exception in insertCommiteeDetails : "+Ex);
				
			}
			return insertFlag;
		}
		 

		public int committeeMemberRelation(CommitteeVO committeeVO,int genratedID) {
		    
		    int insertFlag = 0;
		    List commiteeMemberIDList=null;  
		    
		    try {
		    	commiteeMemberIDList = committeeVO.getCommityMemberList();
		     logger.debug("Entry : public int committeeMemberRelation(CommitteeVO committeeVO,int genID)");
		   	  
		      	   
		   	  for(int i=0;commiteeMemberIDList.size()>i;i++){
		   		DropDownVO drpDwn=new DropDownVO();
				drpDwn=(DropDownVO)commiteeMemberIDList.get(i);	
		
		   		String strQry = "INSERT INTO committee_member_relation (committee_id,member_list_id) VALUES( :genID, :commityMemberList);";
		   		logger.debug("query : " + strQry);
		   		
			       Map namedParam = new HashMap();
			       namedParam.put("genID", genratedID);
			       namedParam.put("commityMemberList",drpDwn.getData());
			
			       insertFlag = namedParameterJdbcTemplate.update(strQry, namedParam);
			       
			   
			   	 
		   	  }
		   
		   	     logger.debug("Exit : public int committeeMemberRelation(CommitteeVO committeeVO,int genratedID)"+insertFlag);
		    
		    }catch (DataAccessException exc) {
		         logger.error("DataAccessException in committeeMemberRelation : " +exc);
		        }

		        catch (Exception e) {
		        logger.error("Exception in committeeMemberRelation :"+e);
		      }

		     return insertFlag;
		}
		
		public int insertCommityWorklog(CommitteeVO committeeVO,String committeeId){
			 
			int insertFlag = 0;
			 Date currentDatetime = new Date(System.currentTimeMillis());   
			    java.sql.Date sqlDate = new java.sql.Date(currentDatetime.getTime());   
				java.sql.Timestamp timestamp = new java.sql.Timestamp(currentDatetime.getTime());
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
				String formattedDate = sdf.format(currentDatetime);
				
			try{
				
				logger.debug("Entry : public int insertCommityWorklog(CommitteeVO committeeVO,String committeeId)");
				 
				String strQry = "UPDATE committee_details SET insert_date =:insertDate, committee_worklog =:workLog WHERE committee_id="+committeeId+";";
				logger.debug("query : " + strQry);
				
			       Map namedParameter = new HashMap();
			       namedParameter.put("insertDate", committeeVO.getInsertDate());
			       namedParameter.put("workLog",formattedDate+"- Updated By:-"+committeeVO.getOwnerName()+"<br>"+committeeVO.getWorkLog()+"<br>---------------------<br>"+committeeVO.getComments());
			      
			       insertFlag = namedParameterJdbcTemplate.update(strQry, namedParameter);
			   				
				logger.debug("Exit : public int insertCommityWorklog(CommitteeVO committeeVO,String committeeId)"+insertFlag);
				
				
				 
			 }	catch(Exception Ex){
				 
				 logger.error("Exception in insertCommityWorklog : "+Ex);
				 
			 }
			
			 return insertFlag;
			 
				
			}
		
		public int removeCommity(String committeeId){
			
			int sucess = 0;
			String strSQL = "";
			try {
					logger.debug("Entry : public int removeCommity(String committeeId)");
					
					strSQL = "DELETE FROM committee_details WHERE committee_id=:commityId" ;
					logger.debug("query : " + strSQL);
					
					// 2. Parameters.
					Map namedParameters = new HashMap();
					namedParameters.put("commityId", committeeId);
					
					sucess = namedParameterJdbcTemplate.update(strSQL,namedParameters);
					
				
					logger.debug("Exit : public int removeCommity(String committeeId)"+sucess);

			}catch(Exception Ex){
				
				Ex.printStackTrace();
				logger.error("Exception in removeCommity : "+Ex);
				
			}
			return sucess;
		}	
		
		 public int updateCommityDetails(CommitteeVO committeeVO,String committeeId){
				
				int insertFlag = 0;
				int flag = 0;
				List memberIdList=null; 
				
				try{
				logger.debug("Entry : public int updateCommityDetails(CommitteeVO committeeVO,String committeeId)");
				
				String strQry = "UPDATE committee_details SET society_id =:societyId,committee_name =:committeeName, committee_owner =:ownerId,description =:committeeDescription WHERE committee_id="+committeeId+";";
				logger.debug("query : " + strQry);
				
			       Map namedParameter = new HashMap();
			       namedParameter.put("societyId", committeeVO.getSocietyId());
			       namedParameter.put("committeeName", committeeVO.getCommitteeName());
			       namedParameter.put("ownerId", committeeVO.getOwnerId());
			       namedParameter.put("committeeDescription", committeeVO.getCommitteeDescription());
			     			      
			       insertFlag = namedParameterJdbcTemplate.update(strQry, namedParameter);
				
				  
			       if(insertFlag != 0){
			    	  flag = updateCommitteeMemberRelation(committeeVO,committeeId);
			       }
				  if(flag != 0 && insertFlag != 0 )	 {
					  insertFlag = 1;
				  }
						
				logger.debug("Exit : public int updateCommityDetails(CommitteeVO committeeVO,String commityId)"+insertFlag);
				}catch(Exception Ex){
					Ex.printStackTrace();
					logger.error("Exception in updateCommityDetails : "+Ex);
					
					
				}
			
				return insertFlag;
			}
		  
		 public int updateCommitteeMemberRelation(CommitteeVO committeeVO,String committeeId){
			 int insertFlag = 0;
			 int sucess=0;
			 int flag=0;
			 List updatedCommiteeMemberIdList=null; 
			 List backupCommitteeMemberIdList=null;
			 
			 backupCommitteeMemberIdList = getCommityMemberList(committeeId);
			 
				logger.debug("Entry : public int updateCommityDetails(CommitteeVO committeeVO,String committeeId)");
			 try{
				 String sql = "DELETE FROM committee_member_relation WHERE committee_id=:commityId" ;
					logger.debug("query : " + sql);
					
					// 2. Parameters.
					Map namedParameters = new HashMap();
					namedParameters.put("commityId", committeeId);
					
					sucess = namedParameterJdbcTemplate.update(sql,namedParameters);
				 
					updatedCommiteeMemberIdList = committeeVO.getCommityMemberList();
				   	 					      	   
				    for(int i=0;updatedCommiteeMemberIdList.size()>i;i++){
				   		DropDownVO drpDwn=new DropDownVO();
						drpDwn=(DropDownVO)updatedCommiteeMemberIdList.get(i);	
						
						String query = " INSERT INTO committee_member_relation (committee_id,member_list_id) VALUES( :commityId, :memberIDList);";
						logger.debug("query : " + query); 
						
						Map namedParameter = new HashMap();
						  namedParameter.put("commityId", committeeId);
					      namedParameter.put("memberIDList",drpDwn.getData());
					
					      insertFlag = namedParameterJdbcTemplate.update(query, namedParameter);
					       
					
					   	 
		                 }
				    if(insertFlag == 0){
				    						      	   
						    for(int i=0;backupCommitteeMemberIdList.size()>i;i++){
						   		DropDownVO drpDwnforBkp=new DropDownVO();
						   		drpDwnforBkp=(DropDownVO)backupCommitteeMemberIdList.get(i);	
								
						   		String query = " INSERT INTO committee_member_relation (committee_id,member_list_id) VALUES( :commityId, :memberIDList);";
								logger.debug("query : " + query); 
								
								Map namedParam = new HashMap();
								  namedParam.put("commityId", committeeId);
								  namedParam.put("memberIDList",drpDwnforBkp.getData());
							
							      flag = namedParameterJdbcTemplate.update(query, namedParam);
				      }
				    }
		    	  }catch(Exception Ex){
						Ex.printStackTrace();
						logger.error("Exception in updateCommitteeMemberRelation : "+Ex);
						
						
					}
		    	  
		    		logger.debug("Exit : public int updateCommityDetails(CommitteeVO committeeVO,String committeeId)"+insertFlag);
			     return insertFlag;
			}	
		
		 public List searchCommittee(String strSocietyID){
				
				List listCommittee = null;		
				String strSQL = "";
				
				try{	
					logger.debug("Entry :  public List searchCommittee(String strSocietyID)" );
				//1. SQL Query
				strSQL = " SELECT * FROM committee_details WHERE society_id= :society_id;";
				logger.debug("query : " + strSQL);
				// 2. Parameters.
				 SqlParameterSource namedParameters = new MapSqlParameterSource("society_id", strSocietyID);
		       
				//3. RowMapper.
				 RowMapper RMapper = new RowMapper() {
				        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				            DropDownVO drpDwn = new DropDownVO();
				            drpDwn.setData(rs.getString("committee_id"));
				            drpDwn.setLabel(rs.getString("committee_name"));
				            return drpDwn;
				        }};
			
				        listCommittee = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
				
				        logger.debug("outside query : " + listCommittee.size() );
				        logger.debug("Exit :  public List searchCommittee(String strSocietyID)" );
				}catch (Exception ex){
					logger.error("Exception in searchCommittee : "+ex);
					
				}
				return listCommittee;
			}
	
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
}
