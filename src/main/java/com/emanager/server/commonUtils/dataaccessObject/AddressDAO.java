package com.emanager.server.commonUtils.dataaccessObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.emanager.server.commonUtils.valueObject.AddressVO;

public class AddressDAO {

	private JdbcTemplate jdbcTemplate;

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private static final Logger logger = Logger.getLogger(AddressDAO.class);
	
	public AddressVO getAddressDetails(int strPersonID,String strCategoryID, String strPersonCategory) {
		String strSQLQuery = "";
		AddressVO addrVO = new AddressVO();
		final String strPerson = strPersonCategory;

		try {
			logger.debug("Entry public AddressVO getAddressDetails(String strPersonID,String strCategoryID,String strPersonCategory)");
			//1. SQL Query
			strSQLQuery = "select addr_book.* from address_book addr_book,address_person_relationship apr "
					+ "where apr.person_id = :member_id AND addr_book.address_id = apr.address_id "
					+ " AND apr.person_category = :person_category AND addr_book.category_id = :addr_category_id order by addr_book.address_id limit 1;";
			logger.debug("Query : " + strSQLQuery);
			// 2. Parameters.
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("member_id", strPersonID);
			namedParameters.addValue("addr_category_id", strCategoryID);
			namedParameters.addValue("person_category", strPersonCategory);

			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					AddressVO addrVO = new AddressVO();
					addrVO.setAddID(rs.getInt("address_id"));
					addrVO.setCategoryID(rs.getString("category_id"));
					addrVO.setPerson_category(strPerson);
					addrVO.setAddLine1(rs.getString("line_1"));
					addrVO.setAddLine2(rs.getString("line_2"));
					addrVO.setNearestLandMark(rs.getString("land_mark"));
					addrVO.setCity(rs.getString("city"));
					addrVO.setState(rs.getString("state"));
					addrVO.setZip(rs.getString("postal_code"));
					addrVO.setMobile(rs.getString("mobile"));
					addrVO.setLandline(rs.getString("phone_1"));
					addrVO.setEmail(rs.getString("email_address"));
					addrVO.setCountryCode(rs.getString("country_code"));
					addrVO.setAddress(rs.getString("line_1")+", "+rs.getString("line_2")+", "+rs.getString("city")+", "+rs.getString("postal_code"));
					return addrVO;
				}
			};

			addrVO = (AddressVO) namedParameterJdbcTemplate.queryForObject(
					strSQLQuery, namedParameters, RMapper);

			logger.debug("Exit public AddressVO getAddressDetails(String strPersonID,String strCategoryID,String strPersonCategory)");

		} catch (EmptyResultDataAccessException Ex) {

			logger.debug("No address details are available  ");

		} catch (Exception e) {
			logger.error("Exception in AddressDAO :" + e+strPersonID+strPersonCategory);
		}

		return addrVO;

	}

	public List getAddressDetailsRenter(int strRenterID,String strPersonCategory) {
		String strSQLQuery = "";
		List adressList=null;	

		try {
			logger.debug("Entry : public List getAddressDetailsRenter(String strRenterID,String strPersonCategory)");
//			1. SQL Query
			strSQLQuery = " SELECT address_book.*,description,person_category " +
					" FROM address_book,address_person_relationship,address_category " +
					" WHERE address_book.address_id = address_person_relationship.address_id " +
					" AND address_book.category_id=address_category.category_id " +
					" AND person_id=:personID"+
					" AND person_category=:strPersonCategory ;";
			logger.debug("Query : " + strSQLQuery);
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("personID", strRenterID);
			namedParameters.put("strPersonCategory",strPersonCategory);

			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					AddressVO addrVO = new AddressVO();
					addrVO.setAddID(rs.getInt("address_id"));
					addrVO.setCategoryID(rs.getString("category_id"));
					addrVO.setPerson_category(rs.getString("person_category"));
					addrVO.setAddLine1(rs.getString("line_1"));
					addrVO.setAddLine2(rs.getString("line_2"));
					addrVO.setNearestLandMark(rs.getString("land_mark"));
					addrVO.setCity(rs.getString("city"));
					addrVO.setState(rs.getString("state"));
					addrVO.setZip(rs.getString("postal_code"));
					addrVO.setMobile(rs.getString("mobile"));
					addrVO.setLandline(rs.getString("phone_1"));
					addrVO.setEmail(rs.getString("email_address"));
					addrVO.setComments(rs.getString("description"));
					addrVO.setAddress(rs.getString("line_1")+", "+rs.getString("line_2")+", "+rs.getString("city")+", "+rs.getString("postal_code"));
					addrVO.setCountryCode(rs.getString("country_code"));
					return addrVO;
				}
			};

			adressList=namedParameterJdbcTemplate.query(strSQLQuery, namedParameters, RMapper);

			logger.debug("Exit public List getAddressDetailsList(String strPersonID,String strPersonCategory)"+adressList.size());

		}catch(EmptyResultDataAccessException Ex) {
			logger.info("No address details are available  ");
		} catch (Exception e) {
			logger.error("Exception in getAddressDetailsList : "+e);
		}

		return adressList;

	}

	public List getAddressDetailsList(int strPersonID,String strPersonCategory) {
		String strSQLQuery = "";
		List adressList=null;		

		try {
			logger.debug("Entry : public List getAddressDetailsList(String strPersonID,String strPersonCategory)");
			//1. SQL Query
			strSQLQuery = " SELECT address_book.*,description,person_category " +
					" FROM address_book,address_person_relationship,address_category " +
					" WHERE address_book.address_id = address_person_relationship.address_id " +
					" AND address_book.category_id=address_category.category_id " +
					" AND person_id=:personID"+
					" AND person_category=:strPersonCategory ;";
			logger.debug("Query : " + strSQLQuery);
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("personID", strPersonID);
			namedParameters.put("strPersonCategory",strPersonCategory);

			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					AddressVO addrVO = new AddressVO();
					addrVO.setAddID(rs.getInt("address_id"));
					addrVO.setCategoryID(rs.getString("category_id"));
					addrVO.setPerson_category(rs.getString("person_category"));
					addrVO.setAddLine1(rs.getString("line_1"));
					addrVO.setAddLine2(rs.getString("line_2"));
					addrVO.setNearestLandMark(rs.getString("land_mark"));
					addrVO.setCity(rs.getString("city"));
					addrVO.setState(rs.getString("state"));
					addrVO.setZip(rs.getString("postal_code"));
					addrVO.setMobile(rs.getString("mobile"));
					addrVO.setLandline(rs.getString("phone_1"));
					addrVO.setEmail(rs.getString("email_address"));
					addrVO.setComments(rs.getString("description"));
					addrVO.setAddress(rs.getString("line_1")+", "+rs.getString("line_2")+", "+rs.getString("city")+", "+rs.getString("postal_code"));
					addrVO.setCountryCode(rs.getString("country_code"));
					return addrVO;
				}
			};

			adressList=namedParameterJdbcTemplate.query(strSQLQuery, namedParameters, RMapper);

			logger.debug("Exit public List getAddressDetailsList(String strPersonID,String strPersonCategory)"+adressList.size());

		}catch(EmptyResultDataAccessException Ex) {
			logger.info("No address details are available  ");
		} catch (Exception e) {
			logger.error("Exception in getAddressDetailsList : "+e);
		}

		return adressList;

	}
	
	public int updateAddressDetails(AddressVO addressVO) {

		int flag = 0;
		String strSQLQuery = "";
		try {

			logger.debug("Entry : public AddressVO updateAddressDetails(AddressVO addressVO)");
			strSQLQuery = "update address_book set category_id = :categoryID, line_1 = :addLine1,line_2 = :addLine2,land_mark =:nearestLandMark,"
					+ "city = :city, postal_code = :zip, state = :state, mobile = :mobile ,phone_1 = :landline, email_address = :email, country_code=:countryCode "
					+ "where address_id = :addID";
			logger.debug("Query : " + strSQLQuery);
			
			SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(
					addressVO);

			flag = this.namedParameterJdbcTemplate.update(strSQLQuery,
					namedParameters);
			logger.debug("Exit : public AddressVO updateAddressDetails(AddressVO addressVO)");

		} catch (Exception Ex) {
			logger.error("Exception in updateAddressDetails : "+Ex);
		}

		return flag;

	}

	/*public AddressVO addNewAddressDetails(AddressVO addrVO, String strMemberID){
	 
	 
	 try{
	 logger.debug("Entry public AddressVO addNewAddressDetails(AddressVO addrVO, String strMemberID)");
	 int lngMaxID = getMaxID("address_id");
	 
	 addrVO.setAddID(lngMaxID+"");
	 
	 String strQuery_addres_book="insert into address_book(address_id,category_id,line_1,line2,land_mark," +
	 "city,state,postal_code,mobile,phone_1,phone_2,email_address) " +
	 "values(:addID,:categoryID,:addLine1,:addLine2,:nearestLandMark," +
	 ":city,:state,:zip,:mobile,:landline,:landline,:email)";
	 
	 
	 SqlParameterSource namedParameters_email = new BeanPropertySqlParameterSource(addrVO);			
	 namedParameterJdbcTemplate.update(strQuery_addres_book,namedParameters_email);
	 
	 String strQuery_addres_relation="insert into address_person_relationship(person_id,address_id,person_category " +
	 "values(:person_ID,:addID,:person_category);"; 
	 
	 logger.debug("Exit public AddressVO addNewAddressDetails(AddressVO addrVO, String strMemberID)");
	 
	 
	 }	catch(Exception Ex){
	 
	 logger.error("DAO exception:"+Ex); 
	 
	 }
	 
	 return addrVO;
	 
	 
	 }*/

	public int addNewAddressDetails(AddressVO addressVO, int strMemberID) {
		int insertFlag = 0;
		int genratedID = 0;
		int success = 0;
		logger.info("Entry : public int addNewAddressDetails(AddressVO addressVO, String strMemberID)"+addressVO.getCountryCode());
		try {
			String insertFileString = "INSERT INTO address_book "
					+ "( category_id, line_1, line_2, land_mark, city, state, postal_code, mobile, phone_1, email_address, country_code) VALUES "
					+ "( :categoryID, :addLine1, :addLine2, :nearestLandMark, :city , :state, :zip, :mobile, :landline, :email, :countryCode) ";
			logger.debug("Query : " + insertFileString);
			
			SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(
					addressVO);
			KeyHolder keyHolder = new GeneratedKeyHolder();
			insertFlag = getNamedParameterJdbcTemplate().update(
					insertFileString, fileParameters, keyHolder);
			genratedID = keyHolder.getKey().intValue();
			logger.debug("key GEnereator ---" + genratedID);
			success = insertAddressPersonRelatn(addressVO, strMemberID, genratedID);
			if ((insertFlag != 0) && (success != 0)) {
				insertFlag = 1;
			}
			logger.debug("Exit : public int addNewAddressDetails(AddressVO addressVO, String strMemberID)"+insertFlag);
		} catch (IllegalArgumentException ex) {
			logger.info("IllegalArgumentException in addNewAddressDetails : "+ex);

			
		} catch (InvalidDataAccessApiUsageException ex) {
			logger.error("InvalidDataAccessApiUsageException in addNewAddressDetails : "+ex);

		} catch (DataAccessException exc) {
			logger.error("DataAccessException in addNewAddressDetails : "+exc);
		} catch (Exception e) {
			logger.error("Exception in addNewAddressDetails : "+e);
		}

		return insertFlag;
	}

	public int insertAddressPersonRelatn(AddressVO addressVO,int strMemberID, int genratedID) {
		int flag = 0;
		logger.debug("Entry : public int insertAddressPersonRelatn(AddressVO addressVO,String strMemberID, int genratedID)");
		try {
			String strQry = "INSERT INTO address_person_relationship (person_id, address_id, person_category) VALUES (:personID, :addressID, :persnCategory);";
			logger.debug("Query : " + strQry);
			Map namedParam = new HashMap();
			namedParam.put("personID", strMemberID);
			namedParam.put("addressID", genratedID);
			namedParam.put("persnCategory", addressVO.getPerson_category());

			flag = namedParameterJdbcTemplate.update(strQry, namedParam);
			logger.debug("Exit :public int insertAddressPersonRelatn(AddressVO addressVO,String strMemberID, int genratedID)"+flag);
		} catch (DataAccessException exc) {
			logger.error("DataAccessException in insertAddressPersonRelatn : "+exc);
		}
		catch (Exception e) {
			logger.error("Exception in insertAddressPersonRelatn : "+e);
		}

		return flag;
	}

	public int removeContactDetails(String addressID){
		
			int success=0;
			try {
				logger.debug("Entry : public int removeContactDetails(String addressID)");
				
				String query="DELETE FROM address_book WHERE address_id=:addressID;";
				
				Map hmap=new HashMap();
				hmap.put("addressID", addressID);
				
				success=namedParameterJdbcTemplate.update(query, hmap);
				
				logger.debug("Exit : public int removeContactDetails(String addressID)"+success);
			} catch (Exception e) {				
				logger.error("Exception in removeContactDetails :  "+e);
			}
			return success;
			
		}
	
	public List getSocietyAddressDetails(int societyID,String strCategoryID,String strPermanantCategory)
	{
		List addressList=null;
		String sql=null;
		try
		{
			logger.debug("Entry : public List getSocietyAddressDetails(String societyID,String strCategoryID,String strPermanantCategory)");
			logger.debug("society details are"+societyID+" category:-"+strCategoryID+" permanent address cate "+strPermanantCategory);
			
			
			sql=" SELECT society_name,city,line_1,line_2,postal_code,state,mobile,phone_1,email_address,person_category,country_code "+
				 " FROM society_details ,address_category,address_person_relationship,address_book " +
				 " WHERE address_book.address_id=address_person_relationship.address_id AND " +
				 " address_person_relationship.person_category=:Category AND " + 
				 " address_person_relationship.person_id=society_details.society_id AND  address_person_relationship.person_id=:SocietyID" +
				 " GROUP BY society_id";
			logger.debug("Query : " + sql);
			
			Map namedParam = new HashMap();
			namedParam.put("SocietyID",societyID);
			namedParam.put("Category", strCategoryID);
			namedParam.put("PermanentCategory",strPermanantCategory);
			
			RowMapper RMapper=new RowMapper()
			{

				public Object mapRow(ResultSet rs, int n) throws SQLException {
					// TODO Auto-generated method stub
					AddressVO addVO=new AddressVO();
					addVO.setConcatAddr(rs.getString("address_book.line_1")+" "+rs.getString("address_book.line_2")+","+rs.getString("city")+"-"+rs.getString("postal_code"));
					addVO.setLandline(rs.getString("phone_1"));
					addVO.setMobile(rs.getString("mobile"));
					addVO.setPerson_category(rs.getString("person_category"));
					addVO.setEmail(rs.getString("email_address"));
					addVO.setAddress(rs.getString("line_1")+", "+rs.getString("line_2")+", "+rs.getString("city")+", "+rs.getString("postal_code"));
					addVO.setCountryCode(rs.getString("country_code"));
					addVO.setAddLine1(rs.getString("line_1")+", "+rs.getString("line_2"));
					addVO.setCity(rs.getString("city"));
					addVO.setState(rs.getString("state"));
					addVO.setZip(rs.getString("postal_code"));
					return addVO;
				}
				
			};
			
			addressList=namedParameterJdbcTemplate.query(sql, namedParam, RMapper);
			
		}
		catch(Exception ex){
			logger.error("Exception in getSocietyAddressDetails : "+ex);			
		}
		logger.debug("Exit : public List getSocietyAddressDetails(String societyID,String strCategoryID,String strPermanantCategory)"+addressList);
		return addressList;
		
	}
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

}
