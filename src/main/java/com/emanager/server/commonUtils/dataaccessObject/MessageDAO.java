package com.emanager.server.commonUtils.dataaccessObject;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.emanager.server.commonUtils.valueObject.DropDownVO;
import com.emanager.server.commonUtils.valueObject.MessageVO;


public class MessageDAO {
	private static final Logger logger = Logger.getLogger(MessageDAO.class);
	List listMessages=null;
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	JdbcTemplate jdbcTemplate;
	
		
		
		public List getMessages(int societyID){
			 String strSQLQuery = ""; 
			
			try{
				 logger.debug("Entry : public List getMessages(int societyID)");
				//1. SQL Query
				strSQLQuery = " SELECT member_message.*,member_details.first_name,member_details.full_name, member_details.last_name,member_details.email,user_type,category_name,tkt_category.category_id,apt_name, building_name "+
			      " FROM member_message,member_details,user_login_details, tkt_category,apartment_details, building_details  "+
			      " WHERE member_message.society_id =:society_id " +
			      " AND member_details.apt_id=apartment_details.apt_id "+
			      " AND apartment_details.building_id=building_details.building_id"+
			      " AND user_login_details.society_id=member_message.society_id " +
			      " AND member_message.member_id=member_details.member_id " +
			      " AND member_message.category_id=tkt_category.category_id "+
			      " AND member_message.stats != 'CLOSED' "+
			      " AND member_details.email IS NOT NULL " + 
				  " GROUP BY tk_id " +
				  " ORDER BY update_date DESC ; ";
						
				logger.debug("Query : " + strSQLQuery );
				// 2. Parameters.
				MapSqlParameterSource namedParameters = new MapSqlParameterSource();
				namedParameters.addValue("society_id", societyID);
				
				
				//3. RowMapper.
				 RowMapper RMapper = new RowMapper() {
				        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				        	MessageVO messageVO=new MessageVO();
				        	messageVO.setMemberID(rs.getString("member_id"));
				           	messageVO.setMsgID(rs.getString("tk_id"));
				        	messageVO.setMsgType(rs.getString("msg_type"));
				        	messageVO.setStatus(rs.getString("stats"));
				        	messageVO.setSubject(rs.getString("sub"));
				        	messageVO.setMemberName(rs.getString("building_name")+" "+rs.getString("apt_name")+" "+rs.getString("full_name"));
				        	messageVO.setInsertDate(rs.getString("insert_date"));
				        	messageVO.setUpdateDate(rs.getString("update_date"));
				        	messageVO.setMessage(rs.getString("message"));
				        	messageVO.setUserType(rs.getString("user_type"));
				        	messageVO.setEmail(rs.getString("email").trim());
				        	messageVO.setSocietyID(rs.getInt("society_id"));
				        	messageVO.setCategory_name(rs.getString("category_name"));
				        	messageVO.setCategory_id(rs.getString("category_id"));
				        	return messageVO;
				        	
							
				        }};
				    	listMessages= namedParameterJdbcTemplate.query(strSQLQuery,namedParameters,RMapper);
			
				    	logger.debug("Exit : public List getMessages(int societyID)");
				 
				 
			 }catch(Exception Ex){				 
				 logger.error("Exception in getMessages : " + Ex);				 
			 }
			
			
			return listMessages;
			
		}
	public List getMessages(int memberID,int societyID){
		 String strSQLQuery = ""; 
		 String selectMember="";
		 
		 if (memberID!=0){
			 selectMember=" AND member_message.member_id="+memberID+"";
		 }else
			 selectMember="";
		
		try{
			logger.debug("Entry : public List getMessages(String memberID,int societyID)");
			//1. SQL Query
			strSQLQuery =  " SELECT member_message.*,member_details.first_name,member_details.last_name,member_details.full_name,email,user_type,category_name,tkt_category.category_id,apt_name, building_name "+
		      " FROM member_message,member_details,user_login_details, tkt_category,apartment_details, building_details  "+
		      " WHERE member_message.member_id=member_details.member_id " +
		      " AND member_message.member_id=user_login_details.user_id " +
		      " AND member_details.apt_id=apartment_details.apt_id "+
		      " AND apartment_details.building_id=building_details.building_id"
		       +selectMember +
		      " AND member_message.society_id=:society_id " +
		      " AND member_message.society_id=user_login_details.society_id "+
		      " AND member_message.category_id=tkt_category.category_id "+
		      " AND member_message.stats != 'CLOSED' "+
		      " AND member_details.email IS NOT NULL " + 
			  " GROUP BY tk_id " +
			  " ORDER BY update_date DESC ; ";
			logger.debug("Query : " + strSQLQuery );
			// 2. Parameters.
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("member_id", memberID);
			namedParameters.addValue("society_id", societyID);
			
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			        	MessageVO messageVO=new MessageVO();
			           	messageVO.setMsgID(rs.getString("tk_id"));
			           	messageVO.setMemberID(rs.getString("member_id"));
			        	messageVO.setMsgType(rs.getString("msg_type"));
			        	messageVO.setStatus(rs.getString("stats"));
			        	messageVO.setSubject(rs.getString("sub"));
			        	messageVO.setMemberName(rs.getString("building_name")+" "+rs.getString("apt_name")+" "+rs.getString("full_name"));
			        	messageVO.setInsertDate(rs.getString("insert_date"));
			        	messageVO.setUpdateDate(rs.getString("update_date"));
			        	messageVO.setMessage(rs.getString("message"));
			        	messageVO.setUserType(rs.getString("user_type"));
			        	messageVO.setEmail(rs.getString("email"));
			        	messageVO.setSocietyID(rs.getInt("society_id"));
			        	messageVO.setCategory_name(rs.getString("category_name"));
			        	messageVO.setCategory_id(rs.getString("category_id"));
			        	return messageVO;
			        	
						
			        }};
			        listMessages= namedParameterJdbcTemplate.query(strSQLQuery,namedParameters,RMapper);
		
			    	logger.debug("Exit : public List getMessages(String memberID,int societyID)");
			 
			 
		 }	catch(Exception Ex){
			 
			 logger.error("Exception in getMessages(String memberID,int societyID) : " + Ex);
			 
		 }
		
		
		return listMessages;
		
	}
	
	public int insertNewMessages(MessageVO messageObj){
		 
       /* int insertFlag = 0;
      	
		try{
			
			logger.debug("Entry : public int insertNewMessages(MemberVo memberObj)");
			 
			insertFlag = this.jdbcTemplate.update("  INSERT INTO member_message(member_id,society_id,sub,message,stats,update_date,msg_type, category_id) VALUES ( ?,?,?,?,?,?,?,?); ", 
					                                             new Object[] {messageObj.getMemberID(),messageObj.getSocietyID(),messageObj.getSubject(),messageObj.getMessage(),messageObj.getStatus(),timestamp,messageObj.getMsgType(),messageObj.getCategory_id()});
			
			
						
			logger.debug("Exit : public int insertNewMessages(MemberVo memberObj)"+insertFlag);
			logger.info("HEre"+insertFlag);
		 }catch(Exception Ex){			 
			 logger.error("Exception in insertNewMessages : " + Ex);			 
		 }
		
	 return insertFlag;*/
		
	 int insertFlag = 0;
		int genratedID = 0;
		int success = 0;
		logger.debug("Entry :public int insertNewMessages(MessageVO messageObj)");
		try {
			String insertFileString = "INSERT INTO member_message "
					+ "( member_id,society_id,sub,message,stats,msg_type, category_id) VALUES "
					+ "( :memberID, :societyID, :subject, :message, :status , :msgType, :category_id) ";
			logger.debug("Query : " + insertFileString);
			
			SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(
					messageObj);
			KeyHolder keyHolder = new GeneratedKeyHolder();
			insertFlag = getNamedParameterJdbcTemplate().update(
					insertFileString, fileParameters, keyHolder);
			genratedID = keyHolder.getKey().intValue();
			logger.debug("key Genereator ---" + genratedID);
			success = updateMessageDate(genratedID);
			if ((insertFlag != 0) && (success != 0)) {
				insertFlag = 1;
			}
			logger.debug("Exit : public int insertNewMessages(MessageVO messageObj)"+insertFlag);
		} catch (InvalidDataAccessApiUsageException ex) {
			logger.error("InvalidDataAccessApiUsageException in insertNewMessages : "+ex);

		} catch (DataAccessException exc) {
			logger.error("DataAccessException in insertNewMessages : "+exc);
		} catch (Exception e) {
			logger.error("Exception in insertNewMessages : "+e);
		}
	 return insertFlag;
	}
	
	private int updateMessageDate(int genratedID) {
		int success=0;
		try{
		logger.debug("Entry : public int updateMessage(MessageVO messageObj) ");
		String strQuery="UPDATE member_message SET update_date=CURRENT_TIMESTAMP WHERE  tk_id="+genratedID+";";
		Map namedParam=new HashMap();
		
				
		success=this.namedParameterJdbcTemplate.update(strQuery ,namedParam);
		
		
		logger.debug("Exit : public int updateMessage(MessageVO messageObj)"+success);
		}
		catch(Exception e){
			logger.error("Exception in updateMessage : " +e);
		}
		return success;
		
	}
	public int updateMessage(MessageVO messageObj,String memberID,String messageId){
		int insertFlag=0;
		try{
			
			Date currentDatetime = new Date(System.currentTimeMillis());   
		    java.sql.Date sqlDate = new java.sql.Date(currentDatetime.getTime());   
			java.sql.Timestamp timestamp = new java.sql.Timestamp(currentDatetime.getTime());
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd - MMM - yyyy h:mm:ss a");
			String formattedDate = sdf.format(currentDatetime);
			
		logger.debug("Entry : public int updateMessage(MessageVO messageObj,String memberID,String messageId) ");
		String strQuery="UPDATE member_message SET message=:message, stats=:status, update_date=CURRENT_TIMESTAMP ,category_id=:categoryID WHERE  tk_id="+messageId+";";
		Map namedParam=new HashMap();
		namedParam.put("message",formattedDate+"- Updated By:-"+messageObj.getMemberName()+"<br>"+messageObj.getComments()+"<br>---------------------<br>"+messageObj.getMessage());
		namedParam.put("status", messageObj.getStatus());
		namedParam.put("categoryID", messageObj.getCategory_id());
				
		insertFlag=this.namedParameterJdbcTemplate.update(strQuery ,namedParam);
		
		
		logger.debug("Exit :public int updateMessage(MessageVO messageObj,String memberID,String messageId)"+insertFlag);
	
		}
		
		
		catch(Exception e){
			logger.error("Exception in updateMessage : " +e);
		}
		return insertFlag;
	}
	
	/*public List getMemberList(String memberID,String userType,int societyID){
		List memberList=null;
		logger.debug("Entry : public List getMemberList(String memberID,String userType) "+memberID);
		try {
			String strQry="SELECT email,first_name,last_name , full_name,user_type " +
					" FROM member_details LEFT JOIN user_login_details ON  user_login_details.user_id=member_details.member_id  AND user_login_details.society_id=:societyId" +
					" WHERE ( user_login_details.user_type=:usrType OR member_details.member_id=:memberID ) AND member_details.type='P';" ;
					
			logger.debug("Query : " + strQry );   
			    
			Map nameparameter = new HashMap();
			nameparameter.put("memberID", memberID);
			nameparameter.put("usrType", userType);
			nameparameter.put("societyId", societyID);
					
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			        	MemberVO mbrVO=new MemberVO();
			        	mbrVO.setUserFirstName(rs.getString("first_name"));
			        	mbrVO.setUserLastName(rs.getString("last_name"));
			        	mbrVO.setMemberFullName(rs.getString("full_name"));
			           	mbrVO.setEmail(rs.getString("email"));
			        	if(mbrVO.getEmail().equalsIgnoreCase("")){
			        		mbrVO.setEmail("helpdesk@esanchalak.com");
			        	}
			        	mbrVO.setBccEmail("techsupport@esanchalak.com");
			        	return mbrVO;
			        }};
			        memberList= namedParameterJdbcTemplate.query(strQry,nameparameter,RMapper);

			        logger.debug("public List getMemberList(String memberID,String userType)" +memberList.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception in getMemberList : "+e);
		}
		 
		
		
		return memberList;
	}*/
	
	
	public List getCategorylist(){
		List categoryList=null;
		
			try {
				logger.debug("Entry : public List getCategorylist() ");
				
				String sqlQry=" SELECT * FROM tkt_category; ";
				
				Map hmap=new HashMap();			
				
				RowMapper RMapper = new RowMapper() {
				    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				        DropDownVO drpDwn = new DropDownVO();
				        drpDwn.setData(rs.getString("category_id"));
				        drpDwn.setLabel(rs.getString("category_name"));
				        return drpDwn;
				    }};
				
				    categoryList=namedParameterJdbcTemplate.query(sqlQry,hmap,RMapper);
				

				logger.debug("Exit : public List getCategorylist() ");
			} catch (DataAccessException e) {
				logger.error("Exception in getCategorylist : "+e);
			}
		return categoryList;
		}
	
	public List countCategories(String societyId){
		List categoryList=null;
		
			try {
				logger.debug("Entry : public List countCategories() ");
				
				String sqlQry=" SELECT tkt_category.category_id,category_name,COUNT(category_name) as count " +
						" FROM member_message,tkt_category" +
						" WHERE member_message.category_id=tkt_category.category_id " +
						"AND society_id=:societyId GROUP BY member_message.category_id;; ";
				
				Map hmap=new HashMap();	
				hmap.put("societyId", societyId);
				
				RowMapper RMapper = new RowMapper() {
				    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				        DropDownVO drpDwn = new DropDownVO();
				        drpDwn.setData(rs.getString("count"));
				        drpDwn.setLabel(rs.getString("category_name"));
				        return drpDwn;
				    }};
				
				    categoryList=namedParameterJdbcTemplate.query(sqlQry,hmap,RMapper);
				

				logger.debug("Exit : public List countCategories(String societyId) "+categoryList.size());
			} catch (DataAccessException e) {
				logger.error("Exception in countCategories(String societyId) : "+e);
			}
		return categoryList;
		}
	
	public List getMessagesForClosedStatus(int memberID,int societyID){
		 String strSQLQuery = ""; 
        String selectMember="";
		 
		 if (memberID!=0){
			 selectMember=" AND member_message.member_id="+memberID+"";
		 }else
			 selectMember="";
		
		try{
			 logger.debug("Entry : public List getMessagesForClosedStatus(int societyID)");
			//1. SQL Query
			strSQLQuery = " SELECT member_message.*,member_details.first_name,member_details.last_name,member_details.full_name,member_details.email,user_type,category_name,tkt_category.category_id,apt_name, building_name "+
		      " FROM member_message,member_details,user_login_details, tkt_category,apartment_details, building_details  "+
		      " WHERE member_message.society_id =:society_id " +
		      " AND member_details.apt_id=apartment_details.apt_id "+
		      " AND apartment_details.building_id=building_details.building_id"+
		      " AND user_login_details.society_id=member_message.society_id " +
		      " AND member_message.member_id=member_details.member_id " 
		      + selectMember+
		      " AND member_message.category_id=tkt_category.category_id "+
		      " AND member_message.stats = 'CLOSED' "+
		      " AND member_details.email IS NOT NULL " + 
			  " GROUP BY tk_id " +
			  " ORDER BY update_date DESC ; ";
					
			logger.debug("Query : " + strSQLQuery );
			// 2. Parameters.
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("society_id", societyID);
			
			
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			        	MessageVO messageVO=new MessageVO();
			        	messageVO.setMemberID(rs.getString("member_id"));
			           	messageVO.setMsgID(rs.getString("tk_id"));
			        	messageVO.setMsgType(rs.getString("msg_type"));
			        	messageVO.setStatus(rs.getString("stats"));
			        	messageVO.setSubject(rs.getString("sub"));
			        	messageVO.setMemberName(rs.getString("building_name")+" "+rs.getString("apt_name")+" "+rs.getString("full_name"));
			        	messageVO.setInsertDate(rs.getString("insert_date"));
			        	messageVO.setUpdateDate(rs.getString("update_date"));
			        	messageVO.setMessage(rs.getString("message"));
			        	messageVO.setUserType(rs.getString("user_type"));
			        	messageVO.setEmail(rs.getString("email").trim());
			        	messageVO.setSocietyID(rs.getInt("society_id"));
			        	messageVO.setCategory_name(rs.getString("category_name"));
			        	messageVO.setCategory_id(rs.getString("category_id"));
			        	return messageVO;
			        	
						
			        }};
			    	listMessages= namedParameterJdbcTemplate.query(strSQLQuery,namedParameters,RMapper);
		
			    	logger.debug("Exit : public List getMessagesForClosedStatus(int societyID)");
			 
			 
		 }catch(Exception Ex){				 
			 logger.error("Exception in getMessagesForClosedStatus : " + Ex);				 
		 }
		
		
		return listMessages;
		
	}
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	

}
