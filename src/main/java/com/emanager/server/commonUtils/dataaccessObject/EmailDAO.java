package com.emanager.server.commonUtils.dataaccessObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.emanager.server.commonUtils.valueObject.EventForm;
import com.emanager.server.commonUtils.valueObject.RecentUpdateVO;


public class EmailDAO {
	 
	 private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	 private static final Logger logger = Logger.getLogger(EmailDAO.class);
	 
	 public RecentUpdateVO sendScheduledEmail(final EventForm eventVO,RecentUpdateVO updateVO){
		 int insertFlag=0;
		 int genID=0;
		 logger.debug("Entry :  public int sendScheduledEmail(final EventVO eventVO)");
		 try {
			String sqlQuery="INSERT INTO event_scheduler(member_id, society_id, temp_id, event_type, event_subject, emailFrequency, set_date, next_date, last_date, event_status, transaction_type, message) "+
			 " VALUES ( :memberID, :societyID, :templeteName, :eventType, :subject, :frequency, :setDate, :nextDate, :lastDate, :status, :transaction_type, :message);" ;
			 
					 
			 SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(
					 eventVO);
				KeyHolder keyHolder = new GeneratedKeyHolder();
				insertFlag = getNamedParameterJdbcTemplate().update(
						sqlQuery, fileParameters, keyHolder);
				genID = keyHolder.getKey().intValue();
				updateVO.setEventID(genID);
			 
			 
		
			 logger.debug("Exit :  public int sendScheduledEmail(final EventVO eventVO)");
		} catch (DataAccessException e) {
			
			logger.error("DataAccessException :"+e);
		}
		catch (Exception ex) {
			
			logger.error("Exception :"+ex);
		}
		return updateVO;
		 
	 }
	 
	 public RecentUpdateVO updateScheduledEmail(final EventForm eventVO,RecentUpdateVO updateVO,int eventID){
		 int insertFlag=0;
		 int genID=0;
		 logger.debug("Entry :  public RecentupdateVO updateScheduledEmail(final EventVO eventVO,RecentUpdateVO updateVO)");
		 try {
			String sqlQuery="UPDATE event_scheduler SET event_subject=:subject ,emailFrequency=:frequency ,set_date=:setDate ,last_date=:lastDate ,message=:message WHERE event_id="+eventID+" ;" ;
			
			 
			 //SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(
				//	 eventVO);
			Map hmap=new HashMap();
			hmap.put("subject", eventVO.getSubject());
			hmap.put("frequency", eventVO.getFrequency());
			hmap.put("setDate", eventVO.getSetDate());
			hmap.put("lastDate", eventVO.getLastDate());
			hmap.put("message", eventVO.getMessage());
			
			
			
				insertFlag = namedParameterJdbcTemplate.update(sqlQuery, hmap);
			 
			 
			
			 logger.debug("Exit :  public RecentupdateVO updateScheduledEmail(final EventVO eventVO,RecentUpdateVO updateVO)");
		} catch (DataAccessException e) {
			
			logger.error("DataAccessException :"+e);
		}
		catch (Exception ex) {
			
			logger.error("Exception :"+ex);
		}
		return updateVO;
		 
	 }
	 
	 public EventForm getEventDetails(int eventID){
	    	EventForm eventVO=null;
	    	logger.debug("Entry : public EventVO getEventDetails(int eventID)");
	    		
	    	String sql="SELECT * FROM event_scheduler WHERE event_id=:eventID";
	    	
	    	Map hmap=new HashMap();
	    	hmap.put("eventID", eventID);
	    	RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					EventForm eventVO = new EventForm();
					eventVO.setSubject(rs.getString("event_subject"));
					eventVO.setMessage(rs.getString("message"));
					eventVO.setSetDate(rs.getString("set_date"));
					eventVO.setLastDate(rs.getString("last_date"));
					eventVO.setFrequency(rs.getString("emailFrequency"));
					return eventVO;
				}
			};
	    	
	    	eventVO=(EventForm) namedParameterJdbcTemplate.queryForObject(sql, hmap, RMapper);
	    	
	    	logger.debug("Exit : public EventVO getEventDetails(int eventID)");
	    	
	    	return eventVO;
	    }
	 
/*	 public List getMemberList(String societyID,String buildingId){
			List memberList=null;
			String strWhereClause;
			
			try {
				logger.debug("Entry : public List getMemberList(String societyID) "+societyID);
				if(buildingId.equalsIgnoreCase("0")){
					 strWhereClause = " ";
				}else			
				   strWhereClause = " AND apartment_details.building_id="+buildingId +" ";
				
			String strQry=" SELECT DISTINCT(member_details.member_id),first_name,last_name,full_name ,member_details.email,apt_name,building_name FROM apartment_details,member_details,society_details,building_details "+ 
 	                      " WHERE member_details.apt_id = apartment_details.apt_id"+ 
 	                      " AND apartment_details.building_id=building_details.building_id "+
 	                        strWhereClause  +
 	                      " AND apartment_details.unit_sold_status='Sold' " +
 	                      " AND society_details.society_id= :societyID AND email!='' "+ 	                     
 	                      " AND society_details.society_id=building_details.society_id and is_current!=0 "+
 	                      "  ORDER BY member_details.member_id; ";
				 			    
				Map nameparameter = new HashMap();
				nameparameter.put("societyID", societyID);
							
				//3. RowMapper.
				 RowMapper RMapper = new RowMapper() {
				        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				        	MemberVO memberVO=new MemberVO();
				        	memberVO.setUserFirstName(rs.getString("first_name"));
				        	memberVO.setAptName(rs.getString("building_name")+" "+rs.getString("apt_name"));
				        	memberVO.setUserLastName(rs.getString("last_name"));
				        	memberVO.setMemberFullName(rs.getString("full_name"));
				        	memberVO.setEmail(rs.getString("email"));
				        	
				        	return memberVO;
				        }};
				        memberList= namedParameterJdbcTemplate.query(strQry,nameparameter,RMapper);

				        logger.debug("Exit :public List getMemberList(String memberID)" +memberList.size());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in getMemberList : "+e);
			}
			 		
			
			return memberList;
		}
	 
	 public List getCommityMember(String societyID,String committeeID){

			List commityMember=null;
			String strSQL = "";
			String strWhereClause=" ";
			try{
				if(committeeID.equalsIgnoreCase("0")){
					 strWhereClause = " ";
				}else			
				   strWhereClause = " AND c.committee_id="+committeeID +" ";
							    		
			logger.debug("Entry :  public List getCommityMember(String committeeID)");
			
			//1. SQL Query
			strSQL = " SELECT DISTINCT(m.member_id),full_name,committee_name,CONCAT(building_name,' ',apt_name)AS flatNo,email FROM committee_details c,member_details m," +
					 " committee_member_relation cmr,apartment_details a,building_details b "
		           + " WHERE  email !='' "
		           + strWhereClause 
		           + " AND m.apt_id = a.apt_id AND a.building_id=b.building_id "
			       + " AND cmr.committee_id=c.committee_id  AND cmr.member_list_id=m.member_id AND c.society_id=:societyID";
		               
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("committeeID", committeeID);
			namedParameters.put("societyID", societyID);
			
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			        	MemberVO memberVO=new MemberVO();
			        	memberVO.setAptName(rs.getString("flatNo"));	
			        	memberVO.setBuildingName(rs.getString("committee_name"));
			        	memberVO.setMemberFullName(rs.getString("full_name"));
			            memberVO.setEmail(rs.getString("email"));
			            return memberVO;
			        }};

			        commityMember = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
			        
			        
			        logger.info("Size of commity member is  : " + commityMember.size() );
			        logger.debug("Exit :  public List getCommityMember(String committeeID)");
			}catch (Exception ex){
				logger.error("Exception in getCommityMember : "+ex);
				
			}
			return commityMember;
			
			
		}*/
	 
	 
	 public int addPaymentReminderEvent(EventForm evnFrm){
	    	int flag=0;
	    	String str="";
	    	com.emanager.server.society.valueObject.MemberVO memberVO=(com.emanager.server.society.valueObject.MemberVO) evnFrm.getMemberlist().get(0);

	        try{
	    	
	    	logger.debug("Entry : public int addPaymentReminderEvent(EventForm evnFrm)");
	    
					
				str = "insert into event_scheduler (member_id,society_id, event_type, temp_id,event_subject,emailFrequency,set_date,next_date,event_status,transaction_type,send_email,send_sms,onetime,email_id,mobile) " +
						"values (:memberID,:societyID,:eventType,:tempID,:subject ,:frequency,:setDate,:nextDate,:eventStatus ,:transactionType ,:sendEmail,:sendSMS, :oneTime,:email,:mobile)"; 
						                 
				
				Map hmap=new HashMap();
				hmap.put("memberID", evnFrm.getMemberID());
				hmap.put("societyID", evnFrm.getSocietyID());
				hmap.put("eventType", evnFrm.getEventType());
				hmap.put("tempID", evnFrm.getTempleteName());
				hmap.put("subject", evnFrm.getSubject());
				hmap.put("frequency", evnFrm.getFrequency());
				hmap.put("setDate", evnFrm.getSetDate());
				hmap.put("nextDate", evnFrm.getNextDate());
				hmap.put("eventStatus", evnFrm.getStatus());
				hmap.put("transactionType", evnFrm.getTransaction_type());
				hmap.put("sendEmail", evnFrm.getSendEmail());
				hmap.put("sendSMS", evnFrm.getSendSMS());
				hmap.put("oneTime" , evnFrm.getOnetime());
				hmap.put("email", memberVO.getEmail());
				hmap.put("mobile", memberVO.getMobile());
				
				
				flag=namedParameterJdbcTemplate.update(str,hmap);
	    	
	    	
	    	logger.debug("Entry : public int addPaymentReminderEvent(EventForm evnFrm)"+flag);
	        }  catch(Exception Ex){
	        logger.error("Exepection in public int addPaymentReminderEvent(EventForm evnFrm)"+Ex);		

	        }
	    	return flag;
	    } 
	 

	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}
	
	
}
