package com.emanager.server.commonUtils.dataaccessObject;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.emanager.server.commonUtils.valueObject.RecentUpdateVO;

public class RecentUpdateDAO {
	
	private JdbcTemplate  jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private static final Logger logger = Logger.getLogger(RecentUpdateDAO.class);
   

    public List getRecentUpdateListForAdmin(String strSocietyID,String type){
		 String strSQLQuery = ""; 
		 List listRecentUpdate = null;	
		try{
			logger.debug("Entry : public List getRecentUpdateListForAdmin(String strSocietyID,String type)");
			//1. SQL Query
			strSQLQuery = "  SELECT r.*," 
						 +"(  "
						 +"	CASE  "
							  + " WHEN r.group_id='0' THEN 'All' "
							  +	" WHEN r.group_type='G' THEN ( SELECT b.building_name FROM building_details b WHERE b.building_id=r.group_id ) "
							  + " WHEN r.group_type='C' THEN ( SELECT c.committee_name FROM committee_details c  WHERE c.committee_id=r.group_id ) "
		       	       		+ " END) AS grpNam "
		      			+ " FROM recent_updates r "
		      			+ " WHERE r.society_id =:society_id  GROUP BY update_id ORDER BY insert_date DESC LIMIT 12;";
			
			logger.debug("Query : " + strSQLQuery );
			// 2. Parameters.
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("society_id", strSocietyID);
			namedParameters.addValue("type", type);
			
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			        	RecentUpdateVO recentUpdateVO = new RecentUpdateVO();
			        	recentUpdateVO.setUpdateID(rs.getString("update_id"));
			        	recentUpdateVO.setSubject(rs.getString("subject"));
			        	recentUpdateVO.setDescription(rs.getString("description"));
			        	recentUpdateVO.setUpdateDate(rs.getString("update_date"));
			        	recentUpdateVO.setEmailSendFlag(rs.getInt("email_send_flag"));
			        	recentUpdateVO.setRecentType(rs.getString("type"));
			        	recentUpdateVO.setEventID(rs.getInt("event_id"));
			        	recentUpdateVO.setLastDate(rs.getDate("last_date"));
			        	recentUpdateVO.setInsertDate(rs.getString("insert_date"));
			        	recentUpdateVO.setPublishDate(rs.getString("mail_sent_date"));
			        	recentUpdateVO.setGroupType(rs.getString("group_type"));
			        	recentUpdateVO.setGroupID(rs.getString("group_id"));
			        	recentUpdateVO.setGroupName(rs.getString("grpNam"));
			        	//recentUpdateVO.setGridDisplayMessage();
			            return recentUpdateVO;
			        }};
		
			        listRecentUpdate= namedParameterJdbcTemplate.query(strSQLQuery,namedParameters,RMapper);
		
			        logger.debug("Exit : public List getRecentUpdateListForAdmin(String strSocietyID,String type)");
			 
			 
		 }catch(Exception Ex){			 
			 logger.error("Exception in getRecentUpdateListForAdmin : "+Ex);			 
		 }
		 
		 return listRecentUpdate;		 
			
		}
    
    public List getRecentUpdateListForMembers(String strSocietyID,String type){
		 String strSQLQuery = ""; 
		 List listRecentUpdate = null;	
		try{
			logger.debug("Entry : public List getRecentUpdateListForMembers(String strSocietyID,String type)");
			//1. SQL Query
			
			strSQLQuery = "  SELECT r.*," 
				 +"(  "
				 +"	CASE  "
					  + " WHEN r.group_id='0' THEN 'All' "
					  +	" WHEN r.group_type='G' THEN ( SELECT b.building_name FROM building_details b WHERE b.building_id=r.group_id ) "
					  + " WHEN r.group_type='C' THEN ( SELECT c.committee_name FROM committee_details c  WHERE c.committee_id=r.group_id ) "
       	       		+ " END) AS grpNam "
      			+ " FROM recent_updates r "
      			+ " WHERE r.society_id =:society_id  GROUP BY update_id ORDER BY insert_date DESC LIMIT 12;";
			
			logger.debug("Query : " + strSQLQuery );
			// 2. Parameters.
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("society_id", strSocietyID);
			namedParameters.addValue("type", type);
			
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			        	RecentUpdateVO recentUpdateVO = new RecentUpdateVO();
			        	recentUpdateVO.setUpdateID(rs.getString("update_id"));
			        	recentUpdateVO.setSubject(rs.getString("subject"));
			        	recentUpdateVO.setDescription(rs.getString("description"));
			        	recentUpdateVO.setUpdateDate(rs.getString("update_date"));
			        	recentUpdateVO.setEmailSendFlag(rs.getInt("email_send_flag"));
			        	recentUpdateVO.setRecentType(rs.getString("type"));
			        	recentUpdateVO.setEventID(rs.getInt("event_id"));
			        	recentUpdateVO.setLastDate(rs.getDate("last_date"));
			        	recentUpdateVO.setInsertDate(rs.getString("insert_date"));
			        	recentUpdateVO.setPublishDate(rs.getString("mail_sent_date"));
			        	recentUpdateVO.setGroupType(rs.getString("group_type"));
			        	recentUpdateVO.setGroupID(rs.getString("group_id"));
			        	recentUpdateVO.setGroupName(rs.getString("grpNam"));
			        	//recentUpdateVO.setGridDisplayMessage();
			            return recentUpdateVO;
			        }};
		
			        listRecentUpdate= namedParameterJdbcTemplate.query(strSQLQuery,namedParameters,RMapper);
		
			        logger.debug("Exit : public List getRecentUpdateListForMembers(String strSocietyID,String type)");
			 
			 
		 }catch(Exception Ex){			 
			 logger.error("Exception in getRecentUpdateListForMembers : " + Ex);			 
		 }
		
		 return listRecentUpdate;		 
			
		}
    
    
    public int insertRecentUpdate(RecentUpdateVO recentUpdateVO){
		 
		int insertFlag = 0;
		String str=null;
		 Date currentDatetime = new Date(System.currentTimeMillis());   
		    java.sql.Date sqlDate = new java.sql.Date(currentDatetime.getTime());   
			java.sql.Timestamp timestamp = new java.sql.Timestamp(currentDatetime.getTime());
		
		try{			
			logger.debug("Entry : public int insertRecentUpdate(RecentUpdateVO recentUpdateVO)");
			 
			if(recentUpdateVO.getEmailSendFlag()==0){
			str = "insert into recent_updates (society_id, type, update_date,subject,description,updateby_id,updateby_name,email_send_flag,last_date,insert_date) values (:societyID,:recType,:updateDate,:subject,:description ,:updatedBy,:updateName,:emailSendFlag ,:lastDate ,:insertDate)"; 
		                 
			}else if(recentUpdateVO.getEmailSendFlag()!=0){
				
			str = "insert into recent_updates (society_id, type, update_date,subject,description,updateby_id,updateby_name,email_send_flag,last_date,insert_date,mail_sent_date,group_type,group_id) values (:societyID,:recType,:updateDate,:subject,:description ,:updatedBy,:updateName,:emailSendFlag ,:lastDate ,:insertDate,:updateDate, :grpName,:grNameID)"; 
					                 
			}//,,  , 
			Map hmap=new HashMap();
			hmap.put("societyID", recentUpdateVO.getSocietyID());
			hmap.put("recType", recentUpdateVO.getRecentType());
			hmap.put("updateDate", timestamp);
			hmap.put("subject", recentUpdateVO.getSubject());
			hmap.put("description", recentUpdateVO.getDescription());
			hmap.put("updatedBy", recentUpdateVO.getUpdateMemeberID());
			hmap.put("updateName", recentUpdateVO.getUpdateBy());
			hmap.put("emailSendFlag", recentUpdateVO.getEmailSendFlag());
			hmap.put("lastDate", recentUpdateVO.getEndDate());
			hmap.put("insertDate", recentUpdateVO.getInsertDate());
			hmap.put("grpName" , recentUpdateVO.getGroupType());
			hmap.put("grNameID" , recentUpdateVO.getGroupID());
			
			insertFlag=namedParameterJdbcTemplate.update(str,hmap);
						
			logger.debug("Exit : public int insertRecentUpdate(RecentUpdateVO recentUpdateVO)");
			
					 
		 }catch(Exception Ex){		 
			 logger.error("Exception in insertRecentUpdate : "+Ex);			 
		 }
		
		 return insertFlag;
		 
			
		}
    
    public int updateRecentUpdate(RecentUpdateVO recentUpdateVO,int noticeID){
		int updateFlag=0;
		 String strQuery=null;
		 Date currentDatetime = new Date(System.currentTimeMillis());   
		    java.sql.Date sqlDate = new java.sql.Date(currentDatetime.getTime());   
			java.sql.Timestamp timestamp = new java.sql.Timestamp(currentDatetime.getTime());
		try{
			
		logger.debug("Entry :  public int updateRecentUpdate(RecentUpdateVO recentUpdateVO,int noticeID)"+recentUpdateVO.getRecentType());
		if(recentUpdateVO.getEmailSendFlag()==0){
			 strQuery = "UPDATE recent_updates SET  description=:message, email_send_flag=:status ,last_date=:lastDate , update_date=:updateDate ,type=:type  WHERE  update_id="+noticeID+";";
		}else if(recentUpdateVO.getEmailSendFlag()!=0){
		  strQuery = "UPDATE recent_updates SET mail_sent_date=:updateDate, description=:message, email_send_flag=:status ,last_date=:lastDate , update_date=:updateDate ,type=:type, group_type=:grpName ,group_id=:grNameID  WHERE  update_id="+noticeID+";";
		}
	    Map namedParam=new HashMap();
		namedParam.put("message",recentUpdateVO.getDescription());
		namedParam.put("status", recentUpdateVO.getEmailSendFlag());
		namedParam.put("updateDate", timestamp);
		namedParam.put("lastDate", recentUpdateVO.getEndDate());
		namedParam.put("type", recentUpdateVO.getRecentType());
		namedParam.put("grpName" , recentUpdateVO.getGroupType());
		namedParam.put("grNameID" , recentUpdateVO.getGroupID());
		
		updateFlag=this.namedParameterJdbcTemplate.update(strQuery ,namedParam);		
		
		logger.debug("Exit :  public int updateRecentUpdate(RecentUpdateVO recentUpdateVO,int noticeID) ");
		}
		catch(Exception e){
			logger.error("Exception in updating messages::" +e);
		}
		return updateFlag;
	}
    
    

    public int deleteDrafts(int noticeID){
		int success=0;
		try {
			logger.debug("Entry : public int deleteDrafts(int noticeID)");
			
			String query="DELETE FROM recent_updates WHERE update_id="+noticeID+";";
					
			success=this.jdbcTemplate.update(query);
			
			logger.debug("Exit : public int deleteDrafts(int noticeID)"+success);
		} catch (Exception e) {
			
			logger.error("Exception in deleteDrafts : "+e);
		}
		return success;
		
	}
    
    

	/**
	 * @return the jdbcTemplate
	 */
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	/**
	 * @param jdbcTemplate the jdbcTemplate to set
	 */
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	/**
	 * @return the namedParameterJdbcTemplate
	 */
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	/**
	 * @param namedParameterJdbcTemplate the namedParameterJdbcTemplate to set
	 */
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
		
	
	
	

}
