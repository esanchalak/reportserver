package com.emanager.server.commonUtils.services;

import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.commonUtils.domainObject.RecentUpdateDomain;
import com.emanager.server.commonUtils.valueObject.RecentUpdateVO;

public class RecentUpdateService {
	

	public RecentUpdateDomain recentUpdateDomain;
	List listRecentUpdate  = null;
	private static final Logger logger = Logger.getLogger(RecentUpdateService.class);
	
public List getRecentUpdateDetailsForAdmin(String strSocietyID,String recentType){	 
			
	try{
		logger.debug("Entry : public List getRecentUpdateDetailsForAdmin(String strSocietyID,String recentType)");
		listRecentUpdate = recentUpdateDomain.getRecentUpdateListForAdmin(strSocietyID,recentType);
		 
		 logger.debug("List Size " + listRecentUpdate.size());
		 logger.debug("Exit : public List getRecentUpdateDetailsForAdmin(String strSocietyID,String recentType)");
		 
	 }catch(Exception Ex){		 
		 logger.error("Exception in getRecentUpdateDetailsForAdmin : "+Ex);		 
	 }
	
	 return listRecentUpdate;
	 
		
	}
	
	public List getRecentUpdateDetailsForMembers(String strSocietyID,String recentType){
		
		try{
			logger.debug("Entry : public List getRecentUpdateDetailsForMembers(String strSocietyID,String recentType)");
			listRecentUpdate = recentUpdateDomain.getRecentUpdateListForMembers(strSocietyID,recentType);
			 
			 logger.debug("List Size " + listRecentUpdate.size());
			 logger.debug("Exit : public List getRecentUpdateDetailsForMembers(String strSocietyID,String recentType)");
			 
		 }catch(Exception Ex){			 
			 logger.error("Exception in getRecentUpdateDetailsForMembers : "+Ex);			 
		 }
		
		 return listRecentUpdate;		 
			
		}
	
	public int insertRecentUpdateDetails(RecentUpdateVO recentUpdateVO){
		 
		int insertFlag = 0;
		
		try{
			logger.debug("Entry: public insertRecentUpdateDetails(RecentUpdateVO recentUpdateVO)");
			 insertFlag = recentUpdateDomain.insertRecentUpdate(recentUpdateVO);
			 
			 logger.debug("Exit: public insertRecentUpdateDetails(RecentUpdateVO recentUpdateVO)");
			 
		 }catch(Exception Ex){
			 logger.error("Exception in insertRecentUpdateDetails : "+Ex);			 
		 }
		
		 return insertFlag;
		 
			
		}
	
	public int updateRecentUpdateDetails(RecentUpdateVO recentUpdateVO,int noticeID){
		 
		int updateFlag = 0;		
		try{
			logger.debug("Entry : public int updateRecentUpdateDetails(RecentUpdateVO recentUpdateVO,int noticeID)");
			 updateFlag = recentUpdateDomain.updateRecentUpdate(recentUpdateVO, noticeID);
			 
			 logger.debug("Exit: public int updateRecentUpdateDetails(RecentUpdateVO recentUpdateVO,int noticeID)");
			 
		 }catch(Exception Ex){
			 logger.error("Exception in updateRecentUpdateDetails : "+Ex);			 
		 }
		
		 return updateFlag;
		 
			
		}

	public int deleteDrafts(int noticeID){
		int success=0;
		logger.debug("Entry : public int deleteDrafts(int noticeID)");
		
		success=recentUpdateDomain.deleteDrafts(noticeID);
		
		logger.debug("Exit : public int deleteDrafts(int noticeID)");
		return success;
		
	}
	

	/**
	 * @return the recentUpdateDomain
	 */
	public RecentUpdateDomain getRecentUpdateDomain() {
		return recentUpdateDomain;
	}


	/**
	 * @param recentUpdateDomain the recentUpdateDomain to set
	 */
	public void setRecentUpdateDomain(RecentUpdateDomain recentUpdateDomain) {
		this.recentUpdateDomain = recentUpdateDomain;
	}
	
	
	

}
