package com.emanager.server.commonUtils.services;

import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.commonUtils.domainObject.AddressDomain;
import com.emanager.server.commonUtils.valueObject.AddressVO;
//import com.sun.org.apache.xml.internal.resolver.helpers.PublicId;

public class AddressService {
	
	public AddressDomain addressDomain;
	private static final Logger logger = Logger.getLogger(AddressService.class);
	
	public AddressVO getAddressDetails(int strMemberID,String aptName,String strPersonCategory,int societyID){
		 
		AddressVO addrVO = new AddressVO();	
		try{
			
			logger.debug("inside getAddressDetails(String strMemberID,String strCategoryID,String strPersonCategory)");
			 
			addrVO = addressDomain.getAddressDetails(strMemberID, aptName, strPersonCategory,societyID);			
			
						
			logger.debug("outside getAddressDetails  Address ID : " + addrVO.getAddID());
			 
		 }	catch(Exception Ex){
			 
			 logger.error("domain error");
			 
		 }
		
		 return addrVO;
		 
			
		}
	
	
  public List getAddressDetailsRenter(int strRenterID,String strPersonCategory,int societyId,String aptName){
		
		List adressList=null;
	
		try{
		 
		logger.debug("Entry : public List getAddressDetailsRenter(String strRenterID,String strPersonCategory)");
		
		adressList = addressDomain.getAddressDetailsRenter(strRenterID, strPersonCategory,societyId,aptName);		
		
		logger.debug("Exit : public List getAddressDetailsRenter(String strRenterID,String strPersonCategory)"+adressList.size());
		 
	 }catch(Exception Ex){		 
		 logger.error("Exception in getAddressDetailsRenter : "+Ex);		 
	 }
	
	 return adressList;
	 
		
	}
	
	
	
	public List getAddressDetailsList(int strPersonID,String strPersonCategory,int societyID,String aptName) {
		
		List adressList=null;
	
		try {
			logger.debug("Entry public List getAddressDetailsList(String strPersonID,String strPersonCategory)");
		
			adressList=addressDomain.getAddressDetailsList(strPersonID,strPersonCategory,societyID,aptName);

			logger.debug("Exit public List getAddressDetailsList(String strPersonID,String strPersonCategory)"+adressList.size());

		}
		 catch (Exception e) {
			logger.error("Exception in getAddressDetailsList : "+e);
		}

		return adressList;

	}
	
	
	
	public int addUpdateAddressDetails(AddressVO addressVO,int strMemberID){
		 int flag=0;

		try{
			logger.debug("Entry : public int addUpdateAddressDetails(AddressVO addressVO,String strMemberID)");
			 flag =  addressDomain.addUpdateAddressDetails(addressVO,strMemberID);
			 logger.debug("Exit : public int addUpdateAddressDetails(AddressVO addressVO,String strMemberID)");
			 
		 }catch(Exception Ex){			 
			 logger.error("Exception in getAddressDetailsList : "+Ex);			 
		 }
		
		 return flag;
		 
			
		}
	
	
	
public int removeContactDetails(String addressID){
		
		int success=0;
		try{
			logger.debug("Entry : public int removeContactDetails(String addressID)");
			
			success=addressDomain.removeContactDetails(addressID);			
		
			logger.debug("Exit : public int removeContactDetails(String addressID)"+success);
       }catch(Exception Ex){			 
	        logger.error("Exception in removeContactDetails : "+Ex);			 
       }
	return success;	
	}

public List getSocietyAddressDetails(int societyID,String strCategoryID,String strPermanantCategory){
	
	List addressList=null;
	try
	{
		logger.debug("Entry : public List getSocietyAddressDetails(String societyID,String strCategoryID,String strPermanantCategory) ");
		addressList=addressDomain.getSocietyAddressDetails(societyID, strCategoryID, strPermanantCategory);		
	}
	catch(Exception ex){
		logger.error("Exception in getSocietyAddressDetails : "+ex);
		
	}
	logger.debug("Exit : public List getSocietyAddressDetails(String societyID,String strCategoryID,String strPermanantCategory) ");
	return addressList;
	
}

	public AddressDomain getAddressDomain() {
		return addressDomain;
	}


	public void setAddressDomain(AddressDomain addressDomain) {
		this.addressDomain = addressDomain;
	}
	
	

}
