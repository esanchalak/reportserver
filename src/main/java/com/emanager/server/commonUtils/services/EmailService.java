package com.emanager.server.commonUtils.services;

import java.io.Serializable;

import org.apache.log4j.Logger;

import com.emanager.server.commonUtils.domainObject.EmailDomain;
import com.emanager.server.commonUtils.valueObject.EventForm;
import com.emanager.server.commonUtils.valueObject.RecentUpdateVO;
import com.emanager.server.financialReports.valueObject.RptTransactionVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.BankInfoVO;


public class EmailService {
	private static final Logger logger = Logger.getLogger(EmailService.class);
    public EmailDomain emailDomain;
    public SocietyService societyServiceBean;

   
	public RecentUpdateVO sendScheduledEmail(EventForm eventVO,RecentUpdateVO updateVO){

      int status = 0;

    try{

         logger.debug("Entry : public int sendScheduledEmail(EventVO eventVO)");

         updateVO=emailDomain.sendScheduledEmail(eventVO,updateVO);

         logger.debug("Exit : public int sendScheduledEmail(EventVO eventVO)");

     }  catch(Exception Ex){


     }

     return updateVO;
    }
    
    public RecentUpdateVO updateScheduledEmail(final EventForm eventVO,RecentUpdateVO updateVO,int eventID ){
        int  status = 0;
          
          logger.debug("Entry : public updateScheduledEmail(final EventVO eventVO,RecentUpdateVO updateVO)");
          

       updateVO=emailDomain.updateScheduledEmail(eventVO, updateVO,eventID);

              
          logger.debug("Exit : public updateScheduledEmail(final EventVO eventVO,RecentUpdateVO updateVO)");

           return updateVO;


          }
    
    public EventForm getEventDetails(int eventID){
    	EventForm eventVO=null;
    	logger.debug("Entry : public EventVO getEventDetails(int eventID)");
    		
    	eventVO=emailDomain.getEventDetails(eventID);
    	
    	logger.debug("Exit : public EventVO getEventDetails(int eventID)");
    	
    	return eventVO;
    }
    
    
 /*   public int sendEmailNote(EventForm evnFrm,String societyName,String email){
    	int flag=0;

        try{
    	
    	logger.debug("Entry : public void sendEmail(EventForm evnFrm,String societyName,String email)");
    	
    	flag=emailDomain.sendEmailNote(evnFrm,societyName,email);
    	
    	logger.debug("Entry : public void sendEmail(EventForm evnFrm,String societyName,String email)");
        }  catch(Exception Ex){
        logger.error(Ex);		

        }
    	return flag;
    }
    
    public int sendEmailDue(RptTransactionVO tranVO,String societyName,int societyId,String email,String month,String year){
    	int flag=0;
        BankInfoVO bankVO=new BankInfoVO();
        try{
    	
    	logger.debug("Entry : public int sendEmailDue(RptTransactionVO tranVO,String societyName,String societyId,String email,String month,String year)");
    	bankVO=societyServiceBean.getBankDetails(societyId);
    	flag=emailDomain.sendEmailDue(tranVO,societyName,""+societyId,email,bankVO,month,year);
    	
    	logger.debug("Entry : public int sendEmailDue(RptTransactionVO tranVO,String societyName,String societyId,String email,String month,String year)");
        }  catch(Exception Ex){
        logger.error(Ex);		

        }
    	return flag;
    }*/
    
    public int addPaymentReminderEvent(EventForm evnFrm){
    	int flag=0;

        try{
    	
    	logger.debug("Entry : public int addPaymentReminderEvent(EventForm evnFrm)");
    	
    	flag=emailDomain.addPaymentReminderEvent(evnFrm);
    	
    	logger.debug("Entry : public int addPaymentReminderEvent(EventForm evnFrm)");
        }  catch(Exception Ex){
        	  logger.error("Exepection in public int addPaymentReminderEvent(EventForm evnFrm)"+Ex);	

        }
    	return flag;
    }
    
    
    public int sendEmail(final Serializable obj){
    	int flag=0;

        try{
    	
    	logger.debug("Entry : public void sendEmail(final Serializable obj)"+obj.getClass());
    	
    	flag=emailDomain.sendEmail(obj);
    	
    	logger.debug("Entry : public void sendEmail(final Serializable obj)");
        }  catch(Exception Ex){
        logger.error(Ex);		

        }
    	return flag;
    }
    
    public EmailDomain getEmailDomain()
    {
        return emailDomain;
    }


    public void setEmailDomain(EmailDomain emailDomain)
    {
        this.emailDomain = emailDomain;
    }

	public SocietyService getSocietyServiceBean() {
		return societyServiceBean;
	}

	public void setSocietyServiceBean(SocietyService societyServiceBean) {
		this.societyServiceBean = societyServiceBean;
	}

	
    
    
  


  



}
