package com.emanager.server.commonUtils.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;

import com.emanager.server.commonUtils.domainObject.MessageDomain;
import com.emanager.server.commonUtils.valueObject.MessageVO;
import com.emanager.server.society.valueObject.MemberVO;


public class MessageService {
	
	public MessageDomain messageDomain;
	List listMessages  = null;
	private static final Logger logger = Logger.getLogger(MessageService.class);
	
	
	public List getMessages(int memberID,String userType,int societyID) {
		
		logger.debug("Entry : public List getMessages(String memberID,String userType,int societyID)");
		try {
			listMessages = messageDomain.getMessages(memberID,userType,societyID);
		} catch (Exception e) {
			logger.error("Exception in getMessages : "+e);
		}
		
		logger.debug("Exit : public List getMessages(String memberID,String userType,int societyID)");
		return listMessages;
		
	}
    
	public List getMessagesForClosedStatus(int memberID,String userType,int societyID){
		
		logger.debug("Entry : public List getMessagesForClosedStatus(String userType,int societyID)");
		try {
			listMessages = messageDomain.getMessagesForClosedStatus(memberID,userType, societyID);
		} catch (Exception e) {
			logger.error("Exception in getMessagesForClosedStatus : "+e);
		}
		
		logger.debug("Exit : public List getMessagesForClosedStatus(String userType,int societyID)");
		return listMessages;
		
	}
	/*public int insertNewMessages(MessageVO messageObj) {
		int insertFlag=0;
		try{
		logger.debug("Entry : public int insertNewMessages(MessageVO messageObj) ");
		
		logger.info("Recieved Request for new Message.");
		
		insertFlag=messageDomain.insertNewMessage(messageObj);
		
		logger.debug("Exit : public int insertNewMessages(MessageVO messageObj) ");
		} catch (Exception e) {
			logger.error("Exception in insertNewMessages : "+e);
		}
		return insertFlag;
	}
	
	
	public int updateMessage(MessageVO messageObj,String memberID, String messageId){
		int insertFlag=0;
		try{
		logger.debug("Entry : public int updateMessage(MessageVO messageObj,String memberID,, String messageId)");
		
		insertFlag=messageDomain.updateMessage(messageObj, memberID,messageId);
		
		logger.debug("Exit : public int updateMessage(MessageVO messageObj,String memberID,, String messageId)"+insertFlag);
		}catch (Exception e) {
			logger.error("Exception in updateMessage : "+e);
		}
		return insertFlag;
	}*/
	
	public List getCategorylist(){
		List categoryList=null;
		
			try {
				logger.debug("Entry : public List getCategorylist() ");
						
				categoryList=messageDomain.getCategorylist();
			
				logger.debug("Exit : public List getCategorylist() ");
			} catch (DataAccessException e) {
				logger.error("Exception in getCategorylist : "+e);
			}
		return categoryList;
		}
	
	 public List getCategorylistUpdate(){
			
    	 List categoryList=null;
 		
			try {
				logger.debug("Entry : public List getCategorylistUpdate() ");
						
				categoryList=messageDomain.getCategorylist();
				
				logger.debug("Exit : public List getCategorylistUpdate() ");
			} catch (DataAccessException e) {
				logger.error("Exception in getCategorylistUpdate : "+e);
			}
		return categoryList;
	  }
		
	public Boolean sendMessage(MemberVO memberVO) {
		Boolean success=false;
		try{
		logger.debug("Entry : public boolean sendMessage(MemberVO memberVO) ");
		
		logger.info("Recieved Request for new Message.");
		
		success=messageDomain.sendMessage(memberVO);
		
		logger.debug("Exit : public boolean sendMessage(MemberVO memberVO) ");
		} catch (Exception e) {
			logger.error("Exception in sendMessage(MemberVO memberVO) : "+e);
		}
		return success;
	}
	
	
	public List countCategories(String societyId){
		List categoryList=null;
		
			try {
				logger.debug("Entry : public List countCategories() ");
			
				
				    categoryList=messageDomain.countCategories(societyId);
				

				logger.debug("Exit : public List countCategories(String societyId) "+categoryList.size());
			} catch (DataAccessException e) {
				logger.error("Exception in countCategories(String societyId) : "+e);
			}
		return categoryList;
		}

	public void setMessageDomain(MessageDomain messageDomain) {
		this.messageDomain = messageDomain;
	}

}
