package com.emanager.server.commonUtils.valueObject;

import java.math.BigDecimal;

public class ImportMemberVO {

	// @ExcelCellName("Building/Group")
	    public String buildingName;

	    //@ExcelCellName("Unit No")
	    public String unitName;

	    //@ExcelCellName("Title")
	    public String title;

	   // @ExcelCellName("Name")
	    public String primaryName;
	    
	   // @ExcelCellName("Email")
	    public String primayEmail;
	    
	   // @ExcelCellName("Mobile No1")
	    public String primaryMobile1;
	    
	   // @ExcelCellName("Mobile No2")
	    public String primaryMobile2;
	    
	   // @ExcelCellName("Area(Sq Ft)")
	    public BigDecimal area;
	    
	   // @ExcelCellName("AssociateTitle")
		 public String associateTitle;

		  //  @ExcelCellName("AssociateMemberName")
		    public String associateMemberName;
		    
		   // @ExcelCellName("AssociateEmail")
		    public String associateEmail;
		    
		   // @ExcelCellName("AMobile No1")
		    public String associateMobile1;
		    
		   // @ExcelCellName("AMobile No2")
		    public String associateMobile2;
	
}
