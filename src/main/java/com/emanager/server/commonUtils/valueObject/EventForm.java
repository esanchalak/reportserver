package com.emanager.server.commonUtils.valueObject;


import java.io.Serializable;
import java.util.List;

public class EventForm implements Serializable {

	
	int eventID;
	int memberID;
	String eventType;
	int templetID;
	String templeteName;
	String setDate;
	String nextDate;
	String lastDate;
	String reminderType;
	String SocietyName;
	String societyID;
	String subject;
	List memberlist; 
	List memberVO;
	String transaction_type;
	String htmlString;
	String frequency;
	String status;
	String message;
	String content;
	int sendEmail;
	int sendSMS;
	int onetime;

	public int getEventID() {
		return eventID;
	}

	public void setEventID(int eventID) {
		this.eventID = eventID;
	}

	public int getTempletID() {
		return templetID;
	}

	public void setTempletID(int templetID) {
		this.templetID = templetID;
	}

	public String getTempleteName() {
		return templeteName;
	}

	public void setTempleteName(String templeteName) {
		this.templeteName = templeteName;
	}

	public String getSetDate() {
		return setDate;
	}

	public void setSetDate(String setDate) {
		this.setDate = setDate;
	}

	public String getNextDate() {
		return nextDate;
	}

	public void setNextDate(String nextDate) {
		this.nextDate = nextDate;
	}

	public String getLastDate() {
		return lastDate;
	}

	public void setLastDate(String lastDate) {
		this.lastDate = lastDate;
	}

	public String getReminderType() {
		return reminderType;
	}

	public void setReminderType(String reminderType) {
		this.reminderType = reminderType;
	}

	public String getSocietyName() {
		return SocietyName;
	}

	public void setSocietyName(String societyName) {
		SocietyName = societyName;
	}

	public String getSocietyID() {
		return societyID;
	}

	public void setSocietyID(String societyID) {
		this.societyID = societyID;
	}

	
	public String getEventType() {
		return eventType;
	}

	/**
	 * @param eventType the eventType to set
	 */
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the memberVO
	 */
	public List getMemberVO() {
		return memberVO;
	}

	/**
	 * @param memberVO the memberVO to set
	 */
	public void setMemberVO(List memberVO) {
		this.memberVO = memberVO;
	}

	

	/**
	 * @return the memberlist
	 */
	public List getMemberlist() {
		return memberlist;
	}

	/**
	 * @param memberlist the memberlist to set
	 */
	public void setMemberlist(List memberlist) {
		this.memberlist = memberlist;
	}

	/**
	 * @return the transaction_type
	 */
	public String getTransaction_type() {
		return transaction_type;
	}

	/**
	 * @param transactionType the transaction_type to set
	 */
	public void setTransaction_type(String transactionType) {
		transaction_type = transactionType;
	}

	/**
	 * @return the htmlString
	 */
	public String getHtmlString() {
		return htmlString;
	}

	/**
	 * @param htmlString the htmlString to set
	 */
	public void setHtmlString(String htmlString) {
		this.htmlString = htmlString;
	}



	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getMemberID() {
		return memberID;
	}

	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getOnetime() {
		return onetime;
	}

	public void setOnetime(int onetime) {
		this.onetime = onetime;
	}

	public int getSendEmail() {
		return sendEmail;
	}

	public void setSendEmail(int sendEmail) {
		this.sendEmail = sendEmail;
	}

	public int getSendSMS() {
		return sendSMS;
	}

	public void setSendSMS(int sendSMS) {
		this.sendSMS = sendSMS;
	}

	

	
}
