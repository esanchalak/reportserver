package com.emanager.server.commonUtils.valueObject;

import java.util.Date;




public class RecentUpdateVO {
	
	String updateMemeberID;
	String updateID;
	String updateDate;
	String updateBy;
	String subject;
	String description;
	String gridDisplayMessage;
	String societyID;
	String buildingID;
	String nextDate;
	String frequency;
	String setDate;
	String endDate;
	int emailSendFlag;
	String recentType;
	int eventID;
	Date lastDate;
	String insertDate;
	String societyName;
	String publishDate;
	String groupID;
	String groupType;
	String groupName;
	
	
	
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupType() {
		return groupType;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	public String getGroupID() {
		return groupID;
	}
	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}
	public String getRecentType() {
		return recentType;
	}
	public void setRecentType(String recentType) {
		this.recentType = recentType;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return the updateBy
	 */
	public String getUpdateBy() {
		return updateBy;
	}
	/**
	 * @param updateBy the updateBy to set
	 */
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	/**
	 * @return the updateDate
	 */
	public String getUpdateDate() {
		return updateDate;
	}
	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	/**
	 * @return the updateID
	 */
	public String getUpdateID() {
		return updateID;
	}
	/**
	 * @param updateID the updateID to set
	 */
	public void setUpdateID(String updateID) {
		this.updateID = updateID;
	}
	/**
	 * @return the gridDisplayMessage
	 */
	public String getGridDisplayMessage() {
		return gridDisplayMessage;
	}
	/**
	 * @param gridDisplayMessage the gridDisplayMessage to set
	 */
	public void setGridDisplayMessage(String gridDisplayMessage) {
		this.gridDisplayMessage = gridDisplayMessage;
	}
	public String getSocietyID() {
		return societyID;
	}
	public void setSocietyID(String societyID) {
		this.societyID = societyID;
	}
		
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
		
	public String getSetDate() {
		return setDate;
	}
	public void setSetDate(String setDate) {
		this.setDate = setDate;
	}
	public String getNextDate() {
		return nextDate;
	}
	public void setNextDate(String nextDate) {
		this.nextDate = nextDate;
	}
	public int getEmailSendFlag() {
		return emailSendFlag;
	}
	public void setEmailSendFlag(int emailSendFlag) {
		this.emailSendFlag = emailSendFlag;
	}
	public int getEventID() {
		return eventID;
	}
	public void setEventID(int eventID) {
		this.eventID = eventID;
	}
	public String getUpdateMemeberID() {
		return updateMemeberID;
	}
	public void setUpdateMemeberID(String updateMemeberID) {
		this.updateMemeberID = updateMemeberID;
	}
	public Date getLastDate() {
		return lastDate;
	}
	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}
	public String getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(String insertDate) {
		this.insertDate = insertDate;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	
	public String getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}
	public String getBuildingID() {
		return buildingID;
	}
	public void setBuildingID(String buildingID) {
		this.buildingID = buildingID;
	}
	
	
	

}
