package com.emanager.server.commonUtils.valueObject;


/**
 * @author Niranjan Arude
 * Date  July 24, 2007
 */
public class AddressVO 
{
	private String person_ID;
	private String person_category;
	private int addID;
	private String categoryID ;
	private String addLine1;
	private String addLine2;
	private String nearestLandMark;
	private String zip;
	private String city ;
	private String state ;
	private String comments;
	private String email;
	private String landline;
	private String mobile;
	private String address;
	private String concatAddr;
	private String countryCode ;
    private Boolean isSocietyAddress;

	public Boolean getIsSocietyAddress() {
		return isSocietyAddress;
	}
	public void setIsSocietyAddress(Boolean isSocietyAddress) {
		this.isSocietyAddress = isSocietyAddress;
	}
	public int getAddID() {
		return addID;
	}
	public void setAddID(int addID) {
		this.addID = addID;
	}
	public String getConcatAddr() {
		return concatAddr;
	}
	public void setConcatAddr(String concatAddr) {
		this.concatAddr = concatAddr;
	}
	public String getPerson_category() {
		return person_category;
	}
	public void setPerson_category(String person_category) {
		this.person_category = person_category;
	}
	public String getPerson_ID() {
		return person_ID;
	}
	public void setPerson_ID(String person_ID) {
		this.person_ID = person_ID;
	}
	
	public String getAddLine1() {
		return addLine1;
	}
	public void setAddLine1(String addLine1) {
		this.addLine1 = addLine1;
	}
	public String getAddLine2() {
		return addLine2;
	}
	public void setAddLine2(String addLine2) {
		this.addLine2 = addLine2;
	}
	public String getCategoryID() {
		return categoryID;
	}
	public void setCategoryID(String categoryID) {
		this.categoryID = categoryID;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLandline() {
		return landline;
	}
	public void setLandline(String landline) {
		this.landline = landline;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getNearestLandMark() {
		return nearestLandMark;
	}
	public void setNearestLandMark(String nearestLandMark) {
		this.nearestLandMark = nearestLandMark;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}




  
	
	
}