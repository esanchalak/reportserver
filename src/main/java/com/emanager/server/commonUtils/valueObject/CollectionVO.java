package com.emanager.server.commonUtils.valueObject;

public class CollectionVO {
	
	private String data;
	private String label;
	
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

}
