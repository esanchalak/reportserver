package com.emanager.server.commonUtils.valueObject;


public class MessageVO {

	String memberID;
	int  societyID;
	String subject;
	String message;
	String comments;
	String status;
	String insertDate;
	String updateDate;
	String memberName;
	String msgType;
	String msgID;
	String email;
	String userType;
	String societyName;
	int openCounter;
	int processingCounter;
	int closedCounter;
	int escCounter;
	String category_id;
	String category_name;
	
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(String insertDate) {
		this.insertDate = insertDate;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getMsgID() {
		return msgID;
	}
	public void setMsgID(String msgID) {
		this.msgID = msgID;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	public int getClosedCounter() {
		return closedCounter;
	}
	public void setClosedCounter(int closedCounter) {
		this.closedCounter = closedCounter;
	}
	public int getOpenCounter() {
		return openCounter;
	}
	public void setOpenCounter(int openCounter) {
		this.openCounter = openCounter;
	}
	public int getProcessingCounter() {
		return processingCounter;
	}
	public void setProcessingCounter(int processingCounter) {
		this.processingCounter = processingCounter;
	}
	public String getCategory_id() {
		return category_id;
	}
	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public int getEscCounter() {
		return escCounter;
	}
	public void setEscCounter(int escCounter) {
		this.escCounter = escCounter;
	}
}
