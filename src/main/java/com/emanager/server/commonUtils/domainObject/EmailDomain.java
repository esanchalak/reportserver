package com.emanager.server.commonUtils.domainObject;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.log4j.Logger;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import com.emanager.server.commonUtils.dataaccessObject.EmailDAO;
import com.emanager.server.commonUtils.valueObject.EventForm;
import com.emanager.server.commonUtils.valueObject.RecentUpdateVO;
import com.emanager.server.financialReports.valueObject.RptTransactionVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceDetailsVO;
import com.emanager.server.society.valueObject.BankInfoVO;
import com.emanager.server.society.valueObject.SocietyVO;


public class EmailDomain {
	private static final Logger logger = Logger.getLogger(EmailDomain.class);

	EmailDAO emailDAO;
	JmsTemplate eventJMSTemplet;
	HtmlTableGenerator htmlTable=new HtmlTableGenerator();
	DateUtility dateUtility=new DateUtility();

    public RecentUpdateVO sendScheduledEmail(final EventForm eventVO,RecentUpdateVO updateVO){
      int  status = 0;
        
        logger.debug("Entry : public int sendScheduledEmail(final EventVO eventVO");
        

     updateVO=emailDAO.sendScheduledEmail(eventVO,updateVO);

            
        logger.info("Exit : public int sendScheduledEmail(final EventVO eventVO"+status);

         return updateVO;


        }
    
    public RecentUpdateVO updateScheduledEmail(final EventForm eventVO,RecentUpdateVO updateVO,int eventID){
        int  status = 0;
          
          logger.debug("Entry : public updateScheduledEmail(final EventVO eventVO,RecentUpdateVO updateVO)");
          

       updateVO=emailDAO.updateScheduledEmail(eventVO, updateVO,eventID);

              
          logger.info("Exit : public updateScheduledEmail(final EventVO eventVO,RecentUpdateVO updateVO)");

           return updateVO;


          }
    
    public EventForm getEventDetails(int eventID){
    	EventForm eventVO=null;
    	logger.debug("Entry : public EventVO getEventDetails(int eventID)");
    		
    	eventVO=emailDAO.getEventDetails(eventID);
    	
    	logger.debug("Exit : public EventVO getEventDetails(int eventID)");
    	
    	return eventVO;
    }
   
    /* public int sendEmailNote(EventForm eventForm,String societyName,String email){
    	int flag=0;
    	com.esanchalak.email.common.valueObject.EventVO eventVo=new com.esanchalak.email.common.valueObject.EventVO();
        try{
        	ConfigManager c=new ConfigManager();
        	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");	
        	
    	logger.debug("Entry : public void sendEmailNote(EventForm evnFrm,String societyName,String email)");
    	eventVo.setMemberID(eventForm.getMemberID());
    	eventVo.setSocietyId(Integer.parseInt(eventForm.getSocietyID()));
    	eventVo.setTempleteName(c.getPropertiesValue("template.newNotice"));
    	eventVo.setEventType("User");
    	logger.debug("member template "+eventVo.getTempleteName());
    	eventVo.setEventID(0);
    	java.util.Date date = formatter.parse(eventForm.getSetDate());
    	java.sql.Date sqlStartDate = new Date(date.getTime());  
    	eventVo.setSetDate(sqlStartDate);
    	eventVo.setSubject(eventForm.getSubject());
    	eventVo.setReminderType("Daily");
    	eventVo.setSocietyName(societyName);
      
       	eventVo.setHtmlString(eventForm.getHtmlString());
       	eventVo.setMessage(eventForm.getContent());
    	
    	logger.debug("member address "+eventVo.getMessage()+"***"+eventVo.getHtmlString());
    	eventVo.setTransaction_type("Notice");
    	
     	if (email.equalsIgnoreCase("not")) {
     		if(eventForm.getReminderType().equalsIgnoreCase("G")){
     			List memberList=emailDAO.getMemberList(eventForm.getSocietyID(),eventVo.getHtmlString());    
     			logger.debug(societyName+" member list building   "+eventVo.getHtmlString()+"---size "+memberList.size());
     			eventVo.setMemberlist(memberList);     			
     		}else if(eventForm.getReminderType().equalsIgnoreCase("C")){
     			List commityMemberList=emailDAO.getCommityMember(eventForm.getSocietyID(),eventVo.getHtmlString());    	
     			logger.debug(societyName+" committee member list    "+eventVo.getHtmlString()+"---size "+commityMemberList.size());
     			eventVo.setMemberlist(commityMemberList);     		
     		}
     		flag=sendEmail(eventVo);
     		
     	}else{
     		MemberVO membervo=new MemberVO();
     		membervo.setEmail(email);
     		membervo.setBccEmail(email);
     		logger.info("here is "+email);
     		List memberEmailList = new java.util.ArrayList();
			memberEmailList.add(membervo);
			eventVo.setMemberlist(memberEmailList);
			flag=sendEmail(eventVo);
					     		
     	}
    	
    	
    	logger.debug("Entry : public void sendEmail(EventForm evnFrm,String societyName,String email)");
        }catch(NumberFormatException Ex){
        logger.error("Number format exception"+Ex);		

        } 
        
        catch(Exception Ex){
        logger.error(Ex);		

        }
    	return flag;
    }
    
    
    
    public int sendEmailDue(RptTransactionVO tranVO,String societyName,String societyId,String email,BankInfoVO bankVO,String month,String year){
    	int flag=0;
    	BigDecimal due = new BigDecimal("0.00");
    	BigDecimal lateFees = new BigDecimal("0.00");
    	BigDecimal rentFees = new BigDecimal("0.00");
    	BigDecimal totalDues = new BigDecimal("0.00");
    	BigDecimal arrears = new BigDecimal("0.00");
    	BigDecimal balance = new BigDecimal("0.00");
    	BigDecimal mmc = new BigDecimal("0.00");
    	BigDecimal lateFeeRate = new BigDecimal("0.00");
    	BigDecimal rentFeeRate = new BigDecimal("0.00");
    	String period;
    	com.esanchalak.email.common.valueObject.EventVO eventVo=new com.esanchalak.email.common.valueObject.EventVO();
    	
    	Date currentDatetime = new Date(System.currentTimeMillis());   
	    java.sql.Date sqlDate = new java.sql.Date(currentDatetime.getTime());  
	    
        try{
        	ConfigManager c=new ConfigManager();
        	SimpleDateFormat formatter = new SimpleDateFormat("MMM yyyy");	
        	
    	logger.debug("Entry :   public int sendEmailDue(RptTransactionVO tranVO,String societyName,String societyId,String email,BankInfoVO bankVO,String name)");
    	eventVo.setMemberID(tranVO.getMember_id());
    	eventVo.setSocietyId(Integer.parseInt(societyId));
    	eventVo.setTempleteName(c.getPropertiesValue("template.paymentReminder"));
    	eventVo.setEventType("Application");
    	eventVo.setEventID(0);
        eventVo.setSetDate(sqlDate);
        eventVo.setNextDate(sqlDate);
        eventVo.setLastDate(sqlDate);
    	eventVo.setSubject("Payment reminder");
    	eventVo.setReminderType("Daily");
    	eventVo.setSocietyName(societyName);      
       	eventVo.setTransaction_type("NOTPAID");
     
     		MemberVO membervo=new MemberVO();
     		 membervo.setMemberFullName(tranVO.getMember_name()+" ( "+tranVO.getAddress()+" )");
     		 membervo.setEmail(email);
     	
     		logger.info("here is "+email);
     		
     	
     		 due=tranVO.getDue().setScale(2);
			 lateFees=tranVO.getLateFees().setScale(2);
			 rentFees=tranVO.getRentalFees().setScale(2);
			 totalDues=tranVO.getActualTotal().setScale(2);
		     arrears=tranVO.getArrears().setScale(2);
		     balance=tranVO.getBalance().setScale(2);
		     mmc=tranVO.getSocietyMonthlyCharges().setScale(2);
		     
		      membervo.setSocietyMMC(mmc);
		      membervo.setDueAmount(due);		      
		      membervo.setArrears(arrears);		     
		      membervo.setSumDueAmnt(totalDues);
		      membervo.setBalance(balance);
		      membervo.setMonthParam(tranVO.getDayParam());
		      membervo.setMonthsDiffernce(0);
		      membervo.setLateFeeRate(lateFeeRate);
		      membervo.setRentalFeeRate(rentFeeRate);
		      
		      if(membervo.getSumDueAmnt().compareTo(new BigDecimal("0.00"))==0){
		    	  membervo.setLateFee(new BigDecimal("0.00"));
		    	  membervo.setRentalFees(new BigDecimal("0.00"));
		      }else{
		    	  membervo.setLateFee(lateFees);
		    	  membervo.setRentalFees(rentFees);
		      }
	           
		      Date uptodate =  new java.sql.Date(tranVO.getDisplay_date().getTime());
				Calendar calEnddt=Calendar.getInstance();
				calEnddt.setTime(uptodate);			
					
					calEnddt.add(Calendar.MONTH, 1);
					
					java.sql.Date endDate =  new java.sql.Date(calEnddt.getTime().getTime());
					
				period= formatter.format(endDate)+" to "+month+" "+year  ;    
				
		  	 membervo.setHtmlString(period);					    	
			 membervo.setBankName(bankVO.getBankName());
			 membervo.setBranchCode(bankVO.getBranchCode());
			 String accntNo=bankVO.getAccountNo().toString();
			 membervo.setAccNo(accntNo);
			 membervo.setAccType(bankVO.getAccType());
			 membervo.setIfsc(bankVO.getIfscNo());
			 membervo.setMicr(bankVO.getMicrNo());
			 membervo.setInFavour(bankVO.getFavour());
			
			 List memberEmailList = new java.util.ArrayList();
			memberEmailList.add(membervo);
			eventVo.setMemberlist(memberEmailList);
		 	flag=sendEmail(eventVo);
					     		
     	
    	
    	
    	logger.debug("Exit : public int sendEmailDue(RptTransactionVO tranVO,String societyName,String societyId,String email,BankInfoVO bankVO,String name)");
        }catch(NumberFormatException Ex){
        logger.error("Number format exception"+Ex);		

        } 
        
        catch(Exception Ex){
        logger.error(Ex);		

        }
    	return flag;
    }*/

	//notice Send to ActiveMQ
	public int sendEmail(final Serializable obj)  {
		int flag = 0;
		
		logger.debug("Entry :public void sendEmail(final Serializable obj) ");
		try {
			MessageCreator mc = new MessageCreator() {

			
				public Message createMessage(Session session) throws JMSException  {
					logger.debug("Sending Message");
					
					ObjectMessage omsg = session.createObjectMessage();
				
					omsg.setObject(obj);
					logger.debug("Waiting for Acknowlagement");
					omsg.acknowledge();
					logger.info("Recieved Acknowledgement Message has been sent to queue sucessfully");
					return omsg;
				}
			};
				
		
			eventJMSTemplet.send(mc);
				
				logger.info("The object to be queued is :"+mc);
		
				logger.info("Message queued..... ");
				flag=1;
				logger.debug("Exit : public void messageSender(final Serializable obj,final  String EventType) ");
		}
		catch (JmsException e) {
			
			logger.info("Error in MessageSender"+e);
			flag=0;
		}
		catch (Exception e) {
			
			logger.info("Error in MessageSender a"+e);
			flag=0;
		}
		logger.info("flag :"+flag);
		return flag;
	}

	public EmailDAO getEmailDAO() {
		return emailDAO;
	}


	
	 /*public int sendInvoiceToMember(InvoiceDetailsVO invoiceVO,com.emanager.server.society.valueObject.MemberVO memberVO,SocietyVO societyVO,BankInfoVO bankVO){
	    	int flag=0;
	    	
	    	com.esanchalak.email.common.valueObject.EventVO eventVo=new com.esanchalak.email.common.valueObject.EventVO();
	    	List lineItemList=invoiceVO.getListOfLineItems();
	    	int j=lineItemList.size();
	    	InvoiceLineItemsVOForEmailScheduler liVO=new InvoiceLineItemsVOForEmailScheduler();
	    	Date currentDatetime = new Date(System.currentTimeMillis());   
		    java.sql.Date sqlDate = new java.sql.Date(currentDatetime.getTime());  
		    
	        try{
	        	ConfigManager c=new ConfigManager();
	        	SimpleDateFormat formatter = new SimpleDateFormat("MMM yyyy");	
	        	
	    	logger.debug("Entry : public int sendEmailDue(RptTransactionVO tranVO,String societyName,String societyId,String email,BankInfoVO bankVO,String name)");
	    	//eventVo.setMemberID(memberVO.getMemberId());
	    	eventVo.setSocietyId(societyVO.getSocietyID());
	    	String template=c.getPropertiesValue("template.invoice."+societyVO.getSocietyID());
	    	
	    	logger.info("here template is "+template);
	    	eventVo.setTempleteName(template);
	    	eventVo.setEventType("Application");
	    	eventVo.setEventID(0);
	        eventVo.setSetDate(sqlDate);
	        eventVo.setNextDate(sqlDate);
	        eventVo.setLastDate(sqlDate);
	    	eventVo.setSubject("Invoice");
	    	eventVo.setReminderType("Daily");
	    	eventVo.setSocietyName(societyVO.getSocietyName());  
	    	eventVo.setSocietyShortName(societyVO.getSocShortName());
	       	eventVo.setTransaction_type("NOTPAID");
	     
	     		MemberVO membervo=new MemberVO();
	     		 membervo.setMemberFullName(memberVO.getFullName());
	     		 membervo.setEmail(memberVO.getEmail());
	     		 membervo.setAptName(memberVO.getFlatNo());
	     		 membervo.setBankName(bankVO.getBankName());
	     		 membervo.setInFavour(bankVO.getFavour());
	     		 membervo.setAccNo(bankVO.getAccountNo());
	     		 membervo.setBranchCode(bankVO.getBranchCode());
	     		 membervo.setAccType(bankVO.getAccType());
	     		 membervo.setIfsc(bankVO.getIfscNo());
	     	
	     			
	     		 membervo.setInvoiceLineItems(htmlTable.getHTMLTableForLineItems(lineItemList));
			      
			      
	     		if(j!=0){
	    			
	    			liVO.setDescription(invoiceVO.getDescription());
	    			liVO.setInvoiceID(invoiceVO.getInvoiceID());
	    			liVO.setCarriedBalance(invoiceVO.getCarriedBalance());
	    			liVO.setGrandTotal(invoiceVO.getTotalAmount());
	    			liVO.setFromDate(dateUtility.getConvetedDate(invoiceVO.getFromDate()));
	    			liVO.setUptoDate(dateUtility.getConvetedDate(invoiceVO.getUptoDate()));
	    			liVO.setDueDate(dateUtility.getConvetedDate(invoiceVO.getDueDate()));
	    			liVO.setCreateDate(dateUtility.getConvetedDate(invoiceVO.getCreateDate()));
	    			membervo.setHtmlString(liVO.getDescription());
	    			membervo.setTotal(liVO.getGrandTotal());
	    			membervo.setIsInvoiceApplicable(1);
	    			//membervo.setLineItemsVO(liVO);
	    			List memberEmailList = new java.util.ArrayList();
	    			memberEmailList.add(membervo);
					eventVo.setMemberlist(memberEmailList);
				 	flag=sendEmail(eventVo);
	    			 if (liVO.getGrandTotal().compareTo(new BigDecimal("0.00")) == -1){
	    				 logger.debug("-------------------"+liVO.getGrandTotal());
	    				 membervo.setIsInvoiceApplicable(0);
	    				
	    			 }
	    			}else{
	    				 membervo.setIsInvoiceApplicable(0);
	    				
	    				
	    			}
				
				 
				
						     	
	     	
	    	
	    	
	    	logger.debug("Exit : public int sendEmailDue(RptTransactionVO tranVO,String societyName,String societyId,String email,BankInfoVO bankVO,String name)");
	        }catch(NumberFormatException Ex){
	        logger.error("Number format exception"+Ex);		

	        } 
	        
	        catch(Exception Ex){
	        logger.error("Exception while sending to que "+Ex);

	        }
	    	return flag;
	    }*/
	
	
	 public int addPaymentReminderEvent(EventForm evnFrm){
	    	int flag=0;

	        try{
	    	
	    	logger.debug("Entry : public int addPaymentReminderEvent(EventForm evnFrm)");
	    	
	    	flag=emailDAO.addPaymentReminderEvent(evnFrm);
	    	
	    	logger.debug("Entry : public int addPaymentReminderEvent(EventForm evnFrm)");
	        }  catch(Exception Ex){
	        	  logger.error("Exepection in public int addPaymentReminderEvent(EventForm evnFrm)"+Ex);	

	        }
	    	return flag;
	    }
	    
	 
	 
	public void setEmailDAO(EmailDAO emailDAO) {
		this.emailDAO = emailDAO;
	}




	public JmsTemplate getEventJMSTemplet() {
		return eventJMSTemplet;
	}




	public void setEventJMSTemplet(JmsTemplate eventJMSTemplet) {
		this.eventJMSTemplet = eventJMSTemplet;
	}




	

	




	




  

}
