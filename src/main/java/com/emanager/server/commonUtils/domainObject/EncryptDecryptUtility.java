package com.emanager.server.commonUtils.domainObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;

import com.emanager.server.userManagement.valueObject.UserVO;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.codec.binary.Base64;

public class EncryptDecryptUtility 
{
		private static final Logger logger=Logger.getLogger(EncryptDecryptUtility.class);
		private static final String ALGO = "AES";
		private static final byte[] keyValue = new byte[] { 'T', 'h', 'e', 'E', 's', 'a', 'n','c', 'h', 'a', 'l','a','k', 'K', 'e', 'y' };
		static ObjectMapper mapper = new ObjectMapper();
	
	 public static String encrypt(String Data)  {
	        String encryptedValue = null;
			try {
				Key key = generateKey();
				Cipher c = Cipher.getInstance(ALGO);
				c.init(Cipher.ENCRYPT_MODE, key);
				byte[] encVal = c.doFinal(Data.getBytes());
				encryptedValue=new Base64().encodeToString(encVal);
			
			} catch (InvalidKeyException e) {
				logger.error("Exception occurred while encrypting the data "+e);
			} catch (NoSuchAlgorithmException e) {
				logger.error("Exception occurred while encrypting the data "+e);
			} catch (NoSuchPaddingException e) {
				logger.error("Exception occurred while encrypting the data "+e);
			} catch (IllegalBlockSizeException e) {
				logger.error("Exception occurred while encrypting the data "+e);
			} catch (BadPaddingException e) {
				logger.error("Exception occurred while encrypting the data "+e);
			} catch (Exception e) {
				logger.error("Exception occurred while encrypting the data "+e);
			}
	        return encryptedValue;
	    }
		
		public static String decrypt(String encryptedData)  {
	        String decryptedValue = null;
			try {
				Key key = generateKey(); 
				Cipher c = Cipher.getInstance(ALGO);
				c.init(Cipher.DECRYPT_MODE, key);
				byte[] decordedValue = 	new Base64().decode(encryptedData);
				byte[] decValue = c.doFinal(decordedValue);
				decryptedValue = new String(decValue);
			} catch (InvalidKeyException e) {
				logger.error("Exception occurred while decrypting the data "+e);
			} catch (NoSuchAlgorithmException e) {
				logger.error("Exception occurred while decrypting the data "+e);
			} catch (NoSuchPaddingException e) {
				logger.error("Exception occurred while decrypting the data "+e);
			} catch (IllegalBlockSizeException e) {
				logger.error("Exception occurred while decrypting the data "+e);
			} catch (BadPaddingException e) {
				logger.error("Exception occurred while decrypting the data "+e);
			} catch (Exception e) {
				logger.error("Exception occurred while decrypting the data "+e);
			}
	        return decryptedValue;
	    }
		
		  private static Key generateKey()  {
		        Key key = null;
				try {
					key = new SecretKeySpec(keyValue, ALGO);
				} catch (Exception e) {
					logger.error("Exception occurred while generating the key "+e);
				}
		        return key;
		}
	   
		  public static String toJSON(Object o) {
			  String result = "";
			  if (o != null) {
			   ByteArrayOutputStream out = new ByteArrayOutputStream();
			   try {
			    mapper.writeValue(out, o);
			   } catch (JsonGenerationException e) {
				   logger.error("Exception occurred while decrypting the data "+e);
			   } catch (JsonMappingException e) {
				   logger.error("Exception occurred while decrypting the data "+e);
			   } catch (IOException e) {
				   logger.error("Exception occurred while decrypting the data "+e);
			   }
			   try {
			    result = out.toString("UTF-8");
			   } catch (UnsupportedEncodingException e) {
				   logger.error("Exception occurred string to json convert "+e);
			   }
			  }
			  return result;
			 }

			 public static Object parseJSON(String jsonString, Class beanClass)
			   throws JsonParseException, JsonMappingException, IOException {
			  return mapper.readValue(jsonString, beanClass);
			 }  
			 
			 public String encodeBeanToString(UserVO userVO){
			  String result = null;
			  
			  try {
			  String jsonString = this.toJSON(userVO);
			    Key key = generateKey();
				Cipher c = Cipher.getInstance(ALGO);
				c.init(Cipher.ENCRYPT_MODE, key);
				byte[] encVal = c.doFinal(jsonString.getBytes());
				result = new Base64().encodeToString(encVal);
			   } catch (IllegalBlockSizeException e) {
				   logger.error("Exception occurred encodeBeanToString "+e);
			   }catch (BadPaddingException e) {
				   logger.error("Exception occurred encodeBeanToString "+e);
			   }catch (InvalidKeyException e) {
				   logger.error("Exception occurred encodeBeanToString "+e);
			   } catch (NoSuchAlgorithmException e) {
				   logger.error("Exception occurred encodeBeanToString "+e);
			   } catch (NoSuchPaddingException e) {
				   logger.error("Exception occurred encodeBeanToString "+e);
			   }
			   return result;
			 }

			 public UserVO decodeStringToBean(String encryptedData){
				 String decryptedValue = "";
				 byte[] decValue;
				 UserVO userVO= new UserVO();
				 try{
								  
					  Key key = generateKey(); 
						Cipher c = Cipher.getInstance(ALGO);						
						c.init(Cipher.DECRYPT_MODE, key);
						byte[] decordedValue = 	new Base64().decode(encryptedData);
						decValue = c.doFinal(decordedValue);
						decryptedValue = new String(decValue);
						userVO= (UserVO) this.parseJSON(new String(decValue), UserVO.class);
				 }catch (IOException e) {
					   logger.error("Exception occurred encodeBeanToString "+e);
				 }catch (InvalidKeyException e) {
				   logger.error("Exception occurred decodeStringToBean "+e);
				 }catch (IllegalBlockSizeException e) {
					   logger.error("Exception occurred decodeStringToBean "+e);
				 } catch (BadPaddingException e) {
					   logger.error("Exception occurred decodeStringToBean "+e);
				 } catch (NoSuchPaddingException e) {
					  logger.error("Exception occurred decodeStringToBean "+e);
				} catch (NoSuchAlgorithmException e) {
					  logger.error("Exception occurred decodeStringToBean "+e);
				}
			

			  return userVO;
			 }  
	 
}
