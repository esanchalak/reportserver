package com.emanager.server.commonUtils.domainObject;

import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import com.emanager.server.userManagement.valueObject.PermissionVO;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CommonUtility {
	
	Logger logger=Logger.getLogger(this.getClass().getName());
	public static final String GSTINFORMAT_REGEX = "[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9A-Za-z]{1}[Z]{1}[0-9a-zA-Z]{1}";
	public static final String GSTN_CODEPOINT_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	 //method generate 6 digit random number 
	public String generateOTP(){
	     return new BigInteger(130, new SecureRandom()).toString().substring(0,12);
    }
	
	 //method generate 6 digit random number 
	public String generateOTPPwd(){
		  return new BigInteger(130, new SecureRandom()).toString().substring(0,6);
	}
	 
    // create client permission hashmap list 
	public HashMap<String, String> createClientPermissionHashMap(List<PermissionVO> permissionList){
		 HashMap<String, String> permisionHashMap=new HashMap<String, String>();
		 if(permissionList.size()>0){		
		  for (int i = 0; i < permissionList.size(); i++) {
			 PermissionVO permVO=permissionList.get(i);
			 permisionHashMap.put(permVO.getAccessName(),permVO.getAccessID().toString());
		  }
		 }
	     return permisionHashMap;
    }
	
	// create server permission hashmap list 
	public HashMap<String, String> createServerPermissionHashMap(List<PermissionVO> permissionList){
	   HashMap<String, String> permisionHashMap=new HashMap<String, String>();
			 if(permissionList.size()>0){		
			  for (int i = 0; i < permissionList.size(); i++) {
				 PermissionVO permVO=permissionList.get(i);
				 permisionHashMap.put(permVO.getServerAccessMethod(),permVO.getAccessID().toString());
			  }
			 }
		     return permisionHashMap;
	}
	
	public	static ArrayList<String> removeDuplicates(ArrayList<String> list) {

		// Store unique items in result.
		ArrayList<String> result = (ArrayList<String>) new ArrayList();

		// Record encountered Strings in HashSet.
		HashSet<String> set = (HashSet<String>) new HashSet();

		// Loop over argument list.
		for (String item : list) {

		    // If String is not in set, add it to the list and the set.
		    if (!set.contains(item)) {
			result.add(item);
			set.add(item);
		    }
		}
		return result;
	    }
	// create hashmap from string
		public HashMap<String, String> createPrintTemlateHashMap(String templateString){
			  ObjectMapper mapper = new ObjectMapper();
			 HashMap<String, String> hashmap=new HashMap<String, String>();
				 if(templateString.length()>0){		
					
	                   try {
	                	   hashmap=mapper.readValue(templateString, new TypeReference<HashMap<String,String>>(){});
					} catch (JsonParseException e) {
						// TODO Auto-generated catch block
						logger.debug(" createPrintTemlateHashMap  "+e);
					} catch (JsonMappingException ex) {
						// TODO Auto-generated catch block
						logger.debug(" createPrintTemlateHashMap  "+ex);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						logger.debug(" createPrintTemlateHashMap  "+e);
					};
				 }
			     return hashmap;
		}
		
		//Validate email ID
		public  boolean validateEmailID(String email) {
		    String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
			
			Pattern pattern = Pattern.compile(regex);

			 Matcher matcher = pattern.matcher(email);
			  System.out.println(email +" : "+ matcher.matches());
			  if(matcher.matches()){
			     return true;
			  }else{
			     return false;
			  }		  

		 }


		//Validate phone number
		public  Boolean validatePhoneNumber(String phoneNo) {
			 		//validate phone numbers of format "1234567890"
			 		if (phoneNo.matches("\\d{10}")) return true;
			 		//validating phone number with -, . or spaces
			 		else if(phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) return true;
			 		//validating phone number with extension length from 3 to 5
			 		else if(phoneNo.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}")) return true;
			 		//validating phone number where area code is in braces ()
			 		else if(phoneNo.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}")) return true;
			 		//return false if nothing matches the input
			 		else return false;
			 		
		 	}
		
		
		//Validate custom user ID
			public Boolean validateCustomName(String customName) {					 		
				if (customName.matches("^[s|S]{1}[0-9]+(-[a-zA-Z0-9]+)+$")) return true;
				else return false;
					 		
		 	}		
		
		
		//====== Validate GST Number ============//
			
			/**
			 * Method to check if a GSTIN is valid. Checks the GSTIN format and the
			 * check digit is valid for the passed input GSTIN
			 * 
			 * @param gstin
			 * @return boolean - valid or not
			 * @throws Exception
			 */
			public  boolean validGSTIN(String gstin) throws Exception {
				boolean isValidFormat = false;
				if (checkPattern(gstin, GSTINFORMAT_REGEX)) {
					isValidFormat = verifyCheckDigit(gstin);
				}
				return isValidFormat;

			}

			/**
			 * Method for checkDigit verification.
			 * 
			 * @param gstinWCheckDigit
			 * @return
			 * @throws Exception
			 */
			private  boolean verifyCheckDigit(String gstinWCheckDigit) throws Exception {
				Boolean isCDValid = false;
				String newGstninWCheckDigit = getGSTINWithCheckDigit(
						gstinWCheckDigit.substring(0, gstinWCheckDigit.length() - 1));

				if (gstinWCheckDigit.trim().equals(newGstninWCheckDigit)) {
					isCDValid = true;
				}
				return isCDValid;
			}

			/**
			 * Method to check if an input string matches the regex pattern passed
			 * 
			 * @param inputval
			 * @param regxpatrn
			 * @return boolean
			 */
			public  boolean checkPattern(String inputval, String regxpatrn) {
				boolean result = false;
				if ((inputval.trim()).matches(regxpatrn)) {
					result = true;
				}
				return result;
			}

			/**
			 * Method to get the check digit for the gstin (without checkdigit)
			 * 
			 * @param gstinWOCheckDigit
			 * @return : GSTIN with check digit
			 * @throws Exception
			 */
			public  String getGSTINWithCheckDigit(String gstinWOCheckDigit) throws Exception {
				int factor = 2;
				int sum = 0;
				int checkCodePoint = 0;
				char[] cpChars;
				char[] inputChars;

				try {
					if (gstinWOCheckDigit == null) {
						throw new Exception("GSTIN supplied for checkdigit calculation is null");
					}
					cpChars = GSTN_CODEPOINT_CHARS.toCharArray();
					inputChars = gstinWOCheckDigit.trim().toUpperCase().toCharArray();

					int mod = cpChars.length;
					for (int i = inputChars.length - 1; i >= 0; i--) {
						int codePoint = -1;
						for (int j = 0; j < cpChars.length; j++) {
							if (cpChars[j] == inputChars[i]) {
								codePoint = j;
							}
						}
						int digit = factor * codePoint;
						factor = (factor == 2) ? 1 : 2;
						digit = (digit / mod) + (digit % mod);
						sum += digit;
					}
					checkCodePoint = (mod - (sum % mod)) % mod;
					return gstinWOCheckDigit + cpChars[checkCodePoint];
				} finally {
					inputChars = null;
					cpChars = null;
				}
		}

			
			
}
