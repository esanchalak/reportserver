package com.emanager.server.commonUtils.domainObject;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.LedgerTrnsVO;
import com.emanager.server.accounts.DataAccessObjects.PenaltyChargesVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.invoice.Services.InvoiceService;
import com.emanager.server.invoice.dataAccessObject.BillDetailsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceBillingCycleVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;

public class CalculateLateFeeForTransactions {
	Logger logger = Logger.getLogger(CalculateLateFeeForTransactions.class);
	BigDecimal hundred = new BigDecimal("100");
	LedgerService ledgerService;
	SocietyService societyService;
	TransactionService transactionService;
	InvoiceService invoiceService;
	DateUtility dateUtil=new DateUtility();
	
	public TransactionVO getLateFee(PenaltyChargesVO chargeVO,int societyID,MemberVO memberVO,String fromDate,String toDate,String dueDate,InvoiceBillingCycleVO billingVO){
 		BigDecimal lateFees=BigDecimal.ZERO;
 		BigDecimal balance=BigDecimal.ZERO;
 		TransactionVO lateTxVO=new TransactionVO();
 		logger.debug("Entry : Public BigDecimal getLateFee(ChargesVO invLineVO,int societyID,MemberVO memberVO,String fromDate,String toDate)");
 		try {
			
			SocietyVO societyVO=societyService.getSocietyDetails("0", societyID);
			//AccountHeadVO achVO=ledgerService.getOpeningClosingBalance(societyID, memberVO.getLedgerID(), fromDate, toDate, "L");
			LedgerTrnsVO ledgerTrnsVO=new LedgerTrnsVO();
			if((chargeVO.getCalMethod().equalsIgnoreCase("DL")||	chargeVO.getCalMethod().equalsIgnoreCase("RB")||(chargeVO.getCalMethod().equalsIgnoreCase("RBDT"))||(chargeVO.getCalMethod().equalsIgnoreCase("RBP"))||(chargeVO.getCalMethod().equalsIgnoreCase("RBPDT")) )){
				ledgerTrnsVO= ledgerService.getBillTransactionList(societyID, memberVO.getLedgerID(), fromDate, dueDate ,"C");	
			}else
			ledgerTrnsVO= ledgerService.getBillTransactionList(societyID, memberVO.getLedgerID(), fromDate, chargeVO.getPenaltyChargeDate(), "C");
	
		/*	if(ledgerTrnsVO.getClosingBalance().intValue()>=ledgerTrnsVO.getTotalAmount().intValue())
			{
				balance=ledgerTrnsVO.getCrOpnBalance();
			}else balance=ledgerTrnsVO.getClosingBalance();
			//*/
			  balance=ledgerTrnsVO.getClosingBalance();
			 
			if(balance.compareTo(societyVO.getThresholdBalance()) <= 0 ){
				logger.info("The late fee should be waived off since balance is "+balance+"  Threshold Amount should be  "+societyVO.getThresholdBalance()+" for ledger ID "+memberVO.getLedgerID());
			}
			else{
			
			if((chargeVO.getChargeFrequency().equalsIgnoreCase("OneTime")&&(balance.intValue()>0))){
				lateTxVO=calculateLateFeeFixedAmtWise(balance, chargeVO,fromDate,toDate,memberVO,societyVO);
			}
			else {
			
			if((chargeVO.getCalMethod().equalsIgnoreCase("SI")&&(balance.signum()>0))){ // Calculation of late fees by simple interest
				lateTxVO=calculateLateFeePercentWiseSimple(balance, chargeVO, memberVO, fromDate, toDate, societyVO,billingVO);
				
			}else	if((chargeVO.getCalMethod().equalsIgnoreCase("CI")&&(balance.signum()>0))){// Calculation of late fee by cumulative interest.
				lateTxVO=calculateLateFeePercentWiseCommulative(balance, chargeVO);
				
			}else if((chargeVO.getCalMethod().equalsIgnoreCase("F")&&(balance.signum()>0))){ // Calculation of late fee on fixed amount like Rs 50 per month etc
				lateTxVO=calculateLateFeeFixedAmtWise(balance, chargeVO,fromDate,toDate,memberVO,societyVO);
			}
			
			else if(chargeVO.getCalMethod().equalsIgnoreCase("RB")){ // Calculation on daily reducing balance
				lateTxVO=calculateLateFeeDailyReducingBalance(balance, chargeVO,memberVO,fromDate,toDate);
			}
			else if(chargeVO.getCalMethod().equalsIgnoreCase("RBP")){ // Calculation on daily reducing balance on principal balance only
				lateTxVO=calculateLateFeeDailyReducingBalanceOnPrincipal(balance, chargeVO,memberVO,fromDate,toDate,dueDate);
			}
			else if(chargeVO.getCalMethod().equalsIgnoreCase("RBDT")){// Calculation on daily reducing balance calculated after due date
				lateTxVO=calculateLateFeeDailyReducingBalanceAfterDueDate(balance, chargeVO,memberVO,fromDate,toDate,dueDate);
			}
			else if(chargeVO.getCalMethod().equalsIgnoreCase("RBPDT")){// Calculation on daily reducing balance on principal balance ony after due date
				lateTxVO=calculateLateFeeDailyReducingBalanceAfterDueDateOnPrincipal(balance, chargeVO,memberVO,fromDate,toDate,dueDate);
			}
			
			else if(chargeVO.getCalMethod().equalsIgnoreCase("DL")){ // Calculation of late fee on daily basis fixed amount ex. 50 Rs per day etc
				lateTxVO=calculateLateFeeWithDailyFixedAmount(balance, chargeVO, memberVO, fromDate, toDate,dueDate);
			}
			
			
			}
			}
			
		} catch (Exception e) {
			logger.error("Exception occured in getLateFeeForInvoice "+e);
		}
 		
 		
 		
 		logger.debug("Exit : Public BigDecimal getLateFeeForInvoice(InvoiceLineItemsVO invoiceVO)"+lateFees);
 		return lateTxVO;
 	}

	private TransactionVO calculateLateFeeFixedAmtWise(BigDecimal balance,PenaltyChargesVO chargeVO,String fromDate,String toDate,MemberVO memberVO,SocietyVO societyVO){
 		logger.debug("Entry : private BigDecimal calculateLateFeeFixedAmtWise(BigDecimal balance,ChargesVO chargeVO)");
 		TransactionVO lateTxVO=new TransactionVO();
 		BigDecimal lateFees=BigDecimal.ZERO;
 		String firstDate=dateUtil.getFirstDateOfFY(fromDate);
 		MathContext mc = new MathContext(4);
 		BigDecimal latefeesInHundred=chargeVO.getAmount().divide(hundred);
 		BigDecimal lateFeeCharged=BigDecimal.ZERO;
 		BigDecimal netBalance=balance;
 		
 		List transactionList=transactionService.getTransctionList(memberVO.getSocietyID(), memberVO.getLedgerID(), firstDate, toDate, "ALL");
 		
 		
 		
 		if(transactionList.size()>0){
			
			for(int i=0;transactionList.size()>i;i++){
				
				TransactionVO txVO=(TransactionVO) transactionList.get(i);
			
				if(chargeVO.getLedgerID()==txVO.getLedgerID()){
					lateFeeCharged=lateFeeCharged.add(txVO.getAmount());
				}
				
			}
			
			netBalance=netBalance.subtract(lateFeeCharged);
			
			if(netBalance.compareTo(societyVO.getThresholdBalance()) < 0 ){
				logger.info("The late fee should be waived off since balance is "+balance+"  Threshold Amount should be  "+societyVO.getThresholdBalance());
			}
			else{
				lateFees =lateFees = chargeVO.getAmount();
			}
				
				
			lateTxVO.setAmount(lateFees);
	 		lateTxVO.setDescription("Late fee of Rs "+lateFees+" per cycle on balance amount Rs "+netBalance+" , Previous late fee total Rs "+lateFeeCharged);
				
				
			}
 		
 		
 		logger.debug("Exit : private BigDecimal calculateLateFeeFixedAmtWise(BigDecimal balance,ChargesVO chargeVO)");
 		return lateTxVO;
 	}
	
	private TransactionVO calculateLateFeePercentWiseCommulative(BigDecimal balance,PenaltyChargesVO chargeVO){
 		logger.debug("Entry : private BigDecimal calculateLateFeePercentWiseCommulative(BigDecimal balance,ChargesVO chargeVO)");
 		BigDecimal lateFees=BigDecimal.ZERO;
 		MathContext mc = new MathContext(4);
 		TransactionVO lateTxVO=new TransactionVO();
 		BigDecimal latefeesInHundred=chargeVO.getAmount().divide(hundred);
 		
 		lateFees =balance.multiply(latefeesInHundred).setScale(0,RoundingMode.HALF_UP).setScale(2);
 		
 		lateTxVO.setAmount(lateFees);
 		lateTxVO.setDescription("Late fee calculated at the rate "+chargeVO.getAmount()+" % "+chargeVO.getChargeFrequency() );
 		
 		logger.debug("Exit : private BigDecimal calculateLateFeePercentWiseCommulative(BigDecimal balance,ChargesVO chargeVO)");
 		return lateTxVO;
 	}
	
	
	private TransactionVO calculateLateFeePercentWiseSimple(BigDecimal balance,PenaltyChargesVO chargeVO,MemberVO memberVO,String fromDate,String toDate,SocietyVO societyVO,InvoiceBillingCycleVO billingCycleVO){
 		logger.debug("Entry : private BigDecimal calculateLateFeePercentWiseCommulative(BigDecimal balance,ChargesVO chargeVO)");
 		BigDecimal lateFees=BigDecimal.ZERO;
 		TransactionVO lateTxVO=new TransactionVO();
 		String firstDate=dateUtil.getFirstDateOfFY(fromDate);
 		MathContext mc = new MathContext(4);
 		BigDecimal latefeesInHundred=chargeVO.getAmount().divide(hundred);
 		BigDecimal lateFeeCharged=BigDecimal.ZERO;
 		BigDecimal invBal=BigDecimal.ZERO;
 		BigDecimal netBalance=balance;
 		
 	//	LedgerTrnsVO txVO=ledgerService.getBillTransactionList(memberVO.getSocietyID(), memberVO.getLedgerID(), firstDate, toDate, "C");
 		AccountHeadVO txVO = new AccountHeadVO();
		try {
			txVO = ledgerService.getOpeningClosingBalance(memberVO.getSocietyID(), memberVO.getLedgerID(), firstDate, toDate, "L");
		} catch (ParseException e) {
			logger.error(" Exception in calculating simple late fees "+e);
		}
 		
 		
 			netBalance=txVO.getClosingBalance().subtract(txVO.getIntClsBalance());
 	
		
 		
 		
 				if(netBalance.compareTo(societyVO.getThresholdBalance()) < 0 ){
 					logger.info("The late fee should be waived off since balance is "+balance+"  Threshold Amount should be  "+societyVO.getThresholdBalance()+" for ledger ID "+memberVO.getLedgerID());
 				}
 				else{
 					lateFees =netBalance.multiply(latefeesInHundred).setScale(0,RoundingMode.HALF_UP).setScale(2);
 				}
 				
 			
 		
 		
				
			lateTxVO.setAmount(lateFees);
	 		lateTxVO.setDescription("Late fee calculated at the rate "+chargeVO.getAmount()+" %  "+chargeVO.getChargeFrequency());
				
				
			
 		logger.debug("Exit : private BigDecimal calculateLateFeePercentWiseCommulative(BigDecimal balance,ChargesVO chargeVO)");
 		return lateTxVO;
 	}
	/*
	private TransactionVO calculateLateFeePercentWiseSimple(BigDecimal balance,PenaltyChargesVO chargeVO,MemberVO memberVO,String fromDate,String toDate,SocietyVO societyVO){
 		logger.debug("Entry : private BigDecimal calculateLateFeePercentWiseCommulative(BigDecimal balance,ChargesVO chargeVO)");
 		BigDecimal lateFees=BigDecimal.ZERO;
 		TransactionVO lateTxVO=new TransactionVO();
 		BigDecimal latefeesInHundred=chargeVO.getAmount().divide(hundred);
 	    BigDecimal societyMaintenance=new BigDecimal(memberVO.getSocietyMMC());
 		BigDecimal chargableBalance=BigDecimal.ZERO;
 		BigDecimal netBalance=balance;
 		List transactionList=formulateScheduledTransaction(societyVO.getSocietyID(),  memberVO);
 		if(transactionList.size()>0){
 			for(int i=0;transactionList.size()>i;i++){
 				TransactionVO txVO=(TransactionVO) transactionList.get(i);
 				societyMaintenance=societyMaintenance.add(txVO.getAmount());
 				
 			}
 		}
 		
 		
 		
 		if(netBalance.compareTo(societyVO.getThresholdBalance()) < 0 ){
				logger.info("The late fee should be waived off since balance is "+balance+"  Threshold Amount should be  "+societyVO.getThresholdBalance());
			}	else{
				
				if(netBalance.compareTo(societyMaintenance) < 0 )
					chargableBalance=netBalance;
					else 
						chargableBalance=societyMaintenance;
				lateFees =chargableBalance.multiply(latefeesInHundred).setScale(0,RoundingMode.HALF_UP).setScale(2);
			}
				
				
			lateTxVO.setAmount(lateFees);
	 		lateTxVO.setDescription("Balance Rs "+chargableBalance+" , late fee calculated at the rate "+chargeVO.getAmount()+" %  "+chargeVO.getChargeFrequency());
				
				
			
 		logger.debug("Exit : private BigDecimal calculateLateFeePercentWiseCommulative(BigDecimal balance,ChargesVO chargeVO)");
 		return lateTxVO;
 	}*/
	
	private TransactionVO calculateLateFeeDailyReducingBalance(BigDecimal balance,PenaltyChargesVO chargeVO,MemberVO memberVO,String fromDate,String toDate){
 		logger.debug("Entry : private BigDecimal calculateLateFeeDailyReducingBalance(BigDecimal balance,ChargesVO chargeVO)");
 		BigDecimal lateFees=BigDecimal.ZERO;
 		Boolean isLast=false;
 		TransactionVO lateTxVO=new TransactionVO();
 		BigDecimal latefeesInHundred=chargeVO.getAmount().divide(hundred);
 		Date currentDate=dateUtil.findCurrentDate();
 		String firstDate=dateUtil.getFirstDateOfFY(currentDate+"");
 		BigDecimal netBalance=BigDecimal.ZERO;
 		BigDecimal lateFeeCharged=BigDecimal.ZERO;
 		String useFromDate=fromDate;
		BigDecimal lateFeesPerDay=BigDecimal.ZERO;
		try{
 		
		
			
 			LedgerTrnsVO achVO=ledgerService.getLedgerTransactionList(memberVO.getSocietyID(), memberVO.getLedgerID(), useFromDate,toDate, "l");
 			balance=achVO.getOpeningBalance();
 			netBalance=achVO.getOpeningBalance();
 			List txList=transactionService.getTransctionList(memberVO.getSocietyID(), memberVO.getLedgerID(), firstDate, toDate, "ALL");
 	 		
 	 		 	 		
 	 	/*	if(txList.size()>0){
 				
 				for(int i=0;txList.size()>i;i++){
 					
 					TransactionVO txVO=(TransactionVO) txList.get(i);
 				
 					if(chargeVO.getLedgerID()==txVO.getLedgerID()){
 						lateFeeCharged=lateFeeCharged.add(txVO.getAmount());
 					}
 					
 				}
 				
 				netBalance=netBalance.subtract(lateFeeCharged);
 	 		}*/
		
		
		
		List transactionList=transactionService.getTransctionList(memberVO.getSocietyID(), memberVO.getLedgerID(), useFromDate, toDate, "ALL");
		
		int txCount=transactionList.size();
		String txFromDate=useFromDate;
		
		if(transactionList.size()>0){
				
				for(int i=0;transactionList.size()>i;i++){
					
					TransactionVO txVO=new TransactionVO();
					BigDecimal tempLateFee=BigDecimal.ZERO;
					
					int diffInDays=0;
					if(isLast){
						diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(toDate),dateUtil.convertStringDateToSql(txFromDate) );
						if((diffInDays>0)&&(netBalance.intValue()>0)){
	 						
	 						
	 						lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
	 						
	 						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
	 						
	 						lateFees=lateFees.add(tempLateFee);
						}
	 						
					}else{
					txVO=(TransactionVO) transactionList.get(i);
					
					if(i==0){
					//	netBalance=netBalance.add(txVO.getAmount());
						//logger.info("-------------"+i);
											
						diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(dateUtil.getPrevOrNextDate(useFromDate,1) ));
						if(transactionList.size()==1){
							isLast=true;
							if((diffInDays>0)&&(netBalance.intValue()>0)){
								
								
								lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
								
								tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
								
								lateFees=lateFees.add(tempLateFee);
								
							
								
							}
							txFromDate=txVO.getTransactionDate();
							--i;
							netBalance=netBalance.add(txVO.getAmount());
							netBalance=netBalance.subtract(txVO.getIntAmount());
						continue;
						}else{
							if((diffInDays>0)&&(netBalance.intValue()>0)){
		 						
		 						
		 						lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
		 						
		 						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
		 						
		 						lateFees=lateFees.add(tempLateFee);
		 					
		 						
		 					}
						}
					}else if((i==(transactionList.size()-1))){
						//logger.info("-------------"+i);
						
						diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(txFromDate) );
						isLast=true;
						--i;
						if((diffInDays>0)&&(netBalance.intValue()>0)){
							
							
							lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
							
							tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
							
							lateFees=lateFees.add(tempLateFee);
						
							
							
						}
						netBalance=netBalance.add(txVO.getAmount());
						netBalance=netBalance.subtract(txVO.getIntAmount());
						txFromDate=txVO.getTransactionDate();
					
						
					}else{					
					diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(txFromDate) );
					
					if((diffInDays>0)&&(netBalance.intValue()>0)){
						
						
						lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
						
						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
						
						lateFees=lateFees.add(tempLateFee);
					
						
					}
					}
					
						
					}
					if(!isLast){
					
					netBalance=netBalance.add(txVO.getAmount());
					netBalance=netBalance.subtract(txVO.getIntAmount());
					txFromDate=txVO.getTransactionDate();
					}
				}
				
				
			}else if((!isLast)&&(netBalance.intValue()>0)){
				
				int diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(dateUtil.getPrevOrNextDate(toDate,0)),dateUtil.convertStringDateToSql(useFromDate) );
				lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
				
			    BigDecimal	tmpLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
				
				lateFees=lateFees.add(tmpLateFee);
			
				
			}
		
		lateTxVO.setAmount(lateFees);
		lateTxVO.setDescription("");
 		
			
		
		}catch (Exception e) {
			logger.error("Exception : Exception while calculating late fee per day "+e);
		}
 		
 		logger.debug("Exit : private BigDecimal calculateLateFeeDailyReducingBalance(BigDecimal balance,ChargesVO chargeVO)");
 		return lateTxVO;
 	}
	
	
	private TransactionVO calculateLateFeeDailyReducingBalanceAfterDueDate(BigDecimal balance,PenaltyChargesVO chargeVO,MemberVO memberVO,String fromDate,String toDate,String dueDate){
 		logger.debug("Entry : private BigDecimal calculateLateFeeDailyReducingBalance(BigDecimal balance,ChargesVO chargeVO)");
 		BigDecimal lateFees=BigDecimal.ZERO;
 		Boolean isLast=false;
 		TransactionVO lateTxVO=new TransactionVO();
 		BigDecimal latefeesInHundred=chargeVO.getAmount().divide(hundred);
 		Date currentDate=dateUtil.findCurrentDate();
 		String firstDate=dateUtil.getFirstDateOfFY(currentDate+"");
 		BigDecimal netBalance=BigDecimal.ZERO;
 		BigDecimal lateFeeCharged=BigDecimal.ZERO;
 		String useFromDate="";
		BigDecimal lateFeesPerDay=BigDecimal.ZERO;
		try{
			dueDate=dateUtil.getPrevOrNextDate(dueDate,0);
			int diffs=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(dueDate),dateUtil.convertStringDateToSql(fromDate) );
 			if(diffs>0){
 				useFromDate=dueDate;
 			}else useFromDate=fromDate;
			
 			LedgerTrnsVO achVO=ledgerService.getLedgerTransactionList(memberVO.getSocietyID(), memberVO.getLedgerID(), useFromDate,toDate, "l");
 			balance=achVO.getOpeningBalance();
 			netBalance=achVO.getOpeningBalance();
 			List txList=transactionService.getTransctionList(memberVO.getSocietyID(), memberVO.getLedgerID(), firstDate, toDate, "ALL");
 	 		
 	 		 	 		
 	 		/*if(txList.size()>0){
 				
 				for(int i=0;txList.size()>i;i++){
 					
 					TransactionVO txVO=(TransactionVO) txList.get(i);
 				
 					if(chargeVO.getLedgerID()==txVO.getLedgerID()){
 						lateFeeCharged=lateFeeCharged.add(txVO.getAmount());
 					}
 					
 				}
 				
 				netBalance=netBalance.subtract(lateFeeCharged);
 	 		}*/
		
		
		
		List transactionList=transactionService.getTransctionList(memberVO.getSocietyID(), memberVO.getLedgerID(), useFromDate, toDate, "ALL");
		String txFromDate=dueDate;
		int txCount=transactionList.size();
		/*if(transactionList.size()>0){
			
			for(int i=0;transactionList.size()>i;i++){
				
				TransactionVO txVO=new TransactionVO();
				BigDecimal tempLateFee=BigDecimal.ZERO;
				int diffInDays=0;
				if(isLast){
					diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(toDate),dateUtil.convertStringDateToSql(dateUtil.getPrevOrNextDate(txFromDate, 1)) );
				}else{
				txVO=(TransactionVO) transactionList.get(i);
				
				if(i==0){
					//logger.info("-------------"+i);
										
					diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(dueDate) );
					if(transactionList.size()==1){
						isLast=true;
						balance=balance.add(txVO.getAmount());
						txFromDate=txVO.getTransactionDate();
						--i;
					}
				}else if(i==(transactionList.size()-1)){
					//logger.info("-------------"+i);
					
					diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(txFromDate) );
					isLast=true;
					--i;
					if((diffInDays>0)&&(balance.intValue()>0)){
						
						
						lateFeesPerDay = balance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
						
						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
						
						lateFees=lateFees.add(tempLateFee);
						
						
						
					}
					balance=balance.add(txVO.getAmount());
					txFromDate=txVO.getTransactionDate();
				
					
				}else{					
				diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txFromDate),dateUtil.convertStringDateToSql(txVO.getTransactionDate()) );
				}
				}
				if((diffInDays>0)&&(balance.intValue()>0)){
					
					
					lateFeesPerDay = balance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
					
					tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
					
					lateFees=lateFees.add(tempLateFee);
					
					
					
				}
				if(!isLast){
				balance=balance.add(txVO.getAmount());
				txFromDate=txVO.getTransactionDate();
				}
			}
			
			
		}else if(balance.intValue()>0){
			
			int difInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(toDate),dateUtil.convertStringDateToSql(dueDate) );
			lateFeesPerDay = balance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
			
		    BigDecimal	tmpLateFee=lateFeesPerDay.multiply(new BigDecimal(difInDays));
			
			lateFees=lateFees.add(tmpLateFee);
			
		}*/
		
		if(transactionList.size()>0){
				
				for(int i=0;transactionList.size()>i;i++){
					
					TransactionVO txVO=new TransactionVO();
					BigDecimal tempLateFee=BigDecimal.ZERO;
					
					int diffInDays=0;
					if(isLast){
						diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(toDate),dateUtil.convertStringDateToSql(txFromDate) );
						if((diffInDays>0)&&(netBalance.intValue()>0)){
	 						
	 						
	 						lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
	 						
	 						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
	 						
	 						lateFees=lateFees.add(tempLateFee);
						}
	 						
					}else{
					txVO=(TransactionVO) transactionList.get(i);
					
					if(i==0){
					//	netBalance=netBalance.add(txVO.getAmount());
						//logger.info("-------------"+i);
											
						diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(dateUtil.getPrevOrNextDate(useFromDate,1) ));
						if(transactionList.size()==1){
							isLast=true;
							if((diffInDays>0)&&(netBalance.intValue()>0)){
								
								
								lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
								
								tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
								
								lateFees=lateFees.add(tempLateFee);
								
							
								
							}
							txFromDate=txVO.getTransactionDate();
							--i;
							netBalance=netBalance.add(txVO.getAmount());
							netBalance=netBalance.subtract(txVO.getIntAmount());
						continue;
						}else{
							if((diffInDays>0)&&(netBalance.intValue()>0)){
		 						
		 						
		 						lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
		 						
		 						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
		 						
		 						lateFees=lateFees.add(tempLateFee);
		 					
		 						
		 					}
						}
					}else if((i==(transactionList.size()-1))){
						//logger.info("-------------"+i);
						
						diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(txFromDate) );
						isLast=true;
						--i;
						if((diffInDays>0)&&(netBalance.intValue()>0)){
							
							
							lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
							
							tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
							
							lateFees=lateFees.add(tempLateFee);
						
							
							
						}
						netBalance=netBalance.add(txVO.getAmount());
						netBalance=netBalance.subtract(txVO.getIntAmount());
						txFromDate=txVO.getTransactionDate();
					
						
					}else{					
					diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(txFromDate) );
					
					if((diffInDays>0)&&(netBalance.intValue()>0)){
						
						
						lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
						
						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
						
						lateFees=lateFees.add(tempLateFee);
					
						
					}
					}
					
						
					}
					if(!isLast){
					
					netBalance=netBalance.add(txVO.getAmount());
					netBalance=netBalance.subtract(txVO.getIntAmount());
					txFromDate=txVO.getTransactionDate();
					}
				}
				
				
			}else if((!isLast)&&(netBalance.intValue()>0)){
				
				int diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(dateUtil.getPrevOrNextDate(toDate,0)),dateUtil.convertStringDateToSql(useFromDate) );
				lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
				
			    BigDecimal	tmpLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
				
				lateFees=lateFees.add(tmpLateFee);
			
				
			}
		
		lateTxVO.setAmount(lateFees);
		lateTxVO.setDescription("");
 		
			
		
		}catch (Exception e) {
			logger.error("Exception : Exception while calculating late fee per day "+e);
		}
 		
 		logger.debug("Exit : private BigDecimal calculateLateFeeDailyReducingBalance(BigDecimal balance,ChargesVO chargeVO)");
 		return lateTxVO;
 	}
	
	
	private TransactionVO calculateLateFeeDailyReducingBalanceOnPrincipal(BigDecimal balance,PenaltyChargesVO chargeVO,MemberVO memberVO,String fromDate,String toDate,String dueDate){
 		logger.debug("Entry : private BigDecimal calculateLateFeeDailyReducingBalanceOnPrincipal(BigDecimal balance,ChargesVO chargeVO)");
 		BigDecimal lateFees=BigDecimal.ZERO;
 		Boolean isLast=false;
 		TransactionVO lateTxVO=new TransactionVO();
 		BigDecimal latefeesInHundred=chargeVO.getAmount().divide(hundred);
 		Date currentDate=dateUtil.findCurrentDate();
 		String firstDate=dateUtil.getFirstDateOfFY(currentDate+"");
 		BigDecimal netBalance=BigDecimal.ZERO;
 		BigDecimal lateFeeCharged=BigDecimal.ZERO;
 		String useFromDate="";
		BigDecimal lateFeesPerDay=BigDecimal.ZERO;
		try{
			dueDate=dateUtil.getPrevOrNextDate(dueDate,0);
			int diffs=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(dueDate),dateUtil.convertStringDateToSql(fromDate) );
 			if(diffs>0){
 				useFromDate=dueDate;
 			}else useFromDate=fromDate;
			
 			LedgerTrnsVO achVO=ledgerService.getQuickLedgerTransactionList(memberVO.getSocietyID(), memberVO.getLedgerID(), useFromDate,toDate, "l");
 			balance=achVO.getOpeningBalance().subtract(achVO.getIntOpeningBalance());
 			netBalance=achVO.getOpeningBalance().subtract(achVO.getIntOpeningBalance());
 			//List txList=transactionService.getTransctionList(memberVO.getSocietyID(), memberVO.getLedgerID(), firstDate, toDate, "ALL");
 	 		
 	 		 	 		
 	 		/*if(txList.size()>0){
 				
 				for(int i=0;txList.size()>i;i++){
 					
 					TransactionVO txVO=(TransactionVO) txList.get(i);
 				
 					if(chargeVO.getLedgerID()==txVO.getLedgerID()){
 						lateFeeCharged=lateFeeCharged.add(txVO.getAmount());
 					}
 					
 				}
 				
 				netBalance=netBalance.subtract(lateFeeCharged);
 	 		}*/
		
		
		
		LedgerTrnsVO trnxVO=ledgerService.getQuickLedgerTransactionList(memberVO.getSocietyID(), memberVO.getLedgerID(), useFromDate,toDate, "l");
		String txFromDate=dueDate;
		List transactionList=trnxVO.getTransactionList();
		int txCount=transactionList.size();
		/*if(transactionList.size()>0){
			
			for(int i=0;transactionList.size()>i;i++){
				
				TransactionVO txVO=new TransactionVO();
				BigDecimal tempLateFee=BigDecimal.ZERO;
				int diffInDays=0;
				if(isLast){
					diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(toDate),dateUtil.convertStringDateToSql(dateUtil.getPrevOrNextDate(txFromDate, 1)) );
				}else{
				txVO=(TransactionVO) transactionList.get(i);
				
				if(i==0){
					//logger.info("-------------"+i);
										
					diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(dueDate) );
					if(transactionList.size()==1){
						isLast=true;
						balance=balance.add(txVO.getAmount());
						txFromDate=txVO.getTransactionDate();
						--i;
					}
				}else if(i==(transactionList.size()-1)){
					//logger.info("-------------"+i);
					
					diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(txFromDate) );
					isLast=true;
					--i;
					if((diffInDays>0)&&(balance.intValue()>0)){
						
						
						lateFeesPerDay = balance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
						
						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
						
						lateFees=lateFees.add(tempLateFee);
						
						
						
					}
					balance=balance.add(txVO.getAmount());
					txFromDate=txVO.getTransactionDate();
				
					
				}else{					
				diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txFromDate),dateUtil.convertStringDateToSql(txVO.getTransactionDate()) );
				}
				}
				if((diffInDays>0)&&(balance.intValue()>0)){
					
					
					lateFeesPerDay = balance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
					
					tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
					
					lateFees=lateFees.add(tempLateFee);
					
					
					
				}
				if(!isLast){
				balance=balance.add(txVO.getAmount());
				txFromDate=txVO.getTransactionDate();
				}
			}
			
			
		}else if(balance.intValue()>0){
			
			int difInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(toDate),dateUtil.convertStringDateToSql(dueDate) );
			lateFeesPerDay = balance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
			
		    BigDecimal	tmpLateFee=lateFeesPerDay.multiply(new BigDecimal(difInDays));
			
			lateFees=lateFees.add(tmpLateFee);
			
		}*/
		
		if(transactionList.size()>0){
				
				for(int i=0;transactionList.size()>i;i++){
					
					TransactionVO txVO=new TransactionVO();
					BigDecimal tempLateFee=BigDecimal.ZERO;
					
					int diffInDays=0;
					if(isLast){
						diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(toDate),dateUtil.convertStringDateToSql(txFromDate) );
						if((diffInDays>0)&&(netBalance.intValue()>0)){
	 						
	 						
	 						lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
	 						
	 						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
	 						
	 						lateFees=lateFees.add(tempLateFee);
						}
	 						
					}else{
					txVO=(TransactionVO) transactionList.get(i);
					
					if(i==0){
					//	netBalance=netBalance.add(txVO.getAmount());
						//logger.info("-------------"+i);
											
						diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(dateUtil.getPrevOrNextDate(useFromDate,1) ));
						if(transactionList.size()==1){
							isLast=true;
							if((diffInDays>0)&&(netBalance.intValue()>0)){
								
								
								lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
								
								tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
								
								lateFees=lateFees.add(tempLateFee);
								
							
								
							}
							txFromDate=txVO.getTransactionDate();
							--i;
							netBalance=netBalance.add(txVO.getAmount());
							netBalance=netBalance.subtract(txVO.getIntAmount());
						continue;
						}else{
							if((diffInDays>0)&&(netBalance.intValue()>0)){
		 						
		 						
		 						lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
		 						
		 						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
		 						
		 						lateFees=lateFees.add(tempLateFee);
		 					
		 						
		 					}
						}
					}else if((i==(transactionList.size()-1))){
						//logger.info("-------------"+i);
						
						diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(txFromDate) );
						isLast=true;
						--i;
						if((diffInDays>0)&&(netBalance.intValue()>0)){
							
							
							lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
							
							tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
							
							lateFees=lateFees.add(tempLateFee);
						
							
							
						}
						netBalance=netBalance.add(txVO.getPrincipleAmount());
						//netBalance=netBalance.subtract(txVO.getIntAmount());
						txFromDate=txVO.getTransactionDate();
					
						
					}else{					
					diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(txFromDate) );
					
					if((diffInDays>0)&&(netBalance.intValue()>0)){
						
						
						lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
						
						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
						
						lateFees=lateFees.add(tempLateFee);
					
						
					}
					}
					
						
					}
					if(!isLast){
					
					netBalance=netBalance.add(txVO.getPrincipleAmount());
					//netBalance=netBalance.subtract(txVO.getIntAmount());
					txFromDate=txVO.getTransactionDate();
					}
				}
				
				
			}else if((!isLast)&&(netBalance.intValue()>0)){
				
				int diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(dateUtil.getPrevOrNextDate(toDate,0)),dateUtil.convertStringDateToSql(useFromDate) );
				lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
				
			    BigDecimal	tmpLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
				
				lateFees=lateFees.add(tmpLateFee);
			
				
			}
		
		lateTxVO.setAmount(lateFees);
		lateTxVO.setDescription("");
 		
			
		
		}catch (Exception e) {
			logger.error("Exception : Exception while calculating late fee per day "+e);
		}
 		
 		logger.debug("Exit : private BigDecimal calculateLateFeeDailyReducingBalanceOnPrincipal(BigDecimal balance,ChargesVO chargeVO)");
 		return lateTxVO;
 	}
	
	
	private TransactionVO calculateLateFeeDailyReducingBalanceAfterDueDateOnPrincipal(BigDecimal balance,PenaltyChargesVO chargeVO,MemberVO memberVO,String fromDate,String toDate,String dueDate){
 		logger.debug("Entry : private BigDecimal calculateLateFeeDailyReducingBalanceAfterDueDateOnPrincipal(BigDecimal balance,ChargesVO chargeVO)");
 		BigDecimal lateFees=BigDecimal.ZERO;
 		Boolean isLast=false;
 		TransactionVO lateTxVO=new TransactionVO();
 		BigDecimal latefeesInHundred=chargeVO.getAmount().divide(hundred);
 		Date currentDate=dateUtil.findCurrentDate();
 		String firstDate=dateUtil.getFirstDateOfFY(currentDate+"");
 		BigDecimal netBalance=BigDecimal.ZERO;
 		BigDecimal lateFeeCharged=BigDecimal.ZERO;
 		String useFromDate="";
		BigDecimal lateFeesPerDay=BigDecimal.ZERO;
		try{
			dueDate=dateUtil.getPrevOrNextDate(dueDate,0);
			int diffs=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(dueDate),dateUtil.convertStringDateToSql(fromDate) );
 			if(diffs>0){
 				useFromDate=dueDate;
 			}else useFromDate=fromDate;
			
 			LedgerTrnsVO achVO=ledgerService.getQuickLedgerTransactionList(memberVO.getSocietyID(), memberVO.getLedgerID(), useFromDate,toDate, "l");
 			balance=achVO.getOpeningBalance().subtract(achVO.getIntOpeningBalance());
 			netBalance=achVO.getOpeningBalance().subtract(achVO.getIntOpeningBalance());
 			//List txList=transactionService.getTransctionList(memberVO.getSocietyID(), memberVO.getLedgerID(), firstDate, toDate, "ALL");
 	 		
 	 		 	 		
 	 		/*if(txList.size()>0){
 				
 				for(int i=0;txList.size()>i;i++){
 					
 					TransactionVO txVO=(TransactionVO) txList.get(i);
 				
 					if(chargeVO.getLedgerID()==txVO.getLedgerID()){
 						lateFeeCharged=lateFeeCharged.add(txVO.getAmount());
 					}
 					
 				}
 				
 				netBalance=netBalance.subtract(lateFeeCharged);
 	 		}*/
		
		
		
		LedgerTrnsVO trnxVO=ledgerService.getQuickLedgerTransactionList(memberVO.getSocietyID(), memberVO.getLedgerID(), useFromDate,toDate, "l");
		String txFromDate=dueDate;
		List transactionList=trnxVO.getTransactionList();
		int txCount=transactionList.size();
		/*if(transactionList.size()>0){
			
			for(int i=0;transactionList.size()>i;i++){
				
				TransactionVO txVO=new TransactionVO();
				BigDecimal tempLateFee=BigDecimal.ZERO;
				int diffInDays=0;
				if(isLast){
					diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(toDate),dateUtil.convertStringDateToSql(dateUtil.getPrevOrNextDate(txFromDate, 1)) );
				}else{
				txVO=(TransactionVO) transactionList.get(i);
				
				if(i==0){
					//logger.info("-------------"+i);
										
					diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(dueDate) );
					if(transactionList.size()==1){
						isLast=true;
						balance=balance.add(txVO.getAmount());
						txFromDate=txVO.getTransactionDate();
						--i;
					}
				}else if(i==(transactionList.size()-1)){
					//logger.info("-------------"+i);
					
					diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(txFromDate) );
					isLast=true;
					--i;
					if((diffInDays>0)&&(balance.intValue()>0)){
						
						
						lateFeesPerDay = balance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
						
						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
						
						lateFees=lateFees.add(tempLateFee);
						
						
						
					}
					balance=balance.add(txVO.getAmount());
					txFromDate=txVO.getTransactionDate();
				
					
				}else{					
				diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txFromDate),dateUtil.convertStringDateToSql(txVO.getTransactionDate()) );
				}
				}
				if((diffInDays>0)&&(balance.intValue()>0)){
					
					
					lateFeesPerDay = balance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
					
					tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
					
					lateFees=lateFees.add(tempLateFee);
					
					
					
				}
				if(!isLast){
				balance=balance.add(txVO.getAmount());
				txFromDate=txVO.getTransactionDate();
				}
			}
			
			
		}else if(balance.intValue()>0){
			
			int difInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(toDate),dateUtil.convertStringDateToSql(dueDate) );
			lateFeesPerDay = balance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
			
		    BigDecimal	tmpLateFee=lateFeesPerDay.multiply(new BigDecimal(difInDays));
			
			lateFees=lateFees.add(tmpLateFee);
			
		}*/
		
		if(transactionList.size()>0){
				
				for(int i=0;transactionList.size()>i;i++){
					
					TransactionVO txVO=new TransactionVO();
					BigDecimal tempLateFee=BigDecimal.ZERO;
					
					int diffInDays=0;
					if(isLast){
						diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(toDate),dateUtil.convertStringDateToSql(txFromDate) );
						if((diffInDays>0)&&(netBalance.intValue()>0)){
	 						
	 						
	 						lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
	 						
	 						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
	 						
	 						lateFees=lateFees.add(tempLateFee);
						}
	 						
					}else{
					txVO=(TransactionVO) transactionList.get(i);
					
					if(i==0){
					//	netBalance=netBalance.add(txVO.getAmount());
						//logger.info("-------------"+i);
											
						diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(dateUtil.getPrevOrNextDate(useFromDate,1) ));
						if(transactionList.size()==1){
							isLast=true;
							if((diffInDays>0)&&(netBalance.intValue()>0)){
								
								
								lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
								
								tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
								
								lateFees=lateFees.add(tempLateFee);
								
							
								
							}
							txFromDate=txVO.getTransactionDate();
							--i;
							netBalance=netBalance.add(txVO.getAmount());
							netBalance=netBalance.subtract(txVO.getIntAmount());
						continue;
						}else{
							if((diffInDays>0)&&(netBalance.intValue()>0)){
		 						
		 						
		 						lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
		 						
		 						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
		 						
		 						lateFees=lateFees.add(tempLateFee);
		 					
		 						
		 					}
						}
					}else if((i==(transactionList.size()-1))){
						//logger.info("-------------"+i);
						
						diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(txFromDate) );
						isLast=true;
						--i;
						if((diffInDays>0)&&(netBalance.intValue()>0)){
							
							
							lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
							
							tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
							
							lateFees=lateFees.add(tempLateFee);
						
							
							
						}
						netBalance=netBalance.add(txVO.getAmount());
						netBalance=netBalance.subtract(txVO.getIntAmount());
						txFromDate=txVO.getTransactionDate();
					
						
					}else{					
					diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(txFromDate) );
					
					if((diffInDays>0)&&(netBalance.intValue()>0)){
						
						
						lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
						
						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
						
						lateFees=lateFees.add(tempLateFee);
					
						
					}
					}
					
						
					}
					if(!isLast){
					
					netBalance=netBalance.add(txVO.getAmount());
					netBalance=netBalance.subtract(txVO.getIntAmount());
					txFromDate=txVO.getTransactionDate();
					}
				}
				
				
			}else if((!isLast)&&(netBalance.intValue()>0)){
				
				int diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(dateUtil.getPrevOrNextDate(toDate,0)),dateUtil.convertStringDateToSql(useFromDate) );
				lateFeesPerDay = netBalance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
				
			    BigDecimal	tmpLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
				
				lateFees=lateFees.add(tmpLateFee);
			
				
			}
		
		lateTxVO.setAmount(lateFees);
		lateTxVO.setDescription("");
 		
			
		
		}catch (Exception e) {
			logger.error("Exception : Exception while calculateLateFeeDailyReducingBalanceAfterDueDateOnPrincipal "+e);
		}
 		
 		logger.debug("Exit : private BigDecimal calculateLateFeeDailyReducingBalanceAfterDueDateOnPrincipal(BigDecimal balance,ChargesVO chargeVO)");
 		return lateTxVO;
 	}
	
	
	private TransactionVO calculateLateFeeWithDailyFixedAmount(BigDecimal balance,PenaltyChargesVO chargeVO,MemberVO memberVO,String fromDate,String toDate,String dueDate){
 		logger.debug("Entry : private BigDecimal calculateLateFeeDailyReducingBalance(BigDecimal balance,ChargesVO chargeVO)");
 		BigDecimal lateFees=BigDecimal.ZERO;
 		Boolean isLast=false;
 		TransactionVO lateTxVO=new TransactionVO();
 		BigDecimal latefeesInHundred=chargeVO.getAmount().divide(hundred);
 		Date currentDate=dateUtil.findCurrentDate();
 		String firstDate=dateUtil.getFirstDateOfFY(currentDate+"");
 		BigDecimal netBalance=BigDecimal.ZERO;
 		BigDecimal lateFeeCharged=BigDecimal.ZERO;
 		String useFromDate="";
 		
 		
 		int difInDays=0;
 		//String fromDate=dateUtil.getFirstDateOfFY(currentDate+"");
		
		BigDecimal lateFeesPerDay=chargeVO.getAmount();
 		try{
 			
 			int diffs=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(dueDate),dateUtil.convertStringDateToSql(fromDate) );
 			if(diffs>0){
 				useFromDate=dueDate;
 			}else useFromDate=fromDate;
			
 			AccountHeadVO achVO=ledgerService.getOpeningClosingBalance(memberVO.getSocietyID(), memberVO.getLedgerID(), useFromDate, toDate, "l");
 			balance=achVO.getOpeningBalance();
 			netBalance=achVO.getOpeningBalance();
 			List txList=transactionService.getTransctionList(memberVO.getSocietyID(), memberVO.getLedgerID(), firstDate, toDate, "ALL");
 	 		
 	 		
 	 		
 	 		if(txList.size()>0){
 				
 				for(int i=0;txList.size()>i;i++){
 					
 					TransactionVO txVO=(TransactionVO) txList.get(i);
 				
 					if(chargeVO.getLedgerID()==txVO.getLedgerID()){
 						lateFeeCharged=lateFeeCharged.add(txVO.getAmount());
 					}
 					
 				}
 				
 				netBalance=netBalance.subtract(lateFeeCharged);
 	 		}
 			 			
 			List transactionList=transactionService.getTransctionList(memberVO.getSocietyID(), memberVO.getLedgerID(), useFromDate, toDate, "ALL");
 			String txFromDate=useFromDate;
 			int txCount=transactionList.size();
 			if(transactionList.size()>0){
 				
 				for(int i=0;transactionList.size()>i;i++){
 					
 					TransactionVO txVO=new TransactionVO();
 					BigDecimal tempLateFee=BigDecimal.ZERO;
 					
 					int diffInDays=0;
 					if(isLast){
 						diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(toDate),dateUtil.convertStringDateToSql(txFromDate) );
 						if((diffInDays>0)&&(netBalance.intValue()>0)){
 	 						
 	 						
 	 						//lateFeesPerDay = balance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
 	 						
 	 						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
 	 						
 	 						lateFees=lateFees.add(tempLateFee);
 	 						difInDays=difInDays+diffInDays;}
 	 						
 					}else{
 					txVO=(TransactionVO) transactionList.get(i);
 					
 					if(i==0){
 						netBalance=netBalance.add(txVO.getAmount());
 						//logger.info("-------------"+i);
 											
 						diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(dateUtil.getPrevOrNextDate(useFromDate,1) ));
 						if(transactionList.size()==1){
 							isLast=true;
 							if((diffInDays>0)&&(netBalance.intValue()>0)){
 								
 								
 								//lateFeesPerDay = balance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
 								
 								tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
 								
 								lateFees=lateFees.add(tempLateFee);
 								
 								difInDays=difInDays+diffInDays;
 								
 							}
 							txFromDate=txVO.getTransactionDate();
 							--i;
 							//netBalance=netBalance.add(txVO.getAmount());
 						continue;
 						}else{
 							if((diffInDays>0)&&(netBalance.intValue()>0)){
 		 						
 		 						
 		 						//lateFeesPerDay = balance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
 		 						
 		 						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
 		 						
 		 						lateFees=lateFees.add(tempLateFee);
 		 						difInDays=difInDays+diffInDays;
 		 						
 		 					}
 						}
 					}else if((i==(transactionList.size()-1))){
 						//logger.info("-------------"+i);
 						
 						diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(txFromDate) );
 						isLast=true;
 						--i;
 						if((diffInDays>0)&&(netBalance.intValue()>0)){
 							
 							
 							//lateFeesPerDay = balance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
 							
 							tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
 							
 							lateFees=lateFees.add(tempLateFee);
 							difInDays=difInDays+diffInDays;
 							
 							
 						}
 						netBalance=netBalance.add(txVO.getAmount());
 						txFromDate=txVO.getTransactionDate();
 					
 						
 					}else{					
 					diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(txVO.getTransactionDate()),dateUtil.convertStringDateToSql(txFromDate) );
 					
 					if((diffInDays>0)&&(netBalance.intValue()>0)){
 						
 						
 						//lateFeesPerDay = balance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
 						
 						tempLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
 						
 						lateFees=lateFees.add(tempLateFee);
 						difInDays=difInDays+diffInDays;
 						
 					}
 					}
 					
 						
 					}
 					if(!isLast){
 					if(i!=0)
 					netBalance=netBalance.add(txVO.getAmount());
 					txFromDate=txVO.getTransactionDate();
 					}
 				}
 				
 				
 			}else if((!isLast)&&(netBalance.intValue()>0)){
 				
 				int diffInDays=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(dateUtil.getPrevOrNextDate(toDate,0)),dateUtil.convertStringDateToSql(useFromDate) );
 				//lateFeesPerDay = balance.multiply(latefeesInHundred).setScale(2,RoundingMode.HALF_UP);
 				
 			    BigDecimal	tmpLateFee=lateFeesPerDay.multiply(new BigDecimal(diffInDays));
 				
 				lateFees=lateFees.add(tmpLateFee);
 				difInDays=difInDays+diffInDays;
 				
 			}
			
			lateTxVO.setAmount(lateFees);
			lateTxVO.setDescription("Late fee : "+difInDays+" days X  "+chargeVO.getAmount()+" (per day) = "+lateFees);
 			
		}catch (Exception e) {
			logger.error("Exception : Exception while calculating late fee per day "+e);
		}
 		
 		logger.debug("Exit : private BigDecimal calculateLateFeeDailyReducingBalance(BigDecimal balance,ChargesVO chargeVO)");
 		return lateTxVO;
 	}
	
	/**
	 * @param ledgerService the ledgerService to set
	 */
	public void setLedgerService(LedgerService ledgerService) {
		this.ledgerService = ledgerService;
	}

	/**
	 * @param societyService the societyService to set
	 */
	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}

	/**
	 * @param transactionService the transactionService to set
	 */
	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	/**
	 * @param invoiceService the invoiceService to set
	 */
	public void setInvoiceService(InvoiceService invoiceService) {
		this.invoiceService = invoiceService;
	}


}
