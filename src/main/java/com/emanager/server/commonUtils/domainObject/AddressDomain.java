package com.emanager.server.commonUtils.domainObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;

import com.emanager.server.commonUtils.dataaccessObject.AddressDAO;
import com.emanager.server.commonUtils.services.AddressService;
import com.emanager.server.commonUtils.valueObject.AddressVO;


public class AddressDomain {
	
	public AddressDAO addressDAO;
	private static final Logger logger = Logger.getLogger(AddressDomain.class);
	
	public AddressVO getAddressDetails(int strMemberID,String aptName,String strPersonCategory,int societyID){
		List addressList=null;
		AddressVO addrVO = new AddressVO();	
		try{
			
			logger.debug("inside getAddressDetails(String strMemberID,String strCategoryID,String strPersonCategory)");
			 
			addrVO = addressDAO.getAddressDetails(strMemberID, "1", strPersonCategory);
			
			
			if(addrVO.getAddID()==0){
				addressList=getSocietyAddressDetails(societyID, "S", "s");
				addrVO=(AddressVO) addressList.get(0);
				addrVO.setAddress(aptName+" ,"+addrVO.getAddress());
				
				
			}
			
			logger.debug("outside getAddressDetails domain Address ID : memberId:  "+strMemberID+" AddID: " + addrVO.getAddID());
			 
		 }	catch(Exception Ex){
			 
			 logger.error("domain error");
			 
		 }
		
		 return addrVO;
		 
			
		}
		
	
	public List getAddressDetailsRenter(int strRenterID,String strPersonCategory,int societyID,String aptName){
		
		List adressList=null;
		AddressVO addressVO=new AddressVO();
		try{		
				logger.debug("Entry public List getAddressDetailsRenter(String strRenterID,String strPersonCategory)"+strRenterID);
				
				adressList=addressDAO.getAddressDetailsRenter(strRenterID, strPersonCategory);
	           
				logger.debug("Exit public List getAddressDetailsRenter(String strRenterID,String strPersonCategory)"+adressList.size());
            
			if(adressList.size()==0){
				
				logger.info("No Address Found for this RenterId : "+strRenterID);
				adressList=getSocietyAddressDetails(societyID, "S", "s");
				
					addressVO=(AddressVO) adressList.get(0);
					addressVO.setAddress(aptName+" , "+addressVO.getAddress());
					adressList.remove(0);					
					adressList.add(0,addressVO);
			}
			}catch (Exception e) {
				logger.error("Exception in getAddressDetailsRenter :" + e);
			}

			return adressList;
	 
			
		}
		
	public List getAddressDetailsList(int strPersonID,String strPersonCategory,int societyID,String aptName) {
		
		List adressList=null;
		AddressVO addressVO=new AddressVO();
		try {
			logger.debug("Entry public List getAddressDetailsList(String strPersonID,String strPersonCategory)");
			
			adressList=addressDAO.getAddressDetailsList(strPersonID, strPersonCategory);
           
			logger.debug("Exit public List getAddressDetailsList(String strPersonID,String strPersonCategory)"+adressList.size());
			
			if(adressList.size()==0){
				adressList=getSocietyAddressDetails(societyID, "S", "s");
				
					 addressVO=(AddressVO) adressList.get(0);
					addressVO.setAddress(aptName+" , "+addressVO.getAddress());
					adressList.remove(0);
					adressList.add(0,addressVO);
				}
				
				logger.info(aptName+"No Address Found for this MemberId  : "+strPersonID);
				

		}catch (Exception e) {
			logger.error("Exception in getAddressDetailsList :" + e);
		}

		return adressList;

	}
	
	
	public int addUpdateAddressDetails(AddressVO addressVO,int strMemberID){
			 
		int flag=0;
			try{
				logger.debug("Entry : public AddressVO addUpdateAddressDetails(AddressVO addressVO,String strMemberID)");
			   if(!addressVO.getIsSocietyAddress()){
				if(addressVO.getAddID()==0 ){			    	 
			    	 flag =  addressDAO.addNewAddressDetails(addressVO,strMemberID); 	    	 
			     }else{			    	 
			    	 flag =  addressDAO.updateAddressDetails(addressVO); 			    	 
			     }
			   }else{
				   logger.debug("Society Address Cannot update " );
				   
			   }
			     logger.debug("Exit : public AddressVO addUpdateAddressDetails(AddressVO addressVO,String strMemberID)");
			 }catch(NullPointerException Ex){
				 logger.info("Exception in addUpdateAddressDetails :  No details found  for "+strMemberID+"    "+Ex); 				 
			 }catch(Exception Ex){
				 logger.error("Exception in addUpdateAddressDetails : "+Ex); 				 
			 }			
			 return flag;
			 
				
			}
	
	
	/*public List getRecentUpdateList(String strSocietyID){
		 
		List lstRecentUpdate = null;
			
		try{
			
			logger.debug("inside domain");
			 
			lstRecentUpdate = addressDAO.getRecentUpdateList(strSocietyID);
			
			
			
			
			 
		 }	catch(Exception Ex){
			 
			 logger.error("domain error"+Ex);
			 
		 }
		
		 return lstRecentUpdate;
		 
			
		}*/

	public int removeContactDetails(String addressID){
		
		int success=0;
		try{
		
			logger.debug("Entry : public int removeContactDetails(String addressID)");
			
			success=addressDAO.removeContactDetails(addressID);			
		
			logger.debug("Exit : public int removeContactDetails(String addressID)"+success);
		}catch(Exception Ex){
			 logger.error("Exception in removeContactDetails : "+Ex); 				 
		}			
	return success;	
	}

	
	public List getSocietyAddressDetails(int societyID,String strCategoryID,String strPermanantCategory)
	{
		List addressList=null;
		try
		{
		logger.debug("Entry : public List getSocietyAddressDetails(String societyID,String strCategoryID,String strPermanantCategory )");
		addressList=addressDAO.getSocietyAddressDetails(societyID, strCategoryID, strPermanantCategory);		
		}
		catch(Exception ex){
			logger.error("Exception in getSocietyAddressDetails : "+ex);			
		}
		logger.debug("Exit : public List getSocietyAddressDetails(String societyID,String strCategoryID,String strPermanantCategory )");
		return addressList;
		
	}
	
		public AddressDAO getAddressDAO() {
			return addressDAO;
		}


		public void setAddressDAO(AddressDAO addressDAO) {
			this.addressDAO = addressDAO;
		}
		

	
	

}
