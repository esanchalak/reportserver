package com.emanager.server.commonUtils.domainObject;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.joda.time.MutableDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.financialReports.valueObject.RptTransactionVO;
public class DateUtility 
{
	Logger logger=Logger.getLogger(this.getClass().getName());
	
	static int  i=0;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	public int getDateDifference(Date currentDate,Date previousDate)
	{
		Months diff=null;
		int monthDifference = 0;
		try
		{
			logger.debug("Entry:inside getDateDifference(Date currentDate,Date previousDate) and DateUtility");
			
		DateTime start=new DateTime(currentDate);
		DateTime end=new DateTime(previousDate);
		 diff=Months.monthsBetween(end,start);
		 monthDifference=diff.getMonths();
		// logger.info(currentDate+"From "+previousDate+"month Difference is:-"+monthDifference);
		}catch(Exception ex)
		{
			logger.error("Exception:"+ex);
		}
		logger.debug("Exit:getDateDifference(Date currentDate,Date previousDate) and DateUtility");		
		return monthDifference;
	}	
	public Date getMonthLastDate(int month,int year)
	{
		Date lastDate=null;
		MutableDateTime monthLastDate;
		SimpleDateFormat simpleDateFormat;
		try
		{
			logger.debug("Entry:inside getMonthLastDate(int month,int year) and DateUtility");
			monthLastDate=new MutableDateTime();
			monthLastDate.setDayOfMonth(1);
			if(month==13){
				month=1;
				year=year+1;
			}
			
			monthLastDate.setMonthOfYear(month);
			monthLastDate.setYear(year);
			int a=monthLastDate.dayOfMonth().getMaximumValue();
			//logger.info(">>>>>"+a);
			String concat=year+"-"+month+"-"+a;
			//logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+concat);
			simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date lastdate=simpleDateFormat.parse(concat);
			lastDate=new java.sql.Date(lastdate.getTime());
			
			logger.debug("Entry:inside getMonthLastDate(int month,int year) and DateUtility");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.error("Exception:"+ex);
		}
		
		return lastDate;
	}
	public Date getMonthFirstDate(int index,int year)
	{		
		SimpleDateFormat simpleDateFormate;		
		MutableDateTime monthFirstDate;
		java.sql.Date firstDate = null;
		try
		{
			logger.debug("Entry:inside getMonthFirstDate(int index,int year) and DateUtility class");
		monthFirstDate=new MutableDateTime();		
		monthFirstDate.setDayOfMonth(1);
		monthFirstDate.setMonthOfYear(index);
		monthFirstDate.setYear(year);
		String s=monthFirstDate.toString();
		simpleDateFormate=new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date firstdate=simpleDateFormate.parse(s);
		 firstDate=new java.sql.Date(firstdate.getTime());
		
		 logger.debug("Exit: getMonthFirstDate(int index,int year) and DateUtility class");
		}catch(Exception ex)
			{
			logger.error("Error:"+ex);
			}
		
		return firstDate;
	}
	
	public Date findCurrentDate()
	{ 
		Calendar calendar=Calendar.getInstance();
	java.util.Date sd=calendar.getTime();
	final java.sql.Date currentDate=new java.sql.Date(sd.getTime());
		
		return currentDate;
	}
	
	public Boolean isLateFeeApplicable(RptTransactionVO rptVO)
	{
		Boolean status=false;
		try
		{
			logger.debug("Entry:public Boolean isLateFeeApplicable(RptTransactionVO rptVO)");
			java.util.Date today=new java.util.Date();
			Calendar lastPdDtCal = Calendar.getInstance();
			lastPdDtCal.setTime(rptVO.getTx_to_date());
			
			int month=rptVO.getMonthParam();
			int day=rptVO.getDayParam();
			
			lastPdDtCal.add(Calendar.MONTH, month);
			lastPdDtCal.set(Calendar.DAY_OF_MONTH, day);
			
			Calendar calendar=Calendar.getInstance();
			calendar.setTime(today);
			
			if (lastPdDtCal.before(calendar)) {
				status=true;
			}else{
				status=false;
			}
				
					
			logger.debug(rptVO.getMember_name()+" *********"+calendar.getTime()+"**********here "+status+" is the date-------"+lastPdDtCal.getTime());
		// logger.info("month Difference is:-"+monthDifference);
		}catch(Exception ex)
		{
			logger.error("Exception:"+ex);
		}
		logger.debug("Exit:public Boolean isLateFeeApplicable(RptTransactionVO rptVO)");		
		return status;
	}	
	
	public Date getFYyearFirstDay(Date date)
	{		
		SimpleDateFormat simpleDateFormate;		
		java.sql.Date firstDate = null;
		try
		{
			logger.debug("Entry:inside getMonthFirstDate(int index,int year) and DateUtility class");
			Calendar cal = Calendar.getInstance();
		    cal.setTime(date);
		    int year = cal.get(Calendar.YEAR);
		    int month=cal.get(Calendar.MONTH);
		    logger.debug(year+" - "+month);
		    
		String fyFirstDay=year+"-04-01";
		simpleDateFormate=new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date firstdate=simpleDateFormate.parse(fyFirstDay);
		 firstDate=new java.sql.Date(firstdate.getTime());
		
		 logger.debug("Exit: getMonthFirstDate(int index,int year) and DateUtility class");
		}catch(Exception ex)
			{
			logger.error("Error:"+ex);
			}
		
		return firstDate;
	}
	
	public String getPrevOrNextDate(String sDate,int isNextDate)
	{		
		String outputDate="";
		try
		{	
			SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd" );   
			Calendar calFrm = Calendar.getInstance();    
			Calendar calTo=Calendar.getInstance();
			
			if(isNextDate==1){
			
			calFrm.setTime( dateFormat.parse(sDate)); 
			calFrm.add( Calendar.DATE, -1 ); 
			outputDate=dateFormat.format(calFrm.getTime());
			}else if(isNextDate==0){
				calTo.setTime(dateFormat.parse(sDate));
				calTo.add(Calendar.DATE,1);
				outputDate=dateFormat.format(calTo.getTime());
			}
			   
			
		}catch(Exception ex)
			{
			logger.error("Error:"+ex);
			}
		
		return outputDate;
	}
	
	public String getConvetedDate(String oldDate){
		logger.info("Entry : public String getConvetedDate(String oldDate)"+oldDate);
		String newDateString="";
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date newDate=sdf.parse(oldDate);
		logger.debug("convert date-"+newDate);
		SimpleDateFormat sDateFormat = new SimpleDateFormat("dd-MMM-yy");
		
		newDateString=sDateFormat.format(newDate);
		logger.debug("convert date-"+newDateString);
		}catch(NullPointerException ex){
			logger.info("No date found to convert  ");
			newDateString="NA";
		}catch(Exception e){
			logger.error("Exception in getConvertedDate : "+e);
		}
		
		logger.info("Exit : public String getConvetedDate(String oldDate)"+newDateString);
		return newDateString;
	}
	
	
	public String getConvetedDateTimeStamptoDate(String oldDate){
		logger.debug("Entry : public String getConvetedDate(String oldDate)"+oldDate);
		String newDateString="";
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.util.Date newDate=sdf.parse(oldDate);
		logger.debug("convert date-"+newDate);
		SimpleDateFormat sDateFormat = new SimpleDateFormat("dd-MMM-yy");
		
		newDateString=sDateFormat.format(newDate);
		logger.debug("convert date-"+newDateString);
		}catch(NullPointerException ex){
			logger.info("No date found to convert  ");
			newDateString="NA";
		}catch(Exception e){
			logger.error("Exception in getConvertedDate : "+e);
		}
		
		logger.debug("Exit : public String getConvetedDate(String oldDate)");
		return newDateString;
	}
	
	public ReportVO getReportDate(String fromDate,String uptoDate)
	{
		ReportVO dateVO= new ReportVO();
		String convertedFrmDate=null;
		String convertedToDate=null;
		try{
			DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");	            

	        LocalDate frmDt = formatter.parseLocalDate(fromDate);		       
	        int monthFrmDt=frmDt.getMonthOfYear();
	        int yearFrmDt=frmDt.getYear();		        
	        logger.debug("From Date :"+frmDt+" Month: "+monthFrmDt+" Year: "+yearFrmDt);
	        
	        LocalDate toDt = formatter.parseLocalDate(uptoDate);		       
	        int monthToDt=toDt.getMonthOfYear();
	        int yearToDt=toDt.getYear();		        
	        logger.debug("To Date :"+toDt+" Month: "+monthToDt+" Year: "+yearToDt);
	        
	        int monthResult =(monthToDt+1)-monthFrmDt;
	        int yearResult=yearToDt-yearFrmDt;
	        logger.debug("Month Result:"+monthResult+" Year Result: "+yearResult);
	        if(monthResult==1 && yearResult==0){
	        	SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd");
	        	SimpleDateFormat sdf = new SimpleDateFormat("MMM-yy");
	        	java.util.Date date = sdfSource.parse(fromDate);
	        	convertedFrmDate=sdf.format(date);			           
	        	dateVO.setFromDate(convertedFrmDate);	
	            Calendar cal = Calendar.getInstance();
	           	cal.setTime(date);
	            cal.add(Calendar.MONTH, -1);
	            cal.set(Calendar.DAY_OF_MONTH,
	            cal.getActualMaximum(Calendar.DAY_OF_MONTH));
	            dateVO.setUptDate(sdf.format(cal.getTime()));			         	
	        	
	            //date previous month first day
	            Calendar cal1 = Calendar.getInstance();
	            cal1.setTime(date);
	            cal1.add(Calendar.MONTH, -1);
	            cal1.set(Calendar.DAY_OF_MONTH,1);
	        
	            dateVO.setRptFromDate(sdfSource.format(cal1.getTime()));	
	            dateVO.setRptUptoDate(sdfSource.format(cal.getTime()));
	           	
	        	
	        }else if((monthResult==3)  ){
	        	SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd");
	        	SimpleDateFormat sdf = new SimpleDateFormat("MMM");
	        	SimpleDateFormat sdfYear = new SimpleDateFormat("yy");
	        	java.util.Date frmDate = sdfSource.parse(fromDate);
	        	java.util.Date toDate = sdfSource.parse(uptoDate);		         
	        	convertedFrmDate= sdf.format(frmDate)+"-"+sdf.format(toDate)+"-"+sdfYear.format(frmDate);
	        	dateVO.setFromDate(convertedFrmDate);
	        	        	
	           	java.util.Date frmPrevDate = sdfSource.parse(fromDate);		           	
	           	Calendar cal = Calendar.getInstance();
	           	cal.setTime(frmPrevDate);
	            cal.add(Calendar.MONTH, -3);
	            java.util.Date toPrevDate = sdfSource.parse(uptoDate);	
	            Calendar cal1 = Calendar.getInstance();	            
	           	cal1.setTime(toPrevDate);
	            cal1.add(Calendar.MONTH, -3);
	            cal1.set(Calendar.DAY_OF_MONTH,
	    	    cal1.getActualMaximum(Calendar.DAY_OF_MONTH));
	            convertedToDate=sdf.format(cal.getTime())+"-"+sdf.format(cal1.getTime())+"-"+sdfYear.format(cal.getTime());
	            dateVO.setUptDate(convertedToDate);
	            
	            //date previous year
	            dateVO.setRptFromDate(sdfSource.format(cal.getTime()));	
	            dateVO.setRptUptoDate(sdfSource.format(cal1.getTime()));
	   	        	
	        }else if((monthResult==6 ) ){
	        	SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd");
	        	SimpleDateFormat sdf = new SimpleDateFormat("MMM");
	        	SimpleDateFormat sdfYear = new SimpleDateFormat("yy");
	        	java.util.Date frmDate = sdfSource.parse(fromDate);
	        	java.util.Date toDate = sdfSource.parse(uptoDate);		         
	        	convertedFrmDate= sdf.format(frmDate)+"-"+sdf.format(toDate)+"-"+sdfYear.format(frmDate);
	        	dateVO.setFromDate(convertedFrmDate);
	        	
	           	java.util.Date frmPrevDate = sdfSource.parse(fromDate);	
	           	Calendar cal = Calendar.getInstance();
	           	cal.setTime(frmPrevDate);
	            cal.add(Calendar.MONTH, -6);
	        	java.util.Date toPrevDate = sdfSource.parse(uptoDate);	
	            Calendar cal1 = Calendar.getInstance();	            
	           	cal1.setTime(toPrevDate);
	            cal1.add(Calendar.MONTH, -6);
	            cal1.set(Calendar.DAY_OF_MONTH,
	            cal1.getActualMaximum(Calendar.DAY_OF_MONTH));

	            convertedToDate=sdf.format(cal.getTime())+"-"+sdf.format(cal1.getTime())+"-"+sdfYear.format(cal.getTime());
	            dateVO.setUptDate(convertedToDate);
	            
	          //date previous year
	            dateVO.setRptFromDate(sdfSource.format(cal.getTime()));	
	            dateVO.setRptUptoDate(sdfSource.format(cal1.getTime()));
	   	        	
	        }else if((monthResult==-6 ) ){
	        	SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd");
	        	SimpleDateFormat sdf = new SimpleDateFormat("MMM");
	        	SimpleDateFormat sdfYear = new SimpleDateFormat("yy");
	        	SimpleDateFormat sdfYear1 = new SimpleDateFormat("yy");
	        	java.util.Date frmDate = sdfSource.parse(fromDate);
	        	java.util.Date toDate = sdfSource.parse(uptoDate);		         
	        	convertedFrmDate= sdf.format(frmDate)+"-"+sdf.format(toDate)+"-"+sdfYear.format(frmDate)+"-"+sdfYear1.format(toDate);
	        	dateVO.setFromDate(convertedFrmDate);
	        	
	           	java.util.Date frmPrevDate = sdfSource.parse(fromDate);	
	           	Calendar cal = Calendar.getInstance();
	           	cal.setTime(frmPrevDate);
	            cal.add(Calendar.MONTH, -6);
	        	java.util.Date toPrevDate = sdfSource.parse(uptoDate);	
	            Calendar cal1 = Calendar.getInstance();	            
	           	cal1.setTime(toPrevDate);
	            cal1.add(Calendar.MONTH, -6);
	            cal1.set(Calendar.DAY_OF_MONTH,
	    	    cal1.getActualMaximum(Calendar.DAY_OF_MONTH));
	            convertedToDate=sdf.format(cal.getTime())+"-"+sdf.format(cal1.getTime())+"-"+sdfYear.format(cal.getTime());
	            dateVO.setUptDate(convertedToDate);
	            
	          //date previous year
	            dateVO.setRptFromDate(sdfSource.format(cal.getTime()));	
	            dateVO.setRptUptoDate(sdfSource.format(cal1.getTime()));
	   	        	
	        }else if(yearResult==1){
	        	SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd");
	        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
	        	SimpleDateFormat sdfYear = new SimpleDateFormat("yy");
	        	java.util.Date frmDate = sdfSource.parse(fromDate);
	        	java.util.Date toDate = sdfSource.parse(uptoDate);
	        	convertedFrmDate=sdf.format(frmDate)+"-"+sdfYear.format(toDate);
	        	dateVO.setFromDate(convertedFrmDate);
	        	Calendar calFrmDate = Calendar.getInstance();
	        	calFrmDate.setTime(frmDate);
	        	calFrmDate.add(Calendar.YEAR, -1);
	        	Calendar cal = Calendar.getInstance();
	           	cal.setTime(toDate);
	            cal.add(Calendar.YEAR, -1);
	            convertedToDate=sdf.format(calFrmDate.getTime())+"-"+sdfYear.format(cal.getTime());
	            dateVO.setUptDate(convertedToDate);
	            
	            //date previous year
	            dateVO.setRptFromDate(sdfSource.format(calFrmDate.getTime()));	
	            dateVO.setRptUptoDate(sdfSource.format(cal.getTime()));
	     
	        }
	        logger.debug("Converted From Date Format: "+dateVO.getFromDate()+" To date: "+ dateVO.getUptDate());
			
		}catch(Exception e){
			logger.error("Exception in getConvertedDate : "+e);
		}
		return dateVO;
	}		
	
	/*Add time in dates*/
	public Timestamp AddingHHMMSSToDate(int nombreHeure, int nombreMinute, int nombreSeconde) {
	    String convertedToStringDate=null;
	    java.util.Date today=new java.util.Date();
	    java.sql.Timestamp currentTimestamp = null;
	   logger.debug("Entry:public Timestamp AddingHHMMSSToDate(int nombreHeure, int nombreMinute, int nombreSeconde))");
	   try {
	    
	    logger.debug("Current Date --> "+today);
		Calendar calendar = Calendar.getInstance();
	    calendar.setTime(today);
	    calendar.add(Calendar.HOUR_OF_DAY, nombreHeure);
	    calendar.add(Calendar.MINUTE, nombreMinute);
	    calendar.add(Calendar.SECOND, nombreSeconde);
	    java.util.Date sd=calendar.getTime();
	   		
	    currentTimestamp = new java.sql.Timestamp(sd.getTime());
	    logger.debug("Updated Date Time --> "+currentTimestamp);
	 } catch (Exception e) {
		// TODO Auto-generated catch block
		logger.debug("Exception in AddingHHMMSSToDate: "+e);
	 }
	   logger.debug("Exit:public Timestamp AddingHHMMSSToDate(int nombreHeure, int nombreMinute, int nombreSeconde))");   
	    return currentTimestamp;
	}
	
	public Boolean checkTokenDateExpired(Timestamp expiredDate){
		Boolean flag = false;
		java.util.Date today = new java.util.Date();
	    try{  
			logger.debug("Entry:public int checkTokenDateExpired(String expiredDate)");
			 java.sql.Timestamp currentTimestamp= new Timestamp(today.getTime());
	
			 if(expiredDate.after(currentTimestamp)){
				 flag=true;
			 }else{
				 flag=false;
			 }
		logger.debug(" Current Date: "+currentTimestamp+" Expired Date "+expiredDate+" Status: "+flag);
		}catch(Exception ex){
			flag=false;
			logger.error("Exception in checkTokenDateExpired: "+ex);
		}
		logger.debug("Exit:public int checkTokenDateExpired(Timestamp expiredDate)");		
		return flag;
	}

	public String getFirstDateOfFY(String anyDate){
		logger.debug("Entry : public String getFirstDateOfFY(String oldDate)");
		
		String startDate="";
		try{
		java.util.Date newDate=sdf.parse(anyDate);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(newDate);
		int month = cal.get(Calendar.MONTH);
		month=month+1;
		int year  = cal.get(Calendar.YEAR);
		if(month<3){
			year=year-1;
		}
		startDate=year+"-04-01";
		
		}catch(Exception e){
			logger.error("Exception in getFirstDateOfFY : "+e);
		}
		
		logger.debug("Exit : public String getFirstDateOfFY(String oldDate)");
		return startDate;
	}
	
	public String getLastDateOfFY(String anyDate){
		logger.debug("Entry : public String getLastDateOfFY(String oldDate)");
		
		String startDate="";
		try{
		java.util.Date newDate=sdf.parse(anyDate);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(newDate);
		int month = cal.get(Calendar.MONTH);
		month=month+1;
		int year  = cal.get(Calendar.YEAR);
		if(month>3){
			year=year+1;
		}
		startDate=year+"-03-31";
		
		}catch(Exception e){
			logger.error("Exception in getLastDateOfFY : "+e);
		}
		
		logger.debug("Exit : public String getFirstDateOfFY(String oldDate)");
		return startDate;
	}
	
	public Date convertStringDateToSql(String date){
		
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		   java.util.Date uDate;
		   java.sql.Date sqlDate = null;
		try {
			uDate = sdf.parse(date);
		
		    sqlDate= new Date(uDate.getTime()); 
		} catch (Exception e) {
			logger.error("Exception in convertStringDateToSql(String date) : "+e);
		}
		
		  
		return sqlDate;
		
	}
	
	
	public int getDateDiffernceInDays(Date currentDate,Date previousDate) 
	{
		
		int daysDifference = 0;
		
		try{
		
			logger.debug("Entry:public int getDateDiffernceInDays(Date currentDate,Date previousDate)");
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy - MMM - dd h:mm:ss a");
		
							 
		 Calendar sDate = Calendar.getInstance();
		 Calendar eDate = Calendar.getInstance();
		 sDate.setTime(currentDate);
		 eDate.setTime(previousDate);
		  long diffInMillisec=sDate.getTimeInMillis()-eDate.getTimeInMillis();
		   long diffInDays = diffInMillisec / (24 * 60 * 60 * 1000);   
		   
		   daysDifference=(int) diffInDays;
		 logger.debug("Days Difference is:-"+diffInDays);
		}catch(Exception ex)
		{
			logger.error("Exception:"+ex);
		}
		logger.debug("Exit:public int getDateDiffernceInDays(Date currentDate,Date previousDate)");		
		return daysDifference;
	}	
	
	public String updatePeriodtoNextDate(String oldDate,String Period){
		logger.debug("Entry : public String updatePeriodtoNextDate(String oldDate)"+oldDate);
		String newDateString="";
		try{
		boolean isLastDay=false;
		java.util.Date newDate=sdf.parse(oldDate);
		SimpleDateFormat sDateFormat = new SimpleDateFormat("dd-MMM-yy");
		SimpleDateFormat sDF = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
		Calendar cal  = Calendar.getInstance();
		cal.setTime(sdf.parse(oldDate));
		isLastDay=checkIfDayIsLastDayOfMonth(cal);
		
		
		if(Period.equalsIgnoreCase("Monthly")){
			cal.add(Calendar.MONTH, 1);
		}else if(Period.equalsIgnoreCase("BIMONTHLY")){
			cal.add(Calendar.MONTH, 2);
		}
		else if(Period.equalsIgnoreCase("Quarterly")){
			cal.add(Calendar.MONTH, 3);
		}else if(Period.equalsIgnoreCase("triannualy")){
			cal.add(Calendar.MONTH, 4);
		}else if(Period.equalsIgnoreCase("HalfYearly")){
			cal.add(Calendar.MONTH, 6);
		}else if(Period.equalsIgnoreCase("Yearly")){
			cal.add(Calendar.YEAR, 1);
		}
		  java.util.Date lastDayOfMonth = cal.getTime();  

	        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
	     	        
		
		if(isLastDay){
			cal.add(Calendar.MONTH, 1);  
	        cal.set(Calendar.DAY_OF_MONTH, 1);  
	        cal.add(Calendar.DATE, -1);  
		}
		if (cal != null) {
			java.util.Date newSQLDate=cal.getTime();
			
			
			newDateString = sdf.format(newSQLDate);
			
			} 
		
		
		}catch(Exception e){
			logger.error("Exception in getConvertedDate : "+e);
		}
		logger.debug("Exit : public String getConvetedDate(String oldDate)");
		return newDateString;
	}
	
	
    private  boolean checkIfDayIsLastDayOfMonth(Calendar calendar) {
    	 
        boolean success=false;
   
 
       success = calendar.get(Calendar.DATE) == calendar.getActualMaximum(Calendar.DATE);
        System.out.println("The calendar date " +
                (success ? "is " : "is not ") +
                "the last day of the month.");
        
       return success;
    }
	
    public String getLastDayOfMonth(String date){
		
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		 String lastDayOfTheMonth = "";
		  
		try {
			java.util.Date dt= sdf.parse(date);
	        Calendar calendar = Calendar.getInstance();  
	        calendar.setTime(dt);  

	        calendar.add(Calendar.MONTH, 1);  
	        calendar.set(Calendar.DAY_OF_MONTH, 1);  
	        calendar.add(Calendar.DATE, -1);  

	        java.util.Date lastDay = calendar.getTime();  

	        lastDayOfTheMonth = sdf.format(lastDay);
		} catch (Exception e) {
			logger.error("Exception in convertStringDateToSql(String date) : "+e);
		}
		
		  
		return lastDayOfTheMonth;
		
	}
	
    public ReportVO getCurrentFYDates(String anyDate){
		
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		 ReportVO reportVO=new ReportVO();
		  
		try {
			String startDate="";
			String toDate="";
			
			java.util.Date newDate=sdf.parse(anyDate);
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(newDate);
			int month = cal.get(Calendar.MONTH);
			month=month+1;
			int year  = cal.get(Calendar.YEAR);
			if(month>3){
				year=year+1;
			}
			toDate=year+"-03-31";
			startDate=year-1+"-04-01";
			reportVO.setFromDate(startDate);
			reportVO.setUptDate(toDate);
		
		} catch (Exception e) {
			logger.error("Exception in convertStringDateToSql(String date) : "+e);
		}
		
		  
		return reportVO;
		
	}
}
