package com.emanager.server.commonUtils.domainObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;

import com.emanager.server.commonUtils.dataaccessObject.MessageDAO;
import com.emanager.server.commonUtils.services.EmailService;
import com.emanager.server.commonUtils.valueObject.MessageVO;
import com.emanager.server.society.valueObject.MemberVO;


public class MessageDomain {
	private static final Logger logger = Logger.getLogger(MessageDomain.class);
	public MessageDAO messageDAO;
	List listMessages=null;
	MessageVO msgVO=new MessageVO();
	
	EmailService emailServiceBean;
	static final String charset = "UTF-8";
	static final String _url = "http://xsms.webxion.com/sendsms.jsp";
	int checkFlag;
	int opnlcntr, procntr, closecntr, escntr;
	String formedDate;
	
	public List getMessages(int memberID,String userType,int societyID) {
		
		logger.debug("Entry : public List getMessages(String memberID,String userType,int societyID)");		
		try{
		
		if ((userType.equalsIgnoreCase("A"))||(userType.equalsIgnoreCase("SM")))
			listMessages = messageDAO.getMessages(societyID);
			else
				listMessages=messageDAO.getMessages(memberID,societyID);
		opnlcntr=0;
		procntr=0;
		escntr=0;
		if(listMessages.size()!=0)
		{for(int i=0;listMessages.size()>i;i++){
			MessageVO messageVO=(MessageVO) listMessages.get(i);
			java.util.Date convertedDate =new java.util.Date();
			java.util.Date insertDate =new java.util.Date();
			
			SimpleDateFormat isource=new SimpleDateFormat("yyyy-MM-dd");
			
			SimpleDateFormat source = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			 
			
				convertedDate=source.parse(messageVO.getUpdateDate());
				insertDate=source.parse(messageVO.getInsertDate());
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy h:mm a");
			String formedDate = sdf.format(convertedDate);
			String formatedInsertDate=sdf.format(insertDate);
			
			messageVO.setUpdateDate(formedDate);
			messageVO.setInsertDate(formatedInsertDate);
			String sts=messageVO.getStatus();
			if(sts.equalsIgnoreCase("OPEN")){
			opnlcntr++;
			}else if(sts.equalsIgnoreCase("PROCESSING")){
				procntr++;
			}else if(sts.equalsIgnoreCase("ESCALATED")){
				escntr++;
			}
			/*else if(sts.equalsIgnoreCase("CLOSED")){
				closecntr++;
			}*/
		}
		MessageVO msgvo=(MessageVO) listMessages.get(0);
		msgvo.setOpenCounter(opnlcntr);
		msgvo.setProcessingCounter(procntr);
		//msgvo.setClosedCounter(closecntr);
		msgvo.setEscCounter(escntr);
		
		}
		
		}catch (ParseException e) {
			logger.error("Exception in getMessages : "+e);
		}
		catch(IndexOutOfBoundsException exp){
			logger.info("No messages present to display");
		}
		catch(Exception ex){
			logger.error("Exception in getMessages : "+ex);
		}
		
		
		logger.debug("Exit : public List getMessages(String memberID,String userType,int societyID)");
		return listMessages;
	}
	
public List getMessagesForClosedStatus(int memberID,String userType,int societyID) {
		
		logger.debug("Entry : public List getMessagesForClosedStatus(String userType,int societyID)");		
		try{
		
		
			listMessages = messageDAO.getMessagesForClosedStatus(memberID, societyID);
			
		
		closecntr=0;
		
		if(listMessages.size()!=0)
		{for(int i=0;listMessages.size()>i;i++){
			MessageVO messageVO=(MessageVO) listMessages.get(i);
			java.util.Date convertedDate =new java.util.Date();
			java.util.Date insertDate =new java.util.Date();
			
			SimpleDateFormat isource=new SimpleDateFormat("yyyy-MM-dd");
			
			SimpleDateFormat source = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			 
			
				convertedDate=source.parse(messageVO.getUpdateDate());
				insertDate=source.parse(messageVO.getInsertDate());
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy h:mm a");
			String formedDate = sdf.format(convertedDate);
			String formatedInsertDate=sdf.format(insertDate);
			
			messageVO.setUpdateDate(formedDate);
			messageVO.setInsertDate(formatedInsertDate);
			
			
			
		}
		
		
		}
		
		}catch (ParseException e) {
			logger.error("Exception in getMessagesForClosedStatus : "+e);
		}
		catch(IndexOutOfBoundsException exp){
			logger.info("No messages present to display");
		}
		catch(Exception ex){
			logger.error("Exception in getMessagesForClosedStatus : "+ex);
		}
		
		
		logger.debug("Exit : public List getMessagesForClosedStatus(String userType,int societyID)");
		return listMessages;
	}
	
/*	public int insertNewMessage(MessageVO messageObj){
		int insertFlag = 0;
		try {
			
			int status=0;
			logger.debug("Entry : public int insertNewMessage(MemberVO memberObj)");
			
			insertFlag=messageDAO.insertNewMessages(messageObj);			
	
			if(insertFlag!=0){
				checkFlag=0;
				eventVO=fillEventvo(messageObj);
				
				 status = emailServiceBean.sendEmail(eventVO);
			}
			
			logger.debug("Exit : public int insertNewMessage(MemberVO memberObj)");
		} catch (Exception e) {
			logger.error("Exception in insertNewMessage : "+e);
		}
		return insertFlag;	
		
	}

	public int updateMessage(MessageVO messageObj,String memberID, String messsageId){
		int insertFlag=0;
		int status=0;
		try {
			logger.debug("Entry : public int updateMessage(MessageVO messageObj,String memberID,String messsageId)");
			
			insertFlag=messageDAO.updateMessage(messageObj, memberID,messsageId);
			
			if(insertFlag!=0){
				checkFlag=1;
				eventVO=fillEventvo(messageObj);
				
				 status = emailServiceBean.sendEmail(eventVO);
			}
			logger.debug("Exit : public int updateMessage(MessageVO messageObj,String memberID,String messsageId)"+insertFlag);
		} catch (Exception e) {			
			logger.error("Exception in UpdateMessage : "+e);
		}
		return insertFlag;
	}*/
	public List getCategorylist(){
		List categoryList=null;
		
			try {
				logger.debug("Entry : public List getCategorylist() ");
						
				categoryList=messageDAO.getCategorylist();
				
				logger.debug("Exit : public List getCategorylist() ");
			} catch (DataAccessException e) {
				logger.error("Exception in getCategorylist : "+e);
			}
		return categoryList;
		}
	
     public List getCategorylistUpdate(){
		
    	 List categoryList=null;
 		
			try {
				logger.debug("Entry : public List getCategorylistUpdate() ");
						
				categoryList=messageDAO.getCategorylist();
				
				logger.debug("Exit : public List getCategorylistUpdate() ");
			} catch (DataAccessException e) {
				logger.error("Exception in getCategorylistUpdate : "+e);
			}
		return categoryList;
	  }
	
	
/*public EventVO fillEventvo(MessageVO messageObj){
		
		logger.debug("Entry : public EventVO emailSend(MessageVO messageObj)");
		Date currentDatetime = new Date(System.currentTimeMillis());   
		java.sql.Date sqlDate = new java.sql.Date(currentDatetime.getTime());   
		java.sql.Timestamp timestamp = new java.sql.Timestamp(currentDatetime.getTime());
		SimpleDateFormat sdf = new SimpleDateFormat("dd - MMM - yyyy h:mm:ss a");
		String formattedDate = sdf.format(currentDatetime);
		
		EventVO event = new EventVO();
		
		try{
			
			ConfigManager c=new ConfigManager();
		event.setEventID(0);
		event.setEventType("user");
		event.setMemberID(0);
		event.setSocietyId((messageObj.getSocietyID()));
		event.setSocietyName(messageObj.getSocietyName());
		event.setSubject(messageObj.getSubject());
		logger.debug("Subject is"+event.getSocietyName());
		event.setHtmlString(messageObj.getStatus());
		if(checkFlag==0){
			event.setMessage(messageObj.getMessage());
		}else if(checkFlag==1){
		event.setMessage(formattedDate+"- Updated By:-"+messageObj.getMemberName()+"<br>"+messageObj.getComments()+"<br>---------------------<br>"+messageObj.getMessage());
		}
		event.setTempleteName(c.getPropertiesValue("template.message"));
		event.setTransaction_type(messageObj.getMsgType());
		event.setReminderType("Daily");
		
		
		List memberList =messageDAO.getMemberList( messageObj.getMemberID(),"A",messageObj.getSocietyID());
				
		event.setMemberlist(memberList);
		
		logger.debug("Exit : public EventVO emailSend(MessageVO messageObj)");	
		
	    }
		catch(NullPointerException Ex){
			Ex.printStackTrace();
			logger.error("NullPointerException in fillEventvo : "+Ex);
		}
	    logger.info("return eventVO ");
		return event;
	
	
	
	}*/

public Boolean sendMessage(MemberVO memberVO) {
	Boolean success=false;
	try{
	logger.debug("Entry : public int sendMessage(MemberVO memberVO) ");
	
	  success=sendMessageUrl(memberVO);
	
	
	logger.debug("Exit : public int sendMessage(MemberVO memberVO) ");
	} catch (Exception e) {
		
		logger.error("Exception in sendMessage(MemberVO memberVO) : "+e);
	}
	return success;
}


private static String buildRequestString( MemberVO memberVO) throws IOException
{	
 
 	
 	
 	
 	java.util.Date today = new java.util.Date();
 	DateFormat dateFormat=new SimpleDateFormat("MMM-yy");
 	String encodedUrl = URLEncoder.encode(memberVO.getMessage(), "UTF-8");
 
 	
  logger.debug("Entry :  private static String buildRequestString(EventVO eventVO, MemberVO memberVO)");
  ConfigManager c=new ConfigManager();
    String [] params = new String [6];
    params[0] =c.getPropertiesValue("sms.username");
    params[1] =c.getPropertiesValue("sms.password");
    params[2] =memberVO.getMobile();
    params[4] =c.getPropertiesValue("sms.sendername");
  
    params[3] =encodedUrl;
    params[5]="19";
    logger.info("The sms sent to "+memberVO.getMobile()+" is -->"+encodedUrl);
    String query = String.format("user=%s&password=%s$&mobiles=%s&sms=%s&senderid=%s&version=3",
    URLEncoder.encode(params[0],charset),
    URLEncoder.encode(params[1],charset),
    URLEncoder.encode(params[2],charset),
    URLEncoder.encode(params[3],charset),
    URLEncoder.encode(params[4],charset),
    URLEncoder.encode(params[5],charset)
    );
    logger.debug("the query is : "+query);
    logger.debug("Exit :  private static String buildRequestString(EventVO eventVO, MemberVO memberVO)");
return query;
}

public static Boolean sendMessageUrl(MemberVO memberVO) 
{
	Boolean success=false;
	String sendSmsStatus=null;
    logger.debug("Entry :  public static void sendMessage(EventVO eventVO, MemberVO memberVO)");
    BufferedReader br = null;
	try {
		//To establish the connection and perform the post request
		URLConnection connection = new URL(_url + "?" + buildRequestString(memberVO)).openConnection();
		connection.setRequestProperty("Accept-Charset", charset);

		//This automatically fires the request and we can use it to determine the response status
		InputStream response = connection.getInputStream();
		StringBuffer string=new StringBuffer();
		br = new BufferedReader(new InputStreamReader(response));
		String inputLine;
    	while ((inputLine = br.readLine()) != null) {
    		  	string.append(inputLine);
    		  	sendSmsStatus=string.toString();
    		  	
    	}br.close();
    	
    	if(sendSmsStatus.matches(".*\\berror\\b.*")){
    		logger.info("Caught the error");
    		success=false;
    		
    	}else
    		success=true;
	} catch (MalformedURLException e) {
	logger.error("MalformedURLException :" +e);
	} catch (UnsupportedEncodingException e) {
		logger.error("UnsupportedEncodingException :"+e);
	} catch (IOException e) {
		logger.error("IOException :"+e);
	} catch (Exception e) {
		logger.error("Exception in sending sms :" +e);
	}
	    
    logger.debug("Exit :  public static void sendMessage(EventVO eventVO, MemberVO memberVO)");
    return success;
}


public List countCategories(String societyId){
	List categoryList=null;
	
		try {
			logger.debug("Entry : public List countCategories() ");
		
			
			    categoryList=messageDAO.countCategories(societyId);
			

			logger.debug("Exit : public List countCategories(String societyId) ");
		} catch (DataAccessException e) {
			logger.error("Exception in countCategories(String societyId) : "+e);
		}
	return categoryList;
	}

	public void setMessageDAO(MessageDAO messageDAO) {
		this.messageDAO = messageDAO;
	}

	public void setEmailServiceBean(EmailService emailServiceBean) {
		this.emailServiceBean = emailServiceBean;
	}

}
