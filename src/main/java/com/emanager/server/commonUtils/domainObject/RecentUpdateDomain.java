package com.emanager.server.commonUtils.domainObject;



import java.sql.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.commonUtils.dataaccessObject.RecentUpdateDAO;
import com.emanager.server.commonUtils.valueObject.RecentUpdateVO;

public class RecentUpdateDomain {
	private static final Logger logger = Logger.getLogger(RecentUpdateDomain.class);
	public RecentUpdateDAO recentUpdateDAO;
	public EmailDomain emailDomain;
	List listRecentUpdate = null;
 
	
	public List getRecentUpdateListForAdmin(String strSocietyID,String recentType){
		Date currentDatetime = new Date(System.currentTimeMillis());
		RecentUpdateVO recentUpdateVO = new RecentUpdateVO();	
		try{
			
			logger.debug("Entry : public List getRecentUpdateListForAdmin(String strSocietyID,String recentType)");
			 
			listRecentUpdate = recentUpdateDAO.getRecentUpdateListForAdmin(strSocietyID,recentType);
			
			for(int i=0;listRecentUpdate.size()>i;i++){
				RecentUpdateVO rUpdateVO=(RecentUpdateVO) listRecentUpdate.get(i);
				if(rUpdateVO.getRecentType().equalsIgnoreCase("REM")){
				if(rUpdateVO.getLastDate()!=null){
				int result=rUpdateVO.getLastDate().compareTo(currentDatetime);
				if(result>0)
				{	rUpdateVO.setEmailSendFlag(0);
					
					
				}else{
					rUpdateVO.setEmailSendFlag(1);
				}
			}
				
			}
			}
			logger.debug("Exit : public List getRecentUpdateListForAdmin(String strSocietyID,String recentType)");
		 }catch(Exception Ex){			 
			 logger.debug("Exception in getRecentUpdateListForAdmin : "+Ex);			 
		 }
		
		 return listRecentUpdate;
		 
			
		}
	
	public List getRecentUpdateListForMembers(String strSocietyID,String recentType){
		 
		RecentUpdateVO recentUpdateVO = new RecentUpdateVO();	
		try{
			
			logger.debug("Entry : public List getRecentUpdateListForMembers(String strSocietyID,String recentType)");
			 
			listRecentUpdate = recentUpdateDAO.getRecentUpdateListForMembers(strSocietyID,recentType);
			
			logger.debug("Exit : public List getRecentUpdateListForMembers(String strSocietyID,String recentType)");
		 }	catch(Exception Ex){
			 
			 logger.debug("Exception in getRecentUpdateListForMembers : "+Ex);
			 
		 }
		
		 return listRecentUpdate;		 
			
		}
	
	
	public int insertRecentUpdate(RecentUpdateVO recentUpdateVO){
		 
		int insertFlag = 0;
		try{
			
			logger.debug("Entry : public int insertRecentUpdate(RecentUpdateVO recentUpdateVO)");
			 
			insertFlag = recentUpdateDAO.insertRecentUpdate(recentUpdateVO);
			
			logger.debug("Exit : public int insertRecentUpdate(RecentUpdateVO recentUpdateVO)");
		 
		 }catch(Exception Ex){			 
			 logger.error("Exception in insertRecentUpdate : "+Ex);		 
		 }
		
		 return insertFlag;
		 
			
		}
	
	public int updateRecentUpdate(RecentUpdateVO recentUpdateVO,int noticeID){
		 
		int updateFlag = 0;
		try{
			
			logger.debug("Entry : public int updateRecentUpdate(RecentUpdateVO recentUpdateVO,int noticeID)");
			 
			updateFlag = recentUpdateDAO.updateRecentUpdate(recentUpdateVO, noticeID);
			
			logger.debug("Exit : public int updateRecentUpdate(RecentUpdateVO recentUpdateVO,int noticeID)");
		 
		 }catch(Exception Ex){			 
			 logger.error("Exception in updateRecentUpdate : "+Ex);			 
		 }
		
		 return updateFlag;
		 
			
		}
	public int deleteDrafts(int noticeID){
		int success=0;
		try{
		logger.debug("Entry : public int deleteDrafts(int noticeID)");
		
		success=recentUpdateDAO.deleteDrafts(noticeID);
	
		logger.debug("Exit : public int deleteDrafts(int noticeID)");
		 }catch(Exception Ex){			 
			 logger.error("Exception in deleteDrafts : "+Ex);			 
		 }
		return success;
		
	}
	

	/**
	 * @return the recentUpdateDAO
	 */
	public RecentUpdateDAO getRecentUpdateDAO() {
		return recentUpdateDAO;
	}

	/**
	 * @param recentUpdateDAO the recentUpdateDAO to set
	 */
	public void setRecentUpdateDAO(RecentUpdateDAO recentUpdateDAO) {
		this.recentUpdateDAO = recentUpdateDAO;
	}


	/**
	 * @return the emailDomain
	 */
	public EmailDomain getEmailDomain() {
		return emailDomain;
	}


	/**
	 * @param emailDomain the emailDomain to set
	 */
	public void setEmailDomain(EmailDomain emailDomain) {
		this.emailDomain = emailDomain;
	}
	
	
	
}
