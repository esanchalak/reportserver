package com.emanager.server.commonUtils.domainObject;

import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;


public class VMTemplatePreparator {
	static Logger logger = Logger.getLogger(VMTemplatePreparator.class.getName());
	static VelocityEngine velocityEngine;

	
public String createMessage(VelocityContext context, String templateURL)
	{

String emailText = "";
try {
	//velocityEngine.init();
	 Template t = velocityEngine.getTemplate(templateURL);
        /*  now render the template into a Writer  */
        StringWriter writer = new StringWriter();
        t.merge( context, writer );
        emailText=writer.toString();
} catch (VelocityException e) {
	e.printStackTrace();
}
logger.debug("creatingMessage");
return emailText;
}

/**
 * @param velocityEngine the velocityEngine to set
 */
public static void setVelocityEngine(VelocityEngine velocityEngine) {
	VMTemplatePreparator.velocityEngine = velocityEngine;
}

	
}
