package com.emanager.server.commonUtils.domainObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.ScheduledTransactionResponseVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.taskAndEventManagement.valueObject.EventVO;
import com.emanager.server.taskAndEventManagement.valueObject.SMSGatewayVO;
import com.emanager.server.taskAndEventManagement.valueObject.TicketVO;


public class HtmlTableGenerator {
	static Logger log = Logger.getLogger(HtmlTableGenerator.class.getName());
		
	public String getHTMLTableForBills(List billList) throws Exception{
		log.debug("Entry : 	public String getHTMLTableForBills(List billList)");
		String htmlTable = "";
		try{
		 
		
   	 Iterator iterator=billList.iterator();	  
   	
		int serialNumber  = 1;
		while(iterator.hasNext()){
			
			TransactionVO txVO=  (TransactionVO) iterator.next();
			
			
					
			
		
			
			htmlTable  = htmlTable + "<tr> " +
			        "<td  style='text-align: center; width: 15px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
			        serialNumber++ + 
				    "</td>" +
				    "<td style='text-align: left; width: 100px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
			        txVO.getLedgerName() +			       
					"</td>" +
					"<td style='text-align: right;width: 100px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
					txVO.getAmount() +			       
					"</td>" +
			     
					"</tr>";
			
				
					
			
		}}catch (Exception e) {
			log.error("Exception occured in public String getHTMLTableForBills(List billList) "+e);
		}
		
		log.debug("Exit :public String getHTMLTableForBills(List billList)"+htmlTable);
		return htmlTable;
	}
	
	
	public String getHTMLTableForManagementPendingTickets(List openStatusMessages){
		log.debug("Entry : 	public String getHTMLTableForManagementPendingTickets(List openStatusMessages)");
		String htmlTable = "";
		Iterator iterator=openStatusMessages.iterator();	  
		
		int serialNumber  = 1;
		while(iterator.hasNext()){
			
			TicketVO ticketVO=(TicketVO) iterator.next();
			java.util.Date convertedDate =new java.util.Date();
	    	 SimpleDateFormat source = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");			 
			
				try {
					
			 	convertedDate=source.parse(ticketVO.getCreated_date());
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
					String formedDate = sdf.format(convertedDate);
				
					ticketVO.setCreated_date(formedDate);
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					
					log.info("Exce--"+e);
				}				
			
			
			
		
			
			htmlTable  = htmlTable + "<tr> " +
			        "<td  style='text-align: center; width: 50px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
			        serialNumber++ + 
				    "</td>" +
				   
					"<td  style='text-align: left; width: 400px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
			        ticketVO.getSubject() +			       
					"</td>" +
				
			        "<td  style='text-align: center; width: 150px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
			        ticketVO.getCreated_date() +			       
					"</td>" +
					
					
				
					"</tr>";
			
		}
		log.debug("Exit : public String getHTMLTableForManagementPendingTickets(List openStatusMessages)"+htmlTable);
		return htmlTable;
	}
	
	public String getHTMLTableForInnvoiceGenSummary(List respsonseList) throws Exception{
		log.debug("Entry : 	public String getHTMLTableForInnvoiceGenSummary(List respsonseList)");
		String htmlTable = "";
		try{
		 
		
   	 Iterator iterator=respsonseList.iterator();	  
   	
		int serialNumber  = 1;
		while(iterator.hasNext()){
			
			ScheduledTransactionResponseVO responsVO= (ScheduledTransactionResponseVO) iterator.next();
			
			
					
			
		
			
			htmlTable  = htmlTable + "<tr> " +
			        "<td  style='text-align: center; width: 15px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
			        serialNumber++ + 
				    "</td>" +
				    "<td style='text-align: left; width: 100px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
			        responsVO.getSocietyName() +			       
					"</td>" +
					"<td style='text-align: right;width: 100px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
			        responsVO.getSuccessCount() +			       
					"</td>" +
			       "<td style='text-align: right; width: 100px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
			        responsVO.getLateFeeSucccessCount()+			       
					"</td>" +
					/*"<td style='text-align: center;width: 100px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
					memberVO.getRmPaidUptoDate()+
					"</td>"+*/
					"</tr>";
			
				
					
			
		}}catch (Exception e) {
			log.error("Exception occured in getHTMLTableInvoiceGenaration "+e);
		}
		
		log.debug("Exit :public String getHTMLTableForInnvoiceGenSummary(List respsonseList)"+htmlTable);
		return htmlTable;
	}
	
	public String getHTMLTableForLowSMSCredit(List orgList) throws Exception{
		log.debug("Entry : 	public String getHTMLTableForLowSMSCredit(List respsonseList)");
		String htmlTable = "";
		DateUtility dateUtil=new DateUtility();
		try{
		 
		
   	 Iterator iterator=orgList.iterator();	  
   	
		int serialNumber  = 1;
		while(iterator.hasNext()){
			
			SMSGatewayVO smsVO=  (SMSGatewayVO) iterator.next();
			
			
					
			
		
			
			htmlTable  = htmlTable + "<tr> " +
			        "<td  style='text-align: center; width: 15px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
			        serialNumber++ + 
				    "</td>" +
				    "<td style='text-align: left; width: 100px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
			        smsVO.getOrgName() +			       
					"</td>" +
					"<td style='text-align: right;width: 100px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
			       smsVO.getAvailableSMSCredits() +			       
					"</td>" +
			     
					"<td style='text-align: center;width: 100px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
					smsVO.getSmsCredits()+
					"</td>"+
					"</tr>";
			
				
					
			
		}}catch (Exception e) {
			log.error("Exception occured in getHTMLTableForLowSMSCredit "+e);
		}
		
		log.debug("Exit :public String getHTMLTableForLowSMSCredit(List respsonseList)"+htmlTable);
		return htmlTable;
	}
	
	public String getHTMLTableForNotificationOfPaymentReminder(List eventList) throws Exception{
		log.debug("Entry : 	public String getHTMLTableForNotificationOfPaymentReminder(List respsonseList)");
		String htmlTable = "";
		DateUtility dateUtil=new DateUtility();
		try{
		 
		
   	 Iterator iterator=eventList.iterator();	  
   	
		int serialNumber  = 1;
		while(iterator.hasNext()){
			
			EventVO eventVO=  (EventVO) iterator.next();
			
			
					
			
		
			
			htmlTable  = htmlTable + "<tr> " +
			        "<td  style='text-align: center; width: 15px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
			        serialNumber++ + 
				    "</td>" +
				    "<td style='text-align: left; width: 100px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
			        eventVO.getSocietyName() +			       
					"</td>" +
					"<td style='text-align: right;width: 100px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
			       dateUtil.getConvetedDate(eventVO.getNextDate()) +			       
					"</td>" +
			     
					/*"<td style='text-align: center;width: 100px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: rgb(238, 238, 238);padding: 5px 5px;font-size: 11px;font-family: Verdana;color: rgb(0, 0, 0);border-left: 1px solid rgb(255, 255, 255);border-top: 1px solid rgb(255, 255, 255);border-bottom: 1px solid rgb(255, 255, 255);border-right: 1px solid rgb(255, 255, 255);border-collapse: collapse;'>" +
					memberVO.getRmPaidUptoDate()+
					"</td>"+*/
					"</tr>";
			
				
					
			
		}}catch (Exception e) {
			log.error("Exception occured in getHTMLTableForNotificationOfPaymentReminder "+e);
		}
		
		log.debug("Exit :public String getHTMLTableForNotificationOfPaymentReminder(List respsonseList)"+htmlTable);
		return htmlTable;
	}
	
	public String getHTMLTableForCashBalance(List cashAccountList) throws Exception{
		log.debug("Entry : 	public String getHTMLTableForCashBalance(List openStatusMessages)");
		String htmlTable = "";
		String cashOver="Cash balance should not be more than Rs.5000 as per co-operative guidelines.";
		String cashBelow="Cash balance is negative please look if you have missed reporting any expense transaction at the earliest.";
	
   	 Iterator iterator=cashAccountList.iterator();	  
		
		int serialNumber  = 1;
		while( iterator.hasNext() ){
			
			AccountHeadVO accHVO=(AccountHeadVO) iterator.next();
			
			
			String comment="";
			int res;
			int result;
			res=accHVO.getClosingBalance().compareTo(new BigDecimal("5000"));
			result=accHVO.getClosingBalance().compareTo(BigDecimal.ZERO);		
			if(res==1){
				comment=cashOver;
			}
			if(result==-1){
				comment=cashBelow;
			}
			
			
		
			
			htmlTable  = htmlTable +"<b> A/C - "+accHVO.getStrAccountHeadName()+"  = Rs. "+accHVO.getClosingBalance()+" <br> Comments </b>: Last transaction for above account was recorded on "+accHVO.getEffectiveDate()+". "+comment+" <br> <br> ";
			        
					
			
					
			
		}
		log.debug("Exit : public String getHTMLTableForCashBalance(List openStatusMessages)"+htmlTable);
		return htmlTable;
	}
	
}
