package com.emanager.server.commonUtils.domainObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ConfigManager
{
static Properties pro=new Properties();
String propertiesFilePath;
private static Logger logger=Logger.getLogger(ConfigManager.class);
public void loadRestartParam()
{
	
	
	try
	{
	logger.debug("Entry: inside loadProperties and FinancialYear class");
	
	File f=new File(propertiesFilePath);
	FileInputStream in = new FileInputStream(f);
	Properties reloadProperties = new Properties(); 
	reloadProperties.load(in);
	pro=reloadProperties;
	in.close();
	}
	
	catch(FileNotFoundException ex)
	{
	logger.error("could not find Configuration.properties file.check file path");
		
	}
	catch(IOException e)
	{
		e.printStackTrace();
	
	}
}

public static String getPropertiesValue(String key)
{
	
 	return (pro.getProperty(key));
	
}

public void setPropertiesFilePath(String propertiesFilePath) {
	this.propertiesFilePath = propertiesFilePath;
}


}
