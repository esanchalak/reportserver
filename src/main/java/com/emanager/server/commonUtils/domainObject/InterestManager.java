package com.emanager.server.commonUtils.domainObject;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.LedgerEntries;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.LedgerService;

/*
 * Create one column InterestBalance in ledger Details. 
 * Create two columns in ledger entries. InterestBalance and InterestAmount post transaction.
 * Both the ledgers will get affected as the interest is applied or deducted. 
 * 
 */

public class InterestManager {
	
	/*
	 * Logic For every Income Transactions for 
	 * All recievables ledger and whose interest Amount > 0.
	 *  Add interestBalance in transactions and Ledger Table with default 0.
	 * */
	
	private static final Logger logger = Logger.getLogger(InterestManager.class);
	LedgerService ledgerService;
	
	public TransactionVO checkForInterest(TransactionVO txVO)
	{
		logger.debug("Entry : public TransactionVO checkForInterest(TransactionVO txVO)");
		
		List ledgerEntries=txVO.getLedgerEntries();
		
		Map<Integer,AccountHeadVO> hashMap=new HashMap<Integer, AccountHeadVO>();
		
		txVO=checkIfInterestApplied(txVO);
		
		List ledgerList=ledgerService.getLedgerListWithClosingBalance(txVO.getSocietyID(), 0, "A", txVO.getTransactionDate(), txVO.getTransactionDate());
		
		for(int i=0;ledgerList.size()>i;i++){
			AccountHeadVO ledgerVO=(AccountHeadVO) ledgerList.get(i);
			
			hashMap.put(ledgerVO.getLedgerID(), ledgerVO);
			
			
		}
		
		
		for(int i=0;ledgerEntries.size()>i;i++){
			LedgerEntries ledgerEntry=(LedgerEntries) ledgerEntries.get(i);
			
			AccountHeadVO accVO=hashMap.get(ledgerEntry.getLedgerID());
			BigDecimal intBalance=BigDecimal.ZERO;
			
				
			if(accVO.getAccountGroupID()==1){
			
			if((txVO.getType()!=null)&&(txVO.getType().equalsIgnoreCase("Interest"))){
				if(txVO.getTransactionType().equalsIgnoreCase("Credit Note")){
					intBalance=ledgerEntry.getAmount().negate();
				}else
				intBalance=ledgerEntry.getAmount();
			}else{
				if((accVO.getIntClsBalance()!=null)&&(ledgerEntry.getCreditDebitFlag().equalsIgnoreCase("C"))){
				if(ledgerEntry.getAmount().compareTo(accVO.getIntClsBalance())<0){
			
				intBalance=ledgerEntry.getAmount().negate();
			}else{
				intBalance=accVO.getIntClsBalance().negate();
			}
			}
				}
			}
			ledgerEntry.setIntAmount(intBalance);
			
			
			
		}
		
		
		
		logger.debug("Exit : public TransactionVO checkForInterest(TransactionVO txVO)");
		
		return txVO;
	}
	
	public TransactionVO checkIfInterestApplied(TransactionVO txVO)
	{
		logger.debug("Entry : public TransactionVO checkForInterest(TransactionVO txVO)");
		
		List ledgerEntries=txVO.getLedgerEntries();
		
		Map<Integer,AccountHeadVO> hashMap=new HashMap<Integer, AccountHeadVO>();
		
		AccountHeadVO interstLedgerVO=ledgerService.getLedgerByType(txVO.getSocietyID(), "Interest");
		
		
		
		
		for(int i=0;ledgerEntries.size()>i;i++){
			LedgerEntries ledgerEntry=(LedgerEntries) ledgerEntries.get(i);
			
			AccountHeadVO accVO=hashMap.get(ledgerEntry.getLedgerID());
			BigDecimal intBalance=BigDecimal.ZERO;
			
			if(interstLedgerVO.getLedgerID()==ledgerEntry.getLedgerID()){
				txVO.setType("Interest");
			}else{
				txVO.setType("");
			}
		
			
			
			
		}
		
		
		
		logger.debug("Exit : public TransactionVO checkForInterest(TransactionVO txVO)");
		
		return txVO;
	}

	/**
	 * @param ledgerService the ledgerService to set
	 */
	public void setLedgerService(LedgerService ledgerService) {
		this.ledgerService = ledgerService;
	}

	

}
