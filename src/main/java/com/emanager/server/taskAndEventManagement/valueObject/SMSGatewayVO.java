package com.emanager.server.taskAndEventManagement.valueObject;

public class SMSGatewayVO {
	
	int smsGatewayID;
	int orgID;
	String smsUserName;
	String smsPassword;
	String smsGatewayName;
	String smsSenderName;
	int smsCredits;// No of SMSes alloted per month
	int availableSMSCredits; //Available no of SMS for current month
	String orgName;
	
	
	/**
	 * @return the smsGatewayID
	 */
	public int getSmsGatewayID() {
		return smsGatewayID;
	}
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @return the smsUserName
	 */
	public String getSmsUserName() {
		return smsUserName;
	}
	/**
	 * @return the smsPassword
	 */
	public String getSmsPassword() {
		return smsPassword;
	}
	/**
	 * @return the smsGatewayName
	 */
	public String getSmsGatewayName() {
		return smsGatewayName;
	}
	/**
	 * @param smsGatewayID the smsGatewayID to set
	 */
	public void setSmsGatewayID(int smsGatewayID) {
		this.smsGatewayID = smsGatewayID;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	/**
	 * @param smsUserName the smsUserName to set
	 */
	public void setSmsUserName(String smsUserName) {
		this.smsUserName = smsUserName;
	}
	/**
	 * @param smsPassword the smsPassword to set
	 */
	public void setSmsPassword(String smsPassword) {
		this.smsPassword = smsPassword;
	}
	/**
	 * @param smsGatewayName the smsGatewayName to set
	 */
	public void setSmsGatewayName(String smsGatewayName) {
		this.smsGatewayName = smsGatewayName;
	}
	/**
	 * @return the smsSenderName
	 */
	public String getSmsSenderName() {
		return smsSenderName;
	}
	/**
	 * @param smsSenderName the smsSenderName to set
	 */
	public void setSmsSenderName(String smsSenderName) {
		this.smsSenderName = smsSenderName;
	}
	/**
	 * @return the smsCredits
	 */
	public int getSmsCredits() {
		return smsCredits;
	}
	/**
	 * @return the availableSMSCredits
	 */
	public int getAvailableSMSCredits() {
		return availableSMSCredits;
	}
	/**
	 * @param smsCredits the smsCredits to set
	 */
	public void setSmsCredits(int smsCredits) {
		this.smsCredits = smsCredits;
	}
	/**
	 * @param availableSMSCredits the availableSMSCredits to set
	 */
	public void setAvailableSMSCredits(int availableSMSCredits) {
		this.availableSMSCredits = availableSMSCredits;
	}
	/**
	 * @return the orgName
	 */
	public String getOrgName() {
		return orgName;
	}
	/**
	 * @param orgName the orgName to set
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	

}
