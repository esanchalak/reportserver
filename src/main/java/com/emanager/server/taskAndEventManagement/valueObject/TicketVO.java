package com.emanager.server.taskAndEventManagement.valueObject;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class TicketVO implements Serializable{

	
	
	    private int count_open;
	    private int count_management;
	    private int count_society;
	    private int count_closed;
	    private int count_service;
	    private List<TicketVO> ticketInfo;
	    private int ticket_no;
	    private int user_id;
	    private String name;
	    private String subject;
	    private String dept_name;
	    private String status;
	    private String status_name;
	    private String created_date;
	    private String email;
	    private String topic;
	    private int topic_id;
	    private String type;
	    private String message;
	    private String poster;
	    private String thread_date;
	   	private int org_id;
	    private String content;
	    private String mobile;
	    private String orgName;
	    private String unitName;
	    private List<TicketVO> thread;
	    private String staff_id;
	    private String staff_name;
	    private String updated_date;
	    private String close_date;
	    private String city;
	    private String noOfUnits;
	    
	    public String getStaff_id() {
			return staff_id;
		}
		public void setStaff_id(String staff_id) {
			this.staff_id = staff_id;
		}
		public String getStaff_name() {
			return staff_name;
		}
		public void setStaff_name(String staff_name) {
			this.staff_name = staff_name;
		}
		public String getUpdated_date() {
			return updated_date;
		}
		public void setUpdated_date(String updated_date) {
			this.updated_date = updated_date;
		}
		public String getClose_date() {
			return close_date;
		}
		public void setClose_date(String close_date) {
			this.close_date = close_date;
		}
		public String getMobile() {
			return mobile;
		}
		public void setMobile(String mobile) {
			this.mobile = mobile;
		}
		public String getOrgName() {
			return orgName;
		}
		public void setOrgName(String orgName) {
			this.orgName = orgName;
		}
		public String getUnitName() {
			return unitName;
		}
		public void setUnitName(String unitName) {
			this.unitName = unitName;
		}
		public String getContent() {
			return content;
		}
		public void setContent(String content) {
			this.content = content;
		}
		public int getOrg_id() {
			return org_id;
		}
		public void setOrg_id(int org_id) {
			this.org_id = org_id;
		}
	    public int getTopic_id() {
			return topic_id;
		}
		public void setTopic_id(int topic_id) {
			this.topic_id = topic_id;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getTopic() {
			return topic;
		}
		public void setTopic(String topic) {
			this.topic = topic;
		}
		
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public String getPoster() {
			return poster;
		}
		public void setPoster(String poster) {
			this.poster = poster;
		}
		public String getThread_date() {
			return thread_date;
		}
		public void setThread_date(String thread_date) {
			this.thread_date = thread_date;
		}
		public List<TicketVO> getThread() {
			return thread;
		}
		public void setThread(List<TicketVO> thread) {
			this.thread = thread;
		}
		public int getCount_closed() {
			return count_closed;
		}
		public void setCount_closed(int count_closed) {
			this.count_closed = count_closed;
		}
		public int getCount_service() {
			return count_service;
		}
		public void setCount_service(int count_service) {
			this.count_service = count_service;
		}
		public int getCount_management() {
			return count_management;
		}
		public void setCount_management(int count_management) {
			this.count_management = count_management;
		}
		public int getCount_open() {
			return count_open;
		}
		public void setCount_open(int count_open) {
			this.count_open = count_open;
		}
		public int getCount_society() {
			return count_society;
		}
		public void setCount_society(int count_society) {
			this.count_society = count_society;
		}
		public String getCreated_date() {
			return created_date;
		}
		public void setCreated_date(String created_date) {
			this.created_date = created_date;
		}
		public String getDept_name() {
			return dept_name;
		}
		public void setDept_name(String dept_name) {
			this.dept_name = dept_name;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getStatus_name() {
			return status_name;
		}
		public void setStatus_name(String status_name) {
			this.status_name = status_name;
		}
		public String getSubject() {
			return subject;
		}
		public void setSubject(String subject) {
			this.subject = subject;
		}
		public int getTicket_no() {
			return ticket_no;
		}
		public void setTicket_no(int ticket_no) {
			this.ticket_no = ticket_no;
		}
		
		public int getUser_id() {
			return user_id;
		}
		public void setUser_id(int user_id) {
			this.user_id = user_id;
		}
		public List<TicketVO> getTicketInfo() {
			return ticketInfo;
		}
		public void setTicketInfo(List<TicketVO> ticketInfo) {
			this.ticketInfo = ticketInfo;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getNoOfUnits() {
			return noOfUnits;
		}
		public void setNoOfUnits(String noOfUnits) {
			this.noOfUnits = noOfUnits;
		}
	    
	
	
	
}
