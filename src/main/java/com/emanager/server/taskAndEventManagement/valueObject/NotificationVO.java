package com.emanager.server.taskAndEventManagement.valueObject;

import java.util.List;

public class NotificationVO {
	
	private int orgID;
    private String templateID;
    private String transactionType;
    private String subject;
    private List<EmailMessage> emailList;
    private List<SMSMessage> smsList;
    private List<PushNotificationVO> pushNotificationList;
    private Boolean sendEmail=false;
    private Boolean sendSMS=false;
    private Boolean sendPushNotifications=false;
    private int sentEmailsCount;
    private int failedEmailsCount;
    private int sentSMSCount;
    private int failedSMSCount;
    private int sentPushNotificationsCount;
    private int failedPushNotificationsCount;
    private int statusCode;
    private String appID;
    private String groupName;
    private String profileType;
    private String description;
    
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @return the templateID
	 */
	public String getTemplateID() {
		return templateID;
	}
	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return transactionType;
	}
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @return the emailList
	 */
	public List<EmailMessage> getEmailList() {
		return emailList;
	}
	/**
	 * @return the smsList
	 */
	public List<SMSMessage> getSmsList() {
		return smsList;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	/**
	 * @param templateID the templateID to set
	 */
	public void setTemplateID(String templateID) {
		this.templateID = templateID;
	}
	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @param emailList the emailList to set
	 */
	public void setEmailList(List<EmailMessage> emailList) {
		this.emailList = emailList;
	}
	/**
	 * @param smsList the smsList to set
	 */
	public void setSmsList(List<SMSMessage> smsList) {
		this.smsList = smsList;
	}

	
	/**
	 * @return the sentEmailsCount
	 */
	public int getSentEmailsCount() {
		return sentEmailsCount;
	}
	/**
	 * @return the failedEmailsCount
	 */
	public int getFailedEmailsCount() {
		return failedEmailsCount;
	}
	/**
	 * @return the sendSMSCount
	 */
	public int getSentSMSCount() {
		return sentSMSCount;
	}
	/**
	 * @return the failedSMSCount
	 */
	public int getFailedSMSCount() {
		return failedSMSCount;
	}
	/**
	 * @param sentEmailsCount the sentEmailsCount to set
	 */
	public void setSentEmailsCount(int sentEmailsCount) {
		this.sentEmailsCount = sentEmailsCount;
	}
	/**
	 * @param failedEmailsCount the failedEmailsCount to set
	 */
	public void setFailedEmailsCount(int failedEmailsCount) {
		this.failedEmailsCount = failedEmailsCount;
	}
	/**
	 * @param sendSMSCount the sendSMSCount to set
	 */
	public void setSentSMSCount(int sentSMSCount) {
		this.sentSMSCount = sentSMSCount;
	}
	/**
	 * @param failedSMSCount the failedSMSCount to set
	 */
	public void setFailedSMSCount(int failedSMSCount) {
		this.failedSMSCount = failedSMSCount;
	}
	/**
	 * @return the statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @return the sendEmail
	 */
	public Boolean getSendEmail() {
		return sendEmail;
	}
	/**
	 * @return the sendSMS
	 */
	public Boolean getSendSMS() {
		return sendSMS;
	}
	/**
	 * @param sendEmail the sendEmail to set
	 */
	public void setSendEmail(Boolean sendEmail) {
		this.sendEmail = sendEmail;
	}
	/**
	 * @param sendSMS the sendSMS to set
	 */
	public void setSendSMS(Boolean sendSMS) {
		this.sendSMS = sendSMS;
	}
	/**
	 * @return the pushNotificationList
	 */
	public List<PushNotificationVO> getPushNotificationList() {
		return pushNotificationList;
	}
	/**
	 * @param pushNotificationList the pushNotificationList to set
	 */
	public void setPushNotificationList(List<PushNotificationVO> pushNotificationList) {
		this.pushNotificationList = pushNotificationList;
	}
	/**
	 * @return the sendPushNotification
	 */
	public Boolean getSendPushNotifications() {
		return sendPushNotifications;
	}
	/**
	 * @param sendPushNotification the sendPushNotification to set
	 */
	public void setSendPushNotifications(Boolean sendPushNotifications) {
		this.sendPushNotifications = sendPushNotifications;
	}
	/**
	 * @return the appID
	 */
	public String getAppID() {
		return appID;
	}
	/**
	 * @param appID the appID to set
	 */
	public void setAppID(String appID) {
		this.appID = appID;
	}
	/**
	 * @return the sentPushNotificationsCount
	 */
	public int getSentPushNotificationsCount() {
		return sentPushNotificationsCount;
	}
	/**
	 * @return the failedPushNotificationsCount
	 */
	public int getFailedPushNotificationsCount() {
		return failedPushNotificationsCount;
	}
	/**
	 * @param sentPushNotificationsCount the sentPushNotificationsCount to set
	 */
	public void setSentPushNotificationsCount(int sentPushNotificationsCount) {
		this.sentPushNotificationsCount = sentPushNotificationsCount;
	}
	/**
	 * @param failedPushNotificationsCount the failedPushNotificationsCount to set
	 */
	public void setFailedPushNotificationsCount(int failedPushNotificationsCount) {
		this.failedPushNotificationsCount = failedPushNotificationsCount;
	}
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * @return the profileType
	 */
	public String getProfileType() {
		return profileType;
	}
	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	/**
	 * @param profileType the profileType to set
	 */
	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
    

}
