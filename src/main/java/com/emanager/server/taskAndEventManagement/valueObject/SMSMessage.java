package com.emanager.server.taskAndEventManagement.valueObject;

public class SMSMessage {
	
	String mobileNumber;
	String message;
	String groupName;
	int orgID;
	int userID;
	boolean sendPushNotifications=false;
	boolean sendSMS=false;
	int appID;
	String profileType;
	
	
	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}
	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	/**
	 * @return the sendPushNotifications
	 */
	public boolean isSendPushNotifications() {
		return sendPushNotifications;
	}
	/**
	 * @return the appID
	 */
	public int getAppID() {
		return appID;
	}
	/**
	 * @param sendPushNotifications the sendPushNotifications to set
	 */
	public void setSendPushNotifications(boolean sendPushNotifications) {
		this.sendPushNotifications = sendPushNotifications;
	}
	/**
	 * @param appID the appID to set
	 */
	public void setAppID(int appID) {
		this.appID = appID;
	}
	/**
	 * @return the profileType
	 */
	public String getProfileType() {
		return profileType;
	}
	/**
	 * @param profileType the profileType to set
	 */
	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}
	/**
	 * @return the sendSMS
	 */
	public boolean isSendSMS() {
		return sendSMS;
	}
	/**
	 * @param sendSMS the sendSMS to set
	 */
	public void setSendSMS(boolean sendSMS) {
		this.sendSMS = sendSMS;
	}
	

}
