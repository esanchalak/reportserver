package com.emanager.server.taskAndEventManagement.valueObject;

import java.util.ArrayList;

public class TaskVO {
	
	private int taskID;
	private int orgID;
	private String taskType;
	private int taskTypeID;
	private String description;
	private String taskStatus;
	private String taskStartDate;
	private int taskProcessID;
	private String taskFrequency;
	private String taskUpdateDate;
	private ArrayList<TaskVO> taskList;
	private int statusCode;
	private String message;
	
	
	/**
	 * @return the taskID
	 */
	public int getTaskID() {
		return taskID;
	}
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @return the taskType
	 */
	public String getTaskType() {
		return taskType;
	}
	/**
	 * @return the taskTypeID
	 */
	public int getTaskTypeID() {
		return taskTypeID;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @return the taskStatus
	 */
	public String getTaskStatus() {
		return taskStatus;
	}
	/**
	 * @return the taskStartDate
	 */
	public String getTaskStartDate() {
		return taskStartDate;
	}
	/**
	 * @return the taskProcessID
	 */
	public int getTaskProcessID() {
		return taskProcessID;
	}
	/**
	 * @return the taskFrequency
	 */
	public String getTaskFrequency() {
		return taskFrequency;
	}
	/**
	 * @return the taskUpdateDate
	 */
	public String getTaskUpdateDate() {
		return taskUpdateDate;
	}
	/**
	 * @param taskID the taskID to set
	 */
	public void setTaskID(int taskID) {
		this.taskID = taskID;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	/**
	 * @param taskType the taskType to set
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	/**
	 * @param taskTypeID the taskTypeID to set
	 */
	public void setTaskTypeID(int taskTypeID) {
		this.taskTypeID = taskTypeID;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @param taskStatus the taskStatus to set
	 */
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}
	/**
	 * @param taskStartDate the taskStartDate to set
	 */
	public void setTaskStartDate(String taskStartDate) {
		this.taskStartDate = taskStartDate;
	}
	/**
	 * @param taskProcessID the taskProcessID to set
	 */
	public void setTaskProcessID(int taskProcessID) {
		this.taskProcessID = taskProcessID;
	}
	/**
	 * @param taskFrequency the taskFrequency to set
	 */
	public void setTaskFrequency(String taskFrequency) {
		this.taskFrequency = taskFrequency;
	}
	/**
	 * @param taskUpdateDate the taskUpdateDate to set
	 */
	public void setTaskUpdateDate(String taskUpdateDate) {
		this.taskUpdateDate = taskUpdateDate;
	}
	/**
	 * @return the taskList
	 */
	public ArrayList<TaskVO> getTaskList() {
		return taskList;
	}
	/**
	 * @return the statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param taskList the taskList to set
	 */
	public void setTaskList(ArrayList<TaskVO> taskList) {
		this.taskList = taskList;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	

}
