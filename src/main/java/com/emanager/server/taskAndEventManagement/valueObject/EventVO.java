package com.emanager.server.taskAndEventManagement.valueObject;

import java.util.ArrayList;

public class EventVO {
	
	private int eventID;
	private int societyID;
	private int memberID;
	private String societyName;
	private String societyShortName;
	private int createdBy;
	private String templateID;
	private String eventType;
	private String eventSubject;
	private String emailFrequency;
	private String setDate;
	private String nextDate;
	private String lastDate;
	private String eventStatus;
	private String transactionType;
	private String message;
	private int sendEmail;
	private int sendSMS;
	private int onetime;//for Onetime reminders
	private String smsTemplate;
	private int sendToGroup;
	private String emailID;
	private int isBcc;
	private String bccMail;
	private String mobile;
	private ArrayList<EventVO> eventList;
	private int statusCode;
	private int documentID;
	private int sendSummary;
	private String ccEmail;
	private int isManual;
	private int updatedBy;
	private String createrName;
	private String updaterName;
	private String createDate;
	private String updateDate;
	
	/**
	 * @return the eventID
	 */
	public int getEventID() {
		return eventID;
	}
	/**
	 * @return the societyID
	 */
	public int getSocietyID() {
		return societyID;
	}
	/**
	 * @return the createdBy
	 */
	public int getCreatedBy() {
		return createdBy;
	}
	/**
	 * @return the templateID
	 */
	public String getTemplateID() {
		return templateID;
	}
	/**
	 * @return the eventType
	 */
	public String getEventType() {
		return eventType;
	}
	/**
	 * @return the eventSubject
	 */
	public String getEventSubject() {
		return eventSubject;
	}
	/**
	 * @return the emailFrequency
	 */
	public String getEmailFrequency() {
		return emailFrequency;
	}
	/**
	 * @return the setDate
	 */
	public String getSetDate() {
		return setDate;
	}
	/**
	 * @return the nextDate
	 */
	public String getNextDate() {
		return nextDate;
	}
	/**
	 * @return the lastDate
	 */
	public String getLastDate() {
		return lastDate;
	}
	/**
	 * @return the eventStatus
	 */
	public String getEventStatus() {
		return eventStatus;
	}
	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return transactionType;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @return the sendEmail
	 */
	public int getSendEmail() {
		return sendEmail;
	}
	/**
	 * @return the sendSMS
	 */
	public int getSendSMS() {
		return sendSMS;
	}
	/**
	 * @return the onetime
	 */
	public int getOnetime() {
		return onetime;
	}
	/**
	 * @return the smsTemplate
	 */
	public String getSmsTemplate() {
		return smsTemplate;
	}
	/**
	 * @return the sendToGroup
	 */
	public int getSendToGroup() {
		return sendToGroup;
	}
	/**
	 * @return the emailID
	 */
	public String getEmailID() {
		return emailID;
	}
	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * @param eventID the eventID to set
	 */
	public void setEventID(int eventID) {
		this.eventID = eventID;
	}
	/**
	 * @param societyID the societyID to set
	 */
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @param templateID the templateID to set
	 */
	public void setTemplateID(String templateID) {
		this.templateID = templateID;
	}
	/**
	 * @param eventType the eventType to set
	 */
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	/**
	 * @param eventSubject the eventSubject to set
	 */
	public void setEventSubject(String eventSubject) {
		this.eventSubject = eventSubject;
	}
	/**
	 * @param emailFrequency the emailFrequency to set
	 */
	public void setEmailFrequency(String emailFrequency) {
		this.emailFrequency = emailFrequency;
	}
	/**
	 * @param setDate the setDate to set
	 */
	public void setSetDate(String setDate) {
		this.setDate = setDate;
	}
	/**
	 * @param nextDate the nextDate to set
	 */
	public void setNextDate(String nextDate) {
		this.nextDate = nextDate;
	}
	/**
	 * @param lastDate the lastDate to set
	 */
	public void setLastDate(String lastDate) {
		this.lastDate = lastDate;
	}
	/**
	 * @param eventStatus the eventStatus to set
	 */
	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}
	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @param sendEmail the sendEmail to set
	 */
	public void setSendEmail(int sendEmail) {
		this.sendEmail = sendEmail;
	}
	/**
	 * @param sendSMS the sendSMS to set
	 */
	public void setSendSMS(int sendSMS) {
		this.sendSMS = sendSMS;
	}
	/**
	 * @param onetime the onetime to set
	 */
	public void setOnetime(int onetime) {
		this.onetime = onetime;
	}
	/**
	 * @param smsTemplate the smsTemplate to set
	 */
	public void setSmsTemplate(String smsTemplate) {
		this.smsTemplate = smsTemplate;
	}
	/**
	 * @param sendToGroup the sendToGroup to set
	 */
	public void setSendToGroup(int sendToGroup) {
		this.sendToGroup = sendToGroup;
	}
	/**
	 * @param emailID the emailID to set
	 */
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/**
	 * @return the societyName
	 */
	public String getSocietyName() {
		return societyName;
	}
	/**
	 * @param societyName the societyName to set
	 */
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	/**
	 * @return the societyShortName
	 */
	public String getSocietyShortName() {
		return societyShortName;
	}
	/**
	 * @param societyShortName the societyShortName to set
	 */
	public void setSocietyShortName(String societyShortName) {
		this.societyShortName = societyShortName;
	}
	/**
	 * @return the bccMail
	 */
	public String getBccMail() {
		return bccMail;
	}
	/**
	 * @param bccMail the bccMail to set
	 */
	public void setBccMail(String bccMail) {
		this.bccMail = bccMail;
	}
	/**
	 * @return the isBcc
	 */
	public int getIsBcc() {
		return isBcc;
	}
	/**
	 * @param isBcc the isBcc to set
	 */
	public void setIsBcc(int isBcc) {
		this.isBcc = isBcc;
	}
	/**
	 * @return the eventList
	 */
	public ArrayList<EventVO> getEventList() {
		return eventList;
	}
	/**
	 * @param eventList the eventList to set
	 */
	public void setEventList(ArrayList<EventVO> eventList) {
		this.eventList = eventList;
	}
	/**
	 * @return the statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @return the memberID
	 */
	public int getMemberID() {
		return memberID;
	}
	/**
	 * @param memberID the memberID to set
	 */
	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}
	/**
	 * @return the documentID
	 */
	public int getDocumentID() {
		return documentID;
	}
	/**
	 * @param documentID the documentID to set
	 */
	public void setDocumentID(int documentID) {
		this.documentID = documentID;
	}
	/**
	 * @return the sendSummary
	 */
	public int getSendSummary() {
		return sendSummary;
	}
	/**
	 * @param sendSummary the sendSummary to set
	 */
	public void setSendSummary(int sendSummary) {
		this.sendSummary = sendSummary;
	}
	/**
	 * @return the ccEmail
	 */
	public String getCcEmail() {
		return ccEmail;
	}
	/**
	 * @param ccEmail the ccEmail to set
	 */
	public void setCcEmail(String ccEmail) {
		this.ccEmail = ccEmail;
	}
	/**
	 * @return the isManual
	 */
	public int getIsManual() {
		return isManual;
	}
	/**
	 * @param isManual the isManual to set
	 */
	public void setIsManual(int isManual) {
		this.isManual = isManual;
	}
	public int getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getCreaterName() {
		return createrName;
	}
	public void setCreaterName(String createrName) {
		this.createrName = createrName;
	}
	public String getUpdaterName() {
		return updaterName;
	}
	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	
	
	
	
	

}
