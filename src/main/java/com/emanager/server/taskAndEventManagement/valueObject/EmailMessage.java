package com.emanager.server.taskAndEventManagement.valueObject;

public class EmailMessage {
	
	String emailTo;
	String emailCc;
	String emailBcc;
	String subject;
	String Message;
	String defaultSender;
	String fromEmail;
	String replyTo;
	String groupName;
	int orgID;
	int userID;
	boolean sendPushNotifications=false;
	int appID;
	String profileType;
	
	
	/**
	 * @return the emailto
	 */
	public String getEmailTO() {
		return emailTo;
	}
	/**
	 * @param emailto the emailto to set
	 */
	public void setEmailTO(String emailto) {
		this.emailTo = emailto;
	}
	/**
	 * @return the emailCc
	 */
	public String getEmailCc() {
		return emailCc;
	}
	/**
	 * @param emailCc the emailCc to set
	 */
	public void setEmailCc(String emailCc) {
		this.emailCc = emailCc;
	}
	/**
	 * @return the emailBcc
	 */
	public String getEmailBcc() {
		return emailBcc;
	}
	/**
	 * @param emailBcc the emailBcc to set
	 */
	public void setEmailBcc(String emailBcc) {
		this.emailBcc = emailBcc;
	}
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return Message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		Message = message;
	}
	/**
	 * @return the defaultSender
	 */
	public String getDefaultSender() {
		return defaultSender;
	}
	/**
	 * @param defaultSender the defaultSender to set
	 */
	public void setDefaultSender(String defaultSender) {
		this.defaultSender = defaultSender;
	}
	/**
	 * @return the fromEmail
	 */
	public String getFromEmail() {
		return fromEmail;
	}
	/**
	 * @return the replyTo
	 */
	public String getReplyTo() {
		return replyTo;
	}
	/**
	 * @param fromEmail the fromEmail to set
	 */
	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}
	/**
	 * @param replyTo the replyTo to set
	 */
	public void setReplyTo(String replyTo) {
		this.replyTo = replyTo;
	}
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @return the userID
	 */
	public int getUserID() {
		return userID;
	}
	/**
	 * @return the sendPushNotifications
	 */
	public boolean isSendPushNotifications() {
		return sendPushNotifications;
	}
	/**
	 * @return the appID
	 */
	public int getAppID() {
		return appID;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	/**
	 * @param userID the userID to set
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}
	/**
	 * @param sendPushNotifications the sendPushNotifications to set
	 */
	public void setSendPushNotifications(boolean sendPushNotifications) {
		this.sendPushNotifications = sendPushNotifications;
	}
	/**
	 * @param appID the appID to set
	 */
	public void setAppID(int appID) {
		this.appID = appID;
	}
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	/**
	 * @return the profileType
	 */
	public String getProfileType() {
		return profileType;
	}
	/**
	 * @param profileType the profileType to set
	 */
	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}

	
	
	

}
