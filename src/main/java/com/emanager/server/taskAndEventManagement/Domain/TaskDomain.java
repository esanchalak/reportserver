package com.emanager.server.taskAndEventManagement.Domain;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;

import com.emanager.server.taskAndEventManagement.DAO.TaskDAO;
import com.emanager.server.taskAndEventManagement.valueObject.TaskVO;

public class TaskDomain {
	static Logger log = Logger.getLogger(TaskDomain.class.getName());
	TaskDAO taskDAO;

	
public List getTaskList(int orgID)  {
		
		log.debug("Entry : public List getTaskList(int orgID)");
		
		List taskList = null;
		try {
		
			
		

			taskList = taskDAO.getTaskList(orgID);
			
			
			if (taskList.size() != 0) {
				log.debug("size Of the List" + taskList.size());
				
			}
			
			log.debug("Exit : public List getTaskList(int orgID)");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException : public List getTaskList(int orgID)"+e);
		}
		 catch (Exception e) {
				
				log.error("Exception : public List getTaskList(int orgID)"+e);
			}
		return taskList;
	}
	
	

	public int addTask(TaskVO taskVO)  {
	
	log.debug("Entry : public int addTask(TaskVO taskVO)");
	
	int successFlag=0;
	try {
		
		
		
		successFlag=taskDAO.addTask(taskVO);
		
		
		
		log.debug("Exit : public int addTask(TaskVO taskVO)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int addTask(TaskVO taskVO)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int addTask(TaskVO taskVO) "+e);
		}
	return successFlag;
}


public int updateTask(TaskVO taskVO,int taskID)  {
	
	log.debug("Entry : public int updateTask(TaskVO taskVO,int taskID)");
	
	int successFlag=0;
	try {
	
		   successFlag=taskDAO.updateTask(taskVO, taskID);
		
		log.debug("Exit : public int updateTask(TaskVO taskVO,int taskID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int updateTask(TaskVO taskVO,int taskID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int updateTask(TaskVO taskVO,int taskID) "+e);
		}
	return successFlag;
}



public int deleteTask(int taskID)  {
	
	log.debug("Entry : public int deleteTask(int taskID)");
	
	int successFlag=0;
	try {
		   
		   successFlag=taskDAO.deleteTask(taskID);
		
		log.debug("Exit : public int deleteTask(int taskID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int deleteTask(int taskID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int deleteTask(int taskID)"+e);
		}
	return successFlag;
}

public int updateTaskStatus(TaskVO taskVO)  {
	
	log.debug("Entry : public int updateTaskStatus(int taskID)");
	int successFlag=0;
	try {
		successFlag=taskDAO.updateTaskStatus(taskVO);
				
		log.debug("Exit : public int updateTaskStatus(int taskID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int updateTaskStatus(int taskID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int updateTaskStatus(int taskID) "+e);
		}
	return successFlag;
}


public int updateTaskRunningStatus(TaskVO taskVO)  {
	
	log.debug("Entry : public int updateTaskRunningStatus(TaskVO taskVO)");
	int successFlag=0;
	try {
	 successFlag=taskDAO.updateTaskRunningStatus(taskVO);
		
		log.debug("Exit : public int updateTaskRunningStatus(TaskVO taskVO)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int updateTaskRunningStatus(TaskVO taskVO)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int updateTaskRunningStatus(TaskVO taskVO) "+e);
		}
	return successFlag;
}



/**
 * @param taskDAO the taskDAO to set
 */
public void setTaskDAO(TaskDAO taskDAO) {
	this.taskDAO = taskDAO;
}

	
	
	
}
