package com.emanager.server.taskAndEventManagement.Domain;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.joda.time.LocalDateTime;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.emanager.server.adminReports.valueObject.RptTenantVO;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.domainObject.HtmlTableGenerator;
import com.emanager.server.commonUtils.domainObject.VMTemplatePreparator;
import com.emanager.server.commonUtils.services.AddressService;
import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.taskAndEventManagement.DAO.EventDAO;
import com.emanager.server.taskAndEventManagement.Service.NotificationService;
import com.emanager.server.taskAndEventManagement.valueObject.EmailMessage;
import com.emanager.server.taskAndEventManagement.valueObject.EventVO;
import com.emanager.server.taskAndEventManagement.valueObject.NotificationVO;
import com.emanager.server.taskAndEventManagement.valueObject.PushNotificationVO;
import com.emanager.server.taskAndEventManagement.valueObject.SMSGatewayVO;
import com.emanager.server.taskAndEventManagement.valueObject.SMSMessage;
import com.emanager.server.taskAndEventManagement.valueObject.TicketVO;
import com.emanager.server.userManagement.service.AppDetailsService;
import com.emanager.server.userManagement.valueObject.AppVO;
import com.emanager.server.userManagement.valueObject.UserVO;
import com.google.gson.Gson;

public class EventDomain {
	
	static Logger log = Logger.getLogger(EventDomain.class.getName());
	EventDAO eventDAO;
	SocietyService societyServiceBean;
	MemberService memberServiceBean;
	AddressService addressServiceBean;
	NotificationService notificationService;
	AppDetailsService appDetailsService;
	HtmlTableGenerator htmlTableGenerator=new HtmlTableGenerator();
	VMTemplatePreparator templatePreparator=new VMTemplatePreparator();
	DateUtility dateUtil=new DateUtility();
	
public List getEventList(int societyID)  {
		
		log.debug("Entry : public List getEventList(int societyID)");
		
		List eventList = null;
		try {
		
	

			eventList = eventDAO.getEventList(societyID);
			
			
			
			log.debug("Exit : public List getEventList(int societyID)");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException :"+e);
		}
		 catch (Exception e) {
				
				log.error("DataAccessException :"+e);
			}
		return eventList;
	}

public List getEventListForOrg(int orgID)  {
	
	log.debug("Entry : public List getEventListForOrg(int orgID)");
	
	List eventList = null;
	try {
		eventList = eventDAO.getEventListForOrg(orgID);	
		
		log.debug("Exit : public List getEventListForOrg(int orgID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException :"+e);
	}
	 catch (Exception e) {
			
			log.error("DataAccessException :"+e);
		}
	return eventList;
}

public List getPaymentReminderEventDetails(int societyID)  {
	
	log.debug("Entry : public List getPaymentReminderEventDetails(int societyID)");
	
	List eventList = null;
	try {
	


		eventList = eventDAO.getPaymentReminderEventDetails(societyID);
		
		
		
		log.debug("Exit : public List getPaymentReminderEventDetails(int societyID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException :"+e);
	}
	 catch (Exception e) {
			
			log.error("DataAccessException :"+e);
		}
	return eventList;
}


public int addEvent(EventVO eventVO)  {
	
	log.debug("Entry : public int addEvent(EventVO eventVO)");
	
	int successFlag=0;
	try {
	


		successFlag = eventDAO.addEvent(eventVO);
		
		
		
		log.debug("Exit : public int addEvent(EventVO eventVO)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int addEvent(EventVO eventVO)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int addEvent(EventVO eventVO) "+e);
		}
	return successFlag;
}


public int updateEvent(EventVO eventVO,int eventID)  {
	
	log.debug("Entry : public int updateEvent(EventVO eventVO,int eventID)");
	
	int successFlag=0;
	try {
	


		successFlag = eventDAO.updateEvent(eventVO,eventID);
		
		
		
		log.debug("Exit : public int updateEvent(EventVO eventVO,int eventID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int updateEvent(EventVO eventVO,int eventID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int updateEvent(EventVO eventVO,int eventID) "+e);
		}
	return successFlag;
}


public int updateEventDateFrequency(EventVO eventVO,int eventID)  {
	
	log.debug("Entry : public int updateEvent(EventVO eventVO,int eventID)");
	
	int successFlag=0;
	try {
		successFlag = eventDAO.updateEventDateFrequency(eventVO,eventID);		
		log.debug("Exit : public int updateEventDateFrequency(EventVO eventVO,int eventID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int updateEventDateFrequency(EventVO eventVO,int eventID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int updateEventDateFrequency(EventVO eventVO,int eventID) "+e);
		}
	return successFlag;
}

public int updateEventFreuency(List eventList)  {
	
	log.debug("Entry : public int updateEventFreuency(EventVO eventVO,int eventID)");
	
	int successFlag=0;
	try {
	
		for(int i=0;eventList.size()>i;i++){
			EventVO eventVO=(EventVO) eventList.get(i);

		successFlag = eventDAO.updateEventFreuency(eventVO,eventVO.getEventID());
		}
		
		
		log.debug("Exit : public int updateEventFreuency(EventVO eventVO,int eventID)");	
	} catch (ArrayIndexOutOfBoundsException e) {
		
		log.info("ArrayIndexOutOfBoundsException : public int updateEventFreuency(EventVO eventVO,int eventID)"+e);
	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int updateEventFreuency(EventVO eventVO,int eventID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int updateEventFreuency(EventVO eventVO,int eventID) "+e);
		}
	return successFlag;
}

public int deleteEvent(int eventID)  {
	
	log.debug("Entry : public int deleteEvent(int eventID)");
	
	int successFlag=0;
	try {
	


		successFlag = eventDAO.deleteEvent(eventID);
		
		
		
		log.debug("Exit : public int deleteEvent(int eventID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int deleteEvent(int eventID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int deleteEvent(int eventID)"+e);
		}
	return successFlag;
}

public int sendAuditDocReminderEvent(EventVO eventVO)  {
	
	log.debug("Entry : public int sendAuditDocReminderEvent(EventVO eventVO) ");
	TicketVO ticketVO = new TicketVO();
	int successFlag=0;
	try {
		EmailMessage emailVO=new EmailMessage();
		
		NotificationVO notificationVO=new NotificationVO();
		
		List emailList=new ArrayList();
	
		SocietyVO societyVO=new SocietyVO();
		
		societyVO=societyServiceBean.getSocietyDetails("0", eventVO.getSocietyID());
		
		ticketVO=getTicketStatus(societyVO.getSocietyID(), societyVO.getOrgID(), "alltkt");
		
		if((ticketVO.getTicketInfo()!=null)&&(ticketVO.getTicketInfo().size()>0)){
			String htmlTable=htmlTableGenerator.getHTMLTableForManagementPendingTickets(ticketVO.getTicketInfo());
			emailVO.setDefaultSender("E-Sanchalak Services");
			emailVO.setEmailTO(eventVO.getEmailID());
			if(eventVO.getCcEmail().length()>0){
				emailVO.setEmailCc(eventVO.getCcEmail());
			}
			emailVO.setSubject(eventVO.getEventSubject());
			 VelocityContext model = new VelocityContext();
		     model.put("htmlString", htmlTable);
		     emailVO.setMessage(templatePreparator.createMessage(model, eventVO.getTemplateID()));
			
		     emailList.add(emailVO);
		     notificationVO.setEmailList(emailList);
		     notificationVO.setSendEmail(true);
		     notificationVO.setSendSMS(false);
		     notificationVO=SendNotifications(notificationVO);
		}
			
		
		
		if(notificationVO.getSentEmailsCount()>0){
			successFlag=1;
		}
		
		
		
		
		log.debug("Exit : public int sendAuditDocReminderEvent(EventVO eventVO) ");	
	} catch (DataAccessException e) {
		successFlag=0;
		log.error("DataAccessException : public int sendAuditDocReminderEvent(EventVO eventVO) "+e);
	}
	 catch (Exception e) {
		 successFlag=0;
			log.error("Exception : public int sendAuditDocReminderEvent(EventVO eventVO) "+e);
		}
	return successFlag;
}

public TicketVO getTicketStatus(int societID,int orgID,String statusType){
	NotificationVO notificationVO=new NotificationVO();
	String apiURL="";
	String apiStatus="";
	TicketVO ticketsVO=new TicketVO();
	ConfigManager c=new ConfigManager();
	log.debug("Entry : private NotificationVO getTicketStatus(String societID,int orgID,String statusType)");
	
	try {
		char quotes ='"';
		
		apiURL=c.getPropertiesValue("ticket.apiURL")+"/tickets/request/document";
        apiStatus="alltkt";
        String data ="{"+quotes+URLEncoder.encode("Status", "UTF-8") +quotes+ ":" +quotes+ URLEncoder.encode(apiStatus, "UTF-8")+quotes;
	    data += "," +quotes+ URLEncoder.encode("OrgID", "UTF-8")+quotes + ":" +quotes+ URLEncoder.encode(""+orgID, "UTF-8")+quotes+"}";
	    
	 
	    URL url;
		
			url = new URL(apiURL);
		
	
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			//add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		    con.setRequestProperty("Content-Type", 
	        "application/x-www-form-urlencoded");
			
			//data={"Status":"mgmt","OrgID":"2"};
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(data);
			wr.flush();
			wr.close();
			
			int responseCode = con.getResponseCode();
			log.debug("\nSending 'POST' request to URL : " + url);
			log.debug("Post parameters : " + data);
			log.debug("Response Code : " + responseCode);
			if(responseCode==200){
	    BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
	    String line;
	    while ((line = rd.readLine()) != null) {
	    	 JSONParser j = new JSONParser();
             JSONObject o = (JSONObject)j.parse(line);
             String json=o.toString();
          
             
             // Now do the magic.
             ticketsVO = new Gson().fromJson(json, TicketVO.class);

             // Show it.
            
            
	 
	    }
	   
			}
	
	}catch(NullPointerException ex) {
        log.debug("No tickets request found for societyID: "+societID);
       
    }catch(Exception e) {
        log.error("Exception occured in  "+e);
    }   
	
	
	log.debug("Exit : private NotificationVO getTicketStatus(String societID,int orgID,String statusType)");
	return ticketsVO;
}

public int sendTenantExpiredReminder(EventVO eventVO)  {
	
	log.debug("Entry : public int sendAuditDocReminderEvent(EventVO eventVO) ");
	
	int successFlag=0;
	List tenantList=new ArrayList();
	try {
		LocalDateTime now = LocalDateTime.now();
		int year = now.getYear();
		int month = now.getMonthOfYear();
		NotificationVO notificationVO=new NotificationVO();
		SocietyVO societyVO=societyServiceBean.getSocietyDetails(eventVO.getSocietyID());
	       Date uptoDate=dateUtil.findCurrentDate();
	      List ownerList=new ArrayList();
	      List tenantsList=new ArrayList();
	      List emailList=new ArrayList();

		tenantList = memberServiceBean.getExpiredtenantDetails(eventVO.getSocietyID(),uptoDate.toString(),false);
		
		for(int i=0;tenantList.size()>i;i++){
			RptTenantVO rptTenantVO=(RptTenantVO) tenantList.get(i);
			EmailMessage ownerEmail=new EmailMessage();
			EmailMessage tenantEmail=new EmailMessage();
			
			if(rptTenantVO.getOwnerEmail()!=null){
				ownerEmail=prepareOwnerEmailObject(rptTenantVO, eventVO.getTemplateID(), societyVO, eventVO);
				ownerList.add(ownerEmail);
				emailList.add(ownerEmail);
			}
			
			if(rptTenantVO.getEmail()!=null){
				tenantEmail=prepareTenantEmailObject(rptTenantVO, eventVO.getTemplateID(), societyVO, eventVO);
				ownerList.add(tenantEmail);
				emailList.add(tenantEmail);
			}
			
		} 
		
	     notificationVO.setEmailList(emailList);
	     notificationVO.setSendEmail(true);
	     notificationVO.setSendSMS(false);
	     
	     
		
	 	
			notificationVO=SendNotifications(notificationVO);
			
			if(notificationVO.getSentEmailsCount()>0){
				successFlag=1;
				if(eventVO.getSendSummary()==1){
					
					NotificationVO summaryNotification=processTenantSummaryEmailObject(eventVO, tenantList.size(),societyVO);
				
					
					
				}
				
			}
			
		
		
		log.debug("Exit : public int sendTenantExpiredReminder(EventVO eventVO) ");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int sendTenantExpiredReminder(EventVO eventVO) "+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int sendTenantExpiredReminder(EventVO eventVO) "+e);
		}
	return successFlag;
}


public int sendTenantAboutToExpireReminder(EventVO eventVO)  {
	
	log.debug("Entry : public int sendTenantAboutToExpireReminder(EventVO eventVO) ");
	
	int successFlag=0;
	List tenantList=new ArrayList();
	try {
		LocalDateTime now = LocalDateTime.now();
		int year = now.getYear();
		int month = now.getMonthOfYear();
		NotificationVO notificationVO=new NotificationVO();
		SocietyVO societyVO=societyServiceBean.getSocietyDetails(eventVO.getSocietyID());
	       Date uptoDate=dateUtil.getMonthLastDate((month+1), year);
	      List ownerList=new ArrayList();
	      List tenantsList=new ArrayList();
	      List emailList=new ArrayList();
	     

		tenantList = memberServiceBean.getExpiredtenantDetails(eventVO.getSocietyID(),uptoDate.toString(),true);
		
		for(int i=0;tenantList.size()>i;i++){
			RptTenantVO rptTenantVO=(RptTenantVO) tenantList.get(i);
			EmailMessage ownerEmail=new EmailMessage();
			EmailMessage tenantEmail=new EmailMessage();
			
			if(rptTenantVO.getOwnerEmail()!=null){
				ownerEmail=prepareOwnerEmailObject(rptTenantVO,eventVO.getTemplateID(), societyVO, eventVO);
				ownerList.add(ownerEmail);
				emailList.add(ownerEmail);
			}
			
			if(rptTenantVO.getEmail()!=null){
				tenantEmail=prepareTenantEmailObject(rptTenantVO, eventVO.getTemplateID(), societyVO, eventVO);
				ownerList.add(tenantEmail);
				emailList.add(tenantEmail);
			}
			
		} 
		
	     notificationVO.setEmailList(emailList);
	     notificationVO.setSendEmail(true);
	     notificationVO.setSendSMS(false);
	     
	     
		
	 	
			notificationVO=SendNotifications(notificationVO);
			
	
			
			
			
			if(notificationVO.getSentEmailsCount()>0){
				successFlag=1;
				if(eventVO.getSendSummary()==1){
					
					NotificationVO summaryNotification=processTenantSummaryEmailObject(eventVO, tenantList.size(),societyVO);
				
					
					
				}
				
				
			}
			
		
		
		log.debug("Exit : public int sendTenantAboutToExpireReminder(EventVO eventVO) ");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int sendTenantAboutToExpireReminder(EventVO eventVO) "+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int sendTenantAboutToExpireReminder(EventVO eventVO) "+e);
		}
	return successFlag;
}

public int sendSubscriptionExpiringReminder(EventVO eventVO)  {
	
	log.debug("Entry : public int sendSubscriptionExpiringReminder(EventVO eventVO) ");
	
	int successFlag=0;
	try {
			
			
		log.debug("Exit : public int sendSubscriptionExpiringReminder(EventVO eventVO) ");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int sendSubscriptionExpiringReminder(EventVO eventVO) "+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int sendSubscriptionExpiringReminder(EventVO eventVO) "+e);
		}
	return successFlag;
}


public int sendSubscriptionExpiringSoonReminder()  {
	
	log.debug("Entry : public int sendSubscriptionExpiringSoonReminder(EventVO eventVO) ");
	
	int successFlag=0;
	List tenantList=new ArrayList();
	try {
		//Send 30 days prior reminders to organizations 
		int success30=sendSubscriptionExpiringReminder(30);
		
		//Send 15 days prior reminders to organizations 
		int success15=sendSubscriptionExpiringReminder( 15);
		
		//Send 7 days prior reminders to organizations 
		int success7=sendSubscriptionExpiringReminder( 7);
				
		//Send 1 days prior reminders to organizations 
		int success1=sendSubscriptionExpiringReminder( 1);
			
	
		log.debug("Exit : public int sendSubscriptionExpiringSoonReminder(EventVO eventVO) ");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int sendSubscriptionExpiringSoonReminder(EventVO eventVO) "+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int sendSubscriptionExpiredReminder(EventVO eventVO) "+e);
		}
	return successFlag;
}

public int sendSubscriptionExpiringReminder(int period)  {
	
	log.debug("Entry : public int sendSubscriptionExpiringReminder(int appID,int period) ");
	
	int successFlag=0;
	NotificationVO notificationVO=new NotificationVO();
	
	try {
		
			List appsList=appDetailsService.getAppDetailsList();
			
			for(int j=0;appsList.size()>j;j++){
			AppVO appVO=(AppVO) appsList.get(j);
			List<EmailMessage> emailList=new ArrayList<>();
			List orgList=societyServiceBean.getSubscriptionExpiringOrgList(appVO.getAppID(), period);
	    
			for(int i=0;orgList.size()>i;i++){
				EmailMessage emailVO=new EmailMessage();
				SocietyVO societyVO=(SocietyVO) orgList.get(i);
			
	    	 
	    	if((societyVO.getMailListA()!=null)&&(societyVO.getMailListA().length()>0)&&(!appVO.getAppID().equalsIgnoreCase("1"))&&(!appVO.getAppID().equalsIgnoreCase("5"))){
	    	 emailVO.setEmailTO(societyVO.getMailListA());
	    	 emailVO.setEmailCc("billling@esanchalak.com");
	    	
	    	emailVO.setDefaultSender(appVO.getAppFullName()+" Services");
	    	emailVO.setSubject("Subscription Renewal Notice");
	    
	    	java.util.Date date=new java.util.Date(societyVO.getAppSubscriptionValidity().getTime());  
		   SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");  
		   String expiryDate = formatter.format(date);  
	        	  
	    	    VelocityContext model = new VelocityContext();
	    	    model.put("societyName", societyVO.getSocFullName());
	    	 	model.put("expiryDate", expiryDate);
	    	 	if(period==1)
	    	 	model.put("days", period+" day ");
	    	 	else
	    	 	model.put("days", period+" days ");	
	    	 	model.put("appName", appVO.getAppFullName());
	    	 		model.put("imageUrl", appVO.getLogoUrl());
	    	 
	    	    
	    	    /*  add that list to a VelocityContext  */
	    	  
	    	    emailVO.setMessage(templatePreparator.createMessage(model, "subscriptionExpiringSoon.vm"));
	    	  emailList.add(emailVO);
	    	 log.info("Subscription has been expired.");
			}
	     
	}

	     notificationVO.setEmailList(emailList);
	     notificationVO.setSendEmail(true);
	     notificationVO.setSendSMS(false);
	     if(emailList.size()>0)
			notificationVO=SendNotifications(notificationVO);
			
			}
		log.debug("Exit : public int sendSubscriptionExpiringReminder(int appID,int period) ");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int sendSubscriptionExpiringReminder(int appID,int period) "+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int sendSubscriptionExpiredReminder(EventVO eventVO) "+e);
		}
	return successFlag;
}

public int sendSubscriptionExpiredReminder(int appID)  {
	
	log.debug("Entry : public int sendSubscriptionExpiredReminder(EventVO eventVO) ");
	
	int successFlag=0;
	NotificationVO notificationVO=new NotificationVO();
	List<EmailMessage> emailList=new ArrayList<>();
	try {
		
			List orgList=societyServiceBean.getSubscriptionExpiredOrgList(appID,1);
	    
			for(int i=0;orgList.size()>i;i++){
				
				SocietyVO societyVO=(SocietyVO) orgList.get(i);
				EmailMessage emailVO=new EmailMessage();
	    	 emailVO.setDefaultSender("E-Sanchalak Services");
	    	if(societyVO.getMailListA().length()>0){
	    	 emailVO.setEmailTO(societyVO.getMailListA());
	    	}
	    	emailVO.setSubject("Subscription Renewal Notice");
	    	java.util.Date date=new java.util.Date(societyVO.getAppSubscriptionValidity().getTime());
	    	 Calendar cal = Calendar.getInstance();
	    	    cal.setTime(date);
	    	    cal.add(Calendar.MONTH, -1);
	    	    date=cal.getTime();
		   SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");  
		   String expiryDate = formatter.format(date);  
	        	  
	    	    VelocityContext model = new VelocityContext();
	    	    model.put("societyName", societyVO.getSocFullName());
	    	 	model.put("expiryDate", expiryDate);
	    	
	    	    
	    	    /*  add that list to a VelocityContext  */
	    	  
	    	    emailVO.setMessage(templatePreparator.createMessage(model, "subscriptionExpired.vm"));
	    	  emailList.add(i,emailVO);
	    	 log.info("Subscription has been expired.");
	     
	     
	}

	     notificationVO.setEmailList(emailList);
	     notificationVO.setSendEmail(true);
	     notificationVO.setSendSMS(false);
	     
			notificationVO=SendNotifications(notificationVO);
			
	
		log.debug("Exit : public int sendSubscriptionExpiredReminder(EventVO eventVO) ");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int sendSubscriptionExpiredReminder(EventVO eventVO) "+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int sendSubscriptionExpiredReminder(EventVO eventVO) "+e);
		}
	return successFlag;
}

private EmailMessage prepareOwnerEmailObject(RptTenantVO ownerVO,String template,SocietyVO societyVO,EventVO eventVO){
	EmailMessage emailVO=new EmailMessage();
	BigDecimal totalAmount=BigDecimal.ZERO;
	java.util.Date today = new java.util.Date();
	SimpleDateFormat newFormat=new SimpleDateFormat("dd-MMM-yy");
   AddressVO addressVO=addressServiceBean.getAddressDetails(ownerVO.getMemberID(), ownerVO.getApt_name(), "M",societyVO.getSocietyID());
	String address="";
	
	emailVO.setDefaultSender(societyVO.getSocietyName());
	emailVO.setEmailTO(ownerVO.getOwnerEmail());
	if(eventVO.getCcEmail().length()>0){
		emailVO.setEmailCc(eventVO.getCcEmail());
	}
	emailVO.setSubject(eventVO.getEventSubject());
	
	try {
			
		
		
	
  
    VelocityContext model = new VelocityContext();
    model.put("userName", ownerVO.getOwnerName());
    model.put("currentDate",newFormat.format(today));
    model.put("unitName", ownerVO.getApt_name());
    model.put("mobile", ownerVO.getOwnerMobile());
	model.put("address", addressVO.getAddress());
	model.put("aggrementEndDate", dateUtil.getConvetedDate(ownerVO.getAgg_end_date()));
	model.put("societyName", societyVO.getSocietyName());
	model.put("societyFullName",societyVO.getSocFullName());
	model.put("regNo", societyVO.getSocietyRegNo());
	model.put("societyAddress", societyVO.getAddress());
	model.put("user", "Member");
	model.put("stayingComment", "sublet your");
	model.put("vacateComment", "the tenant vacates");
	model.put("vacateComment2", "<b>please ensure that the tenant vacates the flat <u>immediately</u> and inform the society accordingly.</b>");
	model.put("vacateComment3", "by replying this communiation at the ealiest, as<b> your tenant is being served a notice to vacate the flat.</b>");
   
    /*  add that list to a VelocityContext  */
  
    emailVO.setMessage(templatePreparator.createMessage(model, template));
	} catch (Exception e) {
		log.error("Exception in preparing member object "+e);
	}
	
    
	return emailVO;
}

private EmailMessage prepareTenantEmailObject(RptTenantVO tenantVO,String template,SocietyVO societyVO,EventVO eventVO){
	EmailMessage emailVO=new EmailMessage();
	BigDecimal totalAmount=BigDecimal.ZERO;
	java.util.Date today = new java.util.Date();
	SimpleDateFormat newFormat=new SimpleDateFormat("dd-MMM-yy");
   AddressVO addressVO=addressServiceBean.getAddressDetails(tenantVO.getRenterID(), tenantVO.getApt_name(), "R",societyVO.getSocietyID());
	String address="";
	
	emailVO.setDefaultSender(societyVO.getSocietyName());
	emailVO.setEmailTO(tenantVO.getEmail());
	if(eventVO.getCcEmail().length()>0){
		emailVO.setEmailCc(eventVO.getCcEmail());
	}
	emailVO.setSubject(eventVO.getEventSubject());
	
	try {
			
		
		
	
  
    VelocityContext model = new VelocityContext();
    model.put("userName", tenantVO.getName());
    model.put("currentDate",newFormat.format(today));
    model.put("unitName", tenantVO.getApt_name());
    model.put("mobile", tenantVO.getMobile());
	model.put("address", addressVO.getAddress());
	model.put("aggrementEndDate", dateUtil.getConvetedDate(tenantVO.getAgg_end_date()));
	model.put("societyName", societyVO.getSocietyName());
	model.put("societyFullName",societyVO.getSocFullName());
	model.put("regNo", societyVO.getSocietyRegNo());
	model.put("societyAddress", societyVO.getAddress());
	model.put("user", "Tenant");
	model.put("stayingComment", "been staying in the");
	model.put("vacateComment", "you vacate");
	model.put("vacateComment2", "<u>treat this communication as a notice</u> asking you <u>to vacate the flat immediately.</u>");
	model.put("vacateComment3", "accordingly.");
   
    /*  add that list to a VelocityContext  */
  
    emailVO.setMessage(templatePreparator.createMessage(model, template));
	} catch (Exception e) {
		log.error("Exception in preparing member object "+e);
	}
	
    
	return emailVO;
}

private NotificationVO processTenantSummaryEmailObject(EventVO eventVO,int tenantCount,SocietyVO societyVO){
	EmailMessage emailVO=new EmailMessage();
	NotificationVO notificationVO=new NotificationVO();
	List emailList=new ArrayList();

	
	try {
		
		EventVO summaryEvent=eventDAO.getSummaryEventVO(eventVO.getSocietyID(), eventVO.getEventID());
		
		VelocityContext velocityObj=new VelocityContext();
		velocityObj.put("tenantCount", tenantCount);
	
		 emailVO=prepareSummaryEmailObject(summaryEvent, summaryEvent.getTemplateID(),velocityObj);
		 emailVO.setDefaultSender("E-Sanchalak Services");
		 emailList.add(emailVO);
		 
		 notificationVO.setEmailList(emailList);
	     notificationVO.setSendEmail(true);
	     notificationVO.setSendSMS(false);
	     notificationVO=SendNotifications(notificationVO);
    
	} catch (Exception e) {
		log.error("Exception in preparing prepareSummaryEmailObjectt "+e);
	}
	
    
	return notificationVO;
}


private EmailMessage prepareSummaryEmailObject(EventVO eventVO,String template,VelocityContext velocityObj){
	EmailMessage emailVO=new EmailMessage();

	
	try {
	emailVO.setEmailTO(eventVO.getEmailID());
	emailVO.setSubject(eventVO.getEventSubject());
	
	
			
 
    emailVO.setMessage(templatePreparator.createMessage(velocityObj, template));
	} catch (Exception e) {
		log.error("Exception in preparing prepareSummaryEmailObjectt "+e);
	}
	
    
	return emailVO;
}






private NotificationVO SendNotifications(NotificationVO notificationVO){
	
	try {
	log.debug("Entry : private NotificationVO SendNotifications");
	ConfigManager c=new ConfigManager();
	
	
	MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
    Map map = new HashMap<String, String>();
    map.put("Content-Type", "application/json");

    headers.setAll(map);
    
     HttpEntity<?> requestEmail = new HttpEntity<>(notificationVO, headers);
    String urlForEmail =c.getPropertiesValue("finbot.sendNotification")+ "sendNotification/";

    ResponseEntity<NotificationVO> respEmail = new RestTemplate().postForEntity(urlForEmail, requestEmail, NotificationVO.class);
    HttpStatus status = respEmail.getStatusCode();
    
    notificationVO=respEmail.getBody();
    notificationVO.setStatusCode(status.value());
    notificationVO.setEmailList(null);
    notificationVO.setSmsList(null);
    
	} catch (Exception e) {
		log.error("Exception occurred in NotificationVO SendNotifications "+e);
	}
	log.debug("Exit : private NotificationVO SendNotifications");
	
	
return notificationVO;
}

public NotificationVO sendEmailandPushNotificationsToAGroup(EmailMessage emailVO){
	
	NotificationVO notificationVO=new NotificationVO();
	log.debug("Entry : public NotificationVO sendEmailandPushNotificationsToAGroup(EmailMessage emailVO)");
	
	try {
		List emailList=new ArrayList();
		List pushNotifications=new ArrayList();
		notificationVO.setOrgID(emailVO.getOrgID());
		notificationVO.setAppID(emailVO.getAppID()+"");
		notificationVO.setProfileType(emailVO.getProfileType());
		notificationVO.setGroupName(emailVO.getGroupName());
		
		
		
		List userList=notificationService.getProfileDetailedList(notificationVO);
		
		for(int i=0;userList.size()>i;i++){
			UserVO userVO= (UserVO) userList.get(i);
			if((userVO.getUserID()!=null)&&(userVO.getUserID().length()>0)){
			EmailMessage emailObj=new EmailMessage();
			emailObj.setMessage(emailVO.getMessage());
			emailObj.setEmailTO(userVO.getUserID());
			emailObj.setDefaultSender(emailVO.getDefaultSender());
			if(emailVO.getEmailCc()!=null)
			emailObj.setEmailCc(emailVO.getEmailCc());
			emailObj.setSubject(emailVO.getSubject());
			emailList.add(emailObj);
			if(emailVO.isSendPushNotifications()){
			
			if(userVO.getAndriodDeviceID()!=null){
				PushNotificationVO pushVO=new PushNotificationVO();
				pushVO.setTo(userVO.getAndriodDeviceID());
				pushVO.setTitle("");
				pushVO.setBody(emailVO.getSubject());
				pushNotifications.add(pushVO);
			}
				
			}
			
			}
		}
		
	 if(emailList.size()>0){
		 notificationVO.setEmailList(emailList);
	 }
	 if(pushNotifications.size()>0){
		 notificationVO.setPushNotificationList(pushNotifications);
	 }
		
		if(notificationVO.getEmailList().size()>0 && emailVO.getMessage().length()>0 ){
			log.info("Sender UserID: "+emailVO.getUserID()+", OrgID: "+emailVO.getOrgID()+", Group: "+emailVO.getGroupName()+", Email List size: "+notificationVO.getEmailList().size()+", Subject: "+emailVO.getSubject());
			notificationVO.setSendEmail(true);
			
			notificationVO.setSendSMS(false);
			if((notificationVO.getPushNotificationList()!=null)&&(notificationVO.getPushNotificationList().size()>0)){
			
			notificationVO.setSendPushNotifications(true);
			}
			notificationVO.setOrgID(emailVO.getOrgID());
			notificationVO.setAppID(emailVO.getAppID()+"");
		    notificationVO=notificationService.SendNotifications(notificationVO);
			log.info("Email has been sent successfully ");
			notificationVO.setStatusCode(200);
		}else{
			log.info("Unable to send Emails ");			
			notificationVO.setStatusCode(404);
		}
		
		
		
		
		
	 }catch(Exception e) {
	    	log.error("Exception in public NotificationVO sendEmailandPushNotificationsToAGroup(EmailMessage emailVO) "+e);
	 }   
		
		
	log.debug("Exit : public NotificationVO sendEmailandPushNotificationsToAGroup(EmailMessage emailVO)");
	return notificationVO;
}

public NotificationVO sendGroupSMSNotificationsToAGroup(SMSMessage smsVO){
	
	NotificationVO notificationVO=new NotificationVO();
	log.debug("Entry : public NotificationVO sendGroupSMSNotificationsToAGroup(EmailMessage emailVO)");
	
	try {
		notificationVO.setOrgID(smsVO.getOrgID());
		notificationVO.setAppID(smsVO.getAppID()+"");
		notificationVO.setProfileType(smsVO.getProfileType());
		notificationVO.setGroupName(smsVO.getGroupName());
		notificationVO.setSendSMS(smsVO.isSendSMS());
	
			
			List userList=notificationService.getProfileDetailedList(notificationVO);
			List smsList=new ArrayList();
			List pushNotifications=new ArrayList();
			
			for(int i=0;userList.size()>i;i++){
				UserVO userVO= (UserVO) userList.get(i);
				if((smsVO.isSendSMS())&&(userVO.getMobile()!=null)&&(!userVO.getMobile().equalsIgnoreCase("0000000000"))){
				SMSMessage smsObjVO=new SMSMessage();
				smsObjVO.setMessage(smsVO.getMessage());
				smsObjVO.setMobileNumber(userVO.getMobile());
				smsList.add(smsObjVO);
				}
				if(smsVO.isSendPushNotifications()){
			
				if(userVO.getAndriodDeviceID()!=null){
					PushNotificationVO pushVO=new PushNotificationVO();
					pushVO.setTo(userVO.getAndriodDeviceID());
					pushVO.setTitle("");
					pushVO.setBody(smsVO.getMessage());
					pushNotifications.add(pushVO);
				}
					
				}
				
				
			}
			notificationVO.setPushNotificationList(pushNotifications);
			notificationVO.setSmsList(smsList);
			
		
		log.info("Sender UserID: "+smsVO.getUserID()+", OrgID: "+smsVO.getOrgID()+", Group: "+smsVO.getGroupName()+", SMS List size: "+notificationVO.getSmsList().size()+", Message: "+smsVO.getMessage());
		SMSGatewayVO smsGatewayVO=notificationService.getSMSGatewayDetails(notificationVO.getOrgID());
		if(smsVO.isSendSMS()){ // For sending SMS with/ without notifications
		if(smsGatewayVO.getAvailableSMSCredits()>=notificationVO.getSmsList().size()){
				
		if((notificationVO.getSmsList()!=null)&&(notificationVO.getSmsList().size()>0) && (smsVO.getMessage().length()>0) ){
			
			
			notificationVO.setSendSMS(true);
			
			notificationVO.setSendEmail(false);
			if(notificationVO.getPushNotificationList().size()>0){
			
			notificationVO.setSendPushNotifications(true);
			}
			notificationVO.setOrgID(smsVO.getOrgID());
			notificationVO.setAppID(smsVO.getAppID()+"");
			SMSGatewayVO smsGatwVO=new SMSGatewayVO();
			smsGatwVO.setAvailableSMSCredits(notificationVO.getSmsList().size());
			smsGatwVO.setOrgID(notificationVO.getOrgID());
			int deductSMScredits=notificationService.deductSMSCredit(smsGatwVO);
			notificationVO=notificationService.SendNotifications(notificationVO);
			
		
			log.info("SMS has been sent successfully ");
			notificationVO.setStatusCode(200);
		}else{
			log.info("Unable to send SMS ");			
			notificationVO.setStatusCode(404);
		}
		} else{
			notificationVO.setStatusCode(542);
			notificationVO.setDescription("Available SMS Credits = "+smsGatewayVO.getAvailableSMSCredits()+" No of SMS tried to Send "+notificationVO.getSmsList().size());
		}
		}else if((smsVO.isSendPushNotifications()&&(!smsVO.isSendSMS()))){ //For sending only push notifications without SMS
			
			if(notificationVO.getPushNotificationList().size()>0){
				
				notificationVO.setSendPushNotifications(true);
				}
			notificationVO.setOrgID(smsVO.getOrgID());
			notificationVO.setAppID(smsVO.getAppID()+"");
			notificationVO=notificationService.SendNotifications(notificationVO);
			log.info("Push Notifications  have been sent successfully ");
			notificationVO.setStatusCode(200);
		}
	 }catch(Exception e) {
	    	log.error("Exception in public NotificationVO sendGroupSMSNotificationsToAGroup(EmailMessage emailVO) "+e);
	 }   
		
		
	log.debug("Exit : public NotificationVO sendGroupSMSNotificationsToAGroup(EmailMessage emailVO)");
	return notificationVO;
} 


public List getNotificationsListOfPaymentReminders(int days)  {
	
	log.debug("Entry : public List getNotificationsListOfPaymentReminders()");
	
	List eventList = null;
	try {
	
		eventList = eventDAO.getNotificationsListOfPaymentReminders(days);
		log.debug("in function getEvent()");
		
		if (eventList.size() != 0) {
			log.debug("size Of the List" + eventList.size());
			
		}
		
		log.debug("Exit : public List getNotificationsListOfPaymentReminders");	
	
	} catch (Exception e) {
			
			log.error("DataAccessException :"+e);
		}
	return eventList;
}


/**
 * @param eventDAO the eventDAO to set
 */
public void setEventDAO(EventDAO eventDAO) {
	this.eventDAO = eventDAO;
}



/**
 * @param societyServiceBean the societyServiceBean to set
 */
public void setSocietyServiceBean(SocietyService societyServiceBean) {
	this.societyServiceBean = societyServiceBean;
}



/**
 * @param memberServiceBean the memberServiceBean to set
 */
public void setMemberServiceBean(MemberService memberServiceBean) {
	this.memberServiceBean = memberServiceBean;
}



/**
 * @param addressServiceBean the addressServiceBean to set
 */
public void setAddressServiceBean(AddressService addressServiceBean) {
	this.addressServiceBean = addressServiceBean;
}

/**
 * @param notificationService the notificationService to set
 */
public void setNotificationService(NotificationService notificationService) {
	this.notificationService = notificationService;
}

/**
 * @param appDetailsService the appDetailsService to set
 */
public void setAppDetailsService(AppDetailsService appDetailsService) {
	this.appDetailsService = appDetailsService;
}

}
