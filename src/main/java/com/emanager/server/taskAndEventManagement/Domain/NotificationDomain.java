package com.emanager.server.taskAndEventManagement.Domain;

import java.io.FileReader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.Date;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.olap4j.impl.IdentifierParser.MemberListBuilder;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.LedgerTrnsVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.adminReports.Services.NominationRegService;
import com.emanager.server.adminReports.valueObject.RptTenantVO;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.commonUtils.domainObject.HtmlTableGenerator;
import com.emanager.server.commonUtils.domainObject.VMTemplatePreparator;
import com.emanager.server.dashBoard.Services.DashBoardService;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardAccountVO;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.invoice.Services.InvoiceService;
import com.emanager.server.invoice.dataAccessObject.BillDetailsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceBillingCycleVO;
import com.emanager.server.invoice.dataAccessObject.ProfileDetailsVO;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.BankInfoVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.taskAndEventManagement.DAO.NotificationDAO;
import com.emanager.server.taskAndEventManagement.Service.EventService;
import com.emanager.server.taskAndEventManagement.valueObject.EmailMessage;
import com.emanager.server.taskAndEventManagement.valueObject.EventVO;
import com.emanager.server.taskAndEventManagement.valueObject.NotificationVO;
import com.emanager.server.taskAndEventManagement.valueObject.PushNotificationVO;
import com.emanager.server.taskAndEventManagement.valueObject.SMSGatewayVO;
import com.emanager.server.taskAndEventManagement.valueObject.SMSMessage;
import com.emanager.server.userManagement.service.UserService;
import com.emanager.server.userManagement.valueObject.UserVO;



public class NotificationDomain {
	
	
	static Logger log = Logger.getLogger(NotificationDomain.class.getName());
	MemberService memberServiceBean;
	InvoiceService invoiceServiceBean;
	LedgerService ledgerService;
	SocietyService societyServiceBean;
	NominationRegService nominationRegService;
	DashBoardService dashBoardService;
	ReportService reportServiceBean;
	UserService userService;
	NotificationDAO notificationDAO;
	EventService eventService;
	HtmlTableGenerator htmlTable=new HtmlTableGenerator();
	DateUtility dateUtil=new DateUtility();
	VMTemplatePreparator templatePreparator=new VMTemplatePreparator();

	
	//=========Maintenance Payment Reminder =============//
	
public NotificationVO getMemberBalance(EventVO eventVO)  {
		
		log.debug("Entry : public NotificationVO getMemberBalance(EmailMessage emailVO) ");
		NotificationVO notificationVO=new NotificationVO();
		
		try {
			MemberVO memberVO=memberServiceBean.searchMembersInfo(eventVO.getMemberID()+"");
			
			
			
			
			
			
			
			SocietyVO societyVO=societyServiceBean.getSocietyDetails(memberVO.getSocietyID());
			
			notificationVO=prepareMemberBalance(eventVO,societyVO);
			
			notificationVO=SendNotifications(notificationVO);
						
			
			log.debug("Exit : public NotificationVO getMemberBalance(EventVO eventVO)");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException : public NotificationVO getMemberBalance(EventVO eventVO)"+e);
		}
		 catch (Exception e) {
				
				log.error("Exception : public NotificationVO getMemberBalance(EventVO eventVO)"+e);
			}
		return notificationVO;
	}

public NotificationVO getMemberBalanceForSociety(EventVO eventVO)  {
	
	log.debug("Entry : public NotificationVO getMemberBalance(int memberID) ");
	NotificationVO notificationVO=new NotificationVO();
	ConfigManager config=new ConfigManager();
	
	try {
	
		List memberList=memberServiceBean.getActivePrimaryMemberList(eventVO.getSocietyID());
		List billingCycleList=invoiceServiceBean.getBillingCycleList(eventVO.getSocietyID());
		SocietyVO societyVO=societyServiceBean.getSocietyDetails(eventVO.getSocietyID());
		
		List emailList=new ArrayList<EmailMessage>();
		List smsList=new ArrayList<SMSMessage>();
		List pushNotificationList=new ArrayList<PushNotificationVO>();
		
		if(billingCycleList.size()>0){
			InvoiceBillingCycleVO billingCycleVO=(InvoiceBillingCycleVO) billingCycleList.get(billingCycleList.size()-1);
		for(int i=0;memberList.size()>i;i++){
		MemberVO memberVO=(MemberVO) memberList.get(i);	
		BillDetailsVO billVO=invoiceServiceBean.getBillsDetailsSociety(memberVO.getSocietyID(),billingCycleVO.getFromDate() , billingCycleVO.getUptoDate(), memberVO.getAptID());
		
		billVO.setDueDate(billingCycleVO.getDueDate());
		
	
		
		
		if(memberVO.getEmail().length()>0){
			EmailMessage emailVO=new EmailMessage();
			if(billVO.getClosingBalance().compareTo(BigDecimal.ZERO)>0){
				emailVO=prepareReminderEmailObject(memberVO, billVO, eventVO.getTemplateID(),billingCycleVO);
				if((eventVO.getCcEmail()!=null)&&(eventVO.getCcEmail().length()>0)){
					emailVO.setEmailCc(eventVO.getCcEmail());
				}
			
			
			}else if((societyVO.getReminderToAll()==1)&&(eventVO.getOnetime()==1)){
				if(societyVO.getSocietyID()==16){
					emailVO=prepareReminderEmailObject(memberVO, billVO, config.getPropertiesValue("email.negativeTemplateIrene"),billingCycleVO);
				}else
				emailVO=prepareReminderEmailObject(memberVO, billVO, config.getPropertiesValue("email.negativeTemplate"),billingCycleVO);
			}
		emailList.add(emailVO);
		}
		
		SMSMessage smsVO=new SMSMessage();
		if((memberVO.getMobile().length()>0)&&(!memberVO.getMobile().equalsIgnoreCase("0000000000"))){
			if((billVO.getClosingBalance().compareTo(BigDecimal.ZERO)>0)){
			smsVO=prepareReminderSMSObject(memberVO, billVO, eventVO,billingCycleVO);
			}else if((societyVO.getReminderToAll()==1)&&(eventVO.getOnetime()==1)){
				smsVO=prepareReminderSMSObject(memberVO, billVO, eventVO,billingCycleVO);
			}
		smsList.add(smsVO);
		
			
			
		}
		
		if(billVO.getClosingBalance().compareTo(BigDecimal.ZERO)>0){
			
	
				PushNotificationVO pnVO=prepareReminderPushNotificationObject(memberVO, billVO, eventVO.getOnetime());
				if((pnVO.getTo()!=null)&&(pnVO.getTo().length()>0)){
					pushNotificationList.add(pnVO);
				}
				
				}
		
			
		}
		
		if(emailList.size()>0){
			notificationVO.setEmailList(emailList);
			notificationVO.setSendEmail(true);
		}
		if(smsList.size()>0){
			notificationVO.setSmsList(smsList);
			notificationVO.setSendSMS(true);
		}
		if(pushNotificationList.size()>0){
			notificationVO.setPushNotificationList(pushNotificationList); 
			notificationVO.setSendPushNotifications(true);
		}
		notificationVO.setAppID("1");
		notificationVO.setOrgID(eventVO.getSocietyID());
		notificationVO=SendNotifications(notificationVO);
		notificationVO.setOrgID(eventVO.getSocietyID());
		
		NotificationVO summaryNotification=new NotificationVO();
		summaryNotification.setOrgID(eventVO.getSocietyID());
		summaryNotification.setSentEmailsCount(notificationVO.getSentEmailsCount());
		summaryNotification.setFailedEmailsCount(notificationVO.getFailedEmailsCount());
		List emailStatusList=new ArrayList<EmailMessage>();
		
		EmailMessage emaiStatuslVO=preparePaymentReminderStatusEmailObject(summaryNotification);
		
		emailStatusList.add(emaiStatuslVO);
		
		summaryNotification.setEmailList(emailStatusList);
		summaryNotification.setSendEmail(true);
		summaryNotification.setSendSMS(false);
		summaryNotification.setSendPushNotifications(false);
		if(eventVO.getOnetime()!=1)
		summaryNotification=SendNotifications(summaryNotification);
		
		}
		
		
		log.debug("Exit : public NotificationVO getMemberBalance(int memberID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public NotificationVO getMemberBalance(int memberID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public NotificationVO getMemberBalance(int memberID)"+e);
		}
	return notificationVO;
}


//=========Bill Payment Reminder for customers =============//

public NotificationVO getCustomerBalance(EventVO eventVO)  {
	
	log.debug("Entry : public NotificationVO getCustomerBalance(EventVO eventVO) ");
	NotificationVO notificationVO=new NotificationVO();
	
	try {
		
		SocietyVO societyVO=societyServiceBean.getSocietyDetails(eventVO.getSocietyID());
		
		notificationVO=prepareCustomerBalance(eventVO, societyVO);
		notificationVO.setOrgID(eventVO.getSocietyID());
		notificationVO=SendNotifications(notificationVO);
		
		
		
		log.debug("Exit : public NotificationVO getCustomerBalance(EventVO eventVO)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public NotificationVO getCustomerBalance(EventVO eventVO)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public NotificationVO getCustomerBalance(EventVO eventVO)"+e);
		}
	return notificationVO;
}
		

public NotificationVO getCustomerBalanceForOrg(int orgID)  {
	
	log.debug("Entry : public NotificationVO getCustomerBalanceForOrg(int orgID)   ");
	NotificationVO notificationVO=new NotificationVO();
	
	try {
	
		List customerList=ledgerService.getLedgerProfileList(orgID, "C");
	
		SocietyVO societyVO=societyServiceBean.getSocietyDetails(orgID);
		String fromDate=	dateUtil.getFirstDateOfFY(dateUtil.findCurrentDate().toString());
		String toDate=dateUtil.getLastDateOfFY(dateUtil.findCurrentDate().toString());
		
		List emailList=new ArrayList<EmailMessage>();
		List smsList=new ArrayList<SMSMessage>();
		
	
		for(int i=0;customerList.size()>i;i++){
		AccountHeadVO acVO=(AccountHeadVO) customerList.get(i);	
		ProfileDetailsVO customerVO=acVO.getProfileVO();
		if(customerVO.getOrgID()>0){
		AccountHeadVO achVO=ledgerService.getOpeningClosingBalance(customerVO.getOrgID(), customerVO.getProfileLedgerID(), fromDate, toDate, "L");
		
	
		
			if(customerVO.getThresholdBalance().compareTo(achVO.getClosingBalance())<0){
				if((customerVO.getProfileEmailID()!=null)&&(customerVO.getProfileEmailID().length()>0)){
			//	EmailMessage emailVO=prepareBillReminderEmailObjectForCustomer(customerVO, achVO, "customerBillReminder.vm");
			//	emailList.add(emailVO);
				}
				if((customerVO.getProfileMobile().length()>0)&&(!customerVO.getProfileMobile().equalsIgnoreCase("0000000000"))){
				SMSMessage smsVO =prepareBillReminderSMSObject(achVO.getClosingBalance(), customerVO.getProfileMobile());
				smsList.add(smsVO);
				}
		}
	
		}
		}
		
		if(emailList.size()>0){
			notificationVO.setEmailList(emailList);
			notificationVO.setSendEmail(true);
		}
		if(smsList.size()>0){
			notificationVO.setSmsList(smsList);
			notificationVO.setSendSMS(true);
		}
		notificationVO.setOrgID(orgID);
		notificationVO=SendNotifications(notificationVO);
			
		
		
		
		log.debug("Exit : public NotificationVO getCustomerBalanceForOrg(int orgID)  ");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public NotificationVO getCustomerBalanceForOrg(int orgID)  "+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public NotificationVO getCustomerBalanceForOrg(int orgID)  "+e);
		}
	return notificationVO;
}
	
	private NotificationVO prepareCustomerBalance(EventVO eventVO,SocietyVO societyVO){
		log.debug("Entry : private NotificationVO prepareCustomerBalance(ProfileDetailsVO customerVO,SocietyVO societyVO) ");
		NotificationVO notificationVO=new NotificationVO();
		List emailList=new ArrayList<EmailMessage>();
		List smsList=new ArrayList<SMSMessage>();
		try {
			Date currentDate=dateUtil.findCurrentDate();
		String fromDate=dateUtil.getFirstDateOfFY(currentDate.toString());
		String toDate=dateUtil.getLastDateOfFY(currentDate.toString());
		
		ProfileDetailsVO customerVO=ledgerService.getLedgerProfile(eventVO.getMemberID(),eventVO.getSocietyID());		
				
		AccountHeadVO achVO=ledgerService.getOpeningClosingBalance(customerVO.getOrgID(), customerVO.getProfileLedgerID(), fromDate, toDate, "L");
		
	
		if((eventVO.getEmailID()!=null)&&(eventVO.getEmailID().length()>0)){
			eventVO.setCreaterName(customerVO.getProfileName());
		EmailMessage emailVO=prepareBillReminderEmailObjectForCustomer(eventVO, achVO, "customerBillReminder.vm");
		emailList.add(emailVO);
		}
		if((customerVO.getProfileMobile()!=null)&&(customerVO.getProfileMobile().length()>0)&&(!customerVO.getProfileMobile().equalsIgnoreCase("0000000000"))){
		SMSMessage smsVO =prepareBillReminderSMSObject(achVO.getClosingBalance(), customerVO.getProfileMobile());
		smsList.add(smsVO);
		}
		
		
		
		notificationVO.setEmailList(emailList);
		notificationVO.setSmsList(smsList);
	
			notificationVO.setSendEmail(true);
			notificationVO.setSendSMS(true);
		
		
			
			
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		log.debug("Entry : private NotificationVO prepareCustomerBalance(ProfileDetailsVO customerVO,SocietyVO societyVO) ");
		return notificationVO;
	}

	
	


private EmailMessage preparePaymentReminderStatusEmailObject(NotificationVO notificationVO){
	EmailMessage emailVO=new EmailMessage();
	BigDecimal totalAmount=BigDecimal.ZERO;
	java.util.Date today = new java.util.Date();
	
	SocietyVO societyVO=societyServiceBean.getSocietyDetails(notificationVO.getOrgID());
	emailVO.setDefaultSender("E-Sanchalak Services");
	emailVO.setEmailTO(societyVO.getMailListA());
	emailVO.setSubject("Payment Reminder Summary Report ");
	try {
			
		
		
	
  
    VelocityContext model = new VelocityContext();
    model.put("totalMembers", notificationVO.getSentEmailsCount()+notificationVO.getFailedEmailsCount());
       

   
    /*  add that list to a VelocityContext  */
  
    emailVO.setMessage(templatePreparator.createMessage(model, "SummaryMail.vm"));
	} catch (Exception e) {
		log.error("Exception in preparing member object "+e);
	}
	
    
	return emailVO;
}



private NotificationVO prepareMemberBalance(EventVO eventVO,SocietyVO societyVO){
	log.debug("Entry : private NotificationVO prepareMemberBalance(int memberID) ");
	NotificationVO notificationVO=new NotificationVO();
	List emailList=new ArrayList<EmailMessage>();
	List smsList=new ArrayList<SMSMessage>();
	ConfigManager config=new ConfigManager();
	String templateName="";
	try {
	notificationVO.setOrgID(societyVO.getSocietyID());
	MemberVO memberVO=memberServiceBean.searchMembersInfo(eventVO.getMemberID()+"");
	List memberList=memberServiceBean.getActivePrimaryMemberList(eventVO.getSocietyID());
	 for(int i=0;memberList.size()>i;i++){
		 MemberVO temp=(MemberVO) memberList.get(i);
				 if(eventVO.getMemberID()==temp.getMemberID()){
					 memberVO.setLedgerID(temp.getLedgerID());
					 continue;
				 }
		 
	 }
	List evList=eventService.getPaymentReminderEventDetails(societyVO.getSocietyID());
	
	if(evList.size()>0){
		EventVO eve=(EventVO) evList.get(0);
		templateName=eve.getTemplateID();
	}else
		templateName="balance.vm";
	
	List billingCycleList=invoiceServiceBean.getBillingCycleList(memberVO.getSocietyID());
	
	if(billingCycleList.size()>0){
	
	InvoiceBillingCycleVO billingCycleVO=(InvoiceBillingCycleVO) billingCycleList.get(billingCycleList.size()-1);
	
	BillDetailsVO billVO=invoiceServiceBean.getBillsDetailsSociety(memberVO.getSocietyID(),billingCycleVO.getFromDate() , billingCycleVO.getUptoDate(), memberVO.getAptID());
	
	billVO.setDueDate(billingCycleVO.getDueDate());
	
	memberVO.setEmail(eventVO.getEmailID());
	memberVO.setOldEmailID(eventVO.getCcEmail());
	EmailMessage emailVO=new EmailMessage();
	if(billVO.getClosingBalance().compareTo(BigDecimal.ZERO)>0){
		emailVO=prepareReminderEmailObject(memberVO, billVO, templateName,billingCycleVO);
		if((eventVO.getCcEmail()!=null)&&(eventVO.getCcEmail().length()>0)){
			emailVO.setEmailCc(eventVO.getCcEmail());
		}
	
	
	}else if((societyVO.getReminderToAll()==1)){
		if(societyVO.getSocietyID()==16){
			emailVO=prepareReminderEmailObject(memberVO, billVO, config.getPropertiesValue("email.negativeTemplateIrene"),billingCycleVO);
		}else
		emailVO=prepareReminderEmailObject(memberVO, billVO, config.getPropertiesValue("email.negativeTemplate"),billingCycleVO);
	}
			prepareReminderEmailObject(memberVO, billVO, templateName,billingCycleVO);
	if((eventVO.getCcEmail()!=null)&&(eventVO.getCcEmail().length()>0)){
		emailVO.setEmailCc(eventVO.getCcEmail());
	}
	SMSMessage smsVO=prepareReminderSMSObject(memberVO, billVO, eventVO,billingCycleVO);
	
	emailList.add(emailVO);
	smsList.add(smsVO);
	
	notificationVO.setEmailList(emailList);
	notificationVO.setSmsList(smsList);
	if(societyVO.getReminderToAll()==0){
	if(billVO.getClosingBalance().intValue()>0){
	notificationVO.setSendEmail(true);
	notificationVO.setSendSMS(true);
	}
	}else{
		notificationVO.setSendEmail(true);
		notificationVO.setSendSMS(true);
	}
	
		
		
	}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	log.debug("Entry : private NotificationVO prepareMemberBalance(int memberID) ");
	return notificationVO;
}

	
private EmailMessage prepareReminderEmailObject(MemberVO memberVO,BillDetailsVO billVO,String template,InvoiceBillingCycleVO billingVO){
	EmailMessage emailVO=new EmailMessage();
	BigDecimal totalAmount=BigDecimal.ZERO;
	EncryptDecryptUtility endeUtil=new EncryptDecryptUtility();
	java.util.Date today = new java.util.Date();
	SimpleDateFormat newFormat=new SimpleDateFormat("dd-MMM-yy");
    String oneDayBeforeFromDate=dateUtil.getPrevOrNextDate(billVO.getFromDate(), 1);
    BankInfoVO bankInfoVO=societyServiceBean.getBankDetails(memberVO.getSocietyID());
    ProfileDetailsVO profileVO=ledgerService.getLedgerProfile(memberVO.getLedgerID(),memberVO.getSocietyID());
    if((profileVO.getVirtualAccNo()!=null)&&(profileVO.getVirtualAccNo().length()>0)){
		bankInfoVO.setAccountNo(profileVO.getVirtualAccNo());
	}
	String address="";
	
	emailVO.setDefaultSender(bankInfoVO.getSocietyName());
	emailVO.setEmailTO(memberVO.getEmail());
	
	emailVO.setSubject("[Accounts team] Society Maintenance bill of  "+memberVO.getFlatNo()+" for "+dateUtil.getConvetedDate(billVO.getFromDate())+" to "+dateUtil.getConvetedDate(billVO.getUptoDate()));
	try {
		
		String seckretKey=(endeUtil.encrypt(memberVO.getSocietyID()+","+memberVO.getLedgerID()+","+memberVO.getAptID())+",M");
		if(seckretKey.contains("/")){
			seckretKey=seckretKey.replaceAll("/", "_");
			}
		String url=URLEncoder.encode(seckretKey, "UTF-8");
		
		url="https://www.esanchalak.com/member/#/payment/"+url;
	
		
	
  
    VelocityContext model = new VelocityContext();
    model.put("fullName", memberVO.getFullName());
    model.put("aptName", memberVO.getFlatNo());
    model.put("currentCharges", htmlTable.getHTMLTableForBills(billVO.getObjectList()));
	model.put("openingBalance", billVO.getOpeningBalance());
	model.put("closingBalance", billVO.getClosingBalance());
	model.put("charegedAmount", billVO.getChargedAmount());
	model.put("due", billVO.getClosingBalance());
	model.put("paidAmount", billVO.getPaidAmount());
	model.put("beforeDate",dateUtil.getConvetedDate(oneDayBeforeFromDate));
	model.put("fromDate", dateUtil.getConvetedDate(billVO.getFromDate()));
	model.put("uptoDate", dateUtil.getConvetedDate(billVO.getUptoDate()));
	model.put("currDate", newFormat.format(today));
	model.put("dueDate", dateUtil.getConvetedDate(billVO.getDueDate()));
	model.put("societyName", bankInfoVO.getSocietyName());
	model.put("bankName", bankInfoVO.getBankName());
	model.put("branchCode", bankInfoVO.getBranchCode());
	model.put("accNo", bankInfoVO.getAccountNo());
	model.put("accType", bankInfoVO.getAccType());
	model.put("ifsc", bankInfoVO.getIfscNo());
	model.put("micr", bankInfoVO.getMicrNo());
	model.put("favour", bankInfoVO.getFavour());
    model.put("secretKey",url);
    model.put("url", "https://www.esanchalak.com/member#/bill-download/"+bankInfoVO.getSocietyId()+"/"+billingVO.getBillingCycleID()+"/"+memberVO.getAptID()+"/"+billingVO.getFromDate()+"/"+billingVO.getUptoDate()+"/"+billingVO.getDueDate());
    if((billVO.getLedgerDescription()!=null)&&(billVO.getLedgerDescription().length()>0)){
    model.put("accountNotes", billVO.getLedgerDescription());
    }else
    model.put("accountNotes", "0.00");

   
    /*  add that list to a VelocityContext  */
  
    emailVO.setMessage(templatePreparator.createMessage(model, template));
	} catch (Exception e) {
		log.error("Exception in preparing member object "+e);
	}
	
    
	return emailVO;
}
	
	private SMSMessage prepareReminderSMSObject(MemberVO memberVO,BillDetailsVO billVO,EventVO eventVO,InvoiceBillingCycleVO billingCycleVO){//1-If new Invoice generated else 0 for reminder
		SMSMessage smsVO=new SMSMessage();
		try {
		log.debug("Entry : private SMSMessage prepareReminderSMSObject");
		ConfigManager config=new ConfigManager();
		String unitName=memberVO.getFlatNo();
		String totalAmt=billVO.getClosingBalance().toString();
		String dueDate=dateUtil.getConvetedDate(billVO.getDueDate());
		String societyName=billVO.getSocietyName();
		String memberName=memberVO.getFullName();
		java.util.Date today = new java.util.Date();
		SimpleDateFormat newFormat=new SimpleDateFormat("dd-MMM-yy");
		
		String smsString="";
	 	Properties properties=new Properties();
	 	if((eventVO.getSmsTemplate()==null)||(eventVO.getSmsTemplate().length()==0)||(eventVO.getSmsTemplate().equalsIgnoreCase("default"))){
	 	if(eventVO.getOnetime()==1){
	 		  smsString=config.getPropertiesValue("sms.newInvoice");
			  smsString=smsString.replace("$unitName", unitName);
			  smsString=smsString.replace("$totalAmt", totalAmt);
			  smsString=smsString.replace("$dueDate", dueDate);
			  smsString=smsString.replace("$memberName", memberName);
			  smsString=smsString.replace("$societyName", societyName);
	 	}else{
	 		  smsString =config.getPropertiesValue("sms.reminder");
	 		  smsString=smsString.replace("$unitName", unitName);
 		      smsString=smsString.replace("$totalAmt", totalAmt);
			  smsString=smsString.replace("$societyName", societyName);
			  smsString=smsString.replace("$memberName", memberName);
	 	}
	 	}else{
	 		smsString=config.getPropertiesValue("sms."+eventVO.getSmsTemplate());
	 		smsString=smsString.replace("$unitName", unitName);
			  smsString=smsString.replace("$totalAmt", totalAmt);
			  smsString=smsString.replace("$dueDate", dueDate);
			  smsString=smsString.replace("$memberName", memberName);
			  smsString=smsString.replace("$societyName", societyName);
			  smsString=smsString.replace("$billingCycle", billingCycleVO.getDescription());
			  smsString=smsString.replace("$currDate", newFormat.format(today));
	 	}
	 	
		
		smsVO.setMobileNumber(memberVO.getMobile());
		smsVO.setMessage(smsString);
		
		
		} catch (Exception e) {
			log.error("Exception occurred in private SMSMessage prepareReminderSMSObject "+e);
		}
		log.debug("Exit : private SMSMessage prepareReminderSMSObject");
		
		
		return smsVO;
	}
	
	private PushNotificationVO prepareReminderPushNotificationObject(MemberVO memberVO,BillDetailsVO billVO,int newInvoice){//1-If new Invoice generated else 0 for reminder
		PushNotificationVO pushNotificationVO=new PushNotificationVO();
		try {
		log.debug("Entry : private PushNotificationVO prepareReminderPushNotificationObject(MemberVO memberVO,BillDetailsVO billVO,int newInvoice)");
		ConfigManager config=new ConfigManager();
		UserVO deviceDetailsVO=userService.getUserDeviceDetailsFromMemberID(memberVO.getMemberID(), "1","M");
		pushNotificationVO.setTo("");
		if((deviceDetailsVO!=null)&&(deviceDetailsVO.getAndriodDeviceID()!=null)){
		String unitName=memberVO.getFlatNo();
		String totalAmt=billVO.getClosingBalance().toString();
		String dueDate=dateUtil.getConvetedDate(billVO.getDueDate());
		String societyName=billVO.getSocietyName();
		String memberName=memberVO.getFullName();
		
		String smsString="";
		String title="";
	 	Properties properties=new Properties();
	 	if(newInvoice==1){
	 		  smsString=config.getPropertiesValue("sms.newInvoice");
	 		  title="New Invoice generated";
			  smsString=smsString.replace("$unitName", unitName);
			  smsString=smsString.replace("$totalAmt", totalAmt);
			  smsString=smsString.replace("$dueDate", dueDate);
			  smsString=smsString.replace("$memberName", memberName); 
			  smsString=smsString.replace("$societyName", societyName);
	 	}else{
	 		  title="Society Maintenance bill for "+unitName;
	 		  smsString =config.getPropertiesValue("sms.reminder");
	 		  smsString=smsString.replace("$unitName", unitName);
 		      smsString=smsString.replace("$totalAmt", totalAmt);
			  smsString=smsString.replace("$societyName", societyName);
			  smsString=smsString.replace("$memberName", memberName);
	 	}
	 	
		
		pushNotificationVO.setTo(deviceDetailsVO.getAndriodDeviceID());
		pushNotificationVO.setTitle(title);
		pushNotificationVO.setBody(smsString);
		
		}
		} catch (Exception e) {
			log.error("Exception occurred in private PushNotificationVO prepareReminderPushNotificationObject(MemberVO memberVO,BillDetailsVO billVO,int newInvoice) "+e);
		}
		log.debug("Exit : private PushNotificationVO prepareReminderPushNotificationObject(MemberVO memberVO,BillDetailsVO billVO,int newInvoice)");
		
		
		return pushNotificationVO;
	}
	
	private EmailMessage prepareBillReminderEmailObjectForCustomer(EventVO eventVO,AccountHeadVO billVO,String template){
		EmailMessage emailVO=new EmailMessage();
		BigDecimal totalAmount=BigDecimal.ZERO;
		java.util.Date today = new java.util.Date();
		SimpleDateFormat newFormat=new SimpleDateFormat("dd-MMM-yy");
	     BankInfoVO bankInfoVO=societyServiceBean.getBankDetails(billVO.getOrgID());
		String address="";
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
		symbols.setGroupingSeparator(',');

		DecimalFormat formatter = new DecimalFormat("#,##,###.00", symbols);
		
		emailVO.setDefaultSender(bankInfoVO.getSocietyName());
		emailVO.setEmailTO(eventVO.getEmailID());
		emailVO.setEmailCc(eventVO.getCcEmail());
		emailVO.setSubject(eventVO.getEventSubject());
		try {
					
    
      VelocityContext model = new VelocityContext();
      model.put("fullName", eventVO.getCreaterName());
      model.put("htmlBody", eventVO.getMessage());
    	model.put("closingBalance", formatter.format(billVO.getClosingBalance().longValue()));
		
		model.put("due", billVO.getClosingBalance());
		model.put("currDate", newFormat.format(today));
		model.put("societyName", bankInfoVO.getSocietyName());
		model.put("bankName", bankInfoVO.getBankName());
		model.put("branchCode", bankInfoVO.getBranchCode());
		model.put("accNo", bankInfoVO.getAccountNo());
		model.put("accType", bankInfoVO.getAccType());
		model.put("ifsc", bankInfoVO.getIfscNo());
		model.put("micr", bankInfoVO.getMicrNo());
		model.put("favour", bankInfoVO.getFavour());
      

     
      /*  add that list to a VelocityContext  */
    
      emailVO.setMessage(templatePreparator.createMessage(model, template));
		} catch (Exception e) {
			log.error("Exception in preparing member object "+e);
		}
		
      
		return emailVO;
	}
	
	private SMSMessage prepareBillReminderSMSObject(BigDecimal closingBalance,String mobileNO){
		SMSMessage smsVO=new SMSMessage();
		try {
		log.debug("Entry : private SMSMessage prepareBillReminderSMSObject(BigDecimal closingBalance,String mobileNO)");
		ConfigManager config=new ConfigManager();
	
		String smsString="";
	 	Properties properties=new Properties();
	 	SimpleDateFormat newFormat=new SimpleDateFormat("dd-MMM-yy");
	    String address="";
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
		symbols.setGroupingSeparator(',');

		DecimalFormat formatter = new DecimalFormat("#,##,###.00", symbols);
	 		  smsString =config.getPropertiesValue("sms.customerReminder");
	 		  smsString=smsString.replace("$totalAmt", formatter.format(closingBalance.longValue()));
				 	
	 	
		
		smsVO.setMobileNumber(mobileNO);
		smsVO.setMessage(smsString);
		
		
		} catch (Exception e) {
			log.error("Exception occurred in private SMSMessage prepareBillReminderSMSObject(BigDecimal closingBalance,String mobileNO) "+e);
		}
		log.debug("Exit : private SMSMessage prepareBillReminderSMSObject(BigDecimal closingBalance,String mobileNO)");
		
		
		return smsVO;
	}
	
	
	//=====================Nomination Document Reminder ====================//
	
public NotificationVO getNominationStatusForMember(int memberID)  {
		
		log.debug("Entry : public NotificationVO getNominationStatusForMember(int memberID) ");
		NotificationVO notificationVO=new NotificationVO();
		try {
		
			
			notificationVO=prepareNominationReminder(memberID);

			notificationVO=SendNotifications(notificationVO);
			
				
			
			
			log.debug("Exit : public NotificationVO getNominationStatusForMember(int memberID)");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException : public NotificationVO getNominationStatusForMember(int memberID)"+e);
		}
		 catch (Exception e) {
				
				log.error("Exception : public NotificationVO getNominationStatusForMember(int memberID) for member ID "+memberID+"-- "+e);
			}
		return notificationVO;
	}


	public NotificationVO sendNominationReminderToSociety(EventVO eventVO)  {
	
	log.debug("Entry : public NotificationVO sendNominationReminderToSociety(int societyID) ");
	NotificationVO notificationVO=new NotificationVO();
	try {
	
		
		List memberList=nominationRegService.getNominationMissingList(0,eventVO.getSocietyID());
		List emailList=new ArrayList<EmailMessage>();
		List smsList=new ArrayList<SMSMessage>();
		List pushNotificationList=new ArrayList<PushNotificationVO>();
		
		for(int i=0;memberList.size()>i;i++){
		MemberVO memberVO=(MemberVO) memberList.get(i);	
		if(memberVO.getEmail().length()>0){
		EmailMessage emailVO=prepareNominationReminderEmailObject(eventVO,memberVO, "nominationReminder.vm");
		emailList.add(emailVO);
		}
		
		/*if((memberVO.getMobile().length()>0)&&(!memberVO.getMobile().equalsIgnoreCase("0000000000"))){
		SMSMessage smsVO=prepareNominationReminderSMSObject(memberVO  );
		smsList.add(smsVO);
		}*/
		
		PushNotificationVO pnVO=prepareNominationReminderPushNotificationObject(memberVO);
		if((pnVO.getTo()!=null)&&(pnVO.getTo().length()>0)){
			pushNotificationList.add(pnVO);
		}
		
			
		}
		
		if(emailList.size()>0){
			notificationVO.setEmailList(emailList);
			notificationVO.setSendEmail(true);
		}
		if(smsList.size()>0){
			notificationVO.setSmsList(smsList);
			notificationVO.setSendSMS(false);
		}
		if(pushNotificationList.size()>0){
			notificationVO.setPushNotificationList(pushNotificationList);
			notificationVO.setSendPushNotifications(true);
			notificationVO.setAppID("1");
		}
		
		notificationVO=SendNotifications(notificationVO);
		notificationVO.setOrgID(eventVO.getSocietyID());
		
		NotificationVO summaryNotification=new NotificationVO();
		summaryNotification.setOrgID(eventVO.getSocietyID());
		List emailStatusList=new ArrayList<EmailMessage>();
		
		EmailMessage emaiStatuslVO=prepareNominationStatusEmailObject(summaryNotification);
		
		emailStatusList.add(emaiStatuslVO);
		
		summaryNotification.setEmailList(emailStatusList);
		summaryNotification.setSendEmail(true);
		summaryNotification.setSendSMS(false);
		summaryNotification.setSendPushNotifications(false);
		
		summaryNotification=SendNotifications(summaryNotification);
		
		
		log.debug("Exit : public NotificationVO sendNominationReminderToSociety(int memberID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public NotificationVO sendNominationReminderToSociety(int memberID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public NotificationVO sendNominationReminderToSociety(int societyID)  for societyID "+eventVO.getSocietyID()+" "+e);
		}
	return notificationVO;
}



	private NotificationVO prepareNominationReminder(int memberID){
	log.debug("Entry : private NotificationVO prepareNominationReminder(int memberID) ");
		NotificationVO notificationVO=new NotificationVO();
		List emailList=new ArrayList<EmailMessage>();
		List smsList=new ArrayList<SMSMessage>();
	try {
		EventVO eventVO=new EventVO();
		MemberVO memberVO=memberServiceBean.searchMembersInfo(String.valueOf(memberID));
		notificationVO.setOrgID(memberVO.getSocietyID());
		List nomineeList=nominationRegService.getNominationListForMember(memberID);
		
	
	if(nomineeList.size()==0){
	
	
	
	EmailMessage emailVO=prepareNominationReminderEmailObject(eventVO,memberVO, "nominationReminder.vm");
	
	//SMSMessage smsVO=prepareNominationReminderSMSObject(memberVO  );
	SMSMessage smsVO=new SMSMessage();
	
	emailList.add(emailVO);
	smsList.add(smsVO);
	
	notificationVO.setEmailList(emailList);
	notificationVO.setSmsList(smsList);

	notificationVO.setSendEmail(true);
	notificationVO.setSendSMS(false);
	
	
	
		
		
	}
	} catch (Exception e) {
		log.error("Exception : private Notification prepareNominationReminder "+e);
	}
	
	
	log.debug("Entry : private NotificationVO prepareNominationReminder(int memberID) ");
	return notificationVO;
}
	
	private EmailMessage prepareNominationReminderEmailObject(EventVO  eventVO,MemberVO memberVO,String template){
		EmailMessage emailVO=new EmailMessage();
		BigDecimal totalAmount=BigDecimal.ZERO;
		java.util.Date today = new java.util.Date();
		
		
		emailVO.setDefaultSender(memberVO.getSocietyName());
		emailVO.setEmailTO(memberVO.getEmail());
		/*
		if(eventVO.getCcEmail().length()>0){
			emailVO.setEmailCc(eventVO.getCcEmail());
		}*/
		emailVO.setSubject("Request to update your Nomination for unit  "+memberVO.getFlatNo());
		try {
				
			
			
		
      
        VelocityContext model = new VelocityContext();
        model.put("fullName", memberVO.getFullName());
        model.put("aptName", memberVO.getFlatNo());
       	model.put("societyName", memberVO.getSocietyName());
		
        
 
       
        /*  add that list to a VelocityContext  */
      
        emailVO.setMessage(templatePreparator.createMessage(model, template));
		} catch (Exception e) {
			log.error("Exception in preparing member object "+e);
		}
		
        
		return emailVO;
	}
	
	private EmailMessage prepareNominationStatusEmailObject(NotificationVO notificationVO){
		EmailMessage emailVO=new EmailMessage();
		BigDecimal totalAmount=BigDecimal.ZERO;
		java.util.Date today = new java.util.Date();
		
		SocietyVO societyVO=societyServiceBean.getSocietyDetails(notificationVO.getOrgID());
		List nomineeList=nominationRegService.getNominationMissingList(0,societyVO.getSocietyID());
		emailVO.setDefaultSender("E-Sanchalak Services");
		emailVO.setEmailTO(societyVO.getMailListA());
		emailVO.setSubject("Reminders sent to members for filing Nomination ");
		try {
				
			
			
		
      
        VelocityContext model = new VelocityContext();
        model.put("nomineeCount", nomineeList.size());
           
 
       
        /*  add that list to a VelocityContext  */
      
        emailVO.setMessage(templatePreparator.createMessage(model, "nominationSummary.vm"));
		} catch (Exception e) {
			log.error("Exception in preparing member object "+e);
		}
		
        
		return emailVO;
	}
	
	private SMSMessage prepareNominationReminderSMSObject(MemberVO memberVO){
		SMSMessage smsVO=new SMSMessage();
		try {
		log.debug("Entry : private SMSMessage prepareReminderSMSObject");
		ConfigManager config=new ConfigManager();
		String unitName=memberVO.getFlatNo();
		String societyName=memberVO.getSocietyShortName();
		String memberName=memberVO.getFullName();
		
		String smsString="";
	 	
	 		  smsString=config.getPropertiesValue("sms.nomination");
			  smsString=smsString.replace("$unitName", unitName);
			  smsString=smsString.replace("$memberName", memberName);
			  smsString=smsString.replace("$societyName", societyName);
			
		
	 	
		
		smsVO.setMobileNumber(memberVO.getMobile());
		smsVO.setMessage(smsString);
		
		
		} catch (Exception e) {
			log.error("Exception occurred in private SMSMessage prepareReminderSMSObject "+e);
		}
		log.debug("Exit : private SMSMessage prepareReminderSMSObject");
		
		
		return smsVO;
	}
	
	//================Prepare nomination push Notification ==============//
	
	private PushNotificationVO prepareNominationReminderPushNotificationObject(MemberVO memberVO){
		PushNotificationVO pushNotificationVO=new PushNotificationVO();
		try {
		log.debug("Entry : private PushNotificationVO prepareNominationReminderPushNotificationObject(MemberVO memberVO)");
		
		UserVO deviceDetailsVO=userService.getUserDeviceDetailsFromMemberID(memberVO.getMemberID(), "1","M");
		ConfigManager config=new ConfigManager();
		String unitName=memberVO.getFlatNo();
		String societyName=memberVO.getSocietyShortName();
		String memberName=memberVO.getFullName();
		
		
		String smsString="";
	 	
	 		  smsString=config.getPropertiesValue("sms.nomination");
			  smsString=smsString.replace("$unitName", unitName);
			  smsString=smsString.replace("$memberName", memberName);
			  smsString=smsString.replace("$societyName", societyName);
	
			
		String title="";
	 	
	 	
		
		pushNotificationVO.setTo(deviceDetailsVO.getAndriodDeviceID());
		pushNotificationVO.setTitle(title);
		pushNotificationVO.setBody(smsString);

		
		
		
		
		} catch (Exception e) {
			log.error("Exception occurred in private PushNotificationVO prepareNominationReminderPushNotificationObject(MemberVO memberVO)t "+e);
		}
		log.debug("Exit : private PushNotificationVO prepareNominationReminderPushNotificationObject(MemberVO memberVO)");
		
		
		return pushNotificationVO;
	}
	
	
	//===================== Document Reminder ====================//
	
	public NotificationVO sendDocumentReminder(EventVO eventVO)  {
			
			log.debug("Entry : public NotificationVO sendDocumentReminder(EventVO eventVO)");
			NotificationVO notificationVO=new NotificationVO();
			try {
			
				
			notificationVO=prepareDocumentReminder(eventVO);

				notificationVO=SendNotifications(notificationVO);
				
					
				
				
				log.debug("Exit : public NotificationVO sendDocumentReminder(EventVO eventVO)");	
			} catch (DataAccessException e) {
			
				log.error("DataAccessException : public NotificationVO sendDocumentReminder(EventVO eventVO)"+e);
			}
			 catch (Exception e) {
					
					log.error("Exception : public NotificationVO sendDocumentReminder(EventVO eventVO)"+e);
				}
			return notificationVO;
		}


		public NotificationVO sendDocumentReminderToSociety(EventVO eventVO)  {
		
		log.debug("Entry : public NotificationVO sendDocumentReminderToSociety(EventVO eventVO) ");
		NotificationVO notificationVO=new NotificationVO();
		try {
		
			int societyID=eventVO.getSocietyID();
			List memberList=memberServiceBean.getDocumentMissingList(0, eventVO.getSocietyID(), 5);
			List emailList=new ArrayList<EmailMessage>();
			List smsList=new ArrayList<SMSMessage>();
			
			for(int i=0;memberList.size()>i;i++){
			MemberVO memberVO=(MemberVO) memberList.get(i);	
			if(memberVO.getEmail().length()>0){
			EmailMessage emailVO=prepareDocumentReminderEmailObject(memberVO, eventVO);
			emailList.add(emailVO);
			}
			
			/*if((memberVO.getMobile().length()>0)&&(!memberVO.getMobile().equalsIgnoreCase("0000000000"))){
			SMSMessage smsVO=prepareDocumentReminderSMSObject(memberVO,eventVO.getSmsTemplate()  );
			smsList.add(smsVO);
			}
			*/
			
				
			}
			
			if(emailList.size()>0){
				notificationVO.setEmailList(emailList);
				notificationVO.setSendEmail(true);
			}
			if(smsList.size()>0){
				notificationVO.setSmsList(smsList);
				notificationVO.setSendSMS(false);
			}
			notificationVO=SendNotifications(notificationVO);
			notificationVO.setOrgID(societyID);
			
			NotificationVO summaryNotification=new NotificationVO();
			summaryNotification.setOrgID(societyID);
			List emailStatusList=new ArrayList<EmailMessage>();
			
			EmailMessage emaiStatuslVO=prepareDocumentStatusEmailObject(notificationVO);
			
			emailStatusList.add(emaiStatuslVO);
			
			summaryNotification.setEmailList(emailStatusList);
			summaryNotification.setSendEmail(true);
			summaryNotification.setSendSMS(false);
			
			summaryNotification=SendNotifications(summaryNotification);
			
		
			
			
			log.debug("Exit : public NotificationVO sendDocumentReminderToSociety(EventVO eventVO)");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException : public NotificationVO sendDocumentReminderToSociety(EventVO eventVO)"+e);
		}
		 catch (Exception e) {
				
				log.error("Exception : public NotificationVO sendDocumentReminderToSociety(EventVO eventVO)"+e);
			}
		return notificationVO;
	}



		private NotificationVO prepareDocumentReminder(EventVO eventVO){
		log.debug("Entry : private NotificationVO prepareDocumentReminder(EventVO eventVO)");
			NotificationVO notificationVO=new NotificationVO();
			List emailList=new ArrayList<EmailMessage>();
			List smsList=new ArrayList<SMSMessage>();
		try {
		
			MemberVO memberVO=memberServiceBean.searchMembersInfo(String.valueOf(eventVO.getMemberID()));
			notificationVO.setOrgID(memberVO.getSocietyID());
			List documentList=memberServiceBean.getMemberMissingDocument(eventVO.getMemberID(),eventVO.getDocumentID());
			
		
		if(documentList.size()==0){
		
		
		
		EmailMessage emailVO=prepareDocumentReminderEmailObject(memberVO, eventVO);
		
		SMSMessage smsVO=new SMSMessage();//prepareDocumentReminderSMSObject(memberVO ,eventVO.getSmsTemplate() );
		
		emailList.add(emailVO);
		smsList.add(smsVO);
		
		notificationVO.setEmailList(emailList);
		notificationVO.setSmsList(smsList);

		notificationVO.setSendEmail(true);
		notificationVO.setSendSMS(false);
		
		
		
			
			
		}
		} catch (Exception e) {
			log.error("Exception : private Notification prepareDocumentReminder(EventVO eventVO) "+e);
		}
		
		
		log.debug("Entry : private NotificationVO prepareDocumentReminder(EventVO eventVO) ");
		return notificationVO;
	}
		
		private EmailMessage prepareDocumentReminderEmailObject(MemberVO memberVO,EventVO eventVO){
			EmailMessage emailVO=new EmailMessage();
			BigDecimal totalAmount=BigDecimal.ZERO;
			java.util.Date today = new java.util.Date();
			
			
			emailVO.setDefaultSender(memberVO.getSocietyName());
			emailVO.setEmailTO(memberVO.getEmail());
			if((eventVO.getCcEmail()!=null)&&(eventVO.getCcEmail().length()>0)){
				emailVO.setEmailCc(eventVO.getCcEmail());
			}
			emailVO.setSubject(eventVO.getEventSubject()+" "+memberVO.getFlatNo());
			try {
					
				
				
			
	      
	        VelocityContext model = new VelocityContext();
	        model.put("fullName", memberVO.getFullName());
	        model.put("aptName", memberVO.getFlatNo());
	       	model.put("societyName", memberVO.getSocietyName());
			
	        
	 
	       
	        /*  add that list to a VelocityContext  */
	      
	        emailVO.setMessage(templatePreparator.createMessage(model, eventVO.getTemplateID()));
			} catch (Exception e) {
				log.error("Exception in preparing member object "+e);
			}
			
	        
			return emailVO;
		}
		
		private EmailMessage prepareDocumentStatusEmailObject(NotificationVO notificationVO){
			EmailMessage emailVO=new EmailMessage();
			BigDecimal totalAmount=BigDecimal.ZERO;
			java.util.Date today = new java.util.Date();
			
			SocietyVO societyVO=societyServiceBean.getSocietyDetails(notificationVO.getOrgID());
			List indexIIList=memberServiceBean.getDocumentMissingList(0, societyVO.getSocietyID(), 5);
			emailVO.setDefaultSender("E-Sanchalak Services");
			emailVO.setEmailTO(societyVO.getMailListA());
			
			emailVO.setSubject("Reminders sent to members for Submitting IndexII copy");
			try {
					
				
				
			
	      
	        VelocityContext model = new VelocityContext();
	        model.put("indexIICount", indexIIList.size());
	           
	 
	       
	        /*  add that list to a VelocityContext  */
	      
	        emailVO.setMessage(templatePreparator.createMessage(model, "indexIISummary.vm"));
			} catch (Exception e) {
				log.error("Exception in preparing member object "+e);
			}
			
	        
			return emailVO;
		}
		
		private SMSMessage prepareDocumentReminderSMSObject(MemberVO memberVO,String smsTemplate){
			SMSMessage smsVO=new SMSMessage();
			try {
			log.debug("Entry : private SMSMessage prepareIndexIIReminderSMSObject");
			ConfigManager config=new ConfigManager();
			String unitName=memberVO.getFlatNo();
			String societyName=memberVO.getSocietyShortName();
			String memberName=memberVO.getFullName();
			
			String smsString="";
		 	
		 		  smsString=config.getPropertiesValue("sms."+smsTemplate);
				  smsString=smsString.replace("$unitName", unitName);
				  smsString=smsString.replace("$memberName", memberName);
				  smsString=smsString.replace("$societyName", societyName);
				
			
		 	
			
			smsVO.setMobileNumber(memberVO.getMobile());
			smsVO.setMessage(smsString);
			
			
			} catch (Exception e) {
				log.error("Exception occurred in private SMSMessage prepareIndexIIReminderSMSObject "+e);
			}
			log.debug("Exit : private SMSMessage prepareIndexIIReminderSMSObject");
			
			
			return smsVO;
		}

		
		public int sendBankStatementRequestReminder(EventVO eventVO)  {
			
			log.debug("Entry : public int sendBankStatementRequestReminder(EventVO eventVO)");
			int successFlag=0;
			NotificationVO notificationVO=new NotificationVO();
			try {
			
				
				SocietyVO societyVO=societyServiceBean.getSocietyDetails(eventVO.getSocietyID());
				
				
				List emailList=new ArrayList<EmailMessage>();
				List smsList=new ArrayList<SMSMessage>();
				
				
				
				EmailMessage emailVO=prepareBankStatementEmailObject( societyVO, "bankStatement.vm", eventVO);
				emailList.add(emailVO);
				
				
							
				
				
				if(emailList.size()>0){
					notificationVO.setEmailList(emailList);
					notificationVO.setSendEmail(true);
				}
			
				notificationVO=SendNotifications(notificationVO);
				
				if(notificationVO.getSentEmailsCount()>0){
					successFlag=1;
				}
			
				
				
				log.debug("Exit : public int sendBankStatementRequestReminder(EventVO eventVO)");	
			} catch (DataAccessException e) {
			
				log.error("DataAccessException : public int sendBankStatementRequestReminder(EventVO eventVO)"+e);
			}
			 catch (Exception e) {
					
					log.error("Exception : public int sendBankStatementRequestReminder(EventVO eventVO)"+e);
				}
			
			return successFlag;
		}
		
		private EmailMessage prepareBankStatementEmailObject(SocietyVO societyVO,String template,EventVO eventVO){
			EmailMessage emailVO=new EmailMessage();
			BigDecimal totalAmount=BigDecimal.ZERO;
			java.util.Date today = new java.util.Date();
			Date currentDate=dateUtil.findCurrentDate();
			int month=currentDate.getMonth();
			int year=currentDate.getYear();
			Date firstDate=dateUtil.getMonthFirstDate((month+1), year);
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");    	 
			
			
			emailVO.setDefaultSender("E-Sanchalak Services");
			emailVO.setEmailTO(eventVO.getEmailID());
			if((eventVO.getCcEmail()!=null)&&(eventVO.getCcEmail().length()>0)){
				emailVO.setEmailCc(eventVO.getCcEmail());
			}
			emailVO.setSubject(eventVO.getEventSubject()+" "+societyVO.getSocietyName());
			try {
					
				
				
			
	      
	        VelocityContext model = new VelocityContext();
	        model.put("currentDate", sdf.format(firstDate)+" to "+sdf.format(currentDate));
	     
			
	        
	 
	       
	        /*  add that list to a VelocityContext  */
	      
	        emailVO.setMessage(templatePreparator.createMessage(model, template));
			} catch (Exception e) {
				log.error("Exception in preparing member object "+e);
			}
			
	        
			return emailVO;
		}
		
		//========Send Cash balance to Org=========//
		public int sendCashBalanceReminder(EventVO eventVO)  {
			
			log.debug("Entry : public int sendCashBalanceReminder(EventVO eventVO)");
			int successFlag=0;
			NotificationVO notificationVO=new NotificationVO();
			try {
				List cashAccountList=new ArrayList();
				List accountList=ledgerService.getLedgerList(eventVO.getSocietyID(), 5);
				java.util.Date convertedDate =new java.util.Date();
				SimpleDateFormat source = new SimpleDateFormat("yyyy-MM-dd");
				for(int i=0;accountList.size()>i;i++){
					AccountHeadVO acHVO=(AccountHeadVO) accountList.get(i);
					Date currentDate=dateUtil.findCurrentDate();
					int month=currentDate.getMonth();
					int year=currentDate.getYear()+1900;
					String firstDate=year-1+"-04-01";
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");    	 
					acHVO.setCreateDate(sdf.format(currentDate));
					LedgerTrnsVO CashObj=ledgerService.getQuickLedgerTransactionList(eventVO.getSocietyID(), acHVO.getLedgerID(), ""+firstDate, ""+currentDate,"ALL");
					List txList=CashObj.getTransactionList();
					acHVO=ledgerService.getOpeningClosingBalance(eventVO.getSocietyID(), acHVO.getLedgerID(), ""+currentDate, ""+currentDate,"L");
					if(txList.size()>0){
						TransactionVO tempAccHvo=(TransactionVO) txList.get(txList.size()-1);
						convertedDate=source.parse(tempAccHvo.getTransactionDate());
					
						acHVO.setEffectiveDate(sdf.format(convertedDate));
							
						
					}else
						acHVO.setEffectiveDate("NA");
						acHVO.setClosingBalance(acHVO.getClosingBalance());
						acHVO.setEffectiveDate(acHVO.getEffectiveDate());
						cashAccountList.add(acHVO);
					
					
				}
				
				String htmlTabl=htmlTable.getHTMLTableForCashBalance(cashAccountList);
				
				SocietyVO societyVO=societyServiceBean.getSocietyDetails(eventVO.getSocietyID());
				
				
				List emailList=new ArrayList<EmailMessage>();
				List smsList=new ArrayList<SMSMessage>();
				
				
				
				EmailMessage emailVO=prepareCashBalanceEmailObject( societyVO, "cashBalance.vm", eventVO,htmlTabl);
				emailList.add(emailVO);
				
				
							
				
				
				if(emailList.size()>0){
					notificationVO.setEmailList(emailList);
					notificationVO.setSendEmail(true);
				}
			
				notificationVO=SendNotifications(notificationVO);
				
				if(notificationVO.getSentEmailsCount()>0){
					successFlag=1;
				}
			
				
				
				log.debug("Exit : public int sendCashBalanceReminder(EventVO eventVO)");	
			} catch (DataAccessException e) {
			
				log.error("DataAccessException : public int sendCashBalanceReminder(EventVO eventVO)"+e);
			}
			 catch (Exception e) {
					
					log.error("Exception : public int sendCashBalanceReminder(EventVO eventVO)"+e);
				}
			
			return successFlag;
		}
		
		private EmailMessage prepareCashBalanceEmailObject(SocietyVO societyVO,String template,EventVO eventVO,String htmlString){
			EmailMessage emailVO=new EmailMessage();
			BigDecimal totalAmount=BigDecimal.ZERO;
			java.util.Date today = new java.util.Date();
			Date currentDate=dateUtil.findCurrentDate();
			int month=currentDate.getMonth();
			int year=currentDate.getYear();
			Date firstDate=dateUtil.getMonthFirstDate((month+1), year);
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");    	 
			
			
			emailVO.setDefaultSender("E-Sanchalak Services");
			emailVO.setEmailTO(eventVO.getEmailID());
			if((eventVO.getCcEmail()!=null)&&(eventVO.getCcEmail().length()>0)){
				emailVO.setEmailCc(eventVO.getCcEmail());
			}
			emailVO.setSubject(eventVO.getEventSubject()+" "+societyVO.getSocietyName());
			try {
					
				
				
			
	      
	        VelocityContext model = new VelocityContext();
	        model.put("currentDate", sdf.format(currentDate));
	        model.put("htmlString",htmlString);
			
	        
	 
	       
	        /*  add that list to a VelocityContext  */
	      
	        emailVO.setMessage(templatePreparator.createMessage(model, template));
			} catch (Exception e) {
				log.error("Exception in preparing member object "+e);
			}
			
	        
			return emailVO;
		}
		
		//== Send Suspense Transactions status to society ========//
		
		public NotificationVO sendSuspenseStatusToSociety(EventVO eventVO)  {
			
			log.debug("Entry : public NotificationVO sendSuspenseStatusToSociety(int societyID) ");
			NotificationVO notificationVO=new NotificationVO();
			try {
			
				
				SocietyVO societyVO=societyServiceBean.getSocietyDetails(eventVO.getSocietyID());
				DashBoardAccountVO dbAccVO=dashBoardService.getSuspenceEntryDetails(eventVO.getSocietyID());
				
				List emailList=new ArrayList<EmailMessage>();
				List smsList=new ArrayList<SMSMessage>();
				if(dbAccVO.getSuspenseEntries()>0 ){
				
				if(societyVO.getMailListA().length()>0){
				EmailMessage emailVO=prepareSuspenceReminderEmailObject(dbAccVO,societyVO, "suspenceSummary.vm",eventVO);
				emailList.add(emailVO);
				}
				
							
				
				
				if(emailList.size()>0){
					notificationVO.setEmailList(emailList);
					notificationVO.setSendEmail(true);
				}
			
				notificationVO=SendNotifications(notificationVO);
				
				}
				
				log.debug("Exit : public NotificationVO sendIndexIIReminderToSociety(int memberID)");	
			} catch (DataAccessException e) {
			
				log.error("DataAccessException : public NotificationVO sendIndexIIReminderToSociety(int memberID)"+e);
			}
			 catch (Exception e) {
					
					log.error("Exception : public NotificationVO sendIndexIIReminderToSociety(int memberID)"+e);
				}
			return notificationVO;
		}
		
		//== Send Pending Transactions status to society ========//
		
				public NotificationVO sendPendingTransactionStatusToSociety(EventVO eventVO)  {
					
					log.debug("Entry : public NotificationVO sendPendingTransactionStatusToSociety(int societyID) ");
					NotificationVO notificationVO=new NotificationVO();
					try {
					
						
						SocietyVO societyVO=societyServiceBean.getSocietyDetails(eventVO.getSocietyID());
						DashBoardAccountVO dbAccVO=dashBoardService.getPendingTransactionsCount(societyVO.getSocietyID());
						
						List emailList=new ArrayList<EmailMessage>();
						List smsList=new ArrayList<SMSMessage>();
						if(dbAccVO.getPendingTxCount()>0 ){
						
						
						EmailMessage emailVO=preparePendingTransactionEmailObject(dbAccVO,societyVO, "nonApprovedTxSummary.vm",eventVO);
						emailList.add(emailVO);
						
						
									
						
						
						if(emailList.size()>0){
							notificationVO.setEmailList(emailList);
							notificationVO.setSendEmail(true);
						}
					
						notificationVO=SendNotifications(notificationVO);
						
						}
						
						log.debug("Exit : public NotificationVO sendPendingTransactionStatusToSociety(int memberID)");	
					} catch (DataAccessException e) {
					
						log.error("DataAccessException : public NotificationVO sendPendingTransactionStatusToSociety(int memberID)"+e);
					}
					 catch (Exception e) {
							
							log.error("Exception : public NotificationVO sendPendingTransactionStatusToSociety(int memberID)"+e);
						}
					return notificationVO;
				}
		
		private EmailMessage prepareSuspenceReminderEmailObject(DashBoardAccountVO dbAccVO,SocietyVO societyVO,String template,EventVO eventVO){
			EmailMessage emailVO=new EmailMessage();
			BigDecimal totalAmount=BigDecimal.ZERO;
			java.util.Date today = new java.util.Date();
			
			
			emailVO.setDefaultSender("E-Sanchalak Services");
			emailVO.setEmailTO(eventVO.getEmailID());
			if((eventVO.getCcEmail()!=null)&&(eventVO.getCcEmail().length()>0)){
				emailVO.setEmailCc(eventVO.getCcEmail());
			}
			emailVO.setSubject("Found few suspense entries in accounts ");
			try {
					
				
				
			
	      
	        VelocityContext model = new VelocityContext();
	        model.put("susReceiptCnt", dbAccVO.getSuspenceReceiptCount());
	        model.put("susReceiptSum", dbAccVO.getSuspenceReceiptSum());
	       	model.put("susPaymentCnt", dbAccVO.getSuspencePaymentCount());
	       	model.put("susPaymentSum", dbAccVO.getSuspencePaymentSum());
			
	        
	 
	       
	        /*  add that list to a VelocityContext  */
	      
	        emailVO.setMessage(templatePreparator.createMessage(model, template));
			} catch (Exception e) {
				log.error("Exception in preparing member object "+e);
			}
			
	        
			return emailVO;
		}
		
		private EmailMessage preparePendingTransactionEmailObject(DashBoardAccountVO dbAccVO,SocietyVO societyVO,String template,EventVO eventVO){
			EmailMessage emailVO=new EmailMessage();
			BigDecimal totalAmount=BigDecimal.ZERO;
			java.util.Date today = new java.util.Date();
			
			
			emailVO.setDefaultSender("E-Sanchalak Services");
			emailVO.setEmailTO(eventVO.getEmailID());
			if((eventVO.getCcEmail()!=null)&&(eventVO.getCcEmail().length()>0)){
				emailVO.setEmailCc(eventVO.getCcEmail());
			}
			emailVO.setSubject("["+societyVO.getSocietyName()+"] "+dbAccVO.getPendingTxCount()+" Transactions are waiting for your approval ");
			try {
					
				
				
			
	      
	        VelocityContext model = new VelocityContext();
	        model.put("count", dbAccVO.getPendingTxCount());
	     
			
	        
	 
	       
	        /*  add that list to a VelocityContext  */
	      
	        emailVO.setMessage(templatePreparator.createMessage(model, template));
			} catch (Exception e) {
				log.error("Exception in preparing member object "+e);
			}
			
	        
			return emailVO;
		}
	
		//=====================Send Virtual Account Number Reminder ====================//
		
		public NotificationVO sendVirtualAccNumbers(int societyID)  {
			
		log.debug("Entry : public NotificationVO sendVirtualAccountNumbers(int societyID) ");
		NotificationVO notificationVO=new NotificationVO();
		try {
		
		
			List memberList=memberServiceBean.getActivePrimaryMemberList(societyID);
			BankInfoVO bankInfoVO=societyServiceBean.getBankDetails(societyID);
			List emailList=new ArrayList<EmailMessage>();
			List smsList=new ArrayList<SMSMessage>();
			
			for(int i=0;memberList.size()>i;i++){
			MemberVO memberVO=(MemberVO) memberList.get(i);	
			ProfileDetailsVO profileVO=ledgerService.getLedgerProfile(memberVO.getLedgerID(), societyID);
			BankInfoVO memberBankVO=bankInfoVO;
			if((profileVO.getVirtualAccNo()!=null)&&(profileVO.getVirtualAccNo().length()>0)){
				memberBankVO.setAccountNo(profileVO.getVirtualAccNo());
			}
			if((memberVO.getEmail()!=null)&&(memberVO.getEmail().length()>0)){
			EmailMessage emailVO=prepareVirtualAccountEmailObject(memberVO, memberBankVO);
			emailList.add(emailVO);
			}
			
			if((memberVO.getMobile()!=null)&&(memberVO.getMobile().length()>0)&&(!memberVO.getMobile().equalsIgnoreCase("0000000000"))){
			SMSMessage smsVO=prepareVirtualAccountSMSObject(memberVO, memberBankVO  );
			smsList.add(smsVO);
			}
			
			
				
			}
			
			if(emailList.size()>0){
				notificationVO.setEmailList(emailList);
				notificationVO.setSendEmail(true);
			}
			if(smsList.size()>0){
				notificationVO.setSmsList(smsList);
				notificationVO.setSendSMS(true);
			}
			notificationVO.setOrgID(societyID);
			notificationVO=SendNotifications(notificationVO);
						
		
			
	
			
			log.debug("Exit : public NotificationVO sendVirtualAccountNumbers(int societyID)");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException : public NotificationVO sendVirtualAccountNumbers(int societyID)"+e);
		}
		 catch (Exception e) {
				
				log.error("Exception : public NotificationVO sendVirtualAccountNumbers(int societyID)"+e);
			}
		return notificationVO;
	}

		public NotificationVO sendNotificationOfPaymentReminder()  {
			
			log.debug("Entry : public NotificationVO sendNotificationOfPaymentReminder()  ");
			NotificationVO notificationVO=new NotificationVO();
			ConfigManager config=new ConfigManager();
			
			try {
					
				List eventList=eventService.getNotificationsListOfPaymentReminders(2);
				if(eventList.size()>0){
				List emailList=new ArrayList<EmailMessage>();
				
				String htmlTabl=htmlTable.getHTMLTableForNotificationOfPaymentReminder(eventList);
				EmailMessage emailVO=new EmailMessage();
				emailVO.setDefaultSender("E-Sanchalak Services");
				emailVO.setEmailTO(config.getPropertiesValue("mail.reminderNotification"));
				
				emailVO.setSubject(" Upcoming notifications for Maintenance Reminder  ");
				
						
					
					
				
		      
		        VelocityContext model = new VelocityContext();
		        model.put("htmlTable", htmlTabl);
		     
				
		        
		 
		       
		        /*  add that list to a VelocityContext  */
		      
		        emailVO.setMessage(templatePreparator.createMessage(model,"notificationOfReminder.vm"));
				
				emailList.add(emailVO);
				if(emailList.size()>0){
					notificationVO.setEmailList(emailList);
					notificationVO.setSendEmail(true);
				}
				
				
				notificationVO=SendNotifications(notificationVO);
				}
				
				List eventsList=eventService.getNotificationsListOfPaymentReminders(0);
				if(eventsList.size()>0){
				List emailList=new ArrayList<EmailMessage>();
				
				String htmlTab=htmlTable.getHTMLTableForNotificationOfPaymentReminder(eventsList);
				EmailMessage emailVO=new EmailMessage();
				emailVO.setDefaultSender("E-Sanchalak Services");
				emailVO.setEmailTO(config.getPropertiesValue("mail.lowSMSNotification"));
				
				emailVO.setSubject(" Todays notifications for Maintenance Reminder  ");
				
						
					
					
				
		      
		        VelocityContext model = new VelocityContext();
		        model.put("htmlTable", htmlTab);
		     
				
		        
		 
		       
		        /*  add that list to a VelocityContext  */
		      
		        emailVO.setMessage(templatePreparator.createMessage(model,"notificationOfReminder.vm"));
				
				emailList.add(emailVO);
				if(emailList.size()>0){
					notificationVO.setEmailList(emailList);
					notificationVO.setSendEmail(true);
				}
				
				
				notificationVO=SendNotifications(notificationVO);
				}
				log.debug("Exit : public NotificationVO sendNotificationOfPaymentReminder ");	
			
			} catch (Exception e) {
					
					log.error("Exception : public NotificationVO sendNotificationOfPaymentReminder() "+e);
				}
			return notificationVO;
		}
		
		public NotificationVO sendNotificationForLowSMSCredit()  {
			
			log.debug("Entry : public NotificationVO sendNotificationForLowSMSCredit()  ");
			NotificationVO notificationVO=new NotificationVO();
			ConfigManager config=new ConfigManager();
			
			try {
					
				List orgList=notificationDAO.sendNotificationForLowSMSCredit();
				if(orgList.size()>0){
				List emailList=new ArrayList<EmailMessage>();
				
				String htmlTabl=htmlTable.getHTMLTableForLowSMSCredit(orgList);
				EmailMessage emailVO=new EmailMessage();
				emailVO.setDefaultSender("E-Sanchalak Services");
				emailVO.setEmailTO(config.getPropertiesValue("mail.lowSMSNotification"));
				
				emailVO.setSubject(" The following organizations have low SMS credit  ");
				
						
		        VelocityContext model = new VelocityContext();
		        model.put("htmlTable", htmlTabl);
		     
				
		        
		 
		       
		        /*  add that list to a VelocityContext  */
		      
		        emailVO.setMessage(templatePreparator.createMessage(model,"lowSMSCountReminder.vm"));
				
				emailList.add(emailVO);
				if(emailList.size()>0){
					notificationVO.setEmailList(emailList);
					notificationVO.setSendEmail(true);
				}
				
				
				notificationVO=SendNotifications(notificationVO);
				}
				log.debug("Exit : public NotificationVO sendNotificationForLowSMSCredit() ");	
			
			} catch (Exception e) {
					
					log.error("Exception : public NotificationVO sendNotificationForLowSMSCredit() "+e);
				}
			return notificationVO;
		}
		
	
		private EmailMessage prepareVirtualAccountEmailObject(MemberVO memberVO,BankInfoVO bankInfoVO){
			EmailMessage emailVO=new EmailMessage();
		
		  
			String address="";
			
			emailVO.setDefaultSender(bankInfoVO.getSocietyName());
			emailVO.setEmailTO(memberVO.getEmail());
			emailVO.setSubject("[Accounts team] Your virtual account number for "+memberVO.getFlatNo());
			String template="virtualAccountNumber.vm";
			try {
					
	        VelocityContext model = new VelocityContext();
	        model.put("fullName", memberVO.getFullName());
	        model.put("aptName", memberVO.getFlatNo());
	
			model.put("societyName", bankInfoVO.getSocietyName());
			model.put("bankName", bankInfoVO.getBankName());
			model.put("branchCode", bankInfoVO.getBranchCode());
			model.put("accNo", bankInfoVO.getAccountNo());
			model.put("accType", bankInfoVO.getAccType());
			model.put("ifsc", bankInfoVO.getIfscNo());
			model.put("micr", bankInfoVO.getMicrNo());
			model.put("favour", bankInfoVO.getFavour());
	        
	 
	       
	        /*  add that list to a VelocityContext  */
	      
	        emailVO.setMessage(templatePreparator.createMessage(model, template));
			} catch (Exception e) {
				log.error("Exception in preparing member object "+e);
			}
			
	        
			return emailVO;
		}
		
		private SMSMessage prepareVirtualAccountSMSObject(MemberVO memberVO,BankInfoVO bankVO){//1-If new Invoice generated else 0 for reminder
			SMSMessage smsVO=new SMSMessage();
			try {
			log.debug("Entry : private SMSMessage prepareReminderSMSObject");
			ConfigManager config=new ConfigManager();
			String unitName=memberVO.getFlatNo();
			String memberName=memberVO.getFullName();
			
			String smsString="Dear Member, Please make NEFT payments to below account number only for your Unit. IFS code -"+bankVO.getIfscNo()+",  "+bankVO.getAccountNo()+" Account number. Do not share this account with other members.-"+bankVO.getSocietyShortName();
		 	Properties properties=new Properties();
		
		 	
			
			smsVO.setMobileNumber(memberVO.getMobile());
			smsVO.setMessage(smsString);
			
			
			} catch (Exception e) {
				log.error("Exception occurred in private SMSMessage prepareReminderSMSObject "+e);
			}
			log.debug("Exit : private SMSMessage prepareReminderSMSObject");
			
			
			return smsVO;
		}
		
		public void sendSummaryNotifiactionToTechsupport(List respList){
			log.debug("Entry : Public void sendSummaryNotifiactionToTechsupport(List respList) ");
			List memberList=new java.util.ArrayList();
			
			try{
				 NotificationVO notificationVO=new NotificationVO();
			  	EmailMessage emailVO=new EmailMessage();
			 	List emailList=new ArrayList();
			 	List smsList=new ArrayList();
				
				EventVO eventVO=new EventVO();
				java.sql.Date curdate=dateUtil.findCurrentDate();
				eventVO.setEventID(0000);
				eventVO.setTransactionType("Summary Mail");
			
				emailVO.setSubject("Invoice Generation Summary");
			
				emailVO.setDefaultSender("Esanchalak Solutions and Services Pvt Ltd");
				emailVO.setEmailTO("sudarshan_sathe@esanchalak.com");
			
				
			     VelocityContext model = new VelocityContext();
			     model.put("htmlTable", htmlTable.getHTMLTableForInnvoiceGenSummary(respList));
			     model.put("subject","Invoice Generation Summary");
			     model.put("updateDate",dateUtil.getConvetedDate(""+curdate));
			     emailVO.setMessage(templatePreparator.createMessage(model, "invoiceGenSummary.vm"));
			     emailList.add(emailVO);
			     
			     notificationVO.setEmailList(emailList);
			     notificationVO.setSendEmail(true);
			     notificationVO.setSendSMS(false);
			     
			
			     notificationVO=SendNotifications(notificationVO);
			     
			
			}catch (Exception e) {
				log.error("Exception in sending sendSummaryNotifiactionToTechsupport "+e);
			}
			
			log.debug("Exit : Public void sendSummaryNotifiactionToTechsupport(List respList) ");
		}
		
		public NotificationVO SendNotifications(NotificationVO notificationVO){
			
			try {
			log.debug("Entry : private NotificationVO SendNotifications");
			
			if(notificationVO.getSendSMS()&&(notificationVO.getSmsList().size()>0)){
			
			SMSGatewayVO smsGatewayVO=getSMSGatewayDetails(notificationVO.getOrgID());
		
			if(smsGatewayVO.getAvailableSMSCredits()>=notificationVO.getSmsList().size()){
					
			if((notificationVO.getSmsList()!=null)&&(notificationVO.getSmsList().size()>0)  ){
				
				
				notificationVO.setSendSMS(true);
				
				
				
				SMSGatewayVO smsGatwVO=new SMSGatewayVO();
				smsGatwVO.setAvailableSMSCredits(notificationVO.getSmsList().size());
				smsGatwVO.setOrgID(notificationVO.getOrgID());
				int deductSMScredits=deductSMSCredit(smsGatwVO);
			}
			
			}else {
			notificationVO.setSendSMS(false);
			notificationVO.setDescription("Available SMS Credits = "+smsGatewayVO.getAvailableSMSCredits()+" No of SMS tried to Send "+notificationVO.getSmsList().size());
			notificationVO.setStatusCode(542);
			}
			}
			notificationVO=sendNotification(notificationVO);
			
			} catch (Exception e) {
				log.error("Exception occurred in NotificationVO SendNotifications "+e);
			}
			log.debug("Exit : private NotificationVO SendNotifications");
			
			
		return notificationVO;
		}

	private NotificationVO sendNotification(NotificationVO notificationVO){
		
		try {
		log.debug("Entry : private NotificationVO SendNotifications");
		int statusCode=notificationVO.getStatusCode();
		ConfigManager c=new ConfigManager();
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        Map map = new HashMap<String, String>();
        map.put("Content-Type", "application/json");

        headers.setAll(map);
        
       
      

        HttpEntity<?> requestEmail = new HttpEntity<>(notificationVO, headers);
        String urlForEmail =c.getPropertiesValue("finbot.sendNotification")+ "sendNotification/";

        ResponseEntity<NotificationVO> respEmail = new RestTemplate().postForEntity(urlForEmail, requestEmail, NotificationVO.class);
        HttpStatus status = respEmail.getStatusCode();
        
        notificationVO=respEmail.getBody();
        if(statusCode==542)
        {
        	notificationVO.setStatusCode(542);
        	notificationVO.setDescription("Insufficient SMS credits ");
        }else
        notificationVO.setStatusCode(status.value());
       
        notificationVO.setEmailList(null);
        notificationVO.setSmsList(null);
        
		} catch (Exception e) {
			log.error("Exception occurred in NotificationVO SendNotifications "+e);
		}
		log.debug("Exit : private NotificationVO SendNotifications");
		
		
	return notificationVO;
	}
	
	
	//===== Get SMS Gateway Details =========//
	   public SMSGatewayVO getSMSGatewayDetails(int orgID)  {
			
			log.debug("Entry : public SMSGatewayVO getSMSGatewayDetails(int orgID) ");
			SMSGatewayVO smsGatewayVO=new SMSGatewayVO();
			try {
			
				smsGatewayVO=notificationDAO.getSMSGatewayDetails(orgID);
				smsGatewayVO.setOrgID(orgID);
						
				log.debug("Exit :public SMSGatewayVO getSMSGatewayDetails(int orgID)");	
			} catch (DataAccessException e) {
			
				log.error("DataAccessException : public SMSGatewayVO getSMSGatewayDetails(int orgID)"+e);
			}
			 catch (Exception e) {
					
					log.error("Exception : public SMSGatewayVO getSMSGatewayDetails(int orgID)"+e);
				}
			
			return smsGatewayVO;
		}
	   
	 //===== API to allot monthly SMS credits =========//
	   public int allotSMSCredits()  {
			
			log.debug("Entry : public SMSGatewayVO allotSMSCredits ");
			int success=0;
			try {
			
				
				success=notificationDAO.allotSMSCredits();

						
				log.debug("Exit :public SMSGatewayVO allotSMSCredits");	
			} catch (DataAccessException e) {
			
				log.error("DataAccessException : public SMSGatewayVO allotSMSCredits"+e);
			}
			 catch (Exception e) {
					
					log.error("Exception : public SMSGatewayVO allotSMSCredits"+e);
				}
			
			return success;
		}
	   
	 //===== Add SMS Credit  =========//
	   public int addSMSCredit(SMSGatewayVO smsVO)  {
			int updateFlag=0;
			log.debug("Entry : public int addSMSCreditDetails(SMSGatewayVO smsVO) ");
			try {
			
				updateFlag=notificationDAO.updateSMSCreditDetails(smsVO, "A");
				
						
				log.debug("Exit : public int addSMSCreditDetails(SMSGatewayVO smsVO)");	
			} catch (DataAccessException e) {
			
				log.error("DataAccessException : public int addSMSCreditDetails(SMSGatewayVO smsVO)"+e);
			}
			 catch (Exception e) {
					
					log.error("Exception : public int addSMSCreditDetails(SMSGatewayVO smsVO)"+e);
				}
			
			return updateFlag;
		}
	
	   
	   //===== Deduct SMS Credit  =========//
	   public int deductSMSCredit(SMSGatewayVO smsVO)  {
			int updateFlag=0;
			log.debug("Entry : public int deductSMSCredit(SMSGatewayVO smsVO) ");
			try {
			
				updateFlag=notificationDAO.updateSMSCreditDetails(smsVO, "D");
				
						
				log.debug("Exit : public int deductSMSCredit(SMSGatewayVO smsVO)");	
			} catch (DataAccessException e) {
			
				log.error("DataAccessException : public int deductSMSCredit(SMSGatewayVO smsVO)"+e);
			}
			 catch (Exception e) {
					
					log.error("Exception : public int deductSMSCredit(SMSGatewayVO smsVO)"+e);
				}
			
			return updateFlag;
		}
	   //===== Get Profile Details in list format =========//
	   public List getProfileDetailedList(NotificationVO notificationVO)  {
			List userList=new ArrayList<UserVO>();
			log.debug("Entry :  public List getProfileDetailedList(NotificationVO notificationVO) ");
			try {
			
				if(notificationVO.getProfileType().equalsIgnoreCase("M")){
					
					if(notificationVO.getGroupName().equalsIgnoreCase("tenant")){
						List tenantList=reportServiceBean.tenantDetailsRpt(notificationVO.getOrgID());
						
						
						
						for(int i=0;tenantList.size()>i;i++){
							RptTenantVO tenantVO=(RptTenantVO) tenantList.get(i);
							if((tenantVO.getEmail()!=null)&&(tenantVO.getEmail().length()>0)){
							UserVO userVO=new UserVO();
							userVO.setProfileID(tenantVO.getRenterID());
							userVO.setUserID(tenantVO.getEmail());
							userVO.setMobile(tenantVO.getMobile());
							userVO.setFullName(tenantVO.getName());
							
							userList.add(userVO);
							}
						}
						
						
					}else{
						
						List memberList=memberServiceBean.getMembersListFromGroup(notificationVO.getOrgID(), notificationVO.getGroupName());
						
						
						for(int i=0;memberList.size()>i;i++){
							MemberVO memberVO= (MemberVO) memberList.get(i);
							UserVO userVO=new UserVO();
						
							userVO.setProfileID(memberVO.getMemberID());
							userVO.setUserID(memberVO.getEmail());
							userVO.setMobile(memberVO.getMobile());
							userVO.setFullName(memberVO.getFullName());
						
							
							
							if((notificationVO.getAppID()!=null) &&(notificationVO.getAppID().length()>0)){
							UserVO tempUserVO=userService.getUserDeviceDetailsFromMemberID(memberVO.getMemberID(), notificationVO.getAppID()+"","M");
							if(tempUserVO.getAndriodDeviceID()!=null){
								
								userVO.setAndriodDeviceID(tempUserVO.getAndriodDeviceID());
								
							}
							}
							userList.add(userVO);
								
							}
							
							}
								
				}else if(notificationVO.getProfileType().equalsIgnoreCase("C")){
					List customerList=ledgerService.getLedgerProfilesList(notificationVO.getOrgID(), "C",notificationVO.getGroupName());
					
					for(int i=0;customerList.size()>i;i++){
						ProfileDetailsVO profileVO=   (ProfileDetailsVO) customerList.get(i);
						UserVO userVO=new UserVO();
						userVO.setProfileID(profileVO.getProfileID());				
						userVO.setUserID(profileVO.getProfileEmailID());
						userVO.setMobile(profileVO.getProfileMobile());
						userVO.setFullName(profileVO.getProfileName());
						
						
						
					/*	if(notificationVO.getAppID().length()>0){
						UserVO tempUserVO=userService.getUserDeviceDetailsFromMemberID(memberVO.getMemberID(), notificationVO.getAppID()+"");
						if(userVO.getAndriodDeviceID()!=null){
							
							userVO.setAndriodDeviceID(tempUserVO.getAndriodDeviceID());
							
						}
						}*/
						userList.add(userVO);
							
					}
					
					
				}else if(notificationVO.getProfileType().equalsIgnoreCase("V")){
					
					List vendorList=ledgerService.getLedgerProfilesList(notificationVO.getOrgID(), "V", notificationVO.getGroupName());
					
					for(int i=0;vendorList.size()>i;i++){
						
						UserVO userVO=new UserVO();
						ProfileDetailsVO profileVO=(ProfileDetailsVO) vendorList.get(i);
						userVO.setProfileID(profileVO.getProfileID());		
						userVO.setUserID(profileVO.getProfileEmailID());
						userVO.setMobile(profileVO.getProfileMobile());
						userVO.setFullName(profileVO.getProfileName());
						
						
						
					/*	if(notificationVO.getAppID().length()>0){
						UserVO tempUserVO=userService.getUserDeviceDetailsFromMemberID(memberVO.getMemberID(), notificationVO.getAppID()+"");
						if(userVO.getAndriodDeviceID()!=null){
							
							userVO.setAndriodDeviceID(tempUserVO.getAndriodDeviceID());
							
						}
						}*/
						userList.add(userVO);
							
					}
					
					
				}else if(notificationVO.getProfileType().equalsIgnoreCase("E")){
					
					List employeeList=ledgerService.getLedgerProfilesList(notificationVO.getOrgID(), "E",notificationVO.getGroupName());
					
					for(int i=0;employeeList.size()>i;i++){
						UserVO userVO=new UserVO();
						ProfileDetailsVO profileVO=(ProfileDetailsVO) employeeList.get(i);
						userVO.setProfileID(profileVO.getProfileID());			
						userVO.setUserID(profileVO.getProfileEmailID());
						userVO.setMobile(profileVO.getProfileMobile());
						userVO.setFullName(profileVO.getProfileName());
						
						
						
					/*	if(notificationVO.getAppID().length()>0){
						UserVO tempUserVO=userService.getUserDeviceDetailsFromMemberID(memberVO.getMemberID(), notificationVO.getAppID()+"");
						if(userVO.getAndriodDeviceID()!=null){
							
							userVO.setAndriodDeviceID(tempUserVO.getAndriodDeviceID());
							
						}
						}*/
						userList.add(userVO);
							
					}
					
				}else if(notificationVO.getProfileType().equalsIgnoreCase("S")){
					
					List studentList=ledgerService.getLedgerProfilesList(notificationVO.getOrgID(), "S",notificationVO.getGroupName());
					
					for(int i=0;studentList.size()>i;i++){
						UserVO userVO=new UserVO();
						ProfileDetailsVO profileVO=(ProfileDetailsVO) studentList.get(i);
						userVO.setProfileID(profileVO.getProfileID());				
						userVO.setUserID(profileVO.getProfileEmailID());
						userVO.setMobile(profileVO.getProfileMobile());
						userVO.setFullName(profileVO.getProfileName());
						
						
						
					/*	if(notificationVO.getAppID().length()>0){
						UserVO tempUserVO=userService.getUserDeviceDetailsFromMemberID(memberVO.getMemberID(), notificationVO.getAppID()+"");
						if(userVO.getAndriodDeviceID()!=null){
							
							userVO.setAndriodDeviceID(tempUserVO.getAndriodDeviceID());
							
						}
						}*/
						userList.add(userVO);
							
					}
					
					
				}
				
				
				

						
				log.debug("Exit :  public List getProfileDetailedList(NotificationVO notificationVO)");	
			} catch (DataAccessException e) {
			
				log.error("DataAccessException :  public List getProfileDetailedList(NotificationVO notificationVO)"+e);
			}
			 catch (Exception e) {
					
					log.error("Exception :  public List getProfileDetailedList(NotificationVO notificationVO)"+e);
				}
			
			return userList;
		}
	
	   public int insertAuditActivity(String userID,String appID,String orgID,String activity,String module){
		   int flag = 0;
		   log.debug("Entry : public int insertAuditActivity(String userID,String appID,String orgID,String activity,String module) ");
			try{
				
				flag=notificationDAO.insertAuditActivity(userID,appID,orgID,activity,module);
				
			}catch(Exception ex){
				log.error("Exception in insertAuditActivity : "+ex);            
		    }
			log.debug("Exit :public int insertAuditActivity(String userID,String appID,String orgID,String activity,String module) ");
			return flag;	
		}
	   
	
	/**
	 * @param memberServiceBean the memberServiceBean to set
	 */
	public void setMemberServiceBean(MemberService memberServiceBean) {
		this.memberServiceBean = memberServiceBean;
	}



	/**
	 * @param invoiceServiceBean the invoiceServiceBean to set
	 */
	public void setInvoiceServiceBean(InvoiceService invoiceServiceBean) {
		this.invoiceServiceBean = invoiceServiceBean;
	}



	/**
	 * @param societyServiceBean the societyServiceBean to set
	 */
	public void setSocietyServiceBean(SocietyService societyServiceBean) {
		this.societyServiceBean = societyServiceBean;
	}



	/**
	 * @param nominationRegService the nominationRegService to set
	 */
	public void setNominationRegService(NominationRegService nominationRegService) {
		this.nominationRegService = nominationRegService;
	}



	/**
	 * @param dashBoardService the dashBoardService to set
	 */
	public void setDashBoardService(DashBoardService dashBoardService) {
		this.dashBoardService = dashBoardService;
	}



	/**
	 * @param ledgerService the ledgerService to set
	 */
	public void setLedgerService(LedgerService ledgerService) {
		this.ledgerService = ledgerService;
	}


	/**
	 * @param userService the userService to set
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}


	/**
	 * @param notificationDAO the notificationDAO to set
	 */
	public void setNotificationDAO(NotificationDAO notificationDAO) {
		this.notificationDAO = notificationDAO;
	}


	/**
	 * @param reportServiceBean the reportServiceBean to set
	 */
	public void setReportServiceBean(ReportService reportServiceBean) {
		this.reportServiceBean = reportServiceBean;
	}


	/**
	 * @param eventService the eventService to set
	 */
	public void setEventService(EventService eventService) {
		this.eventService = eventService;
	}
}
