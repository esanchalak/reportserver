package com.emanager.server.taskAndEventManagement.Service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;

import com.emanager.server.taskAndEventManagement.Domain.EventDomain;
import com.emanager.server.taskAndEventManagement.valueObject.EmailMessage;
import com.emanager.server.taskAndEventManagement.valueObject.EventVO;
import com.emanager.server.taskAndEventManagement.valueObject.NotificationVO;
import com.emanager.server.taskAndEventManagement.valueObject.SMSMessage;


public class EventService {
	
	static Logger log = Logger.getLogger(EventService.class.getName());
	EventDomain eventDomain;
	
public List getEventList(int societyID)  {
		
		log.debug("Entry : public List getEventList(int societyID)");
		
		List eventList = null;
		try {
		
	

			eventList = eventDomain.getEventList(societyID);
			
			
			
			log.debug("Exit : public List getEventList(int societyID)");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException : public List getEventList(int societyID)"+e);
		}
		 catch (Exception e) {
				
				log.error("Exception : public List getEventList(int societyID)"+e);
			}
		return eventList;
	}

public List getEventListForOrg(int orgID)  {
	
	log.debug("Entry : public List getEventListForOrg(int orgID)");
	
	List eventList = null;
	try {
		eventList = eventDomain.getEventListForOrg(orgID);	
		
		log.debug("Exit : public List getEventListForOrg(int orgID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException :"+e);
	}
	 catch (Exception e) {
			
			log.error("DataAccessException :"+e);
		}
	return eventList;
}

public List getPaymentReminderEventDetails(int societyID)  {
	
	log.debug("Entry : public List getPaymentReminderEventDetails(int societyID)");
	
	List eventList = null;
	try {
	


		eventList = eventDomain.getPaymentReminderEventDetails(societyID);
		
		
		
		log.debug("Exit : public List getPaymentReminderEventDetails(int societyID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException :"+e);
	}
	 catch (Exception e) {
			
			log.error("DataAccessException :"+e);
		}
	return eventList;
}



public int addEvent(EventVO eventVO)  {
	
	log.debug("Entry : public int addEvent(EventVO eventVO)");
	
	int successFlag=0;
	try {
	


		successFlag = eventDomain.addEvent(eventVO);
		
		
		
		log.debug("Exit : public int addEvent(EventVO eventVO)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int addEvent(EventVO eventVO)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int addEvent(EventVO eventVO) "+e);
		}
	return successFlag;
}


public int updateEvent(EventVO eventVO,int eventID)  {
	
	log.debug("Entry : public int updateEvent(EventVO eventVO,int eventID)");
	
	int successFlag=0;
	try {
	


		successFlag = eventDomain.updateEvent(eventVO,eventID);
		
		
		
		log.debug("Exit : public int updateEvent(EventVO eventVO,int eventID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int updateEvent(EventVO eventVO,int eventID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int updateEvent(EventVO eventVO,int eventID) "+e);
		}
	return successFlag;
}


public int updateEventDateFrequency(EventVO eventVO,int eventID)  {
	
	log.debug("Entry : public int updateEvent(EventVO eventVO,int eventID)");
	
	int successFlag=0;
	try {
		successFlag = eventDomain.updateEventDateFrequency(eventVO,eventID);		
		log.debug("Exit : public int updateEvent(EventVO eventVO,int eventID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int updateEventDateFrequency(EventVO eventVO,int eventID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int updateEventDateFrequency(EventVO eventVO,int eventID) "+e);
		}
	return successFlag;
}

public int updateEventFreuency(List eventList)  {
	
	log.debug("Entry : public int updateEventFreuency(List eventList)");
	
	int successFlag=0;
	try {
	


		successFlag = eventDomain.updateEventFreuency(eventList);
		
		
		
		log.debug("Exit : public int updateEventFreuency(List eventList)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int updateEventFreuency(List eventList)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int updateEventFreuency(EventVO eventVO,int eventID) "+e);
		}
	return successFlag;
}



public int deleteEvent(int eventID)  {
	
	log.debug("Entry : public int deleteEvent(int eventID)");
	
	int successFlag=0;
	try {
	


		successFlag = eventDomain.deleteEvent(eventID);
		
		
		
		log.debug("Exit : public int deleteEvent(int eventID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int deleteEvent(int eventID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int deleteEvent(int eventID)"+e);
		}
	return successFlag;
}


public int sendAuditDocReminderEvent(EventVO eventVO)  {
	
	log.debug("Entry : public int sendAuditDocReminderEvent(EventVO eventVO) ");
	
	int successFlag=0;
	try {
	


		successFlag = eventDomain.sendAuditDocReminderEvent(eventVO);
		
		
		
		log.debug("Exit : public int sendAuditDocReminderEvent(EventVO eventVO) ");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int sendAuditDocReminderEvent(EventVO eventVO) "+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int sendAuditDocReminderEvent(EventVO eventVO) "+e);
		}
	return successFlag;
}

public int sendTenantExpiredReminder(EventVO eventVO)  {
	
	log.debug("Entry : public int sendTenantExpiredReminder(EventVO eventVO) ");
	
	int successFlag=0;
	try {
	


		successFlag = eventDomain.sendTenantExpiredReminder(eventVO);
		
		
		
		log.debug("Exit : public int sendTenantExpiredReminder(EventVO eventVO)) ");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int sendTenantExpiredReminder(EventVO eventVO) "+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int sendTenantExpiredReminder(EventVO eventVO) "+e);
		}
	return successFlag;
}

public int sendTenantAboutTOExpireReminder(EventVO eventVO)  {
	
	log.debug("Entry : public int sendTenantAboutTOExpireReminder(EventVO eventVO) ");
	
	int successFlag=0;
	try {
	


		successFlag = eventDomain.sendTenantAboutToExpireReminder(eventVO);
		
		
		
		log.debug("Exit : public int sendTenantAboutTOExpireReminder(EventVO eventVO) ");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int sendTenantAboutTOExpireReminder(EventVO eventVO) "+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int sendTenantAboutTOExpireReminder(EventVO eventVO) "+e);
		}
	return successFlag;
}

public int sendSubscriptionExpiringReminder()  {
	
	log.debug("Entry : public int sendSubscriptionExpiringReminder() ");
	
	int successFlag=0;
	try {
			successFlag = eventDomain.sendSubscriptionExpiringSoonReminder();
			
		log.debug("Exit : public int sendSubscriptionExpiringReminder() ");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int sendSubscriptionExpiringReminder() "+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int sendSubscriptionExpiringReminder() "+e);
		}
	return successFlag;
}

public int sendSubscriptionExpiredReminder(int appID)  {
	
	log.debug("Entry : public int sendSubscriptionExpiredReminder(int appID) ");
	
	int successFlag=0;
	try {
			successFlag = eventDomain.sendSubscriptionExpiredReminder(appID);
			
		log.debug("Exit : public int sendSubscriptionExpiredReminder(int appID) ");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int sendSubscriptionExpiredReminder(int appID) "+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int sendSubscriptionExpiredReminder(EventVO eventVO) "+e);
		}
	return successFlag;
}

public NotificationVO sendEmailandPushNotificationsToAGroup(EmailMessage emailVO){
	
	NotificationVO notificationVO=new NotificationVO();
	log.debug("Entry : public NotificationVO sendEmailandPushNotificationsToAGroup(EmailMessage emailVO)");
	
	try {
		notificationVO=eventDomain.sendEmailandPushNotificationsToAGroup(emailVO);
		
	 }catch(Exception e) {
	    	log.error("Exception in public NotificationVO sendEmailandPushNotificationsToAGroup(EmailMessage emailVO) "+e);
	 }   
		
		
	log.debug("Exit : public NotificationVO sendEmailandPushNotificationsToAGroup(EmailMessage emailVO)");
	return notificationVO;
} 

public NotificationVO sendGroupSMSNotificationsToAGroup(SMSMessage smsVO){
	
	NotificationVO notificationVO=new NotificationVO();
	log.debug("Entry : public NotificationVO sendGroupSMSNotificationsToAGroup(EmailMessage emailVO)");
	
	try {
		notificationVO=eventDomain.sendGroupSMSNotificationsToAGroup(smsVO);
		
	 }catch(Exception e) {
	    	log.error("Exception in public NotificationVO sendGroupSMSNotificationsToAGroup(EmailMessage emailVO) "+e);
	 }   
		
		
	log.debug("Exit : public NotificationVO sendGroupSMSNotificationsToAGroup(EmailMessage emailVO)");
	return notificationVO;
} 

public List getNotificationsListOfPaymentReminders(int days)  {
	
	log.debug("Entry : public List getNotificationsListOfPaymentReminders()");
	
	List eventList = null;
	try {
	
		eventList = eventDomain.getNotificationsListOfPaymentReminders(days);
		log.debug("in function getEvent()");
		
		if (eventList.size() != 0) {
			log.debug("size Of the List" + eventList.size());
			
		}
		
		log.debug("Exit : public List getNotificationsListOfPaymentReminders");	
	
	} catch (Exception e) {
			
			log.error("DataAccessException :"+e);
		}
	return eventList;
}


/**
 * @param eventDomain the eventDomain to set
 */
public void setEventDomain(EventDomain eventDomain) {
	this.eventDomain = eventDomain;
}

}
