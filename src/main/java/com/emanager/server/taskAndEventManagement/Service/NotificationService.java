package com.emanager.server.taskAndEventManagement.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.taskAndEventManagement.Domain.NotificationDomain;
import com.emanager.server.taskAndEventManagement.valueObject.EventVO;
import com.emanager.server.taskAndEventManagement.valueObject.NotificationVO;
import com.emanager.server.taskAndEventManagement.valueObject.SMSGatewayVO;
import com.emanager.server.userManagement.valueObject.UserVO;

public class NotificationService {
	
	
	static Logger log = Logger.getLogger(NotificationService.class.getName());
	private NotificationDomain notificationDomain;
	

	
public NotificationVO getMemberBalance(EventVO eventVO)  {
		
		log.debug("Entry : public NotificationVO getMemberBalance(EmailMessage eventVO) ");
		NotificationVO notificationVO=new NotificationVO();
		
		try {		
			
			notificationVO=notificationDomain.getMemberBalance(eventVO);	
			
			
			log.debug("Exit : public NotificationVO getMemberBalance(EventVO eventVO)");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException : public NotificationVO getMemberBalance(EventVO eventVO)"+e);
		}
		 catch (Exception e) {
				
				log.error("Exception : public NotificationVO getMemberBalance(EventVO eventVO)"+e);
			}
		return notificationVO;
	}

public NotificationVO getMemberBalanceForSociety(EventVO eventVO)  {
		
		log.debug("Entry : public NotificationVO getMemberBalance(int memberID) ");
		NotificationVO notificationVO=new NotificationVO();
		
		try {
		
			
			notificationVO=notificationDomain.getMemberBalanceForSociety(eventVO);

			
			
			
			
			log.debug("Exit : public NotificationVO getMemberBalance(int memberID)");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException : public NotificationVO getMemberBalance(int memberID)"+e);
		}
		 catch (Exception e) {
				
				log.error("Exception : public NotificationVO getMemberBalance(int memberID)"+e);
			}
		return notificationVO;
	}
	
public NotificationVO getCustomerBalance(EventVO eventVO)  {
	
	log.debug("Entry : public NotificationVO getCustomerBalance(EventVO eventVO)  ");
	NotificationVO notificationVO=new NotificationVO();
	
	try {
	notificationVO=notificationDomain.getCustomerBalance(eventVO);
		log.debug("Exit : public NotificationVO getCustomerBalance(EventVO eventVO) ");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public NotificationVO getCustomerBalance(EventVO eventVO) "+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public NotificationVO getCustomerBalance(EventVO eventVO) "+e);
		}
	return notificationVO;
}

public NotificationVO sendCustomerBillPaymentReminderOfOrg(int orgID)  {
	
	log.debug("Entry : public NotificationVO sendCustomerBillPaymentReminderOfOrg(int orgID) ");
	NotificationVO notificationVO=new NotificationVO();
	
	try {
	notificationVO=notificationDomain.getCustomerBalanceForOrg(orgID);
		log.debug("Exit : public NotificationVO sendCustomerBillPaymentReminderOfOrg(int orgID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public NotificationVO sendCustomerBillPaymentReminderOfOrg(int orgID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public NotificationVO sendCustomerBillPaymentReminderOfOrg(int orgID)"+e);
		}
	return notificationVO;
}


	public NotificationVO getNominationStatusForMember(int memberID)  {
		
		log.debug("Entry : public NotificationVO getNominationStatusForMember(int memberID) ");
		NotificationVO notificationVO=new NotificationVO();
		try {
		
			
		 notificationVO=notificationDomain.getNominationStatusForMember(memberID);

			
			
			
			
			log.debug("Exit : public NotificationVO getNominationStatusForMember(int memberID)");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException : public NotificationVO getNominationStatusForMember(int memberID)"+e);
		}
		 catch (Exception e) {
				
				log.error("Exception : public NotificationVO getNominationStatusForMember(int memberID)"+e);
			}
		
		return notificationVO;
	}

public NotificationVO sendNominationReminderToSociety(EventVO eventVO)  {
		
		log.debug("Entry : public NotificationVO sendNominationReminderToSociety(int societyID) ");
		NotificationVO notificationVO=new NotificationVO();
		try {
		
			
		 notificationVO=notificationDomain.sendNominationReminderToSociety(eventVO);

			
			
			
			
			log.debug("Exit : public NotificationVO sendNominationReminderToSociety(int societyID)");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException : public NotificationVO sendNominationReminderToSociety(int societyID)"+e);
		}
		 catch (Exception e) {
				
				log.error("Exception : public NotificationVO sendNominationReminderToSociety(int societyID)"+e);
			}
		
		return notificationVO;
	}
	


public NotificationVO sendDocumentReminder(EventVO eventVO)  {
	
	log.debug("Entry : public NotificationVO sendDocumentReminder(EventVO eventVO)");
	NotificationVO notificationVO=new NotificationVO();
	try {
	
		
	 notificationVO=notificationDomain.sendDocumentReminder(eventVO);
	
		
		
		log.debug("Exit : public NotificationVO sendDocumentReminder(EventVO eventVO)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public NotificationVO sendDocumentReminder(EventVO eventVO)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public NotificationVO sendDocumentReminder(EventVO eventVO)"+e);
		}
	
	return notificationVO;
}
	//========Send Bank Statement request=========//
	public int sendBankStatementRequestReminder(EventVO eventVO)  {
	
	log.debug("Entry : public int sendBankStatementRequestReminder(EventVO eventVO)");
	int successFlag=0;
	try {
	
		
	 successFlag=notificationDomain.sendBankStatementRequestReminder(eventVO);
	
		
		
		log.debug("Exit : public int sendBankStatementRequestReminder(EventVO eventVO)");	
		} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int sendBankStatementRequestReminder(EventVO eventVO)"+e);
		}
	 	catch (Exception e) {
			
			log.error("Exception : public int sendBankStatementRequestReminder(EventVO eventVO)"+e);
	 	}
	
		return successFlag;
		}

	
		//========Send Cash balance to Org=========//
		public int sendCashBalanceReminder(EventVO eventVO)  {
		
		log.debug("Entry : public int sendCashBalanceReminder(EventVO eventVO)");
		int successFlag=0;
		try {
		
			
		 successFlag=notificationDomain.sendCashBalanceReminder(eventVO);
		
			
			
			log.debug("Exit : public int sendCashBalanceReminder(EventVO eventVO)");	
			} catch (DataAccessException e) {
		
			log.error("DataAccessException : public int sendCashBalanceReminder(EventVO eventVO)"+e);
			}
		 	catch (Exception e) {
				
				log.error("Exception : public int sendCashBalanceReminder(EventVO eventVO)"+e);
		 	}
		
			return successFlag;
			}
	
		
	

public NotificationVO sendDocumentReminderToSociety(EventVO eventVO)  {
	
	log.debug("Entry : public NotificationVO sendDocumentReminderToSociety(EventVO eventVO)  ");
	NotificationVO notificationVO=new NotificationVO();
	try {
	
		
	 notificationVO=notificationDomain.sendDocumentReminderToSociety(eventVO);

				
		log.debug("Exit : public NotificationVO sendDocumentReminderToSociety(EventVO eventVO) ");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public NotificationVO sendDocumentReminderToSociety(EventVO eventVO) "+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public NotificationVO sendDocumentReminderToSociety(EventVO eventVO) "+e);
		}
	
	return notificationVO;
}


public NotificationVO sendSuspenseStatusToSociety(EventVO eventVO)  {
	
	log.debug("Entry : public NotificationVO sendSuspenseStatusToSociety(int societyID) ");
	NotificationVO notificationVO=new NotificationVO();
	try {
	
		
	 notificationVO=notificationDomain.sendSuspenseStatusToSociety(eventVO);

				
		log.debug("Exit : public NotificationVO sendSuspenseStatusToSociety(int societyID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public NotificationVO sendSuspenseStatusToSociety(int societyID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public NotificationVO sendSuspenseStatusToSociety(int societyID)"+e);
		}
	
	return notificationVO;
}

public NotificationVO sendPendingTransactionStatusToSociety(EventVO eventVO)  {
	
	log.debug("Entry : public NotificationVO sendPendingTransactionStatusToSociety(int societyID) ");
	NotificationVO notificationVO=new NotificationVO();
	try {
	
		
	 notificationVO=notificationDomain.sendPendingTransactionStatusToSociety(eventVO);

				
		log.debug("Exit : public NotificationVO sendPendingTransactionStatusToSociety(int societyID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public NotificationVO sendPendingTransactionStatusToSociety(int societyID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public NotificationVO sendPendingTransactionStatusToSociety(int societyID)"+e);
		}
	
	return notificationVO;
}


public NotificationVO sendVirtualAccNumbers(int societyID)  {
	
	log.debug("Entry : public NotificationVO sendVirtualAccNumbers(int societyID)  ");
	NotificationVO notificationVO=new NotificationVO();
	
	try {
			notificationVO=notificationDomain.sendVirtualAccNumbers(societyID);
	
		log.debug("Exit : public NotificationVO sendVirtualAccNumbers(int societyID) ");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public NotificationVO sendVirtualAccNumbers(int societyID) "+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public NotificationVO sendVirtualAccNumbers(int societyID) "+e);
		}
	return notificationVO;
}


public NotificationVO sendNotificationOfPaymentReminder()  {
	
	log.debug("Entry : public NotificationVO sendNotificationOfPaymentReminder()  ");
	NotificationVO notificationVO=new NotificationVO();
	
	try {
			notificationVO=notificationDomain.sendNotificationOfPaymentReminder();
	
		log.debug("Exit : public NotificationVO sendNotificationOfPaymentReminder ");	
	
	} catch (Exception e) {
			
			log.error("Exception : public NotificationVO sendNotificationOfPaymentReminder() "+e);
		}
	return notificationVO;
}

public NotificationVO sendNotificationForLowSMSCredit()  {
	
	log.debug("Entry : public NotificationVO sendNotificationForLowSMSCredit()  ");
	NotificationVO notificationVO=new NotificationVO();
	
	try {
			notificationVO=notificationDomain.sendNotificationForLowSMSCredit();
	
		log.debug("Exit : public NotificationVO sendNotificationForLowSMSCredit ");	
	
	} catch (Exception e) {
			
			log.error("Exception : public NotificationVO sendNotificationForLowSMSCredit() "+e);
		}
	return notificationVO;
}

public NotificationVO sendSummaryNotifiactionToTechsupport(List responseList)  {
	
	log.debug("Entry : public NotificationVO sendSummaryNotifiactionToTechsupport(List responseList)   ");
	NotificationVO notificationVO=new NotificationVO();
	
	try {
			notificationDomain.sendSummaryNotifiactionToTechsupport(responseList);
	
		log.debug("Exit : public NotificationVO sendSummaryNotifiactionToTechsupport(List responseList)  ");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public NotificationVO sendSummaryNotifiactionToTechsupport(List responseList)  "+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public NotificationVO sendSummaryNotifiactionToTechsupport(List responseList)  "+e);
		}
	return notificationVO;
}

   public NotificationVO SendNotifications(NotificationVO notificationVO){
	   
	      try {
	       log.debug("Entry : private NotificationVO SendNotifications");
	         
	       notificationVO=notificationDomain.SendNotifications(notificationVO);
	  
			} catch (Exception e) {
				log.error("Exception occurred in NotificationVO SendNotifications "+e);
			}
			log.debug("Exit : private NotificationVO SendNotifications");
			
			
		return notificationVO;
	}
	
   
   //===== Get SMS Gateway Details =========//
   public SMSGatewayVO getSMSGatewayDetails(int orgID)  {
		
		log.debug("Entry : public SMSGatewayVO getSMSGatewayDetails(int orgID) ");
		SMSGatewayVO smsGatewayVO=new SMSGatewayVO();
		try {
		
			
			smsGatewayVO=notificationDomain.getSMSGatewayDetails(orgID);

					
			log.debug("Exit :public SMSGatewayVO getSMSGatewayDetails(int orgID)");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException : public SMSGatewayVO getSMSGatewayDetails(int orgID)"+e);
		}
		 catch (Exception e) {
				
				log.error("Exception : public SMSGatewayVO getSMSGatewayDetails(int orgID)"+e);
			}
		
		return smsGatewayVO;
	}
   
   //===== API to allot monthly SMS credits =========//
   public int allotSMSCredits()  {
		
		log.debug("Entry : public SMSGatewayVO allotSMSCredits ");
		int success=0;
		try {
		
			
			success=notificationDomain.allotSMSCredits();

					
			log.debug("Exit :public SMSGatewayVO allotSMSCredits");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException : public SMSGatewayVO allotSMSCredits"+e);
		}
		 catch (Exception e) {
				
				log.error("Exception : public SMSGatewayVO allotSMSCredits"+e);
			}
		
		return success;
	}
   
 //===== Add SMS Credit  =========//
   public int addSMSCredit(SMSGatewayVO smsVO)  {
		int updateFlag=0;
		log.debug("Entry : public int addSMSCreditDetails(SMSGatewayVO smsVO) ");
		SMSGatewayVO smsGatewayVO=new SMSGatewayVO();
		try {
		
			updateFlag=notificationDomain.addSMSCredit(smsVO);
			
					
			log.debug("Exit : public int addSMSCreditDetails(SMSGatewayVO smsVO)");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException : public int addSMSCreditDetails(SMSGatewayVO smsVO)"+e);
		}
		 catch (Exception e) {
				
				log.error("Exception : public int addSMSCreditDetails(SMSGatewayVO smsVO)"+e);
			}
		
		return updateFlag;
	}

   
   //===== Deduct SMS Credit  =========//
   public int deductSMSCredit(SMSGatewayVO smsVO)  {
		int updateFlag=0;
		log.debug("Entry : public int deductSMSCredit(SMSGatewayVO smsVO) ");
		SMSGatewayVO smsGatewayVO=new SMSGatewayVO();
		try {
		
			updateFlag=notificationDomain.deductSMSCredit(smsVO);
			
					
			log.debug("Exit : public int deductSMSCredit(SMSGatewayVO smsVO)");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException : public int deductSMSCredit(SMSGatewayVO smsVO)"+e);
		}
		 catch (Exception e) {
				
				log.error("Exception : public int deductSMSCredit(SMSGatewayVO smsVO)"+e);
			}
		
		return updateFlag;
	}
   
   //===== Get Profile Details in list format =========//
   public List getProfileDetailedList(NotificationVO notificationVO)  {
		List userList=new ArrayList<UserVO>();
		log.debug("Entry :  public List getProfileDetailedList(NotificationVO notificationVO) ");
		try {
		
			
			userList=notificationDomain.getProfileDetailedList(notificationVO);

					
			log.debug("Exit :  public List getProfileDetailedList(NotificationVO notificationVO)");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException :  public List getProfileDetailedList(NotificationVO notificationVO)"+e);
		}
		 catch (Exception e) {
				
				log.error("Exception :  public List getProfileDetailedList(NotificationVO notificationVO)"+e);
			}
		
		return userList;
	}
   
   public int insertAuditActivity(String userID,String appID,String orgID,String activity,String module){
	   int flag = 0;
	   log.debug("Entry : public int insertAuditActivity(String userID,String appID,String orgID,String activity,String module) ");
		try{
			
			flag=notificationDomain.insertAuditActivity(userID,appID,orgID,activity,module);
			
		}catch(Exception ex){
			log.error("Exception in insertAuditActivity : "+ex);            
	    }
		log.debug("Exit :public int insertAuditActivity(String userID,String appID,String orgID,String activity,String module) ");
		return flag;	
	}
   
	/**
	 * @param notificationDomain the notificationDomain to set
	 */
	public void setNotificationDomain(NotificationDomain notificationDomain) {
		this.notificationDomain = notificationDomain;
	}
	
	

	




}
