package com.emanager.server.taskAndEventManagement.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.taskAndEventManagement.valueObject.TaskVO;



public class TaskDAO {
	
	static Logger log = Logger.getLogger(TaskDAO.class.getName());
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	DateUtility dateUtil=new DateUtility();
	
	
public List getTaskList(int orgID)  {
		
		log.debug("Entry : public List getTaskList(int orgID)");
		
		List taskList = null;
		try {
			String strCondition="";
			if(orgID!=0){
				strCondition="and t.org_id=:orgID ";
			}
		
			
		String strSQL = "SELECT t.*,tt.task_type FROM task_details t,task_type_details tt WHERE t.task_type_id=tt.task_type_id "+strCondition+" and t.status='Active' and t.running_state=0 ;";

		log.debug("Query is  : " + strSQL);

		Map hMap = new HashMap();
		hMap.put("orgID", orgID);

	
			RowMapper RMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					TaskVO taskVO = new TaskVO();
					taskVO.setTaskID(rs.getInt("task_id"));
					taskVO.setOrgID(rs.getInt("org_id"));
					taskVO.setTaskTypeID(rs.getInt("task_type_id"));
					taskVO.setTaskType(rs.getString("task_type"));
					taskVO.setTaskFrequency(rs.getString("task_frequency"));
					taskVO.setTaskStartDate(rs.getString("task_start_date"));
					
								
					
					return taskVO;

				}

			};

			taskList = namedParameterJdbcTemplate.query(strSQL, hMap, RMapper );
			
			
			if (taskList.size() != 0) {
				log.debug("size Of the List" + taskList.size());
				
			}
			
			log.debug("Exit : public List getTaskList(int orgID)");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException : public List getTaskList(int orgID)"+e);
		}
		 catch (Exception e) {
				
				log.error("Exception : public List getTaskList(int orgID)"+e);
			}
		return taskList;
	}
	
	

	public int addTask(TaskVO taskVO)  {
	
	log.debug("Entry : public int addTask(TaskVO taskVO)");
	
	int successFlag=0;
	try {
		Date dt=dateUtil.findCurrentDate();
		java.sql.Date currentDate = new java.sql.Date(dt.getTime());
		java.sql.Timestamp currentTimeStamp=new java.sql.Timestamp(dt.getTime());

		String sql="INSERT INTO task_details (org_id ,task_type_id ,task_frequency ,task_start_date ,insert_date) VALUES" +
				   "(:orgID, :taskTypeID, :taskFrequency, :taskStartDate, :insertDate ); ";
		log.debug("query : " + sql);
		
		Map namedParam = new HashMap();
		namedParam.put("orgID",taskVO.getOrgID());
		namedParam.put("taskTypeID",taskVO.getTaskTypeID());
		namedParam.put("taskFrequency",taskVO.getTaskFrequency());			
		namedParam.put("taskStartDate",taskVO.getTaskStartDate());	
		namedParam.put("insertDate",currentTimeStamp);
		
		
		successFlag=namedParameterJdbcTemplate.update(sql,namedParam);
		
		
		
		log.debug("Exit : public int addTask(TaskVO taskVO)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int addTask(TaskVO taskVO)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int addTask(TaskVO taskVO) "+e);
		}
	return successFlag;
}


public int updateTask(TaskVO taskVO,int taskID)  {
	
	log.debug("Entry : public int updateTask(TaskVO taskVO,int taskID)");
	
	int successFlag=0;
	try {
	

		 String str = "Update task_details set task_type_id=:taskTypeID, task_frequency=:taskFrequency, task_start_date=:taskStartDate  where task_id=:taskID ";
		   
		   
		 Map namedParam = new HashMap();
			
			namedParam.put("taskID", taskID);
			namedParam.put("taskTypeID",taskVO.getTaskTypeID());			
			namedParam.put("taskFrequency",taskVO.getTaskFrequency());	
			namedParam.put("taskStartDate",taskVO.getTaskStartDate());
			
		   
		   
		   successFlag=namedParameterJdbcTemplate.update(str, namedParam);
		
		
		
		
		log.debug("Exit : public int updateTask(TaskVO taskVO,int taskID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int updateTask(TaskVO taskVO,int taskID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int updateTask(TaskVO taskVO,int taskID) "+e);
		}
	return successFlag;
}


public int updateTaskStatus(TaskVO taskVO)  {
	
	log.debug("Entry : public int updateTaskStatus(TaskVO taskVO)");
	String condition="";
	int successFlag=0;
	try {
		if(taskVO.getTaskFrequency().equalsIgnoreCase("Daily")){
			condition="INTERVAL 1 DAY";
		}else if(taskVO.getTaskFrequency().equalsIgnoreCase("weekly")){
			condition="INTERVAL 1 Week";
		}else if(taskVO.getTaskFrequency().equalsIgnoreCase("halfmonthly")){
			condition="INTERVAL 15 day";
		}else if(taskVO.getTaskFrequency().equalsIgnoreCase("HalfYearly")){
			condition="INTERVAL 6 MONTH";
		}else if(taskVO.getTaskFrequency().equalsIgnoreCase("Monthly")){
			condition="INTERVAL 1 MONTH";
		}else if(taskVO.getTaskFrequency().equalsIgnoreCase("BIMONTHLY")){
			condition="INTERVAL 2 MONTH";
		}
		else if(taskVO.getTaskFrequency().equalsIgnoreCase("Quarterly")){
			condition="INTERVAL 3 MONTH";
		}else if(taskVO.getTaskFrequency().equalsIgnoreCase("HalfYearly")){
			condition="INTERVAL 6 MONTH";
		}else if(taskVO.getTaskFrequency().equalsIgnoreCase("Yearly")){
			condition="INTERVAL 1 YEAR";
		}

		 String str = "Update task_details set task_start_date=DATE_ADD(task_start_date, "+condition+" ), running_state=0 where task_id=:taskID and org_id=:orgID; ";
		   
		   
		 Map namedParam = new HashMap();
			
			namedParam.put("taskID", taskVO.getTaskID());
			namedParam.put("orgID",taskVO.getOrgID());
			
		   
		   
		   successFlag=namedParameterJdbcTemplate.update(str, namedParam);
		
		
		
		
		log.debug("Exit : public int updateTaskStatus(TaskVO taskVO)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int updateTaskStatus(TaskVO taskVO)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int updateTaskStatus(TaskVO taskVO) "+e);
		}
	return successFlag;
}



public int updateTaskRunningStatus(TaskVO taskVO)  {
	
	log.debug("Entry : public int updateTaskRunningStatus(TaskVO taskVO)");
	String condition="";
	int successFlag=0;
	try {
		

		 String str = "Update task_details set running_state=1 where task_id=:taskID and org_id=:orgID; ";
		   
		   
		 Map namedParam = new HashMap();
			
			namedParam.put("taskID", taskVO.getTaskID());
			namedParam.put("orgID",taskVO.getOrgID());
			
		   
		   
		   successFlag=namedParameterJdbcTemplate.update(str, namedParam);
		
		
		
		
		log.debug("Exit : public int updateTaskRunningStatus(TaskVO taskVO)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int updateTaskRunningStatus(TaskVO taskVO)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int updateTaskRunningStatus(TaskVO taskVO) "+e);
		}
	return successFlag;
}






public int deleteTask(int taskID)  {
	
	log.debug("Entry : public int deleteTask(int taskID)");
	
	int successFlag=0;
	try {
	
		 
		   String str = "Update task_details set status=:taskStatus  where task_id=:taskID";
		   
		   
		   Map hMap=new HashMap();
		
		   hMap.put("taskID", taskID);
		   hMap.put("taskStatus", "InActive");
		   
		   successFlag=namedParameterJdbcTemplate.update(str, hMap);
		
		
		
		log.debug("Exit : public int deleteTask(int taskID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int deleteTask(int taskID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int deleteTask(int taskID)"+e);
		}
	return successFlag;
}



/**
 * @param namedParameterJdbcTemplate the namedParameterJdbcTemplate to set
 */
public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
	this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
}

	

}
