package com.emanager.server.taskAndEventManagement.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.taskAndEventManagement.valueObject.EventVO;
import com.emanager.server.userManagement.valueObject.UserVO;


public class EventDAO {
	static Logger log = Logger.getLogger(EventDAO.class.getName());
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	DateUtility dateUtil=new DateUtility();
	
	
	public List getEventList(int societyID)  {
		
		log.debug("Entry : public List getEventList(int societyID)");
		
		List eventList = null;
		try {
		String strCondition="";
		if(societyID!=0){
			strCondition="and society_details.society_id=:societyID ";
		}
			
		String strSQL = "select event_scheduler.*, society_details.* "
				+ "from  event_scheduler , society_details	"
				+ "where event_scheduler.society_id = society_details.society_id "+strCondition+" "
				+ "and event_scheduler.NEXT_DATE<=curdate() and event_scheduler.event_status = 'active'  AND event_scheduler.is_deleted=0 ;";

		log.debug("Query is  : " + strSQL);

		Map hMap = new HashMap();
		hMap.put("societyID", societyID);

	
			RowMapper RMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					EventVO eventVO = new EventVO();
					eventVO.setEventID(rs.getInt("event_id"));
					eventVO.setEventType(rs.getString("event_type").trim());
					eventVO.setSetDate(rs.getString("set_date"));
					eventVO.setNextDate(rs.getString("next_date"));
					eventVO.setLastDate(rs.getString("last_date"));
					eventVO.setEmailFrequency(rs.getString("emailFrequency"));
					eventVO.setSocietyID(rs.getInt("society_id"));
					eventVO.setTemplateID(rs.getString("temp_id"));
					eventVO.setSocietyName(rs.getString("society_name"));
					eventVO.setEventSubject(rs.getString("event_subject"));
					eventVO.setTransactionType(rs.getString("transaction_type"));
					eventVO.setMessage(rs.getString("message"));
					eventVO.setSendEmail(rs.getInt("send_email"));
					eventVO.setSendSMS(rs.getInt("send_sms"));
					eventVO.setSmsTemplate(rs.getString("sms_template"));
					eventVO.setSocietyShortName(rs.getString("short_name"));
					eventVO.setSendToGroup(rs.getInt("sendto_group"));
					eventVO.setEmailID(rs.getString("email_id"));
					eventVO.setBccMail(rs.getString("bcc_mail"));
					eventVO.setIsBcc(rs.getInt("is_bcc"));
					eventVO.setOnetime(rs.getInt("onetime"));
					eventVO.setSendSummary(rs.getInt("send_summary"));
					eventVO.setCcEmail(rs.getString("cc_email"));
					eventVO.setIsManual(rs.getInt("is_manual"));
								
					
					
					return eventVO;

				}

			};

			eventList = namedParameterJdbcTemplate.query(strSQL, hMap, RMapper );
			log.debug("in function getEvent()");
			
			if (eventList.size() != 0) {
				log.debug("size Of the List" + eventList.size());
				
			}
			
			log.debug("Exit : public List getEventList(int societyID)");	
		} catch (DataAccessException e) {
		
			log.error("DataAccessException :"+e);
		}
		 catch (Exception e) {
				
				log.error("DataAccessException :"+e);
			}
		return eventList;
	}
	
	
public List getEventListForOrg(int orgID)  {
		
		log.debug("Entry : public List getEventListForOrg(int orgID)");
		
		List eventList = null;
		try {
		String strCondition="";
		if(orgID!=0){
			strCondition=" and s.society_id=:orgID ";
		}
			
		String strSQL = " SELECT e.*, s.*,m.full_name AS createdBy,mi.full_name AS updatedBy   FROM  event_scheduler e, society_details s, um_users m ,um_users mi "
				      + " WHERE e.society_id = s.society_id AND m.id=e.created_by AND mi.id=e.updated_by "+strCondition+" "
				      + " AND e.is_deleted=0 ;";

		log.debug("Query is  : " + strSQL);

		Map hMap = new HashMap();
		hMap.put("orgID", orgID);	
	
			RowMapper RMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					EventVO eventVO = new EventVO();
					eventVO.setEventID(rs.getInt("event_id"));
					eventVO.setEventType(rs.getString("event_type").trim());
					eventVO.setSetDate(rs.getString("set_date"));
					eventVO.setNextDate(rs.getString("next_date"));
					eventVO.setLastDate(rs.getString("last_date"));
					eventVO.setEmailFrequency(rs.getString("emailFrequency"));
					eventVO.setSocietyID(rs.getInt("society_id"));
					eventVO.setTemplateID(rs.getString("temp_id"));
					eventVO.setSocietyName(rs.getString("society_name"));
					eventVO.setEventSubject(rs.getString("event_subject"));
					eventVO.setTransactionType(rs.getString("transaction_type"));
					eventVO.setMessage(rs.getString("message"));
					eventVO.setSendEmail(rs.getInt("send_email"));
					eventVO.setSendSMS(rs.getInt("send_sms"));
					eventVO.setSmsTemplate(rs.getString("sms_template"));
					eventVO.setSocietyShortName(rs.getString("short_name"));
					eventVO.setSendToGroup(rs.getInt("sendto_group"));
					eventVO.setEmailID(rs.getString("email_id"));
					eventVO.setBccMail(rs.getString("bcc_mail"));
					eventVO.setIsBcc(rs.getInt("is_bcc"));
					eventVO.setOnetime(rs.getInt("onetime"));
					eventVO.setSendSummary(rs.getInt("send_summary"));
					eventVO.setCcEmail(rs.getString("cc_email"));
					eventVO.setIsManual(rs.getInt("is_manual"));		
					eventVO.setEventStatus(rs.getString("event_status"));		
					eventVO.setCreatedBy(rs.getInt("created_by"));
					eventVO.setUpdatedBy(rs.getInt("updated_by"));
					eventVO.setCreaterName(rs.getString("createdBy"));	
					eventVO.setUpdaterName(rs.getString("updatedBy"));	
					eventVO.setCreateDate(rs.getString("create_date"));	
					eventVO.setUpdateDate(rs.getString("update_date"));	
					return eventVO;

				}

			};

			eventList = namedParameterJdbcTemplate.query(strSQL, hMap, RMapper );
			log.debug("in function getEvent()");
			
			if (eventList.size() != 0) {
				log.debug("size Of the List" + eventList.size());
				
			}
			
			log.debug("Exit : public List getEventListForOrg(int orgID)");	
		} catch (EmptyResultDataAccessException ex) {
			
			log.debug("No event found for this orgID :"+orgID);
		
		} catch (DataAccessException e) {
		
			log.error("DataAccessException :"+e);
		}
		catch (Exception e) {
				
				log.error("Exception :"+e);
		}
		return eventList;
	}
	
public List getPaymentReminderEventDetails(int societyID)  {
	
	log.debug("Entry : public List getEventList(int societyID)");
	
	List eventList = null;
	try {
	String strCondition="";
	if(societyID!=0){
		strCondition="and society_details.society_id=:societyID ";
	}
		
	String strSQL = "select event_scheduler.*, society_details.* "
			+ "from  event_scheduler , society_details	"
			+ "where event_scheduler.society_id = society_details.society_id "+strCondition+" "
			+ "and transaction_type='NOTPAID' order by event_id limit 1;";

	log.debug("Query is  : " + strSQL);

	Map hMap = new HashMap();
	hMap.put("societyID", societyID);


		RowMapper RMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				EventVO eventVO = new EventVO();
				eventVO.setEventID(rs.getInt("event_id"));
				eventVO.setEventType(rs.getString("event_type").trim());
				eventVO.setSetDate(rs.getString("set_date"));
				eventVO.setNextDate(rs.getString("next_date"));
				eventVO.setLastDate(rs.getString("last_date"));
				eventVO.setEmailFrequency(rs.getString("emailFrequency"));
				eventVO.setSocietyID(rs.getInt("society_id"));
				eventVO.setTemplateID(rs.getString("temp_id"));
				eventVO.setSocietyName(rs.getString("society_name"));
				eventVO.setEventSubject(rs.getString("event_subject"));
				eventVO.setTransactionType(rs.getString("transaction_type"));
				eventVO.setMessage(rs.getString("message"));
				eventVO.setSendEmail(rs.getInt("send_email"));
				eventVO.setSendSMS(rs.getInt("send_sms"));
				eventVO.setSmsTemplate(rs.getString("sms_template"));
				eventVO.setSocietyShortName(rs.getString("short_name"));
				eventVO.setSendToGroup(rs.getInt("sendto_group"));
				eventVO.setEmailID(rs.getString("email_id"));
				eventVO.setBccMail(rs.getString("bcc_mail"));
				eventVO.setIsBcc(rs.getInt("is_bcc"));
				eventVO.setOnetime(rs.getInt("onetime"));
				eventVO.setSendSummary(rs.getInt("send_summary"));
				eventVO.setCcEmail(rs.getString("cc_email"));
				eventVO.setIsManual(rs.getInt("is_manual"));
							
				
				
				return eventVO;

			}

		};

		eventList = namedParameterJdbcTemplate.query(strSQL, hMap, RMapper );
		log.debug("in function getEvent()");
		
		if (eventList.size() != 0) {
			log.debug("size Of the List" + eventList.size());
			
		}
		
		log.debug("Exit : public List getEventList(int societyID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException :"+e);
	}
	 catch (Exception e) {
			
			log.error("DataAccessException :"+e);
		}
	return eventList;
}

public int addEvent(EventVO eventVO)  {
	
	log.debug("Entry : public int addEvent(EventVO eventVO)");
	
	int successFlag=0;
	try {
		Date dt=dateUtil.findCurrentDate();
		java.sql.Date currentDate = new java.sql.Date(dt.getTime());
		 final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(dt.getTime());

		String sql="INSERT INTO event_scheduler (society_id,member_id,temp_id,event_type,event_subject,emailFrequency,set_date,next_date,last_date,transaction_type,message,send_email,send_sms,onetime,sendto_group,email_id,mobile,send_summary,cc_email,is_manual,update_date,created_by,updated_by) VALUES" +
				   "(:societyID, :memberID, :templateID, :eventType, :eventSubject, :emailFrequency, :setDate, :nextDate, :lastDate, :transactionType ,:message, :sendEmail, :sendSms, :onetime, :sendToGroup, :email, :mobile,:sendSummary,:ccEmail,:isManual,:updateDate,:createdBy,:updatedBy); ";
		log.debug("query : " + sql);
		
		Map namedParam = new HashMap();
		namedParam.put("societyID",eventVO.getSocietyID());
		namedParam.put("memberID",eventVO.getCreatedBy());
		namedParam.put("templateID",eventVO.getTemplateID());			
		namedParam.put("eventType",eventVO.getEventType());	
		namedParam.put("eventSubject",eventVO.getEventSubject());
		namedParam.put("emailFrequency", eventVO.getEmailFrequency());
		namedParam.put("setDate", currentDate);
		namedParam.put("nextDate", eventVO.getNextDate());
		namedParam.put("lastDate", eventVO.getLastDate());
		namedParam.put("transactionType", eventVO.getTransactionType());
		namedParam.put("message", eventVO.getMessage());
		namedParam.put("sendEmail", eventVO.getSendEmail());
		namedParam.put("sendSms", eventVO.getSendSMS());
		namedParam.put("onetime", eventVO.getOnetime());
		namedParam.put("sendToGroup", eventVO.getSendToGroup());
		namedParam.put("email", eventVO.getEmailID());
		namedParam.put("mobile", eventVO.getMobile());
		namedParam.put("sendSummary", eventVO.getSendSummary());
		namedParam.put("ccEmail", eventVO.getCcEmail());
		namedParam.put("isManual", eventVO.getIsManual());
		namedParam.put("updateDate", currentTimestamp);
		namedParam.put("createdBy", eventVO.getCreatedBy());
		namedParam.put("updatedBy", eventVO.getUpdatedBy());
		
		successFlag=namedParameterJdbcTemplate.update(sql,namedParam);
		
		
		
		log.debug("Exit : public int addEvent(EventVO eventVO)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int addEvent(EventVO eventVO)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int addEvent(EventVO eventVO) "+e);
		}
	return successFlag;
}


public int updateEvent(EventVO eventVO,int eventID)  {
	
	log.debug("Entry : public int updateEvent(EventVO eventVO,int eventID)");
	
	int successFlag=0;
	try {
		Date dt=dateUtil.findCurrentDate();
		 final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(dt.getTime());
		 String str = " Update event_scheduler set temp_id=:templateID,event_type=:eventType,event_subject=:eventSubject ,emailFrequency=:emailFrequency ,next_date=:nextDate ,last_date=:lastDate ,transaction_type=:transactionType,message=:message ,send_email=:sendEmail, send_sms=:sendSMS ,onetime=:onetime, sendto_group=:sendToGroup ,email_id=:email , mobile=:mobile ,"
		 		    + " is_manual=:isManual, update_date=:updateDate, updated_by=:updatedBy  where event_id=:eventID ";
		   
		   
		 Map namedParam = new HashMap();
			
			namedParam.put("eventID", eventID);
			namedParam.put("templateID",eventVO.getTemplateID());			
			namedParam.put("eventType",eventVO.getEventType());	
			namedParam.put("eventSubject",eventVO.getEventSubject());
			namedParam.put("emailFrequency", eventVO.getEmailFrequency());
			namedParam.put("nextDate", eventVO.getNextDate());
			namedParam.put("lastDate", eventVO.getLastDate());
			namedParam.put("transactionType", eventVO.getTransactionType());
			namedParam.put("message", eventVO.getMessage());
			namedParam.put("sendEmail", eventVO.getSendEmail());
			namedParam.put("sendSms", eventVO.getSendSMS());
			namedParam.put("onetime", eventVO.getOnetime());
			namedParam.put("sendToGroup", eventVO.getSendToGroup());
			namedParam.put("email", eventVO.getEmailID());
			namedParam.put("mobile", eventVO.getMobile());
			namedParam.put("isManual",eventVO.getIsManual());
			namedParam.put("updateDate", currentTimestamp);			
			namedParam.put("updatedBy", eventVO.getUpdatedBy());
		   
		   successFlag=namedParameterJdbcTemplate.update(str, namedParam);
		
		
		
		
		log.debug("Exit : public int updateEvent(EventVO eventVO,int eventID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int updateEvent(EventVO eventVO,int eventID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int updateEvent(EventVO eventVO,int eventID) "+e);
		}
	return successFlag;
}

public int updateEventDateFrequency(EventVO eventVO,int eventID)  {
	
	log.debug("Entry : public int updateEventDateFrequency(EventVO eventVO,int eventID)");
	
	int successFlag=0;
	try {
		Date dt=dateUtil.findCurrentDate();
		 final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(dt.getTime());
		 String str = " Update event_scheduler set emailFrequency=:emailFrequency ,next_date=:nextDate ,"
		 		    + " event_status=:status, update_date=:updateDate, updated_by=:updatedBy  where event_id=:eventID ";
		   
		   
		 Map namedParam = new HashMap();
			
			namedParam.put("eventID", eventID);			
			namedParam.put("emailFrequency", eventVO.getEmailFrequency());
			namedParam.put("nextDate", eventVO.getNextDate());
			namedParam.put("status", eventVO.getEventStatus());
			namedParam.put("updateDate", currentTimestamp);			
			namedParam.put("updatedBy", eventVO.getUpdatedBy());
		   
		   successFlag=namedParameterJdbcTemplate.update(str, namedParam);
		
		
		log.debug("Exit : public int updateEventDateFrequency(EventVO eventVO,int eventID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int updateEventDateFrequency(EventVO eventVO,int eventID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int updateEventDateFrequency(EventVO eventVO,int eventID) "+e);
		}
	return successFlag;
}


public int updateEventFreuency(EventVO eventVO,int eventID)  {
	
	log.debug("Entry : public int updateEventFreuency(EventVO eventVO,int eventID)");
	int updateFlag=0;
	String sql="";
	String condition="";
	if(eventID!=0){
	try {
		if((eventVO.getEmailFrequency().equalsIgnoreCase("OneTime")||(eventVO.getEmailFrequency().equalsIgnoreCase("Once"))||(eventVO.getIsManual()==1))){
			sql="Update scheduled_charges set status='Inactive'  where id=:ID and org_id=:societyID ;";
		}else{
			if(eventVO.getEmailFrequency().equalsIgnoreCase("Daily")){
				condition="INTERVAL 1 DAY";
			}else 	if(eventVO.getEmailFrequency().equalsIgnoreCase("Weekly")){
				condition="INTERVAL 1 WEEK";
			}else if(eventVO.getEmailFrequency().equalsIgnoreCase("HlfMonthly")){
				condition="INTERVAL 15 DAY";
			}else	if(eventVO.getEmailFrequency().equalsIgnoreCase("Monthly")){
				condition="INTERVAL 1 MONTH";
			}else if(eventVO.getEmailFrequency().equalsIgnoreCase("BIMONTHLY")){
				condition="INTERVAL 2 MONTH";
			}
			else if(eventVO.getEmailFrequency().equalsIgnoreCase("Quarterly")){
				condition="INTERVAL 3 MONTH";
			}else if(eventVO.getEmailFrequency().equalsIgnoreCase("HalfYearly")){
				condition="INTERVAL 6 MONTH";
			}else if(eventVO.getEmailFrequency().equalsIgnoreCase("Yearly")){
				condition="INTERVAL 1 YEAR";
			}
			
			sql="Update event_scheduler set next_date=DATE_ADD(next_date, "+condition+" ) , onetime=0  where event_id=:ID and society_id=:societyID; ";
		}
		
		Map hashMap = new HashMap();

		hashMap.put("societyID",eventVO.getSocietyID());
		hashMap.put("ID", eventID);
		
		updateFlag=namedParameterJdbcTemplate.update(sql, hashMap);

	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int updateEventFreuency(EventVO eventVO,int eventID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int updateEventFreuency(EventVO eventVO,int eventID) "+e);
		}
	}
	return updateFlag;
}


public int deleteEvent(int eventID)  {
	
	log.debug("Entry : public int deleteEvent(int eventID)");
	
	int successFlag=0;
	try {
	
		 
		   String str = "Update event_scheduler set event_status=:eventStatus,is_deleted=1  where event_id=:eventID";
		   
		   
		   Map hMap=new HashMap();
		
		   hMap.put("eventID", eventID);
		   hMap.put("eventStatus", "InActive");
		   
		   successFlag=namedParameterJdbcTemplate.update(str, hMap);
		
		
		
		log.debug("Exit : public int deleteEvent(int eventID)");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException : public int deleteEvent(int eventID)"+e);
	}
	 catch (Exception e) {
			
			log.error("Exception : public int deleteEvent(int eventID)"+e);
		}
	return successFlag;
}

public EventVO getSummaryEventVO(int societyID,int eventID)  {
	
	log.debug("Entry : public EventVO getSummaryEventVO(int societyID,int eventID)");
	
	EventVO summaryEvent = new EventVO();
	try {

		
	String strSQL = "select * from event_summary_details where event_id=:eventID and society_id=:societyID  ;";


	log.debug("Query is  : " + strSQL);

	Map hMap = new HashMap();
	hMap.put("societyID", societyID);
	hMap.put("eventID", eventID);


		RowMapper RMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				EventVO eventVO = new EventVO();
				eventVO.setEventID(rs.getInt("event_id"));
			
				eventVO.setSocietyID(rs.getInt("society_id"));
				eventVO.setTemplateID(rs.getString("template_id"));
				
				eventVO.setEventSubject(rs.getString("event_subject"));
				eventVO.setEmailID(rs.getString("email_id"));
							
				
				
				return eventVO;

			}

		};

		summaryEvent = (EventVO)  namedParameterJdbcTemplate.queryForObject(strSQL, hMap, RMapper );
		log.debug("in function getEvent()");
		
	
		
		log.debug("Exit : public EventVO getSummaryEventVO(int societyID,int eventID)");	
	} catch (DataAccessException e) {
	
		log.info ("DataAccessException :  public EventVO getSummaryEventVO(int societyID,int eventID) : No summary event found for Org ID :"+societyID+"   "+e);
	}
	 catch (Exception e) {
			
			log.error("Exception :"+e);
		}
	return summaryEvent;
}
	
public List getNotificationsListOfPaymentReminders(int days)  {
	
	log.debug("Entry : public List getNotificationsListOfPaymentReminders()");
	
	List eventList = null;
	try {
	
		
	String strSQL = "select event_scheduler.*, society_details.* "
			+ "from  event_scheduler , society_details	"
			+ "where event_scheduler.society_id = society_details.society_id  AND event_scheduler.transaction_type='Notpaid' "
			+ " and date(event_scheduler.NEXT_DATE)=(CURDATE()+INTERVAL :days DAY) and event_scheduler.event_status = 'active'  AND event_scheduler.is_deleted=0 ;";

	log.debug("Query is  : " + strSQL);

	Map hMap = new HashMap();
	hMap.put("days", days);


		RowMapper RMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				EventVO eventVO = new EventVO();
				eventVO.setEventID(rs.getInt("event_id"));
				eventVO.setEventType(rs.getString("event_type").trim());
				eventVO.setSetDate(rs.getString("set_date"));
				eventVO.setNextDate(rs.getString("next_date"));
				eventVO.setLastDate(rs.getString("last_date"));
				eventVO.setEmailFrequency(rs.getString("emailFrequency"));
				eventVO.setSocietyID(rs.getInt("society_id"));
				eventVO.setTemplateID(rs.getString("temp_id"));
				eventVO.setSocietyName(rs.getString("society_name"));
				eventVO.setEventSubject(rs.getString("event_subject"));
				eventVO.setTransactionType(rs.getString("transaction_type"));
				eventVO.setMessage(rs.getString("message"));
				eventVO.setSendEmail(rs.getInt("send_email"));
				eventVO.setSendSMS(rs.getInt("send_sms"));
				eventVO.setSmsTemplate(rs.getString("sms_template"));
				eventVO.setSocietyShortName(rs.getString("short_name"));
				eventVO.setSendToGroup(rs.getInt("sendto_group"));
				eventVO.setEmailID(rs.getString("email_id"));
				eventVO.setBccMail(rs.getString("bcc_mail"));
				eventVO.setIsBcc(rs.getInt("is_bcc"));
				eventVO.setOnetime(rs.getInt("onetime"));
				eventVO.setSendSummary(rs.getInt("send_summary"));
				eventVO.setCcEmail(rs.getString("cc_email"));
				eventVO.setIsManual(rs.getInt("is_manual"));
							
				
				
				return eventVO;

			}

		};

		eventList = namedParameterJdbcTemplate.query(strSQL, hMap, RMapper );
		log.debug("in function getEvent()");
		
		if (eventList.size() != 0) {
			log.debug("size Of the List" + eventList.size());
			
		}
		
		log.debug("Exit : public List getNotificationsListOfPaymentReminders");	
	} catch (DataAccessException e) {
	
		log.error("DataAccessException :"+e);
	}
	 catch (Exception e) {
			
			log.error("DataAccessException :"+e);
		}
	return eventList;
}
	

	/**
	 * @param namedParameterJdbcTemplate the namedParameterJdbcTemplate to set
	 */
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

}
