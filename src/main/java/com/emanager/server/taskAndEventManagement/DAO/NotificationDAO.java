package com.emanager.server.taskAndEventManagement.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.taskAndEventManagement.valueObject.SMSGatewayVO;
import com.emanager.server.taskAndEventManagement.valueObject.TaskVO;
import com.emanager.server.userManagement.valueObject.UserVO;



public class NotificationDAO {
	
	static Logger log = Logger.getLogger(NotificationDAO.class.getName());
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	DateUtility dateUtil=new DateUtility();
	
	
	public SMSGatewayVO getSMSGatewayDetails(int orgID){
		SMSGatewayVO smsGatewayVO=new SMSGatewayVO();
		String strSQLQuery=null;
						try{
			log.debug("Entry :  public SMSGatewayVO getSMSGatewayDetails(int orgID)");
//			1. SQL Query
			strSQLQuery = " SELECT s.*,ss.sms_credit,ss.available_sms_credit,ss.society_id FROM sms_gateway_details s,society_settings ss WHERE ss.sms_gateway_id=s.id AND ss.society_id=:orgID; ";				
			log.debug("query : " + strSQLQuery);		
			
			 Map hMap=new HashMap();
			 hMap.put("orgID", orgID);
			 			 			 
//			3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			        	SMSGatewayVO smsVO = new SMSGatewayVO();			        	
			        	
			        	smsVO.setSmsGatewayID(rs.getInt("id"));
			        	smsVO.setSmsGatewayName(rs.getString("sms_gateway_name"));
			        	smsVO.setSmsUserName(rs.getString("sms_user_name"));
			        	smsVO.setSmsPassword(rs.getString("sms_password"));
			        	smsVO.setSmsSenderName(rs.getString("sms_sender_name"));
			        	smsVO.setOrgID(rs.getInt("society_id"));
			        	smsVO.setSmsCredits(rs.getInt("sms_credit"));
			        	smsVO.setAvailableSMSCredits(rs.getInt("available_sms_credit"));
			        	return smsVO;
			        }};
			
			
			       smsGatewayVO = (SMSGatewayVO) namedParameterJdbcTemplate.queryForObject(strSQLQuery,hMap,RMapper);		
			
			log.debug("Exit : public SMSGatewayVO getSMSGatewayDetails(int orgID)");
			
		}catch(EmptyResultDataAccessException ex){
		
		  log.info("No details found for SMS Gateway for Given org ID :  "+orgID);            
		    	
		}catch(RecoverableDataAccessException ex){
			
	        log.info("RecoverableDataAccessException in public SMSGatewayVO getSMSGatewayDetails(int orgID) ");            
	    }catch(Exception ex){
			
	        log.error("Exception in public SMSGatewayVO getSMSGatewayDetails(int orgID)  for org ID :"+orgID+"  "+ex);            
	    }
	   log.debug("Exit : public SMSGatewayVO getSMSGatewayDetails(int orgID) ");
		return smsGatewayVO;	
	}		 

	
	//===== API to allot monthly SMS credits =========//
	   public int allotSMSCredits()  {
			
			log.debug("Entry : public int allotSMSCredits ");
			int success=0;
			try {
			
			String	strSQLQuery = "Update society_settings set available_sms_credit=sms_credit*2 where sms_credit is not null ";				
				log.debug("query : " + strSQLQuery);		
				
				 Map hMap=new HashMap();
				 
				       success=namedParameterJdbcTemplate.update(strSQLQuery,hMap);	

						
				log.debug("Exit :public int allotSMSCredits");	
			} catch (DataAccessException e) {
			
				log.error("DataAccessException : public int allotSMSCredits"+e);
			}
			 catch (Exception e) {
					
					log.error("Exception : public int allotSMSCredits"+e);
				}
			
			return success;
		}

	public int updateSMSCreditDetails(SMSGatewayVO smsGatewayVO,String action){
			String strSQLQuery=null;
			int updateFlag=0;
						try{
			log.debug("Entry :  public SMSGatewayVO getSMSCreditDetails(int orgID)");
//			1. SQL Query
			String condition="";
			if(action.equalsIgnoreCase("A")){
				condition="+"+smsGatewayVO.getAvailableSMSCredits();
			}else if(action.equalsIgnoreCase("D")){
				condition="-"+smsGatewayVO.getAvailableSMSCredits();
			}
			
			strSQLQuery = "Update society_settings set available_sms_credit=available_sms_credit "+condition+" where society_id= :orgID ";				
			log.debug("query : " + strSQLQuery);		
			
			 Map hMap=new HashMap();
			 hMap.put("orgID", smsGatewayVO.getOrgID());
			 hMap.put("smsCount", smsGatewayVO.getAvailableSMSCredits());
			 			 			 

			 
			
			       updateFlag=namedParameterJdbcTemplate.update(strSQLQuery,hMap);		
			
			log.debug("Exit : public SMSGatewayVO getSMSCreditDetails(int orgID)");
			
		}catch(EmptyResultDataAccessException ex){
		
		  log.info("No details found for SMS Gateway for Given org ID :  "+smsGatewayVO.getOrgID());            
		    	
		}catch(RecoverableDataAccessException ex){
			
	        log.info("RecoverableDataAccessException in public SMSGatewayVO getSMSCreditDetails(int orgID) ");            
	    }catch(Exception ex){
			
	        log.error("Exception in public SMSGatewayVO getSMSGatewayDetails(int orgID)  for org ID :"+smsGatewayVO.getOrgID()+"  "+ex);            
	    }
	   log.debug("Exit : public SMSGatewayVO getSMSGatewayDetails(int orgID) ");
		return updateFlag;	
	}
	
	public List sendNotificationForLowSMSCredit(){
		String strSQLQuery=null;
	List orgList=new ArrayList();
					try{
		log.debug("Entry :  public List sendNotificationForLowSMSCredit()");
//		1. SQL Query
				
		strSQLQuery = "SELECT ss.* FROM  (SELECT d.society_name,s.society_id,s.sms_credit,s.available_sms_credit ,available_sms_credit/sms_credit*100 AS percentage FROM society_settings s,society_details d WHERE s.society_id=d.society_id AND s.sms_credit>0 ) AS ss WHERE ss.percentage<50; ";				
		log.debug("query : " + strSQLQuery);		
		
		 Map hMap=new HashMap();
		
//			3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			        	SMSGatewayVO smsVO = new SMSGatewayVO();			        	
			        	
			        	smsVO.setOrgID(rs.getInt("society_id"));
			        	smsVO.setOrgName(rs.getString("society_name"));
			        	smsVO.setSmsCredits(rs.getInt("sms_credit"));
			        	smsVO.setAvailableSMSCredits(rs.getInt("available_sms_credit"));
			        	return smsVO;
			        }};		 			 

		 
		
		       orgList=namedParameterJdbcTemplate.query(strSQLQuery,hMap,RMapper);		
		
		log.debug("Exit : public SMSGatewayVO getSMSCreditDetails(int orgID)");
		
	}catch(EmptyResultDataAccessException ex){
	
	  log.info("No details found for SMS Gateway  ");            
	    	
	}catch(RecoverableDataAccessException ex){
		
        log.info("RecoverableDataAccessException in public SMSGatewayVO public List sendNotificationForLowSMSCredit() ");            
    }catch(Exception ex){
		
       log.error("Exception in public SMSGatewayVO public List sendNotificationForLowSMSCredit() "+ex);            
    }
   log.debug("Exit : public List sendNotificationForLowSMSCredit() ");
	return orgList;	
}
	
	public int insertAuditActivity(String userID,String appID,String orgID,String activity,String module) {
		 Date currentDatetime = new Date(System.currentTimeMillis());   
		 java.sql.Timestamp timestamp = new java.sql.Timestamp(currentDatetime.getTime());
		
		 int flag = 0;
		
		try{
			
			log.debug(" Entry : public int insertAuditActivity(String member_id,String activity,String module) ");
			 
			String sqlQuery=" INSERT INTO audit_trial(user_id,app_id,org_id,timestamp,activity,module) VALUES (:userID,:appID,:orgID,:timestamp,:activity,:module); ";
			 Map hmap = new HashMap();
				
			 hmap.put("appID", appID);
			 hmap.put("userID", userID);
			 hmap.put("orgID", orgID);
			 hmap.put("timestamp", timestamp);
			 hmap.put("activity", activity);
			 hmap.put("module", module);
									 
			 flag = namedParameterJdbcTemplate.update(sqlQuery, hmap);		                                             
			
			
			 log.debug(" Exit :public int insertAuditActivity(String member_id,String activity,String module) ");
			
			 log.debug("flag for insert in audit:"+flag);
			 
		 }	catch(Exception Ex){
			 
			 log.error("domain error : " + Ex);
		 } 
		
		return flag;	
	}
	

/**
 * @param namedParameterJdbcTemplate the namedParameterJdbcTemplate to set
 */
public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
	this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
}

	

}
