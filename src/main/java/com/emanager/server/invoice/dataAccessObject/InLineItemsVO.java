package com.emanager.server.invoice.dataAccessObject;

import java.math.BigDecimal;

public class InLineItemsVO {
	
	private int invoiceID;
	private String itemID;
	private int chargeID;// Used to generate member invoices
	private int ledgerID;
	private BigDecimal amount=BigDecimal.ZERO;
	private BigDecimal discount=BigDecimal.ZERO;
	private int paymentPriority;

	private BigDecimal quantity;
	private BigDecimal totalPrice=BigDecimal.ZERO;
	private BigDecimal perUnitPrice=BigDecimal.ZERO;
	private int isDeleted;
	
	private String description;
	private int orderPosition;
	private String unitOfMeasurement;
	
	private String createDate;
	
	private String updatedDate;
	private String ledgerName;
	private String invoiceDate;


	private BigDecimal cGST=BigDecimal.ZERO;
	private BigDecimal sGST=BigDecimal.ZERO;
	private BigDecimal iGST=BigDecimal.ZERO;
	private BigDecimal uGST=BigDecimal.ZERO;
	private BigDecimal cess=BigDecimal.ZERO;
	private BigDecimal gstAmount=BigDecimal.ZERO;
	private BigDecimal gstPercent=BigDecimal.ZERO;
	private BigDecimal cessPercent=BigDecimal.ZERO;
	private BigDecimal roundOff=BigDecimal.ZERO;

	private String hsnSacCode;
	private BigDecimal purchasePrice;
	private String itemName;
	private String invoiceType;
	private int gstCategoryID;
	private String gstCategoryName;
	private String lineItemTags;
	private int invLineItemID;
	private String notes;

	public String getItemID() {
		return itemID;
	}
	public void setItemID(String itemID) {
		this.itemID = itemID;
	}
	public String getHsnSacCode() {
		return hsnSacCode;
	}
	public void setHsnSacCode(String hsnSacCode) {
		this.hsnSacCode = hsnSacCode;
	}
	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}
	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	public BigDecimal getcGST() {
		return cGST;
	}
	public void setcGST(BigDecimal cGST) {
		this.cGST = cGST;
	}
	public BigDecimal getsGST() {
		return sGST;
	}
	public void setsGST(BigDecimal sGST) {
		this.sGST = sGST;
	}
	public BigDecimal getiGST() {
		return iGST;
	}
	public void setiGST(BigDecimal iGST) {
		this.iGST = iGST;
	}
	public BigDecimal getuGST() {
		return uGST;
	}
	public void setuGST(BigDecimal uGST) {
		this.uGST = uGST;
	}
	public BigDecimal getCess() {
		return cess;
	}
	public void setCess(BigDecimal cess) {
		this.cess = cess;
	}
	public BigDecimal getGstAmount() {
		return gstAmount;
	}
	public void setGstAmount(BigDecimal gstAmount) {
		this.gstAmount = gstAmount;
	}
	public BigDecimal getGstPercent() {
		return gstPercent;
	}
	public void setGstPercent(BigDecimal gstPercent) {
		this.gstPercent = gstPercent;
	}
	public BigDecimal getCessPercent() {
		return cessPercent;
	}
	public void setCessPercent(BigDecimal cessPercent) {
		this.cessPercent = cessPercent;
	}
	/**
	 * @return the invoiceID
	 */
	public int getInvoiceID() {
		return invoiceID;
	}

	/**
	 * @return the ledgerID
	 */
	public int getLedgerID() {
		return ledgerID;
	}
	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * @return the discount
	 */
	public BigDecimal getDiscount() {
		return discount;
	}
	/**
	 * @return the paymentPriority
	 */
	public int getPaymentPriority() {
		return paymentPriority;
	}
	/**
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}
	/**
	 * @return the totalPrice
	 */
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	/**
	 * @return the perUnitPrice
	 */
	public BigDecimal getPerUnitPrice() {
		return perUnitPrice;
	}
	/**
	 * @return the isDeleted
	 */
	public int getIsDeleted() {
		return isDeleted;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @return the orderPosition
	 */
	public int getOrderPosition() {
		return orderPosition;
	}
	/**
	 * @return the unitOfMeasurement
	 */
	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}
	/**
	 * @return the createDate
	 */
	public String getCreateDate() {
		return createDate;
	}
	/**
	 * @return the updatedDate
	 */
	public String getUpdatedDate() {
		return updatedDate;
	}
	/**
	 * @return the ledgerName
	 */
	public String getLedgerName() {
		return ledgerName;
	}

	/**
	 * @param invoiceID the invoiceID to set
	 */
	public void setInvoiceID(int invoiceID) {
		this.invoiceID = invoiceID;
	}

	/**
	 * @param ledgerID the ledgerID to set
	 */
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * @param discount the discount to set
	 */
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	/**
	 * @param paymentPriority the paymentPriority to set
	 */
	public void setPaymentPriority(int paymentPriority) {
		this.paymentPriority = paymentPriority;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	/**
	 * @param totalPrice the totalPrice to set
	 */
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	/**
	 * @param perUnitPrice the perUnitPrice to set
	 */
	public void setPerUnitPrice(BigDecimal perUnitPrice) {
		this.perUnitPrice = perUnitPrice;
	}
	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @param orderPosition the orderPosition to set
	 */
	public void setOrderPosition(int orderPosition) {
		this.orderPosition = orderPosition;
	}
	/**
	 * @param unitOfMeasurement the unitOfMeasurement to set
	 */
	public void setUnitOfMeasurement(String unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}
	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	/**
	 * @param ledgerName the ledgerName to set
	 */
	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}
	/**
	 * @return the chargeID
	 */
	public int getChargeID() {
		return chargeID;
	}
	/**
	 * @param chargeID the chargeID to set
	 */
	public void setChargeID(int chargeID) {
		this.chargeID = chargeID;
	}
	/**
	 * @return the invoiceDate
	 */
	public String getInvoiceDate() {
		return invoiceDate;
	}
	/**
	 * @param invoiceDate the invoiceDate to set
	 */
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	/**
	 * @return the roundOff
	 */
	public BigDecimal getRoundOff() {
		return roundOff;
	}
	/**
	 * @param roundOff the roundOff to set
	 */
	public void setRoundOff(BigDecimal roundOff) {
		this.roundOff = roundOff;
	}
	public String getInvoiceType() {
		return invoiceType;
	}
	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}
	public int getGstCategoryID() {
		return gstCategoryID;
	}
	public void setGstCategoryID(int gstCategoryID) {
		this.gstCategoryID = gstCategoryID;
	}
	public String getGstCategoryName() {
		return gstCategoryName;
	}
	public void setGstCategoryName(String gstCategoryName) {
		this.gstCategoryName = gstCategoryName;
	}
	public String getLineItemTags() {
		return lineItemTags;
	}
	public void setLineItemTags(String lineItemTags) {
		this.lineItemTags = lineItemTags;
	}
	public int getInvLineItemID() {
		return invLineItemID;
	}
	public void setInvLineItemID(int invLineItemID) {
		this.invLineItemID = invLineItemID;
	}
	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}
	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}


}
