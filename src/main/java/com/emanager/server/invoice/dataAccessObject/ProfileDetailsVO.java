package com.emanager.server.invoice.dataAccessObject;

import java.math.BigDecimal;

import com.emanager.server.commonUtils.valueObject.AddressVO;

public class ProfileDetailsVO {

	
	private int profileID;
	private int profileLedgerID;
	private String profileLedgerName;
	private int orgID;
	private String title;
	private String profileName;
	private String profileMobile;
	private String profileEmailID;
	private String contactPrsnName;
	private String cntctPrsnMobile;
	private String cntctPrsnEmail;
	private AddressVO address;
	private String panNo;
	private String panName;
	private String tanNO;
	private String gstIN;
	private String profileType;//C-customer,V-Vendor,E-employee
	private String additionalProps;
	private String virtualAccNo;
	private String deducteeCode;//1-Indiviual-HUF, 2- Pvt Ltd,others
	private String tdsSectionID;
	private String tdsSectionNo;
	private String tdsSectionName;	
	private BigDecimal tdsCutOffAmount;
	private BigDecimal tdsRateForIndiv;
	private BigDecimal tdsRateForOther;
	private BigDecimal thresholdBalance;
	private String tags; //Various tags for Customer,vendor,employees
	private String profileCode;
	private int categoryID;
	private int rateCardID;
	
	public String getAdditionalProps() {
		return additionalProps;
	}
	public void setAdditionalProps(String additionalProps) {
		this.additionalProps = additionalProps;
	}
	/**
	 * @return the profileID
	 */
	public int getProfileID() {
		return profileID;
	}
	/**
	 * @return the profileLedgerID
	 */
	public int getProfileLedgerID() {
		return profileLedgerID;
	}
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @return the profileName
	 */
	public String getProfileName() {
		return profileName;
	}

	/**
	 * @return the address
	 */
	public AddressVO getAddress() {
		return address;
	}
	/**
	 * @return the panNo
	 */
	public String getPanNo() {
		return panNo;
	}
	/**
	 * @return the tanNO
	 */
	public String getTanNO() {
		return tanNO;
	}
	/**
	 * @return the gstIN
	 */
	public String getGstIN() {
		return gstIN;
	}
	/**
	 * @param profileID the profileID to set
	 */
	public void setProfileID(int profileID) {
		this.profileID = profileID;
	}
	/**
	 * @param profileLedgerID the profileLedgerID to set
	 */
	public void setProfileLedgerID(int profileLedgerID) {
		this.profileLedgerID = profileLedgerID;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	/**
	 * @param profileName the profileName to set
	 */
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}
	
	/**
	 * @param address the address to set
	 */
	public void setAddress(AddressVO address) {
		this.address = address;
	}
	/**
	 * @param panNo the panNo to set
	 */
	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}
	/**
	 * @param tanNO the tanNO to set
	 */
	public void setTanNO(String tanNO) {
		this.tanNO = tanNO;
	}
	/**
	 * @param gstIN the gstIN to set
	 */
	public void setGstIN(String gstIN) {
		this.gstIN = gstIN;
	}
	/**
	 * @return the profileType
	 */
	public String getProfileType() {
		return profileType;
	}
	/**
	 * @param profileType the profileType to set
	 */
	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the profileMobile
	 */
	public String getProfileMobile() {
		return profileMobile;
	}
	/**
	 * @return the profileEmailID
	 */
	public String getProfileEmailID() {
		return profileEmailID;
	}
	/**
	 * @return the contactPrsnName
	 */
	public String getContactPrsnName() {
		return contactPrsnName;
	}
	/**
	 * @return the cntctPrsnMobile
	 */
	public String getCntctPrsnMobile() {
		return cntctPrsnMobile;
	}
	/**
	 * @return the cntctPrsnEmail
	 */
	public String getCntctPrsnEmail() {
		return cntctPrsnEmail;
	}
	/**
	 * @param profileMobile the profileMobile to set
	 */
	public void setProfileMobile(String profileMobile) {
		this.profileMobile = profileMobile;
	}
	/**
	 * @param profileEmailID the profileEmailID to set
	 */
	public void setProfileEmailID(String profileEmailID) {
		this.profileEmailID = profileEmailID;
	}
	/**
	 * @param contactPrsnName the contactPrsnName to set
	 */
	public void setContactPrsnName(String contactPrsnName) {
		this.contactPrsnName = contactPrsnName;
	}
	/**
	 * @param cntctPrsnMobile the cntctPrsnMobile to set
	 */
	public void setCntctPrsnMobile(String cntctPrsnMobile) {
		this.cntctPrsnMobile = cntctPrsnMobile;
	}
	/**
	 * @param cntctPrsnEmail the cntctPrsnEmail to set
	 */
	public void setCntctPrsnEmail(String cntctPrsnEmail) {
		this.cntctPrsnEmail = cntctPrsnEmail;
	}
	/**
	 * @return the virtualAccNo
	 */
	public String getVirtualAccNo() {
		return virtualAccNo;
	}
	/**
	 * @param virtualAccNo the virtualAccNo to set
	 */
	public void setVirtualAccNo(String virtualAccNo) {
		this.virtualAccNo = virtualAccNo;
	}

	/**
	 * @return the deducteeCode
	 */
	public String getDeducteeCode() {
		return deducteeCode;
	}
	/**
	 * @param deducteeCode the deducteeCode to set
	 */
	public void setDeducteeCode(String deducteeCode) {
		this.deducteeCode = deducteeCode;
	}
	public String getTdsSectionID() {
		return tdsSectionID;
	}
	public void setTdsSectionID(String tdsSectionID) {
		this.tdsSectionID = tdsSectionID;
	}
	public String getTdsSectionNo() {
		return tdsSectionNo;
	}
	public void setTdsSectionNo(String tdsSectionNo) {
		this.tdsSectionNo = tdsSectionNo;
	}
	public String getTdsSectionName() {
		return tdsSectionName;
	}
	public void setTdsSectionName(String tdsSectionName) {
		this.tdsSectionName = tdsSectionName;
	}
	public BigDecimal getTdsCutOffAmount() {
		return tdsCutOffAmount;
	}
	public void setTdsCutOffAmount(BigDecimal tdsCutOffAmount) {
		this.tdsCutOffAmount = tdsCutOffAmount;
	}
	public BigDecimal getTdsRateForIndiv() {
		return tdsRateForIndiv;
	}
	public void setTdsRateForIndiv(BigDecimal tdsRateForIndiv) {
		this.tdsRateForIndiv = tdsRateForIndiv;
	}
	public BigDecimal getTdsRateForOther() {
		return tdsRateForOther;
	}
	public void setTdsRateForOther(BigDecimal tdsRateForOther) {
		this.tdsRateForOther = tdsRateForOther;
	}
	/**
	 * @return the thresholdBalance
	 */
	public BigDecimal getThresholdBalance() {
		return thresholdBalance;
	}
	/**
	 * @param thresholdBalance the thresholdBalance to set
	 */
	public void setThresholdBalance(BigDecimal thresholdBalance) {
		this.thresholdBalance = thresholdBalance;
	}
	/**
	 * @return the panName
	 */
	public String getPanName() {
		return panName;
	}
	/**
	 * @param panName the panName to set
	 */
	public void setPanName(String panName) {
		this.panName = panName;
	}
	/**
	 * @return the tags
	 */
	public String getTags() {
		return tags;
	}
	/**
	 * @param tags the tags to set
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}
	public String getProfileLedgerName() {
		return profileLedgerName;
	}
	public void setProfileLedgerName(String profileLedgerName) {
		this.profileLedgerName = profileLedgerName;
	}
	/**
	 * @return the profileCode
	 */
	public String getProfileCode() {
		return profileCode;
	}
	/**
	 * @param profileCode the profileCode to set
	 */
	public void setProfileCode(String profileCode) {
		this.profileCode = profileCode;
	}
	/**
	 * @return the categoryID
	 */
	public int getCategoryID() {
		return categoryID;
	}
	/**
	 * @param categoryID the categoryID to set
	 */
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}
	/**
	 * @return the rateCardID
	 */
	public int getRateCardID() {
		return rateCardID;
	}
	/**
	 * @param rateCardID the rateCardID to set
	 */
	public void setRateCardID(int rateCardID) {
		this.rateCardID = rateCardID;
	}
	
}
