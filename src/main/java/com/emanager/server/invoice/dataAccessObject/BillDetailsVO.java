package com.emanager.server.invoice.dataAccessObject;

import java.math.BigDecimal;
import java.util.List;

import com.emanager.server.society.valueObject.BankInfoVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;

public class BillDetailsVO {
	
	private int societyID;
	private String societyName;
	private int aptID;
	private int memberID;
	private String memberName;
	private int ledgerID;
	private BigDecimal chargedAmount;
	private BigDecimal paidAmount;
	private BigDecimal totalAmount;
	private BigDecimal openingBalance;
	private BigDecimal closingBalance;
	private BigDecimal roundOff=BigDecimal.ZERO;
	private String billNo;
	private String fromDate;
	private String uptoDate;
	private String dueDate;
	private int billingCycleID;
	private List objectList;
	private String ledgerName;  
	private SocietyVO societyVO;
	private BankInfoVO bankVO;
	private String profileType;  
	private InvoiceVO invoiceVO;
	private MemberVO memberVO;
	private String closingBalanceInwords;
	private BigDecimal intBalance;
	private BigDecimal principleBalance;
	private String ledgerDescription;
	private String emailID;
	private List<BillDetailsVO> summaryBillList;
	
	
	public String getProfileType() {
		return profileType;
	}
	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}
	public String getLedgerName() {
		return ledgerName;
	}
	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}
	public int getAptID() {
		return aptID;
	}
	public void setAptID(int aptID) {
		this.aptID = aptID;
	}
	public int getMemberID() {
		return memberID;
	}
	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public int getLedgerID() {
		return ledgerID;
	}
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getUptoDate() {
		return uptoDate;
	}
	public void setUptoDate(String uptoDate) {
		this.uptoDate = uptoDate;
	}
	public String getBillNo() {
		return billNo;
	}
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	public BigDecimal getChargedAmount() {
		return chargedAmount;
	}
	public void setChargedAmount(BigDecimal chargedAmount) {
		this.chargedAmount = chargedAmount;
	}
	public BigDecimal getClosingBalance() {
		return closingBalance;
	}
	public void setClosingBalance(BigDecimal closingBalance) {
		this.closingBalance = closingBalance;
	}
	public BigDecimal getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(BigDecimal openingBalance) {
		this.openingBalance = openingBalance;
	}
	public BigDecimal getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}
	public int getBillingCycleID() {
		return billingCycleID;
	}
	public void setBillingCycleID(int billingCycleID) {
		this.billingCycleID = billingCycleID;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public List getObjectList() {
		return objectList;
	}
	public void setObjectList(List objectList) {
		this.objectList = objectList;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	/**
	 * @return the societyVO
	 */
	public SocietyVO getSocietyVO() {
		return societyVO;
	}
	/**
	 * @return the bankVO
	 */
	public BankInfoVO getBankVO() {
		return bankVO;
	}
	/**
	 * @param societyVO the societyVO to set
	 */
	public void setSocietyVO(SocietyVO societyVO) {
		this.societyVO = societyVO;
	}
	/**
	 * @param bankVO the bankVO to set
	 */
	public void setBankVO(BankInfoVO bankVO) {
		this.bankVO = bankVO;
	}
	/**
	 * @return the roundOff
	 */
	public BigDecimal getRoundOff() {
		return roundOff;
	}
	/**
	 * @param roundOff the roundOff to set
	 */
	public void setRoundOff(BigDecimal roundOff) {
		this.roundOff = roundOff;
	}
	/**
	 * @return the invoiceVO
	 */
	public InvoiceVO getInvoiceVO() {
		return invoiceVO;
	}
	/**
	 * @param invoiceVO the invoiceVO to set
	 */
	public void setInvoiceVO(InvoiceVO invoiceVO) {
		this.invoiceVO = invoiceVO;
	}
	public MemberVO getMemberVO() {
		return memberVO;
	}
	public void setMemberVO(MemberVO memberVO) {
		this.memberVO = memberVO;
	}
	public String getClosingBalanceInwords() {
		return closingBalanceInwords;
	}
	public void setClosingBalanceInwords(String closingBalanceInwords) {
		this.closingBalanceInwords = closingBalanceInwords;
	}
	/**
	 * @return the intBalance
	 */
	public BigDecimal getIntBalance() {
		return intBalance;
	}
	/**
	 * @return the principleBalance
	 */
	public BigDecimal getPrincipleBalance() {
		return principleBalance;
	}
	/**
	 * @param intBalance the intBalance to set
	 */
	public void setIntBalance(BigDecimal intBalance) {
		this.intBalance = intBalance;
	}
	/**
	 * @param principleBalance the principleBalance to set
	 */
	public void setPrincipleBalance(BigDecimal principleBalance) {
		this.principleBalance = principleBalance;
	}
	/**
	 * @return the ledgerDescription
	 */
	public String getLedgerDescription() {
		return ledgerDescription;
	}
	/**
	 * @param ledgerDescription the ledgerDescription to set
	 */
	public void setLedgerDescription(String ledgerDescription) {
		this.ledgerDescription = ledgerDescription;
	}
	
	public String getEmailID() {
		return emailID;
	}
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
	/**
	 * @return the summaryBillList
	 */
	public List<BillDetailsVO> getSummaryBillList() {
		return summaryBillList;
	}
	/**
	 * @param summaryBillList the summaryBillList to set
	 */
	public void setSummaryBillList(List<BillDetailsVO> summaryBillList) {
		this.summaryBillList = summaryBillList;
	}
	
	
	
	
	
	
	
}
