package com.emanager.server.invoice.dataAccessObject;

import java.math.BigDecimal;
import java.util.List;

import com.emanager.server.accounts.DataAccessObjects.OnlinePaymentGatewayVO;
import com.emanager.server.adminReports.valueObject.PrintReportVO;
import com.emanager.server.society.valueObject.BankInfoVO;

public class InvoiceDetailsVO extends PrintReportVO {
	
	
	private InvoiceMemberChargesVO memberVO;
	private String invoiceID;
	private int ledgerID;
	private int invoiceTypeID;
	private int aptID;
	private int statusID;
	private int delegatedInvoiceID;
	private String dueDate;
	private BigDecimal invoiceAmount;
	private BigDecimal totalAmount;
	private int paymentAttempts;
	private BigDecimal balance;
	private BigDecimal carriedBalance;
	private int inProcessPayment;
	private int isReview;
	private int currencyID;
	private int isDeleted;
	private int paperInvoiceBatchID;
	private String customerNotes;
	private String publicNumber;
	private String lastReminder;
	private int overDueSteps;
	private String createdTimestamp;
	private int categoryID;
	private String invoiceDate;
	private String buildingName;
	private String buildingId;
	private String fullName;
	private String unitNo;
	
	//Tax,recurring,sf,repairs
	private String paymentType;
	private int paymentPriority;
	private int quantity;
	private BigDecimal itemPrice;
	private String fromDate;
	private String uptoDate;
	private String createDate;
	private String updatedDate;
	private String runDate;
	private String nextRunDate;
	private String status;
	private String description;
	
	private String invoiceType;
	
	private int entityID;
	private String calculationMethod;
	private String categoryName;
	private List listOfLineItems;
	private int billCycleID;
	private String memberName;
	private String invoiceTypeCategory;
	private Boolean isApplicable;
	private int isMailSent;
	private BigDecimal paidAmount;
	private BigDecimal currentBalance;
	private int invoiceFor;
	private BankInfoVO bankInfoVO;
	private BigDecimal discount;
	private String secretKey;
	private int isPaymentGatewayAvailable;
	private List <OnlinePaymentGatewayVO> conveineceFeeList;
	private BigDecimal processingFees;
	private String memberGroupMailID;
	private int societyID;
	private int billID;
	private OnlinePaymentGatewayVO opGatewayVO;
	private String gstType;

	
	public List<OnlinePaymentGatewayVO> getConveineceFeeList() {
		return conveineceFeeList;
	}
	public void setConveineceFeeList(List<OnlinePaymentGatewayVO> conveineceFeeList) {
		this.conveineceFeeList = conveineceFeeList;
	}
	public OnlinePaymentGatewayVO getOpGatewayVO() {
		return opGatewayVO;
	}
	public void setOpGatewayVO(OnlinePaymentGatewayVO opGatewayVO) {
		this.opGatewayVO = opGatewayVO;
	}
	public int getBillID() {
		return billID;
	}
	public void setBillID(int billID) {
		this.billID = billID;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	
	public int getBillCycleID() {
		return billCycleID;
	}
	public void setBillCycleID(int billCycleID) {
		this.billCycleID = billCycleID;
	}
	public String getCalculationMethod() {
		return calculationMethod;
	}
	public void setCalculationMethod(String calculationMethod) {
		this.calculationMethod = calculationMethod;
	}
	public BigDecimal getCarriedBalance() {
		return carriedBalance;
	}
	public void setCarriedBalance(BigDecimal carriedBalance) {
		this.carriedBalance = carriedBalance;
	}
	public int getCategoryID() {
		return categoryID;
	}
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreatedTimestamp() {
		return createdTimestamp;
	}
	public void setCreatedTimestamp(String createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}
	public int getCurrencyID() {
		return currencyID;
	}
	public void setCurrencyID(int currencyID) {
		this.currencyID = currencyID;
	}
	public String getCustomerNotes() {
		return customerNotes;
	}
	public void setCustomerNotes(String customerNotes) {
		this.customerNotes = customerNotes;
	}
	public int getDelegatedInvoiceID() {
		return delegatedInvoiceID;
	}
	public void setDelegatedInvoiceID(int delegatedInvoiceID) {
		this.delegatedInvoiceID = delegatedInvoiceID;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public int getEntityID() {
		return entityID;
	}
	public void setEntityID(int entityID) {
		this.entityID = entityID;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public int getInProcessPayment() {
		return inProcessPayment;
	}
	public void setInProcessPayment(int inProcessPayment) {
		this.inProcessPayment = inProcessPayment;
	}
	public String getInvoiceID() {
		return invoiceID;
	}
	public void setInvoiceID(String invoiceID) {
		this.invoiceID = invoiceID;
	}
	public int getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	public int getIsReview() {
		return isReview;
	}
	public void setIsReview(int isReview) {
		this.isReview = isReview;
	}
	public BigDecimal getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(BigDecimal itemPrice) {
		this.itemPrice = itemPrice;
	}
	public String getLastReminder() {
		return lastReminder;
	}
	public void setLastReminder(String lastReminder) {
		this.lastReminder = lastReminder;
	}
	public List getListOfLineItems() {
		return listOfLineItems;
	}
	public void setListOfLineItems(List listOfLineItems) {
		this.listOfLineItems = listOfLineItems;
	}
	public InvoiceMemberChargesVO getMemberVO() {
		return memberVO;
	}
	public void setMemberVO(InvoiceMemberChargesVO memberVO) {
		this.memberVO = memberVO;
	}
	public String getNextRunDate() {
		return nextRunDate;
	}
	public void setNextRunDate(String nextRunDate) {
		this.nextRunDate = nextRunDate;
	}
	public int getOverDueSteps() {
		return overDueSteps;
	}
	public void setOverDueSteps(int overDueSteps) {
		this.overDueSteps = overDueSteps;
	}
	public int getPaperInvoiceBatchID() {
		return paperInvoiceBatchID;
	}
	public void setPaperInvoiceBatchID(int paperInvoiceBatchID) {
		this.paperInvoiceBatchID = paperInvoiceBatchID;
	}
	public int getPaymentAttempts() {
		return paymentAttempts;
	}
	public void setPaymentAttempts(int paymentAttempts) {
		this.paymentAttempts = paymentAttempts;
	}
	public int getPaymentPriority() {
		return paymentPriority;
	}
	public void setPaymentPriority(int paymentPriority) {
		this.paymentPriority = paymentPriority;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getPublicNumber() {
		return publicNumber;
	}
	public void setPublicNumber(String publicNumber) {
		this.publicNumber = publicNumber;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getRunDate() {
		return runDate;
	}
	public void setRunDate(String runDate) {
		this.runDate = runDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getStatusID() {
		return statusID;
	}
	public void setStatusID(int statusID) {
		this.statusID = statusID;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getUptoDate() {
		return uptoDate;
	}
	public void setUptoDate(String uptoDate) {
		this.uptoDate = uptoDate;
	}
	public int getAptID() {
		return aptID;
	}
	public void setAptID(int aptID) {
		this.aptID = aptID;
	}
	public int getLedgerID() {
		return ledgerID;
	}
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public BigDecimal getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(BigDecimal invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public String getInvoiceType() {
		return invoiceType;
	}
	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getInvoiceTypeCategory() {
		return invoiceTypeCategory;
	}
	public void setInvoiceTypeCategory(String invoiceTypeCategory) {
		this.invoiceTypeCategory = invoiceTypeCategory;
	}
	public int getInvoiceTypeID() {
		return invoiceTypeID;
	}
	public void setInvoiceTypeID(int invoiceTypeID) {
		this.invoiceTypeID = invoiceTypeID;
	}
	public Boolean getIsApplicable() {
		return isApplicable;
	}
	public void setIsApplicable(Boolean isApplicable) {
		this.isApplicable = isApplicable;
	}
	public int getIsMailSent() {
		return isMailSent;
	}
	public void setIsMailSent(int isMailSent) {
		this.isMailSent = isMailSent;
	}
	public BigDecimal getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}
	public BigDecimal getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(BigDecimal currentBalance) {
		this.currentBalance = currentBalance;
	}
	public int getInvoiceFor() {
		return invoiceFor;
	}
	public void setInvoiceFor(int invoiceFor) {
		this.invoiceFor = invoiceFor;
	}
	public String getBuildingName() {
		return buildingName;
	}
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	public String getBuildingId() {
		return buildingId;
	}
	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}
	public BankInfoVO getBankInfoVO() {
		return bankInfoVO;
	}
	public void setBankInfoVO(BankInfoVO bankInfoVO) {
		this.bankInfoVO = bankInfoVO;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getUnitNo() {
		return unitNo;
	}
	public void setUnitNo(String unitNo) {
		this.unitNo = unitNo;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public int getIsPaymentGatewayAvailable() {
		return isPaymentGatewayAvailable;
	}
	public void setIsPaymentGatewayAvailable(int isPaymentGatewayAvailable) {
		this.isPaymentGatewayAvailable = isPaymentGatewayAvailable;
	}
	
	public BigDecimal getProcessingFees() {
		return processingFees;
	}
	public void setProcessingFees(BigDecimal processingFees) {
		this.processingFees = processingFees;
	}
	public String getMemberGroupMailID() {
		return memberGroupMailID;
	}
	public void setMemberGroupMailID(String memberGroupMailID) {
		this.memberGroupMailID = memberGroupMailID;
	}
	/**
	 * @return the societyID
	 */
	public int getSocietyID() {
		return societyID;
	}
	/**
	 * @param societyID the societyID to set
	 */
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	public String getGstType() {
		return gstType;
	}
	public void setGstType(String gstType) {
		this.gstType = gstType;
	}

	
	
	
}
