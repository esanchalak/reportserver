package com.emanager.server.invoice.dataAccessObject;

import java.math.BigDecimal;

public class InvoiceLineItemsVO {
	
	
	private int invoiceID;
	private int categoryID;
	private BigDecimal totalAmount;
	private int paymentPriority;
	private BigDecimal balance;
	private BigDecimal carriedBalance;
	private int quantity;
	private int price;
	private int isDeleted;
	private int sourceUserID;
	private int isPercentage;
	private String description;
	private int orderPosition;
	private BigDecimal entityID;
	private String calculationMethod;
	
	private String createDate;
	
	private String updatedDate;
	private String ledgerName;
	private int ledgerID;
	private int aptID;
	
	private String invoiceDate;
	private int billingProcessID;
	private int invoiceTypeID;
	
	
	
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public String getCalculationMethod() {
		return calculationMethod;
	}
	public void setCalculationMethod(String calculationMethod) {
		this.calculationMethod = calculationMethod;
	}
	public int getCategoryID() {
		return categoryID;
	}
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getEntityID() {
		return entityID;
	}
	public void setEntityID(BigDecimal entityID) {
		this.entityID = entityID;
	}
	public int getInvoiceID() {
		return invoiceID;
	}
	public void setInvoiceID(int invoiceID) {
		this.invoiceID = invoiceID;
	}
	public int getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	public int getIsPercentage() {
		return isPercentage;
	}
	public void setIsPercentage(int isPercentage) {
		this.isPercentage = isPercentage;
	}
	public int getOrderPosition() {
		return orderPosition;
	}
	public void setOrderPosition(int orderPosition) {
		this.orderPosition = orderPosition;
	}
	public int getPaymentPriority() {
		return paymentPriority;
	}
	public void setPaymentPriority(int paymentPriority) {
		this.paymentPriority = paymentPriority;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getSourceUserID() {
		return sourceUserID;
	}
	public void setSourceUserID(int sourceUserID) {
		this.sourceUserID = sourceUserID;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	public BigDecimal getCarriedBalance() {
		return carriedBalance;
	}
	public void setCarriedBalance(BigDecimal carriedBalance) {
		this.carriedBalance = carriedBalance;
	}
	
	public int getLedgerID() {
		return ledgerID;
	}
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	
	public int getAptID() {
		return aptID;
	}
	public void setAptID(int aptID) {
		this.aptID = aptID;
	}
	
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public int getBillingProcessID() {
		return billingProcessID;
	}
	public void setBillingProcessID(int billingProcessID) {
		this.billingProcessID = billingProcessID;
	}
	public int getInvoiceTypeID() {
		return invoiceTypeID;
	}
	public void setInvoiceTypeID(int invoiceTypeID) {
		this.invoiceTypeID = invoiceTypeID;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getLedgerName() {
		return ledgerName;
	}
	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}
	
	
	
	
	
	
}
