package com.emanager.server.invoice.dataAccessObject;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.society.valueObject.BankInfoVO;
import com.emanager.server.society.valueObject.EmployeeVO;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.vendorManagement.valueObject.VendorVO;

public class InvoiceVO  {
	
	
	private int orgID;
	private int invoiceID;
	private String invoiceNumber;
	private String docID;
	private String myGSTIN;// GST Identification Number
	private String customerGSTIN;//Customer GST Identification Number
	private String customerPAN;//Customer PAN Card
	private int paperInvoiceBatchID;
	private String invoiceDate;
	private String dueDate;
	private int entityID;
	private String entityType;// Customer, Employee, Vendor
	private int ledgerID;
	private String invoiceType;//Sales,Purchase,PO,Salary slip etc
	private int invoiceTypeID;
	private int gstInvoiceTypeID;
	private String gstInvoiceType;
	private int paymentStatus;// Paid , Unpaid
	private int delegatedInvoiceID;//Ex. To relate PO or Performa Inv to Sales Inv

	private BigDecimal invoiceAmount=BigDecimal.ZERO;
	private BigDecimal balance=BigDecimal.ZERO;
	private BigDecimal carriedBalance=BigDecimal.ZERO;
	private int paymentAttempts;
	
	private int inProcessPayment;
	private int isReview;//0-Confirm,1-Draft,2-Pending Review,3-Template
	private int currencyID;
	private int isDeleted;

	private String lastReminderDate;
	private int overDueSteps;

    private String paymentType;
	private int paymentPriority;
	private int billCycleID;
	private String fromDate;
	private String uptoDate;	
	private String notes;
	private int isMailSent;

	private String supplyState;
	private String supplyStateID;
	
	private String billingAddress;
	private String billingCity;
	private String billingState;
	private String billingZipCode;
	
	private String shippingAddress;
	private String shippingCity;
	private String shippingState;
	private String shippingZipCode;
	
	private CustomerVO customerVO;
	private VendorVO vendorVO;
	private EmployeeVO employeeVO;
	private String emailID;
	
	private BigDecimal totalCGST=BigDecimal.ZERO;
	private BigDecimal totalSGST=BigDecimal.ZERO;
	private BigDecimal totalIGST=BigDecimal.ZERO;
	private BigDecimal totalUGST=BigDecimal.ZERO;
		
	private Timestamp updatedTimestamp;
	private Timestamp createdTimestamp;
	
	private BigDecimal totalGST=BigDecimal.ZERO;
	private BigDecimal totalGSTPercent=BigDecimal.ZERO;
	private BigDecimal tds=BigDecimal.ZERO;
	private BigDecimal totalAmount=BigDecimal.ZERO;
	private BigDecimal totalDiscount=BigDecimal.ZERO;
	private BigDecimal totalCess=BigDecimal.ZERO;
	private BigDecimal totalRoundUpAmount=BigDecimal.ZERO;
	private List<InLineItemsVO> lineItemsList;
	
	private String entityName;
    private String entityAddress;
    private String entityCity;
    private String entityState;
    private String entityZipCode;
    
    private String creatorName;
	private String updaterName;
	private int createdBy;
	private int updatedBy;
	private String approvalName;
	private String reviewerName;
	private List<InLineItemsVO> gstSummary;
	private List<TransactionVO> receiptList;
	private String transactionID;
	private int statusCode;
	private String statusMessage;
	private String chargeName;
	private String tdsSectionNo;
	private BigDecimal tdsRate=BigDecimal.ZERO;
	private String totalAmountInWords;
	private SocietyVO orgVO;
	private BankInfoVO bankVO;
	private int isGstCalculationInclusive;
	private int isSales;
	private int isReverse;
	private String invoiceTags;
	private CurrencyVO currencyVO;
	private BigDecimal currencyRate=BigDecimal.ZERO;
	private String portCode;
	private String shippingBillNo;
	private String shippingDate;
	private int invLineItemID;
	private int linkedInvoiceID;
	private int seqNo;
	private BigDecimal paidInvAmount=BigDecimal.ZERO;
	private List<InvoiceVO> linkInvList;
	private String gstCategory;
	private BigDecimal zeroAmount;
	private BigDecimal thirtyAmount;
	private BigDecimal sixtyAmount;
	private BigDecimal ninetyAmount;
	private BigDecimal ninetyPlusAmount;
	private BigDecimal overDueAmount;
	private BigDecimal overDueThirtyAmount;
	private BigDecimal overDueSixtyAmount;
	private BigDecimal overDueNinetyAmount;
	private BigDecimal overDueNinetyPlusAmount;
	private int isDueDate;
	private int productID;
	private BigDecimal quantity;
	private String productName;
	private BigDecimal price;
	private BigDecimal totalPrice;
	
	public List<TransactionVO> getReceiptList() {
		return receiptList;
	}
	public void setReceiptList(List<TransactionVO> receiptList) {
		this.receiptList = receiptList;
	}
	public String getChargeName() {
		return chargeName;
	}
	public void setChargeName(String chargeName) {
		this.chargeName = chargeName;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	public String getCreatorName() {
		return creatorName;
	}
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	public String getUpdaterName() {
		return updaterName;
	}
	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName;
	}
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	public int getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getApprovalName() {
		return approvalName;
	}
	public void setApprovalName(String approvalName) {
		this.approvalName = approvalName;
	}
	public String getReviewerName() {
		return reviewerName;
	}
	public void setReviewerName(String reviewerName) {
		this.reviewerName = reviewerName;
	}
		
	public List<InLineItemsVO> getGstSummary() {
		return gstSummary;
	}
	public void setGstSummary(List<InLineItemsVO> gstSummary) {
		this.gstSummary = gstSummary;
	}
	public String getDocID() {
		return docID;
	}
	public void setDocID(String docID) {
		this.docID = docID;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public BigDecimal getTotalRoundUpAmount() {
		return totalRoundUpAmount;
	}
	public void setTotalRoundUpAmount(BigDecimal totalRoundUpAmount) {
		this.totalRoundUpAmount = totalRoundUpAmount;
	}
	public String getInvoiceType() {
		return invoiceType;
	}
	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}
	public String getCustomerGSTIN() {
		return customerGSTIN;
	}
	public void setCustomerGSTIN(String customerGSTIN) {
		this.customerGSTIN = customerGSTIN;
	}
	public BigDecimal getTotalCess() {
		return totalCess;
	}
	public void setTotalCess(BigDecimal totalCess) {
		this.totalCess = totalCess;
	}

	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public String getEntityAddress() {
		return entityAddress;
	}
	public void setEntityAddress(String entityAddress) {
		this.entityAddress = entityAddress;
	}
	public String getEntityCity() {
		return entityCity;
	}
	public void setEntityCity(String entityCity) {
		this.entityCity = entityCity;
	}
	public String getEntityState() {
		return entityState;
	}
	public void setEntityState(String entityState) {
		this.entityState = entityState;
	}
	public String getEntityZipCode() {
		return entityZipCode;
	}
	public void setEntityZipCode(String entityZipCode) {
		this.entityZipCode = entityZipCode;
	}
	public Timestamp getUpdatedTimestamp() {
		return updatedTimestamp;
	}
	public void setUpdatedTimestamp(Timestamp updatedTimestamp) {
		this.updatedTimestamp = updatedTimestamp;
	}
	public Timestamp getCreatedTimestamp() {
		return createdTimestamp;
	}
	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}
	public int getInvoiceID() {
		return invoiceID;
	}
	public void setInvoiceID(int invoiceID) {
		this.invoiceID = invoiceID;
	}
	public List<InLineItemsVO> getLineItemsList() {
		return lineItemsList;
	}
	public void setLineItemsList(List<InLineItemsVO> lineItemsList) {
		this.lineItemsList = lineItemsList;
	}
	public String getMyGSTIN() {
		return myGSTIN;
	}
	public void setMyGSTIN(String myGSTIN) {
		this.myGSTIN = myGSTIN;
	}
	public String getSupplyState() {
		return supplyState;
	}
	public void setSupplyState(String supplyState) {
		this.supplyState = supplyState;
	}
	public BigDecimal getTotalUGST() {
		return totalUGST;
	}
	public void setTotalUGST(BigDecimal totalUGST) {
		this.totalUGST = totalUGST;
	}
	public BigDecimal getTotalGST() {
		return totalGST;
	}
	public void setTotalGST(BigDecimal totalGST) {
		this.totalGST = totalGST;
	}
	public BigDecimal getTotalGSTPercent() {
		return totalGSTPercent;
	}
	public void setTotalGSTPercent(BigDecimal totalGSTPercent) {
		this.totalGSTPercent = totalGSTPercent;
	}
	public BigDecimal getTds() {
		return tds;
	}
	public void setTds(BigDecimal tds) {
		this.tds = tds;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	
	
	
	/**
	 * @return the paperInvoiceBatchID
	 */
	public int getPaperInvoiceBatchID() {
		return paperInvoiceBatchID;
	}
	/**
	 * @return the invoiceDate
	 */
	public String getInvoiceDate() {
		return invoiceDate;
	}
	/**
	 * @return the dueDate
	 */
	public String getDueDate() {
		return dueDate;
	}
	/**
	 * @return the entityID
	 */
	public int getEntityID() {
		return entityID;
	}
	/**
	 * @return the entityType
	 */
	public String getEntityType() {
		return entityType;
	}
	/**
	 * @return the ledgerID
	 */
	public int getLedgerID() {
		return ledgerID;
	}
	
	/**
	 * @return the paymentStatus
	 */
	public int getPaymentStatus() {
		return paymentStatus;
	}
	/**
	 * @return the delegatedInvoiceID
	 */
	public int getDelegatedInvoiceID() {
		return delegatedInvoiceID;
	}
	/**
	 * @return the invoiceAmount
	 */
	public BigDecimal getInvoiceAmount() {
		return invoiceAmount;
	}
	/**
	 * @return the balance
	 */
	public BigDecimal getBalance() {
		return balance;
	}
	/**
	 * @return the carriedBalance
	 */
	public BigDecimal getCarriedBalance() {
		return carriedBalance;
	}
	/**
	 * @return the paymentAttempts
	 */
	public int getPaymentAttempts() {
		return paymentAttempts;
	}
	/**
	 * @return the inProcessPayment
	 */
	public int getInProcessPayment() {
		return inProcessPayment;
	}
	/**
	 * @return the isReview
	 */
	public int getIsReview() {
		return isReview;
	}
	/**
	 * @return the currencyID
	 */
	public int getCurrencyID() {
		return currencyID;
	}
	/**
	 * @return the isDeleted
	 */
	public int getIsDeleted() {
		return isDeleted;
	}
	/**
	 * @return the lastReminderDate
	 */
	public String getLastReminderDate() {
		return lastReminderDate;
	}
	/**
	 * @return the overDueSteps
	 */
	public int getOverDueSteps() {
		return overDueSteps;
	}
	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}
	/**
	 * @return the paymentPriority
	 */
	public int getPaymentPriority() {
		return paymentPriority;
	}
	/**
	 * @return the billCycleID
	 */
	public int getBillCycleID() {
		return billCycleID;
	}
	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @return the uptoDate
	 */
	public String getUptoDate() {
		return uptoDate;
	}
	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}
	/**
	 * @return the isMailSent
	 */
	public int getIsMailSent() {
		return isMailSent;
	}
	/**
	 * @return the totalDiscount
	 */
	public BigDecimal getTotalDiscount() {
		return totalDiscount;
	}

	/**
	 * @return the billingAddress
	 */
	public String getBillingAddress() {
		return billingAddress;
	}
	/**
	 * @return the billingCity
	 */
	public String getBillingCity() {
		return billingCity;
	}
	/**
	 * @return the billingState
	 */
	public String getBillingState() {
		return billingState;
	}
	/**
	 * @return the billingZipCode
	 */
	public String getBillingZipCode() {
		return billingZipCode;
	}
	/**
	 * @return the shippingAddress
	 */
	public String getShippingAddress() {
		return shippingAddress;
	}
	/**
	 * @return the shippingCity
	 */
	public String getShippingCity() {
		return shippingCity;
	}
	/**
	 * @return the shippingState
	 */
	public String getShippingState() {
		return shippingState;
	}
	/**
	 * @return the shippingZipCode
	 */
	public String getShippingZipCode() {
		return shippingZipCode;
	}
	/**
	 * @return the customerVO
	 */
	public CustomerVO getCustomerVO() {
		return customerVO;
	}
	/**
	 * @return the vendorVO
	 */
	public VendorVO getVendorVO() {
		return vendorVO;
	}
	/**
	 * @return the employeeVO
	 */
	public EmployeeVO getEmployeeVO() {
		return employeeVO;
	}
	/**
	 * @return the emailID
	 */
	public String getEmailID() {
		return emailID;
	}
	/**
	 * @return the totalCGST
	 */
	public BigDecimal getTotalCGST() {
		return totalCGST;
	}
	/**
	 * @return the totalSGST
	 */
	public BigDecimal getTotalSGST() {
		return totalSGST;
	}
	/**
	 * @return the totalIGST
	 */
	public BigDecimal getTotalIGST() {
		return totalIGST;
	}

	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}

	
	/**
	 * @param paperInvoiceBatchID the paperInvoiceBatchID to set
	 */
	public void setPaperInvoiceBatchID(int paperInvoiceBatchID) {
		this.paperInvoiceBatchID = paperInvoiceBatchID;
	}
	/**
	 * @param invoiceDate the invoiceDate to set
	 */
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	/**
	 * @param dueDate the dueDate to set
	 */
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	/**
	 * @param entityID the entityID to set
	 */
	public void setEntityID(int entityID) {
		this.entityID = entityID;
	}
	/**
	 * @param entityType the entityType to set
	 */
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	/**
	 * @param ledgerID the ledgerID to set
	 */
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	
	/**
	 * @param paymentStatus the paymentStatus to set
	 */
	public void setPaymentStatus(int paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	/**
	 * @param delegatedInvoiceID the delegatedInvoiceID to set
	 */
	public void setDelegatedInvoiceID(int delegatedInvoiceID) {
		this.delegatedInvoiceID = delegatedInvoiceID;
	}
	/**
	 * @param invoiceAmount the invoiceAmount to set
	 */
	public void setInvoiceAmount(BigDecimal invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	/**
	 * @param carriedBalance the carriedBalance to set
	 */
	public void setCarriedBalance(BigDecimal carriedBalance) {
		this.carriedBalance = carriedBalance;
	}
	/**
	 * @param paymentAttempts the paymentAttempts to set
	 */
	public void setPaymentAttempts(int paymentAttempts) {
		this.paymentAttempts = paymentAttempts;
	}
	/**
	 * @param inProcessPayment the inProcessPayment to set
	 */
	public void setInProcessPayment(int inProcessPayment) {
		this.inProcessPayment = inProcessPayment;
	}
	/**
	 * @param isReview the isReview to set
	 */
	public void setIsReview(int isReview) {
		this.isReview = isReview;
	}
	/**
	 * @param currencyID the currencyID to set
	 */
	public void setCurrencyID(int currencyID) {
		this.currencyID = currencyID;
	}
	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	/**
	 * @param lastReminderDate the lastReminderDate to set
	 */
	public void setLastReminderDate(String lastReminderDate) {
		this.lastReminderDate = lastReminderDate;
	}
	/**
	 * @param overDueSteps the overDueSteps to set
	 */
	public void setOverDueSteps(int overDueSteps) {
		this.overDueSteps = overDueSteps;
	}
	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	/**
	 * @param paymentPriority the paymentPriority to set
	 */
	public void setPaymentPriority(int paymentPriority) {
		this.paymentPriority = paymentPriority;
	}
	/**
	 * @param billCycleID the billCycleID to set
	 */
	public void setBillCycleID(int billCycleID) {
		this.billCycleID = billCycleID;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @param uptoDate the uptoDate to set
	 */
	public void setUptoDate(String uptoDate) {
		this.uptoDate = uptoDate;
	}
	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}
	/**
	 * @param isMailSent the isMailSent to set
	 */
	public void setIsMailSent(int isMailSent) {
		this.isMailSent = isMailSent;
	}
	/**
	 * @param totalDiscount the totalDiscount to set
	 */
	public void setTotalDiscount(BigDecimal totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
	
	/**
	 * @param billingAddress the billingAddress to set
	 */
	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}
	/**
	 * @param billingCity the billingCity to set
	 */
	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}
	/**
	 * @param billingState the billingState to set
	 */
	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}
	/**
	 * @param billingZipCode the billingZipCode to set
	 */
	public void setBillingZipCode(String billingZipCode) {
		this.billingZipCode = billingZipCode;
	}
	/**
	 * @param shippingAddress the shippingAddress to set
	 */
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	/**
	 * @param shippingCity the shippingCity to set
	 */
	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}
	/**
	 * @param shippingState the shippingState to set
	 */
	public void setShippingState(String shippingState) {
		this.shippingState = shippingState;
	}
	/**
	 * @param shippingZipCode the shippingZipCode to set
	 */
	public void setShippingZipCode(String shippingZipCode) {
		this.shippingZipCode = shippingZipCode;
	}
	/**
	 * @param customerVO the customerVO to set
	 */
	public void setCustomerVO(CustomerVO customerVO) {
		this.customerVO = customerVO;
	}
	/**
	 * @param vendorVO the vendorVO to set
	 */
	public void setVendorVO(VendorVO vendorVO) {
		this.vendorVO = vendorVO;
	}
	/**
	 * @param employeeVO the employeeVO to set
	 */
	public void setEmployeeVO(EmployeeVO employeeVO) {
		this.employeeVO = employeeVO;
	}
	/**
	 * @param emailID the emailID to set
	 */
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
	/**
	 * @param totalCGST the totalCGST to set
	 */
	public void setTotalCGST(BigDecimal totalCGST) {
		this.totalCGST = totalCGST;
	}
	/**
	 * @param totalSGST the totalSGST to set
	 */
	public void setTotalSGST(BigDecimal totalSGST) {
		this.totalSGST = totalSGST;
	}
	/**
	 * @param totalIGST the totalIGST to set
	 */
	public void setTotalIGST(BigDecimal totalIGST) {
		this.totalIGST = totalIGST;
	}
	/**
	 * @return the invoiceTypeID
	 */
	public int getInvoiceTypeID() {
		return invoiceTypeID;
	}
	/**
	 * @param invoiceTypeID the invoiceTypeID to set
	 */
	public void setInvoiceTypeID(int invoiceTypeID) {
		this.invoiceTypeID = invoiceTypeID;
	}

	public String getTdsSectionNo() {
		return tdsSectionNo;
	}
	public void setTdsSectionNo(String tdsSectionNo) {
		this.tdsSectionNo = tdsSectionNo;
	}
	public BigDecimal getTdsRate() {
		return tdsRate;
	}
	public void setTdsRate(BigDecimal tdsRate) {
		this.tdsRate = tdsRate;
	}
	public String getTotalAmountInWords() {
		return totalAmountInWords;
	}
	public void setTotalAmountInWords(String totalAmountInWords) {
		this.totalAmountInWords = totalAmountInWords;
	}
	public SocietyVO getOrgVO() {
		return orgVO;
	}
	public void setOrgVO(SocietyVO orgVO) {
		this.orgVO = orgVO;
	}
	public BankInfoVO getBankVO() {
		return bankVO;
	}
	public void setBankVO(BankInfoVO bankVO) {
		this.bankVO = bankVO;
	}
	public int getIsGstCalculationInclusive() {
		return isGstCalculationInclusive;
	}
	public void setIsGstCalculationInclusive(int isGstCalculationInclusive) {
		this.isGstCalculationInclusive = isGstCalculationInclusive;
	}
	/**
	 * @return the isSales
	 */
	public int getIsSales() {
		return isSales;
	}
	/**
	 * @return the isReverse
	 */
	public int getIsReverse() {
		return isReverse;
	}
	/**
	 * @param isSales the isSales to set
	 */
	public void setIsSales(int isSales) {
		this.isSales = isSales;
	}
	/**
	 * @param isReverse the isReverse to set
	 */
	public void setIsReverse(int isReverse) {
		this.isReverse = isReverse;
	}
	public CurrencyVO getCurrencyVO() {
		return currencyVO;
	}
	public void setCurrencyVO(CurrencyVO currencyVO) {
		this.currencyVO = currencyVO;
	}
	public BigDecimal getCurrencyRate() {
		return currencyRate;
	}
	public void setCurrencyRate(BigDecimal currencyRate) {
		this.currencyRate = currencyRate;
	}
	/**
	 * @return the invoiceTags
	 */
	public String getInvoiceTags() {
		return invoiceTags;
	}
	/**
	 * @param invoiceTags the invoiceTags to set
	 */
	public void setInvoiceTags(String invoiceTags) {
		this.invoiceTags = invoiceTags;
	}
	/**
	 * @return the gstInvoiceTypeID
	 */
	public int getGstInvoiceTypeID() {
		return gstInvoiceTypeID;
	}
	/**
	 * @param gstInvoiceTypeID the gstInvoiceTypeID to set
	 */
	public void setGstInvoiceTypeID(int gstInvoiceTypeID) {
		this.gstInvoiceTypeID = gstInvoiceTypeID;
	}
	/**
	 * @return the gstInvoiceType
	 */
	public String getGstInvoiceType() {
		return gstInvoiceType;
	}
	/**
	 * @param gstInvoiceType the gstInvoiceType to set
	 */
	public void setGstInvoiceType(String gstInvoiceType) {
		this.gstInvoiceType = gstInvoiceType;
	}
	/**
	 * @return the portCode
	 */
	public String getPortCode() {
		return portCode;
	}
	/**
	 * @param portCode the portCode to set
	 */
	public void setPortCode(String portCode) {
		this.portCode = portCode;
	}
	/**
	 * @return the shippingBillNo
	 */
	public String getShippingBillNo() {
		return shippingBillNo;
	}
	/**
	 * @param shippingBillNo the shippingBillNo to set
	 */
	public void setShippingBillNo(String shippingBillNo) {
		this.shippingBillNo = shippingBillNo;
	}
	/**
	 * @return the shippingDate
	 */
	public String getShippingDate() {
		return shippingDate;
	}
	/**
	 * @param shippingDate the shippingDate to set
	 */
	public void setShippingDate(String shippingDate) {
		this.shippingDate = shippingDate;
	}
	public String getSupplyStateID() {
		return supplyStateID;
	}
	public void setSupplyStateID(String supplyStateID) {
		this.supplyStateID = supplyStateID;
	}
	public int getInvLineItemID() {
		return invLineItemID;
	}
	public void setInvLineItemID(int invLineItemID) {
		this.invLineItemID = invLineItemID;
	}
	public int getLinkedInvoiceID() {
		return linkedInvoiceID;
	}
	public void setLinkedInvoiceID(int linkedInvoiceID) {
		this.linkedInvoiceID = linkedInvoiceID;
	}
	/**
	 * @return the seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * @param seqNo the seqNo to set
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * @return the paidInvAmount
	 */
	public BigDecimal getPaidInvAmount() {
		return paidInvAmount;
	}
	/**
	 * @param paidInvAmount the paidInvAmount to set
	 */
	public void setPaidInvAmount(BigDecimal paidInvAmount) {
		this.paidInvAmount = paidInvAmount;
	}
	/**
	 * @return the linkInvList
	 */
	public List<InvoiceVO> getLinkInvList() {
		return linkInvList;
	}
	/**
	 * @param linkInvList the linkInvList to set
	 */
	public void setLinkInvList(List<InvoiceVO> linkInvList) {
		this.linkInvList = linkInvList;
	}
	/**
	 * @return the gstCategory
	 */
	public String getGstCategory() {
		return gstCategory;
	}
	/**
	 * @param gstCategory the gstCategory to set
	 */
	public void setGstCategory(String gstCategory) {
		this.gstCategory = gstCategory;
	}
	/**
	 * @return the zeroAmount
	 */
	public BigDecimal getZeroAmount() {
		return zeroAmount;
	}
	/**
	 * @param zeroAmount the zeroAmount to set
	 */
	public void setZeroAmount(BigDecimal zeroAmount) {
		this.zeroAmount = zeroAmount;
	}
	/**
	 * @return the thirtyAmount
	 */
	public BigDecimal getThirtyAmount() {
		return thirtyAmount;
	}
	/**
	 * @param thirtyAmount the thirtyAmount to set
	 */
	public void setThirtyAmount(BigDecimal thirtyAmount) {
		this.thirtyAmount = thirtyAmount;
	}
	/**
	 * @return the sixtyAmount
	 */
	public BigDecimal getSixtyAmount() {
		return sixtyAmount;
	}
	/**
	 * @param sixtyAmount the sixtyAmount to set
	 */
	public void setSixtyAmount(BigDecimal sixtyAmount) {
		this.sixtyAmount = sixtyAmount;
	}
	/**
	 * @return the nintyAmount
	 */
	public BigDecimal getNinetyAmount() {
		return ninetyAmount;
	}
	/**
	 * @param nintyAmount the nintyAmount to set
	 */
	public void setNinetyAmount(BigDecimal ninetyAmount) {
		this.ninetyAmount = ninetyAmount;
	}
	/**
	 * @return the ninetyPlusAmount
	 */
	public BigDecimal getNinetyPlusAmount() {
		return ninetyPlusAmount;
	}
	/**
	 * @param ninetyPlusAmount the ninetyPlusAmount to set
	 */
	public void setNinetyPlusAmount(BigDecimal ninetyPlusAmount) {
		this.ninetyPlusAmount = ninetyPlusAmount;
	}
	/**
	 * @return the overDueAmount
	 */
	public BigDecimal getOverDueAmount() {
		return overDueAmount;
	}
	/**
	 * @param overDueAmount the overDueAmount to set
	 */
	public void setOverDueAmount(BigDecimal overDueAmount) {
		this.overDueAmount = overDueAmount;
	}
	/**
	 * @return the isDueDate
	 */
	public int getIsDueDate() {
		return isDueDate;
	}
	/**
	 * @param isDueDate the isDueDate to set
	 */
	public void setIsDueDate(int isDueDate) {
		this.isDueDate = isDueDate;
	}
	/**
	 * @return the customerPAN
	 */
	public String getCustomerPAN() {
		return customerPAN;
	}
	/**
	 * @param customerPAN the customerPAN to set
	 */
	public void setCustomerPAN(String customerPAN) {
		this.customerPAN = customerPAN;
	}
	/**
	 * @return the overDueThirtyAmount
	 */
	public BigDecimal getOverDueThirtyAmount() {
		return overDueThirtyAmount;
	}
	/**
	 * @param overDueThirtyAmount the overDueThirtyAmount to set
	 */
	public void setOverDueThirtyAmount(BigDecimal overDueThirtyAmount) {
		this.overDueThirtyAmount = overDueThirtyAmount;
	}
	/**
	 * @return the overDueSixtyAmount
	 */
	public BigDecimal getOverDueSixtyAmount() {
		return overDueSixtyAmount;
	}
	/**
	 * @param overDueSixtyAmount the overDueSixtyAmount to set
	 */
	public void setOverDueSixtyAmount(BigDecimal overDueSixtyAmount) {
		this.overDueSixtyAmount = overDueSixtyAmount;
	}
	/**
	 * @return the overDueNinetyAmount
	 */
	public BigDecimal getOverDueNinetyAmount() {
		return overDueNinetyAmount;
	}
	/**
	 * @param overDueNinetyAmount the overDueNinetyAmount to set
	 */
	public void setOverDueNinetyAmount(BigDecimal overDueNinetyAmount) {
		this.overDueNinetyAmount = overDueNinetyAmount;
	}
	/**
	 * @return the overDueNinetyPlusAmount
	 */
	public BigDecimal getOverDueNinetyPlusAmount() {
		return overDueNinetyPlusAmount;
	}
	/**
	 * @param overDueNinetyPlusAmount the overDueNinetyPlusAmount to set
	 */
	public void setOverDueNinetyPlusAmount(BigDecimal overDueNinetyPlusAmount) {
		this.overDueNinetyPlusAmount = overDueNinetyPlusAmount;
	}
	/**
	 * @return the productID
	 */
	public int getProductID() {
		return productID;
	}
	/**
	 * @param productID the productID to set
	 */
	public void setProductID(int productID) {
		this.productID = productID;
	}
	/**
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	/**
	 * @return the totalPrice
	 */
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	/**
	 * @param totalPrice the totalPrice to set
	 */
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

}
