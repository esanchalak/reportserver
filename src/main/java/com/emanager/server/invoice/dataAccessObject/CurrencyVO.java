package com.emanager.server.invoice.dataAccessObject;

public class CurrencyVO {

	
	private int currencyID;
	private String currencyName;
	private String country;
	private String currencyCode;
	private String currencySymbol;
	private String decimalCurrency;
	
	public int getCurrencyID() {
		return currencyID;
	}
	public void setCurrencyID(int currencyID) {
		this.currencyID = currencyID;
	}
	public String getCurrencyName() {
		return currencyName;
	}
	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getCurrencySymbol() {
		return currencySymbol;
	}
	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}
	public String getDecimalCurrency() {
		return decimalCurrency;
	}
	public void setDecimalCurrency(String decimalCurrency) {
		this.decimalCurrency = decimalCurrency;
	}
}
