package com.emanager.server.invoice.dataAccessObject;

import java.math.BigDecimal;
import java.util.List;

public class InvoiceBillingCycleVO {
	
	private int societyId;
	private String societyName;
	private int aptID;
	private int billingProcessID;
	private String fromDate;
	private String uptoDate;
	private String dueDate;
	private String description;
	private String runDate;
	private String nextRunDate;
	private String invoiceType;
	private int billingCycleID;
	private List invoiceList;
	private String invoiceDate;
	private String invoicePeriod;
	private int masterBillingID;
	
	private int memberID;

	public int getAptID() {
		return aptID;
	}

	public void setAptID(int aptID) {
		this.aptID = aptID;
	}

	public int getBillingProcessID() {
		return billingProcessID;
	}

	public void setBillingProcessID(int billingProcessID) {
		this.billingProcessID = billingProcessID;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public int getMemberID() {
		return memberID;
	}

	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}

	public String getNextRunDate() {
		return nextRunDate;
	}

	public void setNextRunDate(String nextRunDate) {
		this.nextRunDate = nextRunDate;
	}

	public String getRunDate() {
		return runDate;
	}

	public void setRunDate(String runDate) {
		this.runDate = runDate;
	}

	public int getSocietyId() {
		return societyId;
	}

	public void setSocietyId(int societyId) {
		this.societyId = societyId;
	}

	public String getSocietyName() {
		return societyName;
	}

	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}

	public String getUptoDate() {
		return uptoDate;
	}

	public void setUptoDate(String uptoDate) {
		this.uptoDate = uptoDate;
	}

	public int getBillingCycleID() {
		return billingCycleID;
	}

	public void setBillingCycleID(int billingCycleID) {
		this.billingCycleID = billingCycleID;
	}

	public List getInvoiceList() {
		return invoiceList;
	}

	public void setInvoiceList(List invoiceList) {
		this.invoiceList = invoiceList;
	}

	public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	/**
	 * @return the invoicePeriod
	 */
	public String getInvoicePeriod() {
		return invoicePeriod;
	}

	/**
	 * @param invoicePeriod the invoicePeriod to set
	 */
	public void setInvoicePeriod(String invoicePeriod) {
		this.invoicePeriod = invoicePeriod;
	}

	/**
	 * @return the masterBillingID
	 */
	public int getMasterBillingID() {
		return masterBillingID;
	}

	/**
	 * @param masterBillingID the masterBillingID to set
	 */
	public void setMasterBillingID(int masterBillingID) {
		this.masterBillingID = masterBillingID;
	}
}
