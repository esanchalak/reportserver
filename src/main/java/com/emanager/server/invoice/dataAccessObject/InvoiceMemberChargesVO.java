package com.emanager.server.invoice.dataAccessObject;

import java.math.BigDecimal;
import java.util.List;

public class InvoiceMemberChargesVO {
	
	private int societyID;
	private String societyName;
	private int aptID;
	private int memberID;
	private String memberName;
	private int ledgerID;
	private BigDecimal squareFt;
	private BigDecimal unitCost;
	private int unitID;
	private int buildingID;
	private int isRented;
	private Boolean isActive;
	private String fromDate;
	private String uptoDate;
	private int invoiceTypeID;
	private BigDecimal invBalance;
	private BigDecimal ledgBalance;
	private BigDecimal balDifference;
	private String lateFeeType;
	private int lateFeeFor;
	private String categoryName;
	private String runDate;
	private String nextRunDate;
	private String dueDate;
	private String invoiceType;
	private Boolean isApplicable;
	private String frequency;
	private int subGroupID;
	private String status;
	private int chargeID;
	private BigDecimal entityID;
	private String calculationMethod;
	private BigDecimal totalAmount;
	private BigDecimal balance;
	private BigDecimal carriedBalance;
	private int billingProcessID;
	private String description;
	private int paymentPriority;
	private String invoiceDate;
	private String ledgerName;
	private int invoiceID;
	private List objectList;
	private String invoiceGroups;
	private String params;
	private int precison;
	
	
	public int getAptID() {
		return aptID;
	}
	public void setAptID(int aptID) {
		this.aptID = aptID;
	}
	public int getMemberID() {
		return memberID;
	}
	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public int getLedgerID() {
		return ledgerID;
	}
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	public BigDecimal getSquareFt() {
		return squareFt;
	}
	public void setSquareFt(BigDecimal squareFt) {
		this.squareFt = squareFt;
	}
	public BigDecimal getUnitCost() {
		return unitCost;
	}
	public void setUnitCost(BigDecimal unitCost) {
		this.unitCost = unitCost;
	}
	public int getUnitID() {
		return unitID;
	}
	public void setUnitID(int unitID) {
		this.unitID = unitID;
	}
	public int getBuildingID() {
		return buildingID;
	}
	public void setBuildingID(int buildingID) {
		this.buildingID = buildingID;
	}
	public int getIsRented() {
		return isRented;
	}
	public void setIsRented(int isRented) {
		this.isRented = isRented;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getUptoDate() {
		return uptoDate;
	}
	public void setUptoDate(String uptoDate) {
		this.uptoDate = uptoDate;
	}
	public int getInvoiceTypeID() {
		return invoiceTypeID;
	}
	public void setInvoiceTypeID(int invoiceTypeID) {
		this.invoiceTypeID = invoiceTypeID;
	}
	public BigDecimal getBalDifference() {
		return balDifference;
	}
	public void setBalDifference(BigDecimal balDifference) {
		this.balDifference = balDifference;
	}
	public BigDecimal getInvBalance() {
		return invBalance;
	}
	public void setInvBalance(BigDecimal invBalance) {
		this.invBalance = invBalance;
	}
	public BigDecimal getLedgBalance() {
		return ledgBalance;
	}
	public void setLedgBalance(BigDecimal ledgBalance) {
		this.ledgBalance = ledgBalance;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public int getChargeID() {
		return chargeID;
	}
	public void setChargeID(int chargeID) {
		this.chargeID = chargeID;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getInvoiceType() {
		return invoiceType;
	}
	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}
	public Boolean getIsApplicable() {
		return isApplicable;
	}
	public void setIsApplicable(Boolean isApplicable) {
		this.isApplicable = isApplicable;
	}
	public int getLateFeeFor() {
		return lateFeeFor;
	}
	public void setLateFeeFor(int lateFeeFor) {
		this.lateFeeFor = lateFeeFor;
	}
	public String getLateFeeType() {
		return lateFeeType;
	}
	public void setLateFeeType(String lateFeeType) {
		this.lateFeeType = lateFeeType;
	}
	public String getNextRunDate() {
		return nextRunDate;
	}
	public void setNextRunDate(String nextRunDate) {
		this.nextRunDate = nextRunDate;
	}
	public String getRunDate() {
		return runDate;
	}
	public void setRunDate(String runDate) {
		this.runDate = runDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getSubGroupID() {
		return subGroupID;
	}
	public void setSubGroupID(int subGroupID) {
		this.subGroupID = subGroupID;
	}
	public BigDecimal getEntityID() {
		return entityID;
	}
	public void setEntityID(BigDecimal entityID) {
		this.entityID = entityID;
	}
	public String getCalculationMethod() {
		return calculationMethod;
	}
	public void setCalculationMethod(String calculationMethod) {
		this.calculationMethod = calculationMethod;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getPaymentPriority() {
		return paymentPriority;
	}
	public void setPaymentPriority(int paymentPriority) {
		this.paymentPriority = paymentPriority;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public BigDecimal getCarriedBalance() {
		return carriedBalance;
	}
	public void setCarriedBalance(BigDecimal carriedBalance) {
		this.carriedBalance = carriedBalance;
	}
	public int getBillingProcessID() {
		return billingProcessID;
	}
	public void setBillingProcessID(int billingProcessID) {
		this.billingProcessID = billingProcessID;
	}
	public String getLedgerName() {
		return ledgerName;
	}
	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}
	public int getInvoiceID() {
		return invoiceID;
	}
	public void setInvoiceID(int invoiceID) {
		this.invoiceID = invoiceID;
	}
	public List getObjectList() {
		return objectList;
	}
	public void setObjectList(List objectList) {
		this.objectList = objectList;
	}
	public String getInvoiceGroups() {
		return invoiceGroups;
	}
	public void setInvoiceGroups(String invoiceGroups) {
		this.invoiceGroups = invoiceGroups;
	}
	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}
	public int getPrecison() {
		return precison;
	}
	public void setPrecison(int precison) {
		this.precison = precison;
	}
	
	
	
	
	
	
}
