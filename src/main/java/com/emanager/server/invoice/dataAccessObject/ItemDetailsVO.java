package com.emanager.server.invoice.dataAccessObject;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class ItemDetailsVO {
	
	private int itemID;
	private int orgID;
	private int ledgerID;
	private String itemName;
	private String description;
	private BigDecimal  purchasePrice=BigDecimal.ZERO;
	private BigDecimal sellingPrice=BigDecimal.ZERO;
	private BigDecimal discount=BigDecimal.ZERO;
	private String type;
	private String hsnsacCode;
	private String unitOfMeasurement;
	private BigDecimal balanceQuantity;//Current balance of products
	private BigDecimal reorderLevel; //Threshould level of stocks	
	private BigDecimal gst;
	private BigDecimal cess;
	private int chapterID;
	private String chapterName;
	private String chapterDescription;
	private String salesLedgerName;
	private int isActive;
	private String hsnSacID;
	private String salesLedgerID;
	private String purchaseLedgerID;	
	private String itemType;
	private String purchaseLedgerName;
	private int tdsSectionID;// TDS section ID from TDS Rate details table
	private String ledgerType;
	private String productCode;
	private String productBrand;
	private String tags;
	private int gstCategoryID;
	private String gstCategoryName;
	private int rateCardID;
	private String rateCardName;
	private int rateCardItemID;
    private String creatorName;
    private String updaterName;
    private int createdBy;
    private int updatedBy;
	private Timestamp createdDate;
	private Timestamp updatedDate;
	
	public String getLedgerType() {
		return ledgerType;
	}

	public void setLedgerType(String ledgerType) {
		this.ledgerType = ledgerType;
	}

	public String getSalesLedgerName() {
		return salesLedgerName;
	}

	public void setSalesLedgerName(String salesLedgerName) {
		this.salesLedgerName = salesLedgerName;
	}

	public String getSalesLedgerID() {
		return salesLedgerID;
	}

	public void setSalesLedgerID(String salesLedgerID) {
		this.salesLedgerID = salesLedgerID;
	}

	public String getPurchaseLedgerID() {
		return purchaseLedgerID;
	}

	public void setPurchaseLedgerID(String purchaseLedgerID) {
		this.purchaseLedgerID = purchaseLedgerID;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getPurchaseLedgerName() {
		return purchaseLedgerName;
	}

	public void setPurchaseLedgerName(String purchaseLedgerName) {
		this.purchaseLedgerName = purchaseLedgerName;
	}

	public String getHsnsacCode() {
		return hsnsacCode;
	}

	public void setHsnsacCode(String hsnsacCode) {
		this.hsnsacCode = hsnsacCode;
	}

	public String getHsnSacID() {
		return hsnSacID;
	}

	public void setHsnSacID(String hsnSacID) {
		this.hsnSacID = hsnSacID;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	
	public String getChapterName() {
		return chapterName;
	}
	public void setChapterName(String chapterName) {
		this.chapterName = chapterName;
	}
	public String getChapterDescription() {
		return chapterDescription;
	}
	public void setChapterDescription(String chapterDescription) {
		this.chapterDescription = chapterDescription;
	}
	public int getChapterID() {
		return chapterID;
	}
	public void setChapterID(int chapterID) {
		this.chapterID = chapterID;
	}
	public BigDecimal getGst() {
		return gst;
	}
	public void setGst(BigDecimal gst) {
		this.gst = gst;
	}
	public BigDecimal getCess() {
		return cess;
	}
	public void setCess(BigDecimal cess) {
		this.cess = cess;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * @return the itemID
	 */
	public int getItemID() {
		return itemID;
	}
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @return the ledgerID
	 */
	public int getLedgerID() {
		return ledgerID;
	}
	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @return the purchasePrice
	 */
	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}
	/**
	 * @return the sellingPrice
	 */
	public BigDecimal getSellingPrice() {
		return sellingPrice;
	}
	/**
	 * @return the discount
	 */
	public BigDecimal getDiscount() {
		return discount;
	}
	
	/**
	 * @return the unitOfMeasurement
	 */
	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}
	/**
	 * @return the balanceQuantity
	 */
	public BigDecimal getBalanceQuantity() {
		return balanceQuantity;
	}
	/**
	 * @return the reorderLevel
	 */
	public BigDecimal getReorderLevel() {
		return reorderLevel;
	}

	/**
	 * @param itemID the itemID to set
	 */
	public void setItemID(int itemID) {
		this.itemID = itemID;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	/**
	 * @param ledgerID the ledgerID to set
	 */
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	/**
	 * @param itemName the itemName to set
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @param purchasePrice the purchasePrice to set
	 */
	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	/**
	 * @param sellingPrice the sellingPrice to set
	 */
	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}
	/**
	 * @param discount the discount to set
	 */
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	
	/**
	 * @param unitOfMeasurement the unitOfMeasurement to set
	 */
	public void setUnitOfMeasurement(String unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}
	/**
	 * @param balanceQuantity the balanceQuantity to set
	 */
	public void setBalanceQuantity(BigDecimal balanceQuantity) {
		this.balanceQuantity = balanceQuantity;
	}
	/**
	 * @param reorderLevel the reorderLevel to set
	 */
	public void setReorderLevel(BigDecimal reorderLevel) {
		this.reorderLevel = reorderLevel;
	}

	/**
	 * @return the tdsSectionID
	 */
	public int getTdsSectionID() {
		return tdsSectionID;
	}

	/**
	 * @param tdsSectionID the tdsSectionID to set
	 */
	public void setTdsSectionID(int tdsSectionID) {
		this.tdsSectionID = tdsSectionID;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductBrand() {
		return productBrand;
	}

	public void setProductBrand(String productBrand) {
		this.productBrand = productBrand;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getGstCategoryName() {
		return gstCategoryName;
	}

	public void setGstCategoryName(String gstCategoryName) {
		this.gstCategoryName = gstCategoryName;
	}

	public int getGstCategoryID() {
		return gstCategoryID;
	}

	public void setGstCategoryID(int gstCategoryID) {
		this.gstCategoryID = gstCategoryID;
	}

	/**
	 * @return the rateCardID
	 */
	public int getRateCardID() {
		return rateCardID;
	}

	/**
	 * @param rateCardID the rateCardID to set
	 */
	public void setRateCardID(int rateCardID) {
		this.rateCardID = rateCardID;
	}

	/**
	 * @return the rateCardName
	 */
	public String getRateCardName() {
		return rateCardName;
	}

	/**
	 * @param rateCardName the rateCardName to set
	 */
	public void setRateCardName(String rateCardName) {
		this.rateCardName = rateCardName;
	}

	/**
	 * @return the rateCardItemID
	 */
	public int getRateCardItemID() {
		return rateCardItemID;
	}

	/**
	 * @param rateCardItemID the rateCardItemID to set
	 */
	public void setRateCardItemID(int rateCardItemID) {
		this.rateCardItemID = rateCardItemID;
	}

	/**
	 * @return the creatorName
	 */
	public String getCreatorName() {
		return creatorName;
	}

	/**
	 * @param creatorName the creatorName to set
	 */
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	/**
	 * @return the updaterName
	 */
	public String getUpdaterName() {
		return updaterName;
	}

	/**
	 * @param updaterName the updaterName to set
	 */
	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName;
	}

	/**
	 * @return the createdBy
	 */
	public int getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the updatedBy
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the createdDate
	 */
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the updatedDate
	 */
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}

	


}
