package com.emanager.server.invoice.Services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;

import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.invoice.Domain.CustomerDomain;
import com.emanager.server.invoice.dataAccessObject.CustomerVO;
import com.emanager.server.vendorManagement.domainObject.VendorDomain;
import com.emanager.server.vendorManagement.valueObject.VendorVO;


public class CustomerService {
    
    Logger logger=Logger.getLogger(CustomerService.class);
    CustomerDomain customerDomain;
    
    public List getCustomerDetaiilsList(int orgID)
	{
		List customerList=null;
		try
		{
			logger.debug("Entry: public List getCustomerDetaiilsList(int orgID)");
			customerList=customerDomain.getCustomerDetaiilsList(orgID);
			
		}catch(Exception ex)
		{
			logger.error("Exception in getCustomerDetaiilsList : "+ex);
			
		}
		logger.debug("Exit: public List getCustomerDetaiilsList()");
		return customerList; 
	}

	
	
	public int updateCustomerDetails(CustomerVO custVO,AddressVO addressVO) {

		int sucess = 0;  
		int successAdress=0;
		try{
		
		    logger.debug("Entry : public int updateCustomerDetails(CustomerVO custVO,AddressVO addressVO)");
		
		    sucess = customerDomain.updateCustomerDetails(custVO, addressVO);
		    
		   					
		    logger.debug("Exit : public int updateCustomerDetails(CustomerVO custVO,AddressVO addressVO)");

		}catch(Exception Ex){
			
		
			logger.error("Exception in updateCustomerDetails(CustomerVO custVO,AddressVO addressVO) : "+Ex);
			
		}
		return sucess;
	}
	
	public int deleteCustomer(int custID){
		
		int sucess = 0;
		try{
		
		logger.debug("Entry : public int deleteCustomer(int custID)");
		
		sucess = customerDomain.deleteCustomer(custID);
				
		logger.debug("Exit : public int  deleteCustomer(int custID)");

		}catch(Exception Ex){		
			logger.error("Exception in deleteCustomer : "+Ex);			
		}
		return sucess;
	}


    public void setCustomerDomain(CustomerDomain customerDomain) {
        this.customerDomain = customerDomain;
    }

}

