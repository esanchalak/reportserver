package com.emanager.server.invoice.Services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import com.emanager.server.accounts.DataAccessObjects.ChargesVO;
import com.emanager.server.accounts.DataAccessObjects.OnlinePaymentGatewayVO;
import com.emanager.server.accounts.DataAccessObjects.ScheduledTransactionResponseVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Domain.ReceiptInvoiceDomain;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.financialReports.valueObject.MonthwiseChartVO;
import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.invoice.Domain.InvoiceDomain;
import com.emanager.server.invoice.dataAccessObject.BillDetailsVO;
import com.emanager.server.invoice.dataAccessObject.InLineItemsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceBillingCycleVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceDetailsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceMemberChargesVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceVO;
import com.emanager.server.invoice.dataAccessObject.ItemDetailsVO;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.webservice.JSONResponseVO;

public class InvoiceService {
	InvoiceDomain invoiceDomain;
	MemberService memberServiceBean;
	ReceiptInvoiceDomain receiptInvoiceDomain;
	TransactionService transactionService;
	
	private static final Logger logger = Logger.getLogger(InvoiceService.class);
	
	public void executeInvoiceInsert(String societyID,String billingCycleID){
		try{
		logger.debug("Entry :public void executeInvoiceInsert(String societyID,String billingCycleID)");
		List societyList=getActiveSocietyList(societyID,billingCycleID);
		for(int i=0;societyList.size()>i;i++){
			InvoiceBillingCycleVO bcVO=(InvoiceBillingCycleVO) societyList.get(i);
		List memberList=getActiveMemberList(bcVO);
		
		}
		logger.debug("Exit :public void executeInvoiceInsert(String societyID)");
		}catch (Exception e) {
			logger.debug("Exception :public void executeInvoiceInsert(String societyID)"+e);
		}
	}
	
	public List getBillingCycleList(int societyID){
		logger.debug("Entry : public List getBillingCycleList(String societyID)");
		List billingCycleList=null;
		
		billingCycleList=invoiceDomain.getBillingCycleList(societyID);
		
		logger.debug("Exit : public List getBillingCycleList(String societyID)");
		
		return billingCycleList;
	}
	
	public InvoiceBillingCycleVO getBillingCycleDetails(int billingCycleID){
		logger.debug("Entry : public InvoiceBillingCycleVO getBillingCycleDetails(int billingCycleID)");
		InvoiceBillingCycleVO billingCycleVO=new InvoiceBillingCycleVO();
		
		billingCycleVO=invoiceDomain.getBillingCycleDetails(billingCycleID);
		
		logger.debug("Exit : public InvoiceBillingCycleVO getBillingCycleDetails(int billingCycleID)");
		
		return billingCycleVO;
	}
	
	
	/* Get list of active society list with billing cycle details */
	public List getActiveSocietyList(String societyID,String billingCycleID){
		List societyList=null;
		try{
		logger.debug("Entry : public List getActiveSocietyList()");
		
		societyList=invoiceDomain.getActiveSocietyList(societyID,billingCycleID);
		
		
		
		logger.debug("Exit : public List getActiveSocietyList()");
		}catch (Exception e) {
			logger.debug("Exception : public List getActiveSocietyList()");
		}
		return societyList;		
	}
	
	/* Get list of all active members of the given society  */
	public List getActiveMemberListAll(int societyID){
		List memberList=null;
		List invoiceList=new ArrayList();
		logger.debug("Entry : public List getActiveMemberList(String societyID)");
		
		try{
		
		memberList=invoiceDomain.getActiveMemberListAll(societyID);
		
		}catch (Exception e) {
			logger.error("Exception in getActiveMemberList "+e);
		}
		
		logger.debug("Exit : public List getActiveMemberList(String societyID)"+memberList.size());
		return memberList;		
	}
	
	/* Get list of all active members of the given society who's run_date is current_date  */
	public List getActiveMemberList(InvoiceBillingCycleVO bcVO){
		List invoiceList=null;
		logger.debug("Entry : public List getActiveMemberList(String societyID)");
		int societyID=bcVO.getSocietyId();
		invoiceList=invoiceDomain.getActiveMemberList(bcVO);
		
		for(int i=0;invoiceList.size()>i;i++){
			InvoiceDetailsVO invoiceVO=(InvoiceDetailsVO) invoiceList.get(i);
			invoiceVO.setBillCycleID(bcVO.getBillingCycleID());
			int success=0;
			success=insertNewInvoice(invoiceVO,societyID);
			if(success==1){
				logger.info("Invoice Successfully added");
			}
			
		}
		
		logger.debug("Exit : public List getActiveMemberList(String societyID)");
		return invoiceList;		
	}
	/* Check the invoices before commit for Manual invoice */
	public List checkInvoices(InvoiceMemberChargesVO memberChrgVO,List memberChargeList,ArrayList<Integer> invoiceTypeList){
		int insertFlag=0;
		List invoiceList=new ArrayList();
		logger.debug("Entry : public List checkInvoices(List MemberList,List memberChargeList)"+invoiceTypeList.size());
			for(int i=0;invoiceTypeList.size()>i;i++){
			int invoiceType= (int) invoiceTypeList.get(i);
			
			
			InvoiceDetailsVO invoiceVO=invoiceDomain.generateInvoiceObject(memberChrgVO, memberChargeList,invoiceType);
			
			if(invoiceVO.getInvoiceAmount().signum()!=0){
			//invoiceVO.setListOfLineItems(memberChargeList);
			invoiceList.add(invoiceVO);
			}}
					
		logger.debug("Exit :public List checkInvoices(List MemberList,List memberChargeList)");
		return invoiceList;
	}
	/* Add manualInvoices with journal entries */
	public int addInvoice(List invoiceList,int societyID,int isAutoComment){
		logger.debug("Entry : public int addInvoice(List invoiceList,String societyID)");
		int success=0;
		
		success=invoiceDomain.addInvoice(invoiceList, societyID,isAutoComment);
		
		logger.debug("Entry : public int addInvoice(List invoiceList,String societyID)");
		return success;
	}
	/* Insert bulk invoices  */
	public List checkInvoicesList(List memberList,InvoiceBillingCycleVO billingVO,int invoiceTypeID){
		int insertFlag=0;
		List invoiceList=new ArrayList();
		logger.debug("Entry : public List checkInvoicesList(List MemberList,InvoiceDetailsVO invoiceVO)");
		
		if(invoiceTypeID==10){
			invoiceList=invoiceDomain.checkLateFeeInvoicesList(memberList, billingVO, invoiceTypeID);
		}else
		invoiceList=invoiceDomain.checkInvoicesList(memberList, billingVO, invoiceTypeID);
		
		
		
		logger.debug("Exit : public List checkInvoicesList(List MemberList,InvoiceDetailsVO invoiceVO)");
		return invoiceList;
	}
	/* Get list of all applicable fees for the given member  */
	public int insertNewInvoice(InvoiceDetailsVO invoiceVO,int societyID){
		int insertFlag=0;
		logger.debug("Entry : public int insertNewInvoice(int societyID,int apartmentID)");
		
		insertFlag=invoiceDomain.insertInvoice(invoiceVO,societyID);
		
		logger.debug("Exit : public int insertNewInvoice(int societyID,int apartmentID)");
		return insertFlag;		
	}
	
	
	
	/* Update the charge period after adding the invoice  */
	public int updateChargeStatus(InvoiceDetailsVO invoiceVO,int societyID,InvoiceMemberChargesVO chargeVO){
		int insertFlag=0;
		logger.debug("Entry : public int updateChargeStatus(int societyID,int apartmentID)");
		
		insertFlag=invoiceDomain.updateChargeStatus(invoiceVO, societyID, chargeVO);
		
		logger.debug("Exit : public int updateChargeStatus(int societyID,int apartmentID)");
		return insertFlag;		
	}
	
	/* Get list of all applicable fees for the given member  */
	public List getCharges(int societyID,int aptID,InvoiceMemberChargesVO membrChrgVO){
		List memberChargeList=null;
		logger.debug("Entry : public List getCharges(int societyID,int aptID)");
		
		InvoiceDetailsVO invoiceVO=new InvoiceDetailsVO();
		InvoiceBillingCycleVO bcVO=new InvoiceBillingCycleVO();
		int isCommon=1;
		
		invoiceVO.setMemberVO(membrChrgVO);
		
		
		
		
		
		memberChargeList=invoiceDomain.getCharges(invoiceVO,bcVO,isCommon);
		
		logger.debug("Exit : public List getApplicableChargesToMember(InvoiceDetailsVO inoiceVO,InvoiceBillingCycleVO bcVO,int isCommon)"+memberChargeList.size());
		return memberChargeList;		
	}
	
	/* Get list of all applicable fees for the given member  */
	public List getChargeList(int societyID,int aptID,InvoiceMemberChargesVO memberChrgVO){
		List memberChargeList=new ArrayList();
		logger.debug("Entry : public List getChargList(int aptID,int societyID)");
		try{
			
					
		memberChargeList=invoiceDomain.getChargeList(aptID, societyID,memberChrgVO);
		
	
		
		
		}catch(Exception e){
			logger.error("Exception at public List getChargList(int aptID,int societyID) "+e);
		}
		
		logger.debug("Exit : public List getChargList(int aptID,int societyID)");
		return memberChargeList;		
	}
	
	/* Get list of all applicable fees for the given member  */
	public List getApplicableChargesToMember(InvoiceDetailsVO invoiceVO,InvoiceBillingCycleVO bcVO,int isCommon){
		List memberChargeList=null;
		logger.debug("Entry : public List getApplicableChargesToMember(InvoiceDetailsVO inoiceVO,InvoiceBillingCycleVO bcVO,int isCommon)");
		
		memberChargeList=invoiceDomain.getApplicableChargesToMember(invoiceVO,bcVO,isCommon);
		
		logger.debug("Exit : public List getApplicableChargesToMember(InvoiceDetailsVO inoiceVO,InvoiceBillingCycleVO bcVO,int isCommon)"+memberChargeList.size());
		return memberChargeList;		
	}
	
	

	/* Get list of all invoices the given society   */
	public List getInvoicesForSociety(int societyID ,int billingID,int status,int invoiceTypeID){
		List invoiceList=null;
		logger.debug("Entry : public List getInvoicesForSociety(String societyID,int billingID ,int status,int invoiceTypeID)");
		
		invoiceList=invoiceDomain.getInvoicesForSociety(societyID, billingID,status,invoiceTypeID);
		
		logger.debug("Exit : public List getInvoicesForSociety(String societyID,int billingID ,int status,int invoiceTypeID)"+invoiceList.size());
		return invoiceList;		
	}
	/* Get list of all inovoices for the period for a member */
	public List getInvoicesForMember(int societyID ,int aptID,String fromDate,String uptoDate){
		List invoiceList=null;
		logger.debug("Entry : public List getActiveMemberList(String societyID)");
		
		invoiceList=invoiceDomain.getAllInvoicesForMember(societyID, aptID,fromDate,uptoDate);
		
		logger.debug("Exit : public List getActiveMemberList(String societyID)"+invoiceList.size());
		return invoiceList;		
	}
	
	
	public List getSingleInvoiceForMember(int societyID ,int aptID,String invoiceID){
		List invoiceList=null;
		logger.debug("Entry : public List getSingleInvoiceForMember(String societyID ,String aptID,String invoiceID)");
		
		invoiceList=invoiceDomain.getSingleInvoiceForMember(societyID, aptID, invoiceID);
		
		logger.debug("Exit : public List getSingleInvoiceForMember(String societyID ,String aptID,String invoiceID)"+invoiceList.size());
		return invoiceList;		
	}
	
	public List getUnpaidInvoicesForMember(int societyID,String ledgerID){
		List newInvoiceList=new ArrayList();
		List invoiceList=null;
		MemberVO memberVO=new MemberVO();
		logger.debug("Entry : public List getUnpaidInvoicesForMember(String societyID,String ledgerID)");
		
		memberVO=memberServiceBean.getMemberInfoThroughLedgerID(societyID, ledgerID);
		
		invoiceList=getInvoicesForApartment(societyID, memberVO.getAptID());
		
		logger.debug("Exit : public List getUnpaidInvoicesForMember(String societyID,String ledgerID)"+invoiceList.size());
		return invoiceList;
	}
	
	
	/* Get list of all inovoices for the period for a member */
	public List getInvoicesForApartment(int societyID ,int aptID){
		List invoiceList=null;
		logger.debug("Entry : public List getInvoicesForApartment(String societyID ,int aptID,String type)");
		
		invoiceList=invoiceDomain.getInvoicesForApartment(societyID, aptID);
		
		logger.debug("Exit : public List getInvoicesForApartment(String societyID ,int aptID,String type)"+invoiceList.size());
		return invoiceList;		
	}
	
	/* Get list of all inovoices for the period for a member */
	public List getUnpaidInvoicesForApartment(int societyID ,String aptID,int invoiceTypeID){
		List invoiceList=null;
		logger.debug("Entry : public List getUnpaidInvoicesForApartment(String societyID ,String aptID,int invoiceTypeID)");
		
		invoiceList=invoiceDomain.getUnpaidInvoicesForApartment(societyID ,aptID,invoiceTypeID);
		
		logger.debug("Exit : public getUnpaidInvoicesForApartment(String societyID ,String aptID,int invoiceTypeID)"+invoiceList.size());
		return invoiceList;		
	}
	
	/* Get list of all line items of the given invoice   */
	public List getLineItemsForInvoice(int societyID ,int invoiceID){
		List invoiceList=null;
		logger.debug("Entry : public List getLineItemsForInvoice(String societyID)");
		
		invoiceList=invoiceDomain.getLineItemsForInvoice(societyID, invoiceID);
		
		logger.debug("Exit : public List getLineItemsForInvoice(String societyID)"+invoiceList.size());
		return invoiceList;		
	}
	

	
	private int updateStatusOfInvoices(String societyID,int billingID){
		int success=0;
		
		success=invoiceDomain.updateStatusOfInvoices(societyID,billingID);
		
		return success;
	}
	
	/* Update the invoice  */
	public int updateInvoice(InvoiceDetailsVO invoiceVO,int societyID){
		int updateFlag=0;
		
		logger.debug("Entry : public int updateInvoice(InvoiceDetailsVO invoiceVO,int societyID)");
		
		try {
			updateFlag=invoiceDomain.updateInvoice(invoiceVO, societyID);
		} catch (Exception e) {
			logger.error("Exception Occurred while updating Invoices "+e);
		}
		
		logger.debug("Exit : public int updateInvoice(InvoiceDetailsVO invoiceVO,int societyID)");
		return updateFlag;		
	}
	
	
	/* Delete the invoice  */
	public int cancelInvoice(InvoiceVO invoiceVO){
		int insertFlag=0;
		
		logger.debug("Entry : public int cancelInvoice(int invoiceID,int societyID)");
		
		insertFlag=invoiceDomain.cancelInvoice(invoiceVO);
		
		logger.debug("Exit : public int cancelInvoice(int invoiceID,int societyID)");
		return insertFlag;		
	}
	
	/* Add new charge to a member  */
	public int insertMemberCharge(InvoiceMemberChargesVO inLIVO,String societyID){
		int insertFlag=0;
		logger.debug("Entry : public int insertMemberCharge(InvoiceLineItemsVO inLIVO,String societyID)");
		
		insertFlag=invoiceDomain.insertMemberCharge(inLIVO,societyID);
		
		logger.debug("Exit : public int insertMemberCharge(InvoiceLineItemsVO inLIVO,String societyID)");
		return insertFlag;		
	}
	/* Delete a charge of member  */
	public int deleteMemberCharge(InvoiceMemberChargesVO chargeVO){
		int insertFlag=0;
		logger.debug("Entry : public int deleteMemberCharge(InoviceMemberChargesVO chargeVO)");
		
		insertFlag=invoiceDomain.deleteMemberCharge(chargeVO);
		
		logger.debug("Exit : public int deleteMemberCharge(InvoiceMemberChargesVO chargeVO)");
		return insertFlag;		
	}
	
	
	/* Update invoice balance with the transaction */
	public TransactionVO addReceptForInvoice(int societyID ,TransactionVO txVO,List invoiceList){
		int success=0;
		logger.debug("Entry : public int insertReceptForInvoice(String societyID ,TransactionVO txVO)");
		
		txVO=receiptInvoiceDomain.addReceptForInvoice(societyID ,txVO,invoiceList);
		
		logger.debug("Exit : public int insertReceptForInvoice(String societyID ,TransactionVO txVO)");
		return txVO;		
	}
	
	/* Update invoice balance with the transaction */
	public TransactionVO addReceptForInvoiceForOnlinePayment(int societyID ,TransactionVO txVO){
		int success=0;
		logger.debug("Entry : public int insertReceptForInvoice(String societyID ,TransactionVO txVO)");
		
		txVO=receiptInvoiceDomain.addReceptForInvoiceForOnlinePayment(societyID, txVO);
		
		logger.debug("Exit : public int insertReceptForInvoice(String societyID ,TransactionVO txVO)");
		return txVO;		
	}
	
	
	/* Add receipts against invoice/bill balance with the transaction */
	public TransactionVO addInvoiceReceptTransaction(int societyID ,TransactionVO txVO){
		int success=0;
		logger.debug("Entry : public int addInvoiceReceptTransaction(String societyID ,TransactionVO txVO)");
		
		txVO=invoiceDomain.addInvoiceReceptTransaction(societyID ,txVO);
		
		logger.debug("Exit : public int addInvoiceReceptTransaction(String societyID ,TransactionVO txVO)");
		return txVO;		
	}
	
	/* Send Selected invoice as mail 
	public int sendSelectedInvoiceMail( List invoiceList,String societyID){
		int success=0;
		logger.debug("Entry : public int sendSelectedInvoiceMail( List invoiceList,String societyID)");
		
		success=invoiceDomain.sendSelectedInvoiceMail(invoiceList,societyID);
		
		logger.debug("Exit : public int sendSelectedInvoiceMail( List invoiceList,String societyID)");
		return success;		
	}*/
	
	/* Get invoice type list */
	public List getInvoiceTypeList(String type){
		List invoiceList=null;
		logger.debug("Entry : public List getInvoiceTypeList(type)");
		
		invoiceList=invoiceDomain.getInvoiceTypeList(type);
		
		logger.debug("Exit : public List getInvoiceTypeList(type)");
		return invoiceList;		
	}
	
	/* Get invoice list for WebService */
	public JSONResponseVO getInvoiceForMember(int societyID,int aptID,String fromDate,String uptoDate){
		JSONResponseVO jsonVO=new JSONResponseVO();
		List invoiceList=new ArrayList();
		logger.debug("Entry : JsonRespAccountsVO getInvoiceForMember(String societyID,String aptID,String fromDate,String uptoDate)");
		
		jsonVO=invoiceDomain.getInvoiceForMember(societyID, aptID, fromDate, uptoDate);
		
		
		
		logger.debug("Exit : JsonRespAccountsVO getInvoiceForMember(String societyID,String aptID,String fromDate,String uptoDate)");
		return jsonVO;		
	}
	
	
	/* Get invoice balance and ledger balance list */
	public List getInvoiceAndLedgerBalanceList(int societyID,String date){
		List invoiceList=null;
		logger.debug("Entry : public List getInvoiceAndLedgerBalanceList(societyID,date)");
		
		invoiceList=invoiceDomain.getInvoiceAndLedgerBalanceList(societyID,date);
		
		logger.debug("Exit : public List getInvoiceAndLedgerBalanceList(societyID,date)");
		return invoiceList;		
	}
	
	public InvoiceDetailsVO getInvoiceBalance(InvoiceDetailsVO invoiceVO,int societyID,int typeID){
		BigDecimal balance=BigDecimal.ZERO;
		InvoiceDetailsVO invVO=new InvoiceDetailsVO();
		logger.debug("Entry : public getInvoiceBalance(InvoiceDetailsVO invoiceVO)");
		
		invVO=invoiceDomain.getInvoiceBalance(invoiceVO,societyID,invoiceVO.getInvoiceTypeID());
		
		
		
		logger.debug("Exit : public getInvoiceBalance(InvoiceDetailsVO invoiceVO)");
		return invVO;		
	}
	
	
	public InvoiceDetailsVO getUnpaidInvoiceBalance(InvoiceDetailsVO invoiceVO,String societyID,int typeID){
		BigDecimal balance=BigDecimal.ZERO;
		InvoiceDetailsVO invVO=new InvoiceDetailsVO();
		logger.debug("Entry : public getUnpaidInvoiceBalance(InvoiceDetailsVO invoiceVO)");
		
		invVO=invoiceDomain.getUnpaidInvoiceBalance(invoiceVO,societyID,invoiceVO.getInvoiceTypeID());
		
		
		
		logger.debug("Exit : public getUnpaidInvoiceBalance(InvoiceDetailsVO invoiceVO)");
		return invVO;		
	}
	
	public List getUnpaidInvoicesDetails(int societyID ,String buildingID,String groupType){
		List invoiceList=null;
		logger.debug("Entry : public List getUnpaidInvoicesDetails(String societyID ,String buildingID)");
		
		invoiceList=invoiceDomain.getUnpaidInvoicesDetails(societyID, buildingID ,groupType);
		
		logger.debug("Exit : public List getUnpaidInvoicesDetails(String societyID ,String buildingID)"+invoiceList.size());
		return invoiceList;		
	}
	
	public InvoiceMemberChargesVO getInvoiceInfoDetails(int societyID ){
		InvoiceMemberChargesVO memberChargesVO=new InvoiceMemberChargesVO();
		logger.debug("Entry : public List getInvoiceInfoDetails(String societyID)");
		
		try {
			
			memberChargesVO=invoiceDomain.getInvoiceInfoDetails(societyID);
			
		} catch (Exception e) {
			logger.error("Exception in getInvoiceInfo Details for society ID "+societyID);
		}
		
		logger.debug("Exit : public List getInvoiceInfoDetails(String societyID )");
		return memberChargesVO;		
	}
	
	public OnlinePaymentGatewayVO getOnlinePaymentDetailsForMember(int societyID,int aptID,int ledgerID){
		OnlinePaymentGatewayVO onlinePaymentVO=new OnlinePaymentGatewayVO();
		logger.debug("Entry : public OnlinePaymentGatewayVO getOnlinePaymentDetailsForMember(String societyID)");
		
		try {
			onlinePaymentVO=invoiceDomain.getOnlinePaymentDetailsForMember(societyID, aptID,ledgerID);
			
		} catch (Exception e) {
			logger.error("Exception in getOnlinePaymentDetailsForMember Details for society ID "+societyID);
		}
		
		logger.debug("Exit : public OnlinePaymentGatewayVO getOnlinePaymentDetailsForMember(String societyID )");
		return onlinePaymentVO;		
	}
	
	public InvoiceDetailsVO calculateProcessingFee(InvoiceDetailsVO invoiceVO){
		
		logger.debug("Entry : public InvoiceDetailsVO calculateProcessingFee(InvoiceDetailsVO invoiceVO)");
		
		try {
			invoiceVO=invoiceDomain.calculateProcessingFee(invoiceVO);
			
		} catch (Exception e) {
			logger.error("Exception in public InvoiceDetailsVO calculateProcessingFee(InvoiceDetailsVO invoiceVO) "+e);
		}
		
		logger.debug("Exit : public InvoiceDetailsVO calculateProcessingFee(InvoiceDetailsVO invoiceVO)");
		return invoiceVO;		
	}
	
	/* Get list of all bills of the given member for particular period  */
	public List getBillsForMember(int societyID,String fromDate, String toDate,int aptID){
		List billList=new ArrayList();
		logger.debug("Entry : public List getBillsForMember(int societyID,String fromDate,String toDate)");
	
		try{
					
		billList=invoiceDomain.getBillsForMember(societyID, fromDate, toDate,aptID);
		
		}catch(Exception e){
			logger.error("Exception in public List getBillsForMember(int societyID,String fromDate,String toDate) "+e);
		}
		logger.debug("Exit : public List getBillsForMember(int societyID,String fromDate,String toDate)"+billList.size());
		return billList;		
	}

	/* Get list of all bills of the given society for that particular billing cycle  */
	public List getBillsForSociety(int societyID,String fromDate, String toDate, int billingCycleID){
		List billList=new ArrayList();
		logger.debug("Entry : public List getBillsForSociety(int societyID,String fromDate,String toDate)");
	
		try{
					
		billList=invoiceDomain.getBillsForSociety(societyID, fromDate, toDate, billingCycleID);
		
		}catch(Exception e){
			logger.error("Exception in public List getBillsForSociety(int societyID,String fromDate,String toDate) "+e);
		}
		logger.debug("Exit : public List getBillsForSociety(int societyID,String fromDate,String toDate)"+billList.size());
		return billList;		
	}
	
	
	/* Get Bill Datails of selected bill cycle  */
	public BillDetailsVO getBillsDetailsSociety(int societyID,String fromDate, String toDate,int aptID){
		BillDetailsVO billVO=new BillDetailsVO();
		logger.debug("Entry : 	public List getBillsDetailsSociety(int societyID,String fromDate, String toDate,int aptID)");
	
		try{
					
		billVO=invoiceDomain.getBillsDetailsSociety(societyID, fromDate, toDate, aptID);
		
		}catch(Exception e){
			logger.error("Exception in 	public List getBillsDetailsSociety(int societyID,String fromDate, String toDate,int aptID) "+e);
		}
		logger.debug("Exit : 	public List getBillsDetailsSociety(int societyID,String fromDate, String toDate,int aptID)"+billVO.getObjectList().size());
		return billVO;		
	}
	
	/* Get invoice based Bill Datails of selected bill cycle  */
	public BillDetailsVO getInvoiceBasedBillDetails(int orgiD,String fromDate, String toDate,int ledgerID){
		BillDetailsVO billVO=new BillDetailsVO();
		logger.debug("Entry : 	public List getInvoiceBasedBillDetails(int societyID,String fromDate, String toDate,int ledgerID)");
	
		try{
					
		billVO=invoiceDomain.getInvoiceBasedBillDetails(orgiD, fromDate, toDate, ledgerID);
		
		}catch(Exception e){
			logger.error("Exception in 	public List getInvoiceBasedBillDetails(int societyID,String fromDate, String toDate,int ledgerID) "+e);
		}
		logger.debug("Exit : 	public List getInvoiceBasedBillDetails(int societyID,String fromDate, String toDate,int ledgerID)"+billVO.getObjectList().size());
		return billVO;		
	}
	
	/* Get Bill Datails of selected bill cycle fo a perticular apartment  */
	public BillDetailsVO getBillDetails(int societyID,String fromDate, String toDate,int aptID){
		BillDetailsVO billVO=new BillDetailsVO();
		logger.debug("Entry : 	public BillDetailsVO getBillDetails(int societyID,String fromDate, String toDate,int aptID)");
	
		try{
					
		billVO=invoiceDomain.getBillDetails(societyID, fromDate, toDate, aptID);
		
		}catch(Exception e){
			logger.error("Exception in 	public List getBillDetails(int societyID,String fromDate, String toDate,int ledgerID) "+e);
		}
		logger.debug("Exit : 	public BillDetailsVO getBillDetails(int societyID,String fromDate, String toDate,int aptID)"+billVO.getObjectList().size());
		return billVO;		
	}
	
	/* Get list of all bills invoice based of the given ledger for particular period  */
	public List getBillsForLedger(int orgID,String fromDate, String toDate,int ledgerID){
		List billList=new ArrayList();
		logger.debug("Entry : public List getBillsForMember(int societyID,String fromDate,String toDate)");
	
		try{
					
		billList=invoiceDomain.getBillsForLedger(orgID, fromDate, toDate,ledgerID);
		
		}catch(Exception e){
			logger.error("Exception in public List getBillsForLedger(int orgID,String fromDate,String toDate) "+e);
		}
		logger.debug("Exit : public List getBillsForLedger(int orgID,String fromDate,String toDate)"+billList.size());
		return billList;		
	}

	/* Get list of all bills of the given org for that particular billing cycle  */
	public List getBillsForOrg(int orgID,String fromDate, String toDate, int billingCycleID){
		List billList=new ArrayList();
		logger.debug("Entry : public List getBillsForSociety(int orgID,String fromDate,String toDate)");
	
		try{
					
		billList=invoiceDomain.getBillsForOrg(orgID, fromDate, toDate, billingCycleID);
		
		}catch(Exception e){
			logger.error("Exception in public List getBillsForOrg(int orgID,String fromDate,String toDate) "+e);
		}
		logger.debug("Exit : public List getBillsForOrg(int orgID,String fromDate,String toDate)"+billList.size());
		return billList;		
	}
	
	/* Get list of all gst invoices list for a ledger in given period */
	public List getGSTInvoicesListForBills(int orgID,String fromDate,String toDate,int ledgerID){
		List invoiceList=null;
		logger.debug("Entry : public List getGSTInvoicesForOrg(int orgID,String fromDate,String toDate,int ledgerID)");
		try{
			
		invoiceList=invoiceDomain.getGSTInvoicesListForBills(orgID, fromDate, toDate, ledgerID);
		
		}catch(Exception e){
			  logger.error("Exception in getGSTInvoicesForOrg(int orgID,String fromDate,String toDate,int ledgerID) "+e);
		}					
		logger.debug("Exit : public List getGSTInvoicesForOrg(int orgID,String fromDate,String toDate,int ledgerID)");
		return invoiceList;		
	}
	
	
	/* Get list of all invoices of the given society for that particular period  */
	public List getInvoicesForSociety(final InvoiceDetailsVO invoiceVO){
		List invoiceList=null;
	
		logger.debug("Entry : public List getInvoicesForSociety(final InvoiceDetailsVO invoiceVO)");
	
		try{
				
		invoiceList=invoiceDomain.getInvoicesForSociety(invoiceVO);
		
		}catch(Exception e){
			logger.error("Exception : public List getInvoicesForSociety(final InvoiceDetailsVO invoiceVO) "+e);
		}
		logger.debug("Exit : public List getInvoicesForSociety(final InvoiceDetailsVO invoiceVO)"+invoiceList.size());
		return invoiceList;		
	}
	
	/* Get list of all one time invoices of the given society   */
	public List getOnetimeInvoicesForSociety(final InvoiceDetailsVO invoiceVO){
		List invoiceList=null;
	
		logger.debug("Entry : public List getOnetimeInvoicesForSociety(final InvoiceDetailsVO invoiceVO)");
	
		try{
				
		invoiceList=invoiceDomain.getOnetimeInvoicesForSociety(invoiceVO);
		
		}catch(Exception e){
			logger.error("Exception : public List getOnetimeInvoicesForSociety(final InvoiceDetailsVO invoiceVO) "+e);
		}
		logger.debug("Exit : public List getOnetimeInvoicesForSociety(final InvoiceDetailsVO invoiceVO)"+invoiceList.size());
		return invoiceList;		
	}
	
	
	/* Get invoices bill for a  for that particular billing cycle  */
	public List getInvoicesForLedger(final InvoiceDetailsVO invoiceVO){
		List invoiceList=null;
		
		logger.debug("Entry : public List getInvoicesForLedger(final InvoiceDetailsVO invoiceVO)");
	
		try{
			
					
		invoiceList=invoiceDomain.getInvoicesForLedger(invoiceVO);
		
		}catch(Exception e){
			logger.error("Exception in public List getInvoicesForLedger(final InvoiceDetailsVO invoiceVO) "+e);
		}
		logger.debug("Exit : public List getBillsForSociety(int societyID,String fromDate,String toDate)"+invoiceList.size());
		return invoiceList;		
	}
	
	
	/* Get GST invoice  */
	public InvoiceVO insertGSTInvoice(InvoiceVO invoiceVO){
	
		logger.debug("Entry : public int insertGSTInvoice(InvoiceVO invoiceVO)");
		
		invoiceVO=invoiceDomain.insertGSTInvoice(invoiceVO);
		
		logger.debug("Exit : public int insertGSTInvoice(InvoiceVO invoiceVO)");
		return invoiceVO;		
	}
	
	
	/* Get invoices for customer my accountant  */
	public List getInvoicesForCustomerLedger(InvoiceVO invoiceVO){
		List invoiceList=null;
		
		logger.debug("Entry : public List getInvoicesForCustomerLedger(InvoiceVO invoiceVO)");	
		try{			
					
		invoiceList=invoiceDomain.getInvoicesForCustomerLedger(invoiceVO);
		
		}catch(Exception e){
			logger.error("Exception in getInvoicesForCustomerLedger "+e);
		}
		logger.debug("Exit : public List getInvoicesForCustomerLedger(InvoiceVO invoiceVO) "+invoiceList.size());
		return invoiceList;		
	}
	
	/* Get invoice details  */
	public InvoiceVO getInvoiceDetails(InvoiceVO invoiceVO){
	
		logger.debug("Entry : public int getInvoiceDetails(InvoiceVO invoiceVO)");
		try{
			
		  invoiceVO=invoiceDomain.getInvoiceDetails(invoiceVO);
		
		}catch(Exception e){
			logger.error("Exception in getInvoiceDetails "+e);
		}
		logger.debug("Exit : public int getInvoiceDetails(InvoiceVO invoiceVO)");
		return invoiceVO;		
	}
	
	/* Get invoice details by invoice number  */
	public InvoiceVO getInvoiceDetailsByInvoiceNumber(InvoiceVO invoiceVO){
	
		logger.debug("Entry : public int getInvoiceDetailsByInvoiceNumber(InvoiceVO invoiceVO)");
		try{
			
		  invoiceVO=invoiceDomain.getInvoiceDetailsByInvoiceNumber(invoiceVO);
		
		}catch(Exception e){
			logger.error("Exception in getInvoiceDetailsByInvoiceNumber "+e);
		}
		logger.debug("Exit : public int getInvoiceDetailsByInvoiceNumber(InvoiceVO invoiceVO)");
		return invoiceVO;		
	}
	
	public InvoiceVO updateGSTInvoice(InvoiceVO invoiceVO){		
		logger.debug("Entry : public InvoiceVO updateGSTInvoice(InvoiceVO invoiceVO)");
		try{
			
			invoiceVO=invoiceDomain.updateGSTInvoice(invoiceVO);
			  
		}catch(Exception e){
			logger.error("Exception in updateGSTInvoice "+e);
		}
		
		logger.debug("Exit : public InvoiceVO updateGSTInvoice(InvoiceVO invoiceVO)");
		return invoiceVO;		
	}
		
	
	public InvoiceVO deleteGSTInvoice(InvoiceVO invoiceVO){
		int updateFlag=0;
		logger.debug("Entry : public InvoiceVO deleteGSTInvoice(InvoiceVO invoiceVO)");
		try{
			
			invoiceVO=invoiceDomain.deleteGSTInvoice(invoiceVO);
			  
		}catch(Exception e){
			logger.error("Exception in deleteGSTInvoice "+e);
		}
		
		logger.debug("Exit : public InvoiceVO deleteGSTInvoice(InvoiceVO invoiceVO)");
		return invoiceVO;		
	}
	
	public InvoiceVO updateReportGroupInvoiceLineItem(InvoiceVO invoiceVO){
		int updateFlag=0;
		logger.debug("Entry : public InvoiceVO updateReportGroupInvoiceLineItem(InvoiceVO invoiceVO)");
		try{
			
			invoiceVO=invoiceDomain.updateReportGroupInvoiceLineItem(invoiceVO);
			  
		}catch(Exception e){
			logger.error("Exception in updateReportGroupInvoiceLineItem "+e);
		}
		
		logger.debug("Exit : public InvoiceVO updateReportGroupInvoiceLineItem(InvoiceVO invoiceVO)");
		return invoiceVO;		
	}
	
	/* Used for changing statuses of invoices i.e. adding is_deleted flag */
	public int changeInvoiceStatus(InvoiceVO invoiceVO) {
		int success=0;
		logger.debug("Entry : public int changeInvoiceStatus(InvoiceVO invoiceVO) ");
		try {
			logger.debug("Here invoice ID is "+invoiceVO.getInvoiceID()+" "+invoiceVO.getOrgID());
			
			
			success=invoiceDomain.changeInvoiceStatus(invoiceVO);
			
			
		} catch (Exception e) {
			logger.error("Exception occured in  public int changeInvoiceStatus(InvoiceVO invoiceVO) "+e);
		}
		
		logger.debug("Exit :  public int changeInvoiceStatus(InvoiceVO invoiceVO)");
		return success;
	}
	
	//====== Fetch TDS calculatiin Report =====//
	public List getTDSCalculationReport(TransactionVO txVO){
		List tdsTxList=null;
		
		logger.debug("Entry : public List getTDSCalculationReport(TransactionVO txVO)");
	
		try{
			
		tdsTxList=invoiceDomain.getTDSCalculationReport(txVO);
		
		}catch(Exception e){
			logger.error("Exception in public List getTDSCalculationReport(TransactionVO txVO) "+e);
		}
		logger.debug("Exit : public List getTDSCalculationReport(TransactionVO txVO)"+tdsTxList.size());
		return tdsTxList;		
	}
	
	//====== Fetch TDS calculation Report on invoice based =====//
			public List getTDSCalculationRptOnInvoices(InvoiceVO invVO){
				List tdsTxList=null;
				
				logger.debug("Entry : public List getTDSCalculationRptOnInvoices(TransactionVO txVO)");
			
				try{
					
				tdsTxList=invoiceDomain.getTDSCalculationRptOnInvoices(invVO);
				
				}catch(Exception e){
					logger.error("Exception in public List getTDSCalculationRptOnInvoices(TransactionVO txVO) "+e);
				}
				logger.debug("Exit : public List getTDSCalculationRptOnInvoices(TransactionVO txVO)"+tdsTxList.size());
				return tdsTxList;		
			}
	
	
	/* Get list of all gst invoices list for org  */
	public List getGSTInvoicesForOrg(InvoiceVO invoiceVO){
		List invoiceList=null;
		logger.debug("Entry : public List getGSTInvoicesForOrg(InvoiceVO invoiceVO)");
		try{
			
		invoiceList=invoiceDomain.getGSTInvoicesForOrg(invoiceVO);
		
		}catch(Exception e){
			  logger.error("Exception in getGSTInvoicesForOrg "+e);
		}					
		logger.debug("Exit : public List getGSTInvoicesForOrg(InvoiceVO invoiceVO)");
		return invoiceList;		
	}
	
	/* Get list of all gst invoices list for org  */
	public List getQuickGSTInvoicesForOrg(InvoiceVO invoiceVO){
		List invoiceList=null;
		logger.debug("Entry : public List getQuickGSTInvoicesForOrg(InvoiceVO invoiceVO)");
		try{
			
		invoiceList=invoiceDomain.getQuickGSTInvoicesForOrg(invoiceVO);
		
		}catch(Exception e){
			  logger.error("Exception in getQuickGSTInvoicesForOrg "+e);
		}					
		logger.debug("Exit : public List getQuickGSTInvoicesForOrg(InvoiceVO invoiceVO)");
		return invoiceList;		
	}
	
	/* Get gst invoice list like day book   */
	public List getGSTInvoicesListDayBook(InvoiceVO invoiceVO){
		List invoiceList=null;
		logger.debug("Entry : public List getGSTInvoicesListDayBook(InvoiceVO invoiceVO)");
		try{
			
		invoiceList=invoiceDomain.getGSTInvoicesListDayBook(invoiceVO);
		
	    }catch(Exception e){
		 logger.error("Exception in getGSTInvoicesListDayBook "+e);
        }				
		
		logger.debug("Exit : public List getGSTInvoicesListDayBook(InvoiceVO invoiceVO)");
		return invoiceList;		
	}
	
	public List getGSTChapterList(){
		List chapterList=null;
		logger.debug("Entry : public List getGSTChapterList()");
		try{
			
		chapterList=invoiceDomain.getGSTChapterList();
		
		}catch(Exception e){
			  logger.error("Exception in getGSTChapterList "+e);
		}	
						
		logger.debug("Exit : public List getGSTChapterList()");
		return chapterList;		
	}
	
	 public List getHsnSacList(int chapterID){
		List hsnSacList=null;
		logger.debug("Entry :  public List getHsnSacList(int chapterID)");
		try{
			
			hsnSacList=invoiceDomain.getHsnSacList(chapterID);
		
		}catch(Exception e){
			  logger.error("Exception in getHsnSacList "+e);
		}		
						
		logger.debug("Exit :  public List getHsnSacList(int chapterID)");
		return hsnSacList;		
	}
	 
	 public List getProductServiceLedgerRelationList(int orgID){
			List hsnSacList=null;
			logger.debug("Entry : public List getProductServiceLedgerRelationList(int orgID)");
			try{
				
				hsnSacList=invoiceDomain.getProductServiceLedgerRelationList(orgID);
				
			}catch(Exception e){
				  logger.error("Exception in getProductServiceLedgerRelationList "+e);
			}				
			logger.debug("Exit : public List getProductServiceLedgerRelationList(int orgID)");
			return hsnSacList;		
		}
	 
	 public ItemDetailsVO getProductServiceLedgerDetails(int itemID,int orgID){
			logger.debug("Entry : public List getProductServiceLedgerDetails(int itemID)");
			
			ItemDetailsVO hsnSacVO=invoiceDomain.getProductServiceLedgerDetails(itemID,orgID);
							
			logger.debug("Exit : public List getProductServiceLedgerDetails(int itemID)");
			return hsnSacVO;		
		}
	 
	   /* insert product service  */
		public int insertProductService(ItemDetailsVO itemDetailsVO){
			 int success=0;
			logger.debug("Entry : public int insertProductService(ItemDetailsVO itemDetailsVO)");
			try{
				
				success=invoiceDomain.insertProductService(itemDetailsVO);
				
			}catch(Exception e){
				  logger.error("Exception in insertProductService "+e);
			}
			logger.debug("Exit : public int insertProductService(ItemDetailsVO itemDetailsVO)");
			return success;		
		}
		
		
		public int updateProductService(ItemDetailsVO itemDetailsVO){
			 int success=0;
			logger.debug("Entry : public int updateProductService(ItemDetailsVO itemDetailsVO)");
			try{
				
			success=invoiceDomain.updateProductService(itemDetailsVO);
			
		    }catch(Exception e){
			  logger.error("Exception in updateProductService "+e);
		    }
			
			logger.debug("Exit : public int updateProductService(ItemDetailsVO itemDetailsVO)");
			return success;		
		}
		
		public int deleteProductService(ItemDetailsVO itemDetailsVO){
			int deleteFlag=0;
			logger.debug("Entry : public int deleteProductService(ItemDetailsVO itemDetailsVO)");
			try{
				
				deleteFlag=invoiceDomain.deleteProductService(itemDetailsVO);
								  
			}catch(Exception e){
				logger.error("Exception in deleteProductService "+e);
			}
			
			logger.debug("Exit : public int deleteProductService(ItemDetailsVO itemDetailsVO)");
			return deleteFlag;		
		}
		//====== Generate Scheduled Invoices ================//
			public List generateScheduledInvoices(int societyID,int billingCycleID){
				List invoiceList=new ArrayList();
				
				try {
					logger.debug("Entry : public List generateScheduledInvoices(int societyID,int billingCycleID)");
					
					invoiceList=invoiceDomain.generateScheduledInvoices(societyID, billingCycleID);
					
					
					logger.debug("Exit : public List generateScheduledInvoices(int societyID,int billingCycleID)");
				} catch (Exception e) {
					logger.error("Exception :public List generateScheduledInvoices(int societyID,int billingCycleID)"+e );
				}
				return invoiceList;
				
			}
			
			/*-- Add Bulk Journal invoices---*/
			public ScheduledTransactionResponseVO addBulkInvoices(List invoiceList,int societyID){
				logger.debug("Entry :  public ScheduledTransactionResponseVO addBulkInvoices(List invoiceList,int societyID)");
				ScheduledTransactionResponseVO responseVO=new ScheduledTransactionResponseVO();
				
				responseVO=invoiceDomain.addBulkInvoices(invoiceList, societyID);
				
				logger.debug("Exit :  public ScheduledTransactionResponseVO addBulkInvoices(List invoiceList,int societyID)");
				return responseVO;
			}
			
			public List generateScheduledLateFeeInvoices(int societyID,int billingCycleID){
				List invoiceList=new ArrayList();
				
				try {
					logger.debug("Entry :  public List generateScheduledLateFeeInvoices(int societyID,int billingCycleID)");
					
					invoiceList=invoiceDomain.generateScheduledLateFeeInvoices(societyID, billingCycleID);
					
					
					logger.debug("Exit :  public List generateScheduledLateFeeInvoice(int societyID,int billingCycleID)");
				} catch (Exception e) {
					logger.error("Exception : public List generateScheduledLateFeeInvoices(int societyID,int billingCycleID)"+e );
				}
				return invoiceList;
				
			}
			
			public InvoiceVO getInvoiceBalanceInGivenPeriod(int ledgerID,int orgID,String fromDate,String toDate){
				logger.debug("public List getInvoiceListInGivenPeriod(int ledgerID,int orgID,String fromDate,String toDate)");
			InvoiceVO balanceInvoiceVO=new InvoiceVO();
				
					
					
					balanceInvoiceVO = invoiceDomain.getInvoiceBalanceInGivenPeriod(ledgerID, orgID, fromDate, toDate);
			
				logger.debug("Exit : public List getInvoiceListInGivenPeriod(int ledgerID,int orgID,String fromDate,String toDate)");
				
				return balanceInvoiceVO;
			}
			
			
			
			public ScheduledTransactionResponseVO generateNormalInvoices(int societyID,int billingCycleID){
				logger.debug("Entry : public void generateNormalInvoices(int societyID,EventVO societyVO)");
				
				ScheduledTransactionResponseVO respVO=new ScheduledTransactionResponseVO();
				List responseList=new ArrayList();
				List invoiceList=new ArrayList();
		
				
				invoiceList=generateScheduledInvoices(societyID, billingCycleID);
				
				if(invoiceList.size()>0){
					respVO=addBulkInvoices(invoiceList,societyID);
				
					if(respVO.getSuccessCount()>respVO.getFailureCount()){
						for(String element:respVO.getChargeList()){
							int chargeID=Integer.parseInt(element);
						ChargesVO chargeVO=transactionService.getChargeDetails(chargeID,societyID);
						int success=transactionService.updateChargeStatus(societyID, chargeVO);
						//int updateReminderDate=invoiceService.updateReminderDate(billingVO.getSocietyId());
						}
					}
				}
			logger.debug("++++++++++Regular Invoices++++++++++///////// "+respVO.getSocietyID()+" No of Inovices Created "+respVO.getSuccessCount()+" No of invoices Failuers "+respVO.getFailureCount());
				logger.debug("Entry : public void generateNormalInvoices(int societyID,EventVO societyVO)");
				return respVO;
			}

			public ScheduledTransactionResponseVO generateLateFeeInvoices(int societyID,int billingCycleID){
				logger.debug("Entry : public void generateLateFeeInvoices(InvoiceBillingCycleVO billingVO,EventVO societyVO)");
				
				ScheduledTransactionResponseVO respVO=new ScheduledTransactionResponseVO();
				List responseList=new ArrayList();
				List invoiceList=new ArrayList();
			
				invoiceList=generateScheduledLateFeeInvoices(societyID, billingCycleID);
				
				if(invoiceList.size()>0){
					respVO=addBulkInvoices(invoiceList,societyID);
				
					if(respVO.getSuccessCount()>respVO.getFailureCount()){
						for(String element:respVO.getChargeList()){
							int chargeID=Integer.parseInt(element);
							ChargesVO chargeVO=transactionService.getPenaltyChargeDetails(chargeID, societyID);
							int success=transactionService.updateLateFeeChargeDate(societyID, chargeVO);
								success=transactionService.updatePenaltyChargeStatus(societyID, chargeVO);
						//int updateReminderDate=invoiceService.updateReminderDate(billingVO.getSocietyId());
						}
					}
				}
			
				logger.debug("++++++++Late Fees++++++++++++///////// "+respVO.getSocietyID()+" No of Inovices Created "+respVO.getSuccessCount()+" No of invoices Failuers "+respVO.getFailureCount());
				logger.debug("Entry : public void generateLateFeeInvoices(int societyID,EventVO societyVO)");
				return respVO;
			}
			
			
	
			public BillDetailsVO getDemandNoticeBalanceDetails(int societyID,String fromDate, String toDate,int aptID){
				BillDetailsVO billVO=new BillDetailsVO();
			
				logger.debug("Entry : public BillDetailsVO getDemandNoticeBalanceDetails(int societyID,String fromDate, String toDate,int aptID)");
			
				try{
						
					billVO=invoiceDomain.getDemandNoticeBalanceDetails(societyID, fromDate, toDate, aptID);
				
				}catch(Exception e){
					logger.error("Exception : getDemandNoticeBalanceDetails "+e);
				}
				logger.debug("Exit :public BillDetailsVO getDemandNoticeBalanceDetails(int societyID,String fromDate, String toDate,int aptID)");
				return billVO;		
			}
			
			public InvoiceVO convertChargesIntoLineItemsObj(InvoiceVO invVO,List chargeList){
				
				List lineItems=new ArrayList();
				
				logger.debug("Entry :  private InvoiceVO convertChargesIntoLineItemsObj(InvoiceVO invVO,List chargeList) ");
				
				invVO=invoiceDomain.convertChargesIntoLineItemsObj(invVO, chargeList);
				 					
				logger.debug("Exit :  private InvoiceVO convertChargesIntoLineItemsObj(InvoiceVO invVO,List chargeList)");
				return invVO;
				
			}
			
			/* Get Currency list  */
			public List getCurrencyList(){
				List currencyList=null;
				logger.debug("Entry : public List getCurrencyList()");
				try{
				
					currencyList=invoiceDomain.getCurrencyList();
				
				}catch (Exception e) {
					logger.error("Exception in getCurrencyList "+e);
				}
				
				logger.debug("Exit : public List getCurrencyList()");
				return currencyList;		
			}
			
			
			//=============Invoice linking and unlinking with invoices ===========//
			public List getUnlinkedInvoiceList(int orgID,int ledgerID,String fromDate, String toDate){
				List invoiceReceiptList=null;
			
					logger.debug("Entry :  public List getUnlinkedInvoiceList(int orgID,int ledgerID)");
					
					invoiceReceiptList=invoiceDomain.getUnlinkedInvoiceList(orgID, ledgerID, fromDate, toDate);
					
					logger.debug("Exit :  public List getUnlinkedInvoiceList(int orgID,int ledgerID)");
				return invoiceReceiptList;
				}
			
			
			public int linkInvoiceList(int orgID,List invoiceList){
				int successCount=0;
			
					logger.debug("Entry :  public int LinkInvoiceList(int orgID,List invoiceList)");
					
					successCount=invoiceDomain.linkInvoiceList(orgID, invoiceList);
					
				
					logger.debug("Exit : public int LinkInvoiceList(int orgID,List receiptList)");
				return successCount;
				}
			
			public int unlinkInvoiceFromInvoice(int orgID ,InvoiceVO invoiceVO){
				int success=0;
				logger.debug("Entry : public int unlinkInvoiceFromInvoice(int orgID ,InvoiceDetailsVO invoiceVO)");
				  
					   success=invoiceDomain.unlinkInvoiceFromInvoice(orgID, invoiceVO);			
							
				logger.debug("Exit : public int unlinkInvoiceFromInvoice(int orgID ,InvoiceDetailsVO invoiceVO)");
				return success;		
			}
			
			
			
			//=============== Age wise receivables and payable reports ================//
			
			
			public List getAgewiseReceivalesList(int orgID, int isDueDate){
				List receivableList=null;
			
					logger.debug("Entry :  public List getAgewiseReceivalesList(int orgID, int isDueDate)");
			
					receivableList=invoiceDomain.getAgewiseReceivalesList(orgID, isDueDate);
					
					logger.debug("Exit : public List getAgewiseReceivalesList(int orgID, int isDueDate)");
					return receivableList;
			}
			
			
			public List getAgewisePayablesList(int orgID, int isDueDate){
				List payableList=null;
			
					logger.debug("Entry :  public List getAgewisePayablesList(int orgID, int isDueDate)");
		
					payableList=invoiceDomain.getAgewisePayablesList(orgID, isDueDate);
					
					logger.debug("Exit : public List getAgewisePayablesList(int orgID, int isDueDate)");
					return payableList;
			}
			
			
			public List getSalesByCustomer(int orgID, String fromDate, String toDate, int productID){
				List invoiceList=null;
			
					logger.debug("Entry :  public List getSalesByCustomer(int orgID, String fromDate, String toDate, int productID)");
		
					invoiceList=invoiceDomain.getSalesByCustomer(orgID, fromDate,toDate,productID);
					
					logger.debug("Exit : public List getSalesByCustomer(int orgID, String fromDate, String toDate, int productID)");
					return invoiceList;
			}
			
			
			public List getTotalSalesByCustomer(int orgID, String fromDate, String toDate){
				List invoiceList=null;
			
					logger.debug("Entry :  public List getTotalSalesByCustomer(int orgID, String fromDate, String toDate)");
		
					invoiceList=invoiceDomain.getTotalSalesByCustomer(orgID, fromDate,toDate);
					
					logger.debug("Exit : public List getSalesByCustomer(int orgID, String fromDate, String toDate)");
					return invoiceList;
			}
			
			public List getTotalSalesByCustomerDetails(int orgID, String fromDate, String toDate,int ledgerID){
				List invoiceList=null;
			
					logger.debug("Entry :  public List getTotalSalesByCustomerDetails(int orgID, String fromDate, String toDate)");
		
					invoiceList=invoiceDomain.getTotalSalesByCustomerDetails(orgID, fromDate,toDate,ledgerID);
					
					logger.debug("Exit : public List getTotalSalesByCustomerDetails(int orgID, String fromDate, String toDate)");
					return invoiceList;
			}
			
			public MonthwiseChartVO getMonthWiseSalesReportForChart(int orgID, String fromDate, String toDate){
				MonthwiseChartVO reportVO=new MonthwiseChartVO();
			
					logger.debug("Entry :  public ReportVO getMonthWiseSalesReportForChart(int orgID, String fromDate, String toDate)");
		
					reportVO=invoiceDomain.getMonthWiseSalesReportForChart(orgID, fromDate,toDate);
					
					
					
					logger.debug("Exit : public ReportVO getMonthWiseSalesReportForChart(int orgID, String fromDate, String toDate)");
					return reportVO;
			}
			
			public List getMonthWiseSalesReport(int orgID, String fromDate, String toDate){
				List invoiceList=null;
				
			
					logger.debug("Entry :  public ReportVO getMonthWiseSalesReport(int orgID, String fromDate, String toDate)");
		
					invoiceList=invoiceDomain.getMonthWiseSalesReport(orgID, fromDate,toDate);
					
					
					logger.debug("Exit : public ReportVO getMonthWiseSalesReport(int orgID, String fromDate, String toDate)");
					return invoiceList;
			}
			
			public List getMonthWiseSalesReportDetails(int orgID, String fromDate, String toDate,int productID){
				List invoiceList=null;
				
			
					logger.debug("Entry :  public ReportVO getMonthWiseSalesReportDetails(int orgID, String fromDate, String toDate)");
		
					invoiceList=invoiceDomain.getMonthWiseSalesReportDetails(orgID, fromDate,toDate,productID);
					
					
					logger.debug("Exit : public ReportVO getMonthWiseSalesReportDetails(int orgID, String fromDate, String toDate)");
					return invoiceList;
			}
			
			public List getTop20SalesProduct(int orgID, String fromDate, String toDate){
				List invoiceList=null;
			
					logger.debug("Entry :  public List getTop20SalesProduct(int orgID, String fromDate, String toDate)");
		
					invoiceList=invoiceDomain.getTop20SalesProduct(orgID, fromDate,toDate);
					
					logger.debug("Exit : public List getTop20SalesProduct(int orgID, String fromDate, String toDate)");
					return invoiceList;
			}
			
			
			public List getSalesReportProductWise(int orgID, String fromDate, String toDate, int productID){
				List invoiceList=null;
			
					logger.debug("Entry :  public List getSalesReportProductWise(int orgID, String fromDate, String toDate, int productID)");
		
					invoiceList=invoiceDomain.getSalesReportProductWise(orgID, fromDate,toDate,productID);
					
					logger.debug("Exit : public List getSalesReportProductWise(int orgID, String fromDate, String toDate, int productID)");
					return invoiceList;
			}
			
			public List getSalesReportProductWiseDetails(int orgID, String fromDate, String toDate, int productID,int ledgerID){
				List invoiceList=null;
			
					logger.debug("Entry :  public List getSalesReportProductWiseDetails(int orgID, String fromDate, String toDate, int productID)");
		
					invoiceList=invoiceDomain.getSalesReportProductWiseDetails(orgID, fromDate,toDate,productID,ledgerID);
					
					logger.debug("Exit : public List getSalesReportProductWiseDetails(int orgID, String fromDate, String toDate, int productID)");
					return invoiceList;
			}
			
			public List getRateCardList(int orgID){
				List rateCardList=null;
			
					logger.debug("Entry :   public List getRateCardList(int orgID)");
		
					rateCardList=invoiceDomain.getRateCardList(orgID);
					
					logger.debug("Exit :  public List getRateCardList(int orgID)");
					return rateCardList;
			}
			
			 /* insert product rate card  */
				public int insertProductRateCard(ItemDetailsVO itemDetailsVO){
				 int success=0;
			
				 logger.debug("Entry : public int insertProductRateCard(ItemDetailsVO itemDetailsVO)");
				 try{		
					
					 success=invoiceDomain.insertProductRateCard(itemDetailsVO);
				
				   		  
				 }catch(Exception e){
						logger.error("Exception in insertProductRateCard "+e);
				 }
						
				 logger.debug("Exit : public int insertProductRateCard(ItemDetailsVO itemDetailsVO)");
					return success;		
				}
				
				public int updateProductRateCard(ItemDetailsVO itemDetailsVO){
					 int success=0;
					 int hsnSacID=0;
					 logger.debug("Entry : public int updateProductRateCard(ItemDetailsVO itemDetailsVO)");
					 try{	
						 				
						 success=invoiceDomain.updateProductRateCard(itemDetailsVO);
					
					 }catch(Exception e){
						logger.error("Exception in updateProductRateCard "+e);
					 }
							
					 logger.debug("Exit : public int updateProductRateCard(ItemDetailsVO itemDetailsVO)");
						return success;		
				}
				
				public int deleteProductRateCard(ItemDetailsVO itemDetailsVO){
					int deleteFlag=0;
					Boolean isPresent=false;
					logger.debug("Entry : public int deleteProductRateCard(ItemDetailsVO itemDetailsVO)");
					try{
					
						deleteFlag=invoiceDomain.deleteProductRateCard(itemDetailsVO);
					
										  
					}catch(Exception e){
						logger.error("Exception in deleteProductRateCard "+e);
					}
					
					logger.debug("Exit : public int deleteProductRateCard(ItemDetailsVO itemDetailsVO)");
					return deleteFlag;		
				}
				
				 /* insert product rate card  item*/
					public int insertProductRateCardItem(ItemDetailsVO itemDetailsVO){
					 int success=0;
				
					 logger.debug("Entry : public int insertProductRateCardItem(ItemDetailsVO itemDetailsVO)");
					 try{		
						
						 success=invoiceDomain.insertProductRateCardItem(itemDetailsVO);
					
					   		  
					 }catch(Exception e){
							logger.error("Exception in insertProductRateCardItem "+e);
					 }
							
					 logger.debug("Exit : public int insertProductRateCardItem(ItemDetailsVO itemDetailsVO)");
						return success;		
					}
					
					public int updateProductRateCardItem(ItemDetailsVO itemDetailsVO){
						 int success=0;
						 int hsnSacID=0;
						 logger.debug("Entry : public int updateProductRateCardItem(ItemDetailsVO itemDetailsVO)");
						 try{	
							 				
							 success=invoiceDomain.updateProductRateCardItem(itemDetailsVO);
						
						 }catch(Exception e){
							logger.error("Exception in updateProductRateCardItem "+e);
						 }
								
						 logger.debug("Exit : public int updateProductRateCardItem(ItemDetailsVO itemDetailsVO)");
							return success;		
					}
					
					public int deleteProductRateCardItem(ItemDetailsVO itemDetailsVO){
						int deleteFlag=0;
						Boolean isPresent=false;
						logger.debug("Entry : public int deleteProductRateCardItem(ItemDetailsVO itemDetailsVO)");
						try{
						
							deleteFlag=invoiceDomain.deleteProductRateCardItem(itemDetailsVO);
						
											  
						}catch(Exception e){
							logger.error("Exception in deleteProductRateCardItem "+e);
						}
						
						logger.debug("Exit : public int deleteProductRateCardItem(ItemDetailsVO itemDetailsVO)");
						return deleteFlag;		
					}
					
					
			 public ItemDetailsVO getProductRateCardDetails(int productID,int rateCardID){
					logger.debug("Entry : public List getProductServiceLedgerDetails(int itemID)");
					
					ItemDetailsVO rateCardVO=invoiceDomain.getProductRateCardDetails(productID, rateCardID);
									
					logger.debug("Exit :  public ItemDetailsVO getProductRateCardDetails(int productID,int rateCardID)");
					return rateCardVO;		
				}
			 
			 public List getProductRateCardItemList(int orgID,int rateCardID){
					List rateCardList=null;
				
						logger.debug("Entry :  public List getProudctRateCardItemList(int orgID,int rateCardID)");
			
						rateCardList=invoiceDomain.getProductRateCardItemList(orgID,rateCardID);
						
						logger.debug("Exit :  public List getProductRateCardItemList(int orgID,int rateCardID)");
						return rateCardList;
				}
			 
		
			 /* Get list of all bills of the given member app */
				public List getBillsForMemberApp(int societyID,int aptID){
					List billList=new ArrayList();
					logger.debug("Entry : public List getBillsForMemberApp(int societyID,String fromDate,String toDate)");
				
					try{
								
					billList=invoiceDomain.getBillsForMemberApp(societyID, aptID);
					
					}catch(Exception e){
						logger.error("Exception in public List getBillsForMember(int societyID,String fromDate,String toDate) "+e);
					}
					logger.debug("Exit : public List getBillsForMember(int societyID,String fromDate,String toDate)"+billList.size());
					return billList;		
				}
			
	public InvoiceDomain getInvoiceDomain() {
		return invoiceDomain;
	}
	public void setInvoiceDomain(
			InvoiceDomain invoiceDomain) {
		this.invoiceDomain = invoiceDomain;
	}

	public MemberService getMemberServiceBean() {
		return memberServiceBean;
	}

	public void setMemberServiceBean(MemberService memberServiceBean) {
		this.memberServiceBean = memberServiceBean;
	}

	public ReceiptInvoiceDomain getReceiptInvoiceDomain() {
		return receiptInvoiceDomain;
	}

	public void setReceiptInvoiceDomain(ReceiptInvoiceDomain receiptInvoiceDomain) {
		this.receiptInvoiceDomain = receiptInvoiceDomain;
	}

	/**
	 * @param transactionService the transactionService to set
	 */
	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	

}
