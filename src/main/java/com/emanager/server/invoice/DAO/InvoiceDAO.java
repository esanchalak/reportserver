package com.emanager.server.invoice.DAO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.financialReports.valueObject.MonthwiseChartVO;
import com.emanager.server.invoice.dataAccessObject.BillDetailsVO;
import com.emanager.server.invoice.dataAccessObject.CurrencyVO;
import com.emanager.server.invoice.dataAccessObject.InLineItemsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceBillingCycleVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceDetailsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceMemberChargesVO;
//import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.emanager.server.invoice.dataAccessObject.InvoiceVO;
import com.emanager.server.invoice.dataAccessObject.ItemDetailsVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.taxation.dataAccessObject.TDSDetailsVO;

public class InvoiceDAO {

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = Logger
			.getLogger(InvoiceDAO.class);
	
	SocietyService societyService;

	/* Get list of active society list */
	public List getActiveSocietyList(String societyID,String billingCycleID) {
		List societyList = null;
		logger.debug("Entry : public List getActiveSocietyList()");
		try {
			String strQuery = "SELECT society_name,b.* FROM society_settings s,in_billing_cycle_details b,society_details sd "
					+ "WHERE sd.working!=0 AND sd.society_id=s.society_id AND b.id=:billingID AND sd.society_id=b.society_id AND  b.society_id=:societyID;";

			Map nameparam = new HashMap();
			nameparam.put("societyID", societyID);
			nameparam.put("billingID", billingCycleID);
			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					InvoiceBillingCycleVO inBillingVO = new InvoiceBillingCycleVO();
					inBillingVO.setBillingCycleID(rs.getInt("id"));
					inBillingVO.setSocietyId(rs.getInt("society_id"));
					inBillingVO.setDescription(rs.getString("description"));
					inBillingVO.setFromDate(rs.getString("from_date"));
					inBillingVO.setUptoDate(rs.getString("to_date"));
					inBillingVO.setRunDate(rs.getString("rundate"));
					inBillingVO.setNextRunDate(rs.getString("next_run_date"));
					inBillingVO.setInvoiceType(rs.getString("type"));
					return inBillingVO;
				}
			};
			societyList = namedParameterJdbcTemplate.query(strQuery, nameparam,Rmapper);
		} catch (Exception e) {
			logger.error("Exception at getActiveSocietyList " + e);
		}
		logger.debug("Exit : public List getActiveSocietyList()");
		return societyList;
	}

	/* Get list of billingCycle list  of society*/
	public List getBillingCycleList(int societyID){
		logger.debug("Entry : public List getBillingCycleList(String societyID)");
		List billingCycleList=null;
		
		try {
			String strQuery = "SELECT * FROM in_billing_cycle_details WHERE society_id=:societyID; ";
					

			Map nameparam = new HashMap();
			nameparam.put("societyID", societyID);
			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					InvoiceBillingCycleVO inBillingVO = new InvoiceBillingCycleVO();
					inBillingVO.setBillingCycleID(rs.getInt("id"));
					inBillingVO.setSocietyId(rs.getInt("society_id"));
					inBillingVO.setDescription(rs.getString("description"));
					inBillingVO.setFromDate(rs.getString("from_date"));
					inBillingVO.setUptoDate(rs.getString("to_date"));
					inBillingVO.setRunDate(rs.getString("rundate"));
					inBillingVO.setDueDate(rs.getString("due_date"));
					inBillingVO.setNextRunDate(rs.getString("next_run_date"));
					return inBillingVO;
				}
			};
			billingCycleList = namedParameterJdbcTemplate.query(strQuery, nameparam,Rmapper);
		} catch (Exception e) {
			logger.error("Exception at getBillingCycleList " + e);
		}
		
		logger.debug("Exit : public List getBillingCycleList(String societyID)");
		
		return billingCycleList;
	}
	/* Get list of billingCycle Details  of society*/
	public InvoiceBillingCycleVO getBillingCycleDetails(int billingCycleID){
		logger.debug("Entry : public InvoiceBillingCycleVO getBillingCycleDetails(int billingCycleID)");
		InvoiceBillingCycleVO billingCycleVO=new InvoiceBillingCycleVO();
		
		try {
			String strQuery = "SELECT * FROM in_billing_cycle_details WHERE id=:billingCycleID; ";
					

			Map nameparam = new HashMap();
			nameparam.put("billingCycleID", billingCycleID);
			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					InvoiceBillingCycleVO inBillingVO = new InvoiceBillingCycleVO();
					inBillingVO.setBillingCycleID(rs.getInt("id"));
					inBillingVO.setSocietyId(rs.getInt("society_id"));
					inBillingVO.setDescription(rs.getString("description"));
					inBillingVO.setFromDate(rs.getString("from_date"));
					inBillingVO.setUptoDate(rs.getString("to_date"));
					inBillingVO.setRunDate(rs.getString("rundate"));
					inBillingVO.setDueDate(rs.getString("due_date"));
					inBillingVO.setNextRunDate(rs.getString("next_run_date"));
					return inBillingVO;
				}
			};
			billingCycleVO =(InvoiceBillingCycleVO) namedParameterJdbcTemplate.queryForObject(strQuery, nameparam,Rmapper);
		} catch (Exception e) {
			logger.error("Exception at getBillingCycleList " + e);
		}
		
		logger.debug("Exit : public InvoiceBillingCycleVO getBillingCycleDetails(int billingCycleID)");
		
		return billingCycleVO;
	}
	
	/* Get list of all active members of the given society */
	public List getActiveMemberList(int societyID) {
		List memberList = null;
		try {
			logger.debug("Entry : public List getActiveMemberList(String societyID)");
			SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			String sql = "SELECT CONCAT(b.building_name,' ',a.apt_name,' - ',m.title,' ',m.full_name) AS member_name,m.*,l.*,lu.*,b.society_id,a.*,a.is_Rented as tenant FROM apartment_details a,member_details m,building_details b,account_ledgers_"+societyVO.getDataZoneID()+" l,ledger_user_mapping_"+societyVO.getDataZoneID()+" lu "+
						  " WHERE l.org_id=lu.org_id and lu.org_id=:societyID and  a.apt_id=m.apt_id and m.type='P'  AND a.building_id=b.building_id AND m.member_id=lu.user_id AND lu.ledger_id=l.id AND b.society_id=:societyID;";
			
			
			Map hashMap = new HashMap();

			hashMap.put("societyID", societyID);

			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					// TODO Auto-generated method stub
					InvoiceMemberChargesVO inMemberVO = new InvoiceMemberChargesVO();
					inMemberVO.setAptID(rs.getInt("apt_id"));
					inMemberVO.setMemberName(rs.getString("member_name"));
					inMemberVO.setMemberID(rs.getInt("member_id"));
					inMemberVO.setSocietyID(rs.getInt("society_id"));
					inMemberVO.setLedgerID(rs.getInt("ledger_id"));
					inMemberVO.setBuildingID(rs.getInt("building_id"));
					inMemberVO.setUnitID(rs.getInt("unit_id"));
					inMemberVO.setSquareFt(rs.getBigDecimal("sq_feet"));
					inMemberVO.setUnitCost(rs.getBigDecimal("cost"));
					inMemberVO.setIsRented(rs.getInt("tenant"));
					inMemberVO.setInvoiceGroups(rs.getString("inv_group"));
					inMemberVO.setIsActive(true);
					return inMemberVO;
				}
			};
			memberList = namedParameterJdbcTemplate.query(sql, hashMap, rMapper);

		} catch (Exception ex) {
			logger.error("Exception in public List getMemberList(String societyId) : "
							+ ex);
		}

		logger.debug("Exit : public List getActiveMemberList(String societyID)"+memberList.size());
		return memberList;
	}
	
	/* Get list of all active members of the given society */
	public InvoiceMemberChargesVO getActiveMemberVO(int societyID,int aptID) {
		List memberList = null;
		InvoiceMemberChargesVO memberVO=new InvoiceMemberChargesVO();
		try {
			logger.debug("Entry : public InvoiceMemberChargesVO getActiveMemberVO(String societyID)");
			SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			String sql = "SELECT CONCAT(b.building_name,' ',a.apt_name,' - ',m.title,' ',m.full_name) AS member_name, m.*,l.*,lu.*,b.society_id,a.* FROM apartment_details a,member_details m,building_details b,account_ledgers_"+societyVO.getDataZoneID()+" l,ledger_user_mapping_"+societyVO.getDataZoneID()+" lu "+
						  " WHERE l.org_id=lu.org_id and lu.org_id=:societyID and a.apt_id=m.apt_id AND a.building_id=b.building_id AND m.member_id=lu.user_id AND lu.ledger_id=l.id AND b.society_id=:societyID and a.apt_id=m.apt_id and m.apt_id=:aptID and m.type='P' ";

			Map hashMap = new HashMap();

			hashMap.put("societyID", societyID);
			hashMap.put("aptID", aptID);

			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					// TODO Auto-generated method stub
					InvoiceMemberChargesVO inMemberVO = new InvoiceMemberChargesVO();
					inMemberVO.setAptID(rs.getInt("apt_id"));
					inMemberVO.setMemberName(rs.getString("member_name"));
					inMemberVO.setMemberID(rs.getInt("member_id"));
					inMemberVO.setSocietyID(rs.getInt("society_id"));
					inMemberVO.setLedgerID(rs.getInt("ledger_id"));
					inMemberVO.setUnitID(rs.getInt("unit_id"));
					inMemberVO.setSquareFt(rs.getBigDecimal("sq_feet"));
					inMemberVO.setUnitCost(rs.getBigDecimal("cost"));
					inMemberVO.setIsRented(rs.getInt("is_Rented"));
					inMemberVO.setInvoiceGroups(rs.getString("inv_group"));
					

					return inMemberVO;
				}
			};
			memberVO = (InvoiceMemberChargesVO) namedParameterJdbcTemplate.queryForObject(sql, hashMap, rMapper);

		} catch (Exception ex) {
			logger.error("Exception in public InvoiceMemberChargesVO getActiveMemberVO(String societyId) : "
							+ ex);
		}

		logger.debug("Exit : public InvoiceMemberChargesVO getActiveMemberVO(String societyID)");
		return memberVO;
	}
		
	/* Get list of all applicable fees for the given member */
	public List getApplicableChargesToMember(InvoiceDetailsVO invoiceVO,InvoiceBillingCycleVO bcVO) {
		logger.debug("Entry : public List getApplicableChargesToMember(int societyID,int memberID)");
		List memberChargeList = null;
		InvoiceMemberChargesVO memberVO=invoiceVO.getMemberVO();
		
		
		
		try {
			String sql = "SELECT m.id, m.entity_id,m.cal_method,m.ledger_id,m.amount,m.charge_type,m.payment_priority,b.* "
					+ " FROM in_member_charge_details m,in_billing_cycle_details b WHERE m.society_id=b.society_id AND (b.society_id=:societyID )" +
							" AND b.id=:billingID AND m.invoice_type=:invoiceType AND m.status='Active' ORDER BY payment_priority; ";
			logger.debug(sql+"  "+memberVO.getSocietyID()+" "+memberVO.getAptID()+"  "+bcVO.getInvoiceType());
			Map hashMap = new HashMap();

			hashMap.put("societyID", memberVO.getSocietyID());
			
			hashMap.put("billingID", bcVO.getBillingCycleID());
			hashMap.put("invoiceType", bcVO.getInvoiceType());

			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					InvoiceMemberChargesVO inMemberVO = new InvoiceMemberChargesVO();
					inMemberVO.setChargeID(rs.getInt("id"));
					inMemberVO.setCalculationMethod(rs.getString("cal_method"));
					inMemberVO.setEntityID(rs.getBigDecimal("entity_id"));
					inMemberVO.setLedgerID(rs.getInt("ledger_id"));
					inMemberVO.setTotalAmount(rs.getBigDecimal("amount"));
					inMemberVO.setCategoryName(rs.getString("charge_type"));
					inMemberVO.setPaymentPriority(rs.getInt("payment_priority"));
					inMemberVO.setFromDate(rs.getString("from_date"));
					inMemberVO.setUptoDate(rs.getString("to_date"));
					inMemberVO.setDescription(rs.getString("description"));
					inMemberVO.setRunDate(rs.getString("rundate"));
					inMemberVO.setNextRunDate(rs.getString("next_run_date"));
					inMemberVO.setDueDate(rs.getString("due_date"));
					inMemberVO.setInvoiceType(rs.getString("type"));
					inMemberVO.setParams(rs.getString("param"));
					inMemberVO.setIsApplicable(true);
					return inMemberVO;
				}
			};
			memberChargeList = namedParameterJdbcTemplate.query(sql, hashMap,rMapper);

		} catch (Exception e) {
			logger.error("Exception at getApplicableChargesToMember " + e);
		}
		logger.debug("Exit : public List getApplicableChargesToMember(int societyID,int memberID)");
		return memberChargeList;
	}
	
	/* Get list of all unpaid invoices the given society   
	public List getInvoicesForSociety(int societyID,int billingID,int status ,int invoiceTypeID){
		List invoiceList=null;
		logger.debug("Entry : public List getInvoicesForSociety(String societyID)"+invoiceTypeID);
		String statusCondition="";
		String invoiceTypeCondition="";
		try{
		if(status==10){
			statusCondition="";
		}else{
			statusCondition="And ins.status_id="+status+"";
		}
		if(invoiceTypeID==0){
			invoiceTypeCondition="";
		}else
			invoiceTypeCondition="and ins.invoice_type_id="+invoiceTypeID+"";
		
		
		String sql = "SELECT CONCAT(b.building_name,' - ',a.apt_name)AS unitNo,CONCAT(m.title,' ',m.full_name) AS fullName,CONCAT(b.building_name,' ',a.apt_name,' ',m.title,' ',m.full_name) AS memberName,CAST(ins.create_date AS DATE) AS crDt, ins.*,itd.invoice_type as invoiceType "+
					 " FROM in_invoice_"+societyID+" ins,member_details m,apartment_details a, building_details b ,in_invoice_type_details itd "+
					 " WHERE a.apt_id=ins.apt_id AND ins.apt_id=m.apt_id AND a.building_id=b.building_id AND b.society_id=:societyID And ins.billing_process_id=:billingID "+ 
					 " AND m.type='P' AND m.is_current=1 and itd.id=ins.invoice_type_id "+statusCondition+" "+invoiceTypeCondition+" AND ins.is_deleted=0 ORDER BY seq_no;";
		logger.debug("Query is "+sql);

	Map hashMap = new HashMap();

	hashMap.put("societyID",societyID);
	hashMap.put("billingID", billingID);
	hashMap.put("status", status);
	

	RowMapper rMapper = new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvoiceDetailsVO inMemberVO = new InvoiceDetailsVO();
			
			inMemberVO.setAptID(rs.getInt("apt_id"));
			inMemberVO.setInvoiceID(rs.getString("id"));
			inMemberVO.setMemberName(rs.getString("memberName"));
			inMemberVO.setFullName(rs.getString("fullName"));
			inMemberVO.setUnitNo(rs.getString("unitNo"));
			inMemberVO.setCreateDate(rs.getString("crDt"));
			inMemberVO.setInvoiceDate(rs.getString("invoice_date"));
			inMemberVO.setTotalAmount(rs.getBigDecimal("total_amount"));
			inMemberVO.setFromDate(rs.getString("from_date"));
			inMemberVO.setUptoDate(rs.getString("to_date"));
			inMemberVO.setCarriedBalance(rs.getBigDecimal("carried_balance"));
			inMemberVO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
			inMemberVO.setBillCycleID(rs.getInt("billing_process_id"));
			inMemberVO.setDueDate(rs.getString("due_date"));
			inMemberVO.setInvoiceType(rs.getString("invoiceType"));
			inMemberVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
			inMemberVO.setBalance(rs.getBigDecimal("balance"));
			inMemberVO.setCurrentBalance(rs.getBigDecimal("current_balance"));
			inMemberVO.setStatusID(rs.getInt("status_id"));
			inMemberVO.setIsMailSent(rs.getInt("is_sent"));
			if(inMemberVO.getIsMailSent()==0){
				inMemberVO.setIsApplicable(true);
			}
			return inMemberVO;
		}
	};
		
		invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
		}catch(Exception e){
			logger.error("Exception in getInvoicesForSociety "+e);
		}
		logger.debug("Exit : public List getInvoicesForSociety(String societyID)"+invoiceList.size());
		return invoiceList;		
	}
	*/
	/* Get list of all unpaid invoices the given society   
	public List getInvoicesForMember(int societyID,int aptID,String fromDate,String uptoDate,String invoiceID ){
		List invoiceList=null;
		logger.debug("Entry : public List getInvoicesForMember(String societyID,int aptID,String fromDate,String uptoDate )"+aptID);
		String strCondition="";
		if(invoiceID.equalsIgnoreCase("0")){
			strCondition= " AND ( invoice_date BETWEEN :fromDate AND :uptoDate)";
			
		}else{
			strCondition=" AND ins.id=:invoiceID ";
			
		}
		try{
		
		String sql = "SELECT CONCAT(unit_name,' ',m.title,' ',m.full_name) AS memberName, CONCAT(m.title,' ',m.full_name) AS fullName, CONCAT(m.building_name,' - ',m.apt_name) AS unitName, m.apt_id, invoice_date,ins.id AS invoiceID, invoice_amount,carried_balance,total_amount,balance,ins.current_balance," +
					 " invoice_type_id,ins.status_id,COALESCE(ri.amount, 0) as paidAmt,itd.invoice_type AS invoiceType ,from_date,to_date,billing_process_id,invoice_type_id,is_sent,due_date"+
					 " FROM (in_invoice_"+societyID+" ins,members_info m,in_invoice_type_details itd)  LEFT JOIN receipt_invoice_mapping_"+societyID+" ri ON  ri.invoice_id =ins.id"+
					 " WHERE ins.apt_id=m.apt_id AND m.apt_id=:aptID AND m.type='P' AND m.is_current=1 "+strCondition+
					 " AND itd.id=ins.invoice_type_id AND ins.is_deleted=0 group by ins.id ORDER BY ins.invoice_date desc;";
		

	Map hashMap = new HashMap();

	hashMap.put("societyID",societyID);
	hashMap.put("aptID", aptID);
	hashMap.put("fromDate", fromDate);
	hashMap.put("uptoDate", uptoDate);
	hashMap.put("invoiceID", invoiceID);

	RowMapper rMapper = new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvoiceDetailsVO inMemberVO = new InvoiceDetailsVO();
			
			inMemberVO.setAptID(rs.getInt("apt_id"));
			inMemberVO.setInvoiceID(rs.getString("invoiceID"));
			inMemberVO.setMemberName(rs.getString("memberName"));
			inMemberVO.setInvoiceDate(rs.getString("invoice_date"));
			inMemberVO.setPaidAmount(rs.getBigDecimal("paidAmt"));
			inMemberVO.setTotalAmount(rs.getBigDecimal("total_amount"));
			inMemberVO.setFromDate(rs.getString("from_date"));
			inMemberVO.setUptoDate(rs.getString("to_date"));
			inMemberVO.setCarriedBalance(rs.getBigDecimal("carried_balance"));
			inMemberVO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
			inMemberVO.setBillCycleID(rs.getInt("billing_process_id"));
			inMemberVO.setDueDate(rs.getString("due_date"));
			inMemberVO.setInvoiceType(rs.getString("invoiceType"));
			inMemberVO.setBalance(rs.getBigDecimal("balance"));
			inMemberVO.setCurrentBalance(rs.getBigDecimal("current_balance"));
			inMemberVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
			inMemberVO.setStatusID(rs.getInt("status_id"));
			inMemberVO.setIsMailSent(rs.getInt("is_sent"));
		    inMemberVO.setFullName(rs.getString("fullName"));
		    inMemberVO.setUnitNo(rs.getString("unitName"));
		    if(inMemberVO.getIsMailSent()==0){
				inMemberVO.setIsApplicable(true);
			}
			return inMemberVO;
		}
	};
		
		invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
		}catch(Exception e){
			logger.error("Exception in getInvoicesForSociety "+e);
		}
		logger.debug("Exit : public List getInvoicesForMember(String societyID)"+invoiceList.size());
		return invoiceList;		
	}*/

	
/*	public List getUnpaidInvoicesForMember(String societyID,String aptID,String fromDate,String uptoDate, String invoiceID ){
		List invoiceList=null;
		logger.debug("Entry : public List getUnpaidInvoicesForMember(String societyID,int aptID,String fromDate,String uptoDate )");
		String strCondition="";
		if(invoiceID.equalsIgnoreCase("0")){
			strCondition= " AND ( invoice_date BETWEEN :fromDate AND :uptoDate)";
			
		}else{
			strCondition=" AND ins.id=:invoiceID ";
			
		}
		try{
		
		String sql = "SELECT CONCAT(b.building_name,' - ',a.apt_name)AS unitNo,CONCAT(m.title,' ',m.full_name) AS fullName,CONCAT(b.building_name,' ',a.apt_name,' ',m.title,' ',m.full_name) AS memberName,CAST(ins.create_date AS DATE) AS crDt, ins.* ,itd.invoice_type as invoiceType"+
					 " FROM in_invoice_"+societyID+" ins,member_details m,apartment_details a, building_details b ,in_invoice_type_details itd"+
					 " WHERE a.apt_id=ins.apt_id AND ins.apt_id=m.apt_id AND a.building_id=b.building_id AND b.society_id=:societyID And m.apt_id=:aptID "+ 
					 " AND m.type='P' AND m.is_current=1 "+strCondition+" and ins.status_id<2 and itd.id=ins.invoice_type_id AND ins.is_deleted=0 ORDER BY ins.status_id;";
		logger.debug("Query is "+sql+" "+aptID+" "+fromDate+" "+uptoDate);

	Map hashMap = new HashMap();

	hashMap.put("societyID",societyID);
	hashMap.put("aptID", aptID);
	hashMap.put("fromDate", fromDate);
	hashMap.put("uptoDate", uptoDate);
	hashMap.put("invoiceID", invoiceID);

	RowMapper rMapper = new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvoiceDetailsVO inMemberVO = new InvoiceDetailsVO();
			
			inMemberVO.setAptID(rs.getInt("apt_id"));
			inMemberVO.setInvoiceID(rs.getString("id"));
			inMemberVO.setMemberName(rs.getString("memberName"));
			inMemberVO.setFullName(rs.getString("fullName"));
			inMemberVO.setUnitNo(rs.getString("unitNo"));
			inMemberVO.setInvoiceDate(rs.getString("invoice_date"));
			inMemberVO.setCreateDate(rs.getString("crDt"));
			inMemberVO.setTotalAmount(rs.getBigDecimal("total_amount"));
			inMemberVO.setFromDate(rs.getString("from_date"));
			inMemberVO.setUptoDate(rs.getString("to_date"));
			inMemberVO.setCarriedBalance(rs.getBigDecimal("carried_balance"));
			inMemberVO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
			inMemberVO.setBillCycleID(rs.getInt("billing_process_id"));
			inMemberVO.setDueDate(rs.getString("due_date"));
			inMemberVO.setInvoiceType(rs.getString("invoiceType"));
			inMemberVO.setBalance(rs.getBigDecimal("balance"));
			inMemberVO.setCurrentBalance(rs.getBigDecimal("current_balance"));
			inMemberVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
			inMemberVO.setStatusID(rs.getInt("status_id"));
			inMemberVO.setIsMailSent(rs.getInt("is_sent"));
			if(inMemberVO.getIsMailSent()==0){
				inMemberVO.setIsApplicable(true);
			}
			return inMemberVO;
		}
	};
		
		invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
		}catch(Exception e){
			logger.error("Exception in getUnpaidInvoicesForMember "+e);
		}
		logger.debug("Exit : public List getUnpaidInvoicesForMember(String societyID)"+invoiceList.size());
		return invoiceList;		
	}*/
	
	/* Get list of all unpaid invoices the given society   
	public List getInvoicesForApartment(int societyID ,int aptID,int invoiceTypeID){
		List invoiceList=null;
		logger.debug("Entry : public List getInvoicesForApartment(String societyID ,int aptID,String type)"+invoiceTypeID);
		String strCondition="";	
		if(invoiceTypeID!=0){
			strCondition="and ins.invoice_type_id=:invoiceTypeID";
		}
		try{
		
		String sql = "SELECT CONCAT(b.building_name,' - ',a.apt_name)AS unitNo,CONCAT(m.title,' ',m.full_name) AS fullName,CONCAT(b.building_name,' ',a.apt_name,' ',m.title,' ',m.full_name) AS memberName,CAST(ins.create_date AS DATE) AS crDt, ins.* ,itd.invoice_type AS invoiceType "+
					 " FROM in_invoice_"+societyID+" ins,member_details m,apartment_details a, building_details b ,in_invoice_type_details itd"+
					 " WHERE a.apt_id=ins.apt_id AND ins.apt_id=m.apt_id AND ins.invoice_type_id=itd.id and a.building_id=b.building_id AND b.society_id=:societyID And m.apt_id=:aptID "+ 
					 " AND m.type='P' AND m.is_current=1 "+strCondition+" and (status_id=0 or status_id=1) AND ins.is_deleted=0  ORDER BY ins.id;";
		logger.debug("Query is "+sql);

	Map hashMap = new HashMap();

	hashMap.put("societyID",societyID);
	hashMap.put("aptID", aptID);
	hashMap.put("invoiceTypeID", invoiceTypeID);
	
	

	RowMapper rMapper = new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvoiceDetailsVO inMemberVO = new InvoiceDetailsVO();
			
			inMemberVO.setAptID(rs.getInt("apt_id"));
			inMemberVO.setInvoiceID(rs.getString("id"));
			inMemberVO.setMemberName(rs.getString("memberName"));
			inMemberVO.setFullName(rs.getString("fullName"));
			inMemberVO.setUnitNo(rs.getString("unitNo"));
			inMemberVO.setCreateDate(rs.getString("crDt"));
			inMemberVO.setInvoiceDate(rs.getString("invoice_date"));
			inMemberVO.setTotalAmount(rs.getBigDecimal("total_amount"));
			inMemberVO.setFromDate(rs.getString("from_date"));
			inMemberVO.setUptoDate(rs.getString("to_date"));
			inMemberVO.setCarriedBalance(rs.getBigDecimal("carried_balance"));
			inMemberVO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
			inMemberVO.setBillCycleID(rs.getInt("billing_process_id"));
			inMemberVO.setDueDate(rs.getString("due_date"));
			inMemberVO.setInvoiceType(rs.getString("invoiceType"));
			inMemberVO.setBalance(rs.getBigDecimal("balance"));
			inMemberVO.setCurrentBalance(rs.getBigDecimal("current_balance"));
			if(inMemberVO.getBalance().signum()<0){
				inMemberVO.setPaidAmount(BigDecimal.ZERO);
			}else
			inMemberVO.setPaidAmount(rs.getBigDecimal("balance"));
			inMemberVO.setStatusID(rs.getInt("status_id"));
			inMemberVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
			inMemberVO.setIsMailSent(rs.getInt("is_sent"));
			if(inMemberVO.getIsMailSent()==0){
				inMemberVO.setIsApplicable(true);
			}
			return inMemberVO;
		}
	};
		
		invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
		if(((invoiceTypeID<4)||(invoiceTypeID==14)||(invoiceTypeID==15))&&(invoiceList.size()==0)){
			
			invoiceList=getLatestRegularInvoices(societyID, aptID, invoiceTypeID);
		}
		}catch(Exception e){
			logger.error("Exception in getInvoicesForApartment "+e);
		}
		logger.debug("Exit : public List getInvoicesForApartment(String societyID ,int aptID,String type))"+invoiceList.size());
		return invoiceList;		
	}
	
	 Get list of all unpaid invoices the given society   
	public List getLatestRegularInvoices(int societyID ,int aptID,int invoiceTypeID){
		List invoiceList=null;
		logger.debug("Entry : public List getInvoicesForApartment(String societyID ,int aptID,String type)");
			
		
		try{
		
		String sql = "SELECT CONCAT(b.building_name,' ',a.apt_name,' ',m.title,' ',m.full_name) AS memberName,CAST(ins.create_date AS DATE) AS crDt, ins.* ,itd.invoice_type AS invoiceType "+
					 " FROM in_invoice_"+societyID+" ins,member_details m,apartment_details a, building_details b ,in_invoice_type_details itd"+
					 " WHERE a.apt_id=ins.apt_id AND ins.apt_id=m.apt_id AND ins.invoice_type_id=itd.id and a.building_id=b.building_id AND b.society_id=:societyID And m.apt_id=:aptID "+ 
					 " AND m.type='P' AND m.is_current=1 and ins.invoice_type_id=:invoiceTypeID  AND ins.is_deleted=0 ORDER BY ins.id desc limit 1;";
		logger.debug("Query is "+sql);

	Map hashMap = new HashMap();

	hashMap.put("societyID",societyID);
	hashMap.put("aptID", aptID);
	hashMap.put("invoiceTypeID", invoiceTypeID);
	
	

	RowMapper rMapper = new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvoiceDetailsVO inMemberVO = new InvoiceDetailsVO();
			
			inMemberVO.setAptID(rs.getInt("apt_id"));
			inMemberVO.setInvoiceID(rs.getString("id"));
			inMemberVO.setMemberName(rs.getString("memberName"));
			inMemberVO.setCreateDate(rs.getString("crDt"));
			inMemberVO.setInvoiceDate(rs.getString("invoice_date"));
			inMemberVO.setTotalAmount(rs.getBigDecimal("total_amount"));
			inMemberVO.setFromDate(rs.getString("from_date"));
			inMemberVO.setUptoDate(rs.getString("to_date"));
			inMemberVO.setCarriedBalance(rs.getBigDecimal("carried_balance"));
			inMemberVO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
			inMemberVO.setBillCycleID(rs.getInt("billing_process_id"));
			inMemberVO.setDueDate(rs.getString("due_date"));
			inMemberVO.setInvoiceType(rs.getString("invoiceType"));
			inMemberVO.setBalance(rs.getBigDecimal("balance"));
			inMemberVO.setCurrentBalance(rs.getBigDecimal("current_balance"));
			
			if(inMemberVO.getBalance().signum()<0){
				
				inMemberVO.setPaidAmount(BigDecimal.ZERO);
			}else
			inMemberVO.setPaidAmount(rs.getBigDecimal("balance"));
			inMemberVO.setStatusID(rs.getInt("status_id"));
			inMemberVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
			inMemberVO.setIsMailSent(rs.getInt("is_sent"));
			if(inMemberVO.getIsMailSent()==0){
				inMemberVO.setIsApplicable(true);
			}
			return inMemberVO;
		}
	};
		
		invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
		
		}catch(Exception e){
			logger.error("Exception in getInvoicesForApartment "+e);
		}
		
		logger.debug("Exit : public List getInvoicesForApartment(String societyID ,int aptID,String type))"+invoiceList.size());
		return invoiceList;		
	}
	
	 Get list of all unpaid invoices the given society   
	public List getUnpaidInvoicesForApartment(int societyID ,String aptID,int invoiceTypeID){
		List invoiceList=null;
		logger.debug("Entry : public List getInvoicesForApartment(String societyID ,int aptID,String type)");
		String strCondition=" and itd.id=:invoiceTypeID ";
		String strApt=" And m.apt_id=:aptID ";
		String strLimit="";
		if(invoiceTypeID==0){
			strCondition="";
			strLimit="";			
		}
		if(aptID=="0"){
			 strApt="";
			
		}	
		
		try{
		
		String sql = "SELECT CONCAT(b.building_name,' - ',a.apt_name)AS unitNo,CONCAT(b.building_name,' ',a.apt_name,' ',m.title,' ',m.full_name) AS fullName,CONCAT(b.building_name,' ',a.apt_name,' ',m.title,' ',m.full_name) AS memberName,CAST(ins.create_date AS DATE) AS crDt, ins.*,itd.invoice_type as invoiceType "+
					 " FROM in_invoice_"+societyID+" ins,member_details m,apartment_details a, building_details b ,in_invoice_type_details itd"+
					 " WHERE a.apt_id=ins.apt_id AND ins.apt_id=m.apt_id AND a.building_id=b.building_id AND b.society_id=:societyID "+ strApt+  
					 " AND m.type='P' AND m.is_current=1 and (status_id=0 or status_id=1) AND ins.is_deleted=0 and ins.invoice_type_id=itd.id  "+strCondition+" ORDER BY ins.id;";
		logger.debug("Query is "+sql +" "+aptID+" "+invoiceTypeID);

	Map hashMap = new HashMap();

	hashMap.put("societyID",societyID);
	hashMap.put("aptID", aptID);
	hashMap.put("invoiceTypeID", invoiceTypeID);
	
	
	
	RowMapper rMapper = new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvoiceDetailsVO inMemberVO = new InvoiceDetailsVO();
			
			inMemberVO.setAptID(rs.getInt("apt_id"));
			inMemberVO.setInvoiceID(rs.getString("id"));
			inMemberVO.setFullName(rs.getString("fullName"));
			inMemberVO.setUnitNo(rs.getString("unitNo"));
			inMemberVO.setMemberName(rs.getString("memberName"));
			inMemberVO.setCreateDate(rs.getString("crDt"));
			inMemberVO.setTotalAmount(rs.getBigDecimal("total_amount"));
			inMemberVO.setFromDate(rs.getString("from_date"));
			inMemberVO.setUptoDate(rs.getString("to_date"));
			inMemberVO.setInvoiceDate(rs.getString("invoice_date"));
			inMemberVO.setCarriedBalance(rs.getBigDecimal("carried_balance"));
			inMemberVO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
			inMemberVO.setBillCycleID(rs.getInt("billing_process_id"));
			inMemberVO.setDueDate(rs.getString("due_date"));
			inMemberVO.setInvoiceType(rs.getString("invoiceType"));
			inMemberVO.setBalance(rs.getBigDecimal("balance"));
			inMemberVO.setCurrentBalance(rs.getBigDecimal("current_balance"));
			inMemberVO.setStatusID(rs.getInt("status_id"));
			inMemberVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
			inMemberVO.setIsMailSent(rs.getInt("is_sent"));
			if(inMemberVO.getIsMailSent()==0){
				inMemberVO.setIsApplicable(true);
			}
			
			return inMemberVO;
		}
	};
		
		invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
		}catch(Exception e){
			logger.error("Exception in getInvoicesForApartment "+e);
		}
		logger.debug("Exit : public List getInvoicesForApartment(String societyID ,int aptID,String type))"+invoiceList.size());
		return invoiceList;		
	}
	public int updateStatusOfInvoices(String societyID,int invoiceID){
		int success=0;
		
		try{
		
		String sql="UPDATE in_invoice_"+societyID+" SET is_sent=1 WHERE id=:invoiceID;";	
		Map hMap=new HashMap();
		hMap.put("invoiceID", invoiceID);
		
		
		success=namedParameterJdbcTemplate.update(sql, hMap);
		
		
		}catch(Exception e){
			logger.error("Exception in updateStatusOfInvoices "+e);
		}
		return success;
	}
*/	/* Get list of all line items of the given invoice   */
	public List getLineItemsForInvoice(int societyID ,int invoiceID){
		List invoiceList=null;
		logger.debug("Entry : public List getLineItemsForInvoice(String societyID)"+invoiceID);
		
		try{
			SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			 String sql = "SELECT SUM(total_price) AS sAmt, SUM(discount) AS discAmt ,sum(igst) as iGstAmt,sum(cgst) as cGstAmt,sum(sgst) as sGstAmt,sum(ugst) as uGstAmt,sum(cess) as cessAmt , ins.*,ledger_name  FROM invoice_line_item_details ins, account_ledgers_"+societyVO.getDataZoneID()+" l WHERE l.org_id=:societyID  and ins.ledger_id=l.id AND ins.invoice_id=:invoiceID group by ins.id order by ins.id ;";

		Map hashMap = new HashMap();

		hashMap.put("societyID",societyID);
		hashMap.put("invoiceID", invoiceID);
		

		RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				InLineItemsVO inMemberVO = new InLineItemsVO();
				
				inMemberVO.setLedgerID(rs.getInt("ledger_id"));
				inMemberVO.setInvoiceID(rs.getInt("invoice_id"));
				inMemberVO.setInvLineItemID(rs.getInt("id"));
				inMemberVO.setLedgerName(rs.getString("ledger_name"));
				inMemberVO.setDescription(rs.getString("description"));
				inMemberVO.setTotalPrice(rs.getBigDecimal("sAmt"));
				inMemberVO.setDiscount(rs.getBigDecimal("discAmt"));	
				inMemberVO.setcGST(rs.getBigDecimal("cGstAmt"));
				inMemberVO.setiGST(rs.getBigDecimal("iGstAmt"));
				inMemberVO.setsGST(rs.getBigDecimal("sGstAmt"));
				inMemberVO.setuGST(rs.getBigDecimal("uGstAmt"));
				inMemberVO.setCess(rs.getBigDecimal("cessAmt"));
				inMemberVO.setLineItemTags(rs.getString("line_item_tags"));
				return inMemberVO;
			}
		};
			
			invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
			
			}catch(Exception e){
				logger.error("Exception in getInvoicesForSociety "+e);
			}
		
		logger.debug("Exit : public List getLineItemsForInvoice(String societyID)"+invoiceList.size());
		return invoiceList;		
	}
	
/*	 Insert the invoice 
	public int insertInvoice(InvoiceDetailsVO invoiceVO) {
		int insertFlag = 0;
		int generatedID = 0;
		InvoiceMemberChargesVO memberVO=invoiceVO.getMemberVO();
		try {

			logger.debug("Entry : public int insertInvoice(int societyID,List memberChargeList)"+memberVO.getAptID());

			String query = "INSERT INTO `in_invoice_"
					+ memberVO.getSocietyID()
					+ "`(`invoice_date`,`create_date`,`billing_process_id`,`apt_id`,`from_date`,`to_date`,`due_date`,`total_amount`,`invoice_amount`,`balance`,`current_balance`,`carried_balance`,`customer_notes`,`invoice_type`,`status_id`,`invoice_type_id`) "
					+ " VALUES ( :invoiceDate,:createDate,:billProcessID,:aptID,:fromDate,:uptoDate,:dueDate,:totalAmount,:invoiceAmount,:balance,:currentBalance,:carriedBalance,:description,:invoiceType,:statusID,:invoiceTypeID) ";

			SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(
					invoiceVO);
			KeyHolder keyHolder = new GeneratedKeyHolder();
			insertFlag = getNamedParameterJdbcTemplate().update(query,
					fileParameters, keyHolder);
			generatedID = keyHolder.getKey().intValue();
			logger.debug("key GEnereator ---" + generatedID);
		} catch (Exception e) {
			logger.error("Exception at insertInvoice " + e);
		}
		logger.debug("Exit : public int insertInvoice(int societyID,List memberChargeList)");
		return generatedID;
	}

	 Insert the invoice line items 
	public int insertInvoiceLineItems(final InvoiceDetailsVO invoiceVO) {
		int flag = 0;
		
		final List<InvoiceLineItemsVO> invoiceLineList = invoiceVO.getListOfLineItems();
		InvoiceMemberChargesVO memberVO=invoiceVO.getMemberVO();
		logger.debug("Entry : public int insertInvoice(int societyID,List memberChargeList)");

		try {
			String str = "INSERT INTO in_invoice_line_"
					+ memberVO.getSocietyID()
					+ "(invoice_id,ledger_id,amount,payment_priority,balance,carried_balance,description) "
					+ " VALUES ( ?,?,?,?,?,?,?) ;";

			getJdbcTemplate().batchUpdate(str,new BatchPreparedStatementSetter() {

						public void setValues(PreparedStatement ps, int i)
								throws SQLException {
							InvoiceLineItemsVO imVO = invoiceLineList.get(i);
							ps.setString(1, invoiceVO.getInvoiceID());
							ps.setInt(2, imVO.getCategoryID());
							ps.setBigDecimal(3, imVO.getTotalAmount());
							ps.setInt(4, imVO.getPaymentPriority());
							ps.setBigDecimal(5, imVO.getBalance());
							ps.setBigDecimal(6, imVO.getCarriedBalance());
							ps.setString(7,imVO.getDescription());

						}

						public int getBatchSize() {
							return invoiceLineList.size();
						}

					});

		} catch (DataAccessException e) {
			flag = 0;
			logger.error("DataACCESSException in insertInvoice : " + e);
			return flag;
		} catch (Exception ex) {
			flag = 0;
			logger.error("Exception in insertInvoice : " + ex);
			return flag;
		}
		flag = 1;

		// return flag;

		logger.debug("Exit : public int insertInvoice(int societyID,List memberChargeList)");
		return flag;
		
	}
	
	 Update the charge period after adding the invoice  
	public int updateChargeStatus(InvoiceDetailsVO invoiceVO,int societyID,InvoiceMemberChargesVO liVO){
		int insertFlag=0;
		logger.debug("Entry : public int updateChargeStatus(int societyID,int apartmentID)");
		String sql="";
		String condition="";
		
		try {
			if(liVO.getFrequency().equalsIgnoreCase("OneTime")){
				sql="Update in_member_charge_details set status='Inactive' where id=:ID and society_id=:societyID ;";
			}else{
				if(liVO.getFrequency().equalsIgnoreCase("Monthly")){
					condition="INTERVAL 1 MONTH";
				}else if(liVO.getFrequency().equalsIgnoreCase("BIMONTHLY")){
					condition="INTERVAL 2 MONTH";
				}
				else if(liVO.getFrequency().equalsIgnoreCase("Quarterly")){
					condition="INTERVAL 3 MONTH";
				}else if(liVO.getFrequency().equalsIgnoreCase("HalfYearly")){
					condition="INTERVAL 6 MONTH";
				}else if(liVO.getFrequency().equalsIgnoreCase("Yearly")){
					condition="INTERVAL 1 YEAR";
				}
				
				sql="Update in_member_charge_details set next_charge_date=DATE_ADD(next_charge_date, "+condition+" ) where id=:ID and society_id=:societyID; ";
			}
			
			Map hashMap = new HashMap();

			hashMap.put("societyID",societyID);
			hashMap.put("ID", liVO.getChargeID());
			
			insertFlag=namedParameterJdbcTemplate.update(sql, hashMap);
		} catch (Exception e) {
		logger.error("Exception at updateChargeStatus "+e);
		}
		
		logger.debug("Exit : public int updateChargeStatus(int societyID,int apartmentID)");
		return insertFlag;		
	}
	
	
	 Update the invoice 
	public int updateInvoice(InvoiceDetailsVO invoiceVO,int societyID) {
		int updateFlag = 0;
		logger.debug("Entry : public int updateInvoice(InvoiceDetailsVO invoiceVO,int societyID)");
		
		try {
			String sql="Update in_invoice_"+societyID+" set invoice_date=:invoiceDate, due_date=:dueDate, status_id=:statusID, invoice_amount=:invoiceAmount," +
					" total_amount=:totalAmount, balance=:balance, carried_balance=:carriedBalance, current_balance=:currentBalance, invoice_type=:invoiceType," +
					" invoice_type_id=:invoiceTypeID where id=:invoiceID ";
			
			Map hashMap = new HashMap();

			hashMap.put("societyID",societyID);
			hashMap.put("invoiceID", invoiceVO.getInvoiceID());
			hashMap.put("invoiceDate",invoiceVO.getInvoiceDate());
			hashMap.put("dueDate", invoiceVO.getDueDate());
			hashMap.put("statusID",invoiceVO.getStatusID());
			hashMap.put("invoiceAmount", invoiceVO.getInvoiceAmount());
			hashMap.put("totalAmount",invoiceVO.getTotalAmount());
			hashMap.put("balance", invoiceVO.getBalance());
			hashMap.put("carriedBalance",invoiceVO.getCarriedBalance());
			hashMap.put("currentBalance", invoiceVO.getCurrentBalance());
			hashMap.put("invoiceType",invoiceVO.getInvoiceType());
			hashMap.put("invoiceTypeID", invoiceVO.getInvoiceTypeID());
			
			updateFlag=namedParameterJdbcTemplate.update(sql, hashMap);
			
		} catch (Exception e) {
			logger.error("Exception Occurred while updating Invoices "+e);
		}
			
		

		logger.debug("Exit : public int updateInvoice(int invoiceID,int societyID)");
		return updateFlag;
	}

	 Delete the invoice 
	public int deleteInvoice(int invoiceID, int societyID) {
		int insertFlag = 0;

		logger.debug("Entry : public int deleteInvoice(int invoiceID,int apartmentID)");
		
		try{
			
			String sql="Update in_invoice_"+societyID+" set is_deleted=1,balance=0,current_balance=0 where id=:invoiceID";
			
			Map namedParameters = new HashMap();
			namedParameters.put("invoiceID", invoiceID);
			
			insertFlag = namedParameterJdbcTemplate.update(sql,namedParameters);
			
		}catch (Exception e) {
			logger.error("Exception occurred at deleting invoices "+e);
		}
		
		
		logger.debug("Exit : public int deleteInvoice(int invoiceID,int apartmentID)");
		return insertFlag;
	}
	
	 Get list of all applicable fees for the given member 
	public List getApplicableCharges(InvoiceDetailsVO invoiceVO) {
		logger.debug("Entry : public List getApplicableChargesToMember(int societyID,int memberID)");
		List memberChargeList = null;
		String strCondition="";
		if(invoiceVO.getInvoiceTypeID()!=0){
			strCondition="And i.id=:invoiceTypeID";
		}
		InvoiceMemberChargesVO memberVO=invoiceVO.getMemberVO();
		
		
		
		try {
			String sql = "SELECT m.*,i.invoice_type as invoiceType,s.precision "
					+ " FROM in_member_charge_details m,in_invoice_type_details i ,society_settings s WHERE m.invoice_type_id=i.id  AND m.society_id=s.society_id AND (m.society_id=:societyID )" +
							" AND m.status='Active'" +
							" AND ((next_charge_date BETWEEN :fromDate AND :uptoDate) ) "+strCondition+"  ORDER BY payment_priority; ";
			logger.debug(sql+"  "+memberVO.getSocietyID()+" "+memberVO.getAptID()+" "+memberVO.getFromDate()+" "+memberVO.getUptoDate()+" "+invoiceVO.getInvoiceTypeID());
			Map hashMap = new HashMap();

			hashMap.put("societyID", memberVO.getSocietyID());
			hashMap.put("apartmentID", memberVO.getAptID());
			hashMap.put("fromDate", memberVO.getFromDate());
			hashMap.put("uptoDate", memberVO.getUptoDate());
			hashMap.put("invoiceTypeID", invoiceVO.getInvoiceTypeID());
			
			

			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					InvoiceMemberChargesVO inMemberVO = new InvoiceMemberChargesVO();
					inMemberVO.setChargeID(rs.getInt("id"));
					inMemberVO.setCalculationMethod(rs.getString("cal_method"));
					inMemberVO.setEntityID(rs.getBigDecimal("entity_id"));
					inMemberVO.setLedgerID(rs.getInt("ledger_id"));
					inMemberVO.setTotalAmount(rs.getBigDecimal("amount"));
					inMemberVO.setCategoryName(rs.getString("charge_type"));
					inMemberVO.setPaymentPriority(rs.getInt("payment_priority"));
					inMemberVO.setFromDate(rs.getString("from_date"));
					inMemberVO.setUptoDate(rs.getString("to_date"));
					inMemberVO.setInvoiceType(rs.getString("invoiceType"));
					inMemberVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
					inMemberVO.setDescription(rs.getString("description"));
					inMemberVO.setNextRunDate(rs.getString("next_charge_date"));
					inMemberVO.setFrequency(rs.getString("charge_frequency"));
					inMemberVO.setLateFeeType(rs.getString("late_fee_type"));
					inMemberVO.setLateFeeFor(rs.getInt("late_fee_for"));
					inMemberVO.setParams(rs.getString("param"));
					inMemberVO.setPrecison(rs.getInt("precision"));
					inMemberVO.setIsApplicable(true);
					return inMemberVO;
				}
			};
			memberChargeList = namedParameterJdbcTemplate.query(sql, hashMap,rMapper);

		} catch (Exception e) {
			logger.error("Exception at getApplicableChargesToMember " + e);
		}
		logger.debug("Exit : public List getApplicableChargesToMember(int societyID,int memberID)"+memberChargeList.size());
		return memberChargeList;
	}
	
	
	 Get list of all applicable fees for the given member 
	public List getChargList(int aptID,int societyID) {
		logger.debug("Entry : public List getChargList(int aptID,int societyID)");
		List memberChargeList = null;
	
		
		
		
		try {
			String sql = "SELECT m.*,i.invoice_type as invoiceType "
					+ " FROM in_member_charge_details m,in_invoice_type_details i WHERE m.invoice_type_id=i.id and  (m.society_id=:societyID )" +
							" AND m.status='Active'   ORDER BY id,payment_priority; ";
			logger.debug(sql+"  "+societyID+" "+aptID);
			Map hashMap = new HashMap();

			hashMap.put("societyID", societyID);
			hashMap.put("apartmentID",aptID);
			
			
			

			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					InvoiceMemberChargesVO inMemberVO = new InvoiceMemberChargesVO();
					inMemberVO.setChargeID(rs.getInt("id"));
					inMemberVO.setCalculationMethod(rs.getString("cal_method"));
					inMemberVO.setEntityID(rs.getBigDecimal("entity_id"));
					inMemberVO.setLedgerID(rs.getInt("ledger_id"));
					inMemberVO.setTotalAmount(rs.getBigDecimal("amount"));
					inMemberVO.setCategoryName(rs.getString("charge_type"));
					inMemberVO.setPaymentPriority(rs.getInt("payment_priority"));
					inMemberVO.setFromDate(rs.getString("from_date"));
					inMemberVO.setUptoDate(rs.getString("to_date"));
					inMemberVO.setInvoiceType(rs.getString("invoiceType"));
					inMemberVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
					inMemberVO.setDescription(rs.getString("description"));
					inMemberVO.setNextRunDate(rs.getString("next_charge_date"));
					inMemberVO.setFrequency(rs.getString("charge_frequency"));
					inMemberVO.setLateFeeType(rs.getString("late_fee_type"));
					inMemberVO.setLateFeeFor(rs.getInt("late_fee_for"));
					inMemberVO.setParams(rs.getString("param"));
					inMemberVO.setIsApplicable(true);
					return inMemberVO;
				}
			};
			memberChargeList = namedParameterJdbcTemplate.query(sql, hashMap,rMapper);

		} catch (Exception e) {
			logger.error("Exception at public List getChargList(int aptID,int societyID) for apartmentID "+aptID+" and societyID "+societyID+e);
		}
		logger.debug("Exit : public List public List getChargList(int aptID,int societyID)"+memberChargeList.size());
		return memberChargeList;
	}
	
	
	 Add new charge to a member  
	public int insertMemberCharge(InvoiceMemberChargesVO chargeVO,String societyID){
		int insertFlag=0;
		DateUtility dateUtly=new DateUtility();
		logger.debug("Entry : public int insertMemberCharge(InvoiceLineItemsVO inLIVO,String societyID)");
		try{
		String sql="INSERT INTO in_member_charge_details(society_id,entity_id,cal_method,ledger_id,charge_type,amount,from_date,to_date,added_date,update_date,status,description,charge_frequency,next_charge_date,invoice_type_id,late_fee_type,late_fee_for) " +
		" values (:societyID,:entityID,:calMethod,:ledgerID,:chargeType,:amount,:fromDate,:uptoDate,:createDate,:updateDate,:status,:description,:chargeFrequency,:nextRunDate,:invoiceTypeID,:lateFeeType,:lateFeeFor)";
		logger.debug("query : " + sql);

		Map hashMap=new HashMap();
		hashMap.put("societyID", societyID);
		hashMap.put("entityID", chargeVO.getEntityID());
		hashMap.put("calMethod", chargeVO.getCalculationMethod());
		hashMap.put("ledgerID", chargeVO.getLedgerID());
		hashMap.put("chargeType", chargeVO.getCategoryName());
		hashMap.put("amount", chargeVO.getTotalAmount());
		hashMap.put("fromDate", dateUtly.findCurrentDate());
		hashMap.put("uptoDate", chargeVO.getUptoDate());
		hashMap.put("createDate", dateUtly.findCurrentDate());
		hashMap.put("updateDate", dateUtly.findCurrentDate());
		hashMap.put("status", chargeVO.getStatus());
		hashMap.put("invoiceType", chargeVO.getInvoiceType());
		hashMap.put("chargeFrequency", chargeVO.getFrequency());
		hashMap.put("description", chargeVO.getDescription());
		hashMap.put("nextRunDate", chargeVO.getNextRunDate());
		hashMap.put("invoiceTypeID", chargeVO.getInvoiceTypeID()); 
		hashMap.put("lateFeeType", chargeVO.getLateFeeType());
		hashMap.put("lateFeeFor", chargeVO.getLateFeeFor());
		
		

			

		insertFlag=namedParameterJdbcTemplate.update(sql,hashMap);
		}catch (Exception e) {
			logger.error("Exception : Occured while inserting Charges "+e);
		}
		logger.debug("Exit : public int insertMemberCharge(InvoiceLineItemsVO inLIVO,String societyID)");
		return insertFlag;		
	}
	
	 Delete a charge of member  
	public int deleteMemberCharge(InvoiceMemberChargesVO chargeVO){
		int insertFlag=0;
		logger.debug("Entry : public int deleteMemberCharge(InvoiceLineItemsVO inLIVO)");
		try{
			String sqlQuery="Delete from in_member_charge_details where id=:id and amount=:amount ;";
			Map hashMap=new HashMap();
			hashMap.put("id", chargeVO.getChargeID());
			hashMap.put("amount", chargeVO.getTotalAmount());
			
			
			
			insertFlag=namedParameterJdbcTemplate.update(sqlQuery, hashMap);
		}
		catch (Exception e) {
			logger.error("Exception : public int deleteMemberCarge "+e);
		}
		
		logger.debug("Exit : public int deleteMemberCharge(InvoiceLineItemsVO inLIVO)");
		return insertFlag;		
	}
	
	 Update invoice balance with the transaction 
	public int updateInvoiceBalance(int societyID ,InvoiceDetailsVO invoiceVO){
		int success=0;
		logger.debug("Entry : public int updateInvoiceBalance(String societyID ,TransactionVO txVO)");
		
		try {
			String sqlQuery="Update in_invoice_"+societyID+" set balance=:balance,current_balance=(current_balance - :currentBalance), status_id=:status where id=:invoiceID ";
			logger.debug("Query is "+sqlQuery);
			Map hmap=new HashMap();
			hmap.put("balance", invoiceVO.getBalance());
			hmap.put("status", invoiceVO.getStatusID());
			hmap.put("invoiceID", invoiceVO.getInvoiceID());
			hmap.put("currentBalance", invoiceVO.getCurrentBalance());
			
			success=namedParameterJdbcTemplate.update(sqlQuery, hmap);
			
		} catch (DataAccessException e) {
			logger.error("Exception at updateInvoieBalance "+e);
		}
		
		logger.debug("Exit : public int updateInvoiceBalance(String societyID ,TransactionVO txVO)");
		return success;		
	}
	*/

	
	/* Update invoice balance with the transaction */
	public int insertReceiptInvoice(int societyID ,InvoiceDetailsVO invoiceVO,int receiptID){
		int success=0;
		logger.debug("Entry : public int insertReceiptInvoice(String societyID ,InvoiceDetailsVO invoiceVO,int reciptID)"+invoiceVO.getBalance());
		
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			String sqlQuery="INSERT INTO `receipt_invoice_mapping_"+societyVO.getDataZoneID()+"`(`receipt_id`,`invoice_id`,`status_id`,`amount`,`current_balance`,`org_id`)" +
							" VALUES ( :receiptID,:invoiceID,:statusID,:balance,:currentBalance,:societyID )"; 
		logger.debug("query : " + sqlQuery);
			Map hmap=new HashMap();
			hmap.put("balance", invoiceVO.getBalance());
			hmap.put("receiptID", receiptID);
			hmap.put("invoiceID", invoiceVO.getInvoiceID());
			hmap.put("statusID", invoiceVO.getStatusID());
			hmap.put("currentBalance", invoiceVO.getCurrentBalance());
			hmap.put("societyID", societyID);
			
			success=namedParameterJdbcTemplate.update(sqlQuery, hmap);
			
		} catch (DataAccessException e) {
			logger.error("Exception at insertReceiptInvoice(String societyID ,InvoiceDetailsVO invoiceVO,int reciptID) "+e);
		}
		
		logger.debug("Exit : public int insertReceiptInvoice(String societyID ,InvoiceDetailsVO invoiceVO,int reciptID)");
		return success;		
	}
	
	/* Update invoice balance with the transaction */
	public int insertReceiptAgainstInvoice(int societyID ,int billID, int invoiceID,int receiptID,BigDecimal amount){
		int success=0;
		logger.debug("Entry : public int insertReceiptAgainstInvoice(String societyID ,InvoiceDetailsVO invoiceVO,int reciptID)");
		
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			String sqlQuery="INSERT INTO `receipt_invoice_mapping_"+societyVO.getDataZoneID()+"`(`receipt_id`,`bill_id`,`invoice_id`,`amount`,`org_id`)" +
							" VALUES ( :receiptID,:billID,:invoiceID,:balance,:orgID )"; 
		logger.debug("query : " + sqlQuery);
			Map hmap=new HashMap();
			
			hmap.put("receiptID", receiptID);
			hmap.put("billID",billID);
			hmap.put("invoiceID",invoiceID);
			hmap.put("balance", amount);			
			hmap.put("orgID", societyID);
			
			success=namedParameterJdbcTemplate.update(sqlQuery, hmap);
			
		} catch (DataAccessException e) {
			logger.error("Exception at insertReceiptAgainstInvoice(String societyID ,InvoiceDetailsVO invoiceVO,int reciptID) "+e);
		}
		
		logger.debug("Exit : public int insertReceiptAgainstInvoice(String societyID ,InvoiceDetailsVO invoiceVO,int reciptID)");
		return success;		
	}
	
	 /*Get Receipt Invoice Details 
	public List getReceiptInvoiceDetailsList(String societyID ,int receiptID){
		List invoiceList=new ArrayList();
		logger.debug("Entry : public List getReceiptInvoiceDetailsList(String societyID ,int receiptID)");
		
		try {
			String sqlQuery="select ii.total_amount,i.* from receipt_invoice_mapping_"+societyID+" i,in_invoice_"+societyID+" ii where receipt_id=:receiptID AND i.invoice_id=ii.id order by id " ; 
		logger.debug("query : " + sqlQuery);
			Map hmap=new HashMap();
			
			hmap.put("receiptID", receiptID);
			
			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
				InvoiceDetailsVO invoiceVO=new InvoiceDetailsVO();
				invoiceVO.setInvoiceID(rs.getString("invoice_id"));
				invoiceVO.setStatusID(rs.getInt("status_id"));
				invoiceVO.setBalance(rs.getBigDecimal("amount"));
				invoiceVO.setCurrentBalance(rs.getBigDecimal("current_balance"));
				invoiceVO.setTotalAmount(rs.getBigDecimal("total_amount"));
				return invoiceVO;
				}
			};
			
			invoiceList=namedParameterJdbcTemplate.query(sqlQuery, hmap, rMapper);
			
		} catch (DataAccessException e) {
			logger.error("Exception at List getReceiptInvoiceDetailsList(String societyID ,int receiptID) "+e);
		}
		
		logger.debug("Exit : public List getReceiptInvoiceDetailsList(String societyID ,int receiptID)");
		return invoiceList;		
	}
	
	
	 Update invoice reverse balance when we cancel the transaction 
	public int updateInvoiceBalanceAfterCancel(String societyID ,InvoiceDetailsVO invoiceVO){
		int success=0;
		String strCondition="";
		if(invoiceVO.getTotalAmount().intValue()>0){
			strCondition=" current_balance=(current_balance + :currentBalance), status_id='0' ";
		}else{
			strCondition=" current_balance=(0), status_id='2' ";
		}
		logger.debug("Entry : public int updateInvoiceBalanceAfterCancel(String societyID ,InvoiceDetailsVO invoiceVO)"+invoiceVO.getTotalAmount());
		
		try {
			String sqlQuery="Update in_invoice_"+societyID+" set balance=(balance + :balance),"+strCondition+" where id=:invoiceID ";
			logger.debug("Query is "+sqlQuery);
			Map hmap=new HashMap();
			hmap.put("balance", invoiceVO.getBalance());
			hmap.put("currentBalance", invoiceVO.getCurrentBalance());
			
			hmap.put("invoiceID", invoiceVO.getInvoiceID());
			
			success=namedParameterJdbcTemplate.update(sqlQuery, hmap);
			
		} catch (DataAccessException e) {
			logger.error("Exception at updateInvoieBalance "+e);
		}
		
		logger.debug("Exit : public int updateInvoiceBalanceAfterCancel(String societyID ,InvoiceDetailsVO invoiceVO)"+success);
		return success;		
	}*/
	
	
	
	
	/* Get invoice type list */
	public List getInvoiceTypeList(String type){
		List invoiceList=null;
		logger.debug("Entry : public List getInvoiceTypeList()");
		
		String sqlQuery="Select * from in_invoice_type_details where gst_category LIKE '%"+type+"%' order by id";
		
		Map hmap=new HashMap();
		
		RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
			InvoiceDetailsVO invoiceVO=new InvoiceDetailsVO();
			invoiceVO.setInvoiceTypeID(rs.getInt("id"));
			invoiceVO.setInvoiceType(rs.getString("invoice_type"));
			invoiceVO.setInvoiceTypeCategory(rs.getString("invoice_category"));
			invoiceVO.setGstType(rs.getString("gst_category"));
			return invoiceVO;
			}
		};
		
		invoiceList=namedParameterJdbcTemplate.query(sqlQuery, hmap, rMapper);
		
		
		
		logger.debug("Exit : public List getInvoiceTypeList()");
		return invoiceList;		
	}
	
/*	public InvoiceDetailsVO getInvoiceBalance(InvoiceDetailsVO invoiceVO,int societyID,int typeID){
		BigDecimal balance=BigDecimal.ZERO;
		InvoiceDetailsVO invoicVO=new InvoiceDetailsVO();
		String strCondition="";
		if(typeID!=0){
			strCondition=" And invoice_type_id=:invoiceTypeID";
					
		}
		
		logger.debug("Entry : public getInvoiceBalance(InvoiceDetailsVO invoiceVO)"+invoiceVO.getInvoiceTypeID()+" "+invoiceVO.getAptID()+" "+invoiceVO.getInvoiceDate());
		
		
		String strQuery="Select  COALESCE(SUM(balance), 0) as sum,id,balance from in_invoice_"+societyID+" where apt_id=:aptID and invoice_date<=:invoiceDate "+strCondition+" AND is_deleted=0 ORDER BY id DESC LIMIT 1 ";
		//String strQuery="Select  * from in_invoice_"+societyID+" where apt_id=:aptID and invoice_date<=:invoiceDate "+strCondition+" ORDER BY id DESC LIMIT 1 ";
		logger.debug("Here "+strQuery);
		try {
			Map hmap=new HashMap();
			hmap.put("invoiceTypeID", invoiceVO.getInvoiceTypeID());
			hmap.put("aptID", invoiceVO.getAptID());
			hmap.put("invoiceDate", invoiceVO.getInvoiceDate());
			
			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
				InvoiceDetailsVO invoiceVO=new InvoiceDetailsVO();
				invoiceVO.setBalance(rs.getBigDecimal("sum"));
				invoiceVO.setCurrentBalance(rs.getBigDecimal("balance"));
				invoiceVO.setInvoiceID(rs.getString("id"));
				if(invoiceVO.getInvoiceID().equalsIgnoreCase("0")){
					invoiceVO.setStatus("N");
				}else
				
				invoiceVO.setStatus("A");
				return invoiceVO;
				}
			};
			
			
			invoicVO=(InvoiceDetailsVO) namedParameterJdbcTemplate.queryForObject(strQuery, hmap, rMapper);
		}catch (EmptyResultDataAccessException e) {
			logger.debug("No previous invoice found for this invoiceType "+e);
			invoicVO.setBalance(BigDecimal.ZERO);
			invoicVO.setCurrentBalance(BigDecimal.ZERO);
			invoicVO.setStatus("N");
		
		} catch (Exception e) {
			logger.error("Exception : getInvoiceBalance "+e);
		}
		
		logger.debug("Exit : public getInvoiceBalance(InvoiceDetailsVO invoiceVO)"+invoicVO.getBalance());
		return invoicVO;		
	}
	public InvoiceDetailsVO getInvoiceBalanceForLateFees(InvoiceDetailsVO invoiceVO,int societyID,int typeID,int lateFeeFor){
		BigDecimal balance=BigDecimal.ZERO;
		InvoiceDetailsVO invoicVO=new InvoiceDetailsVO();
		String strCondition="";
		if(typeID!=0){
			strCondition=" And invoice_type_id=:invoiceTypeID";
					
		}
		if(invoiceVO.getInvoiceTypeID()==10){
			
			strCondition=" and invoice_type_id="+lateFeeFor+"";
		}
		
		logger.debug("Entry : public getInvoiceBalance(InvoiceDetailsVO invoiceVO)"+invoiceVO.getInvoiceTypeID()+" "+invoiceVO.getAptID()+" "+invoiceVO.getInvoiceDate());
		
		
		String strQuery="Select  COALESCE(SUM(balance), 0) as sum,id,balance from in_invoice_"+societyID+" where apt_id=:aptID and invoice_date<=:invoiceDate "+strCondition+" AND is_deleted=0 ORDER BY id DESC LIMIT 1 ";
		//String strQuery="Select  * from in_invoice_"+societyID+" where apt_id=:aptID and invoice_date<=:invoiceDate "+strCondition+" ORDER BY id DESC LIMIT 1 ";
		logger.debug("Here "+strQuery);
		try {
			Map hmap=new HashMap();
			hmap.put("invoiceTypeID", invoiceVO.getInvoiceTypeID());
			hmap.put("aptID", invoiceVO.getAptID());
			hmap.put("invoiceDate", invoiceVO.getInvoiceDate());
			
			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
				InvoiceDetailsVO invoiceVO=new InvoiceDetailsVO();
				invoiceVO.setBalance(rs.getBigDecimal("sum"));
				invoiceVO.setCurrentBalance(rs.getBigDecimal("balance"));
				invoiceVO.setInvoiceID(rs.getString("id"));
				if(invoiceVO.getInvoiceID().equalsIgnoreCase("0")){
					invoiceVO.setStatus("N");
				}else
				
				invoiceVO.setStatus("A");
				return invoiceVO;
				}
			};
			
			
			invoicVO=(InvoiceDetailsVO) namedParameterJdbcTemplate.queryForObject(strQuery, hmap, rMapper);
		}catch (EmptyResultDataAccessException e) {
			logger.debug("No previous invoice found for this invoiceType "+e);
			invoicVO.setBalance(BigDecimal.ZERO);
			invoicVO.setCurrentBalance(BigDecimal.ZERO);
			invoicVO.setStatus("N");
		
		} catch (Exception e) {
			logger.error("Exception : getInvoiceBalance "+e);
		}
		
		logger.debug("Exit : public getInvoiceBalance(InvoiceDetailsVO invoiceVO)"+invoicVO.getBalance());
		return invoicVO;		
	}
	
	public InvoiceDetailsVO getUnpaidInvoiceBalance(InvoiceDetailsVO invoiceVO,String societyID,int typeID){
		BigDecimal balance=BigDecimal.ZERO;
		InvoiceDetailsVO invoicVO=new InvoiceDetailsVO();
		String strCondition="";
		if(typeID!=0){
			strCondition=" And invoice_type_id=:invoiceTypeID";
					
		}
		logger.debug("Entry : public getUnpaidInvoiceBalance(InvoiceDetailsVO invoiceVO)"+invoiceVO.getInvoiceTypeID()+" "+invoiceVO.getAptID()+" "+invoiceVO.getInvoiceDate());
		
		
		String strQuery="SELECT  COALESCE(SUM(balance), 0) AS SUM,id FROM in_invoice_"+societyID+" WHERE apt_id=:aptID AND invoice_date<=:invoiceDate "+strCondition+" AND (status_id=0 OR status_id=1) AND is_deleted=0 ORDER BY id DESC LIMIT 1 ";
		//String strQuery="Select  * from in_invoice_"+societyID+" where apt_id=:aptID and invoice_date<=:invoiceDate "+strCondition+" ORDER BY id DESC LIMIT 1 ";
		logger.debug("Here "+strQuery);
		try {
			Map hmap=new HashMap();
			hmap.put("invoiceTypeID", invoiceVO.getInvoiceTypeID());
			hmap.put("aptID", invoiceVO.getAptID());
			hmap.put("invoiceDate", invoiceVO.getInvoiceDate());
			
			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
				InvoiceDetailsVO invoiceVO=new InvoiceDetailsVO();
				invoiceVO.setBalance(rs.getBigDecimal("sum"));
				invoiceVO.setInvoiceID(rs.getString("id"));
				if(invoiceVO.getInvoiceID().equalsIgnoreCase("0")){
					invoiceVO.setStatus("N");
				}else
				
				invoiceVO.setStatus("A");
				return invoiceVO;
				}
			};
			
			
			invoicVO=(InvoiceDetailsVO) namedParameterJdbcTemplate.queryForObject(strQuery, hmap, rMapper);
		}catch (EmptyResultDataAccessException e) {
			logger.debug("No previous invoice found for this invoiceType "+e);
			invoicVO.setBalance(BigDecimal.ZERO);
			invoicVO.setCurrentBalance(BigDecimal.ZERO);
			invoicVO.setStatus("N");
		
		} catch (Exception e) {
			logger.error("Exception : getUnpaidInvoiceBalance "+e);
		}
		
		logger.debug("Exit : public getUnpaidInvoiceBalance(InvoiceDetailsVO invoiceVO)"+invoicVO.getBalance());
		return invoicVO;		
	}
	
	public List getUnpaidInvoicesDetails(int societyID ,String buildingID,String groupType){
		List invoiceList=null;
		logger.debug("Entry : public List getUnpaidInvoicesDetails(String societyID ,String buildingID)");
			
		try{
			
    	String strWhereClause=null;
    	String groupByClause=null;
    	String orderByClause=null;

		if(groupType.equalsIgnoreCase("APT")){
			groupByClause=" invoice_type_id,apt_id ";
		    orderByClause=" seq_no,ins.id ";
		}else if(groupType.equalsIgnoreCase("BLDG")){
			groupByClause=" invoice_type_id,b.building_id ";
		    orderByClause=" b.building_id,invoice_type_id ";
		}else if(groupType.equalsIgnoreCase("INSTYP")){
			groupByClause=" invoice_type_id ";
		     orderByClause=" invoice_type_id ";
		}
    	
		if(buildingID.equalsIgnoreCase("0"))
			strWhereClause=" ";
		else
	    	strWhereClause="  AND a.building_id="+buildingID+"";
		
		String sql = "SELECT b.building_id,b.building_name,CONCAT(b.building_name,' ',a.apt_name,' ',m.title,' ',m.full_name) AS memberName,CAST(ins.create_date AS DATE) AS crDt, ins.*,itd.invoice_type as invoiceType,SUM(ins.current_balance)AS totalBalance "+
					 " FROM in_invoice_"+societyID+" ins,member_details m,apartment_details a, building_details b ,in_invoice_type_details itd"+
					 " WHERE a.apt_id=ins.apt_id AND ins.apt_id=m.apt_id AND a.building_id=b.building_id "+strWhereClause+" AND b.society_id=:societyID  "+ 
					 " AND m.type='P' AND m.is_current=1 and (status_id=0 or status_id=1)  and ins.invoice_type_id=itd.id GROUP BY "+groupByClause+" ORDER BY "+orderByClause+";";
	
		logger.debug("Query is "+sql +" "+buildingID);

	Map hashMap = new HashMap();

	hashMap.put("societyID",societyID);
	hashMap.put("buildingID", buildingID);
	
	RowMapper rMapper = new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvoiceDetailsVO inMemberVO = new InvoiceDetailsVO();
			
			inMemberVO.setAptID(rs.getInt("apt_id"));
			inMemberVO.setBuildingId(rs.getString("building_id"));
			inMemberVO.setBuildingName(rs.getString("building_name"));
			inMemberVO.setInvoiceID(rs.getString("id"));
			inMemberVO.setMemberName(rs.getString("memberName"));
			inMemberVO.setCreateDate(rs.getString("crDt"));
			inMemberVO.setTotalAmount(rs.getBigDecimal("total_amount"));
			inMemberVO.setFromDate(rs.getString("from_date"));
			inMemberVO.setUptoDate(rs.getString("to_date"));
			inMemberVO.setInvoiceDate(rs.getString("invoice_date"));
			inMemberVO.setCarriedBalance(rs.getBigDecimal("carried_balance"));
			inMemberVO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
			inMemberVO.setBillCycleID(rs.getInt("billing_process_id"));
			inMemberVO.setDueDate(rs.getString("due_date"));
			inMemberVO.setInvoiceType(rs.getString("invoiceType"));
			inMemberVO.setBalance(rs.getBigDecimal("totalBalance"));
			inMemberVO.setCurrentBalance(rs.getBigDecimal("current_balance"));
			inMemberVO.setStatusID(rs.getInt("status_id"));
			inMemberVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
			inMemberVO.setIsMailSent(rs.getInt("is_sent"));
			if(inMemberVO.getIsMailSent()==0){
				inMemberVO.setIsApplicable(true);
			}
			
			return inMemberVO;
		}
	};
		
		invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
		}catch(Exception e){
			logger.error("Exception in getUnpaidInvoicesDetails "+e);
		}
		logger.debug("Exit : public List getUnpaidInvoicesDetails(String societyID ,String buildingID)))"+invoiceList.size());
		return invoiceList;		
	}
	*/
	public InvoiceMemberChargesVO getInvoiceInfoDetails(int societyID ){
		InvoiceMemberChargesVO memberChargeVO=new InvoiceMemberChargesVO();
		logger.debug("Entry : public List getInvoiceInfoDetails(String societyID )");
		
		
		try {
			String sql = "Select * from in_billing_cycle_details s,society_settings ss where s.society_id=ss.society_id and  s.society_id=:societyID order by id desc limit 1";

			
			Map hashMap = new HashMap();

			hashMap.put("societyID",societyID);


			RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				InvoiceMemberChargesVO inMemberVO = new InvoiceMemberChargesVO();
				
				
				inMemberVO.setDueDate(rs.getString("due_date"));
				inMemberVO.setFrequency(rs.getString("invoice_period"));
				
				return inMemberVO;
			}
			};
			
			memberChargeVO=(InvoiceMemberChargesVO) namedParameterJdbcTemplate.queryForObject(sql, hashMap,rMapper);
		}catch(EmptyResultDataAccessException ex){
				logger.info("No billing cycle is available in societyID = "+societyID+"  "+ex);
			
		} catch (Exception e) {
			logger.error("Exception in getInvoiceInfoDetails for society ID = "+societyID+e);
		}
		
		logger.debug("Exit : public List getInvoiceInfoDetails(String societyID )");
		return memberChargeVO;		
	}
	
	/*public InvoiceMemberChargesVO getLateFeeChargeDetails(int societyID ){
		InvoiceMemberChargesVO memberChargeVO=new InvoiceMemberChargesVO();
		logger.debug("Entry : public List getInvoiceInfoDetails(String societyID )");
		
		
		try {
			String sql = "Select * from in_member_charge_details where society_id=:societyID and invoice_type_id=10 and status='active' order by id desc limit 1";

			
			Map hashMap = new HashMap();

			hashMap.put("societyID",societyID);


			RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				InvoiceMemberChargesVO inMemberVO = new InvoiceMemberChargesVO();
				
				inMemberVO.setSocietyID(rs.getInt("society_id"));
				inMemberVO.setFrequency(rs.getString("charge_frequency"));
				inMemberVO.setNextRunDate(rs.getString("next_charge_date"));
				inMemberVO.setTotalAmount(rs.getBigDecimal("amount"));
				inMemberVO.setCalculationMethod(rs.getString("cal_method"));
				return inMemberVO;
			}
			};
			
			memberChargeVO=(InvoiceMemberChargesVO) namedParameterJdbcTemplate.queryForObject(sql, hashMap,rMapper);
		}catch(EmptyResultDataAccessException ex){
			logger.info("No Late fee charge is available in societyID = "+societyID+"  "+ex);
		} catch (Exception e) {
			logger.error("Exception in getLateFeeChargeDetails "+e);
		}
		
		logger.debug("Exit : public List getInvoiceInfoDetails(String societyID )");
		return memberChargeVO;		
	}*/
	
	
	/* Get list of all bills of the given society for that particular billing cycle  */
	public List getBillsForSociety(final int societyID,final String fromDate,final String toDate,final int billingCycleID){
		List billList=null;
		logger.debug("Entry : public List getBillsForSociety(int societyID,String fromDate,String toDate)");
	
		try{
		
		
		SocietyVO societyVO=societyService.getSocietyDetails(societyID);
		String sql = " SELECT CONCAT(m.unit_name,' ',m.title,' ',m.full_name) AS memberName,m.apt_id,a.id AS ledgerId,IF(txTable.closing_balance IS NULL,0,txTable.closing_balance) AS closing_balance,txTable.int_closing_balance,(txTable.closing_balance-txTable.int_closing_balance) AS principle_balance,txTable.opening_balance,a.opening_balance AS openingBalWhenNoTx,a.sub_group_id AS group_id,a.ledger_name AS ledger_name,IF(sumCredit IS NULL,0,sumCredit) AS sumCredit,IF(sumDebit IS NULL,0,sumDebit) AS sumDebit FROM members_info m ,ledger_user_mapping_"+societyVO.getDataZoneID()+" l,account_ledgers_"+societyVO.getDataZoneID()+" a LEFT JOIN (SELECT closingBal.ledger_id AS ledger_id, closingBal.closing_balance AS closing_balance,closingBal.int_closing_balance,openingBal.opening_balance ,(closingBal.sumCredit-IF(openingBal.sumCredit IS NULL,0,openingBal.sumCredit)) AS sumCredit ,(closingBal.sumDebit-IF(openingBal.sumDebit IS NULL,0,openingBal.sumDebit)) AS sumDebit FROM(SELECT ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0," +
					 " SUM(ale.amount) + al.opening_balance) AS closing_balance,IF(SUM(ale.int_amount) IS NULL, 0, SUM(ale.int_amount) + al.int_balance) AS int_closing_balance, SUM(CASE WHEN ale.type = 'C' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumCredit,SUM(CASE WHEN ale.type = 'D' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumDebit,al.is_deleted FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale WHERE ats.tx_id=ale.tx_id  AND ats.tx_date <=:toDate AND ats.is_deleted=0 "+
					 " AND ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID AND ale.ledger_id = al.id AND sub_group_id=1  GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS closingBal LEFT JOIN (SELECT ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS opening_balance,SUM(CASE WHEN ale.type = 'C' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumCredit,SUM(CASE WHEN ale.type = 'D' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumDebit,al.is_deleted  "+
					 " FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale WHERE  ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID AND ats.tx_id=ale.tx_id AND ats.tx_date <:fromDate AND ats.is_deleted=0 AND ale.ledger_id = al.id AND sub_group_id=1  GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS openingBal ON closingBal.ledger_id=openingBal.ledger_id ) AS txTable ON a.id=txTable.ledger_id WHERE a.org_id=:societyID and a.sub_group_id=1 AND m.member_id=l.user_id and a.org_id=l.org_id AND a.id=l.ledger_id AND m.type='P' ;";
		logger.debug("Query is "+sql);

	Map hashMap = new HashMap();

	hashMap.put("societyID",societyID);
	hashMap.put("fromDate", fromDate);
	hashMap.put("toDate", toDate);
	

	RowMapper rMapper = new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			BillDetailsVO billVO=new BillDetailsVO();
			
			billVO.setAptID(rs.getInt("apt_id"));
			billVO.setBillingCycleID(billingCycleID);
			billVO.setLedgerID(rs.getInt("ledgerId"));
			billVO.setBillNo(billingCycleID+" - "+societyID+" - "+billVO.getLedgerID());			
			billVO.setMemberName(rs.getString("memberName"));			
			if(rs.getBigDecimal("opening_balance")==null){
				billVO.setOpeningBalance(rs.getBigDecimal("openingBalWhenNoTx"));
			}else billVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
			if(rs.getBigDecimal("closing_balance")==null){
				billVO.setClosingBalance(BigDecimal.ZERO);
			}else billVO.setClosingBalance(rs.getBigDecimal("closing_balance"));
			//logger.info(rs.getInt("ledgerId")+" "+rs.getBigDecimal("sumDebit")+" Here "+rs.getBigDecimal("opening_balance"));
			billVO.setTotalAmount(billVO.getOpeningBalance().add(rs.getBigDecimal("sumDebit").abs()));
			billVO.setChargedAmount(rs.getBigDecimal("sumDebit").abs());
			billVO.setPaidAmount(rs.getBigDecimal("sumCredit").abs());
			billVO.setIntBalance(rs.getBigDecimal("int_closing_balance"));
			billVO.setPrincipleBalance(rs.getBigDecimal("principle_balance"));
			billVO.setFromDate(fromDate);
			billVO.setUptoDate(toDate);
			return billVO;
		}
	};
		
		billList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
		}catch(Exception e){
			logger.error("Exception in public List getBillsForSociety(int societyID,String fromDate,String toDate) "+e);
		}
		logger.debug("Exit : public List getBillsForSociety(int societyID,String fromDate,String toDate)"+billList.size());
		return billList;		
	}
	
	
	/* Get single bill for a member for that particular billing cycle  */
	public BillDetailsVO getBillsForMember(final int societyID,final String fromDate,final String toDate,final int aptID){
		BillDetailsVO billVO=new BillDetailsVO();
		logger.debug("Entry : public List getBillsForMember(int societyID,String fromDate,String toDate)");
	
		try{
		
		
		SocietyVO societyVO=societyService.getSocietyDetails(societyID);
		String sql = " SELECT CONCAT(m.unit_name,' ',m.title,' ',m.full_name) AS memberName,m.apt_id,a.id AS ledgerId,txTable.closing_balance,txTable.opening_balance,a.opening_balance AS openingBalWhenNoTx,a.sub_group_id AS group_id,a.ledger_name AS ledger_name,IF(sumCredit IS NULL,0,sumCredit) AS sumCredit,IF(sumDebit IS NULL,0,sumDebit) AS sumDebit FROM members_info m ,ledger_user_mapping_"+societyVO.getDataZoneID()+" l,account_ledgers_"+societyVO.getDataZoneID()+" a LEFT JOIN (SELECT closingBal.ledger_id AS ledger_id, closingBal.closing_balance AS closing_balance,openingBal.opening_balance ,(closingBal.sumCredit-IF(openingBal.sumCredit IS NULL,0,openingBal.sumCredit)) AS sumCredit ,(closingBal.sumDebit-IF(openingBal.sumDebit IS NULL,0,openingBal.sumDebit)) AS sumDebit FROM(SELECT ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0," +
					 " SUM(ale.amount) + al.opening_balance) AS closing_balance,SUM(CASE WHEN ale.type = 'C' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumCredit,SUM(CASE WHEN ale.type = 'D' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumDebit,al.is_deleted FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale WHERE  ats.tx_id=ale.tx_id AND ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID  AND ats.tx_date <=:toDate AND ats.is_deleted=0 "+
					 " AND ale.ledger_id = al.id AND sub_group_id=1  GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS closingBal LEFT JOIN (SELECT ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS opening_balance,SUM(CASE WHEN ale.type = 'C' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumCredit,SUM(CASE WHEN ale.type = 'D' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumDebit,al.is_deleted  "+
					 " FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale WHERE ats.tx_id=ale.tx_id AND ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID   AND ats.tx_date <:fromDate AND ats.is_deleted=0 AND ale.ledger_id = al.id AND sub_group_id=1  GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS openingBal ON closingBal.ledger_id=openingBal.ledger_id ) AS txTable ON a.id=txTable.ledger_id WHERE m.apt_id=:aptID AND l.org_id=a.org_id and a.org_id=:societyID and m.member_id=l.user_id AND a.id=l.ledger_id AND m.type='P'  ;";
		//logger.debug("Query is "+sql);

	Map hashMap = new HashMap();

	hashMap.put("societyID",societyID);
	hashMap.put("fromDate", fromDate);
	hashMap.put("toDate", toDate);
	hashMap.put("aptID", aptID);
	

	RowMapper rMapper = new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			BillDetailsVO billVO=new BillDetailsVO();
			
			billVO.setAptID(rs.getInt("apt_id"));
		//	
			billVO.setMemberName(rs.getString("memberName"));
			billVO.setLedgerID(rs.getInt("ledgerId"));
			if(rs.getBigDecimal("opening_balance")==null){
				billVO.setOpeningBalance(rs.getBigDecimal("openingBalWhenNoTx"));
			}else
			
			billVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
			billVO.setClosingBalance(rs.getBigDecimal("closing_balance"));
			billVO.setTotalAmount(billVO.getOpeningBalance().add(rs.getBigDecimal("sumDebit").abs()));
			billVO.setChargedAmount(rs.getBigDecimal("sumDebit").abs());
			billVO.setPaidAmount(rs.getBigDecimal("sumCredit").abs());
			billVO.setFromDate(fromDate);
			billVO.setUptoDate(toDate);
			
			return billVO;
		}
	};
		
		billVO=(BillDetailsVO) namedParameterJdbcTemplate.queryForObject(sql, hashMap,rMapper);
		
		}catch(EmptyResultDataAccessException e){
			logger.info("No Data found for the given billing cycle "+fromDate+" to "+toDate+" for the APT id "+aptID+" and SocietyID  "+societyID);
		
		}catch(NullPointerException e){
			logger.info("No Data found for the given billing cycle "+fromDate+" to "+toDate+" for the APT id "+aptID+" and SocietyID  "+societyID);
		
		
		}catch(Exception e){
			logger.error("Exception in public List getBillsForMember(int societyID,String fromDate,String toDate) "+e);
		}
		logger.debug("Exit : public List getBillsForMember(int societyID,String fromDate,String toDate)"+billVO.getClosingBalance());
		return billVO;		
	}
	
	
	/* Get list of all bills of the given org for that particular billing cycle  */
	public List getBillsForOrg(final int orgID,final String fromDate,final String toDate,final int billingCycleID){
		List billList=null;
		logger.debug("Entry : public List getBillsForOrg(int societyID,String fromDate,String toDate)");
		
		try{
		final SocietyVO societyVO=societyService.getSocietyDetails(orgID);
		String sql = " SELECT a.id AS ledgerId,l.profile_type,l.email,IF(txTable.closing_balance IS NULL,0,txTable.closing_balance) AS closing_balance,txTable.opening_balance,a.opening_balance AS openingBalWhenNoTx,a.sub_group_id AS group_id,a.ledger_name AS ledger_name,IF(sumCredit IS NULL,0,sumCredit) AS sumCredit,IF(sumDebit IS NULL,0,sumDebit) AS sumDebit FROM account_ledger_profile_details l,account_groups g,account_ledgers_"+societyVO.getDataZoneID()+" a LEFT JOIN (SELECT closingBal.ledger_id AS ledger_id, closingBal.closing_balance AS closing_balance,openingBal.opening_balance ,(closingBal.sumCredit-IF(openingBal.sumCredit IS NULL,0,openingBal.sumCredit)) AS sumCredit ,(closingBal.sumDebit-IF(openingBal.sumDebit IS NULL,0,openingBal.sumDebit)) AS sumDebit FROM(SELECT ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0,"
				   + " SUM(ale.amount) + al.opening_balance) AS closing_balance,SUM(CASE WHEN ale.type = 'C' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumCredit,SUM(CASE WHEN ale.type = 'D' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumDebit,al.is_deleted FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_groups gg WHERE ats.tx_id=ale.tx_id  AND ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:orgID   AND ats.tx_date <=:toDate AND ats.is_deleted=0 "
				   + " AND ale.ledger_id = al.id AND sub_group_id=gg.id and gg.category_id=1  GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS closingBal LEFT JOIN (SELECT ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS opening_balance,SUM(CASE WHEN ale.type = 'C' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumCredit,SUM(CASE WHEN ale.type = 'D' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumDebit,al.is_deleted  "
				   + " FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_groups ggg WHERE ats.tx_id=ale.tx_id AND ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:orgID  AND  ats.tx_date <:fromDate AND ats.is_deleted=0 AND ale.ledger_id = al.id AND sub_group_id=ggg.id and ggg.category_id=1  GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS openingBal ON closingBal.ledger_id=openingBal.ledger_id ) AS txTable ON a.id=txTable.ledger_id WHERE a.org_id=l.org_id and a.sub_group_id=g.id and g.category_id=1 AND a.id=l.ledger_id  AND l.org_id=:orgID ;";
		logger.debug("Query is "+sql);

	Map hashMap = new HashMap();

	hashMap.put("orgID",orgID);
	hashMap.put("fromDate", fromDate);
	hashMap.put("toDate", toDate);
	

	RowMapper rMapper = new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			BillDetailsVO billVO=new BillDetailsVO();			
			BigDecimal closingBalanace=rs.getBigDecimal("closing_balance");
			BigDecimal openingBalanace=rs.getBigDecimal("opening_balance");
			BigDecimal sumDebit=rs.getBigDecimal("sumDebit");
			BigDecimal sumCredit=rs.getBigDecimal("sumCredit");
			billVO.setBillingCycleID(billingCycleID);
			billVO.setBillNo(billingCycleID+" - "+orgID+" - "+rs.getInt("ledgerId"));	
			billVO.setLedgerName(rs.getString("ledger_name"));
			billVO.setProfileType(rs.getString("profile_type"));
			billVO.setEmailID(rs.getString("email"));
			
			billVO.setLedgerID(rs.getInt("ledgerId"));
			if(openingBalanace==null){
				billVO.setOpeningBalance(rs.getBigDecimal("openingBalWhenNoTx"));
			}else billVO.setOpeningBalance(openingBalanace);
			
			if((openingBalanace==null)&&((sumDebit.compareTo(BigDecimal.ZERO)==0)&&(sumCredit.compareTo(BigDecimal.ZERO)==0))){
				billVO.setClosingBalance(rs.getBigDecimal("openingBalWhenNoTx"));
			}else billVO.setClosingBalance(closingBalanace);
			//logger.info(rs.getInt("ledgerId")+" "+rs.getBigDecimal("sumDebit")+" Here "+rs.getBigDecimal("opening_balance"));
			billVO.setTotalAmount(billVO.getOpeningBalance().add(sumDebit.abs()));
			billVO.setChargedAmount(sumDebit.abs());
			billVO.setPaidAmount(sumCredit.abs());
			billVO.setFromDate(fromDate);
			billVO.setUptoDate(toDate);
			
			return billVO;
		}
	};
		
		billList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
		}catch(Exception e){
			logger.error("Exception in public List getBillsForOrg(int societyID,String fromDate,String toDate) "+e);
		}
		logger.debug("Exit : public List getBillsForOrg(int societyID,String fromDate,String toDate)"+billList.size());
		return billList;		
	}
	
	
	/* Get single bill for a ledger for that particular billing cycle  */
	public BillDetailsVO getBillsForLedger( int orgID,final String fromDate,final String toDate,final int ledgerID){
		BillDetailsVO billVO=new BillDetailsVO();
		logger.debug("Entry : public List getBillsForLedger(int societyID,String fromDate,String toDate)");
	
		try{	
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String sql = " SELECT a.id AS ledgerId,l.profile_type,IF(txTable.closing_balance IS NULL,0,txTable.closing_balance) AS closing_balance,txTable.opening_balance,a.opening_balance AS openingBalWhenNoTx,a.sub_group_id AS group_id,a.ledger_name AS ledger_name,IF(sumCredit IS NULL,0,sumCredit) AS sumCredit,IF(sumDebit IS NULL,0,sumDebit) AS sumDebit FROM account_ledger_profile_details l,account_ledgers_"+societyVO.getDataZoneID()+" a LEFT JOIN (SELECT closingBal.ledger_id AS ledger_id, closingBal.closing_balance AS closing_balance,openingBal.opening_balance ,(closingBal.sumCredit-IF(openingBal.sumCredit IS NULL,0,openingBal.sumCredit)) AS sumCredit ,(closingBal.sumDebit-IF(openingBal.sumDebit IS NULL,0,openingBal.sumDebit)) AS sumDebit FROM(SELECT ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0,"
					   + " SUM(ale.amount) + al.opening_balance) AS closing_balance,SUM(CASE WHEN ale.type = 'C' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumCredit,SUM(CASE WHEN ale.type = 'D' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumDebit,al.is_deleted FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale WHERE ats.tx_id=ale.tx_id AND ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID   AND  ats.tx_date <=:toDate AND ats.is_deleted=0 "
					   + " AND ale.ledger_id = al.id AND sub_group_id=1  GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS closingBal LEFT JOIN (SELECT ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS opening_balance,SUM(CASE WHEN ale.type = 'C' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumCredit,SUM(CASE WHEN ale.type = 'D' THEN IF(ale.amount IS NULL ,0, ale.amount) ELSE 0 END) AS sumDebit,al.is_deleted  "
					   + " FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale WHERE ats.tx_id=ale.tx_id AND ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID  AND ats.tx_date <:fromDate AND ats.is_deleted=0 AND ale.ledger_id = al.id AND sub_group_id=1  GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS openingBal ON closingBal.ledger_id=openingBal.ledger_id ) AS txTable ON a.id=txTable.ledger_id WHERE a.sub_group_id=1 AND a.id=l.ledger_id and a.org_id=l.org_id   AND l.org_id=:orgID  and a.id=:ledgerID ";
		logger.debug("Query is "+sql);

	Map hashMap = new HashMap();

	hashMap.put("orgID",orgID);
	hashMap.put("fromDate", fromDate);
	hashMap.put("toDate", toDate);
	hashMap.put("ledgerID", ledgerID);
	

	RowMapper rMapper = new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			BillDetailsVO billVO=new BillDetailsVO();
			
			billVO.setLedgerName(rs.getString("ledger_name"));
			billVO.setLedgerID(rs.getInt("ledgerId"));
			billVO.setProfileType(rs.getString("profile_type"));
			if(rs.getBigDecimal("opening_balance")==null){
				billVO.setOpeningBalance(rs.getBigDecimal("openingBalWhenNoTx"));
			}else
			
			billVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
			billVO.setClosingBalance(rs.getBigDecimal("closing_balance"));
			billVO.setTotalAmount(billVO.getOpeningBalance().add(rs.getBigDecimal("sumDebit").abs()));
			billVO.setChargedAmount(rs.getBigDecimal("sumDebit").abs());
			billVO.setPaidAmount(rs.getBigDecimal("sumCredit").abs());
			billVO.setFromDate(fromDate);
			billVO.setUptoDate(toDate);
			
			return billVO;
		}
	};
		
		billVO=(BillDetailsVO) namedParameterJdbcTemplate.queryForObject(sql, hashMap,rMapper);
		
		}catch(EmptyResultDataAccessException e){
			logger.info("No Data found for the given billing cycle "+fromDate+" to "+toDate+" for the ledgerID "+ledgerID+" and OrgID:  "+orgID);
		
		}catch(NullPointerException e){
			logger.info("No Data found for the given billing cycle "+fromDate+" to "+toDate+" for the ledgerID "+ledgerID+" and OrgID:  "+orgID);
		
		
		}catch(Exception e){
			logger.error("Exception in public List getBillsForLedger(int OrgID,String fromDate,String toDate) "+e);
		}
		logger.debug("Exit : public List getBillsForLedger(int OrgID,String fromDate,String toDate)"+billVO.getClosingBalance());
		return billVO;		
	}
	
	/* Get list of billingCycle list  of society for given period */
	public List getBillingCycleListForGivenPeriod(int societyID,String fromDate,String toDate){
		logger.debug("Entry : public List getBillingCycleList(String societyID)");
		List billingCycleList=null;
		
		try {
			String strQuery = "SELECT * FROM in_billing_cycle_details WHERE society_id=:societyID AND from_date>=:fromDate AND to_date<=:toDate";
					

			Map nameparam = new HashMap();
			nameparam.put("societyID", societyID);
			nameparam.put("fromDate", fromDate);
			nameparam.put("toDate", toDate);
			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					InvoiceBillingCycleVO inBillingVO = new InvoiceBillingCycleVO();
					inBillingVO.setBillingCycleID(rs.getInt("id"));
					inBillingVO.setSocietyId(rs.getInt("society_id"));
					inBillingVO.setDescription(rs.getString("description"));
					inBillingVO.setFromDate(rs.getString("from_date"));
					inBillingVO.setUptoDate(rs.getString("to_date"));
					inBillingVO.setRunDate(rs.getString("rundate"));
					inBillingVO.setDueDate(rs.getString("due_date"));
					inBillingVO.setNextRunDate(rs.getString("next_run_date"));
					return inBillingVO;
				}
			};
			billingCycleList = namedParameterJdbcTemplate.query(strQuery, nameparam,Rmapper);
		} catch (Exception e) {
			logger.error("Exception at getBillingCycleList " + e);
		}
		
		logger.debug("Exit : public List getBillingCycleList(String societyID)");
		
		return billingCycleList;
	}
	
	
	/* Get list of billingCycle list  of society for given period */
	public List getBillingCycleListForMemberApp(int societyID){
		logger.debug("Entry : public List getBillingCycleListForMemberApp(String societyID)");
		List billingCycleList=null;
		
		try {
			String strQuery = "SELECT * FROM in_billing_cycle_details WHERE society_id=:societyID ORDER BY id DESC LIMIT 12;";
					

			Map nameparam = new HashMap();
			nameparam.put("societyID", societyID);
			
			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					InvoiceBillingCycleVO inBillingVO = new InvoiceBillingCycleVO();
					inBillingVO.setBillingCycleID(rs.getInt("id"));
					inBillingVO.setSocietyId(rs.getInt("society_id"));
					inBillingVO.setDescription(rs.getString("description"));
					inBillingVO.setFromDate(rs.getString("from_date"));
					inBillingVO.setUptoDate(rs.getString("to_date"));
					inBillingVO.setRunDate(rs.getString("rundate"));
					inBillingVO.setDueDate(rs.getString("due_date"));
					inBillingVO.setNextRunDate(rs.getString("next_run_date"));
					return inBillingVO;
				}
			};
			billingCycleList = namedParameterJdbcTemplate.query(strQuery, nameparam,Rmapper);
		} catch (Exception e) {
			logger.error("Exception at getBillingCycleListForMemberApp " + e);
		}
		
		logger.debug("Exit : public List getBillingCycleListForMemberApp(String societyID)");
		
		return billingCycleList;
	}
	

	
	/* Get list of all invoices of the given society for that particular period  */
	public List getInvoicesForSociety(final InvoiceDetailsVO invoiceVO){
		List invoiceList=null;
		String statusCondition="";
		String invoiceTypeCondition="";
		logger.debug("Entry : public List getInvoicesForSociety(final InvoiceDetailsVO invoiceVO)");
	
		try{
			
			if(invoiceVO.getStatusID()==10){
				statusCondition="";
			}else{
				statusCondition="And s.status_id="+invoiceVO.getStatusID()+"";
			}
			if(invoiceVO.getInvoiceTypeID()==0){
				invoiceTypeCondition="";
			}else
				invoiceTypeCondition="and s.invoice_type_id="+invoiceVO.getInvoiceTypeID()+"";
		
		
		SocietyVO societyVO=societyService.getSocietyDetails(invoiceVO.getSocietyID());
		String sql = " SELECT invTable.*,IFNULL((invTable.amount-(IFNULL(paidAmt,0))),0) AS balance FROM (SELECT l.ledger_name AS memberName,CAST(ins.create_date AS DATE) AS crDt,ins.invoice_id,ins.tx_date, ins.amount,s.*,itd.invoice_type AS invoiceType "+ 
					 " FROM account_transactions_"+societyVO.getDataZoneID()+" ins,account_ledgers_"+societyVO.getDataZoneID()+" l, in_invoice_type_details itd ,soc_tx_meta s "+
					 " WHERE l.id=s.ledger_id  "+statusCondition+" "+invoiceTypeCondition+" AND ins.org_id=l.org_id and l.org_id=:societyID  AND ins.tx_id=s.tx_id AND s.society_id=:societyID  AND itd.id=s.invoice_type_id   AND ins.is_deleted=0 AND ( ins.tx_date BETWEEN :fromDate AND :toDate ) ) AS invTable "+
					 " LEFT JOIN (SELECT SUM(r.amount) AS paidAmt,r.invoice_id,r.bill_id,receipt_id FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" r,account_transactions_"+societyVO.getDataZoneID()+" a WHERE a.org_id=r.org_id and r.org_id=:societyID and  r.receipt_id=a.tx_id AND a.is_deleted=0  GROUP BY bill_id ) AS l ON invTable.bill_id=l.bill_id ORDER BY invTable.bill_id ;";
					  	
		logger.debug("Query is "+sql);

	Map hashMap = new HashMap();

	hashMap.put("societyID",invoiceVO.getSocietyID());
	hashMap.put("fromDate", invoiceVO.getFromDate());
	hashMap.put("toDate", invoiceVO.getUptoDate());
	

	RowMapper rMapper = new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvoiceDetailsVO inMemberVO=new InvoiceDetailsVO();
				
			inMemberVO.setBillID(rs.getInt("bill_id"));
			inMemberVO.setInvoiceID(rs.getString("invoice_no"));
			inMemberVO.setMemberName(rs.getString("memberName"));
			inMemberVO.setCreateDate(rs.getString("crDt"));
			inMemberVO.setInvoiceDate(rs.getString("invoice_date"));
			inMemberVO.setTotalAmount(rs.getBigDecimal("amount"));
			inMemberVO.setFromDate(invoiceVO.getFromDate());
			inMemberVO.setUptoDate(invoiceVO.getUptoDate());
			inMemberVO.setBillCycleID(rs.getInt("billing_cycle_id"));
			inMemberVO.setInvoiceType(rs.getString("invoiceType"));
			inMemberVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
			inMemberVO.setBalance(rs.getBigDecimal("balance"));
			inMemberVO.setStatusID(rs.getInt("status_id"));
			
			return inMemberVO;
		}
	};
		
		invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
		}catch(Exception e){
			logger.error("Exception : public List getInvoicesForSociety(final InvoiceDetailsVO invoiceVO) "+e);
		}
		logger.debug("Exit : public List getInvoicesForSociety(final InvoiceDetailsVO invoiceVO)"+invoiceList.size());
		return invoiceList;		
	}
	
	
	/* Get list of all invoices of the given society for that particular period  */
	public List getGSTInvoicesListForBills(int orgID,String fromDate,String toDate,int ledgerID){
		List invoiceList=null;
		String statusCondition="";
		String invoiceTypeCondition="";
		logger.debug("Entry : public List getGSTInvoicesListForBills(int orgID,String fromDate,String toDate,int ledgerID)");
	
		try{
			
		SocietyVO societyVO=societyService.getSocietyDetails(orgID);
		String sql = " SELECT i.*,SUM(l.quantity) AS quantity,SUM(l.price) AS price,SUM(l.total_price) AS totalPrice,SUM(l.discount) AS discount,SUM(sgst) AS sgst,SUM(cgst) AS cgst,SUM(igst) AS igst,SUM(ugst) AS ugst,SUM(cess) AS cess,SUM(l.amount) AS amount,item_name,ledger_id,item_id,l.description,a.ledger_name,l.invoice_id,l.cess_percentage,l.gst_percentage,l.hsn_sac_code "
				   + " FROM invoice_details i,invoice_line_item_details l,account_ledgers_"+societyVO.getDataZoneID()+" a WHERE i.org_id=a.org_id and i.id=l.invoice_id AND i.entity_id=:ledgerID AND l.ledger_id=a.id "
				   + " AND ( (i.invoice_type='Sales') OR (i.invoice_type='Purchase') OR (i.invoice_type='Credit Note') OR (i.invoice_type='Debit Note') OR (i.invoice_type='Journal')) "
				   + " AND (invoice_date BETWEEN :fromDate AND :toDate) AND i.is_review=0 AND i.is_deleted=0 AND i.org_id=:orgID GROUP BY l.id;";
					  	
		logger.debug("Query is "+sql);

	Map hashMap = new HashMap();

	hashMap.put("orgID",orgID);
	hashMap.put("fromDate", fromDate);
	hashMap.put("toDate", toDate);
	hashMap.put("ledgerID", ledgerID);
	

	RowMapper rMapper = new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InLineItemsVO lineItemsVO=new InLineItemsVO();
			lineItemsVO.setInvoiceID(rs.getInt("invoice_id"));
			lineItemsVO.setInvoiceDate(rs.getString("invoice_date"));
			lineItemsVO.setItemID(rs.getString("item_id"));
			lineItemsVO.setInvoiceType(rs.getString("invoice_type"));
			lineItemsVO.setItemName(rs.getString("item_name"));
			lineItemsVO.setDescription(rs.getString("notes"));
			lineItemsVO.setNotes(rs.getString("description"));
			lineItemsVO.setLedgerID(rs.getInt("ledger_id"));
			lineItemsVO.setLedgerName(rs.getString("ledger_name"));
			lineItemsVO.setQuantity(rs.getBigDecimal("quantity"));
			lineItemsVO.setPerUnitPrice(rs.getBigDecimal("price"));
			lineItemsVO.setcGST(rs.getBigDecimal("cgst"));
			lineItemsVO.setiGST(rs.getBigDecimal("igst"));
			lineItemsVO.setsGST(rs.getBigDecimal("sgst"));
			lineItemsVO.setuGST(rs.getBigDecimal("ugst"));
			lineItemsVO.setDiscount(rs.getBigDecimal("discount"));
			lineItemsVO.setTotalPrice(rs.getBigDecimal("totalPrice"));
			lineItemsVO.setAmount(rs.getBigDecimal("amount"));
			lineItemsVO.setRoundOff(rs.getBigDecimal("roundup_amount"));				
			lineItemsVO.setCess(rs.getBigDecimal("cess"));
			lineItemsVO.setCessPercent(rs.getBigDecimal("cess_percentage"));
			lineItemsVO.setGstPercent(rs.getBigDecimal("gst_percentage"));	
			lineItemsVO.setGstAmount(rs.getBigDecimal("cgst").add(rs.getBigDecimal("sgst")).add(rs.getBigDecimal("ugst").add(rs.getBigDecimal("igst"))));
			lineItemsVO.setHsnSacCode(rs.getString("hsn_sac_code"));
			return lineItemsVO;
		}
	};
		
		invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
		}catch(Exception e){
			logger.error("Exception : public List getGSTInvoicesListForBills(int orgID,String fromDate,String toDate,int ledgerID) "+e);
		}
		logger.debug("Exit : public List getGSTInvoicesListForBills(int orgID,String fromDate,String toDate,int ledgerID)"+invoiceList.size());
		return invoiceList;		
	}
	
	/* Get list of all one time  invoices of the given society   */
	public List getOnetimeInvoicesForSociety(final InvoiceDetailsVO invoiceVO){
		List invoiceList=null;
		String statusCondition="";
		String invoiceTypeCondition="";
		logger.debug("Entry : public List getOnetimeInvoicesForSociety(final InvoiceDetailsVO invoiceVO)");
	
		try{
			
			if(invoiceVO.getStatusID()==10){
				statusCondition="";
			}else{
				statusCondition="And s.status_id="+invoiceVO.getStatusID()+"";
			}
			if(invoiceVO.getInvoiceTypeID()==0){
				invoiceTypeCondition="";
			}else
				invoiceTypeCondition="and s.invoice_type_id="+invoiceVO.getInvoiceTypeID()+"";
		
		
		SocietyVO societyVO=societyService.getSocietyDetails(invoiceVO.getSocietyID());
		String sql = " SELECT invTable.*,IFNULL((invTable.amount-(IFNULL(paidAmt,0))),0) AS balance FROM (SELECT l.ledger_name AS memberName,CAST(ins.create_date AS DATE) AS crDt,ins.invoice_id,ins.tx_date, ins.amount,s.*,itd.invoice_type AS invoiceType "+ 
					 " FROM account_transactions_"+societyVO.getDataZoneID()+" ins,account_ledgers_"+societyVO.getDataZoneID()+" l, in_invoice_type_details itd ,soc_tx_meta s "+
					 " WHERE l.id=s.ledger_id  "+statusCondition+" "+invoiceTypeCondition+" AND ins.org_id=l.org_id and ins.org_id=s.society_id  AND ins.tx_id=s.tx_id AND s.society_id=:societyID  AND itd.id=s.invoice_type_id   AND ins.is_deleted=0 ) AS invTable "+
					 " LEFT JOIN (SELECT SUM(r.amount) AS paidAmt,r.invoice_id,r.bill_id,receipt_id FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" r,account_transactions_"+societyVO.getDataZoneID()+" a WHERE r.org_id=a.org_id and a.org_id=:societyID and  r.receipt_id=a.tx_id AND a.is_deleted=0  GROUP BY bill_id ) AS l ON invTable.bill_id=l.bill_id ORDER BY invTable.bill_id ;";
					  	
		logger.debug("Query is "+sql);

	Map hashMap = new HashMap();

	hashMap.put("societyID",invoiceVO.getSocietyID());
	

	RowMapper rMapper = new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvoiceDetailsVO inMemberVO=new InvoiceDetailsVO();
				
			inMemberVO.setBillID(rs.getInt("bill_id"));
			inMemberVO.setInvoiceID(rs.getString("invoice_no"));
			inMemberVO.setMemberName(rs.getString("memberName"));
			inMemberVO.setCreateDate(rs.getString("crDt"));
			inMemberVO.setInvoiceDate(rs.getString("invoice_date"));
			inMemberVO.setTotalAmount(rs.getBigDecimal("amount"));
			inMemberVO.setFromDate(invoiceVO.getFromDate());
			inMemberVO.setUptoDate(invoiceVO.getUptoDate());
			inMemberVO.setBillCycleID(rs.getInt("billing_cycle_id"));
			inMemberVO.setInvoiceType(rs.getString("invoiceType"));
			inMemberVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
			inMemberVO.setBalance(rs.getBigDecimal("balance"));
			inMemberVO.setStatusID(rs.getInt("status_id"));
			
			return inMemberVO;
		}
	};
		
		invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
		}catch(Exception e){
			logger.error("Exception : public List getOnetimeInvoicesForSociety(final InvoiceDetailsVO invoiceVO) "+e);
		}
		logger.debug("Exit : public List getOnetimeInvoicesForSociety(final InvoiceDetailsVO invoiceVO)"+invoiceList.size());
		return invoiceList;		
	}
	
	
	/* Get invoices bill for a  for that particular billing cycle  */
	public List getInvoicesForLedger(final InvoiceDetailsVO invoiceVO){
		List invoiceList=null;
		String statusCondition="";
		String invoiceTypeCondition="";
		logger.debug("Entry : public List getInvoicesForLedger(final InvoiceDetailsVO invoiceVO)");
	
		try{
			
			if(invoiceVO.getStatusID()==10){
				statusCondition="";
			}else{
				statusCondition="And s.status_id="+invoiceVO.getStatusID()+"";
			}
			if(invoiceVO.getInvoiceTypeID()==0){
				invoiceTypeCondition="";
			}else
				invoiceTypeCondition="and s.invoice_type_id="+invoiceVO.getInvoiceTypeID()+"";
		
		
		SocietyVO societyVO=societyService.getSocietyDetails(invoiceVO.getSocietyID());
		String sql = " SELECT invTable.*,IFNULL((invTable.amount-(IFNULL(paidAmt,0))),0) AS balance FROM (SELECT l.ledger_name AS memberName,CAST(ins.create_date AS DATE) AS crDt,ins.invoice_id,ins.tx_date, ins.amount,s.*,itd.invoice_type AS invoiceType "+ 
					 " FROM account_transactions_"+societyVO.getDataZoneID()+" ins,account_ledgers_"+societyVO.getDataZoneID()+" l, in_invoice_type_details itd ,soc_tx_meta s "+
					 " WHERE l.id=s.ledger_id  "+statusCondition+" "+invoiceTypeCondition+" AND ins.org_id=l.org_id and ins.org_id=s.society_id  AND ins.tx_id=s.tx_id AND s.society_id=:societyID  AND itd.id=s.invoice_type_id AND s.ledger_id=:ledgerID  AND ins.is_deleted=0 AND ( ins.tx_date BETWEEN :fromDate AND :toDate ) ) AS invTable "+
					 " LEFT JOIN (SELECT SUM(r.amount) AS paidAmt,r.invoice_id,r.bill_id,receipt_id FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" r,account_transactions_"+societyVO.getDataZoneID()+" a WHERE r.org_id=a.org_id and a.org_id=:societyID and r.receipt_id=a.tx_id AND a.is_deleted=0  GROUP BY bill_id) AS l ON invTable.bill_id=l.bill_id ORDER BY invTable.bill_id ;";
					  	
		logger.debug("Query is "+sql);

	Map hashMap = new HashMap();

	hashMap.put("societyID",invoiceVO.getSocietyID());
	hashMap.put("fromDate", invoiceVO.getFromDate());
	hashMap.put("toDate", invoiceVO.getUptoDate());
	hashMap.put("ledgerID", invoiceVO.getLedgerID());
	

	RowMapper rMapper = new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			InvoiceDetailsVO inMemberVO=new InvoiceDetailsVO();
				
			inMemberVO.setBillID(rs.getInt("bill_id"));
			inMemberVO.setInvoiceID(rs.getString("invoice_no"));
			inMemberVO.setMemberName(rs.getString("memberName"));
			inMemberVO.setCreateDate(rs.getString("crDt"));
			inMemberVO.setInvoiceDate(rs.getString("invoice_date"));
			inMemberVO.setTotalAmount(rs.getBigDecimal("amount"));
			inMemberVO.setFromDate(invoiceVO.getFromDate());
			inMemberVO.setUptoDate(invoiceVO.getUptoDate());
			inMemberVO.setBillCycleID(rs.getInt("billing_cycle_id"));
			inMemberVO.setInvoiceType(rs.getString("invoiceType"));
			inMemberVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
			inMemberVO.setBalance(rs.getBigDecimal("balance"));
			inMemberVO.setStatusID(rs.getInt("status_id"));
			
			return inMemberVO;
		}
	};
		
		invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
		}catch(Exception e){
			logger.error("Exception in public List getInvoicesForLedger(final InvoiceDetailsVO invoiceVO) "+e);
		}
		logger.debug("Exit : public List getBillsForSociety(int societyID,String fromDate,String toDate)"+invoiceList.size());
		return invoiceList;		
	}
	
	//=== Invoicing for new myAccountant site      ==== //
		/* Insert the invoice */
		public int insertGSTInvoice(InvoiceVO invoiceVO) {
			int insertFlag = 0;
			int generatedID = 0;
			 java.util.Date today = new java.util.Date();
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			   invoiceVO.setCreatedTimestamp(currentTimestamp);
			   invoiceVO.setUpdatedTimestamp(currentTimestamp);
			try {

				logger.debug("Entry : public int insertInvoice(InvoiceVO invoiceVO)");
				String query = "INSERT INTO `invoice_details` (`invoice_number`,`doc_id`,`invoice_date`,`billing_cycle_id`,`entity_id`,`entity_type`,`from_date`,`to_date`,`due_date`,`invoice_amount`,`balance`,`notes`,`invoice_type`,`payment_status`,`delegated_invoice_id`,`paper_invoice_batch_id`, `currency_id`, `currency_rate`, `is_deleted`, `last_reminder_date`, `email_id`, `is_review`,`org_id`,"
						+ "`gstin`,`entity_gstin`,`place_of_supply_id`,`place_of_supply`,`total_discount`,`total_cgst`, `total_sgst`, `total_igst`,`total_ugst`,`total_cess`,`tds_section_no`,`tds_rate`,`tds_amount`,`total_amount`,`roundup_amount`,`create_date`,`update_date`,`in_process_payment`,`billing_address`,`billing_city`,`billing_state`,`billing_zip`,`shipping_address`,`shipping_city`,`shipping_state`,`shipping_zip`,`entity_address`,`entity_city`,`entity_state`,`entity_zip`,`created_by`,`updated_by`,`invoice_type_id`,`gst_invoice_type_id`,`gst_calculation_inclusive`,`is_sales`,`is_reverse`,`invoice_tags`,`port_code`,`shipping_bill_no`,`shipping_date`) "
						+ " VALUES ( :invoiceNumber,:docID,:invoiceDate,:billCycleID,:entityID,:entityType,:fromDate,:uptoDate,:dueDate,:invoiceAmount,:balance,:notes,:invoiceType,:paymentStatus,:delegatedInvoiceID,:paperInvoiceBatchID,:currencyID, :currencyRate,:isDeleted,:lastReminderDate,:emailID, :isReview,:orgID, "
						+ " :myGSTIN,:customerGSTIN,:supplyStateID,:supplyState,:totalDiscount,:totalCGST,:totalSGST,:totalIGST,:totalUGST,:totalCess,:tdsSectionNo,:tdsRate,:tds,:totalAmount,:totalRoundUpAmount,:createdTimestamp,:updatedTimestamp,:inProcessPayment,:billingAddress,:billingCity,:billingState,:billingZipCode,:shippingAddress,:shippingCity,:shippingState,:shippingZipCode,:entityAddress,:entityCity,:entityState,:entityZipCode,:createdBy,:updatedBy,:invoiceTypeID,:gstInvoiceTypeID,:isGstCalculationInclusive,:isSales,:isReverse,:invoiceTags,:portCode,:shippingBillNo,:shippingDate) ";

				
				SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(
						invoiceVO);
				KeyHolder keyHolder = new GeneratedKeyHolder();
				insertFlag = getNamedParameterJdbcTemplate().update(query,
						fileParameters, keyHolder);
				generatedID = keyHolder.getKey().intValue();
				logger.debug("key GEnereator ---" + generatedID);
			} catch (Exception e) {
				logger.error("Exception at insertInvoice " + e);
			}
			logger.debug("Exit :public int insertInvoice(InvoiceVO invoiceVO)");
			return generatedID;
		}

		/* Insert the invoice line items */
		public int insertInvoiceLineItems(final InvoiceVO invoiceVO) {
			int flag = 0;
			
			final List<InLineItemsVO> invoiceLineList = invoiceVO.getLineItemsList();
			  java.util.Date today = new java.util.Date();
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			 
			
			logger.debug("Entry : public int insertInvoiceLineItems(final InvoiceVO invoiceVO)");

			try {
				String str = "INSERT INTO invoice_line_item_details (invoice_id,item_id,hsn_sac_code,ledger_id,item_name,purchase_price,quantity,price,total_price,description,sgst,cgst,igst,ugst,cess,gst_category_id,gst_percentage,cess_percentage,unit_of_measurement,amount,discount,order_position,create_date,update_date,line_item_tags) "
						+ " VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ;";

				getJdbcTemplate().batchUpdate(str,new BatchPreparedStatementSetter() {

							public void setValues(PreparedStatement ps, int i)
									throws SQLException {
								InLineItemsVO imVO = invoiceLineList.get(i);
								ps.setInt(1, invoiceVO.getInvoiceID());
								ps.setString(2, imVO.getItemID());
								ps.setString(3, imVO.getHsnSacCode());
								ps.setInt(4, imVO.getLedgerID());
								ps.setString(5, imVO.getItemName());
								ps.setBigDecimal(6, imVO.getPurchasePrice());
								ps.setBigDecimal(7, imVO.getQuantity());
								ps.setBigDecimal(8, imVO.getPerUnitPrice());
								ps.setBigDecimal(9, imVO.getTotalPrice());
								ps.setString(10,imVO.getDescription());
								ps.setBigDecimal(11, imVO.getsGST());
								ps.setBigDecimal(12, imVO.getcGST());
								ps.setBigDecimal(13, imVO.getiGST());
								ps.setBigDecimal(14, imVO.getuGST());
								ps.setBigDecimal(15, imVO.getCess());
								ps.setInt(16, imVO.getGstCategoryID());
								ps.setBigDecimal(17, imVO.getGstPercent());
								ps.setBigDecimal(18, imVO.getCessPercent());
								ps.setString(19, imVO.getUnitOfMeasurement());
								ps.setBigDecimal(20, imVO.getAmount());
								ps.setBigDecimal(21, imVO.getDiscount());
								ps.setInt(22, imVO.getOrderPosition());
								ps.setString(23, currentTimestamp.toString());
								ps.setString(24, currentTimestamp.toString());
								ps.setString(25, imVO.getLineItemTags());	

							}

							public int getBatchSize() {
								return invoiceLineList.size();
							}

						});

			} catch (DataAccessException e) {
				flag = 0;
				logger.error("DataACCESSException in public int insertInvoiceLineItems(final InvoiceVO invoiceVO) : " + e);
				return flag;
			} catch (Exception ex) {
				flag = 0;
				logger.error("Exception in public int insertInvoiceLineItems(final InvoiceVO invoiceVO) : " + ex);
				return flag;
			}
			flag = 1;

			// return flag;

			logger.debug("Exit : public int insertInvoiceLineItems(final InvoiceVO invoiceVO)");
			return flag;
			
		}
		
		/* Get invoices for customer  */
		public List getInvoicesForCustomerLedger(InvoiceVO invoiceVO){
			List invoiceList=null;
			String statusCondition="";
			String invoiceTypeCondition="";
			String sql="";
			String invoiceCondition=" inv.invoice_date ";
			logger.debug("Entry : public List getInvoicesForCustomerLedger(InvoiceVO invoiceVO)");
		
			try{
				
				if(invoiceVO.getIsDueDate()==1){
					invoiceCondition=" inv.due_date ";
				}
				
			SocietyVO societyVO=societyService.getSocietyDetails(invoiceVO.getOrgID());	
			if(invoiceVO.getIsReview()==1)	{//draft		
			
			 sql = " SELECT l.ledger_name AS memberName,inv.*, IFNULL(inv.balance,0) AS pBalance,IFNULL(inv.balance,0) AS invBalance,m.full_name As creatorName,mi.full_name As updatorName,t.invoice_type AS chargeName ,g.invoice_type AS gstChargeName FROM account_ledgers_"+societyVO.getDataZoneID()+" l, invoice_details inv, um_users m, um_users mi, in_invoice_type_details t, in_invoice_type_details g "
	                    +" WHERE l.id=inv.entity_id and inv.org_id=l.org_id   AND inv.org_id=:orgID  AND inv.entity_id=:entityID AND inv.entity_type=:entityType AND inv.is_review!=0 AND inv.is_deleted=0 "
	                    +" AND ( "+invoiceCondition+" BETWEEN :fromDate AND :toDate ) and m.id=inv.created_by and mi.id=inv.updated_by AND inv.invoice_type_id=t.id AND inv.gst_invoice_type_id=g.id  ORDER BY inv.id  ;";
			
			}else if(invoiceVO.getIsReview()==0){ //Save 
				
			 sql =" SELECT invTable.*, IFNULL((ats.txBalanceAmount-(IFNULL(b.paidAmt,0))),0) AS pBalance , IFNULL((invTable.total_amount-(IFNULL(a.paidAmt,0))),0) AS invBalance   FROM (SELECT l.ledger_name AS memberName,inv.*,m.full_name As creatorName,mi.full_name As updatorName,t.invoice_type AS chargeName ,g.invoice_type AS gstChargeName FROM account_ledgers_"+societyVO.getDataZoneID()+" l, invoice_details inv, um_users m, um_users mi, in_invoice_type_details t ,in_invoice_type_details g " 
                      + " WHERE l.id=inv.entity_id   AND inv.org_id=l.org_id and  inv.org_id=:orgID  AND inv.entity_id=:entityID AND inv.entity_type=:entityType AND inv.is_review=0 AND inv.is_deleted=0 " 
                      + " AND ( "+invoiceCondition+" BETWEEN :fromDate AND :toDate )  and m.id=inv.created_by and mi.id=inv.updated_by AND inv.invoice_type_id=t.id AND inv.gst_invoice_type_id=g.id  ) AS invTable "
                      + " LEFT JOIN (SELECT IFNULL(amount,0) AS txBalanceAmount,IFNULL(invoice_id,0) AS invoiceID FROM account_transactions_"+societyVO.getDataZoneID()+" where org_id=:orgID )AS ats ON invoiceID=invTable.id  "
                      + " LEFT JOIN (SELECT SUM(r.amount) AS paidAmt,r.invoice_id,receipt_id FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" r,account_transactions_"+societyVO.getDataZoneID()+" a WHERE a.org_id=r.org_id and a.org_id=:orgID AND r.receipt_id=a.tx_id AND a.is_deleted=0  GROUP BY invoice_id ) AS b ON invTable.id=b.invoice_id "
                      + " LEFT JOIN (SELECT SUM(r.amount) AS paidAmt,r.invoice_id,r.linked_invoice_id FROM invoice_linking_details r WHERE r.org_id=:orgID GROUP BY r.invoice_id ) AS a ON invTable.id=a.invoice_id"
                      +  " ORDER BY invTable.id ";
			}
						  	
			logger.debug("Query is "+sql);

		Map hashMap = new HashMap();

		hashMap.put("orgID",invoiceVO.getOrgID());
		hashMap.put("fromDate", invoiceVO.getFromDate());
		hashMap.put("toDate", invoiceVO.getUptoDate());
		hashMap.put("entityType", invoiceVO.getEntityType());
		hashMap.put("entityID", invoiceVO.getEntityID());
		

		RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				InvoiceVO invoiceEntityVO=new InvoiceVO();
					
				invoiceEntityVO.setInvoiceID(rs.getInt("id"));
				invoiceEntityVO.setOrgID(rs.getInt("org_id"));
				invoiceEntityVO.setInvoiceNumber(rs.getString("invoice_number"));
				invoiceEntityVO.setDocID(rs.getString("doc_id"));
				invoiceEntityVO.setEntityName(rs.getString("memberName"));
				
				invoiceEntityVO.setInvoiceDate(rs.getString("invoice_date"));
				invoiceEntityVO.setDueDate(rs.getString("due_date"));
				invoiceEntityVO.setBillCycleID(rs.getInt("billing_cycle_id"));
				invoiceEntityVO.setInvoiceType(rs.getString("invoice_type"));
				invoiceEntityVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
				invoiceEntityVO.setChargeName(rs.getString("chargeName"));
				invoiceEntityVO.setCreatedTimestamp(rs.getTimestamp("create_date"));
				invoiceEntityVO.setUpdatedTimestamp(rs.getTimestamp("update_date"));
				invoiceEntityVO.setGstInvoiceTypeID(rs.getInt("gst_invoice_type_id"));
				invoiceEntityVO.setGstInvoiceType(rs.getString("gstChargeName"));
				
				invoiceEntityVO.setMyGSTIN(rs.getString("gstin"));
				invoiceEntityVO.setCustomerGSTIN(rs.getString("entity_gstin"));
				invoiceEntityVO.setSupplyState(rs.getString("place_of_supply"));
				invoiceEntityVO.setSupplyStateID(rs.getString("place_of_supply_id"));
				
				invoiceEntityVO.setEntityID(rs.getInt("entity_id"));
				invoiceEntityVO.setEntityType(rs.getString("entity_type"));			
				invoiceEntityVO.setEmailID(rs.getString("email_id"));
				invoiceEntityVO.setEntityAddress(rs.getString("entity_address"));
				invoiceEntityVO.setEntityCity(rs.getString("entity_city"));
				invoiceEntityVO.setEntityState(rs.getString("entity_state"));
				invoiceEntityVO.setEntityZipCode(rs.getString("entity_zip"));
				
				invoiceEntityVO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
				invoiceEntityVO.setTotalDiscount(rs.getBigDecimal("total_discount"));
				invoiceEntityVO.setTotalCGST(rs.getBigDecimal("total_cgst"));
				invoiceEntityVO.setTotalSGST(rs.getBigDecimal("total_sgst"));
				invoiceEntityVO.setTotalUGST(rs.getBigDecimal("total_ugst"));
				invoiceEntityVO.setTotalIGST(rs.getBigDecimal("total_igst"));
				invoiceEntityVO.setTotalCess(rs.getBigDecimal("total_cess"));
				invoiceEntityVO.setTdsRate(rs.getBigDecimal("tds_rate"));
				invoiceEntityVO.setTdsSectionNo(rs.getString("tds_section_no"));
				invoiceEntityVO.setTds(rs.getBigDecimal("tds_amount"));	
				invoiceEntityVO.setTotalAmount(rs.getBigDecimal("total_amount"));	
				invoiceEntityVO.setTotalRoundUpAmount(rs.getBigDecimal("roundup_amount"));
				if((rs.getString("invoice_type").equalsIgnoreCase("Proforma"))||(rs.getString("invoice_type").equalsIgnoreCase("Purchase Order"))){
					invoiceEntityVO.setBalance(rs.getBigDecimal("invBalance"));
					
				}else
				invoiceEntityVO.setBalance(rs.getBigDecimal("pBalance"));
				invoiceEntityVO.setTotalGST(invoiceEntityVO.getTotalCGST().add(invoiceEntityVO.getTotalSGST()).add(invoiceEntityVO.getTotalIGST()).add(invoiceEntityVO.getTotalUGST()));
				
				invoiceEntityVO.setBillingAddress(rs.getString("billing_address"));
				invoiceEntityVO.setBillingCity(rs.getString("billing_city"));
				invoiceEntityVO.setBillingState(rs.getString("billing_state"));
				invoiceEntityVO.setBillingZipCode(rs.getString("billing_zip"));
				
				invoiceEntityVO.setShippingAddress(rs.getString("shipping_address"));
				invoiceEntityVO.setShippingCity(rs.getString("shipping_city"));
				invoiceEntityVO.setShippingState(rs.getString("shipping_state"));
				invoiceEntityVO.setShippingZipCode(rs.getString("shipping_zip"));
				invoiceEntityVO.setNotes(rs.getString("notes"));
				invoiceEntityVO.setIsReview(rs.getInt("is_review"));
				invoiceEntityVO.setCreatedBy(rs.getInt("created_by"));
				invoiceEntityVO.setUpdatedBy(rs.getInt("updated_by"));
				invoiceEntityVO.setCreatorName(rs.getString("creatorName"));
				invoiceEntityVO.setUpdaterName(rs.getString("updatorName"));
				invoiceEntityVO.setIsSales(rs.getInt("is_sales"));
				invoiceEntityVO.setIsGstCalculationInclusive(rs.getInt("gst_calculation_inclusive"));
				invoiceEntityVO.setPortCode(rs.getString("port_code"));
				invoiceEntityVO.setShippingBillNo(rs.getString("shipping_bill_no"));
				invoiceEntityVO.setShippingDate(rs.getString("shipping_date"));
				invoiceEntityVO.setIsReverse(rs.getInt("is_reverse"));
				return invoiceEntityVO;
			}
		};
			
			invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
			
			}catch(Exception e){
				logger.error("Exception in getInvoicesForCustomerLedger "+e);
			}
			logger.debug("Exit : public List getInvoicesForCustomerLedger(InvoiceVO invoiceVO)"+invoiceList.size());
			return invoiceList;		
		}
		
		
		/* Get gst invoices for org   */
		public List getGSTInvoicesForOrg(InvoiceVO invoiceVO){
			List invoiceList=null;
			String statusCondition="";
			String invoiceTypeCondition="";
			if(invoiceVO.getInvoiceType()==null){
				invoiceTypeCondition=""; 
			}else if(invoiceVO.getInvoiceType().equalsIgnoreCase("S")){
				invoiceTypeCondition=" AND ( inv.invoice_type='Sales' or inv.invoice_type='Credit Note')";
			}else if(invoiceVO.getInvoiceType().equalsIgnoreCase("P")){
				invoiceTypeCondition=" AND ( inv.invoice_type='Purchase' or inv.invoice_type='Debit Note')";
			}else {
				invoiceTypeCondition=" AND inv.invoice_type='"+invoiceVO.getInvoiceType()+"'";
			}
			String sql="";
			logger.debug("Entry : public List getGSTInvoicesForOrg(InvoiceVO invoiceVO)");
		
			try{
		
			SocietyVO societyVO=societyService.getSocietyDetails(invoiceVO.getOrgID());
			 sql =" SELECT invTable.*, IFNULL((ats.txBalanceAmount-(IFNULL(b.paidAmt,0))),0) AS pBalance , IFNULL((invTable.total_amount-(IFNULL(a.paidAmt,0))),0) AS invBalance   FROM (SELECT l.ledger_name AS memberName,inv.*,m.full_name As creatorName,mi.full_name As updatorName,t.invoice_type AS chargeName,g.invoice_type AS gstChargeName,CONCAT(IFNULL(l.project_tags,''),il.line_item_tags) AS line_item_tags,il.gst_category  FROM account_ledgers_"+societyVO.getDataZoneID()+" l, invoice_details inv, um_users m, um_users mi, in_invoice_type_details t, in_invoice_type_details  g,"
			 		+ "(SELECT GROUP_CONCAT(IFNULL(ii.line_item_tags,'') SEPARATOR '') AS line_item_tags,ii.invoice_id,ii.gst_category_id,g.name AS gst_category FROM invoice_line_item_details ii,invoice_details i,gst_category_details g WHERE ii.gst_category_id=g.id AND i.id=ii.invoice_id AND i.org_id=:orgID AND i.is_deleted=0 AND ( i.invoice_date BETWEEN :fromDate AND :toDate ) GROUP BY i.id) AS il " 
                      + " WHERE l.id=inv.entity_id And inv.org_id=l.org_id AND inv.id=il.invoice_id   AND inv.org_id=:orgID "+invoiceTypeCondition+"  AND inv.is_review=:reviewType AND inv.is_deleted=0 " 
                      + " AND ( inv.invoice_date BETWEEN :fromDate AND :toDate ) and m.id=inv.created_by and mi.id=inv.updated_by AND inv.invoice_type_id=t.id AND inv.gst_invoice_type_id=g.id ) AS invTable "
                      + " LEFT JOIN (SELECT IFNULL(amount,0) AS txBalanceAmount,IFNULL(invoice_id,0) AS invoiceID FROM account_transactions_"+societyVO.getDataZoneID()+" where org_id=:orgID )AS ats ON invoiceID=invTable.id "
                      + " LEFT JOIN (SELECT SUM(r.amount) AS paidAmt,r.invoice_id,receipt_id FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" r,account_transactions_"+societyVO.getDataZoneID()+" a WHERE r.receipt_id=a.tx_id and r.org_id=a.org_id and a.org_id=:orgID AND a.is_deleted=0  GROUP BY invoice_id ) AS b ON invTable.id=b.invoice_id "
                      + " LEFT JOIN (SELECT SUM(r.amount) AS paidAmt,r.invoice_id,r.linked_invoice_id FROM invoice_linking_details r WHERE r.org_id=:orgID GROUP BY r.invoice_id ) AS a ON invTable.id=a.invoice_id"
                      + " ORDER BY invTable.id ";
			
						  	
			logger.debug("Query is "+sql);

		Map hashMap = new HashMap();

		hashMap.put("orgID",invoiceVO.getOrgID());
		hashMap.put("fromDate", invoiceVO.getFromDate());
		hashMap.put("toDate", invoiceVO.getUptoDate());
		hashMap.put("reviewType", invoiceVO.getIsReview());
		hashMap.put("invoiceType", invoiceVO.getInvoiceType());
		
		RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				InvoiceVO invoiceEntityVO=new InvoiceVO();
					
				invoiceEntityVO.setInvoiceID(rs.getInt("id"));
				invoiceEntityVO.setOrgID(rs.getInt("org_id"));
				invoiceEntityVO.setInvoiceNumber(rs.getString("invoice_number"));
				invoiceEntityVO.setDocID(rs.getString("doc_id"));
				invoiceEntityVO.setEntityName(rs.getString("memberName"));
				invoiceEntityVO.setCreatedTimestamp(rs.getTimestamp("create_date"));
				invoiceEntityVO.setUpdatedTimestamp(rs.getTimestamp("update_date"));
				invoiceEntityVO.setInvoiceDate(rs.getString("invoice_date"));
				invoiceEntityVO.setDueDate(rs.getString("due_date"));
				invoiceEntityVO.setBillCycleID(rs.getInt("billing_cycle_id"));
				invoiceEntityVO.setInvoiceType(rs.getString("invoice_type"));
				invoiceEntityVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
				invoiceEntityVO.setChargeName(rs.getString("chargeName"));
				
				invoiceEntityVO.setGstInvoiceTypeID(rs.getInt("gst_invoice_type_id"));
				invoiceEntityVO.setGstInvoiceType(rs.getString("gstChargeName"));
				
				invoiceEntityVO.setMyGSTIN(rs.getString("gstin"));
				invoiceEntityVO.setCustomerGSTIN(rs.getString("entity_gstin"));
				invoiceEntityVO.setSupplyState(rs.getString("place_of_supply"));
				invoiceEntityVO.setSupplyStateID(rs.getString("place_of_supply_id"));
				
				invoiceEntityVO.setEntityID(rs.getInt("entity_id"));
				invoiceEntityVO.setEntityType(rs.getString("entity_type"));			
				invoiceEntityVO.setEmailID(rs.getString("email_id"));
				invoiceEntityVO.setEntityAddress(rs.getString("entity_address"));
				invoiceEntityVO.setEntityCity(rs.getString("entity_city"));
				invoiceEntityVO.setEntityState(rs.getString("entity_state"));
				invoiceEntityVO.setEntityZipCode(rs.getString("entity_zip"));
				
				invoiceEntityVO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
				invoiceEntityVO.setTotalDiscount(rs.getBigDecimal("total_discount"));
				invoiceEntityVO.setTotalCGST(rs.getBigDecimal("total_cgst"));
				invoiceEntityVO.setTotalSGST(rs.getBigDecimal("total_sgst"));
				invoiceEntityVO.setTotalUGST(rs.getBigDecimal("total_ugst"));
				invoiceEntityVO.setTotalIGST(rs.getBigDecimal("total_igst"));
				invoiceEntityVO.setTotalCess(rs.getBigDecimal("total_cess"));
				invoiceEntityVO.setTdsRate(rs.getBigDecimal("tds_rate"));
				invoiceEntityVO.setTdsSectionNo(rs.getString("tds_section_no"));
				invoiceEntityVO.setTds(rs.getBigDecimal("tds_amount"));	
				invoiceEntityVO.setTotalAmount(rs.getBigDecimal("total_amount"));	
				invoiceEntityVO.setTotalRoundUpAmount(rs.getBigDecimal("roundup_amount"));
				if((rs.getString("invoice_type").equalsIgnoreCase("Proforma"))||(rs.getString("invoice_type").equalsIgnoreCase("Purchase Order"))){
					invoiceEntityVO.setBalance(rs.getBigDecimal("invBalance"));
					
				}else
				invoiceEntityVO.setBalance(rs.getBigDecimal("pBalance"));
				invoiceEntityVO.setTotalGST(invoiceEntityVO.getTotalCGST().add(invoiceEntityVO.getTotalSGST()).add(invoiceEntityVO.getTotalIGST()).add(invoiceEntityVO.getTotalUGST()));
				
				invoiceEntityVO.setBillingAddress(rs.getString("billing_address"));
				invoiceEntityVO.setBillingCity(rs.getString("billing_city"));
				invoiceEntityVO.setBillingState(rs.getString("billing_state"));
				invoiceEntityVO.setBillingZipCode(rs.getString("billing_zip"));
				
				invoiceEntityVO.setShippingAddress(rs.getString("shipping_address"));
				invoiceEntityVO.setShippingCity(rs.getString("shipping_city"));
				invoiceEntityVO.setShippingState(rs.getString("shipping_state"));
				invoiceEntityVO.setShippingZipCode(rs.getString("shipping_zip"));
				invoiceEntityVO.setNotes(rs.getString("notes"));
				invoiceEntityVO.setIsReview(rs.getInt("is_review"));
				invoiceEntityVO.setCreatedBy(rs.getInt("created_by"));
				invoiceEntityVO.setUpdatedBy(rs.getInt("updated_by"));
				invoiceEntityVO.setCreatorName(rs.getString("creatorName"));
				invoiceEntityVO.setUpdaterName(rs.getString("updatorName"));
				invoiceEntityVO.setIsGstCalculationInclusive(rs.getInt("gst_calculation_inclusive"));
				invoiceEntityVO.setIsSales(rs.getInt("is_sales"));
				invoiceEntityVO.setInvoiceTags(rs.getString("line_item_tags"));
				invoiceEntityVO.setIsReverse(rs.getInt("is_reverse"));
				invoiceEntityVO.setPortCode(rs.getString("port_code"));
				invoiceEntityVO.setShippingBillNo(rs.getString("shipping_bill_no"));
				invoiceEntityVO.setShippingDate(rs.getString("shipping_date"));
				invoiceEntityVO.setGstCategory(rs.getString("gst_category"));
				return invoiceEntityVO;
			}
		};
			
			invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
			
			}catch(Exception e){
				logger.error("Exception in getGSTInvoicesForOrg "+e);
			}
			logger.debug("Exit : public List getGSTInvoicesForOrg(InvoiceVO invoiceVO)"+invoiceList.size());
			return invoiceList;		
		}
		
		
		/* Get gst invoices for org   */
		public List getQuickGSTInvoicesForOrg(InvoiceVO invoiceVO){
			List invoiceList=null;
			String statusCondition="";
			String invoiceTypeCondition="";
			if(invoiceVO.getInvoiceType()==null){
				invoiceTypeCondition=""; 
			}else if(invoiceVO.getInvoiceType().equalsIgnoreCase("S")){
				invoiceTypeCondition=" AND ( inv.invoice_type='Sales' or inv.invoice_type='Credit Note')";
			}else if(invoiceVO.getInvoiceType().equalsIgnoreCase("P")){
				invoiceTypeCondition=" AND ( inv.invoice_type='Purchase' or inv.invoice_type='Debit Note')";
			}else {
				invoiceTypeCondition=" AND inv.invoice_type='"+invoiceVO.getInvoiceType()+"'";
			}
			String sql="";
			logger.debug("Entry : public List getGSTInvoicesForOrg(InvoiceVO invoiceVO)");
		
			try{
		
			SocietyVO societyVO=societyService.getSocietyDetails(invoiceVO.getOrgID());
			 sql =" SELECT invTable.*   FROM (SELECT l.ledger_name AS memberName,inv.*,m.full_name As creatorName,mi.full_name As updatorName,t.invoice_type AS chargeName,g.invoice_type AS gstChargeName,CONCAT(IFNULL(l.project_tags,''),il.line_item_tags) AS line_item_tags,il.gst_category  FROM account_ledgers_"+societyVO.getDataZoneID()+" l, invoice_details inv, um_users m, um_users mi, in_invoice_type_details t, in_invoice_type_details  g,"
			 		+ "(SELECT GROUP_CONCAT(IFNULL(ii.line_item_tags,'') SEPARATOR '') AS line_item_tags,ii.invoice_id,ii.gst_category_id,g.name AS gst_category FROM invoice_line_item_details ii,invoice_details i,gst_category_details g WHERE ii.gst_category_id=g.id AND i.id=ii.invoice_id AND i.org_id=:orgID AND i.is_deleted=0 AND ( i.invoice_date BETWEEN :fromDate AND :toDate ) GROUP BY i.id) AS il " 
                      + " WHERE l.id=inv.entity_id And inv.org_id=l.org_id AND inv.id=il.invoice_id   AND inv.org_id=:orgID "+invoiceTypeCondition+"  AND inv.is_review=:reviewType AND inv.is_deleted=0 " 
                      + " AND ( inv.invoice_date BETWEEN :fromDate AND :toDate ) and m.id=inv.created_by and mi.id=inv.updated_by AND inv.invoice_type_id=t.id AND inv.gst_invoice_type_id=g.id ) AS invTable;" ;
			
						  	
			logger.debug("Query is "+sql);

		Map hashMap = new HashMap();

		hashMap.put("orgID",invoiceVO.getOrgID());
		hashMap.put("fromDate", invoiceVO.getFromDate());
		hashMap.put("toDate", invoiceVO.getUptoDate());
		hashMap.put("reviewType", invoiceVO.getIsReview());
		hashMap.put("invoiceType", invoiceVO.getInvoiceType());
		
		RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				InvoiceVO invoiceEntityVO=new InvoiceVO();
					
				invoiceEntityVO.setInvoiceID(rs.getInt("id"));
				invoiceEntityVO.setOrgID(rs.getInt("org_id"));
				invoiceEntityVO.setInvoiceNumber(rs.getString("invoice_number"));
				invoiceEntityVO.setDocID(rs.getString("doc_id"));
				invoiceEntityVO.setEntityName(rs.getString("memberName"));
				invoiceEntityVO.setCreatedTimestamp(rs.getTimestamp("create_date"));
				invoiceEntityVO.setUpdatedTimestamp(rs.getTimestamp("update_date"));
				invoiceEntityVO.setInvoiceDate(rs.getString("invoice_date"));
				invoiceEntityVO.setDueDate(rs.getString("due_date"));
				invoiceEntityVO.setBillCycleID(rs.getInt("billing_cycle_id"));
				invoiceEntityVO.setInvoiceType(rs.getString("invoice_type"));
				invoiceEntityVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
				invoiceEntityVO.setChargeName(rs.getString("chargeName"));
				
				invoiceEntityVO.setGstInvoiceTypeID(rs.getInt("gst_invoice_type_id"));
				invoiceEntityVO.setGstInvoiceType(rs.getString("gstChargeName"));
				
				invoiceEntityVO.setMyGSTIN(rs.getString("gstin"));
				invoiceEntityVO.setCustomerGSTIN(rs.getString("entity_gstin"));
				invoiceEntityVO.setSupplyState(rs.getString("place_of_supply"));
				invoiceEntityVO.setSupplyStateID(rs.getString("place_of_supply_id"));
				
				invoiceEntityVO.setEntityID(rs.getInt("entity_id"));
				invoiceEntityVO.setEntityType(rs.getString("entity_type"));			
				invoiceEntityVO.setEmailID(rs.getString("email_id"));
				invoiceEntityVO.setEntityAddress(rs.getString("entity_address"));
				invoiceEntityVO.setEntityCity(rs.getString("entity_city"));
				invoiceEntityVO.setEntityState(rs.getString("entity_state"));
				invoiceEntityVO.setEntityZipCode(rs.getString("entity_zip"));
				
				invoiceEntityVO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
				invoiceEntityVO.setTotalDiscount(rs.getBigDecimal("total_discount"));
				invoiceEntityVO.setTotalCGST(rs.getBigDecimal("total_cgst"));
				invoiceEntityVO.setTotalSGST(rs.getBigDecimal("total_sgst"));
				invoiceEntityVO.setTotalUGST(rs.getBigDecimal("total_ugst"));
				invoiceEntityVO.setTotalIGST(rs.getBigDecimal("total_igst"));
				invoiceEntityVO.setTotalCess(rs.getBigDecimal("total_cess"));
				invoiceEntityVO.setTdsRate(rs.getBigDecimal("tds_rate"));
				invoiceEntityVO.setTdsSectionNo(rs.getString("tds_section_no"));
				invoiceEntityVO.setTds(rs.getBigDecimal("tds_amount"));	
				invoiceEntityVO.setTotalAmount(rs.getBigDecimal("total_amount"));	
				invoiceEntityVO.setTotalRoundUpAmount(rs.getBigDecimal("roundup_amount"));
				
				invoiceEntityVO.setTotalGST(invoiceEntityVO.getTotalCGST().add(invoiceEntityVO.getTotalSGST()).add(invoiceEntityVO.getTotalIGST()).add(invoiceEntityVO.getTotalUGST()));
				
				invoiceEntityVO.setBillingAddress(rs.getString("billing_address"));
				invoiceEntityVO.setBillingCity(rs.getString("billing_city"));
				invoiceEntityVO.setBillingState(rs.getString("billing_state"));
				invoiceEntityVO.setBillingZipCode(rs.getString("billing_zip"));
				
				invoiceEntityVO.setShippingAddress(rs.getString("shipping_address"));
				invoiceEntityVO.setShippingCity(rs.getString("shipping_city"));
				invoiceEntityVO.setShippingState(rs.getString("shipping_state"));
				invoiceEntityVO.setShippingZipCode(rs.getString("shipping_zip"));
				invoiceEntityVO.setNotes(rs.getString("notes"));
				invoiceEntityVO.setIsReview(rs.getInt("is_review"));
				invoiceEntityVO.setCreatedBy(rs.getInt("created_by"));
				invoiceEntityVO.setUpdatedBy(rs.getInt("updated_by"));
				invoiceEntityVO.setCreatorName(rs.getString("creatorName"));
				invoiceEntityVO.setUpdaterName(rs.getString("updatorName"));
				invoiceEntityVO.setIsGstCalculationInclusive(rs.getInt("gst_calculation_inclusive"));
				invoiceEntityVO.setIsSales(rs.getInt("is_sales"));
				invoiceEntityVO.setInvoiceTags(rs.getString("line_item_tags"));
				invoiceEntityVO.setIsReverse(rs.getInt("is_reverse"));
				invoiceEntityVO.setPortCode(rs.getString("port_code"));
				invoiceEntityVO.setShippingBillNo(rs.getString("shipping_bill_no"));
				invoiceEntityVO.setShippingDate(rs.getString("shipping_date"));
				invoiceEntityVO.setGstCategory(rs.getString("gst_category"));
				return invoiceEntityVO;
			}
		};
			
			invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
			
			}catch(Exception e){
				logger.error("Exception in getGSTInvoicesForOrg "+e);
			}
			logger.debug("Exit : public List getGSTInvoicesForOrg(InvoiceVO invoiceVO)"+invoiceList.size());
			return invoiceList;		
		}
		
		/* Get gst invoice list like day book   */
		public List getGSTInvoicesListDayBook(InvoiceVO invoiceVO){
			List invoiceList=null;
			String statusCondition="";
			
			String sql="";
			logger.debug("Entry : public List getGSTInvoicesListDayBook(InvoiceVO invoiceVO)");
		
			try{
				
			if((invoiceVO.getInvoiceType()!=null)&&(invoiceVO.getInvoiceType().length()>0))
				statusCondition="AND inv.invoice_type=:invoiceType";
				
			SocietyVO societyVO=societyService.getSocietyDetails(invoiceVO.getOrgID());
			 sql ="SELECT l.ledger_name AS memberName,inv.*,m.full_name As creatorName,mi.full_name As updatorName,t.invoice_type AS chargeName ,g.invoice_type AS gstChargeName  FROM account_ledgers_"+societyVO.getDataZoneID()+" l, invoice_details inv, um_users m, um_users mi, in_invoice_type_details t, in_invoice_type_details g" 
                      + " WHERE l.id=inv.entity_id And inv.org_id=l.org_id   AND inv.org_id=:orgID   AND inv.is_review=:reviewType  "+statusCondition 
                      + " AND ( inv.invoice_date BETWEEN :fromDate AND :toDate ) and m.id=inv.created_by and mi.id=inv.updated_by AND inv.invoice_type_id=t.id AND inv.gst_invoice_type_id=g.id  ORDER BY inv.doc_id,inv.invoice_date";
                     
			
						  	
			logger.debug("Query is "+sql);

		Map hashMap = new HashMap();

		hashMap.put("orgID",invoiceVO.getOrgID());
		hashMap.put("fromDate", invoiceVO.getFromDate());
		hashMap.put("toDate", invoiceVO.getUptoDate());
		hashMap.put("reviewType", invoiceVO.getIsReview());
		hashMap.put("invoiceType", invoiceVO.getInvoiceType());
		
		RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				InvoiceVO invoiceEntityVO=new InvoiceVO();
					
				invoiceEntityVO.setInvoiceID(rs.getInt("id"));
				invoiceEntityVO.setOrgID(rs.getInt("org_id"));
				invoiceEntityVO.setInvoiceNumber(rs.getString("invoice_number"));
				invoiceEntityVO.setDocID(rs.getString("doc_id"));
				invoiceEntityVO.setEntityName(rs.getString("memberName"));
				
				invoiceEntityVO.setInvoiceDate(rs.getString("invoice_date"));
				invoiceEntityVO.setDueDate(rs.getString("due_date"));
				invoiceEntityVO.setBillCycleID(rs.getInt("billing_cycle_id"));
				invoiceEntityVO.setInvoiceType(rs.getString("invoice_type"));
				invoiceEntityVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
				invoiceEntityVO.setChargeName(rs.getString("chargeName"));
				
				invoiceEntityVO.setGstInvoiceTypeID(rs.getInt("gst_invoice_type_id"));
				invoiceEntityVO.setGstInvoiceType(rs.getString("gstChargeName"));
				
				invoiceEntityVO.setMyGSTIN(rs.getString("gstin"));
				invoiceEntityVO.setCustomerGSTIN(rs.getString("entity_gstin"));
				invoiceEntityVO.setSupplyState(rs.getString("place_of_supply"));
				invoiceEntityVO.setSupplyStateID(rs.getString("place_of_supply_id"));
				
				invoiceEntityVO.setEntityID(rs.getInt("entity_id"));
				invoiceEntityVO.setEntityType(rs.getString("entity_type"));			
				invoiceEntityVO.setEmailID(rs.getString("email_id"));
				invoiceEntityVO.setEntityAddress(rs.getString("entity_address"));
				invoiceEntityVO.setEntityCity(rs.getString("entity_city"));
				invoiceEntityVO.setEntityState(rs.getString("entity_state"));
				invoiceEntityVO.setEntityZipCode(rs.getString("entity_zip"));
				invoiceEntityVO.setIsDeleted(rs.getInt("is_deleted"));
				invoiceEntityVO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
				invoiceEntityVO.setTotalDiscount(rs.getBigDecimal("total_discount"));
				invoiceEntityVO.setTotalCGST(rs.getBigDecimal("total_cgst"));
				invoiceEntityVO.setTotalSGST(rs.getBigDecimal("total_sgst"));
				invoiceEntityVO.setTotalUGST(rs.getBigDecimal("total_ugst"));
				invoiceEntityVO.setTotalIGST(rs.getBigDecimal("total_igst"));
				invoiceEntityVO.setTotalCess(rs.getBigDecimal("total_cess"));
				invoiceEntityVO.setTdsRate(rs.getBigDecimal("tds_rate"));
				invoiceEntityVO.setTdsSectionNo(rs.getString("tds_section_no"));
				invoiceEntityVO.setTds(rs.getBigDecimal("tds_amount"));	
				invoiceEntityVO.setTotalAmount(rs.getBigDecimal("total_amount"));	
				invoiceEntityVO.setTotalRoundUpAmount(rs.getBigDecimal("roundup_amount"));
				invoiceEntityVO.setTotalGST(invoiceEntityVO.getTotalCGST().add(invoiceEntityVO.getTotalSGST()).add(invoiceEntityVO.getTotalIGST()).add(invoiceEntityVO.getTotalUGST()));
				invoiceEntityVO.setBillingAddress(rs.getString("billing_address"));
				invoiceEntityVO.setBillingCity(rs.getString("billing_city"));
				invoiceEntityVO.setBillingState(rs.getString("billing_state"));
				invoiceEntityVO.setBillingZipCode(rs.getString("billing_zip"));
				
				invoiceEntityVO.setShippingAddress(rs.getString("shipping_address"));
				invoiceEntityVO.setShippingCity(rs.getString("shipping_city"));
				invoiceEntityVO.setShippingState(rs.getString("shipping_state"));
				invoiceEntityVO.setShippingZipCode(rs.getString("shipping_zip"));
				invoiceEntityVO.setNotes(rs.getString("notes"));
				invoiceEntityVO.setIsReview(rs.getInt("is_review"));
				invoiceEntityVO.setCreatedBy(rs.getInt("created_by"));
				invoiceEntityVO.setUpdatedBy(rs.getInt("updated_by"));
				invoiceEntityVO.setCreatorName(rs.getString("creatorName"));
				invoiceEntityVO.setUpdaterName(rs.getString("updatorName"));
				invoiceEntityVO.setIsGstCalculationInclusive(rs.getInt("gst_calculation_inclusive"));
				invoiceEntityVO.setIsSales(rs.getInt("is_sales"));
				invoiceEntityVO.setInvoiceTags(rs.getString("invoice_tags"));
				invoiceEntityVO.setIsReverse(rs.getInt("is_reverse"));
				invoiceEntityVO.setPortCode(rs.getString("port_code"));
				invoiceEntityVO.setShippingBillNo(rs.getString("shipping_bill_no"));
				invoiceEntityVO.setShippingDate(rs.getString("shipping_date"));
				return invoiceEntityVO;
			}
		};
			
			invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
			
			}catch(Exception e){
				logger.error("Exception in getGSTInvoicesListDayBook "+e);
			}
			logger.debug("Exit : public List getGSTInvoicesListDayBook(InvoiceVO invoiceVO)"+invoiceList.size());
			return invoiceList;		
		}
		
		/* Get invoice details   */
		public InvoiceVO getInvoiceDetails(InvoiceVO invoiceVO){
			List invoiceList=null;
			String statusCondition="";
			String invoiceTypeCondition="";
			InvoiceVO invoiceDetailsVO =new InvoiceVO();
			logger.debug("Entry : public List getInvoiceDetails(InvoiceVO invoiceVO)"+invoiceVO.getOrgID());
		
			try{
			SocietyVO societyVO=societyService.getSocietyDetails(invoiceVO.getOrgID());
					
			//String sql = " SELECT l.ledger_name AS memberName,inv.*,m.full_name As creatorName,mi.full_name As updatorName  FROM account_ledgers_"+invoiceVO.getOrgID()+" l, invoice_details inv, um_users m, um_users mi  "
	               //    + " WHERE l.id=inv.entity_id  AND inv.org_id=:orgID  AND inv.id=:invoiceID and m.id=inv.created_by and mi.id=inv.updated_by AND inv.is_deleted=0 ";
			  String sql =  " SELECT invTable.*,ats.tx_id,cd.*, IFNULL((ats.txBalanceAmount-(IFNULL(b.paidAmt,0))),0) AS pBalance , IFNULL((invTable.total_amount-(IFNULL(a.paidAmt,0))),0) AS invBalance FROM (SELECT l.ledger_name AS memberName,inv.*,m.full_name AS creatorName,mi.full_name AS updatorName,t.invoice_type AS chargeName ,g.invoice_type AS gstChargeName  FROM account_ledgers_"+societyVO.getDataZoneID()+" l, invoice_details inv, um_users m, um_users mi, in_invoice_type_details t ,in_invoice_type_details g "
                          + " WHERE l.id=inv.entity_id  AND inv.org_id=:orgID and l.org_id=inv.org_id  AND inv.id=:invoiceID AND m.id=inv.created_by AND mi.id=inv.updated_by  AND inv.invoice_type_id=t.id AND inv.gst_invoice_type_id=g.id AND inv.is_deleted=0)AS invTable "
                          + " LEFT JOIN (SELECT tx_id,invoice_id,IFNULL(amount,0) AS txBalanceAmount,IFNULL(invoice_id,0) AS invoiceID  FROM account_transactions_"+societyVO.getDataZoneID()+" where org_id=:orgID )AS ats ON invTable.id=ats.invoice_id "			  	
			              + " LEFT JOIN (SELECT SUM(r.amount) AS paidAmt,r.invoice_id,receipt_id FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" r,account_transactions_"+societyVO.getDataZoneID()+" a WHERE r.receipt_id=a.tx_id AND r.org_id=a.org_id AND a.org_id=:orgID AND a.is_deleted=0  GROUP BY invoice_id ) AS b ON invTable.id=b.invoice_id "
			              + " LEFT JOIN (SELECT SUM(r.amount) AS paidAmt,r.invoice_id,r.linked_invoice_id FROM invoice_linking_details r WHERE r.org_id=:orgID GROUP BY r.invoice_id ) AS a ON invTable.id=a.invoice_id	"
                          + " LEFT JOIN (SELECT * FROM currency_details )AS cd ON cd.id=invTable.currency_id ";	
			  logger.debug("Query is "+sql);

		Map hashMap = new HashMap();

		hashMap.put("orgID",invoiceVO.getOrgID());	
		hashMap.put("invoiceID", invoiceVO.getInvoiceID());
		

		RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				InvoiceVO invoiceEntityVO=new InvoiceVO();
				CurrencyVO currencyVO=new CurrencyVO();
				
				invoiceEntityVO.setOrgID(rs.getInt("org_id"));
				invoiceEntityVO.setInvoiceID(rs.getInt("id"));
				invoiceEntityVO.setInvoiceNumber(rs.getString("invoice_number"));
				invoiceEntityVO.setDocID(rs.getString("doc_id"));
				invoiceEntityVO.setEntityName(rs.getString("memberName"));
				
				invoiceEntityVO.setInvoiceDate(rs.getString("invoice_date"));
				invoiceEntityVO.setDueDate(rs.getString("due_date"));
				invoiceEntityVO.setBillCycleID(rs.getInt("billing_cycle_id"));
				invoiceEntityVO.setInvoiceType(rs.getString("invoice_type"));
				invoiceEntityVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
				invoiceEntityVO.setChargeName(rs.getString("chargeName"));
				
				invoiceEntityVO.setGstInvoiceTypeID(rs.getInt("gst_invoice_type_id"));
				invoiceEntityVO.setGstInvoiceType(rs.getString("gstChargeName"));
				
				invoiceEntityVO.setMyGSTIN(rs.getString("gstin"));
				invoiceEntityVO.setCustomerGSTIN(rs.getString("entity_gstin"));
				invoiceEntityVO.setSupplyState(rs.getString("place_of_supply"));
				invoiceEntityVO.setSupplyStateID(rs.getString("place_of_supply_id"));
				
				invoiceEntityVO.setEntityID(rs.getInt("entity_id"));
				invoiceEntityVO.setEntityType(rs.getString("entity_type"));			
				invoiceEntityVO.setEmailID(rs.getString("email_id"));
				invoiceEntityVO.setEntityAddress(rs.getString("entity_address"));
				invoiceEntityVO.setEntityCity(rs.getString("entity_city"));
				invoiceEntityVO.setEntityState(rs.getString("entity_state"));
				invoiceEntityVO.setEntityZipCode(rs.getString("entity_zip"));
				
				invoiceEntityVO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
				invoiceEntityVO.setTotalDiscount(rs.getBigDecimal("total_discount"));
				invoiceEntityVO.setTotalCGST(rs.getBigDecimal("total_cgst"));
				invoiceEntityVO.setTotalSGST(rs.getBigDecimal("total_sgst"));
				invoiceEntityVO.setTotalUGST(rs.getBigDecimal("total_ugst"));
				invoiceEntityVO.setTotalIGST(rs.getBigDecimal("total_igst"));
				invoiceEntityVO.setTotalCess(rs.getBigDecimal("total_cess"));
				invoiceEntityVO.setTdsRate(rs.getBigDecimal("tds_rate"));
				invoiceEntityVO.setTdsSectionNo(rs.getString("tds_section_no"));
				invoiceEntityVO.setTds(rs.getBigDecimal("tds_amount"));			
				invoiceEntityVO.setTotalAmount(rs.getBigDecimal("total_amount"));	
				invoiceEntityVO.setTotalRoundUpAmount(rs.getBigDecimal("roundup_amount"));
				invoiceEntityVO.setTotalGST(invoiceEntityVO.getTotalCGST().add(invoiceEntityVO.getTotalSGST()).add(invoiceEntityVO.getTotalIGST()).add(invoiceEntityVO.getTotalUGST()));
				if((rs.getString("invoice_type").equalsIgnoreCase("Proforma"))||(rs.getString("invoice_type").equalsIgnoreCase("Purchase Order"))){
					invoiceEntityVO.setBalance(rs.getBigDecimal("invBalance"));
					
				}else
				invoiceEntityVO.setBalance(rs.getBigDecimal("pBalance"));
				
				invoiceEntityVO.setBillingAddress(rs.getString("billing_address"));
				invoiceEntityVO.setBillingCity(rs.getString("billing_city"));
				invoiceEntityVO.setBillingState(rs.getString("billing_state"));
				invoiceEntityVO.setBillingZipCode(rs.getString("billing_zip"));
				
				invoiceEntityVO.setShippingAddress(rs.getString("shipping_address"));
				invoiceEntityVO.setShippingCity(rs.getString("shipping_city"));
				invoiceEntityVO.setShippingState(rs.getString("shipping_state"));
				invoiceEntityVO.setShippingZipCode(rs.getString("shipping_zip"));
				
				invoiceEntityVO.setNotes(rs.getString("notes"));
				invoiceEntityVO.setIsReview(rs.getInt("is_review"));
				invoiceEntityVO.setCreatedBy(rs.getInt("created_by"));
				invoiceEntityVO.setUpdatedBy(rs.getInt("updated_by"));
				invoiceEntityVO.setCreatorName(rs.getString("creatorName"));
				invoiceEntityVO.setUpdaterName(rs.getString("updatorName"));
				invoiceEntityVO.setTransactionID(rs.getString("tx_id"));
				invoiceEntityVO.setIsSales(rs.getInt("is_sales"));
				invoiceEntityVO.setIsGstCalculationInclusive(rs.getInt("gst_calculation_inclusive"));
				invoiceEntityVO.setInvoiceTags(rs.getString("invoice_tags"));
				invoiceEntityVO.setPortCode(rs.getString("port_code"));
				invoiceEntityVO.setShippingBillNo(rs.getString("shipping_bill_no"));
				invoiceEntityVO.setShippingDate(rs.getString("shipping_date"));
				invoiceEntityVO.setIsReverse(rs.getInt("is_reverse"));
				invoiceEntityVO.setCurrencyID(rs.getInt("currency_id"));
				invoiceEntityVO.setCurrencyRate(rs.getBigDecimal("currency_rate"));
				
				currencyVO.setCurrencyID(rs.getInt("currency_id"));
				currencyVO.setCountry(rs.getString("country_name"));
				currencyVO.setCurrencyCode(rs.getString("currency_code"));
				currencyVO.setCurrencyName(rs.getString("currency_name"));
				currencyVO.setCurrencySymbol(rs.getString("symbol_html"));
				currencyVO.setDecimalCurrency(rs.getString("decimal_currency"));
				invoiceEntityVO.setCurrencyVO(currencyVO);
				return invoiceEntityVO;
			}
		};
			
		     invoiceDetailsVO =(InvoiceVO) namedParameterJdbcTemplate.queryForObject(sql, hashMap,rMapper);
			
			}catch(EmptyResultDataAccessException e){
				logger.info("No info found for InvoiceDetails  - Invoice ID "+invoiceVO.getInvoiceID()+" for Org ID "+invoiceVO.getOrgID());
			}catch(Exception e){
				logger.error("Exception in getInvoiceDetails "+e);
			}
			logger.debug("Exit : public List getInvoiceDetails(InvoiceVO invoiceVO)");
			return invoiceDetailsVO;		
		}
		
		
		/* Get invoice details by invoice number   */
		public InvoiceVO getInvoiceDetailsByInvoiceNumber(InvoiceVO invoiceVO){
			List invoiceList=null;
			String statusCondition="";
			String invoiceTypeCondition="";
			InvoiceVO invoiceDetailsVO =new InvoiceVO();
			logger.debug("Entry : public List getInvoiceDetailsByInvoiceNumber(InvoiceVO invoiceVO)"+invoiceVO.getOrgID());
		
			try{
			SocietyVO societyVO=societyService.getSocietyDetails(invoiceVO.getOrgID());
					
			//String sql = " SELECT l.ledger_name AS memberName,inv.*,m.full_name As creatorName,mi.full_name As updatorName  FROM account_ledgers_"+invoiceVO.getOrgID()+" l, invoice_details inv, um_users m, um_users mi  "
	               //    + " WHERE l.id=inv.entity_id  AND inv.org_id=:orgID  AND inv.id=:invoiceID and m.id=inv.created_by and mi.id=inv.updated_by AND inv.is_deleted=0 ";
			  String sql =  " SELECT invTable.*,ats.tx_id,cd.*, IFNULL((ats.txBalanceAmount-(IFNULL(b.paidAmt,0))),0) AS pBalance , IFNULL((invTable.total_amount-(IFNULL(a.paidAmt,0))),0) AS invBalance FROM (SELECT l.ledger_name AS memberName,inv.*,m.full_name AS creatorName,mi.full_name AS updatorName,t.invoice_type AS chargeName ,g.invoice_type AS gstChargeName  FROM account_ledgers_"+societyVO.getDataZoneID()+" l, invoice_details inv, um_users m, um_users mi, in_invoice_type_details t ,in_invoice_type_details g "
                          + " WHERE l.id=inv.entity_id  AND inv.org_id=:orgID and l.org_id=inv.org_id  AND inv.invoice_number=:invoiceNumber AND inv.invoice_type=:invoiceType AND m.id=inv.created_by AND mi.id=inv.updated_by  AND inv.invoice_type_id=t.id AND inv.gst_invoice_type_id=g.id AND inv.is_deleted=0)AS invTable "
                          + " LEFT JOIN (SELECT tx_id,invoice_id,IFNULL(amount,0) AS txBalanceAmount,IFNULL(invoice_id,0) AS invoiceID  FROM account_transactions_"+societyVO.getDataZoneID()+" where org_id=:orgID )AS ats ON invTable.id=ats.invoice_id "			  	
			              + " LEFT JOIN (SELECT SUM(r.amount) AS paidAmt,r.invoice_id,receipt_id FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" r,account_transactions_"+societyVO.getDataZoneID()+" a WHERE r.receipt_id=a.tx_id AND r.org_id=a.org_id AND a.org_id=:orgID AND a.is_deleted=0  GROUP BY invoice_id ) AS b ON invTable.id=b.invoice_id "
			              + " LEFT JOIN (SELECT SUM(r.amount) AS paidAmt,r.invoice_id,r.linked_invoice_id FROM invoice_linking_details r WHERE r.org_id=:orgID GROUP BY r.invoice_id ) AS a ON invTable.id=a.invoice_id	"
                          + " LEFT JOIN (SELECT * FROM currency_details )AS cd ON cd.id=invTable.currency_id ";	
			  logger.debug("Query is "+sql);

		Map hashMap = new HashMap();

		hashMap.put("orgID",invoiceVO.getOrgID());	
		hashMap.put("invoiceNumber", invoiceVO.getInvoiceNumber());
		hashMap.put("invoiceType", invoiceVO.getInvoiceType());
		

		RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				InvoiceVO invoiceEntityVO=new InvoiceVO();
				CurrencyVO currencyVO=new CurrencyVO();
				
				invoiceEntityVO.setOrgID(rs.getInt("org_id"));
				invoiceEntityVO.setInvoiceID(rs.getInt("id"));
				invoiceEntityVO.setInvoiceNumber(rs.getString("invoice_number"));
				invoiceEntityVO.setDocID(rs.getString("doc_id"));
				invoiceEntityVO.setEntityName(rs.getString("memberName"));
				
				invoiceEntityVO.setInvoiceDate(rs.getString("invoice_date"));
				invoiceEntityVO.setDueDate(rs.getString("due_date"));
				invoiceEntityVO.setBillCycleID(rs.getInt("billing_cycle_id"));
				invoiceEntityVO.setInvoiceType(rs.getString("invoice_type"));
				invoiceEntityVO.setInvoiceTypeID(rs.getInt("invoice_type_id"));
				invoiceEntityVO.setChargeName(rs.getString("chargeName"));
				
				invoiceEntityVO.setGstInvoiceTypeID(rs.getInt("gst_invoice_type_id"));
				invoiceEntityVO.setGstInvoiceType(rs.getString("gstChargeName"));
				
				invoiceEntityVO.setMyGSTIN(rs.getString("gstin"));
				invoiceEntityVO.setCustomerGSTIN(rs.getString("entity_gstin"));
				invoiceEntityVO.setSupplyState(rs.getString("place_of_supply"));
				invoiceEntityVO.setSupplyStateID(rs.getString("place_of_supply_id"));
				
				invoiceEntityVO.setEntityID(rs.getInt("entity_id"));
				invoiceEntityVO.setEntityType(rs.getString("entity_type"));			
				invoiceEntityVO.setEmailID(rs.getString("email_id"));
				invoiceEntityVO.setEntityAddress(rs.getString("entity_address"));
				invoiceEntityVO.setEntityCity(rs.getString("entity_city"));
				invoiceEntityVO.setEntityState(rs.getString("entity_state"));
				invoiceEntityVO.setEntityZipCode(rs.getString("entity_zip"));
				
				invoiceEntityVO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
				invoiceEntityVO.setTotalDiscount(rs.getBigDecimal("total_discount"));
				invoiceEntityVO.setTotalCGST(rs.getBigDecimal("total_cgst"));
				invoiceEntityVO.setTotalSGST(rs.getBigDecimal("total_sgst"));
				invoiceEntityVO.setTotalUGST(rs.getBigDecimal("total_ugst"));
				invoiceEntityVO.setTotalIGST(rs.getBigDecimal("total_igst"));
				invoiceEntityVO.setTotalCess(rs.getBigDecimal("total_cess"));
				invoiceEntityVO.setTdsRate(rs.getBigDecimal("tds_rate"));
				invoiceEntityVO.setTdsSectionNo(rs.getString("tds_section_no"));
				invoiceEntityVO.setTds(rs.getBigDecimal("tds_amount"));			
				invoiceEntityVO.setTotalAmount(rs.getBigDecimal("total_amount"));	
				invoiceEntityVO.setTotalRoundUpAmount(rs.getBigDecimal("roundup_amount"));
				invoiceEntityVO.setTotalGST(invoiceEntityVO.getTotalCGST().add(invoiceEntityVO.getTotalSGST()).add(invoiceEntityVO.getTotalIGST()).add(invoiceEntityVO.getTotalUGST()));
				if((rs.getString("invoice_type").equalsIgnoreCase("Proforma"))||(rs.getString("invoice_type").equalsIgnoreCase("Purchase Order"))){
					invoiceEntityVO.setBalance(rs.getBigDecimal("invBalance"));
					
				}else
				invoiceEntityVO.setBalance(rs.getBigDecimal("pBalance"));
				
				invoiceEntityVO.setBillingAddress(rs.getString("billing_address"));
				invoiceEntityVO.setBillingCity(rs.getString("billing_city"));
				invoiceEntityVO.setBillingState(rs.getString("billing_state"));
				invoiceEntityVO.setBillingZipCode(rs.getString("billing_zip"));
				
				invoiceEntityVO.setShippingAddress(rs.getString("shipping_address"));
				invoiceEntityVO.setShippingCity(rs.getString("shipping_city"));
				invoiceEntityVO.setShippingState(rs.getString("shipping_state"));
				invoiceEntityVO.setShippingZipCode(rs.getString("shipping_zip"));
				
				invoiceEntityVO.setNotes(rs.getString("notes"));
				invoiceEntityVO.setIsReview(rs.getInt("is_review"));
				invoiceEntityVO.setCreatedBy(rs.getInt("created_by"));
				invoiceEntityVO.setUpdatedBy(rs.getInt("updated_by"));
				invoiceEntityVO.setCreatorName(rs.getString("creatorName"));
				invoiceEntityVO.setUpdaterName(rs.getString("updatorName"));
				invoiceEntityVO.setTransactionID(rs.getString("tx_id"));
				invoiceEntityVO.setIsSales(rs.getInt("is_sales"));
				invoiceEntityVO.setIsGstCalculationInclusive(rs.getInt("gst_calculation_inclusive"));
				invoiceEntityVO.setInvoiceTags(rs.getString("invoice_tags"));
				invoiceEntityVO.setPortCode(rs.getString("port_code"));
				invoiceEntityVO.setShippingBillNo(rs.getString("shipping_bill_no"));
				invoiceEntityVO.setShippingDate(rs.getString("shipping_date"));
				invoiceEntityVO.setIsReverse(rs.getInt("is_reverse"));
				invoiceEntityVO.setCurrencyID(rs.getInt("currency_id"));
				invoiceEntityVO.setCurrencyRate(rs.getBigDecimal("currency_rate"));
				
				currencyVO.setCurrencyID(rs.getInt("currency_id"));
				currencyVO.setCountry(rs.getString("country_name"));
				currencyVO.setCurrencyCode(rs.getString("currency_code"));
				currencyVO.setCurrencyName(rs.getString("currency_name"));
				currencyVO.setCurrencySymbol(rs.getString("symbol_html"));
				currencyVO.setDecimalCurrency(rs.getString("decimal_currency"));
				invoiceEntityVO.setCurrencyVO(currencyVO);
				return invoiceEntityVO;
			}
		};
			
		     invoiceDetailsVO =(InvoiceVO) namedParameterJdbcTemplate.queryForObject(sql, hashMap,rMapper);
			
			}catch(EmptyResultDataAccessException e){
				logger.info("No info found for getInvoiceDetailsByInvoiceNumber  - Invoice ID "+invoiceVO.getInvoiceNumber()+" for Org ID "+invoiceVO.getOrgID());
			}catch(Exception e){
				logger.error("Exception in getInvoiceDetailsByInvoiceNumber "+e);
			}
			logger.debug("Exit : public List getInvoiceDetailsByInvoiceNumber(InvoiceVO invoiceVO)");
			return invoiceDetailsVO;		
		}
		
		
		/* Get invoice line items list  */
		public List getInvoiceLineItemList(InvoiceVO invoiceVO){
			List invoiceList=null;
			
			logger.debug("Entry : public List getInvoiceLineItemList(InvoiceVO invoiceVO)");
		
			try{				
					
			//String sql = " SELECT li.*,h.item_code FROM invoice_line_item_details li,product_service_legder_relation r, hsn_sac_master h WHERE invoice_id=:invoiceID  AND r.id=li.item_id AND h.id=hsn_sac_id ";
			  String sql =" SELECT inv.*,c.*,gc.gstCategoryID, gc.gstCategoryName FROM (SELECT * FROM invoice_line_item_details WHERE invoice_id=:invoiceID )AS inv "
                        + " LEFT JOIN ( SELECT h.*,r.id AS itemId FROM hsn_sac_master h,product_service_legder_relation r "
                        + " WHERE h.id=r.hsn_sac_id) AS c ON  c.itemId=inv.item_id"
                        + " LEFT JOIN ( SELECT gc.id AS gstCategoryID, gc.name AS gstCategoryName FROM gst_category_details gc ) AS gc ON inv.gst_category_id=gc.gstCategoryID ";
                        		  	
			logger.debug("Query is "+sql);

		Map hashMap = new HashMap();

		hashMap.put("invoiceID", invoiceVO.getInvoiceID());
		

		RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				InLineItemsVO invoiceEntityVO=new InLineItemsVO();
				
				invoiceEntityVO.setInvLineItemID(rs.getInt("id"));
				invoiceEntityVO.setInvoiceID(rs.getInt("invoice_id"));
				invoiceEntityVO.setItemID(rs.getString("item_id"));
				invoiceEntityVO.setHsnSacCode(rs.getString("hsn_sac_code"));
				invoiceEntityVO.setLedgerID(rs.getInt("ledger_id"));
				invoiceEntityVO.setQuantity(rs.getBigDecimal("quantity"));
				invoiceEntityVO.setPerUnitPrice(rs.getBigDecimal("price"));
				invoiceEntityVO.setTotalPrice(rs.getBigDecimal("total_price"));
				invoiceEntityVO.setDiscount(rs.getBigDecimal("discount"));
				invoiceEntityVO.setDescription(rs.getString("description"));
				invoiceEntityVO.setcGST(rs.getBigDecimal("cgst"));
				invoiceEntityVO.setsGST(rs.getBigDecimal("sgst"));
				invoiceEntityVO.setuGST(rs.getBigDecimal("ugst"));
				invoiceEntityVO.setiGST(rs.getBigDecimal("igst"));
				invoiceEntityVO.setCess(rs.getBigDecimal("cess"));
				invoiceEntityVO.setGstPercent(rs.getBigDecimal("gst_percentage"));
				invoiceEntityVO.setCessPercent(rs.getBigDecimal("cess_percentage"));
				invoiceEntityVO.setGstAmount(rs.getBigDecimal("cgst").add(rs.getBigDecimal("sgst")).add(rs.getBigDecimal("ugst").add(rs.getBigDecimal("igst"))));
				invoiceEntityVO.setAmount(rs.getBigDecimal("amount"));
				invoiceEntityVO.setPurchasePrice(rs.getBigDecimal("purchase_price"));
				invoiceEntityVO.setItemName(rs.getString("item_name"));
				invoiceEntityVO.setGstCategoryID(rs.getInt("gstCategoryID"));
				invoiceEntityVO.setGstCategoryName(rs.getString("gstCategoryName"));
				invoiceEntityVO.setLineItemTags(rs.getString("line_item_tags"));
				return invoiceEntityVO;
			}
		};
			
			invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
			
			}catch(Exception e){
				logger.error("Exception in getInvoiceLineItemList "+e);
			}
			logger.debug("Exit : public List getInvoiceLineItemList(InvoiceVO invoiceVO)"+invoiceList.size());
			return invoiceList;		
		}	
		
		
		/* Update the GSt invoice */
		public int updateGSTInvoice(InvoiceVO invoiceVO) {
			int updateFlag = 0;
			logger.debug("Entry : public int updateInvoice(InvoiceVO invoiceVO)");
			 java.util.Date today = new java.util.Date();
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			 
			   invoiceVO.setUpdatedTimestamp(currentTimestamp);
			try {
				String sql="Update invoice_details set invoice_number=:invoiceNo,doc_id=:docID,invoice_date=:invoiceDate, due_date=:dueDate, payment_status=:statusID, invoice_amount=:invoiceAmount," +
						" gstin=:gstIN, entity_gstin=:entityGSTIN, place_of_supply=:supplyState, place_of_supply_id=:supplyStateID, is_review=:isReview, total_discount=:totalDiscount, total_cgst=:totalCGST, total_sgst=:totalSGST, total_igst=:totalIGST, is_sales=:isSales, is_reverse=:isReverse , "+
						" total_ugst=:totalUGST, total_cess=:totalCess,tds_rate=:tdsRate,tds_section_no=:tdsSectionNo,tds_amount=:tdsAmount, total_amount=:totalAmount, roundup_amount=:totalRoundupAmount, billing_address=:billingAddress, billing_city=:billingCity, "+
					    " billing_state=:billingState, billing_zip=:billingZip, shipping_address=:shippingAddress,shipping_city=:shippingCity,shipping_state=:shippingState,shipping_zip=:shippingZip," +
						" notes=:notes, update_date=:updatedDate,currency_id=:currencyID,currency_rate=:currencyRate, invoice_type_id=:invoiceTypeID, gst_invoice_type_id=:gstInvoiceTypeID ,invoice_tags=:invoiceTags, updated_by=:updatedBy,port_code=:portCode,shipping_bill_no=:shippingBillNo, shipping_date=:shippingDate where id=:invoiceID ";
				
				Map hashMap = new HashMap();

				hashMap.put("orgID",invoiceVO.getOrgID());
				hashMap.put("invoiceID", invoiceVO.getInvoiceID());
				hashMap.put("docID", invoiceVO.getDocID());
				hashMap.put("invoiceDate",invoiceVO.getInvoiceDate());			
				hashMap.put("dueDate", invoiceVO.getDueDate());
				hashMap.put("invoiceAmount", invoiceVO.getInvoiceAmount());
				hashMap.put("statusID",invoiceVO.getPaymentStatus());
				hashMap.put("gstIN", invoiceVO.getMyGSTIN());
				hashMap.put("entityGSTIN", invoiceVO.getCustomerGSTIN());
				hashMap.put("supplyState", invoiceVO.getSupplyState());
				hashMap.put("supplyStateID", invoiceVO.getSupplyStateID());
				hashMap.put("totalDiscount", invoiceVO.getTotalDiscount());
			//	hashMap.put("carriedBalance",invoiceVO.getCarriedBalance());			
				hashMap.put("isReview", invoiceVO.getIsReview());
				hashMap.put("totalCGST", invoiceVO.getTotalCGST());
				hashMap.put("totalIGST", invoiceVO.getTotalIGST());
				hashMap.put("totalSGST", invoiceVO.getTotalSGST());
				hashMap.put("totalUGST", invoiceVO.getTotalUGST());
				hashMap.put("totalCess", invoiceVO.getTotalCess());
				hashMap.put("totalAmount", invoiceVO.getTotalAmount());
				hashMap.put("totalRoundupAmount", invoiceVO.getTotalRoundUpAmount());
				hashMap.put("tdsRate", invoiceVO.getTdsRate());
				hashMap.put("tdsSectionNo", invoiceVO.getTdsSectionNo());
				hashMap.put("tdsAmount", invoiceVO.getTds());
				hashMap.put("billingAddress", invoiceVO.getBillingAddress());
				hashMap.put("billingState", invoiceVO.getBillingState());
				hashMap.put("billingCity", invoiceVO.getBillingCity());
				hashMap.put("billingZip", invoiceVO.getBillingZipCode());
				
				hashMap.put("shippingAddress", invoiceVO.getShippingAddress());
				hashMap.put("shippingState", invoiceVO.getShippingState());
				hashMap.put("shippingCity", invoiceVO.getShippingCity());
				hashMap.put("shippingZip", invoiceVO.getShippingZipCode());
				hashMap.put("notes", invoiceVO.getNotes());
				hashMap.put("updatedDate", invoiceVO.getUpdatedTimestamp());
				hashMap.put("updatedBy", invoiceVO.getUpdatedBy());
				hashMap.put("invoiceTypeID", invoiceVO.getInvoiceTypeID());
				hashMap.put("gstInvoiceTypeID", invoiceVO.getGstInvoiceTypeID());
				hashMap.put("invoiceNo", invoiceVO.getInvoiceNumber());
				hashMap.put("isSales", invoiceVO.getIsSales());
				hashMap.put("isReverse", invoiceVO.getIsReverse());
				hashMap.put("invoiceTags", invoiceVO.getInvoiceTags());
				hashMap.put("currencyID", invoiceVO.getCurrencyID());
				hashMap.put("currencyRate", invoiceVO.getCurrencyRate());
				hashMap.put("portCode", invoiceVO.getPortCode());
				hashMap.put("shippingBillNo", invoiceVO.getShippingBillNo());
				hashMap.put("shippingDate", invoiceVO.getShippingDate());
				
				updateFlag=namedParameterJdbcTemplate.update(sql, hashMap);
				
			} catch (Exception e) {
				logger.error("Exception in updateGSTInvoice "+e);
			}
				
			

			logger.debug("Exit : public int updateGSTInvoice(InvoiceVO invoiceVO)");
			return updateFlag;
		}
		
		
		/* Delete a invoice line items  */
		public int deleteGSTInvoiceLineItems(InvoiceVO invoiceVO){
			int insertFlag=0;
			logger.debug("Entry : public int deleteGSTInvoiceLineItems(InvoiceVO invoiceVO)");
			try{
				String sqlQuery="Delete from invoice_line_item_details where invoice_id=:invoiceID ;";
				Map hashMap=new HashMap();
				hashMap.put("invoiceID", invoiceVO.getInvoiceID());
							
				insertFlag=namedParameterJdbcTemplate.update(sqlQuery, hashMap);
			}
			catch (Exception e) {
				logger.error("Exception : public int deleteGSTInvoiceLineItems "+e);
			}
			
			logger.debug("Exit : public int deleteGSTInvoiceLineItems(InvoiceVO invoiceVO)");
			return insertFlag;		
		}
	
		/* Delete the invoice */
		public int deleteGSTInvoice(InvoiceVO invoiceVO) {
			int insertFlag = 0;
			String condtion="";
			logger.debug("Entry : public int deleteGSTInvoice(int invoiceID,int orgID)"+invoiceVO.getInvoiceID()+invoiceVO.getOrgID());
			if(invoiceVO.getNotes().length()>0){
				condtion=", notes=:notes ";
			}
			try{
				
				String sql="Update invoice_details set is_deleted=1,updated_by=:updatedBy  "+condtion+" where id=:invoiceID and org_id=:orgID";
				
				Map namedParameters = new HashMap();
				namedParameters.put("invoiceID", invoiceVO.getInvoiceID());
				namedParameters.put("orgID",invoiceVO.getOrgID());
				namedParameters.put("notes", invoiceVO.getNotes());
				namedParameters.put("updatedBy", invoiceVO.getUpdatedBy());
				
				insertFlag = namedParameterJdbcTemplate.update(sql,namedParameters);
				
			}catch (Exception e) {
				logger.error("Exception occurred at deleting invoices "+e);
			}
			
			
			logger.debug("Exit : public int deleteGSTInvoice(InvoiceVO invoiceVO)"+insertFlag);
			return insertFlag;
		}
		
		
		/*Permanent Delete the invoice */
		public int permanentDeleteGSTInvoice(InvoiceVO invoiceVO) {
			int insertFlag = 0;
			String condtion="";
			logger.debug("Entry : public int permanentDeleteGSTInvoice(int invoiceID,int orgID)"+invoiceVO.getInvoiceID()+invoiceVO.getOrgID());
			
			try{
				
				String sql="Delete from invoice_details where id=:invoiceID and org_id=:orgID";
				
				Map namedParameters = new HashMap();
				namedParameters.put("invoiceID", invoiceVO.getInvoiceID());
				namedParameters.put("orgID",invoiceVO.getOrgID());
				
				
				insertFlag = namedParameterJdbcTemplate.update(sql,namedParameters);
				
			}catch (Exception e) {
				logger.error("Exception occurred at deleting invoices "+e);
			}
			
			
			logger.debug("Exit : public int permanentDeleteGSTInvoice(InvoiceVO invoiceVO)"+insertFlag);
			return insertFlag;
		}
		
		/* Update invoice lineitem report group  */
		public int updateReportGroupInvoiceLineItem(InvoiceVO invoiceVO) {
			int insertFlag = 0;
			logger.debug("Entry : public int updateReportGroupInvoiceLineItem(int invoiceID,int orgID)"+invoiceVO.getInvLineItemID());
			  java.util.Date today = new java.util.Date();
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			 
		
			try{
				
				String sql="Update invoice_line_item_details set line_item_tags=:lineItemTag, update_date=:updateDate where id=:lineItemID ";
				
				Map namedParameters = new HashMap();
				namedParameters.put("lineItemID", invoiceVO.getInvLineItemID());
				namedParameters.put("lineItemTag",invoiceVO.getInvoiceTags());
    			namedParameters.put("updateDate", currentTimestamp.toString());
				
				insertFlag = namedParameterJdbcTemplate.update(sql,namedParameters);
				
			}catch (Exception e) {
				logger.error("Exception occurred at update report group invoices "+e);
			}
			
			
			logger.debug("Exit : public int updateReportGroupInvoiceLineItem(InvoiceVO invoiceVO)"+insertFlag);
			return insertFlag;
		}
		
		/* Used for changing statuses of invoices i.e. adding is_deleted flag */
		public int changeInvoiceStatus(InvoiceVO invoiceVO) {
			int success=0;
			 java.util.Date today = new java.util.Date();
			 final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			logger.debug("Entry : public int changeInvoiceStatus(InvoiceVO invoiceVO)");
			try {
				String	strSQL = "update invoice_details set is_deleted=:statusID,notes=:notes,update_date=:time, updated_by=:updatedBy WHERE id=:invoiceID and org_id=:orgID;" ;
				// 2. Parameters.
				Map namedParameters = new HashMap();
				namedParameters.put("invoiceID", invoiceVO.getInvoiceID());
				namedParameters.put("notes", invoiceVO.getNotes());
				namedParameters.put("time", currentTimestamp);
				namedParameters.put("updatedBy", invoiceVO.getUpdatedBy());
				namedParameters.put("orgID", invoiceVO.getOrgID());
				namedParameters.put("statusID",invoiceVO.getIsDeleted());
				success = namedParameterJdbcTemplate.update(strSQL,namedParameters);
				
				logger.debug("Exit : public int changeInvoiceStatus(InvoiceVO invoiceVO)"+success);	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.debug("Exception in public int changeInvoiceStatus(InvoiceVO invoiceVO) "+e);
			}
			
			return success;
		}
		
		
		
		/* Get gst summary of items   */
	 public List getGSTSummaryofItems(int invoiceID){
			List gstList=null;			
			String sql="";
			logger.debug("Entry : public List getGSTSummaryofItems(int invoiceID)");		
			try{		
				
			 sql = " SELECT SUM(i.cgst)AS totalC,SUM(i.sgst)AS totalS,SUM(i.ugst)AS totalU,SUM(i.igst)AS totalI,sum(i.cess) as totalCess,h.*,i.hsn_sac_code  "
                 + " FROM invoice_line_item_details i ,product_service_legder_relation p,hsn_sac_master h "
                 + " WHERE i.invoice_id=:invoiceID AND p.id=i.item_id  AND p.hsn_sac_id=h.id GROUP BY h.item_code ORDER BY h.item_code ";
								  	
			logger.debug("Query is "+sql);

		Map hashMap = new HashMap();
		hashMap.put("invoiceID",invoiceID);
	
		RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				InLineItemsVO inLineItemsVO=new InLineItemsVO();
				inLineItemsVO.setItemID(rs.getString("item_code"));
				inLineItemsVO.setHsnSacCode(rs.getString("hsn_sac_code"));
				inLineItemsVO.setcGST(rs.getBigDecimal("totalC"));
				inLineItemsVO.setsGST(rs.getBigDecimal("totalS"));
				inLineItemsVO.setuGST(rs.getBigDecimal("totalU"));
				inLineItemsVO.setiGST(rs.getBigDecimal("totalI"));
				inLineItemsVO.setGstPercent(rs.getBigDecimal("gst"));	
				inLineItemsVO.setCessPercent(rs.getBigDecimal("cess"));
				inLineItemsVO.setCess(rs.getBigDecimal("totalCess"));
				return inLineItemsVO;
			}
		};
			
		     gstList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
			
			}catch(Exception e){
				logger.error("Exception in getGSTInvoicesForOrg "+e);
			}
			logger.debug("Exit : public List getGSTSummaryofItems(int invoiceID)"+gstList.size());
			return gstList;		
		}
	 
	 /* Get gst summary of items   */
	 public List getGSTSummaryofItemsForBills(int ledgerID,String fromDate,String toDate,int orgID){
			List gstList=null;			
			String sql="";
			logger.debug("Entry :  public List getGSTSummaryofItemsForBills(int ledgerID,String fromDate,String toDate,int orgID)");		
			try{		
				
			 sql = " SELECT SUM(i.cgst)AS totalC,SUM(i.sgst)AS totalS,SUM(i.ugst)AS totalU,SUM(i.igst)AS totalI, sum(i.cess) as totalCess, h.*,i.hsn_sac_code "
                 + " FROM invoice_line_item_details i ,product_service_legder_relation p,hsn_sac_master h ,invoice_details ii"
                 + " WHERE ii.entity_id=:ledgerID and i.invoice_id=ii.id and ii.org_id=:orgID and (ii.invoice_date between :fromDate and :toDate ) AND ii.is_deleted=0 AND p.id=i.item_id  AND p.hsn_sac_id=h.id GROUP BY h.item_code ORDER BY h.item_code ";
								  	
			logger.debug("Query is "+sql);

		Map hashMap = new HashMap();
		hashMap.put("ledgerID",ledgerID);
		hashMap.put("orgID",orgID);
		hashMap.put("fromDate", fromDate);
		hashMap.put("toDate",toDate);
	
		RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				InLineItemsVO inLineItemsVO=new InLineItemsVO();
				inLineItemsVO.setItemID(rs.getString("item_code"));
				inLineItemsVO.setHsnSacCode(rs.getString("hsn_sac_code"));
				inLineItemsVO.setcGST(rs.getBigDecimal("totalC"));
				inLineItemsVO.setsGST(rs.getBigDecimal("totalS"));
				inLineItemsVO.setuGST(rs.getBigDecimal("totalU"));
				inLineItemsVO.setiGST(rs.getBigDecimal("totalI"));
				inLineItemsVO.setGstPercent(rs.getBigDecimal("gst"));	
				inLineItemsVO.setCessPercent(rs.getBigDecimal("cess"));
				inLineItemsVO.setCess(rs.getBigDecimal("totalCess"));
				return inLineItemsVO;
			}
		};
			
		     gstList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
			
			}catch(Exception e){
				logger.error("Exception in  public List getGSTSummaryofItemsForBills(int ledgerID,String fromDate,String toDate,int orgID) "+e);
			}
			logger.debug("Exit :  public List getGSTSummaryofItemsForBills(int ledgerID,String fromDate,String toDate,int orgID)"+gstList.size());
			return gstList;		
		}
	
		public List getTDSCalculationReport(TransactionVO txVO){
			List tdsTxList=null;
			
			logger.debug("Entry : public List getTDSCalculationReport(TransactionVO txVO)");
		
			try{
				SocietyVO societyVO=societyService.getSocietyDetails(txVO.getSocietyID());
				String sql = " SELECT * FROM (SELECT ats.tx_number AS invoice_no ,s.tds_section_no,s.tds_rate,ats.tx_id,ats.tx_date,vr.vendor_ledger_id,s.invoice_date,vr.service_name,t.pan_no,t.authorized_person_name,(ats.amount+s.tds_amount-s.tax_amount) AS net_bill_amount,s.tax_amount,((ats.amount+s.tds_amount)) AS gross_total,s.tds_amount,ats.amount AS net_payable FROM vendor_ledger_relation vr,vendor_details v,tax_details t,account_transactions_"+societyVO.getDataZoneID()+" ats,soc_tx_meta s ,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,  "+ 
						 "  tds_rate_details tds WHERE vr.society_id=s.society_id AND vr.vendor_id=v.vendor_id AND v.vendor_id=t.entity_id AND t.entity='V' AND ats.tx_type='Journal' and ats.org_id=ale.org_id and ats.org_id=s.society_id and  s.society_id=:societyID "+
						 " AND vr.vendor_ledger_id=ale.ledger_id  AND vr.tds_section_id=tds.id AND tds.section_no=s.tds_section_no  AND ale.tx_id=ats.tx_id AND ats.tx_id=s.tx_id AND (ats.tx_date BETWEEN :fromDate AND :toDate) AND ats.is_deleted=0 GROUP BY ats.tx_id,vendor_ledger_id ORDER BY ats.tx_date) AS tds "+
						 " LEFT JOIN (SELECT SUM(r.amount) AS paidAmt,r.invoice_id,r.bill_id,receipt_id FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" r,account_transactions_"+societyVO.getDataZoneID()+" a WHERE r.org_id=a.org_id and a.org_id=:societyID AND  r.receipt_id=a.tx_id AND a.is_deleted=0  GROUP BY bill_id ) AS l ON tds.tx_id=l.bill_id ORDER BY tds.tx_date ;";
						  	
			logger.debug("Query is "+sql);

		Map hashMap = new HashMap();

		hashMap.put("societyID",txVO.getSocietyID());
		hashMap.put("fromDate", txVO.getFromDate());
		hashMap.put("toDate", txVO.getToDate());


		RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				TDSDetailsVO tdsVO=new TDSDetailsVO();
				
				tdsVO.setInvoiceNo(rs.getString("invoice_no"));			
				tdsVO.setInvoiceDate(rs.getString("invoice_date"));
				tdsVO.setParticulars(rs.getString("service_name"));
				tdsVO.setAuthorizedPersonName(rs.getString("authorized_person_name"));
				tdsVO.setVendorPANNo(rs.getString("pan_no"));
				tdsVO.setNetBillAmount(rs.getBigDecimal("net_bill_amount"));
				tdsVO.setTaxAmount(rs.getBigDecimal("tax_amount"));
				tdsVO.setTdsSection(rs.getString("tds_section_no"));
				tdsVO.setTdsRate(rs.getBigDecimal("tds_rate"));
				tdsVO.setGrossTotal(rs.getBigDecimal("gross_total"));
				tdsVO.setTdsAmount(rs.getBigDecimal("tds_amount"));
				tdsVO.setNetPayableAmount(rs.getBigDecimal("net_payable"));
				tdsVO.setPaidAmount(rs.getBigDecimal("paidAmt"));
				
				return tdsVO;
			}
		};
			
		tdsTxList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
			
			}catch(Exception e){
				logger.error("Exception in public List getTDSCalculationReport(TransactionVO txVO) "+e);
			}
			logger.debug("Exit : public List getTDSCalculationReport(TransactionVO txVO)"+tdsTxList.size());
			return tdsTxList;		
		}
		
		/* Get gst invoices for org   */
		public List getTDSCalculationRptOnInvoices(InvoiceVO invoiceVO){
			List invoiceList=null;
			String statusCondition="";
			String invoiceTypeCondition="";
			String sql="";
			logger.debug("Entry : public List getGSTInvoicesForOrg(InvoiceVO invoiceVO)");
		
			try{
		
			SocietyVO societyVO=societyService.getSocietyDetails(invoiceVO.getOrgID());
			 sql =" SELECT invTable.*, IFNULL((ats.txBalanceAmount-(IFNULL(paidAmt,0))),0) AS pBalance   FROM (SELECT l.ledger_name ,inv.*,m.full_name As creatorName,mi.full_name As updatorName,t.invoice_type AS chargeName ,a.profile_name,a.email,a.mobile,a.pan_no,a.tan_no,a.pan_name  FROM account_ledgers_"+societyVO.getDataZoneID()+" l, invoice_details inv, um_users m, um_users mi, in_invoice_type_details t ,account_ledger_profile_details a" 
                      + " WHERE l.id=inv.entity_id   AND inv.org_id=:orgID   AND inv.is_review=0 AND inv.is_deleted=0  AND inv.org_id=a.org_id AND inv.entity_id=a.ledger_id AND inv.entity_type=a.profile_type " 
                      + " AND ( inv.invoice_date BETWEEN :fromDate AND :toDate ) and m.id=inv.created_by and mi.id=inv.updated_by AND inv.invoice_type_id=t.id AND inv.invoice_type='Purchase' ) AS invTable "
                      + " LEFT JOIN (SELECT IFNULL(amount,0) AS txBalanceAmount,IFNULL(invoice_id,0) AS invoiceID FROM account_transactions_"+societyVO.getDataZoneID()+" where org_id=:orgID )AS ats ON invoiceID=invTable.id "
                      + " LEFT JOIN (SELECT SUM(r.amount) AS paidAmt,r.invoice_id,receipt_id FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" r,account_transactions_"+societyVO.getDataZoneID()+" a WHERE a.org_id=r.org_ID and a.org_id=:orgID and r.receipt_id=a.tx_id AND a.is_deleted=0  GROUP BY invoice_id ) AS b ON invTable.id=b.invoice_id "
                      +  " ORDER BY invTable.id ";
			
						  	
			logger.debug("Query is "+sql);

		Map hashMap = new HashMap();

		hashMap.put("orgID",invoiceVO.getOrgID());
		hashMap.put("fromDate", invoiceVO.getFromDate());
		hashMap.put("toDate", invoiceVO.getUptoDate());
		
		RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				TDSDetailsVO tdsVO=new TDSDetailsVO();
				
				tdsVO.setInvoiceNo(rs.getString("invoice_number"));			
				tdsVO.setInvoiceDate(rs.getString("invoice_date"));
				tdsVO.setParticulars(rs.getString("ledger_name"));
				tdsVO.setAuthorizedPersonName(rs.getString("profile_name"));
				tdsVO.setVendorPANName(rs.getString("pan_name"));
				tdsVO.setVendorPANNo(rs.getString("pan_no"));
				tdsVO.setNetBillAmount(rs.getBigDecimal("invoice_amount"));
				tdsVO.setTaxAmount(rs.getBigDecimal("total_cgst").add(rs.getBigDecimal("total_igst")).add(rs.getBigDecimal("total_sgst")).add(rs.getBigDecimal("total_ugst")));
				tdsVO.setTdsSection(rs.getString("tds_section_no"));
				tdsVO.setTdsRate(rs.getBigDecimal("tds_rate"));
				tdsVO.setGrossTotal(rs.getBigDecimal("total_amount").add(rs.getBigDecimal("tds_amount")));
				tdsVO.setTdsAmount(rs.getBigDecimal("tds_amount"));
				tdsVO.setNetPayableAmount(rs.getBigDecimal("total_amount"));
				tdsVO.setPaidAmount(rs.getBigDecimal("total_amount").subtract(rs.getBigDecimal("pBalance")));
				return tdsVO;
				
				
			}
		};
			
			invoiceList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
			
			}catch(Exception e){
				logger.error("Exception in getGSTInvoicesForOrg "+e);
			}
			logger.debug("Exit : public List getGSTInvoicesForOrg(InvoiceVO invoiceVO)"+invoiceList.size());
			return invoiceList;		
		}
		/* Get gst chapter list   */
		 public List getGSTChapterList(){
				List chapterList=null;			
				String sql="";
				logger.debug("Entry : public List getGSTChapterList()");		
				try{		
					
				 sql =" SELECT * FROM gst_chapter_details ";
									  	
				logger.debug("Query is "+sql);

			Map hashMap = new HashMap();
						
			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					ItemDetailsVO itemDetailsVO=new ItemDetailsVO();
					itemDetailsVO.setChapterID(rs.getInt("id"));
					itemDetailsVO.setChapterName(rs.getString("chapter"));
					itemDetailsVO.setChapterDescription(rs.getString("description"));				
					return itemDetailsVO;
				}
			};
				
			chapterList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
				
				}catch(Exception e){
					logger.error("Exception in getGSTChapterList "+e);
				}
				logger.debug("Exit : public List getGSTChapterList()"+chapterList.size());
				return chapterList;		
			}
		 
		 
		 /* Get hsn sac list by chapterID    */
		 public List getHsnSacList(int chapterID){
				List hsnSacList=null;			
				String sql="";
				logger.debug("Entry :  public List getHsnSacList(int chapterID)");		
				try{		
					
				 sql =" SELECT * FROM hsn_sac_master WHERE chapter_id=:chapterID ";
									  	
				logger.debug("Query is "+sql);

			Map hashMap = new HashMap();
			hashMap.put("chapterID", chapterID);
			
			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					
					ItemDetailsVO itemDetailsVO=new ItemDetailsVO();
					itemDetailsVO.setItemID(rs.getInt("id"));
					itemDetailsVO.setHsnsacCode(rs.getString("item_code"));
					itemDetailsVO.setDescription(rs.getString("description"));
					itemDetailsVO.setChapterID(rs.getInt("chapter_id"));
					itemDetailsVO.setType(rs.getString("type"));	
					itemDetailsVO.setGst(rs.getBigDecimal("gst"));	
					itemDetailsVO.setCess(rs.getBigDecimal("cess"));	
					
					return itemDetailsVO;
				}
			};
				
			hsnSacList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
				
				}catch(Exception e){
					logger.error("Exception in getHsnSacList "+e);
				}
				logger.debug("Exit :  public List getHsnSacList(int chapterID)"+hsnSacList.size());
				return hsnSacList;		
			}
		 
		 /* Get product service list by org_id   */
		 public List getProductServiceLedgerRelationList(int orgID){
				List hsnSacList=null;			
				String sql="";
				logger.debug("Entry : public List getHsnSacLedgerRelationList(int orgID)");		
				try{		
					SocietyVO societyVO=societyService.getSocietyDetails(orgID);
						 sql="SELECT s.ledgerId AS salesLedgerID,s.salesLedgerName, ps.ledgerId AS purchaseLedgerID,ps.purchaseLedgerName, r.*,r.id AS productID,r.org_id,r.name,r.description AS itemDescription,c.chapter,c.description AS chapterDescription,IFNULL(c.gstPerc,0)AS gstPerc,IFNULL(c.cessPerc,0)AS cessPerc,c.*, gc.gstCategoryID, gc.gstCategoryName  "
							 + " FROM (SELECT r.* FROM product_service_legder_relation r WHERE r.org_id=:orgID  ) AS r "
							 + " LEFT JOIN (SELECT ledger_name AS purchaseLedgerName ,id  AS ledgerId FROM account_ledgers_"+societyVO.getDataZoneID()+"  WHERE  org_id=:orgID ) AS ps ON r.purchase_ledger_id=ps.ledgerId "
							 + " LEFT JOIN (SELECT ledger_name AS salesLedgerName,id  AS ledgerId   FROM account_ledgers_"+societyVO.getDataZoneID()+"  WHERE org_id=:orgID  ) AS s ON r.sales_ledger_id=s.ledgerId "
							 + " LEFT JOIN ( SELECT gc.id AS gstCategoryID, gc.name AS gstCategoryName FROM gst_category_details gc ) AS gc ON r.gst_category_id=gc.gstCategoryID "
							 + " LEFT JOIN ( SELECT h.*,c.chapter,c.description AS chapterDescription,IFNULL(h.gst,0)AS gstPerc,IFNULL(h.cess,0)AS cessPerc  FROM hsn_sac_master h, gst_chapter_details c WHERE c.id=h.chapter_id) AS c ON c.id=r.hsn_sac_id";
					 
					 logger.debug("Query is "+sql);

			Map hashMap = new HashMap();
			hashMap.put("orgID", orgID);
			
			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					
					ItemDetailsVO itemDetailsVO=new ItemDetailsVO();
					itemDetailsVO.setItemID(rs.getInt("productID"));
					itemDetailsVO.setHsnSacID(rs.getString("hsn_sac_id"));
					itemDetailsVO.setHsnsacCode(rs.getString("item_code"));
					itemDetailsVO.setItemName(rs.getString("name"));
					itemDetailsVO.setDescription(rs.getString("itemDescription"));
					itemDetailsVO.setChapterID(rs.getInt("chapter_id"));
					itemDetailsVO.setType(rs.getString("type"));	
					itemDetailsVO.setGst(rs.getBigDecimal("gstPerc"));	
					itemDetailsVO.setCess(rs.getBigDecimal("cessPerc"));	
					itemDetailsVO.setSalesLedgerName(rs.getString("salesLedgerName"));
					itemDetailsVO.setSalesLedgerID(rs.getString("salesLedgerID"));	
					itemDetailsVO.setPurchaseLedgerID(rs.getString("purchaseLedgerID"));	
					itemDetailsVO.setPurchaseLedgerName(rs.getString("purchaseLedgerName"));	
					itemDetailsVO.setItemType(rs.getString("item_type"));
					itemDetailsVO.setLedgerType(rs.getString("ledger_type"));	
					itemDetailsVO.setOrgID(rs.getInt("org_id"));	
					itemDetailsVO.setPurchasePrice(rs.getBigDecimal("purchase_price"));
					itemDetailsVO.setSellingPrice(rs.getBigDecimal("selling_price"));
					itemDetailsVO.setUnitOfMeasurement(rs.getString("unit_of_measurement"));
					itemDetailsVO.setIsActive(rs.getInt("is_active"));
					itemDetailsVO.setChapterName(rs.getString("chapter"));
					itemDetailsVO.setChapterDescription(rs.getString("chapterDescription"));
					itemDetailsVO.setProductCode(rs.getString("product_code"));
					itemDetailsVO.setProductBrand(rs.getString("brand"));
					itemDetailsVO.setTags(rs.getString("tags"));
					itemDetailsVO.setGstCategoryID(rs.getInt("gstCategoryID"));
					itemDetailsVO.setGstCategoryName(rs.getString("gstCategoryName"));	
					return itemDetailsVO;
				}
			};
				
			hsnSacList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
				
				}catch(Exception e){
					logger.error("Exception in getHsnSacLedgerRelationList "+e);
				}
				logger.debug("Exit : public List getProductServiceLedgerRelationList(int orgID)"+hsnSacList.size());
				return hsnSacList;		
			}
		 
		 /* Get product service details of given ItemID   */
		 public ItemDetailsVO getProductServiceLedgerDetails(int itemID,int orgID){
				ItemDetailsVO hsnSacVO=new ItemDetailsVO();			
				String sql="";
				logger.debug("Entry : public ItemDetailsVO getProductServiceLedgerDetails(int itemID,int orgID)");		
				try{		
					SocietyVO societyVO=societyService.getSocietyDetails(orgID);
					 sql =" SELECT s.sales_ledger_id AS salesLedgerID,s.salesLedgerName, ps.purchase_ledger_id AS purchaseLedgerID,ps.purchaseLedgerName, r.*,r.id AS productID,r.org_id,r.name,r.description AS itemDescription,c.chapter,c.description AS chapterDescription,IFNULL(c.gstPerc,0)AS gstPerc,IFNULL(c.cessPerc,0)AS cessPerc,c.*, gc.gstCategoryID, gc.gstCategoryName  "
						+ " FROM (SELECT r.* FROM product_service_legder_relation r WHERE r.org_id=:orgID  and r.id=:itemID   ) AS r "
						+ " LEFT JOIN (SELECT l.ledger_name AS purchaseLedgerName ,p.purchase_ledger_id  FROM product_service_legder_relation p,account_ledgers_"+societyVO.getDataZoneID()+" l WHERE p.purchase_ledger_id=l.id AND p.org_id=l.org_id and p.org_id=:orgID ) AS ps ON r.purchase_ledger_id=ps.purchase_ledger_id "
						+ " LEFT JOIN (SELECT l.ledger_name AS salesLedgerName,p.sales_ledger_id   FROM product_service_legder_relation p,account_ledgers_"+societyVO.getDataZoneID()+" l WHERE p.sales_ledger_id=l.id AND p.org_id=l.org_id and p.org_id=:orgID GROUP BY l.id  ) AS s ON r.sales_ledger_id=s.sales_ledger_id "
						+ " LEFT JOIN ( SELECT gc.id AS gstCategoryID, gc.name AS gstCategoryName FROM gst_category_details gc ) AS gc ON r.gst_category_id=gc.gstCategoryID "
						+ " LEFT JOIN ( SELECT h.*,c.chapter,c.description AS chapterDescription,IFNULL(h.gst,0)AS gstPerc,IFNULL(h.cess,0)AS cessPerc  FROM hsn_sac_master h, gst_chapter_details c WHERE c.id=h.chapter_id) AS c ON c.id=r.hsn_sac_id ";
									  	
				logger.debug("Query is "+sql);

			Map hashMap = new HashMap();
			hashMap.put("orgID", orgID);
			hashMap.put("itemID", itemID);
			
			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					
					ItemDetailsVO itemDetailsVO=new ItemDetailsVO();
					itemDetailsVO.setItemID(rs.getInt("productID"));
					itemDetailsVO.setHsnSacID(rs.getString("hsn_sac_id"));
					itemDetailsVO.setHsnsacCode(rs.getString("item_code"));
					itemDetailsVO.setItemName(rs.getString("name"));
					itemDetailsVO.setDescription(rs.getString("itemDescription"));
					itemDetailsVO.setChapterID(rs.getInt("chapter_id"));
					itemDetailsVO.setType(rs.getString("type"));	
					itemDetailsVO.setGst(rs.getBigDecimal("gstPerc"));	
					itemDetailsVO.setCess(rs.getBigDecimal("cessPerc"));	
					itemDetailsVO.setSalesLedgerName(rs.getString("salesLedgerName"));
					itemDetailsVO.setSalesLedgerID(rs.getString("salesLedgerID"));	
					itemDetailsVO.setPurchaseLedgerID(rs.getString("purchaseLedgerID"));	
					itemDetailsVO.setPurchaseLedgerName(rs.getString("purchaseLedgerName"));	
					itemDetailsVO.setItemType(rs.getString("item_type"));
					itemDetailsVO.setLedgerType(rs.getString("ledger_type"));
					itemDetailsVO.setOrgID(rs.getInt("org_id"));	
					itemDetailsVO.setPurchasePrice(rs.getBigDecimal("purchase_price"));
					itemDetailsVO.setSellingPrice(rs.getBigDecimal("selling_price"));
					itemDetailsVO.setUnitOfMeasurement(rs.getString("unit_of_measurement"));
					itemDetailsVO.setIsActive(rs.getInt("is_active"));
					itemDetailsVO.setChapterName(rs.getString("chapter"));
					itemDetailsVO.setChapterDescription(rs.getString("chapterDescription"));
					itemDetailsVO.setProductCode(rs.getString("product_code"));
					itemDetailsVO.setProductBrand(rs.getString("brand"));
					itemDetailsVO.setTags(rs.getString("tags"));
					itemDetailsVO.setGstCategoryID(rs.getInt("gstCategoryID"));
					itemDetailsVO.setGstCategoryName(rs.getString("gstCategoryName"));	
					return itemDetailsVO;
				}
			};
				
			hsnSacVO=(ItemDetailsVO)namedParameterJdbcTemplate.queryForObject(sql, hashMap,rMapper);
				
				}catch(Exception e){
					logger.error("Exception in public ItemDetailsVO getProductServiceLedgerDetails(int itemID,int orgID) "+e);
				}
				logger.debug("Exit : public ItemDetailsVO getProductServiceLedgerDetails(int itemID,int orgID)");
				return hsnSacVO;		
			}
		 
		 public int insertProductService(ItemDetailsVO itemDetailsVO) {
				int insertFlag = 0;
				int generatedID = 0;
		
				try {

					logger.debug("Entry : public int insertProductService(ItemDetailsVO itemDetailsVO)");
					String query = "INSERT INTO `product_service_legder_relation` (`org_id`,`hsn_sac_id`,`sales_ledger_id`,`purchase_ledger_id`,`ledger_type`,`item_type`,`name`,`description`,`purchase_price`,`selling_price`,`unit_of_measurement`,`tds_section_id`,`product_code`,`brand`,`tags`,`gst_category_id`) "
							+ " VALUES ( :orgID, :hsnSacID, :salesLedgerID, :purchaseLedgerID, :ledgerType, :itemType, :itemName, :description, :purchasePrice, :sellingPrice, :unitOfMeasurement,:tdsSectionID, :productCode, :productBrand, :tags, :gstCategoryID) ";
							
					SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(
							itemDetailsVO);
					KeyHolder keyHolder = new GeneratedKeyHolder();
					insertFlag = getNamedParameterJdbcTemplate().update(query,
							fileParameters, keyHolder);
					generatedID = keyHolder.getKey().intValue();
					logger.debug("key GEnereator ---" + generatedID);
				
				} catch (DuplicateKeyException e) {
					logger.info("Product already present " );
					insertFlag=534;
				} catch (Exception e) {
					logger.error("Exception at insertProductService " + e);
				}
				logger.debug("Exit :public int insertProductService(ItemDetailsVO itemDetailsVO)");
				return insertFlag;
			}
		 
		 
		
			public int updateProductService(ItemDetailsVO itemDetailsVO) {
				int updateFlag = 0;
				logger.debug("Entry : public int updateProductService(ItemDetailsVO itemDetailsVO)");
				
				try {
					String sql="Update product_service_legder_relation set hsn_sac_id=:hsnSacID, sales_ledger_id=:salesLedgerID, purchase_ledger_id=:purchaseLedgerID, ledger_type=:ledgerType, "
							+ " name=:itemName,description=:description, purchase_price=:purchasePrice, selling_price=:sellingPrice, unit_of_measurement=:unit,tds_section_id=:tdsSectionID, item_type=:itemType, "
							+ " product_code=:productCode, brand=:brand, tags=:tags,gst_category_id=:gstCategoryID where id=:itemID ";
					
					Map hashMap = new HashMap();

					hashMap.put("itemID",itemDetailsVO.getItemID());
					hashMap.put("salesLedgerID", itemDetailsVO.getSalesLedgerID());
					hashMap.put("purchaseLedgerID", itemDetailsVO.getPurchaseLedgerID());
					hashMap.put("ledgerType", itemDetailsVO.getLedgerType());
					hashMap.put("itemName", itemDetailsVO.getItemName());
					hashMap.put("description", itemDetailsVO.getDescription());
					hashMap.put("purchasePrice",itemDetailsVO.getPurchasePrice());	
					hashMap.put("sellingPrice",itemDetailsVO.getSellingPrice());
					hashMap.put("unit", itemDetailsVO.getUnitOfMeasurement());
					hashMap.put("hsnSacID",itemDetailsVO.getHsnSacID());
					hashMap.put("tdsSectionID", itemDetailsVO.getTdsSectionID());
					hashMap.put("productCode", itemDetailsVO.getProductCode());
					hashMap.put("brand", itemDetailsVO.getProductBrand());
					hashMap.put("tags", itemDetailsVO.getTags());
					hashMap.put("gstCategoryID", itemDetailsVO.getGstCategoryID());		
					hashMap.put("itemType", itemDetailsVO.getItemType());
			
					updateFlag=namedParameterJdbcTemplate.update(sql, hashMap);
					
				} catch (Exception e) {
					logger.error("Exception in updateProductService "+e);
				}
					
				logger.debug("Exit : public int updateProductService(ItemDetailsVO itemDetailsVO)");
				return updateFlag;
			}
		 
		
			public int deleteProductService(ItemDetailsVO itemDetailsVO) {
				int insertFlag = 0;			
				String sql="";
				logger.debug("Entry : public int deleteProductService(ItemDetailsVO itemDetailsVO)"+itemDetailsVO.getItemID()+itemDetailsVO.getOrgID());
				try{
					if(itemDetailsVO.getIsActive()==2){
						sql=" Delete from product_service_legder_relation where id=:itemID and org_id=:orgID ";	
					}else{
					 sql=" Update product_service_legder_relation set is_active=:active  where id=:itemID and org_id=:orgID ";	
					}
					
					logger.debug("Query is "+sql);
					
					Map namedParameters = new HashMap();
					namedParameters.put("itemID", itemDetailsVO.getItemID());
					namedParameters.put("orgID", itemDetailsVO.getOrgID());
					namedParameters.put("active", itemDetailsVO.getIsActive());
					
					insertFlag = namedParameterJdbcTemplate.update(sql,namedParameters);
					
				}catch (Exception e) {
					logger.error("Exception occurred at deleteProductService "+e);
				}
				
				
				logger.debug("Exit : public int deleteProductService(ItemDetailsVO itemDetailsVO) "+insertFlag);
				return insertFlag;
			}
			
			 public Boolean checkProuductServicelinkToInvoice(int itemID){
					String strSQLQuery = "";
					Boolean isPresent=true;
					int count=0;
					try{
						logger.debug("Entry :  public Boolean checkProuductServicelinkToInvoice(int itemID)"+itemID );
		                 //	1. SQL Query
						strSQLQuery = " SELECT item_id FROM  invoice_line_item_details WHERE item_id=:itemID  LIMIT 1 " ;				
						logger.debug("query : " + strSQLQuery);		
						
						 Map hMap=new HashMap();
						 hMap.put("itemID", itemID);
												 			 
						 count=namedParameterJdbcTemplate.queryForObject(strSQLQuery, hMap, Integer.class);
						 if(count>0){
							 isPresent=true; 
						 }
						logger.debug("Exit :public Boolean checkProuductServicelinkToInvoice(int itemID)"+isPresent);
						
					}catch(EmptyResultDataAccessException e){
						logger.debug("Proudct is not link to any invoice");
						isPresent=false;
				
					}catch(Exception ex){
				         logger.error("Exception in checkProuductServicelinkToInvoice : "+ex);       
				         isPresent=false;
				    }
				   logger.debug("Exit : public Boolean checkProuductServicelinkToInvoice(int itemID)");
					return isPresent;	
				} 
			 
			 public int validateHsnSacNo(String hsnSacNo){
					String strSQLQuery = "";					
					int hsnSacID=0;
					try{
						logger.debug("Entry :  public String validateHsnSacNo(int hsnSacNo)"+hsnSacNo );
		                 //	1. SQL Query
						strSQLQuery = " SELECT id FROM  hsn_sac_master WHERE item_code=:hsnSacNo  LIMIT 1 " ;				
						logger.debug("query : " + strSQLQuery);		
						
						 Map hMap=new HashMap();
						 hMap.put("hsnSacNo", hsnSacNo);
												 			 
						 hsnSacID=namedParameterJdbcTemplate.queryForObject(strSQLQuery, hMap, Integer.class);
						 
						logger.debug("Exit :public int validateHsnSacNo(int hsnSacNo)"+hsnSacID);
						
					}catch(EmptyResultDataAccessException e){
						logger.debug("Hsn or Sac code is not valid");
						hsnSacID=0;
				
					}catch(Exception ex){
				         logger.error("Exception String validateHsnSacNo : "+ex);       
				         hsnSacID=0;
				    }
				   logger.debug("Exit : public String validateHsnSacNo(int hsnSacNo)");
					return hsnSacID;	
				} 
	
	
			 /* Get Balance of invoice in given period for a ledger */
				public List getInvoiceBalanceInGivenPeriod(int ledgerID,int orgID,String fromDate,String toDate){
					logger.debug("public List getInvoiceListInGivenPeriod(int ledgerID,int orgID,String fromDate,String toDate)");
				List invoiceList=new ArrayList<>();
					
					try {
						SocietyVO societyVO=societyService.getSocietyDetails(orgID);
						String strQuery = "SELECT invTable.*, IFNULL((ats.txBalanceAmount-(IFNULL(paidAmt,0))),0) AS pBalance   FROM (SELECT l.ledger_name ,inv.*,m.full_name AS creatorName,mi.full_name AS updatorName,t.invoice_type AS chargeName  FROM account_ledgers_"+societyVO.getDataZoneID()+" l, invoice_details inv, um_users m, um_users mi, in_invoice_type_details t "+
															" WHERE l.id=inv.entity_id   AND inv.org_id=l.org_id and  inv.org_id=:orgID   AND inv.is_review=0 AND inv.is_deleted=0  AND entity_id=:ledgerID AND penalty_applicable=1 "+
															" AND ( inv.invoice_date BETWEEN :fromDate AND :toDate ) AND m.id=inv.created_by AND mi.id=inv.updated_by AND inv.invoice_type_id=t.id ) AS invTable "+ 
															" LEFT JOIN (SELECT IFNULL(amount,0) AS txBalanceAmount,IFNULL(invoice_id,0) AS invoiceID FROM account_transactions_"+societyVO.getDataZoneID()+" where org_id=:orgID )AS ats ON invoiceID=invTable.id "+
															" LEFT JOIN (SELECT SUM(r.amount) AS paidAmt,r.invoice_id,receipt_id FROM receipt_invoice_mapping_"+orgID+" r,account_transactions_"+orgID+" a WHERE r.org_id=a.org_id and r.receipt_id=a.tx_id and ( a.tx_date BETWEEN '2018-05-2' AND '2018-06-21') AND a.is_deleted=0  GROUP BY invoice_id ) AS b ON invTable.id=b.invoice_id "+
															" ORDER BY invTable.id  ";
								

						Map nameparam = new HashMap();
						nameparam.put("orgID", orgID);
						nameparam.put("ledgerID", ledgerID);
						nameparam.put("fromDate",fromDate);
						nameparam.put("toDate",toDate);

						RowMapper rMapper = new RowMapper() {

							public Object mapRow(ResultSet rs, int rowNum)
									throws SQLException {
								InvoiceVO invoiceEntityVO=new InvoiceVO();
								invoiceEntityVO.setOrgID(rs.getInt("org_id"));
								invoiceEntityVO.setInvoiceID(rs.getInt("id"));
						
								
				
								invoiceEntityVO.setBalance(rs.getBigDecimal("pBalance"));
								
								return invoiceEntityVO;
							}
						};
						
						
						invoiceList =  namedParameterJdbcTemplate.query(strQuery, nameparam, rMapper);
					} catch (Exception e) {
						logger.error("Exception at getInvoiceListInGivenPeriod " + e);
					}
					
					logger.debug("Exit : public List getInvoiceListInGivenPeriod(int ledgerID,int orgID,String fromDate,String toDate)");
					
					return invoiceList;
				}
				
				 /* Get Currency List    */
				 public List getCurrencyList(){
						List currencyList=null;			
						String sql="";
						logger.debug("Entry :  public List getCurrencyList()");		
						try{		
							
						 sql =" SELECT * FROM currency_details  ";
											  	
						logger.debug("Query is "+sql);

					Map hashMap = new HashMap();
					
					
					RowMapper rMapper = new RowMapper() {

						public Object mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							
							CurrencyVO currencyVO=new CurrencyVO();
							currencyVO.setCurrencyID(rs.getInt("id"));
							currencyVO.setCountry(rs.getString("country_name"));
							currencyVO.setCurrencyName(rs.getString("currency_name"));
							currencyVO.setCurrencySymbol(rs.getString("symbol_html"));
							currencyVO.setCurrencyCode(rs.getString("currency_code"));	
							currencyVO.setDecimalCurrency(rs.getString("decimal_currency"));								
							return currencyVO;
						}
					};
						
					currencyList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
						
						}catch(Exception e){
							logger.error("Exception in getCurrencyList "+e);
						}
						logger.debug("Exit :  public List getCurrencyList()"+currencyList.size());
						return currencyList;		
					}
	
				 public List getLinkedInvoiceList(int orgID,int invoiceID){
						List invoiceInvoiceList=null;
					
							logger.debug("Entry :  public List getLinkedInvoiceList(int orgID,int invoiceID)");
							try{
							
								String str= " SELECT a.*,r.id AS seqNo,r.invoice_id,r.linked_invoice_id,r.amount AS recAmt  FROM invoice_linking_details r, invoice_details a "
										  + " WHERE r.org_id=:orgID AND r.invoice_id=:invoiceID AND a.org_id=r.org_id AND r.linked_invoice_id=a.id AND a.is_deleted=0";
						
								logger.debug("query: "+str);
								
								   Map hMap=new HashMap();
								   hMap.put("orgID", orgID);
								   hMap.put("invoiceID", invoiceID);
								   
								  
								   RowMapper Rmapper = new RowMapper() {
										public Object mapRow(ResultSet rs, int rowNum)
												throws SQLException {
										 	InvoiceVO txVO=new InvoiceVO();
										 	txVO.setInvoiceID(rs.getInt("id"));		
											txVO.setInvoiceNumber(rs.getString("invoice_number"));
										 	txVO.setDocID(rs.getString("doc_id"));
											txVO.setCreatedTimestamp(rs.getTimestamp("create_date"));
											txVO.setInvoiceDate(rs.getString("invoice_date"));
											txVO.setInvoiceType(rs.getString("invoice_type"));
											txVO.setTotalAmount(rs.getBigDecimal("recAmt"));
											txVO.setLedgerID(rs.getInt("entity_id"));											
											txVO.setSeqNo(rs.getInt("seqNo"));
											txVO.setLinkedInvoiceID(rs.getInt("linked_invoice_id"));	
											return txVO;
											
										}

									   };
									   invoiceInvoiceList=namedParameterJdbcTemplate.query(
											str, hMap, Rmapper);
								   logger.debug("Exit :  public List getLinkedInvoiceList(int orgID,int invoiceID)"+invoiceInvoiceList.size());
								}catch(EmptyResultDataAccessException Ex){
									logger.info("No Data found at  List getLinkedInvoiceList(int orgID,int invoiceID) "+Ex);
								
							
									}catch(Exception se){
									logger.error("Exception :public List getLinkedInvoiceList(int orgID,int invoiceID)"+se);
								   
								}
							
							logger.debug("Exit : public List getLinkedInvoiceList(int orgID,int ledgerID)");
						return invoiceInvoiceList;
						}
				 
				 public List getUnlinkedInvoiceList(int orgID,int ledgerID, String fromDate, String toDate){
						List invoiceInvoiceList=null;
					
							logger.debug("Entry :  public List getUnlinkedInvoiceList(int orgID,int ledgerID)");
							try{
								
								String str="SELECT a.*,ABS(SUM(a.total_amount))-IFNULL(SUM(r.amount),0)  AS avBal,a.doc_id,a.id,a.invoice_number,r.id As seqNo,r.invoice_id,r.linked_invoice_id,r.amount AS recAmt FROM (SELECT a.* "
										+ "FROM invoice_details a WHERE a.entity_id=:ledgerID AND a.is_deleted=0 AND a.org_id=:orgID AND a.invoice_date>=:fromDate  ) AS a LEFT JOIN ( SELECT i.id,invoice_id,i.linked_invoice_id,SUM(i.amount) AS amount,i.org_id FROM invoice_linking_details i,invoice_details i1,invoice_details i2 WHERE i.invoice_id=i1.id AND i.linked_invoice_id=i2.id AND i1.is_deleted=0 AND i2.is_deleted=0 AND i.org_id=:orgID GROUP BY i.invoice_id,i.linked_invoice_id) r ON r.linked_invoice_id=a.id AND a.org_id=r.org_id GROUP BY a.id;";
						
								logger.debug("query: "+str);
								
								   Map hMap=new HashMap();
								   hMap.put("orgID", orgID);
								   hMap.put("ledgerID", ledgerID);
								   hMap.put("fromDate", fromDate);
								   hMap.put("toDate", toDate);
								  
								   RowMapper Rmapper = new RowMapper() {
										public Object mapRow(ResultSet rs, int rowNum)
												throws SQLException {
										 	InvoiceVO txVO=new InvoiceVO();
										 	txVO.setInvoiceID(rs.getInt("a.id"));		
											txVO.setInvoiceNumber(rs.getString("invoice_number"));
										 	txVO.setDocID(rs.getString("doc_id"));
											txVO.setCreatedTimestamp(rs.getTimestamp("create_date"));
											txVO.setInvoiceDate(rs.getString("invoice_date"));
											txVO.setInvoiceType(rs.getString("invoice_type"));
											txVO.setTotalAmount(rs.getBigDecimal("total_amount"));
											txVO.setLedgerID(rs.getInt("entity_id"));
											txVO.setBalance(rs.getBigDecimal("avBal"));
											txVO.setSeqNo(rs.getInt("seqNo"));
											txVO.setLinkedInvoiceID(rs.getInt("linked_invoice_id"));	
															
											
											return txVO;
											
										}

									   };
									   invoiceInvoiceList=namedParameterJdbcTemplate.query(
											str, hMap, Rmapper);
								   logger.debug("Exit :  public List getUnlinkedInvoiceList(int orgID,int ledgerID)"+invoiceInvoiceList.size());
								}catch(EmptyResultDataAccessException Ex){
									logger.info("No Data found at  List getUnlinkedInvoiceList(int orgID,int invoiceID) "+Ex);
								
							
									}catch(Exception se){
									logger.error("Exception :public List getUnlinkedInvoiceList(int orgID,int ledgerID)"+se);
								   
								}
							
							logger.debug("Exit : public List getUnlinkedRceiptList(int orgID,int ledgerID)");
						return invoiceInvoiceList;
						}
				 
				 
				 /* Update invoice balance with the linked invoice */
					public int insertInvAgainstInvoice(int orgID ,InvoiceVO invoiceVO){
						int success=0;
						logger.debug("Entry : public int insertInvAgainstInvoice(int orgID ,InvoiceDetailsVO invoiceVO)");
						
						try {
							SocietyVO societyVO=societyService.getSocietyDetails(orgID);
							String sqlQuery="INSERT INTO `invoice_linking_details`(`linked_invoice_id`,`invoice_id`,`amount`,`org_id`)" +
											" VALUES ( :linkedInvoiceID,:invoiceID,:balance ,:orgID)"; 
						logger.debug("query : " + sqlQuery);
							Map hmap=new HashMap();
							
							hmap.put("linkedInvoiceID", invoiceVO.getLinkedInvoiceID());
							hmap.put("invoiceID",invoiceVO.getInvoiceID());
							hmap.put("balance", invoiceVO.getPaidInvAmount());	
							hmap.put("orgID", orgID);
							
							success=namedParameterJdbcTemplate.update(sqlQuery, hmap);
							
						} catch (Exception e) {
							logger.error("Exception at public int insertInvAgainstInvoice(int orgID ,InvoiceDetailsVO invoiceVO) "+e);
						}
						
						logger.debug("Exit : public int insertInvAgainstInvoice(int orgID ,InvoiceDetailsVO invoiceVO)");
						return success;		
					}
					
					
					/* Unlink  linkedInvoice from invoice */
					public int unlinkLinkedInvoiceFromInvoice(int orgID ,InvoiceVO invoiceVO){
						int success=0;
						logger.debug("Entry : public int unlinkLinkedInvoiceFromInvoice(int orgID ,InvoiceVO invoiceVO)");
						
						try {
							
							String str = "Delete from invoice_linking_details where id=:seqNo and org_id=:orgID ;";
							 
							   
							   Map hMap=new HashMap();
							   hMap.put("seqNo", invoiceVO.getSeqNo());
							   hMap.put("invoiceID", invoiceVO.getInvoiceID());
							   hMap.put("orgID", orgID);
							   
							   
							   success=namedParameterJdbcTemplate.update(str, hMap);
							
							
						} catch (Exception e) {
							logger.error("Exception at public int unlinkLinkedInvoiceFromInvoice(int orgID ,InvoiceDetailsVO invoiceVO) "+e);
						}
						
						logger.debug("Exit : public int unlinkLinkedInvoiceFromInvoice(int orgID ,InvoiceDetailsVO invoiceVO)");
						return success;		
					}
				 
				 
					public List getAgewiseReceivalesList(int orgID, int isDueDate){
						List receivableList=null;
					
							logger.debug("Entry :  public List getAgewiseReceivalesList(int orgID, int isDueDate)");
							try{
								String strCondition="";
								if(isDueDate==0){
									strCondition="invoice_date";
								}if(isDueDate==1){
									strCondition="due_date";
								}
								SocietyVO societyVO=societyService.getSocietyDetails(orgID);
								String str="SELECT payable.*,SUM(unpaidPayments.avBal) AS unpaidAmt FROM ( SELECT ledger_name,ledgerID,id,invoice_date,entity_type,invoice_type,SUM(pBalance) as balance,SUM(total_amount) as total_amount,SUM(invoice_amount) as invoice_amount, DATEDIFF(CURDATE(), "+strCondition+") AS days_past_due, "+
										" SUM(IF(DATEDIFF(CURDATE(), "+strCondition+") = 0, pBalance, 0)) AS zero, "+
										" SUM(IF(DATEDIFF(CURDATE(), "+strCondition+") BETWEEN 1 AND 30, pBalance, 0)) AS thirty, "+
										" SUM(IF(DATEDIFF(CURDATE(), "+strCondition+") BETWEEN 31 AND 60, pBalance, 0)) AS sixty, "+
										" SUM(IF(DATEDIFF(CURDATE(), "+strCondition+") BETWEEN 61 AND 90, pBalance, 0)) AS ninty, "+
										" SUM(IF(DATEDIFF(CURDATE(), "+strCondition+") BETWEEN 91 AND 365, pBalance, 0)) as nintyPlus "+
										" FROM (SELECT invTable.entity_type,invTable.ledger_name,ledgerID, IFNULL((ats.txBalanceAmount-(IFNULL(paidAmt,0))),0) AS pBalance,id,invoice_date,due_date,invoice_amount,total_amount ,org_id,is_deleted,invoice_type  FROM (SELECT l.ledger_name ,l.id AS ledgerID,inv.*,t.invoice_type AS chargeName,g.invoice_type AS gstChargeName  FROM account_ledgers_"+societyVO.getDataZoneID()+" l, invoice_details inv,  in_invoice_type_details t, in_invoice_type_details  g "+
										" WHERE l.id=inv.entity_id AND inv.org_id=l.org_id AND inv.org_id=:orgID   AND inv.is_review=0 AND inv.is_deleted=0 and inv.invoice_type='Sales' "+
										" AND inv.invoice_type_id=t.id AND inv.gst_invoice_type_id=g.id ) AS invTable "+ 
										" LEFT JOIN (SELECT IFNULL(amount,0) AS txBalanceAmount,IFNULL(invoice_id,0) AS invoiceID FROM account_transactions_"+societyVO.getDataZoneID()+" WHERE org_id=:orgID )AS ats ON invoiceID=invTable.id "+
										" LEFT JOIN (SELECT SUM(r.amount) AS paidAmt,r.invoice_id,receipt_id FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" r,account_transactions_"+societyVO.getDataZoneID()+" a WHERE r.receipt_id=a.tx_id AND r.org_id=a.org_id AND a.org_id=:orgID AND a.is_deleted=0  GROUP BY invoice_id ) AS b ON invTable.id=b.invoice_id ORDER BY invTable.id) AS invoice WHERE org_id=:orgID AND is_deleted=0 GROUP BY ledgerID "+
										" ) AS payable LEFT JOIN (Select a.*,ABS(SUM(a.amt))-IFNULL(SUM(r.amount),0)  AS avBal,r.id,r.receipt_id,r.amount AS recAmt  FROM (SELECT a.*,l.amount AS amt,l.ledger_id,al.ledger_name FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" l,account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups g WHERE al.sub_group_id=g.id AND a.tx_id=l.tx_id AND al.id=l.ledger_id AND a.is_deleted=0  AND l.org_id=:orgID AND a.org_id=l.org_id AND a.org_id=al.org_id AND ( a.tx_type='Receipt'  OR a.tx_type='Credit Note'  ) AND (a.tx_date BETWEEN DATE_SUB(NOW(), INTERVAL 365 DAY) AND CURDATE()) AND g.category_id=1 ) AS a LEFT JOIN ( SELECT id,org_id,receipt_id,invoice_id,SUM(amount) AS amount FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" WHERE org_id=:orgID GROUP BY receipt_id ) r ON r.receipt_id=a.tx_id AND a.org_id=r.org_id GROUP BY a.ledger_id)"+
										" AS unpaidPayments ON payable.ledgerID=unpaidPayments.ledger_id GROUP BY payable.ledgerID;";	
								
						
								logger.debug("query: "+str);
								
								   Map hMap=new HashMap();
								   hMap.put("orgID", orgID);
								   
								  
								   RowMapper Rmapper = new RowMapper() {
										public Object mapRow(ResultSet rs, int rowNum)
												throws SQLException {
										 	InvoiceVO txVO=new InvoiceVO();
										 	txVO.setEntityName(rs.getString("ledger_name"));
										 	txVO.setLedgerID(rs.getInt("ledgerID"));
										 	txVO.setEntityID(rs.getInt("ledgerID"));
										 	txVO.setEntityType(rs.getString("entity_type"));
										 	txVO.setInvoiceType(rs.getString("invoice_type"));
										 	txVO.setBalance(rs.getBigDecimal("balance"));
											txVO.setZeroAmount(rs.getBigDecimal("zero"));
											txVO.setThirtyAmount(rs.getBigDecimal("thirty"));
											txVO.setSixtyAmount(rs.getBigDecimal("sixty"));
											txVO.setNinetyAmount(rs.getBigDecimal("ninty"));
											txVO.setNinetyPlusAmount(rs.getBigDecimal("nintyPlus"));
											txVO.setCarriedBalance(rs.getBigDecimal("unpaidAmt"));
											return txVO;
											
										}

									   };
									   receivableList=namedParameterJdbcTemplate.query(
											str, hMap, Rmapper);
								   logger.debug("Exit :  public List getAgewiseReceivalesList(int orgID, int isDueDate)"+receivableList.size());
								}catch(EmptyResultDataAccessException Ex){
									logger.info("No Data found at  List getAgewiseReceivalesList(int orgID, int isDueDate) "+Ex);
								
							
									}catch(Exception se){
									logger.error("Exception :public List getAgewiseReceivalesList(int orgID, int isDueDate)"+se);
								   
								}
							
							logger.debug("Exit : public List getAgewiseReceivalesList(int orgID, int isDueDate)");
						return receivableList;
						}
					
					public List getAgewisePayablesList(int orgID, int isDueDate){
						List payableList=null;
					
							logger.debug("Entry :  public List getAgewisePayablesList(int orgID, int isDueDate)");
							try{
								String strCondition="";
								if(isDueDate==0){
									strCondition="invoice_date";
								}if(isDueDate==1){
									strCondition="due_date";
								}
								SocietyVO societyVO=societyService.getSocietyDetails(orgID);
								String str="SELECT payable.*,SUM(unpaidPayments.avBal) AS unpaidAmt FROM (SELECT ledger_name,ledgerID,id,invoice_date,entity_type,invoice_type,SUM(pBalance) as balance,SUM(total_amount) as total_amount,SUM(invoice_amount) as invoice_amount, DATEDIFF(CURDATE(), "+strCondition+") AS days_past_due, "+
										" SUM(IF(DATEDIFF(CURDATE(), "+strCondition+") = 0, pBalance, 0)) AS zero, "+
										" SUM(IF(DATEDIFF(CURDATE(), "+strCondition+") BETWEEN -30 AND -1, pBalance, 0)) AS thirty, "+
										" SUM(IF(DATEDIFF(CURDATE(), "+strCondition+") BETWEEN -60 AND -31, pBalance, 0)) AS sixty, "+
										" SUM(IF(DATEDIFF(CURDATE(), "+strCondition+") BETWEEN -90 AND -31, pBalance, 0)) AS ninty, "+
										" SUM(IF(DATEDIFF(CURDATE(), "+strCondition+") BETWEEN -365 AND -91, pBalance, 0)) as nintyPlus, "+
										" SUM(IF(DATEDIFF(CURDATE(), "+strCondition+") BETWEEN 0 AND 365, pBalance, 0)) as overdueAmount, "+
										" SUM(IF(DATEDIFF(CURDATE(), "+strCondition+") BETWEEN 0 AND 30, pBalance, 0)) AS thirtyOverDue, "+
										" SUM(IF(DATEDIFF(CURDATE(), "+strCondition+") BETWEEN 31 AND 60, pBalance, 0)) AS sixtyOverDue, "+
										" SUM(IF(DATEDIFF(CURDATE(), "+strCondition+") BETWEEN 61 AND 90, pBalance, 0)) AS ninetyOverDue, "+
										" SUM(IF(DATEDIFF(CURDATE(), "+strCondition+") BETWEEN 91 AND 365, pBalance, 0)) as ninetyPlusOverDue "+
										" FROM (SELECT invTable.entity_type,invTable.ledger_name,ledgerID, IFNULL((ats.txBalanceAmount-(IFNULL(paidAmt,0))),0) AS pBalance,id,invoice_date,due_date,invoice_amount,total_amount ,org_id,is_deleted,invoice_type  FROM (SELECT l.ledger_name ,l.id AS ledgerID,inv.*,t.invoice_type AS chargeName,g.invoice_type AS gstChargeName  FROM account_ledgers_"+societyVO.getDataZoneID()+" l, invoice_details inv,  in_invoice_type_details t, in_invoice_type_details  g "+
										" WHERE l.id=inv.entity_id AND inv.org_id=l.org_id AND inv.org_id=:orgID   AND inv.is_review=0 AND inv.is_deleted=0 and inv.invoice_type='Purchase' "+
										" AND inv.invoice_type_id=t.id AND inv.gst_invoice_type_id=g.id ) AS invTable "+ 
										" LEFT JOIN (SELECT IFNULL(amount,0) AS txBalanceAmount,IFNULL(invoice_id,0) AS invoiceID FROM account_transactions_"+societyVO.getDataZoneID()+" WHERE org_id=:orgID )AS ats ON invoiceID=invTable.id "+
										" LEFT JOIN (SELECT SUM(r.amount) AS paidAmt,r.invoice_id,receipt_id FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" r,account_transactions_"+societyVO.getDataZoneID()+" a WHERE r.receipt_id=a.tx_id AND r.org_id=a.org_id AND a.org_id=:orgID AND a.is_deleted=0  GROUP BY invoice_id ) AS b ON invTable.id=b.invoice_id ORDER BY invTable.id) AS invoice WHERE org_id=:orgID AND is_deleted=0 GROUP BY ledgerID "+
										" ) AS payable LEFT JOIN (SELECT a.*,ABS(SUM(a.amt))-IFNULL(SUM(r.amount),0)  AS avBal,r.id,r.receipt_id,r.amount AS recAmt  FROM (SELECT a.*,l.amount AS amt,l.ledger_id,al.ledger_name FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" l,account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups g WHERE al.sub_group_id=g.id AND a.tx_id=l.tx_id AND al.id=l.ledger_id AND a.is_deleted=0  AND l.org_id=:orgID AND a.org_id=l.org_id AND a.org_id=al.org_id AND (a.tx_type='Payment' OR a.tx_type='Debit Note' ) AND a.tx_date BETWEEN DATE_SUB(NOW(), INTERVAL 365 DAY) AND DATE_ADD(NOW(), INTERVAL 365 DAY) AND g.category_id=96 ) AS a LEFT JOIN ( SELECT id,org_id,receipt_id,invoice_id,SUM(amount) AS amount FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" WHERE org_id=:orgID GROUP BY receipt_id ) r ON r.receipt_id=a.tx_id AND a.org_id=r.org_id GROUP BY a.ledger_id) "+
										" AS unpaidPayments ON payable.ledgerID=unpaidPayments.ledger_id GROUP BY payable.ledgerID;";	
								
						
								logger.debug("query: "+str);
								
								   Map hMap=new HashMap();
								   hMap.put("orgID", orgID);
								   
								  
								   RowMapper Rmapper = new RowMapper() {
										public Object mapRow(ResultSet rs, int rowNum)
												throws SQLException {
										 	InvoiceVO txVO=new InvoiceVO();
										 	txVO.setEntityName(rs.getString("ledger_name"));
										 	txVO.setLedgerID(rs.getInt("ledgerID"));
										 	txVO.setEntityID(rs.getInt("ledgerID"));
										 	txVO.setEntityType(rs.getString("entity_type"));
										 	txVO.setInvoiceType(rs.getString("invoice_type"));
										 	txVO.setBalance(rs.getBigDecimal("balance"));
											txVO.setZeroAmount(rs.getBigDecimal("zero"));
											txVO.setThirtyAmount(rs.getBigDecimal("thirty"));
											txVO.setSixtyAmount(rs.getBigDecimal("sixty"));
											txVO.setNinetyAmount(rs.getBigDecimal("ninty"));
											txVO.setNinetyPlusAmount(rs.getBigDecimal("nintyPlus"));
											txVO.setOverDueAmount(rs.getBigDecimal("overdueAmount"));
											txVO.setOverDueThirtyAmount(rs.getBigDecimal("thirtyOverDue"));
											txVO.setOverDueSixtyAmount(rs.getBigDecimal("sixtyOverDue"));
											txVO.setOverDueNinetyAmount(rs.getBigDecimal("ninetyOverDue"));
											txVO.setOverDueNinetyPlusAmount(rs.getBigDecimal("ninetyPlusOverDue"));
											txVO.setCarriedBalance(rs.getBigDecimal("unpaidAmt"));
											return txVO;
											
										}

									   };
									   payableList=namedParameterJdbcTemplate.query(
											str, hMap, Rmapper);
								   logger.debug("Exit :  public List getAgewisePayablesList(int orgID, int isDueDate)"+payableList.size());
								}catch(EmptyResultDataAccessException Ex){
									logger.info("No Data found at  List getAgewisePayablesList(int orgID, int isDueDate) "+Ex);
								
							
									}catch(Exception se){
									logger.error("Exception :public List getAgewisePayablesList(int orgID, int isDueDate)"+se);
								   
								}
							
							logger.debug("Exit : public List getAgewisePayablesList(int orgID, int isDueDate)");
						return payableList;
						}
	
					public List getSalesByCustomer(int orgID, String fromDate, String toDate, int ledgerID){
						List invoiceList=null;
					
							logger.debug("Entry :  public List getSalesByCustomer(int orgID, String fromDate, String toDate, int productID)");
							try{
								
								SocietyVO societyVO=societyService.getSocietyDetails(orgID);
								String str="SELECT r.name, l.ledger_name AS ledger_name,l.id as ledgerID,ii.item_id,inv.*,m.full_name AS creatorName,mi.full_name AS updatorName,CONCAT(IFNULL(l.project_tags,'')) AS line_item_tags,SUM(ii.quantity) AS qty,SUM(ii.price) AS price,SUM(ii.total_price) AS total_price,SUM(ii.sgst) AS sgst,SUM(ii.cgst) AS cgst,SUM(ii.igst) AS igst,SUM(ii.ugst) AS ugst,ii.gst_percentage,SUM(ii.amount) AS amt "+
										" FROM account_ledgers_"+societyVO.getDataZoneID()+" l, invoice_details inv, um_users m, um_users mi, invoice_line_item_details ii ,product_service_legder_relation r"+
										" WHERE l.id=inv.entity_id AND inv.org_id=l.org_id  AND inv.id=ii.invoice_id AND ii.item_id=r.id AND inv.org_id=:orgID AND inv.invoice_type='Sales' AND  inv.is_deleted=0 AND ( inv.invoice_date BETWEEN :fromDate AND :toDate ) AND m.id=inv.created_by AND mi.id=inv.updated_by AND inv.entity_id=:ledgerID GROUP BY ii.item_id;";	
								
						
								logger.debug("query: "+str);
								
								   Map hMap=new HashMap();
								   hMap.put("orgID", orgID);
								   hMap.put("fromDate", fromDate);
								   hMap.put("toDate", toDate);
								   hMap.put("ledgerID", ledgerID);
								   
								  
								   RowMapper Rmapper = new RowMapper() {
										public Object mapRow(ResultSet rs, int rowNum)
												throws SQLException {
										 	InvoiceVO txVO=new InvoiceVO();
										 	txVO.setProductName(rs.getString("name"));
											txVO.setProductID(rs.getInt("item_id"));
										 	txVO.setInvoiceAmount(rs.getBigDecimal("total_price"));
										 	txVO.setTotalAmount(rs.getBigDecimal("amt"));
										 	txVO.setTotalCGST(rs.getBigDecimal("cgst"));
										 	txVO.setTotalSGST(rs.getBigDecimal("sgst"));
										 	txVO.setTotalUGST(rs.getBigDecimal("ugst"));
										 	txVO.setTotalIGST(rs.getBigDecimal("igst"));
										 	txVO.setTotalGSTPercent(rs.getBigDecimal("gst_percentage"));
										 	txVO.setQuantity(rs.getBigDecimal("qty"));
											return txVO;
											
										}

									   };
									   invoiceList=namedParameterJdbcTemplate.query(
											str, hMap, Rmapper);
								   logger.debug("Exit :  public List getSalesByCustomer(int orgID, String fromDate, String toDate, int productID)"+invoiceList.size());
								}catch(EmptyResultDataAccessException Ex){
									logger.info("No Data found at  public List getSalesByCustomer(int orgID, String fromDate, String toDate, int productID) "+Ex);
								
							
									}catch(Exception se){
									logger.error("Exception :public List getSalesByCustomer(int orgID, String fromDate, String toDate, int productID)"+se);
								   
								}
							
							logger.debug("Exit : public List getSalesByCustomer(int orgID, String fromDate, String toDate, int productID))");
						return invoiceList;
						}
		
					public List getTotalSalesByCustomer(int orgID, String fromDate, String toDate){
						List payableList=null;
					
							logger.debug("Entry :  public List getTotalSalesByCustomer(int orgID, String fromDate, String toDate)");
							try{
								
								SocietyVO societyVO=societyService.getSocietyDetails(orgID);
								String str="SELECT  l.ledger_name AS ledger_name,l.id AS ledgerID,inv.*,m.full_name AS creatorName,mi.full_name AS updatorName,CONCAT(IFNULL(l.project_tags,'')) AS line_item_tags,SUM(ii.quantity) AS qty,SUM(ii.price) AS price,SUM(ii.total_price) AS total_price,SUM(ii.sgst) AS sgst,SUM(ii.cgst) AS cgst,SUM(ii.igst) AS igst,SUM(ii.ugst) AS ugst,ii.gst_percentage,SUM(ii.amount) AS amt,SUM(inv.tds_amount) AS total_tds "+
										" FROM account_ledgers_"+societyVO.getDataZoneID()+" l, invoice_details inv, um_users m, um_users mi ,invoice_line_item_details ii"+
										" WHERE l.id=inv.entity_id AND inv.org_id=l.org_id AND inv.id=ii.invoice_id AND inv.org_id=:orgID AND inv.invoice_type='Sales' AND  inv.is_deleted=0 AND ( inv.invoice_date BETWEEN :fromDate AND :toDate ) AND m.id=inv.created_by AND mi.id=inv.updated_by GROUP BY inv.entity_id order by amt desc;";	
								
						
								logger.debug("query: "+str);
								
								   Map hMap=new HashMap();
								   hMap.put("orgID", orgID);
								   hMap.put("fromDate", fromDate);
								   hMap.put("toDate", toDate);
								   
								   
								  
								   RowMapper Rmapper = new RowMapper() {
										public Object mapRow(ResultSet rs, int rowNum)
												throws SQLException {
										 	InvoiceVO txVO=new InvoiceVO();
										 	txVO.setEntityName(rs.getString("ledger_name"));
										 	txVO.setLedgerID(rs.getInt("ledgerID"));
										 	txVO.setEntityID(rs.getInt("ledgerID"));
										 	txVO.setEntityType(rs.getString("entity_type"));
										 	txVO.setInvoiceType(rs.getString("invoice_type"));
										 	txVO.setInvoiceAmount(rs.getBigDecimal("total_price"));
										 	txVO.setTotalAmount(rs.getBigDecimal("amt"));
										 	txVO.setTds(rs.getBigDecimal("total_tds"));
										 	txVO.setTotalCGST(rs.getBigDecimal("cgst"));
										 	txVO.setTotalSGST(rs.getBigDecimal("sgst"));
										 	txVO.setTotalUGST(rs.getBigDecimal("ugst"));
										 	txVO.setTotalIGST(rs.getBigDecimal("igst"));
										 	
											return txVO;
											
										}

									   };
									   payableList=namedParameterJdbcTemplate.query(
											str, hMap, Rmapper);
								   logger.debug("Exit :  public List getTotalSalesByCustomer(int orgID, String fromDate, String toDate, int productID)"+payableList.size());
								}catch(EmptyResultDataAccessException Ex){
									logger.info("No Data found at  public List getTotalSalesByCustomer(int orgID, String fromDate, String toDate, int productID) "+Ex);
								
							
									}catch(Exception se){
									logger.error("Exception :public List getTotalSalesByCustomer(int orgID, String fromDate, String toDate, int productID)"+se);
								   
								}
							
							logger.debug("Exit : public List getSalesByCustomer(int orgID, String fromDate, String toDate, int productID))");
						return payableList;
						}
					
					public List getTotalSalesByCustomerDetails(int orgID, String fromDate, String toDate,int ledgerID){
						List payableList=null;
					
							logger.debug("Entry :  public List getTotalSalesByCustomerDetails(int orgID, String fromDate, String toDate)");
							try{
								
								SocietyVO societyVO=societyService.getSocietyDetails(orgID);
								String str="SELECT ii.item_name,ii.invoice_id, l.ledger_name AS ledger_name,l.id AS ledgerID,inv.*,m.full_name AS creatorName,mi.full_name AS updatorName,CONCAT(IFNULL(l.project_tags,'')) AS line_item_tags,SUM(ii.quantity) AS qty,SUM(ii.price) AS price,SUM(ii.total_price) AS total_price,SUM(ii.sgst) AS sgst,SUM(ii.cgst) AS cgst,SUM(ii.igst) AS igst,SUM(ii.ugst) AS ugst,ii.gst_percentage,SUM(ii.amount) AS amt "+
										" FROM account_ledgers_"+societyVO.getDataZoneID()+" l, invoice_details inv, um_users m, um_users mi ,invoice_line_item_details ii"+
										" WHERE l.id=inv.entity_id AND inv.org_id=l.org_id AND inv.id=ii.invoice_id  AND inv.org_id=:orgID AND inv.invoice_type='Sales' AND  inv.is_deleted=0 AND ( inv.invoice_date BETWEEN :fromDate AND :toDate ) AND m.id=inv.created_by AND mi.id=inv.updated_by AND inv.entity_id=:entityID GROUP BY ii.id order by inv.invoice_date,inv.id;";	
								
						
								logger.debug("query: "+str);
								
								   Map hMap=new HashMap();
								   hMap.put("orgID", orgID);
								   hMap.put("fromDate", fromDate);
								   hMap.put("toDate", toDate);
								   hMap.put("entityID", ledgerID);
								   
								  
								   RowMapper Rmapper = new RowMapper() {
										public Object mapRow(ResultSet rs, int rowNum)
												throws SQLException {
										 	InvoiceVO txVO=new InvoiceVO();
										 	txVO.setEntityName(rs.getString("ledger_name"));
										 	txVO.setProductName(rs.getString("item_name"));
										 	txVO.setInvoiceDate(rs.getString("invoice_date"));
										 	txVO.setLedgerID(rs.getInt("ledgerID"));
										 	txVO.setEntityID(rs.getInt("ledgerID"));
										 	txVO.setEntityType(rs.getString("entity_type"));
										 	txVO.setInvoiceType(rs.getString("invoice_type"));
										 	txVO.setInvoiceAmount(rs.getBigDecimal("total_price"));
										 	txVO.setTotalAmount(rs.getBigDecimal("amt"));
										 	txVO.setInvoiceID(rs.getInt("invoice_id"));
										 	txVO.setTotalCGST(rs.getBigDecimal("cgst"));
										 	txVO.setTotalSGST(rs.getBigDecimal("sgst"));
										 	txVO.setTotalUGST(rs.getBigDecimal("ugst"));
										 	txVO.setTotalIGST(rs.getBigDecimal("igst"));
										 	txVO.setQuantity(rs.getBigDecimal("qty"));
										 	txVO.setPrice(rs.getBigDecimal("price"));
										 	txVO.setTotalPrice(rs.getBigDecimal("total_price"));
											return txVO;
											
										}

									   };
									   payableList=namedParameterJdbcTemplate.query(
											str, hMap, Rmapper);
								   logger.debug("Exit :  public List getTotalSalesByCustomerDetails(int orgID, String fromDate, String toDate, int productID)"+payableList.size());
								}catch(EmptyResultDataAccessException Ex){
									logger.info("No Data found at  public List getTotalSalesByCustomerDetails(int orgID, String fromDate, String toDate, int productID) "+Ex);
								
							
									}catch(Exception se){
									logger.error("Exception :public List getTotalSalesByCustomer(int orgID, String fromDate, String toDate, int productID)"+se);
								   
								}
							
							logger.debug("Exit : public List getSalesByCustomer(int orgID, String fromDate, String toDate, int productID))");
						return payableList;
						}
					
					public List getMonthWiseSalesReportForChart(int orgID,String fromDate,String uptoDate)
				 	{   
				 	   List groupNameList=new ArrayList();
				 		try
				 		{
				 			logger.debug("Entry : public List getMonthWiseSalesReportForChart(int orgID,String fromDate,String uptoDate)");
				 			
							SocietyVO societyVO=societyService.getSocietyDetails(orgID);
							String sqlDue = "SELECT SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'April' THEN i.invoice_amount ELSE '0.00' END) AS apr, SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'May' THEN i.invoice_amount ELSE '0.00' END) AS may, "+
											"SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'June' THEN i.invoice_amount ELSE '0.00' END) AS jun,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'July' THEN i.invoice_amount ELSE '0.00' END) AS jul,"+
											"SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'August' THEN i.invoice_amount ELSE '0.00' END) AS aug,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'September' THEN i.invoice_amount ELSE '0.00' END) AS sep,"+
											"SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'October' THEN i.invoice_amount ELSE '0.00' END) AS oct,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'November' THEN i.invoice_amount ELSE '0.00' END) AS nov,"+
											"SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'December' THEN i.invoice_amount ELSE '0.00' END) AS decs,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'January' THEN i.invoice_amount ELSE '0.00' END) AS jan,"+
											"SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'February' THEN i.invoice_amount ELSE '0.00' END) AS feb,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'March' THEN i.invoice_amount ELSE '0.00' END) AS mar,invoice_type,org_id FROM invoice_details i WHERE  i.org_id=:orgID AND i.invoice_date BETWEEN :fromDate AND :toDate AND i.is_deleted=0 AND i.invoice_type='Sales'  ;" ;
							
							logger.debug("sql "+sqlDue);
							Map hMap = new HashMap();
							hMap.put("orgID", orgID);
							hMap.put("fromDate", fromDate);
							hMap.put("toDate",uptoDate);

							RowMapper rMapper = new RowMapper() {
								public Object mapRow(ResultSet rs, int num) throws SQLException {
									// TODO Auto-generated method stub
									MonthwiseChartVO reportVO=new MonthwiseChartVO();
									reportVO.setTransactionType(rs.getString("invoice_type"));
									reportVO.setSocietyID(rs.getInt("org_id"));
									reportVO.setJanAmt(rs.getBigDecimal("jan").setScale(2, RoundingMode.HALF_UP));
									reportVO.setFebAmt(rs.getBigDecimal("feb").setScale(2, RoundingMode.HALF_UP));
									reportVO.setMarAmt(rs.getBigDecimal("mar").setScale(2, RoundingMode.HALF_UP));
									reportVO.setAprAmt(rs.getBigDecimal("apr").setScale(2, RoundingMode.HALF_UP));
									reportVO.setMayAmt(rs.getBigDecimal("may").setScale(2, RoundingMode.HALF_UP));
									reportVO.setJunAmt(rs.getBigDecimal("jun").setScale(2, RoundingMode.HALF_UP));
									reportVO.setJulAmt(rs.getBigDecimal("jul").setScale(2, RoundingMode.HALF_UP));
									reportVO.setAugAmt(rs.getBigDecimal("aug").setScale(2, RoundingMode.HALF_UP));
									reportVO.setSeptAmt(rs.getBigDecimal("sep").setScale(2, RoundingMode.HALF_UP));
									reportVO.setOctAmt(rs.getBigDecimal("oct").setScale(2, RoundingMode.HALF_UP));
									reportVO.setNovAmt(rs.getBigDecimal("nov").setScale(2, RoundingMode.HALF_UP));
									reportVO.setDecAmt(rs.getBigDecimal("decs").setScale(2, RoundingMode.HALF_UP));
									return reportVO;
								}
							};
							groupNameList = namedParameterJdbcTemplate
									.query(sqlDue, hMap, rMapper);
				 		    
				 		    logger.debug("Exit : public List getMonthWiseSalesReportForChart(int orgID,String fromDate,String uptoDate)");
				 		}
				 		catch(Exception ex)
				 		{
				 			//ex.printStackTrace();
				 			logger.error("Exception in public List getMonthWiseSalesReportForChart(int orgID,String fromDate,String uptoDate) : "+ex);
				 		}
				 	
				 		return groupNameList;
				 		
				 	}
					
					
					public List getMonthWiseSalesReport(int orgID,String fromDate,String uptoDate)
				 	{   
				 	   List groupNameList=new ArrayList();
				 		try
				 		{
				 			logger.debug("Entry : public List getMonthWiseSalesReport(int orgID,String fromDate,String uptoDate)");
				 			
							SocietyVO societyVO=societyService.getSocietyDetails(orgID);
							String sqlDue = "SELECT r.name,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'April' THEN ii.amount ELSE '0.00' END) AS apr, SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'May' THEN ii.amount ELSE '0.00' END) AS may, "+
											"SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'April' THEN ii.quantity ELSE '0' END) AS aprQty, SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'May' THEN ii.quantity ELSE '0.00' END) AS mayQty, "+
											"SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'June' THEN ii.amount ELSE '0.00' END) AS jun,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'July' THEN ii.amount ELSE '0.00' END) AS jul,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'June' THEN ii.quantity ELSE '0' END) AS junQty,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'July' THEN ii.quantity ELSE '0' END) AS julQty,"+
											"SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'August' THEN ii.amount ELSE '0.00' END) AS aug,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'September' THEN ii.amount ELSE '0.00' END) AS sep,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'August' THEN ii.quantity ELSE '0' END) AS augQty,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'September' THEN ii.quantity ELSE '0' END) AS sepQty,"+
											"SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'October' THEN ii.amount ELSE '0.00' END) AS OCT,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'November' THEN ii.amount ELSE '0.00' END) AS nov, SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'October' THEN ii.quantity ELSE '0' END) AS octQty,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'November' THEN ii.quantity ELSE '0' END) AS novQty,"+
											"SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'December' THEN ii.amount ELSE '0.00' END) AS decs,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'January' THEN ii.amount ELSE '0.00' END) AS jan, SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'December' THEN ii.quantity ELSE '0' END) AS decQty,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'January' THEN ii.quantity ELSE '0' END) AS janQty,"+
											"SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'February' THEN ii.amount ELSE '0.00' END) AS feb,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'March' THEN ii.amount ELSE '0.00' END) AS mar, SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'February' THEN ii.quantity ELSE '0.00' END) AS febQty,SUM(CASE WHEN MONTHNAME(i.invoice_date) = 'March' THEN ii.quantity ELSE '0.00' END) AS marQty,invoice_type,i.org_id,ii.item_id FROM invoice_details i,invoice_line_item_details ii,product_service_legder_relation r WHERE i.id=ii.invoice_id AND ii.item_id=r.id AND i.org_id=:orgID AND i.invoice_date BETWEEN :fromDate AND :toDate  AND i.invoice_type='Sales' AND i.is_deleted=0 GROUP BY ii.item_id  ;" ;
							
							logger.debug("sql "+sqlDue);
							Map hMap = new HashMap();
							hMap.put("orgID", orgID);
							hMap.put("fromDate", fromDate);
							hMap.put("toDate",uptoDate);

							RowMapper rMapper = new RowMapper() {
								public Object mapRow(ResultSet rs, int num) throws SQLException {
									// TODO Auto-generated method stub
									MonthwiseChartVO reportVO=new MonthwiseChartVO();
									reportVO.setTransactionType(rs.getString("invoice_type"));
									reportVO.setLedgerName(rs.getString("name"));
									reportVO.setSocietyID(rs.getInt("org_id"));
									reportVO.setJanAmt(rs.getBigDecimal("jan").setScale(2, RoundingMode.HALF_UP));
									reportVO.setFebAmt(rs.getBigDecimal("feb").setScale(2, RoundingMode.HALF_UP));
									reportVO.setMarAmt(rs.getBigDecimal("mar").setScale(2, RoundingMode.HALF_UP));
									reportVO.setAprAmt(rs.getBigDecimal("apr").setScale(2, RoundingMode.HALF_UP));
									reportVO.setMayAmt(rs.getBigDecimal("may").setScale(2, RoundingMode.HALF_UP));
									reportVO.setJunAmt(rs.getBigDecimal("jun").setScale(2, RoundingMode.HALF_UP));
									reportVO.setJulAmt(rs.getBigDecimal("jul").setScale(2, RoundingMode.HALF_UP));
									reportVO.setAugAmt(rs.getBigDecimal("aug").setScale(2, RoundingMode.HALF_UP));
									reportVO.setSeptAmt(rs.getBigDecimal("sep").setScale(2, RoundingMode.HALF_UP));
									reportVO.setOctAmt(rs.getBigDecimal("oct").setScale(2, RoundingMode.HALF_UP));
									reportVO.setNovAmt(rs.getBigDecimal("nov").setScale(2, RoundingMode.HALF_UP));
									reportVO.setDecAmt(rs.getBigDecimal("decs").setScale(2, RoundingMode.HALF_UP));
									reportVO.setJanQty(rs.getBigDecimal("janQty"));
									reportVO.setFebQty(rs.getBigDecimal("febQty"));
									reportVO.setMarQty(rs.getBigDecimal("marQty"));
									reportVO.setAprQty(rs.getBigDecimal("aprQty"));
									reportVO.setMayQty(rs.getBigDecimal("mayQty"));
									reportVO.setJunQty(rs.getBigDecimal("junQty"));
									reportVO.setJulQty(rs.getBigDecimal("julQty"));
									reportVO.setAugQty(rs.getBigDecimal("augQty"));
									reportVO.setSeptQty(rs.getBigDecimal("sepQty"));
									reportVO.setOctQty(rs.getBigDecimal("octQty"));
									reportVO.setNovQty(rs.getBigDecimal("novQty"));
									reportVO.setDecQty(rs.getBigDecimal("decQty"));
									
									reportVO.setProductID(rs.getInt("item_id"));
									return reportVO;
								}
							};
							groupNameList = namedParameterJdbcTemplate
									.query(sqlDue, hMap, rMapper);
				 		    
				 		    logger.debug("Exit : public List getMonthWiseSalesReport(int orgID,String fromDate,String uptoDate)");
				 		}
				 		catch(Exception ex)
				 		{
				 			//ex.printStackTrace();
				 			logger.error("Exception in public List getMonthWiseSalesReport(int orgID,String fromDate,String uptoDate) : "+ex);
				 		}
				 	
				 		return groupNameList;
				 		
				 	}
					
					public List getMonthWiseSalesReportDetails(int orgID,String fromDate,String uptoDate,int productID)
				 	{   
				 	   List invoiceList=new ArrayList();
				 		try
				 		{
				 			logger.debug("Entry : public List getMonthWiseSalesReportDetails(int orgID,String fromDate,String uptoDate)");
				 			
				 			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
							String str="SELECT ii.item_name,ii.invoice_id, l.ledger_name AS ledger_name,l.id AS ledgerID,inv.*,m.full_name AS creatorName,mi.full_name AS updatorName,CONCAT(IFNULL(l.project_tags,'')) AS line_item_tags,SUM(ii.quantity) AS qty,SUM(ii.price) AS price,SUM(ii.total_price) AS total_price,SUM(ii.sgst) AS sgst,SUM(ii.cgst) AS cgst,SUM(ii.igst) AS igst,SUM(ii.ugst) AS ugst,ii.gst_percentage,SUM(ii.amount) AS amt "+
									" FROM account_ledgers_"+societyVO.getDataZoneID()+" l, invoice_details inv, um_users m, um_users mi ,invoice_line_item_details ii"+
									" WHERE l.id=inv.entity_id AND inv.org_id=l.org_id AND inv.id=ii.invoice_id  AND inv.org_id=:orgID AND inv.invoice_type='Sales' AND  inv.is_deleted=0 AND ( inv.invoice_date BETWEEN :fromDate AND :toDate ) AND m.id=inv.created_by AND mi.id=inv.updated_by AND ii.item_id=:itemID GROUP BY ii.id order by inv.invoice_date,inv.id;";	
							
					
							logger.debug("query: "+str);
							
							   Map hMap=new HashMap();
							   hMap.put("orgID", orgID);
							   hMap.put("fromDate", fromDate);
							   hMap.put("toDate", uptoDate);
							   hMap.put("itemID", productID);
							   
							  
							   RowMapper Rmapper = new RowMapper() {
									public Object mapRow(ResultSet rs, int rowNum)
											throws SQLException {
									 	InvoiceVO txVO=new InvoiceVO();
									 	txVO.setEntityName(rs.getString("ledger_name"));
									 	txVO.setProductName(rs.getString("item_name"));
									 	txVO.setInvoiceDate(rs.getString("invoice_date"));
									 	txVO.setLedgerID(rs.getInt("ledgerID"));
									 	txVO.setEntityID(rs.getInt("ledgerID"));
									 	txVO.setEntityType(rs.getString("entity_type"));
									 	txVO.setInvoiceType(rs.getString("invoice_type"));
									 	txVO.setInvoiceAmount(rs.getBigDecimal("total_price"));
									 	txVO.setTotalAmount(rs.getBigDecimal("amt"));
									 	txVO.setInvoiceID(rs.getInt("invoice_id"));
									 	txVO.setTotalCGST(rs.getBigDecimal("cgst"));
									 	txVO.setTotalSGST(rs.getBigDecimal("sgst"));
									 	txVO.setTotalUGST(rs.getBigDecimal("ugst"));
									 	txVO.setTotalIGST(rs.getBigDecimal("igst"));
									 	txVO.setQuantity(rs.getBigDecimal("qty"));
									 	txVO.setPrice(rs.getBigDecimal("price"));
									 	txVO.setTotalPrice(rs.getBigDecimal("total_price"));
										return txVO;
										
									}

								   };
								   invoiceList=namedParameterJdbcTemplate.query(
										str, hMap, Rmapper);
				 		    logger.debug("Exit : public List getMonthWiseSalesReportDetails(int orgID,String fromDate,String uptoDate)");
				 		}
				 		catch(Exception ex)
				 		{
				 			//ex.printStackTrace();
				 			logger.error("Exception in public List getMonthWiseSalesReportDetails(int orgID,String fromDate,String uptoDate) : "+ex);
				 		}
				 	
				 		return invoiceList;
				 		
				 	}
					
					public List getTop20SalesProduct(int orgID, String fromDate, String toDate){
						List invoiceList=null;
					
							logger.debug("Entry : public List getTop20SalesProduct(int orgID, String fromDate, String toDate)");
							try{
								
								SocietyVO societyVO=societyService.getSocietyDetails(orgID);
								String str="SELECT r.*,SUM(ii.quantity) AS qty,SUM(ii.price) AS price,SUM(ii.total_price) AS total_price,SUM(ii.sgst) AS sgst,SUM(ii.cgst) AS cgst,SUM(ii.igst) AS igst,SUM(ii.ugst) AS ugst,ii.gst_percentage,SUM(ii.amount) AS amt FROM invoice_details i, invoice_line_item_details ii ,product_service_legder_relation r "+
										" WHERE i.id=ii.invoice_id AND i.org_id=:orgID AND i.invoice_date BETWEEN :fromDate AND :toDate AND ii.item_id=r.id AND i.invoice_type='Sales' AND i.is_deleted=0 GROUP BY r.id ORDER BY qty DESC LIMIT 20 ; ";
										
								
						
								logger.debug("query: "+str);
								
								   Map hMap=new HashMap();
								   hMap.put("orgID", orgID);
								   hMap.put("fromDate", fromDate);
								   hMap.put("toDate", toDate);
								   
								   
								  
								   RowMapper Rmapper = new RowMapper() {
										public Object mapRow(ResultSet rs, int rowNum)
												throws SQLException {
										 	InvoiceVO txVO=new InvoiceVO();
										 	txVO.setProductName(rs.getString("name"));
										 
										 	txVO.setInvoiceAmount(rs.getBigDecimal("total_price"));
										 	txVO.setTotalAmount(rs.getBigDecimal("amt"));
										 	txVO.setTotalCGST(rs.getBigDecimal("cgst"));
										 	txVO.setTotalSGST(rs.getBigDecimal("sgst"));
										 	txVO.setTotalUGST(rs.getBigDecimal("ugst"));
										 	txVO.setTotalIGST(rs.getBigDecimal("igst"));
										 	txVO.setTotalGSTPercent(rs.getBigDecimal("gst_percentage"));
										 	txVO.setQuantity(rs.getBigDecimal("qty"));
											return txVO;
											
										}

									   };
									   invoiceList=namedParameterJdbcTemplate.query(
											str, hMap, Rmapper);
								   logger.debug("Exit : public List getTop20SalesProduct(int orgID, String fromDate, String toDate)"+invoiceList.size());
								}catch(EmptyResultDataAccessException Ex){
									logger.info("No Data found at public List getTop20SalesProduct(int orgID, String fromDate, String toDate) "+Ex);
								
							
									}catch(Exception se){
									logger.error("Exception : public List getTop20SalesProduct(int orgID, String fromDate, String toDate)"+se);
								   
								}
							
							logger.debug("Exit : public List getSalesByCustomer(int orgID, String fromDate, String toDate, int productID))");
						return invoiceList;
						}
					
					public List getSalesReportProductWise(int orgID, String fromDate, String toDate, int productID){
						List invoiceList=null;
					
							logger.debug("Entry :  public List getSalesReportProductWise(int orgID, String fromDate, String toDate, int productID)");
							try{
								
								SocietyVO societyVO=societyService.getSocietyDetails(orgID);
								String str="SELECT l.ledger_name AS ledger_name,l.id as ledgerID,ii.item_name,ii.item_id,inv.*,m.full_name AS creatorName,mi.full_name AS updatorName,CONCAT(IFNULL(l.project_tags,'')) AS line_item_tags,SUM(ii.quantity) AS qty,SUM(ii.price) AS price,SUM(ii.total_price) AS total_price,SUM(ii.sgst) AS sgst,SUM(ii.cgst) AS cgst,SUM(ii.igst) AS igst,SUM(ii.ugst) AS ugst,ii.gst_percentage,SUM(ii.amount) AS amt "+
										" FROM account_ledgers_"+societyVO.getDataZoneID()+" l, invoice_details inv, um_users m, um_users mi, invoice_line_item_details ii "+
										" WHERE l.id=inv.entity_id AND inv.org_id=l.org_id  AND inv.id=ii.invoice_id  AND inv.org_id=:orgID AND inv.invoice_type='Sales' AND  inv.is_deleted=0 AND ( inv.invoice_date BETWEEN :fromDate AND :toDate ) AND m.id=inv.created_by AND mi.id=inv.updated_by AND ii.item_id=:productID GROUP BY inv.entity_id;";	
								
						
								logger.debug("query: "+str);
								
								   Map hMap=new HashMap();
								   hMap.put("orgID", orgID);
								   hMap.put("fromDate", fromDate);
								   hMap.put("toDate", toDate);
								   hMap.put("productID", productID);
								   
								  
								   RowMapper Rmapper = new RowMapper() {
										public Object mapRow(ResultSet rs, int rowNum)
												throws SQLException {
										 	InvoiceVO txVO=new InvoiceVO();
										 	txVO.setEntityName(rs.getString("ledger_name"));
										 	txVO.setLedgerID(rs.getInt("ledgerID"));
										 	txVO.setEntityID(rs.getInt("ledgerID"));
										 	txVO.setEntityType(rs.getString("entity_type"));
										 	txVO.setInvoiceType(rs.getString("invoice_type"));
										 	txVO.setInvoiceAmount(rs.getBigDecimal("total_price"));
										 	txVO.setTotalAmount(rs.getBigDecimal("amt"));
										 	txVO.setTotalCGST(rs.getBigDecimal("cgst"));
										 	txVO.setTotalSGST(rs.getBigDecimal("sgst"));
										 	txVO.setTotalUGST(rs.getBigDecimal("ugst"));
										 	txVO.setTotalIGST(rs.getBigDecimal("igst"));
										 	txVO.setTotalGSTPercent(rs.getBigDecimal("gst_percentage"));
										 	txVO.setQuantity(rs.getBigDecimal("qty"));
										 	txVO.setProductID(rs.getInt("item_id"));
										 	txVO.setProductName(rs.getString("item_name"));
											return txVO;
											
										}

									   };
									   invoiceList=namedParameterJdbcTemplate.query(
											str, hMap, Rmapper);
								
								
								   logger.debug("Exit :  public List getSalesReportProductWise(int orgID, String fromDate, String toDate, int productID)"+invoiceList.size());
								}catch(EmptyResultDataAccessException Ex){
									logger.info("No Data found at  public List getSalesReportProductWise(int orgID, String fromDate, String toDate, int productID) "+Ex);
								
							
									}catch(Exception se){
									logger.error("Exception :public List getSalesReportProductWise(int orgID, String fromDate, String toDate, int productID)"+se);
								   
								}
							
							logger.debug("Exit : public List getSalesReportProductWise(int orgID, String fromDate, String toDate, int productID))");
						return invoiceList;
						}
					
					public List getSalesReportProductWiseDetails(int orgID, String fromDate, String toDate, int productID,int ledgerID){
						List invoiceList=null;
					
							logger.debug("Entry :  public List getSalesReportProductWiseDetails(int orgID, String fromDate, String toDate, int productID)");
							try{
								
								SocietyVO societyVO=societyService.getSocietyDetails(orgID);
								String str="SELECT ii.item_name,ii.item_id,ii.invoice_id,l.ledger_name AS ledger_name,l.id as ledgerID,inv.*,m.full_name AS creatorName,mi.full_name AS updatorName,CONCAT(IFNULL(l.project_tags,'')) AS line_item_tags,SUM(ii.quantity) AS qty,SUM(ii.price) AS price,SUM(ii.total_price) AS total_price,SUM(ii.sgst) AS sgst,SUM(ii.cgst) AS cgst,SUM(ii.igst) AS igst,SUM(ii.ugst) AS ugst,ii.gst_percentage,SUM(ii.amount) AS amt "+
										" FROM account_ledgers_"+societyVO.getDataZoneID()+" l, invoice_details inv, um_users m, um_users mi, invoice_line_item_details ii "+
										" WHERE l.id=inv.entity_id AND inv.org_id=l.org_id  AND inv.id=ii.invoice_id  AND inv.org_id=:orgID AND inv.invoice_type='Sales' AND  inv.is_deleted=0 AND ( inv.invoice_date BETWEEN :fromDate AND :toDate ) AND m.id=inv.created_by AND mi.id=inv.updated_by AND ii.item_id=:productID and inv.entity_id=:entityID GROUP BY ii.id;";	
								
						
								logger.debug("query: "+str);
								
								   Map hMap=new HashMap();
								   hMap.put("orgID", orgID);
								   hMap.put("fromDate", fromDate);
								   hMap.put("toDate", toDate);
								   hMap.put("productID", productID);
								   hMap.put("entityID", ledgerID);
								   
								  
								   RowMapper Rmapper = new RowMapper() {
										public Object mapRow(ResultSet rs, int rowNum)
												throws SQLException {
										 	InvoiceVO txVO=new InvoiceVO();
										 	txVO.setEntityName(rs.getString("ledger_name"));
										 	txVO.setProductName(rs.getString("item_name"));
										 	txVO.setInvoiceDate(rs.getString("invoice_date"));
										 	txVO.setLedgerID(rs.getInt("ledgerID"));
										 	txVO.setEntityID(rs.getInt("ledgerID"));
										 	txVO.setEntityType(rs.getString("entity_type"));
										 	txVO.setInvoiceType(rs.getString("invoice_type"));
										 	txVO.setInvoiceAmount(rs.getBigDecimal("total_price"));
										 	txVO.setTotalAmount(rs.getBigDecimal("amt"));
										 	txVO.setTotalCGST(rs.getBigDecimal("cgst"));
										 	txVO.setTotalSGST(rs.getBigDecimal("sgst"));
										 	txVO.setTotalUGST(rs.getBigDecimal("ugst"));
										 	txVO.setTotalIGST(rs.getBigDecimal("igst"));
										 	txVO.setTotalGSTPercent(rs.getBigDecimal("gst_percentage"));
										 	txVO.setQuantity(rs.getBigDecimal("qty"));
										 	txVO.setProductID(rs.getInt("item_id"));
										 	txVO.setInvoiceID(rs.getInt("invoice_id"));
										 	txVO.setPrice(rs.getBigDecimal("price"));
											return txVO;
											
										}

									   };
									   invoiceList=namedParameterJdbcTemplate.query(
											str, hMap, Rmapper);
								
								
								   logger.debug("Exit :  public List getSalesReportProductWiseDetails(int orgID, String fromDate, String toDate, int productID)"+invoiceList.size());
								}catch(EmptyResultDataAccessException Ex){
									logger.info("No Data found at  public List getSalesReportProductWiseDetails(int orgID, String fromDate, String toDate, int productID) "+Ex);
								
							
									}catch(Exception se){
									logger.error("Exception :public List getSalesReportProductWiseDetails(int orgID, String fromDate, String toDate, int productID)"+se);
								   
								}
							
							logger.debug("Exit : public List getSalesReportProductWiseDetails(int orgID, String fromDate, String toDate, int productID))");
						return invoiceList;
						}
					
	
/*****************Product Rate Card *************/
					
					public List getRateCardList(int orgID){
						List rateCardList=null;
					
							logger.debug("Entry :  public List getRateCardList(int orgID)");
							try{
								
								String str=" SELECT r.*,m.full_name AS creatorName,mi.full_name AS updatorName FROM product_rate_card r,um_users m, um_users mi WHERE r.org_id=:orgID AND m.id=r.created_by AND mi.id=r.updated_by AND r.is_deleted=0 ";
						
								logger.debug("query: "+str);
								
								   Map hMap=new HashMap();
								   hMap.put("orgID", orgID);
				   
								  
								   RowMapper Rmapper = new RowMapper() {
										public Object mapRow(ResultSet rs, int rowNum)
												throws SQLException {
											ItemDetailsVO itemDetailsVO=new ItemDetailsVO();
											itemDetailsVO.setRateCardID(rs.getInt("id"));
											itemDetailsVO.setRateCardName(rs.getString("name"));
											itemDetailsVO.setCreatedBy(rs.getInt("created_by"));
											itemDetailsVO.setUpdatedBy(rs.getInt("updated_by"));
											itemDetailsVO.setCreatorName(rs.getString("creatorName"));
											itemDetailsVO.setUpdaterName(rs.getString("updatorName"));
											itemDetailsVO.setCreatedDate(rs.getTimestamp("create_date"));
											itemDetailsVO.setUpdatedDate(rs.getTimestamp("update_date"));
											itemDetailsVO.setIsActive(rs.getInt("is_deleted"));										
											
											return itemDetailsVO;
											
										}

									   };
									   rateCardList=namedParameterJdbcTemplate.query(
											str, hMap, Rmapper);
								   logger.debug("Exit :  public List getRateCardList(int orgID)"+rateCardList.size());
								}catch(EmptyResultDataAccessException Ex){
									logger.info("No Data found at  public List getRateCardList(int orgID)"+Ex);
								
							
									}catch(Exception se){
									logger.error("Exception :public List getRateCardList(int orgID)"+se);
								   
								}
							
							logger.debug("Exit : public List getRateCardList(int orgID)");
						return rateCardList;
					}
					
					/***Insert Product Rate Card***/
					 public int insertProductRateCard(ItemDetailsVO itemDetailsVO) {
							int insertFlag = 0;
							int generatedID = 0;
					
							try {
								java.util.Date today = new java.util.Date();
								final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
								itemDetailsVO.setCreatedDate(currentTimestamp);
								itemDetailsVO.setUpdatedDate(currentTimestamp);

								logger.debug("Entry : public int insertProductRateCard(ItemDetailsVO itemDetailsVO)");
								String query = "INSERT INTO `product_rate_card` (`org_id`,`name`,`created_by`,`updated_by`,`create_date`,`update_date`) "
										+ " VALUES ( :orgID, :rateCardName, :createdBy, :updatedBy, :createdDate, :updatedDate) ";
										
								SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(
										itemDetailsVO);
								KeyHolder keyHolder = new GeneratedKeyHolder();
								insertFlag = getNamedParameterJdbcTemplate().update(query,
										fileParameters, keyHolder);
								generatedID = keyHolder.getKey().intValue();
								logger.debug("key GEnereator ---" + generatedID);
							
							} catch (DuplicateKeyException e) {
								logger.info("Product Rate Card already present " );
								insertFlag=534;
							} catch (Exception e) {
								logger.error("Exception at insertProductRateCard " + e);
							}
							logger.debug("Exit :public int insertProductRateCard(ItemDetailsVO itemDetailsVO)");
							return insertFlag;
						}
					 
					 
						/***Update Product Rate Card***/
						public int updateProductRateCard(ItemDetailsVO itemDetailsVO) {
							int updateFlag = 0;
							logger.debug("Entry : public int updateProductRateCard(ItemDetailsVO itemDetailsVO)");
							
							try {
								java.util.Date today = new java.util.Date();
								final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
								
								String sql="Update product_rate_card set name=:rateCardName,updated_by=:updatedBy,update_date=:updateDate where id=:rateCardID and is_deleted=0 ";
								
								Map hashMap = new HashMap();

								hashMap.put("rateCardID",itemDetailsVO.getRateCardID());								
								hashMap.put("rateCardName", itemDetailsVO.getRateCardName());
								hashMap.put("updatedBy", itemDetailsVO.getUpdatedBy());
								hashMap.put("updateDate", currentTimestamp);
									
								updateFlag=namedParameterJdbcTemplate.update(sql, hashMap);
								
							} catch (Exception e) {
								logger.error("Exception in updateProductRateCard "+e);
							}
								
							logger.debug("Exit : public int updateProductRateCard(ItemDetailsVO itemDetailsVO)");
							return updateFlag;
						}
					 
						/***Delete Product Rate Card***/
						public int deleteProductRateCard(ItemDetailsVO itemDetailsVO) {
							int insertFlag = 0;			
							String sql="";
							logger.debug("Entry : public int deleteProductRateCard(ItemDetailsVO itemDetailsVO)"+itemDetailsVO.getRateCardID()+itemDetailsVO.getOrgID());
							try{
							
								 sql=" Update product_rate_card set is_deleted=:active  where id=:rateCardID and org_id=:orgID ";	
								logger.debug("Query is "+sql);
								
								Map namedParameters = new HashMap();
								namedParameters.put("rateCardID",itemDetailsVO.getRateCardID());			
								namedParameters.put("orgID", itemDetailsVO.getOrgID());
								namedParameters.put("active", itemDetailsVO.getIsActive());
								
								insertFlag = namedParameterJdbcTemplate.update(sql,namedParameters);
								
							}catch (Exception e) {
								logger.error("Exception occurred at deleteProductRateCard "+e);
							}
							
							
							logger.debug("Exit : public int deleteRateCard(ItemDetailsVO itemDetailsVO) "+insertFlag);
							return insertFlag;
						}
					
						
						/***Insert Product Rate Card Item***/
						 public int insertProductRateCardItem(ItemDetailsVO itemDetailsVO) {
								int insertFlag = 0;
								int generatedID = 0;
						
								try {
									java.util.Date today = new java.util.Date();
									final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
									itemDetailsVO.setCreatedDate(currentTimestamp);
									itemDetailsVO.setUpdatedDate(currentTimestamp);

									logger.debug("Entry : public int insertProductRateCardItem(ItemDetailsVO itemDetailsVO)");
									String query = "INSERT INTO `product_rate_card_details` (`rate_card_id`,`product_id`,`purchase_price`,`selling_price`,`created_by`,`updated_by`,`create_date`,`update_date`) "
											+ " VALUES ( :rateCardID, :itemID, :purchasePrice, :sellingPrice, :createdBy, :updatedBy, :createdDate, :updatedDate) ";
											
									SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(
											itemDetailsVO);
									KeyHolder keyHolder = new GeneratedKeyHolder();
									insertFlag = getNamedParameterJdbcTemplate().update(query,
											fileParameters, keyHolder);
									generatedID = keyHolder.getKey().intValue();
									logger.debug("key GEnereator ---" + generatedID);
								
								
								} catch (Exception e) {
									logger.error("Exception at insertProductRateCardItem " + e);
								}
								logger.debug("Exit :public int insertProductRateCardItem(ItemDetailsVO itemDetailsVO)");
								return insertFlag;
							}
						 
						 
							/***Update Product Rate Card Item***/
							public int updateProductRateCardItem(ItemDetailsVO itemDetailsVO) {
								int updateFlag = 0;
								logger.debug("Entry : public int updateProductRateCardItem(ItemDetailsVO itemDetailsVO)");
								
								try {
									java.util.Date today = new java.util.Date();
									final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
									
									String sql="Update product_rate_card_details set product_id=:productId,purchase_price=:purchasePrice,selling_price=:sellingPrice,updated_by=:updatedBy,update_date=:updateDate where id=:rateCardItemID and is_deleted=0 ";
									
									Map hashMap = new HashMap();

									hashMap.put("rateCardItemID",itemDetailsVO.getRateCardID());								
									hashMap.put("productId", itemDetailsVO.getItemID());
									hashMap.put("sellingPrice", itemDetailsVO.getSellingPrice());
									hashMap.put("purchasePrice", itemDetailsVO.getPurchasePrice());
									hashMap.put("updatedBy", itemDetailsVO.getUpdatedBy());
									hashMap.put("updateDate", currentTimestamp);
										
									updateFlag=namedParameterJdbcTemplate.update(sql, hashMap);
									
								} catch (Exception e) {
									logger.error("Exception in updateProductRateCardItem "+e);
								}
									
								logger.debug("Exit : public int updateProductRateCardItem(ItemDetailsVO itemDetailsVO)");
								return updateFlag;
							}
						 
							/***Delete Product Rate Card Item***/
							public int deleteProductRateCardItem(ItemDetailsVO itemDetailsVO) {
								int insertFlag = 0;			
								String sql="";
								logger.debug("Entry : public int deleteProductRateCardItem(ItemDetailsVO itemDetailsVO)"+itemDetailsVO.getRateCardID()+itemDetailsVO.getOrgID());
								try{
								
									 sql=" Update product_rate_card_details set is_deleted=:active  where id=:rateCardItemID ";	
									logger.debug("Query is "+sql);
									
									Map namedParameters = new HashMap();
									namedParameters.put("rateCardItemID",itemDetailsVO.getRateCardItemID());			
									namedParameters.put("orgID", itemDetailsVO.getOrgID());
									namedParameters.put("active", itemDetailsVO.getIsActive());
									
									insertFlag = namedParameterJdbcTemplate.update(sql,namedParameters);
									
								}catch (Exception e) {
									logger.error("Exception occurred at deleteProductRateCardItem "+e);
								}
								
								
								logger.debug("Exit : public int deleteProductRateCardItem(ItemDetailsVO itemDetailsVO) "+insertFlag);
								return insertFlag;
							}
					
					 /* Get product rate card by productId   */
					 public ItemDetailsVO getProductRateCardDetails(int productID,int rateCardID){
						 ItemDetailsVO	rateCardVO=new 	ItemDetailsVO();
							String sql="";
							logger.debug("Entry :  public ItemDetailsVO getProductRateCardDetails(int productID,int rateCardID)");		
							try{		
								
							 sql =" SELECT * FROM product_rate_card_details WHERE product_id=:productID and rate_card_id=:rateCardID and is_deleted=0";
												  	
							logger.debug("Query is "+sql);

						Map hashMap = new HashMap();
						hashMap.put("productID", productID);
						hashMap.put("rateCardID", rateCardID);
						
						RowMapper rMapper = new RowMapper() {

							public Object mapRow(ResultSet rs, int rowNum)
									throws SQLException {
								
								ItemDetailsVO itemDetailsVO=new ItemDetailsVO();
								itemDetailsVO.setItemID(rs.getInt("product_id"));
								itemDetailsVO.setRateCardID(rs.getInt("rate_card_id"));
					
								itemDetailsVO.setPurchasePrice(rs.getBigDecimal("purchase_price"));	
								itemDetailsVO.setSellingPrice(rs.getBigDecimal("selling_price"));	
								
								return itemDetailsVO;
							}
						};
							
						rateCardVO=(ItemDetailsVO)namedParameterJdbcTemplate.queryForObject(sql, hashMap,rMapper);
						
							}catch(EmptyResultDataAccessException Ex){
								logger.info("No Data found at  public List getProductRateCardDetails"+Ex);
							}catch(Exception e){
								logger.error("Exception in getProductRateCardDetails "+e);
							}
							logger.debug("Exit :  public ItemDetailsVO getProductRateCardDetails(int productID,int rateCardID)"+rateCardVO.getRateCardID());
							return rateCardVO;		
						}
			
					 public List getProductRateCardItemList(int orgID,int rateCardID){
							List rateCardList=null;
						
								logger.debug("Entry :  public List getProudctRateCardItemList(int orgID,int rateCardID)");
								try{
									
									String str=" SELECT r.*,m.full_name AS creatorName,mi.full_name AS updatorName,p.name As itemName FROM product_rate_card c, product_rate_card_details r,product_service_legder_relation p ,um_users m, um_users mi WHERE c.org_id=:orgID AND c.id=r.rate_card_id AND r.rate_card_id=:rateCardID AND p.id=r.product_id AND m.id=r.created_by AND mi.id=r.updated_by AND r.is_deleted=0 ";
							
									logger.debug("query: "+str);
									
									   Map hMap=new HashMap();
									   hMap.put("orgID", orgID);
									   hMap.put("rateCardID", rateCardID);
									  
									   RowMapper Rmapper = new RowMapper() {
											public Object mapRow(ResultSet rs, int rowNum)
													throws SQLException {
												ItemDetailsVO itemDetailsVO=new ItemDetailsVO();
												itemDetailsVO.setRateCardItemID(rs.getInt("id"));
												itemDetailsVO.setRateCardID(rs.getInt("rate_card_id"));
												itemDetailsVO.setItemID(rs.getInt("product_id"));
												itemDetailsVO.setItemName(rs.getString("itemName"));
												itemDetailsVO.setPurchasePrice(rs.getBigDecimal("purchase_price"));
												itemDetailsVO.setSellingPrice(rs.getBigDecimal("selling_price"));
												itemDetailsVO.setCreatedBy(rs.getInt("created_by"));
												itemDetailsVO.setUpdatedBy(rs.getInt("updated_by"));
												itemDetailsVO.setCreatorName(rs.getString("creatorName"));
												itemDetailsVO.setUpdaterName(rs.getString("updatorName"));
												itemDetailsVO.setCreatedDate(rs.getTimestamp("create_date"));
												itemDetailsVO.setUpdatedDate(rs.getTimestamp("update_date"));
												itemDetailsVO.setIsActive(rs.getInt("is_deleted"));										
												
												return itemDetailsVO;
												
											}

										   };
										   rateCardList=namedParameterJdbcTemplate.query(
												str, hMap, Rmapper);
									   logger.debug("Exit :  public List getRateCardList(int orgID)"+rateCardList.size());
									}catch(EmptyResultDataAccessException Ex){
										logger.info("No Data found at  public List getRateCardList(int orgID)"+Ex);
									
								
										}catch(Exception se){
										logger.error("Exception :public List getRateCardList(int orgID)"+se);
									   
									}
								
								logger.debug("Exit : public List getRateCardList(int orgID)");
							return rateCardList;
						}
			 
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	/**
	 * @param societyService the societyService to set
	 */
	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}

}
