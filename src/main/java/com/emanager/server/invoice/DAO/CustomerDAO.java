package com.emanager.server.invoice.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.invoice.dataAccessObject.CustomerVO;
import com.emanager.server.invoice.dataAccessObject.ProfileDetailsVO;

public class CustomerDAO {

	Logger logger=Logger.getLogger(CustomerDAO.class);
	public NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	
	
	public List getCustomerDetaiilsList(int orgID)
	{
		List vendorList=null;
		try
		{
			logger.debug("Entry : public List getVendorDetaiilsList()");
			String sql="SELECT * FROM customer_details c,tax_details t where c.customer_id=t.entity_id and t.entity='C' and org_id=:orgID  ";
			Map hmap=new HashMap();
			hmap.put("orgID",orgID);
			
			RowMapper rMapper=new RowMapper()
			{

				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					// TODO Auto-generated method stub
					CustomerVO customerVO=new CustomerVO();
					customerVO.setCustomerID(rs.getInt("customer_id"));
					customerVO.setTitle(rs.getString("title"));
					customerVO.setFullName(rs.getString("full_name"));
					customerVO.setPrimaryContact(rs.getString("contact_primary"));
					customerVO.setSecondaryContact(rs.getString("contact_secondary"));
					customerVO.setPrimaryEmail(rs.getString("email_primary"));
					customerVO.setSecondaryContact(rs.getString("email_secondary"));
					customerVO.setPanNo(rs.getString("pan_no"));
					customerVO.setGSTIN(rs.getString("GSTIN"));
				
					
					return customerVO;
				}
				
				
			};
			vendorList=namedParameterJdbcTemplate.query(sql, hmap, rMapper);
		
			
		}catch(Exception ex)
		{
			logger.error("Exception in getVendorDetaiilsList : "+ex);			
		}
		logger.debug("Exit: public List getVendorDetaiilsList"+vendorList.size());
		return vendorList; 
	}
	
	public int insertCustomerDetails(CustomerVO customerVO){
		
		int flag = 0;
		int insertFlag = 0;
		int success = 0;
	    int success1 =0;
	    int genID = 0;
	    
		try {
			logger.debug("Entry : public int insertCustomerDetails(CustomerVO customerVO)");
			
		    String insertFileString = "INSERT INTO customer_details(title,full_name,contact_primary,contact_secondary,email_primary,email_secondary) VALUES( :title, :fullName, :primaryContact, :secondaryContact, :primaryEmail, :secondaryEmail)";
		    logger.debug("query : " + insertFileString);
		    
	    	SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(
				                          customerVO);
		
		    KeyHolder keyHolder = new GeneratedKeyHolder();
		    insertFlag = getNamedParameterJdbcTemplate().update(
				    insertFileString, fileParameters, keyHolder);
		    genID = keyHolder.getKey().intValue();
		    logger.debug("key GEnereator ---" + genID);
			
		   
	 	
		  
		
		   
		    logger.debug("Exit : public int insertCustomerDetails(CustomerVO customerVO)"+genID);
		}catch (DataAccessException exc) {
		  logger.error("DataAccessException : insertCustomerDetails(CustomerVO customerVO)" + exc);
		}
		catch (Exception e) {
		  logger.error("Exception in insertCustomerDetails(CustomerVO customerVO) : " + e);
		}

		return genID;
		}

	
	
	
public int insertCustomerLedgerMappings(CustomerVO customerVO){
		
		int flag = 0;
	
	    
		try {
			logger.debug("Entry : public insertCustomerLedgerMappings(CustomerVO customerVO)");
			
		    String insertFileString = "INSERT INTO customer_org_mapping(org_id,cust_id,ledger_id) " +
		    		"VALUES( :orgID, :custID, :ledgerID)";
		    logger.debug("query : " + insertFileString);
		    
		    Map hmap=new HashMap();
			hmap.put("orgID", customerVO.getOrgID());
			hmap.put("custID", customerVO.getCustomerID());
			hmap.put("ledgerID", customerVO.getLedgerID());
		
			
			flag=namedParameterJdbcTemplate.update(insertFileString,hmap);
		   
	 	
		  
		
		   
		    logger.debug("Exit : public int insertCustomerLedgerMappings(CustomerVO customerVO)"+flag);
		}catch (DataAccessException exc) {
		  logger.error("DataAccessException : insertCustomerLedgerMappings(CustomerVO customerVO)" + exc);
		}
		catch (Exception e) {
		  logger.error("Exception in insertCustomerLedgerMappings(CustomerVO customerVO) : " + e);
		}

		return flag;
		}
	
	public int updateCustomerDetails(CustomerVO customerVO,AddressVO addressVO) {

		int flag = 0;
		int updateFlag=0;
		int flagM=0;
		String strSQLQuery = "";
		
		try {
			logger.debug("Entry : public int updateCustomer(CustomerVO customerVO,AddressVO addressVO) ");
			
			strSQLQuery = "UPDATE customer_details c,tax_details t SET title=:title,full_name=:fullName, email_primary=:emailPrimary,email_secondary=:emailSecondary, contact_primary=:contactPrimary ,contact_secondary=:contactSecondary, org_id=:orgID ,pan_no= :panNo, "+
	                      " authorized_person_name= :fullName, authorized_person_email=:emailPrimary, authorized_person_mobile=:contactPrimary " +
	                      " WHERE c.customer_id=t.entity and c.customer_id="+customerVO.getCustomerID()+";";
			
			logger.debug("query : " + strSQLQuery);

			Map hmap = new HashMap();
			hmap.put("title", customerVO.getTitle());
			hmap.put("full_name", customerVO.getFullName());
			hmap.put("email_primary", customerVO.getPrimaryEmail());
			hmap.put("email_secondary", customerVO.getSecondaryEmail());
			hmap.put("contact_primary", customerVO.getPrimaryContact());
			hmap.put("contact_secondary", customerVO.getSecondaryContact());
			hmap.put("org_id", customerVO.getOrgID());
			hmap.put("panNo",customerVO.getPanNo());
			hmap.put("authPersonName", customerVO.getFullName());
			hmap.put("authPersonEmail", customerVO.getPrimaryEmail());
			hmap.put("authPersonMobile", customerVO.getPrimaryContact());
		    
			flag=namedParameterJdbcTemplate.update(strSQLQuery, hmap);
			
			
			
			
			
			logger.debug("Exit : public int updateCustomer(CustomerVO customerVO,AddressVO addressVO) "+flag);

		} catch (Exception Ex) {
			logger.error("Exception in updateCustomer(CustomerVO customerVO,AddressVO addressVO)  : "+Ex);
		}

		return flag;

	}
	
	public int deleteCustomer(int customerID){
		
		int sucess = 0;
		String strSQL = "";
		try {
				logger.debug("Entry : public int deleteCustomer(int customerID)");
				strSQL = "update  customer_details set is_deleted=1 WHERE customer_id=:customer_id;" ;
				logger.debug("query : " + strSQL);
				// 2. Parameters.
				Map namedParameters = new HashMap();
				namedParameters.put("customer_id", customerID);
				
				sucess = namedParameterJdbcTemplate.update(strSQL,namedParameters);
				
				logger.debug(sucess);
				logger.debug("Exit : public int deleteCustomer(int customerID)");

		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in deleteCustomer(int customerID) : "+Ex);
			
		}
		return sucess;
	}	
	
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	


	
}
