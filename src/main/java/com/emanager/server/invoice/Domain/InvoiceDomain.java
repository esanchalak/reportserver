package com.emanager.server.invoice.Domain;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.ChargesVO;
import com.emanager.server.accounts.DataAccessObjects.LedgerEntries;
import com.emanager.server.accounts.DataAccessObjects.LedgerTrnsVO;
import com.emanager.server.accounts.DataAccessObjects.OnlinePaymentGatewayVO;
import com.emanager.server.accounts.DataAccessObjects.PenaltyChargesVO;
import com.emanager.server.accounts.DataAccessObjects.ScheduledTransactionResponseVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionMetaVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.commonUtils.domainObject.CalculateLateFeeForTransactions;
import com.emanager.server.commonUtils.domainObject.CommonUtility;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.domainObject.EmailDomain;
import com.emanager.server.financialReports.domainObject.AmountInWords;
import com.emanager.server.financialReports.domainObject.CalculateLateFee;
import com.emanager.server.financialReports.valueObject.MonthwiseChartVO;
import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.invoice.DAO.InvoiceDAO;
import com.emanager.server.invoice.dataAccessObject.BillDetailsVO;
import com.emanager.server.invoice.dataAccessObject.InLineItemsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceBillingCycleVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceDetailsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceLineItemsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceMemberChargesVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceVO;
import com.emanager.server.invoice.dataAccessObject.ItemDetailsVO;
import com.emanager.server.invoice.dataAccessObject.ProfileDetailsVO;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.BankInfoVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.webservice.JSONResponseVO;




public class InvoiceDomain {
	
	InvoiceDAO invoiceDAO;
	TransactionService transactionService;
	EmailDomain emailDomain;
	MemberService memberServiceBean;
	SocietyService societyService;
	CalculateLateFee calculateLateFees;
	CalculateLateFeeForTransactions calculateLateFeesForTransaction;
	LedgerService ledgerService;
	DateUtility dateUtil=new DateUtility();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	AmountInWords amtInWrds=new AmountInWords();
	
	private static final Logger logger = Logger.getLogger(InvoiceDomain.class);
	
	
	/* Get list of active society list */
	public List getActiveSocietyList(String societyID,String billingCycleID){
		List societyList=null;
		logger.debug("Entry : public List getActiveSocietyList()");
		
		societyList=invoiceDAO.getActiveSocietyList(societyID,billingCycleID);
		
		logger.debug("Exit : public List getActiveSocietyList()");
		return societyList;		
	}
	
	/* Get list of billingCycle list  of society*/
	public List getBillingCycleList(int societyID){
		logger.debug("Entry : public List getBillingCycleList(String societyID)");
		List billingCycleList=null;
		
		billingCycleList=invoiceDAO.getBillingCycleList(societyID);
		
		logger.debug("Exit : public List getBillingCycleList(String societyID)");
		
		return billingCycleList;
	}
	
	public InvoiceBillingCycleVO getBillingCycleDetails(int billingCycleID){
		logger.debug("Entry : public InvoiceBillingCycleVO getBillingCycleDetails(int billingCycleID)");
		InvoiceBillingCycleVO billingCycleVO=new InvoiceBillingCycleVO();
		
		billingCycleVO=invoiceDAO.getBillingCycleDetails(billingCycleID);
		
		logger.debug("Exit : public InvoiceBillingCycleVO getBillingCycleDetails(int billingCycleID)");
		
		return billingCycleVO;
	}
	
	/* Get list of all active members of the given society  */
	public List getActiveMemberListAll(int societyID){
		List memberList=null;
		List invoiceList=new ArrayList();
		logger.debug("Entry : public List getActiveMemberList(String societyID)");
		
		try{
		
		memberList=invoiceDAO.getActiveMemberList(societyID);
		
		}catch (Exception e) {
			logger.error("Exception in getActiveMemberList "+e);
		}
		
		logger.debug("Exit : public List getActiveMemberList(String societyID)");
		return memberList;		
	}
	
	/* Get list of all active members of the given society  */
	public List getActiveMemberList(InvoiceBillingCycleVO inbcVO){
		List memberList=null;
		List invoiceList=new ArrayList();
		logger.debug("Entry : public List getActiveMemberList(String societyID)");
		
		try{
		int societyID=inbcVO.getSocietyId();
		memberList=invoiceDAO.getActiveMemberList(societyID);
		
		}catch (Exception e) {
			logger.error("Exception in getActiveMemberList "+e);
		}
		
		logger.debug("Exit : public List getActiveMemberList(String societyID)");
		return invoiceList;		
	}
	
	/* Insert the invoices */
	public List checkInvoicesList(List memberList,InvoiceBillingCycleVO billingVO,int invoiceTypeID){
		int insertFlag=0;
		List invoiceList=new ArrayList();
		List invoiceTypeList=invoiceDAO.getInvoiceTypeList("S");
		
		logger.debug("Entry : public List checkInvoicesList(List MemberList,InvoiceDetailsVO invoiceVO)"+invoiceTypeList.size());
		try{
		for(int i=0;memberList.size()>i;i++){
			
			InvoiceMemberChargesVO memberVO=(InvoiceMemberChargesVO) memberList.get(i);
			
			for(int j=0;invoiceTypeList.size()>j;j++){
				InvoiceDetailsVO invoiceVO=new InvoiceDetailsVO();
				
				invoiceVO.setMemberVO(memberVO);
				invoiceVO.setMemberName(memberVO.getMemberName());
			InvoiceDetailsVO invoiceTypeVO=(InvoiceDetailsVO) invoiceTypeList.get(j);
			
			invoiceVO.setInvoiceTypeID(invoiceTypeVO.getInvoiceTypeID());
			
			invoiceVO=calculateApplicableChargesToMember(invoiceVO,billingVO,invoiceTypeID);
				
				
				if(invoiceVO.getInvoiceAmount().signum()!=0){
					logger.debug("Here invoice amount is "+invoiceVO.getInvoiceAmount()+" "+invoiceVO.getTotalAmount()+" "+invoiceVO.getInvoiceTypeID()+" "+invoiceVO.getBillCycleID());
				invoiceList.add(invoiceVO);
				}
			
			else 
				continue;
			}
		
			}
			
		
		}
		catch(Exception e){
			logger.error("Exception in checkInvoiceList"+e);
		}
		
		logger.debug("Exit : public List checkInvoicesList(List MemberList,InvoiceDetailsVO invoiceVO)");
		return invoiceList;
	}
	
	
	
	
	/* Insert the invoices */
	public List checkLateFeeInvoicesList(List memberList,InvoiceBillingCycleVO billingVO,int invoiceTypeID){
		int insertFlag=0;
		List invoiceList=new ArrayList();
		List invoiceTypeList=invoiceDAO.getInvoiceTypeList("S");
		
		logger.debug("Entry : public List checkLateFeeInvoicesList(List MemberList,InvoiceDetailsVO invoiceVO)"+invoiceTypeList.size());
		try{
		for(int i=0;memberList.size()>i;i++){
			
			InvoiceMemberChargesVO memberVO=(InvoiceMemberChargesVO) memberList.get(i);
			InvoiceDetailsVO invoiceVO=new InvoiceDetailsVO();
			invoiceVO.setMemberVO(memberVO);
			invoiceVO.setMemberName(memberVO.getMemberName());
		
			
			
			
			
		
			 if((invoiceTypeID==10)){
				invoiceVO=calculateApplicableChargesToMember(invoiceVO,billingVO,invoiceTypeID);
				
				
				if(invoiceVO.getInvoiceAmount().signum()!=0){
					logger.debug("Here invoice amount is "+invoiceVO.getInvoiceAmount()+" "+invoiceVO.getTotalAmount()+" "+invoiceVO.getInvoiceTypeID()+" "+invoiceVO.getBillCycleID());
				invoiceList.add(invoiceVO);
				}
			
			else 
				continue;
			}
		}
			
		
		}
		catch(Exception e){
			logger.error("Exception in checkLateFeeInvoicesList"+e);
		}
		
		logger.debug("Exit : public List checkLateFeeInvoicesList(List MemberList,InvoiceDetailsVO invoiceVO)");
		return invoiceList;
	}
	
	public InvoiceDetailsVO generateInvoiceObject(InvoiceMemberChargesVO memberChargesVO,List memberChargeList,int invoiceType){
		InvoiceDetailsVO invoiceVO=new InvoiceDetailsVO();
		BigDecimal grandTotal=BigDecimal.ZERO;
		List applicableChargeList=new ArrayList();
		BigDecimal carriedBalance=BigDecimal.ZERO;
		BigDecimal currentBalance=BigDecimal.ZERO;
		
		try{
		logger.debug("Entry : public InvoiceDetailsVO generateInvoiceObject(InvoiceMemberChargesVO memberChargesVO)");
		Date currentDatetime = new Date(System.currentTimeMillis());   
	     //   java.sql.Date sqlDate = new java.sql.Date(currentDatetime.getTime());   
		java.sql.Timestamp timestamp = new java.sql.Timestamp(currentDatetime.getTime());
		int societyID=memberChargesVO.getSocietyID();
	
		AccountHeadVO acHeadVO=new AccountHeadVO();
		invoiceVO.setMemberVO(memberChargesVO);
		acHeadVO.setLedgerID(memberChargesVO.getLedgerID());
		invoiceVO.setAptID(memberChargesVO.getAptID());
		invoiceVO.setMemberName(memberChargesVO.getMemberName());
		
		
		for(int i=0;memberChargeList.size()>i;i++){
			InvoiceMemberChargesVO inMemberVO=(InvoiceMemberChargesVO) memberChargeList.get(i);
			
			//inMemberVO=getConditionalBalance(inMemberVO, societyID,memberChargesVO);
			
			
			if((inMemberVO.getIsApplicable()&&(inMemberVO.getInvoiceTypeID()==invoiceType)&&(inMemberVO.getInvoiceTypeID()!=10))){
				grandTotal=grandTotal.add(inMemberVO.getTotalAmount());
				
				invoiceVO.setInvoiceTypeID(inMemberVO.getInvoiceTypeID());
				
			invoiceVO.setInvoiceDate(inMemberVO.getInvoiceDate());
			
			invoiceVO.setDueDate(inMemberVO.getDueDate());
			
			invoiceVO.setFromDate(inMemberVO.getFromDate());
			invoiceVO.setUptoDate(inMemberVO.getUptoDate());
			inMemberVO.setBalance(BigDecimal.ZERO);
			inMemberVO.setCarriedBalance(BigDecimal.ZERO);
			
			invoiceVO.setInvoiceType(inMemberVO.getInvoiceType());
			invoiceVO.setCarriedBalance(inMemberVO.getCarriedBalance());
			
			invoiceVO.setCreateDate(""+timestamp);
			invoiceVO.setDescription(inMemberVO.getDescription());
			invoiceVO.setInvoiceDate(inMemberVO.getInvoiceDate());
			invoiceVO.setBillCycleID(inMemberVO.getBillingProcessID());
			
		
			applicableChargeList.add(inMemberVO);
			
			
			
			}else if((inMemberVO.getInvoiceTypeID()==10)){
				try {
					
					BigDecimal lateFees=BigDecimal.ZERO;
					invoiceVO.setEntityID(inMemberVO.getEntityID().intValue());
					
					invoiceVO.setInvoiceDate(inMemberVO.getInvoiceDate());
					invoiceVO.setInvoiceTypeID(inMemberVO.getInvoiceTypeID());
					
					lateFees=calculateLateFees.getLateFeeForInvoiceSingle(inMemberVO, societyID, invoiceVO);
					
					grandTotal=grandTotal.add(lateFees);
					
					inMemberVO.setTotalAmount(lateFees);
					invoiceVO.setInvoiceTypeID(inMemberVO.getInvoiceTypeID());
					
					invoiceVO.setDescription(inMemberVO.getDescription());

					invoiceVO.setDueDate(inMemberVO.getDueDate());


					invoiceVO.setFromDate(inMemberVO.getFromDate());
					invoiceVO.setUptoDate(inMemberVO.getUptoDate());
					inMemberVO.setBalance(BigDecimal.ZERO);
					inMemberVO.setCarriedBalance(BigDecimal.ZERO);
					invoiceVO.setInvoiceType(inMemberVO.getInvoiceType());

					invoiceVO.setCarriedBalance(inMemberVO.getCarriedBalance());
					
					invoiceVO.setCreateDate(""+timestamp);
					invoiceVO.setInvoiceDate(inMemberVO.getInvoiceDate());
					invoiceVO.setBillCycleID(inMemberVO.getBillingProcessID());
					
					applicableChargeList.add(inMemberVO);
				} catch (Exception e) {
					logger.error("Here the exception occurred in lateFee "+e);
				}
			invoiceVO.setListOfLineItems(applicableChargeList);
			InvoiceDetailsVO invBal=getInvoiceBalance(invoiceVO, societyID,invoiceVO.getInvoiceTypeID());
			carriedBalance=invBal.getBalance();
			
			
			//logger.info(acHeadVO.getClosingBalance()+"0----------->"+invoiceVO.getCreateDate()+"    ---- >"+invBal.getBalance());
			
			
			invoiceVO.setCarriedBalance(carriedBalance);
			invoiceVO.setBalance(grandTotal);
			
		/*	
			invoiceVO.setInvoiceAmount(grandTotal);
			logger.info("--------1-------->"+grandTotal);
			grandTotal=grandTotal.add(invoiceVO.getCarriedBalance());
			logger.info("--------2-------->"+grandTotal);
			logger.info("--------3-------->"+invoiceVO.getBalance()+" "+invoiceVO.getCurrentBalance());
			invoiceVO.setTotalAmount(grandTotal);
			invoiceVO.setCurrentBalance(invoiceVO.getBalance());
			
			
			if(invoiceVO.getTotalAmount().signum()==0){
				invoiceVO.setBalance(BigDecimal.ZERO);
				invoiceVO.setCurrentBalance(BigDecimal.ZERO);
			}if(invoiceVO.getTotalAmount().signum()<0){
				invoiceVO.setCurrentBalance(BigDecimal.ZERO);
			}
			*/
			}
			
			
			
			
			
		}
		
		
			invoiceVO.setListOfLineItems(applicableChargeList);
			InvoiceDetailsVO invBal=getInvoiceBalance(invoiceVO, societyID,invoiceVO.getInvoiceTypeID());
			carriedBalance=invBal.getBalance();
			
			/*if(invBal.getStatus().equalsIgnoreCase("N")){
				acHeadVO=transactionService.getOpeningClosingBalance(societyID, acHeadVO.getLedgerID(), invoiceVO.getFromDate(), invoiceVO.getUptoDate(), "L");
				carriedBalance=acHeadVO.getClosingBalance();
				
			}*/
			//logger.info(acHeadVO.getClosingBalance()+"0----------->"+invoiceVO.getCreateDate()+"    ---- >"+invBal.getBalance());
			invoiceVO.setCarriedBalance(carriedBalance);
			invoiceVO.setBalance(grandTotal);
			
			invoiceVO.setInvoiceAmount(grandTotal);
			
			grandTotal=grandTotal.add(invoiceVO.getCarriedBalance());
			
			invoiceVO.setTotalAmount(grandTotal);
			//invoiceVO.setCurrentBalance(invoiceVO.getTotalAmount());
			
				currentBalance=invoiceVO.getBalance();
			
			invoiceVO.setCurrentBalance(currentBalance);
			//logger.info(invoiceVO.getBalance()+"------------->>>>>>>>>>>>>>>>>-------"+invoiceVO.getCurrentBalance());
			
			if(invoiceVO.getTotalAmount().signum()==0){
				invoiceVO.setBalance(BigDecimal.ZERO);
				invoiceVO.setCurrentBalance(BigDecimal.ZERO);
			}if(invoiceVO.getTotalAmount().signum()<0){
				invoiceVO.setCurrentBalance(BigDecimal.ZERO);
			}
			if((invoiceVO.getCarriedBalance().signum()<0)&&(invoiceVO.getTotalAmount().signum()>0)){
				invoiceVO.setCurrentBalance(invoiceVO.getTotalAmount());
			}
		
		
		logger.debug(memberChargesVO.getMemberName()+": Name ,Amount :"+invoiceVO.getTotalAmount()+" The line items list contains  "+invoiceVO.getListOfLineItems().size()+" "+invoiceVO.getCurrentBalance());
		}catch (Exception e) {
			logger.error("Exception in calculateApplicableChargesToMember "+e);
		}
		
		logger.debug("Exit : public List generateInvoiceObject(InvoiceMemberChargesVO memberChargesVO)");
		return invoiceVO;
		
		
	}
	
	/* Get list of all unpaid invoices the given society   */
	public List getInvoicesForSociety(int societyID,int billingID ,int status,int invoiceTypeID){
		List invoiceList=null;
		logger.debug("Entry : public List getInvoicesForSociety(String societyID,int billingID ,int status,int invoiceTypeID)");
		
		//invoiceList=invoiceDAO.getInvoicesForSociety(societyID,billingID,status,invoiceTypeID);
		
		for(int i=0;invoiceList.size()>i;i++){
			InvoiceDetailsVO tempInvVO=(InvoiceDetailsVO) invoiceList.get(i);
			List invoiceLineItemsLst=new ArrayList();
			invoiceLineItemsLst=getLineItemsForInvoice(societyID,Integer.parseInt(tempInvVO.getInvoiceID()));
			
			tempInvVO.setListOfLineItems(invoiceLineItemsLst);
			
		}
	
	
		
				
		logger.debug("Exit : getInvoicesForSociety(String societyID,int billingID ,int status,int invoiceTypeID)");
		return invoiceList;		
	}
	
	/* Get list of all inovoices for the period for a member */
	public List getAllInvoicesForMember(int societyID ,int aptID,String fromDate,String uptoDate){
		List invoiceList=null;
		logger.debug("Entry : public List getActiveMemberList(String societyID)");
		
		//invoiceList=invoiceDAO.getInvoicesForMember(societyID, aptID,fromDate,uptoDate,"0");
		for(int i=0;invoiceList.size()>i;i++){
			InvoiceDetailsVO tempInvFrm=(InvoiceDetailsVO) invoiceList.get(i);
			List lineItemsList=new ArrayList();
			lineItemsList=getLineItemsForInvoice(societyID, Integer.parseInt(tempInvFrm.getInvoiceID()));
			tempInvFrm.setListOfLineItems(lineItemsList);
		}
		
		
		logger.debug("Exit : public List getActiveMemberList(String societyID)"+invoiceList.size());
		return invoiceList;		
	}
	
	
	/* Get list of all inovoices for the period for a member */
	public List getInvoicesForMember(int societyID ,int aptID,String fromDate,String uptoDate){
		List invoiceList=null;
		logger.debug("Entry : public List getActiveMemberList(String societyID)");
			
		//invoiceList=invoiceDAO.getInvoicesForMember(societyID, aptID, fromDate, uptoDate,"0");
		for(int i=0;invoiceList.size()>i;i++){
			InvoiceDetailsVO tempInvFrm=(InvoiceDetailsVO) invoiceList.get(i);
			List lineItemsList=new ArrayList();
			lineItemsList=getLineItemsForInvoice(societyID, Integer.parseInt(tempInvFrm.getInvoiceID()));
			tempInvFrm.setListOfLineItems(lineItemsList);
		}
		
		
		logger.debug("Exit : public List getActiveMemberList(String societyID)"+invoiceList.size());
		return invoiceList;		
	}
	
	/* Get list of all inovoices for the period for a member */
	public List getUnpaidInvoicesForApartment(int societyID ,String aptID,int invoiceTypeID){
		List invoiceList=null;
		logger.debug("Entry : public List getUnpaidInvoicesForApartment(String societyID ,String aptID,int invoiceTypeID)");
		
		//invoiceList=invoiceDAO.getUnpaidInvoicesForApartment(societyID ,aptID,invoiceTypeID);
		
		logger.debug("Exit : public getUnpaidInvoicesForApartment(String societyID ,String aptID,int invoiceTypeID)"+invoiceList.size());
		return invoiceList;		
	}
	
	
	/* Get list of all inovoices for the period for a member */
	public List getInvoicesForApartment(int societyID ,int aptID){
		List invoiceList=new ArrayList();
		List invoiceTypeList=new ArrayList();
		logger.debug("Entry : public List getInvoicesForApartment(String societyID ,int aptID,String type)");
		invoiceTypeList=getInvoiceTypeList("S");
		try{
		for(int j=0;invoiceTypeList.size()>j;j++){
		
		InvoiceDetailsVO invTypeVo=(InvoiceDetailsVO) invoiceTypeList.get(j);
		 List	tempInvoiceList=null;
				 //invoiceDAO.getInvoicesForApartment(societyID, aptID,invTypeVo.getInvoiceTypeID() );	
		
		 
		 InvoiceDetailsVO invoiceVO=new InvoiceDetailsVO();
		 	if(tempInvoiceList.size()!=0){
			List lineItemsList=new ArrayList();
			
			int i=tempInvoiceList.size();
			
			invoiceVO=(InvoiceDetailsVO) tempInvoiceList.get(i-1);
			lineItemsList=getLineItemsForInvoice(societyID, Integer.parseInt(invoiceVO.getInvoiceID()));
			invoiceVO.setIsApplicable(true);
			invoiceVO.setListOfLineItems(lineItemsList);
			
			DateUtility dateUtility=new DateUtility();
			Date date=dateUtility.findCurrentDate();
			invoiceVO.setInvoiceDate(date+"");
			InvoiceDetailsVO tempInvoiceVO=getInvoiceBalance(invoiceVO, societyID,Integer.parseInt(invoiceVO.getInvoiceID()) );
			
			invoiceVO.setBalance(tempInvoiceVO.getBalance());
			
			if(invoiceVO.getBalance().signum()<0){
				invoiceVO.setPaidAmount(BigDecimal.ZERO);
			}else
			invoiceVO.setPaidAmount(invoiceVO.getBalance());
			invoiceVO.setDiscount(BigDecimal.ZERO);
			invoiceList.add(invoiceVO);
		}
		
		
		}}catch (Exception e) {
			logger.info("Exception : getInvoicesForApartment "+e);
		}
		logger.debug("Exit : public List getInvoicesForApartment(String societyID ,int aptID,String type)"+invoiceList.size());
		return invoiceList;		
	}
	
	public List getSingleInvoiceForMember(int societyID ,int aptID,String invoiceID){
		List invoiceList=null;
		logger.debug("Entry : public List getSingleInvoiceForMember(String societyID ,String aptID,String invoiceID)");
			
		//invoiceList=invoiceDAO.getInvoicesForMember(societyID, aptID,"","",invoiceID);
		for(int i=0;invoiceList.size()>i;i++){
			InvoiceDetailsVO tempInvFrm=(InvoiceDetailsVO) invoiceList.get(i);
			List lineItemsList=new ArrayList();
			lineItemsList=getLineItemsForInvoice(societyID, Integer.parseInt(tempInvFrm.getInvoiceID()));
			tempInvFrm.setListOfLineItems(lineItemsList);
		}
		
		
		logger.debug("Exit : public List getSingleInvoiceForMember(String societyID ,String aptID,String invoiceID)"+invoiceList.size());
		return invoiceList;		
	}
	/* Get list of all line items of the given invoice   */
	public List getLineItemsForInvoice(int societyID ,int invoiceID){
		List invoiceList=null;
		logger.debug("Entry : public List getLineItemsForInvoice(String societyID)");
		
		invoiceList=invoiceDAO.getLineItemsForInvoice(societyID, invoiceID);
		
		logger.debug("Exit : public List getLineItemsForInvoice(String societyID)"+invoiceList.size());
		return invoiceList;		
	}
	/* Insert the invoice  */
	public int insertNewInvoice(int societyID,int apartmentID){
		int insertFlag=0;
		List memberChargeList=null;
		logger.debug("Entry : public int insertNewInvoice(int societyID,int apartmentID)");
		
		//memberChargeList=getApplicableChargesToMember(societyID, apartmentID);
		
		logger.debug("Exit : public int insertNewInvoice(int societyID,int apartmentID)");
		return insertFlag;		
	}
	
	public List getCharges(InvoiceDetailsVO invoiceVO,InvoiceBillingCycleVO bcVO,int isCommon){
		List memberChargeList=null;
		logger.debug("Entry : public List getCharges(int societyID,int aptID)");
			InvoiceMemberChargesVO memberChargesVO=invoiceVO.getMemberVO();
		
		memberChargeList=getApplicableChargesToMember(invoiceVO,bcVO,isCommon);
		logger.debug("All charges are "+memberChargeList.size());
		for(int i=0;memberChargeList.size()>i;i++){
			InvoiceMemberChargesVO inLIVO=(InvoiceMemberChargesVO) memberChargeList.get(i); 
			inLIVO=getConditionalBalance(inLIVO,memberChargesVO.getSocietyID(), memberChargesVO);
			if(inLIVO.getIsApplicable()){
				logger.debug(inLIVO.getCategoryName()+" this charge is not applicable");
			}else{
				memberChargeList.remove(i);
				--i;
			}
			
			
		}
		
		logger.debug("Exit : public List getApplicableChargesToMember(InvoiceDetailsVO inoiceVO,InvoiceBillingCycleVO bcVO,int isCommon)"+memberChargeList.size());
		return memberChargeList;		
	}
	
	/* Get list of all applicable fees for the given member  */
	public List getApplicableChargesToMember(InvoiceDetailsVO invoiceVO,InvoiceBillingCycleVO bcVO,int isCommon){
		List memberChargeList=new ArrayList();
		logger.debug("Entry : public List getApplicableChargesToMember(InvoiceDetailsVO inoiceVO,InvoiceBillingCycleVO bcVO,int isCommon)");
		try{
			
					
		//memberChargeList=invoiceDAO.getApplicableCharges(invoiceVO);
		
	
		
		
		}catch(Exception e){
			logger.error("Exception at getApplicableChargesToMember "+e);
		}
		
		logger.debug("Exit : public List getApplicableChargesToMember(InvoiceDetailsVO inoiceVO,InvoiceBillingCycleVO bcVO,int isCommon)");
		return memberChargeList;		
	}
	
	/* Get list of all applicable fees for the given member  */
	public List getChargeList(int aptID,int societyID,InvoiceMemberChargesVO memberChrgVO){
		List memberChargeList=new ArrayList();
		logger.debug("Entry : public List getChargList(int aptID,int societyID)");
		try{
			
					
	//	memberChargeList=invoiceDAO.getChargList(aptID, societyID);
		
		
		
		logger.debug("All charges are "+memberChargeList.size());
		for(int i=0;memberChargeList.size()>i;i++){
			InvoiceMemberChargesVO inLIVO=(InvoiceMemberChargesVO) memberChargeList.get(i); 
			inLIVO=getConditionalBalance(inLIVO,societyID, memberChrgVO);
			if(inLIVO.getIsApplicable()){
				logger.debug(inLIVO.getCategoryName()+" this charge is  applicable");
			}else{
				logger.debug(inLIVO.getCategoryName()+" this charge is not applicable");
				memberChargeList.remove(i);
				--i;
			}
			
			
		}
	
		
		
		}catch(Exception e){
			logger.error("Exception at public List getChargList(int aptID,int societyID) "+e);
		}
		
		logger.debug("Exit : public List getChargList(int aptID,int societyID)"+memberChargeList.size());
		return memberChargeList;		
	}
	
	
	/* Calculate applicable fees for the given member  */
	public InvoiceDetailsVO calculateApplicableChargesToMember(InvoiceDetailsVO invoiceVO,InvoiceBillingCycleVO bcVO,int invoiceTypeID){
		List memberChargeList=null;
		BigDecimal grandTotal=BigDecimal.ZERO;
		BigDecimal carriedBalance=BigDecimal.ZERO;
		BigDecimal currentBalance=BigDecimal.ZERO;
		InvoiceMemberChargesVO memberVO=invoiceVO.getMemberVO();
		try{
		logger.debug("Entry : public List getApplicableChargesToMember(int societyID,int memberID)"+memberVO.getAptID());
		Date currentDatetime = new Date(System.currentTimeMillis());   
	    java.sql.Date sqlDate = new java.sql.Date(currentDatetime.getTime());   
		java.sql.Timestamp timestamp = new java.sql.Timestamp(currentDatetime.getTime());
		
		int societyID=bcVO.getSocietyId();
		memberVO.setFromDate(bcVO.getFromDate());
		memberVO.setUptoDate(bcVO.getUptoDate());
		AccountHeadVO acHeadVO=new AccountHeadVO();
		acHeadVO.setLedgerID(memberVO.getLedgerID());
		invoiceVO.setAptID(memberVO.getAptID());
		invoiceVO.setInvoiceDate(bcVO.getInvoiceDate());
		
		List applicableChargeList=new ArrayList();
		//memberChargeList=invoiceDAO.getApplicableCharges(invoiceVO);
		if(memberChargeList.size()!=0){
		for(int i=0;memberChargeList.size()>i;i++){
			
			
			InvoiceMemberChargesVO inMemberVO=(InvoiceMemberChargesVO) memberChargeList.get(i);
			
			inMemberVO=getConditionalBalance(inMemberVO, societyID,memberVO);
			
			
			
			if((inMemberVO.getIsApplicable())&&(inMemberVO.getInvoiceTypeID()!=10)&&(invoiceTypeID!=10)){
				grandTotal=grandTotal.add(inMemberVO.getTotalAmount());
				
			
			invoiceVO.setDueDate(bcVO.getDueDate());
			
			invoiceVO.setCarriedBalance(inMemberVO.getCarriedBalance());
			invoiceVO.setFromDate(memberVO.getFromDate());
			invoiceVO.setUptoDate(memberVO.getUptoDate());
			
			inMemberVO.setBalance(BigDecimal.ZERO);
			inMemberVO.setCarriedBalance(BigDecimal.ZERO);
			
			invoiceVO.setInvoiceType(inMemberVO.getInvoiceType());
			invoiceVO.setCarriedBalance(inMemberVO.getCarriedBalance());
			invoiceVO.setCreateDate(""+timestamp);
			currentBalance=invoiceVO.getBalance();
			
			invoiceVO.setCurrentBalance(currentBalance);
			
			invoiceVO.setInvoiceTypeID(inMemberVO.getInvoiceTypeID());
			invoiceVO.setBillCycleID(bcVO.getBillingCycleID());
			applicableChargeList=memberChargeList;
			
			}else if((inMemberVO.getIsApplicable())&&(inMemberVO.getInvoiceTypeID()==10)&&(invoiceTypeID==10)){
				BigDecimal lateFee=BigDecimal.ZERO;
				invoiceVO.setEntityID(inMemberVO.getEntityID().intValue());
				
				invoiceVO.setInvoiceTypeID(inMemberVO.getInvoiceTypeID());
				lateFee=calculateLateFees.getLateFeeForInvoice(inMemberVO, societyID, invoiceVO);
			
				
				grandTotal=grandTotal.add(lateFee);
				inMemberVO.setTotalAmount(lateFee);
				
				invoiceVO.setInvoiceTypeID(inMemberVO.getInvoiceTypeID());
				
				invoiceVO.setInvoiceType(inMemberVO.getInvoiceType());
			
			invoiceVO.setDueDate(bcVO.getDueDate());
			
			
			invoiceVO.setFromDate(memberVO.getFromDate());
			invoiceVO.setUptoDate(memberVO.getUptoDate());
			inMemberVO.setBalance(BigDecimal.ZERO);
			inMemberVO.setCarriedBalance(BigDecimal.ZERO);
			
			
			invoiceVO.setCarriedBalance(inMemberVO.getCarriedBalance());
			invoiceVO.setCreateDate(""+timestamp);
			
				currentBalance=invoiceVO.getBalance();
			
			invoiceVO.setCurrentBalance(currentBalance);
			
			
			invoiceVO.setBillCycleID(bcVO.getBillingCycleID());
			applicableChargeList.add(inMemberVO);
				
			
			}else{
				memberChargeList.remove(i);
				--i;
			}
			
			
			
			
		}
		invoiceVO.setListOfLineItems(applicableChargeList);
		InvoiceDetailsVO updateVO=new InvoiceDetailsVO();
		updateVO=invoiceVO;
		
		invoiceVO.setInvoiceDate(bcVO.getInvoiceDate());
		InvoiceDetailsVO invBal=getInvoiceBalance(invoiceVO, societyID,invoiceVO.getInvoiceTypeID());
		 carriedBalance=invBal.getBalance();
		 /*
			if(invBal.getStatus().equalsIgnoreCase("N")&&(invoiceTypeID!=10)){
				logger.debug(societyID+"------"+acHeadVO.getLedgerID()+" -- "+updateVO.getFromDate()+" "+updateVO.getUptoDate());
				acHeadVO=ledgerService.getOpeningClosingBalance(societyID, acHeadVO.getLedgerID(), updateVO.getFromDate(), updateVO.getUptoDate(), "L");
				carriedBalance=acHeadVO.getClosingBalance();
				
			}*/
		 
		invoiceVO.setCarriedBalance(carriedBalance);
		invoiceVO.setBalance(grandTotal);
	
		invoiceVO.setInvoiceAmount(grandTotal);
		
		grandTotal=grandTotal.add(invoiceVO.getCarriedBalance());
		
		invoiceVO.setTotalAmount(grandTotal);
		
			currentBalance=invoiceVO.getBalance();
		
		invoiceVO.setCurrentBalance(currentBalance);
		
		if(invoiceVO.getTotalAmount().signum()<1){
			invoiceVO.setCurrentBalance(BigDecimal.ZERO);
		}
		if((invoiceVO.getCarriedBalance().signum()<0)&&(invoiceVO.getTotalAmount().signum()>0)){
			invoiceVO.setCurrentBalance(invoiceVO.getTotalAmount());
		}
		
		
		}
		logger.debug(memberVO.getMemberName()+": Name ,Amount :"+invoiceVO.getTotalAmount()+" The line items list contains  "+invoiceVO.getListOfLineItems().size());
		
		}catch (NullPointerException e) {
			logger.debug("No charges applicable for  "+memberVO.getMemberName());
			invoiceVO.setCarriedBalance(BigDecimal.ZERO);
			invoiceVO.setInvoiceAmount(BigDecimal.ZERO);
			invoiceVO.setTotalAmount(BigDecimal.ZERO);
		}
		catch (Exception e) {
			logger.error("Exception in calculateApplicableChargesToMember "+e);
		}
		
		logger.debug("Exit : public List getApplicableChargesToMember(int societyID,int memberID)");
		return invoiceVO;		
	}
	
	
	/* Insert the invoice  */
	public int insertInvoice(InvoiceDetailsVO invoiceVO,int societyID){
		int flag=0;
		int insertInvoiceFlag=0;
		int insertInvoiceLineItemsFlag=0;
		InvoiceMemberChargesVO memberVO=invoiceVO.getMemberVO();
		TransactionVO txVO=new TransactionVO();
		
		try{
		
		logger.debug("Entry : public int insertInvoice(int societyID,List memberChargeList)");
		
		logger.debug("Memeber "+invoiceVO.getFromDate()+"  "+invoiceVO.getCurrentBalance()+" "+memberVO.getLedgerID());
		
		
		
		if(invoiceVO.getTotalAmount().signum()==0){
			invoiceVO.setStatusID(2);
			
		}if(invoiceVO.getTotalAmount().signum()<0){
			invoiceVO.setStatusID(3);
		}else{
			invoiceVO.setBalance(invoiceVO.getInvoiceAmount());
		}
		
		invoiceVO.setAptID(memberVO.getAptID());
		invoiceVO.setLedgerID(memberVO.getLedgerID());
		//insertInvoiceFlag=invoiceDAO.insertInvoice(invoiceVO);
		
		invoiceVO.setInvoiceID(""+insertInvoiceFlag);
		invoiceVO=convertMemberChargeToInvoiceLineItems(invoiceVO);
		//insertInvoiceLineItemsFlag=invoiceDAO.insertInvoiceLineItems(invoiceVO);
		//txVO=convertInvoiceVOTOTx(invoiceVO,societyID);
		//logger.info("--->"+txVO.getAmount());
		//transactionService.addTransaction(txVO, societyID);
		
		if((insertInvoiceLineItemsFlag==1)){
			flag=insertInvoiceFlag;
		}
		}catch (Exception e) {
			logger.error("Exception in insertInvoice "+e);
		}
		logger.debug("Exit : public int insertInvoice(int societyID,List memberChargeList)");
		return flag;		
	}
	
	/* Update the charge period after adding the invoice  */
	public int updateChargeStatus(InvoiceDetailsVO invoiceVO,int societyID,InvoiceMemberChargesVO chargeVO){
		int insertFlag=0;
		logger.debug("Entry : public int updateChargeStatus(int societyID,int apartmentID)");
		
		//insertFlag=invoiceDAO.updateChargeStatus(invoiceVO, societyID, chargeVO);
		
		logger.debug("Exit : public int updateChargeStatus(int societyID,int apartmentID)");
		return insertFlag;		
	}
	
	
	/* Update the invoice  */
	public int updateInvoice(InvoiceDetailsVO invoiceVO,int societyID){
		int updateFlag=0;
		
		logger.debug("Entry : public int updateInvoice(InvoiceDetailsVO invoiceVO,int societyID)");
		
		try {
		//	updateFlag=invoiceDAO.updateInvoice(invoiceVO, societyID);
		} catch (Exception e) {
			logger.error("Exception Occurred while updating Invoices "+e);
		}
		
		logger.debug("Exit : public int updateInvoice(InvoiceDetailsVO invoiceVO,int societyID)");
		return updateFlag;		
	}
	
	/* Delete the invoice  */
	public int cancelInvoice(InvoiceVO invoiceVO){
		int insertFlag=0;
		int deleteJE=0;
		
		logger.debug("Entry : public int cancelInvoice(InvoiceVO invoiceVO)");
		
		//insertFlag=invoiceDAO.deleteInvoice(invoiceID, societyID);
		
		if(insertFlag==1){
			deleteJE=transactionService.deleteJournalEntries(invoiceVO);
		}
		
		logger.debug("Exit : public int cancelInvoice(int invoiceID,int societyID)");
		return insertFlag;		
	}
	

	/* Convert Invoice VO to TxVO  */
	private TransactionVO convertInvoiceVOTOTx(InvoiceDetailsVO invoiceVO,int societyID,int isAutoComment){
		TransactionVO txVO=new TransactionVO();
		List<LedgerEntries> ledgerEntriesList=new ArrayList<LedgerEntries>();
		List lineItemsList;
		LedgerEntries revLedgerEntry=new LedgerEntries();
		try{
		logger.debug("Entry : private TransactionVO convertInvoiceVOTOTx(InvoiceDetailsVO invoiceVO)");
		
		txVO.setTransactionNumber(invoiceVO.getInvoiceID()+"");
		txVO.setInvoiceID(Integer.parseInt(invoiceVO.getInvoiceID()));
		txVO.setAmount(invoiceVO.getInvoiceAmount().abs());
		txVO.setTransactionDate(invoiceVO.getInvoiceDate());
		txVO.setTransactionType("Journal");
		if(isAutoComment==0){
			txVO.setDescription(invoiceVO.getDescription());
		}else
		txVO.setDescription("For the period of "+dateUtil.getConvetedDate(invoiceVO.getFromDate())+" to "+dateUtil.getConvetedDate(invoiceVO.getUptoDate()));
		
		lineItemsList=invoiceVO.getListOfLineItems();
		
		for(int i=0;lineItemsList.size()>i;i++){
			InvoiceLineItemsVO lineItem=(InvoiceLineItemsVO) lineItemsList.get(i);
			LedgerEntries ledgerEntry=new LedgerEntries();
			ledgerEntry=convertInvoiceLineItemsTOLedgerEntries(lineItem);
			
			ledgerEntriesList.add(i, ledgerEntry);
			
		}
		
		revLedgerEntry=convertInvoiceTOLedgerEntries(invoiceVO,societyID);
		
		ledgerEntriesList.add(revLedgerEntry);
		txVO.setLedgerEntries(ledgerEntriesList);
		
		
		
		
		}catch(Exception e){
			
			logger.error("Exception : convertInvoiceVoToTx "+e);
		}
		
		logger.debug("Exit : private TransactionVO convertInvoiceVOTOTx(InvoiceDetailsVO invoiceVO)");
		return txVO;		
	}
	
	/* Convert Invoice VO to TxVO  */
	private LedgerEntries convertInvoiceLineItemsTOLedgerEntries(InvoiceLineItemsVO invoiceVO){
		LedgerEntries ledgerEntry=new LedgerEntries();
		
		logger.debug("Entry : private LedgerEntries convertInvoiceLineItemsTOLedgerEntries(InvoiceLineItemsVO invoiceVO)"+invoiceVO.getTotalAmount());
		
		ledgerEntry.setAmount(invoiceVO.getTotalAmount());
		if(ledgerEntry.getAmount().signum()>0){
		ledgerEntry.setCreditDebitFlag("C");
		}else ledgerEntry.setCreditDebitFlag("D");
		ledgerEntry.setLedgerID(invoiceVO.getCategoryID());
		ledgerEntry.setStrComments(invoiceVO.getDescription());
		
			
		
		
		logger.debug("Exit : private LedgerEntries convertInvoiceLineItemsTOLedgerEntries(InvoiceLineItemsVO invoiceVO)");
		return ledgerEntry;		
	}
	/* Convert Invoice VO to TxVO  */
	private LedgerEntries convertInvoiceTOLedgerEntries(InvoiceDetailsVO invoiceVO,int societyID){
		LedgerEntries ledgerEntry=new LedgerEntries();
		
		logger.debug("Entry : private LedgerEntries convertInvoiceLineItemsTOLedgerEntries(InvoiceLineItemsVO invoiceVO)");
		InvoiceMemberChargesVO meChargesVO=invoiceDAO.getActiveMemberVO(societyID, invoiceVO.getAptID());
		
		
		ledgerEntry.setAmount(invoiceVO.getInvoiceAmount());
		
		if(ledgerEntry.getAmount().signum()>0){
			ledgerEntry.setCreditDebitFlag("D");
			}else ledgerEntry.setCreditDebitFlag("C");
		ledgerEntry.setLedgerID(meChargesVO.getLedgerID());
		ledgerEntry.setStrComments("Auto generated journal entry from "+invoiceVO.getFromDate()+" to "+invoiceVO.getUptoDate());
		
		logger.debug("amount >"+ledgerEntry.getAmount()+" ledgerID -->"+ledgerEntry.getLedgerID()+"  "+ledgerEntry.getStrComments());
			
		
		
		logger.debug("Exit : private LedgerEntries convertInvoiceLineItemsTOLedgerEntries(InvoiceLineItemsVO invoiceVO)");
		return ledgerEntry;		
	}
	
	
	
	
	public int generateJournalEntries(int societyID ,List invoiceList,int isAutoComment){
		int success=0;
		
		logger.debug("Entry : public int generateJournalEntries(String societyID ,List invoiceList)");
		
		
		try{
			if(invoiceList.size()==0){
				success=3;
			}else{
		for(int i=0;invoiceList.size()>i;i++){
			TransactionVO txVO=new TransactionVO();
			InvoiceDetailsVO invoiceVO=(InvoiceDetailsVO) invoiceList.get(i);
			
			if(!invoiceVO.getInvoiceType().equalsIgnoreCase("DISCOFFER")){
			List lineItemsList=getLineItemsForInvoice(societyID, Integer.parseInt(invoiceVO.getInvoiceID()));
			invoiceVO.setListOfLineItems(lineItemsList);
			
			
			txVO=convertInvoiceVOTOTx(invoiceVO,societyID,isAutoComment);
			
			//success=transactionService.addTransaction(txVO, societyID);
			}else{
				success=2;
			}
			
		}
			}
		}catch (Exception e) {
			logger.error("Exception in generateJournalEntries "+e);
		}
		
		logger.debug("Entry : public int generateJournalEntries(String societyID ,List invoiceList)");
		return success;
	}
	
	/* Add manualInvoices with journal entries */
	public int addInvoice(List invoiceList,int societyID,int isAutoComment){
		logger.debug("Entry : public int addInvoice(List invoiceList,String societyID)");
		int success=0;
		
		for(int i=0;invoiceList.size()>i;i++){
			int insertFlag=0;
			List invList=new ArrayList();
			List memberChargeList=new ArrayList();
			InvoiceDetailsVO invoiceVO=(InvoiceDetailsVO) invoiceList.get(i);
			
			memberChargeList=invoiceVO.getListOfLineItems();
			insertFlag=insertInvoice(invoiceVO, societyID);
			if(insertFlag!=0){
				invList.add(0, invoiceVO);
				
				success=generateJournalEntries(societyID, invList,isAutoComment);
			}
			if(insertFlag!=0){
				for(int j=0;memberChargeList.size()>j;j++){
					InvoiceMemberChargesVO livo=(InvoiceMemberChargesVO) memberChargeList.get(j);
					insertFlag=updateChargeStatus(invoiceVO, societyID, livo);
				}
			}
			
			
			
		}
		
		
		logger.debug("Entry : public int addInvoice(List invoiceList,String societyID)");
		return success;
	}
	
	public int updateStatusOfInvoices(String societyID,int billingID){
		int success=0;
		
		// success=invoiceDAO.updateStatusOfInvoices(societyID,billingID);
		
		return success;
	}
	
	private InvoiceMemberChargesVO getConditionalBalance(InvoiceMemberChargesVO inVo,int societyID,InvoiceMemberChargesVO memberVO){
		logger.debug("Entry : private InvoiceLineItemsVO getConditionalBalance(InvoiceLineItemsVO inVo,String societyID)"+inVo.getIsApplicable()+"  "+inVo.getChargeID());
		try{
		if(!inVo.getCalculationMethod().equalsIgnoreCase("S")){
			
		if(inVo.getCalculationMethod().equalsIgnoreCase("t")){
			if(memberVO.getUnitID()!=(inVo.getEntityID().intValue())){
				logger.debug("Not applicable for this type");
				inVo.setIsApplicable(false);
				inVo.setTotalAmount(BigDecimal.ZERO);
			}
			
		}else if((inVo.getCalculationMethod().equalsIgnoreCase("a"))||(inVo.getCalculationMethod().equalsIgnoreCase("AL"))){
			if(memberVO.getAptID()!=(inVo.getEntityID().intValue())){
				logger.debug("Not applicable for this apartment");
				inVo.setIsApplicable(false);
				inVo.setTotalAmount(BigDecimal.ZERO);
			}
		}else if(inVo.getCalculationMethod().equalsIgnoreCase("b")){
			if(memberVO.getBuildingID()!=(inVo.getEntityID().intValue())){
				logger.debug("Not applicable for this Building");
				inVo.setIsApplicable(false);
				inVo.setTotalAmount(BigDecimal.ZERO);
			}
		}else if(inVo.getCalculationMethod().equalsIgnoreCase("C")){
			BigDecimal due=BigDecimal.ZERO;
			BigDecimal percentage=inVo.getTotalAmount().multiply(new BigDecimal("0.01")).setScale(6);
			due=memberVO.getUnitCost().multiply(percentage).setScale(inVo.getPrecison(),RoundingMode.HALF_UP);
			inVo.setTotalAmount(due);
		}else if(inVo.getCalculationMethod().equalsIgnoreCase("sq")){
			if(memberVO.getSquareFt().intValue()!=(inVo.getEntityID().intValue())){
				logger.debug("Not applicable for this Building");
				inVo.setIsApplicable(false);
				inVo.setTotalAmount(BigDecimal.ZERO);
			}
		}else if(inVo.getCalculationMethod().equalsIgnoreCase("sqf")){
					//Square feet calculation with a fixwd formula
					BigDecimal due=BigDecimal.ZERO;
					due=memberVO.getSquareFt().multiply(inVo.getTotalAmount()).setScale(inVo.getPrecison(),RoundingMode.HALF_UP);
					inVo.setIsApplicable(true);
					inVo.setTotalAmount(due);
					
				
		}else if(inVo.getCalculationMethod().equalsIgnoreCase("g")){
				if(memberVO.getInvoiceGroups().length()>0){
				String str = memberVO.getInvoiceGroups();
				ArrayList aList= new ArrayList(Arrays.asList(str.split(",")));
				String entity=inVo.getEntityID().toString();
				
				
			
					 if((inVo.getParams().equalsIgnoreCase("t"))&&(aList.contains("t"))){
					if(memberVO.getUnitID()!=(inVo.getEntityID().intValue())){
						logger.debug("Not applicable for this type");
						inVo.setIsApplicable(false);
						inVo.setTotalAmount(BigDecimal.ZERO);
					}
					
				}else if((inVo.getParams().equalsIgnoreCase("a"))||(inVo.getCalculationMethod().equalsIgnoreCase("AL"))&&(aList.contains("a"))){
					if(memberVO.getAptID()!=(inVo.getEntityID().intValue())){
						logger.debug("Not applicable for this apartment");
						inVo.setIsApplicable(false);
						inVo.setTotalAmount(BigDecimal.ZERO);
					}
				}else if((inVo.getParams().equalsIgnoreCase("b"))&&(aList.contains("b"))){
					if(memberVO.getBuildingID()!=(inVo.getEntityID().intValue())){
						logger.debug("Not applicable for this Building");
						inVo.setIsApplicable(false);
						inVo.setTotalAmount(BigDecimal.ZERO);
					}
				}else if((inVo.getParams().equalsIgnoreCase("C"))&&(aList.contains("c"))){
					BigDecimal due=BigDecimal.ZERO;
					if(aList.contains(entity)){
					BigDecimal percentage=inVo.getTotalAmount().multiply(new BigDecimal("0.01")).setScale(6);
					due=memberVO.getUnitCost().multiply(percentage).setScale(inVo.getPrecison(),RoundingMode.HALF_UP);
					inVo.setTotalAmount(due);
					}else inVo.setIsApplicable(false);
					
				}else if((inVo.getParams().equalsIgnoreCase("sq"))&&(aList.contains("sq"))){
					if(!aList.contains(entity)){
						logger.debug("Not applicable for this Building");
						inVo.setIsApplicable(false);
						inVo.setTotalAmount(BigDecimal.ZERO);
					}
				}else if((inVo.getParams().equalsIgnoreCase("sqf"))&&(aList.contains("sqf"))){
							//Square feet calculation with a fixwd formula
					if(aList.contains(entity)){
							BigDecimal due=BigDecimal.ZERO;
							due=memberVO.getSquareFt().multiply(inVo.getTotalAmount()).setScale(inVo.getPrecison(),RoundingMode.HALF_UP);
							inVo.setIsApplicable(true);
							if(inVo.getPrecison()==10){
								due=new BigDecimal(due.setScale(-1, RoundingMode.UP).toPlainString());
							}
						
							
							inVo.setTotalAmount(due);
							
						
					}else inVo.setIsApplicable(false);
				}
					 else{
				
					
						if(memberVO.getIsRented()==0){
											
							inVo.setIsApplicable(false);
							inVo.setTotalAmount(BigDecimal.ZERO);
						}				
					if((inVo.getParams().equalsIgnoreCase("TR")&&(aList.contains("tr")))){
						if(memberVO.getUnitID()!=(inVo.getEntityID().intValue())){
							logger.debug("Not applicable for this type");
							inVo.setIsApplicable(false);
							inVo.setTotalAmount(BigDecimal.ZERO);
						}
						
					}else if((inVo.getParams().equalsIgnoreCase("AR")&&(aList.contains("ar")))){
						if(memberVO.getAptID()!=(inVo.getEntityID().intValue())){
							logger.debug("Not applicable for this apartment");
							inVo.setIsApplicable(false);
							inVo.setTotalAmount(BigDecimal.ZERO);
						}
					}else if((inVo.getParams().equalsIgnoreCase("BR"))&&(aList.contains("br"))){
						if(memberVO.getBuildingID()!=(inVo.getEntityID().intValue())){
							logger.debug("Not applicable for this Building");
							inVo.setIsApplicable(false);
							inVo.setTotalAmount(BigDecimal.ZERO);
						}
					}else if((inVo.getParams().equalsIgnoreCase("CR"))&&(aList.contains("cr"))){
						BigDecimal due=BigDecimal.ZERO;
						BigDecimal percentage=inVo.getTotalAmount().multiply(new BigDecimal("0.01")).setScale(6);
						due=memberVO.getUnitCost().multiply(percentage).setScale(inVo.getPrecison(),RoundingMode.HALF_UP);
						inVo.setTotalAmount(due);
					}
					else if((inVo.getParams().equalsIgnoreCase("sqfr"))&&(aList.contains("sqf"))){
						if(aList.contains(entity)){
							//Square feet calculation with a fixwd formula
						
						BigDecimal due=BigDecimal.ZERO;
						
						BigDecimal rental=inVo.getTotalAmount().divide(new BigDecimal("10"));
						
						due=memberVO.getSquareFt().multiply(rental).setScale(inVo.getPrecison(),RoundingMode.HALF_UP);
						if(due.compareTo(BigDecimal.ZERO)==0){
							inVo.setIsApplicable(false);
						}
						if(inVo.getPrecison()==10){
							due=new BigDecimal(due.setScale(-1, RoundingMode.UP).toPlainString());
						}
					
						inVo.setTotalAmount(due);
						}else inVo.setIsApplicable(false);
					}else if((inVo.getParams().equalsIgnoreCase("sqr"))&&(aList.contains("sq"))){
						if(!aList.contains(entity)){
							logger.debug("Not applicable for this Square feet");
							inVo.setIsApplicable(false);
							inVo.setTotalAmount(BigDecimal.ZERO);
						}
					
				}
				}
		}else{
			inVo.setIsApplicable(false);
		}
		}
		
		
		
		}
		if((inVo.getCalculationMethod().equalsIgnoreCase("R"))||(inVo.getCalculationMethod().equalsIgnoreCase("TR"))||(inVo.getCalculationMethod().equalsIgnoreCase("CR"))||(inVo.getCalculationMethod().equalsIgnoreCase("BR"))||(inVo.getCalculationMethod().equalsIgnoreCase("AR"))||(inVo.getCalculationMethod().equalsIgnoreCase("SQR"))||(inVo.getCalculationMethod().equalsIgnoreCase("sqfr"))){
			if(memberVO.getIsRented()==0){
								
				inVo.setIsApplicable(false);
				inVo.setTotalAmount(BigDecimal.ZERO);
			}if(inVo.getCalculationMethod().equalsIgnoreCase("TR")){
				if(memberVO.getUnitID()!=(inVo.getEntityID().intValue())){
					logger.debug("Not applicable for this type");
					inVo.setIsApplicable(false);
					inVo.setTotalAmount(BigDecimal.ZERO);
				}
				
			}else if(inVo.getCalculationMethod().equalsIgnoreCase("AR")){
				if(memberVO.getAptID()!=(inVo.getEntityID().intValue())){
					logger.debug("Not applicable for this apartment");
					inVo.setIsApplicable(false);
					inVo.setTotalAmount(BigDecimal.ZERO);
				}
			}else if(inVo.getCalculationMethod().equalsIgnoreCase("BR")){
				if(memberVO.getBuildingID()!=(inVo.getEntityID().intValue())){
					logger.debug("Not applicable for this Building");
					inVo.setIsApplicable(false);
					inVo.setTotalAmount(BigDecimal.ZERO);
				}
			}else if(inVo.getCalculationMethod().equalsIgnoreCase("CR")){
				BigDecimal due=BigDecimal.ZERO;
				BigDecimal percentage=inVo.getTotalAmount().multiply(new BigDecimal("0.01")).setScale(6);
				due=memberVO.getUnitCost().multiply(percentage).setScale(inVo.getPrecison(),RoundingMode.HALF_UP);
				inVo.setTotalAmount(due);
			}
			else if(inVo.getCalculationMethod().equalsIgnoreCase("sqfr")){
				//Square feet calculation with a fixwd formula
				BigDecimal due=BigDecimal.ZERO;
				BigDecimal rental=inVo.getTotalAmount().divide(new BigDecimal("10")).setScale(2);
				due=memberVO.getSquareFt().multiply(rental).setScale(inVo.getPrecison(),RoundingMode.UP);
				if(due.compareTo(BigDecimal.ZERO)==0){
					inVo.setIsApplicable(false);
				}
				
				inVo.setTotalAmount(due);
			}else if(inVo.getCalculationMethod().equalsIgnoreCase("sqr")){
				if(memberVO.getSquareFt().intValue()!=(inVo.getEntityID().intValue())){
					logger.debug("Not applicable for this Square feet");
					inVo.setIsApplicable(false);
					inVo.setTotalAmount(BigDecimal.ZERO);
				}
			
		}
		}
		
		}catch(Exception e){
			logger.error("Exception in getConditionalBalance : "+e+" "+e.getMessage());
		}
		logger.debug("Exit : private InvoiceLineItemsVO getConditionalBalance(InvoiceLineItemsVO inVo,String societyID)"+inVo.getIsApplicable()+"  "+inVo.getChargeID() );
		return inVo;
	}
	
	
	/* Add new charge to a member  */
	public int insertMemberCharge(InvoiceMemberChargesVO inLIVO,String societyID){
		int insertFlag=0;
		logger.debug("Entry : public int insertMemberCharge(InvoiceLineItemsVO inLIVO,String societyID)");
		
		//insertFlag=invoiceDAO.insertMemberCharge(inLIVO,societyID);
		
		logger.debug("Exit : public int insertMemberCharge(InvoiceLineItemsVO inLIVO,String societyID)");
		return insertFlag;		
	}
	
	/* Delete a charge of member  */
	public int deleteMemberCharge(InvoiceMemberChargesVO chargeVO){
		int insertFlag=0;
		logger.debug("Entry : public int deleteMemberCharge(InvoiceMemberChargeVO inLIVO)");
		
		//insertFlag=invoiceDAO.deleteMemberCharge(chargeVO);
		
		logger.debug("Exit : public int deleteMemberCharge(InvoiceMemberChargeVO inLIVO)");
		return insertFlag;		
	}
	
	
	
	
	private String generateKeyString(InvoiceDetailsVO invoiceVO,String societyID){
		String keyString="";
		logger.debug("Entry : private String generateKeyString(InvoiceDetailsVO invoiceVO,String societyID)");
		
		// keyString contains first 3 letters as societyID ,next 5 are aptID, next 5 are invoiceID
		
		try {
			//String sctID = String.format("%03d", societyID);
			//logger.info("-------------->"+sctID);
			String aptID = String.format("%05d", invoiceVO.getAptID());
			logger.info("-------------->"+aptID);
			String invoiceID = String.format("%05d", invoiceVO.getInvoiceID());
			logger.info("-------------->"+invoiceID);
			
			//keyString=sctID.concat(aptID);
			
			logger.info("-------------->"+keyString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception in genrateKeyString "+e);
		}
		
		logger.debug("Entry : private String generateKeyString(InvoiceDetailsVO invoiceVO,String societyID)");
		return keyString;
	}
	
	/* Get invoice type list */
	public List getInvoiceTypeList(String type){
		List invoiceList=null;
		logger.debug("Entry : public List getInvoiceTypeList(String type)");
		
		invoiceList=invoiceDAO.getInvoiceTypeList(type);
		
		logger.debug("Exit : public List getInvoiceTypeList(String type)");
		return invoiceList;		
	}
	
	public InvoiceDetailsVO getInvoiceBalance(InvoiceDetailsVO invoiceVO,int societyID,int typeID){
		BigDecimal balance=BigDecimal.ZERO;
		InvoiceDetailsVO invVO=new InvoiceDetailsVO();
		logger.debug("Entry : public getInvoiceBalance(InvoiceDetailsVO invoiceVO)"+invoiceVO.getInvoiceDate()+" "+typeID+" "+societyID);
	
		try {
		//	invVO=invoiceDAO.getInvoiceBalance(invoiceVO,societyID,invoiceVO.getInvoiceTypeID());
		} catch (Exception e) {
			logger.error("Exception occurred in getInvoiceBalance "+e);
		}
		
		
		
		logger.debug("Exit : public getInvoiceBalance(InvoiceDetailsVO invoiceVO)");
		return invVO;		
	}
	
	public InvoiceDetailsVO getInvoiceBalanceForLateFee(InvoiceDetailsVO invoiceVO,int societyID,int typeID,int lateFeeFor){
		BigDecimal balance=BigDecimal.ZERO;
		InvoiceDetailsVO invVO=new InvoiceDetailsVO();
		logger.debug("Entry : public getInvoiceBalanceForLateFees(InvoiceDetailsVO invoiceVO)"+invoiceVO.getInvoiceDate()+" "+typeID+" "+societyID);
	
		try {
		//	invVO=invoiceDAO.getInvoiceBalanceForLateFees(invoiceVO,societyID,invoiceVO.getInvoiceTypeID(),lateFeeFor);
		} catch (Exception e) {
			logger.error("Exception occurred in getInvoiceBalanceForLateFees "+e);
		}
		
		
		
		logger.debug("Exit : public getInvoiceBalanceForLateFees(InvoiceDetailsVO invoiceVO)");
		return invVO;		
	}
	
	public InvoiceDetailsVO getUnpaidInvoiceBalance(InvoiceDetailsVO invoiceVO,String societyID,int typeID){
		BigDecimal balance=BigDecimal.ZERO;
		InvoiceDetailsVO invVO=new InvoiceDetailsVO();
		logger.debug("Entry : public getUnpaidInvoiceBalance(InvoiceDetailsVO invoiceVO)"+invoiceVO.getInvoiceDate()+" "+typeID+" "+societyID);
	
		try {
			//invVO=invoiceDAO.getUnpaidInvoiceBalance(invoiceVO,societyID,invoiceVO.getInvoiceTypeID());
		} catch (Exception e) {
			logger.error("Exception occurred in getUnpaidInvoiceBalance "+e);
		}
		
		
		
		logger.debug("Exit : public getUnpaidInvoiceBalance(InvoiceDetailsVO invoiceVO)");
		return invVO;		
	}
	
	
	/* Get invoice list for WebService */
	public JSONResponseVO getInvoiceForMember(int societyID,int aptID,String fromDate,String uptoDate){
		JSONResponseVO jsonVO=new JSONResponseVO();
		List invoiceList=new ArrayList();
		logger.debug("Entry : JsonRespAccountsVO getInvoiceForMember(String societyID,String aptID,String fromDate,String uptoDate)");
		
		try {
			invoiceList=getInvoicesForMember(societyID, aptID, fromDate, uptoDate);
			
			jsonVO.setAptID(aptID);
			jsonVO.setSocietyID(societyID);
			jsonVO.setFromDate(fromDate);
			jsonVO.setUptoDate(uptoDate);
			if(invoiceList.size()==0){
				jsonVO.setStatusCode(0);
			}else{
				jsonVO.setStatusCode(1);
				jsonVO.setObjectList(invoiceList);
			}
		} catch (Exception e) {
			jsonVO.setStatusCode(0);
			logger.error("Exceptio occurred in JsonRespAccountsVO getInvoiceForMember(String societyID,String aptID,String fromDate,String uptoDate) "+e);
		}
		
		
		
		
		logger.debug("Exit : JsonRespAccountsVO getInvoiceForMember(String societyID,String aptID,String fromDate,String uptoDate)");
		return jsonVO;		
	}
	
	/*public int sendInvoiceMail(InvoiceDetailsVO invoiceVO,int societyID){
		int success=0;
		logger.debug("Entry : public int sendInvoiceMail(InvoiceDetailsVO invoiceVO,MemberVO memberVO,int societyID)");
		MemberVO memberVO=memberServiceBean.getMemberInfo(""+invoiceVO.getAptID());
		SocietyVO societyVO=societyService.getSocietyDetails(""+invoiceVO.getAptID(),""+societyID);
		BankInfoVO bankVO=societyService.getBankDetails(""+societyVO.getSocietyID());
		success=emailDomain.sendInvoiceToMember(invoiceVO, memberVO, societyVO,bankVO);
		if(success==1){
			success=invoiceDAO.updateStatusOfInvoices(""+societyID, invoiceVO.getInvoiceID());
			
		}
		
		logger.debug("Exit : public int sendInvoiceMail(InvoiceDetailsVO invoiceVO,MemberVO memberVO,int societyID)");
		return success;
	}
	
	
	/* Send Selected invoice as mail 
	public int sendSelectedInvoiceMail( List invoiceList,String societyID){
		int success=0;
		logger.debug("Entry : public int sendSelectedInvoiceMail( List invoiceList,String societyID)");
		try{
		
			for(int i=0;invoiceList.size()>i;i++){
				InvoiceDetailsVO invoiceVO=(InvoiceDetailsVO) invoiceList.get(i);
				if(invoiceVO.getTotalAmount().signum()>0){
					success=sendInvoiceMail(invoiceVO, Integer.parseInt(societyID));
				}
				
			}
			
		}catch(Exception e){
			logger.error("Exception while sending selected invoices "+e);
		}
		
		
		
		logger.debug("Exit : public int sendSelectedInvoiceMail( List invoiceList,String societyID)");
		return success;		
	}
	*/
	
	/* Get invoice balance and ledger balance list */
	public List getInvoiceAndLedgerBalanceList(int societyID,String date){
		List invoiceList=new ArrayList();
		List memberList=new ArrayList();
		logger.debug("Entry : public List getInvoiceAndLedgerBalanceList(societyID,date)");
		try{
		
		memberList=getActiveMemberListAll(societyID);
		
		for(int i=0;memberList.size()>i;i++){
			InvoiceDetailsVO invoiceVO=new InvoiceDetailsVO();
			BigDecimal balDiff=BigDecimal.ZERO;
			AccountHeadVO achVO=new AccountHeadVO();
			InvoiceMemberChargesVO memberVO=(InvoiceMemberChargesVO) memberList.get(i);
			memberVO=calculateInvLdgrBalDifference(societyID, date, memberVO);
			
			
		}
		
		//invoiceList=invoiceDomain.getInvoiceAndLedgerBalanceList(societyID,date);
		
		}catch (Exception e) {
			logger.error("Exception in getInvoiceAndLedgerBalanceList : "+e);
		}
		logger.debug("Exit : public List getInvoiceAndLedgerBalanceList(societyID,date)");
		return memberList;		
	}
	
	
	public InvoiceMemberChargesVO calculateInvLdgrBalDifference(int societyID,String date,InvoiceMemberChargesVO memberVO){
		logger.debug("Entry : public InvoiceMemberChargesVO calculateInvLdgrBalDifference(String societyID,String Date,InvoiceMemberChargesVO memberVO)");
		
		try{
			List invoiceTypeList=new ArrayList();
			InvoiceDetailsVO invoiceVO=new InvoiceDetailsVO();
			BigDecimal balDiff=BigDecimal.ZERO;
			BigDecimal balance=BigDecimal.ZERO;
			AccountHeadVO achVO=new AccountHeadVO();
			invoiceTypeList=getInvoiceTypeList("S");
			
			for(int i=0;invoiceTypeList.size()>i;i++){
				InvoiceDetailsVO invoiceTypeVO=(InvoiceDetailsVO) invoiceTypeList.get(i);
				invoiceVO.setInvoiceTypeID(invoiceTypeVO.getInvoiceTypeID());
				invoiceVO.setAptID(memberVO.getAptID());
				invoiceVO.setInvoiceDate(date);
				invoiceVO=getInvoiceBalance(invoiceVO, societyID, invoiceTypeVO.getInvoiceTypeID());
				
				balance=balance.add(invoiceVO.getBalance());
				
			}
			
			achVO=ledgerService.getOpeningClosingBalance(societyID, memberVO.getLedgerID(), date, date, "L");
			logger.debug(balance+"--------------->"+achVO.getClosingBalance());
			
			
			balDiff=balance.subtract(achVO.getClosingBalance());
			memberVO.setBalDifference(balDiff);
			memberVO.setInvBalance(balance);
			memberVO.setLedgBalance(achVO.getClosingBalance());
			
		}catch (Exception e) {
			logger.error("Exception in calculateInvLdgrBalDifference "+e);
		}
		
		logger.debug("Exit : public InvoiceMemberChargesVO calculateInvLdgrBalDifference(String societyID,String Date,InvoiceMemberChargesVO memberVO)");
		return memberVO;
	}
	
	public List getUnpaidInvoicesDetails(int societyID ,String buildingID, String groupType){
		List invoiceList=null;
		logger.debug("Entry : public List getUnpaidInvoicesDetails(String societyID ,String buildingID)");
		
		//invoiceList=invoiceDAO.getUnpaidInvoicesDetails(societyID, buildingID,groupType);
		
		logger.debug("Exit : public List getUnpaidInvoicesDetails(String societyID ,String buildingID)"+invoiceList.size());
		return invoiceList;		
	}
	
	private InvoiceDetailsVO convertMemberChargeToInvoiceLineItems(InvoiceDetailsVO invVO){
		List inLiList=new ArrayList();
		List memberChargeList=invVO.getListOfLineItems();
		
		if(memberChargeList.size()!=0){
			for(int i=0;memberChargeList.size()>i;i++){
				InvoiceMemberChargesVO chargeVO=(InvoiceMemberChargesVO) memberChargeList.get(i);
				InvoiceLineItemsVO tempInLiVO=new InvoiceLineItemsVO();
				tempInLiVO.setInvoiceID(chargeVO.getInvoiceID());
				tempInLiVO.setCategoryID(chargeVO.getLedgerID());
				tempInLiVO.setTotalAmount(chargeVO.getTotalAmount());
				tempInLiVO.setPaymentPriority(chargeVO.getPaymentPriority());
				tempInLiVO.setBalance(chargeVO.getBalance());
				tempInLiVO.setCarriedBalance(chargeVO.getCarriedBalance());
				inLiList.add(tempInLiVO);
			}
		
		if(inLiList.size()==memberChargeList.size())
			invVO.setListOfLineItems(inLiList);
		}
		
		
		
		return invVO;
	}
	
	public InvoiceMemberChargesVO getInvoiceInfoDetails(int societyID ){
		
		InvoiceMemberChargesVO memberChargeVO=new InvoiceMemberChargesVO();
		InvoiceMemberChargesVO memberChargVo=new InvoiceMemberChargesVO();
		logger.debug("Entry : public List getInvoiceInfoDetails(String societyID )");
		
		try {
			memberChargeVO=invoiceDAO.getInvoiceInfoDetails(societyID);
			//memberChargVo=invoiceDAO.getLateFeeChargeDetails(societyID);
			memberChargeVO.setDueDate(dateUtil.getConvetedDate(memberChargeVO.getDueDate()));
			memberChargeVO.setDescription(memberChargVo.getFrequency());
			memberChargeVO.setNextRunDate(dateUtil.getConvetedDate(memberChargVo.getNextRunDate()));
			memberChargeVO.setTotalAmount(memberChargVo.getTotalAmount());
			memberChargeVO.setCalculationMethod(memberChargVo.getCalculationMethod());
			//memberChargeVO.setObjectList(invoiceDAO.getChargList(0, societyID));
			
			
		} catch (Exception e) {
			logger.error("Exception in getInvoiceInfo Details for society ID "+societyID);
		}
		
		
		logger.debug("Exit : public List getInvoiceInfoDetails(String societyID ,String buildingID)");
		return memberChargeVO;		
	}
	
	
	public OnlinePaymentGatewayVO getOnlinePaymentDetailsForMember(int societyID,int aptID,int ledgerID){
		OnlinePaymentGatewayVO onlinePaymentVO=new OnlinePaymentGatewayVO();
		logger.debug("Entry : public OnlinePaymentGatewayVO getOnlinePaymentDetailsForMember(String societyID)");
		OnlinePaymentGatewayVO parentVO =new OnlinePaymentGatewayVO();
		OnlinePaymentGatewayVO childVO =new OnlinePaymentGatewayVO();
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(Integer.toString(aptID), societyID);
			MemberVO memberVO=memberServiceBean.getMemberInfo(aptID);
			if((memberVO.getEmail()==null) || (memberVO.getEmail().length()==0) ){
				memberVO.setEmail("payu@esanchalak.com");
			}
			
			if((memberVO.getMobile().length()==0 )|| (memberVO.getMobile().length()>10) ){
				memberVO.setMobile("7774098844");
			}
			
			memberVO.setLedgerID(ledgerID);
			onlinePaymentVO.setSocietyVO(societyVO);
			onlinePaymentVO.setMemberVO(memberVO);
			
			onlinePaymentVO.setBankInfoVO(societyService.getBankDetails(societyID));
			
			parentVO =transactionService.getOnlinePaymentDetails(4, "P");
			childVO =transactionService.getOnlinePaymentDetails(societyID, "C");
			
		    onlinePaymentVO.setGatewayName(childVO.getGatewayName());
		    onlinePaymentVO.setGatewayID(childVO.getGatewayID());
		    onlinePaymentVO.setGatewayUrl(childVO.getGatewayUrl());
		    
		    onlinePaymentVO.setMerchantID(parentVO.getMerchantID());
		    onlinePaymentVO.setMerchantKey(parentVO.getMerchantKey());
		    onlinePaymentVO.setMerchantSaltKey(parentVO.getMerchantSaltKey());
			onlinePaymentVO.setChildMerchantID(childVO.getMerchantID());
			
			//Gateway charges
			onlinePaymentVO.setGtNetBanking(childVO.getGtNetBanking());
			onlinePaymentVO.setGtCreditCard(childVO.getGtCreditCard());
			onlinePaymentVO.setGtDcLess1000(childVO.getGtDcLess1000());;
			onlinePaymentVO.setGtDc1000to2000(childVO.getGtDc1000to2000());
			onlinePaymentVO.setGtDcGreater2000(childVO.getGtDcGreater2000());
			
			//Merchant charges
			onlinePaymentVO.setMtNetBanking(childVO.getMtNetBanking());
			onlinePaymentVO.setMtCreditCard(childVO.getMtCreditCard());
			onlinePaymentVO.setMtDcLess1000(childVO.getMtDcLess1000());
			onlinePaymentVO.setMtDc1000to2000(childVO.getMtDc1000to2000());
			onlinePaymentVO.setMtDcGreater2000(childVO.getMtDcGreater2000());
			onlinePaymentVO.setIsOnlnPymtAccptd(childVO.getIsOnlnPymtAccptd());
			onlinePaymentVO.setIsPayableAmountEditable(childVO.getIsPayableAmountEditable());
			onlinePaymentVO.setIsCalculationInclusive(childVO.getIsCalculationInclusive());
			
		} catch (Exception e) {
			logger.error("Exception in getOnlinePaymentDetailsForMember Details for society ID "+societyID);
		}
		
		logger.debug("Exit : public OnlinePaymentGatewayVO getOnlinePaymentDetailsForMember(String societyID )");
		return onlinePaymentVO;		
	}
	
	
		public InvoiceDetailsVO calculateProcessingFee(InvoiceDetailsVO invoiceVO){
		
		logger.debug("Entry : public InvoiceDetailsVO calculateProcessingFee(InvoiceDetailsVO invoiceVO)");
		BigDecimal hundred = new BigDecimal("100");
		BigDecimal thousand = new BigDecimal("1000");
		BigDecimal twoThousand = new BigDecimal("2000");
		BigDecimal totalAmount=BigDecimal.ZERO;
		BigDecimal proccFees=BigDecimal.ZERO;
		OnlinePaymentGatewayVO opgVO=new OnlinePaymentGatewayVO();		
		List processingFessList=new ArrayList();
		SocietyVO societyVO= invoiceVO.getOpGatewayVO().getSocietyVO();
		try {
			MathContext mc = new MathContext(5);
			opgVO=invoiceVO.getOpGatewayVO();			
			//invoiceVO.setBalance(new BigDecimal("4091.12"));
			
						
			//Net bank
			OnlinePaymentGatewayVO netbankVO= calculateConveienceFee("NB",invoiceVO.getBalance(),opgVO.getMtNetBanking(),opgVO.getGtNetBanking(),societyVO.getServiceTax());
			processingFessList.add(netbankVO);
			
			//Credit card
			OnlinePaymentGatewayVO cc1VO=calculateConveienceFee("CC",invoiceVO.getBalance(),opgVO.getMtCreditCard(),opgVO.getGtCreditCard(),societyVO.getServiceTax());
			processingFessList.add(cc1VO);
			
			
			//Debit Card			
			if(invoiceVO.getBalance().compareTo(thousand)<0 || invoiceVO.getBalance().compareTo(thousand)==0 ){
				logger.debug("1. DC charges Less than 1000 or equal ");
				OnlinePaymentGatewayVO dc1VO=calculateConveienceFee("DC",invoiceVO.getBalance(),opgVO.getMtDcLess1000(),opgVO.getGtDcLess1000(),societyVO.getServiceTax());
				processingFessList.add(dc1VO);
			}else if((invoiceVO.getBalance().compareTo(thousand)>0) && ( invoiceVO.getBalance().compareTo(twoThousand)<0 || invoiceVO.getBalance().compareTo(twoThousand)==0)){
				logger.debug("2. DC charges Between 1000 to 2000 ");
				OnlinePaymentGatewayVO dc2VO=calculateConveienceFee("DC",invoiceVO.getBalance(),opgVO.getMtDc1000to2000(),opgVO.getGtDc1000to2000(),societyVO.getServiceTax());				
				processingFessList.add(dc2VO);
			}else if(invoiceVO.getBalance().compareTo(twoThousand)>0){
				logger.debug("3. DC charges Above 2000 ");
				OnlinePaymentGatewayVO dc3VO=calculateConveienceFee("DC",invoiceVO.getBalance(),opgVO.getMtDcGreater2000(),opgVO.getGtDcGreater2000(),societyVO.getServiceTax());				
				processingFessList.add(dc3VO);
			}
			
			invoiceVO.setConveineceFeeList(processingFessList);
		 		
	 		
		} catch (Exception e) {
			logger.error("Exception in public InvoiceDetailsVO calculateProcessingFee(InvoiceDetailsVO invoiceVO) "+e);
		}
		
		logger.debug("Exit : public InvoiceDetailsVO calculateProcessingFee(InvoiceDetailsVO invoiceVO)");
		return invoiceVO;		
	}
		
		public OnlinePaymentGatewayVO calculateConveienceFee(String paymentType,BigDecimal dueAmount,BigDecimal merchantFee,BigDecimal gatewayFee, BigDecimal serviceTax){
			BigDecimal hundred = new BigDecimal("100");
		//	BigDecimal serviceTax=new BigDecimal("15");
			BigDecimal finalTotalAmount=BigDecimal.ZERO;
			BigDecimal totalAmount=BigDecimal.ZERO;
			BigDecimal merchantProccFees=BigDecimal.ZERO;
			BigDecimal gatewayProccFees=BigDecimal.ZERO;			
			BigDecimal merchantTotalAmount=BigDecimal.ZERO;
			OnlinePaymentGatewayVO opgVO =new OnlinePaymentGatewayVO();
			BigDecimal mtServiceTaxFee=BigDecimal.ZERO;
			BigDecimal gtServiceTaxFee=BigDecimal.ZERO;
			BigDecimal totalConveienceFee=BigDecimal.ZERO;	
			try{
				
				logger.info("PaymentType: "+paymentType+"  Due Amount: "+dueAmount+" Merchant Fee: "+merchantFee+" Gateway Fee: "+gatewayFee);
				MathContext mc = new MathContext(4);
				MathContext round = new MathContext(2);
				
				if(paymentType.equalsIgnoreCase("NB")){			
					BigDecimal serviceTaxInHundred=serviceTax.divide(hundred);	
					
					//Merchant								
					mtServiceTaxFee = merchantFee.multiply(serviceTaxInHundred,mc);
					merchantProccFees = merchantFee.add(mtServiceTaxFee);					
					
					logger.info("MerchantFee: "+merchantFee+" Service tax of merchant fee "+mtServiceTaxFee);
					//merchantProccFees=merchantProccFees.setScale(3, RoundingMode.FLOOR);
					logger.info("Calculated Merchant fee with service tax: "+merchantProccFees );
					merchantTotalAmount=dueAmount.add(merchantProccFees);
					
					logger.info(" Merchant Fees: "+merchantProccFees+" Total Amount with merchant Fee: "+merchantTotalAmount);
					
					//Gateway 
					gtServiceTaxFee = gatewayFee.multiply(serviceTaxInHundred,mc);					
					logger.info("Gayeway fee: "+gatewayFee+" Service tax of gateway: "+gtServiceTaxFee );
					gatewayProccFees=gatewayFee.add(gtServiceTaxFee);
					//gatewayProccFees=gatewayProccFees.setScale(3, RoundingMode.FLOOR);
					totalAmount=merchantTotalAmount.add(gatewayProccFees);	
				    
				    totalConveienceFee=gatewayProccFees.add(merchantProccFees);				   			    
					logger.info("Total Conveience Fee: "+totalConveienceFee+" Due Amount with fee: "+totalAmount);					
					totalConveienceFee=totalConveienceFee.setScale(0, RoundingMode.HALF_UP);
					finalTotalAmount= dueAmount.add(totalConveienceFee);
							
					opgVO.setGtProcessingFees(gatewayProccFees.setScale(0, RoundingMode.HALF_UP));
					opgVO.setMtProcessingFees(merchantProccFees.setScale(0, RoundingMode.HALF_UP));
					opgVO.setProcessingFees(totalConveienceFee.setScale(0, RoundingMode.HALF_UP));
					opgVO.setTotalAmount(finalTotalAmount);
					opgVO.setType(paymentType);
					opgVO.setPaymentMode("Net Banking");
					
					logger.info("Final Conveience Fee: "+opgVO.getProcessingFees()+ " Total Amount: "+opgVO.getTotalAmount());
					
				}else if(paymentType.equalsIgnoreCase("CC") || paymentType.equalsIgnoreCase("DC")){
					BigDecimal gatewayInHundred=gatewayFee.divide(hundred);		
					BigDecimal merchantInHundred=merchantFee.divide(hundred);
					BigDecimal serviceTaxInHundred=serviceTax.divide(hundred);					
					
					if(merchantFee.compareTo(BigDecimal.ZERO)==0){
						logger.info(" Merchant Fee is zero");
						merchantTotalAmount=dueAmount;
					}else{
						merchantProccFees = dueAmount.multiply(merchantInHundred,mc);
						mtServiceTaxFee = merchantProccFees.multiply(serviceTaxInHundred,mc);
						
						logger.info("Merchant Fees : "+merchantProccFees+" Service tax of Merchant Fees: "+mtServiceTaxFee);
						merchantProccFees=merchantProccFees.add(mtServiceTaxFee);
											
						//merchantProccFees=merchantProccFees.setScale(3,  RoundingMode.FLOOR);
						logger.info("Calculated MerchantProccFees: "+merchantProccFees );
						
						merchantTotalAmount=dueAmount.add(merchantProccFees);		
					}
						
					
					logger.info(" Merchant Fees: "+merchantProccFees+" Total Amount with merchant Fee: "+merchantTotalAmount);
					if(gatewayFee.compareTo(BigDecimal.ZERO)==0){	
						logger.info(" Gateway Fee is zero");
						opgVO.setGtProcessingFees(gatewayProccFees);
						opgVO.setMtProcessingFees(merchantProccFees);
						opgVO.setProcessingFees(merchantProccFees);
						opgVO.setTotalAmount(merchantTotalAmount);
						
					}else{
						
						gatewayProccFees = merchantTotalAmount.multiply(gatewayInHundred,mc);
						gtServiceTaxFee = gatewayProccFees.multiply(serviceTaxInHundred,mc);
						
						logger.info("Gayeway fee: "+gatewayProccFees+" Gateway Service tax "+gtServiceTaxFee);
						gatewayProccFees=gatewayProccFees.add(gtServiceTaxFee);
						
						logger.info("Gateway fee with tax "+gatewayProccFees );
						//gatewayProccFees=gatewayProccFees.setScale(3,  RoundingMode.FLOOR);
						
						totalAmount=merchantTotalAmount.add(gatewayProccFees);
						logger.info("DueAmount: "+dueAmount+" gatewayProccFees: "+gatewayProccFees+" totalAmount "+totalAmount);
						
						totalConveienceFee=gatewayProccFees.add(merchantProccFees);						
						logger.info("Total Conveience Fee: "+totalConveienceFee+ " Due Amount with fee: "+totalAmount);
						totalConveienceFee=totalConveienceFee.setScale(0, RoundingMode.HALF_UP);
						finalTotalAmount= dueAmount.add(totalConveienceFee);
						
						opgVO.setGtProcessingFees(gatewayProccFees.setScale(0, RoundingMode.HALF_UP));
						opgVO.setMtProcessingFees(merchantProccFees.setScale(0, RoundingMode.HALF_UP));
						opgVO.setProcessingFees(totalConveienceFee.setScale(0, RoundingMode.HALF_UP));
						opgVO.setTotalAmount(finalTotalAmount);
						
						logger.info("Final Conveience Fee: "+opgVO.getProcessingFees()+ " Total Amount: "+opgVO.getTotalAmount());
					}
					
					/*logger.info("Revsersed Calculation: "+totalAmount);
					gatewayProccFees = totalAmount.multiply(gatewayInHundred,mc);
					gtServiceTaxFee = gatewayProccFees.multiply(serviceTaxInHundred,mc);
					
					logger.info("Gateway Process Fee : "+gatewayProccFees+ " Service Tax: "+gtServiceTaxFee);
					gatewayProccFees=gatewayProccFees.add(gtServiceTaxFee);
					logger.info("Final Gateway Process Fee : "+gatewayProccFees.setScale(2, RoundingMode.FLOOR)); 
					
					BigDecimal deductFee = totalAmount.subtract(gatewayProccFees);
					logger.info("After deduct gateway fee amount : "+deductFee); 
					
					BigDecimal deductMerchantFee = deductFee.subtract(dueAmount);
					logger.info("Commission fee amount : "+deductMerchantFee.setScale(2, RoundingMode.FLOOR)); 
					
					BigDecimal actualDueAmount= deductFee.subtract(deductMerchantFee);
					logger.info("Actual due amount : "+actualDueAmount); */
					
					opgVO.setType(paymentType);
					if(paymentType.equalsIgnoreCase("CC") ){
						opgVO.setPaymentMode("Credit Card");
					}else if(paymentType.equalsIgnoreCase("DC")){
						opgVO.setPaymentMode("Debit Card");
					}
				
			
				}
				
			} catch (Exception e) {
				logger.error("Exception in calculateConveienceFee "+e);
			}
			return opgVO;
			
			
		}
	
		/* Get list of all bills of the given member for particular period  */
		public List getBillsForMember(int societyID,String fromDate, String toDate,int aptID){
			List billList=new ArrayList();
			logger.debug("Entry : public List getBillsForMember(int societyID,String fromDate,String toDate)"+fromDate+toDate);
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			try{
			
			List billingCycleList=invoiceDAO.getBillingCycleListForGivenPeriod(societyID, fromDate, toDate);
			
			
			
			for(int i=0;billingCycleList.size()>i;i++){
				InvoiceBillingCycleVO bCycleVO=(InvoiceBillingCycleVO) billingCycleList.get(i);
				
				if((format.parse(bCycleVO.getFromDate()).compareTo(format.parse(fromDate))>=0)&&(format.parse(bCycleVO.getUptoDate()).compareTo(format.parse(toDate))<=0)){
				
				
				BillDetailsVO billVO=invoiceDAO.getBillsForMember(societyID, bCycleVO.getFromDate(), bCycleVO.getUptoDate(), aptID);
				
				
				billVO.setBillNo(bCycleVO.getBillingCycleID()+" - "+societyID+" - "+billVO.getLedgerID());
				billVO.setBillingCycleID(bCycleVO.getBillingCycleID());
				billVO.setDueDate(bCycleVO.getDueDate());
				billList.add(billVO);
				
				}
				
			}
			
				
				
						
			//billList;//=invoiceDomain.getBillsForMember(societyID, fromDate, toDate);
			
			}catch(Exception e){
				logger.error("Exception in public List getBillsForMember(int societyID,String fromDate,String toDate) "+e);
			}
			logger.debug("Exit : public List getBillsForMember(int societyID,String fromDate,String toDate)"+billList.size());
			return billList;		
		}
		
		
		
		/* Get list of all bills of the given society for that particular billing cycle  */
		public List getBillsForSociety(final int societyID,final String fromDate,final String toDate,final int billingCycleID){
			List billList=null;
			logger.debug("Entry : public List getBillsForSociety(int societyID,String fromDate,String toDate)");
		
			try{
						
			billList=invoiceDAO.getBillsForSociety(societyID, fromDate, toDate, billingCycleID);
			
			}catch(Exception e){
				logger.error("Exception in public List getBillsForSociety(int societyID,String fromDate,String toDate) "+e);
			}
			logger.debug("Exit : public List getBillsForSociety(int societyID,String fromDate,String toDate)"+billList.size());
			return billList;		
		}
		
		
		/* Get Bill Datails of selected bill cycle  */
		public BillDetailsVO getBillsDetailsSociety(int societyID,String fromDate, String toDate,int aptID){
			BillDetailsVO billVO=new BillDetailsVO();
			logger.debug("Entry : 	public List getBillsDetailsSociety(int societyID,String fromDate, String toDate,int aptID)");
			try{
			MemberVO memberVO=new MemberVO();
			List transactionList=new ArrayList();
			LedgerTrnsVO ledgerCredit=new LedgerTrnsVO();
            LedgerTrnsVO ledgerDebit=new LedgerTrnsVO();
            memberVO=memberServiceBean.getMemberInfo(aptID); 
            ProfileDetailsVO profileVO=ledgerService.getLedgerProfile(memberVO.getLedgerID(),societyID);
            BankInfoVO bankVO=societyService.getBankDetails(societyID);
        	if((profileVO.getVirtualAccNo()!=null)&&(profileVO.getVirtualAccNo().length()>0)){
				bankVO.setAccountNo(profileVO.getVirtualAccNo());
			}
			
			ledgerCredit=ledgerService.getBillTransactionList(societyID, memberVO.getLedgerID(),fromDate,toDate,"C");
           // ledgerDebit=ledgerService.getBillTransactionList(societyID, memberVO.getLedgerID(),fromDate, toDate,"D");
             
              billVO.setOpeningBalance(ledgerCredit.getOpeningBalance());
              billVO.setClosingBalance(ledgerCredit.getClosingBalance());
              billVO.setChargedAmount(ledgerCredit.getCrOpnBalance());
              billVO.setPaidAmount(ledgerCredit.getDrOpnBalance());
              
              if(ledgerCredit.getTransactionList().size()>0){
             	 transactionList=ledgerCredit.getTransactionList();
              }
			billVO.setBankVO(bankVO);
			billVO.setObjectList(transactionList);
			billVO.setFromDate(fromDate);
			billVO.setUptoDate(toDate);
			billVO.setSocietyName(memberVO.getSocietyName());
			billVO.setLedgerDescription(ledgerCredit.getDescription());
						
			
			
			}catch(Exception e){
				logger.error("Exception in 	public List getBillsDetailsSociety(int societyID,String fromDate, String toDate,int aptID) "+e);
			}
			logger.debug("Exit : 	public List getBillsDetailsSociety(int societyID,String fromDate, String toDate,int aptID)"+billVO.getObjectList().size());
			return billVO;		
		}
		
		
		/* Get Bill Datails of selected bill cycle  */
		public BillDetailsVO getInvoiceBasedBillDetails(int societyID,String fromDate, String toDate,int ledgerID){
			BillDetailsVO billVO=new BillDetailsVO();
			logger.debug("Entry : 	public List getInvoiceBasedBillDetails(int societyID,String fromDate, String toDate,int ledgerID)");
			try{
			MemberVO memberVO=new MemberVO();
			List lineItemsList=new ArrayList();
			BigDecimal iGstTotal=BigDecimal.ZERO;
			BigDecimal cGstTotal=BigDecimal.ZERO;
			BigDecimal sGstTotal=BigDecimal.ZERO;
			BigDecimal uGstTotal=BigDecimal.ZERO;
			BigDecimal cessTotal=BigDecimal.ZERO;
			BigDecimal roundOff=BigDecimal.ZERO;
			BigDecimal discountTotal=BigDecimal.ZERO;			
			LedgerTrnsVO ledgerCredit=new LedgerTrnsVO();
            LedgerTrnsVO ledgerDebit=new LedgerTrnsVO();
            InvoiceVO inv=new InvoiceVO();
            ProfileDetailsVO profileVO=ledgerService.getLedgerProfile(ledgerID,societyID);
            BankInfoVO bankVO=societyService.getBankDetails(societyID);
        	if((profileVO.getVirtualAccNo()!=null)&&(profileVO.getVirtualAccNo().length()>0)){
				bankVO.setAccountNo(profileVO.getVirtualAccNo());
			}
	
			  ledgerCredit=ledgerService.getBillTransactionList(societyID, ledgerID,fromDate,toDate,"C");
           // ledgerDebit=ledgerService.getBillTransactionList(societyID, memberVO.getLedgerID(),fromDate, toDate,"D");
              lineItemsList=invoiceDAO.getGSTInvoicesListForBills(societyID, fromDate, toDate, ledgerID);
             
              
              
              if(lineItemsList.size()>0){
            	  int invoiceID=0;
            	  
            	  inv.setOrgID(societyID);
            	  InLineItemsVO lineItemVO=(InLineItemsVO) lineItemsList.get(lineItemsList.size()-1);
            	  inv.setInvoiceID(lineItemVO.getInvoiceID());
            	  inv=getInvoiceDetails(inv);
            	
            	  
            	  for(int i=0;lineItemsList.size()>i;i++){
            		  	InLineItemsVO lineItems=(InLineItemsVO) lineItemsList.get(i);
            		  	
            		  	 if(lineItems.getInvoiceType().equalsIgnoreCase("Credit Note")||(lineItems.getInvoiceType().equalsIgnoreCase("Debit Note"))){
            	           	  lineItems.setAmount(lineItems.getAmount().negate());
            	           	  lineItems.setTotalPrice(lineItems.getTotalPrice().negate());
            	           	  lineItems.setiGST(lineItems.getiGST().negate());
            	           	  lineItems.setcGST(lineItems.getcGST().negate());
            	           	  lineItems.setsGST(lineItems.getsGST().negate());
            	           	  lineItems.setuGST(lineItems.getuGST().negate());
            	           	  lineItems.setDiscount(lineItems.getDiscount().negate());
            	           	  lineItems.setGstAmount(lineItems.getGstAmount().negate());
            	              lineItems.setCess(lineItems.getCess().negate());
            	           	  
            	           	  iGstTotal=iGstTotal.add(lineItems.getiGST());
            	           	  cGstTotal=cGstTotal.add(lineItems.getcGST());
            	           	  sGstTotal=sGstTotal.add(lineItems.getsGST());
            	           	  uGstTotal=uGstTotal.add(lineItems.getuGST());
            	           	  cessTotal=cessTotal.add(lineItems.getCess());
            	           	  discountTotal=discountTotal.add(lineItems.getDiscount());
            	              
            	         }else{
            	        	 iGstTotal=iGstTotal.add(lineItems.getiGST());
           	           	     cGstTotal=cGstTotal.add(lineItems.getcGST());
           	           	     sGstTotal=sGstTotal.add(lineItems.getsGST());
           	           	     uGstTotal=uGstTotal.add(lineItems.getuGST());
           	           	     cessTotal=cessTotal.add(lineItems.getCess());
           	           	     discountTotal=discountTotal.add(lineItems.getDiscount());
           	                            	        	 
            	         }
            		  	 
            		  	if(i==0){
            		  	roundOff=roundOff.add(lineItems.getRoundOff());
            		  	}else if(invoiceID!=lineItems.getInvoiceID()){
            		  	  	roundOff=roundOff.add(lineItems.getRoundOff());
            		  	}
            		  	invoiceID=lineItems.getInvoiceID();
            		  	
            	  }
            	  inv.setTotalIGST(iGstTotal);
            	  inv.setTotalCGST(cGstTotal);
            	  inv.setTotalSGST(sGstTotal);
            	  inv.setTotalUGST(uGstTotal);
            	  inv.setTotalCess(cessTotal);
            	  inv.setTotalDiscount(discountTotal);
            	  inv.setTotalGST(inv.getTotalCGST().add(inv.getTotalSGST()).add(inv.getTotalIGST()).add(inv.getTotalUGST()));
            	  List gstSummaryList=invoiceDAO.getGSTSummaryofItemsForBills(ledgerID, fromDate, toDate, societyID);
            	  inv.setGstSummary(gstSummaryList);
            	  inv.setLineItemsList(lineItemsList);
            	  billVO.setInvoiceVO(inv);
              }
              
              billVO.setOpeningBalance(ledgerCredit.getOpeningBalance());
              billVO.setClosingBalance(ledgerCredit.getClosingBalance());
              billVO.setChargedAmount(ledgerCredit.getCrOpnBalance());
              billVO.setPaidAmount(ledgerCredit.getDrOpnBalance());
              billVO.setRoundOff(roundOff);
              billVO.setLedgerName(ledgerCredit.getLedgerName());
              billVO.setLedgerID(ledgerID);
              billVO.setLedgerDescription(ledgerCredit.getDescription());
      		  billVO.setProfileType(profileVO.getProfileType());
			  billVO.setEmailID(profileVO.getProfileEmailID());
			
              
              if(billVO.getClosingBalance().compareTo(BigDecimal.ZERO)<=0){
                  billVO.setClosingBalanceInwords(amtInWrds.convertToAmt(billVO.getClosingBalance().negate().toString()));
                  billVO.setClosingBalanceInwords(" - "+billVO.getClosingBalanceInwords());
              }else
                 billVO.setClosingBalanceInwords(amtInWrds.convertToAmt(billVO.getClosingBalance().toString()));
              
			billVO.setObjectList(lineItemsList);
			billVO.setFromDate(fromDate);
			billVO.setUptoDate(toDate);
			billVO.setSocietyName(memberVO.getSocietyName());
			billVO.setSocietyVO(societyService.getSocietyDetails(societyID));
			billVO.setBankVO(bankVO);
			List billingCycleList=invoiceDAO.getBillingCycleListForGivenPeriod(societyID, fromDate, toDate);
			InvoiceBillingCycleVO billingCycleVO=(InvoiceBillingCycleVO) billingCycleList.get(billingCycleList.size()-1);
			billVO.setDueDate(billingCycleVO.getDueDate());
			billVO.setBillNo(billingCycleVO.getBillingCycleID()+" - "+societyID+" - "+billVO.getLedgerID());
			billVO.setBillingCycleID(billingCycleVO.getBillingCycleID());
	
			
			}catch(Exception e){
				logger.error("Exception in 	public List getBillsDetailsSociety(int societyID,String fromDate, String toDate,int aptID) "+e);
			}
			logger.debug("Exit : 	public List getBillsDetailsSociety(int societyID,String fromDate, String toDate,int aptID)"+billVO.getObjectList().size());
			return billVO;		
		}
		
		/* Get Bill Datails of selected bill cycle fo a perticular apartment  */
		public BillDetailsVO getBillDetails(int societyID,String fromDate, String toDate,int aptID){
			BillDetailsVO billVO=new BillDetailsVO();
			logger.debug("Entry : 	public BillDetailsVO getBillDetails(int societyID,String fromDate, String toDate,int aptID)");
			try{
			MemberVO memberVO=new MemberVO();
			List lineItemsList=new ArrayList();
			BigDecimal iGstTotal=BigDecimal.ZERO;
			BigDecimal cGstTotal=BigDecimal.ZERO;
			BigDecimal sGstTotal=BigDecimal.ZERO;
			BigDecimal uGstTotal=BigDecimal.ZERO;
			BigDecimal cessTotal=BigDecimal.ZERO;
			BigDecimal roundOff=BigDecimal.ZERO;
			BigDecimal discountTotal=BigDecimal.ZERO;
			int ledgerID;
			LedgerTrnsVO ledgerCredit=new LedgerTrnsVO();
            LedgerTrnsVO ledgerDebit=new LedgerTrnsVO();
            InvoiceVO inv=new InvoiceVO();
            memberVO=memberServiceBean.getMemberInfo(aptID);
            ledgerID=memberVO.getLedgerID();
            ProfileDetailsVO profileVO=ledgerService.getLedgerProfile(ledgerID,societyID);
            BankInfoVO bankVO=societyService.getBankDetails(societyID);
        	if((profileVO.getVirtualAccNo()!=null)&&(profileVO.getVirtualAccNo().length()>0)){
				bankVO.setAccountNo(profileVO.getVirtualAccNo());
			}
            
			 ledgerCredit=ledgerService.getBillTransactionList(societyID, ledgerID,fromDate,toDate,"C");
           // ledgerDebit=ledgerService.getBillTransactionList(societyID, memberVO.getLedgerID(),fromDate, toDate,"D");
              lineItemsList=invoiceDAO.getGSTInvoicesListForBills(societyID, fromDate, toDate, ledgerID);
             
              
              
              if(lineItemsList.size()>0){
            	  int invoiceID=0;
            	  
            	  inv.setOrgID(societyID);
            	  InLineItemsVO lineItemVO=(InLineItemsVO) lineItemsList.get(lineItemsList.size()-1);
            	  inv.setInvoiceID(lineItemVO.getInvoiceID());
            	  inv=getInvoiceDetails(inv);
            	
            	  
            	  for(int i=0;lineItemsList.size()>i;i++){
            		  	InLineItemsVO lineItems=(InLineItemsVO) lineItemsList.get(i);
            		  	
            		 	if(lineItems.getInvoiceType().equalsIgnoreCase("Credit Note")||(lineItems.getInvoiceType().equalsIgnoreCase("Debit Note"))){
            		  	  lineItems.setAmount(lineItems.getAmount().negate());
          	           	  lineItems.setTotalPrice(lineItems.getTotalPrice().negate());
          	           	  lineItems.setiGST(lineItems.getiGST().negate());
          	           	  lineItems.setcGST(lineItems.getcGST().negate());
          	           	  lineItems.setsGST(lineItems.getsGST().negate());
          	           	  lineItems.setuGST(lineItems.getuGST().negate());
          	           	  lineItems.setDiscount(lineItems.getDiscount().negate());
          	           	  lineItems.setCess(lineItems.getCess().negate());
          	           	  
          	           	  iGstTotal=iGstTotal.add(lineItems.getiGST());
          	           	  cGstTotal=cGstTotal.add(lineItems.getcGST());
          	           	  sGstTotal=sGstTotal.add(lineItems.getsGST());
          	           	  uGstTotal=uGstTotal.add(lineItems.getuGST());
          	           	  cessTotal=cessTotal.add(lineItems.getCess());
          	           	  discountTotal=discountTotal.add(lineItems.getDiscount());
          	           
            	        }else{
            	        	 
            	          iGstTotal=iGstTotal.add(lineItems.getiGST());
           	           	  cGstTotal=cGstTotal.add(lineItems.getcGST());
           	           	  sGstTotal=sGstTotal.add(lineItems.getsGST());
           	           	  uGstTotal=uGstTotal.add(lineItems.getuGST());
           	           	  cessTotal=cessTotal.add(lineItems.getCess());
           	           	  discountTotal=discountTotal.add(lineItems.getDiscount());
           	          
            	         }
            		  	 
            		  	if(i==0){
            		  	roundOff=roundOff.add(lineItems.getRoundOff());
            		  	}else if(invoiceID!=lineItems.getInvoiceID()){
            		  	  	roundOff=roundOff.add(lineItems.getRoundOff());
            		  	}
            		  	invoiceID=lineItems.getInvoiceID();
            	  }
            	  inv.setTotalIGST(iGstTotal);
            	  inv.setTotalCGST(cGstTotal);
            	  inv.setTotalSGST(sGstTotal);
            	  inv.setTotalUGST(uGstTotal);
            	  inv.setTotalCess(cessTotal);
            	  inv.setTotalDiscount(discountTotal);            	  
            	  inv.setTotalGST(inv.getTotalCGST().add(inv.getTotalSGST()).add(inv.getTotalIGST()).add(inv.getTotalUGST()));
            	  List gstSummaryList=invoiceDAO.getGSTSummaryofItemsForBills(ledgerID, fromDate, toDate, societyID);
            	  inv.setGstSummary(gstSummaryList);
            	  inv.setLineItemsList(lineItemsList);
            	  billVO.setInvoiceVO(inv);
              }
              
              billVO.setOpeningBalance(ledgerCredit.getOpeningBalance());
              billVO.setClosingBalance(ledgerCredit.getClosingBalance());
              billVO.setChargedAmount(ledgerCredit.getCrOpnBalance());
              billVO.setPaidAmount(ledgerCredit.getDrOpnBalance());
              billVO.setRoundOff(roundOff);
              billVO.setLedgerName(ledgerCredit.getLedgerName());
              billVO.setLedgerID(ledgerID);
              billVO.setLedgerDescription(ledgerCredit.getDescription());
              
              if(billVO.getClosingBalance().compareTo(BigDecimal.ZERO)<=0){
                  billVO.setClosingBalanceInwords(amtInWrds.convertToAmt(billVO.getClosingBalance().negate().toString()));
                  billVO.setClosingBalanceInwords(" - "+billVO.getClosingBalanceInwords());
              }else
                 billVO.setClosingBalanceInwords(amtInWrds.convertToAmt(billVO.getClosingBalance().toString()));
              
			billVO.setObjectList(lineItemsList);
			billVO.setFromDate(fromDate);
			billVO.setUptoDate(toDate);
			billVO.setSocietyName(memberVO.getSocietyName());
			billVO.setSocietyVO(societyService.getSocietyDetails(societyID));
			billVO.setBankVO(bankVO);
			List billingCycleList=invoiceDAO.getBillingCycleListForGivenPeriod(societyID, fromDate, toDate);
			InvoiceBillingCycleVO billingCycleVO=(InvoiceBillingCycleVO) billingCycleList.get(billingCycleList.size()-1);
			billVO.setDueDate(billingCycleVO.getDueDate());
			billVO.setBillNo(billingCycleVO.getBillingCycleID()+" - "+societyID+" - "+billVO.getLedgerID());
			billVO.setBillingCycleID(billingCycleVO.getBillingCycleID());
			billVO.setMemberVO(memberVO);	
			
			
			
			}catch(Exception e){
				logger.error("Exception in 	public List getBillsDetailsSociety(int societyID,String fromDate, String toDate,int aptID) "+e);
			}
			logger.debug("Exit : 	public List getBillsDetailsSociety(int societyID,String fromDate, String toDate,int aptID)"+billVO.getObjectList().size());
			return billVO;		
		}
		
		/* Get list of all bills invoice based of the given ledger for particular period  */
		public List getBillsForLedger(int orgID,String fromDate, String toDate,int ledgerID){
			List billList=new ArrayList();
			logger.debug("Entry : public List getBillsForMember(int societyID,String fromDate,String toDate)");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			try{
			
	        List billingCycleList=invoiceDAO.getBillingCycleListForGivenPeriod(orgID, fromDate, toDate);
			
			
			for(int i=0;billingCycleList.size()>i;i++){
				InvoiceBillingCycleVO bCycleVO=(InvoiceBillingCycleVO) billingCycleList.get(i);
				
				if((format.parse(bCycleVO.getFromDate()).compareTo(format.parse(fromDate))>=0)&&(format.parse(bCycleVO.getUptoDate()).compareTo(format.parse(toDate))<=0)){
				
				
				BillDetailsVO billVO=invoiceDAO.getBillsForLedger(orgID, bCycleVO.getFromDate(), bCycleVO.getUptoDate(), ledgerID);
				
				
				billVO.setBillNo(orgID+""+billVO.getLedgerID()+""+bCycleVO.getBillingCycleID());
				billVO.setBillingCycleID(bCycleVO.getBillingCycleID());
				billVO.setDueDate(bCycleVO.getDueDate());
				billList.add(billVO);
				
				}
				
			}
			
			}catch(Exception e){
				logger.error("Exception in public List getBillsForLedger(int orgID,String fromDate,String toDate) "+e);
			}
			logger.debug("Exit : public List getBillsForLedger(int orgID,String fromDate,String toDate)"+billList.size());
			return billList;		
		}

		/* Get list of all bills of the given org for that particular billing cycle  */
		public List getBillsForOrg(int orgID,String fromDate, String toDate, int billingCycleID){
			List billList=new ArrayList();
			logger.debug("Entry : public List getBillsForSociety(int orgID,String fromDate,String toDate)");
		
			try{
						
			billList=invoiceDAO.getBillsForOrg(orgID, fromDate, toDate, billingCycleID);
			
			}catch(Exception e){
				logger.error("Exception in public List getBillsForOrg(int orgID,String fromDate,String toDate) "+e);
			}
			logger.debug("Exit : public List getBillsForOrg(int orgID,String fromDate,String toDate)"+billList.size());
			return billList;		
		}
		
		/* Get list of all gst invoices list for a ledger in given period */
		public List getGSTInvoicesListForBills(int orgID,String fromDate,String toDate,int ledgerID){
			List invoiceList=null;
			logger.debug("Entry : public List getGSTInvoicesForOrg(int orgID,String fromDate,String toDate,int ledgerID)");
			try{
				
			invoiceList=invoiceDAO.getGSTInvoicesListForBills(orgID, fromDate, toDate, ledgerID);
			
			}catch(Exception e){
				  logger.error("Exception in getGSTInvoicesForOrg(int orgID,String fromDate,String toDate,int ledgerID) "+e);
			}					
			logger.debug("Exit : public List getGSTInvoicesForOrg(int orgID,String fromDate,String toDate,int ledgerID)");
			return invoiceList;		
		}
		
		
		/* Get list of all invoices of the given society for that particular period  */
		public List getInvoicesForSociety(final InvoiceDetailsVO invoiceVO){
			List invoiceList=null;
		
			logger.debug("Entry : public List getInvoicesForSociety(final InvoiceDetailsVO invoiceVO)");
		
			try{
				
				invoiceList=invoiceDAO.getInvoicesForSociety(invoiceVO);
			
			}catch(Exception e){
				logger.error("Exception : public List getInvoicesForSociety(final InvoiceDetailsVO invoiceVO) "+e);
			}
			logger.debug("Exit : public List getInvoicesForSociety(final InvoiceDetailsVO invoiceVO)"+invoiceList.size());
			return invoiceList;		
		}
		
		
		/* Get list of all one time invoices of the given society   */
		public List getOnetimeInvoicesForSociety(final InvoiceDetailsVO invoiceVO){
			List invoiceList=null;
		
			logger.debug("Entry : public List getOnetimeInvoicesForSociety(final InvoiceDetailsVO invoiceVO)");
		
			try{
					
			invoiceList=invoiceDAO.getOnetimeInvoicesForSociety(invoiceVO);
			
			}catch(Exception e){
				logger.error("Exception : public List getOnetimeInvoicesForSociety(final InvoiceDetailsVO invoiceVO) "+e);
			}
			logger.debug("Exit : public List getOnetimeInvoicesForSociety(final InvoiceDetailsVO invoiceVO)"+invoiceList.size());
			return invoiceList;		
		}
		
		
		/* Get invoices bill for a  for that particular billing cycle  */
		public List getInvoicesForLedger(final InvoiceDetailsVO invoiceVO){
			List invoiceList=null;
			
			logger.debug("Entry : public List getInvoicesForLedger(final InvoiceDetailsVO invoiceVO)");
		
			try{
				
						
			invoiceList=invoiceDAO.getInvoicesForLedger(invoiceVO);
			
			}catch(Exception e){
				logger.error("Exception in public List getInvoicesForLedger(final InvoiceDetailsVO invoiceVO) "+e);
			}
			logger.debug("Exit : public List getBillsForSociety(int societyID,String fromDate,String toDate)"+invoiceList.size());
			return invoiceList;		
		}
			
	
		/* Insert the invoice  */
		public TransactionVO addInvoiceReceptTransaction(int societyID,TransactionVO txVO){
			int insertFlag=0;
			List memberChargeList=null;
			logger.debug("Entry : public int addInvoiceReceptTransaction(String societyID,TransactionVO txVO)");
			
			txVO=transactionService.addTransaction(txVO, societyID);
			
			logger.debug("Receipt no is "+txVO.getTransactionID());
			
			if(txVO.getTransactionID()!=0){
				if(txVO.getInvoiceDetailsVO().size()>0){
				for(int i=0;txVO.getInvoiceDetailsVO().size()>i;i++){
			InvoiceDetailsVO invoiceVO=txVO.getInvoiceDetailsVO().get(i);
			insertFlag=updateInvoiceBalance(societyID, invoiceVO.getBillID(),Integer.parseInt(invoiceVO.getInvoiceID()), txVO.getTransactionID(),invoiceVO.getPaidAmount());
				}
				}
			}
			logger.debug("Exit : public int addInvoiceReceptTransaction(String societyID,TransactionVO txVO)");
			return txVO;		
		}
		
		
		/* Update invoice balance with the transaction */
		public int updateInvoiceBalance(int societyID ,int billID,int invoiceID, int receiptID,BigDecimal amount){
			int success=0;
		
			BigDecimal txBal=BigDecimal.ZERO;
			logger.debug("Entry : public int updateInvoiceBalance(String societyID ,TransactionVO txVO)");
			try {				
						success=invoiceDAO.insertReceiptAgainstInvoice(societyID,billID,invoiceID,receiptID,amount);
				}
			 catch (Exception e) {
				logger.error("Exception : updateInvoiceBalance(String societyID,TransactionVO txVO) "+e);
			
					
			}
			logger.debug("Exit : public int updateInvoiceBalance(String societyID ,TransactionVO txVO)");
			return success;		
		}
		
		/* Insert the invoice  */
		public InvoiceVO insertGSTInvoice(InvoiceVO invoiceVO){
			int flag=0;
			int insertInvoiceFlag=0;
			int insertInvoiceLineItemsFlag=0;
			TransactionVO txVO=new TransactionVO();
			InvoiceVO validationVO=new InvoiceVO();
			String counterType="";
					
			try{
			
			logger.debug("Entry : public InvoiceVO insertInvoice(InvoiceVO invoiceVO)");		
			invoiceVO.setLedgerID(invoiceVO.getEntityID());
			validationVO=validateInvoice(invoiceVO, invoiceVO.getOrgID());
						
			if(validationVO.getStatusCode()==200){
				
				if(((invoiceVO.getInvoiceType()!=null)&&(invoiceVO.getInvoiceType().length()>0)&&(invoiceVO.getIsReview()==0))){
				     counterType=transactionService.getCounterType(invoiceVO.getInvoiceType());
				     int autoIncNo=transactionService.getLatestCounterNo(invoiceVO.getOrgID(), counterType);
				     invoiceVO.setInvoiceNumber(""+autoIncNo);
				     
				     if(((invoiceVO.getDocID()==null)||(invoiceVO.getDocID().length()==0))&&((invoiceVO.getInvoiceType().equalsIgnoreCase("Journal"))||(invoiceVO.getInvoiceType().equalsIgnoreCase("Sales"))||(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note")))){
				     //int gstDocRefID=transactionService.getLatestCounterNo(invoiceVO.getOrgID(), "gst_doc_id");
				     invoiceVO.setDocID(""+autoIncNo);
				     }
				}else{
					invoiceVO.setInvoiceNumber("");
				}
				if(invoiceVO.getCreatedBy()==0)
					invoiceVO.setCreatedBy(15);
				if(invoiceVO.getUpdatedBy()==0)
					invoiceVO.setUpdatedBy(15);
				
				//currency conversion
				invoiceVO=currencyConversion(invoiceVO,"M");
				
				insertInvoiceFlag=invoiceDAO.insertGSTInvoice(invoiceVO);
				
				invoiceVO.setInvoiceID(insertInvoiceFlag);
				
			
				
				if(insertInvoiceFlag>0){
							
					   insertInvoiceLineItemsFlag=invoiceDAO.insertInvoiceLineItems(invoiceVO);			
					
						if((insertInvoiceLineItemsFlag>0)&&(invoiceVO.getIsReview()==0)&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Sales")||(invoiceVO.getInvoiceType().equalsIgnoreCase("Purchase"))||(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note"))||(invoiceVO.getInvoiceType().equalsIgnoreCase("Debit Note"))||(invoiceVO.getInvoiceType().equalsIgnoreCase("Journal")))){
							txVO=convertInvoiceVOTOTx(invoiceVO,invoiceVO.getOrgID());
							logger.info("--->"+invoiceVO.getTotalAmount());
							txVO=transactionService.addTransaction(txVO, invoiceVO.getOrgID());
						}else{
							
							txVO.setStatusCode(200);
						}
										
						
						logger.debug("insertInvoiceLineItemsFlag "+insertInvoiceLineItemsFlag);
						invoiceVO.setInvoiceID(insertInvoiceFlag);
					
						if((txVO.getStatusCode()==200)&&(insertInvoiceLineItemsFlag>0)){							 
							 
							 invoiceVO.setStatusCode(200);
							 invoiceVO.setStatusMessage("Invoice has been inserted succefully");
							 logger.info("Invoice inserted succefully invoiceID: "+invoiceVO.getInvoiceID());	
							 
							 if((invoiceVO.getInvoiceType()!=null)&&(invoiceVO.getIsReview()==0)){
								  int counterNo=transactionService.incrementCounterNumber(invoiceVO.getOrgID(), counterType);
								 
								  if(((invoiceVO.getDocID()!=null)||(invoiceVO.getDocID().length()>0))&&((invoiceVO.getInvoiceType().equalsIgnoreCase("Journal"))||(invoiceVO.getInvoiceType().equalsIgnoreCase("Sales"))||(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note")))){

								 // int gstDocRefID=transactionService.incrementCounterNumber(invoiceVO.getOrgID(), "gst_doc_id");
								}
							 }
							 
						 }else{
							 int success=invoiceDAO.permanentDeleteGSTInvoice(invoiceVO);
							 invoiceVO.setStatusCode(406);
							 invoiceVO.setStatusMessage("Unable to add invoice");
						 }
						
						
					
					}else{				
						invoiceVO.setInvoiceID(flag);
						invoiceVO.setStatusCode(406);
						invoiceVO.setStatusMessage("Unable to add invoice");	
					}
				
				
			}else{				
				invoiceVO.setStatusCode(validationVO.getStatusCode());
				invoiceVO.setStatusMessage(validationVO.getStatusMessage());		
			}		
			
		
			
			}catch (Exception e) {
				logger.error("Exception in insertGSTInvoice "+e);
			}
			logger.debug("Exit : public InvoiceVO insertGSTInvoice(InvoiceVO invoiceVO)");
			return invoiceVO;		
		}
		
		//Calculate currency
		public InvoiceVO currencyConversion(InvoiceVO invoiceVO,String operationType){
			
			List<InLineItemsVO> inLineItemsList=new ArrayList<InLineItemsVO>();
				
			if((operationType.equalsIgnoreCase("M")) && (invoiceVO.getCurrencyID()>1) && (invoiceVO.getCurrencyRate().compareTo(BigDecimal.ZERO)>0) ){
				//Currency Rate 
				BigDecimal total=BigDecimal.ZERO;
				invoiceVO.setInvoiceAmount(invoiceVO.getInvoiceAmount().multiply(invoiceVO.getCurrencyRate()).setScale(2,RoundingMode.HALF_UP));
				invoiceVO.setTotalDiscount(invoiceVO.getTotalDiscount().multiply(invoiceVO.getCurrencyRate()).setScale(2,RoundingMode.HALF_UP));
				invoiceVO.setTotalCGST(invoiceVO.getTotalCGST().multiply(invoiceVO.getCurrencyRate()).setScale(2,RoundingMode.HALF_UP));
				invoiceVO.setTotalSGST(invoiceVO.getTotalSGST().multiply(invoiceVO.getCurrencyRate()).setScale(2,RoundingMode.HALF_UP));
				invoiceVO.setTotalUGST(invoiceVO.getTotalUGST().multiply(invoiceVO.getCurrencyRate()).setScale(2,RoundingMode.HALF_UP));
				invoiceVO.setTotalIGST(invoiceVO.getTotalIGST().multiply(invoiceVO.getCurrencyRate()).setScale(2,RoundingMode.HALF_UP));
				
				invoiceVO.setTotalCess(invoiceVO.getTotalCess().multiply(invoiceVO.getCurrencyRate()).setScale(2,RoundingMode.HALF_UP));
				invoiceVO.setTds(invoiceVO.getTds().multiply(invoiceVO.getCurrencyRate()).setScale(2,RoundingMode.HALF_UP));
				invoiceVO.setTotalRoundUpAmount(invoiceVO.getTotalRoundUpAmount().multiply(invoiceVO.getCurrencyRate()).setScale(2,RoundingMode.HALF_UP));
				total=total.add(invoiceVO.getInvoiceAmount());
				total=total.add(invoiceVO.getTotalIGST());
				total=total.add(invoiceVO.getTotalCGST());
				total=total.add(invoiceVO.getTotalSGST());
				total=total.add(invoiceVO.getTotalUGST());
				total=total.add(invoiceVO.getTotalCess());
				
				total=total.add(invoiceVO.getTotalRoundUpAmount());
				total=total.subtract(invoiceVO.getTds());
				invoiceVO.setTotalAmount(total);
				
				
				//Line 
				for(int i=0; invoiceVO.getLineItemsList().size()>i;i++){
					InLineItemsVO inLineItemsVO =(InLineItemsVO) invoiceVO.getLineItemsList().get(i);
					inLineItemsVO.setPerUnitPrice(inLineItemsVO.getPerUnitPrice().multiply(invoiceVO.getCurrencyRate()).setScale(2,RoundingMode.HALF_UP));
					inLineItemsVO.setTotalPrice(inLineItemsVO.getTotalPrice().multiply(invoiceVO.getCurrencyRate()).setScale(2,RoundingMode.HALF_UP));
					inLineItemsVO.setDiscount(inLineItemsVO.getDiscount().multiply(invoiceVO.getCurrencyRate()).setScale(2,RoundingMode.HALF_UP));
					inLineItemsVO.setcGST(inLineItemsVO.getcGST().multiply(invoiceVO.getCurrencyRate()).setScale(2,RoundingMode.HALF_UP));
					inLineItemsVO.setsGST(inLineItemsVO.getsGST().multiply(invoiceVO.getCurrencyRate()).setScale(2,RoundingMode.HALF_UP));
					inLineItemsVO.setiGST(inLineItemsVO.getiGST().multiply(invoiceVO.getCurrencyRate()).setScale(2,RoundingMode.HALF_UP));
					inLineItemsVO.setuGST(inLineItemsVO.getuGST().multiply(invoiceVO.getCurrencyRate()).setScale(2,RoundingMode.HALF_UP));
					inLineItemsVO.setAmount(inLineItemsVO.getAmount().multiply(invoiceVO.getCurrencyRate()).setScale(2,RoundingMode.HALF_UP));
					
					inLineItemsList.add(inLineItemsVO);
				}
				
				invoiceVO.setLineItemsList(inLineItemsList);
			}
			
			else if((operationType.equalsIgnoreCase("D")) && (invoiceVO.getCurrencyID()>1) && (invoiceVO.getCurrencyRate().compareTo(BigDecimal.ZERO)>0) ){
				//Currency Rate 
				if((invoiceVO.getInvoiceAmount()!=null)&&(invoiceVO.getInvoiceAmount().compareTo(BigDecimal.ZERO)>0))
				invoiceVO.setInvoiceAmount(invoiceVO.getInvoiceAmount().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
				if((invoiceVO.getTotalDiscount()!=null)&&(invoiceVO.getTotalDiscount().compareTo(BigDecimal.ZERO)>0))
				invoiceVO.setTotalDiscount(invoiceVO.getTotalDiscount().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
				if((invoiceVO.getTotalCGST()!=null)&&(invoiceVO.getTotalCGST().compareTo(BigDecimal.ZERO)>0))
				invoiceVO.setTotalCGST(invoiceVO.getTotalCGST().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
				if((invoiceVO.getTotalSGST()!=null)&&(invoiceVO.getTotalSGST().compareTo(BigDecimal.ZERO)>0))
				invoiceVO.setTotalSGST(invoiceVO.getTotalSGST().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
				if((invoiceVO.getTotalUGST()!=null)&&(invoiceVO.getTotalUGST().compareTo(BigDecimal.ZERO)>0))
				invoiceVO.setTotalUGST(invoiceVO.getTotalUGST().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
				if((invoiceVO.getTotalIGST()!=null)&&(invoiceVO.getTotalIGST().compareTo(BigDecimal.ZERO)>0))
				invoiceVO.setTotalIGST(invoiceVO.getTotalIGST().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
				
				if((invoiceVO.getTotalCess()!=null)&&(invoiceVO.getTotalCess().compareTo(BigDecimal.ZERO)>0))
				invoiceVO.setTotalCess(invoiceVO.getTotalCess().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
				if((invoiceVO.getTds()!=null)&&(invoiceVO.getTds().compareTo(BigDecimal.ZERO)>0))
				invoiceVO.setTds(invoiceVO.getTds().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
				if((invoiceVO.getTotalRoundUpAmount()!=null)&&(invoiceVO.getTotalRoundUpAmount().compareTo(BigDecimal.ZERO)>0))
				invoiceVO.setTotalRoundUpAmount(invoiceVO.getTotalRoundUpAmount().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
				if((invoiceVO.getTotalAmount()!=null)&&(invoiceVO.getTotalAmount().compareTo(BigDecimal.ZERO)>0))
				invoiceVO.setTotalAmount(invoiceVO.getTotalAmount().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
				if((invoiceVO.getTotalGST()!=null)&&(invoiceVO.getTotalGST().compareTo(BigDecimal.ZERO)>0))
				invoiceVO.setTotalGST(invoiceVO.getTotalGST().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
				
				
				//Line 
				for(int i=0; invoiceVO.getLineItemsList().size()>i;i++){
					InLineItemsVO inLineItemsVO =(InLineItemsVO) invoiceVO.getLineItemsList().get(i);
					if((inLineItemsVO.getPerUnitPrice()!=null)&&(inLineItemsVO.getPerUnitPrice().compareTo(BigDecimal.ZERO)>0))
					inLineItemsVO.setPerUnitPrice(inLineItemsVO.getPerUnitPrice().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
					if((inLineItemsVO.getTotalPrice()!=null)&&(inLineItemsVO.getTotalPrice().compareTo(BigDecimal.ZERO)>0))
					inLineItemsVO.setTotalPrice(inLineItemsVO.getTotalPrice().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
					if((inLineItemsVO.getDiscount()!=null)&&(inLineItemsVO.getDiscount().compareTo(BigDecimal.ZERO)>0))
					inLineItemsVO.setDiscount(inLineItemsVO.getDiscount().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
					if((inLineItemsVO.getcGST()!=null)&&(inLineItemsVO.getcGST().compareTo(BigDecimal.ZERO)>0))
					inLineItemsVO.setcGST(inLineItemsVO.getcGST().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
					if((inLineItemsVO.getsGST()!=null)&&(inLineItemsVO.getsGST().compareTo(BigDecimal.ZERO)>0))
					inLineItemsVO.setsGST(inLineItemsVO.getsGST().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
					if((inLineItemsVO.getiGST()!=null)&&(inLineItemsVO.getiGST().compareTo(BigDecimal.ZERO)>0))
					inLineItemsVO.setiGST(inLineItemsVO.getiGST().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
					if((inLineItemsVO.getuGST()!=null)&&(inLineItemsVO.getuGST().compareTo(BigDecimal.ZERO)>0))
					inLineItemsVO.setuGST(inLineItemsVO.getuGST().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
					if((inLineItemsVO.getAmount()!=null)&&(inLineItemsVO.getAmount().compareTo(BigDecimal.ZERO)>0))
					inLineItemsVO.setAmount(inLineItemsVO.getAmount().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
					if((inLineItemsVO.getGstAmount()!=null)&&(inLineItemsVO.getGstAmount().compareTo(BigDecimal.ZERO)>0))
						inLineItemsVO.setGstAmount(inLineItemsVO.getGstAmount().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
					
					inLineItemsList.add(inLineItemsVO);
				}
				
				invoiceVO.setLineItemsList(inLineItemsList);
			}
			
			return invoiceVO;		
		}
		
		
		/* Get invoices for customer my accountant  */
		public List getInvoicesForCustomerLedger(InvoiceVO invoiceVO){
			List invoiceList=null;
			List invoiceDraftList=null;
			logger.debug("Entry : public List getInvoicesForCustomerLedger(InvoiceVO invoiceVO)");	
			try{			
			if(invoiceVO.getIsReview()==10)		{
				invoiceVO.setIsReview(0);
				invoiceList=invoiceDAO.getInvoicesForCustomerLedger(invoiceVO);
				//Draft
				invoiceVO.setIsReview(1);
				invoiceDraftList=invoiceDAO.getInvoicesForCustomerLedger(invoiceVO);
				invoiceList.addAll(invoiceDraftList);
			}else{				
				invoiceList=invoiceDAO.getInvoicesForCustomerLedger(invoiceVO);
			}
			
			
			}catch(Exception e){
				logger.error("Exception in getInvoicesForCustomerLedger "+e);
			}
			logger.debug("Exit : public List getInvoicesForCustomerLedger(InvoiceVO invoiceVO) "+invoiceList.size());
			return invoiceList;		
		}
		
		
		/* Get invoice details  */
		public InvoiceVO getInvoiceDetails(InvoiceVO invoiceVO){
			List invoiceLineItemList=null;
			List gstSummary=null;
			List invoiceReceiptList=null;
			List invoiceLinkList=null;
			SocietyVO orgVO= new SocietyVO();
			logger.debug("Entry : public int getInvoiceDetails(InvoiceVO invoiceVO)");
			try{			  
			  
			  invoiceVO=invoiceDAO.getInvoiceDetails(invoiceVO);
			  
			  if(invoiceVO.getInvoiceID()>0){
				  invoiceLineItemList=invoiceDAO.getInvoiceLineItemList(invoiceVO);
				  invoiceVO.setLineItemsList(invoiceLineItemList);	
				  
				//currency conversion
				  invoiceVO=currencyConversion(invoiceVO,"D");
				  
				  gstSummary=invoiceDAO.getGSTSummaryofItems(invoiceVO.getInvoiceID());
				
				 if((gstSummary.size()>0) && (invoiceVO.getCurrencyID()>1) && (invoiceVO.getCurrencyRate().compareTo(BigDecimal.ZERO)>0) ){
				    for(int i=0; gstSummary.size()>i;i++){
						InLineItemsVO inLineItemsVO =(InLineItemsVO) gstSummary.get(i);

						if((inLineItemsVO.getcGST()!=null)&&(inLineItemsVO.getcGST().compareTo(BigDecimal.ZERO)>0))
						inLineItemsVO.setcGST(inLineItemsVO.getcGST().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
						if((inLineItemsVO.getsGST()!=null)&&(inLineItemsVO.getsGST().compareTo(BigDecimal.ZERO)>0))
						inLineItemsVO.setsGST(inLineItemsVO.getsGST().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
						if((inLineItemsVO.getiGST()!=null)&&(inLineItemsVO.getiGST().compareTo(BigDecimal.ZERO)>0))
						inLineItemsVO.setiGST(inLineItemsVO.getiGST().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
						if((inLineItemsVO.getuGST()!=null)&&(inLineItemsVO.getuGST().compareTo(BigDecimal.ZERO)>0))
						inLineItemsVO.setuGST(inLineItemsVO.getuGST().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
						if((inLineItemsVO.getCess()!=null)&&(inLineItemsVO.getCess().compareTo(BigDecimal.ZERO)>0))
						inLineItemsVO.setCess(inLineItemsVO.getCess().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
						
						
					}
				  }
				  
				  invoiceVO.setGstSummary(gstSummary);	
				  
				  invoiceReceiptList=transactionService.getInvoiceRceiptList(invoiceVO.getOrgID(),invoiceVO.getInvoiceID());
				  invoiceVO.setReceiptList(invoiceReceiptList);
				  
				  orgVO=societyService.getSocietyDetails("0", invoiceVO.getOrgID()); 	
				  invoiceVO.setOrgVO(orgVO);
				  
				  invoiceLinkList=invoiceDAO.getLinkedInvoiceList(invoiceVO.getOrgID(),invoiceVO.getInvoiceID());
				  invoiceVO.setLinkInvList(invoiceLinkList);
				  
				  BankInfoVO bankVO=societyService.getBankDetails(invoiceVO.getOrgID());
				  invoiceVO.setBankVO(bankVO);
				  
				  invoiceVO.setTotalAmountInWords(amtInWrds.convertToAmtAllCurrency(invoiceVO.getTotalAmount().toString(),invoiceVO.getCurrencyVO().getCurrencyName(),invoiceVO.getCurrencyVO().getDecimalCurrency()));
				  
				  ProfileDetailsVO profileVO=ledgerService.getLedgerProfile(invoiceVO.getEntityID(), invoiceVO.getOrgID());
				  
				  if((profileVO.getPanNo()!=null)&&(profileVO.getPanNo().length()>0)){
				  invoiceVO.setCustomerPAN(profileVO.getPanNo());
				  }
			  }
			}catch(EmptyResultDataAccessException e){
				logger.info("No info found for InvoiceDetails  - Invoice ID "+invoiceVO.getInvoiceID()+" for Org ID "+invoiceVO.getOrgID());
			}
			catch(Exception e){
				logger.error("Exception in getInvoiceDetails "+e);
			}
			logger.debug("Exit : public int getInvoiceDetails(InvoiceVO invoiceVO)");
			return invoiceVO;		
		}
		
		/* Get invoice details by invoice number */
		public InvoiceVO getInvoiceDetailsByInvoiceNumber(InvoiceVO invoiceVO){
			List invoiceLineItemList=null;
			List gstSummary=null;
			List invoiceReceiptList=null;
			List invoiceLinkList=null;
			SocietyVO orgVO= new SocietyVO();
			logger.debug("Entry : public int getInvoiceDetailsByInvoiceNumber(InvoiceVO invoiceVO)");
			try{			  
			  
			  invoiceVO=invoiceDAO.getInvoiceDetailsByInvoiceNumber(invoiceVO);
			  
			  if(invoiceVO.getInvoiceID()>0){
				  invoiceLineItemList=invoiceDAO.getInvoiceLineItemList(invoiceVO);
				  invoiceVO.setLineItemsList(invoiceLineItemList);	
				  
				//currency conversion
				  invoiceVO=currencyConversion(invoiceVO,"D");
				  
				  gstSummary=invoiceDAO.getGSTSummaryofItems(invoiceVO.getInvoiceID());
				
				 if((gstSummary.size()>0) && (invoiceVO.getCurrencyID()>1) && (invoiceVO.getCurrencyRate().compareTo(BigDecimal.ZERO)>0) ){
				    for(int i=0; gstSummary.size()>i;i++){
						InLineItemsVO inLineItemsVO =(InLineItemsVO) gstSummary.get(i);

						if((inLineItemsVO.getcGST()!=null)&&(inLineItemsVO.getcGST().compareTo(BigDecimal.ZERO)>0))
						inLineItemsVO.setcGST(inLineItemsVO.getcGST().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
						if((inLineItemsVO.getsGST()!=null)&&(inLineItemsVO.getsGST().compareTo(BigDecimal.ZERO)>0))
						inLineItemsVO.setsGST(inLineItemsVO.getsGST().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
						if((inLineItemsVO.getiGST()!=null)&&(inLineItemsVO.getiGST().compareTo(BigDecimal.ZERO)>0))
						inLineItemsVO.setiGST(inLineItemsVO.getiGST().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
						if((inLineItemsVO.getuGST()!=null)&&(inLineItemsVO.getuGST().compareTo(BigDecimal.ZERO)>0))
						inLineItemsVO.setuGST(inLineItemsVO.getuGST().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
						if((inLineItemsVO.getCess()!=null)&&(inLineItemsVO.getCess().compareTo(BigDecimal.ZERO)>0))
						inLineItemsVO.setCess(inLineItemsVO.getCess().divide(invoiceVO.getCurrencyRate(),2,RoundingMode.HALF_UP));
						
						
					}
				  }
				  
				  invoiceVO.setGstSummary(gstSummary);	
				  
				  invoiceReceiptList=transactionService.getInvoiceRceiptList(invoiceVO.getOrgID(),invoiceVO.getInvoiceID());
				  invoiceVO.setReceiptList(invoiceReceiptList);
				  
				  orgVO=societyService.getSocietyDetails("0", invoiceVO.getOrgID()); 	
				  invoiceVO.setOrgVO(orgVO);
				  
				  invoiceLinkList=invoiceDAO.getLinkedInvoiceList(invoiceVO.getOrgID(),invoiceVO.getInvoiceID());
				  invoiceVO.setLinkInvList(invoiceLinkList);
				  
				  BankInfoVO bankVO=societyService.getBankDetails(invoiceVO.getOrgID());
				  invoiceVO.setBankVO(bankVO);
				  
				  invoiceVO.setTotalAmountInWords(amtInWrds.convertToAmtAllCurrency(invoiceVO.getTotalAmount().toString(),invoiceVO.getCurrencyVO().getCurrencyName(),invoiceVO.getCurrencyVO().getDecimalCurrency()));
				  
				  ProfileDetailsVO profileVO=ledgerService.getLedgerProfile(invoiceVO.getEntityID(), invoiceVO.getOrgID());
				  
				  if((profileVO.getPanNo()!=null)&&(profileVO.getPanNo().length()>0)){
				  invoiceVO.setCustomerPAN(profileVO.getPanNo());
				  }
			  }
			}catch(EmptyResultDataAccessException e){
				logger.info("No info found for getInvoiceDetailsByInvoiceNumber  - Invoice Number "+invoiceVO.getInvoiceNumber()+" for Org ID "+invoiceVO.getOrgID());
			}
			catch(Exception e){
				logger.error("Exception in getInvoiceDetailsByInvoiceNumber "+e);
			}
			logger.debug("Exit : public int getInvoiceDetailsByInvoiceNumber(InvoiceVO invoiceVO)");
			return invoiceVO;		
		}
		
		public InvoiceVO updateGSTInvoice(InvoiceVO invoiceVO){
			int updateFlag=0;
			int success=0;
			int insertInvoiceLineItemsFlag=0;
			int deleteInvoiceLineItemsFlag=0;
			TransactionVO txVO=new TransactionVO();
			InvoiceVO validationVO=new InvoiceVO();
			String counterType="";			
			logger.debug("Entry : public InvoiceVO updateGSTInvoice(InvoiceVO invoiceVO)");
			try{
				
				//currency conversion
				  invoiceVO=currencyConversion(invoiceVO,"M");
				  
				//Draft update invoice			
				if(invoiceVO.getIsReview()==1){
					
					 updateFlag=invoiceDAO.updateGSTInvoice(invoiceVO);
					 invoiceVO.setLedgerID(invoiceVO.getEntityID());
									 
					 if(updateFlag==1){
						 deleteInvoiceLineItemsFlag=invoiceDAO.deleteGSTInvoiceLineItems(invoiceVO);
					 }				 
					 
					 if((updateFlag==1) && (deleteInvoiceLineItemsFlag>0) ){			  
					   insertInvoiceLineItemsFlag=invoiceDAO.insertInvoiceLineItems(invoiceVO);
					 }
					 logger.debug("updateInvoice flag:  "+updateFlag+" deleteInvoiceLineItemsFlag: "+deleteInvoiceLineItemsFlag+" insertInvoiceLineItemsFlag: "+insertInvoiceLineItemsFlag);
					 if(insertInvoiceLineItemsFlag>0){					 
						 invoiceVO.setStatusCode(200);
						 invoiceVO.setStatusMessage("Invoice has been updated succefully");
						 logger.info("invoice updated succefully invoiceID: "+invoiceVO.getInvoiceID());
					 }else{
						 invoiceVO.setStatusCode(530);
						 invoiceVO.setStatusMessage("Unable to update invoice");
					 }
					 
				}else if(invoiceVO.getIsReview()==0){//final invoice
					
					validationVO=validateInvoice(invoiceVO, invoiceVO.getOrgID());
					
					if(validationVO.getStatusCode()==200){
						
						//counter
						if(((invoiceVO.getInvoiceType()!=null)&&(invoiceVO.getInvoiceType().length()>0)&&(invoiceVO.getIsReview()==0)&&(invoiceVO.getInvoiceNumber()==null|| invoiceVO.getInvoiceNumber().length()==0))){
						     counterType=transactionService.getCounterType(invoiceVO.getInvoiceType());
						     int autoIncNo=transactionService.getLatestCounterNo(invoiceVO.getOrgID(), counterType);
						     invoiceVO.setInvoiceNumber(""+autoIncNo);
						    
						}
						
					     if(((invoiceVO.getDocID()==null)||(invoiceVO.getDocID().length()==0))&&((invoiceVO.getInvoiceType().equalsIgnoreCase("Journal"))||(invoiceVO.getInvoiceType().equalsIgnoreCase("Sales"))||(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note")))){

						    // int gstDocRefID=transactionService.getLatestCounterNo(invoiceVO.getOrgID(), "gst_doc_id");
						     invoiceVO.setDocID("");
						     }
						
						 updateFlag=invoiceDAO.updateGSTInvoice(invoiceVO);
						 invoiceVO.setLedgerID(invoiceVO.getEntityID());
										 
						 if(updateFlag==1){
							 deleteInvoiceLineItemsFlag=invoiceDAO.deleteGSTInvoiceLineItems(invoiceVO);
						 }				 
						 
						 if((updateFlag==1) && (deleteInvoiceLineItemsFlag>0) ){			  
						   insertInvoiceLineItemsFlag=invoiceDAO.insertInvoiceLineItems(invoiceVO);
						 }
						 
						 logger.debug("updateInvoice flag:  "+updateFlag+" deleteInvoiceLineItemsFlag: "+deleteInvoiceLineItemsFlag+" insertInvoiceLineItemsFlag: "+insertInvoiceLineItemsFlag);
						 
						 if((insertInvoiceLineItemsFlag>0)&&(invoiceVO.getIsReview()==0)&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Journal")||invoiceVO.getInvoiceType().equalsIgnoreCase("Sales")||(invoiceVO.getInvoiceType().equalsIgnoreCase("Purchase"))||(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note"))||(invoiceVO.getInvoiceType().equalsIgnoreCase("Debit Note")))){
													
								logger.debug("Update invoice--->"+invoiceVO.getTotalAmount());
								if((invoiceVO.getTransactionID()==null)||(invoiceVO.getTransactionID().length()==0)){
									//create transaction
									txVO=convertInvoiceVOTOTx(invoiceVO,invoiceVO.getOrgID());					   
									 txVO=transactionService.addTransaction(txVO, invoiceVO.getOrgID());		
									
								}else{
									//update transaction
									txVO=convertInvoiceVOTOTx(invoiceVO,invoiceVO.getOrgID());	
									TransactionVO transactionVO=new TransactionVO();
									transactionVO=transactionService.getTransctionDetails(invoiceVO.getOrgID(), Integer.parseInt(invoiceVO.getTransactionID()));
									transactionVO.setLedgerEntries(txVO.getLedgerEntries());
									transactionVO.setIsUpdated("Y");
									transactionVO.setAmount(txVO.getAmount());
									transactionVO.setUpdatedBy(invoiceVO.getUpdatedBy());
									transactionVO.setDescription(txVO.getDescription());
									transactionVO.setDocID(txVO.getDocID());		
									transactionVO.setTransactionNumber(txVO.getTransactionNumber());
									transactionVO.setTransactionDate(txVO.getTransactionDate());
									logger.debug("TX Number--->"+transactionVO.getTransactionNumber());
									txVO=transactionService.updateTransaction(transactionVO, invoiceVO.getOrgID(), transactionVO.getTransactionID());
									logger.debug("Update transaction result:  "+txVO.getStatusCode());
								}
								
							
						 }else{
							 txVO.setStatusCode(200);
						 }
						 
						
						 if((txVO.getStatusCode()==200)){						 
							
								//increment counter
							logger.debug("counterType--->"+counterType);
							if((invoiceVO.getInvoiceType()!=null)&&(invoiceVO.getInvoiceNumber().length()>0)&&(counterType.length()>0)){
								int counterNo=transactionService.incrementCounterNumber(invoiceVO.getOrgID(), counterType);
							}
							
						     if(((invoiceVO.getDocID()!=null)&&(invoiceVO.getDocID().length()>0)&&(counterType.length()>0))&&((invoiceVO.getInvoiceType().equalsIgnoreCase("Journal"))||(invoiceVO.getInvoiceType().equalsIgnoreCase("Sales"))||(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note")))){
	
								int gstDocRefID=transactionService.incrementCounterNumber(invoiceVO.getOrgID(), "gst_doc_id");
						    }
							
							 invoiceVO.setStatusCode(200);
							 invoiceVO.setStatusMessage("Invoice has been updated succefully");
							 logger.info("Invoice updated succefully invoiceID: "+invoiceVO.getInvoiceID());							
							 
						 }else{
							 if(insertInvoiceLineItemsFlag==0){
								 txVO.setStatusCode(406);
								 txVO.setErrorMessage("Unable to update invoice");
							 }
							 invoiceVO.setStatusCode(txVO.getStatusCode());
							 invoiceVO.setStatusMessage(txVO.getErrorMessage());
							 
						 }
						
						
					}else{
						invoiceVO.setStatusCode(validationVO.getStatusCode());
						invoiceVO.setStatusMessage(validationVO.getStatusMessage());						
					}	 
					 
					
			   }
				 
			}catch(Exception e){
				logger.error("Exception in updateGSTInvoice "+e);
			}
			
			
			logger.debug("Exit : public InvoiceVO updateGSTInvoice(InvoiceVO invoiceVO)");
			return invoiceVO;		
		}
		
		/* Used for validation invoice */
		public InvoiceVO validateInvoice(InvoiceVO invoiceVO,int societyID){
			logger.debug("Entry :public InvoiceVO validateInvoice(InvoiceVO invoiceVO,int societyID)"+societyID);
			BigDecimal sumOfCr=BigDecimal.ZERO;
			BigDecimal sumOfDr=BigDecimal.ZERO;
			SocietyVO societyVO =new SocietyVO();
			Boolean isAuditDateValid=false;
			Boolean isGSTDateValid=false;
			Boolean isSocDateValid=false;
			try{		
				Date socLocDate;
				Date auditLocDate;
				Date gstLocDate;
				Date invoiceDate=sdf.parse(invoiceVO.getInvoiceDate());
				societyVO=societyService.getSocietyDetails(societyID);
				
				//audit lock date
				if(societyVO.getAuditCloseDate()!=null){
					auditLocDate=sdf.parse(societyVO.getAuditCloseDate());
					if(invoiceDate.compareTo(auditLocDate)<=0){
						isAuditDateValid=false;
						invoiceVO.setStatusCode(530);
						invoiceVO.setStatusMessage("Your organization audits has been locked till "+dateUtil.getConvetedDate(societyVO.getAuditCloseDate())+", So that no transactions can be added");
						return invoiceVO;
					}else{					
						isAuditDateValid=true;
					}						
					
				}else{
					isAuditDateValid=true;
				}				
			
				//gst lock date
				if((societyVO.getGstCloseDate()!=null)&&(societyVO.getGstIN()!=null)&&(societyVO.getGstIN().length()>0)){
					gstLocDate=sdf.parse(societyVO.getGstCloseDate());
					if(invoiceDate.compareTo(gstLocDate)<=0){ 
						isGSTDateValid=false;
						invoiceVO.setStatusCode(530);
						invoiceVO.setStatusMessage("Your organization GST has been locked till "+dateUtil.getConvetedDate(societyVO.getGstCloseDate())+", So that no transactions can be added.");
						return invoiceVO;
					}else{					
						isGSTDateValid=true;
					}
				
				}else{
					isGSTDateValid=true;
				}
				
				//society lock date
				if(societyVO.getTxCloseDate()!=null){
					socLocDate=sdf.parse(societyVO.getTxCloseDate());
					if(invoiceDate.compareTo(socLocDate)<=0){ 
						isSocDateValid=false;
						invoiceVO.setStatusCode(530);
						invoiceVO.setStatusMessage("Your organization accounts has been locked till "+dateUtil.getConvetedDate(societyVO.getTxCloseDate())+", So that no transactions can be added");
						return invoiceVO;
					}else{					
						isSocDateValid=true;
					}
				
				}else{
					isSocDateValid=true;
				}
				
			
			   //ledger lock date	
			   if((isAuditDateValid)&&(isGSTDateValid)&&(isSocDateValid)){
				   
				   List<AccountHeadVO> tempLedgerList=new ArrayList<>();
					
					for(int i=0;invoiceVO.getLineItemsList().size()>i;i++){					
						
						InLineItemsVO inLineItemEntry=(InLineItemsVO) invoiceVO.getLineItemsList().get(i);
				
						AccountHeadVO tempLedgerVO=new AccountHeadVO();
						tempLedgerVO=ledgerService.getLedgerDetails(inLineItemEntry.getLedgerID(), societyID);
						tempLedgerList.add(tempLedgerVO);
					}
				  	LedgerEntries revLedgerEntry=convertInvoiceTOLedgerEntries(invoiceVO,societyID);
					AccountHeadVO revLedger=ledgerService.getLedgerDetails(revLedgerEntry.getLedgerID(), societyID);
					tempLedgerList.add(revLedger);
					
					List gstList=convertGSTToLedgerEntries(invoiceVO,societyID);
					List utilityList=convertUtilityToLedgerEntries(invoiceVO, societyID);
					for(int i=0;gstList.size()>i;i++){					
						
						LedgerEntries ledgerEntry=(LedgerEntries) gstList.get(i);
				
						AccountHeadVO tempLedgerVO=new AccountHeadVO();
						tempLedgerVO=ledgerService.getLedgerDetails(ledgerEntry.getLedgerID(), societyID);
						tempLedgerList.add(tempLedgerVO);
					}
					for(int i=0;utilityList.size()>i;i++){					
						
						LedgerEntries ledgerEntry=(LedgerEntries) utilityList.get(i);
				
						AccountHeadVO tempLedgerVO=new AccountHeadVO();
						tempLedgerVO=ledgerService.getLedgerDetails(ledgerEntry.getLedgerID(), societyID);
						tempLedgerList.add(tempLedgerVO);
					}
					
					for(int i=0;tempLedgerList.size()>i;i++){					
						
						AccountHeadVO tempLedgerVO=tempLedgerList.get(i);
						
						if(invoiceDate.compareTo(sdf.parse(tempLedgerVO.getEffectiveDate()))<=0){
							invoiceVO.setStatusCode(553);
							invoiceVO.setStatusMessage("Cannot complete your request since, ledger "+tempLedgerVO.getStrAccountHeadName()+"  effective date is "+dateUtil.getConvetedDate(tempLedgerVO.getEffectiveDate())+"  , Please check the ledger.");
							break;
						}else 	if(tempLedgerVO.getIsDeleted()!=0){
							invoiceVO.setStatusCode(552);
							invoiceVO.setStatusMessage("Cannot complete your request since Ledger "+tempLedgerVO.getStrAccountHeadName()+" is inactive , Please check the ledger.");
							break;
						}else	if(tempLedgerVO.getLockDate()!=null){
							if(invoiceDate.compareTo(sdf.parse(tempLedgerVO.getLockDate())) >= 0){
							
								invoiceVO.setStatusCode(200);
								invoiceVO.setStatusMessage("Invoice validated successfully");
							}else{
								invoiceVO.setStatusCode(531);
								invoiceVO.setStatusMessage("Cannot complete your request since Ledger "+tempLedgerVO.getStrAccountHeadName()+" has been locked till "+dateUtil.getConvetedDate(tempLedgerVO.getLockDate())+", Please co-ordinate with your administrator to remove ledger or account lock.");
								break;
							 }
						 }else invoiceVO.setStatusCode(200);							
					
						
					}
							
											
				}
				   
					
				logger.debug("Status code : "+invoiceVO.getStatusCode()+" Message: "+invoiceVO.getStatusMessage());
			
			}catch(NullPointerException e){
				logger.debug("Exception in validateInvoice NullPointerException ");
				
				
			}catch(Exception e){
				logger.error("Exception in validateInvoice "+e);
			}
			
			logger.debug("Exit :public InvoiceVO validateInvoice(InvoiceVO invoiceVO,int societyID)");
			return invoiceVO;
		
		}
		

		/* Convert Invoice VO to TxVO  */
		private TransactionVO convertInvoiceVOTOTx(InvoiceVO invoiceVO,int societyID){
			TransactionVO txVO=new TransactionVO();
			List<LedgerEntries> ledgerEntriesList=new ArrayList<LedgerEntries>();
			TransactionMetaVO txMeta=new TransactionMetaVO();
			List<String> ledgerTxList=new ArrayList<String>();
			List lineItemsList;
			List tempList=new ArrayList();
			String legerTags="";
			LedgerEntries revLedgerEntry=new LedgerEntries();
			CommonUtility commonUtils=new CommonUtility();
			try{
			logger.debug("Entry : private TransactionVO convertInvoiceVOTOTx(InvoiceDetailsVO invoiceVO)");
			SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			txVO.setTransactionNumber(invoiceVO.getInvoiceNumber()+"");
			if(invoiceVO.getDocID()==null || invoiceVO.getDocID().length()==0){
				txVO.setDocID(invoiceVO.getInvoiceNumber()+"");
			}else{
				txVO.setDocID(invoiceVO.getDocID()+"");
			}			
			txVO.setInvoiceID(invoiceVO.getInvoiceID());
			txVO.setAmount(invoiceVO.getTotalAmount().abs());
			txVO.setTransactionDate(invoiceVO.getInvoiceDate());
			txVO.setTransactionType(invoiceVO.getInvoiceType());
		    txVO.setAdditionalProps(txMeta);
		    txVO.setTransactionMode("");
			txVO.setDescription(invoiceVO.getNotes());
			txVO.setAutoInc(false);
			txVO.setCreatedBy(invoiceVO.getCreatedBy());
			txVO.setUpdatedBy(invoiceVO.getUpdatedBy());
			txVO.setTxTags(invoiceVO.getInvoiceTags());
			txVO.setValidation(false);
			lineItemsList=invoiceDAO.getLineItemsForInvoice(societyID, invoiceVO.getInvoiceID());
			invoiceVO.setTotalGST(invoiceVO.getTotalCGST().add(invoiceVO.getTotalSGST()).add(invoiceVO.getTotalIGST()).add(invoiceVO.getTotalUGST()));
			
		   
			
			
		
			for(int i=0;lineItemsList.size()>i;i++){
				InLineItemsVO lineItem=(InLineItemsVO) lineItemsList.get(i);
				LedgerEntries ledgerEntry=new LedgerEntries();
				ledgerEntry=convertLineItemsTOLedgerEntries(lineItem,invoiceVO.getInvoiceType(),invoiceVO);
				ledgerEntry.setLedgerTags(lineItem.getLineItemTags());
				ledgerEntriesList.add(i, ledgerEntry);
				if((lineItem.getLineItemTags()!=null)&&(lineItem.getLineItemTags().length()>0)){
				List<String> result = Arrays.asList(lineItem.getLineItemTags().split("\\s*#"));
				  for(int j=0;result.size()>j;j++){
					  String tags=result.get(j);
					  if((j>0)&&(tags.length()>0)){
						  ledgerTxList.add(tags.trim());
					  }
				  }
				}
			}
			
			ledgerTxList=commonUtils.removeDuplicates((ArrayList<String>) ledgerTxList);
			
			for(int i=0;ledgerTxList.size()>i;i++){
				String tags=ledgerTxList.get(i);
				legerTags=legerTags+"#"+tags;
			}
			
			
			revLedgerEntry=convertInvoiceTOLedgerEntries(invoiceVO,societyID);
			revLedgerEntry.setLedgerTags(legerTags);
			ledgerEntriesList.add(0,revLedgerEntry);
			
			List gstList=convertGSTToLedgerEntries(invoiceVO,societyID);
			List utilityList=convertUtilityToLedgerEntries(invoiceVO, societyID);
			
			if((invoiceVO.getMyGSTIN()!=null)&&(invoiceVO.getMyGSTIN().length()>0)){
				
				if((gstList.size()>0) && (societyVO.getGstAsExpense()==0) &&(invoiceVO.getInvoiceType().equalsIgnoreCase("Purchase")||invoiceVO.getInvoiceType().equalsIgnoreCase("Debit Note"))){
				for(int i=0; gstList.size()>i;i++){
					LedgerEntries ledgerEntry=(LedgerEntries) gstList.get(i);
					ledgerEntry.setLedgerTags(legerTags);
				    ledgerEntriesList.add(ledgerEntry);
				}
			}else if((gstList.size()>0) && (societyVO.getGstAsIncome()==0)&& (invoiceVO.getInvoiceType().equalsIgnoreCase("Sales")||invoiceVO.getInvoiceType().equalsIgnoreCase("Journal")||invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note"))){
				for(int i=0; gstList.size()>i;i++){
					LedgerEntries ledgerEntry=(LedgerEntries) gstList.get(i);
					ledgerEntry.setLedgerTags(legerTags);
					ledgerEntriesList.add(ledgerEntry);
				}
			}
			}
			
			if(utilityList.size()>0){
				for(int i=0; utilityList.size()>i;i++){
					LedgerEntries ledgerEntry=(LedgerEntries) utilityList.get(i);
					ledgerEntry.setLedgerTags(legerTags);
					ledgerEntriesList.add(ledgerEntry);
				}
			}
			
			txVO.setLedgerEntries(ledgerEntriesList);
			
			txVO=processMulticurrencyTransactions(txVO);
			
			
			}catch(Exception e){
				
				logger.error("Exception : convertInvoiceVoToTx "+e);
			}
			
			logger.debug("Exit : private TransactionVO convertInvoiceVOTOTx(InvoiceDetailsVO invoiceVO)");
			return txVO;		
		}
		
		private TransactionVO processMulticurrencyTransactions(TransactionVO txVO){
			TransactionVO transactionVO=new TransactionVO();
			List ledgerEntries=txVO.getLedgerEntries();
			BigDecimal oppLedgerSum=BigDecimal.ZERO;
			LedgerEntries oppLedger=(LedgerEntries) ledgerEntries.get(0);
			if(ledgerEntries.size()>0){
				for(int i=0;ledgerEntries.size()>i;i++){
					LedgerEntries ledgers=(LedgerEntries) ledgerEntries.get(i);
					if(i>0){
						if(oppLedger.getCreditDebitFlag().equalsIgnoreCase("C")){
							if(ledgers.getCreditDebitFlag().equalsIgnoreCase("D")){
								oppLedgerSum=oppLedgerSum.add(ledgers.getAmount());
							}else if(ledgers.getCreditDebitFlag().equalsIgnoreCase("C")){
								oppLedgerSum=oppLedgerSum.subtract(ledgers.getAmount().abs());
							}
							
							
						}else if(oppLedger.getCreditDebitFlag().equalsIgnoreCase("D")){
							if(ledgers.getCreditDebitFlag().equalsIgnoreCase("C")){
								oppLedgerSum=oppLedgerSum.add(ledgers.getAmount());
							}else if(ledgers.getCreditDebitFlag().equalsIgnoreCase("D")){
								oppLedgerSum=oppLedgerSum.subtract(ledgers.getAmount().abs());
							}
							
							
						}
						
						
						
						
					}
					
					
					
				}
				
				txVO.getLedgerEntries().get(0).setAmount(oppLedgerSum);
				
				
				
			}
			
			
			
			return txVO;
		}
		
		
		/* Convert Invoice VO to TxVO  */
		private LedgerEntries convertLineItemsTOLedgerEntries(InLineItemsVO lineItemVO,String invoiceType,InvoiceVO invoiceVO){
			LedgerEntries ledgerEntry=new LedgerEntries();
			
			logger.debug("Entry : private LedgerEntries convertInvoiceLineItemsTOLedgerEntries(InvoiceLineItemsVO invoiceVO)"+lineItemVO.getTotalPrice());
			SocietyVO societyVO=societyService.getSocietyDetails(invoiceVO.getOrgID());
			if(invoiceType.equalsIgnoreCase("Sales")){

				ledgerEntry.setAmount(lineItemVO.getTotalPrice().add(lineItemVO.getDiscount()));
				if(ledgerEntry.getAmount().signum()>0){
				ledgerEntry.setCreditDebitFlag("C");
				}else ledgerEntry.setCreditDebitFlag("D");
				ledgerEntry.setLedgerID(lineItemVO.getLedgerID());
				ledgerEntry.setStrComments(lineItemVO.getDescription());
				ledgerEntry.setInvLineItemID(lineItemVO.getInvLineItemID());  

			}else if(invoiceType.equalsIgnoreCase("Purchase")){
				if((invoiceVO.getMyGSTIN()!=null)&&(invoiceVO.getMyGSTIN().length()>0)&&(societyVO.getGstAsExpense()==0)){
					ledgerEntry.setAmount(lineItemVO.getTotalPrice().add(lineItemVO.getDiscount()));
				}else
					ledgerEntry.setAmount(lineItemVO.getTotalPrice().add(lineItemVO.getDiscount()).add(lineItemVO.getcGST()).add(lineItemVO.getiGST()).add(lineItemVO.getsGST()).add(lineItemVO.getuGST()).add(lineItemVO.getCess()));
				if(ledgerEntry.getAmount().signum()>0){
				ledgerEntry.setCreditDebitFlag("D");
				}else ledgerEntry.setCreditDebitFlag("C");
				ledgerEntry.setLedgerID(lineItemVO.getLedgerID());
				ledgerEntry.setStrComments(lineItemVO.getDescription());
				ledgerEntry.setInvLineItemID(lineItemVO.getInvLineItemID());
				
			}else if(invoiceType.equalsIgnoreCase("Credit Note")){

				ledgerEntry.setAmount(lineItemVO.getTotalPrice().add(lineItemVO.getDiscount()));
				if(ledgerEntry.getAmount().signum()>0){
				   ledgerEntry.setCreditDebitFlag("D");
				}else ledgerEntry.setCreditDebitFlag("C");
					ledgerEntry.setLedgerID(lineItemVO.getLedgerID());
					ledgerEntry.setStrComments(lineItemVO.getDescription());
					ledgerEntry.setInvLineItemID(lineItemVO.getInvLineItemID());

			}else if(invoiceType.equalsIgnoreCase("Debit Note")){
				if((invoiceVO.getMyGSTIN()!=null)&&(invoiceVO.getMyGSTIN().length()>0)&&(societyVO.getGstAsExpense()==0)){
				ledgerEntry.setAmount(lineItemVO.getTotalPrice().add(lineItemVO.getDiscount()));
				}else
					ledgerEntry.setAmount(lineItemVO.getTotalPrice().add(lineItemVO.getDiscount()).add(lineItemVO.getcGST()).add(lineItemVO.getiGST()).add(lineItemVO.getsGST()).add(lineItemVO.getuGST()).add(lineItemVO.getCess()));
				if(ledgerEntry.getAmount().signum()>0){
				ledgerEntry.setCreditDebitFlag("C");
				}else ledgerEntry.setCreditDebitFlag("D");
				ledgerEntry.setLedgerID(lineItemVO.getLedgerID());
				ledgerEntry.setStrComments(lineItemVO.getDescription());
				ledgerEntry.setInvLineItemID(lineItemVO.getInvLineItemID());
			}else if(invoiceType.equalsIgnoreCase("Journal")){
				ledgerEntry.setAmount(lineItemVO.getTotalPrice().add(lineItemVO.getDiscount()));
				if(ledgerEntry.getAmount().signum()>0){
				ledgerEntry.setCreditDebitFlag("C");
				}else ledgerEntry.setCreditDebitFlag("D");
				ledgerEntry.setLedgerID(lineItemVO.getLedgerID());
				ledgerEntry.setStrComments(lineItemVO.getDescription());
				ledgerEntry.setInvLineItemID(lineItemVO.getInvLineItemID());

			}

				
			
			
			logger.debug("Exit : private LedgerEntries convertInvoiceLineItemsTOLedgerEntries(InvoiceLineItemsVO invoiceVO)");
			return ledgerEntry;		
		}
		
		
		
		/* Convert Invoice VO to TxVO  */
		private List convertGSTToLedgerEntries(InvoiceVO invoiceVO,int orgID){
			List gstList=new ArrayList();
			LedgerEntries ledgerEntry=new LedgerEntries();
			
			logger.debug("Entry : private LedgerEntries convertInvoiceLineItemsTOLedgerEntries(InvoiceLineItemsVO invoiceVO)");
			
			
			List taxLedgersList=ledgerService.getLedgersListByProperties(orgID, "%tax%");
			
			if(invoiceVO.getTotalCGST().compareTo(BigDecimal.ZERO)!=0){
				LedgerEntries totalcGST=new LedgerEntries();
				int compare=invoiceVO.getTotalCGST().compareTo(BigDecimal.ZERO);
				
				for(int i=0;taxLedgersList.size()>i;i++){
					AccountHeadVO achVO=(AccountHeadVO) taxLedgersList.get(i);
					
					if((achVO.getAdditionalProps().contains("#cGST")&&((invoiceVO.getInvoiceType().equalsIgnoreCase("Sales")) ||(invoiceVO.getInvoiceType().equalsIgnoreCase("Journal"))))){
						totalcGST.setLedgerID(achVO.getLedgerID());
						totalcGST.setAmount(invoiceVO.getTotalCGST());
						if(compare>0){
							totalcGST.setCreditDebitFlag("C");
						}else{
							totalcGST.setCreditDebitFlag("D");
						}
					}else if((achVO.getAdditionalProps().contains("#cGST")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Purchase")))){
						totalcGST.setLedgerID(achVO.getLedgerID());
						totalcGST.setAmount(invoiceVO.getTotalCGST());
						if(compare>0){
							totalcGST.setCreditDebitFlag("D");
						}else{
							totalcGST.setCreditDebitFlag("C");
						}
					}else if((achVO.getAdditionalProps().contains("#cGST")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note")))){
						totalcGST.setLedgerID(achVO.getLedgerID());
						totalcGST.setAmount(invoiceVO.getTotalCGST());
						if(compare>0){
							totalcGST.setCreditDebitFlag("D");
						}else{
							totalcGST.setCreditDebitFlag("C");
						}
					}else if((achVO.getAdditionalProps().contains("#cGST")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Debit Note")))){
						totalcGST.setLedgerID(achVO.getLedgerID());
						totalcGST.setAmount(invoiceVO.getTotalCGST());
						if(compare>0){
							totalcGST.setCreditDebitFlag("C");
						}else{
							totalcGST.setCreditDebitFlag("D");
						}
					}
					
				}
				if(totalcGST.getLedgerID()!=0){
					gstList.add(totalcGST);
				}
				
			}
			
			if(invoiceVO.getTotalIGST().compareTo(BigDecimal.ZERO)!=0){
				LedgerEntries totalIGST=new LedgerEntries();
				int compare=invoiceVO.getTotalIGST().compareTo(BigDecimal.ZERO);
				
				for(int i=0;taxLedgersList.size()>i;i++){
					AccountHeadVO achVO=(AccountHeadVO) taxLedgersList.get(i);
					
					if((achVO.getAdditionalProps().contains("#iGST")&&((invoiceVO.getInvoiceType().equalsIgnoreCase("Sales")) ||(invoiceVO.getInvoiceType().equalsIgnoreCase("Journal"))))){
						totalIGST.setLedgerID(achVO.getLedgerID());
						totalIGST.setAmount(invoiceVO.getTotalIGST());
						if(compare>0){
							totalIGST.setCreditDebitFlag("C");
						}else{
							totalIGST.setCreditDebitFlag("D");
						}
					}else if((achVO.getAdditionalProps().contains("#iGST")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Purchase")))){
						totalIGST.setLedgerID(achVO.getLedgerID());
						totalIGST.setAmount(invoiceVO.getTotalIGST());
						if(compare>0){
							totalIGST.setCreditDebitFlag("D");
						}else{
							totalIGST.setCreditDebitFlag("C");
						}
					}else if((achVO.getAdditionalProps().contains("#iGST")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note")))){
						totalIGST.setLedgerID(achVO.getLedgerID());
						totalIGST.setAmount(invoiceVO.getTotalIGST());
						if(compare>0){
							totalIGST.setCreditDebitFlag("D");
						}else{
							totalIGST.setCreditDebitFlag("C");
						}
					}else if((achVO.getAdditionalProps().contains("#iGST")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Debit Note")))){
						totalIGST.setLedgerID(achVO.getLedgerID());
						totalIGST.setAmount(invoiceVO.getTotalIGST());
						if(compare>0){
							totalIGST.setCreditDebitFlag("C");
						}else{
							totalIGST.setCreditDebitFlag("D");
						}
					}
					
				}
				if(totalIGST.getLedgerID()!=0){
					gstList.add(totalIGST);
				}
				
			}
			
			if(invoiceVO.getTotalSGST().compareTo(BigDecimal.ZERO)!=0){
				LedgerEntries totalSGST=new LedgerEntries();
				int compare=invoiceVO.getTotalSGST().compareTo(BigDecimal.ZERO);
				
				for(int i=0;taxLedgersList.size()>i;i++){
					AccountHeadVO achVO=(AccountHeadVO) taxLedgersList.get(i);
					
					if((achVO.getAdditionalProps().contains("#sGST")&&((invoiceVO.getInvoiceType().equalsIgnoreCase("Sales")) ||(invoiceVO.getInvoiceType().equalsIgnoreCase("Journal"))))){
						totalSGST.setLedgerID(achVO.getLedgerID());
						totalSGST.setAmount(invoiceVO.getTotalSGST());
						if(compare>0){
							totalSGST.setCreditDebitFlag("C");
						}else{
							totalSGST.setCreditDebitFlag("D");
						}
					}else if((achVO.getAdditionalProps().contains("#sGST")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Purchase")))){
						totalSGST.setLedgerID(achVO.getLedgerID());
						totalSGST.setAmount(invoiceVO.getTotalSGST());
						if(compare>0){
							totalSGST.setCreditDebitFlag("D");
						}else{
							totalSGST.setCreditDebitFlag("C");
						}
					}else if((achVO.getAdditionalProps().contains("#sGST")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note")))){
						totalSGST.setLedgerID(achVO.getLedgerID());
						totalSGST.setAmount(invoiceVO.getTotalSGST());
						if(compare>0){
							totalSGST.setCreditDebitFlag("D");
						}else{
							totalSGST.setCreditDebitFlag("C");
						}
					}else if((achVO.getAdditionalProps().contains("#sGST")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Debit Note")))){
						totalSGST.setLedgerID(achVO.getLedgerID());
						totalSGST.setAmount(invoiceVO.getTotalSGST());
						if(compare>0){
							totalSGST.setCreditDebitFlag("C");
						}else{
							totalSGST.setCreditDebitFlag("D");
						}
					}
					
				}
				if(totalSGST.getLedgerID()!=0){
					gstList.add(totalSGST);
				}
				
			}
			if(invoiceVO.getTotalUGST().compareTo(BigDecimal.ZERO)!=0){
				LedgerEntries totalUGST=new LedgerEntries();
				int compare=invoiceVO.getTotalUGST().compareTo(BigDecimal.ZERO);
				
				for(int i=0;taxLedgersList.size()>i;i++){
					AccountHeadVO achVO=(AccountHeadVO) taxLedgersList.get(i);
					
					if((achVO.getAdditionalProps().contains("#uGST")&&((invoiceVO.getInvoiceType().equalsIgnoreCase("Sales")) ||(invoiceVO.getInvoiceType().equalsIgnoreCase("Journal"))))){
						totalUGST.setLedgerID(achVO.getLedgerID());
						totalUGST.setAmount(invoiceVO.getTotalUGST());
						if(compare>0){
							totalUGST.setCreditDebitFlag("C");
						}else{
							totalUGST.setCreditDebitFlag("D");
						}
					}else if((achVO.getAdditionalProps().contains("#uGST")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Purchase")))){
						totalUGST.setLedgerID(achVO.getLedgerID());
						totalUGST.setAmount(invoiceVO.getTotalUGST());
						if(compare>0){
							totalUGST.setCreditDebitFlag("D");
						}else{
							totalUGST.setCreditDebitFlag("C");
						}
					}else if((achVO.getAdditionalProps().contains("#uGST")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note")))){
						totalUGST.setLedgerID(achVO.getLedgerID());
						totalUGST.setAmount(invoiceVO.getTotalUGST());
						if(compare>0){
							totalUGST.setCreditDebitFlag("D");
						}else{
							totalUGST.setCreditDebitFlag("C");
						}
					}else if((achVO.getAdditionalProps().contains("#uGST")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Debit Note")))){
						totalUGST.setLedgerID(achVO.getLedgerID());
						totalUGST.setAmount(invoiceVO.getTotalUGST());
						if(compare>0){
							totalUGST.setCreditDebitFlag("C");
						}else{
							totalUGST.setCreditDebitFlag("D");
						}
					}
					
				}
				if(totalUGST.getLedgerID()!=0){
					gstList.add(totalUGST);
				}
				
			}
			
			if(invoiceVO.getTotalCess().compareTo(BigDecimal.ZERO)!=0){
				LedgerEntries totalCess=new LedgerEntries();
				int compare=invoiceVO.getTotalCess().compareTo(BigDecimal.ZERO);
				
				for(int i=0;taxLedgersList.size()>i;i++){
					AccountHeadVO achVO=(AccountHeadVO) taxLedgersList.get(i);
					
					if((achVO.getAdditionalProps().contains("#cess")&&((invoiceVO.getInvoiceType().equalsIgnoreCase("Sales")) ||(invoiceVO.getInvoiceType().equalsIgnoreCase("Journal"))))){
						totalCess.setLedgerID(achVO.getLedgerID());
						totalCess.setAmount(invoiceVO.getTotalCess());
						if(compare>0){
							totalCess.setCreditDebitFlag("C");
						}else{
							totalCess.setCreditDebitFlag("D");
						}
					}else if((achVO.getAdditionalProps().contains("#cess")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Purchase")))){
						totalCess.setLedgerID(achVO.getLedgerID());
						totalCess.setAmount(invoiceVO.getTotalCess());
						if(compare>0){
							totalCess.setCreditDebitFlag("D");
						}else{
							totalCess.setCreditDebitFlag("C");
						}
					}else if((achVO.getAdditionalProps().contains("#cess")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note")))){
						totalCess.setLedgerID(achVO.getLedgerID());
						totalCess.setAmount(invoiceVO.getTotalCess());
						if(compare>0){
							totalCess.setCreditDebitFlag("D");
						}else{
							totalCess.setCreditDebitFlag("C");
						}
					}else if((achVO.getAdditionalProps().contains("#cess")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Debit Note")))){
						totalCess.setLedgerID(achVO.getLedgerID());
						totalCess.setAmount(invoiceVO.getTotalCess());
						if(compare>0){
							totalCess.setCreditDebitFlag("C");
						}else{
							totalCess.setCreditDebitFlag("D");
						}
					}
					
				}
				if(totalCess.getLedgerID()!=0){
					gstList.add(totalCess);
				}
				
			}
		
			
			
		
				
			
		
			
			logger.debug("amount >"+ledgerEntry.getAmount()+" ledgerID -->"+ledgerEntry.getLedgerID()+"  "+ledgerEntry.getStrComments());
				
			
			
			logger.debug("Exit : private LedgerEntries convertInvoiceLineItemsTOLedgerEntries(InvoiceLineItemsVO invoiceVO)");
			return gstList;		
		}


		/* Convert Invoice VO to TxVO  */
		private List convertUtilityToLedgerEntries(InvoiceVO invoiceVO,int orgID){
			List gstList=new ArrayList();
			LedgerEntries ledgerEntry=new LedgerEntries();
			
			logger.debug("Entry : private LedgerEntries convertInvoiceLineItemsTOLedgerEntries(InvoiceLineItemsVO invoiceVO)");
			
			
			List taxLedgersList=ledgerService.getLedgersListByProperties(orgID, "%tax%");
			
		
			
		
			
			
		
			
			
			if(invoiceVO.getTds().compareTo(BigDecimal.ZERO)!=0){
				LedgerEntries totalTDS=new LedgerEntries();
				int compare=invoiceVO.getTds().compareTo(BigDecimal.ZERO);
				
				for(int i=0;taxLedgersList.size()>i;i++){
					AccountHeadVO achVO=(AccountHeadVO) taxLedgersList.get(i);
					
					if((achVO.getAdditionalProps().contains("#tClaim")&&((invoiceVO.getInvoiceType().equalsIgnoreCase("Sales")) ||(invoiceVO.getInvoiceType().equalsIgnoreCase("Journal"))))){
						totalTDS.setLedgerID(achVO.getLedgerID());
						totalTDS.setAmount(invoiceVO.getTds());
						if(compare>0){
							totalTDS.setCreditDebitFlag("D");
						}else{
							totalTDS.setCreditDebitFlag("C");
						}
					}else if((achVO.getAdditionalProps().contains("#tds")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Purchase")))){
						totalTDS.setLedgerID(achVO.getLedgerID());
						totalTDS.setAmount(invoiceVO.getTds()); 
						if(compare>0){
							totalTDS.setCreditDebitFlag("C");
						}else{
							totalTDS.setCreditDebitFlag("D");
						}
					}else if((achVO.getAdditionalProps().contains("#tds")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note")))){
						totalTDS.setLedgerID(achVO.getLedgerID());
						totalTDS.setAmount(invoiceVO.getTds());
						if(compare>0){
							totalTDS.setCreditDebitFlag("D");
						}else{
							totalTDS.setCreditDebitFlag("C");
						}
					}else if((achVO.getAdditionalProps().contains("#tds")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Debit Note")))){
						totalTDS.setLedgerID(achVO.getLedgerID());
						totalTDS.setAmount(invoiceVO.getTds());
						if(compare>0){
							totalTDS.setCreditDebitFlag("C");
						}else{
							totalTDS.setCreditDebitFlag("D");
						}
					}
					
				}
				if(totalTDS.getLedgerID()!=0){
					gstList.add(totalTDS);
				}
				
			}
			
			
			
			if(invoiceVO.getTotalDiscount().compareTo(BigDecimal.ZERO)!=0){
				LedgerEntries totalDiscount=new LedgerEntries();
				int compare=invoiceVO.getTotalDiscount().compareTo(BigDecimal.ZERO);
				
				for(int i=0;taxLedgersList.size()>i;i++){
					AccountHeadVO achVO=(AccountHeadVO) taxLedgersList.get(i);
					
					if((achVO.getAdditionalProps().contains("#discount")&&((invoiceVO.getInvoiceType().equalsIgnoreCase("Sales")) ||(invoiceVO.getInvoiceType().equalsIgnoreCase("Journal"))))){
						totalDiscount.setLedgerID(achVO.getLedgerID());
						totalDiscount.setAmount(invoiceVO.getTotalDiscount());
						if(compare>0){
							totalDiscount.setCreditDebitFlag("D");
						}else{
							totalDiscount.setCreditDebitFlag("C");
						}
					}else if((achVO.getAdditionalProps().contains("#discount")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Purchase")))){
						totalDiscount.setLedgerID(achVO.getLedgerID());
						totalDiscount.setAmount(invoiceVO.getTotalDiscount());
						if(compare>0){
							totalDiscount.setCreditDebitFlag("C");
						}else{
							totalDiscount.setCreditDebitFlag("D");
						}
					}else if((achVO.getAdditionalProps().contains("#discount")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note")))){
						totalDiscount.setLedgerID(achVO.getLedgerID());
						totalDiscount.setAmount(invoiceVO.getTotalDiscount());
						if(compare>0){
							totalDiscount.setCreditDebitFlag("C");
						}else{
							totalDiscount.setCreditDebitFlag("D");
						}
					}else if((achVO.getAdditionalProps().contains("#discount")&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Debit Note")))){
						totalDiscount.setLedgerID(achVO.getLedgerID());
						totalDiscount.setAmount(invoiceVO.getTotalDiscount());
						if(compare>0){
							totalDiscount.setCreditDebitFlag("D");
						}else{
							totalDiscount.setCreditDebitFlag("C");
						}
					}
					
				}
				if(totalDiscount.getLedgerID()!=0){
					gstList.add(totalDiscount);
				}
				
			}
			
			//round off ledger
			if(invoiceVO.getTotalRoundUpAmount().compareTo(BigDecimal.ZERO)!=0){
				LedgerEntries roundupAmount=new LedgerEntries();
				int compare=invoiceVO.getTotalRoundUpAmount().compareTo(BigDecimal.ZERO);				
								
				for(int i=0;taxLedgersList.size()>i;i++){
					AccountHeadVO achVO=(AccountHeadVO) taxLedgersList.get(i);
					
					if((achVO.getAdditionalProps().contains("#roundOffAmount")&&((invoiceVO.getInvoiceType().equalsIgnoreCase("Sales")) ||(invoiceVO.getInvoiceType().equalsIgnoreCase("Journal"))))){
						roundupAmount.setLedgerID(achVO.getLedgerID());
						roundupAmount.setAmount(invoiceVO.getTotalRoundUpAmount());
						if(compare>0){
							roundupAmount.setCreditDebitFlag("C");
						}else {
							roundupAmount.setCreditDebitFlag("D");
						}
					}else if((achVO.getAdditionalProps().contains("#roundOffAmount"))&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Purchase"))){
						roundupAmount.setLedgerID(achVO.getLedgerID());
						roundupAmount.setAmount(invoiceVO.getTotalRoundUpAmount());
						if(compare>0){
							roundupAmount.setCreditDebitFlag("D");
						}else {
							roundupAmount.setCreditDebitFlag("C");
						}
					}else if((achVO.getAdditionalProps().contains("#roundOffAmount"))&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note"))){
						roundupAmount.setLedgerID(achVO.getLedgerID());
						roundupAmount.setAmount(invoiceVO.getTotalRoundUpAmount());
						if(compare>0){
							roundupAmount.setCreditDebitFlag("D");
						}else {
							roundupAmount.setCreditDebitFlag("C");
						}
					}else if((achVO.getAdditionalProps().contains("#roundOffAmount"))&&(invoiceVO.getInvoiceType().equalsIgnoreCase("Debit Note"))){
						roundupAmount.setLedgerID(achVO.getLedgerID());
						roundupAmount.setAmount(invoiceVO.getTotalRoundUpAmount());
						if(compare>0){
							roundupAmount.setCreditDebitFlag("C");
						}else {
							roundupAmount.setCreditDebitFlag("D");
						}
					}
					
				}
				if(roundupAmount.getLedgerID()!=0){
					gstList.add(roundupAmount);
				}
				
			}
			
			
			//Reverse charge GST
			if((invoiceVO.getTotalGST().compareTo(BigDecimal.ZERO)!=0 )&&(invoiceVO.getIsReverse()==1)){
				LedgerEntries reverseChargeGST=new LedgerEntries();
				int compare=invoiceVO.getTotalGST().compareTo(BigDecimal.ZERO);				
								
				for(int i=0;taxLedgersList.size()>i;i++){
					AccountHeadVO achVO=(AccountHeadVO) taxLedgersList.get(i);
					
					if((achVO.getAdditionalProps().contains("#reverseChargeGST")&&((invoiceVO.getInvoiceType().equalsIgnoreCase("Purchase"))))){
						reverseChargeGST.setLedgerID(achVO.getLedgerID());
						reverseChargeGST.setAmount(invoiceVO.getTotalGST());
						if(compare>0){
							reverseChargeGST.setCreditDebitFlag("C");
						}else {
							reverseChargeGST.setCreditDebitFlag("D");
						}
					}
				}
				if(reverseChargeGST.getLedgerID()!=0){
					gstList.add(reverseChargeGST);
				}
				
			}
		
			
			logger.debug("amount >"+ledgerEntry.getAmount()+" ledgerID -->"+ledgerEntry.getLedgerID()+"  "+ledgerEntry.getStrComments());
				
			
			
			logger.debug("Exit : private LedgerEntries convertInvoiceLineItemsTOLedgerEntries(InvoiceLineItemsVO invoiceVO)");
			return gstList;		
		}

		
		
		/* Convert Invoice VO to TxVO  */
		private LedgerEntries convertInvoiceTOLedgerEntries(InvoiceVO invoiceVO,int societyID){
			LedgerEntries ledgerEntry=new LedgerEntries();
			
			logger.debug("Entry : private LedgerEntries convertInvoiceLineItemsTOLedgerEntries(InvoiceLineItemsVO invoiceVO)");
			
			if(invoiceVO.getInvoiceType().equalsIgnoreCase("Sales")){
			
			ledgerEntry.setAmount(invoiceVO.getTotalAmount());
			
			if(ledgerEntry.getAmount().signum()>0){
				ledgerEntry.setCreditDebitFlag("D");
				}else ledgerEntry.setCreditDebitFlag("C");
			ledgerEntry.setLedgerID(invoiceVO.getLedgerID());
			}else if(invoiceVO.getInvoiceType().equalsIgnoreCase("Purchase")){
				//for reverse charge
				if((invoiceVO.getIsReverse()==1) && (invoiceVO.getTotalGST().compareTo(BigDecimal.ZERO)>0)){
					ledgerEntry.setAmount(invoiceVO.getTotalAmount().subtract(invoiceVO.getTotalGST()));
				}else{
					ledgerEntry.setAmount(invoiceVO.getTotalAmount());
				}
								
				if(ledgerEntry.getAmount().signum()>0){
					ledgerEntry.setCreditDebitFlag("C");
					}else ledgerEntry.setCreditDebitFlag("D");
				ledgerEntry.setLedgerID(invoiceVO.getLedgerID());
				
			}else if(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note")){
				ledgerEntry.setAmount(invoiceVO.getTotalAmount());
				if(ledgerEntry.getAmount().signum()>0){
				   ledgerEntry.setCreditDebitFlag("C");
				}else ledgerEntry.setCreditDebitFlag("D");
					ledgerEntry.setLedgerID(invoiceVO.getLedgerID());
					
					
			}else if(invoiceVO.getInvoiceType().equalsIgnoreCase("Debit Note")){
				ledgerEntry.setAmount(invoiceVO.getTotalAmount());
				if(ledgerEntry.getAmount().signum()>0){
				ledgerEntry.setCreditDebitFlag("D");
				}else ledgerEntry.setCreditDebitFlag("C");
				ledgerEntry.setLedgerID(invoiceVO.getLedgerID());
				
			}else if(invoiceVO.getInvoiceType().equalsIgnoreCase("Journal")){
				ledgerEntry.setAmount(invoiceVO.getTotalAmount());
				if(ledgerEntry.getAmount().signum()>0){
				ledgerEntry.setCreditDebitFlag("D");
				}else ledgerEntry.setCreditDebitFlag("C");
				ledgerEntry.setLedgerID(invoiceVO.getLedgerID());
				
			}
			
			logger.debug("amount >"+ledgerEntry.getAmount()+" ledgerID -->"+ledgerEntry.getLedgerID()+"  "+ledgerEntry.getStrComments());
				
			
			
			logger.debug("Exit : private LedgerEntries convertInvoiceLineItemsTOLedgerEntries(InvoiceLineItemsVO invoiceVO)");
			return ledgerEntry;		
		}
		
		
		public InvoiceVO deleteGSTInvoice(InvoiceVO invoiceVO){
			int deleteFlag=0;
			logger.debug("Entry : public int deleteGSTInvoice(InvoiceVO invoiceVO)");
			InvoiceVO validationVO=new InvoiceVO();
			InvoiceVO detailsVO=new InvoiceVO();
			try{
				detailsVO=getInvoiceDetails(invoiceVO);
				detailsVO.setUpdatedBy(invoiceVO.getUpdatedBy());
				detailsVO.setNotes(invoiceVO.getNotes());
				detailsVO.setOrgID(invoiceVO.getOrgID());
				validationVO=validateInvoice(detailsVO, invoiceVO.getOrgID());
				
				if(validationVO.getStatusCode()==200){
					
					deleteFlag=invoiceDAO.deleteGSTInvoice(invoiceVO);			
					
					if(deleteFlag==1){
					 int deleteTxFlag=transactionService.deleteJournalEntries(invoiceVO);
					}
					
					if(deleteFlag!=0){						
						invoiceVO.setStatusCode(200);
					}else{						
						invoiceVO.setStatusCode(406);
						invoiceVO.setStatusMessage("Unable to delete invoice");
					}
					
				}else{
					
					invoiceVO.setStatusCode(validationVO.getStatusCode());
					invoiceVO.setStatusMessage(validationVO.getStatusMessage());
					
				}				
				  
			}catch(Exception e){
				logger.error("Exception in deleteGSTInvoice "+e);
			}
			
			logger.debug("Exit : public int deleteGSTInvoice(InvoiceVO invoiceVO)");
			return invoiceVO;		
		}
		
		public InvoiceVO updateReportGroupInvoiceLineItem(InvoiceVO invoiceVO){
			int updateFlag=0;
			int updateTxFlag=0;
			logger.debug("Entry : public InvoiceVO updateReportGroupInvoiceLineItem(InvoiceVO invoiceVO)");
			try{
				
				updateFlag=invoiceDAO.updateReportGroupInvoiceLineItem(invoiceVO);
				
				if(updateFlag>0){
				
				  if((invoiceVO.getTransactionID().equalsIgnoreCase("0"))  ||  invoiceVO.getTransactionID()==null){
					  invoiceVO.setStatusCode(200);
				   }else{
						//update transaction line item
						TransactionVO transactionVO=new TransactionVO();
						transactionVO.setInvLineItemID(invoiceVO.getInvLineItemID());
						transactionVO.setTxTags(invoiceVO.getInvoiceTags());
						transactionVO.setTxLineItemID(0);
						transactionVO.setSocietyID(invoiceVO.getOrgID());
						
						transactionVO=transactionService.updateReportGroupJournalEntry(transactionVO);
							
							
							if(transactionVO.getStatusCode()==200){						
								invoiceVO.setStatusCode(200);
							}else{						
								invoiceVO.setStatusCode(406);
								invoiceVO.setStatusMessage("Unable to update report group");
							}
				   }
				  
				}else{
					
					invoiceVO.setStatusCode(406);
					invoiceVO.setStatusMessage("Unable to update report group");
				}
				  
			}catch(Exception e){
				logger.error("Exception in updateReportGroupInvoiceLineItem "+e);
			}
			
			logger.debug("Exit : public InvoiceVO updateReportGroupInvoiceLineItem(InvoiceVO invoiceVO)");
			return invoiceVO;		
		}
		
		/* Used for changing statuses of invoices i.e. adding is_deleted flag */
		public int changeInvoiceStatus(InvoiceVO invoiceVO) {
			int success=0;
			logger.debug("Entry : public int changeInvoiceStatus(InvoiceVO invoiceVO) ");
			try {
				logger.debug("Here invoice ID is "+invoiceVO.getInvoiceID()+" "+invoiceVO.getOrgID());
				
				
				success=invoiceDAO.changeInvoiceStatus(invoiceVO);
				
				
			} catch (Exception e) {
				logger.error("Exception occured in  public int changeInvoiceStatus(InvoiceVO invoiceVO) "+e);
			}
			
			logger.debug("Exit :  public int changeInvoiceStatus(InvoiceVO invoiceVO)");
			return success;
		}
		
		
		//====== Fetch TDS calculatiin Report =====//
		public List getTDSCalculationReport(TransactionVO txVO){
			List tdsTxList=null;
			
			logger.debug("Entry : public List getTDSCalculationReport(TransactionVO txVO)");
		
			try{
				
			tdsTxList=invoiceDAO.getTDSCalculationReport(txVO);
			
			}catch(Exception e){
				logger.error("Exception in public List getTDSCalculationReport(TransactionVO txVO) "+e);
			}
			logger.debug("Exit : public List getTDSCalculationReport(TransactionVO txVO)"+tdsTxList.size());
			return tdsTxList;		
		}
		
		//====== Fetch TDS calculation Report on invoice based =====//
		public List getTDSCalculationRptOnInvoices(InvoiceVO invVO){
			List tdsTxList=null;
			
			logger.debug("Entry : public List getTDSCalculationRptOnInvoices(TransactionVO txVO)");
		
			try{
				
			tdsTxList=invoiceDAO.getTDSCalculationRptOnInvoices(invVO);
			
			}catch(Exception e){
				logger.error("Exception in public List getTDSCalculationRptOnInvoices(TransactionVO txVO) "+e);
			}
			logger.debug("Exit : public List getTDSCalculationRptOnInvoices(TransactionVO txVO)"+tdsTxList.size());
			return tdsTxList;		
		}
		
		
		/* Get list of all gst invoices list for org  */
		public List getGSTInvoicesForOrg(InvoiceVO invoiceVO){
			List invoiceList=null;
			logger.debug("Entry : public List getGSTInvoicesForOrg(InvoiceVO invoiceVO)");
			try{
				
			invoiceList=invoiceDAO.getGSTInvoicesForOrg(invoiceVO);
			
					
		    }catch(Exception e){
			 logger.error("Exception in getGSTInvoicesForOrg "+e);
	        }				
			
			logger.debug("Exit : public List getGSTInvoicesForOrg(InvoiceVO invoiceVO)");
			return invoiceList;		
		}
		
		/* Get list of all gst invoices list for org  */
		public List getQuickGSTInvoicesForOrg(InvoiceVO invoiceVO){
			List invoiceList=null;
			logger.debug("Entry : public List getQuickGSTInvoicesForOrg(InvoiceVO invoiceVO)");
			try{
				
			invoiceList=invoiceDAO.getQuickGSTInvoicesForOrg(invoiceVO);
			
					
		    }catch(Exception e){
			 logger.error("Exception in getQuickGSTInvoicesForOrg "+e);
	        }				
			
			logger.debug("Exit : public List getQuickGSTInvoicesForOrg(InvoiceVO invoiceVO)");
			return invoiceList;		
		}
		
		/* Get gst invoice list like day book   */
		public List getGSTInvoicesListDayBook(InvoiceVO invoiceVO){
			List invoiceList=null;
			logger.debug("Entry : public List getGSTInvoicesListDayBook(InvoiceVO invoiceVO)");
			try{
				
			invoiceList=invoiceDAO.getGSTInvoicesListDayBook(invoiceVO);
			
		    }catch(Exception e){
			 logger.error("Exception in getGSTInvoicesListDayBook "+e);
	        }				
			
			logger.debug("Exit : public List getGSTInvoicesListDayBook(InvoiceVO invoiceVO)");
			return invoiceList;		
		}
		
		public List getGSTChapterList(){
			List chapterList=null;
			logger.debug("Entry : public List getGSTChapterList()");
			try{
				
			chapterList=invoiceDAO.getGSTChapterList();
			
		    }catch(Exception e){
			 logger.error("Exception in getGSTChapterList "+e);
	        }
							
			logger.debug("Exit : public List getGSTChapterList()");
			return chapterList;		
		}
		
		public List getHsnSacList(int chapterID){
			List hsnSacList=null;
			logger.debug("Entry :  public List getHsnSacList(int chapterID)");
			try{
				
			hsnSacList=invoiceDAO.getHsnSacList(chapterID);
			
		    }catch(Exception e){
			 logger.error("Exception in getHsnSacList "+e);
	        }
							
			logger.debug("Exit :  public List getHsnSacList(int chapterID)");
			return hsnSacList;		
		}
		
		 public List getProductServiceLedgerRelationList(int orgID){
			List hsnSacList=null;
			logger.debug("Entry : public List getProductServiceLedgerRelationList(int orgID)");
			try{	
				
			hsnSacList=invoiceDAO.getProductServiceLedgerRelationList(orgID);
			
			}catch(Exception e){
				logger.error("Exception in getProductServiceLedgerRelationList "+e);
		    }
								
			logger.debug("Exit : public List getProductServiceLedgerRelationList(int orgID)");
			return hsnSacList;		
		}
		
		 public ItemDetailsVO getProductServiceLedgerDetails(int itemID,int orgID){
				logger.debug("Entry : public List getProductServiceLedgerDetails(int itemID)");
				
				ItemDetailsVO hsnSacVO=invoiceDAO.getProductServiceLedgerDetails(itemID,orgID);
								
				logger.debug("Exit : public List getProductServiceLedgerDetails(int itemID)");
				return hsnSacVO;		
			}
		 
		 
		 
		 /* insert product service  */
		public int insertProductService(ItemDetailsVO itemDetailsVO){
		 int success=0;
		 int hsnSacID=0;
		 logger.debug("Entry : public int insertProductService(ItemDetailsVO itemDetailsVO)");
		 try{		
			
			 if(itemDetailsVO.getHsnsacCode().length()==0){
				 success=invoiceDAO.insertProductService(itemDetailsVO);
			 }else{
			   hsnSacID=invoiceDAO.validateHsnSacNo(itemDetailsVO.getHsnsacCode());
			   logger.debug("hsnSacID "+hsnSacID);
			   if(hsnSacID>0){
				   itemDetailsVO.setHsnSacID(""+hsnSacID);
				   success=invoiceDAO.insertProductService(itemDetailsVO);
			   }else{
				   //invalid hsn /sac code
				   success=2;				   
			   }
			   
			 }
		   		  
		 }catch(Exception e){
				logger.error("Exception in insertProductService "+e);
		 }
				
		 logger.debug("Exit : public int insertProductService(ItemDetailsVO itemDetailsVO)");
			return success;		
		}
		
		public int updateProductService(ItemDetailsVO itemDetailsVO){
			 int success=0;
			 int hsnSacID=0;
			 logger.debug("Entry : public int updateProductService(ItemDetailsVO itemDetailsVO)");
			 try{	
				 
				 if(itemDetailsVO.getHsnsacCode().length()==0){
					 success=invoiceDAO.updateProductService(itemDetailsVO);
				 }else{
				   hsnSacID=invoiceDAO.validateHsnSacNo(itemDetailsVO.getHsnsacCode());
					   logger.debug("hsnSacID "+hsnSacID);
					   if(hsnSacID>0){
						   itemDetailsVO.setHsnSacID(""+hsnSacID);
						   success=invoiceDAO.updateProductService(itemDetailsVO);
					   }else{
						   //invalid hsn /sac code
						   success=2;				   
					   }				   		   
				 }
			
			  
			 }catch(Exception e){
				logger.error("Exception in updateProductService "+e);
			 }
					
			 logger.debug("Exit : public int updateProductService(ItemDetailsVO itemDetailsVO)");
				return success;		
		}
		
		public int deleteProductService(ItemDetailsVO itemDetailsVO){
			int deleteFlag=0;
			Boolean isPresent=false;
			logger.debug("Entry : public int deleteProductService(ItemDetailsVO itemDetailsVO)");
			try{
				if(itemDetailsVO.getIsActive()==2){				
				   isPresent=invoiceDAO.checkProuductServicelinkToInvoice(itemDetailsVO.getItemID());	
				}
				
				if(isPresent==false){
					deleteFlag=invoiceDAO.deleteProductService(itemDetailsVO);
				}else{
					deleteFlag=2;
				}
				
								  
			}catch(Exception e){
				logger.error("Exception in deleteProductService "+e);
			}
			
			logger.debug("Exit : public int deleteProductService(ItemDetailsVO itemDetailsVO)");
			return deleteFlag;		
		}
		
		//====== Generate Scheduled Invoices ================//
		public List generateScheduledInvoices(int societyID,int billingCycleID){
			List invoiceList=new ArrayList();
			
			try {
				logger.debug("Entry : public List generateScheduledInvoices(int societyID,int billingCycleID)");
				
				InvoiceBillingCycleVO billingCycleVO=new InvoiceBillingCycleVO();
				List chargeTypeList=transactionService.getChargeTypeList(societyID);
				if(billingCycleID==0){
				List billingCycleList=invoiceDAO.getBillingCycleList(societyID);
				billingCycleVO=(InvoiceBillingCycleVO) billingCycleList.get(billingCycleList.size()-1);
				}else billingCycleVO=invoiceDAO.getBillingCycleDetails(billingCycleID);
				List memberList=memberServiceBean.getActivePrimaryMemberList(societyID);
				for(int j=0;chargeTypeList.size()>j;j++){
					
				ChargesVO chargeTypeVO=(ChargesVO) chargeTypeList.get(j);
				if(chargeTypeVO.getChargeTypeID()!=10){
				
				for(int i=0;memberList.size()>i;i++){
					MemberVO memberVO=(MemberVO) memberList.get(i);
					
					InvoiceVO invForMember=prepareInvoice(societyID, memberVO, chargeTypeVO.getChargeTypeID(),billingCycleVO);
					
					if(invForMember.getInvoiceAmount().compareTo(BigDecimal.ZERO)!=0){
					//txForMember=insertTransaction(txForMember, societyID);
						
					logger.info(i+"---- Invoice Ledger ID "+invForMember.getEntityID()+" Amount "+invForMember.getInvoiceAmount());
					
					invoiceList.add(invForMember);
					}
					
				}
				}
				}
				
				
				logger.debug("Exit : public List generateScheduledInvoices(int societyID,int billingCycleID)");
			} catch (Exception e) {
				logger.error("Exception :public List generateScheduledInvoices(int societyID,int billingCycleID)"+e );
			}
			return invoiceList;
			
		}
		
		
		public List generateScheduledLateFeeInvoices(int societyID,int billingCycleID){
			List invoiceList=new ArrayList();
			
			try {
				logger.debug("Entry :  public List generateScheduledLateFeeInvoices(int societyID,int billingCycleID)");
				
				InvoiceBillingCycleVO billingCycleVO=new InvoiceBillingCycleVO();
				
				if(billingCycleID==0){
				List billingCycleList=invoiceDAO.getBillingCycleList(societyID);
				billingCycleVO=(InvoiceBillingCycleVO) billingCycleList.get(billingCycleList.size()-1);
				}else
					billingCycleVO=invoiceDAO.getBillingCycleDetails(billingCycleID);
				List memberList=transactionService.getActiveMemberListAll(societyID);
				int chargeTypeID=10;
				
				for(int i=0;memberList.size()>i;i++){
					MemberVO memberVO=(MemberVO) memberList.get(i);
					
					InvoiceVO txForMember=calculateLateFeeTransaction(societyID, memberVO, chargeTypeID, billingCycleVO);
					
					if(txForMember.getInvoiceAmount().compareTo(BigDecimal.ZERO)!=0){
					//txForMember=insertTransaction(txForMember, societyID);
					
					invoiceList.add(txForMember);
					}
				}
				
				
				logger.debug("Exit :  public List generateScheduledLateFeeTransactions(int societyID,int billingCycleID)");
			} catch (Exception e) {
				logger.error("Exception : public List generateScheduledLateFeeTransactions(int societyID,int billingCycleID)"+e );
			}
			return invoiceList;
			
		}

	
		/* Create transactions for member  */
		public InvoiceVO prepareInvoice(int societyID,MemberVO memberVO,int chargeTypeID,InvoiceBillingCycleVO billingCycleVO){
			InvoiceVO invoiceVO=new InvoiceVO();
			BigDecimal grandTotal=BigDecimal.ZERO;
			String txDate="";
			String invMode="";
			List lineItems=new ArrayList();
			ChargesVO roundOffVO=new ChargesVO();
			CommonUtility commonUtils=new CommonUtility();
			roundOffVO.setIsApplicable(false);
			
			logger.debug("Entry : public InvoiceVO prepareInvoice(int societyID,MemberVO memberVO,int chargeTypeID,InvoiceBillingCycleVO billingCycleVO)");
			
			try{
				SocietyVO societyVO=societyService.getSocietyDetails(societyID);
				invoiceVO.setLedgerID(memberVO.getLedgerID());
				invoiceVO.setEntityID(memberVO.getLedgerID());
				List chargesList=transactionService.getApplicableCharges(societyID, chargeTypeID);
			
			if(chargesList.size()>0){
				

				for(int i=0;chargesList.size()>i;i++){
					
					
					ChargesVO chargeVO=(ChargesVO) chargesList.get(i);
					if(chargeVO.getItemID()>0){
					ItemDetailsVO itemVO=invoiceDAO.getProductServiceLedgerDetails(chargeVO.getItemID(), societyID);
					chargeVO.setHsnsacCode(itemVO.getHsnsacCode());
					chargeVO.setGstRate(itemVO.getGst());
					chargeVO.setItemName(itemVO.getItemName());
					chargeVO.setPurchasePrice(itemVO.getPurchasePrice());
					chargeVO.setSellingPrice(chargeVO.getSellingPrice());
					chargeVO.setDescription(itemVO.getDescription());
					chargeVO.setGstCategoryID(itemVO.getGstCategoryID());
					}
					chargeVO=transactionService.getConditionalBalance(chargeVO, societyID,memberVO);
					
					if(chargeVO.getCalMethod().equalsIgnoreCase("Rnd")){
						roundOffVO=chargeVO;
						chargesList.remove(i);
						--i;
					}
					
					if((chargeVO.getIsApplicable())&&(chargeVO.getChargeTypeID()!=10)&&(chargeTypeID!=10)){
						grandTotal=grandTotal.add(chargeVO.getAmount()).setScale(2,2);
						
					if((chargeVO.getChargeTypeID()==1)||(chargeVO.getChargeTypeID()==10)||(chargeVO.getChargeTypeID()==2) ){
						invMode="R";
					}else invMode="O";
				
					if(!chargeVO.getCalMethod().equalsIgnoreCase("Rnd")){
					txDate=chargeVO.getTxDate();
					}
					invoiceVO.setInvoiceTypeID(chargeVO.getChargeTypeID());
					
					invoiceVO.setGstInvoiceTypeID(1);	
					invoiceVO.setCurrencyID(1);	
					invoiceVO.setCurrencyRate(new BigDecimal(1));
			
					invoiceVO.setBillCycleID(billingCycleVO.getBillingCycleID());
					lineItems=chargesList;
					
					}else{
						chargesList.remove(i);
						--i;
					}
					
					
					
					
				}
			}
			
			
			if(memberVO.getTenantFeeOnFullAmt()!=0){
				if(memberVO.getIsRented()==1){
				ChargesVO tenantCharge=new ChargesVO();
				tenantCharge.setCreditLedgerID(memberVO.getTenantLedgerID());
				BigDecimal tenantFee=grandTotal.divide(new BigDecimal(10)).setScale(2,2);
				tenantCharge.setAmount(tenantFee);
				tenantCharge.setCalMethod("SR");
				tenantCharge.setGroupTypeID("0");
				lineItems.add(tenantCharge);
				grandTotal=grandTotal.add(tenantFee);
				
				}
			}
			if(roundOffVO.getIsApplicable()){
				BigDecimal roundedTotal=grandTotal;
				BigDecimal roundedAmt=BigDecimal.ZERO;
				if(roundOffVO.getGroupTypeID().equals("1")){
				roundedTotal=grandTotal.setScale(0,RoundingMode.UP);
				roundedAmt=roundedTotal.subtract(grandTotal);
				roundOffVO.setAmount(roundedAmt);
				invoiceVO.setTotalRoundUpAmount(roundedAmt);
				}else 	if(roundOffVO.getGroupTypeID().equals("0")){
					roundedTotal=grandTotal.setScale(0,RoundingMode.DOWN);
					roundedAmt=roundedTotal.subtract(grandTotal);
					roundOffVO.setAmount(roundedAmt);
					invoiceVO.setTotalRoundUpAmount(roundedAmt);
							}else 	if(roundOffVO.getGroupTypeID().equals("-1")){
								roundedTotal=grandTotal.setScale(-1,RoundingMode.HALF_UP);
								roundedAmt=roundedTotal.subtract(grandTotal);
								roundOffVO.setAmount(roundedAmt);
								invoiceVO.setTotalRoundUpAmount(roundedAmt);
										}
			
				
				grandTotal=grandTotal.add(roundedAmt);
				
				
				}
			
			
			invoiceVO.setEntityType("M");
			invoiceVO.setInvoiceAmount(grandTotal);
			invoiceVO.setTotalAmount(grandTotal);
		
		
				TransactionMetaVO txMeta=new TransactionMetaVO();
				txMeta.setInvoiceTypeID(chargeTypeID);
				txMeta.setLedgerID(memberVO.getLedgerID());
				txMeta.setInvoiceDate(txDate);
				txMeta.setInvoiceMode(invMode);
		//		txVO.setAdditionalProps(txMeta);
				
			if((societyVO.getGstIN()!=null)&&(societyVO.getGstIN().length()>0)&&(commonUtils.validGSTIN(societyVO.getGstIN()))){
				invoiceVO.setIsSales(1);
			}
			invoiceVO.setMyGSTIN(societyVO.getGstIN());
			invoiceVO.setSupplyState(societyVO.getStateName());
			if(societyVO.getStateName()!=null)
			invoiceVO.setSupplyStateID(societyVO.getGstIN().substring(0,2));
			invoiceVO.setOrgID(societyID);
			invoiceVO.setInvoiceType("Sales");
			invoiceVO.setInvoiceDate(txDate);
			invoiceVO.setGstInvoiceTypeID(1);
			invoiceVO.setNotes("For the period of "+dateUtil.getConvetedDate(billingCycleVO.getFromDate())+" to "+dateUtil.getConvetedDate(billingCycleVO.getUptoDate()));
			invoiceVO=convertChargesIntoLineItemsObj(invoiceVO, lineItems);
			
			}catch (Exception e) {
				logger.error("Exception in prepareInvoice for ledger ID - "+memberVO.getLedgerID()+" and orgID "+societyID+" "+e);
			}
			
			logger.debug("Exit : public InvoiceVO prepareInvoice(int societyID,MemberVO memberVO,int chargeTypeID,InvoiceBillingCycleVO billingCycleVO)");
			return invoiceVO;		
		}
		
		/* Create late fee transactions for member  */
		public InvoiceVO calculateLateFeeTransaction(int societyID,MemberVO memberVO,int chargeTypeID,InvoiceBillingCycleVO billingCycleVO){
			InvoiceVO txVO=new InvoiceVO();
			String description="";
			BigDecimal grandTotal=BigDecimal.ZERO;
			String penaltyTxDate="";
			List lineItems=new ArrayList();
			CommonUtility commonUtils=new CommonUtility();
			String invMode="";
			
			logger.debug("Entry : public TransactionVO calculateLateFeeTransaction(int societyID,MemberVO memberVO,int chargeTypeID)");
			
			try{
				SocietyVO societyVO=societyService.getSocietyDetails(societyID);
				txVO.setLedgerID(memberVO.getLedgerID());
				
				List chargesList=transactionService.getApplicablePenaltyCharges(societyID);
			
			if(chargesList.size()>0){
				

				for(int i=0;chargesList.size()>i;i++){
					ItemDetailsVO itemVO=new ItemDetailsVO();
					
					PenaltyChargesVO chargeVO=(PenaltyChargesVO) chargesList.get(i);
					
					 int dateDiff=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(chargeVO.getPenaltyChargeDate()),dateUtil.convertStringDateToSql(billingCycleVO.getDueDate()) );

					    if(dateDiff>=0){
					 	
						TransactionVO lateTxVO=new TransactionVO();
						
						txVO.setInvoiceTypeID(chargeVO.getChargeTypeID());
						
						penaltyTxDate=chargeVO.getTxDate();
						if(chargeVO.getItemID()>0){
							 itemVO=invoiceDAO.getProductServiceLedgerDetails(chargeVO.getItemID(), societyID);
							chargeVO.setHsnsacCode(itemVO.getHsnsacCode());
							chargeVO.setGstRate(itemVO.getGst());
							chargeVO.setItemName(itemVO.getItemName());
							chargeVO.setPurchasePrice(itemVO.getPurchasePrice());
							chargeVO.setSellingPrice(chargeVO.getSellingPrice());
							chargeVO.setDescription(itemVO.getDescription());
							chargeVO.setGstCategoryID(itemVO.getGstCategoryID());
							}
											
						lateTxVO=calculateLateFeesForTransaction.getLateFee(chargeVO, societyID, memberVO, dateUtil.getPrevOrNextDate(chargeVO.getPrevChargeDate(), 0), chargeVO.getPenaltyChargeDate(),dateUtil.getPrevOrNextDate(billingCycleVO.getDueDate(), 0),billingCycleVO);
						
						if(lateTxVO.getAmount()!=null){
						grandTotal=grandTotal.add(lateTxVO.getAmount());
						
						description=lateTxVO.getDescription();
						}
						
						if((chargeVO.getChargeTypeID()==1)||(chargeVO.getChargeTypeID()==10)||(chargeVO.getChargeTypeID()==2) ){
							invMode="R";
						}else invMode="O";
						txVO.setInvoiceTypeID(chargeVO.getChargeTypeID());
						txVO.setBillCycleID(billingCycleVO.getBillingCycleID());
						
					
					if(grandTotal.compareTo(BigDecimal.ZERO)>0){
					chargeVO.setIsApplicable(true);
					chargeVO.setAmount(grandTotal);
					chargeVO.setAmount(grandTotal);
					chargeVO.setDescription(itemVO.getDescription());
					chargeVO.setItemName(itemVO.getItemName());
					chargeVO=getGSTCalculationForScheduledCharge(chargeVO, memberVO);
					chargeVO.setPurchasePrice(BigDecimal.ZERO);
					chargeVO.setSellingPrice(BigDecimal.ZERO);
					lineItems.add(chargeVO);
					}
					
				
					
					    }
					
					
				}
			}
			
		
			txVO.setInvoiceAmount(grandTotal);
			txVO.setTotalAmount(grandTotal);
			txVO.setEntityType("M");
			txVO.setOrgID(societyID);
			
						
			if((societyVO.getGstIN()!=null)&&(societyVO.getGstIN().length()>0)&&(commonUtils.validGSTIN(societyVO.getGstIN()))){
				txVO.setIsSales(1);
			}
			txVO.setMyGSTIN(societyVO.getGstIN());
			txVO.setSupplyState(societyVO.getStateName());
			if(societyVO.getStateName()!=null)
			txVO.setSupplyStateID(societyVO.getGstIN().substring(0,2));
			txVO.setInvoiceType("Sales");
			txVO.setInvoiceDate(penaltyTxDate);
			txVO.setGstInvoiceTypeID(1);
			txVO=convertPenaltyChargesIntoInvoiceObj(txVO, lineItems);
			txVO.setLedgerID(memberVO.getLedgerID());
			txVO.setEntityID(memberVO.getLedgerID());
			txVO.setInvoiceTypeID(10);
			if(description.length()<1){
				description="For the period of "+dateUtil.getConvetedDate(billingCycleVO.getFromDate())+" to "+dateUtil.getConvetedDate(billingCycleVO.getUptoDate());
			}
			txVO.setNotes(description);
			}catch (Exception e) {
				logger.error("Exception in calculateLateFeeTransaction for ledger ID - "+memberVO.getLedgerID()+" and orgID "+societyID+" "+e);
			}
			
			logger.debug("Exit : public TransactionVO calculateLateFeeTransaction(int societyID,MemberVO memberVO,int chargeTypeID)");
			return txVO;		
		}
		
		
		public InvoiceVO convertChargesIntoLineItemsObj(InvoiceVO invVO,List chargeList){
			
			List lineItems=new ArrayList();
			
			logger.debug("Entry :  private InvoiceVO convertChargesIntoLineItemsObj(InvoiceVO invVO,List chargeList) ");
			
			try {
				if(chargeList.size()>0){
					for(int i=0;chargeList.size()>i;i++){
						ChargesVO chargeVO=(ChargesVO) chargeList.get(i);
						InLineItemsVO tempLdgrEnty=new InLineItemsVO();
						BigDecimal gst=chargeVO.getcGstAmount().add(chargeVO.getiGstAmount());
						gst=gst.add(chargeVO.getsGstAmount());
						gst=gst.add(chargeVO.getuGstAmount());
						tempLdgrEnty.setLedgerID(chargeVO.getCreditLedgerID());
						tempLdgrEnty.setAmount(chargeVO.getAmount().add(gst));
						tempLdgrEnty.setTotalPrice(chargeVO.getAmount());
						tempLdgrEnty.setPerUnitPrice(chargeVO.getAmount());
						tempLdgrEnty.setQuantity(new BigDecimal(1));
						tempLdgrEnty.setGstCategoryID(chargeVO.getGstCategoryID());
						tempLdgrEnty.setHsnSacCode(chargeVO.getHsnsacCode());
						tempLdgrEnty.setDescription(chargeVO.getDescription());
						tempLdgrEnty.setChargeID(chargeVO.getChargeID());
						tempLdgrEnty.setItemID(""+chargeVO.getItemID());
						tempLdgrEnty.setItemName(chargeVO.getItemName());
						tempLdgrEnty.setcGST(chargeVO.getcGstAmount());
						tempLdgrEnty.setiGST(chargeVO.getiGstAmount());
						tempLdgrEnty.setsGST(chargeVO.getsGstAmount());
						tempLdgrEnty.setuGST(chargeVO.getuGstAmount());
						tempLdgrEnty.setPurchasePrice(BigDecimal.ZERO);
						invVO.setTotalCGST(invVO.getTotalCGST().add(chargeVO.getcGstAmount()));
						invVO.setTotalIGST(invVO.getTotalIGST().add(chargeVO.getiGstAmount()));
						invVO.setTotalSGST(invVO.getTotalSGST().add(chargeVO.getsGstAmount()));
						invVO.setTotalUGST(invVO.getTotalUGST().add(chargeVO.getuGstAmount()));
						invVO.setTotalAmount(invVO.getTotalAmount().add(gst));
					
						lineItems.add(tempLdgrEnty);
						
					}
					
					invVO.setLineItemsList(lineItems);
									
				}
			} catch (Exception e) {
				logger.debug("Exception :  private InvoiceVO convertChargesIntoLineItemsObj(InvoiceVO invVO,List chargeList)");
			}
				
			logger.debug("Exit :  private InvoiceVO convertChargesIntoLineItemsObj(InvoiceVO invVO,List chargeList)");
			return invVO;
			
		}
		
		private InvoiceVO convertPenaltyChargesIntoInvoiceObj(InvoiceVO invVO,List penaltyCharges){
			
			List lineItems=new ArrayList();
			LedgerEntries revLedgerEntry=new LedgerEntries();
			logger.debug("Entry : private List convertChargesIntoLedgerEntries(List chargeList) ");
			
			try {
				if(penaltyCharges.size()>0){
					for(int i=0;penaltyCharges.size()>i;i++){
						PenaltyChargesVO chargeVO=(PenaltyChargesVO) penaltyCharges.get(i);
						InLineItemsVO tempLdgrEnty=new InLineItemsVO();
						BigDecimal gst=chargeVO.getcGstAmount().add(chargeVO.getiGstAmount());
						gst=gst.add(chargeVO.getsGstAmount());
						gst=gst.add(chargeVO.getuGstAmount());
						tempLdgrEnty.setLedgerID(chargeVO.getLedgerID());
						tempLdgrEnty.setAmount(chargeVO.getAmount().add(gst));
						tempLdgrEnty.setTotalPrice(chargeVO.getAmount());
						tempLdgrEnty.setPerUnitPrice(chargeVO.getAmount());
						tempLdgrEnty.setQuantity(new BigDecimal(1));
						tempLdgrEnty.setGstCategoryID(chargeVO.getGstCategoryID());
						tempLdgrEnty.setHsnSacCode(chargeVO.getHsnsacCode());
						tempLdgrEnty.setDescription(chargeVO.getDescription());
						tempLdgrEnty.setChargeID(chargeVO.getPenaltyChargeID());
						tempLdgrEnty.setItemID(""+chargeVO.getItemID());
						tempLdgrEnty.setItemName(chargeVO.getItemName());
						tempLdgrEnty.setcGST(chargeVO.getcGstAmount());
						tempLdgrEnty.setiGST(chargeVO.getiGstAmount());
						tempLdgrEnty.setsGST(chargeVO.getsGstAmount());
						tempLdgrEnty.setuGST(chargeVO.getuGstAmount());
						tempLdgrEnty.setPurchasePrice(BigDecimal.ZERO);
						invVO.setTotalCGST(invVO.getTotalCGST().add(chargeVO.getcGstAmount()));
						invVO.setTotalIGST(invVO.getTotalIGST().add(chargeVO.getiGstAmount()));
						invVO.setTotalSGST(invVO.getTotalSGST().add(chargeVO.getsGstAmount()));
						invVO.setTotalUGST(invVO.getTotalUGST().add(chargeVO.getuGstAmount()));
						invVO.setTotalAmount(invVO.getTotalAmount().add(gst));
					
						lineItems.add(tempLdgrEnty);
						
					}
					
					invVO.setLineItemsList(lineItems);
									
				}
			} catch (Exception e) {
				logger.debug("Exception : private List convertChargesIntoLedgerEntries(List chargeList) ");
			}
			
			
			logger.debug("Exit : private List convertChargesIntoLedgerEntries(List chargeList) ");
			return invVO;
			
		}
		
		
		/* Add bulk transactions  */
		public ScheduledTransactionResponseVO addBulkInvoices(List invoiceList,int societyID){
			logger.debug("Entry :  public ScheduledTransactionResponseVO addBulkInvoices(List invoiceList,int societyID)");
			int success=0;
			CommonUtility commonUtil=new CommonUtility();
			ScheduledTransactionResponseVO responseVO=new ScheduledTransactionResponseVO();
			try {
				ArrayList<String> chargeList=new ArrayList<String>();
				ArrayList<String> unique=new ArrayList<String>();
				int successCount=0;
				int failureCount=0;
				BigDecimal totalInvoicedAmount=BigDecimal.ZERO;
				String failureMember="";
				for(int i=0;invoiceList.size()>i;i++){
					int insertFlag=0;
					List invList=new ArrayList();
					List memberChargeList=new ArrayList();
					InvoiceVO invVO=(InvoiceVO) invoiceList.get(i);
					
					memberChargeList=invVO.getLineItemsList();
					invVO=insertGSTInvoice(invVO);
					
					if(invVO.getInvoiceID()!=0){
						totalInvoicedAmount=totalInvoicedAmount.add((invVO.getInvoiceAmount()));
						for(int j=0;memberChargeList.size()>j;j++){
							InLineItemsVO ledgerEntry= (InLineItemsVO) memberChargeList.get(j);
							//insertFlag=updateChargeStatus( societyID, livo);
							chargeList.add(""+ledgerEntry.getChargeID());
							//responseVO.setFrequency(ledgerEntry.getChargeFrequency());
						}
						successCount=successCount+1;
					}	else{
						failureCount=failureCount+1;
						failureMember=failureMember+" ,"+invVO.getLedgerID();
					}
					
					
				}
				logger.debug(chargeList.size()+" Heere chargeList is "+ commonUtil.removeDuplicates(chargeList));
				responseVO.setSocietyID(societyID);
				responseVO.setChargeList(commonUtil.removeDuplicates(chargeList));
				responseVO.setSuccessCount(successCount);
				responseVO.setTotalTxAmount(totalInvoicedAmount);
				responseVO.setFailureCount(failureCount);
				responseVO.setFailureTxs(failureMember);
			} catch (Exception e) {
				logger.error("Exception in public addBulkInvoices "+e);
			}
			
			
			logger.debug("Exit :  public ScheduledTransactionResponseVO addBulkInvoices(List invoiceList,int societyID)");
			return responseVO;
		}
		
		public PenaltyChargesVO getGSTCalculationForScheduledCharge(PenaltyChargesVO chargeVO,MemberVO memberVO){
			
			if((chargeVO.getIsApplicable()&&(chargeVO.getSocGstIn()!=null)&&(chargeVO.getHsnsacCode()!=null))){
				if((memberVO.getMemberStateName()==null)||(memberVO.getSocietyStateName()==null)){
					BigDecimal cgst=BigDecimal.ZERO;
				BigDecimal sgst=BigDecimal.ZERO;
				BigDecimal gstRate=chargeVO.getGstRate().divide(new BigDecimal("2")).setScale(2);
				BigDecimal percentage=gstRate.multiply(new BigDecimal("0.01")).setScale(6);
				cgst=chargeVO.getAmount().multiply(percentage).setScale(2);
				sgst=chargeVO.getAmount().multiply(percentage).setScale(2);
				chargeVO.setcGstAmount(cgst);
				chargeVO.setcGstRate(gstRate);
				chargeVO.setsGstAmount(sgst);
				chargeVO.setsGstRate(gstRate);
			
			}else{
					if(memberVO.getMemberStateName().equalsIgnoreCase(memberVO.getSocietyStateName())){
					BigDecimal cgst=BigDecimal.ZERO;
					BigDecimal sgst=BigDecimal.ZERO;
					BigDecimal gstRate=chargeVO.getGstRate().divide(new BigDecimal("2")).setScale(2);
					BigDecimal percentage=gstRate.multiply(new BigDecimal("0.01")).setScale(6);
					cgst=chargeVO.getAmount().multiply(percentage).setScale(2);
					sgst=chargeVO.getAmount().multiply(percentage).setScale(2);
					chargeVO.setcGstAmount(cgst);
					chargeVO.setcGstRate(gstRate);
					chargeVO.setsGstAmount(sgst);
					chargeVO.setsGstRate(gstRate);
				}else{
				BigDecimal igst=BigDecimal.ZERO;
				BigDecimal gstRate=chargeVO.getGstRate();
				BigDecimal percentage=gstRate.multiply(new BigDecimal("0.01")).setScale(6);
				igst=chargeVO.getAmount().multiply(percentage).setScale(2);
				chargeVO.setiGstAmount(igst);
				chargeVO.setiGstRate(gstRate);
				}
			}
		}
			return chargeVO;
		}
		
		
		 /* Get Balance of invoice in given period for a ledger */
		public InvoiceVO getInvoiceBalanceInGivenPeriod(int ledgerID,int orgID,String fromDate,String toDate){
			logger.debug("public List getInvoiceListInGivenPeriod(int ledgerID,int orgID,String fromDate,String toDate)");
		InvoiceVO balanceInvoiceVO=new InvoiceVO();
		List invoiceList=new ArrayList<>();
		BigDecimal balance=BigDecimal.ZERO;
				
				
				invoiceList = invoiceDAO.getInvoiceBalanceInGivenPeriod(ledgerID, orgID, fromDate, toDate);
		
				if(invoiceList.size()>0){
					for(int i=0;invoiceList.size()>i;i++){
						InvoiceVO inv=(InvoiceVO) invoiceList.get(i);
						balance=balance.add(inv.getBalance());
						balanceInvoiceVO.setEntityID(inv.getEntityID());
					}
				}
				balanceInvoiceVO.setBalance(balance);
			logger.debug("Exit : public List getInvoiceListInGivenPeriod(int ledgerID,int orgID,String fromDate,String toDate)");
			
			return balanceInvoiceVO;
		}
		
		/* Get demand notice member balance  */
		public BillDetailsVO getDemandNoticeBalanceDetails(int societyID,String fromDate, String toDate,int aptID){
			BillDetailsVO billVO=new BillDetailsVO();
			logger.debug("Entry : 	public BillDetailsVO getDemandNoticeBalanceDetails(int societyID,String fromDate, String toDate,int aptID)");
			try{
			MemberVO memberVO=new MemberVO();
			List lineItemsList=new ArrayList();
			BigDecimal iGstTotal=BigDecimal.ZERO;
			BigDecimal cGstTotal=BigDecimal.ZERO;
			BigDecimal sGstTotal=BigDecimal.ZERO;
			BigDecimal uGstTotal=BigDecimal.ZERO;
			BigDecimal cessTotal=BigDecimal.ZERO;
			BigDecimal roundOff=BigDecimal.ZERO;
			BigDecimal discountTotal=BigDecimal.ZERO;
			int ledgerID;
			LedgerTrnsVO ledgerCredit=new LedgerTrnsVO();
            LedgerTrnsVO ledgerDebit=new LedgerTrnsVO();
            InvoiceVO inv=new InvoiceVO();
            
            memberVO=memberServiceBean.getMemberInfo(aptID);
            ledgerID=memberVO.getLedgerID();
                     
			 ledgerCredit=ledgerService.getBillTransactionList(societyID, ledgerID,fromDate,toDate,"C");
                        
              billVO.setOpeningBalance(ledgerCredit.getOpeningBalance());
              billVO.setClosingBalance(ledgerCredit.getClosingBalance());
              billVO.setChargedAmount(ledgerCredit.getCrOpnBalance());
              billVO.setPaidAmount(ledgerCredit.getDrOpnBalance());
              billVO.setRoundOff(roundOff);
              billVO.setLedgerName(ledgerCredit.getLedgerName());
              billVO.setLedgerID(ledgerID);
              
              if(billVO.getClosingBalance().compareTo(BigDecimal.ZERO)<=0){
                  billVO.setClosingBalanceInwords(amtInWrds.convertToAmt(billVO.getClosingBalance().negate().toString()));
                  billVO.setClosingBalanceInwords(" - "+billVO.getClosingBalanceInwords());
              }else
                 billVO.setClosingBalanceInwords(amtInWrds.convertToAmt(billVO.getClosingBalance().toString()));
              
			
			billVO.setFromDate(fromDate);
			billVO.setUptoDate(toDate);
			billVO.setSocietyName(memberVO.getSocietyName());
			billVO.setSocietyVO(societyService.getSocietyDetails(societyID));
			billVO.setMemberVO(memberVO);	
					
			
			}catch(Exception e){
				logger.error("Exception in 	public List getDemandNoticeBalanceDetails(int societyID,String fromDate, String toDate,int aptID) "+e);
			}
			logger.debug("Exit : 	public List getDemandNoticeBalanceDetails(int societyID,String fromDate, String toDate,int aptID)");
			return billVO;		
		}
		
		/* Get Currency list  */
		public List getCurrencyList(){
			List currencyList=null;
			logger.debug("Entry : public List getCurrencyList()");
			try{
			
				currencyList=invoiceDAO.getCurrencyList();
			
			}catch (Exception e) {
				logger.error("Exception in getCurrencyList "+e);
			}
			
			logger.debug("Exit : public List getCurrencyList()");
			return currencyList;		
		}
		
		
		
		public List getUnlinkedInvoiceList(int orgID,int ledgerID,String fromDate, String toDate){
			List invoiceInvoiceList=null;
			List invoiceList=new ArrayList<>();
		
				logger.debug("Entry :  public List getUnlinkedInvoiceList(int orgID,int ledgerID)");
				
				invoiceInvoiceList=invoiceDAO.getUnlinkedInvoiceList(orgID, ledgerID, fromDate, toDate);
				if(invoiceInvoiceList.size()>0){
				for(int i=0;invoiceInvoiceList.size()>i;i++){
					InvoiceVO txVO=(InvoiceVO) invoiceInvoiceList.get(i);
					if((txVO.getBalance().compareTo(BigDecimal.ZERO)>=0)){
						invoiceList.add(txVO);
					}
				}
				}
				
				
				logger.debug("Exit :  public List getUnlinkedInvoiceList(int orgID,int ledgerID)");
			return invoiceList;
			}
		
		
		public int linkInvoiceList(int orgID,List invoiceList){
			int successCount=0;
		
				logger.debug("Entry :  public int LinkInvoiceList(int orgID,List invoiceList)");
				
				
						if(invoiceList.size()>0){
							for(int i=0;invoiceList.size()>i;i++){
								InvoiceVO invoiceVO= (InvoiceVO) invoiceList.get(i);
								successCount=successCount+invoiceDAO.insertInvAgainstInvoice(orgID, invoiceVO);
								
							}
							
						}
						
					
				logger.debug("Exit : public int LinkInvoiceList(int orgID,List invoiceList)"+successCount);
			return successCount;
			}
		
		/* Unlink  invoice from invoice */
		public int unlinkInvoiceFromInvoice(int orgID ,InvoiceVO invoiceVO){
			int success=0;
			logger.debug("Entry : public int unlinkInvoiceFromInvoice(int orgID ,InvoiceVO invoiceVO)");
			  
				   success=invoiceDAO.unlinkLinkedInvoiceFromInvoice(orgID, invoiceVO);			
						
			logger.debug("Exit : public int unlinkInvoiceFromInvoice(int orgID ,InvoiceVO invoiceVO)");
			return success;		
		}
		
		
		//=============== Age wise receivables and payable reports ================//
		
		
		public List getAgewiseReceivalesList(int orgID, int isDueDate){
			List receivableList=null;
		
				logger.debug("Entry :  public List getAgewiseReceivalesList(int orgID, int isDueDate)");
		
				receivableList=invoiceDAO.getAgewiseReceivalesList(orgID, isDueDate);
				
				logger.debug("Exit : public List getAgewiseReceivalesList(int orgID, int isDueDate)");
				return receivableList;
		}
		
		
		public List getAgewisePayablesList(int orgID, int isDueDate){
			List payableList=null;
		
				logger.debug("Entry :  public List getAgewisePayablesList(int orgID, int isDueDate)");
	
				payableList=invoiceDAO.getAgewisePayablesList(orgID, isDueDate);
				
				logger.debug("Exit : public List getAgewisePayablesList(int orgID, int isDueDate)");
				return payableList;
		}		
		
		public List getSalesByCustomer(int orgID, String fromDate, String toDate, int productID){
			List invoiceList=null;
		
				logger.debug("Entry :  public List getSalesByCustomer(int orgID, String fromDate, String toDate, int productID)");
	
				invoiceList=invoiceDAO.getSalesByCustomer(orgID, fromDate,toDate,productID);
				
				logger.debug("Exit : public List getSalesByCustomer(int orgID, String fromDate, String toDate, int productID)");
				return invoiceList;
		}
		
		
		public List getTotalSalesByCustomer(int orgID, String fromDate, String toDate){
			List invoiceList=null;
		
				logger.debug("Entry :  public List getTotalSalesByCustomer(int orgID, String fromDate, String toDate)");
	
				invoiceList=invoiceDAO.getTotalSalesByCustomer(orgID, fromDate,toDate);
				
				logger.debug("Exit : public List getTotalSalesByCustomer(int orgID, String fromDate, String toDate)");
				return invoiceList;
		}
		
		public List getTotalSalesByCustomerDetails(int orgID, String fromDate, String toDate,int ledgerID){
			List invoiceList=null;
		
				logger.debug("Entry :  public List getTotalSalesByCustomerDetails(int orgID, String fromDate, String toDate)");
	
				invoiceList=invoiceDAO.getTotalSalesByCustomerDetails(orgID, fromDate,toDate,ledgerID);
				
				logger.debug("Exit : public List getTotalSalesByCustomerDetails(int orgID, String fromDate, String toDate)");
				return invoiceList;
		}
		
		public MonthwiseChartVO getMonthWiseSalesReportForChart(int orgID, String fromDate, String toDate){
			List payableList=null;
			MonthwiseChartVO reportVO=new MonthwiseChartVO();
		
				logger.debug("Entry :  public ReportVO getMonthWiseSalesReportForChart(int orgID, String fromDate, String toDate)");
	
				payableList=invoiceDAO.getMonthWiseSalesReportForChart(orgID, fromDate,toDate);
				
				if(payableList.size()>0){
					reportVO=(MonthwiseChartVO) payableList.get(0);
				}
				
				logger.debug("Exit : public ReportVO getMonthWiseSalesReportForChart(int orgID, String fromDate, String toDate)");
				return reportVO;
		}
		
		public List getMonthWiseSalesReport(int orgID, String fromDate, String toDate){
			List invoiceList=null;
			
		
				logger.debug("Entry :  public ReportVO getMonthWiseSalesReport(int orgID, String fromDate, String toDate)");
	
				invoiceList=invoiceDAO.getMonthWiseSalesReport(orgID, fromDate,toDate);
				
				
				logger.debug("Exit : public ReportVO getMonthWiseSalesReport(int orgID, String fromDate, String toDate)");
				return invoiceList;
		}
		
		public List getMonthWiseSalesReportDetails(int orgID, String fromDate, String toDate,int productID){
			List invoiceList=null;
			
		
				logger.debug("Entry :  public ReportVO getMonthWiseSalesReportDetails(int orgID, String fromDate, String toDate)");
	
				invoiceList=invoiceDAO.getMonthWiseSalesReportDetails(orgID, fromDate,toDate,productID);
				
				
				logger.debug("Exit : public ReportVO getMonthWiseSalesReportDetails(int orgID, String fromDate, String toDate)");
				return invoiceList;
		}
	
		public List getTop20SalesProduct(int orgID, String fromDate, String toDate){
			List invoiceList=null;
		
				logger.debug("Entry :  public List getTop20SalesProduct(int orgID, String fromDate, String toDate)");
	
				invoiceList=invoiceDAO.getTop20SalesProduct(orgID, fromDate,toDate);
				
				logger.debug("Exit : public List getTop20SalesProduct(int orgID, String fromDate, String toDate)");
				return invoiceList;
		}
		
		
		public List getSalesReportProductWise(int orgID, String fromDate, String toDate, int productID){
			List invoiceList=null;
		
				logger.debug("Entry :  public List getSalesReportProductWise(int orgID, String fromDate, String toDate, int productID)");
	
				invoiceList=invoiceDAO.getSalesReportProductWise(orgID, fromDate,toDate,productID);
				
				logger.debug("Exit : public List getSalesReportProductWise(int orgID, String fromDate, String toDate, int productID)");
				return invoiceList;
		}
		
		public List getSalesReportProductWiseDetails(int orgID, String fromDate, String toDate, int productID,int ledgerID){
			List invoiceList=null;
		
				logger.debug("Entry :  public List getSalesReportProductWiseDetails(int orgID, String fromDate, String toDate, int productID)");
	
				invoiceList=invoiceDAO.getSalesReportProductWiseDetails(orgID, fromDate,toDate,productID,ledgerID);
				
				logger.debug("Exit : public List getSalesReportProductWiseDetails(int orgID, String fromDate, String toDate, int productID)");
				return invoiceList;
		}
		
		 public List getRateCardList(int orgID){
				List rateCardList=null;
			
					logger.debug("Entry :   public List getRateCardList(int orgID)");
		
					rateCardList=invoiceDAO.getRateCardList(orgID);
					
					logger.debug("Exit :  public List getRateCardList(int orgID)");
					return rateCardList;
			}
		
		 
		 /* insert product rate card  */
		public int insertProductRateCard(ItemDetailsVO itemDetailsVO){
		 int success=0;
	
		 logger.debug("Entry : public int insertProductRateCard(ItemDetailsVO itemDetailsVO)");
		 try{		
			
			 success=invoiceDAO.insertProductRateCard(itemDetailsVO);
		
		   		  
		 }catch(Exception e){
				logger.error("Exception in insertProductRateCard "+e);
		 }
				
		 logger.debug("Exit : public int insertProductRateCard(ItemDetailsVO itemDetailsVO)");
			return success;		
		}
		
		public int updateProductRateCard(ItemDetailsVO itemDetailsVO){
			 int success=0;
			 int hsnSacID=0;
			 logger.debug("Entry : public int updateProductRateCard(ItemDetailsVO itemDetailsVO)");
			 try{	
				 				
				 success=invoiceDAO.updateProductRateCard(itemDetailsVO);
			
			 }catch(Exception e){
				logger.error("Exception in updateProductRateCard "+e);
			 }
					
			 logger.debug("Exit : public int updateProductRateCard(ItemDetailsVO itemDetailsVO)");
				return success;		
		}
		
		public int deleteProductRateCard(ItemDetailsVO itemDetailsVO){
			int deleteFlag=0;
			Boolean isPresent=false;
			logger.debug("Entry : public int deleteProductRateCard(ItemDetailsVO itemDetailsVO)");
			try{
			
				deleteFlag=invoiceDAO.deleteProductRateCard(itemDetailsVO);
			
								  
			}catch(Exception e){
				logger.error("Exception in deleteProductRateCard "+e);
			}
			
			logger.debug("Exit : public int deleteProductRateCard(ItemDetailsVO itemDetailsVO)");
			return deleteFlag;		
		}
		
		 /* insert product rate card  item*/
			public int insertProductRateCardItem(ItemDetailsVO itemDetailsVO){
			 int success=0;
		
			 logger.debug("Entry : public int insertProductRateCardItem(ItemDetailsVO itemDetailsVO)");
			 try{		
				
				 success=invoiceDAO.insertProductRateCardItem(itemDetailsVO);
			
			   		  
			 }catch(Exception e){
					logger.error("Exception in insertProductRateCardItem "+e);
			 }
					
			 logger.debug("Exit : public int insertProductRateCardItem(ItemDetailsVO itemDetailsVO)");
				return success;		
			}
			
			public int updateProductRateCardItem(ItemDetailsVO itemDetailsVO){
				 int success=0;
				 int hsnSacID=0;
				 logger.debug("Entry : public int updateProductRateCardItem(ItemDetailsVO itemDetailsVO)");
				 try{	
					 				
					 success=invoiceDAO.updateProductRateCardItem(itemDetailsVO);
				
				 }catch(Exception e){
					logger.error("Exception in updateProductRateCardItem "+e);
				 }
						
				 logger.debug("Exit : public int updateProductRateCardItem(ItemDetailsVO itemDetailsVO)");
					return success;		
			}
			
			public int deleteProductRateCardItem(ItemDetailsVO itemDetailsVO){
				int deleteFlag=0;
				Boolean isPresent=false;
				logger.debug("Entry : public int deleteProductRateCardItem(ItemDetailsVO itemDetailsVO)");
				try{
				
					deleteFlag=invoiceDAO.deleteProductRateCardItem(itemDetailsVO);
				
									  
				}catch(Exception e){
					logger.error("Exception in deleteProductRateCardItem "+e);
				}
				
				logger.debug("Exit : public int deleteProductRateCardItem(ItemDetailsVO itemDetailsVO)");
				return deleteFlag;		
			}
			
		
		
		 public ItemDetailsVO getProductRateCardDetails(int productID,int rateCardID){
				logger.debug("Entry : public List getProductServiceLedgerDetails(int itemID)");
				
				ItemDetailsVO rateCardVO=invoiceDAO.getProductRateCardDetails(productID, rateCardID);
								
				logger.debug("Exit :  public ItemDetailsVO getProductRateCardDetails(int productID,int rateCardID)");
				return rateCardVO;		
			}
				
		 
		 public List getProductRateCardItemList(int orgID,int rateCardID){
				List rateCardList=null;
			
					logger.debug("Entry :  public List getProductRateCardItemList(int orgID,int rateCardID)");
		
					rateCardList=invoiceDAO.getProductRateCardItemList(orgID,rateCardID);
					
					logger.debug("Exit :  public List getProductRateCardItemList(int orgID,int rateCardID)");
					return rateCardList;
			}
		 
		 
		 /* Get list of all bills of the given member for particular period  */
			public List getBillsForMemberApp(int societyID,int aptID){
				List billList=new ArrayList();
				logger.debug("Entry : public List getBillsForMember(int societyID,String fromDate,String toDate)");
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				try{
				
				List billingCycleList=invoiceDAO.getBillingCycleListForMemberApp(societyID);
				
				
				
				for(int i=0;billingCycleList.size()>i;i++){
					InvoiceBillingCycleVO bCycleVO=(InvoiceBillingCycleVO) billingCycleList.get(i);
					
					//if((format.parse(bCycleVO.getFromDate()).compareTo(format.parse(fromDate))>=0)&&(format.parse(bCycleVO.getUptoDate()).compareTo(format.parse(toDate))<=0)){
					
					
					BillDetailsVO billVO=invoiceDAO.getBillsForMember(societyID, bCycleVO.getFromDate(), bCycleVO.getUptoDate(), aptID);
					
					
					billVO.setBillNo(bCycleVO.getBillingCycleID()+" - "+societyID+" - "+billVO.getLedgerID());
					billVO.setBillingCycleID(bCycleVO.getBillingCycleID());
					billVO.setDueDate(bCycleVO.getDueDate());
					billList.add(billVO);
					
					
					
				}
				
					
					
							
				//billList;//=invoiceDomain.getBillsForMember(societyID, fromDate, toDate);
				
				}catch(Exception e){
					logger.error("Exception in public List getBillsForMember(int societyID,String fromDate,String toDate) "+e);
				}
				logger.debug("Exit : public List getBillsForMember(int societyID,String fromDate,String toDate)"+billList.size());
				return billList;		
			}
			
				
	public InvoiceDAO getInvoiceDAO() {
		return invoiceDAO;
	}

	public void setInvoiceDAO(InvoiceDAO invoiceDAO) {
		this.invoiceDAO = invoiceDAO;
	}


	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	public void setEmailDomain(EmailDomain emailDomain) {
		this.emailDomain = emailDomain;
	}

	public MemberService getMemberServiceBean() {
		return memberServiceBean;
	}

	public void setMemberServiceBean(MemberService memberServiceBean) {
		this.memberServiceBean = memberServiceBean;
	}

	public SocietyService getSocietyService() {
		return societyService;
	}

	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}

	public CalculateLateFee getCalculateLateFees() {
		return calculateLateFees;
	}

	public void setCalculateLateFees(CalculateLateFee calculateLateFees) {
		this.calculateLateFees = calculateLateFees;
	}

	public void setLedgerService(LedgerService ledgerService) {
		this.ledgerService = ledgerService;
	}

	/**
	 * @param calculateLateFeesForTransaction the calculateLateFeesForTransaction to set
	 */
	public void setCalculateLateFeesForTransaction(CalculateLateFeeForTransactions calculateLateFeesForTransaction) {
		this.calculateLateFeesForTransaction = calculateLateFeesForTransaction;
	}


	




}
