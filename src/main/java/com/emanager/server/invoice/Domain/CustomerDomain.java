package com.emanager.server.invoice.Domain;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;

import com.emanager.server.commonUtils.services.AddressService;
import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.invoice.DAO.CustomerDAO;
import com.emanager.server.invoice.dataAccessObject.CustomerVO;

public class CustomerDomain {
	
	Logger logger=Logger.getLogger(CustomerDomain.class);
	CustomerDAO customerDAO;
	AddressService addressService;
	
	public List getCustomerDetaiilsList(int orgID)
	{
		List customerList=null;
		try
		{
			logger.debug("Entry: public List getCustomerDetaiilsList(int orgID)");
			customerList=customerDAO.getCustomerDetaiilsList(orgID);
			
		}catch(Exception ex)
		{
			logger.error("Exception in getCustomerDetaiilsList : "+ex);
			
		}
		logger.debug("Exit: public List getCustomerDetaiilsList()");
		return customerList; 
	}

	
	
	public int updateCustomerDetails(CustomerVO custVO,AddressVO addressVO) {

		int sucess = 0;  
		int successAdress=0;
		try{
		
		    logger.debug("Entry : public int updateCustomerDetails(CustomerVO custVO,AddressVO addressVO)");
		
		    sucess = customerDAO.updateCustomerDetails(custVO, addressVO);
		    
		    successAdress=addressService.addUpdateAddressDetails(addressVO, custVO.getCustomerID());
		    
						
		    logger.debug("Exit : public int updateCustomerDetails(CustomerVO custVO,AddressVO addressVO)");

		}catch(Exception Ex){
			
		
			logger.error("Exception in updateCustomerDetails(CustomerVO custVO,AddressVO addressVO) : "+Ex);
			
		}
		return sucess;
	}
	
	public int deleteCustomer(int custID){
		
		int sucess = 0;
		try{
		
		logger.debug("Entry : public int deleteCustomer(int custID)");
		
		sucess = customerDAO.deleteCustomer(custID);
				
		logger.debug("Exit : public int  deleteCustomer(int custID)");

		}catch(Exception Ex){		
			logger.error("Exception in deleteCustomer : "+Ex);			
		}
		return sucess;
	}
	
	public void setCustomerDAO(CustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}

	public void setAddressService(AddressService addressService) {
		this.addressService = addressService;
	}


}
