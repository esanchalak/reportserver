package com.emanager.server.vendorManagement.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;

import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.vendorManagement.domainObject.VendorDomain;
import com.emanager.server.vendorManagement.valueObject.VendorVO;

public class VendorService {
    
    Logger logger=Logger.getLogger(VendorService.class);
    VendorDomain vendorDomain;
    
    public List getVendorDetaiilsList()
    {
        List vendorList=null;
        try
        {
            logger.debug("Entry: public List getVendorDetaiilsList()");
            vendorList=vendorDomain.getVendorDetaiilsList();
            
        }catch(Exception ex)
        {
            logger.error("Exception in getVendorDetaiilsList : "+ex);            
        }
        logger.debug("Exit : public List getVendorDetaiilsList()");
        return vendorList;
    }

public int insertVendorDetails(VendorVO vendorVO,AddressVO addrVO){
        
    
        int insertFlag = 0;
        
        try {
            logger.debug("Entry : public int insertVendorDetails(VendorVO vendorVO,AddressVO addrVO)");
                     
            insertFlag = vendorDomain.insertVendorDetails( vendorVO, addrVO);
                       
            logger.debug("Exit : public int insertVendorDetails(VendorVO vendorVO,AddressVO addrVO)"+insertFlag);
        }catch (DataAccessException exc) {
          logger.error("DataAccessException in insertVendorDetails : "+exc);
        }
        catch (Exception e) {
          logger.error("Exception in insertVendorDetails:" + e);
        }

        return insertFlag;
        }
    
public int updateVendor(VendorVO vendorVO,AddressVO addressVO) {

    int sucess = 0;  
    
    try{
    
        logger.debug("Entry : public int updateVendor(VendorVO vendorVO,AddressVO addressVO)");
    
        sucess = vendorDomain.updateVendor(vendorVO, addressVO);
                    
        logger.debug("Exit : public int updateVendor(VendorVO vendorVO,AddressVO addressVO) ");

    }catch(Exception Ex){        
        Ex.printStackTrace();
        logger.error("Exception in updateVendor : "+Ex);
        
    }
    return sucess;
}
public int deleteVendor(int vendorID){
    
    int sucess = 0;
    try{
    
    logger.debug("Entry : public int deleteVendor(int vendorID)");
    
    sucess = vendorDomain.deleteVendor(vendorID);
            
    logger.debug("Exit : public int deleteVendor(int vendorID)");

    }catch(Exception Ex){        
        logger.error("Exception in deleteVendor : "+Ex);        
    }
    return sucess;
}


    public void setVendorDomain(VendorDomain vendorDomain) {
        this.vendorDomain = vendorDomain;
    }

}

