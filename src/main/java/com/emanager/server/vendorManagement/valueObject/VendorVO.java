package com.emanager.server.vendorManagement.valueObject;

import com.emanager.server.commonUtils.valueObject.AddressVO;

public class VendorVO {
	
	private int vendorID;
	private int societyID;
	private int vendorLedgerID;
	private String title;
	private String vendorName;
	private String companyName;
	private String vendorType;
	private String mobile;
	private String email;
	private String phone;
	private int deducteeCode;
	private int tdsSectionID;
	private int serviceLedgerID;
	private String serviceName;
	private String panNo;
	private String tanNo;
	private String authorizedPersonName;
	private String authorizedPersonEmail;
	private String authorizedPersonMobile;
	private String entityType;
	private AddressVO address;
	
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public int getDeducteeCode() {
		return deducteeCode;
	}
	public void setDeducteeCode(int deducteeCode) {
		this.deducteeCode = deducteeCode;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getServiceLedgerID() {
		return serviceLedgerID;
	}
	public void setServiceLedgerID(int serviceLedgerID) {
		this.serviceLedgerID = serviceLedgerID;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	public int getTdsSectionID() {
		return tdsSectionID;
	}
	public void setTdsSectionID(int tdsSectionID) {
		this.tdsSectionID = tdsSectionID;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getVendorID() {
		return vendorID;
	}
	public void setVendorID(int vendorID) {
		this.vendorID = vendorID;
	}
	public int getVendorLedgerID() {
		return vendorLedgerID;
	}
	public void setVendorLedgerID(int vendorLedgerID) {
		this.vendorLedgerID = vendorLedgerID;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVendorType() {
		return vendorType;
	}
	public void setVendorType(String vendorType) {
		this.vendorType = vendorType;
	}
	public String getAuthorizedPersonEmail() {
		return authorizedPersonEmail;
	}
	public void setAuthorizedPersonEmail(String authorizedPersonEmail) {
		this.authorizedPersonEmail = authorizedPersonEmail;
	}
	public String getAuthorizedPersonMobile() {
		return authorizedPersonMobile;
	}
	public void setAuthorizedPersonMobile(String authorizedPersonMobile) {
		this.authorizedPersonMobile = authorizedPersonMobile;
	}
	public String getAuthorizedPersonName() {
		return authorizedPersonName;
	}
	public void setAuthorizedPersonName(String authorizedPersonName) {
		this.authorizedPersonName = authorizedPersonName;
	}
	public String getPanNo() {
		return panNo;
	}
	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}
	public String getTanNo() {
		return tanNo;
	}
	public void setTanNo(String tanNo) {
		this.tanNo = tanNo;
	}
	public String getEntityType() {
		return entityType;
	}
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	/**
	 * @return the address
	 */
	public AddressVO getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(AddressVO address) {
		this.address = address;
	}
	
	
	
	

}
