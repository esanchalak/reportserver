package com.emanager.server.vendorManagement.dataAccessObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.emanager.server.assets.valueObject.AssetsVO;
import com.emanager.server.assets.valueObject.ManufactureVO;
import com.emanager.server.commonUtils.dataaccessObject.AddressDAO;
import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.invoice.dataAccessObject.CustomerVO;
import com.emanager.server.vendorManagement.valueObject.VendorVO;

public class VendorDAO {

	Logger logger=Logger.getLogger(VendorDAO.class);
	public NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	AddressDAO addressDAO;
	
	public List getVendorDetaiilsList()
	{
		List vendorList=null;
		try
		{
			logger.debug("Entry : public List getVendorDetaiilsList()");
			String sql="SELECT * FROM vendor_details  ";
			Map hmap=new HashMap();
			
			RowMapper rMapper=new RowMapper()
			{

				public Object mapRow(ResultSet rs, int arg1) throws SQLException {
					// TODO Auto-generated method stub
					VendorVO vendorVO=new VendorVO();
					vendorVO.setVendorID(rs.getInt("id"));
					vendorVO.setTitle(rs.getString("title"));
					vendorVO.setVendorName(rs.getString("vendor_name"));
					vendorVO.setCompanyName(rs.getString("company_name"));
					vendorVO.setVendorType(rs.getString("vendor_type"));
					vendorVO.setMobile(rs.getString("mobile"));
					vendorVO.setPhone(rs.getString("phone"));
					vendorVO.setEmail(rs.getString("email"));
					vendorVO.setDeducteeCode(rs.getInt("deductee_code"));
					vendorVO.setPanNo(rs.getString("pan_no"));
					vendorVO.setTanNo(rs.getString("tan_no"));
					vendorVO.setAuthorizedPersonName(rs.getString("authorized_person_name"));
					vendorVO.setAuthorizedPersonEmail(rs.getString("authorized_person_email"));
					vendorVO.setAuthorizedPersonMobile(rs.getString("authorized_person_mobile"));
					
					
					return vendorVO;
				}
				
				
			};
			vendorList=namedParameterJdbcTemplate.query(sql, hmap, rMapper);
		
			
		}catch(Exception ex)
		{
			logger.error("Exception in getVendorDetaiilsList : "+ex);			
		}
		logger.debug("Exit: public List getVendorDetaiilsList"+vendorList.size());
		return vendorList; 
	}
	
	public int insertVendorDetails(VendorVO vendorVO){
		
		int flag = 0;
		int insertFlag = 0;
		int success = 0;
	    int success1 =0;
	    int genID = 0;
	    
		try {
			logger.debug("Entry : public int insertVendorDetails(VendorVO vendorVO)");
			
		    String insertFileString = "INSERT INTO vendor_details(title,company_name,mobile,email,vendor_type,phone,deductee_code) VALUES( :title,  :companyName, :mobile, :email, :vendorType, :phone,:deducteeCode)";
		    logger.debug("query : " + insertFileString);
		    
	    	SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(
				                          vendorVO);
		
		    KeyHolder keyHolder = new GeneratedKeyHolder();
		    insertFlag = getNamedParameterJdbcTemplate().update(
				    insertFileString, fileParameters, keyHolder);
		    genID = keyHolder.getKey().intValue();
		    logger.debug("key GEnereator ---" + genID);
			
		   
	 	
		  
		
		   
		    logger.debug("Exit : public int insertVendorDetails(VendorVO vendorVO)"+genID);
		}catch (DataAccessException exc) {
		  logger.error("DataAccessException :" + exc);
		}
		catch (Exception e) {
		  logger.error("Exception in insertVendorDetails : " + e);
		}

		return genID;
		}

	
	public int insertVendorProperties(VendorVO vendorVO){
		
		int flag = 0;
		int insertFlag = 0;
		int success = 0;
	    int success1 =0;
	    int genID = 0;
	    
		try {
			logger.debug("Entry : public int insertVendorProperties(VendorVO vendorVO)");
			
		    String insertFileString = "INSERT INTO tax_details(entity_id,entity,tan_no,pan_no,authorized_person_name,authorized_person_email,authorized_person_mobile) " +
		    		"VALUES( :entityID, :entity, :tanNo, :panNO, :authorizedPersonName, :authorizedPersonEmail, :authorizedPersonMobile)";
		    logger.debug("query : " + insertFileString);
		    
		    Map hmap=new HashMap();
			hmap.put("entityID", vendorVO.getVendorID());
			hmap.put("entity", vendorVO.getEntityType());
			hmap.put("panNo", vendorVO.getPanNo());
			hmap.put("tanNo", vendorVO.getTanNo());
			hmap.put("authorizedPersonName", vendorVO.getAuthorizedPersonName());
			hmap.put("authorizedPersonEmail",vendorVO.getAuthorizedPersonEmail());
			hmap.put("authorizedPersonMobile", vendorVO.getAuthorizedPersonMobile());
			
			insertFlag=namedParameterJdbcTemplate.update(insertFileString,hmap);
		   
	 	
		  
		
		   
		    logger.debug("Exit : public int insertVendorProperties(VendorVO vendorVO)"+genID);
		}catch (DataAccessException exc) {
		  logger.error("DataAccessException :" + exc);
		}
		catch (Exception e) {
		  logger.error("Exception in insertVendorProperties : " + e);
		}

		return genID;
		}
	
public int insertVendorLedgerMappings(VendorVO vendorVO){
		
		int flag = 0;
	
	    
		try {
			logger.debug("Entry : public insertVendorLedgerMappings(VendorVO vendorVO)");
			
		    String insertFileString = "INSERT INTO vendor_ledger_relation(society_id,vendor_id,vendor_ledger_id) " +
		    		"VALUES( :societyID, :vendorID, :ledgerID)";
		    logger.debug("query : " + insertFileString);
		    
		    Map hmap=new HashMap();
			hmap.put("societyID", vendorVO.getSocietyID());
			hmap.put("vendorID", vendorVO.getVendorID());
			hmap.put("ledgerID", vendorVO.getVendorLedgerID());
		
			
			flag=namedParameterJdbcTemplate.update(insertFileString,hmap);
		   
	 	
		  
		
		   
		    logger.debug("Exit : public int insertVendorLedgerMappings(VendorVO vendorVO)"+flag);
		}catch (DataAccessException exc) {
		  logger.error("DataAccessException : iinsertVendorLedgerMappings(VendorVO vendorVO))" + exc);
		}
		catch (Exception e) {
		  logger.error("Exception in insertVendorLedgerMappings(VendorVO vendorVO) : " + e);
		}

		return flag;
		}
	
	public int updateVendor(VendorVO vendorVO,AddressVO addressVO) {

		int flag = 0;
		int updateFlag=0;
		int flagM=0;
		String strSQLQuery = "";
		
		try {
			logger.debug("Entry : public int updateVendor(VendorVO vendorVO,AddressVO addressVO)");
			
			strSQLQuery = "UPDATE vendor_details v,tax_details t SET title=:title,company_name= :company_name, email=:email, mobile=:mobile ,phone=:phone, deductee_code=:deducteeCode ,tan_no= :tanNo, "+
	                      "pan_no= :panNo, authorized_person_name= :authPersonName, authorized_person_email=:authPersonEmail, authorized_person_mobile=:authPersonMobile " +
	                      "WHERE v.vendor_id=t.entity and v.vendor_id="+vendorVO.getVendorID()+";";
			
			logger.debug("query : " + strSQLQuery);

			Map hmap = new HashMap();
			hmap.put("title", vendorVO.getTitle());
			hmap.put("email", vendorVO.getEmail());
			hmap.put("mobile", vendorVO.getMobile());
			hmap.put("phone", vendorVO.getPhone());
			hmap.put("company_name", vendorVO.getCompanyName());
			hmap.put("deducteeCode", vendorVO.getDeducteeCode());
			hmap.put("panNo",vendorVO.getPanNo());
			hmap.put("tanNO", vendorVO.getTanNo());
			hmap.put("authPersonName", vendorVO.getAuthorizedPersonName());
			hmap.put("authPersonEmail", vendorVO.getAuthorizedPersonEmail());
			hmap.put("authPersonMobile", vendorVO.getAuthorizedPersonMobile());
		    
			flag=namedParameterJdbcTemplate.update(strSQLQuery, hmap);
			
			
			
			
			
			logger.debug("Exit : public int updateVendor(VendorVO vendorVO,AddressVO addressVO)"+flag);

		} catch (Exception Ex) {
			logger.error("Exception in updateVendor : "+Ex);
		}

		return flag;

	}
	
	public int deleteVendor(int vendorID){
		
		int sucess = 0;
		String strSQL = "";
		try {
				logger.debug("Entry : public int deleteVendor(int vendorID)");
				strSQL = "update  vendor_details set is_deleted=1 WHERE vendor_id=:vendor_id;" ;
				logger.debug("query : " + strSQL);
				// 2. Parameters.
				Map namedParameters = new HashMap();
				namedParameters.put("vendor_id", vendorID);
				
				sucess = namedParameterJdbcTemplate.update(strSQL,namedParameters);
				
				logger.debug(sucess);
				logger.debug("Exit : public int deleteVendor(int vendorID)");

		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in deleteVendor : "+Ex);
			
		}
		return sucess;
	}	
	
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	public void setAddressDAO(AddressDAO addressDAO) {
		this.addressDAO = addressDAO;
	}


	
}
