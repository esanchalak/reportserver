package com.emanager.server.vendorManagement.domainObject;

import java.util.List;

//import org.apache.batik.css.engine.value.css2.SrcManager;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;

import com.emanager.server.commonUtils.services.AddressService;
import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.vendorManagement.dataAccessObject.VendorDAO;
import com.emanager.server.vendorManagement.valueObject.VendorVO;

public class VendorDomain {
	
	Logger logger=Logger.getLogger(VendorDomain.class);
	VendorDAO vendorDAO;
	AddressService addressService;
	
	public List getVendorDetaiilsList()
	{
		List vendorList=null;
		try
		{
			logger.debug("Entry: public List getVendorDetaiilsList()");
			vendorList=vendorDAO.getVendorDetaiilsList();
			
		}catch(Exception ex)
		{
			logger.error("Exception in getVendorDetaiilsList : "+ex);
			
		}
		logger.debug("Exit: public List getVendorDetaiilsList()");
		return vendorList; 
	}

	public int insertVendorDetails(VendorVO vendorVO,AddressVO addressVO){
				
		int insertFlag = 0;
		int success=0;
		int insertFlagProps=0;
		
		try {
			logger.debug("Entry :public int insertVendorDetails(VendorVO vendorVO,AddressVO addressVO)");
					   
		    insertFlag = vendorDAO.insertVendorDetails(vendorVO);
		    
		    if(insertFlag!=0){
		    	vendorVO.setVendorID(insertFlag);
		    	insertFlagProps=vendorDAO.insertVendorProperties(vendorVO);
		    	if((vendorVO.getVendorLedgerID()>0)&&(vendorVO.getSocietyID()>0)){
		    		int insertMapping=vendorDAO.insertVendorLedgerMappings(vendorVO);
		    	}
		    }
		    
		    int vendorID=insertFlag;
		    
		    if((insertFlag != 0)&&(insertFlagProps!=0)&&(addressVO!=null)){
				  
			     success = addressService.addUpdateAddressDetails(addressVO, vendorID);
		    }
		    if(insertFlag > 0){
			    insertFlag = 1;
		    }
					   
		    logger.debug("Exit :public int insertVendorDetails(VendorVO vendorVO,AddressVO addressVO)"+insertFlag);
		}catch (DataAccessException exc) {
		  logger.error("DataAccessException in insertVendorDetails : "+exc);
		}
		catch (Exception e) {
		  logger.error("Exception in insertVendorDetails : "+e);
		}

		return insertFlag;
		}
	
	public int updateVendor(VendorVO vendorVO,AddressVO addressVO) {

		int sucess = 0;  
		int successAdress=0;
		try{
		
		    logger.debug("Entry : public int updateVendor(VendorVO vendorVO,AddressVO addressVO)");
		
		    sucess = vendorDAO.updateVendor(vendorVO, addressVO);
		    
		    successAdress=addressService.addUpdateAddressDetails(addressVO, vendorVO.getVendorID());
		    
						
		    logger.debug("Exit : public int updateVendor(VendorVO vendorVO,AddressVO addressVO) ");

		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in updateVendor : "+Ex);
			
		}
		return sucess;
	}
	
	public int deleteVendor(int vendorID){
		
		int sucess = 0;
		try{
		
		logger.debug("Entry : public int deleteVendor(int vendorID)");
		
		sucess = vendorDAO.deleteVendor(vendorID);
				
		logger.debug("Exit : public int deleteVendor(int vendorID)");

		}catch(Exception Ex){		
			logger.error("Exception in deleteVendor : "+Ex);			
		}
		return sucess;
	}
	
	public void setVendorDAO(VendorDAO vendorDAO) {
		this.vendorDAO = vendorDAO;
	}

	public void setAddressService(AddressService addressService) {
		this.addressService = addressService;
	}


}
