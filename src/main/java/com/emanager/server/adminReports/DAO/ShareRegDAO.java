package com.emanager.server.adminReports.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.emanager.server.adminReports.valueObject.ShareRegisterVO;
import com.emanager.server.adminReports.valueObject.TransferRegisterVO;

public class ShareRegDAO {
	
	   
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	private static final Logger logger = Logger.getLogger(ShareRegDAO.class);

	public List getShareList(int buildingID,int societyId) {

		List shareList=null;
		
		String strWhereClause="";
		
		if(buildingID==0)
			strWhereClause=" ";
			else
				strWhereClause="  AND m.building_id="+buildingID+"";

		try {
			logger.debug("Entry : public List getShareList(int societyId)"+societyId);
			
		
		String strSQL = "SELECT unit_name,GROUP_CONCAT(m.membership_id ORDER BY m.member_id SEPARATOR ',') AS membership_id1,GROUP_CONCAT(full_name ORDER BY m.member_id SEPARATOR ', ') AS memberName,share_cert_details.*,m.apt_id ,m.member_id AS MemberID ,membership_id "+
    		             "FROM (members_info m,apartment_details a) LEFT  JOIN share_cert_details ON m.apt_id=share_cert_details.apt_id "+
             		     "WHERE   m.society_id=:societyId "+strWhereClause+" AND m.is_current=1 AND m.apt_id=a.apt_id GROUP BY m.apt_id,share_id ORDER BY a.seq_no ; "; 
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyId", societyId);
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				String tempAptName = "";
				String tempMemberName="";
				int i=0;
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					
					  ShareRegisterVO shareRegVO = new ShareRegisterVO();
			            shareRegVO.setAptId(rs.getInt("apt_id"));
			            String aptName =rs.getString("unit_name");			
			            String memberName=rs.getString("memberName");
			            if (!tempAptName.equalsIgnoreCase(aptName)) {
			            	shareRegVO.setAptName(aptName);
							tempAptName = aptName;
							shareRegVO.setMemberName(memberName);
							tempMemberName = memberName;
							shareRegVO.setMemberShipId(rs.getString("membership_id1"));
							++i;
							
						} else {
							shareRegVO.setMemberName(" ");
							shareRegVO.setAptName("");
						}	           
			            shareRegVO.setMemberId(rs.getInt("MemberID"));
			            shareRegVO.setShareCertNo(rs.getString("share_cert_no"));
			            shareRegVO.setShareId(rs.getInt("share_id"));
			            //shareRegVO.setMemberName(rs.getString("memberName"));
			                            
			            shareRegVO.setAllotmentDate(rs.getString("share_date"));
			            shareRegVO.setShareFrom(rs.getInt("share_from"));
			            shareRegVO.setShareTo(rs.getInt("share_upto"));
			            shareRegVO.setNoOfShares(rs.getInt("no_of_share"));
			            shareRegVO.setFaceValue(rs.getBigDecimal("face_value"));
			            shareRegVO.setTotalValue(rs.getBigDecimal("total_share_value"));          
			            return shareRegVO;
				}
			};
			shareList=  namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);

			
			logger.debug("Exit:	public List getShareList(int societyId)");
		} catch (NullPointerException ex) {
			logger.info("No shares found for societyID : "+societyId);

			
		} catch (Exception ex) {
			logger.error("Exception in getShareList : "+ex);

		}
		return shareList;

	}
	
	
	public ShareRegisterVO getShareDetails(int aptID) {

		ShareRegisterVO shareCertVO=new ShareRegisterVO();
		
		String strWhereClause="";
		
		

		try {
			logger.debug("Entry : public List getShareDetails(int aptID)"+aptID);
			
		
		String strSQL = "SELECT * FROM share_cert_details s WHERE apt_id=:aptID; "; 
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("aptID", aptID);
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				String tempAptName = "";
				String tempMemberName="";
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					
					  ShareRegisterVO shareRegVO = new ShareRegisterVO();
			            shareRegVO.setAptId(rs.getInt("apt_id"));
			            
			            shareRegVO.setShareCertNo(rs.getString("share_cert_no"));
			            shareRegVO.setShareId(rs.getInt("share_id"));
			            
			            shareRegVO.setAllotmentDate(rs.getString("share_date"));
			            shareRegVO.setShareFrom(rs.getInt("share_from"));
			            shareRegVO.setShareTo(rs.getInt("share_upto"));
			            shareRegVO.setNoOfShares(rs.getInt("no_of_share"));
			            shareRegVO.setFaceValue(rs.getBigDecimal("face_value"));
			            shareRegVO.setTotalValue(rs.getBigDecimal("total_share_value"));          
			            return shareRegVO;
				}
			};
			shareCertVO=  (ShareRegisterVO) namedParameterJdbcTemplate.queryForObject(strSQL,namedParameters,RMapper);

			
			logger.debug("Exit:	public List getShareList(int societyId)");
		} catch (EmptyResultDataAccessException ex) {
			logger.info("No share cert details found : "+ex);

			
		} catch (Exception ex) {
			logger.error("Exception in getShareList : "+ex);

		}
		return shareCertVO;

	}
	

	public List getTransferRegister(int orgID,String fromDate,String toDate){
		logger.debug("Entry : public List getTransferRegister(int orgID,String fromDate,String toDate)");
		List transferRegister = null;
		try {
		
			
		
		String strSQL = "SELECT m.*,s.share_cert_no,s.share_from,s.share_upto,s.no_of_share,s.face_value,s.total_share_value FROM (SELECT id,h.member_id AS old_member_id,h.membership_id,h.apt_id,h.title,h.full_name,date_of_transfer,h.age,transferee_name,h.org_id,transfer_type,h.notes,mi.unit_name FROM member_history h,members_info mi WHERE h.apt_id=mi.apt_id AND h.org_id=mi.society_id AND mi.type='P' AND "
				+ " (date_of_transfer BETWEEN :fromDate AND :toDate) AND h.type='P' AND h.org_id=:orgID) AS m LEFT JOIN share_cert_details s ON s.apt_id=m.apt_id  order by m.date_of_transfer; "; 
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("orgID", orgID);
			namedParameters.put("fromDate", fromDate);
			namedParameters.put("toDate", toDate);
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					
					  TransferRegisterVO transferVO = new TransferRegisterVO();
			            transferVO.setAptID(rs.getInt("apt_id"));
			            transferVO.setAptName(rs.getString("unit_name"));			           
			            transferVO.setMemberID(rs.getInt("old_member_id"));
			            transferVO.setShareCertNo(rs.getString("share_cert_no"));
			            transferVO.setOldMemberName(rs.getString("title")+" "+rs.getString("full_name"));
			            transferVO.setOldMemberShipID(rs.getInt("membership_id"));
			            transferVO.setTransferDate(rs.getString("date_of_transfer"));
			            transferVO.setNewMemberName(rs.getString("transferee_name"));
			            transferVO.setTransferType(rs.getString("transfer_type"));
			            transferVO.setNotes(rs.getString("notes"));
			            transferVO.setOrgID(rs.getInt("org_id"));
			            transferVO.setShareFrom(rs.getInt("share_from"));
			            transferVO.setShareTo(rs.getInt("share_upto"));
			            transferVO.setNoOfShares(rs.getInt("no_of_share"));
			            transferVO.setFaceValue(rs.getBigDecimal("face_value"));
			            transferVO.setTotalValue(rs.getBigDecimal("total_share_value"));          
			            return transferVO;
				}
			};
			transferRegister=  namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);

			
			logger.debug("Exit:	public List getTransferRegister(int orgID,String fromDate,String toDate)");
		} catch (NullPointerException ex) {
			logger.info("No shares found for public List getTransferRegister(int orgID,String fromDate,String toDate) : "+orgID);

			
		} catch (Exception ex) {
			logger.error("Exception in public List getTransferRegister(int orgID,String fromDate,String toDate) : "+ex);

		}
		return transferRegister;

	}
	
	
	
	
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	

	
	
}
