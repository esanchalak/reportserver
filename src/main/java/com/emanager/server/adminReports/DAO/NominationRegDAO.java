package com.emanager.server.adminReports.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.emanager.server.adminReports.valueObject.NominationRegisterVO;
import com.emanager.server.commonUtils.dataaccessObject.AddressDAO;
import com.emanager.server.society.valueObject.MemberVO;

public class NominationRegDAO {
	
	   
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	private JdbcTemplate jdbcTemplate;
	private AddressDAO addressDAO;
	private static final Logger logger = Logger.getLogger(NominationRegDAO.class);

	public List getNominationList(int buildingID,int societyId) {

		List nominationList=null;
		
		String strWhereClause="";
		
		if(buildingID==0)
			strWhereClause=" ";
			else
				strWhereClause="  AND a.building_id="+buildingID+"";

		try {
			logger.debug("Entry : public List getNominationList(int societyId)"+societyId);
		Map sampleMap=new HashMap();	
		String createAddressView="CREATE VIEW address AS SELECT apr.person_id,ab.* FROM address_book ab,address_person_relationship apr WHERE apr.address_id=ab.address_id AND apr.person_category='N';";	
		//jdbcTemplate.execute(createAddressView);
		String createNomineeAddressView="create view nominee_address as select * from (nominee_details n) left join address a on n.nominee_id=a.person_id ;";	
		//jdbcTemplate.execute(createNomineeAddressView);
		
		String strSQL = "SELECT CONCAT(b.building_name,' ',a.apt_name) AS aptName,m.full_name,n.*,a.apt_id ,m.member_id AS MemberID,s.id As statusId ,s.name As statusName "+
    		             " FROM (apartment_details  a,building_details b,member_details m, nominee_address n,documents_relation dr,status s) "+
             		     " WHERE  a.building_id=b.building_id  AND b.society_id=:societyId "+strWhereClause+" AND m.apt_id=a.apt_id " +
             		     "  AND m.is_current=1 and a.unit_sold_status='Sold' AND m.member_id=n.member_id  AND dr.entity_id=m.member_id AND s.id=dr.status_id GROUP BY n.nominee_id ORDER BY seq_no; "; 
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyId", societyId);
			
			//3. RowMapper.
			
			RowMapper RMapper = new RowMapper() {
				String tempAptName = "";
				String tempMemberName="";
				int i=0;
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					
					  NominationRegisterVO nomRegVO = new NominationRegisterVO();
			            nomRegVO.setAptId(rs.getInt("apt_id"));
			            nomRegVO.setDob(rs.getString("dob"));
			            String aptName =rs.getString("aptName");			           
			            if (!tempAptName.equalsIgnoreCase(aptName)) {
							nomRegVO.setAptName(aptName);
							tempAptName = aptName;
							++i;
							nomRegVO.setSrNo(""+i);
						} else {
							nomRegVO.setSrNo("");
							nomRegVO.setAptName("");
						}
			            nomRegVO.setMemberId(rs.getInt("MemberID"));
			            nomRegVO.setNominationId(rs.getInt("nominee_id"));
			            nomRegVO.setNominationRegisterID(rs.getInt("nomination_id"));
			            nomRegVO.setMemberName(rs.getString("full_name"));
			            String memberName=rs.getString("full_name");
			            if (!tempMemberName.equalsIgnoreCase(memberName)) {
							nomRegVO.setMemberName(memberName);
							tempMemberName = memberName;
						} else {
							nomRegVO.setMemberName(" ");
						}
			            nomRegVO.setNomineeName(rs.getString("nominee_name"));
			            nomRegVO.setRelation(rs.getString("relation"));
			            nomRegVO.setShare(rs.getBigDecimal("share"));
			            nomRegVO.setRemark(rs.getString("remark"));
			            if(rs.getString("city")==null){
			            	nomRegVO.setAddressString("N A");
			            }else
			            nomRegVO.setAddressString(rs.getString("line_1")+","+rs.getString("line_2")+","+rs.getString("city"));
			            nomRegVO.setNominationDate(rs.getString("issue_date"));
			            nomRegVO.setRecordingDate(rs.getString("recording_date"));
			            nomRegVO.setSameAddress(rs.getInt("has_address"));
			            nomRegVO.setRevocationDate(rs.getString("revocation_date"));       
			            nomRegVO.setDocumentStatusID(rs.getInt("statusId"));
			            nomRegVO.setIsMinor(rs.getInt("is_minor"));
			            nomRegVO.setDocumentStatusName(rs.getString("statusName"));
			            return nomRegVO;
				}
			};
			nominationList=  namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);

			
			logger.debug("Exit:	public List getNominationList(int societyId)");
		} catch (EmptyResultDataAccessException ex) {
			logger.info("No nominee found for societyID:  "+societyId);

			
		} catch (IndexOutOfBoundsException ex) {
			logger.info("No nominee found for societyID:  "+societyId);

			
		} catch (NullPointerException ex) {
			logger.info("No nominee found for societyID:  "+societyId);

			
		} catch (Exception ex) {
			logger.error("Exception in getNominationList : "+ex);

		}
		return nominationList;

	}
	public int addNominations(NominationRegisterVO nomRegVO){
		logger.debug("Entry : public int addNominations(List nominationList)");
		int success = 0;
		int genratedID=0;
		int insertFlag=0;
		
			try {
				String insertFileString = "INSERT INTO nominee_details "
						+ "( member_id, nomination_id, nominee_name, dob, issue_date,  recording_date, revocation_date, relation, share, remark ,is_minor, has_address) VALUES "
						+ "( :memberId, :nominationRegisterID, :nomineeName, :dob, :nominationDate, :recordingDate , :revocationDate, :relation, :share, :remark, :isMinor, :sameAddress) ";
				logger.debug("Query : " + insertFileString);
				
				SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(
						nomRegVO);
				KeyHolder keyHolder = new GeneratedKeyHolder();
				success = getNamedParameterJdbcTemplate().update(
						insertFileString, fileParameters, keyHolder);
				genratedID = keyHolder.getKey().intValue();
				logger.debug("key GEnereator ---" + genratedID);
				success = addNomineeAddress(nomRegVO, genratedID);
				if ((insertFlag != 0) && (success != 0)) {
					insertFlag = 1;
				}
				if(success==0){
				 String	strSQL = "DELETE FROM nominee_details WHERE nominee_id=:nomineeId;" ;
					logger.debug("query : " + strSQL);
					// 2. Parameters.
					Map namedParameters = new HashMap();
					namedParameters.put("nomineeId", genratedID);
					
					int i = namedParameterJdbcTemplate.update(strSQL,namedParameters);
				}
				logger.debug("Exit : public int addNominations(List nominationList)"+insertFlag);
			} catch (InvalidDataAccessApiUsageException ex) {
				logger.error("InvalidDataAccessApiUsageException in public int addNominations(List nominationList) : "+ex);

			} catch (DataAccessException exc) {
				logger.error("DataAccessException in public int addNominations(List nominationList) : "+exc);
			} catch (Exception e) {
				logger.error("Exception in public int addNominations(List nominationList) : "+e);
			}	
	
			
		
			
		logger.debug("Exit : public int addNominations(List nominationList)");	
		return success;
		}
	

	public int addNomineeAddress(NominationRegisterVO nomRegVO,int genID){
		logger.debug("Entry : public int addNomineeAddress(NominationRegisterVO nomRegVO)");
		int success=0;
		if(nomRegVO.getSameAddress()==0){
		success=addressDAO.addNewAddressDetails(nomRegVO.getAddress(),genID );
		}else
			success=1;
		logger.debug("Exit : public int addNomineeAddress(NominationRegisterVO nomRegVO)");
		return success;
	}
	
	public List getNominationListForMember(int memberId) {

		List nominationList=null;

		try {
			logger.debug("Entry : public List getNominationListForMember(int memberId)"+memberId);
			
		
		String strSQL = "SELECT CONCAT(b.building_name,' ',a.apt_name) AS aptName,m.full_name,n.*,a.apt_id ,m.member_id AS MemberID "+
    		             "FROM apartment_details  a,building_details b,member_details m, nominee_address n Where m.member_id=n.member_id "+
             		     " AND a.building_id=b.building_id  AND m.member_id=:memberId AND m.apt_id=a.apt_id AND m.type='P' ORDER BY a.apt_id; "; 
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("memberId", memberId);
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				String tempAptName = "";
				String tempMemberName="";
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					
					  NominationRegisterVO nomRegVO = new NominationRegisterVO();
			            nomRegVO.setAptId(rs.getInt("apt_id"));
			            nomRegVO.setDob(rs.getString("dob"));
			            String aptName =rs.getString("aptName");
			           
			            if (!tempAptName.equalsIgnoreCase(aptName)) {
							nomRegVO.setAptName(aptName);
							tempAptName = aptName;
						} else {
							nomRegVO.setAptName(" ");
						}
			            nomRegVO.setMemberId(rs.getInt("MemberID"));
			            nomRegVO.setNominationId(rs.getInt("nominee_id"));
			            nomRegVO.setNominationRegisterID(rs.getInt("nomination_id"));
			            nomRegVO.setMemberName(rs.getString("full_name"));
			            String memberName=rs.getString("full_name");
			            if (!tempMemberName.equalsIgnoreCase(memberName)) {
							nomRegVO.setMemberName(memberName);
							tempMemberName = memberName;
						} else {
							nomRegVO.setMemberName(" ");
						}
			            nomRegVO.setNomineeName(rs.getString("nominee_name"));
			            nomRegVO.setRelation(rs.getString("relation"));
			            nomRegVO.setRemark(rs.getString("remark"));
			            nomRegVO.setShare(rs.getBigDecimal("share"));
			            nomRegVO.setNominationDate(rs.getString("issue_date"));
			            nomRegVO.setRecordingDate(rs.getString("recording_date"));
			            nomRegVO.setRevocationDate(rs.getString("revocation_date"));
			            nomRegVO.setSameAddress(rs.getInt("has_address"));
			            if(rs.getString("city")==null){
			            	nomRegVO.setAddressString("");
			            }else
			            nomRegVO.setAddressString(rs.getString("line_1")+","+rs.getString("line_2")+","+rs.getString("city"));
			           
			            return nomRegVO;
				}
			};
			nominationList=  namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);

			
			logger.debug("Exit:	public List getNominationListForMember(int memberId)"+nominationList.size());
		} catch (NullPointerException ex) {
			logger.info("No nominee found for memberID : "+memberId);		
		
		}catch (Exception ex) {
			logger.error("Exception in getNominationListForMember : "+ex);

		}
		return nominationList;

	}
	
	public int deleteNominees(NominationRegisterVO nominationRegisterVO){
		logger.debug("Entry : public int deleteNominees(String memberId)");
		int success = 0;
		try {
			
			 String	strSQL = "DELETE FROM nominee_details WHERE member_id=:memberId;" ;
				logger.debug("query : " + strSQL);
				// 2. Parameters.
				Map namedParameters = new HashMap();
				namedParameters.put("memberId", nominationRegisterVO.getMemberId());
				
				success = namedParameterJdbcTemplate.update(strSQL,namedParameters);
		
			
		
			
		} catch (Exception e) {
			logger.error("Exception ocurred at public int deleteNominees(String memberId)", e);
		}
		
			
		logger.debug("Exit : public int deleteNominees(String memberId)");	
		return success;
		}
	
	
	public int updateRecordingDate(NominationRegisterVO nomRegVO){
		logger.debug("Entry : updateRecordingDate(NominationRegisterVO nomRegVO)"+nomRegVO.getMemberId());
		int success = 0;
		try {
			
			 String	strSQL = "Update nominee_details set recording_date=:recordingDate, revocation_date=:revocationDate WHERE member_id=:memberId and revocation_date is null ;" ;
				logger.debug("query : " + strSQL);
				// 2. Parameters.
				Map namedParameters = new HashMap();
				namedParameters.put("memberId", nomRegVO.getMemberId());
				namedParameters.put("recordingDate", nomRegVO.getRecordingDate());
				namedParameters.put("revocationDate", nomRegVO.getRevocationDate());
				success = namedParameterJdbcTemplate.update(strSQL,namedParameters);
					
		} catch (Exception e) {
			logger.error("Exception ocurred at updateRecordingDate(NominationRegisterVO nomRegVO)", e);
		}
		
			
		logger.debug("Exit : updateRecordingDate(NominationRegisterVO nomRegVO)");	
		return success;
		}
	
	public List getNominationMissingList(int buildingID,int societyId) {

		List nominationList=null;
		
		String strWhereClause="";
		
		if(buildingID==0)
			strWhereClause=" ";
			else
				strWhereClause="  AND m.building_id="+buildingID+"";

		try {
			logger.debug("Entry : public List getNominationList(int societyId)"+societyId);
	
		//jdbcTemplate.execute(createNomineeAddressView);
		
		String strSQL = "SELECT m.*,s.society_name,s.short_name,s.mailingList_committee FROM (members_info m,society_details s) LEFT JOIN nominee_details n ON n.member_id=m.member_id  WHERE m.society_id=s.society_id AND m.society_id=:societyID "+strWhereClause+" AND m.type='P' AND n.nominee_id IS NULL ORDER BY member_id ; "; 
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("societyID", societyId);
			
			//3. RowMapper.
			
			RowMapper RMapper = new RowMapper() {
				String tempAptName = "";
				String tempMemberName="";
				int i=0;
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					
					  MemberVO membrs = new MemberVO();
			            membrs.setMemberID(rs.getInt("member_id"));
			            membrs.setMembershipID(rs.getInt("membership_id"));
			            membrs.setOldEmailID(rs.getString("email"));
			            membrs.setAptID(rs.getInt("apt_id"));
			            membrs.setFlatNo(rs.getString("unit_name"));
			            membrs.setTitle(rs.getString("title"));
			            membrs.setFullName(rs.getString("full_name"));
			            membrs.setOccupation(rs.getString("occupation"));
			            membrs.setMobile(rs.getString("mobile"));
			            membrs.setPhone(rs.getString("phone"));
			            membrs.setPhone(rs.getString("phone"));
			            membrs.setEmail(rs.getString("email"));
			            membrs.setMembershipDate(rs.getString("issue_date"));
			            membrs.setDateOfBirth(rs.getString("dob"));			      
			            membrs.setSocietyID(rs.getInt("society_id"));
			            membrs.setAge(rs.getInt("age"));
			            membrs.setType(rs.getString("type"));
			            membrs.setComitteeGroupEmail(rs.getString("mailingList_committee"));
			            membrs.setSocietyName(rs.getString("society_name"));
			            membrs.setSocietyShortName(rs.getString("short_name"));
			            return membrs;
				}
			};
			nominationList=  namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);

			
			logger.debug("Exit:	public List getNominationList(int societyId)");
		} catch (NullPointerException ex) {
			logger.info("No missing nominee found for societyID : "+societyId);

			
		} catch (Exception ex) {
			logger.error("Exception in getNominationList : "+ex);

		}
		return nominationList;

	}
	
	
	 public List getNominationRegistrationNoList(int societyID){
	    	List nominationList=null;
	    	
	    	try {
	    		logger.debug("Entry : public List getNominationRegistrationNoList(int societyID)");
	    		
	    		String query= "SELECT nomination_id FROM members_info m,nominee_details n WHERE n.member_id=m.member_id  AND m.society_id=:societyID ";
	    	logger.debug("query : " + query);
	    	Map namedParameters = new HashMap();
	    	namedParameters.put("societyID", societyID);
	    	
	    	
	    	RowMapper RMapper = new RowMapper() {
	    		public Object mapRow(ResultSet rs, int rowNum)
	    				throws SQLException {
	    			NominationRegisterVO nomRegVO=new NominationRegisterVO();
	    			    nomRegVO.setNominationRegisterID(rs.getInt("nomination_id"));
	    	      	return nomRegVO;
	    		}
	    	};
	    	
	    	nominationList =  namedParameterJdbcTemplate.query(query, namedParameters, RMapper);
	    		
	    	logger.debug("Exit : public List getNominationRegistrationNoList(int societyID)"+nominationList.size());
	    	} catch (DataAccessException e) {
	    	    logger.error("DataAccessException in public List getNominationRegistrationNoList(int societyID) : "+e);
	    	}
	    	catch (Exception ex) {
	    		logger.error("Exception in public List getNominationRegistrationNoList(int societyID) : "+ex);
	    	}
	    	return nominationList;
	    }
	 
	 public int incrementNominationRegistrationNumber(String societyiD){
			int nominationRegNO=0;
			try
			{
				logger.debug("Entry :public int incrementNominationRegistrationNumber(String societyiD)");
				
				String sqlQry=" Update society_settings set nomination_counter=nomination_counter+1 where society_id=:societyID" ;
						
				Map hMap=new HashMap();
				hMap.put("societyID", societyiD);
				
				
				
				nominationRegNO=namedParameterJdbcTemplate.update(sqlQry, hMap);
				
			}catch(Exception ex)
			{
				ex.printStackTrace();
				logger.error("Exception in incrementNominationRegistrationNumber : "+ex);
				
			}
				logger.debug("Exit : public int incrementNominationRegistrationNumber(String societyiD)");
			return nominationRegNO;
			}
	
		public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	public AddressDAO getAddressDAO() {
		return addressDAO;
	}
	public void setAddressDAO(AddressDAO addressDAO) {
		this.addressDAO = addressDAO;
	}
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	
	
}
