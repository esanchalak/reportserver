package com.emanager.server.adminReports.valueObject;
import java.util.List;
public class RptDocumentVO extends PrintReportVO {
	
	private int societyID;
	private int buildingID;
	private String builidingName;
	private int memberID;
	private String unitName;
	private String memberName;
	private String index2;
	private String saleDeed;
	private String primaryForm;
	private String associateForm;
	private String nominationForm;
	private String parkingLetter;
	private String possessioLetter;
	private String shareCertificate;
	private List summaryDocs;
	private List receivedDocs;
	private int total;
	private int documentID;
	private String documentName;
	private int documentStatusID;
	private String documentStatusName;
	private String type;
	private String description;
	private String descriptionHistory;
	private List objectList;
	private int documentMapID;
	private String updaterName;
	
	public String getIndex2() {
		return index2;
	}
	public void setIndex2(String index2) {
		this.index2 = index2;
	}
	public String getSaleDeed() {
		return saleDeed;
	}
	public void setSaleDeed(String saleDeed) {
		this.saleDeed = saleDeed;
	}
	public String getPrimaryForm() {
		return primaryForm;
	}
	public void setPrimaryForm(String primaryForm) {
		this.primaryForm = primaryForm;
	}
	public String getAssociateForm() {
		return associateForm;
	}
	public void setAssociateForm(String associateForm) {
		this.associateForm = associateForm;
	}
	public String getNominationForm() {
		return nominationForm;
	}
	public void setNominationForm(String nominationForm) {
		this.nominationForm = nominationForm;
	}
	public String getParkingLetter() {
		return parkingLetter;
	}
	public void setParkingLetter(String parkingLetter) {
		this.parkingLetter = parkingLetter;
	}
	public String getPossessioLetter() {
		return possessioLetter;
	}
	public void setPossessioLetter(String possessioLetter) {
		this.possessioLetter = possessioLetter;
	}
	public String getShareCertificate() {
		return shareCertificate;
	}
	public void setShareCertificate(String shareCertificate) {
		this.shareCertificate = shareCertificate;
	}
	public List getReceivedDocs() {
		return receivedDocs;
	}
	public void setReceivedDocs(List receivedDocs) {
		this.receivedDocs = receivedDocs;
	}
	public String getUpdaterName() {
		return updaterName;
	}
	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName;
	}
	public int getDocumentMapID() {
		return documentMapID;
	}
	public void setDocumentMapID(int documentMapID) {
		this.documentMapID = documentMapID;
	}
	public List getObjectList() {
		return objectList;
	}
	public void setObjectList(List objectList) {
		this.objectList = objectList;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDescriptionHistory() {
		return descriptionHistory;
	}
	public void setDescriptionHistory(String descriptionHistory) {
		this.descriptionHistory = descriptionHistory;
	}
	public int getDocumentStatusID() {
		return documentStatusID;
	}
	public void setDocumentStatusID(int documentStatusID) {
		this.documentStatusID = documentStatusID;
	}
	public int getDocumentID() {
		return documentID;
	}
	public void setDocumentID(int documentID) {
		this.documentID = documentID;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	
	public String getDocumentStatusName() {
		return documentStatusName;
	}
	public void setDocumentStatusName(String documentStatusName) {
		this.documentStatusName = documentStatusName;
	}
	
	public int getBuildingID() {
		return buildingID;
	}
	public void setBuildingID(int buildingID) {
		this.buildingID = buildingID;
	}
	public String getBuilidingName() {
		return builidingName;
	}
	public void setBuilidingName(String builidingName) {
		this.builidingName = builidingName;
	}
	
	public int getMemberID() {
		return memberID;
	}
	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	public List getSummaryDocs() {
		return summaryDocs;
	}
	public void setSummaryDocs(List summaryDocs) {
		this.summaryDocs = summaryDocs;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	
	
}
