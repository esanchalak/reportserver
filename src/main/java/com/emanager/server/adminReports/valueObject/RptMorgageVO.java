package com.emanager.server.adminReports.valueObject;

import java.math.BigDecimal;
import java.sql.Date;

public class RptMorgageVO {
	
	private String morgId;
    private int societyId;	
	private int memberId;
	private int aptId;
	private String flatNo;
	private String mobile;
	private String memberName;
	private String bankId;
	private String bankName;
	private String branchCode;	
	private BigDecimal morgAmount;
	private String morgStatus;	
	private String fromDate;	
    private String toDate;
    private String notes;
    private String comments;
    private String IsDeleted;
    
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getToDate() {
		return toDate;
	}
	
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public BigDecimal getMorgAmount() {
		return morgAmount;
	}
	public void setMorgAmount(BigDecimal morgAmount) {
		this.morgAmount = morgAmount;
	}
	public String getMorgStatus() {
		return morgStatus;
	}
	public void setMorgStatus(String morgStatus) {
		this.morgStatus = morgStatus;
	}
	
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getFlatNo() {
		return flatNo;
	}
	public void setFlatNo(String flatNo) {
		this.flatNo = flatNo;
	}
	public String getMorgId() {
		return morgId;
	}
	public void setMorgId(String morgId) {
		this.morgId = morgId;
	}
	public String getBankId() {
		return bankId;
	}
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getIsDeleted() {
		return IsDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		IsDeleted = isDeleted;
	}
	public int getSocietyId() {
		return societyId;
	}
	public void setSocietyId(int societyId) {
		this.societyId = societyId;
	}
	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	public int getAptId() {
		return aptId;
	}
	public void setAptId(int aptId) {
		this.aptId = aptId;
	}

}
