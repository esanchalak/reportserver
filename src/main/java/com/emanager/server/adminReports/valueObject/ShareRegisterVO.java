package com.emanager.server.adminReports.valueObject;

import java.math.BigDecimal;
import java.util.Date;

import com.emanager.server.commonUtils.valueObject.AddressVO;

public class ShareRegisterVO {
	
	private String societyId;
	private String societyName;
	private String aptName;
	private int aptId;
	private int memberId;
	private String memberName;
	private String allotmentDate;
	private int shareId;
	private String shareCertNo;
	private int shareFrom;
	private int shareTo;
	private int noOfShares;
	private BigDecimal faceValue;
	private BigDecimal totalValue;
	private String memberShipId;
	
	public String getMemberShipId() {
		return memberShipId;
	}
	public void setMemberShipId(String memberShipId) {
		this.memberShipId = memberShipId;
	}
	public String getAllotmentDate() {
		return allotmentDate;
	}
	public void setAllotmentDate(String allotmentDate) {
		this.allotmentDate = allotmentDate;
	}
	public int getAptId() {
		return aptId;
	}
	public void setAptId(int aptId) {
		this.aptId = aptId;
	}
	public String getAptName() {
		return aptName;
	}
	public void setAptName(String aptName) {
		this.aptName = aptName;
	}
	public BigDecimal getFaceValue() {
		return faceValue;
	}
	public void setFaceValue(BigDecimal faceValue) {
		this.faceValue = faceValue;
	}
	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	
	public int getNoOfShares() {
		return noOfShares;
	}
	public void setNoOfShares(int noOfShares) {
		this.noOfShares = noOfShares;
	}
	public String getShareCertNo() {
		return shareCertNo;
	}
	public void setShareCertNo(String shareCertNo) {
		this.shareCertNo = shareCertNo;
	}
	public int getShareFrom() {
		return shareFrom;
	}
	public void setShareFrom(int shareFrom) {
		this.shareFrom = shareFrom;
	}
	public int getShareId() {
		return shareId;
	}
	public void setShareId(int shareId) {
		this.shareId = shareId;
	}
	public int getShareTo() {
		return shareTo;
	}
	public void setShareTo(int shareTo) {
		this.shareTo = shareTo;
	}
	public String getSocietyId() {
		return societyId;
	}
	public void setSocietyId(String societyId) {
		this.societyId = societyId;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	public BigDecimal getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}

}
