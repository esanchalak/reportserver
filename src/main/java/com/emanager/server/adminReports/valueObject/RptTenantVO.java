package com.emanager.server.adminReports.valueObject;
import java.util.List;
public class RptTenantVO extends PrintReportVO
{
	private String name;
	private String mobile;
	private String email;
	private String title;
	private int aptID;
	private String apt_name;
	private String building_name;
	private String agg_start_date;
	private String agg_end_date;
	public String isDocsReceved;
	public String isNRI;
	public String isVerified;
	public int cntNtDocsRecvd;
	public int cntNtVerify;
	public int cntNRI;
	public int cntDocsRecvd;
	public int cntVerify;
	public int cntRI;
	public int noOfMissingInfo;
	public int total;
	public List tenantList;
    private int societyID;
    private int renterID;
    private String ownerName;
    private String ownerMobile;
    private String ownerEmail;
    private int memberID;
    
    
    public int getRenterID() {
		return renterID;
	}
	public void setRenterID(int renterID) {
		this.renterID = renterID;
	}	
	
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	
	public int getCntNtDocsRecvd() {
		return cntNtDocsRecvd;
	}
	public void setCntNtDocsRecvd(int cntNtDocsRecvd) {
		this.cntNtDocsRecvd = cntNtDocsRecvd;
	}
	public int getCntNtVerify() {
		return cntNtVerify;
	}
	public void setCntNtVerify(int cntNtVerify) {
		this.cntNtVerify = cntNtVerify;
	}
	
	public int getCntNRI() {
		return cntNRI;
	}
	public void setCntNRI(int cntNRI) {
		this.cntNRI = cntNRI;
	}
	public String getIsDocsReceved() {
		return isDocsReceved;
	}
	public void setIsDocsReceved(String isDocsReceved) {
		this.isDocsReceved = isDocsReceved;
	}
	public String getIsNRI() {
		return isNRI;
	}
	public void setIsNRI(String isNRI) {
		this.isNRI = isNRI;
	}
	public String getIsVerified() {
		return isVerified;
	}
	public void setIsVerified(String isVerified) {
		this.isVerified = isVerified;
	}
	public String getAgg_end_date() {
		return agg_end_date;
	}
	public void setAgg_end_date(String agg_end_date) {
		this.agg_end_date = agg_end_date;
	}
	public String getAgg_start_date() {
		return agg_start_date;
	}
	public void setAgg_start_date(String agg_start_date) {
		this.agg_start_date = agg_start_date;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getBuilding_name() {
		return building_name;
	}
	public void setBuilding_name(String building_name) {
		this.building_name = building_name;
	}
	public String getApt_name() {
		return apt_name;
	}
	public void setApt_name(String apt_name) {
		this.apt_name = apt_name;
	}
	public int getCntDocsRecvd() {
		return cntDocsRecvd;
	}
	public void setCntDocsRecvd(int cntDocsRecvd) {
		this.cntDocsRecvd = cntDocsRecvd;
	}
	public int getCntRI() {
		return cntRI;
	}
	public void setCntRI(int cntRI) {
		this.cntRI = cntRI;
	}
	public int getCntVerify() {
		return cntVerify;
	}
	public void setCntVerify(int cntVerify) {
		this.cntVerify = cntVerify;
	}
	public int getNoOfMissingInfo() {
		return noOfMissingInfo;
	}
	public void setNoOfMissingInfo(int noOfMissingInfo) {
		this.noOfMissingInfo = noOfMissingInfo;
	}
	public List getTenantList() {
		return tenantList;
	}
	public void setTenantList(List tenantList) {
		this.tenantList = tenantList;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getTitle() {
		return title;
	}
	public int getAptID() {
		return aptID;
	}
	public void setAptID(int aptID) {
		this.aptID = aptID;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the ownerName
	 */
	public String getOwnerName() {
		return ownerName;
	}
	/**
	 * @return the ownerMobile
	 */
	public String getOwnerMobile() {
		return ownerMobile;
	}
	/**
	 * @return the ownerEmail
	 */
	public String getOwnerEmail() {
		return ownerEmail;
	}
	/**
	 * @param ownerName the ownerName to set
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	/**
	 * @param ownerMobile the ownerMobile to set
	 */
	public void setOwnerMobile(String ownerMobile) {
		this.ownerMobile = ownerMobile;
	}
	/**
	 * @param ownerEmail the ownerEmail to set
	 */
	public void setOwnerEmail(String ownerEmail) {
		this.ownerEmail = ownerEmail;
	}
	/**
	 * @return the memberID
	 */
	public int getMemberID() {
		return memberID;
	}
	/**
	 * @param memberID the memberID to set
	 */
	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}

}
