package com.emanager.server.adminReports.valueObject;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonInclude(Include.NON_NULL)
public class NominationRegisterVO extends PrintReportVO{
	
	private String societyId;
	private String societyName;
	private String aptName;
	private int aptId;
	private int memberId;
	private int nominationId;
	private int isMinor;
	private String memberName;
	private String nomineeName;
	private String nominationDate;
	private String recordingDate;
	private String revocationDate;
	private String dob;
	private String relation;
	private BigDecimal share;
	private AddressVO address;
	private String remark;
	private int sameAddress;
	private int isAvailable;
	private String addressString;
	private String srNo;
	private int missingNomination;
	private List<NominationRegisterVO> nomineeList;
	private int documentStatusID;
	private String documentStatusName;
	private MemberVO memberVO;
	private int nominationRegisterID;
	private Boolean autoInc;
	
	public int getDocumentStatusID() {
		return documentStatusID;
	}
	public void setDocumentStatusID(int documentStatusID) {
		this.documentStatusID = documentStatusID;
	}
	public String getDocumentStatusName() {
		return documentStatusName;
	}
	public void setDocumentStatusName(String documentStatusName) {
		this.documentStatusName = documentStatusName;
	}
	public List<NominationRegisterVO> getNomineeList() {
		return nomineeList;
	}
	public void setNomineeList(List<NominationRegisterVO> nomineeList) {
		this.nomineeList = nomineeList;
	}
	public AddressVO getAddress() {
		return address;
	}
	public void setAddress(AddressVO address) {
		this.address = address;
	}
	public int getAptId() {
		return aptId;
	}
	public void setAptId(int aptId) {
		this.aptId = aptId;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getNominationDate() {
		return nominationDate;
	}
	public void setNominationDate(String nominationDate) {
		this.nominationDate = nominationDate;
	}
	public int getNominationId() {
		return nominationId;
	}
	public void setNominationId(int nominationId) {
		this.nominationId = nominationId;
	}
	public String getNomineeName() {
		return nomineeName;
	}
	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}
	public String getRecordingDate() {
		return recordingDate;
	}
	public void setRecordingDate(String recordingDate) {
		this.recordingDate = recordingDate;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getRevocationDate() {
		return revocationDate;
	}
	public void setRevocationDate(String revocationDate) {
		this.revocationDate = revocationDate;
	}
	public BigDecimal getShare() {
		return share;
	}
	public void setShare(BigDecimal share) {
		this.share = share;
	}
	public String getSocietyId() {
		return societyId;
	}
	public void setSocietyId(String societyId) {
		this.societyId = societyId;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	public String getAptName() {
		return aptName;
	}
	public void setAptName(String aptName) {
		this.aptName = aptName;
	}
	public int getIsMinor() {
		return isMinor;
	}
	public void setIsMinor(int isMinor) {
		this.isMinor = isMinor;
	}
	public int getSameAddress() {
		return sameAddress;
	}
	public void setSameAddress(int sameAddress) {
		this.sameAddress = sameAddress;
	}
	public int getIsAvailable() {
		return isAvailable;
	}
	public void setIsAvailable(int isAvailable) {
		this.isAvailable = isAvailable;
	}
	public String getAddressString() {
		return addressString;
	}
	public void setAddressString(String addressString) {
		this.addressString = addressString;
	}
	public String getSrNo() {
		return srNo;
	}
	public void setSrNo(String srNo) {
		this.srNo = srNo;
	}
	public int getMissingNomination() {
		return missingNomination;
	}
	public void setMissingNomination(int missingNomination) {
		this.missingNomination = missingNomination;
	}
	public MemberVO getMemberVO() {
		return memberVO;
	}
	public void setMemberVO(MemberVO memberVO) {
		this.memberVO = memberVO;
	}
	/**
	 * @return the nominationRegisterID
	 */
	public int getNominationRegisterID() {
		return nominationRegisterID;
	}
	/**
	 * @return the autoInc
	 */
	public Boolean getAutoInc() {
		return autoInc;
	}
	/**
	 * @param nominationRegisterID the nominationRegisterID to set
	 */
	public void setNominationRegisterID(int nominationRegisterID) {
		this.nominationRegisterID = nominationRegisterID;
	}
	/**
	 * @param autoInc the autoInc to set
	 */
	public void setAutoInc(Boolean autoInc) {
		this.autoInc = autoInc;
	}
	
	
	
	
	
	
	
	
}
