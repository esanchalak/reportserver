package com.emanager.server.adminReports.valueObject;

import java.util.List;

public class RptVehicleVO extends PrintReportVO {
	
	private int societyID;
	private int buildingID;
	private String builidingName;
	private int memberID;
	private String unitName;
	private String memberName;
	private String stickerId;
	private int commercial;
	private String vehicleNumber;
	private String vehicleType;
	private int twoWheeler;
	private int fourwheeler;
	private List summaryVehicles;
	private List vehiclesList;
	private String description;
	private int total;
	private int vehicleStatus;
	private int vehicleID;
	
	
	public String getStickerId() {
		return stickerId;
	}
	public void setStickerId(String stickerId) {
		this.stickerId = stickerId;
	}
	public int getBuildingID() {
		return buildingID;
	}
	public void setBuildingID(int buildingID) {
		this.buildingID = buildingID;
	}
	public String getBuilidingName() {
		return builidingName;
	}
	public void setBuilidingName(String builidingName) {
		this.builidingName = builidingName;
	}
	public int getCommercial() {
		return commercial;
	}
	public void setCommercial(int commercial) {
		this.commercial = commercial;
	}
	public int getFourwheeler() {
		return fourwheeler;
	}
	public void setFourwheeler(int fourwheeler) {
		this.fourwheeler = fourwheeler;
	}
	public int getMemberID() {
		return memberID;
	}
	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	
	public List getSummaryVehicles() {
		return summaryVehicles;
	}
	public void setSummaryVehicles(List summaryVehicles) {
		this.summaryVehicles = summaryVehicles;
	}
	public int getTwoWheeler() {
		return twoWheeler;
	}
	public void setTwoWheeler(int twoWheeler) {
		this.twoWheeler = twoWheeler;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public List getVehiclesList() {
		return vehiclesList;
	}
	public void setVehiclesList(List vehiclesList) {
		this.vehiclesList = vehiclesList;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}

	public int getVehicleStatus() {
		return vehicleStatus;
	}
	public void setVehicleStatus(int vehicleStatus) {
		this.vehicleStatus = vehicleStatus;
	}
	public int getVehicleID() {
		return vehicleID;
	}
	public void setVehicleID(int vehicleID) {
		this.vehicleID = vehicleID;
	}

}
