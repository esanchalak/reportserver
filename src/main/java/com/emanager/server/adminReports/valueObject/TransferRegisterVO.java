package com.emanager.server.adminReports.valueObject;

import java.math.BigDecimal;

public class TransferRegisterVO {
	
	private int orgID;
	private String societyName;
	private String aptName;
	private int aptID;
	private int memberID;
	private String newMemberName;
	private String oldMemberName;
	private String membershipDate;
	private int shareID;
	private String shareCertNo;
	private int shareFrom;
	private int shareTo;
	private int noOfShares;
	private BigDecimal faceValue;
	private BigDecimal totalValue;
	private int oldMemberShipID;
	private int newMemberShipID;
	private String transferDate;
	private String transferType;
	private String notes;
	/**
	 * @return the societyID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @return the societyName
	 */
	public String getSocietyName() {
		return societyName;
	}
	/**
	 * @return the aptName
	 */
	public String getAptName() {
		return aptName;
	}
	/**
	 * @return the aptID
	 */
	public int getAptID() {
		return aptID;
	}
	/**
	 * @return the memberID
	 */
	public int getMemberID() {
		return memberID;
	}
	/**
	 * @return the newMemberName
	 */
	public String getNewMemberName() {
		return newMemberName;
	}
	/**
	 * @return the oldMemberName
	 */
	public String getOldMemberName() {
		return oldMemberName;
	}
	/**
	 * @return the membershipDate
	 */
	public String getMembershipDate() {
		return membershipDate;
	}
	/**
	 * @return the shareID
	 */
	public int getShareID() {
		return shareID;
	}
	/**
	 * @return the shareCertNo
	 */
	public String getShareCertNo() {
		return shareCertNo;
	}
	/**
	 * @return the shareFrom
	 */
	public int getShareFrom() {
		return shareFrom;
	}
	/**
	 * @return the shareTo
	 */
	public int getShareTo() {
		return shareTo;
	}
	/**
	 * @return the noOfShares
	 */
	public int getNoOfShares() {
		return noOfShares;
	}
	/**
	 * @return the faceValue
	 */
	public BigDecimal getFaceValue() {
		return faceValue;
	}
	/**
	 * @return the totalValue
	 */
	public BigDecimal getTotalValue() {
		return totalValue;
	}
	/**
	 * @return the oldMemberShipID
	 */
	public int getOldMemberShipID() {
		return oldMemberShipID;
	}
	/**
	 * @return the newMemberShipID
	 */
	public int getNewMemberShipID() {
		return newMemberShipID;
	}
	/**
	 * @param societyID the societyID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	/**
	 * @param societyName the societyName to set
	 */
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	/**
	 * @param aptName the aptName to set
	 */
	public void setAptName(String aptName) {
		this.aptName = aptName;
	}
	/**
	 * @param aptID the aptID to set
	 */
	public void setAptID(int aptID) {
		this.aptID = aptID;
	}
	/**
	 * @param memberID the memberID to set
	 */
	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}
	/**
	 * @param newMemberName the newMemberName to set
	 */
	public void setNewMemberName(String newMemberName) {
		this.newMemberName = newMemberName;
	}
	/**
	 * @param oldMemberName the oldMemberName to set
	 */
	public void setOldMemberName(String oldMemberName) {
		this.oldMemberName = oldMemberName;
	}
	/**
	 * @param membershipDate the membershipDate to set
	 */
	public void setMembershipDate(String membershipDate) {
		this.membershipDate = membershipDate;
	}
	/**
	 * @param shareID the shareID to set
	 */
	public void setShareID(int shareID) {
		this.shareID = shareID;
	}
	/**
	 * @param shareCertNo the shareCertNo to set
	 */
	public void setShareCertNo(String shareCertNo) {
		this.shareCertNo = shareCertNo;
	}
	/**
	 * @param shareFrom the shareFrom to set
	 */
	public void setShareFrom(int shareFrom) {
		this.shareFrom = shareFrom;
	}
	/**
	 * @param shareTo the shareTo to set
	 */
	public void setShareTo(int shareTo) {
		this.shareTo = shareTo;
	}
	/**
	 * @param noOfShares the noOfShares to set
	 */
	public void setNoOfShares(int noOfShares) {
		this.noOfShares = noOfShares;
	}
	/**
	 * @param faceValue the faceValue to set
	 */
	public void setFaceValue(BigDecimal faceValue) {
		this.faceValue = faceValue;
	}
	/**
	 * @param totalValue the totalValue to set
	 */
	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}
	/**
	 * @param oldMemberShipID the oldMemberShipID to set
	 */
	public void setOldMemberShipID(int oldMemberShipID) {
		this.oldMemberShipID = oldMemberShipID;
	}
	/**
	 * @param newMemberShipID the newMemberShipID to set
	 */
	public void setNewMemberShipID(int newMemberShipID) {
		this.newMemberShipID = newMemberShipID;
	}
	/**
	 * @return the transferDate
	 */
	public String getTransferDate() {
		return transferDate;
	}
	/**
	 * @param transferDate the transferDate to set
	 */
	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}
	/**
	 * @return the transferType
	 */
	public String getTransferType() {
		return transferType;
	}
	/**
	 * @param transferType the transferType to set
	 */
	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}
	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}
	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	

}
