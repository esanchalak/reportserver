package com.emanager.server.adminReports.valueObject;

import java.util.HashMap;
import java.util.List;

import com.emanager.server.society.valueObject.SocietyVO;

public class PrintReportVO {
	
	protected SocietyVO societyVO;
	protected String reportID;
	protected String reportURL;
	protected String reportContentType;
	protected byte[] reportByteStream;
	private String sourceType;
	private List beanList;
	protected HashMap<String, Object> printParam;
	protected int statusCode;
	protected String statusDescription;
	
	
	public byte[] getReportByteStream() {
		return reportByteStream;
	}
	public void setReportByteStream(byte[] reportByteStream) {
		this.reportByteStream = reportByteStream;
	}
	public String getReportContentType() {
		return reportContentType;
	}
	public void setReportContentType(String reportContentType) {
		this.reportContentType = reportContentType;
	}
	public String getReportID() {
		return reportID;
	}
	public void setReportID(String reportID) {
		this.reportID = reportID;
	}
	public String getReportURL() {
		return reportURL;
	}
	public void setReportURL(String reportURL) {
		this.reportURL = reportURL;
	}
	public SocietyVO getSocietyVO() {
		return societyVO;
	}
	public void setSocietyVO(SocietyVO societyVO) {
		this.societyVO = societyVO;
	}
	public String getSourceType() {
		return sourceType;
	}
	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
	public List getBeanList() {
		return beanList;
	}
	public void setBeanList(List beanList) {
		this.beanList = beanList;
	}
	public HashMap<String, Object> getPrintParam() {
		return printParam;
	}
	public void setPrintParam(HashMap<String, Object> printParam) {
		this.printParam = printParam;
	}
	
	
}
