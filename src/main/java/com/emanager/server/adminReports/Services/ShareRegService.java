package com.emanager.server.adminReports.Services;

import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.adminReports.Domain.ShareRegDomain;
import com.emanager.server.adminReports.valueObject.ShareRegisterVO;


public class ShareRegService {
	ShareRegDomain shareRegDomain;
	private static final Logger logger = Logger.getLogger(ShareRegService.class);
	
	public List getShareList(int buildingId,int societyId){
	logger.debug("Entry : public List getShareList(int societyId)");
	List shareList = null;
	try {
		
	shareList=shareRegDomain.getShareList(buildingId,societyId);	
		
	} catch (Exception e) {
		logger.error("Exception ocurred at getShareList", e);
	}
	
		
	logger.debug("Exit : public List getShareList(int societyId)");	
	return shareList;
	}
    
	public ShareRegisterVO getShareDetails(int aptID){
		logger.debug("Entry : public ShareRegisterVO getShareDetails(int aptID)");
		ShareRegisterVO shareCertVO=new ShareRegisterVO();
		
		shareCertVO=shareRegDomain.getShareDetails(aptID);
		
		
		logger.debug("Exit : public ShareRegisterVO getShareDetails(int aptID)");	
		return shareCertVO;
		}
	
	
	public List getTransferRegister(int societyID,String fromDate,String toDate){
		logger.debug("Entry : public List getTransferRegister(int societyID,String fromDate,String toDate)");
		List transferRegister = null;
		try {
			
		transferRegister=shareRegDomain.getTransferRegister(societyID,fromDate,toDate);
			
		} catch (Exception e) {
			logger.error("Exception ocurred at public List getTransferRegister(int societyID,String fromDate,String toDate)", e);
		}
		
			
		logger.debug("Exit : public List getTransferRegister(int societyID,String fromDate,String toDate)");	
		return transferRegister;
		}
	
	/*public int addNominations(List nominationList){
		logger.debug("Entry : public int addNominations(List nominationList)");
		int success = 0;
		try {
			
		success=nominationRegDomain.addNominations(nominationList);	
			
		} catch (Exception e) {
			logger.error("Exception ocurred at addNominationList", e);
		}
		
			
		logger.debug("Exit : public int addNominations(List nominationList)");	
		return success;
		}
	public List getNominationListForMember(int memberId){
		logger.debug("Entry : public List getNominationListForMember(int memberId)");
		List nominationList=null;
		
		nominationList=nominationRegDomain.getNominationListForMember(memberId);
		
		
		logger.debug("Exit : public List getNominationListForMember(int memberId)");	
		return nominationList;
		}
	
	public int deleteNominees(String memberId){
		logger.debug("Entry : public int deleteNominees(String memberId)");
		int success = 0;
		try {
			
		success=nominationRegDomain.deleteNominees(memberId);	
			
		} catch (Exception e) {
			logger.error("Exception ocurred at public int deleteNominees(String memberId)", e);
		}
		
			
		logger.debug("Exit :public int deleteNominees(String memberId)");	
		return success;
		}
	
	
	public int updateRecordingDate(String recordingDate,String nominationID){
		logger.debug("Entry : updateRecordingDate(String recordingDate,String nominationID)");
		int success = 0;
		try {
			
			
				success = nominationRegDomain.updateRecordingDate(recordingDate, nominationID);
		
			
		
			
		} catch (Exception e) {
			logger.error("Exception ocurred at updateRecordingDate(String recordingDate,String nominationID)", e);
		}
		
			
		logger.debug("Exit : updateRecordingDate(String recordingDate,String nominationID)");	
		return success;
		}
	*/
	
	
	public ShareRegDomain getShareRegDomain() {
		return shareRegDomain;
	}

	public void setShareRegDomain(ShareRegDomain shareRegDomain) {
		this.shareRegDomain = shareRegDomain;
	}
}