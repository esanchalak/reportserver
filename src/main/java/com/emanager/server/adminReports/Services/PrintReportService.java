package com.emanager.server.adminReports.Services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.LedgerEntries;
import com.emanager.server.accounts.DataAccessObjects.LedgerTrnsVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.adminReports.Domain.PrintReportDomain;
import com.emanager.server.adminReports.valueObject.NominationRegisterVO;
import com.emanager.server.adminReports.valueObject.PrintReportVO;
import com.emanager.server.adminReports.valueObject.RptDocumentVO;
import com.emanager.server.adminReports.valueObject.RptTenantVO;
import com.emanager.server.adminReports.valueObject.RptVehicleVO;
import com.emanager.server.commonUtils.domainObject.CommonUtility;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.valueObject.DropDownVO;
import com.emanager.server.financialReports.domainObject.AmountInWords;
import com.emanager.server.financialReports.services.BalanceSheetService;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.financialReports.services.TrialBalanceService;
import com.emanager.server.financialReports.valueObject.ReportDetailsVO;
import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.financialReports.valueObject.RptMonthYearVO;
import com.emanager.server.financialReports.valueObject.RptTransactionVO;
import com.emanager.server.invoice.Services.InvoiceService;
import com.emanager.server.invoice.dataAccessObject.BillDetailsVO;
import com.emanager.server.invoice.dataAccessObject.InLineItemsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceDetailsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceVO;
import com.emanager.server.invoice.dataAccessObject.ProfileDetailsVO;
import com.emanager.server.projects.service.ProjectDetailsService;
import com.emanager.server.projects.valueObjects.ProjectDetailsVO;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.BankInfoVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;


public class PrintReportService {

	private static final Logger logger = Logger.getLogger(PrintReportService.class);
	
	PrintReportDomain printReportDomain;
	TransactionService transactionService;
	MemberService memberService;
	SocietyService societyService;
	ReportService reportService;
	InvoiceService invoiceService;
	LedgerService ledgerService;
	TrialBalanceService trialBalanceService;
	BalanceSheetService balanceSheetService;
	DateUtility dateutility = new DateUtility();
	ShareRegService shareRegService;
	NominationRegService nominationRegService;
	CommonUtility commonUtilty = new CommonUtility();
	DateUtility dateUtil=new DateUtility();
	AmountInWords amtInWrds=new AmountInWords();
	ProjectDetailsService projectDetailsService;
	
	// print report method
	public PrintReportVO printReport(PrintReportVO printReportVO)  {
	  logger.debug("Entry : public ReportVO printReport(PrintReportVO printReportVO) "+printReportVO.getSourceType());
		 try{ 
	  if (printReportVO.getSourceType().equalsIgnoreCase("DATABASE")){ 
		  printReportVO=printReportDomain.printDBReport(printReportVO);
	   }else if(printReportVO.getSourceType().equalsIgnoreCase("BEAN")){
		   printReportVO=printReportDomain.printBeanReport(printReportVO);
	   }
		 }catch (Exception e) {
			logger.error("Exception in printReport: "+e);
		}
	 logger.debug("Exit : public ReportVO printReport(PrintReportVO printReportVO) ");
		return printReportVO;
	}
    
	public PrintReportVO printIndividualLedgerReport(TransactionVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printIndividualLedgerReport(TransactionVO printTxVO) "+printTxVO.getFromDate());
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
    	LedgerTrnsVO ledgerTxVO=new LedgerTrnsVO();
        List collection = new ArrayList();
        List transactionList=new ArrayList();
        AccountHeadVO accountHeadVO=new AccountHeadVO();
        
           try{
               //fill trasaction vo
               String societyId=(String) printTxVO.getPrintParam().get("orgID");
               ledgerTxVO=ledgerService.getQuickLedgerTransactionList(Integer.parseInt(societyId), printTxVO.getLedgerID(),printTxVO.getFromDate(), printTxVO.getToDate(),"ALL");
              //transactionList=transactionService.getTransctionList(Integer.parseInt(societyId), printTxVO.getLedgerID(),printTxVO.getFromDate(), printTxVO.getToDate(), "All");
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));                 
               HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
               printTxVO.setReportID(temmplateHashmap.get("memberLedger").trim());
               
               accountHeadVO.setDrOpnBal(ledgerTxVO.getDrOpnBalance());
               accountHeadVO.setDrClsBal(ledgerTxVO.getDrClsBalance());
               accountHeadVO.setCrOpnBal(ledgerTxVO.getCrOpnBalance());
               accountHeadVO.setCrClsBal(ledgerTxVO.getCrClsBalance());
               transactionList=ledgerTxVO.getTransactionList();               
               
               printTxVO.setLedgerName(ledgerTxVO.getLedgerName());
               printTxVO.setAccountHeadVO(accountHeadVO);
               printTxVO.setLedgerEntries(transactionList);
               printTxVO.setSocietyVO(societyVO);
               
               collection.add(printTxVO);
               
               //fill printreportVO
               printReportVO.setPrintParam( printTxVO.getPrintParam());
               printReportVO.setReportContentType(printTxVO.getReportContentType());
               printReportVO.setReportID(printTxVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printTxVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : public PrintReportVO printIndividualLedgerReport(TransactionVO printTxVO) ");
          }
       logger.debug("Exit : public PrintReportVO printIndividualLedgerReport(TransactionVO printTxVO)  ");
          return printReportVO;
  }
	
	public PrintReportVO printAllMemberLedgerReport(TransactionVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printAllMemberLedgerReport(TransactionVO printTxVO)  ");
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
                   
        List memberList=new ArrayList();
        List ledgerTxList=new ArrayList();              
        
             try{
                   //fill trasaction vo                     
                   String societyId=(String) printTxVO.getPrintParam().get("orgID");
                  
                   societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));  
                   HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
                   printTxVO.setReportID(temmplateHashmap.get("societyMemberLedger").trim());
                   
                   memberList=memberService.getMemberListForSearchByName(societyId);
                   logger.info("Member List :" +memberList.size()+societyVO.getSocietyName()); 
                  
                   for(int i=0;memberList.size()>i;i++){
                        
                         MemberVO memberVO= (MemberVO) memberList.get(i);
                         TransactionVO txVO=new TransactionVO();
                         LedgerTrnsVO ledgerTxVO=new LedgerTrnsVO();
                         List transactionList=new ArrayList();
                         AccountHeadVO accountHeadVO =new AccountHeadVO();
                         
                         ledgerTxVO=ledgerService.getQuickLedgerTransactionList(Integer.parseInt(societyId), memberVO.getLedgerID(),printTxVO.getFromDate(), printTxVO.getToDate(),"ALL");
                         accountHeadVO.setDrOpnBal(ledgerTxVO.getDrOpnBalance());
                         accountHeadVO.setDrClsBal(ledgerTxVO.getDrClsBalance());
                         accountHeadVO.setCrOpnBal(ledgerTxVO.getCrOpnBalance());
                         accountHeadVO.setCrClsBal(ledgerTxVO.getCrClsBalance());
                         if(ledgerTxVO.getTransactionList().size()>0){
                        	 transactionList=ledgerTxVO.getTransactionList();
                         }
      
                         logger.info("Ledger ID: "+ memberVO.getLedgerID()+" Name: "+ memberVO.getFlatNo()+" - "+memberVO.getFullName()+" Transaction Size: "+transactionList.size());
                        
                                  txVO.setLedgerName(memberVO.getFlatNo()+" - "+memberVO.getFullName());
                                   txVO.setFromDate(printTxVO.getFromDate());
                                   txVO.setToDate(printTxVO.getToDate());     
                                   txVO.setSocietyVO(societyVO);
                                   txVO.setAccountHeadVO(accountHeadVO);                                 
                                   txVO.setLedgerEntries(transactionList);                                 
                                   ledgerTxList.add(txVO);
                   }
                                  
                //fill printreportVO
                   
                 printReportVO.setPrintParam( printTxVO.getPrintParam());
                 printReportVO.setReportContentType(printTxVO.getReportContentType());
                 printReportVO.setReportID(printTxVO.getReportID());
                 printReportVO.setBeanList(ledgerTxList);
                 printReportVO.setSourceType(printTxVO.getSourceType());
                 printReportVO= this.printReport(printReportVO);
                 
             
             }catch (Exception e) {
                  logger.error("Exception : public PrintReportVO printAllMemberLedgerReport(TransactionVO printTxVO)  ");
            }
      
       logger.debug("Exit : public PrintReportVO printAllMemberLedgerReport(TransactionVO printTxVO)   ");
            return printReportVO;
}

	
	public PrintReportVO printLedgerDueReport(RptTransactionVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printLedgerDueReport(RptTransactionVO printTxVO)  "+printTxVO.getBuildingId()+printTxVO.getDisplayDate());
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();
        List ledgerDueList=new ArrayList();
        List buildingList=new ArrayList();
        List summaryList=new ArrayList();
           try{
               //fill trasaction vo
               String societyId=(String) printTxVO.getPrintParam().get("orgID");
               ledgerDueList=reportService.getMemberDueWithClosingBal(printTxVO.getDisplayDate(), Integer.parseInt(societyId), 0);
               
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));     
               HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
               printTxVO.setReportID(temmplateHashmap.get("ledgerDueReport").trim());
              // buildingList=memberService.getBuildingList(Integer.parseInt(societyId), "0");
               
               try {
				/*for(int i=0;i<buildingList.size();i++){
					   List transactionList=new ArrayList();
					   RptTransactionVO transactionVO=new RptTransactionVO();
					   DropDownVO buildingVO= (DropDownVO) buildingList.get(i);
					   
					  for(int j=0;j<ledgerDueList.size();j++){
						  
						   RptTransactionVO ledgerDueVO=(RptTransactionVO) ledgerDueList.get(j);
						  
						   if((ledgerDueVO.getBuildingId().equalsIgnoreCase(buildingVO.getData())||(ledgerDueVO.getBuildingId().equalsIgnoreCase("0")))){  
							 //  logger.info(" Buuilding: "+buildingVO.getLabel()+" Building Id "+ ledgerDueVO.getBuildingId()+ " LEdger ID: "+ledgerDueVO.getLedgerID());
							   transactionList.add(ledgerDueVO);
							   transactionVO.setDisplayDate(printTxVO.getDisplayDate());
							   transactionVO.setDueDetails(transactionList);
							   transactionVO.setSocietyVO(societyVO);
							   } 
					   }     
					  
					  
					   collection.add(transactionVO);           	   
					  
				   }*/
            	   RptTransactionVO transactionVO=new RptTransactionVO();            	   
				   transactionVO.setDisplayDate(printTxVO.getDisplayDate());
				   transactionVO.setDueDetails(ledgerDueList);
				   transactionVO.setSocietyVO(societyVO);
				   collection.add(transactionVO);  
			} catch (RuntimeException e) {
				logger.error("Exception occurred in printLedgerDueReport "+e);
			}                             
              
               
               //fill printreportVO
               printReportVO.setPrintParam( printTxVO.getPrintParam());
               printReportVO.setReportContentType(printTxVO.getReportContentType());
               printReportVO.setReportID(printTxVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printTxVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : public PrintReportVO printLedgerDueReport(RptTransactionVO printTxVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO (RptTransactionVO printTxVO)  ");
          return printReportVO;
  }
	
	
	public PrintReportVO printLedgerDueDetailsReport(RptTransactionVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printLedgerDueDetailsReport(RptTransactionVO printTxVO)  "+printTxVO.getDisplayDate());
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();
        List transactionList=new ArrayList();
        List ledgerDetailList=new ArrayList();
        List memberLedgerList=new ArrayList();
           try{
               //fill trasaction vo
               String societyId=(String) printTxVO.getPrintParam().get("society_id");
               transactionList=reportService.getMemberDueWithClosingBal(printTxVO.getDisplayDate(), Integer.parseInt(societyId),Integer.parseInt(printTxVO.getBuildingId()));
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));   
               ledgerDetailList=reportService.getLedgerDueDetails(Integer.parseInt(societyId),printTxVO.getBuildingId(), printTxVO.getDisplayDate());
               
          for(int i=0;transactionList.size()>i;i++){
            	             	  
            RptTransactionVO transactionVO = new RptTransactionVO();
            transactionVO = (RptTransactionVO) transactionList.get(i);   
         //logger.info("Member Id "+transactionVO.getMember_id())  ; 
			List memberLedger=new ArrayList();
			RptTransactionVO reportVO=new RptTransactionVO(); 
			
        	 for(int j=0; ledgerDetailList.size()>j;j++){
        		 
        		   RptTransactionVO ledgerDueVO= new RptTransactionVO();        		  
            	   ledgerDueVO=(RptTransactionVO) ledgerDetailList.get(j);  	 
            	   
        		   if(transactionVO.getMember_id()==ledgerDueVO.getMember_id()){        			   
        			  
        			   RptTransactionVO memberLedgerVO=new RptTransactionVO();                    
                       memberLedgerVO.setAptID(ledgerDueVO.getAptID());
                       memberLedgerVO.setMember_id(ledgerDueVO.getMember_id());                       
                       memberLedgerVO.setDebitAmt(ledgerDueVO.getDebitAmt());
                       memberLedgerVO.setCreditAmt(ledgerDueVO.getCreditAmt());
                       memberLedgerVO.setMember_name(ledgerDueVO.getLedgerName());                                        
            		   memberLedger.add(memberLedgerVO);            		   
            	//	   logger.info(memberLedger.size()+"Equal member id: "+transactionVO.getMember_id()+ledgerDueVO.getLedgerName());
            		   reportVO.setLedgerName(transactionVO.getLedgerName());                            
            		   reportVO.setCreditAmt(transactionVO.getCreditAmt());
            		   reportVO.setDebitAmt(transactionVO.getDebitAmt());     
            		   reportVO.setDisplayDate(printTxVO.getDisplayDate()); 
                       reportVO.setSocietyVO(societyVO);  
                       reportVO.setDueDetails(memberLedger);            
                      
        		   }	  
        		          		  
        	   }
        	
        	  reportVO.setLedgerName(transactionVO.getLedgerName());                            
   		      reportVO.setCreditAmt(transactionVO.getCreditAmt());
   		      reportVO.setDebitAmt(transactionVO.getDebitAmt());     
   		      reportVO.setDisplayDate(printTxVO.getDisplayDate()); 
              reportVO.setSocietyVO(societyVO);     
              collection.add(reportVO);   
        	 
        	
          }
                          
               //fill printreportVO
               printReportVO.setPrintParam( printTxVO.getPrintParam());
               printReportVO.setReportContentType(printTxVO.getReportContentType());
               printReportVO.setReportID(printTxVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printTxVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : public PrintReportVO printLedgerDueDetailsReport(RptTransactionVO printTxVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printLedgerDueDetailsReport(RptTransactionVO printTxVO)  ");
          return printReportVO;
  } 
	
	public PrintReportVO printInvoiceDueReport(RptTransactionVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printInvoiceDueReport(RptTransactionVO printTxVO) " +(String) printTxVO.getPrintParam().get("society_id"));
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();
        List invoiceDueList=new ArrayList();
        List buildingList=new ArrayList();
        List summaryList=new ArrayList();
           try{
               //fill trasaction vo
               String societyId=(String) printTxVO.getPrintParam().get("society_id");
               summaryList=reportService.getMemberDueNew(printTxVO.getDisplayDate(), Integer.parseInt(societyId),"0","BLDG");
               invoiceDueList=reportService.getMemberDueNew(printTxVO.getDisplayDate(), Integer.parseInt(societyId), "0","APT");
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));   
               buildingList=memberService.getBuildingList(Integer.parseInt(societyId), "0");
               
               for(int i=0;i<buildingList.size();i++){
            	   List transactionList=new ArrayList();
            	   RptTransactionVO transactionVO=new RptTransactionVO();
            	   DropDownVO buildingVO= (DropDownVO) buildingList.get(i);
            	   
            	  for(int j=0;j<invoiceDueList.size();j++){
            		   RptTransactionVO invoiceDueVO=(RptTransactionVO) invoiceDueList.get(j);
            		  
            		   if(invoiceDueVO.getBuildingId().equalsIgnoreCase(buildingVO.getData())){            			   
            			   transactionList.add(invoiceDueVO);
            			   transactionVO.setDueSummary(summaryList);
            			   transactionVO.setDisplayDate(printTxVO.getDisplayDate());
            			   transactionVO.setDueDetails(transactionList);
            			   transactionVO.setSocietyVO(societyVO);
            		   }            		   
            	   }                 	  
            	   collection.add(transactionVO);           	   
            	  
               }
                              
               //fill printreportVO
               printReportVO.setPrintParam( printTxVO.getPrintParam());
               printReportVO.setReportContentType(printTxVO.getReportContentType());
               printReportVO.setReportID(printTxVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printTxVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : public PrintReportVO printInvoiceDueReport(RptTransactionVO printTxVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printInvoiceDueReport(RptTransactionVO printTxVO)  ");
          return printReportVO;
  }	
	
	public PrintReportVO printInvoiceDueDetailSummaryReport(RptTransactionVO printTxVO)  {
        logger.debug("Entry :public PrintReportVO printInvoiceDueDetailSummaryReport(RptTransactionVO printTxVO) "+printTxVO.getDisplayDate());
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();
        List transactionList=new ArrayList();      
        List invoiceDue=new ArrayList();
           try{
               //fill trasaction vo
               String societyId=(String) printTxVO.getPrintParam().get("society_id");
               transactionList=reportService.getMemberDueNew(printTxVO.getDisplayDate(), Integer.parseInt(societyId), printTxVO.getBuildingId(),"APT");
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));                
               invoiceDue=invoiceService.getUnpaidInvoicesDetails(Integer.parseInt(societyId),printTxVO.getBuildingId(),"APT");
               
               for(int i=0;transactionList.size()>i;i++){
	             	  
                   RptTransactionVO transactionVO = new RptTransactionVO();
                   transactionVO = (RptTransactionVO) transactionList.get(i);   
                   List memberLedger=new ArrayList();
       		       RptTransactionVO printInvoiceVO=new RptTransactionVO();
       			
               	 for(int j=0; invoiceDue.size()>j;j++){
               		 
               		InvoiceDetailsVO invoiceDueVO= new InvoiceDetailsVO();        		  
               		  invoiceDueVO=(InvoiceDetailsVO) invoiceDue.get(j);  	 
                   	   
               		   if(transactionVO.getAptID()==invoiceDueVO.getAptID()){        			   
               			  
               			  InvoiceDetailsVO inMemberVO = new InvoiceDetailsVO();                
               			  inMemberVO.setAptID(invoiceDueVO.getAptID());                                  
               			  inMemberVO.setCurrentBalance(invoiceDueVO.getCurrentBalance());
               			  inMemberVO.setInvoiceType(invoiceDueVO.getInvoiceType());                                        
                   		  memberLedger.add(inMemberVO);            		   
                   	//	   logger.info(memberLedger.size()+"Equal member id: "+transactionVO.getAptID()+invoiceDueVO.getMemberName());
                   		  printInvoiceVO.setLedgerName(transactionVO.getLedgerName());
     	                  printInvoiceVO.setActualTotal(transactionVO.getActualTotal());
     	                  printInvoiceVO.setDueDetails(memberLedger);
     	                  printInvoiceVO.setSocietyVO(societyVO);
     	                  printInvoiceVO.setDisplayDate(printTxVO.getDisplayDate());                   		       
                         
               		   }	              		          		  
               	   }
               	 
               	 collection.add(printInvoiceVO);
               	               	
                 }
            
               //fill printreportVO
               printReportVO.setPrintParam( printTxVO.getPrintParam());
               printReportVO.setReportContentType(printTxVO.getReportContentType());
               printReportVO.setReportID(printTxVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printTxVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : public PrintReportVO printInvoiceDueDetailSummaryReport(RptTransactionVO printTxVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printInvoiceDueDetailSummaryReport(RptTransactionVO printTxVO)   ");
          return printReportVO;
  }	
	
	
	public PrintReportVO printInvoiceDueDetailReport(RptTransactionVO printTxVO)  {
        logger.debug("Entry :public PrintReportVO printInvoiceDueDetailReport(RptTransactionVO printTxVO) "+printTxVO.getDisplayDate());
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();
        List transactionList=new ArrayList();      
        List invoiceDue=new ArrayList();
           try{
               //fill trasaction vo
               String societyId=(String) printTxVO.getPrintParam().get("society_id");
               transactionList=reportService.getMemberDueNew(printTxVO.getDisplayDate(), Integer.parseInt(societyId),"0","APT");
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));                
               invoiceDue=invoiceService.getUnpaidInvoicesForApartment(Integer.parseInt(societyId), "0", 0);
               
               for(int i=0;transactionList.size()>i;i++){
	             	  
                   RptTransactionVO transactionVO = new RptTransactionVO();
                   transactionVO = (RptTransactionVO) transactionList.get(i);   
                   List memberLedger=new ArrayList();
       		       RptTransactionVO printInvoiceVO=new RptTransactionVO();
       			
               	 for(int j=0; invoiceDue.size()>j;j++){
               		 
               		InvoiceDetailsVO invoiceDueVO= new InvoiceDetailsVO();        		  
               		  invoiceDueVO=(InvoiceDetailsVO) invoiceDue.get(j);  	 
                   	   
               		   if(transactionVO.getAptID()==invoiceDueVO.getAptID()){        			   
               			  
               			  InvoiceDetailsVO inMemberVO = new InvoiceDetailsVO();                
               			  inMemberVO.setAptID(invoiceDueVO.getAptID());                                  
               			  inMemberVO.setCurrentBalance(invoiceDueVO.getCurrentBalance());
               			  inMemberVO.setInvoiceType(invoiceDueVO.getInvoiceType());   
               			  inMemberVO.setFromDate(invoiceDueVO.getFromDate());
               			 inMemberVO.setUptoDate(invoiceDueVO.getUptoDate());
                   		  memberLedger.add(inMemberVO);            		   
                   	//	   logger.info(memberLedger.size()+"Equal member id: "+transactionVO.getAptID()+invoiceDueVO.getMemberName());
                   		  printInvoiceVO.setLedgerName(transactionVO.getLedgerName());
     	                  printInvoiceVO.setActualTotal(transactionVO.getActualTotal());
     	                  printInvoiceVO.setDueDetails(memberLedger);
     	                  printInvoiceVO.setSocietyVO(societyVO);
     	                  printInvoiceVO.setDisplayDate(printTxVO.getDisplayDate());                   		       
                         
               		   }	              		          		  
               	   }
               	 
               	 collection.add(printInvoiceVO);
               	               	
                 }
            
               //fill printreportVO
               printReportVO.setPrintParam( printTxVO.getPrintParam());
               printReportVO.setReportContentType(printTxVO.getReportContentType());
               printReportVO.setReportID(printTxVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printTxVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : public PrintReportVO printInvoiceDueDetailReport(RptTransactionVO printTxVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printInvoiceDueDetailReport(RptTransactionVO printTxVO)   ");
          return printReportVO;
  }	
	
	
	public PrintReportVO printInvoiceTypeWiseReport(RptTransactionVO printTxVO)  {
        logger.debug("Entry :public PrintReportVO printInvoiceDueDetailReport(RptTransactionVO printTxVO) "+printTxVO.getDisplayDate());
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();
        List transactionList=new ArrayList();      
        List invoiceTypeDue=new ArrayList();
        List invoiceTypeSummary=new ArrayList();
        List buildingList=new ArrayList();
           try{
               //fill trasaction vo
               String societyId=(String) printTxVO.getPrintParam().get("society_id");
               buildingList=memberService.getBuildingList(Integer.parseInt(societyId), "0");
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));                
               invoiceTypeDue=invoiceService.getUnpaidInvoicesDetails(Integer.parseInt(societyId),"0","BLDG");
               invoiceTypeSummary=invoiceService.getUnpaidInvoicesDetails(Integer.parseInt(societyId),"0","INSTYP");
               
         //logger.info("Building wise due : "+invoiceTypeDue.size()+" Summary "+invoiceTypeSummary.size());               
           
			  RptTransactionVO printInvoiceVO=new RptTransactionVO();
			  printInvoiceVO.setDueSummary(invoiceTypeSummary);   
			  printInvoiceVO.setDueDetails(invoiceTypeDue);
			  printInvoiceVO.setSocietyVO(societyVO);
			  printInvoiceVO.setDisplayDate(printTxVO.getDisplayDate());   
			  collection.add(printInvoiceVO);
            
               //fill printreportVO
               printReportVO.setPrintParam( printTxVO.getPrintParam());
               printReportVO.setReportContentType(printTxVO.getReportContentType());
               printReportVO.setReportID(printTxVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printTxVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : public PrintReportVO printInvoiceDueDetailReport(RptTransactionVO printTxVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printInvoiceDueDetailReport(RptTransactionVO printTxVO)   ");
          return printReportVO;
  }	
	
	public PrintReportVO printVouchuerReport(TransactionVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printVouchuerReport(TransactionVO printTxVO)"+printTxVO.getLedgerID()+printTxVO.getSourceType()+printTxVO.getPrintParam().get("society_id"));
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();
        List transactionList=new ArrayList();
        
        
           try{
        	
        	   String societyId=(String) printTxVO.getPrintParam().get("orgID");
               
               //fill trasaction vo        
               transactionList=transactionService.getTransctionList(Integer.parseInt(societyId),printTxVO.getLedgerID(),printTxVO.getFromDate(), printTxVO.getToDate(), "C");
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));     
               HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
               printTxVO.setReportID(temmplateHashmap.get("voucherReport").trim());
 
               printTxVO.setLedgerEntries(transactionList);
               printTxVO.setSocietyVO(societyVO);
               collection.add(printTxVO);
               
               //fill printreportVO
               printReportVO.setPrintParam( printTxVO.getPrintParam());
               printReportVO.setReportContentType(printTxVO.getReportContentType());
               printReportVO.setReportID(printTxVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printTxVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception :public PrintReportVO printVouchuerReport(TransactionVO printTxVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printVouchuerReport(TransactionVO printTxVO) ");
          return printReportVO;
  }	
	
	public PrintReportVO printPaymentCheque(TransactionVO printRecptVO)  {
	       logger.debug("Entry :public PrintReportVO printPaymentCheque(TransactionVO printRecptVO)");
	       PrintReportVO printReportVO= new PrintReportVO();
	       TransactionVO voucherVO=new TransactionVO();
	       SocietyVO societyVO= new SocietyVO();
	       List collection = new ArrayList();            
	       ProfileDetailsVO profileVO=new ProfileDetailsVO();
	          try{
	              //fill printRecptVO          
	              String societyId=(String) printRecptVO.getPrintParam().get("orgID");  
	              String transactionID=(String) printRecptVO.getPrintParam().get("paymentID");
	              String reportID=(String) printRecptVO.getPrintParam().get("reportID");
	                       
	              societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));              
	              printRecptVO.setReportID(reportID);              
	             
	            //  profileVO=ledgerService.getLedgerProfile(printRecptVO.getLedgerID(), Integer.parseInt(societyId));
	              voucherVO=transactionService.getTransctionDetails(Integer.parseInt(societyId), Integer.parseInt(transactionID));
	              
	              //split date into digit
	              String[] dateParts = printRecptVO.getTransactionDate().split("-");
	              String year=dateParts[0];
	              String month=dateParts[1];
	              String day=dateParts[2];
	             
	              printRecptVO.setFromDate(day);
	              printRecptVO.setToDate(month);
	              printRecptVO.setDueDate(year);
	             
	              printRecptVO.setLedgerName(voucherVO.getAdditionalProps().getChequeName());
	              printRecptVO.setSocietyVO(societyVO);  
	              collection.add(printRecptVO);
	         
	              //fill printreportVO
	              printReportVO.setPrintParam( printRecptVO.getPrintParam());
	              printReportVO.setReportContentType(printRecptVO.getReportContentType());
	              printReportVO.setReportID(printRecptVO.getReportID());
	              printReportVO.setBeanList(collection);
	              printReportVO.setSourceType(printRecptVO.getSourceType());
	              printReportVO= this.printReport(printReportVO);
	             
	         
	          }catch (Exception e) {
	             logger.error("Exception : public PrintReportVO printPaymentCheque(TransactionVO printRecptVO) "+e);
	         }
	      logger.debug("Exit : public PrintReportVO printPaymentCheque(TransactionVO printRecptVO)  ");
	         return printReportVO;
	 } 
	
	
	
	public PrintReportVO printBalanceSheetReport(ReportVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printBalanceSheetReport(ReportVO printTxVO)  ");
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();
        List transactionList=new ArrayList();
        ReportVO reportVO =new ReportVO();
        ReportVO indAsVO =new ReportVO();
        ReportDetailsVO sumAssets=new ReportDetailsVO();
        ReportDetailsVO sumLiability=new ReportDetailsVO();
           try{                           
        	   //fill report vo
               String societyId=(String) printTxVO.getPrintParam().get("orgID");
               String formatType=(String) printTxVO.getPrintParam().get("formatType");
               String reportID=(String) printTxVO.getPrintParam().get("reportID");
                //logger.info("size: "+societyId+ printTxVO.getUptDate()+printTxVO.getFromDate());          
                            	
               indAsVO=balanceSheetService.getIndASBalanceSheetReport(Integer.parseInt(societyId), printTxVO.getFromDate(), printTxVO.getUptDate(),Integer.parseInt(reportID));
               reportVO=reportService.convertReportObjectForPrinting(indAsVO);
               
                if(formatType.equalsIgnoreCase("N") || formatType.equalsIgnoreCase("T"))  {     
                           
                	//Check two list size
	                if(reportVO.getLiabilityList().size()!=reportVO.getAssetsList().size()){	
	                	
	               	  for(int i=0;reportVO.getLiabilityList().size()>i;i++){
	   	            	   ReportDetailsVO totalInc=new ReportDetailsVO();               	
	   	            	    totalInc =   (ReportDetailsVO) reportVO.getLiabilityList().get(i);
	   	                   if(totalInc.getCategoryName().equalsIgnoreCase("TOTAL")){
	   	                	sumLiability.setCategoryName("TOTAL");
	   	     		 	     sumLiability.setClosingBalance(totalInc.getClosingBalance());
	   	     		 	     sumLiability.setOpeningBalance(totalInc.getOpeningBalance());
	   	     		 	     sumLiability.setRptGrpType("L");
	   	                	   reportVO.getLiabilityList().remove(i);
	   	                	   --i;              	   
	   	                   }            	   
	   	               }
	   	               for(int i=0;reportVO.getAssetsList().size()>i;i++){
	   	            	   ReportDetailsVO totalInc=new ReportDetailsVO();               	
	   	            	      totalInc =  (ReportDetailsVO) reportVO.getAssetsList().get(i);
	   	                   if(totalInc.getCategoryName().equalsIgnoreCase("TOTAL")){
		   	                	sumAssets.setCategoryName("TOTAL");
		   	     		 	sumAssets.setClosingBalance(totalInc.getClosingBalance());
		   	     		 	sumAssets.setOpeningBalance(totalInc.getOpeningBalance());
		   	     		 	sumAssets.setRptGrpType("L");
	   	                	   reportVO.getAssetsList().remove(i);
	   	                	   --i;              	   
	   	                   }            	   
	   	               }
	   	               
	                }
   	               
                	if(formatType.equalsIgnoreCase("N") ){
                		 printTxVO.setReportID("balanceSheetNformat.jasper");
                	}else{
                		 printTxVO.setReportID("balanceSheetTformat.jasper");
                	}
                	
                	
               	 int libilityLength=0,assetsLength=0;
                 libilityLength=reportVO.getLiabilityList().size();
                 assetsLength=reportVO.getAssetsList().size();
                 
                     logger.debug("libilityLength : "+libilityLength+" assetsLength: "+assetsLength);
                     int remainRows=0;
                     
                    if(libilityLength>assetsLength){
                  	  remainRows=(libilityLength+1)-assetsLength;
                  	  int remainRows1=remainRows-1;
                  	  for(int i=0;remainRows>i;i++){
                  		  
                  		if(remainRows1==i){
         					 reportVO.getAssetsList().add(sumAssets);  
         				 }else{
                  			 ReportDetailsVO prtLssVO=new ReportDetailsVO();
                  			 prtLssVO.setRptGrpType("L");
              				 prtLssVO.setCategoryName("");
              				 prtLssVO.setLedgerName("");        				
              				 reportVO.getAssetsList().add(prtLssVO);  
                  	     }
              				 
              				
                        }
                  	  
                 	 reportVO.getLiabilityList().add(sumLiability);  
                 	 
                    }else if(assetsLength>libilityLength){
                  	  remainRows=(assetsLength+1)-libilityLength;
                  	  int remainRows1=remainRows-1;
                  	  for(int i=0;remainRows>i;i++){
                  		  
                  		if(remainRows1==i){
        					 reportVO.getLiabilityList().add(sumAssets);  
        				 }else{	  
	               			 ReportDetailsVO prtLssVO=new ReportDetailsVO();
	               			prtLssVO.setRptGrpType("L");
	           				  prtLssVO.setCategoryName("");
	           				 prtLssVO.setLedgerName("");     				
	           				 reportVO.getLiabilityList().add(prtLssVO);  
        				 }
                       }
                  	  
                  	  
                  	 reportVO.getAssetsList().add(sumAssets);  
                    }
                 
                    
           		 
           		   printTxVO.setLiabilityList(reportVO.getLiabilityList());
         		   printTxVO.setAssetsList(reportVO.getAssetsList());  
           	

                
               }else if(formatType.equalsIgnoreCase("I") || formatType.equalsIgnoreCase("S") ){ 
            	   printTxVO.setReportID("balanceSheetIndAs.jasper");
                        	     	   
            	   printTxVO.setLiabilityList(reportVO.getLiabilityList());
          		   printTxVO.setAssetsList(reportVO.getAssetsList());  
               }
               
               
                 		  
               printTxVO.setSocietyVO(reportVO.getSocietyVO());
               printTxVO.setRptFromDate(reportVO.getRptFromDate());
               printTxVO.setRptUptoDate(reportVO.getRptUptoDate());
                              
               collection.add(printTxVO);
                       
               //fill printreportVO
               printReportVO.setPrintParam( printTxVO.getPrintParam());
               printReportVO.setReportContentType(printTxVO.getReportContentType());
               printReportVO.setReportID(printTxVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printTxVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : public PrintReportVO printBalanceSheetReport(ReportVO printTxVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printBalanceSheetReport(ReportVO printTxVO)  ");
          return printReportVO;
  } 
	
	
	public PrintReportVO printDocumentReport(PrintReportVO printVO)  {
        logger.debug("Entry : public PrintReportVO printDocumentReport(PrintReportVO printVO) " +(String) printVO.getPrintParam().get("orgID"));
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List buildingList = new ArrayList();
        List collection = new ArrayList();
        List summary = new ArrayList();
        List documentList=new ArrayList();
        
           try{
               //fill trasaction vo
               String societyId=(String) printVO.getPrintParam().get("orgID");
               String documentID=(String) printVO.getPrintParam().get("docID");
               buildingList= memberService.getBuildingList(Integer.parseInt(societyId), "");
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));  
               HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
               printVO.setReportID(temmplateHashmap.get("documentRegister").trim());
               
               summary=reportService.getDocumentSummaryReport(Integer.parseInt(societyId), "0", Integer.parseInt(documentID) );    
               documentList=reportService.getSingleDocumentReport(Integer.parseInt(societyId), Integer.parseInt(documentID));
              // logger.info("Size: "+documentList.size());
               RptDocumentVO sumDocVO=(RptDocumentVO) summary.get(0);
               logger.debug("Document Name: "+sumDocVO.getDocumentName());
               for(int i=0;i<buildingList.size();i++){
            	   List docList=new ArrayList();
            	   RptDocumentVO docVO=new RptDocumentVO();
            	   DropDownVO buildingVO= (DropDownVO) buildingList.get(i);
            	   
            	   if(i==0){
            		   docVO.setSummaryDocs(summary);
            		   
            	   }
            	   docVO.setDocumentName(sumDocVO.getDocumentName());
            	   for(int j=0;j<documentList.size();j++){
            		   
            		   RptDocumentVO rptVO= (RptDocumentVO) documentList.get(j);
            		   if(rptVO.getBuildingID()==Integer.parseInt(buildingVO.getData())){
            			 //  logger.info(" ID: "+rptVO.getBuildingID());
            			   docList.add(rptVO);
            			   docVO.setReceivedDocs(docList);
            			   docVO.setSocietyVO(societyVO);
            			  
            		   }
            	   }        	  
            	  
            	   collection.add(docVO);	               	   
            	   
               }
               
               //fill printreportVO
               printReportVO.setPrintParam(printVO.getPrintParam());
               printReportVO.setReportContentType(printVO.getReportContentType());
               printReportVO.setReportID(printVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : public PrintReportVO printDocumentReport(PrintReportVO printVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printDocumentReport(PrintReportVO printVO)  ");
          return printReportVO;
  }	
	
	
	public PrintReportVO printVehicleReport(PrintReportVO printVO)  {
        logger.debug("Entry : public PrintReportVO printVehicleReport(PrintReportVO printVO) " +(String) printVO.getPrintParam().get("orgID"));
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List buildingList = new ArrayList();
        List collection = new ArrayList();
        List summary = new ArrayList();
        List vehicleList=new ArrayList();
        
           try{
               //fill trasaction vo
               String societyId=(String) printVO.getPrintParam().get("orgID");
               buildingList= memberService.getBuildingList(Integer.parseInt(societyId), "");
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));   
               HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
               printVO.setReportID(temmplateHashmap.get("vehicleRegister").trim());
               
               summary=reportService.getVehicleReportSummary(Integer.parseInt(societyId), "0");  
               vehicleList=reportService.getVehicleReport(Integer.parseInt(societyId), "0");
             // logger.info("Size: "+vehicleList.size()+" Summary: "+summary.size());
               for(int i=0;i<buildingList.size();i++){
            	   List vehList=new ArrayList();
            	   RptVehicleVO vehicleVO=new RptVehicleVO();
            	   DropDownVO buildingVO= (DropDownVO) buildingList.get(i);
            	   
            	   if(i==0){
            		   vehicleVO.setSummaryVehicles(summary);
            	   }
            	  
            	   for(int j=0;j<vehicleList.size();j++){            		   
            		   RptVehicleVO rptVO=  (RptVehicleVO) vehicleList.get(j);
            		   String buildingID=rptVO.getBuildingID()+"";
            		   if(buildingID.equalsIgnoreCase(buildingVO.getData())){
            			 //  logger.info(" ID: "+rptVO.getBuildingID());
            			   vehList.add(rptVO);
            			   vehicleVO.setVehiclesList(vehList);
            			   vehicleVO.setSocietyVO(societyVO);
            			  
            		   }
            	   }        	  
            	  
            	   collection.add(vehicleVO);	               	   
            	   
               }
               
               //fill printreportVO
               printReportVO.setPrintParam(printVO.getPrintParam());
               printReportVO.setReportContentType(printVO.getReportContentType());
               printReportVO.setReportID(printVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : public PrintReportVO printVehicleReport(PrintReportVO printVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printVehicleReport(PrintReportVO printVO)  ");
          return printReportVO;
  }	
	
	public PrintReportVO printShareRegister(PrintReportVO printVO)  {
        logger.debug("Entry : public PrintReportVO printShareRegister(PrintReportVO printVO) " +(String) printVO.getPrintParam().get("orgID"));
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();        
        List shareRegList=new ArrayList();
        
           try{
               //fill trasaction vo
               String societyId=(String) printVO.getPrintParam().get("orgID");              
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));  
               HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
               printVO.setReportID(temmplateHashmap.get("shareRegister").trim());
             
               shareRegList =  shareRegService.getShareList(0, Integer.parseInt(societyId));
               printVO.setBeanList(shareRegList);
               printVO.setSocietyVO(societyVO);
               collection.add(printVO);
               
               printReportVO.setPrintParam(printVO.getPrintParam());
               printReportVO.setReportContentType(printVO.getReportContentType());
               printReportVO.setReportID(printVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : printShareRegister "+e);
          }
       logger.debug("Exit : public PrintReportVO printShareRegister(PrintReportVO printVO)  ");
          return printReportVO;
   }	
	
	public PrintReportVO printMortgageRegister(PrintReportVO printVO)  {
        logger.debug("Entry : public PrintReportVO printMortgageRegister(PrintReportVO printVO) " +(String) printVO.getPrintParam().get("orgID"));
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();        
        List mortgageList=new ArrayList();
        
           try{
               //fill trasaction vo
               String societyId=(String) printVO.getPrintParam().get("orgID");              
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));  
               HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
               printVO.setReportID(temmplateHashmap.get("mortgageRegister").trim());
             
               mortgageList =  reportService.getMorgageDetails(Integer.parseInt(societyId));
               printVO.setBeanList(mortgageList);
               printVO.setSocietyVO(societyVO);
               collection.add(printVO);
               
               printReportVO.setPrintParam(printVO.getPrintParam());
               printReportVO.setReportContentType(printVO.getReportContentType());
               printReportVO.setReportID(printVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : printMortgageRegister "+e);
          }
       logger.debug("Exit : public PrintReportVO printMortgageRegister(PrintReportVO printVO)  ");
          return printReportVO;
   }	
	
	public PrintReportVO printNominationRegister(PrintReportVO printVO)  {
        logger.debug("Entry : public PrintReportVO printNominationRegister(PrintReportVO printVO) " +(String) printVO.getPrintParam().get("orgID"));
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();        
        List nominationList=new ArrayList();
        
           try{
               //fill trasaction vo
               String societyId=(String) printVO.getPrintParam().get("orgID");              
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));  
               HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
               printVO.setReportID(temmplateHashmap.get("nomineeRegister").trim());
             
               nominationList =  nominationRegService.getNominationList(0,Integer.parseInt(societyId));
               printVO.setBeanList(nominationList);
               printVO.setSocietyVO(societyVO);
               collection.add(printVO);
               
               printReportVO.setPrintParam(printVO.getPrintParam());
               printReportVO.setReportContentType(printVO.getReportContentType());
               printReportVO.setReportID(printVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : printMortgageRegister "+e);
          }
       logger.debug("Exit : public PrintReportVO printMortgageRegister(PrintReportVO printVO)  ");
          return printReportVO;
   }	
	
	public PrintReportVO printTenantRegister(PrintReportVO printVO)  {
        logger.debug("Entry : public PrintReportVO printMortgageRegister(PrintReportVO printVO) " +(String) printVO.getPrintParam().get("orgID"));
        PrintReportVO printReportVO= new PrintReportVO();
        RptTenantVO printTenant=new RptTenantVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();        
        List tenantList=new ArrayList();
        
           try{
               //fill trasaction vo
               String societyId=(String) printVO.getPrintParam().get("orgID");              
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));  
               HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
               printVO.setReportID(temmplateHashmap.get("tenantRegister").trim());
             
               tenantList =  reportService.tenantDetailsRpt(Integer.parseInt(societyId));
               RptTenantVO rpnTnt = (RptTenantVO) tenantList.get(0);
               printTenant.setCntNtDocsRecvd(rpnTnt.getCntNtDocsRecvd());
               printTenant.setCntNtVerify(rpnTnt.getCntNtVerify());
               printTenant.setCntNRI(rpnTnt.getCntNRI());	
               printTenant.setCntDocsRecvd(rpnTnt.getCntDocsRecvd());
               printTenant.setCntVerify(rpnTnt.getCntVerify());
               printTenant.setCntRI(rpnTnt.getCntRI());
               printTenant.setNoOfMissingInfo(rpnTnt.getNoOfMissingInfo());
               printTenant.setTotal(tenantList.size());
               printTenant.setTenantList(tenantList);
               printTenant.setSocietyVO(societyVO);
               collection.add(printTenant);
               
               printReportVO.setPrintParam(printVO.getPrintParam());
               printReportVO.setReportContentType(printVO.getReportContentType());
               printReportVO.setReportID(printVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : printMortgageRegister "+e);
          }
       logger.debug("Exit : public PrintReportVO printMortgageRegister(PrintReportVO printVO)  ");
          return printReportVO;
   }	
	
	public PrintReportVO printJRegister(PrintReportVO printVO)  {
        logger.debug("Entry : public PrintReportVO printJRegister(PrintReportVO printVO) " +(String) printVO.getPrintParam().get("society_id"));
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List jregisterList = new ArrayList();
        List collection = new ArrayList();
    
        
           try{
               //fill member vo
        	   String societyId=(String) printVO.getPrintParam().get("orgID");              
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));  
               HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
               printVO.setReportID(temmplateHashmap.get("jRegister").trim());
             
               jregisterList = societyService.getJRegisterList(Integer.parseInt(societyId), 0);
               printVO.setBeanList(jregisterList);
               printVO.setSocietyVO(societyVO);
               collection.add(printVO);
               
               printReportVO.setPrintParam(printVO.getPrintParam());
               printReportVO.setReportContentType(printVO.getReportContentType());
               printReportVO.setReportID(printVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : printJRegister "+e);
          }
       logger.debug("Exit : public PrintReportVO printJRegister(PrintReportVO printVO)  ");
          return printReportVO;
  }	
	
	public PrintReportVO printPaymentRequestForSociety(RptTransactionVO printTxVO)  {
        logger.debug("Entry :public PrintReportVO printPaymentRequestForSociety(RptTransactionVO printTxVO) "+printTxVO.getDisplayDate());
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        BankInfoVO bankVO=new BankInfoVO();
        List collection = new ArrayList();
        List dueMemberList=new ArrayList();      
        List invoiceDue=new ArrayList();
           try{
               //fill trasaction vo
               String societyId=(String) printTxVO.getPrintParam().get("society_id");
               dueMemberList=reportService.getMemberDueNew(printTxVO.getDisplayDate(), Integer.parseInt(societyId), "0","APT");
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));               
               bankVO=societyService.getBankDetails((Integer.parseInt(societyId)));
               invoiceDue=invoiceService.getUnpaidInvoicesDetails((Integer.parseInt(societyId)),"0","APT");
               
               for(int i=0;dueMemberList.size()>i;i++){
	             	  
                   RptTransactionVO transactionVO = new RptTransactionVO();
                   transactionVO = (RptTransactionVO) dueMemberList.get(i);   
                   List memberLedger=new ArrayList();
       		       RptTransactionVO printInvoiceVO=new RptTransactionVO();
       			
               	 for(int j=0; invoiceDue.size()>j;j++){
               		 
               		InvoiceDetailsVO invoiceDueVO= new InvoiceDetailsVO();        		  
               		  invoiceDueVO=(InvoiceDetailsVO) invoiceDue.get(j);  	 
                   	   
               		   if(transactionVO.getAptID()==invoiceDueVO.getAptID()){        			   
               			  
               			  InvoiceDetailsVO inMemberVO = new InvoiceDetailsVO();                
               			  inMemberVO.setAptID(invoiceDueVO.getAptID());                                  
               			  inMemberVO.setBalance(invoiceDueVO.getBalance());
               			  inMemberVO.setInvoiceType(invoiceDueVO.getInvoiceType());                                        
                   		  memberLedger.add(inMemberVO);            		   
                   	//	   logger.info(memberLedger.size()+"Equal member id: "+transactionVO.getAptID()+invoiceDueVO.getMemberName());
                   		  printInvoiceVO.setLedgerName(transactionVO.getLedgerName());
     	                  printInvoiceVO.setActualTotal(transactionVO.getActualTotal());
     	                 printInvoiceVO.setDisplayDate(printTxVO.getDisplayDate());
     	                  printInvoiceVO.setDueDetails(memberLedger);
     	                  printInvoiceVO.setSocietyVO(societyVO);
     	                  printInvoiceVO.setBankInfoVO(bankVO);     	                          		       
                         
               		   }	              		          		  
               	   }
               	 
               	 collection.add(printInvoiceVO);
               	               	
                 }
            
               //fill printreportVO
               printReportVO.setPrintParam(printTxVO.getPrintParam());
               printReportVO.setReportContentType(printTxVO.getReportContentType());
               printReportVO.setReportID(printTxVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printTxVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : public PrintReportVO printPaymentRequestForSociety(RptTransactionVO printTxVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printPaymentRequestForSociety(RptTransactionVO printTxVO)   ");
          return printReportVO;
  }	
	
	public PrintReportVO printPaymentRequestForMember(RptTransactionVO printTxVO)  {
        logger.debug("Entry :public PrintReportVO printPaymentRequestForMember(RptTransactionVO printTxVO) "+printTxVO.getDisplayDate());
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        BankInfoVO bankVO=new BankInfoVO();
        List collection = new ArrayList();
         try{
               
               String societyId=(String) printTxVO.getPrintParam().get("society_id");
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));               
               bankVO=societyService.getBankDetails(Integer.parseInt(societyId));
                                         
               	RptTransactionVO printInvoiceVO=new RptTransactionVO();                  	
               	printInvoiceVO.setLedgerName(printTxVO.getLedgerName());
                printInvoiceVO.setActualTotal(printTxVO.getActualTotal().setScale(2));
                printInvoiceVO.setDisplayDate(printTxVO.getDisplayDate());
                printInvoiceVO.setDueDetails(printTxVO.getDueDetails());
                printInvoiceVO.setSocietyVO(societyVO);
                printInvoiceVO.setBankInfoVO(bankVO);
               	 
                collection.add(printInvoiceVO);               	               	
                            
               //fill printreportVO
               printReportVO.setPrintParam(printTxVO.getPrintParam());
               printReportVO.setReportContentType(printTxVO.getReportContentType());
               printReportVO.setReportID(printTxVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printTxVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : public PrintReportVO printPaymentRequestForMember(RptTransactionVO printTxVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printPaymentRequestForMember(RptTransactionVO printTxVO)   ");
          return printReportVO;
  }	
  
	public PrintReportVO printUnpaidInvoicesMember(RptTransactionVO printTxVO)  {
        logger.debug("Entry :public PrintReportVO printUnpaidInvoicesMember(RptTransactionVO printTxVO) ");
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();             
        List invoiceDue=new ArrayList();
           try{
               //fill trasaction vo
               String societyId=(String) printTxVO.getPrintParam().get("society_id");    
               String aptId=(String) printTxVO.getPrintParam().get("apt_id");  
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));  
               invoiceDue=invoiceService.getUnpaidInvoicesForApartment(Integer.parseInt(societyId), aptId, 0);
               
               RptTransactionVO printInvoiceVO=new RptTransactionVO();
               printInvoiceVO.setLedgerName(printTxVO.getLedgerName());
               printInvoiceVO.setDueDetails(invoiceDue);
               printInvoiceVO.setSocietyVO(societyVO);
             
               collection.add(printInvoiceVO);                             	
                 
            
               //fill printreportVO
               printReportVO.setPrintParam( printTxVO.getPrintParam());
               printReportVO.setReportContentType(printTxVO.getReportContentType());
               printReportVO.setReportID(printTxVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printTxVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : public PrintReportVO printUnpaidInvoicesMember(RptTransactionVO printTxVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printUnpaidInvoicesMember(RptTransactionVO printTxVO) ");
          return printReportVO;
  }	
	
	public PrintReportVO printInvoice(InvoiceDetailsVO printInvVO)  {
        logger.debug("Entry :public PrintReportVO printInvoice(InvoiceDetailsVO printInvVO)");
        PrintReportVO printReportVO= new PrintReportVO();
        InvoiceDetailsVO invoiceVO=new InvoiceDetailsVO();
        SocietyVO societyVO= new SocietyVO();
        BankInfoVO bankVO=new BankInfoVO();
        List collection = new ArrayList();             
        List invoice=new ArrayList();
           try{ 
               //fill printInvoice vo        	   
        	   
	               String societyId=(String) printInvVO.getPrintParam().get("society_id");   
	               String invoiceID=(String) printInvVO.getPrintParam().get("invoice_id"); 
	               String aptID=(String) printInvVO.getPrintParam().get("apt_id");  
	               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));   
	               HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
	               printInvVO.setReportID(temmplateHashmap.get("invoice").trim());
	               
	               invoice=invoiceService.getSingleInvoiceForMember(Integer.parseInt(societyId),Integer.parseInt(aptID), invoiceID);
	               bankVO=societyService.getBankDetails(Integer.parseInt(societyId));
	               InvoiceDetailsVO invoicDet=(InvoiceDetailsVO) invoice.get(0);                
	               invoiceVO.setSocietyVO(societyVO);
	               invoiceVO.setBankInfoVO(bankVO);
	               invoiceVO.setListOfLineItems(invoicDet.getListOfLineItems());
	               invoiceVO.setAptID(invoicDet.getAptID());
	               invoiceVO.setInvoiceID(invoicDet.getInvoiceID());
	               invoiceVO.setInvoiceDate(invoicDet.getInvoiceDate());
	               invoiceVO.setInvoiceType(invoicDet.getInvoiceType());
	               invoiceVO.setDueDate(invoicDet.getDueDate());
	               invoiceVO.setFromDate(invoicDet.getFromDate());
	               invoiceVO.setUptoDate(invoicDet.getUptoDate());
	               invoiceVO.setFullName(invoicDet.getFullName());
	               invoiceVO.setUnitNo(invoicDet.getUnitNo());
	               invoiceVO.setCarriedBalance(invoicDet.getCarriedBalance());
	               invoiceVO.setInvoiceAmount(invoicDet.getInvoiceAmount());
	               invoiceVO.setTotalAmount(invoicDet.getTotalAmount());
	               collection.add(invoiceVO);
        	   
               //fill printreportVO
               printReportVO.setPrintParam( printInvVO.getPrintParam());
               printReportVO.setReportContentType(printInvVO.getReportContentType());
               printReportVO.setReportID(printInvVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printInvVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception :public PrintReportVO printInvoice(InvoiceDetailsVO printInvVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printInvoice(InvoiceDetailsVO printInvVO)  ");
          return printReportVO;
  }	
	
	public PrintReportVO printMemberReceipt(TransactionVO printRecptVO)  {
        logger.debug("Entry :public PrintReportVO printReceipt(TransactionVO printRecptVO)");
        PrintReportVO printReportVO= new PrintReportVO();
        TransactionVO receiptVO=new TransactionVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();             
        List receipt=new ArrayList();
 
           try{ 
               //fill printRecptVO         	   
	               String societyId=(String) printRecptVO.getPrintParam().get("orgID");   
	               String receiptID=(String) printRecptVO.getPrintParam().get("receiptID"); 
	               String ledgerID=(String) printRecptVO.getPrintParam().get("ledgerID");  
	               logger.info("ledgerID: "+ledgerID+ " receiptID: "+receiptID);
	               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));   	               
	               HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
	               printRecptVO.setReportID(temmplateHashmap.get("receipt").trim());
	               
	               receiptVO=transactionService.getReceiptDetails(Integer.parseInt(societyId), ledgerID, receiptID);	              
	               receiptVO.setSocietyVO(societyVO);	
	           
	               collection.add(receiptVO);
        	   
               //fill printreportVO
               printReportVO.setPrintParam( printRecptVO.getPrintParam());
               printReportVO.setReportContentType(printRecptVO.getReportContentType());
               printReportVO.setReportID(printRecptVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printRecptVO.getSourceType());
               printReportVO.setSocietyVO(societyVO);
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception :	public PrintReportVO printReceipt(TransactionVO printRecptVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printReceipt(TransactionVO printRecptVO)  ");
          return printReportVO;
  }	
	
	public PrintReportVO printMemberReceiptForMemberApp(TransactionVO printRecptVO)  {
        logger.debug("Entry :public PrintReportVO printReceipt(TransactionVO printRecptVO)");
        PrintReportVO printReportVO= new PrintReportVO();
        TransactionVO receiptVO=new TransactionVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();             
        List receipt=new ArrayList();
 
           try{ 
               //fill printRecptVO         	   
        	   String societyId=(String) printRecptVO.getPrintParam().get("orgID");   
               String refNumber=(String) printRecptVO.getPrintParam().get("refNumber"); 
               String ledgerID=(String) printRecptVO.getPrintParam().get("ledgerID");  
	               logger.info("ledgerID: "+ledgerID+ " referenceNo: "+refNumber);
	               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));   	               
	               HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
	               printRecptVO.setReportID(temmplateHashmap.get("receipt").trim());
	               
	               receiptVO=transactionService.getReceiptDetailsForMemberApp(Integer.parseInt(societyId), ""+ledgerID, refNumber);	              
	               receiptVO.setSocietyVO(societyVO);	
	           
	               collection.add(receiptVO);
        	   
               //fill printreportVO
               printReportVO.setPrintParam( printRecptVO.getPrintParam());
               printReportVO.setReportContentType(printRecptVO.getReportContentType());
               printReportVO.setReportID(printRecptVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printRecptVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception :	public PrintReportVO printReceipt(TransactionVO printRecptVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printReceipt(TransactionVO printRecptVO)  ");
          return printReportVO;
  }
	
	public PrintReportVO printVoucherAndPaymentReceipt(TransactionVO printRecptVO)  {
        logger.debug("Entry :public PrintReportVO printVoucherAndPaymentReceipt(TransactionVO printRecptVO)");
        PrintReportVO printReportVO= new PrintReportVO();
        TransactionVO voucherVO=new TransactionVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();             
        String ledgerTags="";
        List projectList=new ArrayList();
        List invoiceReceiptList=new ArrayList();
        String ledgerTagWithName="";
           try{ 
               //fill printRecptVO         	   
	               String societyId=(String) printRecptVO.getPrintParam().get("orgID");   
	               String transactionID=(String) printRecptVO.getPrintParam().get("paymentID"); 
	          	               
	               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId)); 
	               HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
	               printRecptVO.setReportID(temmplateHashmap.get("voucher").trim());
	               
	              if(printRecptVO.getLedgerID()>0){
	                  voucherVO=transactionService.getTransctionDetails(Integer.parseInt(societyId), printRecptVO.getTransactionID());
	               
		               //Get ledger details
		               for(int i=0;i<voucherVO.getLedgerEntries().size();i++){
		            	   LedgerEntries ledgerDetails=voucherVO.getLedgerEntries().get(i);
		            	   if(ledgerDetails.getLedgerID()==printRecptVO.getLedgerID()){
		            		   ledgerTags=ledgerDetails.getLedgerTags();
		            	   }
		               }
		               
		               if(ledgerTags.length()>0){
		            	   
		           		projectList=projectDetailsService.getProjectListWithTagWise(Integer.parseInt(societyId),"");
		           		
		           		String ledgerTagsArray[] = ledgerTags.split("#");
		           		
		           		for(int i=0;i<projectList.size();i++){
			            	   ProjectDetailsVO projectDetails=(ProjectDetailsVO) projectList.get(i);
			            	  for(int j=0;j<ledgerTagsArray.length;j++){
			            		  
			            		  if(projectDetails.getProjectAcronyms().equalsIgnoreCase(ledgerTagsArray[j])){
			            			  
			            			  if(ledgerTagWithName.length()==0){
			            				  ledgerTagWithName=projectDetails.getProjectName().trim();
			            			  }else{
			            				   ledgerTagWithName=ledgerTagWithName+", "+projectDetails.getProjectName().trim();
			            			  }
			            		 
				            	   }
			            		  
			            	  }
			            	  
			               }
		               }
	              }
	              
	              
	              invoiceReceiptList=transactionService.getTransactionLinkedInvoiceList(Integer.parseInt(societyId),Integer.parseInt(transactionID));
	              printRecptVO.setTransactionsList(invoiceReceiptList);
	              
	              printRecptVO.setTxTags(ledgerTagWithName);
	               printRecptVO.setSocietyVO(societyVO);	  
	               collection.add(printRecptVO);
        	   
               //fill printreportVO
               printReportVO.setPrintParam( printRecptVO.getPrintParam());
               printReportVO.setReportContentType(printRecptVO.getReportContentType());
               printReportVO.setReportID(printRecptVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printRecptVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception :	public PrintReportVO printVoucherAndPaymentReceipt(TransactionVO printRecptVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printVoucherAndPaymentReceipt(TransactionVO printRecptVO)  ");
          return printReportVO;
  }	
	public PrintReportVO printSocietyMemberContactReport(PrintReportVO printVO)  {
        logger.debug("Entry : public PrintReportVO printSocietyMemberContactReport(PrintReportVO printVO) " +(String) printVO.getPrintParam().get("society_id"));
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List buildingList = new ArrayList();
        List collection = new ArrayList();
        List conatctList = new ArrayList();
        
           try{
               //fill member vo
               String societyId=(String) printVO.getPrintParam().get("orgID");
               String memberType=(String) printVO.getPrintParam().get("memberType");
               String infoMissingType=(String) printVO.getPrintParam().get("missingType");
               buildingList= memberService.getBuildingList(Integer.parseInt(societyId), "");
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));     
               HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
               printVO.setReportID(temmplateHashmap.get("memberInfoList").trim());
               
               conatctList=memberService.getMembersContactList(societyId, memberType, infoMissingType);
             
               for(int i=0;i<buildingList.size();i++){
            	   List memberList=new ArrayList();
            	   RptDocumentVO contactVO=new RptDocumentVO();
            	   DropDownVO buildingVO= (DropDownVO) buildingList.get(i);            	   
            	 
            	   for(int j=0;j<conatctList.size();j++){            		   
            		   MemberVO memberVO=  (MemberVO) conatctList.get(j);
            		   if((String.valueOf(memberVO.getBuildingID())).equalsIgnoreCase(buildingVO.getData())){
            			 //  logger.info(" ID: "+rptVO.getBuildingID());
            			   memberList.add(memberVO);
            			   contactVO.setReceivedDocs(memberList);
            			   contactVO.setSocietyVO(societyVO);            			  
            		   }
            	   }        	  
            	  
            	   collection.add(contactVO);	               	   
            	   
               }
               
               //fill printreportVO
               printReportVO.setPrintParam(printVO.getPrintParam());
               printReportVO.setReportContentType(printVO.getReportContentType());
               printReportVO.setReportID(printVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : public PrintReportVO printSocietyMemberContactReport(PrintReportVO printVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printSocietyMemberContactReport(PrintReportVO printVO)  ");
          return printReportVO;
  }	
	
	public PrintReportVO printMissingContactReport(PrintReportVO printVO)  {
        logger.debug("Entry : public PrintReportVO printMissingContactReport(PrintReportVO printVO) " +(String) printVO.getPrintParam().get("society_id"));
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();
        List conatctList = new ArrayList();
        
           try{
               //fill member vo
               String societyId=(String) printVO.getPrintParam().get("orgID");
               String memberType=(String) printVO.getPrintParam().get("memberType");
               String infoMissingType=(String) printVO.getPrintParam().get("missingType");
           
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));         
               HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
               printVO.setReportID(temmplateHashmap.get("missingInfo").trim());
               
               conatctList=memberService.getMembersContactList(societyId, memberType, infoMissingType);
               
               RptDocumentVO contactVO=new RptDocumentVO();
               contactVO.setReceivedDocs(conatctList);
			   contactVO.setSocietyVO(societyVO);  
			   collection.add(contactVO);	      
                             
               //fill printreportVO
               printReportVO.setPrintParam(printVO.getPrintParam());
               printReportVO.setReportContentType(printVO.getReportContentType());
               printReportVO.setReportID(printVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : public PrintReportVO printMissingContactReport(PrintReportVO printVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printMissingContactReport(PrintReportVO printVO)  ");
          return printReportVO;
  }	
	
	public PrintReportVO printSocietySummaryBillReport(TransactionVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printSocietySummaryBillReport(TransactionVO printTxVO)  ");
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        BigDecimal roundOff=BigDecimal.ZERO;           
        List ledgerTxList=new ArrayList();      
        List summaryTxList=new ArrayList();     
        BillDetailsVO billVO=new BillDetailsVO();
             try{
                   //fill trasaction vo                     
                   String societyId=(String) printTxVO.getPrintParam().get("orgID");
                   String billingCycleID= (String)printTxVO.getPrintParam().get("billingCycleID");
                   societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));   
                   HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
                   printTxVO.setReportID("summaryBill.jasper");
                   
                 
                   logger.info("Society Id: "+societyId+" From Date: "+printTxVO.getFromDate()+" To Date: "+printTxVO.getToDate()+"  "+societyVO.getSocietyName()); 
                   billVO.setFromDate(printTxVO.getFromDate());
                   billVO.setUptoDate(printTxVO.getToDate());
                   
                   summaryTxList=invoiceService.getBillsForSociety(societyVO.getSocietyID(), printTxVO.getFromDate(), printTxVO.getToDate(), Integer.parseInt(billingCycleID));
                   billVO.setSummaryBillList(summaryTxList); 
                   billVO.setSocietyVO(societyVO);
                   ledgerTxList.add(billVO);
                                  
                //fill printreportVO
                   
                 printReportVO.setPrintParam( printTxVO.getPrintParam());
                 printReportVO.setReportContentType(printTxVO.getReportContentType());
                 printReportVO.setReportID(printTxVO.getReportID());
                 printReportVO.setBeanList(ledgerTxList);
                 printReportVO.setSourceType(printTxVO.getSourceType());
                 printReportVO= this.printReport(printReportVO);
                 
             
             }catch (Exception e) {
                  logger.info("Exception : public PrintReportVO printSocietySummaryBillReport(TransactionVO printTxVO)  "+e);
            }
      
       logger.debug("Exit : public PrintReportVO printSocietySummaryBillReport(TransactionVO printTxVO)   ");
            return printReportVO;
   }
	
	public PrintReportVO printAllMemberBillReport(TransactionVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printAllMemberBillReport(TransactionVO printTxVO)  ");
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        BigDecimal roundOff=BigDecimal.ZERO;           
        List memberList=new ArrayList();
        List ledgerTxList=new ArrayList();              
        
             try{
                   //fill trasaction vo                     
                   String societyId=(String) printTxVO.getPrintParam().get("orgID");
                   String buildingID=(String) printTxVO.getPrintParam().get("buildingID");
                   String billingCycleID= (String)printTxVO.getPrintParam().get("billingCycleID");
                   societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));   
                   HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
                   printTxVO.setReportID(temmplateHashmap.get("bill").trim());
                   
                  // memberList=memberService.getMemberListForSearchByName(societyId);
                   memberList=memberService.getMemberListBuildingWise(Integer.parseInt(buildingID),Integer.parseInt(societyId));
                   logger.info("Society Id: "+societyId+" buildingID: "+buildingID+" From Date: "+printTxVO.getFromDate()+" To Date: "+printTxVO.getToDate()+" Member List :" +memberList.size()+" "+societyVO.getSocietyName()); 
                  
                   for(int i=0;memberList.size()>i;i++){
                        
                         MemberVO memberVO= (MemberVO) memberList.get(i);
                         TransactionVO txVO=new TransactionVO();
                         LedgerTrnsVO ledgerCredit=new LedgerTrnsVO();                         
                         List invoiceList=new ArrayList();
                         BillDetailsVO billVO=new BillDetailsVO();                        
                        
                         billVO=invoiceService.getBillDetails(societyVO.getSocietyID(), printTxVO.getFromDate(), printTxVO.getToDate(),memberVO.getAptID());
		                 ledgerTxList.add(billVO);                             
                                
                   }
                                  
                //fill printreportVO
                   
                 printReportVO.setPrintParam( printTxVO.getPrintParam());
                 printReportVO.setReportContentType(printTxVO.getReportContentType());
                 printReportVO.setReportID(printTxVO.getReportID());
                 printReportVO.setBeanList(ledgerTxList);
                 printReportVO.setSourceType(printTxVO.getSourceType());
                 printReportVO= this.printReport(printReportVO);
                 
             
             }catch (Exception e) {
                  logger.info("Exception : public PrintReportVO printAllMemberBillReport(TransactionVO printTxVO)  "+e);
            }
      
       logger.debug("Exit : public PrintReportVO printAllMemberBillReport(TransactionVO printTxVO)   ");
            return printReportVO;
   }	
	
	public PrintReportVO printBill(TransactionVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printBill(TransactionVO printTxVO)  ");
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();      
        List ledgerTxList=new ArrayList();       
        BillDetailsVO billVO=new BillDetailsVO();
        
             try{
                   //fill trasaction vo   
            	 logger.debug(" From Date: "+printTxVO.getFromDate()+" To Date: "+printTxVO.getToDate());      
            	   String societyId=(String) printTxVO.getPrintParam().get("orgID");
                   String aptID=(String) printTxVO.getPrintParam().get("aptID");
                   String billingCycleID= (String)printTxVO.getPrintParam().get("billingCycleID");
                   
                   societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId)); 
                   HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
                   printTxVO.setReportID(temmplateHashmap.get("bill").trim());
                   	   
                  billVO=invoiceService.getBillDetails(societyVO.getSocietyID(), printTxVO.getFromDate(), printTxVO.getToDate(),Integer.parseInt(aptID));
                         ledgerTxList.add(billVO);
                                  
                //fill printreportVO                   
                 printReportVO.setPrintParam( printTxVO.getPrintParam());
                 printReportVO.setReportContentType(printTxVO.getReportContentType());
                 printReportVO.setReportID(printTxVO.getReportID());
                 printReportVO.setBeanList(ledgerTxList);
                 printReportVO.setSourceType(printTxVO.getSourceType());
                 printReportVO= this.printReport(printReportVO);
                 
             
             }catch (Exception e) {
                  logger.error("Exception : public PrintReportVO printBill(TransactionVO printTxVO)  "+e);
            }
      
       logger.debug("Exit : public PrintReportVO printBill(TransactionVO printTxVO)   ");
            return printReportVO;
   }	
	
	public PrintReportVO printBillCustomer(TransactionVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printBillCustomer(TransactionVO printTxVO)  ");
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();      
        List ledgerTxList=new ArrayList();       
        BillDetailsVO billVO=new BillDetailsVO();
        
             try{
                   //fill trasaction vo   
            	 logger.debug(" From Date: "+printTxVO.getFromDate()+" To Date: "+printTxVO.getToDate());      
            	   String societyId=(String) printTxVO.getPrintParam().get("orgID");
                   String ledgerID=(String) printTxVO.getPrintParam().get("ledgerID");
                                     
                   societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId)); 
                   HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
                   printTxVO.setReportID(temmplateHashmap.get("bill").trim());
                   	   
                  billVO=invoiceService.getInvoiceBasedBillDetails(societyVO.getSocietyID(), printTxVO.getFromDate(), printTxVO.getToDate(),Integer.parseInt(ledgerID));
                  ledgerTxList.add(billVO);
                                  
                //fill printreportVO                   
                 printReportVO.setPrintParam( printTxVO.getPrintParam());
                 printReportVO.setReportContentType(printTxVO.getReportContentType());
                 printReportVO.setReportID(printTxVO.getReportID());
                 printReportVO.setBeanList(ledgerTxList);
                 printReportVO.setSourceType(printTxVO.getSourceType());
                 printReportVO.setSocietyVO(societyVO);
                 printReportVO= this.printReport(printReportVO);
                 
             
             }catch (Exception e) {
                  logger.error("Exception : public PrintReportVO printBillCustomer(TransactionVO printTxVO)  "+e);
            }
      
       logger.debug("Exit : public PrintReportVO printBillCustomer(TransactionVO printTxVO)   ");
            return printReportVO;
   }	
	
	
	public PrintReportVO printAllCustomerBillReport(TransactionVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printAllCustomerBillReport(TransactionVO printTxVO)  ");
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        BigDecimal roundOff=BigDecimal.ZERO;           
        List memberList=new ArrayList();
        List ledgerTxList=new ArrayList();              
        
             try{
                   //fill trasaction vo                     
                   String societyId=(String) printTxVO.getPrintParam().get("orgID");
                   societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));   
                   HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
                   printTxVO.setReportID(temmplateHashmap.get("bill").trim());
                   
                   memberList=ledgerService.getLedgerProfileList(societyVO.getSocietyID(), "C");
                   logger.info("Org Id: "+societyId+" From Date: "+printTxVO.getFromDate()+" To Date: "+printTxVO.getToDate()+" Member List :" +memberList.size()+societyVO.getSocietyName()); 
                  
                   for(int i=0;memberList.size()>i;i++){
                        
                	   AccountHeadVO acHVO= (AccountHeadVO) memberList.get(i);                    
                          
                         BillDetailsVO billVO=new BillDetailsVO();                        
                        
                         billVO=invoiceService.getInvoiceBasedBillDetails(societyVO.getSocietyID(), printTxVO.getFromDate(), printTxVO.getToDate(),acHVO.getLedgerID());
		                         ledgerTxList.add(billVO);                             
                                
                   }
                                  
                //fill printreportVO
                   
                 printReportVO.setPrintParam( printTxVO.getPrintParam());
                 printReportVO.setReportContentType(printTxVO.getReportContentType());
                 printReportVO.setReportID(printTxVO.getReportID());
                 printReportVO.setBeanList(ledgerTxList);
                 printReportVO.setSourceType(printTxVO.getSourceType());
                 printReportVO= this.printReport(printReportVO);
                 
             
             }catch (Exception e) {
                  logger.info("Exception : public PrintReportVO printAllCustomerBillReport(TransactionVO printTxVO)  "+e);
            }
      
       logger.debug("Exit : public PrintReportVO printAllCustomerBillReport(TransactionVO printTxVO)   ");
            return printReportVO;
   }	
	
	
	public PrintReportVO printCustomerInvoice(TransactionVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printCustomerInvoice(TransactionVO printTxVO)  ");
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();      
        List ledgerTxList=new ArrayList();       
        InvoiceVO invoiceVO=new InvoiceVO();
        
             try{
                   //fill trasaction vo   
                 	   String societyId=(String) printTxVO.getPrintParam().get("orgID");
                   String invoiceID=(String) printTxVO.getPrintParam().get("invoiceID");
                   String invoiceType=(String) printTxVO.getPrintParam().get("invoiceType");
                   
                   societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId)); 
                   HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
                  
                   
             	  invoiceVO.setOrgID(Integer.parseInt(societyId));	   
                  invoiceVO.setInvoiceID(Integer.parseInt(invoiceID));
                  invoiceVO=invoiceService.getInvoiceDetails(invoiceVO);
                  
                   if(invoiceType.equalsIgnoreCase("1")){
                	   //bill of supply invoice
                	  printTxVO.setReportID(temmplateHashmap.get("billofsupply").trim());
                   	  invoiceVO.setTotalAmountInWords(amtInWrds.convertToAmtAllCurrency(invoiceVO.getInvoiceAmount().toString(),invoiceVO.getCurrencyVO().getCurrencyName(),invoiceVO.getCurrencyVO().getDecimalCurrency()));
        
                  }else if (invoiceType.equalsIgnoreCase("0")){
                	  printTxVO.setReportID(temmplateHashmap.get("invoice").trim());
                	  //normal invoice
                
                  }else{
                	  printTxVO.setReportID(invoiceType.trim()+".jasper");
                  }
                                      
                   ledgerTxList.add(invoiceVO);
                 
                                  
                //fill printreportVO                   
                 printReportVO.setPrintParam( printTxVO.getPrintParam());
                 printReportVO.setReportContentType(printTxVO.getReportContentType());
                 printReportVO.setReportID(printTxVO.getReportID());
                 printReportVO.setBeanList(ledgerTxList);
                 printReportVO.setSourceType(printTxVO.getSourceType());
                 printReportVO.setSocietyVO(societyVO);
                 printReportVO= this.printReport(printReportVO);
                 
             
             }catch (Exception e) {
                  logger.error("Exception : public PrintReportVO printCustomerInvoice(TransactionVO printTxVO)  "+e);
            }
      
       logger.debug("Exit : public PrintReportVO printCustomerInvoice(TransactionVO printTxVO)   ");
            return printReportVO;
   }	
	
	
	public PrintReportVO printTransactionReport(TransactionVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printTransactionReport(TransactionVO printTxVO) "+printTxVO.getFromDate());
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
    	LedgerTrnsVO ledgerTxVO=new LedgerTrnsVO();
        List collection = new ArrayList();
        List transactionList=new ArrayList();
        AccountHeadVO accountHeadVO=new AccountHeadVO();
        
           try{
               //fill trasaction vo
               String societyId=(String) printTxVO.getPrintParam().get("orgID");
               ledgerTxVO=ledgerService.getQuickLedgerTransactionList(Integer.parseInt(societyId), printTxVO.getLedgerID(),printTxVO.getFromDate(), printTxVO.getToDate(),"ALL");
               societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));   
               String reportName=printTxVO.getReportID();
               HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
               printTxVO.setReportID(temmplateHashmap.get(reportName).trim());
             
               accountHeadVO.setDrOpnBal(ledgerTxVO.getDrOpnBalance());
               accountHeadVO.setDrClsBal(ledgerTxVO.getDrClsBalance());
               accountHeadVO.setCrOpnBal(ledgerTxVO.getCrOpnBalance());
               accountHeadVO.setCrClsBal(ledgerTxVO.getCrClsBalance());
               accountHeadVO.setClosingBalance(ledgerTxVO.getClosingBalance());
               accountHeadVO.setOpeningBalance(ledgerTxVO.getOpeningBalance());
               
               transactionList=ledgerTxVO.getTransactionList();               
               
               printTxVO.setLedgerName(ledgerTxVO.getLedgerName());
               printTxVO.setAccountHeadVO(accountHeadVO);
               printTxVO.setLedgerEntries(transactionList);
               printTxVO.setSocietyVO(societyVO);
                              
               collection.add(printTxVO);
               
	            //fill printreportVO
            	  
	           printReportVO.setPrintParam( printTxVO.getPrintParam());
	           printReportVO.setReportContentType(printTxVO.getReportContentType());
	           printReportVO.setReportID(printTxVO.getReportID());
	           printReportVO.setBeanList(collection);
	           printReportVO.setSourceType(printTxVO.getSourceType());
	           printReportVO.setSocietyVO(societyVO);
	           
	           printReportVO= this.printReport(printReportVO);
	           printReportVO.setStatusCode(200);
              
          
           }catch (Exception e) {        	   
              logger.error("Exception in printTransactionReport: "+e);
          }
       logger.debug("Exit : public PrintReportVO printTransactionReport(TransactionVO printTxVO)  ");
          return printReportVO;
  }
	
	public PrintReportVO printIncomeExpenseReport(ReportVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printIncomeExpenseReport(TransactionVO printTxVO) "+printTxVO.getFromDate());
        PrintReportVO printReportVO= new PrintReportVO();
        ReportVO reportVO=new ReportVO();
        List collection = new ArrayList();
        ReportVO prevReportVO=new ReportVO();   
        Boolean isSame=false;
        List finalIncomeDetailList=new ArrayList();;
        List finalExpenseDetailList=new ArrayList();;
        try{
        	   
               //fill report vo
              String societyId=(String) printTxVO.getPrintParam().get("orgID");
              String formatType=(String) printTxVO.getPrintParam().get("formatType");
              String reportID=(String) printTxVO.getPrintParam().get("reportID");
                     
               reportVO=reportService.getIndASProfitLossReport(Integer.parseInt(societyId), printTxVO.getFromDate(), printTxVO.getUptDate(),formatType,Integer.parseInt(reportID));		
              HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(reportVO.getSocietyVO().getTemplate());  
              
          	//Fill income details list
              if(reportVO.getIncomeTxList().size()>0){
            	int index=0;
	              for(int i=0;reportVO.getIncomeTxList().size()>i;i++){
	            	  ReportDetailsVO incVO=(ReportDetailsVO) reportVO.getIncomeTxList().get(i);
	            	
	            	  List incExpList=new ArrayList();;
	            	  
	            	  if(incVO.getId()>0){
	            		  incExpList=reportService.getProfitLossDetailsForLabelReportPrint(Integer.parseInt(societyId), printTxVO.getFromDate(), printTxVO.getUptDate(), incVO.getId(),incVO.getCategoryName(),0);
	               		if(incExpList.size()>0) 	            			
	            		  finalIncomeDetailList.addAll(incExpList);
	            	  }
	        		
	              }
              }
              
          	//Fill expense details list
              if(reportVO.getExpenseTxList().size()>0){
            	int index=0;
	              for(int i=0;reportVO.getExpenseTxList().size()>i;i++){
	            	  ReportDetailsVO incVO=(ReportDetailsVO) reportVO.getExpenseTxList().get(i);
	            	
	            	  List expDetailList=new ArrayList();;
	            	  
	            	  if(incVO.getId()>0){
	            		  expDetailList=reportService.getProfitLossDetailsForLabelReportPrint(Integer.parseInt(societyId), printTxVO.getFromDate(), printTxVO.getUptDate(), incVO.getId(),incVO.getCategoryName(),0);
	               		if(expDetailList.size()>0) 	            			
	               			finalExpenseDetailList.addAll(expDetailList);
	            	  }
	        		
	              }
              }
              
       
              
              if(formatType.equalsIgnoreCase("I") ){ 
              printTxVO.setReportID(temmplateHashmap.get("incomeExpense").trim());
              }else if(formatType.equalsIgnoreCase("S") ){             	
              
              printTxVO.setReportID(temmplateHashmap.get("incomeExpense").trim());
             
            }else if(formatType.equalsIgnoreCase("N") || formatType.equalsIgnoreCase("T"))  {
            	
            	ReportVO dateVO=dateUtil.getReportDate(printTxVO.getFromDate(), printTxVO.getUptDate());
            	
            	if(formatType.equalsIgnoreCase("N") ){
            		 printTxVO.setReportID("incomeExpenseNformat.jasper");
            	}else{
            		 printTxVO.setReportID("incomeExpenseTformat.jasper");
            	}
            
            
         
              if(reportVO.getExcessType()!=null){
	              if((reportVO.getExcessType().equalsIgnoreCase("Net Profit after tax"))
	            		  && (reportVO.getPrevExcessType().equalsIgnoreCase("Net Profit after tax"))
	            		  || ((reportVO.getExcessType().equalsIgnoreCase("Net Loss after tax"))
	            		  && reportVO.getPrevExcessType().equalsIgnoreCase("Net Loss after tax"))){
	            	  isSame=true;
	              }else{
	            	  isSame=false;
	              }
              }
              
              if(reportVO.getExcessType().equalsIgnoreCase("Net Loss after tax")){
            	  
            	   ReportDetailsVO excessIncomeVO=new ReportDetailsVO();
                   excessIncomeVO.setCategoryName(reportVO.getExcessType());
                   if(isSame){
                   	   excessIncomeVO.setPrevTotalAmount(reportVO.getPrevExcessAmount());
                   }else{
                	   excessIncomeVO.setPrevTotalAmount(BigDecimal.ZERO);
                   }
                   excessIncomeVO.setTotalAmount(reportVO.getExcessAmount());       
     			   reportVO.getIncomeTxList().add(excessIncomeVO);  
              }else {
           
              
				  ReportDetailsVO excessExpenseVO=new ReportDetailsVO();
				  excessExpenseVO.setCategoryName(reportVO.getExcessType());	 
				  excessExpenseVO.setTotalAmount(reportVO.getExcessAmount());
				  if(isSame){
					  excessExpenseVO.setPrevTotalAmount(reportVO.getPrevExcessAmount());
                  }else{
                	  excessExpenseVO.setPrevTotalAmount(BigDecimal.ZERO);
                  }
			    				
				  reportVO.getExpenseTxList().add(excessExpenseVO);  
              
              }
              
            if(reportVO.getPrevExcessType()!=null){
               if(reportVO.getPrevExcessType().equalsIgnoreCase("Net Loss after tax") && (!isSame)){
            	  ReportDetailsVO excessIncomeVO=new ReportDetailsVO();
                  excessIncomeVO.setCategoryName(reportVO.getPrevExcessType());
                  excessIncomeVO.setPrevTotalAmount(reportVO.getPrevExcessAmount());
                  excessIncomeVO.setTotalAmount(BigDecimal.ZERO);
                  				
	    			  reportVO.getIncomeTxList().add(excessIncomeVO);  
	             }else if(!isSame) {
	          
	             
					  ReportDetailsVO excessExpenseVO=new ReportDetailsVO();
					  excessExpenseVO.setCategoryName(reportVO.getPrevExcessType());			 
					  excessExpenseVO.setTotalAmount(BigDecimal.ZERO);
					  excessExpenseVO.setPrevTotalAmount(reportVO.getPrevExcessAmount());
					 				
					  reportVO.getExpenseTxList().add(excessExpenseVO);  
	             }
              }    
              
              int incomeLength=0,expenseLength=0;
              incomeLength=reportVO.getIncomeTxList().size();
              expenseLength=reportVO.getExpenseTxList().size();   
              logger.debug("incomeLength : "+incomeLength+" expenseLength: "+expenseLength);
              int remainRows=0;
	             if(incomeLength>expenseLength){
	           	  remainRows=incomeLength-expenseLength;
	           	  for(int i=0;remainRows>i;i++){
	           			 ReportDetailsVO prtLssVO=new ReportDetailsVO();
	       				 prtLssVO.setCategoryName("");
	       				 prtLssVO.setLedgerName("");        				
	       				 reportVO.getExpenseTxList().add(prtLssVO);  
	                 }
	           	  
	             }else if(expenseLength>incomeLength){
	           	  remainRows=expenseLength-incomeLength;
	           	  
	           	  for(int i=0;remainRows>i;i++){
	        			 ReportDetailsVO prtLssVO=new ReportDetailsVO();
	    				 prtLssVO.setCategoryName("");
	    				 prtLssVO.setLedgerName("");     				
	    				 reportVO.getIncomeTxList().add(prtLssVO);  
	                }
	             }
	              
          
            }
              
              printTxVO.setIncomeTxList(reportVO.getIncomeTxList());
              printTxVO.setExpenseTxList(reportVO.getExpenseTxList());
              printTxVO.setIncomeDetailsTxList(finalIncomeDetailList);
              printTxVO.setExpenseDetailsTxList(finalExpenseDetailList);
              printTxVO.setSocietyVO(reportVO.getSocietyVO());
              printTxVO.setRptFromDate(reportVO.getRptFromDate());
              printTxVO.setRptUptoDate(reportVO.getRptUptoDate());
              printTxVO.setExcessType(reportVO.getExcessType());
              printTxVO.setExcessAmount(reportVO.getExcessAmount());
              printTxVO.setPrevExcessType(reportVO.getPrevExcessType());
              printTxVO.setPrevExcessAmount(reportVO.getPrevExcessAmount());
              
              printTxVO.setIncomeTotal(reportVO.getIncomeTotal());
              printTxVO.setExpenseTotal(reportVO.getExpenseTotal());
              printTxVO.setPrevIncTotal(reportVO.getPrevIncTotal());
              printTxVO.setPrevExpTotal(reportVO.getPrevExpTotal());
              
              collection.add(printTxVO);
               
	           //fill printreportVO            	  
	           printReportVO.setPrintParam( printTxVO.getPrintParam());
	           printReportVO.setReportContentType(printTxVO.getReportContentType());
	           printReportVO.setReportID(printTxVO.getReportID());
	           printReportVO.setBeanList(collection);
	           printReportVO.setSourceType(printTxVO.getSourceType());
	              
	           printReportVO= this.printReport(printReportVO);
	           printReportVO.setStatusCode(200);
              
          
           }catch (Exception e) {        	   
              logger.error("Exception in printIncomeExpenseReport: "+e);
          }
       logger.debug("Exit : public PrintReportVO printIncomeExpenseReport(ReportVO printTxVO)  ");
          return printReportVO;
  }
	
	public PrintReportVO printProfitLossReport(ReportVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printProfitLossReport(TransactionVO printTxVO) "+printTxVO.getFromDate());
        PrintReportVO printReportVO= new PrintReportVO();
        ReportVO reportVO=new ReportVO();
        List collection = new ArrayList();
           
           try{
        	   
               //fill report vo
              String societyId=(String) printTxVO.getPrintParam().get("orgID");
                      
              reportVO=reportService.getProfitLossReportForPrint(Integer.parseInt(societyId), printTxVO.getFromDate(), printTxVO.getUptDate(),(String) printTxVO.getPrintParam().get("formatType"));
            
              HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(reportVO.getSocietyVO().getTemplate());
              printTxVO.setReportID(temmplateHashmap.get("incomeExpense").trim());
              
              printTxVO.setIncomeTxList(reportVO.getIncomeTxList());
              printTxVO.setExpenseTxList(reportVO.getExpenseTxList());
              printTxVO.setIncomeDetailsTxList(reportVO.getIncomeDetailsTxList());
              printTxVO.setExpenseDetailsTxList(reportVO.getExpenseDetailsTxList());
              printTxVO.setSocietyVO(reportVO.getSocietyVO());
              printTxVO.setRptFromDate(reportVO.getRptFromDate());
              printTxVO.setRptUptoDate(reportVO.getRptUptoDate());
              printTxVO.setExcessType(reportVO.getExcessType());
              printTxVO.setExcessAmount(reportVO.getExcessAmount());
              collection.add(printTxVO);
               
	           //fill printreportVO            	  
	           printReportVO.setPrintParam(printTxVO.getPrintParam());
	           printReportVO.setReportContentType(printTxVO.getReportContentType());
	           printReportVO.setReportID(printTxVO.getReportID());
	           printReportVO.setBeanList(collection);
	           printReportVO.setSourceType(printTxVO.getSourceType());
	              
	           printReportVO= this.printReport(printReportVO);
	           printReportVO.setStatusCode(200);
              
          
           }catch (Exception e) {        	   
              logger.error("Exception in printProfitLossReport: "+e);
          }
       logger.debug("Exit : public PrintReportVO printProfitLossReport(ReportVO printTxVO)  ");
          return printReportVO;
  }	
	
		
	public PrintReportVO printReceiptPaymentReport(ReportVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printReceiptPaymentReport(printReceiptPaymentReport printTxVO) "+printTxVO.getFromDate());
        PrintReportVO printReportVO= new PrintReportVO();
        ReportVO reportVO=new ReportVO();
        List collection = new ArrayList();
           
           try{
        	   
               //fill report vo
              String societyId=(String) printTxVO.getPrintParam().get("orgID");
                      
              reportVO=reportService.getCashBasedReceiptPaymentReport(Integer.parseInt(societyId), printTxVO.getFromDate(), printTxVO.getUptDate(),"IE");
             
              HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(reportVO.getSocietyVO().getTemplate());
              printTxVO.setReportID(temmplateHashmap.get("receiptPayment").trim());
              
              printTxVO.setIncomeTxList(reportVO.getIncomeTxList());
              printTxVO.setExpenseTxList(reportVO.getExpenseTxList());
              printTxVO.setIncomeDetailsTxList(reportVO.getIncomeDetailsTxList());
              printTxVO.setExpenseDetailsTxList(reportVO.getExpenseDetailsTxList());
              printTxVO.setSocietyVO(reportVO.getSocietyVO());
              printTxVO.setRptFromDate(reportVO.getRptFromDate());
              printTxVO.setRptUptoDate(reportVO.getRptUptoDate());
              printTxVO.setExcessType(reportVO.getExcessType());
              printTxVO.setExcessAmount(reportVO.getExcessAmount());
              collection.add(printTxVO);
               
	           //fill printreportVO            	  
	           printReportVO.setPrintParam( printTxVO.getPrintParam());
	           printReportVO.setReportContentType(printTxVO.getReportContentType());
	           printReportVO.setReportID(printTxVO.getReportID());
	           printReportVO.setBeanList(collection);
	           printReportVO.setSourceType(printTxVO.getSourceType());
	              
	           printReportVO= this.printReport(printReportVO);
	           printReportVO.setStatusCode(200);
              
          
           }catch (Exception e) {        	   
              logger.error("Exception in printReceiptPaymentReport: "+e);
          }
       logger.debug("Exit : public PrintReportVO printReceiptPaymentReport(ReportVO printTxVO)  ");
          return printReportVO;
  }
	
	public PrintReportVO printCashFlowReport(ReportVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printReceiptPaymentReport(printReceiptPaymentReport printTxVO) "+printTxVO.getFromDate());
        PrintReportVO printReportVO= new PrintReportVO();
        ReportVO reportVO=new ReportVO();
        List collection = new ArrayList();
           
           try{
        	   
               //fill report vo
              String societyId=(String) printTxVO.getPrintParam().get("orgID");
                      
              reportVO=reportService.getCashBasedReceiptPaymentReport(Integer.parseInt(societyId), printTxVO.getFromDate(), printTxVO.getUptDate(),"CF");
             
              HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(reportVO.getSocietyVO().getTemplate());
              printTxVO.setReportID(temmplateHashmap.get("cashFlow").trim());
              
              printTxVO.setIncomeTxList(reportVO.getIncomeTxList());
              printTxVO.setExpenseTxList(reportVO.getExpenseTxList());
              printTxVO.setIncomeDetailsTxList(reportVO.getIncomeDetailsTxList());
              printTxVO.setExpenseDetailsTxList(reportVO.getExpenseDetailsTxList());
              printTxVO.setSocietyVO(reportVO.getSocietyVO());
              printTxVO.setRptFromDate(reportVO.getRptFromDate());
              printTxVO.setRptUptoDate(reportVO.getRptUptoDate());
              printTxVO.setExcessType(reportVO.getExcessType());
              printTxVO.setExcessAmount(reportVO.getExcessAmount());
              collection.add(printTxVO);
               
	           //fill printreportVO            	  
	           printReportVO.setPrintParam( printTxVO.getPrintParam());
	           printReportVO.setReportContentType(printTxVO.getReportContentType());
	           printReportVO.setReportID(printTxVO.getReportID());
	           printReportVO.setBeanList(collection);
	           printReportVO.setSourceType(printTxVO.getSourceType());
	              
	           printReportVO= this.printReport(printReportVO);
	           printReportVO.setStatusCode(200);
              
          
           }catch (Exception e) {        	   
              logger.error("Exception in printReceiptPaymentReport: "+e);
          }
       logger.debug("Exit : public PrintReportVO printReceiptPaymentReport(ReportVO printTxVO)  ");
          return printReportVO;
  }
	
	public PrintReportVO printTrialBalanceReport(ReportVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printTrialBalanceReport(ReportVO printTxVO) "+printTxVO.getFromDate());
        PrintReportVO printReportVO= new PrintReportVO();
        ReportVO reportVO=new ReportVO();
        SocietyVO societyVO=new SocietyVO();
        List collection = new ArrayList();
        List accHeadListList=new ArrayList();   
        List ledgerDetailList=new ArrayList();   
        List liabilityList=new ArrayList();
		List assetsList=new ArrayList();
		List incomeList=new ArrayList();
		List expenseList=new ArrayList();
           try{
        	  
               //fill trasaction vo
              String societyId=(String) printTxVO.getPrintParam().get("orgID");
              logger.info("org ID: "+societyId+" From Date: "+printTxVO.getFromDate()+" To Date: "+printTxVO.getUptDate());
              accHeadListList=trialBalanceService.getTrialBalanceReport(Integer.parseInt(societyId), printTxVO.getFromDate(), printTxVO.getUptDate());
              ledgerDetailList=trialBalanceService.getTrialBalanceLedgerReport(Integer.parseInt(societyId), printTxVO.getFromDate(), printTxVO.getUptDate());
              
              societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));     		 
     		  HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
               printTxVO.setReportID(temmplateHashmap.get("trialBalance").trim());
               logger.info("size: "+accHeadListList.size()+" Ledger size: "+ledgerDetailList.size()); 
               
             
               
             /* for(int i=0;ledgerDetailList.size()>i;i++){
			 		AccountHeadVO trialBalanceVO=(AccountHeadVO) ledgerDetailList.get(i);
			 				 	   
			 		if(trialBalanceVO.getRootID()==1){
			 			incomeList.add(trialBalanceVO);			 		
			 			 			
			 		}else if(trialBalanceVO.getRootID()==2){
			 			expenseList.add(trialBalanceVO);
			 						 			
			 		}else if(trialBalanceVO.getRootID()==4){
			 			liabilityList.add(trialBalanceVO);			 		
			 			 			
			 		}else if(trialBalanceVO.getRootID()==3){
			 			assetsList.add(trialBalanceVO);
			 						 			
			 		}
			 		
			 	
			 	}*/
              
     		  printTxVO.setAccountBalanceList(accHeadListList);
     		  printTxVO.setIncomeDetailsTxList(ledgerDetailList);     	
              printTxVO.setSocietyVO(societyVO);
              collection.add(printTxVO);
               
	           //fill printreportVO            	  
	           printReportVO.setPrintParam( printTxVO.getPrintParam());
	           printReportVO.setReportContentType(printTxVO.getReportContentType());
	           printReportVO.setReportID(printTxVO.getReportID());
	           printReportVO.setBeanList(collection);
	           printReportVO.setSourceType(printTxVO.getSourceType());
	              
	           printReportVO= this.printReport(printReportVO);
	           printReportVO.setStatusCode(200);
              
          
           }catch (Exception e) {        	   
              logger.error("Exception in printTrialBalanceReport: "+e);
          }
       logger.debug("Exit : public PrintReportVO printTrialBalanceReport(ReportVO printTxVO)  ");
          return printReportVO;
  }	
	
	
	public PrintReportVO printNomination(NominationRegisterVO printNomineeVO)  {
        logger.debug("Entry : public PrintReportVO printNomination(NominationRegisterVO printNomineeVO)  ");
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();        
        MemberVO memberVO= new MemberVO();
        List ledgerTxList=new ArrayList();    
        NominationRegisterVO nominationRegisterVO=new NominationRegisterVO();
        List nomineeList = null;
        HashMap<String, Object> param= printNomineeVO.getPrintParam();
        try{
                   //fill nominee vo               	
        	       String aptID=(String) printNomineeVO.getPrintParam().get("aptID");
        	       memberVO=memberService.getMemberInfo(Integer.parseInt(aptID));
        	       param.put("orgID", String.valueOf(memberVO.getSocietyID()));
        	       printNomineeVO.setPrintParam(param);
                  
                   societyVO=societyService.getSocietyDetails("0", memberVO.getSocietyID()); 
                   HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
                   printNomineeVO.setReportID(temmplateHashmap.get("nominationForm").trim());
                                                  			        
			       nomineeList=nominationRegService.getNominationListForMember(memberVO.getMemberID());
			       
			       logger.info("Society Id: "+memberVO.getSocietyID()+" APT Id: "+aptID+" size of nominee: "+nomineeList.size());    
			       
			       if(nomineeList.size()>0){			    	   
			    	   nominationRegisterVO.setNomineeList(nomineeList);			    	   
			       }else{			    	   
			    	   for(int i=0;i<4;i++){
			    		   NominationRegisterVO nomineeVO=new NominationRegisterVO();			    		   
			    		   nomineeList.add(i, nomineeVO);
			    	   }
			    	   nominationRegisterVO.setNomineeList(nomineeList);
			       }
                                
			       nominationRegisterVO.setSocietyVO(societyVO);                       
			       nominationRegisterVO.setMemberVO(memberVO);
                   ledgerTxList.add(nominationRegisterVO);
                                  
                //fill printreportVO                   
                 printReportVO.setPrintParam( printNomineeVO.getPrintParam());
                 printReportVO.setReportContentType(printNomineeVO.getReportContentType());
                 printReportVO.setReportID(printNomineeVO.getReportID());
                 printReportVO.setBeanList(ledgerTxList);
                 printReportVO.setSourceType(printNomineeVO.getSourceType());
                 printReportVO= this.printReport(printReportVO);
                 
             
             }catch (Exception e) {
                  logger.error("Exception : public PrintReportVO printNomination(NominationRegisterVO printNomineeVO)  "+e);
            }
      
       logger.debug("Exit : public PrintReportVO printNomination(NominationRegisterVO printNomineeVO)   ");
            return printReportVO;
   }	
	
	
	public PrintReportVO printMemberReceiptList(TransactionVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printMemberReceiptList(TransactionVO printTxVO)  ");
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
                   
        List receiptList=new ArrayList();
        List ledgerTxList=new ArrayList();              
        
             try{
                   //fill trasaction vo                     
                   String societyId=(String) printTxVO.getPrintParam().get("orgID");
                  
                   societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));  
                   HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
                   printTxVO.setReportID(temmplateHashmap.get("receipt").trim());
                   
                   receiptList=transactionService.getMemberReceiptList(societyVO.getSocietyID(), printTxVO.getFromDate(), printTxVO.getToDate());
                   logger.info(societyVO.getSocietyName()+" Receipt List :" +receiptList.size()+" From Date "+printTxVO.getFromDate()+" To Date "+printTxVO.getToDate()); 
                  
                   if(receiptList.size()>0){
	                   for(int i=0;receiptList.size()>i;i++){
	                        
	                	   TransactionVO txVO= (TransactionVO) receiptList.get(i);
	        
	                       txVO.setSocietyVO(societyVO);
	                                                          
	                       ledgerTxList.add(txVO);
	                   }
                   }
                                  
                //fill printreportVO
                   
                 printReportVO.setPrintParam( printTxVO.getPrintParam());
                 printReportVO.setReportContentType(printTxVO.getReportContentType());
                 printReportVO.setReportID(printTxVO.getReportID());
                 printReportVO.setBeanList(ledgerTxList);
                 printReportVO.setSourceType(printTxVO.getSourceType());
                 printReportVO= this.printReport(printReportVO);
                 
             
             }catch (Exception e) {
                  logger.error("Exception : public PrintReportVO printAllMemberLedgerReport(TransactionVO printTxVO)  ");
            }
      
       logger.debug("Exit : public PrintReportVO printMemberReceiptList(TransactionVO printTxVO)   ");
            return printReportVO;
}

	public PrintReportVO printMemberDemandNotice(TransactionVO printTxVO)  {
	       logger.debug("Entry : public PrintReportVO printMemberDemandNotice(TransactionVO printTxVO)  ");
	       PrintReportVO printReportVO= new PrintReportVO();
	       SocietyVO societyVO= new SocietyVO();
	       BigDecimal roundOff=BigDecimal.ZERO;           
	       List memberList=new ArrayList();
	       List ledgerTxList=new ArrayList();              
	       BillDetailsVO billVO = new BillDetailsVO();
	            try{
	                  //fill trasaction vo                     
	                  String societyId=(String) printTxVO.getPrintParam().get("orgID");
	                  String dueDate=(String) printTxVO.getPrintParam().get("dueDate");
	                  String aptID=(String) printTxVO.getPrintParam().get("aptID");
	                  String templateType =(String)  printTxVO.getPrintParam().get("templateType");
	                  societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));   
	           //       HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
	     String noticeName="";
	       if(templateType.equalsIgnoreCase("1")){
	 printTxVO.setReportID("demandNotice.jasper");
	 noticeName="DEMAND NOTICE 1";
	}else if(templateType.equalsIgnoreCase("2")){
	 printTxVO.setReportID("demandNotice.jasper");
	 noticeName="DEMAND NOTICE 2";
	}else if(templateType.equalsIgnoreCase("3")){
	noticeName="DEMAND NOTICE 3";
	 printTxVO.setReportID("demandNotice2.jasper");
	}
	                
	                  
	                  
	                  logger.info("Society Id: "+societyId+" Due Date: "+dueDate+" Apt ID: "+aptID);                                        
	                       
	                  billVO=invoiceService.getDemandNoticeBalanceDetails(societyVO.getSocietyID(), dueDate, dueDate,Integer.parseInt(aptID));
	  billVO.setLedgerDescription(noticeName);
	                  billVO.setDueDate(dueDate);
	                  ledgerTxList.add(billVO);                             
	                                                  
	                                 
	               //fill printreportVO
	                  
	                printReportVO.setPrintParam( printTxVO.getPrintParam());
	                printReportVO.setReportContentType(printTxVO.getReportContentType());
	                printReportVO.setReportID(printTxVO.getReportID());
	                printReportVO.setBeanList(ledgerTxList);
	                printReportVO.setSourceType(printTxVO.getSourceType());
	                printReportVO= this.printReport(printReportVO);
	                
	            
	            }catch (Exception e) {
                  logger.info("Exception : public PrintReportVO printMemberDemandNotice(TransactionVO printTxVO)  "+e);
            }
      
       logger.debug("Exit : public PrintReportVO printMemberDemandNotice(TransactionVO printTxVO)   ");
            return printReportVO;
   }	
	
	
	public PrintReportVO printAllLedgerReport(TransactionVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printAllMemberLedgerReport(TransactionVO printTxVO)  ");
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
                   
        List ledgerList=new ArrayList();
        List ledgerTxList=new ArrayList();              
        
             try{
                   //fill trasaction vo                     
                   String societyId=(String) printTxVO.getPrintParam().get("orgID");
                  
                   societyVO=societyService.getSocietyDetails("0", Integer.parseInt(societyId));  
                   HashMap<String, String> temmplateHashmap=commonUtilty.createPrintTemlateHashMap(societyVO.getTemplate());
                   printTxVO.setReportID("allLedgerReport.jasper");
                   
                   ledgerList=ledgerService.getMasterLedgerList(Integer.parseInt(societyId),printTxVO.getSubGroupID(),0);	
                   logger.info("Ledger type:"+printTxVO.getType()+societyVO.getSocietyName()+" Ledger List :" +ledgerList.size()); 
                   
              if(ledgerList.size()>0){ 
                   for(int i=0;ledgerList.size()>i;i++){
                        
                	   AccountHeadVO ledgerVO= (AccountHeadVO) ledgerList.get(i);
                         TransactionVO txVO=new TransactionVO();
                         LedgerTrnsVO ledgerTxVO=new LedgerTrnsVO();
                         List transactionList=new ArrayList();
                         AccountHeadVO accountHeadVO =new AccountHeadVO();
                         
                         ledgerTxVO=ledgerService.getQuickLedgerTransactionList(Integer.parseInt(societyId), ledgerVO.getLedgerID(),printTxVO.getFromDate(), printTxVO.getToDate(),"ALL");
                       
                         accountHeadVO.setDrOpnBal(ledgerTxVO.getDrOpnBalance());
                         accountHeadVO.setDrClsBal(ledgerTxVO.getDrClsBalance());
                         accountHeadVO.setCrOpnBal(ledgerTxVO.getCrOpnBalance());
                         accountHeadVO.setCrClsBal(ledgerTxVO.getCrClsBalance());
                         accountHeadVO.setStrAccountPrimaryHead(ledgerVO.getStrAccountPrimaryHead());
                         if(ledgerTxVO.getTransactionList().size()>0){
                        	 transactionList=ledgerTxVO.getTransactionList();
                         }
      
                         logger.info("Ledger ID: "+ ledgerVO.getLedgerID()+" Name: "+ ledgerVO.getStrAccountHeadName()+" Transaction Size: "+transactionList.size());
                        
                                   txVO.setLedgerName(ledgerVO.getStrAccountHeadName());
                                   txVO.setFromDate(printTxVO.getFromDate());
                                   txVO.setToDate(printTxVO.getToDate());     
                                   txVO.setSocietyVO(societyVO);
                                   txVO.setAccountHeadVO(accountHeadVO);                                 
                                   txVO.setLedgerEntries(transactionList);
                                   txVO.setType(printTxVO.getType());                                
                                   ledgerTxList.add(txVO);
                   }
              }
                                  
                //fill printreportVO
                   
                 printReportVO.setPrintParam( printTxVO.getPrintParam());
                 printReportVO.setReportContentType(printTxVO.getReportContentType());
                 printReportVO.setReportID(printTxVO.getReportID());
                 printReportVO.setBeanList(ledgerTxList);
                 printReportVO.setSourceType(printTxVO.getSourceType());
                 printReportVO= this.printReport(printReportVO);
                 
             
             }catch (Exception e) {
                  logger.error("Exception : public PrintReportVO printAllMemberLedgerReport(TransactionVO printTxVO)  ");
            }
      
       logger.debug("Exit : public PrintReportVO printAllMemberLedgerReport(TransactionVO printTxVO)   ");
            return printReportVO;
}
	
	public PrintReportVO printBalanceSheetNewReport(ReportVO printTxVO)  {
        logger.debug("Entry : public PrintReportVO printBalanceSheetReport(ReportVO printTxVO)  ");
        PrintReportVO printReportVO= new PrintReportVO();
        SocietyVO societyVO= new SocietyVO();
        List collection = new ArrayList();
        List transactionList=new ArrayList();
        ReportVO reportVO =new ReportVO();
        ReportVO indAsVO =new ReportVO();
        
           try{                           
        	   //fill report vo
               String societyId=(String) printTxVO.getPrintParam().get("orgID");
               String formatType=(String) printTxVO.getPrintParam().get("formatType");
                //logger.info("size: "+societyId+ printTxVO.getUptDate()+printTxVO.getFromDate());          
                         
                indAsVO=balanceSheetService.getIndASBalanceSheetReport(Integer.parseInt(societyId), printTxVO.getFromDate(), printTxVO.getUptDate(),1);
                reportVO=reportService.convertReportObjectForPrinting(indAsVO);
            
                if(formatType.equalsIgnoreCase("N") || formatType.equalsIgnoreCase("T"))  {
                	
                	ReportVO dateVO=dateUtil.getReportDate(printTxVO.getFromDate(), printTxVO.getUptDate());
                	
                	if(formatType.equalsIgnoreCase("N") ){
                		 printTxVO.setReportID("balanceSheetNformat.jasper");
                	}else{
                		 printTxVO.setReportID("balanceSheetTformat.jasper");
                	}
                	
                	 int libilityLength=0,assetsLength=0;
                     libilityLength=reportVO.getLiabilityList().size();
                     assetsLength=reportVO.getAssetsList().size();
                     
                     logger.debug("libilityLength : "+libilityLength+" assetsLength: "+assetsLength);
                     int remainRows=0;
                    if(libilityLength>assetsLength){
                  	  remainRows=libilityLength-assetsLength;
                  	  for(int i=0;remainRows>i;i++){
                  			 ReportDetailsVO prtLssVO=new ReportDetailsVO();
                  			 prtLssVO.setRptGrpType("L");
              				 prtLssVO.setCategoryName("");
              				 prtLssVO.setLedgerName("");        				
              				 reportVO.getAssetsList().add(prtLssVO);  
                        }
                  	  
                    }else if(assetsLength>libilityLength){
                  	  remainRows=assetsLength-libilityLength;
                  	  
                  	  for(int i=0;remainRows>i;i++){
               			 ReportDetailsVO prtLssVO=new ReportDetailsVO();
               			prtLssVO.setRptGrpType("L");
           				  prtLssVO.setCategoryName("");
           				 prtLssVO.setLedgerName("");     				
           				 reportVO.getLiabilityList().add(prtLssVO);  
                       }
                    }
                 
                    
           		 
           		   printTxVO.setLiabilityList(reportVO.getLiabilityList());
         		   printTxVO.setAssetsList(reportVO.getAssetsList());  
           	

                
               }else if(formatType.equalsIgnoreCase("I") || formatType.equalsIgnoreCase("S") ){ 
            	   printTxVO.setReportID("balanceSheetIndAs.jasper");
                        	     	   
            	   printTxVO.setLiabilityList(reportVO.getLiabilityList());
          		   printTxVO.setAssetsList(reportVO.getAssetsList());  
               }
               
               
                 		  
               printTxVO.setSocietyVO(reportVO.getSocietyVO());
               printTxVO.setRptFromDate(reportVO.getRptFromDate());
               printTxVO.setRptUptoDate(reportVO.getRptUptoDate());
                              
               collection.add(printTxVO);
                       
               //fill printreportVO
               printReportVO.setPrintParam( printTxVO.getPrintParam());
               printReportVO.setReportContentType(printTxVO.getReportContentType());
               printReportVO.setReportID(printTxVO.getReportID());
               printReportVO.setBeanList(collection);
               printReportVO.setSourceType(printTxVO.getSourceType());
               printReportVO= this.printReport(printReportVO);
               
          
           }catch (Exception e) {
              logger.error("Exception : public PrintReportVO printBalanceSheetReport(ReportVO printTxVO) "+e);
          }
       logger.debug("Exit : public PrintReportVO printBalanceSheetReport(ReportVO printTxVO)  ");
          return printReportVO;
  } 
	  

	
	public PrintReportDomain getPrintReportDomain() {
		return printReportDomain;
	}

	public void setPrintReportDomain(PrintReportDomain printReportDomain) {
		this.printReportDomain = printReportDomain;
	}

	public SocietyService getSocietyService() {
		return societyService;
	}

	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}

	public TransactionService getTransactionService() {
		return transactionService;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	public MemberService getMemberService() {
		return memberService;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	public ReportService getReportService() {
		return reportService;
	}

	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}

	public InvoiceService getInvoiceService() {
		return invoiceService;
	}

	public void setInvoiceService(InvoiceService invoiceService) {
		this.invoiceService = invoiceService;
	}

	public LedgerService getLedgerService() {
		return ledgerService;
	}

	public void setLedgerService(LedgerService ledgerService) {
		this.ledgerService = ledgerService;
	}

	
	public BalanceSheetService getBalanceSheetService() {
		return balanceSheetService;
	}
	
	public void setBalanceSheetService(BalanceSheetService balanceSheetService) {
		this.balanceSheetService = balanceSheetService;
	}

	public TrialBalanceService getTrialBalanceService() {
		return trialBalanceService;
	}

	public void setTrialBalanceService(TrialBalanceService trialBalanceService) {
		this.trialBalanceService = trialBalanceService;
	}

	public ShareRegService getShareRegService() {
		return shareRegService;
	}

	public void setShareRegService(ShareRegService shareRegService) {
		this.shareRegService = shareRegService;
	}

	public NominationRegService getNominationRegService() {
		return nominationRegService;
	}

	public void setNominationRegService(NominationRegService nominationRegService) {
		this.nominationRegService = nominationRegService;
	}

	public ProjectDetailsService getProjectDetailsService() {
		return projectDetailsService;
	}

	public void setProjectDetailsService(ProjectDetailsService projectDetailsService) {
		this.projectDetailsService = projectDetailsService;
	}

	
	
}