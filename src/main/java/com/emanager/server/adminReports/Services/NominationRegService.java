package com.emanager.server.adminReports.Services;

import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.adminReports.Domain.NominationRegDomain;
import com.emanager.server.adminReports.valueObject.NominationRegisterVO;


public class NominationRegService {
	NominationRegDomain nominationRegDomain;
	private static final Logger logger = Logger.getLogger(NominationRegService.class);
	
	public List getNominationList(int buildingId,int societyId){
	logger.debug("Entry : public List getNominationList(int societyId)");
	List nominationList = null;
	try {
		
	nominationList=nominationRegDomain.getNominationList(buildingId,societyId);	
		
	} catch (Exception e) {
		logger.error("Exception ocurred at getNominationList", e);
	}
	
		
	logger.debug("Exit : public List getNominationList(int societyId)");	
	return nominationList;
	}
    
	
	
	
	
	public int addNominations(List nominationList){
		logger.debug("Entry : public int addNominations(List nominationList)");
		int success = 0;
		try {
			
		success=nominationRegDomain.addNominations(nominationList);	
			
		} catch (Exception e) {
			logger.error("Exception ocurred at addNominationList", e);
		}
		
			
		logger.debug("Exit : public int addNominations(List nominationList)");	
		return success;
		}
	public List getNominationListForMember(int memberId){
		logger.debug("Entry : public List getNominationListForMember(int memberId)");
		List nominationList=null;
		
		nominationList=nominationRegDomain.getNominationListForMember(memberId);
		
		
		logger.debug("Exit : public List getNominationListForMember(int memberId)");	
		return nominationList;
		}
	
	public int deleteNominees(NominationRegisterVO nomineeVO){
		logger.debug("Entry : public int deleteNominees(String memberId)");
		int success = 0;
		try {
			
		success=nominationRegDomain.deleteNominees(nomineeVO);	
			
		} catch (Exception e) {
			logger.error("Exception ocurred at public int deleteNominees(String memberId)", e);
		}
		
			
		logger.debug("Exit :public int deleteNominees(String memberId)");	
		return success;
		}
	
	
	public int updateRecordingDate(NominationRegisterVO nomRegVO){
		logger.debug("Entry : updateRecordingDate(NominationRegisterVO nomRegVO)");
		int success = 0;
		try {
			success = nominationRegDomain.updateRecordingDate(nomRegVO);
				
		} catch (Exception e) {
			logger.error("Exception ocurred at updateRecordingDate(NominationRegisterVO nomRegVO)", e);
		}
					
		logger.debug("Exit : updateRecordingDate(NominationRegisterVO nomRegVO)");	
		return success;
	}
	
	public List getNominationMissingList(int buildingId,int societyId){
		logger.debug("Entry : public List getNominationMissingList(int societyId)");
		List nominationList = null;
		try {
			
		nominationList=nominationRegDomain.getNominationMissingList(buildingId,societyId);	
			
		} catch (Exception e) {
			logger.error("Exception ocurred at getNominationMissingList", e);
		}
		
			
		logger.debug("Exit : public List getNominationMissingList(int societyId)");	
		return nominationList;
	}
	
	
	
	public NominationRegDomain getNominationRegDomain() {
		return nominationRegDomain;
	}

	public void setNominationRegDomain(NominationRegDomain nominationRegDomain) {
		this.nominationRegDomain = nominationRegDomain;
	}
}