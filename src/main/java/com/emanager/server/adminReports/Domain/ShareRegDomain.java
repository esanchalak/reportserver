package com.emanager.server.adminReports.Domain;

import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.adminReports.DAO.ShareRegDAO;
import com.emanager.server.adminReports.valueObject.ShareRegisterVO;

public class ShareRegDomain {
	ShareRegDAO shareRegDAO;
	private static final Logger logger = Logger.getLogger(ShareRegDomain.class);
	
	
	
	public List getShareList(int buildingID,int societyId){
	logger.debug("Entry : public List getShareList(int societyId)");
	List shareList=null;
	
	shareList=shareRegDAO.getShareList(buildingID,societyId);
	
	
	logger.debug("Exit : public List getShareList(int societyId)");	
	return shareList;
	}
	
	
	public ShareRegisterVO getShareDetails(int aptID){
		logger.debug("Entry : public ShareRegisterVO getShareDetails(int aptID)");
		ShareRegisterVO shareCertVO=new ShareRegisterVO();
		
		shareCertVO=shareRegDAO.getShareDetails(aptID);
		
		
		logger.debug("Exit : public ShareRegisterVO getShareDetails(int aptID)");	
		return shareCertVO;
		}
	
	public List getTransferRegister(int societyID,String fromDate,String toDate){
		logger.debug("Entry : public List getTransferRegister(int societyID,String fromDate,String toDate)");
		List transferRegister = null;
		try {
			
		transferRegister=shareRegDAO.getTransferRegister(societyID,fromDate,toDate);
			
		} catch (Exception e) {
			logger.error("Exception ocurred at public List getTransferRegister(int societyID,String fromDate,String toDate)", e);
		}
		
			
		logger.debug("Exit : public List getTransferRegister(int societyID,String fromDate,String toDate)");	
		return transferRegister;
		}

	/*public int addNominations(List nominationList){
		logger.debug("Entry : public int addNominations(List nominationList)");
		int success = 0;
		try {
			
		logger.info("The list consists of "+nominationList.size());
		
		for(int i=0;nominationList.size()>i;i++){
			NominationRegisterVO nomRegVO=(NominationRegisterVO) nominationList.get(i);
			success=nominationRegDAO.addNominations(nomRegVO);	
		}
			
		
			
		} catch (Exception e) {
			logger.error("Exception ocurred at addNominationList", e);
		}
		
			
		logger.debug("Exit : public int addNominations(List nominationList)");	
		return success;
		}
	
	public List getNominationListForMember(int memberId){
		logger.debug("Entry : public List getNominationListForMember(int memberId)");
		List nominationList=null;
		
		nominationList=nominationRegDAO.getNominationListForMember(memberId);
		
		
		logger.debug("Exit : public List getNominationListForMember(int memberId)");	
		return nominationList;
		}

	public NominationRegDAO getNominationRegDAO() {
		return nominationRegDAO;
	}

	public int deleteNominees(String memberId){
	logger.debug("Entry : public int deleteNominees(String memberId)");
	int success = 0;
	try {
		
	
		success=nominationRegDAO.deleteNominees(memberId);	
	
		
	} catch (Exception e) {
		logger.error("Exception ocurred at public int deleteNominees(String memberId)", e);
	}		
	logger.debug("Exit : public int deleteNominees(String memberId)");	
	return success;
	}

	
	public int updateRecordingDate(String recordingDate,String nominationID){
		logger.debug("Entry : updateRecordingDate(String recordingDate,String nominationID)");
		int success = 0;
		try {
			
			
				success = nominationRegDAO.updateRecordingDate(recordingDate, nominationID);
		
			
		
			
		} catch (Exception e) {
			logger.error("Exception ocurred at updateRecordingDate(String recordingDate,String nominationID)", e);
		}
		
			
		logger.debug("Exit : updateRecordingDate(String recordingDate,String nominationID)");	
		return success;
		}
	*/
	
	
	

	public void setShareRegDAO(ShareRegDAO shareRegDAO) {
		this.shareRegDAO = shareRegDAO;
	}
	
	
	
	
}