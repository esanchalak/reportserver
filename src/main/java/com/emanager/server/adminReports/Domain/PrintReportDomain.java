package com.emanager.server.adminReports.Domain;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.SecureRandom;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

import org.apache.log4j.Logger;

import com.emanager.server.adminReports.valueObject.PrintReportVO;
import com.emanager.server.commonUtils.domainObject.ConfigManager;


public class PrintReportDomain {

	public static final String MEDIA_TYPE_EXCEL = "application/vnd.ms-excel";
	public static final String MEDIA_TYPE_PDF = "application/pdf";
	public static final String EXTENSION_TYPE_EXCEL = "xlsx";
	public static final String EXTENSION_TYPE_PDF = "pdf";
	public static final String EXTENSION_TYPE_DOC = "docx";
	
	private static final Logger logger = Logger.getLogger(PrintReportDomain.class);
	
	private DataSource datasource;	
	ConfigManager configManager=new ConfigManager();
	String reportName = "";// society_id+reportID_+randomnumber
	
	public PrintReportVO printDBReport(PrintReportVO printReportVO) {
		
	 logger.debug("Entry : public ReportVO printDBReport(PrintReportVO printReportVO) ");
	    JasperPrint jp = new JasperPrint() ;			
	
		try{
		   	   // return invalid report name
			   if (printReportVO.getReportID() == null){
					logger.info("Template name is invalid ");
					printReportVO.setStatusCode(0);
					printReportVO.setStatusDescription("Template name is invalid ");
				   	return printReportVO;					 
				}
			 //Load template
			    //JasperDesign jd  = JRXmlLoader.load(configManager.getPropertiesValue(printReportVO.getReportID()));
				File file=new File(configManager.getPropertiesValue("template.path")+printReportVO.getReportID());
				JasperReport jr = (JasperReport) JRLoader.loadObject(file);
			//compile report
			//JasperReport jr = JasperCompileManager.compileReport(jd);
			logger.info("Compiled Report Successfully");
	
			// Make sure to pass the JasperReport, report parameters, and data source
				logger.info("Data source: Database Connection"+printReportVO.getPrintParam().get("orgID"));
				jp = JasperFillManager.fillReport(jr,printReportVO.getPrintParam(),datasource.getConnection());
				
		
			// As per export type get pdf or excel report  
			if (printReportVO.getReportContentType().equalsIgnoreCase(EXTENSION_TYPE_PDF)) {					        
				printReportVO=exportPdf(jp,printReportVO);								
			} 						
			if (printReportVO.getReportContentType().equalsIgnoreCase(EXTENSION_TYPE_EXCEL)) {						
				printReportVO=exportXls(jp,printReportVO);
			}
			//db and bean method
			if (printReportVO.getReportContentType().equalsIgnoreCase(EXTENSION_TYPE_DOC)) {						
							printReportVO=exportDocument(jp,printReportVO);
			}
			
			}catch(Exception e){
				logger.error("Exception occured at printDBReport: "+e.getMessage());
				printReportVO.setStatusCode(0);
			}
					
		return printReportVO;
	}
	
	
	public PrintReportVO printBeanReport(PrintReportVO printReportVO)  {
		
		 logger.debug("Entry : public ReportVO printBeanReport(ReportVO printReportVO)  "+printReportVO.getReportID()+printReportVO.getBeanList().size());
		    JasperPrint jp = new JasperPrint() ;			
		
			try{
			   	     // return invalid report name
					if (printReportVO.getReportID() == null){
					   logger.info("Template name is invalid ");
						printReportVO.setStatusCode(0);
						printReportVO.setStatusDescription("Template name is invalid ");
					   return printReportVO;					 
					}
				
				 //Load template
				   // JasperDesign jd  = JRXmlLoader.load(configManager.getPropertiesValue(printReportVO.getReportID()));
					File file=new File(configManager.getPropertiesValue("template.path")+printReportVO.getReportID());
					JasperReport jr = (JasperReport) JRLoader.loadObject(file);
					
				//compile report
				//JasperReport jr = JasperCompileManager.compileReport(jd);
				logger.info("Compiled Report Successfully");
				
		
				// Make sure to pass the JasperReport, report parameters, and data source
					logger.info("Data source: Bean Collection");
					
					jp = JasperFillManager.fillReport(jr,printReportVO.getPrintParam(),new JRBeanCollectionDataSource(printReportVO.getBeanList()));
					
			
				// As per export type get pdf or excel report  
				if (printReportVO.getReportContentType().equalsIgnoreCase(EXTENSION_TYPE_PDF)) {					        
					printReportVO=exportPdf(jp,printReportVO);								
				} 						
				if (printReportVO.getReportContentType().equalsIgnoreCase(EXTENSION_TYPE_EXCEL)) {						
					printReportVO=exportXls(jp,printReportVO);
				}
				//db and bean method
				if (printReportVO.getReportContentType().equalsIgnoreCase(EXTENSION_TYPE_DOC)) {						
								printReportVO=exportDocument(jp,printReportVO);
				}
				
				}catch(Exception e){
					logger.error("Exception occured at printBeanReport: "+e.getMessage()+e);
					printReportVO.setStatusCode(0);
				}
						
			return printReportVO;
		}
		
		
	 //Generate pdf report and 
	public PrintReportVO exportPdf(JasperPrint jp, PrintReportVO printReportVO) {
		String randomNumber=generateOTP();//printReportVO.getReportID()+"_"
		reportName=(String) printReportVO.getPrintParam().get("orgID")+randomNumber+"."+printReportVO.getReportContentType();	
		 PrintReportVO print = new PrintReportVO();
	
		try{		
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
 			JRPdfExporter exporter = new JRPdfExporter();
 			 
 			// Here we assign the parameters jp and baos to the exporter
 			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
 			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);	 			 
 			
 			 exporter.exportReport();
 			 
			//write output file
 			 OutputStream outputStream = new FileOutputStream (configManager.getPropertiesValue("template.registerPath")+reportName);// append report Name 
			 baos.writeTo(outputStream);	
			 outputStream.flush();
			 
			 printReportVO.setReportURL(configManager.getPropertiesValue("template.registerURL").trim()+reportName);//append report Name
			 printReportVO.setReportID(reportName);
			 
			 printReportVO.setStatusCode(1);
			 printReportVO.setReportContentType(baos.toString());
			 printReportVO.setPrintParam(null);	
		    
		    // logger.info("Society ID: "+printReportVO.getPrintParam().get("society_id")+" Content Type: "+printReportVO.getReportContentType()+" Report URL: "+printReportVO.getReportURL()); 
			 logger.info("Generated PDF successfully...."+printReportVO.getReportURL());			
	
		}catch(Exception e){
			logger.error("Exception occured at exportPdf: "+e.getMessage()+e);
			print.setStatusCode(0);			
		}
			
		return printReportVO;
	}
	
	// Generate excel report and return result
	public PrintReportVO exportXls(JasperPrint jp,PrintReportVO printReportVO) {
		String randomNumber=generateOTP();
		reportName=(String) printReportVO.getPrintParam().get("orgID")+randomNumber+"."+printReportVO.getReportContentType();			
		try{
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();           
 			// Here we assign the parameters jp and baos to the exporter
 			//exporterXls.setParameter(JRExporterParameter.JASPER_PRINT, jp);
 		//	exporterXls.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);			 			 
 			// Excel specific parameters
 		//	exporterXls.setParameter(JRXlsAbstractExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
 			//exporterXls.setParameter(JRXlsAbstractExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
 			//exporterXls.setParameter(JRXlsAbstractExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
 			//exporterXls.setParameter(JRXlsAbstractExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE); 			
 			//exporterXls.setParameter(JRXlsAbstractExporterParameter.IS_IGNORE_GRAPHICS, Boolean.TRUE);
			
			//report xls	
			/*	JRXlsExporter exporterXls = new JRXlsExporter();
			exporterXls.setExporterInput(new SimpleExporterInput(jp));
			exporterXls.setExporterOutput(new SimpleOutputStreamExporterOutput(baos));
			SimpleXlsReportConfiguration configXls = new SimpleXlsReportConfiguration();
 			configXls.setDetectCellType(true);
 			configXls.setRemoveEmptySpaceBetweenColumns(true);
 			configXls.setRemoveEmptySpaceBetweenRows(true);
 			configXls.setCollapseRowSpan(true);
 			configXls.setWhitePageBackground(false);
 			exporterXls.setConfiguration(configXls); 			
 			exporterXls.exportReport();*/
 			
			//report xlsx
			JRXlsxExporter exporterXlsx = new JRXlsxExporter();
			exporterXlsx.setExporterInput(new SimpleExporterInput(jp));
			exporterXlsx.setExporterOutput(new SimpleOutputStreamExporterOutput(baos));
			SimpleXlsxReportConfiguration configXlsx = new SimpleXlsxReportConfiguration();
			configXlsx.setDetectCellType(true);
			configXlsx.setRemoveEmptySpaceBetweenColumns(true);
			configXlsx.setRemoveEmptySpaceBetweenRows(true);
			configXlsx.setCollapseRowSpan(true);
			configXlsx.setWhitePageBackground(false);
			exporterXlsx.setConfiguration(configXlsx); 			
			exporterXlsx.exportReport();
 			
 			
 			// write output file
 			OutputStream excelStream = new FileOutputStream (configManager.getPropertiesValue("template.registerPath")+ reportName); 
			baos.writeTo(excelStream);	
			excelStream.flush();
			 
			printReportVO.setReportURL(configManager.getPropertiesValue("template.registerURL")+reportName);//append report Name
			printReportVO.setReportID(reportName);
			printReportVO.setStatusCode(1);			 
						
			logger.info("Society ID: "+printReportVO.getPrintParam().get("orgID")+" Content Type: "+printReportVO.getReportContentType()+" Report URL: "+printReportVO.getReportURL()); 
			logger.info("Generated Excel successfully....");
 			
			}catch(Exception e){
				logger.error("Exception occurred at domain exportXls "+e.getMessage());
				printReportVO.setStatusCode(0);
			}
		return printReportVO;
		
	}
	
	// Generate document report and return result
			public PrintReportVO exportDocument(JasperPrint jp,PrintReportVO printReportVO) {
				String randomNumber=generateOTP();
				reportName=(String) printReportVO.getPrintParam().get("orgID")+randomNumber+"."+printReportVO.getReportContentType();			
				try{
					
					ByteArrayOutputStream baos = new ByteArrayOutputStream();           
		 			
		 			
					//report docx
					JRDocxExporter exporterDocs = new JRDocxExporter();
					exporterDocs.setParameter(JRExporterParameter.JASPER_PRINT, jp);
					exporterDocs.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);	 
		 			
					exporterDocs.exportReport();
		 			
		 			
					//write output file
		 			 OutputStream outputStream = new FileOutputStream (configManager.getPropertiesValue("template.registerPath")+reportName);// append report Name 
					 baos.writeTo(outputStream);	
					 outputStream.flush();
					 
					printReportVO.setReportURL(configManager.getPropertiesValue("template.registerURL")+reportName);//append report Name
					printReportVO.setReportID(reportName);
					printReportVO.setStatusCode(1);			 
								
					logger.info("Org ID: "+printReportVO.getPrintParam().get("orgID")+" Content Type: "+printReportVO.getReportContentType()+" Report URL: "+printReportVO.getReportURL()); 
					logger.info("Generated Document successfully....");
		 			
					}catch(Exception e){
						logger.error("Exception occurred at domain exportDocument "+e.getMessage());
						printReportVO.setStatusCode(0);
					}
				return printReportVO;
				
			}
	
  //method generate 6 digit random number 
    private String generateOTP(){
	     return new BigInteger(130, new SecureRandom()).toString().substring(0,6);
    }
	 
	

	public DataSource getDatasource() {
		return datasource;
	}

	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}
	
	
	    
	
}