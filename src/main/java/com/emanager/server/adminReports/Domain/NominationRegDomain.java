package com.emanager.server.adminReports.Domain;

import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.adminReports.DAO.NominationRegDAO;
import com.emanager.server.adminReports.Services.NominationRegService;
import com.emanager.server.adminReports.valueObject.NominationRegisterVO;
import com.emanager.server.adminReports.valueObject.RptDocumentVO;
import com.emanager.server.dashBoard.Services.DashBoardService;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardMemberVO;
import com.emanager.server.dashBoard.dataAccessObject.DashBoardVO;
import com.emanager.server.society.dataAccessObject.MemberDAO;
import com.emanager.server.society.domainObject.MemberDomain;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.MemberVO;


public class NominationRegDomain {
	NominationRegDAO nominationRegDAO;
	MemberDomain memberDomain;
	MemberDAO memberDAO;
	DashBoardService dashBoardService;
	SocietyService societyServiceBean;
	private static final Logger logger = Logger.getLogger(NominationRegDomain.class);
	
	
	
	public List getNominationList(int buildingID,int societyId){
	logger.debug("Entry : public List getNominationList(int societyId)");
	List nominationList=null;
	DashBoardMemberVO dbMemVO=new DashBoardMemberVO();
	try {
	nominationList=nominationRegDAO.getNominationList(buildingID,societyId);
	
	dbMemVO=dashBoardService.getMemberDetails(societyId);
			
	NominationRegisterVO nmRgVO=(NominationRegisterVO) nominationList.get(0);
	nmRgVO.setMissingNomination(dbMemVO.getMissingNomination());
	
	} catch (IndexOutOfBoundsException ex) {
		logger.info("No nominee found for societyID:  "+societyId);

	} catch (Exception e) {
		logger.error("Exception ocurred at getNominationList", e);
	}
	
	logger.debug("Exit : public List getNominationList(int societyId)");	
	return nominationList;
	}

	
	
	
	public int addNominations(List nominationList){
		logger.debug("Entry : public int addNominations(List nominationList)");
		int success = 0;
		RptDocumentVO memberVO=new RptDocumentVO();
		try {
			
		
		
		for(int i=0;nominationList.size()>i;i++){
			NominationRegisterVO nomRegVO=(NominationRegisterVO) nominationList.get(i);
			
			if((nomRegVO.getAutoInc()!=null)&&(nomRegVO.getAutoInc())){
			int nominationNo=societyServiceBean.getLatestNominationNo(Integer.parseInt(nomRegVO.getSocietyId()));
			nomRegVO.setNominationRegisterID(nominationNo);
			}
			success=nominationRegDAO.addNominations(nomRegVO);	
			memberVO.setMemberID(nomRegVO.getMemberId());
			memberVO.setDocumentID(14);
			memberVO.setDocumentStatusID(1);
			memberVO.setDocumentStatusName("Pending Verification");
			memberVO.setDescription("");
			memberVO.setDescriptionHistory("");
			
			if((nomRegVO.getAutoInc()!=null)&&(nomRegVO.getAutoInc())){
				nominationRegDAO.incrementNominationRegistrationNumber(nomRegVO.getSocietyId());
			}
			
			
		}
			
		if(success!=0){
			
			memberDomain.addMemberDocument(memberVO);
		}
			
		} catch (Exception e) {
			logger.error("Exception ocurred at addNominationList", e);
		}
		
			
		logger.debug("Exit : public int addNominations(List nominationList)");	
		return success;
		}
	
	public List getNominationListForMember(int memberId){
		logger.debug("Entry : public List getNominationListForMember(int memberId)");
		List nominationList=null;
		
		nominationList=nominationRegDAO.getNominationListForMember(memberId);
		
		
		logger.debug("Exit : public List getNominationListForMember(int memberId)");	
		return nominationList;
		}

	public NominationRegDAO getNominationRegDAO() {
		return nominationRegDAO;
	}

	public int deleteNominees(NominationRegisterVO nomRegisterVO){
	logger.debug("Entry : public int deleteNominees(String memberId)");
	RptDocumentVO memberVO=new RptDocumentVO();
	int success = 0;
	try {
		
	
		success=nominationRegDAO.deleteNominees(nomRegisterVO);
		memberVO.setMemberID(nomRegisterVO.getMemberId());
		memberVO.setDocumentID(14);
		
		if(success!=0){
			memberDAO.deleteDocuments(memberVO);
		}
		
	
		
	} catch (Exception e) {
		logger.error("Exception ocurred at public int deleteNominees(String memberId)", e);
	}		
	logger.debug("Exit : public int deleteNominees(String memberId)");	
	return success;
	}

	
	public int updateRecordingDate(NominationRegisterVO nomRegVO){
		logger.debug("Entry : updateRecordingDate(String recordingDate,String nominationID)");
		int success = 0;
		try {
			success = nominationRegDAO.updateRecordingDate(nomRegVO);
			
		}catch (Exception e) {
			logger.error("Exception ocurred at updateRecordingDate(NominationRegisterVO nomRegVO)", e);
		}
		
			
		logger.debug("Exit : updateRecordingDate(NominationRegisterVO nomRegVO)");	
		return success;
	}
	

	public List getNominationMissingList(int buildingId,int societyId){
		logger.debug("Entry : public List getNominationMissingList(int societyId)");
		List nominationList = null;
		try {
			
		nominationList=nominationRegDAO.getNominationMissingList(buildingId,societyId);	
			
		} catch (Exception e) {
			logger.error("Exception ocurred at getNominationMissingList", e);
		}
		
			
		logger.debug("Exit : public List getNominationMissingList(int societyId)");	
		return nominationList;
		}
	
	
	public void setNominationRegDAO(NominationRegDAO nominationRegDAO) {
		this.nominationRegDAO = nominationRegDAO;
	}

	public MemberDomain getMemberDomain() {
		return memberDomain;
	}

	public void setMemberDomain(MemberDomain memberDomain) {
		this.memberDomain = memberDomain;
	}

	public MemberDAO getMemberDAO() {
		return memberDAO;
	}

	public void setMemberDAO(MemberDAO memberDAO) {
		this.memberDAO = memberDAO;
	}




	public void setDashBoardService(DashBoardService dashBoardService) {
		this.dashBoardService = dashBoardService;
	}




	/**
	 * @param societyServiceBean the societyServiceBean to set
	 */
	public void setSocietyServiceBean(SocietyService societyServiceBean) {
		this.societyServiceBean = societyServiceBean;
	}
	
	
	
	
}