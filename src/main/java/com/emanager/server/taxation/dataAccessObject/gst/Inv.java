
package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"inum",	
"idt",
"val",
"pos",
"rchrg",
"inv_typ",
"itms",
"chksum",
"sply_ty",
"nil_amt",
"ngsup_amt",
"expt_amt",
"sbpcode",
"sbnum",
"sbdt"
})
public class Inv {

@JsonProperty("itms")
private List<Itm> itms = null;
@JsonProperty("val")
private Integer val;
@JsonProperty("inv_typ")
private String inv_typ;
@JsonProperty("pos")
private String pos;
@JsonProperty("idt")
private String idt;
@JsonProperty("rchrg")
private String rchrg;
@JsonProperty("inum")
private String inum;
@JsonProperty("chksum")
private String chksum;
@JsonProperty("sply_ty")
private String sply_ty;
@JsonProperty("nil_amt")
private Integer nil_amt;
@JsonProperty("ngsup_amt")
private Integer ngsup_amt;
@JsonProperty("expt_amt")
private Integer expt_amt;
@JsonIgnore
private int gst_category_id;
@JsonProperty("sbpcode")
private String sbpcode;
@JsonProperty("sbnum")
private String sbnum;
@JsonProperty("sbdt")
private String sbdt;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("itms")
public List<Itm> getItms() {
return itms;
}

@JsonProperty("itms")
public void setItms(List<Itm> itms) {
this.itms = itms;
}

@JsonProperty("val")
public Integer getVal() {
return val;
}

@JsonProperty("val")
public void setVal(Integer val) {
this.val = val;
}

@JsonProperty("inv_typ")
public String getInv_typ() {
return inv_typ;
}

@JsonProperty("inv_typ")
public void setInvTyp(String inv_typ) {
this.inv_typ = inv_typ;
}

@JsonProperty("pos")
public String getPos() {
return pos;
}

@JsonProperty("pos")
public void setPos(String pos) {
this.pos = pos;
}

@JsonProperty("idt")
public String getIdt() {
return idt;
}

@JsonProperty("idt")
public void setIdt(String idt) {
this.idt = idt;
}

@JsonProperty("rchrg")
public String getRchrg() {
return rchrg;
}

@JsonProperty("rchrg")
public void setRchrg(String rchrg) {
this.rchrg = rchrg;
}

@JsonProperty("inum")
public String getInum() {
return inum;
}

@JsonProperty("inum")
public void setInum(String inum) {
this.inum = inum;
}

@JsonProperty("chksum")
public String getChksum() {
return chksum;
}

@JsonProperty("chksum")
public void setChksum(String chksum) {
this.chksum = chksum;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

@JsonProperty("sply_ty")
public String getSply_ty() {
	return sply_ty;
}
@JsonProperty("sply_ty")
public void setSply_ty(String sply_ty) {
	this.sply_ty = sply_ty;
}


public Integer getNil_amt() {
	return nil_amt;
}

public void setNil_amt(Integer nil_amt) {
	this.nil_amt = nil_amt;
}


public Integer getNgsup_amt() {
	return ngsup_amt;
}

public void setNgsup_amt(Integer ngsup_amt) {
	this.ngsup_amt = ngsup_amt;
}


public Integer getExpt_amt() {
	return expt_amt;
}

public void setExpt_amt(Integer expt_amt) {
	this.expt_amt = expt_amt;
}

public int getGst_category_id(){
	return gst_category_id;
}

public void setGst_category_id(int gst_category_id){
	this.gst_category_id=gst_category_id;
}

@JsonProperty("sbpcode")
public String getSbpcode() {
return sbpcode;
}

@JsonProperty("sbpcode")
public void setSbpcode(String sbpcode) {
this.sbpcode = sbpcode;
}

@JsonProperty("sbnum")
public String getSbnum() {
return sbnum;
}

@JsonProperty("sbnum")
public void setSbnum(String sbnum) {
this.sbnum = sbnum;
}

@JsonProperty("sbdt")
public String getSbdt() {
return sbdt;
}

@JsonProperty("sbdt")
public void setSbdt(String sbdt) {
this.sbdt = sbdt;
}


}
