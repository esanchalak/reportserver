package com.emanager.server.taxation.dataAccessObject.gst;

public class GstStateDetails {
	
	private String gstStateID;
	private String gstStateName;
	private String description;
	/**
	 * @return the gstStateID
	 */
	public String getGstStateID() {
		return gstStateID;
	}
	/**
	 * @param gstStateID the gstStateID to set
	 */
	public void setGstStateID(String gstStateID) {
		this.gstStateID = gstStateID;
	}
	/**
	 * @return the gstStateName
	 */
	public String getGstStateName() {
		return gstStateName;
	}
	/**
	 * @param gstStateName the gstStateName to set
	 */
	public void setGstStateName(String gstStateName) {
		this.gstStateName = gstStateName;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
