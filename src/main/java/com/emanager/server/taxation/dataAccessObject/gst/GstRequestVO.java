
package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.emanager.server.taxation.dataAccessObject.Error;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({
"b2b"
})
public class GstRequestVO {

   private int orgID;
   private String fromDate;
   private String toDate;
   private List<String> gstReports;
   private String aspID; // ASP id given by GSP API provider
   private String password; // password of ASP id given by GSP API provider
   private String gstIN;
   private String userName; // GST Portal user name
   private int statusCode;
   private String message; // API status message
   private String otp;
   private String status_cd;
   private String auth_token;
   private String expiry;
   private String sek;
   private String ret_period;
   private String reference_id;
   private String gstReport;
   private com.emanager.server.taxation.dataAccessObject.Error error;
   private GstINInfo gstInInfo;
   private GstrFileHistory gstrFileHistory;
   private String financialYear;
   @JsonProperty("b2b")
   private List<B2b> b2b = null;
   @JsonIgnore
   private Map<String, Object> additionalProperties = new HashMap<String, Object>();
   
public String getSek() {
	return sek;
}
public void setSek(String sek) {
	this.sek = sek;
}
/**
 * @return the orgID
 */
public int getOrgID() {
	return orgID;
}
/**
 * @return the fromDate
 */
public String getFromDate() {
	return fromDate;
}
/**
 * @return the toDate
 */
public String getToDate() {
	return toDate;
}
/**
 * @return the gstReports
 */
public List<String> getGstReports() {
	return gstReports;
}
/**
 * @param orgID the orgID to set
 */
public void setOrgID(int orgID) {
	this.orgID = orgID;
}
/**
 * @param fromDate the fromDate to set
 */
public void setFromDate(String fromDate) {
	this.fromDate = fromDate;
}
/**
 * @param toDate the toDate to set
 */
public void setToDate(String toDate) {
	this.toDate = toDate;
}
/**
 * @param gstReports the gstReports to set
 */
public void setGstReports(List<String> gstReports) {
	this.gstReports = gstReports;
}
/**
 * @return the aspID
 */
public String getAspID() {
	return aspID;
}
/**
 * @return the password
 */
public String getPassword() {
	return password;
}
/**
 * @return the gstIN
 */
public String getGstIN() {
	return gstIN;
}
/**
 * @return the userName
 */
public String getUserName() {
	return userName;
}
/**
 * @return the statusCode
 */
public int getStatusCode() {
	return statusCode;
}
/**
 * @return the message
 */
public String getMessage() {
	return message;
}
/**
 * @param aspID the aspID to set
 */
public void setAspID(String aspID) {
	this.aspID = aspID;
}
/**
 * @param password the password to set
 */
public void setPassword(String password) {
	this.password = password;
}
/**
 * @param gstIN the gstIN to set
 */
public void setGstIN(String gstIN) {
	this.gstIN = gstIN;
}
/**
 * @param userName the userName to set
 */
public void setUserName(String userName) {
	this.userName = userName;
}
/**
 * @param statusCode the statusCode to set
 */
public void setStatusCode(int statusCode) {
	this.statusCode = statusCode;
}
/**
 * @param message the message to set
 */
public void setMessage(String message) {
	this.message = message;
}

/**
 * @return the status_cd
 */
public String getStatus_cd() {
	return status_cd;
}
/**
 * @param status_cd the status_cd to set
 */
public void setStatus_cd(String status_cd) {
	this.status_cd = status_cd;
}
/**
 * @return the auth_token
 */
public String getAuth_token() {
	return auth_token;
}
/**
 * @return the expiry
 */
public String getExpiry() {
	return expiry;
}
/**
 * @param auth_token the auth_token to set
 */
public void setAuth_token(String auth_token) {
	this.auth_token = auth_token;
}
/**
 * @param expiry the expiry to set
 */
public void setExpiry(String expiry) {
	this.expiry = expiry;
}
public String getOtp() {
	return otp;
}
public void setOtp(String otp) {
	this.otp = otp;
}
public String getRet_period() {
	return ret_period;
}
public void setRet_period(String ret_period) {
	this.ret_period = ret_period;
}
public List<B2b> getB2b() {
	return b2b;
}
public void setB2b(List<B2b> b2b) {
	this.b2b = b2b;
}
public Map<String, Object> getAdditionalProperties() {
	return additionalProperties;
}
public void setAdditionalProperties(Map<String, Object> additionalProperties) {
	this.additionalProperties = additionalProperties;
}
public String getReference_id() {
	return reference_id;
}
public void setReference_id(String reference_id) {
	this.reference_id = reference_id;
}
public String getGstReport() {
	return gstReport;
}
public void setGstReport(String gstReport) {
	this.gstReport = gstReport;
}
public com.emanager.server.taxation.dataAccessObject.Error getError() {
	return error;
}
public void setError(Error error) {
	this.error = error;
}
public GstINInfo getGstInInfo() {
	return gstInInfo;
}
public void setGstInInfo(GstINInfo gstInInfo) {
	this.gstInInfo = gstInInfo;
}
public String getFinancialYear() {
	return financialYear;
}
public void setFinancialYear(String financialYear) {
	this.financialYear = financialYear;
}
public GstrFileHistory getGstrFileHistory() {
	return gstrFileHistory;
}
public void setGstrFileHistory(GstrFileHistory gstrFileHistory) {
	this.gstrFileHistory = gstrFileHistory;
}

}
