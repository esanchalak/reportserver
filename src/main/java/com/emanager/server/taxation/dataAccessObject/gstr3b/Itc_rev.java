package com.emanager.server.taxation.dataAccessObject.gstr3b;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"ty",
"iamt",
"camt",
"samt",
"csamt"
})
public class Itc_rev {

@JsonProperty("ty")
private String ty;
@JsonProperty("iamt")
private Integer iamt;
@JsonProperty("camt")
private Integer camt;
@JsonProperty("samt")
private Integer samt;
@JsonProperty("csamt")
private Integer csamt;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("ty")
public String getTy() {
return ty;
}

@JsonProperty("ty")
public void setTy(String ty) {
this.ty = ty;
}

@JsonProperty("iamt")
public Integer getIamt() {
return iamt;
}

@JsonProperty("iamt")
public void setIamt(Integer iamt) {
this.iamt = iamt;
}

@JsonProperty("camt")
public Integer getCamt() {
return camt;
}

@JsonProperty("camt")
public void setCamt(Integer camt) {
this.camt = camt;
}

@JsonProperty("samt")
public Integer getSamt() {
return samt;
}

@JsonProperty("samt")
public void setSamt(Integer samt) {
this.samt = samt;
}

@JsonProperty("csamt")
public Integer getCsamt() {
return csamt;
}

@JsonProperty("csamt")
public void setCsamt(Integer csamt) {
this.csamt = csamt;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
