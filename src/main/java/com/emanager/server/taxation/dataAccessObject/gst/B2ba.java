
package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonInclude(Include.NON_NULL)
public class B2ba {

    private String ctin;
    private List<Inv> inv = null;
    
    public String getCtin() {
        return ctin;
    }

    public void setCtin(String ctin) {
        this.ctin = ctin;
    }

    public List<Inv> getInv() {
        return inv;
    }

    public void setInv(List<Inv> inv) {
        this.inv = inv;
    }

    

}
