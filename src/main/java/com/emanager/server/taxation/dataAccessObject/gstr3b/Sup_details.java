package com.emanager.server.taxation.dataAccessObject.gstr3b;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"osup_det",
"osup_zero",
"osup_nil_exmp",
"isup_rev",
"osup_nongst"
})
public class Sup_details {

@JsonProperty("osup_det")
private Osup_det osup_det;
@JsonProperty("osup_zero")
private Osup_zero osup_zero;
@JsonProperty("osup_nil_exmp")
private Osup_nil_exmp osup_nil_exmp;
@JsonProperty("isup_rev")
private Isup_rev isup_rev;
@JsonProperty("osup_nongst")
private Osup_nongst osup_nongst;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("osup_det")
public Osup_det getOsup_det() {
return osup_det;
}

@JsonProperty("osup_det")
public void setOsup_det(Osup_det osupDet) {
this.osup_det = osupDet;
}

@JsonProperty("osup_zero")
public Osup_zero getOsup_zero() {
return osup_zero;
}

@JsonProperty("osup_zero")
public void setOsup_zero(Osup_zero osupZero) {
this.osup_zero = osupZero;
}

@JsonProperty("osup_nil_exmp")
public Osup_nil_exmp getOsup_nil_exmp() {
return osup_nil_exmp;
}

@JsonProperty("osup_nil_exmp")
public void setOsup_nil_exmp(Osup_nil_exmp osupNilExmp) {
this.osup_nil_exmp = osupNilExmp;
}

@JsonProperty("isup_rev")
public Isup_rev getIsup_rev() {
return isup_rev;
}

@JsonProperty("isup_rev")
public void setIsup_rev(Isup_rev isupRev) {
this.isup_rev = isupRev;
}

@JsonProperty("osup_nongst")
public Osup_nongst getOsup_nongst() {
return osup_nongst;
}

@JsonProperty("osup_nongst")
public void setOsup_nongst(Osup_nongst osupNongst) {
this.osup_nongst = osupNongst;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}