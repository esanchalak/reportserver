
package com.emanager.server.taxation.dataAccessObject.gst;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Nt {

    private String ont_num;
    private String ont_dt;
    private String nt_num;
    private String nt_dt;
    private String inum;
    private String ntty;
    private String rsn;
    private String idt;
    private Integer val;
    private String p_gst;
    private List<Itm> itms = null;
    private BigDecimal diff_percent;
    
    public String getInum() {
        return inum;
    }

    public void setInum(String inum) {
        this.inum = inum;
    }

    public String getNtty() {
        return ntty;
    }

    public void setNtty(String ntty) {
        this.ntty = ntty;
    }

    public String getRsn() {
        return rsn;
    }

    public void setRsn(String rsn) {
        this.rsn = rsn;
    }

    public String getIdt() {
        return idt;
    }

    public void setIdt(String idt) {
        this.idt = idt;
    }

    public Integer getVal() {
        return val;
    }

    public void setVal(Integer val) {
        this.val = val;
    }


    public List<Itm> getItms() {
        return itms;
    }

    public void setItms(List<Itm> itms) {
        this.itms = itms;
    }

    /**
	 * @return the ont_num
	 */
	public String getOnt_num() {
		return ont_num;
	}

	/**
	 * @return the ont_dt
	 */
	public String getOnt_dt() {
		return ont_dt;
	}

	/**
	 * @return the nt_num
	 */
	public String getNt_num() {
		return nt_num;
	}

	/**
	 * @return the nt_dt
	 */
	public String getNt_dt() {
		return nt_dt;
	}

	/**
	 * @return the p_gst
	 */
	public String getP_gst() {
		return p_gst;
	}

	/**
	 * @param ont_num the ont_num to set
	 */
	public void setOnt_num(String ont_num) {
		this.ont_num = ont_num;
	}

	/**
	 * @param ont_dt the ont_dt to set
	 */
	public void setOnt_dt(String ont_dt) {
		this.ont_dt = ont_dt;
	}

	/**
	 * @param nt_num the nt_num to set
	 */
	public void setNt_num(String nt_num) {
		this.nt_num = nt_num;
	}

	/**
	 * @param nt_dt the nt_dt to set
	 */
	public void setNt_dt(String nt_dt) {
		this.nt_dt = nt_dt;
	}

	/**
	 * @param p_gst the p_gst to set
	 */
	public void setP_gst(String p_gst) {
		this.p_gst = p_gst;
	}

	/**
	 * @return the diff_percent
	 */
	public BigDecimal getDiff_percent() {
		return diff_percent;
	}

	/**
	 * @param diff_percent the diff_percent to set
	 */
	public void setDiff_percent(BigDecimal diff_percent) {
		this.diff_percent = diff_percent;
	}

}
