
package com.emanager.server.taxation.dataAccessObject.gst;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"num",
"itm_det",
"csamt",
"samt",
"rt",
"txval",
"camt",
"iamt"
})
public class Itm {

@JsonProperty("num")
private Integer num;
@JsonProperty("itm_det")
private ItmDet itm_det;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("num")
public Integer getNum() {
return num;
}

@JsonProperty("num")
public void setNum(Integer num) {
this.num = num;
}

@JsonProperty("itm_det")
public ItmDet getItm_det() {
	return itm_det;
}

@JsonProperty("itm_det")
public void setItm_det(ItmDet itm_det) {
	this.itm_det = itm_det;
}


@JsonProperty("csamt")
private BigDecimal csamt;
@JsonProperty("samt")
private BigDecimal samt;
@JsonProperty("rt")
private BigDecimal rt;
@JsonProperty("txval")
private BigDecimal txval;
@JsonProperty("camt")
private BigDecimal camt;
@JsonProperty("iamt")
private BigDecimal iamt;

@JsonProperty("csamt")
public BigDecimal getCsamt() {
return csamt;
}

@JsonProperty("csamt")
public void setCsamt(BigDecimal csamt) {
this.csamt = csamt;
}

@JsonProperty("samt")
public BigDecimal getSamt() {
return samt;
}

@JsonProperty("samt")
public void setSamt(BigDecimal samt) {
this.samt = samt;
}

@JsonProperty("rt")
public BigDecimal getRt() {
return rt;
}

@JsonProperty("rt")
public void setRt(BigDecimal rt) {
this.rt = rt;
}

@JsonProperty("txval")
public BigDecimal getTxval() {
return txval;
}

@JsonProperty("txval")
public void setTxval(BigDecimal txval) {
this.txval = txval;
}

@JsonProperty("camt")
public BigDecimal getCamt() {
return camt;
}

@JsonProperty("camt")
public void setCamt(BigDecimal camt) {
this.camt = camt;
}

@JsonProperty("iamt")
public BigDecimal getIamt() {
return iamt;
}

@JsonProperty("iamt")
public void setIamt(BigDecimal iamt) {
this.iamt = iamt;
}


@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}