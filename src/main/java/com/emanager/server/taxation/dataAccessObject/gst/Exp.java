
package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Exp {

    private String exp_typ;
    private List<Inv> inv = null;
    

    public List<Inv> getInv() {
        return inv;
    }

    public void setInv(List<Inv> inv) {
        this.inv = inv;
    }

    /**
	 * @return the exp_typ
	 */
	public String getExp_typ() {
		return exp_typ;
	}

	/**
	 * @param exp_typ the exp_typ to set
	 */
	public void setExp_typ(String exp_typ) {
		this.exp_typ = exp_typ;
	}

}
