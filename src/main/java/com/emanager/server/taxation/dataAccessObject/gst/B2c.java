
package com.emanager.server.taxation.dataAccessObject.gst;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonInclude(Include.NON_NULL)
public class B2c {

    private String sply_ty;
    private Integer rt;
    private String typ;
    private String pos;
    private Double diff_percent;
    private Double txval;
    private Double iamt;
    private Double camt;
    private Double samt;
    private Integer csamt;
    private Integer gst_category_id;
    
      

    public Integer getRt() {
        return rt;
    }

    public void setRt(Integer rt) {
        this.rt = rt;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public Double getTxval() {
        return txval;
    }

    public void setTxval(Double txval) {
        this.txval = txval;
    }

    public Double getIamt() {
        return iamt;
    }

    public void setIamt(Double iamt) {
        this.iamt = iamt;
    }

    public Integer getCsamt() {
        return csamt;
    }

    public void setCsamt(Integer csamt) {
        this.csamt = csamt;
    }

    /**
	 * @return the sply_ty
	 */
	public String getSply_ty() {
		return sply_ty;
	}

	/**
	 * @return the diff_percent
	 */
	public Double getDiff_percent() {
		return diff_percent;
	}

	/**
	 * @param sply_ty the sply_ty to set
	 */
	public void setSply_ty(String sply_ty) {
		this.sply_ty = sply_ty;
	}

	/**
	 * @param diff_percent the diff_percent to set
	 */
	public void setDiff_percent(Double diff_percent) {
		this.diff_percent = diff_percent;
	}

	/**
	 * @return the camt
	 */
	public Double getCamt() {
		return camt;
	}

	/**
	 * @return the samt
	 */
	public Double getSamt() {
		return samt;
	}

	/**
	 * @param camt the camt to set
	 */
	public void setCamt(Double camt) {
		this.camt = camt;
	}

	/**
	 * @param samt the samt to set
	 */
	public void setSamt(Double samt) {
		this.samt = samt;
	}

	public Integer getGst_category_id() {
		return gst_category_id;
	}

	public void setGst_category_id(Integer gst_category_id) {
		this.gst_category_id = gst_category_id;
	}

}
