
package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class DocIssue {

    private List<DocDet> doc_det = null;
    
   	/**
	 * @return the doc_det
	 */
	public List<DocDet> getDoc_det() {
		return doc_det;
	}

	/**
	 * @param doc_det the doc_det to set
	 */
	public void setDoc_det(List<DocDet> doc_det) {
		this.doc_det = doc_det;
	}

}
