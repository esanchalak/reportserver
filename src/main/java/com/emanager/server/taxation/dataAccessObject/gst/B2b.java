
package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"ctin",	
"inv",
"cfs"
})
public class B2b {

@JsonProperty("inv")
private List<Inv> inv = null;
@JsonProperty("cfs")
private String cfs;
@JsonProperty("ctin")
private String ctin;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("inv")
public List<Inv> getInv() {
return inv;
}

@JsonProperty("inv")
public void setInv(List<Inv> inv) {
this.inv = inv;
}

@JsonProperty("cfs")
public String getCfs() {
return cfs;
}

@JsonProperty("cfs")
public void setCfs(String cfs) {
this.cfs = cfs;
}

@JsonProperty("ctin")
public String getCtin() {
return ctin;
}

@JsonProperty("ctin")
public void setCtin(String ctin) {
this.ctin = ctin;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
