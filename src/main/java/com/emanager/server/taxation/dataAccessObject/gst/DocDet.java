
package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class DocDet {

    private Integer doc_num;
    private String doc_typ;
    private List<Doc> docs = null;
    
    public List<Doc> getDocs() {
        return docs;
    }

    public void setDocs(List<Doc> docs) {
        this.docs = docs;
    }

    /**
	 * @return the doc_num
	 */
	public Integer getDoc_num() {
		return doc_num;
	}

	/**
	 * @return the doc_typ
	 */
	public String getDoc_typ() {
		return doc_typ;
	}

	/**
	 * @param doc_num the doc_num to set
	 */
	public void setDoc_num(Integer doc_num) {
		this.doc_num = doc_num;
	}

	/**
	 * @param doc_typ the doc_typ to set
	 */
	public void setDoc_typ(String doc_typ) {
		this.doc_typ = doc_typ;
	}

}
