package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"bnm",
"st",
"loc",
"bno",
"stcd",
"dst",
"city",
"flno",
"lt",
"pncd",
"lg"
})
public class Addr {

	@JsonProperty("bnm")
	private String bnm;
	@JsonProperty("st")
	private String st;
	@JsonProperty("loc")
	private String loc;
	@JsonProperty("bno")
	private String bno;
	@JsonProperty("stcd")
	private String stcd;
	@JsonProperty("dst")
	private String dst;
	@JsonProperty("city")
	private String city;
	@JsonProperty("flno")
	private String flno;
	@JsonProperty("lt")
	private String lt;
	@JsonProperty("pncd")
	private String pncd;
	@JsonProperty("lg")
	private String lg;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("bnm")
	public String getBnm() {
	return bnm;
	}

	@JsonProperty("bnm")
	public void setBnm(String bnm) {
	this.bnm = bnm;
	}

	@JsonProperty("st")
	public String getSt() {
	return st;
	}

	@JsonProperty("st")
	public void setSt(String st) {
	this.st = st;
	}

	@JsonProperty("loc")
	public String getLoc() {
	return loc;
	}

	@JsonProperty("loc")
	public void setLoc(String loc) {
	this.loc = loc;
	}

	@JsonProperty("bno")
	public String getBno() {
	return bno;
	}

	@JsonProperty("bno")
	public void setBno(String bno) {
	this.bno = bno;
	}

	@JsonProperty("stcd")
	public String getStcd() {
	return stcd;
	}

	@JsonProperty("stcd")
	public void setStcd(String stcd) {
	this.stcd = stcd;
	}

	@JsonProperty("dst")
	public String getDst() {
	return dst;
	}

	@JsonProperty("dst")
	public void setDst(String dst) {
	this.dst = dst;
	}

	@JsonProperty("city")
	public String getCity() {
	return city;
	}

	@JsonProperty("city")
	public void setCity(String city) {
	this.city = city;
	}

	@JsonProperty("flno")
	public String getFlno() {
	return flno;
	}

	@JsonProperty("flno")
	public void setFlno(String flno) {
	this.flno = flno;
	}

	@JsonProperty("lt")
	public String getLt() {
	return lt;
	}

	@JsonProperty("lt")
	public void setLt(String lt) {
	this.lt = lt;
	}

	@JsonProperty("pncd")
	public String getPncd() {
	return pncd;
	}

	@JsonProperty("pncd")
	public void setPncd(String pncd) {
	this.pncd = pncd;
	}

	@JsonProperty("lg")
	public String getLg() {
	return lg;
	}

	@JsonProperty("lg")
	public void setLg(String lg) {
	this.lg = lg;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}
	
}
