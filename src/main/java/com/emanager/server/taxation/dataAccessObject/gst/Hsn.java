
package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"data"
})
public class Hsn {
	 @JsonProperty("data")
    private List<Datum> data = null;
	 @JsonProperty("data")
    public List<Datum> getData() {
        return data;
    }
	 @JsonProperty("data")
    public void setData(List<Datum> data) {
        this.data = data;
    }
    
}
