
package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Nt_ {

	 private String nt_num;
	 private String nt_dt;
	 private String inum;
	 private String ntty;
	 private String rsn;
	 private String idt;
	 private Integer val;
	 private String rchrg;
	 private String inv_typ;
	 private String pos;
	 
	 private List<Itm> itms = null;

	public String getNt_num() {
		return nt_num;
	}

	public void setNt_num(String nt_num) {
		this.nt_num = nt_num;
	}

	public String getNt_dt() {
		return nt_dt;
	}

	public void setNt_dt(String nt_dt) {
		this.nt_dt = nt_dt;
	}

	public String getInum() {
		return inum;
	}

	public void setInum(String inum) {
		this.inum = inum;
	}

	public String getNtty() {
		return ntty;
	}

	public void setNtty(String ntty) {
		this.ntty = ntty;
	}

	public String getRsn() {
		return rsn;
	}

	public void setRsn(String rsn) {
		this.rsn = rsn;
	}

	public String getIdt() {
		return idt;
	}

	public void setIdt(String idt) {
		this.idt = idt;
	}

	public Integer getVal() {
		return val;
	}

	public void setVal(Integer val) {
		this.val = val;
	}

	public String getRchrg() {
		return rchrg;
	}

	public void setRchrg(String rchrg) {
		this.rchrg = rchrg;
	}

	public String getInv_typ() {
		return inv_typ;
	}

	public void setInv_typ(String inv_typ) {
		this.inv_typ = inv_typ;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public List<Itm> getItms() {
		return itms;
	}

	public void setItms(List<Itm> itms) {
		this.itms = itms;
	}
    
     
}
