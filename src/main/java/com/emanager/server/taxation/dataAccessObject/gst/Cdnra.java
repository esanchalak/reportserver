
package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Cdnra {

    private String ctin;
    private List<Nt> nt = null;
    
    public String getCtin() {
        return ctin;
    }

    public void setCtin(String ctin) {
        this.ctin = ctin;
    }

    public List<Nt> getNt() {
        return nt;
    }

    public void setNt(List<Nt> nt) {
        this.nt = nt;
    }

    
}
