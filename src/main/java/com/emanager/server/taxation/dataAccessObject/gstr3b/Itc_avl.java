package com.emanager.server.taxation.dataAccessObject.gstr3b;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"ty",
"iamt",
"camt",
"samt",
"csamt"
})
public class Itc_avl {

@JsonProperty("ty")
private String ty;
@JsonProperty("iamt")
private BigDecimal iamt;
@JsonProperty("camt")
private BigDecimal camt;
@JsonProperty("samt")
private BigDecimal samt;
@JsonProperty("csamt")
private BigDecimal csamt;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("ty")
public String getTy() {
return ty;
}

@JsonProperty("ty")
public void setTy(String ty) {
this.ty = ty;
}

@JsonProperty("iamt")
public BigDecimal getIamt() {
return iamt;
}

@JsonProperty("iamt")
public void setIamt(BigDecimal iamt) {
this.iamt = iamt;
}

@JsonProperty("camt")
public BigDecimal getCamt() {
return camt;
}

@JsonProperty("camt")
public void setCamt(BigDecimal camt) {
this.camt = camt;
}

@JsonProperty("samt")
public BigDecimal getSamt() {
return samt;
}

@JsonProperty("samt")
public void setSamt(BigDecimal samt) {
this.samt = samt;
}

@JsonProperty("csamt")
public BigDecimal getCsamt() {
return csamt;
}

@JsonProperty("csamt")
public void setCsamt(BigDecimal csamt) {
this.csamt = csamt;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
