package com.emanager.server.taxation.dataAccessObject.gstr3b;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"unreg_details",
"comp_details",
"uin_details"
})
public class Inter_sup {

@JsonProperty("unreg_details")
private List<Unreg_details> unregDetails = null;
@JsonProperty("comp_details")
private List<Comp_details> compDetails = null;
@JsonProperty("uin_details")
private List<Uin_details> uinDetails = null;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("unreg_details")
public List<Unreg_details> getUnregDetails() {
return unregDetails;
}

@JsonProperty("unreg_details")
public void setUnregDetails(List<Unreg_details> unregDetails) {
this.unregDetails = unregDetails;
}

@JsonProperty("comp_details")
public List<Comp_details> getCompDetails() {
return compDetails;
}

@JsonProperty("comp_details")
public void setCompDetails(List<Comp_details> compDetails) {
this.compDetails = compDetails;
}

@JsonProperty("uin_details")
public List<Uin_details> getUinDetails() {
return uinDetails;
}

@JsonProperty("uin_details")
public void setUinDetails(List<Uin_details> uinDetails) {
this.uinDetails = uinDetails;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}