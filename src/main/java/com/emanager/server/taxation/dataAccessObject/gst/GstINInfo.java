package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"stjCd",
"lgnm",
"dty",
"adadr",
"cxdt",
"gstin",
"nba",
"lstupdt",
"rgdt",
"ctb",
"pradr",
"sts",
"tradeNam",
"ctjCd",
"ctj"
})

public class GstINInfo {

	@JsonProperty("stjCd")
	private String stjCd;
	@JsonProperty("lgnm")
	private String lgnm;
	@JsonProperty("dty")
	private String dty;
	@JsonProperty("adadr")
	private List<Object> adadr = null;
	@JsonProperty("cxdt")
	private String cxdt;
	@JsonProperty("gstin")
	private String gstin;
	@JsonProperty("nba")
	private List<String> nba = null;
	@JsonProperty("lstupdt")
	private String lstupdt;
	@JsonProperty("rgdt")
	private String rgdt;
	@JsonProperty("ctb")
	private String ctb;
	@JsonProperty("pradr")
	private Pradr pradr;
	@JsonProperty("sts")
	private String sts;
	@JsonProperty("tradeNam")
	private String tradeNam;
	@JsonProperty("ctjCd")
	private String ctjCd;
	@JsonProperty("ctj")
	private String ctj;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("stjCd")
	public String getStjCd() {
	return stjCd;
	}

	@JsonProperty("stjCd")
	public void setStjCd(String stjCd) {
	this.stjCd = stjCd;
	}

	@JsonProperty("lgnm")
	public String getLgnm() {
	return lgnm;
	}

	@JsonProperty("lgnm")
	public void setLgnm(String lgnm) {
	this.lgnm = lgnm;
	}

	@JsonProperty("dty")
	public String getDty() {
	return dty;
	}

	@JsonProperty("dty")
	public void setDty(String dty) {
	this.dty = dty;
	}

	@JsonProperty("adadr")
	public List<Object> getAdadr() {
	return adadr;
	}

	@JsonProperty("adadr")
	public void setAdadr(List<Object> adadr) {
	this.adadr = adadr;
	}

	@JsonProperty("cxdt")
	public String getCxdt() {
	return cxdt;
	}

	@JsonProperty("cxdt")
	public void setCxdt(String cxdt) {
	this.cxdt = cxdt;
	}

	@JsonProperty("gstin")
	public String getGstin() {
	return gstin;
	}

	@JsonProperty("gstin")
	public void setGstin(String gstin) {
	this.gstin = gstin;
	}

	@JsonProperty("nba")
	public List<String> getNba() {
	return nba;
	}

	@JsonProperty("nba")
	public void setNba(List<String> nba) {
	this.nba = nba;
	}

	@JsonProperty("lstupdt")
	public String getLstupdt() {
	return lstupdt;
	}

	@JsonProperty("lstupdt")
	public void setLstupdt(String lstupdt) {
	this.lstupdt = lstupdt;
	}

	@JsonProperty("rgdt")
	public String getRgdt() {
	return rgdt;
	}

	@JsonProperty("rgdt")
	public void setRgdt(String rgdt) {
	this.rgdt = rgdt;
	}

	@JsonProperty("ctb")
	public String getCtb() {
	return ctb;
	}

	@JsonProperty("ctb")
	public void setCtb(String ctb) {
	this.ctb = ctb;
	}

	@JsonProperty("pradr")
	public Pradr getPradr() {
	return pradr;
	}

	@JsonProperty("pradr")
	public void setPradr(Pradr pradr) {
	this.pradr = pradr;
	}

	@JsonProperty("sts")
	public String getSts() {
	return sts;
	}

	@JsonProperty("sts")
	public void setSts(String sts) {
	this.sts = sts;
	}

	@JsonProperty("tradeNam")
	public String getTradeNam() {
	return tradeNam;
	}

	@JsonProperty("tradeNam")
	public void setTradeNam(String tradeNam) {
	this.tradeNam = tradeNam;
	}

	@JsonProperty("ctjCd")
	public String getCtjCd() {
	return ctjCd;
	}

	@JsonProperty("ctjCd")
	public void setCtjCd(String ctjCd) {
	this.ctjCd = ctjCd;
	}

	@JsonProperty("ctj")
	public String getCtj() {
	return ctj;
	}

	@JsonProperty("ctj")
	public void setCtj(String ctj) {
	this.ctj = ctj;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}
	
	
}
