
package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Doc {

    private Integer num;
    private String from;
    private String to;
    private Integer totnum;
    private Integer cancel;
    private Integer net_issue;
    
    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Integer getTotnum() {
        return totnum;
    }

    public void setTotnum(Integer totnum) {
        this.totnum = totnum;
    }

    public Integer getCancel() {
        return cancel;
    }

    public void setCancel(Integer cancel) {
        this.cancel = cancel;
    }

   	/**
	 * @return the net_issue
	 */
	public Integer getNet_issue() {
		return net_issue;
	}

	/**
	 * @param net_issue the net_issue to set
	 */
	public void setNet_issue(Integer net_issue) {
		this.net_issue = net_issue;
	}

}
