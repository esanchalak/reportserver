package com.emanager.server.taxation.dataAccessObject;

import java.math.BigDecimal;


public class TDSDetailsVO {
	
	private int societyID;
	private int vendorID;
	private int ledgerID;
	private int tdsSectionID;
	private int vendorLedgerID;
	private int vendorServiceLedgerID;
	private String tdsSectionNo;
	private String tdsSectionName;
	private String tdsSectionDesctription;
	private BigDecimal tdsCutOffAmount;
	private BigDecimal tdsRateForIndiv;
	private BigDecimal tdsRateForOther;
	private String vendorName;
	private int deducteeCode;
	private int serviceLedgerID;
	private String serviceLedgerName;
	private String particulars;
	private String authorizedPersonName;
	private String vendorPANNo;
	private String vendorPANName;
	private String vendorTANNo;
	private String gstINNo;
	private BigDecimal netBillAmount;
	private BigDecimal taxAmount;
	private BigDecimal grossTotal;
	private BigDecimal tdsAmount;
	private BigDecimal netPayableAmount;
	private BigDecimal paidAmount;
	private BigDecimal tdsRate;
	private String tdsSection;
	private String invoiceDate;
	private String invoiceNo;
	
	
	public int getLedgerID() {
		return ledgerID;
	}
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	public BigDecimal getTdsAmount() {
		return tdsAmount;
	}
	public void setTdsAmount(BigDecimal tdsAmount) {
		this.tdsAmount = tdsAmount;
	}
	public BigDecimal getTdsCutOffAmount() {
		return tdsCutOffAmount;
	}
	public void setTdsCutOffAmount(BigDecimal tdsCutOffAmount) {
		this.tdsCutOffAmount = tdsCutOffAmount;
	}
	public BigDecimal getTdsRateForIndiv() {
		return tdsRateForIndiv;
	}
	public void setTdsRateForIndiv(BigDecimal tdsRateForIndiv) {
		this.tdsRateForIndiv = tdsRateForIndiv;
	}
	public BigDecimal getTdsRateForOther() {
		return tdsRateForOther;
	}
	public void setTdsRateForOther(BigDecimal tdsRateForOther) {
		this.tdsRateForOther = tdsRateForOther;
	}
	public String getTdsSectionDesctription() {
		return tdsSectionDesctription;
	}
	public void setTdsSectionDesctription(String tdsSectionDesctription) {
		this.tdsSectionDesctription = tdsSectionDesctription;
	}
	public int getTdsSectionID() {
		return tdsSectionID;
	}
	public void setTdsSectionID(int tdsSectionID) {
		this.tdsSectionID = tdsSectionID;
	}
	public String getTdsSectionNo() {
		return tdsSectionNo;
	}
	public void setTdsSectionNo(String tdsSectionNo) {
		this.tdsSectionNo = tdsSectionNo;
	}
	public int getVendorID() {
		return vendorID;
	}
	public void setVendorID(int vendorID) {
		this.vendorID = vendorID;
	}
	public String getTdsSectionName() {
		return tdsSectionName;
	}
	public void setTdsSectionName(String tdsSectionName) {
		this.tdsSectionName = tdsSectionName;
	}
	public int getDeducteeCode() {
		return deducteeCode;
	}
	public void setDeducteeCode(int deducteeCode) {
		this.deducteeCode = deducteeCode;
	}
	public int getServiceLedgerID() {
		return serviceLedgerID;
	}
	public void setServiceLedgerID(int serviceLedgerID) {
		this.serviceLedgerID = serviceLedgerID;
	}
	public String getServiceLedgerName() {
		return serviceLedgerName;
	}
	public void setServiceLedgerName(String serviceLedgerName) {
		this.serviceLedgerName = serviceLedgerName;
	}
	/**
	 * @return the vendorLedgerID
	 */
	public int getVendorLedgerID() {
		return vendorLedgerID;
	}
	/**
	 * @return the vendorServiceLedgerID
	 */
	public int getVendorServiceLedgerID() {
		return vendorServiceLedgerID;
	}
	/**
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}
	/**
	 * @return the particulars
	 */
	public String getParticulars() {
		return particulars;
	}
	/**
	 * @return the authorizedPersonName
	 */
	public String getAuthorizedPersonName() {
		return authorizedPersonName;
	}
	/**
	 * @return the vendorPANNo
	 */
	public String getVendorPANNo() {
		return vendorPANNo;
	}
	/**
	 * @return the vendorTANNo
	 */
	public String getVendorTANNo() {
		return vendorTANNo;
	}
	/**
	 * @return the gstINNo
	 */
	public String getGstINNo() {
		return gstINNo;
	}
	/**
	 * @return the netBillAmount
	 */
	public BigDecimal getNetBillAmount() {
		return netBillAmount;
	}
	/**
	 * @return the taxAmount
	 */
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}
	/**
	 * @return the grossTotal
	 */
	public BigDecimal getGrossTotal() {
		return grossTotal;
	}
	/**
	 * @return the netPayableAmount
	 */
	public BigDecimal getNetPayableAmount() {
		return netPayableAmount;
	}
	/**
	 * @return the paidAmount
	 */
	public BigDecimal getPaidAmount() {
		return paidAmount;
	}
	/**
	 * @return the tdsRate
	 */
	public BigDecimal getTdsRate() {
		return tdsRate;
	}
	/**
	 * @return the tdsSection
	 */
	public String getTdsSection() {
		return tdsSection;
	}
	/**
	 * @return the invoiceDate
	 */
	public String getInvoiceDate() {
		return invoiceDate;
	}
	/**
	 * @return the invoiceNo
	 */
	public String getInvoiceNo() {
		return invoiceNo;
	}
	/**
	 * @param vendorLedgerID the vendorLedgerID to set
	 */
	public void setVendorLedgerID(int vendorLedgerID) {
		this.vendorLedgerID = vendorLedgerID;
	}
	/**
	 * @param vendorServiceLedgerID the vendorServiceLedgerID to set
	 */
	public void setVendorServiceLedgerID(int vendorServiceLedgerID) {
		this.vendorServiceLedgerID = vendorServiceLedgerID;
	}
	/**
	 * @param vendorName the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	/**
	 * @param particulars the particulars to set
	 */
	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}
	/**
	 * @param authorizedPersonName the authorizedPersonName to set
	 */
	public void setAuthorizedPersonName(String authorizedPersonName) {
		this.authorizedPersonName = authorizedPersonName;
	}
	/**
	 * @param vendorPANNo the vendorPANNo to set
	 */
	public void setVendorPANNo(String vendorPANNo) {
		this.vendorPANNo = vendorPANNo;
	}
	/**
	 * @param vendorTANNo the vendorTANNo to set
	 */
	public void setVendorTANNo(String vendorTANNo) {
		this.vendorTANNo = vendorTANNo;
	}
	/**
	 * @param gstINNo the gstINNo to set
	 */
	public void setGstINNo(String gstINNo) {
		this.gstINNo = gstINNo;
	}
	/**
	 * @param netBillAmount the netBillAmount to set
	 */
	public void setNetBillAmount(BigDecimal netBillAmount) {
		this.netBillAmount = netBillAmount;
	}
	/**
	 * @param taxAmount the taxAmount to set
	 */
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}
	/**
	 * @param grossTotal the grossTotal to set
	 */
	public void setGrossTotal(BigDecimal grossTotal) {
		this.grossTotal = grossTotal;
	}
	/**
	 * @param netPayableAmount the netPayableAmount to set
	 */
	public void setNetPayableAmount(BigDecimal netPayableAmount) {
		this.netPayableAmount = netPayableAmount;
	}
	/**
	 * @param paidAmount the paidAmount to set
	 */
	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}
	/**
	 * @param tdsRate the tdsRate to set
	 */
	public void setTdsRate(BigDecimal tdsRate) {
		this.tdsRate = tdsRate;
	}
	/**
	 * @param tdsSection the tdsSection to set
	 */
	public void setTdsSection(String tdsSection) {
		this.tdsSection = tdsSection;
	}
	/**
	 * @param invoiceDate the invoiceDate to set
	 */
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	/**
	 * @param invoiceNo the invoiceNo to set
	 */
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	/**
	 * @return the vendorPANName
	 */
	public String getVendorPANName() {
		return vendorPANName;
	}
	/**
	 * @param vendorPANName the vendorPANName to set
	 */
	public void setVendorPANName(String vendorPANName) {
		this.vendorPANName = vendorPANName;
	}
	
}
