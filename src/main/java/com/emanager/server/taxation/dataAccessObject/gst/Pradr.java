package com.emanager.server.taxation.dataAccessObject.gst;


import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"addr",
"ntr"
})
public class Pradr {

	@JsonProperty("addr")
	private Addr addr;
	@JsonProperty("ntr")
	private String ntr;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("addr")
	public Addr getAddr() {
	return addr;
	}

	@JsonProperty("addr")
	public void setAddr(Addr addr) {
	this.addr = addr;
	}

	@JsonProperty("ntr")
	public String getNtr() {
	return ntr;
	}

	@JsonProperty("ntr")
	public void setNtr(String ntr) {
	this.ntr = ntr;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}
	
}
