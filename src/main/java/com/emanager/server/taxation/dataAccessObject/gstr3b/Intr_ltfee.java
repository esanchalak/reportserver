package com.emanager.server.taxation.dataAccessObject.gstr3b;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"intr_details",
"ltfee_details"
})
public class Intr_ltfee {

@JsonProperty("intr_details")
private Intr_details intrDetails;
@JsonProperty("ltfee_details")
private Ltfee_details ltfeeDetails;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("intr_details")
public Intr_details getIntrDetails() {
return intrDetails;
}

@JsonProperty("intr_details")
public void setIntrDetails(Intr_details intrDetails) {
this.intrDetails = intrDetails;
}

@JsonProperty("ltfee_details")
public Ltfee_details getLtfeeDetails() {
return ltfeeDetails;
}

@JsonProperty("ltfee_details")
public void setLtfeeDetails(Ltfee_details ltfeeDetails) {
this.ltfeeDetails = ltfeeDetails;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}