package com.emanager.server.taxation.dataAccessObject.gstr3b;

import java.math.BigDecimal;

public class Uin_details {

	private String pos;

    private BigDecimal txval;

    private BigDecimal iamt;

	/**
	 * @return the pos
	 */
	public String getPos() {
		return pos;
	}

	/**
	 * @param pos the pos to set
	 */
	public void setPos(String pos) {
		this.pos = pos;
	}

	/**
	 * @return the txval
	 */
	public BigDecimal getTxval() {
		return txval;
	}

	/**
	 * @param txval the txval to set
	 */
	public void setTxval(BigDecimal txval) {
		this.txval = txval;
	}

	/**
	 * @return the iamt
	 */
	public BigDecimal getIamt() {
		return iamt;
	}

	/**
	 * @param iamt the iamt to set
	 */
	public void setIamt(BigDecimal iamt) {
		this.iamt = iamt;
	}
	
}
