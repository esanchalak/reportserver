package com.emanager.server.taxation.dataAccessObject.gstr3b;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"gstin",
"ret_period",
"sup_details",
"itc_elg",
"inward_sup",
"intr_ltfee",
"inter_sup"
})
public class Gstr3B {

@JsonProperty("gstin")
private String gstin;
@JsonProperty("ret_period")
private String ret_period;
@JsonProperty("sup_details")
private Sup_details sup_details;
@JsonProperty("itc_elg")
private Itc_elg itc_elg;
@JsonProperty("inward_sup")
private Inward_sup inward_sup;
@JsonProperty("intr_ltfee")
private Intr_ltfee intr_ltfee;
@JsonProperty("inter_sup")
private Inter_sup inter_sup;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("gstin")
public String getGstin() {
return gstin;
}

@JsonProperty("gstin")
public void setGstin(String gstin) {
this.gstin = gstin;
}

@JsonProperty("ret_period")
public String getRet_period() {
return ret_period;
}

@JsonProperty("ret_period")
public void setRet_period(String retPeriod) {
this.ret_period = retPeriod;
}

@JsonProperty("sup_details")
public Sup_details getSup_details() {
return sup_details;
}

@JsonProperty("sup_details")
public void setSup_details(Sup_details supDetails) {
this.sup_details = supDetails;
}

@JsonProperty("itc_elg")
public Itc_elg getItc_elg() {
return itc_elg;
}

@JsonProperty("itc_elg")
public void setItc_elg(Itc_elg itcElg) {
this.itc_elg = itcElg;
}

@JsonProperty("inward_sup")
public Inward_sup getInward_sup() {
return inward_sup;
}

@JsonProperty("inward_sup")
public void setInward_sup(Inward_sup inwardSup) {
this.inward_sup = inwardSup;
}

@JsonProperty("intr_ltfee")
public Intr_ltfee getIntr_ltfee() {
return intr_ltfee;
}

@JsonProperty("intr_ltfee")
public void setIntr_ltfee(Intr_ltfee intrLtfee) {
this.intr_ltfee = intrLtfee;
}

@JsonProperty("inter_sup")
public Inter_sup getInter_sup() {
return inter_sup;
}

@JsonProperty("inter_sup")
public void setInter_sup(Inter_sup interSup) {
this.inter_sup = interSup;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
	