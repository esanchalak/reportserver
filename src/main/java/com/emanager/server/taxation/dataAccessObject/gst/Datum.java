
package com.emanager.server.taxation.dataAccessObject.gst;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Datum {

    private Integer num;
    private String hsn_sc;
    private String desc;
    private String uqc;
    private Double qty;
    private Double val;
    private Double txval;
    private Double iamt;
    private Double samt;
    private Double camt;
    private Double csamt;
    private Double rt;
    
    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

      public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUqc() {
        return uqc;
    }

    public void setUqc(String uqc) {
        this.uqc = uqc;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public Double getVal() {
        return val;
    }

    public void setVal(Double val) {
        this.val = val;
    }

    public Double getTxval() {
        return txval;
    }

    public void setTxval(Double txval) {
        this.txval = txval;
    }

    public Double getIamt() {
        return iamt;
    }

    public void setIamt(Double iamt) {
        this.iamt = iamt;
    }

    public Double getSamt() {
        return samt;
    }

    public void setSamt(Double samt) {
        this.samt = samt;
    }

    public Double getCamt() {
        return camt;
    }

    public void setCamt(Double camt) {
        this.camt = camt;
    }

    public Double getCsamt() {
        return csamt;
    }

    public void setCsamt(Double csamt) {
        this.csamt = csamt;
    }

    /**
	 * @return the hsn_sc
	 */
	public String getHsn_sc() {
		return hsn_sc;
	}

	/**
	 * @param hsn_sc the hsn_sc to set
	 */
	public void setHsn_sc(String hsn_sc) {
		this.hsn_sc = hsn_sc;
	}

	public Double getRt() {
		return rt;
	}

	public void setRt(Double rt) {
		this.rt = rt;
	}

}
