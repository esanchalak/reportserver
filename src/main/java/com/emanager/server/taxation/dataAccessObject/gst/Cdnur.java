
package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Cdnur {

    private String nt_num;
    private String nt_dt;
    private String inum;
    private String ntty;
    private String rsn;
    private String idt;
    private Integer val;
    private String p_gst;
    private String typ;
    private List<Itm> itms = null;
    

    public String getInum() {
        return inum;
    }

    public void setInum(String inum) {
        this.inum = inum;
    }

    public String getNtty() {
        return ntty;
    }

    public void setNtty(String ntty) {
        this.ntty = ntty;
    }

    public String getRsn() {
        return rsn;
    }

    public void setRsn(String rsn) {
        this.rsn = rsn;
    }

    public String getIdt() {
        return idt;
    }

    public void setIdt(String idt) {
        this.idt = idt;
    }

    public Integer getVal() {
        return val;
    }

    public void setVal(Integer val) {
        this.val = val;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public List<Itm> getItms() {
        return itms;
    }

    public void setItms(List<Itm> itms) {
        this.itms = itms;
    }
    
	/**
	 * @return the nt_num
	 */
	public String getNt_num() {
		return nt_num;
	}

	/**
	 * @return the nt_dt
	 */
	public String getNt_dt() {
		return nt_dt;
	}

	/**
	 * @return the p_gst
	 */
	public String getP_gst() {
		return p_gst;
	}

	/**
	 * @param nt_num the nt_num to set
	 */
	public void setNt_num(String nt_num) {
		this.nt_num = nt_num;
	}

	/**
	 * @param nt_dt the nt_dt to set
	 */
	public void setNt_dt(String nt_dt) {
		this.nt_dt = nt_dt;
	}

	/**
	 * @param p_gst the p_gst to set
	 */
	public void setP_gst(String p_gst) {
		this.p_gst = p_gst;
	}

}
