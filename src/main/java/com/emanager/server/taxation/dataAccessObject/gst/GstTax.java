
package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({
"gstin",
"fp",
"gt",
"cur_gt",
"version",
"hash",
"b2b",
"b2ba",
"b2cl",
"b2cla",
"b2cs",
"b2csa",
"nil",
"exp",
"expa",
"cdnra",
"at",
"ata",
"cdnr",
"cdnur",
"cdnura",
"doc_issue",
"hsn",
"txpd",
"txpda"
})
public class GstTax {
	@JsonProperty("gstin")
    private String gstin;
	 @JsonProperty("fp")
    private String fp;
	 @JsonProperty("gt")
    private Integer gt;
	 @JsonProperty("cur_gt")
    private Integer cur_gt;
	 @JsonProperty("version")
    private String version;
	 @JsonProperty("hash")
    private String hash;
	 @JsonProperty("b2b")
    private List<B2b> b2b = null;
	 @JsonProperty("b2ba")
    private List<B2ba> b2ba = null;
	 @JsonProperty("b2cl")
    private List<B2cl> b2cl = null;
	 @JsonProperty("b2cla")
    private List<B2cla> b2cla = null;
	 @JsonProperty("b2cs")
    private List<B2c> b2cs = null;
	 @JsonProperty("b2csa")
    private List<B2csa> b2csa = null;
	 @JsonProperty("nil")
    private Nil nil;
	 @JsonProperty("exp")
    private List<Exp> exp = null;
	 @JsonProperty("expa")
    private List<Expa> expa = null;
	 @JsonProperty("cdnra")
    private List<Cdnra> cdnra = null;
	 @JsonProperty("at")
    private List<At> at = null;
	 @JsonProperty("ata")
    private List<Atum> ata = null;
	 @JsonProperty("cdnr")
    private List<Cdnr> cdnr = null;
	 @JsonProperty("cdnur")
    private List<Cdnur> cdnur = null;
	 @JsonProperty("cdnura")
    private List<Cdnura> cdnura = null;
	 @JsonProperty("doc_issue")
    private DocIssue doc_issue;
	 @JsonProperty("hsn")
    private Hsn hsn;
    @JsonProperty("txpd")
    private List<Txpd> txpd = null;
    @JsonProperty("txpda")
    private List<Txpda> txpda = null;
	public String getGstin() {
		return gstin;
	}
	public void setGstin(String gstin) {
		this.gstin = gstin;
	}
	public String getFp() {
		return fp;
	}
	public void setFp(String fp) {
		this.fp = fp;
	}
	public Integer getGt() {
		return gt;
	}
	public void setGt(Integer gt) {
		this.gt = gt;
	}
	public Integer getCur_gt() {
		return cur_gt;
	}
	public void setCur_gt(Integer cur_gt) {
		this.cur_gt = cur_gt;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public List<B2b> getB2b() {
		return b2b;
	}
	public void setB2b(List<B2b> b2b) {
		this.b2b = b2b;
	}
	public List<B2ba> getB2ba() {
		return b2ba;
	}
	public void setB2ba(List<B2ba> b2ba) {
		this.b2ba = b2ba;
	}
	public List<B2cl> getB2cl() {
		return b2cl;
	}
	public void setB2cl(List<B2cl> b2cl) {
		this.b2cl = b2cl;
	}
	public List<B2cla> getB2cla() {
		return b2cla;
	}
	public void setB2cla(List<B2cla> b2cla) {
		this.b2cla = b2cla;
	}
	public List<B2c> getB2cs() {
		return b2cs;
	}
	public void setB2cs(List<B2c> b2cs) {
		this.b2cs = b2cs;
	}
	public List<B2csa> getB2csa() {
		return b2csa;
	}
	public void setB2csa(List<B2csa> b2csa) {
		this.b2csa = b2csa;
	}
	public Nil getNil() {
		return nil;
	}
	public void setNil(Nil nil) {
		this.nil = nil;
	}
	public List<Exp> getExp() {
		return exp;
	}
	public void setExp(List<Exp> exp) {
		this.exp = exp;
	}
	public List<Expa> getExpa() {
		return expa;
	}
	public void setExpa(List<Expa> expa) {
		this.expa = expa;
	}
	public List<Cdnra> getCdnra() {
		return cdnra;
	}
	public void setCdnra(List<Cdnra> cdnra) {
		this.cdnra = cdnra;
	}
	public List<At> getAt() {
		return at;
	}
	public void setAt(List<At> at) {
		this.at = at;
	}
	public List<Atum> getAta() {
		return ata;
	}
	public void setAta(List<Atum> ata) {
		this.ata = ata;
	}
	public List<Cdnr> getCdnr() {
		return cdnr;
	}
	public void setCdnr(List<Cdnr> cdnr) {
		this.cdnr = cdnr;
	}
	public List<Cdnur> getCdnur() {
		return cdnur;
	}
	public void setCdnur(List<Cdnur> cdnur) {
		this.cdnur = cdnur;
	}
	public List<Cdnura> getCdnura() {
		return cdnura;
	}
	public void setCdnura(List<Cdnura> cdnura) {
		this.cdnura = cdnura;
	}
	public DocIssue getDoc_issue() {
		return doc_issue;
	}
	public void setDoc_issue(DocIssue doc_issue) {
		this.doc_issue = doc_issue;
	}
	public Hsn getHsn() {
		return hsn;
	}
	public void setHsn(Hsn hsn) {
		this.hsn = hsn;
	}
	public List<Txpd> getTxpd() {
		return txpd;
	}
	public void setTxpd(List<Txpd> txpd) {
		this.txpd = txpd;
	}
	public List<Txpda> getTxpda() {
		return txpda;
	}
	public void setTxpda(List<Txpda> txpda) {
		this.txpda = txpda;
	}
    
  

}
