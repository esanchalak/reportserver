package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"valid",
"mof",
"dof",
"ret_prd",
"rtntype",
"arn",
"status"
})
public class EFiledlist {
	
	@JsonProperty("valid")
	private String valid;
	@JsonProperty("mof")
	private String mof;
	@JsonProperty("dof")
	private String dof;
	@JsonProperty("ret_prd")
	private String retPrd;
	@JsonProperty("rtntype")
	private String rtntype;
	@JsonProperty("arn")
	private String arn;
	@JsonProperty("status")
	private String status;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("valid")
	public String getValid() {
	return valid;
	}

	@JsonProperty("valid")
	public void setValid(String valid) {
	this.valid = valid;
	}

	@JsonProperty("mof")
	public String getMof() {
	return mof;
	}

	@JsonProperty("mof")
	public void setMof(String mof) {
	this.mof = mof;
	}

	@JsonProperty("dof")
	public String getDof() {
	return dof;
	}

	@JsonProperty("dof")
	public void setDof(String dof) {
	this.dof = dof;
	}

	@JsonProperty("ret_prd")
	public String getRetPrd() {
	return retPrd;
	}

	@JsonProperty("ret_prd")
	public void setRetPrd(String retPrd) {
	this.retPrd = retPrd;
	}

	@JsonProperty("rtntype")
	public String getRtntype() {
	return rtntype;
	}

	@JsonProperty("rtntype")
	public void setRtntype(String rtntype) {
	this.rtntype = rtntype;
	}

	@JsonProperty("arn")
	public String getArn() {
	return arn;
	}

	@JsonProperty("arn")
	public void setArn(String arn) {
	this.arn = arn;
	}

	@JsonProperty("status")
	public String getStatus() {
	return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
	this.status = status;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

}
