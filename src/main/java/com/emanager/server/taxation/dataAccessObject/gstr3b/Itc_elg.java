package com.emanager.server.taxation.dataAccessObject.gstr3b;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"itc_avl",
"itc_rev",
"itc_net",
"itc_inelg"
})
public class Itc_elg {

@JsonProperty("itc_avl")
private List<Itc_avl> itcAvl = null;
@JsonProperty("itc_rev")
private List<Itc_rev> itcRev = null;
@JsonProperty("itc_net")
private Itc_net itcNet;
@JsonProperty("itc_inelg")
private List<Itc_inelg> itcInelg = null;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("itc_avl")
public List<Itc_avl> getItcAvl() {
return itcAvl;
}

@JsonProperty("itc_avl")
public void setItcAvl(List<Itc_avl> itcAvl) {
this.itcAvl = itcAvl;
}

@JsonProperty("itc_rev")
public List<Itc_rev> getItcRev() {
return itcRev;
}

@JsonProperty("itc_rev")
public void setItcRev(List<Itc_rev> itcRev) {
this.itcRev = itcRev;
}

@JsonProperty("itc_net")
public Itc_net getItcNet() {
return itcNet;
}

@JsonProperty("itc_net")
public void setItcNet(Itc_net itcNet) {
this.itcNet = itcNet;
}

@JsonProperty("itc_inelg")
public List<Itc_inelg> getItcInelg() {
return itcInelg;
}

@JsonProperty("itc_inelg")
public void setItcInelg(List<Itc_inelg> itcInelg) {
this.itcInelg = itcInelg;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
