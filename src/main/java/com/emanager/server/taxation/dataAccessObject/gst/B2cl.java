
package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonInclude(Include.NON_NULL)
public class B2cl {

    private String pos;
    private List<Inv> inv = null;
    
    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public List<Inv> getInv() {
        return inv;
    }

    public void setInv(List<Inv> inv) {
        this.inv = inv;
    }

   
}
