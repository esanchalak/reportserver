package com.emanager.server.taxation.dataAccessObject.gst;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonInclude(Include.NON_NULL)
public class Atum {

    private String omon;
    private String pos;
    private String sply_ty;
    private List<Itm> itms = null;
    
    public String getOmon() {
        return omon;
    }

    public void setOmon(String omon) {
        this.omon = omon;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

   public List<Itm> getItms() {
        return itms;
    }

    public void setItms(List<Itm> itms) {
        this.itms = itms;
    }

  
	/**
	 * @return the sply_ty
	 */
	public String getSply_ty() {
		return sply_ty;
	}

	/**
	 * @param sply_ty the sply_ty to set
	 */
	public void setSply_ty(String sply_ty) {
		this.sply_ty = sply_ty;
	}

}
