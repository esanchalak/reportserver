
package com.emanager.server.taxation.dataAccessObject.gst;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonInclude(Include.NON_NULL)
public class At {

    private String pos;
    private String sply_ty;
    private List<Itm> itms = null;
    private BigDecimal diff_percent;
    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public List<Itm> getItms() {
        return itms;
    }

    public void setItms(List<Itm> itms) {
        this.itms = itms;
    }

    /**
	 * @return the sply_ty
	 */
	public String getSply_ty() {
		return sply_ty;
	}

	/**
	 * @return the diff_percent
	 */
	public BigDecimal getDiff_percent() {
		return diff_percent;
	}

	/**
	 * @param sply_ty the sply_ty to set
	 */
	public void setSply_ty(String sply_ty) {
		this.sply_ty = sply_ty;
	}

	/**
	 * @param diff_percent the diff_percent to set
	 */
	public void setDiff_percent(BigDecimal diff_percent) {
		this.diff_percent = diff_percent;
	}

}
