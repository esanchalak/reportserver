 package com.emanager.server.taxation.domainObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.model.InternalWorkbook;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.emanager.server.commonUtils.domainObject.CommonUtility;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.invoice.dataAccessObject.InLineItemsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.taxation.DAO.GSTReportsDAO;
import com.emanager.server.taxation.dataAccessObject.gst.B2b;
import com.emanager.server.taxation.dataAccessObject.gst.B2c;
import com.emanager.server.taxation.dataAccessObject.gst.Cdnr;
import com.emanager.server.taxation.dataAccessObject.gst.Cdnra;
import com.emanager.server.taxation.dataAccessObject.gst.Cdnur;
import com.emanager.server.taxation.dataAccessObject.gst.Cdnura;
import com.emanager.server.taxation.dataAccessObject.gst.Datum;
import com.emanager.server.taxation.dataAccessObject.gst.Doc;
import com.emanager.server.taxation.dataAccessObject.gst.DocDet;
import com.emanager.server.taxation.dataAccessObject.gst.DocIssue;
import com.emanager.server.taxation.dataAccessObject.gst.EFiledlist;
import com.emanager.server.taxation.dataAccessObject.gst.Exp;
import com.emanager.server.taxation.dataAccessObject.gst.GstINInfo;
import com.emanager.server.taxation.dataAccessObject.gst.GstRequestVO;
import com.emanager.server.taxation.dataAccessObject.gst.GstStateDetails;
import com.emanager.server.taxation.dataAccessObject.gst.GstTax;
import com.emanager.server.taxation.dataAccessObject.gst.GstrFileHistory;
import com.emanager.server.taxation.dataAccessObject.gst.Hsn;
import com.emanager.server.taxation.dataAccessObject.gst.Inv;
import com.emanager.server.taxation.dataAccessObject.gst.Itm;
import com.emanager.server.taxation.dataAccessObject.gst.ItmDet;
import com.emanager.server.taxation.dataAccessObject.gst.Nil;
import com.emanager.server.taxation.dataAccessObject.gst.Nt;
import com.emanager.server.taxation.dataAccessObject.gst.Nt_;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Comp_details;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Gstr3B;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Inter_sup;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Intr_ltfee;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Inward_sup;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Isup_detail;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Isup_rev;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Itc_avl;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Itc_elg;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Itc_inelg;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Osup_det;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Osup_nil_exmp;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Osup_nongst;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Osup_zero;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Sup_details;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Uin_details;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Unreg_details;
import com.fasterxml.jackson.databind.DeserializationFeature;


public class GSTReportDomain {
	GSTReportsDAO gstReportDAO;
	SocietyService societyService;

	private static final Logger logger = Logger.getLogger(GSTReportDomain.class);
	private Itc_avl itcAvlOth;
	
	
	public GstTax getGST1Report(GstRequestVO gstReqVO){
		GstTax gstVO=new GstTax();
		logger.debug("Entry : public GstTax getGST1Report(GstRequestVO gstReqVO)");
		SocietyVO societyVO=societyService.getSocietyDetails(gstReqVO.getOrgID());
		gstVO.setVersion("GST3.0.4");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date newDate = null;
		try {
			newDate = sdf.parse(gstReqVO.getToDate());
		} catch (ParseException e) {
		logger.error("Exception in parsing date "+e);
		}
		
		SimpleDateFormat sDateFormat = new SimpleDateFormat("MMyyyy");
		String newDateString=sDateFormat.format(newDate);
		gstVO.setFp(newDateString);
		gstVO.setHash("hash");
		gstVO.setGstin(societyVO.getGstIN());
		if(gstReqVO.getGstReports().size()>0){
			
			for(int i=0;gstReqVO.getGstReports().size()>i;i++){
				String reportName=gstReqVO.getGstReports().get(i);
				
				   switch (reportName) {
			         case "hsn":

			        	 Hsn hsn=getHSNReport(gstReqVO.getOrgID(), gstReqVO.getFromDate(), gstReqVO.getToDate());
			        	 gstVO.setHsn(hsn);
			        	 break;
			              
			         case "nil":
			        	 
			        	 Nil nil=getExemptedReport(gstReqVO.getOrgID(), gstReqVO.getFromDate(), gstReqVO.getToDate());
			        	 gstVO.setNil(nil);
			              break;    
		        
			         case "b2cs":
			          List<B2c>	 b2cs= getB2CsReport(gstReqVO.getOrgID(), gstReqVO.getFromDate(), gstReqVO.getToDate());
			        gstVO.setB2cs(b2cs);
			          break;    
			              
			         case "b2b":
				          List<B2b> b2b= getB2BReport(gstReqVO.getOrgID(), gstReqVO.getFromDate(), gstReqVO.getToDate());
				        gstVO.setB2b(b2b);
				          break;  
				          
			         case "cdnr":
				          List<Cdnr> cdnr= getCDNRReport(gstReqVO.getOrgID(), gstReqVO.getFromDate(), gstReqVO.getToDate());
				          if(cdnr.size()>0){
					          gstVO.setCdnr(cdnr);
					          }else gstVO.setCdnr(null);
					       break; 
				          
			         case "cdnra":
				          List<Cdnra> cdnra= getCDNRAReport(gstReqVO.getOrgID(), gstReqVO.getFromDate(), gstReqVO.getToDate());
				        gstVO.setCdnra(cdnra);
				          break;  
				              
			         case "cdnur":
				          List<Cdnur> cdnur= getCDNURReport(gstReqVO.getOrgID(), gstReqVO.getFromDate(), gstReqVO.getToDate());
				        gstVO.setCdnur(cdnur);
				          break;  
				          
			         case "cdnrua":
				          List<Cdnura> cdnura= getCDNURAReport(gstReqVO.getOrgID(), gstReqVO.getFromDate(), gstReqVO.getToDate());
				        gstVO.setCdnura(cdnura);
				          break; 
				          
			         case "exp":
				          List<Exp> exp= getExpReport(gstReqVO.getOrgID(), gstReqVO.getFromDate(), gstReqVO.getToDate());
				          if(exp.size()>0){
					          gstVO.setExp(exp);
					          }else gstVO.setExp(null);
					          break; 
				          
			         case "doc_issue":
				          DocIssue	docIssue= getDocIssueReport(gstReqVO.getOrgID(), gstReqVO.getFromDate(), gstReqVO.getToDate());
				        gstVO.setDoc_issue(docIssue);
				          break;  
				          
			         default:
			        	 
			         }
			
			}
		}
		logger.debug("Exit : public GstTax getGST1Report(GstRequestVO gstReqVO)");
		return gstVO;
	}
	
	
	public GstTax getGST2Report(GstRequestVO gstReqVO){
		GstTax gstVO=new GstTax();
		logger.debug("Entry : public GstTax getGST2Report(GstRequestVO gstReqVO)");
		SocietyVO societyVO=societyService.getSocietyDetails(gstReqVO.getOrgID());
		gstVO.setVersion("GST2.3.2");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date newDate = null;
		try {
			newDate = sdf.parse(gstReqVO.getToDate());
		} catch (ParseException e) {
		logger.error("Exception in parsing date "+e);
		}
		
		SimpleDateFormat sDateFormat = new SimpleDateFormat("MMyyyy");
		String newDateString=sDateFormat.format(newDate);
		gstVO.setFp(newDateString);
		gstVO.setHash("hash");
		gstVO.setGstin(societyVO.getGstIN());
		
		List<B2b> b2b= getB2BReportForPurchase(gstReqVO.getOrgID(), gstReqVO.getFromDate(), gstReqVO.getToDate());
		gstVO.setB2b(b2b);
		
		  List<B2c> b2cs= getB2CsReportForPurchase(gstReqVO.getOrgID(), gstReqVO.getFromDate(), gstReqVO.getToDate());
	        gstVO.setB2cs(b2cs);
		
		logger.debug("Exit : public GstTax getGST2Report(GstRequestVO gstReqVO)");
		return gstVO;
	}
	
	public Hsn getHSNReport(int orgID,String fromDate,String toDate){
		
		logger.debug("Entry :public Hsn getHSNReport(int orgID,String fromDate,String toDate)");
		GstTax gstVO=new GstTax();
		List hsnList=null;
		Hsn hsnObj = null;
		
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);

			hsnObj = new Hsn();
			hsnList=gstReportDAO.getHSNReport(orgID, fromDate, toDate);
			
			if(hsnList.size()>0){
				hsnObj.setData(hsnList);
				
			}
		} catch (Exception e) {
			logger.debug("Exception at getHSNReport(int orgID,String fromDate,String toDate) "+e);
		}
		
		logger.debug("Exit :public Hsn getHSNReport(int orgID,String fromDate,String toDate)");
		return hsnObj;
	}

	
	public Nil getExemptedReport(int orgID,String fromDate,String toDate){
		List nillList=new ArrayList<>();
		List extemptList=new ArrayList<>();
		Nil nil=new Nil();
		logger.debug("Entry : public Nil getExemptedReport(int orgID,String fromDate,String toDate)");
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			extemptList=gstReportDAO.getExemptedReport(orgID, fromDate, toDate);
			
			
			if((nillList.size()>0)||(extemptList.size()>0)){
				double intraNilGst = 0;
				double interNilGst=0;
				double intraZeroGst = 0;
				double interZeroGst=0;
				double intraNotRegZeroGst = 0;
				double interNotRegZeroGst=0;
				double interNonGst=0;
				double intraNonGst=0;
				double intraNonRegNilGst=0;
				double interNonRegNilGst=0;
				double interNonRegNonGst=0;
				double intraNonRegNonGst=0;
				double intraExtGst=0;
				double interExtGst=0;
				double intraNonRegExtGst=0;
				double interNonRegExtGst=0;
				
				if(societyVO.getStateName()!=null){
					String state=societyVO.getGstIN().substring(0,2);
				
					
					for(int i=0;extemptList.size()>i;i++){
						Datum inv=(Datum) extemptList.get(i);
						if(societyVO.getStateName().equalsIgnoreCase(inv.getDesc())){
							//inv.setSplyTy("INTRA");
							if((inv.getUqc()!=null)&&(inv.getUqc().length()>0)){
							if((inv.getNum()==3)){
								intraNilGst=intraNilGst+inv.getIamt();
							}else if(inv.getNum()==5){
							intraNonGst=intraNonGst+inv.getIamt();
							}else if(inv.getNum()==4){
								intraExtGst=intraExtGst+inv.getIamt();
							}else if(inv.getNum()==2){
								intraZeroGst=intraZeroGst+inv.getIamt();
							}
							}	else{
								if(inv.getNum()==3){
									intraNonRegNilGst=intraNonRegNilGst+inv.getIamt();
									
								}else if(inv.getNum()==5){
								intraNonRegNonGst=intraNonRegNonGst+inv.getIamt();
								
								}else if(inv.getNum()==4){
									intraNonRegExtGst=intraNonRegExtGst+inv.getIamt();
								}else if(inv.getNum()==2){
									intraNotRegZeroGst=intraNotRegZeroGst+inv.getIamt();
								}
								
							}
						
								
						}else{
							//inv.setSplyTy("INTER");
							if((inv.getUqc()!=null)&&(inv.getUqc().length()>0)){
							if((inv.getNum()==3)){
								interNilGst=interNilGst+inv.getIamt();
							}else	if((inv.getNum()==5)){
								interNonGst=interNonGst+inv.getIamt();
							}else if(inv.getNum()==4){
								interExtGst=interExtGst+inv.getIamt();
							}else if(inv.getNum()==2){
								interZeroGst=interZeroGst+inv.getIamt();
							}
							}else{
								if((inv.getNum()==3)){
									interNonRegNilGst=interNonRegNilGst+inv.getIamt();
								}else	if((inv.getNum()==5)){
									interNonRegNonGst=interNonRegNonGst+inv.getIamt();
								}else if(inv.getNum()==4){
									interNonRegExtGst=interNonRegExtGst+inv.getIamt();
								}else if(inv.getNum()==2){
									interNotRegZeroGst=interNotRegZeroGst+inv.getIamt();
								}
								
							}
							
						}
						
										
					}
					
					ArrayList<Inv> nilInvList=new ArrayList<>();
					Inv interRegInv=new Inv();
					Inv interNonRegInv=new Inv();
					Inv intraRegInv=new Inv();
					Inv intraNonRegInv=new Inv();
					
					interRegInv.setSply_ty("INTRB2B");
					interRegInv.setNil_amt((int) interNilGst);
					interRegInv.setNgsup_amt((int) interNonGst+(int)interNilGst+(int)interZeroGst);
					interRegInv.setExpt_amt((int)interExtGst);
					nilInvList.add(interRegInv);
					
					intraRegInv.setSply_ty("INTRAB2B");
					intraRegInv.setNil_amt((int) intraNilGst);
					intraRegInv.setNgsup_amt((int) intraNonGst+(int)intraNilGst+(int)intraZeroGst);
					intraRegInv.setExpt_amt((int)intraExtGst);
					nilInvList.add(intraRegInv);
					
					interNonRegInv.setSply_ty("INTRB2C");
					interNonRegInv.setNil_amt((int) interNonRegNilGst);
					interNonRegInv.setNgsup_amt((int) interNonRegNonGst+(int)interNonRegNilGst+(int)interNotRegZeroGst);
					interNonRegInv.setExpt_amt((int)interNonRegExtGst);
					nilInvList.add(interNonRegInv);
					
										
					intraNonRegInv.setSply_ty("INTRAB2C");
					intraNonRegInv.setNil_amt((int) intraNonRegNilGst);
					intraNonRegInv.setNgsup_amt((int) intraNonRegNonGst+(int)intraNonRegNilGst+(int)intraNotRegZeroGst);
					intraNonRegInv.setExpt_amt((int)intraNonRegExtGst);
					nilInvList.add(intraNonRegInv);
					
					nil.setInv(nilInvList);
				}
			
			}
		} catch (Exception e) {
			logger.debug("Exception at getExemptedReport(int orgID,String fromDate,String toDate) "+e);
		}
		
		logger.debug("Exit : public Nil getExemptedReport(int orgID,String fromDate,String toDate)");
		
		return nil;
	}
	
	public List<B2b> getB2BReport(int orgID,String fromDate,String toDate){
		B2b b2b=new B2b();
		List<B2b> b2bList=new ArrayList<B2b>();
		logger.debug("Entry :  public List<B2b> getB2BReport(int orgID,String fromDate,String toDate)");
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			
			b2bList=gstReportDAO.getCGstinListForB2BReport(orgID, fromDate, toDate);
			
			if(b2bList.size()>0){
				
				for(int i=0;b2bList.size()>i;i++){
					
					B2b b2bVO=b2bList.get(i);
					List<Inv> InvList=new ArrayList<>();
					String stateName=b2bVO.getCtin().substring(0, 2);
					
					List invoiceList=gstReportDAO.getInvoiceListForB2BReport(orgID, fromDate, toDate, b2bVO.getCtin());
					
					if(invoiceList.size()>0){
						
						for(int j=0;invoiceList.size()>j;j++){
							InvoiceVO invoiceVO=(InvoiceVO) invoiceList.get(j);
							Inv inv=new Inv();
							
							List invoieItemList=gstReportDAO.getInvoiceLineItemsListForB2BReport(orgID, fromDate, toDate, invoiceVO.getInvoiceID());
							
							if(invoieItemList.size()>0){
								
							inv.setInum(invoiceVO.getInvoiceNumber());
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							java.util.Date newDate=sdf.parse(invoiceVO.getInvoiceDate());
							logger.debug("convert date-"+newDate);
							SimpleDateFormat sDateFormat = new SimpleDateFormat("dd-MM-yyyy");
							
							String newDateString=sDateFormat.format(newDate);
							
							inv.setIdt(newDateString);
							inv.setVal(invoiceVO.getTotalAmount().intValue());
							inv.setPos(invoiceVO.getSupplyStateID()+"");
							if(invoiceVO.getIsReverse()==1){
								inv.setRchrg("Y");
							}else
							inv.setRchrg("N");
							if(invoiceVO.getGstInvoiceTypeID()==17){
								inv.setInvTyp("SEWP");
							}else if(invoiceVO.getGstInvoiceTypeID()==18){
								inv.setInvTyp("SEWOP");
							}else if(invoiceVO.getGstInvoiceTypeID()==19){
							inv.setInvTyp("DE");
							}else if(invoiceVO.getGstInvoiceTypeID()==20){
								inv.setInvTyp("CBW");
							}else if(invoiceVO.getGstInvoiceTypeID()==1){
								inv.setInvTyp("R");
							}
							
							
							List<Itm> itemList=new  ArrayList<>();
							for(int k=0;invoieItemList.size()>k;k++){
								InLineItemsVO lnItemVO=(InLineItemsVO) invoieItemList.get(k);
								
								Itm item=new Itm();
								item.setNum(lnItemVO.getChargeID());
								
								ItmDet itemDetails=new ItmDet();
								if(lnItemVO.getiGST().compareTo(BigDecimal.ZERO)>0){
									itemDetails.setIamt(lnItemVO.getiGST());
								}else{
									itemDetails.setCamt(lnItemVO.getcGST());
									itemDetails.setSamt(lnItemVO.getsGST());
								}								
								
								itemDetails.setCsamt(lnItemVO.getCess());
								itemDetails.setRt(lnItemVO.getGstPercent());
								itemDetails.setTxval(lnItemVO.getTotalPrice());
								item.setItm_det(itemDetails);
								//inv.setGst_category_id(lnItemVO.getGstCategoryID());
								itemList.add(item);
								
							}
						   
							inv.setItms(itemList);
							InvList.add(inv);
						  }
						}
						
					}
					
					b2bVO.setInv(InvList);
					
					
				}
				
				
				
				
			}
		} catch (Exception e) {
			logger.debug("Exception at getB2BReport(int orgID,String fromDate,String toDate) "+e);
		}
		
		
		
		logger.debug("Exit :  public List<B2b> getB2BReport(int orgID,String fromDate,String toDate)");
		
		return b2bList;
	}
	
	public List<B2b> getB2BReportForPurchase(int orgID,String fromDate,String toDate){
		B2b b2b=new B2b();
		List<B2b> b2bList=new ArrayList<B2b>();
		logger.debug("Entry :  public List<B2b> getB2BReportForPurchase(int orgID,String fromDate,String toDate)");
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			
			b2bList=gstReportDAO.getCGstinListForB2BReportForPurchase(orgID, fromDate, toDate);
			
			if(b2bList.size()>0){
				String stateName="";
				for(int i=0;b2bList.size()>i;i++){
					
					B2b b2bVO=b2bList.get(i);
					List<Inv> InvList=new ArrayList<>();
					if((b2bVO.getCtin()!=null)&&(b2bVO.getCtin().length()>0))
					stateName=b2bVO.getCtin().substring(0, 2);
					
					List invoiceList=gstReportDAO.getInvoiceListForB2BReportForPurchase(orgID, fromDate, toDate, b2bVO.getCtin());
					
					if(invoiceList.size()>0){
						
						for(int j=0;invoiceList.size()>j;j++){
							InvoiceVO invoiceVO=(InvoiceVO) invoiceList.get(j);
							Inv inv=new Inv();
							inv.setInum(invoiceVO.getInvoiceNumber());
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							java.util.Date newDate=sdf.parse(invoiceVO.getInvoiceDate());
							logger.debug("convert date-"+newDate);
							SimpleDateFormat sDateFormat = new SimpleDateFormat("dd-MM-yyyy");
							
							String newDateString=sDateFormat.format(newDate);
							
							inv.setIdt(newDateString);
							inv.setVal(invoiceVO.getTotalAmount().intValue());
							inv.setPos(stateName);
							if(invoiceVO.getIsReverse()==1){
								inv.setRchrg("Y");
							}else
							inv.setRchrg("N");
							if(invoiceVO.getGstInvoiceTypeID()==17){
								inv.setInvTyp("SEWP");
							}else if(invoiceVO.getGstInvoiceTypeID()==18){
								inv.setInvTyp("SEWOP");
							}else if(invoiceVO.getGstInvoiceTypeID()==19){
							inv.setInvTyp("DE");
							}else if(invoiceVO.getGstInvoiceTypeID()==20){
								inv.setInvTyp("CBW");
							}else if(invoiceVO.getGstInvoiceTypeID()==1){
								inv.setInvTyp("R");
							}
							inv.setGst_category_id(invoiceVO.getInvoiceTypeID());
							
							
							List invoieItemList=gstReportDAO.getInvoiceLineItemsListForB2BReportForPurchase(orgID, fromDate, toDate, invoiceVO.getInvoiceID());
							List<Itm> itemList=new  ArrayList<>();
							if(invoieItemList.size()>0)
							for(int k=0;invoieItemList.size()>k;k++){
								InLineItemsVO lnItemVO=(InLineItemsVO) invoieItemList.get(k);
								
								Itm item=new Itm();
								item.setNum(lnItemVO.getChargeID());
								
								ItmDet itemDetails=new ItmDet();
								itemDetails.setCamt(lnItemVO.getcGST());
								itemDetails.setSamt(lnItemVO.getsGST());
								itemDetails.setIamt(lnItemVO.getiGST());
								itemDetails.setCsamt(lnItemVO.getCess());
								itemDetails.setRt(lnItemVO.getGstPercent());
								itemDetails.setTxval(lnItemVO.getTotalPrice());
								item.setItm_det(itemDetails);
								itemList.add(item);
								
							}
							inv.setItms(itemList);
							InvList.add(inv);
						}
						
					}
					
					b2bVO.setInv(InvList);
					
					
				}
				
				
				
				
			}
		} catch (Exception e) {
			logger.debug("Exception at getB2BReportForPurchase(int orgID,String fromDate,String toDate) "+e);
		}
		
		
		
		logger.debug("Exit :  public List<B2b> getB2BReportForPurchase(int orgID,String fromDate,String toDate)");
		
		return b2bList;
	}
	
	public List<B2c> getB2CsReport(int orgID,String fromDate,String toDate){
		List b2csList=null;
		logger.debug("Entry :  public List<B2c> getB2CsReport(int orgID,String fromDate,String toDate)");
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			
			b2csList=gstReportDAO.getB2CSReport(orgID, fromDate, toDate);
			
			if(b2csList.size()>0){

			
			if(societyVO.getStateName()!=null){
				String state=societyVO.getGstIN().substring(0,2);
				for(int i=0;b2csList.size()>i;i++){
					B2c b2cs=(B2c) b2csList.get(i);
					if(state.equalsIgnoreCase(b2cs.getPos())){
						b2cs.setSply_ty("INTRA");
					}else{
						b2cs.setSply_ty("INTER");
					}
			
					b2cs.setTyp("OE");
						
				}
				
			}
			
			}
		} catch (Exception e) {
			logger.debug("Exception at getB2CsReport(int orgID,String fromDate,String toDate)"+e);
		}
					
		logger.debug("Exit :  public List<B2c> getB2CsReport(int orgID,String fromDate,String toDate)");
		
		return b2csList;
	}

	public List<B2c> getB2CsReportForPurchase(int orgID,String fromDate,String toDate){
		List b2csList=null;
		logger.debug("Entry :  public List<B2c> getB2CsReportForPurchase(int orgID,String fromDate,String toDate)");
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			
			b2csList=gstReportDAO.getB2CSReportForPurchase(orgID, fromDate, toDate);
			
			if(b2csList.size()>0){

			
			if(societyVO.getStateName()!=null){
				String state=societyVO.getGstIN().substring(0,2);
				for(int i=0;b2csList.size()>i;i++){
					B2c b2cs=(B2c) b2csList.get(i);
					if(societyVO.getStateName().equalsIgnoreCase(b2cs.getPos())){
						b2cs.setSply_ty("INTRA");
					}else{
						b2cs.setSply_ty("INTER");
					}
					b2cs.setPos(state);
					b2cs.setTyp("OE");
						
				}
				
			}
			
			}
		} catch (Exception e) {
			logger.debug("Exception at getB2CsReportForPurchase(int orgID,String fromDate,String toDate)"+e);
		}
					
		logger.debug("Exit :  public List<B2c> getB2CsReportForPurchase(int orgID,String fromDate,String toDate)");
		
		return b2csList;
	}
	
	public List<Cdnr> getCDNRReport(int orgID,String fromDate,String toDate){
		List cdnrList=null;
		logger.debug("Entry :  public List<Cdnr> getCDNRReport(int orgID,String fromDate,String toDate)");
		try {
	       SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			
	       cdnrList=gstReportDAO.getCGstinListForCDNRReport(orgID, fromDate, toDate);
			
			if(cdnrList.size()>0){
				
				for(int i=0;cdnrList.size()>i;i++){
					
					Cdnr cdnrVO=(Cdnr) cdnrList.get(i);
					List<Nt_> NtList=new ArrayList<>();
					if(cdnrVO.getCtin()!=null){
					
					String stateName="";
					if(cdnrVO.getCtin().length()>0)
						stateName=	cdnrVO.getCtin().substring(0, 2);
					
					List invoiceList=gstReportDAO.getInvoiceListForCDNRReport(orgID, fromDate, toDate, cdnrVO.getCtin());
					
					if(invoiceList.size()>0){
						
						for(int j=0;invoiceList.size()>j;j++){
							InvoiceVO invoiceVO=(InvoiceVO) invoiceList.get(j);
							Nt_ ntVO=new Nt_();
							
							List invoieItemList=gstReportDAO.getInvoiceLineItemsListForCDNRReport(orgID, fromDate, toDate, invoiceVO.getInvoiceID());
							
							if(invoieItemList.size()>0){
							
							ntVO.setNt_num(invoiceVO.getInvoiceNumber());
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							java.util.Date newDate=sdf.parse(invoiceVO.getInvoiceDate());
							logger.debug("convert date-"+newDate);
							SimpleDateFormat sDateFormat = new SimpleDateFormat("dd-MM-yyyy");
							
							String newDateString=sDateFormat.format(newDate);
							//ntVO.setInum(invoiceVO.getInvoiceNumber());
							ntVO.setNt_dt(newDateString);
							//ntVO.setIdt(newDateString);
							ntVO.setVal(invoiceVO.getTotalAmount().intValue());
							ntVO.setRchrg("N");
							ntVO.setInv_typ("R");
							ntVO.setPos(stateName);
							//ntVO.setP_gst("N");
							if(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note")){
								ntVO.setNtty("C");
							}else if(invoiceVO.getInvoiceType().equalsIgnoreCase("Debit Note")){
								ntVO.setNtty("D");
							}
							
							
							
							List<Itm> itemList=new  ArrayList<>();
							for(int k=0;invoieItemList.size()>k;k++){
								InLineItemsVO lnItemVO=(InLineItemsVO) invoieItemList.get(k);
								
								Itm item=new Itm();
								item.setNum(lnItemVO.getChargeID());
								
								ItmDet itemDetails=new ItmDet();
								if(lnItemVO.getiGST().compareTo(BigDecimal.ZERO)>0){
									itemDetails.setIamt(lnItemVO.getiGST());
								}else{
									itemDetails.setCamt(lnItemVO.getcGST());
									itemDetails.setSamt(lnItemVO.getsGST());
								}	
								itemDetails.setCsamt(lnItemVO.getCess());
								itemDetails.setRt(lnItemVO.getGstPercent());
								itemDetails.setTxval(lnItemVO.getTotalPrice());
								item.setItm_det(itemDetails);
								itemList.add(item);
								
							}
							ntVO.setItms(itemList);
							NtList.add(ntVO);
						  }
						}
						
					}
					
					cdnrVO.setNt(NtList);
					
					}
				}
				
				
				
				
			}
		} catch (Exception e) {
			logger.debug("Exception at public List<Cdnr> getCDNRReport(int orgID,String fromDate,String toDate)"+e);
		}
					
		logger.debug("Exit :  public List<Cdnr> getCDNRReport(int orgID,String fromDate,String toDate)");
		
		return cdnrList;
	}
	
	
	public List<Cdnra> getCDNRAReport(int orgID,String fromDate,String toDate){
		List cdnraList=null;
		logger.debug("Entry :  public List<Cdnra> getCDNRAReport(int orgID,String fromDate,String toDate)");
		try {
	       SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			
	       cdnraList=gstReportDAO.getCGstinListForCDNRAReport(orgID, fromDate, toDate);
			
			if(cdnraList.size()>0){
				
				for(int i=0;cdnraList.size()>i;i++){
					
					Cdnra cdnraVO=(Cdnra) cdnraList.get(i);
					List<Nt> NtList=new ArrayList<>();
					String stateName=cdnraVO.getCtin().substring(0, 2);
					
					List invoiceList=gstReportDAO.getInvoiceListForCDNRAReport(orgID, fromDate, toDate, cdnraVO.getCtin());
					
					if(invoiceList.size()>0){
						
						for(int j=0;invoiceList.size()>j;j++){
							InvoiceVO invoiceVO=(InvoiceVO) invoiceList.get(j);
							Nt ntVO=new Nt();
							ntVO.setOnt_num(invoiceVO.getInvoiceNumber());
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							java.util.Date newDate=sdf.parse(invoiceVO.getInvoiceDate());
							logger.debug("convert date-"+newDate);
							SimpleDateFormat sDateFormat = new SimpleDateFormat("dd-MM-yyyy");
							
							String newDateString=sDateFormat.format(newDate);
							ntVO.setOnt_dt(newDateString);
							ntVO.setIdt(newDateString);
							ntVO.setVal(invoiceVO.getTotalAmount().intValue());
							ntVO.setP_gst("N");
							if(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note")){
								ntVO.setNtty("C");
							}else if(invoiceVO.getInvoiceType().equalsIgnoreCase("Debit Note")){
								ntVO.setNtty("D");
							}
							
							
							
							List invoieItemList=gstReportDAO.getInvoiceLineItemsListForCDNRAReport(orgID, fromDate, toDate, invoiceVO.getInvoiceID());
							List<Itm> itemList=new  ArrayList<>();
							if(invoieItemList.size()>0)
							for(int k=0;invoieItemList.size()>k;k++){
								InLineItemsVO lnItemVO=(InLineItemsVO) invoieItemList.get(k);
								
								Itm item=new Itm();
								item.setNum(lnItemVO.getChargeID());
								
								ItmDet itemDetails=new ItmDet();
								itemDetails.setCamt(lnItemVO.getcGST());
								itemDetails.setSamt(lnItemVO.getsGST());
								itemDetails.setIamt(lnItemVO.getiGST());
								itemDetails.setCsamt(lnItemVO.getCess());
								itemDetails.setRt(lnItemVO.getGstPercent());
								itemDetails.setTxval(lnItemVO.getTotalPrice());
								item.setItm_det(itemDetails);
								itemList.add(item);
								
							}
							ntVO.setItms(itemList);
							NtList.add(ntVO);
						}
						
					}
					
					cdnraVO.setNt(NtList);
					
					
				}
				
				
				
				
			}
		} catch (Exception e) {
			logger.debug("Exception at public List<Cdnr> getCDNRAReport(int orgID,String fromDate,String toDate)"+e);
		}
					
		logger.debug("Exit :  public List<Cdnr> getCDNRAReport(int orgID,String fromDate,String toDate)");
		
		return cdnraList;
	}
	
	public List<Cdnur> getCDNURReport(int orgID,String fromDate,String toDate){
		List cdnurList=null;
		logger.debug("Entry :  public List<Cdnur> getCDNURReport(int orgID,String fromDate,String toDate)");
		try {
	       SocietyVO societyVO=societyService.getSocietyDetails(orgID);
											
	       			List invoiceList=gstReportDAO.getInvoiceListForCDNURReport(orgID, fromDate, toDate);
					
					if(invoiceList.size()>0){
						
						for(int j=0;invoiceList.size()>j;j++){
							InvoiceVO invoiceVO=(InvoiceVO) invoiceList.get(j);
							Cdnur cdnurVO=new Cdnur();
							cdnurVO.setNt_num(invoiceVO.getInvoiceNumber());
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							java.util.Date newDate=sdf.parse(invoiceVO.getInvoiceDate());
							logger.debug("convert date-"+newDate);
							SimpleDateFormat sDateFormat = new SimpleDateFormat("dd-MM-yyyy");
							
							String newDateString=sDateFormat.format(newDate);
							
							cdnurVO.setIdt(newDateString);
							cdnurVO.setVal(invoiceVO.getTotalAmount().intValue());
							cdnurVO.setP_gst("N");
							if(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note")){
								cdnurVO.setNtty("C");
							}else if(invoiceVO.getInvoiceType().equalsIgnoreCase("Debit Note")){
								cdnurVO.setNtty("D");
							}
							
							
							
							List invoieItemList=gstReportDAO.getInvoiceLineItemsListForCDNURReport(orgID, fromDate, toDate, invoiceVO.getInvoiceID());
							List<Itm> itemList=new  ArrayList<>();
							if(invoieItemList.size()>0)
							for(int k=0;invoieItemList.size()>k;k++){
								InLineItemsVO lnItemVO=(InLineItemsVO) invoieItemList.get(k);
								
								Itm item=new Itm();
								item.setNum(lnItemVO.getChargeID());
								
								ItmDet itemDetails=new ItmDet();
								itemDetails.setCamt(lnItemVO.getcGST());
								itemDetails.setSamt(lnItemVO.getsGST());
								itemDetails.setIamt(lnItemVO.getiGST());
								itemDetails.setCsamt(lnItemVO.getCess());
								itemDetails.setRt(lnItemVO.getGstPercent());
								itemDetails.setTxval(lnItemVO.getTotalPrice());
								item.setItm_det(itemDetails);
								itemList.add(item);
								
							}
							cdnurVO.setItms(itemList);
							cdnurList.add(cdnurVO);
						}
						
					}
					
				
				
				
				
				
		
		} catch (Exception e) {
			logger.debug("Exception at public List<Cdnr> getCDNRUReport(int orgID,String fromDate,String toDate)"+e);
		}
					
		logger.debug("Exit :  public List<Cdnr> getCDNURReport(int orgID,String fromDate,String toDate)");
		
		return cdnurList;
	}
	
	
	public List<Cdnura> getCDNURAReport(int orgID,String fromDate,String toDate){
		List cdnuraList=null;
		logger.debug("Entry :  public List<Cdnra> getCDNRAReport(int orgID,String fromDate,String toDate)");
		try {
	       SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			
	       	List invoiceList=gstReportDAO.getInvoiceListForCDNURReport(orgID, fromDate, toDate);
					
					if(invoiceList.size()>0){
						
						for(int j=0;invoiceList.size()>j;j++){
							InvoiceVO invoiceVO=(InvoiceVO) invoiceList.get(j);
							Cdnura cdnuraVO=new Cdnura();
							cdnuraVO.setOnt_num(invoiceVO.getInvoiceNumber());
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							java.util.Date newDate=sdf.parse(invoiceVO.getInvoiceDate());
							logger.debug("convert date-"+newDate);
							SimpleDateFormat sDateFormat = new SimpleDateFormat("dd-MM-yyyy");
							
							String newDateString=sDateFormat.format(newDate);
							cdnuraVO.setOnt_dt(newDateString);
							cdnuraVO.setIdt(newDateString);
							cdnuraVO.setVal(invoiceVO.getTotalAmount().intValue());
							cdnuraVO.setP_gst("N");
							if(invoiceVO.getInvoiceType().equalsIgnoreCase("Credit Note")){
								cdnuraVO.setNtty("C");
							}else if(invoiceVO.getInvoiceType().equalsIgnoreCase("Debit Note")){
								cdnuraVO.setNtty("D");
							}
							
							
							
							List invoieItemList=gstReportDAO.getInvoiceLineItemsListForCDNRAReport(orgID, fromDate, toDate, invoiceVO.getInvoiceID());
							List<Itm> itemList=new  ArrayList<>();
							if(invoieItemList.size()>0)
							for(int k=0;invoieItemList.size()>k;k++){
								InLineItemsVO lnItemVO=(InLineItemsVO) invoieItemList.get(k);
								
								Itm item=new Itm();
								item.setNum(lnItemVO.getChargeID());
								
								ItmDet itemDetails=new ItmDet();
								itemDetails.setCamt(lnItemVO.getcGST());
								itemDetails.setSamt(lnItemVO.getsGST());
								itemDetails.setIamt(lnItemVO.getiGST());
								itemDetails.setCsamt(lnItemVO.getCess());
								itemDetails.setRt(lnItemVO.getGstPercent());
								itemDetails.setTxval(lnItemVO.getTotalPrice());
								item.setItm_det(itemDetails);
								itemList.add(item);
								
							}
							cdnuraVO.setItms(itemList);
							cdnuraList.add(cdnuraVO);
						}
						
					}
					
				
				
				
			
		} catch (Exception e) {
			logger.debug("Exception at public List<Cdnr> getCDNURAReport(int orgID,String fromDate,String toDate)"+e);
		}
					
		logger.debug("Exit :  public List<Cdnr> getCDNRAReport(int orgID,String fromDate,String toDate)");
		
		return cdnuraList;
	}
	
	
	public DocIssue getDocIssueReport(int orgID,String fromDate,String toDate){
		DocIssue docIssue=new DocIssue();
		logger.debug("Entry :  public DocIssue getDocIssueReport(int orgID,String fromDate,String toDate)");
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			//Invoices for outward suppy for registered user
			List docRegList=null;
			DocDet docRegDet=new DocDet();	
			docRegList=gstReportDAO.getInvoiceListForDocIssueRegisteredReport(orgID, fromDate, toDate, 0);
			InvoiceVO fromInvReg=new InvoiceVO();	
			InvoiceVO toInvReg=new InvoiceVO();
			int invCnt=0;
			int invCanclCnt=0;
			
			if(docRegList.size()>0){
				fromInvReg=(InvoiceVO) docRegList.get(0);	
				toInvReg=(InvoiceVO) docRegList.get(docRegList.size()-1);
				
				for(int i=0;docRegList.size()>i;i++){
					InvoiceVO inv=(InvoiceVO) docRegList.get(i);
					if(inv.getIsDeleted()==0){
						invCnt=invCnt+1;
						
					}else if(inv.getIsDeleted()==1){
						invCanclCnt=invCanclCnt+1;
						
					}
				}
				
			}
			
				List cancelDocRegList=gstReportDAO.getInvoiceListForDocIssueRegisteredReport(orgID, fromDate, toDate, 1);
				
				docRegDet.setDoc_num(1);
				docRegDet.setDoc_typ("Invoices for outward supply");
				 Doc docsReg=new Doc();
				 docsReg.setNum(1);
				 docsReg.setFrom(fromInvReg.getInvoiceNumber());
				 docsReg.setTo(toInvReg.getInvoiceNumber());
				 docsReg.setTotnum(docRegList.size());
				 docsReg.setCancel(invCanclCnt);
				 docsReg.setNet_issue(invCnt);
				 List<Doc> docsRegList=new ArrayList<>();
				 docsRegList.add(docsReg);
				 docRegDet.setDocs(docsRegList);
				 
				 List<DocDet> docDetList=new ArrayList<>();
				 if(docsReg.getNet_issue()>0)
				 docDetList.add(docRegDet);
				 
				 //Invoices for outward supplies for unregistered user
					List docUnRegList=null;
					DocDet docUnRegDet=new DocDet();	
					docUnRegList=gstReportDAO.getInvoiceListForDocIssueUnRegisteredReport(orgID, fromDate, toDate, 0);
					InvoiceVO fromInvUnReg=new InvoiceVO();
					InvoiceVO toInvUn=new InvoiceVO();
					
					if(docUnRegList.size()>0){
						fromInvUnReg=(InvoiceVO) docUnRegList.get(0);	
						toInvUn=(InvoiceVO) docUnRegList.get(docUnRegList.size()-1);
					}
					
						List cancelDocUnRegList=gstReportDAO.getInvoiceListForDocIssueUnRegisteredReport(orgID, fromDate, toDate, 1);
						
						docUnRegDet.setDoc_num(2);
						docUnRegDet.setDoc_typ("Invoices for inward supply from unregistered person");
						 Doc docsUnReg=new Doc();
						 docsUnReg.setNum(1);
						 docsUnReg.setFrom(fromInvUnReg.getInvoiceNumber());
						 docsUnReg.setTo(toInvUn.getInvoiceNumber());
						 docsUnReg.setTotnum(docUnRegList.size()+cancelDocUnRegList.size());
						 docsUnReg.setCancel(cancelDocUnRegList.size());
						 docsUnReg.setNet_issue(docUnRegList.size());
						 List<Doc> docsUnRegList=new ArrayList<>();
						 docsUnRegList.add(docsUnReg);
						 docUnRegDet.setDocs(docsUnRegList);
						 
						
						/* if(docsUnReg.getNet_issue()>0)
						 docDetList.add(docUnRegDet);*/
						 
						 //Documents issued for Revised invoices
						 List docRIList=null;
							DocDet docRIDet=new DocDet();	
							//docRIList=gstReportDAO.getInvoiceListForDocIssueUnRegisteredReport(orgID, fromDate, toDate, 0);
						//	InvoiceVO fromInvRI=(InvoiceVO) docRIList.get(0);	
						//	InvoiceVO toInvRI=(InvoiceVO) docRIList.get(docRIList.size()-1);
							
								List cancelDocRIList=null;//gstReportDAO.getInvoiceListForDocIssueUnRegisteredReport(orgID, fromDate, toDate, 1);
								
								docRIDet.setDoc_num(3);
								docRIDet.setDoc_typ("Revised Invoice");
								 Doc docsRI=new Doc();
								 docsRI.setNum(1);
								// docsRI.setFrom(fromInvRI.getInvoiceNumber());
								// docsRI.setTo(toInvRI.getInvoiceNumber());
								// docsRI.setTotnum(docRIList.size()+cancelDocRIList.size());
							//	 docsRI.setCancel(cancelDocRIList.size());
								 docsRI.setNet_issue(0);
								 List<Doc> docsRIList=new ArrayList<>();
								 docsRIList.add(docsRI);
								 docRIDet.setDocs(docsRIList);
								 
								
								 if(docsRI.getNet_issue()>0)
								 docDetList.add(docRIDet);
								 
								 //Documents issued for Debit  note
								 List docDNList=null;
									DocDet docDNDet=new DocDet();	
									docDNList=gstReportDAO.getInvoiceListForDocIssueCreditDebitNoteReport(orgID, fromDate, toDate, 0,"Debit Note");
									InvoiceVO fromInvDN=new InvoiceVO();	
									InvoiceVO toInvDN=new InvoiceVO();
									
									if(docDNList.size()>0){
										fromInvDN=(InvoiceVO) docDNList.get(0);	
										toInvDN=(InvoiceVO) docDNList.get(docDNList.size()-1);
									}
									
										List cancelDocDNList=gstReportDAO.getInvoiceListForDocIssueCreditDebitNoteReport(orgID, fromDate, toDate, 1,"Debit Note");
										
										docDNDet.setDoc_num(4);
										docDNDet.setDoc_typ("Debit Note");
										 Doc docsDN=new Doc();
										 docsDN.setNum(1);
										 docsDN.setFrom(fromInvDN.getInvoiceNumber());
										 docsDN.setTo(toInvDN.getInvoiceNumber());
										 docsDN.setTotnum(docDNList.size()+cancelDocDNList.size());
										 docsDN.setCancel(cancelDocDNList.size());
										 docsDN.setNet_issue(docDNList.size());
										 List<Doc> docsDNList=new ArrayList<>();
										 docsDNList.add(docsDN);
										 docDNDet.setDocs(docsDNList);
										 
										
										 if(docsDN.getNet_issue()>0)
										 docDetList.add(docDNDet);
						 
						//Documents issued for Credit  note
						 List docCNList=null;
						DocDet docCNDet=new DocDet();	
						docCNList=gstReportDAO.getInvoiceListForDocIssueCreditDebitNoteReport(orgID, fromDate, toDate, 0,"Credit Note");
						InvoiceVO fromInvCN=new InvoiceVO();	
						InvoiceVO toInvCN=new InvoiceVO();
						
						if(docCNList.size()>0){
							fromInvCN=(InvoiceVO) docCNList.get(0);	
							toInvCN=(InvoiceVO) docCNList.get(docCNList.size()-1);
						}
											
						List cancelDocCNList=gstReportDAO.getInvoiceListForDocIssueCreditDebitNoteReport(orgID, fromDate, toDate, 1,"Credit Note");
												
							docCNDet.setDoc_num(5);
							docCNDet.setDoc_typ("Credit Note");
					 Doc docsCN=new Doc();
					         docsCN.setNum(1);
							 docsCN.setFrom(fromInvCN.getInvoiceNumber());
							 docsCN.setTo(toInvCN.getInvoiceNumber());
							 docsCN.setTotnum(docCNList.size()+cancelDocCNList.size());
							 docsCN.setCancel(cancelDocCNList.size());
							 docsCN.setNet_issue(docCNList.size());
							 List<Doc> docsCNList=new ArrayList<>();
							 docsCNList.add(docsCN);
							 docCNDet.setDocs(docsCNList);
												 
												
							 if(docsCN.getNet_issue()>0)
							 docDetList.add(docCNDet);						 
										 
				 
				 docIssue.setDoc_det(docDetList);
					
				
				
			//Documents issued for Reverse charged purchase
				List docRevPurList=null;
				DocDet docRPDet=new DocDet();	
				docRevPurList=gstReportDAO.getInvoiceListForReverseChargeDetailsReport(orgID, fromDate, toDate,0);
				InvoiceVO fromInvRP=new InvoiceVO();	
				InvoiceVO toInvRP=new InvoiceVO();
				
				if(docRevPurList.size()>0){
					fromInvRP=(InvoiceVO) docRevPurList.get(0);	
					toInvRP=(InvoiceVO) docRevPurList.get(docRevPurList.size()-1);
				}
									
				List cancelDocRevPurList=gstReportDAO.getInvoiceListForReverseChargeDetailsReport(orgID, fromDate, toDate, 1);
										
					docRPDet.setDoc_num(6);
					docRPDet.setDoc_typ("Invoices for inward supply from unregistered person");
			 Doc docsRP=new Doc();
			         docsRP.setNum(1);
					 docsRP.setFrom(fromInvRP.getInvoiceNumber());
					 docsRP.setTo(toInvRP.getInvoiceNumber());
					 docsRP.setTotnum(docRevPurList.size()+cancelDocRevPurList.size());
					 docsRP.setCancel(cancelDocRevPurList.size());
					 docsRP.setNet_issue(docRevPurList.size());
					 List<Doc> docsRevPurList=new ArrayList<>();
					 docsRevPurList.add(docsRP);
					 docRPDet.setDocs(docsRevPurList);
										 
										
					 if(docsRP.getNet_issue()>0)
					 docDetList.add(docRPDet);						 
								 
		 
		 docIssue.setDoc_det(docDetList);	 
				
				
			
		
		} catch (Exception e) {
			logger.debug("Exception at public DocIssue getDocIssueReport(int orgID,String fromDate,String toDate)"+e);
		}
					
		logger.debug("Exit :  public DocIssue getDocIssueReport(int orgID,String fromDate,String toDate)");
		
		return docIssue;
	}
	
	public List<Exp> getExpReport(int orgID,String fromDate,String toDate){
		Exp expWOPAY=new Exp();
		Exp expWPAY=new Exp();
		List<Exp> expList=new ArrayList<Exp>();
		logger.debug("Entry :  public DocIssue getExpReport(int orgID,String fromDate,String toDate)");
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			
					List<Inv> InvWOList=new ArrayList<>();
					
					List invoiceWOList=gstReportDAO.getInvoiceListForExpReport(orgID, fromDate, toDate, 22);
					
					if(invoiceWOList.size()>0){
						
						for(int j=0;invoiceWOList.size()>j;j++){
							InvoiceVO invoiceVO=(InvoiceVO) invoiceWOList.get(j);
							Inv inv=new Inv();
							
							List invoieItemList=gstReportDAO.getInvoiceLineItemsListForReport(orgID, fromDate, toDate, invoiceVO.getInvoiceID());
														
							if(invoieItemList.size()>0){
								
							inv.setInum(invoiceVO.getInvoiceNumber());
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							java.util.Date newDate=sdf.parse(invoiceVO.getInvoiceDate());
							java.util.Date shippingDate=sdf.parse(invoiceVO.getShippingDate());
							logger.debug("convert date-"+newDate);
							SimpleDateFormat sDateFormat = new SimpleDateFormat("dd-MM-yyyy");
							
							String newDateString=sDateFormat.format(newDate);
							
							inv.setIdt(newDateString);
							inv.setVal(invoiceVO.getTotalAmount().intValue());
							inv.setSbdt(sDateFormat.format(shippingDate));
							inv.setSbnum(invoiceVO.getShippingBillNo());
							inv.setSbpcode(invoiceVO.getPortCode());
							
							List<Itm> itemList=new  ArrayList<>();
							for(int k=0;invoieItemList.size()>k;k++){
								InLineItemsVO lnItemVO=(InLineItemsVO) invoieItemList.get(k);
								
								Itm item=new Itm();
								item.setIamt(lnItemVO.getiGST());						
								item.setCsamt(lnItemVO.getCess());
								item.setRt(lnItemVO.getGstPercent());
								item.setTxval(lnItemVO.getTotalPrice());
							
								itemList.add(item);
								
							}
							inv.setItms(itemList);
							InvWOList.add(inv);
						   }
							
						}
						
					}
					
					expWOPAY.setInv(InvWOList);
					expWOPAY.setExp_typ("WOPAY");
					
					List<Inv> InvList=new ArrayList<>();
					
					List invoiceList=gstReportDAO.getInvoiceListForExpReport(orgID, fromDate, toDate, 21);
					
					if(invoiceList.size()>0){
						
						for(int j=0;invoiceList.size()>j;j++){
							InvoiceVO invoiceVO=(InvoiceVO) invoiceList.get(j);
							Inv inv=new Inv();
							
							List invoieItemList=gstReportDAO.getInvoiceLineItemsListForReport(orgID, fromDate, toDate, invoiceVO.getInvoiceID());
							
							if(invoieItemList.size()>0){
								
							inv.setInum(invoiceVO.getInvoiceNumber());
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							java.util.Date newDate=sdf.parse(invoiceVO.getInvoiceDate());
							java.util.Date shippingDate=sdf.parse(invoiceVO.getShippingDate());
							logger.debug("convert date-"+newDate);
							SimpleDateFormat sDateFormat = new SimpleDateFormat("dd-MM-yyyy");
							
							String newDateString=sDateFormat.format(newDate);
							
							inv.setIdt(newDateString);
							inv.setVal(invoiceVO.getTotalAmount().intValue());
							inv.setSbdt(sDateFormat.format(shippingDate));
							inv.setSbnum(invoiceVO.getShippingBillNo());
							inv.setSbpcode(invoiceVO.getPortCode());
							
							List<Itm> itemList=new  ArrayList<>();
							for(int k=0;invoieItemList.size()>k;k++){
								InLineItemsVO lnItemVO=(InLineItemsVO) invoieItemList.get(k);
								
								Itm item=new Itm();
								item.setIamt(lnItemVO.getiGST());
								item.setCsamt(lnItemVO.getCess());
								item.setRt(lnItemVO.getGstPercent());
								item.setTxval(lnItemVO.getTotalPrice());
	
								itemList.add(item);
								
							}
							inv.setItms(itemList);
							InvList.add(inv);
							}
							
						}
						
					}
					
					expWPAY.setInv(InvList);
					expWPAY.setExp_typ("WPAY");
					
					
				if(expWOPAY.getInv().size()>0)
				expList.add(expWOPAY);
				if(expWPAY.getInv().size()>0)
				expList.add(expWPAY);
				
				
				
			
		} catch (Exception e) {
			logger.debug("Exception at public DocIssue getExpReport(int orgID,String fromDate,String toDate)"+e);
		}
					
		logger.debug("Exit :  public DocIssue getExpReport(int orgID,String fromDate,String toDate)");
		
		return expList;
	}
	
	//== == GSTR 3 B =========//
	
	public Gstr3B getGSTR3BReport(GstRequestVO gstReqVO){
		Gstr3B gstVO=new Gstr3B();
		logger.debug("Entry : public Gstr3B getGSTR3BReport(GstRequestVO gstReqVO)");
		SocietyVO societyVO=societyService.getSocietyDetails(gstReqVO.getOrgID());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date newDate = null;
		try {
			newDate = sdf.parse(gstReqVO.getToDate());
		} catch (ParseException e) {
		logger.error("Exception in parsing date "+e);
		}
		
		SimpleDateFormat sDateFormat = new SimpleDateFormat("MMyyyy");
		String newDateString=sDateFormat.format(newDate);
		gstVO.setRet_period(newDateString);
		gstVO.setGstin(societyVO.getGstIN());
		
		try{
		
			Sup_details supDetails=getSupDetailsReport(gstReqVO.getOrgID(), gstReqVO.getFromDate(), gstReqVO.getToDate());
			gstVO.setSup_details(supDetails);
			
			Itc_elg itcElg=getItcElgReport(gstReqVO.getOrgID(),gstReqVO.getFromDate(),gstReqVO.getToDate());
			gstVO.setItc_elg(itcElg);
			
			Inward_sup inwardSupply=getInwardSupplyReport(gstReqVO.getOrgID(),gstReqVO.getFromDate(),gstReqVO.getToDate());
			gstVO.setInward_sup(inwardSupply);
			
			Intr_ltfee intrLatefee=getInterestLateFeeReport(gstReqVO.getOrgID(),gstReqVO.getFromDate(),gstReqVO.getToDate());
			gstVO.setIntr_ltfee(intrLatefee);
			
			Inter_sup interStateSupply=getInterStateSupplyReport(gstReqVO.getOrgID(),gstReqVO.getFromDate(),gstReqVO.getToDate());
			gstVO.setInter_sup(interStateSupply);
			
			
		}catch(Exception e){
		logger.error("Exception in GSTR3B "+e);
		}
		
			
			       
		logger.debug("Exit : public Gstr3B getGSTR3BReport(GstRequestVO gstReqVO)");
		return gstVO;
	}
	
public Sup_details getSupDetailsReport(int orgID,String fromDate,String toDate){
		
		logger.debug("Entry :public Hsn getSupDetailsReport(int orgID,String fromDate,String toDate)");
		GstTax gstVO=new GstTax();
		List supplyList=null;
		Sup_details supObj = null;
		
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);

			supObj = new Sup_details();
			Osup_det osupplyDetails=new Osup_det();
			Osup_zero osupplyZero=new Osup_zero();
			Osup_nil_exmp osupNilExmp=new Osup_nil_exmp();
			supplyList=gstReportDAO.getInvoiceListForTaxableSupplyDetailsReport(orgID, fromDate, toDate);
			BigDecimal taxableValue=BigDecimal.ZERO;
			BigDecimal iGst=BigDecimal.ZERO;
			BigDecimal cGst=BigDecimal.ZERO;
			BigDecimal sGst=BigDecimal.ZERO;
			BigDecimal uGst=BigDecimal.ZERO;
			BigDecimal cess=BigDecimal.ZERO;
			if(supplyList.size()>0){
				
				for(int i=0;supplyList.size()>i;i++){
					InvoiceVO invVO=(InvoiceVO) supplyList.get(i);
				
					if(invVO.getInvoiceType().equalsIgnoreCase("Credit Note")){
						taxableValue=taxableValue.subtract(invVO.getInvoiceAmount());
						iGst=iGst.subtract(invVO.getTotalIGST());
						cGst=cGst.subtract(invVO.getTotalCGST());
						sGst=sGst.subtract(invVO.getTotalSGST());
						uGst=uGst.subtract(invVO.getTotalUGST());
						cess=cess.subtract(invVO.getTotalCess());
						
					}else if(invVO.getInvoiceType().equalsIgnoreCase("Sales")){
					
						taxableValue=taxableValue.add(invVO.getInvoiceAmount());
						iGst=iGst.add(invVO.getTotalIGST());
						cGst=cGst.add(invVO.getTotalCGST());
						sGst=sGst.add(invVO.getTotalSGST());
						uGst=uGst.add(invVO.getTotalUGST());
						cess=cess.add(invVO.getTotalCess());
					}
					
				}
				
				osupplyDetails.setTxval(taxableValue);
				osupplyDetails.setCamt(cGst);
				osupplyDetails.setCsamt(cess);
				osupplyDetails.setIamt(iGst);
				osupplyDetails.setUamt(uGst);
				osupplyDetails.setSamt(sGst);
			}	
				supObj.setOsup_det(osupplyDetails);
				
				List zeroInvList=gstReportDAO.getInvoiceListForNonTaxableSupplyDetailsReport(orgID, fromDate, toDate,2);
				BigDecimal zeroTaxableValue=BigDecimal.ZERO;
				BigDecimal zeroIGst=BigDecimal.ZERO;
				BigDecimal zeroCGst=BigDecimal.ZERO;
				BigDecimal zeroSGst=BigDecimal.ZERO;
				BigDecimal zeroUGst=BigDecimal.ZERO;
				BigDecimal zeroCess=BigDecimal.ZERO;
				if(zeroInvList.size()>0){
					
					for(int i=0;zeroInvList.size()>i;i++){
						InvoiceVO invVO=(InvoiceVO) zeroInvList.get(i);
						if(invVO.getInvoiceType().equalsIgnoreCase("Credit Note")){
							zeroTaxableValue=zeroTaxableValue.subtract(invVO.getInvoiceAmount());
							zeroIGst=zeroIGst.subtract(invVO.getTotalIGST());
							zeroCGst=zeroCGst.subtract(invVO.getTotalCGST());
							zeroSGst=zeroSGst.subtract(invVO.getTotalSGST());
							zeroUGst=zeroUGst.subtract(invVO.getTotalUGST());
							zeroCess=zeroCess.subtract(invVO.getTotalCess());
							
						}else if(invVO.getInvoiceType().equalsIgnoreCase("Sales")){
						zeroTaxableValue=zeroTaxableValue.add(invVO.getInvoiceAmount());
						zeroIGst=zeroIGst.add(invVO.getTotalIGST());
						zeroCGst=zeroCGst.add(invVO.getTotalCGST());
						zeroSGst=zeroSGst.add(invVO.getTotalSGST());
						zeroUGst=zeroUGst.add(invVO.getTotalUGST());
						zeroCess=zeroCess.add(invVO.getTotalCess());
						}
				}
					
					osupplyZero.setTxval(zeroTaxableValue);
					osupplyZero.setCamt(zeroCGst);
					osupplyZero.setCsamt(zeroCess);
					osupplyZero.setIamt(zeroIGst);
					osupplyZero.setSamt(zeroSGst);
				}
				supObj.setOsup_zero(osupplyZero);
				
				List nilInvList=gstReportDAO.getInvoiceListForNilExmtpSupplyDetailsReport(orgID, fromDate, toDate,2);
				BigDecimal nilTaxableValue=BigDecimal.ZERO;
				BigDecimal nilIGst=BigDecimal.ZERO;
				BigDecimal nilCGst=BigDecimal.ZERO;
				BigDecimal nilSGst=BigDecimal.ZERO;
				BigDecimal nilUGst=BigDecimal.ZERO;
				BigDecimal nilCess=BigDecimal.ZERO;
				if(nilInvList.size()>0){
					
					for(int i=0;nilInvList.size()>i;i++){
						InvoiceVO invVO=(InvoiceVO) nilInvList.get(i);
						if(invVO.getInvoiceType().equalsIgnoreCase("Credit Note")){
							nilTaxableValue=nilTaxableValue.subtract(invVO.getInvoiceAmount());
							nilIGst=nilIGst.subtract(invVO.getTotalIGST());
							nilCGst=nilCGst.subtract(invVO.getTotalCGST());
							nilSGst=nilSGst.subtract(invVO.getTotalSGST());
							nilUGst=nilUGst.subtract(invVO.getTotalUGST());
							nilCess=nilCess.subtract(invVO.getTotalCess());
							
						}else if(invVO.getInvoiceType().equalsIgnoreCase("Sales")){
							nilTaxableValue=nilTaxableValue.add(invVO.getInvoiceAmount());
							nilIGst=nilIGst.add(invVO.getTotalIGST());
							nilCGst=nilCGst.add(invVO.getTotalCGST());
							nilSGst=nilSGst.add(invVO.getTotalSGST());
							nilUGst=nilUGst.add(invVO.getTotalUGST());
							nilCess=nilCess.add(invVO.getTotalCess());
						}
				}
					
					osupNilExmp.setTxval(nilTaxableValue);
					osupNilExmp.setCamt(nilCGst);
					osupNilExmp.setCsamt(nilCess);
					osupNilExmp.setIamt(nilIGst);
					osupNilExmp.setSamt(nilSGst);
				}
				supObj.setOsup_nil_exmp(osupNilExmp);
				
				
				List reverseList=null;
				Isup_rev supplyRevChargeObj=new Isup_rev();
				reverseList=gstReportDAO.getInvoiceListForReverseChargeDetailsReport(orgID, fromDate, toDate,0);
				
				BigDecimal revTaxableValue=BigDecimal.ZERO;
				BigDecimal iGstRev=BigDecimal.ZERO;
				BigDecimal cGstRev=BigDecimal.ZERO;
				BigDecimal sGstRev=BigDecimal.ZERO;
				BigDecimal uGstRev=BigDecimal.ZERO;
				BigDecimal cessRev=BigDecimal.ZERO;
				if(reverseList.size()>0){
					
					for(int i=0;reverseList.size()>i;i++){
						InvoiceVO invVO=(InvoiceVO) reverseList.get(i);
					
							revTaxableValue=revTaxableValue.add(invVO.getInvoiceAmount());
							iGstRev=iGstRev.add(invVO.getTotalIGST());
							cGstRev=cGstRev.add(invVO.getTotalCGST());
							sGstRev=sGstRev.add(invVO.getTotalSGST());
							uGstRev=uGstRev.add(invVO.getTotalUGST());
							cessRev=cessRev.add(invVO.getTotalCess());
																
					}
				supplyRevChargeObj.setCamt(cGstRev);
				supplyRevChargeObj.setCsamt(cessRev);
				supplyRevChargeObj.setIamt(iGstRev);
				supplyRevChargeObj.setSamt(sGstRev);
				supplyRevChargeObj.setTxval(revTaxableValue);
				}
				supObj.setIsup_rev(supplyRevChargeObj);
				
				Osup_nongst supplyNonGst=new Osup_nongst();
				
				List nonGstInvList=gstReportDAO.getInvoiceListForNonGstSupplyDetailsReport(orgID, fromDate, toDate);
				BigDecimal nonTaxableValue=BigDecimal.ZERO;
				BigDecimal nonIGst=BigDecimal.ZERO;
				BigDecimal nonCGst=BigDecimal.ZERO;
				BigDecimal nonSGst=BigDecimal.ZERO;
				BigDecimal nonUGst=BigDecimal.ZERO;
				BigDecimal nonCess=BigDecimal.ZERO;
				if(nonGstInvList.size()>0){
					
					for(int i=0;nonGstInvList.size()>i;i++){
						InvoiceVO invVO=(InvoiceVO) nonGstInvList.get(i);
						if(invVO.getInvoiceType().equalsIgnoreCase("Credit Note")){
							nonTaxableValue=nonTaxableValue.subtract(invVO.getInvoiceAmount());
							nonIGst=nonIGst.subtract(invVO.getTotalIGST());
							nonCGst=nonCGst.subtract(invVO.getTotalCGST());
							nonSGst=nonSGst.subtract(invVO.getTotalSGST());
							nonUGst=nonUGst.subtract(invVO.getTotalUGST());
							nonCess=nonCess.subtract(invVO.getTotalCess());
							
						}else if(invVO.getInvoiceType().equalsIgnoreCase("Sales")){
							nonTaxableValue=nonTaxableValue.add(invVO.getInvoiceAmount());
							nonIGst=nonIGst.add(invVO.getTotalIGST());
							nonCGst=nonCGst.add(invVO.getTotalCGST());
							nonSGst=nonSGst.add(invVO.getTotalSGST());
							nonUGst=nonUGst.add(invVO.getTotalUGST());
							nonCess=nonCess.add(invVO.getTotalCess());
						}
				}
					
					supplyNonGst.setTxval(nonTaxableValue);
					supplyNonGst.setCamt(nonCGst);
					supplyNonGst.setCsamt(nonCess);
					supplyNonGst.setIamt(nonIGst);
					supplyNonGst.setSamt(nonSGst);
				}
				
				supObj.setOsup_nongst(supplyNonGst);
				
		} catch (Exception e) {
			logger.debug("Exception at getHSNReport(int orgID,String fromDate,String toDate) "+e);
		}
		
		logger.debug("Exit :public Hsn getHSNReport(int orgID,String fromDate,String toDate)");
		return supObj;
	}
	

public Itc_elg getItcElgReport(int orgID,String fromDate,String toDate){
	
	logger.debug("Entry :public Itc_elg getItcElgReport(int orgID,String fromDate,String toDate)");
	List supplyList=null;
	Itc_elg itcElgObj = new Itc_elg();
	Itc_avl itcAvlOth =new Itc_avl();
	Itc_avl itcAvlImpg =new Itc_avl();
	Itc_avl itcAvlImps =new Itc_avl();
	Itc_avl itcAvlIsrc =new Itc_avl();
	Itc_avl itcAvlISD=new Itc_avl();
	Itc_inelg itcInelgOther=new Itc_inelg();
	List<Itc_inelg> itcInElgList=new ArrayList<Itc_inelg>();
	List<Itc_avl> itcList=new ArrayList<Itc_avl>();
	CommonUtility commonUtils=new CommonUtility();
	
	try {
		SocietyVO societyVO=societyService.getSocietyDetails(orgID);
		
		List importGoodsInvList=gstReportDAO.getInvoiceLineItemsListForITCElgForImportReport(orgID,fromDate,toDate,25);
		BigDecimal impGoodsIGST=BigDecimal.ZERO;
		BigDecimal impGoodsCess=BigDecimal.ZERO;
		if(importGoodsInvList.size()>0){
			for(int i=0;importGoodsInvList.size()>i;i++){
				InLineItemsVO lineItem=(InLineItemsVO) importGoodsInvList.get(i);
				
					impGoodsIGST=impGoodsIGST.add(lineItem.getiGST());
					impGoodsCess=impGoodsCess.add(lineItem.getCess());
				
							}
					
			}
		itcAvlImpg.setTy("IMPG");
		itcAvlImpg.setIamt(impGoodsIGST);
		itcAvlImpg.setCsamt(impGoodsCess);
		itcList.add(itcAvlImpg);
		
		
		List importServInvList=gstReportDAO.getInvoiceLineItemsListForITCElgForImportReport(orgID,fromDate,toDate,24);
		BigDecimal impSerIGST=BigDecimal.ZERO;
		BigDecimal impSerCess=BigDecimal.ZERO;
		if(importServInvList.size()>0){
			for(int i=0;importServInvList.size()>i;i++){
				InLineItemsVO lineItem=(InLineItemsVO) importServInvList.get(i);
				
				impSerIGST=impSerIGST.add(lineItem.getiGST());
				impSerCess=impSerCess.add(lineItem.getCess());
				
							}
					
			}
		itcAvlImps.setTy("IMPS");
		itcAvlImps.setIamt(impGoodsIGST);
		itcAvlImps.setCsamt(impGoodsCess);
		itcList.add(itcAvlImps);
		
		
		List reverseList=gstReportDAO.getInvoiceListForReverseChargeDetailsReport(orgID, fromDate, toDate,0);
		BigDecimal reverseIGst=BigDecimal.ZERO;
		BigDecimal reverseCGst=BigDecimal.ZERO;
		BigDecimal reverseSGst=BigDecimal.ZERO;
		BigDecimal reverseUGst=BigDecimal.ZERO;
		BigDecimal reverseCess=BigDecimal.ZERO;
		if(reverseList.size()>0){
			for(int i=0;reverseList.size()>i;i++){
				InvoiceVO inv=(InvoiceVO) reverseList.get(i);
				
				reverseIGst=reverseIGst.add(inv.getTotalIGST());
				reverseCGst=reverseCGst.add(inv.getTotalCGST());
				reverseSGst=reverseSGst.add(inv.getTotalSGST());
				reverseCess=reverseCess.add(inv.getTotalCess());
				reverseUGst=reverseUGst.add(inv.getTotalUGST());
				
				
							}
					
			}
		itcAvlIsrc.setTy("ISRC");
		itcAvlIsrc.setIamt(reverseIGst);
		itcAvlIsrc.setCsamt(reverseCess);
		itcAvlIsrc.setCamt(reverseCGst);
		itcAvlIsrc.setSamt(reverseSGst);
		itcList.add(itcAvlIsrc);
		
		
		List ISDInvList=gstReportDAO.getInvoiceLineItemsListForITCElgForImportReport(orgID,fromDate,toDate,30);
		BigDecimal isdIGST=BigDecimal.ZERO;
		BigDecimal isdCess=BigDecimal.ZERO;
		BigDecimal isdSGST=BigDecimal.ZERO;
		BigDecimal isdCGst=BigDecimal.ZERO;
		BigDecimal isdUGst=BigDecimal.ZERO;
		if(ISDInvList.size()>0){
			for(int i=0;ISDInvList.size()>i;i++){
				InLineItemsVO lineItem=(InLineItemsVO) ISDInvList.get(i);
				
				isdIGST=isdIGST.add(lineItem.getiGST());
				isdCess=isdCess.add(lineItem.getCess());
				isdSGST=isdSGST.add(lineItem.getsGST().add(lineItem.getuGST()));
				isdCGst=isdCGst.add(lineItem.getcGST());
				
				
							}
					
			}
		itcAvlISD.setTy("ISD");
		itcAvlISD.setIamt(isdIGST);
		itcAvlISD.setCsamt(isdCess);
		itcAvlISD.setSamt(isdSGST);
		itcAvlISD.setCamt(isdCGst);
		
		itcList.add(itcAvlISD);
		
		List goodsList=gstReportDAO.getInvoiceLineItemsListForITCElgReport(orgID, fromDate, toDate);
		BigDecimal goodsGst=BigDecimal.ZERO;
		BigDecimal goodsCess=BigDecimal.ZERO;
		BigDecimal serviceGst=BigDecimal.ZERO;
		BigDecimal serviceCess=BigDecimal.ZERO;
		BigDecimal otherIGst=BigDecimal.ZERO;
		BigDecimal otherCGst=BigDecimal.ZERO;
		BigDecimal otherSGst=BigDecimal.ZERO;
		BigDecimal otherUGst=BigDecimal.ZERO;
		BigDecimal otherCess=BigDecimal.ZERO;
		if(goodsList.size()>0){
			for(int i=0;goodsList.size()>i;i++){
				InLineItemsVO lineItem=(InLineItemsVO) goodsList.get(i);
				
				
					otherIGst=otherIGst.add(lineItem.getiGST());
					otherCGst=otherCGst.add(lineItem.getcGST());
					otherSGst=otherSGst.add(lineItem.getsGST());
					otherUGst=otherUGst.add(lineItem.getuGST());
					otherCess=otherCess.add(lineItem.getCess());
					
			}
			
			
		}
		itcAvlOth.setTy("OTH");
		itcAvlOth.setIamt(otherIGst);
		itcAvlOth.setCamt(otherCGst);
		itcAvlOth.setSamt(otherSGst);
		itcAvlOth.setCsamt(otherCess);
		itcList.add(itcAvlOth);
		itcElgObj.setItcAvl(itcList);
		
		List inElgblInvList=gstReportDAO.getInvoiceLineItemsListForITCInElgForImportReport(orgID,fromDate,toDate,6);
		BigDecimal inElgIGST=BigDecimal.ZERO;
		BigDecimal inElgCess=BigDecimal.ZERO;
		BigDecimal inElgCGst=BigDecimal.ZERO;
		BigDecimal inElgSGst=BigDecimal.ZERO;
		if(inElgblInvList.size()>0){
			for(int i=0;inElgblInvList.size()>i;i++){
				InLineItemsVO lineItem=(InLineItemsVO) inElgblInvList.get(i);
				
				inElgIGST=inElgIGST.add(lineItem.getiGST());
				inElgCess=inElgCess.add(lineItem.getCess());
				inElgCGst=inElgCGst.add(lineItem.getcGST());
				inElgSGst=inElgSGst.add(lineItem.getsGST());
				
							}
					
			}
		itcInelgOther.setTy("OTH");
		itcInelgOther.setIamt(inElgIGST);
		itcInelgOther.setCsamt(inElgCess);
		itcInelgOther.setCamt(inElgCGst);
		itcInelgOther.setSamt(inElgSGst);
		itcInElgList.add(itcInelgOther);
		itcElgObj.setItcInelg(itcInElgList);
		
	} catch (Exception e) {
		logger.debug("Exception at Itc_elg getItcElgReport(int orgID,String fromDate,String toDate) "+e);
	}
	
	logger.debug("Exit :public Itc_elg getItcElgReport(int orgID,String fromDate,String toDate)");
	return itcElgObj;
}


public Inward_sup getInwardSupplyReport(int orgID,String fromDate,String toDate){
	
	logger.debug("Entry : public Inward_sup getInwardSupplyReport(int orgID,String fromDate,String toDate)");
	List supplyList=new ArrayList<>();
	Inward_sup inwardSpplyObj = new Inward_sup();
	Isup_detail isupNilExp=new Isup_detail();
	Isup_detail isupNonGst=new Isup_detail();
	
	try {
		SocietyVO societyVO=societyService.getSocietyDetails(orgID);

		List nilInvList=gstReportDAO.getInvoiceLineItemsListForInwardExpSupplyReport(orgID, fromDate, toDate);
		BigDecimal intrTxVal=BigDecimal.ZERO;
		BigDecimal intraTxVal=BigDecimal.ZERO;
		if(nilInvList.size()>0){
			for(int i=0;nilInvList.size()>i;i++){
				InvoiceVO inv=(InvoiceVO) nilInvList.get(i);
				
				
				if(societyVO.getStateName().equalsIgnoreCase(inv.getSupplyState())){
					if(inv.getInvoiceType().equalsIgnoreCase("Credit Note"))
						intraTxVal=intraTxVal.subtract(inv.getTotalAmount());
						else
						intraTxVal=intraTxVal.add(inv.getTotalAmount());
				}else{
					if(inv.getInvoiceType().equalsIgnoreCase("Credit Note"))
						intrTxVal=intrTxVal.subtract(inv.getTotalAmount());
						else
					intrTxVal=intrTxVal.add(inv.getTotalAmount());
				}
				
					
					
			}
			
			
		}
		isupNilExp.setTy("From a supplier under composition scheme, Exempt  and Nil rated supply");
		isupNilExp.setInter(intrTxVal);
		isupNilExp.setIntra(intraTxVal);
		supplyList.add(isupNilExp);
		
		
		List nonGstInvList=gstReportDAO.getInvoiceLineItemsListForNonGstInwardSupplyReport(orgID, fromDate, toDate);
		BigDecimal nonGstIntrTxVal=BigDecimal.ZERO;
		BigDecimal nonGstIntraTxVal=BigDecimal.ZERO;
		if(nonGstInvList.size()>0){
			for(int i=0;nonGstInvList.size()>i;i++){
				InvoiceVO inv=(InvoiceVO) nonGstInvList.get(i);
				
				if(societyVO.getStateName().equalsIgnoreCase(inv.getSupplyState())){
					if(inv.getInvoiceType().equalsIgnoreCase("Credit Note"))
						nonGstIntraTxVal=nonGstIntraTxVal.subtract(inv.getTotalAmount());
						else
					nonGstIntraTxVal=nonGstIntraTxVal.add(inv.getTotalAmount());
				}else{
					if(inv.getInvoiceType().equalsIgnoreCase("Credit Note"))
						nonGstIntrTxVal=nonGstIntrTxVal.subtract(inv.getTotalAmount());
						else
					nonGstIntrTxVal=nonGstIntrTxVal.add(inv.getTotalAmount());
				}
				
					
					
			}
			
			
		}
		isupNonGst.setTy("NONGST");
		isupNonGst.setInter(nonGstIntrTxVal);
		isupNonGst.setIntra(nonGstIntraTxVal);
		supplyList.add(isupNonGst);
	
		inwardSpplyObj.setIsupDetails(supplyList);
			
	} catch (Exception e) {
		logger.debug("Exception at public Inward_sup getInwardSupplyReport(int orgID,String fromDate,String toDate) "+e);
	}
	
	logger.debug("Exit : public Inward_sup getInwardSupplyReport(int orgID,String fromDate,String toDate)");
	return inwardSpplyObj;
}


public Intr_ltfee getInterestLateFeeReport(int orgID,String fromDate,String toDate){
	
	logger.debug("Entry : public Intr_ltfee getInterestLateFeeReport(int orgID,String fromDate,String toDate)");
	List supplyList=null;
	Intr_ltfee intrLateFeeObj = null;
	
	try {
		SocietyVO societyVO=societyService.getSocietyDetails(orgID);

		
			
	} catch (Exception e) {
		logger.debug("Exception at public Intr_ltfee getInterestLateFeeReport(int orgID,String fromDate,String toDate) "+e);
	}
	
	logger.debug("Exit : public Intr_ltfee getInterestLateFeeReport(int orgID,String fromDate,String toDate)");
	return intrLateFeeObj;
}

public Inter_sup getInterStateSupplyReport(int orgID,String fromDate,String toDate){
	
	logger.debug("Entry : public Inter_sup getInterStateSupplyReport(int orgID,String fromDate,String toDate)");
	List supplyList=null;
	Inter_sup itrSupObj = new Inter_sup();
	
	try {
		SocietyVO societyVO=societyService.getSocietyDetails(orgID);
		GstStateDetails orgGstState=gstReportDAO.getGstStateDetailsFromName(societyVO.getStateName());
		
		List<Unreg_details> unregList=new ArrayList<Unreg_details>();
		List nonGstInvList=gstReportDAO.getInvoiceListForNonGstInwardSupplyReport(orgID, fromDate, toDate);
		BigDecimal nonGstTxVal=BigDecimal.ZERO;
		BigDecimal nonGstGstVal=BigDecimal.ZERO;		
		if(nonGstInvList.size()>0){
			for(int i=0;nonGstInvList.size()>i;i++){
				InvoiceVO inv=(InvoiceVO) nonGstInvList.get(i);
				Unreg_details unreg=new Unreg_details();				
				if(orgGstState.getGstStateID().equalsIgnoreCase(inv.getSupplyStateID())){
					nonGstInvList.remove(i);
					--i;
				}else{
					
					unreg.setPos(inv.getSupplyStateID());
					unreg.setTxval(inv.getTotalAmount());
					unreg.setIamt(inv.getTotalIGST());
					unregList.add(unreg);
					
				}
				
					
					
			}
		}else{
			Unreg_details unreg=new Unreg_details();
			unreg.setPos("0");
			unreg.setTxval(BigDecimal.ZERO);
			unreg.setIamt(BigDecimal.ZERO);
			unregList.add(unreg);
			
		}
		itrSupObj.setUnregDetails(unregList);
		
		List<Comp_details> compList=new ArrayList<Comp_details>();
		List compGstInvList=gstReportDAO.getInvoiceListForGstInwardSupplyReportStateWise(orgID, fromDate, toDate,28);
		
		if(compGstInvList.size()>0){
			for(int i=0;compGstInvList.size()>i;i++){
				InvoiceVO inv=(InvoiceVO) compGstInvList.get(i);
				Comp_details compDet=new Comp_details();
				
				if(orgGstState.getGstStateID()==inv.getSupplyStateID()){
					nonGstInvList.remove(i);
					--i;
				}else{
					
					compDet.setPos(""+inv.getSupplyStateID());
					compDet.setTxval(inv.getTotalAmount());
					compDet.setIamt(inv.getTotalIGST());
					compList.add(compDet);
					
				}
				
					
					
			}
		}else{
			Comp_details compDet=new Comp_details();
			compDet.setPos("0");
			compDet.setTxval(BigDecimal.ZERO);
			compDet.setIamt(BigDecimal.ZERO);
			compList.add(compDet);
			
		}
		itrSupObj.setCompDetails(compList);
		
		List<Uin_details> uinList=new ArrayList<Uin_details>();
		List uinGstInvList=gstReportDAO.getInvoiceListForGstInwardSupplyReportStateWise(orgID, fromDate, toDate,29);
		
		if(uinGstInvList.size()>0){
			for(int i=0;uinGstInvList.size()>i;i++){
				InvoiceVO inv=(InvoiceVO) uinGstInvList.get(i);
				Uin_details uinDet=new Uin_details();
				
				if(orgGstState.getGstStateID()==inv.getSupplyStateID()){
					nonGstInvList.remove(i);
					--i;
				}else{
					
					uinDet.setPos(""+inv.getSupplyStateID());
					uinDet.setTxval(inv.getTotalAmount());
					uinDet.setIamt(inv.getTotalIGST());
					uinList.add(uinDet);
					
				}
				
					
					
			}
		}else{
			Uin_details uinDet=new Uin_details();
			uinDet.setPos("0");
			uinDet.setTxval(BigDecimal.ZERO);
			uinDet.setIamt(BigDecimal.ZERO);
			uinList.add(uinDet);
			
		}
		itrSupObj.setUinDetails(uinList);
		
		
		

		
			
	} catch (Exception e) {
		logger.debug("Exception at public Inter_sup getInterStateSupplyReport(int orgID,String fromDate,String toDate) "+e);
	}
	
	logger.debug("Exit : public Inter_sup getInterStateSupplyReport(int orgID,String fromDate,String toDate)");
	return itrSupObj;
}

public GstRequestVO getGstOtpRequest(GstRequestVO gstReqVO){
	GstTax gstVO=new GstTax();
	GstRequestVO gstR=new GstRequestVO();
	ObjectMapper mapper = new ObjectMapper();
	logger.debug("Entry :  public GstTax getGstOtpRequest(GstRequestVO gstReqVO)");
	ConfigManager c=new ConfigManager();
	String url=c.getPropertiesValue("gst.apiUrl");
	String uri = c.getPropertiesValue("gst.apiUrl")+"/taxpayerapi/dec/v0.2/authenticate?action=OTPREQUEST&aspid="+c.getPropertiesValue("gst.aspID")+"&password="+c.getPropertiesValue("gst.password")+"&gstin="+gstReqVO.getGstIN()+"&username="+gstReqVO.getUserName()+"";
    
    RestTemplate restTemplate = new RestTemplate();
     
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
     
    ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
    
    System.out.println(result);
    if(result.getStatusCodeValue()==200);
    try {
		gstR=mapper.readValue(result.getBody(), GstRequestVO.class);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
   
	
	
	
	logger.debug("Exit :  public GstTax getGstOtpRequest(GstRequestVO gstReqVO)");
	return gstR;
}


public GstRequestVO getAuthToken(GstRequestVO gstReqVO){
	GstTax gstVO=new GstTax();
	GstRequestVO gstR=new GstRequestVO();
	ObjectMapper mapper = new ObjectMapper();
	logger.debug("Entry :  public GstTax getAuthToken(GstRequestVO gstReqVO)");
	ConfigManager conf=new ConfigManager();

	final String uri = conf.getPropertiesValue("gst.apiUrl")+"/taxpayerapi/dec/v0.2/authenticate?action=AUTHTOKEN&aspid="+conf.getPropertiesValue("gst.aspID")+"&password="+conf.getPropertiesValue("gst.password")+"&gstin="+gstReqVO.getGstIN()+"&username="+gstReqVO.getUserName()+"&OTP="+gstReqVO.getOtp();
    
    RestTemplate restTemplate = new RestTemplate();
     
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
     
    ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
    
    System.out.println(result);
    if(result.getStatusCodeValue()==200);
    try {
		gstR=mapper.readValue(result.getBody(), GstRequestVO.class);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
   
	
	
	
	logger.debug("Exit :  public GstTax getAuthToken(GstRequestVO gstReqVO)");
	return gstR;
}

//========== GSTR2A B2B Report===========//

public  GstRequestVO getGSTR2AB2Breport(GstRequestVO gstReqVO){
	GstTax gstVO=new GstTax();
	GstRequestVO gstR=new GstRequestVO();
	ObjectMapper mapper = new ObjectMapper();
	logger.debug("Entry :  public  GstRequestVO getGSTR2AB2Breport(GstRequestVO gstReqVO)");
	
	try {
		ConfigManager conf=new ConfigManager();

		final String uri = conf.getPropertiesValue("gst.apiUrl")+"/taxpayerapi/dec/v0.3/returns/gstr2a?action=B2B&aspid="+conf.getPropertiesValue("gst.aspID")+"&password="+conf.getPropertiesValue("gst.password")+"&gstin="+gstReqVO.getGstIN()+"&username="+gstReqVO.getUserName()+"&authtoken="+gstReqVO.getAuth_token()+"&ret_period="+gstReqVO.getRet_period();
		
		RestTemplate restTemplate = new RestTemplate();
		 
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		 
		ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
		
		
		System.out.println(result);
		if(result.getStatusCodeValue()==200)
		try {
			gstR=mapper.readValue(result.getBody(), GstRequestVO.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} catch (Exception e) {
		gstR.setStatusCode(400);
		logger.info("No Invoices found  "+e);
		
	}
   
	
	
	
	logger.debug("Exit :  public  GstRequestVO getGSTR2AB2Breport(GstRequestVO gstReqVO)");
	return gstR;
}


public  GstRequestVO getCashLedgersreport(GstRequestVO gstReqVO){
	GstTax gstVO=new GstTax();
	GstRequestVO gstR=new GstRequestVO();
	ObjectMapper mapper = new ObjectMapper();
	logger.debug("Entry :  public  B2b getCashLedgersreport(GstRequestVO gstReqVO)");
	ConfigManager conf=new ConfigManager();

	final String uri = conf.getPropertiesValue("gst.apiUrl")+"/taxpayerapi/dec/v0.3/ledgers?action=CASH&aspid="+conf.getPropertiesValue("gst.aspID")+"&password="+conf.getPropertiesValue("gst.password")+"&gstin="+gstReqVO.getGstIN()+"&username="+gstReqVO.getUserName()+"&authtoken="+gstReqVO.getAuth_token()+"&fr_dt="+gstReqVO.getFromDate()+"&to_dt="+gstReqVO.getToDate();
    
    RestTemplate restTemplate = new RestTemplate();
     
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
     
    ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
    
    
    System.out.println(result);
    if(result.getStatusCodeValue()==200)
    try {
		gstR=mapper.readValue(result.getBody(), GstRequestVO.class);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
   
	
	
	
	logger.debug("Exit :  public  B2b getCashLedgersreport(GstRequestVO gstReqVO)");
	return gstR;
}
	
public  GstRequestVO saveGSTR1Report(GstRequestVO gstReqVO){
	GstTax gstVO=new GstTax();
	GstRequestVO gstR=new GstRequestVO();
	ObjectMapper mapper = new ObjectMapper();
	logger.debug("Entry :  public  GstRequestVO saveGSTR1Report(GstRequestVO gstReqVO)");
	
	try {
		ConfigManager conf=new ConfigManager();

	//	final String uri = conf.getPropertiesValue("gst.apiUrl")+"/taxpayerapi/dec/v1.1/returns/gstr1?action=RETSAVE&aspid="+conf.getPropertiesValue("gst.aspID")+"&password="+conf.getPropertiesValue("gst.password")+"&gstin="+gstReqVO.getGstIN()+"&username="+gstReqVO.getUserName()+"&authtoken="+gstReqVO.getAuth_token()+"&ret_period="+gstReqVO.getRet_period();
		final String uri = "http://testapi.taxprogsp.co.in/taxpayerapi/dec/v1.1/returns/gstr1?action=RETSAVE&aspid="+gstReqVO.getAspID()+"&password="+gstReqVO.getPassword()+"&gstin="+gstReqVO.getGstIN()+"&username="+gstReqVO.getUserName()+"&authtoken="+gstReqVO.getAuth_token()+"&ret_period="+gstReqVO.getRet_period();
		
		/*RestTemplate restTemplate = new RestTemplate();
		 
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		 
		ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);*/
		
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		Map map = new HashMap<String, String>();
		map.put("Content-Type", "application/json");

		headers.setAll(map);
		
   gstVO=getGST1Report(gstReqVO);
   
  String gstR1 = null;
try {
		gstR1 = mapper.writeValueAsString(gstVO);
} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
}
//gstR1=gstReqVO.getGstReport();

  HttpEntity<?> requestEmail = new HttpEntity<>(gstR1, headers);

		ResponseEntity<String> result = new RestTemplate().exchange(uri, HttpMethod.PUT, requestEmail, String.class);
		HttpStatus status = result.getStatusCode();
		
		
		System.out.println(result);
		if(result.getStatusCodeValue()==200);
		try {
			gstR=mapper.readValue(result.getBody(), GstRequestVO.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} catch (Exception e) {
		logger.error("Exception : public  GstRequestVO saveGSTR1Report(GstRequestVO gstReqVO)  "+e);
	}
   
	
	
	
	logger.debug("Exit :  public  GstRequestVO saveGSTR1Report(GstRequestVO gstReqVO)");
	return gstR;
}

public  GstRequestVO submitGSTR1Report(GstRequestVO gstReqVO){
	GstTax gstVO=new GstTax();
	GstRequestVO gstR=new GstRequestVO();
	ObjectMapper mapper = new ObjectMapper();
	logger.debug("Entry :  public  GstRequestVO submitGSTR1Report(GstRequestVO gstReqVO)");
	
	try {
		ConfigManager conf=new ConfigManager();

		final String uri = conf.getPropertiesValue("gst.apiUrl")+"/taxpayerapi/dec/v1.1/returns/gstr1?action=RETSUBMIT&aspid="+conf.getPropertiesValue("gst.aspID")+"&password="+conf.getPropertiesValue("gst.password")+"&gstin="+gstReqVO.getGstIN()+"&username="+gstReqVO.getUserName()+"&authtoken="+gstReqVO.getAuth_token()+"&ret_period="+gstReqVO.getRet_period();
		
		/*RestTemplate restTemplate = new RestTemplate();
		 
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		 
		ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);*/
		
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		Map map = new HashMap<String, String>();
		map.put("Content-Type", "application/json");

		headers.setAll(map);
		
   gstVO=getGST1Report(gstReqVO);
   
  String gstR1 = null;
try {
		gstR1 = mapper.writeValueAsString(gstVO);
} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
}
//gstR1=gstReqVO.getGstReport();

  HttpEntity<?> requestEmail = new HttpEntity<>(gstR1, headers);

		ResponseEntity<String> result = new RestTemplate().exchange(uri, HttpMethod.POST, requestEmail, String.class);
		HttpStatus status = result.getStatusCode();
		
		
		System.out.println(result);
		if(result.getStatusCodeValue()==200);
		try {
			gstR=mapper.readValue(result.getBody(), GstRequestVO.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} catch (Exception e) {
		logger.error("Exception : public  GstRequestVO submitGSTR1Report(GstRequestVO gstReqVO)  "+e);
	}
   
	
	
	
	logger.debug("Exit :  public  GstRequestVO submitGSTR1Report(GstRequestVO gstReqVO)");
	return gstR;
}

public  GstRequestVO retFileGSTR1Report(GstRequestVO gstReqVO){
	GstTax gstVO=new GstTax();
	GstRequestVO gstR=new GstRequestVO();
	ObjectMapper mapper = new ObjectMapper();
	logger.debug("Entry :  public  GstRequestVO retFileGSTR1Report(GstRequestVO gstReqVO)");
	
	try {
		ConfigManager conf=new ConfigManager();

		final String uri = conf.getPropertiesValue("gst.apiUrl")+"/taxpayerapi/dec/v1.1/returns/gstr1?action=RETFILE&aspid="+conf.getPropertiesValue("gst.aspID")+"&password="+conf.getPropertiesValue("gst.password")+"&gstin="+gstReqVO.getGstIN()+"&username="+gstReqVO.getUserName()+"&authtoken="+gstReqVO.getAuth_token()+"&ret_period="+gstReqVO.getRet_period();
		
		/*RestTemplate restTemplate = new RestTemplate();
		 
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		 
		ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);*/
		
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		Map map = new HashMap<String, String>();
		map.put("Content-Type", "application/json");

		headers.setAll(map);
		
   gstVO=getGST1Report(gstReqVO);
   
  String gstR1 = null;
try {
		gstR1 = mapper.writeValueAsString(gstVO);
} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
}
//gstR1=gstReqVO.getGstReport();

  HttpEntity<?> requestEmail = new HttpEntity<>(gstR1, headers);

		ResponseEntity<String> result;
		try {
			result = new RestTemplate().exchange(uri, HttpMethod.POST, requestEmail, String.class);
			

			System.out.println(result);
			if(result.getStatusCodeValue()==200);
			try {
				gstR=mapper.readValue(result.getBody(), GstRequestVO.class);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
	} catch (Exception e) {
		logger.error("Exception : public  GstRequestVO retFileGSTR1Report(GstRequestVO gstReqVO)  "+e);
	}
   
	
	
	
	logger.debug("Exit :  public  GstRequestVO retFileGSTR1Report(GstRequestVO gstReqVO)");
	return gstR;
}

public  GstRequestVO getGstr1Status(GstRequestVO gstReqVO){
	GstTax gstVO=new GstTax();
	GstRequestVO gstR=new GstRequestVO();
	ObjectMapper mapper = new ObjectMapper();
	logger.debug("Entry :  public  GstRequestVO getGstr1Status(GstRequestVO gstReqVO)");
	ConfigManager conf=new ConfigManager();

	final String uri = conf.getPropertiesValue("gst.apiUrl")+"/taxpayerapi/dec/v1.1/returns/gstr1?action=RETSTATUS&aspid="+conf.getPropertiesValue("gst.aspID")+"&password="+conf.getPropertiesValue("gst.password")+"&gstin="+gstReqVO.getGstIN()+"&username="+gstReqVO.getUserName()+"&authtoken="+gstReqVO.getAuth_token()+"&ret_period="+gstReqVO.getRet_period()+"&ref_id="+gstReqVO.getReference_id();
    
    RestTemplate restTemplate = new RestTemplate();
     
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
     
    ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
    
    
    System.out.println(result);
    if(result.getStatusCodeValue()==200);
    try {
		gstR=mapper.readValue(result.getBody(), GstRequestVO.class);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
   
	logger.debug("Exit :  public  GstRequestVO getGstr1Status(GstRequestVO gstReqVO)");
	return gstR;
}
//========== GSTR1 B2B Report===========//
public  GstRequestVO getGSTR1B2Breport(GstRequestVO gstReqVO){
	GstTax gstVO=new GstTax();
	GstRequestVO gstR=new GstRequestVO();
	ObjectMapper mapper = new ObjectMapper();
	logger.debug("Entry :  public  GstRequestVO getGSTR1B2Breport(GstRequestVO gstReqVO)");
	ConfigManager conf=new ConfigManager();

	final String uri = conf.getPropertiesValue("gst.apiUrl")+"/taxpayerapi/dec/v1.1/returns/gstr1?action=B2B&aspid="+conf.getPropertiesValue("gst.aspID")+"&password="+conf.getPropertiesValue("gst.password")+"&gstin="+gstReqVO.getGstIN()+"&username="+gstReqVO.getUserName()+"&authtoken="+gstReqVO.getAuth_token()+"&ret_period="+gstReqVO.getRet_period();
    
    RestTemplate restTemplate = new RestTemplate();
     
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
     
    ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
    
    
  
    if(result.getStatusCodeValue()==200)
    	System.out.println(result);
    try {
		gstR=mapper.readValue(result.getBody(), GstRequestVO.class);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
	
	logger.debug("Exit :  public  GstRequestVO getGSTR1B2Breport (GstRequestVO gstReqVO)");
	return gstR;
}


public  GstRequestVO saveGSTR3BReport(GstRequestVO gstReqVO){
	Gstr3B gstVO=new Gstr3B();
	GstRequestVO gstR=new GstRequestVO();
	ObjectMapper mapper = new ObjectMapper();
	logger.debug("Entry :  public  GstRequestVO saveGSTR3BReport(GstRequestVO gstReqVO)");
	
	try {
		ConfigManager conf=new ConfigManager();

		final String uri = conf.getPropertiesValue("gst.apiUrl")+"/taxpayerapi/dec/v0.3/returns/gstr3b?action=RETSAVE&aspid="+conf.getPropertiesValue("gst.aspID")+"&password="+conf.getPropertiesValue("gst.password")+"&gstin="+gstReqVO.getGstIN()+"&username="+gstReqVO.getUserName()+"&authtoken="+gstReqVO.getAuth_token()+"&ret_period="+gstReqVO.getRet_period();
		
				
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		Map map = new HashMap<String, String>();
		map.put("Content-Type", "application/json");

		headers.setAll(map);
		
   gstVO=getGSTR3BReport(gstReqVO);
   
  String gstR3B = null;
try {
		gstR3B = mapper.writeValueAsString(gstVO);
} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
}
//gstR3B=gstReqVO.getGstReport();

  HttpEntity<?> requestEmail = new HttpEntity<>(gstR3B, headers);

		ResponseEntity<String> result = new RestTemplate().exchange(uri, HttpMethod.PUT, requestEmail, String.class);
		HttpStatus status = result.getStatusCode();
		
		
		System.out.println(result);
		if(result.getStatusCodeValue()==200);
		try {
			gstR=mapper.readValue(result.getBody(), GstRequestVO.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} catch (Exception e) {
		logger.error("Exception : public  GstRequestVO saveGSTR3BReport(GstRequestVO gstReqVO)  "+e);
	}
   
	
	
	
	logger.debug("Exit :  public  GstRequestVO saveGSTR3BReport(GstRequestVO gstReqVO)");
	return gstR;
}

//============== Public GST APIs =================//

public  GstRequestVO getGstInInfoReport(GstRequestVO gstReqVO){
	GstINInfo gstVO=new GstINInfo();
	GstRequestVO gstR=new GstRequestVO();
	ObjectMapper mapper = new ObjectMapper();
	logger.debug("Entry :  public  GstRequestVO getGstInInfoReport(GstRequestVO gstReqVO)");
	ConfigManager conf=new ConfigManager();

	final String uri = conf.getPropertiesValue("gst.apiUrl")+"/commonapi/v1.1/search?aspid="+conf.getPropertiesValue("gst.aspID")+"&password="+conf.getPropertiesValue("gst.password")+"&ACTION=TP&gstin="+gstReqVO.getGstIN();
    
    RestTemplate restTemplate = new RestTemplate();
     
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
     
    ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
    
    
    System.out.println(result);
    if(result.getStatusCodeValue()==200);
    try {
		gstVO=mapper.readValue(result.getBody(), GstINInfo.class);
		gstR.setGstInInfo(gstVO);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
   
	logger.debug("Exit :  public  GstRequestVO getGstInInfoReport(GstRequestVO gstReqVO)");
	return gstR;
}

public  GstRequestVO getGstRFiledHistoryReport(GstRequestVO gstReqVO){
	GstrFileHistory gstVO=new GstrFileHistory();
	EFiledlist efiled=new EFiledlist();
	GstRequestVO gstR=new GstRequestVO();
	com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper();
	mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);	logger.debug("Entry :  public  GstRequestVO getGstRFiledHistoryReport(GstRequestVO gstReqVO)");
	ConfigManager conf=new ConfigManager();

	final String uri = conf.getPropertiesValue("gst.apiUrl")+"/commonapi/v1.0/returns?aspid="+conf.getPropertiesValue("gst.aspID")+"&password="+conf.getPropertiesValue("gst.password")+"&Action=RETTRACK&gstin="+gstReqVO.getGstIN()+"&FY="+gstReqVO.getFinancialYear();
    
    RestTemplate restTemplate = new RestTemplate();
     
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
     
    ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
    
    
    System.out.println(result);
    if(result.getStatusCodeValue()==200);
    try {
    	gstVO=mapper.readValue(result.getBody(), GstrFileHistory.class);
    	/*List<EFiledlist> efiledList=new ArrayList<>();
    	efiledList.add(efiled);
		gstVO.setEFiledlist(efiledList);*/
		gstR.setGstrFileHistory(gstVO);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
   
	logger.debug("Exit :  public  GstRequestVO getGstRFiledHistoryReport(GstRequestVO gstReqVO)");
	return gstR;
}

	/**
	 * @param gstReportDAO the gstReportDAO to set
	 */
	public void setGstReportDAO(GSTReportsDAO gstReportDAO) {
		this.gstReportDAO = gstReportDAO;
	}
	


	/**
	 * @param societyService the societyService to set
	 */
	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}
	



	
}
