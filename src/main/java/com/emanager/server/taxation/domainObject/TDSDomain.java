package com.emanager.server.taxation.domainObject;

import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.accounts.DataAccessObjects.LedgerEntries;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.taxation.DAO.TdsDAO;

public class TDSDomain {
	
	private static final Logger logger = Logger.getLogger(TDSDomain.class);
	
	TdsDAO tdsDAO;
	
	TransactionService transactionService;
	
	
	public TransactionVO insertTDSTransaction(TransactionVO txVO,TransactionVO tdsVO,int societyID){
		int tdsflag=0;
		int expenseFlag=0;
		int successFlag=0;
		List ledgerEntry=txVO.getLedgerEntries();
		try{
		logger.debug("Entry : public int insertTDSTransaction(TransactionVO txVO)");
		
		tdsVO=transactionService.addTransaction(tdsVO, societyID);
		
		if(tdsflag!=0){
			txVO.setDocID(""+tdsVO.getTransactionID());
			txVO=transactionService.addTransaction(txVO, societyID);
			
		}
		
		if((tdsflag!=0)&&(expenseFlag!=0)){
			tdsVO.setStatusCode(200);
		}
		
		
		logger.debug("Exit : public int insertTDSTransaction(TransactionVO txVO)"+successFlag);	
		}catch(Exception Ex){
			
			logger.error("Exception in insertTDSTransaction : "+Ex);
		}
	return tdsVO;

    }


	public void setTdsDAO(TdsDAO tdsDAO) {
		this.tdsDAO = tdsDAO;
	}


	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

}
