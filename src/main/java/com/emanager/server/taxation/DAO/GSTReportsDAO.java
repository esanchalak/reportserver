package com.emanager.server.taxation.DAO;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.emanager.server.invoice.dataAccessObject.InLineItemsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceVO;
import com.emanager.server.taxation.dataAccessObject.gst.B2b;
import com.emanager.server.taxation.dataAccessObject.gst.B2c;
import com.emanager.server.taxation.dataAccessObject.gst.Cdnr;
import com.emanager.server.taxation.dataAccessObject.gst.Cdnra;
import com.emanager.server.taxation.dataAccessObject.gst.Datum;
import com.emanager.server.taxation.dataAccessObject.gst.GstStateDetails;


public class GSTReportsDAO {
	private static final Logger logger = Logger.getLogger(GSTReportsDAO.class);
	
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	public List getHSNReport(int orgID,String fromDate,String toDate){
		List hsnList=null;
		try{
		logger.debug("Entry : public List getHSNReport(int orgID,String fromDate,String toDate)");
		
		String sqlString="SELECT * FROM(SELECT a.id,a.invoice_type,a.invoice_date,h.item_code,h.description,r.name, SUM(i.quantity) AS qty,SUM(total_price) AS total_value,SUM(amount) AS total_price,SUM(igst) AS igst,SUM(cgst) AS cgst,SUM(sgst) AS sgst,SUM(i.cess) AS cess,r.unit_of_measurement,TRIM(i.hsn_sac_code) AS hsn_sac_code ,i.gst_percentage FROM invoice_details a,invoice_line_item_details i,product_service_legder_relation r,hsn_sac_master h WHERE r.hsn_sac_id=h.id AND i.hsn_sac_code IS NOT NULL "
				+ " AND  a.id=i.invoice_id AND i.gst_category_id=1 AND i.item_id=r.id AND a.invoice_date BETWEEN :fromDate AND :toDate AND a.is_deleted=0 AND a.org_id=:orgID  AND (a.invoice_type='Sales' or (a.invoice_type='Journal' and a.is_sales=1 )) AND a.is_review=0 "
				+ " GROUP BY hsn_sac_code) AS hsn  ";

		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		
		RowMapper RMapper = new RowMapper() {
			int i=1;
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				 
				Datum hsnVO=new Datum();
				hsnVO.setHsn_sc(rs.getString("item_code"));
				hsnVO.setNum(i);
				//hsnVO.setDesc(rs.getString("name"));
				hsnVO.setUqc(rs.getString("unit_of_measurement"));
				hsnVO.setQty(rs.getDouble("qty"));
				hsnVO.setTxval(rs.getDouble("total_value"));
				//hsnVO.setVal(rs.getDouble("total_price"));
				hsnVO.setIamt(rs.getDouble("igst"));
				hsnVO.setCamt(rs.getDouble("cgst"));
				hsnVO.setSamt(rs.getDouble("sgst"));
				hsnVO.setCsamt(rs.getDouble("cess"));
				hsnVO.setRt(rs.getDouble("gst_percentage"));
				++i;
				return hsnVO;
			}
		};
		
		hsnList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getHSNReport(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in getHSNReport : "+Ex);
		}
	return hsnList;

    }
	
	public List getExemptedReport(int orgID,String fromDate,String toDate){
		List invList=null;
		try{
		logger.debug("Entry : public List getExemptedReport(int orgID,String fromDate,String toDate)");
		
		String sqlString="SELECT * FROM( SELECT  i.item_id,i.id,i.invoice_type,i.invoice_date, SUM(i.qty) AS qty,SUM(i.total_value) AS total_value,SUM(i.total_price) AS total_price,SUM(i.igst) AS igst,SUM(i.cgst) AS cgst,SUM(i.sgst) AS sgst,SUM(i.cess) AS cess,i.gst_percentage,i.place_of_supply,i.entity_gstin,i.unit_of_measurement,i.gst_category_id FROM (SELECT i.item_id,a.id,a.invoice_type,a.invoice_date, SUM(i.quantity) AS qty,SUM(total_price) AS total_value,SUM(amount) AS total_price,SUM(igst) AS igst,SUM(cgst) AS cgst,SUM(sgst) AS sgst,SUM(i.cess) AS cess,i.gst_percentage,a.place_of_supply,a.entity_gstin,i.unit_of_measurement,i.gst_category_id FROM invoice_details a,invoice_line_item_details i WHERE a.id=i.invoice_id  AND a.invoice_date BETWEEN :fromDate AND :toDate AND a.is_deleted=0 AND a.org_id=:orgID  AND (a.invoice_type='Sales'  or (a.invoice_type='Journal' and a.is_sales=1)) AND (a.place_of_supply!='' OR a.place_of_supply IS NOT NULL) AND a.is_review=0 GROUP BY i.gst_category_id,a.place_of_supply) i "+
											" LEFT JOIN (SELECT r.id AS item_no, h.item_code,h.description,r.name FROM product_service_legder_relation r,hsn_sac_master h WHERE r.hsn_sac_id=h.id ) r ON r.item_no=i.item_id GROUP BY i.gst_category_id) AS hsn ;";

		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				 int i=1;
				Datum data=new Datum();
				data.setDesc(rs.getString("place_of_supply"));
				data.setIamt(rs.getDouble("total_price"));
				
				data.setUqc(rs.getString("entity_gstin"));
				data.setNum(rs.getInt("gst_category_id"));
				i++;
				return data;
			}
		};
		
		invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getExemptedReport(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in getExemptedReport : "+Ex);
		}
	return invList;

    }

	public List getB2CSReport(int orgID,String fromDate,String toDate){
		List b2csList=null;
		try{
		logger.debug("Entry : public List getB2CSReport(int orgID,String fromDate,String toDate)");
		
		String sqlString="SELECT * FROM(SELECT a.id,a.invoice_type,a.invoice_date,h.item_code,h.description,r.name, SUM(i.quantity) AS qty,SUM(total_price) AS total_value,SUM(amount) AS total_price,SUM(igst) AS igst,SUM(cgst) AS cgst,SUM(sgst) AS sgst,SUM(i.cess) AS cess,i.gst_percentage,a.place_of_supply_id FROM invoice_details a,invoice_line_item_details i,product_service_legder_relation r,hsn_sac_master h WHERE r.hsn_sac_id=h.id "
				+ " AND  a.id=i.invoice_id AND i.item_id=r.id AND a.invoice_date BETWEEN :fromDate AND :toDate AND a.is_deleted=0 AND a.org_id=:orgID  AND (a.invoice_type='Sales' or (a.invoice_type='Journal' and a.is_sales=1)) AND a.is_review=0  AND (a.entity_gstin='' OR a.entity_gstin IS NULL) "
				+ " GROUP BY i.gst_percentage,a.place_of_supply_id) AS hsn WHERE hsn.total_value!=hsn.total_price ";

		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				B2c b2cVO=new B2c();
				b2cVO.setRt(rs.getInt("gst_percentage"));
				b2cVO.setPos(rs.getString("place_of_supply_id"));
				b2cVO.setTxval(rs.getDouble("total_value"));				
				//b2cVO.setDiff_percent(0.00);				
				b2cVO.setIamt(rs.getDouble("igst"));		
				b2cVO.setCamt(rs.getDouble("cgst"));
				b2cVO.setSamt(rs.getDouble("sgst"));				
				b2cVO.setCsamt(rs.getInt("cess"));
				
				return b2cVO;
			}
		};
		
		b2csList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getB2CSReport(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in public List getB2CSReport(int orgID,String fromDate,String toDate) : "+Ex);
		}
	return b2csList;

    }
	
	public List getB2CSReportForPurchase(int orgID,String fromDate,String toDate){
		List b2csList=null;
		try{
		logger.debug("Entry : public List getB2CSReportForPurchase(int orgID,String fromDate,String toDate)");
		
		String sqlString="SELECT a.id,i.gst_category_id,a.invoice_type,a.invoice_date,h.item_code,h.description,r.name, SUM(i.quantity) AS qty,SUM(total_price) AS total_value,SUM(amount) AS total_price,SUM(igst) AS igst,SUM(cgst) AS cgst,SUM(sgst) AS sgst,SUM(i.cess) AS cess,i.gst_percentage,a.place_of_supply FROM invoice_details a,invoice_line_item_details i,product_service_legder_relation r,hsn_sac_master h WHERE r.hsn_sac_id=h.id "
				+ " AND  a.id=i.invoice_id AND i.item_id=r.id AND a.invoice_date BETWEEN :fromDate AND :toDate AND a.is_deleted=0 AND a.org_id=:orgID  AND (a.invoice_type='Purchase' ) AND a.is_review=0  AND (a.entity_gstin='' OR a.entity_gstin IS NULL) "
				+ " GROUP BY i.gst_percentage,a.place_of_supply ";

		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				B2c b2cVO=new B2c();
				b2cVO.setRt(rs.getInt("gst_percentage"));
				b2cVO.setPos(rs.getString("place_of_supply"));
				b2cVO.setTxval(rs.getDouble("total_value"));
				b2cVO.setIamt(rs.getDouble("igst"));
				b2cVO.setCamt(rs.getDouble("cgst"));
				//b2cVO.setDiff_percent(0.00);
				b2cVO.setSamt(rs.getDouble("sgst"));
				b2cVO.setCsamt(rs.getInt("cess"));
				
				return b2cVO;
			}
		};
		
		b2csList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getB2CSReportForPurchase(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in public List getB2CSReportForPurchase(int orgID,String fromDate,String toDate) : "+Ex);
		}
	return b2csList;

    }
	
	public List getCGstinListForB2BReport(int orgID,String fromDate,String toDate){
		List invList=null;
		try{
		logger.debug("Entry : public List getCGstinListForB2BReport(int orgID,String fromDate,String toDate)");
		
		String sqlString=" SELECT * FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.is_deleted=0  AND (i.invoice_type='Sales' or (i.invoice_type='Journal' and i.is_sales=1 )) AND i.is_review=0 "
				+ "AND (i.entity_gstin IS NOT NULL AND i.entity_gstin!='') AND (i.gst_invoice_type_id < 21 OR i.gst_invoice_type_id > 22 )  GROUP BY i.entity_gstin;";
		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				 
				B2b data=new B2b();
			    data.setCtin(rs.getString("entity_gstin"));
				return data;
			}
		};
		
		invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getCGstinListForB2BReport(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in getCGstinListForB2BReport : "+Ex);
		}
	return invList;

    }
	
	public List getInvoiceListForB2BReport(int orgID,String fromDate,String toDate,String gstIn){
		List invList=null;
		try{
		logger.debug("Entry : public List getInvoiceListForB2BReport(int orgID,String fromDate,String toDate)");
		
		String sqlString=" SELECT i.id as invoiceID,i.invoice_date,i.invoice_number,i.place_of_supply,i.place_of_supply_id,i.total_amount,i.invoice_amount,i.invoice_type_id,i.gst_invoice_type_id,i.is_reverse FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.is_deleted=0  AND (i.invoice_type='Sales' or (i.invoice_type='Journal' and i.is_sales=1)) AND i.is_review=0 "
				+ "AND (i.entity_gstin IS NOT NULL AND i.entity_gstin!='') AND (i.gst_invoice_type_id < 21 OR i.gst_invoice_type_id > 22 ) and i.entity_gstin=:cGstin  group by i.id; ";
		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		hMap.put("cGstin", gstIn);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				 
				InvoiceVO data=new InvoiceVO();
				data.setInvoiceID(rs.getInt("invoiceID"));
			    data.setInvoiceNumber(rs.getString("invoice_number"));
			    data.setInvoiceDate(rs.getString("invoice_date"));
			    data.setSupplyStateID(rs.getString("place_of_supply_id"));
			    data.setSupplyState(rs.getString("place_of_supply"));
			    data.setTotalAmount(rs.getBigDecimal("total_amount"));
			    data.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
			    data.setInvoiceTypeID(rs.getInt("invoice_type_id"));
			    data.setGstInvoiceTypeID(rs.getInt("gst_invoice_type_id"));
			    data.setIsReverse(rs.getInt("is_reverse"));
				return data;
			}
		};
		
		invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getInvoiceListForB2BReport(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in getInvoiceListForB2BReport : "+Ex);
		}
	return invList;

    }
	
	
	public List getInvoiceLineItemsListForB2BReport(int orgID,String fromDate,String toDate,int invoiceID){
		List invList=null;
		try{
		logger.debug("Entry : public List getInvoiceLineItemsListForB2BReport(int orgID,String fromDate,String toDate)");
		
		String sqlString=" SELECT ii.id,sum(ii.total_price) as total_price,gst_percentage,sum(ii.igst) as igst, sum(ii.sgst) as sgst, sum(ii.cgst) as cgst, sum(ii.ugst) as ugst, sum(ii.cess) as cess,i.id as invoiceID ,ii.gst_category_id FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.is_deleted=0  AND (i.invoice_type='Sales' or (i.invoice_type='Journal' and i.is_sales=1) ) AND i.is_review=0 "
				+ "AND (i.entity_gstin IS NOT NULL AND i.entity_gstin!='')AND (i.gst_invoice_type_id < 21 OR i.gst_invoice_type_id > 22 )  AND ii.amount!=ii.total_price and i.id=:invoiceID group by ii.gst_percentage ";
		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		hMap.put("invoiceID", invoiceID);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				 
				InLineItemsVO data=new InLineItemsVO();
				data.setInvoiceID(rs.getInt("invoiceID"));
				data.setChargeID(rs.getInt("id"));
			    data.setTotalPrice(rs.getBigDecimal("total_price"));
			    data.setGstPercent(rs.getBigDecimal("gst_percentage"));
			    data.setsGST(rs.getBigDecimal("sgst"));
			    data.setiGST(rs.getBigDecimal("igst"));
			    data.setcGST(rs.getBigDecimal("cgst"));
			    data.setuGST(rs.getBigDecimal("ugst"));
			    data.setCess(rs.getBigDecimal("cess"));
			    data.setGstCategoryID(rs.getInt("gst_category_id"));
			  
			   
				return data;
			}
		};
		
		invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getInvoiceLineItemsListForB2BReport(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in getInvoiceLineItemsListForB2BReport : "+Ex);
		}
	return invList;

    }
	
	
	public List getCGstinListForB2BReportForPurchase(int orgID,String fromDate,String toDate){
		List invList=null;
		try{
		logger.debug("Entry : public List getCGstinListForB2BReportForPurchase(int orgID,String fromDate,String toDate)");
		
		String sqlString=" SELECT * FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.is_deleted=0  AND (i.invoice_type='Purchase' ) AND i.is_review=0 "
				+ " AND (i.entity_gstin IS NOT NULL AND i.entity_gstin!='')  GROUP BY i.entity_gstin;";
		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				 
				B2b data=new B2b();
			    data.setCtin(rs.getString("entity_gstin"));
			    
				return data;
			}
		};
		
		invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getCGstinListForB2BReportForPurchase(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in getCGstinListForB2BReportForPurchase : "+Ex);
		}
	return invList;

    }
	
	public List getInvoiceListForB2BReportForPurchase(int orgID,String fromDate,String toDate,String gstIn){
		List invList=null;
		try{
		logger.debug("Entry : public List getInvoiceListForB2BReportForPurchase(int orgID,String fromDate,String toDate)");

		String sqlString=" SELECT i.id as invoiceID,i.invoice_date,i.invoice_number,i.place_of_supply,i.total_amount,i.invoice_amount,ii.gst_category_id,i.gst_invoice_type_id,i.is_reverse,i.invoice_type_id FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.is_deleted=0  AND (i.invoice_type='Purchase'  ) AND i.is_review=0 "
									 + " and i.entity_gstin=:cGstin group by i.id; ";

		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		hMap.put("cGstin", gstIn);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				 
				InvoiceVO data=new InvoiceVO();
				data.setInvoiceID(rs.getInt("invoiceID"));
			    data.setInvoiceNumber(rs.getString("invoice_number"));
			    data.setInvoiceDate(rs.getString("invoice_date"));
			    data.setSupplyState(rs.getString("place_of_supply"));
			    data.setTotalAmount(rs.getBigDecimal("total_amount"));
			    data.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
			    data.setInvoiceTypeID(rs.getInt("gst_category_id"));
			    data.setInvoiceTypeID(rs.getInt("invoice_type_id"));
			    data.setGstInvoiceTypeID(rs.getInt("gst_invoice_type_id"));
			    data.setIsReverse(rs.getInt("is_reverse"));
				return data;
			}
		};
		
		invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getInvoiceListForB2BReportForPurchase(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in getInvoiceListForB2BReportForPurchase : "+Ex);
		}
	return invList;

    }
	
	
	public List getInvoiceLineItemsListForB2BReportForPurchase(int orgID,String fromDate,String toDate,int invoiceID){
		List invList=null;
		try{
		logger.debug("Entry : public List getInvoiceLineItemsListForB2BReportForPurchase(int orgID,String fromDate,String toDate)");
		
		String sqlString=" SELECT ii.id,sum(ii.total_price) as total_price,gst_percentage,sum(ii.igst) as igst, sum(ii.sgst) as sgst, sum(ii.cgst) as cgst, sum(ii.ugst) as ugst, sum(ii.cess) as cess,i.id as invoiceID FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.is_deleted=0  AND (i.invoice_type='Purchase'  ) AND i.is_review=0 "
				+ "  and i.id=:invoiceID group by ii.gst_percentage ";
		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		hMap.put("invoiceID", invoiceID);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				 
				InLineItemsVO data=new InLineItemsVO();
				data.setInvoiceID(rs.getInt("invoiceID"));
				data.setChargeID(rs.getInt("id"));
			    data.setTotalPrice(rs.getBigDecimal("total_price"));
			    data.setGstPercent(rs.getBigDecimal("gst_percentage"));
			    data.setsGST(rs.getBigDecimal("sgst"));
			    data.setiGST(rs.getBigDecimal("igst"));
			    data.setcGST(rs.getBigDecimal("cgst"));
			    data.setuGST(rs.getBigDecimal("ugst"));
			    data.setCess(rs.getBigDecimal("cess"));
			  
			   
				return data;
			}
		};
		
		invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getInvoiceLineItemsListForB2BReportForPurchase(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in getInvoiceLineItemsListForB2BReportForPurchase : "+Ex);
		}
	return invList;

    }
	
	public List getInvoiceListForExpReport(int orgID,String fromDate,String toDate,int gstTypeID){
		List invList=null;
		try{
		logger.debug("Entry : public List getInvoiceListForExpReport(int orgID,String fromDate,String toDate)");
		
		String sqlString=" SELECT i.id as invoiceID,i.invoice_date,i.invoice_number,i.place_of_supply,i.total_amount,i.invoice_amount,i.port_code,i.shipping_bill_no,i.shipping_date FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.is_deleted=0  AND (i.invoice_type='Sales' or (i.invoice_type='Journal' and i.is_sales=1)  ) AND i.is_review=0 "
				+ " and i.gst_invoice_type_id=:gstTypeID group by i.id; ";
		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		hMap.put("gstTypeID", gstTypeID);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				 
				InvoiceVO data=new InvoiceVO();
				data.setInvoiceID(rs.getInt("invoiceID"));
			    data.setInvoiceNumber(rs.getString("invoice_number"));
			    data.setInvoiceDate(rs.getString("invoice_date"));
			    data.setSupplyState(rs.getString("place_of_supply"));
			    data.setTotalAmount(rs.getBigDecimal("total_amount"));
			    data.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
			    data.setPortCode(rs.getString("port_code"));
			    data.setShippingBillNo(rs.getString("shipping_bill_no"));
			    data.setShippingDate(rs.getString("shipping_date"));
			   
				return data;
			}
		};
		
		invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getInvoiceListForExpReport(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in getInvoiceListForExpReport : "+Ex);
		}
	return invList;

    }
	
	public List getInvoiceLineItemsListForReport(int orgID,String fromDate,String toDate,int invoiceID){
		List invList=null;
		try{
		logger.debug("Entry : public List getInvoiceLineItemsListForReport(int orgID,String fromDate,String toDate)");
		
		String sqlString=" SELECT ii.id,sum(ii.total_price) as total_price,gst_percentage,sum(ii.igst) as igst, sum(ii.sgst) as sgst, sum(ii.cgst) as cgst, sum(ii.ugst) as ugst, sum(ii.cess) as cess,i.id as invoiceID FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.is_deleted=0  AND (i.invoice_type='Sales' or (i.invoice_type='Journal' and i.is_sales=1))  AND i.is_review=0 "
				+ " AND ii.amount!=ii.total_price and i.id=:invoiceID group by ii.gst_percentage ";
		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		hMap.put("invoiceID", invoiceID);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				 
				InLineItemsVO data=new InLineItemsVO();
				data.setInvoiceID(rs.getInt("invoiceID"));
				data.setChargeID(rs.getInt("id"));
			    data.setTotalPrice(rs.getBigDecimal("total_price"));
			    data.setGstPercent(rs.getBigDecimal("gst_percentage"));
			    data.setsGST(rs.getBigDecimal("sgst"));
			    data.setiGST(rs.getBigDecimal("igst"));
			    data.setcGST(rs.getBigDecimal("cgst"));
			    data.setuGST(rs.getBigDecimal("ugst"));
			    data.setCess(rs.getBigDecimal("cess"));
			  
			   
				return data;
			}
		};
		
		invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getInvoiceLineItemsListForReport(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in getInvoiceLineItemsListForReport : "+Ex);
		}
	return invList;

    }
	
	public List getInvoiceListForDocIssueRegisteredReport(int orgID,String fromDate,String toDate,int isDeleted){
		List invList=null;
		try{
		logger.debug("Entry : public List getInvoiceListForDocIssueRegisteredReport(int orgID,String fromDate,String toDate)");
		
		String sqlString=" SELECT * FROM invoice_details WHERE invoice_date BETWEEN :fromDate AND :toDate  AND org_id=:orgID   AND (invoice_type='Sales'  or (invoice_type='Journal' or is_sales=1)) AND is_review=0 order by CAST(invoice_number AS UNSIGNED) ; ";
		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		hMap.put("isDeleted", isDeleted);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				 
				InvoiceVO data=new InvoiceVO();
				data.setInvoiceID(rs.getInt("id"));
			    data.setInvoiceNumber(rs.getString("invoice_number"));
			    data.setInvoiceDate(rs.getString("invoice_date"));
			    data.setSupplyState(rs.getString("place_of_supply"));
			    data.setTotalAmount(rs.getBigDecimal("total_amount"));
			    data.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
			    data.setIsDeleted(rs.getInt("is_deleted"));
				return data;
			}
		};
		
		invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getInvoiceListForDocIssueRegisteredReport(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in getInvoiceListForDocIssueRegisteredReport : "+Ex);
		}
	return invList;

    }
	
	public List getInvoiceListForDocIssueUnRegisteredReport(int orgID,String fromDate,String toDate,int isDeleted){
		List invList=null;
		try{
		logger.debug("Entry : public List getInvoiceListForDocIssueUnRegisteredReport(int orgID,String fromDate,String toDate)");
		
		String sqlString=" SELECT * FROM invoice_details WHERE invoice_date BETWEEN :fromDate AND :toDate AND is_deleted=:isDeleted AND org_id=:orgID AND (entity_gstin IS NULL OR entity_gstin='')  AND (invoice_type='Sales'  or (invoice_type='Journal' or is_sales=1)) AND is_review=0 order by CAST(invoice_number AS UNSIGNED) ; ";
		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		hMap.put("isDeleted", isDeleted);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				 
				InvoiceVO data=new InvoiceVO();
				data.setInvoiceID(rs.getInt("id"));
			    data.setInvoiceNumber(rs.getString("invoice_number"));
			    data.setInvoiceDate(rs.getString("invoice_date"));
			    data.setSupplyState(rs.getString("place_of_supply"));
			    data.setTotalAmount(rs.getBigDecimal("total_amount"));
			    data.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
			   
				return data;
			}
		};
		
		invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getInvoiceListForDocIssueUnRegisteredReport(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in getInvoiceListForDocIssueUnRegisteredReport : "+Ex);
		}
	return invList;

    }
	
	public List getInvoiceListForDocIssueCreditDebitNoteReport(int orgID,String fromDate,String toDate,int isDeleted,String type){
		List invList=null;
		try{
		logger.debug("Entry : public List getInvoiceListForDocIssueCreditDebitNoteReport(int orgID,String fromDate,String toDate)");
		
		String sqlString=" SELECT * FROM invoice_details WHERE invoice_date BETWEEN :fromDate AND :toDate AND is_deleted=:isDeleted AND org_id=:orgID   AND invoice_type=:type  AND is_review=0 AND entity_gstin!='' AND entity_gstin IS NOT NULL order by CAST(invoice_number AS UNSIGNED) ; ";
		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		hMap.put("isDeleted", isDeleted);
		hMap.put("type",type);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				 
				InvoiceVO data=new InvoiceVO();
				data.setInvoiceID(rs.getInt("id"));
			    data.setInvoiceNumber(rs.getString("invoice_number"));
			    data.setInvoiceDate(rs.getString("invoice_date"));
			    data.setSupplyState(rs.getString("place_of_supply"));
			    data.setTotalAmount(rs.getBigDecimal("total_amount"));
			    data.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
			   
				return data;
			}
		};
		
		invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getInvoiceListForDocIssueCreditDebitNoteReport(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in getInvoiceListForDocIssueCreditDebitNoteReport : "+Ex);
		}
	return invList;

    }
	
	//========CDNR List ============//
	public List getCGstinListForCDNRReport(int orgID,String fromDate,String toDate){
		List invList=null;
		try{
		logger.debug("Entry : public List getCGstinListForCDNRReport(int orgID,String fromDate,String toDate)");
		
		String sqlString=" SELECT * FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) AND i.org_id=:orgID AND i.invoice_amount<'250000' AND i.is_deleted=0  AND (i.invoice_type='Credit Note'  ) AND i.is_review=0 AND entity_gstin!='' AND entity_gstin IS NOT NULL GROUP BY entity_gstin";
										
		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				 
				Cdnr data=new Cdnr();
			    data.setCtin(rs.getString("entity_gstin"));
				return data;
			}
		};
		
		invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getCGstinListForCDNRReport(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in getCGstinListForCDNRReport : "+Ex);
		}
	return invList;

    }
	
	public List getInvoiceListForCDNRReport(int orgID,String fromDate,String toDate,String gstIn){
		List invList=null;
		try{
		logger.debug("Entry : public List getInvoiceListForCDNRReport(int orgID,String fromDate,String toDate)");
		
		String sqlString=" SELECT i.id as invoiceID,i.invoice_date,i.invoice_number,i.place_of_supply,i.total_amount,i.invoice_amount,i.invoice_type FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.invoice_amount<'250000' AND i.is_deleted=0  AND (i.invoice_type='Credit Note' )  AND i.is_review=0 "
				+ " and i.entity_gstin=:cGstin group by i.id; ";
		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		hMap.put("cGstin", gstIn);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				 
				InvoiceVO data=new InvoiceVO();
				data.setInvoiceID(rs.getInt("invoiceID"));
			    data.setInvoiceNumber(rs.getString("invoice_number"));
			    data.setInvoiceDate(rs.getString("invoice_date"));
			    data.setSupplyState(rs.getString("place_of_supply"));
			    data.setTotalAmount(rs.getBigDecimal("total_amount"));
			    data.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
			    data.setInvoiceType(rs.getString("invoice_type"));
				return data;
			}
		};
		
		invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getInvoiceListForCDNRReport(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in getInvoiceListForCDNRReport : "+Ex);
		}
	return invList;

    }
	
	public List getInvoiceLineItemsListForCDNRReport(int orgID,String fromDate,String toDate,int invoiceID){
		List invList=null;
		try{
		logger.debug("Entry : public List getInvoiceLineItemsListForCDNRReport(int orgID,String fromDate,String toDate)");
		
		String sqlString=" SELECT ii.id,sum(ii.total_price) as total_price,gst_percentage,sum(ii.igst) as igst, sum(ii.sgst) as sgst, sum(ii.cgst) as cgst, sum(ii.ugst) as ugst, sum(ii.cess) as cess,i.id as invoiceID FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.is_deleted=0  AND (i.invoice_type='Credit Note') AND i.is_review=0 "
				+ "  and i.id=:invoiceID group by ii.gst_percentage ";
		Map hMap=new HashMap();
		hMap.put("orgID",orgID);
		hMap.put("fromDate",fromDate);
		hMap.put("toDate", toDate);
		hMap.put("invoiceID", invoiceID);
		
		RowMapper RMapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				 
				InLineItemsVO data=new InLineItemsVO();
				data.setInvoiceID(rs.getInt("invoiceID"));
				data.setChargeID(rs.getInt("id"));
			    data.setTotalPrice(rs.getBigDecimal("total_price"));
			    data.setGstPercent(rs.getBigDecimal("gst_percentage"));
			    data.setsGST(rs.getBigDecimal("sgst"));
			    data.setiGST(rs.getBigDecimal("igst"));
			    data.setcGST(rs.getBigDecimal("cgst"));
			    data.setuGST(rs.getBigDecimal("ugst"));
			    data.setCess(rs.getBigDecimal("cess"));
			  
			   
				return data;
			}
		};
		
		invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
		
		logger.debug("Exit : public List getInvoiceLineItemsListForCDNRReport(int orgID,String fromDate,String toDate)");	
		}catch(Exception Ex){
			
			logger.error("Exception in getInvoiceLineItemsListForCDNRReport : "+Ex);
		}
	return invList;

    }
	
	
	//========CDNRA List ============//
		public List getCGstinListForCDNRAReport(int orgID,String fromDate,String toDate){
			List invList=null;
			try{
			logger.debug("Entry : public List getCGstinListForCDNRAReport(int orgID,String fromDate,String toDate)");
			
			String sqlString=" SELECT * FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) AND i.org_id=:orgID AND i.invoice_amount<'250000' AND i.is_deleted=0  AND (i.invoice_type='Credit Note') AND i.is_review=0 "+
											 "	AND (i.entity_gstin IS NOT NULL AND i.entity_gstin!='') GROUP BY i.entity_gstin;";
			Map hMap=new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate",fromDate);
			hMap.put("toDate", toDate);
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					Cdnra data=new Cdnra();
				    data.setCtin(rs.getString("entity_gstin"));
					return data;
				}
			};
			
			invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public List getCGstinListForCDNRAReport(int orgID,String fromDate,String toDate)");	
			}catch(Exception Ex){
				
				logger.error("Exception in getCGstinListForCDNRAReport : "+Ex);
			}
		return invList;

	    }
		
		public List getInvoiceListForCDNRAReport(int orgID,String fromDate,String toDate,String gstIn){
			List invList=null;
			try{
			logger.debug("Entry : public List getInvoiceListForCDNRAReport(int orgID,String fromDate,String toDate)");
			
			String sqlString=" SELECT i.id as invoiceID,i.invoice_date,i.invoice_number,i.place_of_supply,i.total_amount,i.invoice_amount,i.invoice_type FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.invoice_amount<'250000' AND i.is_deleted=0  AND (i.invoice_type='Credit Note')  AND i.is_review=0 "
					+ "AND (i.entity_gstin IS NOT NULL AND i.entity_gstin!='') and i.entity_gstin=:cGstin group by i.id; ";
			Map hMap=new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate",fromDate);
			hMap.put("toDate", toDate);
			hMap.put("cGstin", gstIn);
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					InvoiceVO data=new InvoiceVO();
					data.setInvoiceID(rs.getInt("invoiceID"));
				    data.setInvoiceNumber(rs.getString("invoice_number"));
				    data.setInvoiceDate(rs.getString("invoice_date"));
				    data.setSupplyState(rs.getString("place_of_supply"));
				    data.setTotalAmount(rs.getBigDecimal("total_amount"));
				    data.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
				    data.setInvoiceType(rs.getString("invoice_type"));
					return data;
				}
			};
			
			invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public List getInvoiceListForCDNRAReport(int orgID,String fromDate,String toDate)");	
			}catch(Exception Ex){
				
				logger.error("Exception in getInvoiceListForCDNRAReport : "+Ex);
			}
		return invList;

	    }
		
		public List getInvoiceLineItemsListForCDNRAReport(int orgID,String fromDate,String toDate,int invoiceID){
			List invList=null;
			try{
			logger.debug("Entry : public List getInvoiceLineItemsListForCDNRAReport(int orgID,String fromDate,String toDate)");
			
			String sqlString=" SELECT ii.id,sum(ii.total_price) as total_price,gst_percentage,sum(ii.igst) as igst, sum(ii.sgst) as sgst, sum(ii.cgst) as cgst, sum(ii.ugst) as ugst, sum(ii.cess) as cess,i.id as invoiceID FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.is_deleted=0  AND (i.invoice_type='Credit Note') AND i.is_review=0 "
					+ "AND (i.entity_gstin IS NOT NULL AND i.entity_gstin!='') AND ii.amount!=ii.total_price and i.id=:invoiceID group by ii.gst_percentage ";
			Map hMap=new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate",fromDate);
			hMap.put("toDate", toDate);
			hMap.put("invoiceID", invoiceID);
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					InLineItemsVO data=new InLineItemsVO();
					data.setInvoiceID(rs.getInt("invoiceID"));
					data.setChargeID(rs.getInt("id"));
				    data.setTotalPrice(rs.getBigDecimal("total_price"));
				    data.setGstPercent(rs.getBigDecimal("gst_percentage"));
				    data.setsGST(rs.getBigDecimal("sgst"));
				    data.setiGST(rs.getBigDecimal("igst"));
				    data.setcGST(rs.getBigDecimal("cgst"));
				    data.setuGST(rs.getBigDecimal("ugst"));
				    data.setCess(rs.getBigDecimal("cess"));
				  
				   
					return data;
				}
			};
			
			invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public List getInvoiceLineItemsListForCDNRAReport(int orgID,String fromDate,String toDate)");	
			}catch(Exception Ex){
				
				logger.error("Exception in getInvoiceLineItemsListForCDNRAReport : "+Ex);
			}
		return invList;

	    }
	
		public List getInvoiceListForCDNURReport(int orgID,String fromDate,String toDate){
			List invList=null;
			try{
			logger.debug("Entry : public List getInvoiceListForCDNURReport(int orgID,String fromDate,String toDate)");
			
			String sqlString=" SELECT i.id as invoiceID,i.invoice_date,i.invoice_number,i.place_of_supply,i.total_amount,i.invoice_amount,i.invoice_type FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.invoice_amount>='250000' AND i.is_deleted=0  AND (i.invoice_type='Credit Note')  AND i.is_review=0 "
					+ "AND (i.entity_gstin IS NULL or i.entity_gstin='') group by i.id; ";
			Map hMap=new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate",fromDate);
			hMap.put("toDate", toDate);
			
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					InvoiceVO data=new InvoiceVO();
					data.setInvoiceID(rs.getInt("invoiceID"));
				    data.setInvoiceNumber(rs.getString("invoice_number"));
				    data.setInvoiceDate(rs.getString("invoice_date"));
				    data.setSupplyState(rs.getString("place_of_supply"));
				    data.setTotalAmount(rs.getBigDecimal("total_amount"));
				    data.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
				    data.setInvoiceType(rs.getString("invoice_type"));
					return data;
				}
			};
			
			invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public List getInvoiceListForCDNURReport(int orgID,String fromDate,String toDate)");	
			}catch(Exception Ex){
				
				logger.error("Exception in getInvoiceListForCDNURReport : "+Ex);
			}
		return invList;

	    }
		
		public List getInvoiceLineItemsListForCDNURReport(int orgID,String fromDate,String toDate,int invoiceID){
			List invList=null;
			try{
			logger.debug("Entry : public List getInvoiceLineItemsListForCDNURReport(int orgID,String fromDate,String toDate)");
			
			String sqlString=" SELECT ii.id,sum(ii.total_price) as total_price,gst_percentage,sum(ii.igst) as igst, sum(ii.sgst) as sgst, sum(ii.cgst) as cgst, sum(ii.ugst) as ugst, sum(ii.cess) as cess,i.id as invoiceID FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.is_deleted=0  AND (i.invoice_type='Credit Note') AND i.is_review=0 "
					+ "AND (i.entity_gstin IS NULL OR i.entity_gstin='') AND ii.amount!=ii.total_price and i.id=:invoiceID group by ii.gst_percentage ";
			Map hMap=new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate",fromDate);
			hMap.put("toDate", toDate);
			hMap.put("invoiceID", invoiceID);
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					InLineItemsVO data=new InLineItemsVO();
					data.setInvoiceID(rs.getInt("invoiceID"));
					data.setChargeID(rs.getInt("id"));
				    data.setTotalPrice(rs.getBigDecimal("total_price"));
				    data.setGstPercent(rs.getBigDecimal("gst_percentage"));
				    data.setsGST(rs.getBigDecimal("sgst"));
				    data.setiGST(rs.getBigDecimal("igst"));
				    data.setcGST(rs.getBigDecimal("cgst"));
				    data.setuGST(rs.getBigDecimal("ugst"));
				    data.setCess(rs.getBigDecimal("cess"));
				  
				   
					return data;
				}
			};
			
			invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public List getInvoiceLineItemsListForCDNRAReport(int orgID,String fromDate,String toDate)");	
			}catch(Exception Ex){
				
				logger.error("Exception in getInvoiceLineItemsListForCDNRAReport : "+Ex);
			}
		return invList;

	    }
		
		public List getInvoiceListForTaxableSupplyDetailsReport(int orgID,String fromDate,String toDate){
			List invList=null;
			try{
			logger.debug("Entry : public List getInvoiceListForTaxableSupplyDetailsReport(int orgID,String fromDate,String toDate)");
			
			String sqlString=" SELECT i.id as invoiceID,i.invoice_date,i.invoice_number,i.place_of_supply,i.total_amount,i.invoice_amount,i.invoice_type ,total_igst,total_cgst,total_sgst,total_ugst,total_cess,SUM(ii.amount) AS amount, SUM(ii.total_price) AS total_price,SUM(ii.igst) AS igst, SUM(ii.sgst) AS sgst, SUM(ii.cgst) AS cgst, SUM(ii.ugst) AS ugst, SUM(ii.cess) AS cess FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ii.gst_category_id=1 and  ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.is_deleted=0  AND (i.invoice_type='Credit Note' or  i.invoice_type='Sales' or (i.invoice_type='Journal' and i.is_sales=1 )) AND i.is_review=0 "
					+ " AND ii.hsn_sac_code IS NOT NULL group by i.id; ";
			Map hMap=new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate",fromDate);
			hMap.put("toDate", toDate);
			
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					InvoiceVO data=new InvoiceVO();
					data.setInvoiceID(rs.getInt("invoiceID"));
				    data.setInvoiceNumber(rs.getString("invoice_number"));
				    data.setInvoiceDate(rs.getString("invoice_date"));
				    data.setSupplyState(rs.getString("place_of_supply"));
				    data.setTotalAmount(rs.getBigDecimal("amount"));
				    data.setInvoiceAmount(rs.getBigDecimal("total_price"));
				    data.setInvoiceType(rs.getString("invoice_type"));
				    data.setTotalIGST(rs.getBigDecimal("igst"));
				    data.setTotalCGST(rs.getBigDecimal("cgst"));
				    data.setTotalSGST(rs.getBigDecimal("sgst"));
				    data.setTotalUGST(rs.getBigDecimal("ugst"));
				    data.setTotalCess(rs.getBigDecimal("cess"));
					return data;
				}
			};
			
			invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public List getInvoiceListForTaxableSupplyDetailsReport(int orgID,String fromDate,String toDate)");	
			}catch(Exception Ex){
				
				logger.error("Exception in getInvoiceListForTaxableSupplyDetailsReport : "+Ex);
			}
		return invList;

	    }
		
		public List getInvoiceListForNonTaxableSupplyDetailsReport(int orgID,String fromDate,String toDate,int gstCategoryID){
			List invList=null;
			try{
			logger.debug("Entry : public List getInvoiceListForTaxableSupplyDetailsReport(int orgID,String fromDate,String toDate)");
			
			String sqlString=" SELECT i.id as invoiceID,i.invoice_date,i.invoice_number,i.place_of_supply,i.total_amount,i.invoice_amount,i.invoice_type ,total_igst,total_cgst,total_sgst,total_ugst,total_cess,SUM(ii.amount) AS amount, SUM(ii.total_price) AS total_price,SUM(ii.igst) AS igst, SUM(ii.sgst) AS sgst, SUM(ii.cgst) AS cgst, SUM(ii.ugst) AS ugst, SUM(ii.cess) AS cess FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ii.gst_category_id=:gstCategoryID and  ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.is_deleted=0  AND  (i.invoice_type='Credit Note' or  i.invoice_type='Sales' or (i.invoice_type='Journal' and i.is_sales=1 )) AND i.is_review=0 "
					+ "  group by i.id; ";
			Map hMap=new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate",fromDate);
			hMap.put("toDate", toDate);
			hMap.put("gstCategoryID", gstCategoryID);
			
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					InvoiceVO data=new InvoiceVO();
					data.setInvoiceID(rs.getInt("invoiceID"));
				    data.setInvoiceNumber(rs.getString("invoice_number"));
				    data.setInvoiceDate(rs.getString("invoice_date"));
				    data.setSupplyState(rs.getString("place_of_supply"));
				    data.setTotalAmount(rs.getBigDecimal("amount"));
				    data.setInvoiceAmount(rs.getBigDecimal("total_price"));
				    data.setInvoiceType(rs.getString("invoice_type"));
				    data.setTotalIGST(rs.getBigDecimal("igst"));
				    data.setTotalCGST(rs.getBigDecimal("cgst"));
				    data.setTotalSGST(rs.getBigDecimal("sgst"));
				    data.setTotalUGST(rs.getBigDecimal("ugst"));
				    data.setTotalCess(rs.getBigDecimal("cess"));
					return data;
				}
			};
			
			invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public List getInvoiceListForTaxableSupplyDetailsReport(int orgID,String fromDate,String toDate)");	
			}catch(Exception Ex){
				
				logger.error("Exception in getInvoiceListForTaxableSupplyDetailsReport : "+Ex);
			}
		return invList;

	    }
		
		public List getInvoiceListForNilExmtpSupplyDetailsReport(int orgID,String fromDate,String toDate,int gstCategoryID){
			List invList=null;
			try{
			logger.debug("Entry : public List getInvoiceListForNilExmtpSupplyDetailsReport(int orgID,String fromDate,String toDate)");
			
			String sqlString=" SELECT i.id as invoiceID,i.invoice_date,i.invoice_number,i.place_of_supply,i.total_amount,i.invoice_amount,i.invoice_type ,total_igst,total_cgst,total_sgst,total_ugst,total_cess,SUM(ii.amount) AS amount, SUM(ii.total_price) AS total_price,SUM(ii.igst) AS igst, SUM(ii.sgst) AS sgst, SUM(ii.cgst) AS cgst, SUM(ii.ugst) AS ugst, SUM(ii.cess) AS cess FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND (ii.gst_category_id=3 or ii.gst_category_id=4) and  ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.is_deleted=0  AND  (i.invoice_type='Credit Note' or  i.invoice_type='Sales' or (i.invoice_type='Journal' and i.is_sales=1 )) AND i.is_review=0 "
					+ "   group by i.id; ";
			Map hMap=new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate",fromDate);
			hMap.put("toDate", toDate);
			hMap.put("gstCategoryID", gstCategoryID);
			
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					InvoiceVO data=new InvoiceVO();
					data.setInvoiceID(rs.getInt("invoiceID"));
				    data.setInvoiceNumber(rs.getString("invoice_number"));
				    data.setInvoiceDate(rs.getString("invoice_date"));
				    data.setSupplyState(rs.getString("place_of_supply"));
				    data.setTotalAmount(rs.getBigDecimal("amount"));
				    data.setInvoiceAmount(rs.getBigDecimal("total_price"));
				    data.setInvoiceType(rs.getString("invoice_type"));
				    data.setTotalIGST(rs.getBigDecimal("igst"));
				    data.setTotalCGST(rs.getBigDecimal("cgst"));
				    data.setTotalSGST(rs.getBigDecimal("sgst"));
				    data.setTotalUGST(rs.getBigDecimal("ugst"));
				    data.setTotalCess(rs.getBigDecimal("cess"));
					return data;
				}
			};
			
			invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public List getInvoiceListForNilExmtpSupplyDetailsReport(int orgID,String fromDate,String toDate)");	
			}catch(Exception Ex){
				
				logger.error("Exception in getInvoiceListForNilExmtpSupplyDetailsReport : "+Ex);
			}
		return invList;

	    }
		
		public List getInvoiceListForReverseChargeDetailsReport(int orgID,String fromDate,String toDate,int isDeleted){
			List invList=null;
			try{
			logger.debug("Entry : public List getInvoiceListForReverseChargeDetailsReport(int orgID,String fromDate,String toDate)");
			
			String sqlString=" SELECT i.id as invoiceID,i.invoice_date,i.invoice_number,i.place_of_supply,i.total_amount,i.invoice_amount,i.invoice_type ,total_igst,total_cgst,total_sgst,total_ugst,total_cess,SUM(ii.amount) AS amount, SUM(ii.total_price) AS total_price,SUM(ii.igst) AS igst, SUM(ii.sgst) AS sgst, SUM(ii.cgst) AS cgst, SUM(ii.ugst) AS ugst, SUM(ii.cess) AS cess FROM invoice_details i,invoice_line_item_details ii  WHERE   i.id=ii.invoice_id AND  ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.is_deleted=:isDeleted  AND ((i.invoice_type='Purchase' ) ) AND i.is_review=0 AND i.is_reverse=1 "
					+ "  group by i.id; ";
			Map hMap=new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate",fromDate);
			hMap.put("toDate", toDate);
			hMap.put("isDeleted", isDeleted);
			
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					InvoiceVO data=new InvoiceVO();
					data.setInvoiceID(rs.getInt("invoiceID"));
				    data.setInvoiceNumber(rs.getString("invoice_number"));
				    data.setInvoiceDate(rs.getString("invoice_date"));
				    data.setSupplyState(rs.getString("place_of_supply"));
				    data.setTotalAmount(rs.getBigDecimal("amount"));
				    data.setInvoiceAmount(rs.getBigDecimal("total_price"));
				    data.setInvoiceType(rs.getString("invoice_type"));
				    data.setTotalIGST(rs.getBigDecimal("igst"));
				    data.setTotalCGST(rs.getBigDecimal("cgst"));
				    data.setTotalSGST(rs.getBigDecimal("sgst"));
				    data.setTotalUGST(rs.getBigDecimal("ugst"));
				    data.setTotalCess(rs.getBigDecimal("cess"));
					return data;
				}
			};
			
			invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public List getInvoiceListForReverseChargeDetailsReport(int orgID,String fromDate,String toDate)");	
			}catch(Exception Ex){
				
				logger.error("Exception in getInvoiceListForReverseChargeDetailsReport : "+Ex);
			}
		return invList;

	    }
		
		public List getInvoiceListForNonGstSupplyDetailsReport(int orgID,String fromDate,String toDate){
			List invList=null;
			try{
			logger.debug("Entry : public List getInvoiceListForNonGstSupplyDetailsReport(int orgID,String fromDate,String toDate)");
			
			String sqlString=" SELECT i.id as invoiceID,i.invoice_date,i.invoice_number,i.place_of_supply,i.total_amount,i.invoice_amount,i.invoice_type ,total_igst,total_cgst,total_sgst,total_ugst,total_cess,ii.amount,ii.total_price,ii.cgst,ii.igst,ii.sgst,ii.ugst,ii.cess FROM invoice_details i,invoice_line_item_details ii WHERE i.id=ii.invoice_id AND ii.gst_category_id=5 and  ( i.invoice_date BETWEEN :fromDate AND :toDate) and i.org_id=:orgID AND i.is_deleted=0  AND  (i.invoice_type='Credit Note' or  i.invoice_type='Sales' or (i.invoice_type='Journal' and i.is_sales=1 )) AND i.is_review=0 "
					+ " group by i.id; ";
			Map hMap=new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate",fromDate);
			hMap.put("toDate", toDate);
					
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					InvoiceVO data=new InvoiceVO();
					data.setInvoiceID(rs.getInt("invoiceID"));
				    data.setInvoiceNumber(rs.getString("invoice_number"));
				    data.setInvoiceDate(rs.getString("invoice_date"));
				    data.setSupplyState(rs.getString("place_of_supply"));
				    data.setTotalAmount(rs.getBigDecimal("amount"));
				    data.setInvoiceAmount(rs.getBigDecimal("total_price"));
				    data.setInvoiceType(rs.getString("invoice_type"));
				    data.setTotalIGST(rs.getBigDecimal("igst"));
				    data.setTotalCGST(rs.getBigDecimal("cgst"));
				    data.setTotalSGST(rs.getBigDecimal("sgst"));
				    data.setTotalUGST(rs.getBigDecimal("ugst"));
				    data.setTotalCess(rs.getBigDecimal("cess"));
					return data;
				}
			};
			
			invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public List getInvoiceListForNonGstSupplyDetailsReport(int orgID,String fromDate,String toDate)");	
			}catch(Exception Ex){
				
				logger.error("Exception in getInvoiceListForNonGstSupplyDetailsReport : "+Ex);
			}
		return invList;

	    }
		
		public List getInvoiceLineItemsListForITCElgForImportReport(int orgID,String fromDate,String toDate,int gstCategoryID){
			List invList=null;
			try{
			logger.debug("Entry : public List getInvoiceLineItemsListForITCElgForImportReport(int orgID,String fromDate,String toDate)");
			
			String sqlString=" SELECT i.entity_gstin, ii.id,SUM(ii.total_price) AS total_price,gst_percentage,SUM(ii.igst) AS igst, SUM(ii.sgst) AS sgst, SUM(ii.cgst) AS cgst, SUM(ii.ugst) AS ugst, SUM(ii.cess) AS cess,i.id AS invoiceID,i.* FROM invoice_details i, invoice_line_item_details ii WHERE  i.id=ii.invoice_id AND i.gst_invoice_type_id=:gstInvoiceTypeID AND ( i.invoice_date BETWEEN :fromDate AND :toDate) AND i.org_id=:orgID AND i.is_deleted=0  AND (i.invoice_type='Purchase' ) AND i.is_review=0 "
											+" AND (i.entity_gstin IS NOT NULL OR i.entity_gstin!='') AND ii.amount!=ii.total_price GROUP BY i.id   ;";
			Map hMap=new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate",fromDate);
			hMap.put("toDate", toDate);
			hMap.put("gstInvoiceTypeID", gstCategoryID);
						
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					InLineItemsVO data=new InLineItemsVO();
					data.setInvoiceID(rs.getInt("invoiceID"));
					data.setChargeID(rs.getInt("id"));
					data.setTotalPrice(rs.getBigDecimal("total_price"));
				    data.setGstPercent(rs.getBigDecimal("gst_percentage"));
				    data.setsGST(rs.getBigDecimal("sgst"));
				    data.setiGST(rs.getBigDecimal("igst"));
				    data.setcGST(rs.getBigDecimal("cgst"));
				    data.setuGST(rs.getBigDecimal("ugst"));
				    data.setCess(rs.getBigDecimal("cess"));
				    data.setDescription(rs.getString("entity_gstin"));
				  
				   
					return data;
				}
			};
			
			invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public List getInvoiceLineItemsListForITCElgForImportReport(int orgID,String fromDate,String toDate)");	
			}catch(Exception Ex){
				
				logger.error("Exception in getInvoiceLineItemsListForITCElgForImportReport : "+Ex);
			}
		return invList;

	    }
		
		public List getInvoiceLineItemsListForITCElgReport(int orgID,String fromDate,String toDate){
			List invList=null;
			try{
			logger.debug("Entry : public List getInvoiceLineItemsListForITCElgReport(int orgID,String fromDate,String toDate)");
			String sqlString=" SELECT i.entity_gstin, ii.id,SUM(ii.total_price) AS total_price,gst_percentage,SUM(ii.igst) AS igst, SUM(ii.sgst) AS sgst, SUM(ii.cgst) AS cgst, SUM(ii.ugst) AS ugst, SUM(ii.cess) AS cess,i.id AS invoiceID,i.* FROM invoice_details i, invoice_line_item_details ii WHERE  i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) AND i.org_id=:orgID AND i.is_deleted=0  AND (i.invoice_type='Purchase' ) AND i.is_review=0 "
					+" AND (i.entity_gstin IS not  NULL OR i.entity_gstin!='') and i.is_reverse!=1 and (i.gst_invoice_type_id!=24 OR i.gst_invoice_type_id!=25 OR i.gst_invoice_type_id!=30) AND ii.amount!=ii.total_price GROUP BY i.id   ;";
			
			Map hMap=new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate",fromDate);
			hMap.put("toDate", toDate);
						
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					InLineItemsVO data=new InLineItemsVO();
					data.setInvoiceID(rs.getInt("invoiceID"));
					data.setChargeID(rs.getInt("id"));
					data.setTotalPrice(rs.getBigDecimal("total_price"));
				    data.setGstPercent(rs.getBigDecimal("gst_percentage"));
				    data.setsGST(rs.getBigDecimal("sgst"));
				    data.setiGST(rs.getBigDecimal("igst"));
				    data.setcGST(rs.getBigDecimal("cgst"));
				    data.setuGST(rs.getBigDecimal("ugst"));
				    data.setCess(rs.getBigDecimal("cess"));
				    data.setDescription(rs.getString("entity_gstin"));
				  
				   
					return data;
				}
			};
			
			invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public List getInvoiceLineItemsListForCDNRAReport(int orgID,String fromDate,String toDate)");	
			}catch(Exception Ex){
				
				logger.error("Exception in getInvoiceLineItemsListForCDNRAReport : "+Ex);
			}
		return invList;

	    }
		
		public List getInvoiceLineItemsListForITCInElgForImportReport(int orgID,String fromDate,String toDate,int gstCategoryID){
			List invList=null;
			try{
			logger.debug("Entry : public List getInvoiceLineItemsListForITCInElgForImportReport(int orgID,String fromDate,String toDate)");
			
			String sqlString=" SELECT i.entity_gstin, ii.id,SUM(ii.total_price) AS total_price,gst_percentage,SUM(ii.igst) AS igst, SUM(ii.sgst) AS sgst, SUM(ii.cgst) AS cgst, SUM(ii.ugst) AS ugst, SUM(ii.cess) AS cess,i.id AS invoiceID,i.* FROM invoice_details i, invoice_line_item_details ii WHERE  i.id=ii.invoice_id AND ii.gst_category_id=:gstCategoryID AND ( i.invoice_date BETWEEN :fromDate AND :toDate) AND i.org_id=:orgID AND i.is_deleted=0  AND (i.invoice_type='Purchase' ) AND i.is_review=0 "
											+" AND (i.entity_gstin IS NOT NULL OR i.entity_gstin!='') AND ii.amount!=ii.total_price GROUP BY i.id   ;";
			Map hMap=new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate",fromDate);
			hMap.put("toDate", toDate);
			hMap.put("gstCategoryID", gstCategoryID);
						
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					InLineItemsVO data=new InLineItemsVO();
					data.setInvoiceID(rs.getInt("invoiceID"));
					data.setChargeID(rs.getInt("id"));
					data.setTotalPrice(rs.getBigDecimal("total_price"));
				    data.setGstPercent(rs.getBigDecimal("gst_percentage"));
				    data.setsGST(rs.getBigDecimal("sgst"));
				    data.setiGST(rs.getBigDecimal("igst"));
				    data.setcGST(rs.getBigDecimal("cgst"));
				    data.setuGST(rs.getBigDecimal("ugst"));
				    data.setCess(rs.getBigDecimal("cess"));
				    data.setDescription(rs.getString("entity_gstin"));
				  
				   
					return data;
				}
			};
			
			invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public List getInvoiceLineItemsListForITCInElgForImportReport(int orgID,String fromDate,String toDate)");	
			}catch(Exception Ex){
				
				logger.error("Exception in getInvoiceLineItemsListForITCElgForImportReport : "+Ex);
			}
		return invList;

	    }

		
		public List getInvoiceLineItemsListForInwardExpSupplyReport(int orgID,String fromDate,String toDate){
			List invList=null;
			try{
			logger.debug("Entry : public List getInvoiceLineItemsListForInwardExpSupplyReport(int orgID,String fromDate,String toDate)");
			
			String sqlString=" SELECT i.entity_gstin, ii.id,SUM(ii.total_price) AS total_price,gst_percentage,SUM(ii.igst) AS igst, SUM(ii.sgst) AS sgst, SUM(ii.cgst) AS cgst, SUM(ii.ugst) AS ugst, SUM(ii.cess) AS cess,i.id AS invoiceID,i.* FROM invoice_details i, invoice_line_item_details ii WHERE  i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) AND  (ii.gst_category_id=3 OR ii.gst_category_id=4 or ii.gst_category_id=2) and i.org_id=:orgID AND i.is_deleted=0  AND (i.invoice_type='Credit Note' or  i.invoice_type='Sales' or (i.invoice_type='Journal' and i.is_sales=1 )) AND i.is_review=0 "
					+"   GROUP BY i.id   ;";
			Map hMap=new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate",fromDate);
			hMap.put("toDate", toDate);
						
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					InvoiceVO data=new InvoiceVO();
					data.setInvoiceID(rs.getInt("invoiceID"));
					data.setTotalAmount(rs.getBigDecimal("total_price"));
				    data.setTotalGSTPercent(rs.getBigDecimal("gst_percentage"));
				    data.setSupplyState(rs.getString("place_of_supply"));
				    data.setInvoiceType(rs.getString("invoice_type"));

				   				  
					return data;
				}
			};
			
			invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public List getInvoiceLineItemsListForInwardExpSupplyReport(int orgID,String fromDate,String toDate)");	
			}catch(Exception Ex){
				
				logger.error("Exception in getInvoiceLineItemsListForInwardExpSupplyReport : "+Ex);
			}
		return invList;

	    }
	
		public List getInvoiceLineItemsListForNonGstInwardSupplyReport(int orgID,String fromDate,String toDate){
			List invList=null;
			try{
			logger.debug("Entry : public List getInvoiceLineItemsListForNonGstInwardSupplyReport(int orgID,String fromDate,String toDate)");
			
			String sqlString=" SELECT i.entity_gstin, ii.id,SUM(ii.total_price) AS total_price,gst_percentage,SUM(ii.igst) AS igst, SUM(ii.sgst) AS sgst, SUM(ii.cgst) AS cgst, SUM(ii.ugst) AS ugst, SUM(ii.cess) AS cess,i.id AS invoiceID,i.* FROM invoice_details i, invoice_line_item_details ii WHERE  i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) AND  (ii.gst_category_id=5 ) and i.org_id=:orgID AND i.is_deleted=0  AND (i.invoice_type='Credit Note' or  i.invoice_type='Sales' or (i.invoice_type='Journal' and i.is_sales=1 )) AND i.is_review=0 "
					+"   GROUP BY i.id   ;";
			Map hMap=new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate",fromDate);
			hMap.put("toDate", toDate);
						
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					InvoiceVO data=new InvoiceVO();
					data.setInvoiceID(rs.getInt("invoiceID"));
					data.setTotalAmount(rs.getBigDecimal("total_price"));
				    data.setTotalGSTPercent(rs.getBigDecimal("gst_percentage"));
				    data.setSupplyState(rs.getString("place_of_supply"));
				    data.setInvoiceType(rs.getString("invoice_type"));
				   
				  
				  
				   
					return data;
				}
			};
			
			invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public List getInvoiceLineItemsListForNonGstInwardSupplyReport(int orgID,String fromDate,String toDate)");	
			}catch(Exception Ex){
				
				logger.error("Exception in getInvoiceLineItemsListForNonGstInwardSupplyReport : "+Ex);
			}
		return invList;

	    }
		
		
		public List getInvoiceListForNonGstInwardSupplyReport(int orgID,String fromDate,String toDate){
			List invList=null;
			try{
			logger.debug("Entry : public List getInvoiceLineItemsListForNonGstInwardSupplyReport(int orgID,String fromDate,String toDate)");
			
			String sqlString=" SELECT i.entity_gstin, ii.id,SUM(ii.total_price) AS total_price,gst_percentage,SUM(ii.igst) AS igst, SUM(ii.sgst) AS sgst, SUM(ii.cgst) AS cgst, SUM(ii.ugst) AS ugst, SUM(ii.cess) AS cess,i.id AS invoiceID,i.* FROM invoice_details i, invoice_line_item_details ii WHERE  i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) AND i.org_id=:orgID AND i.is_deleted=0  AND  (i.invoice_type='Credit Note' or  i.invoice_type='Sales' or (i.invoice_type='Journal' and i.is_sales=1 )) AND i.is_review=0 "
					+"  GROUP BY i.place_of_supply_id   ;";
			Map hMap=new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate",fromDate);
			hMap.put("toDate", toDate);
						
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					InvoiceVO data=new InvoiceVO();
					
					data.setTotalAmount(rs.getBigDecimal("total_price"));
				    data.setTotalGSTPercent(rs.getBigDecimal("igst"));
				    data.setSupplyState(rs.getString("place_of_supply"));
				    data.setSupplyStateID(rs.getString("place_of_supply_id"));
				  		   
					return data;
				}
			};
			
			invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public List getInvoiceLineItemsListForNonGstInwardSupplyReport(int orgID,String fromDate,String toDate)");	
			}catch(Exception Ex){
				
				logger.error("Exception in getInvoiceLineItemsListForNonGstInwardSupplyReport : "+Ex);
			}
		return invList;

	    }
		
		public List getInvoiceListForGstInwardSupplyReportStateWise(int orgID,String fromDate,String toDate,int gstInvoiceTypeID){
			List invList=null;
			try{
			logger.debug("Entry : public List getInvoiceListForGstInwardSupplyReportStateWise(int orgID,String fromDate,String toDate)");
			
			String sqlString=" SELECT i.entity_gstin, ii.id,SUM(ii.total_price) AS total_price,gst_percentage,SUM(ii.igst) AS igst, SUM(ii.sgst) AS sgst, SUM(ii.cgst) AS cgst, SUM(ii.ugst) AS ugst, SUM(ii.cess) AS cess,i.id AS invoiceID,i.* FROM invoice_details i, invoice_line_item_details ii WHERE  i.id=ii.invoice_id AND ( i.invoice_date BETWEEN :fromDate AND :toDate) AND  (i.gst_invoice_type_id=:gstInvoiceTypeID ) and i.org_id=:orgID AND i.is_deleted=0  AND  (i.invoice_type='Credit Note' or  i.invoice_type='Sales' or (i.invoice_type='Journal' and i.is_sales=1 )) AND i.is_review=0 "
					+" AND (i.entity_gstin IS not  NULL OR i.entity_gstin!='')  GROUP BY i.place_of_supply_id   ;";
			Map hMap=new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate",fromDate);
			hMap.put("toDate", toDate);
			hMap.put("gstInvoiceTypeID", gstInvoiceTypeID);
						
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					InvoiceVO data=new InvoiceVO();
					
					data.setTotalAmount(rs.getBigDecimal("total_price"));
				    data.setTotalGSTPercent(rs.getBigDecimal("igst"));
				    data.setSupplyState(rs.getString("place_of_supply"));
				    data.setSupplyStateID(rs.getString("place_of_supply_id")); 
				  
				   
					return data;
				}
			};
			
			invList=namedParameterJdbcTemplate.query(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public List getInvoiceListForGstInwardSupplyReportStateWise(int orgID,String fromDate,String toDate)");	
			}catch(Exception Ex){
				
				logger.error("Exception in getInvoiceListForGstInwardSupplyReportStateWise : "+Ex);
			}
		return invList;

	    }
		
		
		public GstStateDetails getGstStateDetailsFromName(String stateName){
			GstStateDetails gstStateDetails=new GstStateDetails();
			try{
			logger.debug("Entry : public GstStateDetails getGstStateDetailsFromName(String stateName)");
			
			String sqlString=" SELECT * FROM gst_state_details WHERE state_name LIKE '%"+stateName+"%';   ";
			Map hMap=new HashMap();
			
						
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					GstStateDetails data=new GstStateDetails();
					data.setGstStateID(rs.getString("state_id"));
					data.setGstStateName(rs.getString("state_name"));
				    data.setDescription(rs.getString("description"));
				    				   
					return data;
				}
			};
			
			gstStateDetails=(GstStateDetails)namedParameterJdbcTemplate.queryForObject(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public GstStateDetails getGstStateDetailsFromName(String stateName)");	
			}catch(Exception Ex){
				
				logger.error("Exception in public GstStateDetails getGstStateDetailsFromName(String stateName) : "+Ex);
			}
		return gstStateDetails;

	    }
		
		
		public GstStateDetails getGstStateDetailsFromID(int stateID){
			GstStateDetails gstStateDetails=new GstStateDetails();
			try{
			logger.debug("Entry : public GstStateDetails getGstStateDetailsFromID(String stateName)");
			
			String sqlString=" SELECT * FROM gst_state_details WHERE state_id=:stateID';   ";
			Map hMap=new HashMap();
			hMap.put("stateID", stateID);
						
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					GstStateDetails data=new GstStateDetails();
					data.setGstStateID(rs.getString("state_id"));
					data.setGstStateName(rs.getString("state_name"));
				    data.setDescription(rs.getString("description"));
				    				   
					return data;
				}
			};
			
			gstStateDetails=(GstStateDetails)namedParameterJdbcTemplate.queryForObject(sqlString,hMap, RMapper);
			
			logger.debug("Exit : public GstStateDetails getGstStateDetailsFromID(String stateName)");	
			}catch(Exception Ex){
				
				logger.error("Exception in public GstStateDetails getGstStateDetailsFromID(String stateName) : "+Ex);
			}
		return gstStateDetails;

	    }
		
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	
}
