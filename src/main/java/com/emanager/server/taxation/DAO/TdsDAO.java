package com.emanager.server.taxation.DAO;

//import macromedia.asc.parser.ThisExpressionNode;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.emanager.server.accounts.DataAccessObjects.TransactionVO;


public class TdsDAO {
	private static final Logger logger = Logger.getLogger(TdsDAO.class);
	
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	public int insertTDSTransaction(TransactionVO txVO){
		int flag=0;
		
		try{
		logger.debug("Entry : public int insertTDSTransaction(TransactionVO txVO)");
		
		
		logger.debug("Exit : public int insertTDSTransaction(TransactionVO txVO)");	
		}catch(Exception Ex){
			
			logger.error("Exception in insertTDSTransaction : "+Ex);
		}
	return flag;

    }

	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	
}
