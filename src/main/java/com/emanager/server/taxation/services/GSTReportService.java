package com.emanager.server.taxation.services;

import org.apache.log4j.Logger;

import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.taxation.dataAccessObject.gst.B2b;
import com.emanager.server.taxation.dataAccessObject.gst.GstRequestVO;
import com.emanager.server.taxation.dataAccessObject.gst.GstTax;
import com.emanager.server.taxation.dataAccessObject.gstr3b.Gstr3B;
import com.emanager.server.taxation.domainObject.GSTReportDomain;


public class GSTReportService {
	
	GSTReportDomain gstReportDomain;
	

	private static final Logger logger = Logger.getLogger(GSTReportService.class);
	
	
	public GstTax getGST1Report(GstRequestVO gstReqVO){
		GstTax gstVO=new GstTax();
		logger.debug("Entry :  public GstTax getGST1Report(GstRequestVO gstReqVO)");
		
		gstVO=gstReportDomain.getGST1Report(gstReqVO);
		
		logger.debug("Exit :  public GstTax getGST1Report(GstRequestVO gstReqVO)");
		return gstVO;
	}

	public GstTax getGST2Report(GstRequestVO gstReqVO){
		GstTax gstVO=new GstTax();
		logger.debug("Entry :  public GstTax getGST2Report(GstRequestVO gstReqVO)");
		
		gstVO=gstReportDomain.getGST2Report(gstReqVO);
		
		logger.debug("Exit :  public GstTax getGST2Report(GstRequestVO gstReqVO)");
		return gstVO;
	}
	
	public Gstr3B getGSTR3BReport(GstRequestVO gstReqVO){
		Gstr3B gstVO=new Gstr3B();
		logger.debug("Entry :  public Gstr3B getGSTR3BReport(GstRequestVO gstReqVO)");
		
		gstVO=gstReportDomain.getGSTR3BReport(gstReqVO);
		
		logger.debug("Exit :  public Gstr3B getGSTR3BReport(GstRequestVO gstReqVO)");
		return gstVO;
	}
	
	public  GstRequestVO getGstOtpRequest(GstRequestVO gstReqVO){
		GstRequestVO gstVO=new GstRequestVO();
		logger.debug("Entry :  public GstTax getGstOtpRequest(GstRequestVO gstReqVO)");
		
			
		gstVO=gstReportDomain.getGstOtpRequest(gstReqVO);
		
		logger.debug("Exit :  public GstTax getGstOtpRequest(GstRequestVO gstReqVO)");
		return gstVO;
	}
	
	
	public  GstRequestVO getAuthToken(GstRequestVO gstReqVO){
		GstRequestVO gstVO=new GstRequestVO();
		logger.debug("Entry :  public GstTax getAuthToken(GstRequestVO gstReqVO)");
		
		gstVO=gstReportDomain.getAuthToken(gstReqVO);
		
		logger.debug("Exit :  public GstTax getAuthToken(GstRequestVO gstReqVO)");
		return gstVO;
	}
	
	public  GstRequestVO getGSTR2AB2Breport(GstRequestVO gstReqVO){
		GstRequestVO gstVO=new GstRequestVO();
		logger.debug("Entry :  public GstTax getGSTR2AB2Breport(GstRequestVO gstReqVO)");
		
		gstVO=gstReportDomain.getGSTR2AB2Breport(gstReqVO);
		
		logger.debug("Exit :  public GstTax getGSTR2AB2Breport(GstRequestVO gstReqVO)");
		return gstVO;
	}
	
	public  GstRequestVO getGstr1B2Breport(GstRequestVO gstReqVO){
		GstRequestVO gstVO=new GstRequestVO();
		logger.debug("Entry :  public  GstRequestVO getGstr1B2Breport(GstRequestVO gstReqVO)");
		
		gstVO=gstReportDomain.getGSTR1B2Breport(gstReqVO);
		
		logger.debug("Exit :  public  GstRequestVO getGstr1B2Breport(GstRequestVO gstReqVO)");
		return gstVO;
	}
	public  GstRequestVO getCashLedgersreport(GstRequestVO gstReqVO){
		GstRequestVO gstVO=new GstRequestVO();
		logger.debug("Entry :  public GstTax getCashLedgersreport(GstRequestVO gstReqVO)");
		
		gstVO=gstReportDomain.getCashLedgersreport(gstReqVO);
		
		logger.debug("Exit :  public GstTax getCashLedgersreport(GstRequestVO gstReqVO)");
		return gstVO;
	}
	
	public  GstRequestVO saveGSTR1Report(GstRequestVO gstReqVO){
		GstRequestVO gstVO=new GstRequestVO();
		logger.debug("Entry :  public GstTax saveGSTR1Report(GstRequestVO gstReqVO)");
		
		gstVO=gstReportDomain.saveGSTR1Report(gstReqVO);
		
		logger.debug("Exit :  public GstTax saveGSTR1Report(GstRequestVO gstReqVO)");
		return gstVO;
	}
	
	public  GstRequestVO submitGSTR1Report(GstRequestVO gstReqVO){
		GstRequestVO gstVO=new GstRequestVO();
		logger.debug("Entry :  public GstTax submitGSTR1Report(GstRequestVO gstReqVO)");
		
		gstVO=gstReportDomain.submitGSTR1Report(gstReqVO);
		
		logger.debug("Exit :  public GstTax submitGSTR1Report(GstRequestVO gstReqVO)");
		return gstVO;
	}
	
	public  GstRequestVO retFileGSTR1Report(GstRequestVO gstReqVO){
		GstRequestVO gstVO=new GstRequestVO();
		logger.debug("Entry :  public GstTax retFileGSTR1Report(GstRequestVO gstReqVO)");
		
		gstVO=gstReportDomain.retFileGSTR1Report(gstReqVO);
		
		logger.debug("Exit :  public GstTax retFileGSTR1Report(GstRequestVO gstReqVO)");
		return gstVO;
	}

	public  GstRequestVO getGstr1Status(GstRequestVO gstReqVO){
		GstRequestVO gstVO=new GstRequestVO();
		logger.debug("Entry :  public GstTax getGstr1Status(GstRequestVO gstReqVO)");
		
		gstVO=gstReportDomain.getGstr1Status(gstReqVO);
		
		logger.debug("Exit :  public GstTax getGstr1Status(GstRequestVO gstReqVO)");
		return gstVO;
	}
	
	public  GstRequestVO saveGSTR3BReport(GstRequestVO gstReqVO){
		GstRequestVO gstVO=new GstRequestVO();
		logger.debug("Entry :  public GstTax saveGSTR3BReport(GstRequestVO gstReqVO)");
		
		gstVO=gstReportDomain.saveGSTR3BReport(gstReqVO);
		
		logger.debug("Exit :  public GstTax saveGSTR3BReport(GstRequestVO gstReqVO)");
		return gstVO;
	}
	
	
	 //============== Public GST APIs =================//
	public  GstRequestVO getGstInInfoReport(GstRequestVO gstReqVO){
		GstRequestVO gstVO=new GstRequestVO();
		logger.debug("Entry :  public GstTax getGstInInfoReport(GstRequestVO gstReqVO)");
		
		gstVO=gstReportDomain.getGstInInfoReport(gstReqVO);
		
		logger.debug("Exit :  public GstTax getGstInInfoReport(GstRequestVO gstReqVO)");
		return gstVO;
	}
	
	public  GstRequestVO getGstRFiledHistoryReport(GstRequestVO gstReqVO){
		GstRequestVO gstVO=new GstRequestVO();
		logger.debug("Entry :  public GstTax getGstRFiledHistoryReport(GstRequestVO gstReqVO)");
		
		gstVO=gstReportDomain.getGstRFiledHistoryReport(gstReqVO);
		
		logger.debug("Exit :  public GstTax getGstRFiledHistoryReport(GstRequestVO gstReqVO)");
		return gstVO;
	}
	
	/**
	 * @param gstReportDomain the gstReportDomain to set
	 */
	public void setGstReportDomain(GSTReportDomain gstReportDomain) {
		this.gstReportDomain = gstReportDomain;
	}
	



	
}
