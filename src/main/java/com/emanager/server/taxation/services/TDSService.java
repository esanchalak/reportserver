package com.emanager.server.taxation.services;

import org.apache.log4j.Logger;

import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.taxation.domainObject.TDSDomain;

public class TDSService {
	

	private static final Logger logger = Logger.getLogger(TDSService.class);
	TDSDomain TDSDomain;
	
	public TransactionVO addTDSTransaction(TransactionVO txVO,TransactionVO tdsVO,int societyID){
		int flag=0;
		
		try{
		logger.debug("Entry : public int addTDSTransaction(TransactionVO txVO,TransactionVO tdsVO,int societyID)");
		
		tdsVO=TDSDomain.insertTDSTransaction(txVO,tdsVO,societyID);
		
		
		logger.debug("Exit : public int addTDSTransaction(TransactionVO txVO,TransactionVO tdsVO,int societyID)");	
		}catch(Exception Ex){
			
			logger.error("Exception in addTDSTransaction : "+Ex);
		}
	return tdsVO;

    }



	public void setTDSDomain(TDSDomain domain) {
		TDSDomain = domain;
	}
	
}
