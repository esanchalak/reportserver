package com.emanager.server.assets.domainObject;

import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.assets.valueObject.AssetsVO;
import com.emanager.server.assets.valueObject.ContractVO;
import com.emanager.server.assets.valueObject.ContractorVO;
import com.emanager.server.assets.valueObject.MaintainenceVO;
import com.emanager.server.assets.valueObject.ManufactureVO;
import com.emanager.server.assets.dataaccessObject.AssetsDAO;
import com.emanager.server.commonUtils.valueObject.AddressVO;

public class AssetsDomain {
	
	AssetsDAO assetsDAO;
	private static final Logger logger = Logger.getLogger(AssetsDomain.class);
	

	public List assetsDetailList(String assetCateId, String socId){
		
	    List assetList = null;

		try{
		
		logger.debug("Entry : public List assetsDetailList(String assetCateId, String socId)" );
		
		assetList = assetsDAO.assetsDetailList(assetCateId, socId); 		
		
		logger.debug("Exit : public List assetsDetailList(String assetCateId, String socId) " );
		
		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in assetsDetailList : "+Ex);
			
		}
		return assetList;
	}
	
	public List getAssetListCombo(String strSocietyID){
		
	    List assetTypeList = null;

		try{
		
		logger.debug("Entry : public List getAssetListCombo(String strSocietyID) " );
		
		assetTypeList = assetsDAO.searchAssetCombo(strSocietyID);  		
		
		logger.debug("Exit : public List getAssetListCombo(String strSocietyID)" );
		
		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in getAssetListCombo : "+Ex);
			
		}
		return assetTypeList;
	}
	
	public List getContractDetailList(String strSocietyID) {
		 List contractList = null;
			
		try{
			logger.debug("Entry : public List getContractDetailList(String strSocietyID)" );
			
			contractList = assetsDAO.getContractDetailList(strSocietyID);
					
			logger.debug("Exit : public List getContractDetailList(String strSocietyID)" );

			}catch(Exception Ex){
		          logger.error("Exception in getContractDetailList : "+Ex);
		     }
			return contractList;
	}
	public List getHistoryContractList(String strSocietyID) {
		 List hisrcontractList = null;
			
		try{
			logger.debug("Entry : public List getHistoryContractList(String strSocietyID)" );
			
			hisrcontractList = assetsDAO.getHistoryContractList(strSocietyID);
					
			logger.debug("Exit : public List getHistoryContractList(String strSocietyID)" );

			}catch(Exception Ex){
		          logger.error("Exception in getHistoryContractList : "+Ex);
		     }
			return hisrcontractList;
	}
	
	public List getMaintainenceList(String contractId) {
		List maintainenceList = null;
			
		try{			
			logger.debug("Entry : public List getMaintainenceList(String contractId)" );
			
			maintainenceList = assetsDAO.getMaintainenceList(contractId);
					
			logger.debug("Exit : public List getMaintainenceList(String contractId)" );

			}catch(Exception Ex){
				 logger.error("Exception in getMaintainenceList : "+Ex);
		     }
			
			return maintainenceList;
		}
	
	public int insertContractDetails(ContractorVO contractorVO,AddressVO addressVOForContractor, ContractVO contractVO) {

		int sucess = 0;  
		
		try{	
		    logger.debug("Entry : public int insertContractDetails(ContractorVO contractorVO, AddressVO addressVOForContractor, ContractVO contractVO)");
		
		    sucess = assetsDAO.insertContractorDetails(contractorVO,addressVOForContractor,contractVO);
						
		    logger.debug("Exit : public int insertContractDetails(ContractorVO contractorVO, AddressVO addressVOForContractor, ContractVO contractVO) ");

		}catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in insertContractDetails : "+Ex);		
		}
		return sucess;
	}
	
	public int insertMaintainence(MaintainenceVO maintainenceVO) {

		int sucess = 0;  
		
		try{	
		    logger.debug("Entry : public int insertMaintainence(MaintainenceVO maintainenceVO)");
		
		    sucess = assetsDAO.insertMaintainenceDetails(maintainenceVO);
						
		    logger.debug("Exit : public int insertMaintainence(MaintainenceVO maintainenceVO) ");

		}catch(Exception Ex){		
			Ex.printStackTrace();
			logger.error("Exception in insertMaintainence : "+Ex);		
		}
		return sucess;
	}
	
	public int updateContractDetail(ContractVO contractVO,AddressVO addressVO) {

		int sucess = 0;  
		
		try{	
		     logger.debug("Entry : public int updateContractDetail(ContractVO contractVO,AddressVO addressVO)");
		
		     sucess = assetsDAO.updateContractDetail(contractVO,addressVO);
						
		     logger.debug("Exit : public int updateContractDetail(ContractVO contractVO,AddressVO addressVO) ");

		}catch(Exception Ex){		
			Ex.printStackTrace();
			logger.error("Exception in updateContractDetail : "+Ex);		
		}
		return sucess;
	}
	
	public int updateMaintainceDetails(MaintainenceVO maintainenceVO,String maintainenceId) {

		int sucess = 0;  
		
		try{	
		     logger.debug("Entry : public int updateMaintainceDetails(MaintainenceVO maintainenceVO,String maintainenceId)");
		
		      sucess = assetsDAO.updateMaintainceDetails(maintainenceVO,maintainenceId);
						
		     logger.debug("Exit : public int updateMaintainceDetails(MaintainenceVO maintainenceVO,String maintainenceId) ");

		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in updateMaintainceDetails : "+Ex);
			
		}
		return sucess;
	}
	
	public int deleteContractDetails(String contractId){
		int success = 0;
		try{	
		    logger.debug("Entry : public int deleteContractDetails(String contractId)");
		
		    success = assetsDAO.deleteContractDetails(contractId);
				
		   logger.debug("Exit : public int deleteContractDetails(String contractId)");

		}catch(Exception Ex){		
			Ex.printStackTrace();
			logger.error("Exception in deleteContractDetails : "+Ex);	
		}
		return success;
	}
	
	public int deleteMaintainenceDetails(String maintainenceId){
		int success = 0;
		try{	
		    logger.debug("Entry : public int deleteMaintainenceDetails(String maintainenceId)");
		
		    success = assetsDAO.deleteMaintainenceDetails(maintainenceId);
				
		   logger.debug("Exit : public int deleteMaintainenceDetails(String maintainenceId)");

		}catch(Exception Ex){		
			Ex.printStackTrace();
			logger.error("Exception in deleteMaintainenceDetails : "+Ex);	
		}
		return success;
	}
/*public List getFixedList(String strSocietyID){
		
		List fixedAssetList = null;
			
			try{
			
			logger.debug("Entry :public List getFixedList(String strSocietyID)" );
			
			fixedAssetList = assetsDAO.getFixedList(strSocietyID);
					
			logger.debug("Exit: public List getFixedList(String strSocietyID)") ;

			}catch(Exception Ex){				
			   logger.error("Exception in getFixedList : "+Ex);				
			}
			return fixedAssetList;
		}
public List getMovableList(String strSocietyID){
	
	List movableAssetList = null;
		
		try{				
		logger.debug("Entry :public List getMovableList(String strSocietyID)" );
		
		movableAssetList = assetsDAO.getMovableList(strSocietyID);
				
		logger.debug("Exit public List getMovableList(String strSocietyID)") ;

		}catch(Exception Ex){			
	        logger.error("Exception in getMovableList : "+Ex);			
		}
		return movableAssetList;
	}

	public List getFixedAssetList(String strSocietyID,String categoryId){
		
		List fixedAssetList = null;
			
			try{			
			logger.debug("Entry :public List getFixedAssetList(String strSocietyID,String categoryId)" );
			
			fixedAssetList = assetsDAO.getFixedAssetList(strSocietyID,categoryId);
					
			logger.debug("Exit public List getFixedAssetList(String strSocietyID,String categoryId)") ;

			}catch(Exception Ex){				
		        logger.error("Exception in getFixedAssetList : "+Ex);				
			}
			return fixedAssetList;
		}*/
	
	

	
	/*public List getMovableAssetList(String strSocietyID,String categoryId){
		
	    List movableAssetList = null;
		
		try{
		
		logger.debug("Entry : public List getMovableAssetList(String strSocietyID,String categoryId)" );
		
		movableAssetList = assetsDAO.getMovableAssetList(strSocietyID,categoryId);
				
		logger.debug("Exit : public List getMovableAssetList(String strSocietyID,String categoryId)" );

		}catch(Exception Ex){
			logger.error("Exception in getMovableAssetList : "+Ex);
		 }
		return movableAssetList;
	}*/

	
/*public int insertMovableAssetDetails(AssetsVO assetVO,ManufactureVO manufactureVO,AddressVO addressVO){
		
		int sucess = 0;  
	try{
		
		logger.debug("Entry : public int insertMovableAssetDetails(AssetsVO assetVO,ManufactureVO manufactureVO,AddressVO addressVO)");
		
		sucess = assetsDAO.insertManufactureDetails(assetVO,manufactureVO,addressVO);
				
		logger.debug("Exit : public int insertMovableAssetDetails(AssetsVO assetVO,ManufactureVO manufactureVO,AddressVO addressVO)");

		}catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in insertMovableAssetDetails : "+Ex);
		 }
		return sucess;
	}

public int insertFixedAssetDetails(AssetsVO assetVO,ManufactureVO manufactureVO){
	int sucess = 0;
	int genratedID=0;
	
	try{
		logger.debug("Entry : public int insertFixedAssetDetails(AssetsVO assetVO,ManufactureVO manufactureVO)");
		
	    sucess = assetsDAO.insertAssetDetails(assetVO, manufactureVO,genratedID);
	    
		logger.debug("Exit : public int insertFixedAssetDetails(AssetsVO assetVO,ManufactureVO manufactureVO) ");

	}catch(Exception Ex){
		Ex.printStackTrace();
		logger.error("Exception in insertFixedAssetDetails : "+Ex);
	}
	return sucess;
}*/
	


/*public int updateFixedAsset(AssetsVO assetsVO) {

	int sucess = 0;  
	
	try{	
	    logger.debug("Entry : public int updateFixedAsset(AssetsVO assetsVO)");
	
	    sucess = assetsDAO.updateFixedAsset(assetsVO);
					
	    logger.debug("Exit : public int updateFixedAsset(AssetsVO assetsVO) ");

	}catch(Exception Ex){		
		Ex.printStackTrace();
		logger.error("Exception in insertMaintainence : "+Ex);		
	}
	return sucess;
}
public int updateMovableAsset(AssetsVO assetsVO,AddressVO addressVO) {

	int sucess = 0;  
	
	try{	
	    logger.debug("Entry : public int updateMovableAsset(AssetsVO assetsVO,AddressVO addressVO)");
	
	    sucess = assetsDAO.updateMovableAsset(assetsVO,addressVO);
					
	    logger.debug("Exit : public int updateMovableAsset(AssetsVO assetsVO,AddressVO addressVO) ");

	}catch(Exception Ex){		
		Ex.printStackTrace();
		logger.error("Exception in updateMovableAsset : "+Ex);		
	}
	return sucess;
}*/



/*
public int deleteAssetDetails(String assetId){
	int success = 0;
	try{	
	    logger.debug("Entry : public int deleteAssetDetails(String assetId)");
	
	    success = assetsDAO.deleteAssetDetails(assetId);
			
	   logger.debug("Exit : public int deleteAssetDetails(String assetId)");

	}catch(Exception Ex){		
		Ex.printStackTrace();
		logger.error("Exception in deleteAssetDetails : "+Ex);	
	}
	return success;
}
*/


public AssetsDAO getAssetsDAO() {
	return assetsDAO;
}

public void setAssetsDAO(AssetsDAO assetsDAO) {
	this.assetsDAO = assetsDAO;
}

}
