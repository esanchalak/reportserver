package com.emanager.server.assets.services;

import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.assets.domainObject.AssetsDomain;
import com.emanager.server.assets.valueObject.ContractVO;
import com.emanager.server.assets.valueObject.ContractorVO;
import com.emanager.server.assets.valueObject.MaintainenceVO;
import com.emanager.server.commonUtils.valueObject.AddressVO;


public class AssetsService {
		
	AssetsDomain assetsDomain;
	private static final Logger logger = Logger.getLogger(AssetsService.class);
	
	
  public List assetsDetailList(String assetCateId, String socId){
		
	    List assetList = null;

		try{
		
		logger.debug("Entry : public List assetsDetailList(String assetCateId, String socId)" );
		
		assetList = assetsDomain.assetsDetailList(assetCateId, socId); 		
		
		logger.debug("Exit : public List assetsDetailList(String assetCateId, String socId) " );
		
		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in assetsDetailList : "+Ex);
			
		}
		return assetList;
	}
	
	public List getAssetListCombo(String strSocietyID ){
	    List assetTypeList = null;
		
		try{
		
		     logger.debug("Entry public List getAssetListCombo(String strSocietyID ,String type)" );
		     
		     assetTypeList = assetsDomain.getAssetListCombo(strSocietyID);
				
		     logger.debug("Exit public List getAssetListCombo(String strSocietyID)" );

		}catch(Exception Ex){		
			logger.error("Exception in getAssetListCombo : "+Ex);		
		}
		return assetTypeList;
	}
	
	public List getContractDetailList(String strSocietyID) {
		 List contractList = null;
			
		try{		
			logger.debug("Entry : public List getContractDetailList(String strSocietyID)" );
			
			contractList = assetsDomain.getContractDetailList(strSocietyID);
					
			logger.debug("Exit : public List getContractDetailList(String strSocietyID)" );

			}catch(Exception Ex){
				logger.error("Exception in getContractDetailList : "+Ex);				
			}
			return contractList;
	}
	
	public List getHistoryContractList(String strSocietyID) {
		 List hisrcontractList = null;
			
		try{
			logger.debug("Entry : public List getHistoryContractList(String strSocietyID)" );
			
			hisrcontractList = assetsDomain.getHistoryContractList(strSocietyID);
					
			logger.debug("Exit : public List getHistoryContractList(String strSocietyID)" );

			}catch(Exception Ex){
		          logger.error("Exception in getHistoryContractList : "+Ex);
		     }
			return hisrcontractList;
	}

	public List getMaintainenceList(String contractId) {
		 List maintainenceList = null;
			
		try{		
			logger.debug("Entry : public List getMaintainenceList(String contractId) " );
			
			maintainenceList = assetsDomain.getMaintainenceList(contractId);
					
			logger.debug("Exit : public List getMaintainenceList(String contractId) " );

			}catch(Exception Ex){			
				logger.error("Exception in getMaintainenceList : "+Ex);			
			}
			return maintainenceList;
		}
       
	public int insertContractDetails(ContractorVO contractorVO,AddressVO addressVOForContractor, ContractVO contractVO){
		int sucess = 0;
		
		try{
			logger.debug("Entry : public int insertContractDetails(ContractorVO contractorVO,AddressVO addressVOForContractor, ContractVO contractVO)");
		    
			sucess = assetsDomain.insertContractDetails(contractorVO,addressVOForContractor,contractVO);
			
			if(sucess == 1){
				logger.info("Asset ID '"+contractVO.getAssetID()+"' has contract details added sucessfully.");
			}	
		     logger.debug("Exit : public int insertContractDetails(ContractorVO contractorVO,AddressVO addressVOForContractor, ContractVO contractVO)");

		}catch(Exception Ex){		
			Ex.printStackTrace();
			logger.error("Exception in insertContractDetails : "+Ex);		
		}
		return sucess;
	}

	public int insertMaintainence(MaintainenceVO maintainenceVO) {

		int sucess = 0;  
		
		try{
		    logger.debug("Entry : public int insertMaintainence(MaintainenceVO maintainenceVO)");
		
		    sucess = assetsDomain.insertMaintainence(maintainenceVO);
		    if(sucess == 1){
				logger.info("Asset ID '"+maintainenceVO.getAssetId()+"' has maintainence details added sucessfully.");
			}	
			logger.debug("Exit : public int insertMaintainence(MaintainenceVO maintainenceVO) ");

		}catch(Exception Ex){		
			Ex.printStackTrace();
			logger.error("Exception in insertMaintainence : "+Ex);		
		}
		return sucess;
		
	}
	
	public int updateContractDetail(ContractVO contractVO,AddressVO addressVO) {

		int sucess = 0;  
		
		try{	
		    logger.debug("Entry : public int updateContractDetail(ContractVO contractVO,AddressVO addressVO)");
		
		    sucess = assetsDomain.updateContractDetail(contractVO,addressVO);
		    
		    if(sucess == 1){
				logger.info("Contract ID '"+contractVO.getAssetID()+"' has contract details updated sucessfully.");
			}	
			logger.debug("Exit :public int updateContractDetail(ContractVO contractVO,AddressVO addressVO) ");

		}catch(Exception Ex){		
			Ex.printStackTrace();
			logger.error("Exception in updateContractDetail : "+Ex);			
		}
		return sucess;
	}
	
	public int updateMaintainceDetails(MaintainenceVO maintainenceVO,String maintainenceId) {

		int sucess = 0;  
		
		try{	
		    logger.debug("Entry : public int updateMaintainceDetails(MaintainenceVO maintainenceVO,String maintainenceId))");
		
		    sucess = assetsDomain.updateMaintainceDetails(maintainenceVO,maintainenceId);
		    if(sucess == 1){
				logger.info("Maintainence ID '"+maintainenceId+"' has maintainence details updated sucessfully.");
			}				
		    logger.debug("Exit : public int updateMaintainceDetails(MaintainenceVO maintainenceVO,String maintainenceId)");

		}catch(Exception Ex){		
			Ex.printStackTrace();
			logger.error("Exception in updateMaintainceDetails : "+Ex);			
		}
		return sucess;
	}
	
	public int deleteContractDetails(String contractId){
		int success = 0;
		try{	
		    logger.debug("Entry : public int deleteContractDetails(String contractId)");
		
		    success = assetsDomain.deleteContractDetails(contractId);
				
		   logger.debug("Exit : public int deleteContractDetails(String contractId)");

		}catch(Exception Ex){		
			Ex.printStackTrace();
			logger.error("Exception in deleteContractDetails : "+Ex);	
		}
		return success;
	}
	
	public int deleteMaintainenceDetails(String maintainenceId){
		int success = 0;
		try{	
		    logger.debug("Entry : public int deleteMaintainenceDetails(String maintainenceId)");
		
		    success = assetsDomain.deleteMaintainenceDetails(maintainenceId);
				
		   logger.debug("Exit : public int deleteMaintainenceDetails(String maintainenceId)");

		}catch(Exception Ex){		
			Ex.printStackTrace();
			logger.error("Exception in deleteMaintainenceDetails : "+Ex);	
		}
		return success;
	}
	
	

/*public List getFixedList(String strSocietyID){
		
	List fixedAssetList = null;
			
	try{			
		logger.debug("Entry : public List getFixedList(String strSocietyID)" );
			
		fixedAssetList = assetsDomain.getFixedList(strSocietyID);
					
		logger.debug("Exit : public List getFixedList(String strSocietyID)") ;

		}catch(Exception Ex){
			logger.error("Exception in getFixedList : "+Ex);	
		}
		   return fixedAssetList;
		}

public List getMovableList(String strSocietyID){
	
	   List movableAssetList = null;
		
	try{				
		 logger.debug("Entry : public List getMovableList(String strSocietyID)" );
		
		 movableAssetList = assetsDomain.getMovableList(strSocietyID);
				
		 logger.debug("Exit : public List getMovableList(String strSocietyID)") ;

	}catch(Exception Ex){			
		logger.error("Exception in getMovableList : "+Ex);	
	 }
		return movableAssetList;
	}	

public List getFixedAssetList(String strSocietyID,String categoryId){
		
    List fixedAssetList = null;
	
	try{	 
	     logger.debug("Entry : public List getFixedAssetList(String strSocietyID,String categoryId)" );
	     
	     fixedAssetList = assetsDomain.getFixedAssetList(strSocietyID,categoryId);
			
	     logger.debug("Exit : public List getFixedAssetList(String strSocietyID,String categoryId)" );

	}catch(Exception Ex){
		logger.error("Exception in getFixedAssetList : "+Ex);			
	}
	return fixedAssetList;
}*/



/*public List getMovableAssetList(String strSocietyID,String categoryId){
	
    List movableAssetList = null;
	
	try{	  
		 logger.debug("Entry public List getMovableAssetList(String strSocietyID,String categoryId)" );
	     
		 movableAssetList = assetsDomain.getMovableAssetList(strSocietyID,categoryId);
			
	     logger.debug("Exit public List getMovableAssetList(String strSocietyID,String categoryId)" );

	}catch(Exception Ex){		
		logger.error("Exception in getMovableAssetList : "+Ex);		
	}
	return movableAssetList;
}
*/

/*public int insertMovableAssetDetails(AssetsVO assetVO,ManufactureVO manufactureVO,AddressVO addressVO){
	int sucess = 0;
	
	try{
		logger.debug("Entry : public int insertMovableAssetDetails(AssetsVO assetVO,ManufactureVO manufactureVO,AddressVO addressVO)");
	   
		sucess = assetsDomain.insertMovableAssetDetails(assetVO,manufactureVO,addressVO);
		
		if(sucess == 1){
			logger.info("Movable asset category '"+assetVO.getAssetCategoryName()+"' has asset details added sucessfully.");
		}		
	    logger.debug("Exit : public int insertMovableAssetDetails(AssetsVO assetVO,ManufactureVO manufactureVO,AddressVO addressVO)");

	}catch(Exception Ex){		
		Ex.printStackTrace();
		logger.error("Exception in insertMovableAssetDetails : "+Ex);			
	}
	return sucess;
}
public int insertFixedAssetDetails(AssetsVO assetVO,ManufactureVO manufactureVO){
	int sucess = 0;
	
	try{ 
		logger.debug("Entry : public int insertFixedAssetDetails(AssetsVO assetVO,ManufactureVO manufactureVO)");
		
	    sucess = assetsDomain.insertFixedAssetDetails(assetVO,manufactureVO);
	    
	    if(sucess == 1){
			logger.info("Fixed asset category '"+assetVO.getAssetCategoryName()+"' has asset details added sucessfully.");
		}
		
	    logger.debug("Exit : public int insertFixedAssetDetails(AssetsVO assetVO,ManufactureVO manufactureVO) ");

	}catch(Exception Ex){		
		Ex.printStackTrace();
		logger.error("Exception in insertFixedAssetDetails : "+Ex);		
	}
	return sucess;
}*/


/*public int updateFixedAsset(AssetsVO assetsVO) {

	int sucess = 0;  
	
	try{
	
	    logger.debug("Entry : public int updateFixedAsset(AssetsVO assetsVO)");
	
	    sucess = assetsDomain.updateFixedAsset(assetsVO);
		
	    if(sucess == 1){
			logger.info("Fixed Asset ID '"+assetsVO.getAssetId()+"' has asset details updated sucessfully.");
		}	
	    logger.debug("Exit : public int updateFixedAsset(AssetsVO assetsVO) ");

	}catch(Exception Ex){		
		Ex.printStackTrace();
		logger.error("Exception in updateFixedAsset : "+Ex);			
	}
	return sucess;
}
public int updateMovableAsset(AssetsVO assetsVO,AddressVO addressVO) {

	int sucess = 0;  
	
	try{	
	    logger.debug("Entry : public int updateMovableAsset(AssetsVO assetsVO,AddressVO addressVO)");
	
	    sucess = assetsDomain.updateMovableAsset(assetsVO,addressVO);
	    
	    if(sucess == 1){
			logger.info("Movable Asset ID '"+assetsVO.getAssetId()+"' has asset details updated sucessfully.");
		}	
		logger.debug("Exit : public int updateMovableAsset(AssetsVO assetsVO,AddressVO addressVO) ");

	}catch(Exception Ex){		
		Ex.printStackTrace();
		logger.error("Exception in updateMovableAsset : "+Ex);				
	}
	return sucess;
}*/

/*public int deleteAssetDetails(String assetId){
	
    int success = 0;
try{
    logger.debug("Entry : public int deleteAssetDetails(String assetId)");

    success = assetsDomain.deleteAssetDetails(assetId);
		
   logger.debug("Exit : public int deleteAssetDetails(String assetId)");

}catch(Exception Ex){	
	Ex.printStackTrace();
	logger.error("Exception in deleteAssetDetails : "+Ex);	
}
return success;
}*/

public AssetsDomain getAssetsDomain() {
	return assetsDomain;
}

public void setAssetsDomain(AssetsDomain assetsDomain) {
	this.assetsDomain = assetsDomain;
}

}
