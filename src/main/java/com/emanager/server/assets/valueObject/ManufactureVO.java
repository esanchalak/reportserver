package com.emanager.server.assets.valueObject;

public class ManufactureVO {
	
	private String manufactureId;
	private String manufactureName;
	
	public String getManufactureId() {
		return manufactureId;
	}
	public void setManufactureId(String manufactureId) {
		this.manufactureId = manufactureId;
	}
	public String getManufactureName() {
		return manufactureName;
	}
	public void setManufactureName(String manufactureName) {
		this.manufactureName = manufactureName;
	}

}
