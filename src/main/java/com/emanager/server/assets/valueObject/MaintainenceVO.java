package com.emanager.server.assets.valueObject;

public class MaintainenceVO {
	
	private int maintainenceId;
	private String period;
	private String description;
	private String persName;
	private String maintainenceDate;
	private String maintainenceNextDate;
	private String assetId;
	private String status;
	private String contractId;
	
	
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMaintainenceDate() {
		return maintainenceDate;
	}
	public void setMaintainenceDate(String maintainenceDate) {
		this.maintainenceDate = maintainenceDate;
	}
	
	public int getMaintainenceId() {
		return maintainenceId;
	}
	public void setMaintainenceId(int maintainenceId) {
		this.maintainenceId = maintainenceId;
	}
	public String getMaintainenceNextDate() {
		return maintainenceNextDate;
	}
	public void setMaintainenceNextDate(String maintainenceNextDate) {
		this.maintainenceNextDate = maintainenceNextDate;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String getPersName() {
		return persName;
	}
	public void setPersName(String persName) {
		this.persName = persName;
	}
	
	
	

}
