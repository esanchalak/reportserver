package com.emanager.server.assets.valueObject;

import java.math.BigDecimal;

public class ContractVO {
	
	private int contractId;
	private String contractStartDate;
	private String contractEndDate;
	private String contraDescription;
	private String contactPersonName;
	private String contactPersMobile;
	private String contractStatus;
	private String contractorId;
	private String contraName;
	private String assetID;
	private String socID;
	private String assetCategryName;
	private String assetCatId;
	private String assetName;
	private String uniqueId;
	private BigDecimal contractAmount;
	
	
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	public BigDecimal getContractAmount() {
		return contractAmount;
	}
	public void setContractAmount(BigDecimal contractAmount) {
		this.contractAmount = contractAmount;
	}
	public String getAssetCatId() {
		return assetCatId;
	}
	public void setAssetCatId(String assetCatId) {
		this.assetCatId = assetCatId;
	}
	public String getAssetCategryName() {
		return assetCategryName;
	}
	public void setAssetCategryName(String assetCategryName) {
		this.assetCategryName = assetCategryName;
	}
	public String getSocID() {
		return socID;
	}
	public void setSocID(String socID) {
		this.socID = socID;
	}
	public String getAssetID() {
		return assetID;
	}
	public void setAssetID(String assetID) {
		this.assetID = assetID;
	}
	public String getContraName() {
		return contraName;
	}
	public void setContraName(String contraName) {
		this.contraName = contraName;
	}
	
	public String getContractorId() {
		return contractorId;
	}
	public void setContractorId(String contractorId) {
		this.contractorId = contractorId;
	}
	public int getContractId() {
		return contractId;
	}
	public void setContractId(int contractId) {
		this.contractId = contractId;
	}
	public String getContactPersMobile() {
		return contactPersMobile;
	}
	public void setContactPersMobile(String contactPersMobile) {
		this.contactPersMobile = contactPersMobile;
	}
	public String getContactPersonName() {
		return contactPersonName;
	}
	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}
	public String getContractEndDate() {
		return contractEndDate;
	}
	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	public String getContractStartDate() {
		return contractStartDate;
	}
	public void setContractStartDate(String contractStartDate) {
		this.contractStartDate = contractStartDate;
	}
	public String getContractStatus() {
		return contractStatus;
	}
	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}
	public String getContraDescription() {
		return contraDescription;
	}
	public void setContraDescription(String contraDescription) {
		this.contraDescription = contraDescription;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

    
	
	
	
}
