package com.emanager.server.assets.valueObject;

import com.emanager.server.commonUtils.valueObject.AddressVO;

public class AssetsVO {
  
  private int assetId;	
  private String societyId;	
  private String assetUniqueId;
  private String assetName;
  private String assetDescription;
  private String purchasedDate;
  private String warranty;
  private String assetListID;
  private String assetType;
  private String assetCategoryName;
  private String assetCatId;
  private String mfgId;
  private int contrId;
  private String mfgName;
  private String maintanceId;
  private int isDeleted;
  
public int getIsDeleted() {
	return isDeleted;
}
public void setIsDeleted(int isDeleted) {
	this.isDeleted = isDeleted;
}
public String getMaintanceId() {
	return maintanceId;
}
public void setMaintanceId(String maintanceId) {
	this.maintanceId = maintanceId;
}
public String getMfgName() {
	return mfgName;
}
public void setMfgName(String mfgName) {
	this.mfgName = mfgName;
}
public int getContrId() {
	return contrId;
}
public void setContrId(int contrId) {
	this.contrId = contrId;
}

public String getMfgId() {
	return mfgId;
}
public void setMfgId(String mfgId) {
	this.mfgId = mfgId;
}
public String getAssetCatId() {
	return assetCatId;
}
public void setAssetCatId(String assetCatId) {
	this.assetCatId = assetCatId;
}

public int getAssetId() {
	return assetId;
}
public void setAssetId(int assetId) {
	this.assetId = assetId;
}
public String getSocietyId() {
	return societyId;
}
public void setSocietyId(String societyId) {
	this.societyId = societyId;
}
public String getAssetCategoryName() {
	return assetCategoryName;
}
public void setAssetCategoryName(String assetCategoryName) {
	this.assetCategoryName = assetCategoryName;
}
public String getAssetDescription() {
	return assetDescription;
}
public void setAssetDescription(String assetDescription) {
	this.assetDescription = assetDescription;
}
public String getAssetListID() {
	return assetListID;
}
public void setAssetListID(String assetListID) {
	this.assetListID = assetListID;
}
public String getAssetName() {
	return assetName;
}
public void setAssetName(String assetName) {
	this.assetName = assetName;
}
public String getAssetType() {
	return assetType;
}
public void setAssetType(String assetType) {
	this.assetType = assetType;
}
public String getAssetUniqueId() {
	return assetUniqueId;
}
public void setAssetUniqueId(String assetUniqueId) {
	this.assetUniqueId = assetUniqueId;
}
public String getPurchasedDate() {
	return purchasedDate;
}
public void setPurchasedDate(String purchasedDate) {
	this.purchasedDate = purchasedDate;
}
public String getWarranty() {
	return warranty;
}
public void setWarranty(String warranty) {
	this.warranty = warranty;
}
  
  
	
}
