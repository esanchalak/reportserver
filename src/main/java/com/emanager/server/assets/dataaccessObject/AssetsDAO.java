package com.emanager.server.assets.dataaccessObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.emanager.server.assets.valueObject.ContractVO;
import com.emanager.server.assets.valueObject.ContractorVO;
import com.emanager.server.assets.valueObject.MaintainenceVO;
import com.emanager.server.commonUtils.dataaccessObject.AddressDAO;
import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.commonUtils.valueObject.DropDownVO;

public class AssetsDAO {
	
	private JdbcTemplate jdbcTemplate;
    public AddressDAO addressDAO;
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	private static final Logger logger = Logger.getLogger(AssetsDAO.class);

	public List assetsDetailList(String assetCateId, String socId) {

		List assetList = null;

		try {
			logger.debug("public List assetsDetailList(String assetCateId, String socId)");
		
		String strSQL =  " SELECT asset_name,asset_id FROM assets_details WHERE asset_category_id="+assetCateId+" AND society_id="+socId+"; "; 
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  DropDownVO drpDwn = new DropDownVO();
			            drpDwn.setData(rs.getString("asset_id"));
			            drpDwn.setLabel(rs.getString("asset_name"));
			            return drpDwn;
				}
			};

			assetList = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);

			logger.debug("outside query searchAssetCombo : " + assetList.size());
			logger.debug("Exit: public List searchAssetCombo(String assetCateId, String socId)");
		} catch (NullPointerException ex) {
			logger.error("Exception in assetsDetailList : "+ex);

			
		} catch (Exception ex) {
			logger.error("Exception in assetsDetailList : "+ex);

		}
		return assetList;

	}
	public List searchAssetCombo(String strSocietyID) {

		List assetTypeList = null;

		try {
			logger.debug("Getting inside method public List searchAssetCombo(String strSocietyID,String type)");
		
		String strSQL =  " SELECT * FROM assets_category_list "; 
		
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  DropDownVO drpDwn = new DropDownVO();
			            drpDwn.setData(rs.getString("asset_category_id"));
			            drpDwn.setLabel(rs.getString("asset_category_name"));
			            return drpDwn;
				}
			};

			assetTypeList = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);

			logger.debug("outside query searchAssetCombo : " + assetTypeList.size());
			logger.debug("Exit: public List searchAssetCombo(String strSocietyID,String type)");
		} catch (Exception ex) {
			logger.error("Exception in searchAssetCombo : "+ex);

		}
		return assetTypeList;

	}
	
	
	public List getContractDetailList(String strSocietyID) {

		List contractList = null;

		try {
			logger.debug("Entry : public List getContractDetailList(String strSocietyID)");
		
	 String strSQL= "SELECT asset_unique_id,asset_name,assets_details.asset_id,contractor_details.contractor_name,contract_details.*,assets_category_list.asset_category_id,asset_category_name FROM assets_details,contract_details,contractor_details,assets_category_list "   
		 			+ " WHERE assets_details.society_id=:society_ID  "  
		 			+ " AND assets_details.asset_id=contract_details.asset_id "
	                + " AND contract_details.contractor_id=contractor_details.contractor_id "
	                + " AND assets_category_list.asset_category_id=assets_details.asset_category_id "
	                + " AND contract_status ='1' ORDER BY contract_details.contract_id  DESC  ";
			                      
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("society_ID", strSocietyID);
			//namedParameters.addValue("assetId",assetId );
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					ContractVO contract = new ContractVO();
					contract.setContractId(rs.getInt("contract_id"));
					contract.setContractorId(rs.getString("contractor_id"));
					contract.setContractStartDate(rs.getString("contract_start_date"));
					contract.setContractEndDate(rs.getString("contract_end_date"));
					contract.setContraDescription(rs.getString("contract_description"));
				    contract.setContactPersMobile(rs.getString("cont_mobile"));
				    contract.setContraName(rs.getString("contractor_name"));
				    contract.setContactPersonName(rs.getString("contact_person_name"));
				    contract.setAssetID(rs.getString("asset_id"));
				    contract.setContractStatus(rs.getString("contract_status"));
				    contract.setAssetCategryName(rs.getString("asset_category_name"));
				    contract.setAssetCatId(rs.getString("asset_category_id"));
				    contract.setUniqueId(rs.getString("asset_unique_id"));
				    contract.setContractAmount(rs.getBigDecimal("contract_amount"));
				    contract.setAssetName(rs.getString("asset_name"));
				  
					return contract;
				}
			};

			contractList = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);

			logger.debug("outside query : " + contractList.size());
			logger.debug("Exit : public List getContractDetailList(String strSocietyID,String assetId)");
			
		    }catch(EmptyResultDataAccessException Ex) {
	        	logger.info("No contract details are available  ");

	    	} catch (Exception ex) {
			   logger.error("Exception in getContractDetailList : "+ex);
	        }
		return contractList;

	}
	
	public List getHistoryContractList(String strSocietyID) {

		List historycontractList = null;

		try {
			logger.debug("Entry : public List getHistoryContractList(String strSocietyID)");
		
	 String strSQL= "SELECT asset_unique_id,asset_name,assets_details.asset_id,contractor_details.contractor_name,contract_details.*,assets_category_list.asset_category_id,asset_category_name FROM assets_details,contract_details,contractor_details,assets_category_list "   
		 			+ " WHERE assets_details.society_id=:society_ID  "  
		 			+ " AND assets_details.asset_id=contract_details.asset_id "
	                + " AND contract_details.contractor_id=contractor_details.contractor_id "
	                + " AND assets_category_list.asset_category_id=assets_details.asset_category_id "
	                + " AND contract_status ='0' ORDER BY contract_details.contract_id  DESC  ";
			                      
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("society_ID", strSocietyID);
			//namedParameters.addValue("assetId",assetId );
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					ContractVO contract = new ContractVO();
					contract.setContractId(rs.getInt("contract_id"));
					contract.setContractorId(rs.getString("contractor_id"));
					contract.setContractStartDate(rs.getString("contract_start_date"));
					contract.setContractEndDate(rs.getString("contract_end_date"));
					contract.setContraDescription(rs.getString("contract_description"));
				    contract.setContactPersMobile(rs.getString("cont_mobile"));
				    contract.setContraName(rs.getString("contractor_name"));
				    contract.setContactPersonName(rs.getString("contact_person_name"));
				    contract.setAssetID(rs.getString("asset_id"));
				    contract.setContractStatus(rs.getString("contract_status"));
				    contract.setAssetCategryName(rs.getString("asset_category_name"));
				    contract.setAssetCatId(rs.getString("asset_category_id"));
				    contract.setUniqueId(rs.getString("asset_unique_id"));
				    contract.setContractAmount(rs.getBigDecimal("contract_amount"));
				    contract.setAssetName(rs.getString("asset_name"));
				  
					return contract;
				}
			};

			historycontractList = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);

			logger.info("outside query : " + historycontractList.size());
			logger.debug("Exit : public List getHistoryContractList(String strSocietyID,String assetId)");
			
		    }catch(EmptyResultDataAccessException Ex) {
	        	logger.info("No contract details are available  ");

	    	} catch (Exception ex) {
			   logger.error("Exception in getHistoryContractList : "+ex);
	        }
		return historycontractList;

	}
	
	
	public List getMaintainenceList(String contractId) {

		List assetMaintainenceList = null;

		try {
			 logger.debug("Entry : public List getMaintainenceList(String contractId)");
		
	String strSQL = "  SELECT maintainence_history.* FROM maintainence_history,contract_details "
	     			+ " WHERE maintainence_history.contract_id=:contractId " 
	     			+ " AND maintainence_history.contract_id=contract_details.contract_id "
	     			+ " ORDER BY maintainence_hs_id DESC ; ";
		                      
			//1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.		
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			//namedParameters.addValue("society_ID", strSocietyID);
			namedParameters.addValue("contractId", contractId);
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					MaintainenceVO maintainenceVO = new MaintainenceVO();
					maintainenceVO.setContractId(rs.getString("contract_id"));
					maintainenceVO.setDescription(rs.getString("description"));
					maintainenceVO.setMaintainenceDate(rs.getString("maintainence_date"));
					maintainenceVO.setMaintainenceId(rs.getInt("maintainence_hs_id"));
					maintainenceVO.setMaintainenceNextDate(rs.getString("maintainence_next_date"));
					maintainenceVO.setPersName(rs.getString("maintainence_person_name"));
				    maintainenceVO.setStatus(rs.getString("maintainence_status"));
					return maintainenceVO;
				}
			};

			assetMaintainenceList = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);

			logger.debug("outside query : " + assetMaintainenceList.size());
			logger.debug("Exit : public List getMaintainenceList(String contractId)");
			
		}catch (EmptyResultDataAccessException Ex) {
	    	logger.info("No Maintainence details are available  ");
		
		} catch (Exception ex) {
			logger.error("Exception in getMaintainenceList : "+ex);

		}
		return assetMaintainenceList;

	}
	
	public int insertContractorDetails(ContractorVO contractorVO,AddressVO addressVOForContractor,ContractVO contractVO) {
		
		int flag = 0;
	    int insertFlag = 0;
	     int success = 0;
	     int genratedID = 0;
	     int successForAddress = 0;
	     
	     try {
	    	 logger.debug("Entry : public int insertContractorDetails(ContractorVO contractorVO,AddressVO addressVOForContractor,ContractVO contractVO) ");
	    	 
	         String insertFileString = "INSERT INTO contractor_details(contractor_name) VALUES(:contractorName);";
	         logger.debug("query : " + insertFileString);
	         
	         SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(
	        		 contractorVO);
	 	
	 	     KeyHolder keyHolder = new GeneratedKeyHolder();
	 	     insertFlag = getNamedParameterJdbcTemplate().update(
	 			    insertFileString, fileParameters, keyHolder);
	 	    genratedID = keyHolder.getKey().intValue();
	 	     logger.debug("key GEnereator ---" + genratedID);
	        
	 	     int memberId = genratedID;
	           
	         if (insertFlag != 0){
	    	     success = insertContract(contractorVO,addressVOForContractor,contractVO,genratedID);
	         }
	         if(success == 0){
	        	 String sql = "DELETE FROM contractor_details  WHERE contractor_id=:genID; ";
	             
		         Map hmap=new HashMap();
		         hmap.put("genID", genratedID);
		     
		         flag=namedParameterJdbcTemplate.update(sql, hmap);
	         }else if(success == 1){
	        	 successForAddress = addressDAO.addNewAddressDetails(addressVOForContractor,memberId); 
	         }
	         if(successForAddress == 1){
	    	     insertFlag = 1;
	         }
	        logger.debug("Exit :public int insertContractorDetails(ContractorVO contractorVO,AddressVO addressVOForContractor,ContractVO contractVO)"+insertFlag);
	       
	       }catch (DataAccessException exc){
	    	    logger.error("DataAccessException in insertContractorDetails : "+exc);
	       }
	       catch (Exception e){
	          logger.error("Exception in insertContractorDetails : "+e);
	       }

	     return insertFlag;
	}

	public int insertContract(ContractorVO contractorVO,AddressVO addressVOForContractor,ContractVO contractVO,int genratedID){
			int insertFlag = 0;
					
			logger.debug("Entry : public int insertContract(ContractorVO contractorVO,AddressVO addressVOForContractor,ContractVO contractVO,int genratedID)");
			try {
				contractVO.setContractorId(String.valueOf(genratedID));
				String insertFileString = "INSERT INTO contract_details "
				+ "( contract_start_date, contract_end_date, contract_description, contact_person_name, cont_mobile, contract_status, contractor_id, asset_id, contract_amount ) VALUES "
				+ "( :contractStartDate, :contractEndDate, :contraDescription , :contactPersonName, :contactPersMobile, :contractStatus,:contractorId, :assetID, :contractAmount); ";

				logger.debug("query : " + insertFileString);
				
				 Map hmap=new HashMap();
					hmap.put("contractStartDate", contractVO.getContractStartDate());
					hmap.put("contractEndDate", contractVO.getContractEndDate());
					hmap.put("contraDescription", contractVO.getContraDescription());
					hmap.put("contactPersonName", contractVO.getContactPersonName());
					hmap.put("contactPersMobile", contractVO.getContactPersMobile());
					hmap.put("contractStatus",contractVO.getContractStatus());
					hmap.put("contractorId", contractVO.getContractorId());
					hmap.put("assetID", contractVO.getAssetID());
					hmap.put("contractAmount", contractVO.getContractAmount());
			
					insertFlag=namedParameterJdbcTemplate.update(insertFileString,hmap);
				
				logger.debug("Exit : public int insertContract(ContractorVO contractorVO,AddressVO addressVOForContractor,ContractVO contractVO,int genratedID)"+insertFlag);
			
			} catch (InvalidDataAccessApiUsageException ex) {
				logger.error("InvalidDataAccessApiUsageException in insertContract : "+ex);

			} catch (DataAccessException exc) {
				logger.error("DataAccessException in insertContract : "+exc);
			} catch (Exception e) {
				logger.error("Exception in insertContract : "+e);
			}

			return insertFlag;
		}
	
	public int insertMaintainenceDetails(MaintainenceVO maintanenceVO){

		int insertFlag = 0;
		int success = 0;
		int genratedID=0;
			
		logger.debug("Entry : public int insertMaintainenceDetails(MaintainenceVO maintanenceVO)");
		try {
			
			 String insertFileString = "INSERT INTO  maintainence_history (description,maintainence_person_name,maintainence_date, maintainence_next_date, maintainence_status,contract_id) VALUES "+
	                                   "( :description, :persName, :maintainenceDate, :maintainenceNextDate, :status, :contractId);";
			 logger.debug("query : " + insertFileString);
			 	
		     
		      
		      Map hmap=new HashMap();
				hmap.put("description", maintanenceVO.getDescription());
				hmap.put("persName", maintanenceVO.getPersName());
				hmap.put("maintainenceDate", maintanenceVO.getMaintainenceDate());
				hmap.put("maintainenceNextDate", maintanenceVO.getMaintainenceNextDate());
				hmap.put("status", maintanenceVO.getStatus());
				hmap.put("contractId", maintanenceVO.getContractId());
				
				
				insertFlag=namedParameterJdbcTemplate.update(insertFileString,hmap);
							
			
		logger.debug("Exit : public int insertMaintainenceDetails(MaintainenceVO maintanenceVO)"+insertFlag);
		
		}catch(InvalidDataAccessApiUsageException ex) {
		    logger.error("InvalidDataAccessApiUsageException in insertMaintainenceDetails : "+ex);

		}catch (DataAccessException exc) {
		    logger.error("DataAccessException in insertMaintainenceDetails : "+exc);
		}catch (Exception e) {
		    logger.error("Exception in insertMaintainenceDetails : "+e);
		}

		return insertFlag;
		}
	   
	public int updateContractDetail(ContractVO contractVO,AddressVO addressVO) {

		int flag = 0;
		int updateFlag=0;
		int flagC=0;
		String strSQLQuery = "";
		
		try {
			
			logger.debug("Entry : public int updateContractDetail(ContractVO contractVO,AddressVO addressVO)");
			strSQLQuery =" UPDATE contract_details SET contract_start_date= :contractStartDate,contract_end_date= :contractEndDate, "+
	                     " contract_description= :contraDescription,contact_person_name= :contactPersonName,cont_mobile=:contactPersMobile,"+
	                     " contractor_id=:contractorId, contract_amount=:contractAmount, contract_status=:contractStatus "+
	                     " WHERE contract_id="+contractVO.getContractId()+";";
	       
			logger.debug("query : " + strSQLQuery);
			        

			Map hmap = new HashMap();
			hmap.put("contractStartDate", contractVO.getContractStartDate());
			hmap.put("contractEndDate",contractVO.getContractEndDate());
			hmap.put("contraDescription",contractVO.getContraDescription());
			hmap.put("contactPersonName", contractVO.getContactPersonName());
			hmap.put("contactPersMobile", contractVO.getContactPersMobile());
			hmap.put("contractorId", contractVO.getContractorId());
			hmap.put("contractAmount", contractVO.getContractAmount());
			hmap.put("contractStatus",contractVO.getContractStatus());
				    
			flag=namedParameterJdbcTemplate.update(strSQLQuery, hmap);
			SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(
					contractVO);
	        
			flagC = this.namedParameterJdbcTemplate.update("UPDATE contractor_details SET contractor_name=:contraName WHERE contractor_id="+contractVO.getContractorId()+" ",
					namedParameters);
			updateFlag = addressDAO.updateAddressDetails(addressVO);
			
			if ((updateFlag == 1) &&(flag != 0) &&(flagC == 1) ) {
				   flag = 1;
	          }
			logger.debug("Exit : public int updateContractDetail(ContractVO contractVO,AddressVO addressVO)"+flag);

		}catch(Exception Ex) {
			logger.error("Exception in updateContractDetail : "+Ex);
		}

		return flag;

	}

	public int updateMaintainceDetails(MaintainenceVO maintainenceVO,String maintainenceId) {

		int flag = 0;
		String strSQLQuery = "";
		try {

			logger.debug("Entry : public int updateMaintainceDetails(MaintainenceVO maintainenceVO,String maintainenceId)");
			strSQLQuery = " UPDATE maintainence_history SET description= :description,maintainence_person_name= :persName, "+
		                  " maintainence_date= :maintainenceDate,maintainence_next_date= :maintainenceNextDate, maintainence_status= :status " +
		                  " WHERE maintainence_hs_id="+maintainenceId+";";
			
			logger.debug("query : " + strSQLQuery);

			Map hmap=new HashMap();
			hmap.put("description", maintainenceVO.getDescription());
			hmap.put("persName", maintainenceVO.getPersName());
			hmap.put("maintainenceDate", maintainenceVO.getMaintainenceDate());
			hmap.put("maintainenceNextDate", maintainenceVO.getMaintainenceNextDate());
			hmap.put("status", maintainenceVO.getStatus());		
			
			flag=namedParameterJdbcTemplate.update(strSQLQuery, hmap);
			
			logger.debug("Exit : public int updateMaintainceDetails(MaintainenceVO maintainenceVO,String maintainenceId)"+flag);

		}catch(Exception Ex){
			logger.error("Exception in updateMaintainceDetails : "+Ex);
		}

		return flag;

	  }

	public int deleteContractDetails(String contractId){
		
		int sucess = 0;
		String strSQL = "";
		int flag=0;
		try {
				logger.debug("Entry : public int deleteContractDetails(String assetId,String contractId)");
				strSQL = "DELETE FROM contract_details WHERE contract_id= :contractId " ;
				logger.debug("query : " + strSQL);
				// 2. Parameters.
				Map namedParameters = new HashMap();
				namedParameters.put("contractId", contractId);
				
				sucess = namedParameterJdbcTemplate.update(strSQL,namedParameters);			
			    
				logger.debug("Exit : public int deleteContractDetails(String assetId,String contractId)"+sucess);
	            
		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in deleteContractDetails : "+Ex);
			
		}
		
		return sucess;
	}		
	
	public int deleteMaintainenceDetails(String maintainenceId){
		
		int sucess = 0;
		String strSQL = "";
		
		try {
				logger.debug("Entry : public int deleteMaintainenceDetails(String maintainenceId)");
				strSQL = "DELETE FROM maintainence_history WHERE maintainence_hs_id= :maintainenceId" ;
				logger.debug("query : " + strSQL);
				// 2. Parameters.
				Map namedParameters = new HashMap();
				namedParameters.put("maintainenceId", maintainenceId);
				
				sucess = namedParameterJdbcTemplate.update(strSQL,namedParameters);			
			    
				
				logger.debug("Exit : public int deleteMaintainenceDetails(String assetId,String maintainenceId)"+sucess);
	            
		}catch(Exception Ex){
			
			Ex.printStackTrace();
			logger.error("Exception in deleteMaintainenceDetails : "+Ex);
			
		}
		return sucess;
	}
	/*public List getFixedList(String strSocietyID) {

		List fixedAssetList = null;
		
		try {
			   logger.debug("Entry : public List getFixedList(String strSocietyID)");
			   
	    String strSQL = " SELECT assets_details.*,asset_category_name FROM assets_details,assets_category_list" 
                        + " WHERE assets_category_list.asset_category_id = assets_details.asset_category_id "
                        + " AND assets_details.is_deleted='0' " 
                        + " AND  assets_category_list.asset_type='F' "
                        + " AND assets_details.society_id =:society_ID "
                        + " GROUP BY asset_id "
	                    + " ORDER BY asset_id DESC; ";
	          
							
				//1. SQL Query				
				logger.debug("query : " + strSQL);
				
				// 2. Parameters.				
				MapSqlParameterSource namedParameters = new MapSqlParameterSource();
				namedParameters.addValue("society_ID", strSocietyID);
							
				//3. RowMapper.
				RowMapper RMapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						 
						AssetsVO asset = new AssetsVO();
						asset.setAssetId(rs.getInt("asset_id"));
						asset.setAssetName(rs.getString("asset_name"));
						asset.setAssetUniqueId(rs.getString("asset_unique_id"));
						asset.setAssetListID(rs.getString("asset_category_id"));
						asset.setAssetDescription(rs.getString("asset_description"));
						asset.setAssetCategoryName(rs.getString("asset_category_name"));
						asset.setSocietyId(rs.getString("society_id"));
						
					    //asset.setAssetType(rs.getNString("asset_type"));
						return asset;
					}
				};

				fixedAssetList = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);

				logger.debug("outside query : " + fixedAssetList.size());
				logger.debug("Exit : public List getFixedList(String strSocietyID)");

			}catch(EmptyResultDataAccessException Ex) {
				logger.info("No fixed asset details are available  ");

			} catch (Exception ex) {
				logger.error("Exception in getFixedList : "+ex);

			}
			return fixedAssetList;

	}
	public List getMovableList(String strSocietyID) {
	    
			List movableAssetList = null;
			try { 
				logger.debug("Ebtry : public List getMovableList(String strSocietyID)");
				
	    String strSQL = "SELECT assets_details.*,asset_category_name,manufacture_name FROM assets_details,manufacture_details,assets_category_list"
                        + " WHERE assets_details.society_id =:society_ID "
                        + " AND assets_details.is_deleted='0' "
                        + " AND  assets_category_list.asset_type='M'" 
                        + " AND assets_category_list.asset_category_id = assets_details.asset_category_id "
                        + " AND assets_details.manufacture_id=manufacture_details.manufacture_id " 
                        + " GROUP BY asset_id "
	                    + " ORDER BY asset_id DESC; "; 
	    
	    		//1. SQL Query
				logger.debug("query : " + strSQL);
				
				// 2. Parameters.				
				MapSqlParameterSource namedParameters = new MapSqlParameterSource();
				namedParameters.addValue("society_ID", strSocietyID);
				
				//3. RowMapper.
				RowMapper RMapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						 
						AssetsVO asset = new AssetsVO();
						asset.setAssetUniqueId(rs.getString("asset_unique_id"));
						asset.setAssetListID(rs.getString("asset_category_id"));
						asset.setAssetName(rs.getString("asset_name"));
						asset.setAssetId(rs.getInt("asset_id"));
						asset.setSocietyId(rs.getString("society_id"));
						//asset.setAssetType(rs.getString("asset_type"));
						asset.setAssetDescription(rs.getString("asset_description"));
						asset.setPurchasedDate(rs.getString("asset_purchased_date"));
						asset.setWarranty(rs.getString("warranty"));
						asset.setMfgId(rs.getString("manufacture_id"));
						asset.setMfgName(rs.getString("manufacture_name"));
						asset.setAssetCategoryName(rs.getString("asset_category_name"));	
					   	return asset;
					}
				};

				movableAssetList = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
	           
				logger.debug("outside query : " + movableAssetList.size());				
				logger.debug("Exit : public List getMovableList(String strSocietyID)");

	        }catch(EmptyResultDataAccessException Ex) {
		    	logger.info("No Movable Asset details are available  ");  
	
			}catch (Exception ex) {
				logger.error("Exception in getMovableList :" +ex);
			}
			
			return movableAssetList;

		}
	
	public List getFixedAssetList(String strSocietyID,String categoryId) {

		List fixedAssetListCatetoryDetails = null;

		try {
			logger.debug("Entry : public List getFixedAssetList(String strSocietyID,String categoryId)");
		
	    String strSQL = "SELECT assets_details.*,asset_category_name FROM assets_details,assets_category_list "+
	                    " WHERE assets_details.society_id =:society_ID "+
		                " AND assets_details.is_deleted='0' "+
                        " AND  assets_details.asset_category_id =:assetCatId "+  
                        " AND  assets_category_list.asset_type='F' "+
                        " AND assets_category_list.asset_category_id = assets_details.asset_category_id "+
	                    " ORDER BY asset_id DESC; ";
	          
				//1. SQL Query
				logger.debug("query : " + strSQL);
				
				// 2. Parameters.
				MapSqlParameterSource namedParameters = new MapSqlParameterSource();
				namedParameters.addValue("society_ID", strSocietyID);
				namedParameters.addValue("assetCatId", categoryId);
				
				//3. RowMapper.
				RowMapper RMapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						 
						AssetsVO asset = new AssetsVO();
						asset.setAssetId(rs.getInt("asset_id"));
						asset.setAssetName(rs.getString("asset_name"));
						asset.setAssetUniqueId(rs.getString("asset_unique_id"));
						asset.setAssetListID(rs.getString("asset_category_id"));
						asset.setAssetDescription(rs.getString("asset_description"));
						asset.setAssetCategoryName(rs.getString("asset_category_name"));	
						asset.setSocietyId(rs.getString("society_id"));
						//asset.setAssetType(rs.getNString("asset_type"));
						return asset;
					}
				};

				fixedAssetListCatetoryDetails = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);

				logger.debug("outside query : " + fixedAssetListCatetoryDetails.size());
				logger.debug("Exit: public List getFixedAssetList(String strSocietyID,String categoryId)");
				
		    }catch (EmptyResultDataAccessException Ex){
	    	   logger.info("No Fixed Asset details are available  ");
	
			}catch(Exception ex) {
				logger.error("Exception in getFixedAssetList : "+ex);

			}
			return fixedAssetListCatetoryDetails;

	}*/

	
/*public List getMovableAssetList(String strSocietyID,String categoryId){
    
		List movableAssetListCatetoryDetails = null;
		
		 try { 
 	          logger.debug(" Entry : public List getMovableAssetList(String strSocietyID,String categoryId)");
		
    String strSQL = "SELECT assets_details.*,asset_category_name,manufacture_name,manufacture_details.manufacture_id FROM assets_details,manufacture_details,assets_category_list " +
    		" WHERE assets_details.society_id =:society_ID"+
    		"  AND assets_details.is_deleted='0' "+
            " AND  assets_details.asset_category_id =:assetCatId     " +     
            " AND  assets_category_list.asset_type='M' "+
            " AND assets_details.manufacture_id = manufacture_details.manufacture_id "+
            " AND assets_category_list.asset_category_id = assets_details.asset_category_id "+
            " ORDER BY asset_id DESC; ";
   
            //1. SQL Query
			logger.debug("query : " + strSQL);
			
			// 2. Parameters.			
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("society_ID", strSocietyID);
			namedParameters.addValue("assetCatId", categoryId);
			
			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					 
					AssetsVO asset = new AssetsVO();
					asset.setAssetUniqueId(rs.getString("asset_unique_id"));
					asset.setAssetListID(rs.getString("asset_category_id"));
					asset.setAssetName(rs.getString("asset_name"));
					asset.setAssetId(rs.getInt("asset_id"));
					asset.setSocietyId(rs.getString("society_id"));
					//asset.setAssetType(rs.getString("asset_type"));
					asset.setAssetDescription(rs.getString("asset_description"));
					asset.setPurchasedDate(rs.getString("asset_purchased_date"));
					asset.setWarranty(rs.getString("warranty"));
					asset.setMfgId(rs.getString("manufacture_id"));
					asset.setMfgName(rs.getString("manufacture_name"));
					asset.setAssetCategoryName(rs.getString("asset_category_name"));	
				    return asset;
				}
			};

			movableAssetListCatetoryDetails = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
            logger.debug("outside query : " + movableAssetListCatetoryDetails.size());
            logger.debug("Exit : public List getMovableAssetList(String strSocietyID,String categoryId)");
		} catch (Exception ex) {
			logger.error("Exception in getMovableAssetList : "+ex);
		 }
		
		return movableAssetListCatetoryDetails;

	}
*/

   
/*public int insertManufactureDetails(AssetsVO assetVO,ManufactureVO manufactureVO,AddressVO addressVO){
	
	int flag = 0;
	int insertFlag = 0;
	int sucessForAsset = 0;
    int successForAddress =0;
    int genratedID = 0;
    
	try {
		logger.debug("Entry : public int insertManufactureDetails(AssetsVO assetVO,ManufactureVO manufactureVO,AddressVO addressVO)");
		
	    String insertFileString = "INSERT INTO manufacture_details(manufacture_name) VALUES( :manufactureName)";
	    logger.debug("query : " + insertFileString);
	    
    	SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(
    			manufactureVO);
	
	    KeyHolder keyHolder = new GeneratedKeyHolder();
	    insertFlag = getNamedParameterJdbcTemplate().update(
			    insertFileString, fileParameters, keyHolder);
	    genratedID = keyHolder.getKey().intValue();
	    logger.debug("key GEnereator ---" + genratedID);
		
	    String memberId = String.valueOf(genratedID);
 	
	    if(insertFlag != 0){
	    	sucessForAsset = insertAssetDetails(assetVO, manufactureVO,genratedID);
	    }
	    if(sucessForAsset == 2 ){
		    insertFlag = 2;
            String sql = "DELETE FROM manufacture_details  WHERE manufacture_id=:genID; ";
      
		         Map hmap=new HashMap();
		         hmap.put("genID", genratedID);
		     
		    flag=namedParameterJdbcTemplate.update(sql, hmap);
		
	    }else if(sucessForAsset == 1){
	    	successForAddress = addressDAO.addNewAddressDetails(addressVO,memberId);
	    }
	    if(successForAddress == 1){
		    insertFlag = 1;
	    }
	
	  
	    logger.debug("Exit : public int insertManufactureDetails(AssetsVO assetVO,ManufactureVO manufactureVO,AddressVO addressVO)"+insertFlag);
	}catch (DataAccessException exc) {
	  logger.error("DataAccessException in insertManufactureDetails : "+exc);
	}
	catch (Exception e) {
	  logger.error("Exception in insertManufactureDetails : "+e);
	}

	return insertFlag;
	}

public int insertAssetDetails(AssetsVO assetVO,ManufactureVO manufactureVO,int genratedID) {
		int insertFlag = 0;
		int success = 0;
			
		try {
			logger.debug("Entry : public int insertAssetDetails(AssetsVO assetVO,ManufactureVO manufactureVO,int genratedID)");
			
		 String genIDM = String.valueOf(genratedID);
		 insertFlag = this.jdbcTemplate.update("  INSERT INTO assets_details(asset_category_id,society_id,asset_unique_id,asset_name,asset_description,asset_purchased_date,warranty,manufacture_id,is_deleted) VALUES ( ?,?,?,?,?,?,?,?,?); ", 
                  new Object[] {assetVO.getAssetCatId(),assetVO.getSocietyId(),assetVO.getAssetUniqueId(),assetVO.getAssetName(),assetVO.getAssetDescription(),assetVO.getPurchasedDate(),assetVO.getWarranty(),genIDM,assetVO.getIsDeleted()});

			logger.debug("Exit : public int insertAssetDetails(AssetsVO assetVO,ManufactureVO manufactureVO,int genratedID)"+insertFlag);
		}catch(DuplicateKeyException duplicateEntry){
			duplicateEntry.printStackTrace();
			logger.error("DuplicateKeyException : "+duplicateEntry);
			insertFlag = 2;
			return insertFlag;	
		} catch (DataAccessException exc) {
			logger.error("DataAccessException in insertAssetDetails : "+exc);
		} catch (Exception e) {
			logger.error("Exception in insertAssetDetails : "+e);
		}

		return insertFlag;
	}
   */


		
/*public int insertAssetContractRelation(String assetId,int genratedIDForContract) {
	
	int insertFlag = 0;
	
	try {
		logger.debug("Entry : public int insertAssetContractRelation(String assetId,int contractId)");
		
        String strQry = "INSERT INTO asset_contract_relation (asset_id,contract_id) VALUES( :assetId,:contractId);";
        logger.debug("query : " + strQry);
        
		Map namedParam = new HashMap();
		namedParam.put("assetId", assetId);
		namedParam.put("contractId",genratedIDForContract);
		
		insertFlag = namedParameterJdbcTemplate.update(strQry, namedParam);			
					
		logger.debug("Exit : public int insertAssetContractRelation(String assetId,int contractId)"+insertFlag);
		
	}catch(DataAccessException exc) {
		logger.error("DataAccessException in insertAssetContractRelation : "+exc);
	}
	catch(Exception e) {
		logger.error("Exception in insertAssetContractRelation : "+e);
	}

	return insertFlag;
}
*/

    		
/*public int insertAssetMaintainenceRelation(MaintainenceVO maintanenceVO,int genratedID) {
     int flag = 0;
     int insertFlag = 0;
          
     try {
    	  logger.debug("Entry : public int insertAssetMaintainenceRelation(MaintainenceVO maintanenceVO,int genratedID)");
    	  
    	String strQry = "INSERT INTO assets_maintainence_relation (maintainence_hs_id,asset_id) VALUES ( :maintainenceId, :assetId);";
    	 logger.debug("query : " + strQry);
    	 
  		Map namedParam = new HashMap();
  		namedParam.put("maintainenceId", genratedID);
  	    namedParam.put("assetId",maintanenceVO.getAssetId());
  		
  		insertFlag = namedParameterJdbcTemplate.update(strQry, namedParam);
  	        
          logger.debug("Exit :public int insertAssetMaintainenceRelation(MaintainenceVO maintanenceVO,int genratedID)"+insertFlag);
          
      }catch(DataAccessException exc) {
          logger.error("DataAccessException in insertAssetMaintainenceRelation : "+exc);
      }
      catch(Exception e) {
         logger.error("Exception in insertAssetMaintainenceRelation : "+e);
      }

      return insertFlag;
}



public int updateFixedAsset(AssetsVO assetsVO) {
	
	int flag = 0;
	String strSQLQuery = "";
	try {
		logger.debug("Entry : public int updateFixedAsset(AssetsVO assetsVO)");
		
		strSQLQuery="UPDATE assets_details SET asset_category_id= :assetListID,society_id= :societyId,asset_unique_id= :assetUniqueId," 
				+"asset_name= :assetName,asset_description= :assetDescription,is_deleted= :isDeleted WHERE asset_id="+assetsVO.getAssetId()+"";
		
		logger.debug("query : " + strSQLQuery);
		
		Map namedParam = new HashMap();
		namedParam.put("assetListID",assetsVO.getAssetCatId());
		namedParam.put("societyId",assetsVO.getSocietyId());
		namedParam.put("assetUniqueId",assetsVO.getAssetUniqueId());
		namedParam.put("assetName", assetsVO.getAssetName());
		namedParam.put("assetDescription", assetsVO.getAssetDescription());
		namedParam.put("isDeleted", assetsVO.getIsDeleted());
			
		
		flag=namedParameterJdbcTemplate.update(strSQLQuery, namedParam);
		
		logger.debug("Exit : public int updateFixedAsset(AssetsVO assetsVO)"+flag);

	}catch(InvalidDataAccessApiUsageException ex){
	     logger.error("InvalidDataAccessApiUsageException in updateFixedAsset : " +ex);

    }catch(DataAccessException exc){
	     logger.error("DataAccessException in updateFixedAsset : "+exc);
    }catch (Exception e) {
	     logger.error("Exception in updateFixedAsset :  "+e);
    }


	return flag;

	
}


public int updateMovableAsset(AssetsVO assetsVO,AddressVO addressVO) {

	int flag = 0;
	int updateFlag=0;
	int flagM=0;
	String strSQLQuery = "";
	
	try {
		logger.debug("Entry : public int updateMovableAsset(AssetsVO assetsVO,AddressVO addressVO)");
		
		strSQLQuery = "UPDATE assets_details SET asset_category_id= :assetListID,society_id= :societyId, "+
                      "asset_unique_id= :assetUniqueId,asset_name= :assetName,asset_description=:assetDescription,"+
                      "asset_purchased_date=:purchasedDate,warranty=:warranty,manufacture_id=:mfgId,is_deleted=:isDeleted " +
                      "WHERE asset_id="+assetsVO.getAssetId()+";";
		
		logger.debug("query : " + strSQLQuery);

		Map hmap = new HashMap();
		hmap.put("assetListID", assetsVO.getAssetCatId());
		hmap.put("societyId", assetsVO.getSocietyId());
		hmap.put("assetUniqueId",assetsVO.getAssetUniqueId());
		hmap.put("assetName", assetsVO.getAssetName());
		hmap.put("assetDescription", assetsVO.getAssetDescription());
		hmap.put("purchasedDate", assetsVO.getPurchasedDate());
		hmap.put("warranty", assetsVO.getWarranty());
		hmap.put("mfgId", assetsVO.getMfgId());
		hmap.put("isDeleted",assetsVO.getIsDeleted());
	    
		flag=namedParameterJdbcTemplate.update(strSQLQuery, hmap);
		SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(
				assetsVO);
        
		flagM = this.namedParameterJdbcTemplate.update("UPDATE manufacture_details SET manufacture_name=:mfgName WHERE manufacture_id="+assetsVO.getMfgId()+" ",
				namedParameters);
		updateFlag = addressDAO.updateAddressDetails(addressVO);
		
		if ((flag != 0)&&(flagM == 1)&&(updateFlag == 1)) {
			  flag = 1;	
        }
		
		logger.debug("Exit : public int updateMovableAsset(AssetsVO assetsVO,AddressVO addressVO)"+flag);

	 }catch (Exception Ex) {
		logger.error("Exception in updateMovableAsset : "+Ex);
	 }

	return flag;

}
*/


/*public int deleteAssetDetails(String assetId){
	
	int sucess = 0;
	String strSQL = "";
	try {
			logger.debug("Entry : public int deleteAssetDetails(String assetId)");
			strSQL = "UPDATE assets_details SET is_deleted='1'  WHERE asset_id=:assetId" ;
			logger.debug("query : " + strSQL);
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("assetId", assetId);
			
			sucess = namedParameterJdbcTemplate.update(strSQL,namedParameters);			
		
			logger.debug("Exit : public int deleteAssetDetails(String assetId)"+sucess);

	}catch(Exception Ex){
		
		Ex.printStackTrace();
		logger.error("Exception in deleteAssetDetails : "+Ex);
		
	}
	return sucess;
}		
*/



	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	public AddressDAO getAddressDAO() {
		return addressDAO;
	}

	public void setAddressDAO(AddressDAO addressDAO) {
		this.addressDAO = addressDAO;
	}
	
	
}
