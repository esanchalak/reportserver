package com.emanager.server.userLogin.services;

import org.apache.log4j.Logger;

import com.emanager.server.commonUtils.valueObject.EventForm;
import com.emanager.server.userLogin.domainObject.LoginDomain;
import com.emanager.server.userLogin.valueObject.LoginVO;

public class LoginService{
		
	public LoginDomain loginDomain;
	public LoginVO loginVO;

	private static final Logger logger = Logger.getLogger(LoginService.class);
	
	public LoginVO checkLogin(LoginVO loginVO){
		
		try{
		
			logger.info("*****************Entered Inside Esanchalak CHECK LOGIN METHOD*****************");
			
		
		loginVO = loginDomain.checkLogin(loginVO);		
			
		}catch(Exception Ex){			
			Ex.printStackTrace();
			logger.error("Exception in checkLogin : "+Ex);			
		}
		return loginVO;
		
	}
	
	public int getPassword(String emailId){
		
		int status=0 ;
		try{					
		logger.debug("Entry in getPassword method ");	
		
		status = loginDomain.getPassword(emailId);
		
		logger.debug("Out and get status of sending email");
		 			
		}catch(Exception Ex){			
			Ex.printStackTrace();
			logger.error("Exception in getPassword : "+Ex);
			
		}
		
		return status;
	

	}
	
	public int createNewLogin(com.emanager.server.society.valueObject.MemberVO newLoginVO){
		int flag= 0;
		try{
			logger.debug("Entry : public int createNewLogin(LoginVO newLoginVO) ");	
			
			flag = loginDomain.createNewLogin(newLoginVO);
			
			logger.debug("Exit : public int createNewLogin(LoginVO loginVO)");
			
		}catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in createNewLogin : "+Ex);
		}
		return flag;
		
		
	}
	public int resetPassword(LoginVO loginVO,String loginMemberId){
		int flag= 0;
		try{
			logger.debug("Entry : public int resetPassword(LoginVO loginVO,String loginMemberId) ");	
			
			flag = loginDomain.resetPassword(loginVO,loginMemberId);
			
			if(flag == 1){
				logger.info("Member ID '"+loginMemberId+"' has password reset sucessfully.");
			}
			logger.debug("Exit : public int resetPassword(LoginVO loginVO,String loginMemberId)");
			
		}catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in resetPassword : "+Ex);
		}
		return flag;
		
		
	}
	public int matchOldPassword(String societyId,String userId,String password){
		int flag=0;
		try{
			logger.debug("Entry : public int matchOldPassword(String societyId,String userId) ");	
			
			flag = loginDomain.matchOldPassword(societyId,userId,password);
			
			logger.debug("Exit : public int matchOldPassword(String societyId,String userId)");
			
		}catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in matchOldPassword : "+Ex);
		}
		return flag;
		
		
	}
	
	

	public LoginDomain getLoginDomain() {
		return loginDomain;
	}

	public void setLoginDomain(LoginDomain loginDomain) {
		this.loginDomain = loginDomain;
	}
	


	
	
}