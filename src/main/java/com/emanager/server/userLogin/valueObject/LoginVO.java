package com.emanager.server.userLogin.valueObject;

import java.util.List;

/**
 * @author Niranjan Arude
 * Date  July 24, 2007
 */
public class LoginVO 
{
	
	/**
	 * @author Rahul Chaudhari
	 * Following Fields are added by Rahulc for login validation.
	 */
	/** declaration of strChkLoginId */
	private String loginUserFullName;
	/** declaration of strChkPassword */
	private String loginUserPassword;
	/** declaration of strClientId */
	private String loginUserId;
	/** declaration of strLoginId */
	private String strLoginId;
	
	private String comment;
	
	private Boolean loginSuccess;
	
	private String userID;
	private String userType;
	private String societyID;
	private String societyName;
	private String oneTimePassword;
	private String isVerified;
	private String newPassword;
	private String confirmPassword;
	private String brandName;
	private String unitName;
	private String socAddress;
	private String socRegiNo;
	private String sOSUrl;
	private String aptID;
	private String socFullName;
	private String committeeGroupMail;
	private String memberGroupMail;
	private String version;
	private List societyList;
	private List permissionList;
	private String txCloseDate;
	private String effectiveDate;
	
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getIsVerified() {
		return isVerified;
	}
	public void setIsVerified(String isVerified) {
		this.isVerified = isVerified;
	}
	public String getOneTimePassword() {
		return oneTimePassword;
	}
	public void setOneTimePassword(String oneTimePassword) {
		this.oneTimePassword = oneTimePassword;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getLoginUserId() {
		return loginUserId;
	}
	public void setLoginUserId(String loginUserId) {
		this.loginUserId = loginUserId;
	}
	public String getLoginUserFullName() {
		return loginUserFullName;
	}
	public void setLoginUserFullName(String loginUserFullName) {
		this.loginUserFullName = loginUserFullName;
	}
	public String getLoginUserPassword() {
		return loginUserPassword;
	}
	public void setLoginUserPassword(String loginUserPassword) {
		this.loginUserPassword = loginUserPassword;
	}
	public String getStrLoginId() {
		return strLoginId;
	}
	public void setStrLoginId(String strLoginId) {
		this.strLoginId = strLoginId;
	}
	public Boolean getLoginSuccess() {
		return loginSuccess;
	}
	public void setLoginSuccess(Boolean loginSuccess) {
		this.loginSuccess = loginSuccess;
	}
	public String getSocietyID() {
		return societyID;
	}
	public void setSocietyID(String societyID) {
		this.societyID = societyID;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getSocAddress() {
		return socAddress;
	}
	public void setSocAddress(String socAddress) {
		this.socAddress = socAddress;
	}
	public String getSocRegiNo() {
		return socRegiNo;
	}
	public void setSocRegiNo(String socRegiNo) {
		this.socRegiNo = socRegiNo;
	}
	public String getSOSUrl() {
		return sOSUrl;
	}
	public void setSOSUrl(String url) {
		sOSUrl = url;
	}
	public String getAptID() {
		return aptID;
	}
	public void setAptID(String aptID) {
		this.aptID = aptID;
	}
	public String getSocFullName() {
		return socFullName;
	}
	public void setSocFullName(String socFullName) {
		this.socFullName = socFullName;
	}
	public String getCommitteeGroupMail() {
		return committeeGroupMail;
	}
	public void setCommitteeGroupMail(String committeeGroupMail) {
		this.committeeGroupMail = committeeGroupMail;
	}
	public String getMemberGroupMail() {
		return memberGroupMail;
	}
	public void setMemberGroupMail(String memberGroupMail) {
		this.memberGroupMail = memberGroupMail;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public List getSocietyList() {
		return societyList;
	}
	public void setSocietyList(List societyList) {
		this.societyList = societyList;
	}
	public List getPermissionList() {
		return permissionList;
	}
	public void setPermissionList(List permissionList) {
		this.permissionList = permissionList;
	}
	public String getTxCloseDate() {
		return txCloseDate;
	}
	public void setTxCloseDate(String txCloseDate) {
		this.txCloseDate = txCloseDate;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	
	
  
	
	
}