package com.emanager.server.userLogin.domainObject;

import java.math.BigInteger;
import java.security.Key;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;

import com.emanager.server.commonUtils.dataaccessObject.AddressDAO;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.commonUtils.services.EmailService;
import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.userLogin.dataaccessObject.AuditActivityDAO;
import com.emanager.server.userLogin.dataaccessObject.LoginDAO;
import com.emanager.server.userLogin.services.LoginService;
import com.emanager.server.userLogin.valueObject.LoginVO;

public class LoginDomain {
	
	public LoginVO loginVO;
	public LoginService loginService;
	public LoginDAO loginDAO;
	public EmailService emailService;
	public AuditActivityDAO auditActivityDAO;
//	public BuilderServices builderServiceBean;
	public AddressDAO addressDAO;
	public SocietyService societyServiceBean;

	private static final Logger logger = Logger.getLogger(LoginDomain.class);
	public static String memberId;
	

	
	public LoginVO checkLogin(LoginVO loginVO){
		
	///	EnquiryDetailsVO enquiryDetailsVO=new EnquiryDetailsVO();
		List societyList=new ArrayList();
		EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
		try{
	     String activity="login";
	     String module="Login";
	     loginVO.setLoginUserId(enDeUtility.decrypt(loginVO.getLoginUserId()));
		loginVO.setLoginUserPassword(enDeUtility.decrypt(loginVO.getLoginUserPassword()));	
		logger.info("Login ID : " + loginVO.getLoginUserId());
		logger.info("Password : " + loginVO.getLoginUserPassword());
		loginVO = loginDAO.authenticateUser(loginVO);
		
		
		if (loginVO.getUserID()!=null){
			
			loginVO.setLoginSuccess(true);
		
			memberId = loginVO.getUserID();
			
			auditActivityDAO.insertAuditActivity(memberId,activity,module);
			logger.info("Society ID : " + loginVO.getSocietyID());
			logger.info("User ID : " + loginVO.getUserID());
			loginVO.setPermissionList(loginDAO.getCompontsList(memberId));	
			if(loginVO.getUserType().equalsIgnoreCase("SU")){
				loginVO.setSocietyList(societyServiceBean.getSocietyList(0));
			}else 
			loginVO.setSocietyList(societyServiceBean.getSocietyList(Integer.parseInt(loginVO.getUserID())));
			if(loginVO.getUserType().equalsIgnoreCase("B")){
			/*	enquiryDetailsVO=builderServiceBean.getBuilderDetails(loginVO.getSocietyID(),loginVO.getUserID(),loginVO.getUserType());
				loginVO.setBrandName(enquiryDetailsVO.getCompany_name());
				loginVO.setLoginUserFullName(enquiryDetailsVO.getFull_name());
				logger.info("User Type : Builder" );*/
			}
			else{
			List	asdL=addressDAO.getSocietyAddressDetails(Integer.parseInt(loginVO.getSocietyID()),"S", "s");
			AddressVO adVO=(AddressVO) asdL.get(0);
			
				if(loginVO.getUserType().equalsIgnoreCase("SM") ||loginVO.getUserType().equalsIgnoreCase("A")){
			LoginVO	lgnVO=loginDAO.getUserDetailsForSM(loginVO.getUserID(),loginVO.getSocietyID());
				loginVO.setSocAddress(adVO.getConcatAddr());
				loginVO.setBrandName(lgnVO.getBrandName());
				loginVO.setSocFullName(lgnVO.getSocFullName());
				loginVO.setSocRegiNo(lgnVO.getSocRegiNo());
				loginVO.setLoginUserFullName(lgnVO.getLoginUserFullName());
				loginVO.setSocietyID(lgnVO.getSocietyID());
				loginVO.setCommitteeGroupMail(lgnVO.getCommitteeGroupMail());
				loginVO.setMemberGroupMail(lgnVO.getMemberGroupMail());
				loginVO.setTxCloseDate(lgnVO.getTxCloseDate());
				loginVO.setVersion(lgnVO.getVersion());
			}else{
			LoginVO	lgnVO=loginDAO.getUserDetails(loginVO.getUserID(),loginVO.getSocietyID());
			loginVO.setBrandName(lgnVO.getBrandName());
			loginVO.setSocRegiNo(lgnVO.getSocRegiNo());
			loginVO.setSocAddress(adVO.getConcatAddr());
			loginVO.setSocFullName(lgnVO.getSocFullName());
			loginVO.setLoginUserFullName(lgnVO.getLoginUserFullName());
			loginVO.setSocietyID(lgnVO.getSocietyID());
			loginVO.setAptID(lgnVO.getAptID());
			loginVO.setCommitteeGroupMail(lgnVO.getCommitteeGroupMail());
			loginVO.setMemberGroupMail(lgnVO.getMemberGroupMail());
			loginVO.setTxCloseDate(lgnVO.getTxCloseDate());
			loginVO.setVersion(lgnVO.getVersion());
			loginVO.setUnitName(lgnVO.getUnitName());
			
				}
				if(loginVO.getUserType().equalsIgnoreCase("A")){
				   logger.info("User Type : Adminstrator" );
				}else if(loginVO.getUserType().equalsIgnoreCase("M")){
					logger.info("User Type : Member"+loginVO.getLoginUserFullName() );
					
				}else if((loginVO.getUserType().equalsIgnoreCase("SP")) ||(loginVO.getUserType().equalsIgnoreCase("SM"))){
					logger.info("User Type : Support Person" );
				}else if(loginVO.getUserType().equalsIgnoreCase("D")){
					logger.info("User Type : Demo login" );
				}
			}
			
			if(loginVO.getLoginUserFullName()==null){
				loginVO.setLoginSuccess(false);
				loginVO.setComment("Invalid Login ID OR Password.");	
			}
			
		}else{
			loginVO.setLoginSuccess(false);
			loginVO.setComment("Invalid Login ID OR Password.");	
		}
		
		logger.debug("After checkLogin");
				
		
		}catch(Exception Ex){			
			Ex.printStackTrace();
			logger.error("Exception in checkLogin : "+Ex);			
		}
		return loginVO;
		
		
	}
	
	
	
	
	public int getPassword(String emailId) throws Exception {
		
		int status =0;
		boolean success=false;
		EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
		String password = null;
		emailId=enDeUtility.decrypt(emailId);
		logger.debug("Entry : public int getPassword(String emailId)");
		
		try{			
		     password=loginDAO.getUserPassword(emailId);
	         logger.info("password :"+password);
		
		if(password == null){
			
		     status =2;
		     logger.info("Password is null");
		     success = validateMember(emailId);
		     if(success)
		    	 status=3;
		}else{	
			
		   	 logger.info("Send login_id and password to Emailsend method"); 
            
		  /* 	 EventVO eventVO = new EventVO();
		     eventVO = fillEventvo(emailId,password);
		     
		     status = emailService.sendEmail(eventVO);*/
		     		     
		     logger.info("status :"+status);
		     logger.info("getting status of sending Email or not by sendEmail method");
		    }		
		
		   logger.debug("Exit : public int getPassword(String emailId)"); 
		}
           catch(DataAccessException Ex){
			
			Ex.printStackTrace();
			logger.error("DataAccessException in getPassword : "+Ex);
			
		}    
		  
		return status;
		
		
	}
	
	private boolean validateMember(String email){
		com.emanager.server.society.valueObject.MemberVO memberVO=new com.emanager.server.society.valueObject.MemberVO();
		logger.debug("Enrty : private MemberVO validateMember(String email)");
		boolean success=false;
		int flag=0;
		
		try {
			memberVO=loginDAO.validateMember(email);
			
			if(memberVO.getMemberID()!=0){
				 flag=createNewLogin(memberVO);
			}
			if(flag==1){
				success=true;
			}
		} catch (Exception e) {
		
			logger.error("Exception t validateMameber"+e);
		
		}
		
		logger.debug("Exit : private MemberVO validateMember(String email)");
		return success;
	}
	

	/*public EventVO fillEventvo(String emailId, String password){
		
		logger.debug("Entry : public EventVO emailSend(String emailId, String password)");
		EventVO event = new EventVO();
		
		try{
			ConfigManager c=new ConfigManager();
		event.setEventID(0);
		event.setEventType("User");
		event.setMemberID(0);
		event.setSocietyId(1);
		event.setSocietyName("E-Sanchalak");
		event.setSubject("Your E-Sanchalak Login");
		event.setHtmlString(password);
		event.setMessage(password);
		event.setTempleteName(c.getPropertiesValue("template.forgotpassword"));
		event.setTransaction_type("Reminder of Password");
		event.setReminderType("Daily");
		
		MemberVO memberVO = new MemberVO();
		memberVO.setEmail(emailId);
		
		logger.debug("email : "+memberVO.getEmail());
		
		List memberList = new java.util.ArrayList();
		memberList.add(memberVO);
		
		event.setMemberlist(memberList);
		
		logger.debug("Exit: public EventVO emailSend(String emailId, String password");	
		
	    }
		catch(NullPointerException Ex){
			
			logger.error("NullPointerException in fillEventvo : "+Ex);
		}
	    logger.info("return eventVO ");
		return event;
	
	}*/
	
	public int createNewLogin(com.emanager.server.society.valueObject.MemberVO memberVO){
		int flag= 0;
		int status=0;
		String generatedPassword=null;
		String newUserId=null;
		try{
			logger.debug("Entry : public int createNewLogin(LoginVO loginVO) ");	
			
			generatedPassword = generateOTP(); 
						
			flag = loginDAO.createNewLogin(memberVO,generatedPassword);
			
			logger.debug("New User id : "+memberVO.getEmail());
			logger.debug("Password : "+generatedPassword);
			
			if(flag==1){
				
				logger.info("Send New User login_id and password to Emailsend method"); 
				
				/* EventVO eventVO = new EventVO();
			     eventVO = fillEventvoForNewUser(memberVO,generatedPassword);
			     
			     status = emailService.sendEmail(eventVO);*/
				
				
			}
			logger.info("status of new User send email of login details :"+status);
			logger.debug("Exit : public int createNewLogin(LoginVO loginVO)");
			
		}catch(Exception Ex){
			Ex.printStackTrace();
			logger.error("Exception in createNewLogin : "+Ex);
		}
		return flag;
		
		
	}
	
	//method generate 6 digit random number 
	 private String generateOTP(){
		 
             return new BigInteger(130, new SecureRandom()).toString().substring(0,6);
     }
	 
	 //method fill eventVO for sending email to new user login details
	/* public EventVO fillEventvoForNewUser(com.emanager.server.society.valueObject.MemberVO memberVO, String password){
			
			logger.debug("Entry :  public EventVO fillEventvoForNewUser(LoginVO loginVO, String password)");
			EventVO event = new EventVO();
			
			try{
				ConfigManager c=new ConfigManager();
			event.setEventID(0);
			event.setEventType("User");
			event.setMemberID(Integer.parseInt(memberVO.getMemberId()));
			event.setSocietyId(Integer.parseInt(memberVO.getSocietyID()));
			event.setSocietyName("Esanchalak");
			event.setSubject("E-Sanchalak Login Information and Confirmation");
			event.setHtmlString(memberVO.getEmail());
			event.setMessage(password);
			event.setTempleteName(c.getPropertiesValue("template.newUser"));
			event.setTransaction_type("Send Email To New User Details");
			event.setReminderType("Daily");
			
			MemberVO memberVo = new MemberVO();
			memberVo.setEmail(memberVO.getEmail());
			memberVo.setUserFirstName(memberVO.getFullName());
						
			logger.debug("email : "+memberVO.getEmail());
			
			List memberList = new java.util.ArrayList();
			memberList.add(memberVo);
			
			event.setMemberlist(memberList);
			
			logger.debug("Exit: public EventVO fillEventvoForNewUser(LoginVO loginVO, String password)");	
			
		    }
			catch(NullPointerException Ex){				
				logger.error("NullPointerException in fillEventvoForNewUser : "+Ex);
			}
		    logger.info("return eventVO ");
			return event;
		
		
		
		} */
	 
	 public int resetPassword(LoginVO loginVO,String loginMemberId){
			int flag= 0;
			int status =0;
		
			
			 LoginVO login = new LoginVO();
			 EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
			try{
				logger.debug("Entry : public int resetPassword(LoginVO loginVO,String loginMemberId) ");	
					loginVO.setConfirmPassword(enDeUtility.decrypt(loginVO.getConfirmPassword()));
				   flag = loginDAO.resetPassword(loginVO,loginMemberId);
				 
				if( flag ==1){
					logger.debug("Entry in get Member email and Password ");	
					
					 login = loginDAO.getMember(loginVO);					 
					
				
				 if(login.getStrLoginId() != null){
					 
					 logger.debug("Send Updated User login_id and password to SendEmail  method"); 
					 
					    logger.debug("User ID : " + login.getStrLoginId());
						logger.debug("Password : " + login.getConfirmPassword());
						
					/* EventVO eventVO = new EventVO();
				   eventVO = fillEventvoForChangedPassword(login.getStrLoginId(), login.getConfirmPassword());
				     
				    status = emailService.sendEmail(eventVO);*/
				 }
				}
				logger.debug("Exit : public int resetPassword(LoginVO loginVO,String loginMemberId)");
				
			}catch(Exception Ex){
				Ex.printStackTrace();
				logger.error("Exception in resetPassword : "+Ex);
			}
			return flag;
			
			
		}
	
	   public int matchOldPassword(String societyId,String userId,String password){
		 String oldPasswordBackend=null;
		 EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
		 int flag=0;
			try{
				logger.debug("Entry : public int matchOldPassword(String societyId,String userId) ");	
				
				oldPasswordBackend = loginDAO.matchOldPassword(societyId,userId);
				logger.debug("old password="+oldPasswordBackend+" available "+enDeUtility.decrypt(password));
				 if(oldPasswordBackend.equals(enDeUtility.decrypt(password))){
					 flag=1;
				 }
				logger.debug("Exit : public int matchOldPassword(String societyId,String userId)");
				
			}catch(Exception Ex){
				Ex.printStackTrace();
				logger.error("Exception in matchOldPassword : "+Ex);
			}
			return flag;
			
			
		}
	   
	   //method fill eventvo for sending email to user for chnge password
	/*	public EventVO fillEventvoForChangedPassword(String emailId, String password){
			
			logger.debug("Entry :public EventVO fillEventvoForChangedPassword(String emailId, String password)");
			EventVO event = new EventVO();
			
			try{
				ConfigManager c=new ConfigManager();
			event.setEventID(0);
			event.setEventType("User");
			event.setMemberID(0);
			event.setSocietyId(1);
			event.setSocietyName("E-Sanchalak");
			event.setSubject("Your E-Sanchalak Login Details");
			event.setHtmlString(password);
			event.setMessage(password);
			event.setTempleteName(c.getPropertiesValue("template.changedPassword"));
			event.setTransaction_type("Password Changed");
			event.setReminderType("Daily");
			
			MemberVO memberVO = new MemberVO();
			memberVO.setEmail(emailId);
			logger.debug("email : "+memberVO.getEmail());
			
			List memberList = new java.util.ArrayList();
			memberList.add(memberVO);
			
			event.setMemberlist(memberList);
			
			logger.debug("Exit: public EventVO fillEventvoForChangedPassword(String emailId, String password)");	
			
		    }
			catch(NullPointerException Ex){
				
				logger.error("NullPointerException in fillEventvoForChangedPassword : "+Ex);
			}
		    logger.info("return eventVO of fillEventvoForChangedPassword ");
			return event;
			
		}*/
		
	
		
		
	public AuditActivityDAO getAuditActivityDAO() {
		return auditActivityDAO;
	}

	public void setAuditActivityDAO(AuditActivityDAO auditActivityDAO) {
		this.auditActivityDAO = auditActivityDAO;
	}

	public LoginDAO getLoginDAO() {
		return loginDAO;
	}

	public void setLoginDAO(LoginDAO loginDAO) {
		this.loginDAO = loginDAO;
	}
	public EmailService getEmailService() {
		return emailService;
	}

	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}
   
	
	public LoginService getLoginService() {
		return loginService;
	}

	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

	
	public AddressDAO getAddressDAO() {
		return addressDAO;
	}


	public void setAddressDAO(AddressDAO addressDAO) {
		this.addressDAO = addressDAO;
	}


	public SocietyService getSocietyServiceBean() {
		return societyServiceBean;
	}


	public void setSocietyServiceBean(SocietyService societyServiceBean) {
		this.societyServiceBean = societyServiceBean;
	}

	

  }
	


