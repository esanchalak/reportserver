package com.emanager.server.userLogin.dataaccessObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.emanager.server.commonUtils.valueObject.DropDownVO;
import com.emanager.server.society.valueObject.InsertMemberVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.userLogin.valueObject.LoginVO;


public class LoginDAO {
	
	   private JdbcTemplate  jdbcTemplate;
	   private InsertMemberVO insertMemberVO;
	   private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	  
	   
	   private static final Logger logger = Logger.getLogger(LoginDAO.class);
       
	   
	   public LoginVO getUserDetails(String userID,String societyID) {
			String strSQLQuery = "";
			LoginVO lginVO=new LoginVO();
			try{	
				logger.debug("Entry : public InsertMemberVO getUserDetails(String userID,String societyID)");
				//	1. SQL Query
			strSQLQuery = "select user_login_details.*,society_details.*,member_details.*,apartment_details.*,building_details.*,tx_close_date "+
							"from user_login_details,society_details,member_details,apartment_details, building_details ,society_settings"+
							" where user_id ="+userID+"  AND user_type!='B' AND user_login_details.society_id ="+societyID+" "+
				     		"and society_settings.society_id=society_details.society_id AND user_login_details.society_id = society_details.society_id AND user_login_details.user_id = member_details.member_id AND apartment_details.apt_id=member_details.apt_id"+
							" AND apartment_details.building_id=building_details.building_id  and is_current!=0 " ;
			logger.debug("query : " + strSQLQuery);
			
			
				
			 Map hmap=new HashMap();
			 hmap.put("userID", userID);
			 hmap.put("societyId", societyID);
			 
//			3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			        	LoginVO lginVO = new LoginVO();
			        	lginVO.setSocietyID(rs.getString("society_id"));
			        	lginVO.setBrandName(rs.getString("society_name"));
			        	lginVO.setSocFullName(rs.getString("society_full_name"));
			        	lginVO.setSocRegiNo(rs.getString("register_no"));
			        	lginVO.setUnitName(rs.getString("building_name")+" "+rs.getString("apt_name"));
			        	lginVO.setAptID(rs.getString("apt_id"));
			        	lginVO.setTxCloseDate(rs.getString("tx_close_date"));
			        	lginVO.setLoginUserFullName(rs.getString("full_name"));
			        	lginVO.setCommitteeGroupMail(rs.getString("mailingList_committee"));
			        	lginVO.setMemberGroupMail(rs.getString("mailingList_member"));
			        	lginVO.setVersion(rs.getString("version"));
			        	return lginVO;
			        }};
			
			
			  lginVO = (LoginVO)namedParameterJdbcTemplate.queryForObject(strSQLQuery,hmap,RMapper);
			
			logger.debug("Exit :  public InsertMemberVO getUserDetails(String userID,String societyID)");
			}catch (Exception ex){
				logger.info("Exception in getUserDetails : "+ex);				 
			}
			
			return lginVO;
			
			}
	   
	   public LoginVO getUserDetailsForSM(String userID,String societyID) {
		  LoginVO loginVO=new LoginVO();
				String strSQLQuery = "";
				
				try{	
					logger.debug("Entry :  public InsertMemberVO getUserDetails(String userID,String societyID)");
					//	1. SQL Query
				strSQLQuery = "select user_login_details.*,society_details.*,member_details.*,tx_close_date from user_login_details,society_details,member_details,society_settings where user_id = :userID AND user_type !='B' AND user_login_details.society_id = :societyId "+
				              "and society_settings.society_id=society_details.society_id and user_login_details.society_id = society_details.society_id and user_login_details.user_id = member_details.member_id";
				logger.debug("query : " + strSQLQuery);
				
				
					
				 Map hmap=new HashMap();
				 hmap.put("userID", userID);
				 hmap.put("societyId", societyID);
				 
//				3. RowMapper.
				 RowMapper RMapper = new RowMapper() {
				        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				        	LoginVO lginVO = new LoginVO();
				        	lginVO.setSocietyID(rs.getString("society_id"));
				        	lginVO.setBrandName(rs.getString("society_name"));
				        	lginVO.setSocFullName(rs.getString("society_full_name"));	
				        	lginVO.setSocRegiNo(rs.getString("register_no"));
				        	lginVO.setLoginUserFullName(rs.getString("full_name"));
				        	lginVO.setTxCloseDate(rs.getString("tx_close_date"));
				           	lginVO.setCommitteeGroupMail(rs.getString("mailingList_committee"));
				        	lginVO.setMemberGroupMail(rs.getString("mailingList_member"));
				        	lginVO.setVersion(rs.getString("version"));
				        			        	
				        	return lginVO;
				        }};
				
				
				  loginVO = (LoginVO)namedParameterJdbcTemplate.queryForObject(strSQLQuery,hmap,RMapper);
				
				logger.debug("Exit :  public InsertMemberVO getUserDetails(String userID,String societyID)");
				}catch (Exception ex){
					logger.info("Exception in getUserDetails : "+ex);				 
				}
				
				return loginVO;
				
				}
	   
	   public LoginVO authenticateUser(LoginVO loginVO) {
		   
		String strSQLQuery = "";
		
		try{	
			logger.debug("Entry : public LoginVO authenticateUser(LoginVO loginVO)" );
//		1. SQL Query
		strSQLQuery = "select * from user_login_details where login_id = :loginUserId and password = :loginUserPassword " ;				
		logger.debug("query : " + strSQLQuery);		
		
			
		SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(loginVO);

//		3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		        	LoginVO lginVO = new LoginVO();
		        	lginVO.setSocietyID(rs.getString("society_id"));
		        	lginVO.setUserID(rs.getString("user_id"));
		        	lginVO.setUserType(rs.getString("user_type"));	
		        	
		        	return lginVO;
		        }};
		
		
		loginVO = (LoginVO)namedParameterJdbcTemplate.queryForObject(strSQLQuery,namedParameters,RMapper);		
		
		logger.debug(loginVO.getLoginUserId()+"Exit : public LoginVO authenticateUser(LoginVO loginVO)");
		}catch (Exception ex){
			logger.info("Exception in authenticateUser : "+ex);			
		}
		return loginVO;
		
		}

		
		public List getMembers(String strFirstName) {
			int ivar=0;
			
			List lstMembers = null;
			
			String strSQL = "";
			try{	
				logger.debug("Entry: public List getMembers(String strFirstName)" );
			//1. SQL Query
			strSQL = "select * from customer_master where firstname :first_name;";
			logger.debug("query : " + strSQL);
			// 2. Parameters.
			 SqlParameterSource namedParameters = new MapSqlParameterSource("" +
			 "first_name", strFirstName );
			 
			//3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			            MemberVO membrs = new MemberVO();
			            membrs.setMemberID(rs.getInt("customer_id"));
			            membrs.setTitle("Mr.");
			            membrs.setOccupation(rs.getString("cellphone"));
			            return membrs;
			        }};
		
			        lstMembers = namedParameterJdbcTemplate.query(strSQL,namedParameters,RMapper);
			
			//ivar = jdbcTemplate.queryForInt("select count(*) from EMP");
			        logger.debug("Entry: public List getMembers(String strFirstName)" );
			}catch (Exception ex){
				System.out.println("Exception :" + ex);
				
			}
			return lstMembers;
			
			}
       
	
		public String getUserPassword(String emailId){
			
			String Sql="";			
			String password = null;
			
			try{
			logger.debug("Inside query search user password");
			Sql = "Select password from user_login_details  where login_id =? and user_type='M'";
			logger.debug("query : " + Sql);
			
			password = jdbcTemplate.queryForObject(Sql,new Object[] { emailId },String.class);
			
			logger.info("Password : "+password);
			logger.debug("Exit : query search user password");
			}catch(EmptyResultDataAccessException e){
				logger.info("Not a registered user");
			}catch(Exception e){
				logger.error("Exception in getUserPassword : "+e);
			}	       
				
			return password;
		
		}
		
		
		public MemberVO validateMember(String email){
			MemberVO memberVO=new MemberVO();
			logger.debug("Enrty : private MemberVO validateMember(String email)");
			
			try {
				String Sql = "SELECT m.*,b.* FROM member_details m,apartment_details a,building_details b " +
						" WHERE m.apt_id=a.apt_id AND a.building_id=b.building_id AND email =:email and is_current!=0 ";
				logger.debug("query : " + Sql);
				Map namedParameters = new HashMap();
				namedParameters.put("email", email);
				
				RowMapper RMapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						MemberVO memberVO=new MemberVO();
						memberVO.setMemberID(rs.getInt("member_id"));
						memberVO.setFullName(rs.getString("full_name"));
						memberVO.setEmail(rs.getString("email"));
						memberVO.setSocietyID(rs.getInt("society_id"));
						return memberVO;
					}
					};
				memberVO =(MemberVO) namedParameterJdbcTemplate.queryForObject(Sql, namedParameters, RMapper);
			
			}catch(EmptyResultDataAccessException e){
				logger.info("Not a registered user");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception at validate User "+e);
			}
			
			
			logger.debug("Exit : query search user password");
			
			
			logger.debug("Exit : private MemberVO validateMember(String email)");
			return memberVO;
		}
		
		
		
		public int createNewLogin(MemberVO memberVO,String genPassword){
			int flag= 0;
			try{
				logger.debug("Entry : public int createNewLogin(LoginVO loginVO,String genPassword) ");	
			
					String sqlQuery="INSERT INTO user_login_details(society_id, user_id, login_id, password) "+
					 " VALUES ( :societyID, :userID,  :loginUserId, :oneTimePassword );" ;
					logger.debug("query : " + sqlQuery);
					
					 Map hmap = new HashMap();
					
					 hmap.put("societyID", memberVO.getSocietyID());
					 hmap.put("userID", memberVO.getMemberID());
					 hmap.put("loginUserId", memberVO.getEmail());
					 hmap.put("oneTimePassword", genPassword);
					 					 
					 			
					 flag = namedParameterJdbcTemplate.update(sqlQuery, hmap);
				
								
				logger.debug("Exit : public int createNewLogin(LoginVO loginVO,String genPassword)"+flag);
				
			}catch(Exception Ex){
				logger.error("Exception in createNewLogin : "+Ex);
			}
			return flag;
			
			
		}

		 public int resetPassword(LoginVO loginVO,String loginMemberId){
				int flag= 0;
				try{
					logger.debug("Entry : public int resetPassword(LoginVO loginVO,String loginMemberId)");
					
					String sqlQuery=" UPDATE user_login_details SET PASSWORD = :confirmPassword WHERE society_id="+loginVO.getSocietyID()+" AND user_id="+loginMemberId+"; ";
					
					logger.debug("query : " + sqlQuery);
					
					   Map map=new HashMap();
					   map.put("confirmPassword", loginVO.getConfirmPassword());
					   
					   flag=namedParameterJdbcTemplate.update(sqlQuery, map);
				
					
					logger.debug("Exit : public int resetPassword(LoginVO loginVO,String loginMemberId)"+flag);
					
				}catch(Exception Ex){
					Ex.printStackTrace();
					logger.error("Exception in resetPassword : "+Ex);
				}
				return flag;
				
				
			} 
		 
		 public LoginVO getMember(LoginVO loginVO){
				
				String strSQLQuery="";			
								
				try{
				logger.debug("Inside query search user email");
				
				strSQLQuery = "select login_id,PASSWORD from user_login_details,society_details,member_details where user_id =:userID AND user_login_details.society_id =:societyID " +
				"and user_login_details.society_id = society_details.society_id and user_login_details.user_id = member_details.member_id and is_current!=0 ";
		        
				logger.debug("query : " + strSQLQuery);
				
				Map namedParameters = new HashMap();
				namedParameters.put("userID", loginVO.getUserID());
				namedParameters.put("societyID", loginVO.getSocietyID());
				
		//		3. RowMapper.
		 RowMapper RMapper = new RowMapper() {
		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		        	LoginVO lginVO = new LoginVO();
		        	 
		        	lginVO.setStrLoginId(rs.getString("login_id"));
		        	lginVO.setConfirmPassword(rs.getString("PASSWORD"));
		            	
		        	return lginVO;
		        }};
		
		
		       loginVO = (LoginVO) namedParameterJdbcTemplate.queryForObject(strSQLQuery,namedParameters,RMapper);
		
				logger.debug("Exit : query search user email ID :"+loginVO.getStrLoginId()+" "+"Password :"+loginVO.getConfirmPassword());
				
				}catch(Exception e){
					e.printStackTrace();
					logger.error("Exception in getMember : "+e);				
				}		       
					
				return loginVO;
			
			}
		 
		 public String matchOldPassword(String societyId,String userId){
							
				String password="";
				try{
									
				logger.debug("Entry : in query  matchOldPassword method ");		
			
				String sql="select PASSWORD from user_login_details where society_id =? AND user_id =?  ";
				logger.debug("query : " + sql);          
				
		    	password = jdbcTemplate.queryForObject(sql,new Object[] {societyId,userId },String.class);		      
		        
		        logger.debug("Exit : public List matchOldPassword(String societyId,String userId)");
		        
				}catch(Exception Ex){					
					Ex.printStackTrace();
					logger.error("Exception in matchOldPassword : "+Ex);					
				}
				
				return password;
			

			}
		 
		 
		 public List getCompontsList(String userID)
			{
				List componentList=new ArrayList();
				String sql=null;
				try
				{
					logger.debug("Entry : public List getCompontsList(int userID)");
					
					
					
					sql=" select c.component_id,p.user_id,p.component from components c,permission_details p" +
							" where p.component_id=c.component_id and p.user_id=:userID;"; 
					logger.debug("Query : " + sql);
					
					Map namedParam = new HashMap();
					namedParam.put("userID",userID);
					
					RowMapper RMapper=new RowMapper()
					{

						public Object mapRow(ResultSet rs, int n) throws SQLException {
							// TODO Auto-generated method stub
							DropDownVO newDdVo=new DropDownVO();
							newDdVo.setData(rs.getString("component_id"));
							newDdVo.setLabel(rs.getString("component"));
							return newDdVo;
						}
						
					};
					
					componentList=namedParameterJdbcTemplate.query(sql, namedParam, RMapper);
					
				}
				catch(Exception ex){
					logger.error("Exception in getSocietyAddressDetails : "+ex);			
				}
				logger.debug("Exit : public List getCompontsList(int userID)");
				return componentList;
				
			}
		 
		 
		public JdbcTemplate getJdbcTemplate() {
			return jdbcTemplate;
		}

		
		public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
			this.jdbcTemplate = jdbcTemplate;
		}


		public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
			return namedParameterJdbcTemplate;
		}


		public void setNamedParameterJdbcTemplate(
				NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
			this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
		}


		
		

	
}
