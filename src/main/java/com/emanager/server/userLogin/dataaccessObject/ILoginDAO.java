package com.emanager.server.userLogin.dataaccessObject;

import java.util.List;

import com.emanager.server.userLogin.valueObject.LoginVO;

/**@Author Rahul Chaudhari
  *Date Jun 7, 2007
  */
public interface ILoginDAO
{
	/**@Author Rahul Chaudhari
	  *@Date Jun 7, 2007
	  *@return List
	  *@param loginVO
	  */
	public List checkLogin(LoginVO loginVO);
	/**@Author Rahul Chaudhari
	  *@Date Jun 7, 2007
	  *@return List
	  *@param strLoginId
	  */
	public List getRoleBaseMenu(LoginVO loginVO);
	/**@Author Rahul Chaudhari
	  *@Date Jun 7, 2007
	  *@return List
	  *@param strClientId
	  */
	public String ChkClientRegister(String strClientId);
	
	/**@Author Rahul Chaudhari
	  *@Date Jun 7, 2007
	  *@return List
	  *@param StrLoginPersonOrganizationId
	  */
	public List checkControllerId(String StrLoginPersonOrganizationId);
	
	/**@Author Trushant thakare
	  *@Date 09-04-2008
	 * Method get the password and the user mail id to which the mails is to be sent 
	 * @param loginName
	 * @return list
	 */
	public List forgetPassword(String loginName);
	
	/**
	 * 
	 * @param moduleName
	 * @param actionName
	 * @param replaceMessage
	 * @return
	 */
	public List getDataBaseMessage(String moduleName, String actionName, List replaceMessage);
	
	/**
	 * @author rchaudhari
	 * @param loggedInUserId
	 * @return boolean
	 * @date Nov 12, 2008
	 */
	public boolean checkUserIsAdmin(int loggedInUserId);
}