package com.emanager.server.userLogin.dataaccessObject;

import java.sql.*;

import org.springframework.jdbc.core.JdbcTemplate;
import org.apache.log4j.Logger;



public class AuditActivityDAO {
	private static final Logger logger = Logger.getLogger(AuditActivityDAO.class);
	JdbcTemplate jdbcTemplate;
	
	
		
 public void insertAuditActivity(String memberId,String activity,String module) {
	 Date currentDatetime = new Date(System.currentTimeMillis());   
	 java.sql.Timestamp timestamp = new java.sql.Timestamp(currentDatetime.getTime());
	
	 int flag = 0;
	
	try{
		
		logger.debug(" Entry : public int insertAuditActivity(String member_id,String activity,String module) ");
		 
		flag = this.jdbcTemplate.update(" INSERT INTO audit_trial(member_id,timestamp,activity,module) VALUES (?,?,?,?); ", 
				                                             new Object[] {memberId,timestamp,activity,module});
		
		
		logger.debug(" Exit :public int insertAuditActivity(String member_id,String activity,String module) ");
		
		logger.debug("flag for insert in audit:"+flag);
		 
	 }	catch(Exception Ex){
		 
		 logger.error("domain error : " + Ex);
	 } 
	
	
}
  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
	this.jdbcTemplate = jdbcTemplate;
  }

}


