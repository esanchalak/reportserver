package com.emanager.server.projects.dataAccessObject;

import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.financialReports.valueObject.MonthwiseChartVO;
import com.emanager.server.financialReports.valueObject.ReportDetailsVO;
import com.emanager.server.projects.valueObjects.ProjectDetailsVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;


public class ProjectDetailsDAO {
	
	   
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	private static final Logger log = Logger.getLogger(ProjectDetailsDAO.class);
	SocietyService societyService;
	
	//-----Add Project Details -------//
			public int addProjectDetails(ProjectDetailsVO projectVO){
				log.debug("Entry : public int addProjectDetails(ProjectDetailsVO projectVO)");
				int success=0;
				try{
					java.util.Date today = new java.util.Date();
					 final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
					   projectVO.setCreateDate(currentTimestamp);
					   projectVO.setUpdateDate(currentTimestamp);
					 
					  
					   String sql = "INSERT INTO project_master_details "+					
						   "(org_id,project_name,description ,project_cost,type,created_by,updated_by,create_date,update_date,project_acronyms,reference_id )  " +
			                "VALUES (:orgID,:projectName,:description,:projectCost,:type,:createdBy,:updatedBy,:createDate,:updateDate,:projectAcronyms,:referenceID)";
					   log.debug("Sql: "+sql);
					   
					   SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(projectVO);
					   KeyHolder keyHolder = new GeneratedKeyHolder();
					   getNamedParameterJdbcTemplate().update(sql, fileParameters, keyHolder);
					   success=keyHolder.getKey().intValue();
					
					  
				}catch (DuplicateKeyException e) {
					success=-1;
					log.info("Duplicate Project Name can not be added");
											
				}catch(Exception e){
					log.error("Exception in adding project details "+e);
				}						
				log.debug("Exit : public int addProjectDetails(ProjectDetailsVO projectVO)");
				return success;
			}
			
			//-----Update Project Details -------//
				public int updateProjectDetails(ProjectDetailsVO projectVO){
					log.debug("Entry : public int updateProjectDetails(ProjectDetailsVO projectVO)");
					int success=0;
					try{
						  java.util.Date today = new java.util.Date();
						   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
						  
						   String str = "UPDATE project_master_details SET project_name=:projectName," +
						   		" description=:description, project_cost=:projectCost, updated_by=:updatedBy, update_date=:updateDate,reference_id=:referenceID,type=:type WHERE project_id=:projectID and org_id=:orgID ; ";				
							   
						   
						   Map hMap=new HashMap();
						   hMap.put("projectID", projectVO.getProjectID());
						   hMap.put("orgID", projectVO.getOrgID());
						   hMap.put("projectName", projectVO.getProjectName());
						   hMap.put("description", projectVO.getDescription());
						   hMap.put("projectCost", projectVO.getProjectCost());
						   hMap.put("updatedBy", projectVO.getUpdatedBy());
						   hMap.put("updateDate", currentTimestamp);
						   hMap.put("referenceID", projectVO.getReferenceID());
						   hMap.put("type", projectVO.getType());
						  
						   success=namedParameterJdbcTemplate.update(str, hMap);	
						
					}catch(Exception e){
						log.error("Exception in updating project details "+e);
					}
					
					
					log.debug("Exit : public int updateProjectDetails(ProjectDetailsVO projectVO)");
					return success;
				}
			
				//-----Delete Project Details -------//
						public int deleteProjectDetails(ProjectDetailsVO projectVO){
							log.debug("Entry : public int deleteProjectDetails(ProjectDetailsVO projectVO)");
							int success=0;
							try{
								
								  java.util.Date today = new java.util.Date();
								   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
								  
								   String str = "delete from project_master_details WHERE project_id=:projectID and org_id=:orgID ;";				
									   
								   
								   Map hMap=new HashMap();
								   hMap.put("projectID", projectVO.getProjectID());
								   hMap.put("orgID", projectVO.getOrgID());
								   hMap.put("updatedBy", projectVO.getUpdatedBy());
								   hMap.put("updateDate", currentTimestamp);
								  
								   success=namedParameterJdbcTemplate.update(str, hMap);			
								
							}catch(Exception e){
								log.error("Exception in deleting project details "+e);
							}
							
							
							log.debug("Exit : public int deleteProjectDetails(ProjectDetailsVO projectVO)");
							return success;
						}
						
						public int unlinkProjectAssociationFromTransactions(ProjectDetailsVO projectVO){
							log.debug("Entry : public int unlinkProjectAssociationFromTransactions(ProjectDetailsVO projectVO)");
							int success=0;
							try{
								
								  java.util.Date today = new java.util.Date();
								   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
								   SocietyVO societyVO=societyService.getSocietyDetails(projectVO.getOrgID());
								  
								   String str = "UPDATE  account_ledger_entry_"+societyVO.getDataZoneID()+" l,account_transactions_"+societyVO.getDataZoneID()+" a SET  l.tx_le_tags = REPLACE(l.tx_le_tags, '#"+projectVO.getProjectAcronyms()+"','') WHERE a.tx_id=l.tx_id AND a.is_deleted=0 AND (l.tx_le_tags IS NOT NULL OR l.tx_le_tags!='') and a.org_id=l.org_id and a.org_id=:orgID ;";				
									   
								   
								   Map hMap=new HashMap();
								   hMap.put("projectID", projectVO.getProjectID());
								   hMap.put("orgID", projectVO.getOrgID());
								   hMap.put("updatedBy", projectVO.getUpdatedBy());
								   hMap.put("updateDate", currentTimestamp);
								   hMap.put("projectAccronyms", projectVO.getProjectAcronyms());
								  
								   success=namedParameterJdbcTemplate.update(str, hMap);			
								
							}catch(Exception e){
								log.error("Exception in unlinkProjectAssociationFromTransactions project details "+e);
							}
							
							
							log.debug("Exit : public int unlinkProjectAssociationFromTransactions(ProjectDetailsVO projectVO)");
							return success;
						}
						
						public int unlinkProjectAssociationFromLedgers(ProjectDetailsVO projectVO){
							log.debug("Entry : public int unlinkProjectAssociationFromLedgers(ProjectDetailsVO projectVO)");
							int success=0;
							try{
								
								  java.util.Date today = new java.util.Date();
								   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
								   SocietyVO societyVO=societyService.getSocietyDetails(projectVO.getOrgID());
								  
								   String str = "UPDATE account_ledgers_"+societyVO.getDataZoneID()+" a SET  a.group_tags = REPLACE(a.group_tags, '#"+projectVO.getProjectAcronyms()+"',''), a.project_tags = REPLACE(a.project_tags, '#"+projectVO.getProjectAcronyms()+"','') WHERE ((a.group_tags IS NOT NULL OR a.group_tags!='') OR (a.project_tags IS NOT NULL OR a.project_tags!='')) AND a.is_deleted=0 AND a.org_id= :orgID ;";				
									   
								   
								   Map hMap=new HashMap();
								   hMap.put("projectID", projectVO.getProjectID());
								   hMap.put("orgID", projectVO.getOrgID());
								   hMap.put("updatedBy", projectVO.getUpdatedBy());
								   hMap.put("updateDate", currentTimestamp);
								   hMap.put("projectAccronyms", projectVO.getProjectAcronyms());
								  
								   success=namedParameterJdbcTemplate.update(str, hMap);			
								
							}catch(Exception e){
								log.error("Exception in unlinkProjectAssociationFromLedgers project details "+e);
							}
							
							
							log.debug("Exit : public int unlinkProjectAssociationFromLedgers(ProjectDetailsVO projectVO)");
							return success;
						}

						//-----Add Project Item Details -------//
						public int addProjectItemDetails(ProjectDetailsVO projectVO){
							log.debug("Entry : public int addProjectItemDetails(ProjectDetailsVO projectVO)");
							int success=0;
							try{
								java.util.Date today = new java.util.Date();
								 final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
								  projectVO.setCreateDate(currentTimestamp);
								  projectVO.setUpdateDate(currentTimestamp);
								 
								  
								   String sql = "INSERT INTO project_allocation_relation "+					
									   "(org_id,project_id,ledger_id ,amount,amt_cal_type )  " +
						                "VALUES (:orgID,:projectID,:ledgerID,:allocationAmount,:allocationType)";
								   log.debug("Sql: "+sql);
								   
								   SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(projectVO);
								   KeyHolder keyHolder = new GeneratedKeyHolder();
								   getNamedParameterJdbcTemplate().update(sql, fileParameters, keyHolder);
								   success=keyHolder.getKey().intValue();
								
								   //getNamedParameterJdbcTemplate().batchUpdate(
								   
							}catch(Exception e){
								log.error("Exception in adding project details "+e);
							}
							
							
							log.debug("Exit : public int addProjectItemDetails(ProjectDetailsVO projectVO)");
							return success;
						}
					
											
						//-----Update Project Item Details -------//
							public int updateProjectItemDetails(ProjectDetailsVO projectVO){
								log.debug("Entry : public int updateProjectItemDetails(ProjectDetailsVO projectVO)");
								int success=0;
								try{
									  java.util.Date today = new java.util.Date();
									   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
									  
									   String str = "UPDATE project_allocation_relation SET amount=:allocationAmount," +
									   		" amt_cal_type=:allocationType WHERE id=:itemID and org_id=:orgID and ledger_id=:ledgerID ;";				
										   
									   
									   Map hMap=new HashMap();
									   hMap.put("itemID", projectVO.getItemID());
									   hMap.put("orgID", projectVO.getOrgID());
									   hMap.put("ledgerID", projectVO.getLedgerID());
									   hMap.put("allocationTyoe", projectVO.getAllocationType());
									   hMap.put("allocationAmount", projectVO.getAllocationAmount());
									  
									  
									   success=namedParameterJdbcTemplate.update(str, hMap);	
									
								}catch(Exception e){
									log.error("Exception in updating project details "+e);
								}
								
								
								log.debug("Exit : public int updateProjectItemDetails(ProjecttDetailsVO projectVO)");
								return success;
							}
						
							//-----Delete Project Item Details -------//
							public int deleteProjectItemDetails(ProjectDetailsVO projectVO){
										log.debug("Entry : public int deleteProjectItemDetails(ProjectDetailsVO ProjectVO)");
										int success=0;
										try{
											
											  java.util.Date today = new java.util.Date();
											   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
											  
											   String str = "UPDATE project_alloaction_relation SET is_deleted=1 WHERE item_id=:itemID and org_id=:orgID ";				
												   
											   
											   Map hMap=new HashMap();
											   hMap.put("itemID", projectVO.getItemID());
											   hMap.put("orgID", projectVO.getOrgID());
											
											  
											   success=namedParameterJdbcTemplate.update(str, hMap);			
											
										}catch(Exception e){
											log.error("Exception in deleting project details "+e);
										}
										
										
										log.debug("Exit : public int deleteProjectItemDetails(BudgetDetailsVO budgetVO)");
										return success;
									}
						
							
		public List getProjectDetailsList(int orgID){
								log.debug("Entry : public List getProjectDetailsList(int orgID)");
								List projectList=null;
								
								try {
									String query="SELECT * FROM project_master_details WHERE org_id=:orgID;";
									
									
									
									Map hMap = new HashMap();
									hMap.put("orgID",orgID);
									
									
									RowMapper RMapper = new RowMapper() {
										public Object mapRow(ResultSet rs, int rowNum)
												throws SQLException {
											  ProjectDetailsVO projectVO = new ProjectDetailsVO();
									            projectVO.setProjectID(rs.getInt("project_id"));
									            projectVO.setProjectName(rs.getString("project_name"));
									            projectVO.setDescription(rs.getString("description"));
									            projectVO.setOrgID(rs.getInt("org_id"));
									            projectVO.setProjectCost(rs.getBigDecimal("project_cost"));         
									            return projectVO;
										}
									};
									
									
									projectList=namedParameterJdbcTemplate.query(query, hMap, RMapper);
								} catch (DataAccessException e) {
									
									log.error("Exception : public int getProjectDetailsList "+e);
								} catch (Exception e) {
									
									log.error("Exception : public int getProjectDetailsList "+e);
								}
								
								log.debug("Exit : public List getProjectDetailsList(int orgID)");
								return projectList;
								
							}
						
	public List getProjectDetailsListTagWise(ProjectDetailsVO projectVO){
		log.debug("Entry : public List getProjectDetailsList(ProjectDetailsVO projectVO)");
		List projectList=null;
		
		try {
			String query="SELECT * FROM project_master_details WHERE org_id=:orgID and type=:type and is_deleted=0;";
			
			
			
			Map hMap = new HashMap();
			hMap.put("orgID",projectVO.getOrgID());
			hMap.put("type", projectVO.getType());
			
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  ProjectDetailsVO projectVO = new ProjectDetailsVO();
			            projectVO.setProjectID(rs.getInt("project_id"));
			            projectVO.setProjectName(rs.getString("project_name"));
			            projectVO.setDescription(rs.getString("description"));
			            projectVO.setOrgID(rs.getInt("org_id"));
			            projectVO.setType(rs.getString("type"));
			            projectVO.setProjectCost(rs.getBigDecimal("project_cost"));         
			            return projectVO;
				}
			};
			
			
			projectList=namedParameterJdbcTemplate.query(query, hMap, RMapper);
		} catch (DataAccessException e) {
			
			log.error("Exception : public int getProjectDetailsList "+e);
		} catch (Exception e) {
			
			log.error("Exception : public int getProjectDetailsList "+e);
		}
		
		log.debug("Exit : public List getProjectDetailsList(ProjectDetailsVO projectVO)");
		return projectList;
		
	}
	
	
	public List getProjectItemList(int orgID,int projectID){
		log.debug("Entry : public List getProjectItemList(int orgID,int projectID)");
		List projectList=null;
		
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String query="SELECT p.*,l.ledger_name  FROM project_allocation_relation p,account_ledgers_"+societyVO.getDataZoneID()+" l WHERE l.id=p.ledger_id and p.org_id=l.org_id and p.is_deleted=0 and p.project_id=:projectID and p.org_id=:orgID;";
			
			
			
			Map hMap = new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("projectID", projectID);
			
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  ProjectDetailsVO projectVO = new ProjectDetailsVO();
					    projectVO.setItemID(rs.getInt("id"));
			            projectVO.setProjectID(rs.getInt("project_id"));
			            projectVO.setLedgerName(rs.getString("ledger_name"));
			            projectVO.setOrgID(rs.getInt("org_id"));
			            projectVO.setAllocationAmount(rs.getBigDecimal("amount")); 
			            projectVO.setAllocationType(rs.getString("amt_cal_type"));
			            return projectVO;
				}
			};
			
			
			projectList=namedParameterJdbcTemplate.query(query, hMap, RMapper);
		} catch (DataAccessException e) {
			
			log.error("Exception : public int getProjectItemList(int orgID,int projectID) "+e);
		} catch (Exception e) {
			
			log.error("Exception : public int getProjectItemList(int orgID,int projectID) "+e);
		}
		
		log.debug("Exit : public List getProjectItemList(int orgID,int projectID)");
		return projectList;
		
	}
	
	
	public List getProjectDetailsListWithDetails(int orgID,String fromDate,String toDate){
		log.debug("Entry : public List getProjectDetailsListWithDetails(int orgID,String fromDate,String toDate)");
		List projectList=null;
		
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String query="SELECT pro.project_id,pro.org_id,pro.project_name,pro.description,pro.project_cost,p.amount,p.actualAmt FROM (SELECT * FROM project_master_details WHERE org_id=:orgID) AS  pro LEFT JOIN  "
					+ "(SELECT projects.*,SUM(actualAmount) AS actualAmt FROM (SELECT pa.org_id,  pm.project_id,pm.project_name,pm.description,pm.project_cost,pa.id,pa.amount,pa.ledger_id ,ag.category_name,ac.category_id,ac.group_name,al.ledger_name AS ledgerName,SUM(ale.amount) AS txAmount, (SUM(ale.amount)*pa.amount/100) AS actualAmount ,ac.id AS groupID "+
										" FROM account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups ac ,account_category ag ,project_master_details pm ,project_allocation_relation pa "+
										" WHERE ats.org_id=ale.org_id AND pm.org_id=pa.org_id AND pm.project_id=pa.project_id AND pa.ledger_id=ale.ledger_id AND ale.org_id=al.org_id AND pm.org_id=:orgID AND ats.tx_id=ale.tx_id AND ale.ledger_id=al.id AND al.sub_group_id=ac.id  AND ats.is_deleted=0 AND  ac.category_id=ag.category_id "+
										" AND ( ats.tx_date >= :fromDate AND ats.tx_date <= :toDate) GROUP BY pa.id ORDER BY ac.seq_no ) AS projects GROUP BY project_id ) as p ON p.project_id=pro.project_id;";
			
			
			
			Map hMap = new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", toDate);
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  ProjectDetailsVO projectVO = new ProjectDetailsVO();
			            projectVO.setProjectID(rs.getInt("project_id"));
			            projectVO.setProjectName(rs.getString("project_name"));
			            projectVO.setDescription(rs.getString("description"));
			            projectVO.setOrgID(rs.getInt("org_id"));
			            projectVO.setAllocationAmount(rs.getBigDecimal("amount"));
			            projectVO.setActualAmount(rs.getBigDecimal("actualAmt"));
			            projectVO.setProjectCost(rs.getBigDecimal("project_cost"));         
			            return projectVO;
				}
			};
			
			
			projectList=namedParameterJdbcTemplate.query(query, hMap, RMapper);
		} catch (DataAccessException e) {
			
			log.error("Exception : public int getProjectDetailsListWithDetails "+e);
		} catch (Exception e) {
			
			log.error("Exception : public int getProjectDetailsListWithDetails "+e);
		}
		
		log.debug("Exit : public List getProjectDetailsListWithDetails(int orgID,String fromDate,String toDate)");
		return projectList;
		
	}
	
	public List getLedgersListInProject(int orgID, int projectID,String fromDate,String toDate){
		log.debug("Entry : public List getLedgersListInProject(int orgID, int projectID,String fromDate,String toDate)");
		List projectList=null;
		
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String query="SELECT projects.*,SUM(actualAmount) AS actualAmt ,SUM(txAmt) AS txAmount,root_id  FROM (SELECT pa.org_id, pm.project_id,pm.project_name,pm.description,pm.project_cost,pa.id,pa.amount,pa.ledger_id ,ag.category_name,ac.category_id,ac.group_name,al.ledger_name AS ledgerName,SUM(ale.amount) AS txAmt, (SUM(ale.amount)*pa.amount/100) AS actualAmount ,ac.id AS groupID ,ac.root_id "+
										" FROM account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups ac ,account_category ag ,project_master_details pm ,project_allocation_relation pa "+
										" WHERE ats.org_id=ale.org_id AND pm.org_id=pa.org_id AND pm.project_id=pa.project_id AND pa.ledger_id=ale.ledger_id AND ale.org_id=al.org_id AND al.org_id=:orgID  AND pa.project_id=:projectID AND ats.tx_id=ale.tx_id AND ale.ledger_id=al.id AND al.sub_group_id=ac.id  AND ats.is_deleted=0 AND  ac.category_id=ag.category_id "+
										" AND ( ats.tx_date >= :fromDate AND ats.tx_date <= :toDate) GROUP BY pa.id ORDER BY ac.seq_no ) AS projects GROUP BY id ;";
			
			
			
			Map hMap = new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", toDate);
			hMap.put("projectID", projectID);
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  ProjectDetailsVO projectVO = new ProjectDetailsVO();
			            projectVO.setProjectID(rs.getInt("project_id"));
			            projectVO.setProjectName(rs.getString("project_name"));
			            projectVO.setDescription(rs.getString("description"));
			            projectVO.setOrgID(rs.getInt("org_id"));
			            projectVO.setAllocationAmount(rs.getBigDecimal("amount"));
			            projectVO.setActualAmount(rs.getBigDecimal("actualAmt"));
			            projectVO.setProjectCost(rs.getBigDecimal("project_cost"));     
			            projectVO.setLedgerName(rs.getString("ledgerName"));
			            projectVO.setCategoryName(rs.getString("category_name"));
			            projectVO.setGroupName(rs.getString("group_name"));
			            projectVO.setTxAmount(rs.getBigDecimal("txAmount"));
			            projectVO.setGroupID(rs.getInt("groupID"));
			            projectVO.setLedgerID(rs.getInt("ledger_id"));
			            projectVO.setRootID(rs.getInt("root_id"));
			            return projectVO;
				}
			};
			
			
			projectList=namedParameterJdbcTemplate.query(query, hMap, RMapper);
		} catch (DataAccessException e) {
			
			log.error("Exception : public List getLedgersListInProject(int orgID, int projectID,String fromDate,String toDate) "+e);
		} catch (Exception e) {
			
			log.error("Exception : public List getLedgersListInProject(int orgID, int projectID,String fromDate,String toDate) "+e);
		}
		
		log.debug("Exit : public List getLedgersListInProject(int orgID, int projectID,String fromDate,String toDate)");
		return projectList;
		
	}
	
	//========= New cost center list ===================//
	
	public List getProjectDetailsListWithDetailsTagWise(int orgID,String type){
		log.debug("Entry : public List getProjectDetailsListWithDetailsTagWise(int orgID)");
		List projectList=null;
		String strType="";
		if((type!=null)&&(type.length()>0)){
			strType=" type=:type AND ";
		}
		
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String query=" SELECT * FROM project_master_details WHERE "+strType+" org_id=:orgID AND is_deleted=0; ";
			
			
			
			Map hMap = new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("type", type);
		
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  ProjectDetailsVO projectVO = new ProjectDetailsVO();
			            projectVO.setProjectID(rs.getInt("project_id"));
			            projectVO.setProjectName(rs.getString("project_name"));
			            projectVO.setProjectAcronyms(rs.getString("project_acronyms"));
			            projectVO.setDescription(rs.getString("description"));
			            projectVO.setOrgID(rs.getInt("org_id"));
			            projectVO.setProjectCost(rs.getBigDecimal("project_cost"));
			            projectVO.setType(rs.getString("type"));    
			            projectVO.setReferenceID(rs.getString("reference_id"));    
			            return projectVO;
				}
			};
			
			
			projectList=namedParameterJdbcTemplate.query(query, hMap, RMapper);
		} catch (DataAccessException e) {
			
			log.error("Exception : public int getProjectDetailsListWithDetailsTagWise "+e);
		} catch (Exception e) {
			
			log.error("Exception : public int getProjectDetailsListWithDetailsTagWise "+e);
		}
		
		log.debug("Exit : public List getProjectDetailsListWithDetailsTagWise(int orgID)");
		return projectList;
		
	}
	
	//===========New Cost Center Group  list===================//
		public List getLedgersSummaryList(int orgID, String  projectName,String fromDate,String toDate){
			log.debug("Entry : public List getLedgersListInProjectTagWise(int orgID, int projectID,String fromDate,String toDate)");
			List projectList=null;
			
			try {
				SocietyVO societyVO=societyService.getSocietyDetails(orgID);
				String orgTypeCondition="";
				
				if(societyVO.getOrgType().equalsIgnoreCase("S")){
					orgTypeCondition="seq_for_soc";
				}else orgTypeCondition="category_id";
				
				String query="SELECT projects.*,SUM(actualAmount) AS actualAmt ,SUM(txAmt) AS txAmount,root_id FROM(SELECT a.org_id,pm.project_id,pm.project_name,pm.project_acronyms,l.id AS ledgerID,l.ledger_name AS ledgerName,SUM(al.amount) AS txAmt, (SUM(al.amount)) AS actualAmount ,ac.id AS groupID ,ac.root_id ,pm.project_cost ,ag.seq_for_soc, ag.category_id ,ac.group_name"
						+ " FROM account_ledgers_"+societyVO.getSocietyID()+" l,account_transactions_"+societyVO.getSocietyID()+" a,account_ledger_entry_"+societyVO.getSocietyID()+" al,account_groups ac,account_category ag ,project_master_details pm WHERE a.tx_id=al.tx_id "+
											" AND a.org_id=:orgID AND a.org_id=pm.org_id AND l.sub_group_id=ac.id AND ac.category_id=ag.category_id AND a.is_deleted=0 AND al.ledger_id=l.id "
											+ "AND ( a.tx_date >= :fromDate AND a.tx_date <= :toDate) AND pm.project_acronyms=:projectName AND ((al.tx_le_tags LIKE '%#"+projectName+"%')OR(l.project_tags LIKE '%#"+projectName+"')) GROUP BY l.id) AS projects GROUP BY  groupID ORDER BY "+orgTypeCondition+" ;";

				log.debug("Query : "+query);
				
				
				Map hMap = new HashMap();
				hMap.put("orgID",orgID);
				hMap.put("fromDate", fromDate);
				hMap.put("toDate", toDate);
				hMap.put("projectName", projectName);
				
				RowMapper RMapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						  ProjectDetailsVO projectVO = new ProjectDetailsVO();
				            projectVO.setProjectID(rs.getInt("project_id"));
				            projectVO.setProjectName(rs.getString("project_name"));
				            //projectVO.setDescription(rs.getString("description"));
				            projectVO.setOrgID(rs.getInt("org_id"));
				          //  projectVO.setAllocationAmount(rs.getBigDecimal("amount"));
				            projectVO.setActualAmount(rs.getBigDecimal("actualAmt"));
				            projectVO.setProjectCost(rs.getBigDecimal("project_cost"));     
				            projectVO.setLedgerName(rs.getString("ledgerName"));
				           // projectVO.setCategoryName(rs.getString("category_name"));
				            projectVO.setGroupName(rs.getString("group_name"));
				            projectVO.setTxAmount(rs.getBigDecimal("txAmount"));
				            projectVO.setGroupID(rs.getInt("groupID"));
				            projectVO.setLedgerID(rs.getInt("ledgerID"));
				            projectVO.setRootID(rs.getInt("root_id"));
				            projectVO.setProjectAcronyms(rs.getString("project_acronyms"));
				            return projectVO;
					}
				};
				
				
				projectList=namedParameterJdbcTemplate.query(query, hMap, RMapper);
			} catch (DataAccessException e) {
				
				log.error("Exception : public List getLedgersListInProjectTagWise(int orgID, int projectID,String fromDate,String toDate) "+e);
			} catch (Exception e) {
				
				log.error("Exception : public List getLedgersListInProjectTagWise(int orgID, int projectID,String fromDate,String toDate) "+e);
			}
			
			log.debug("Exit : public List getLedgersListInProjectTagWise(int orgID, int projectID,String fromDate,String toDate)");
			return projectList;
			
		}
	
	//===========New Cost Center ledger list===================//
	public List getLedgersListInProjectTagWise(int orgID, String  projectName,String fromDate,String toDate,int groupID,int summary,int rootID){
		log.debug("Entry : public List getLedgersListInProjectTagWise(int orgID, int projectID,String fromDate,String toDate,int groupID)");
		List projectList=null;
		String strCondition="";
		String strGroupCondition="";
		String strRootID="";
		if(summary==1){
			strGroupCondition=" groupID order by seq_no";
		}else strGroupCondition=" ledgerID order by ledgerID";
		if(groupID>0){
			strCondition=" AND ac.id=:groupID ";
		}
		if(rootID>0){
			strRootID=" AND ac.root_id=:rootID ";
		}
		
		String orCondition="";
		String andCondition="";
		//Remove whitespace and split by comma 
        List<String> result = Arrays.asList(projectName.split("\\s*,\\s*"));
        if(result.size()==1){
        	orCondition=orCondition+" pm.project_acronyms='"+projectName.trim()+"'";
        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projectName.trim()+"%')OR(l.project_tags LIKE '%#"+projectName.trim()+"%'))";
        	
        }else{
        for(int i=0;result.size()>i;i++){
        	String projectAcronyms=result.get(i);
        	if(i==0){
        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronyms.trim()+"'";
        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(l.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
        	}else{
        	orCondition=orCondition+" or pm.project_acronyms='"+projectAcronyms.trim()+"'";
        	andCondition=andCondition+" and ((al.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(l.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
        	}}}
		
		try {
			
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String query="SELECT projects.*,SUM(actualAmount) AS actualAmt ,SUM(txAmt) AS txAmount,root_id FROM(SELECT a.org_id,pm.project_id,pm.project_name,pm.project_acronyms,l.id AS ledgerID,l.ledger_name AS ledgerName,SUM(al.amount) AS txAmt, (SUM(al.amount)) AS actualAmount ,ac.id AS groupID ,ac.root_id ,pm.project_cost,ac.group_name, ac.seq_no "
					+ " FROM account_ledgers_"+societyVO.getSocietyID()+" l,account_transactions_"+societyVO.getSocietyID()+" a,account_ledger_entry_"+societyVO.getSocietyID()+" al,account_groups ac,account_category ag ,project_master_details pm WHERE a.tx_id=al.tx_id "+
										" AND a.org_id=:orgID AND a.org_id=pm.org_id AND l.sub_group_id=ac.id AND ac.category_id=ag.category_id AND a.is_deleted=0 AND al.ledger_id=l.id "+strCondition+strRootID
										+ " AND ( a.tx_date >= :fromDate AND a.tx_date <= :toDate) AND ("+orCondition+") AND ("+andCondition+" ) GROUP BY l.id) AS projects GROUP BY "+strGroupCondition;

			log.debug("Query : "+query);
			
			
			Map hMap = new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", toDate);
			hMap.put("projectName", projectName);
			hMap.put("groupID", groupID);
			hMap.put("rootID", rootID);
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  ProjectDetailsVO projectVO = new ProjectDetailsVO();
			            projectVO.setProjectID(rs.getInt("project_id"));
			            projectVO.setProjectName(rs.getString("project_name"));
			            //projectVO.setDescription(rs.getString("description"));
			            projectVO.setOrgID(rs.getInt("org_id"));
			          //  projectVO.setAllocationAmount(rs.getBigDecimal("amount"));
			            projectVO.setActualAmount(rs.getBigDecimal("actualAmt"));
			            projectVO.setProjectCost(rs.getBigDecimal("project_cost"));     
			            projectVO.setLedgerName(rs.getString("ledgerName"));
			           // projectVO.setCategoryName(rs.getString("category_name"));
			            projectVO.setGroupName(rs.getString("group_name"));
			            projectVO.setTxAmount(rs.getBigDecimal("txAmount"));
			            projectVO.setGroupID(rs.getInt("groupID"));
			            projectVO.setLedgerID(rs.getInt("ledgerID"));
			            projectVO.setRootID(rs.getInt("root_id"));
			            projectVO.setProjectAcronyms(rs.getString("project_acronyms"));
			            return projectVO;
				}
			};
			
			
			projectList=namedParameterJdbcTemplate.query(query, hMap, RMapper);
		} catch (DataAccessException e) {
			
			log.error("Exception : public List getLedgersListInProjectTagWise(int orgID, int projectID,String fromDate,String toDate,int groupID) "+e);
		} catch (Exception e) {
			
			log.error("Exception : public List getLedgersListInProjectTagWise(int orgID, int projectID,String fromDate,String toDate,int groupID) "+e);
		}
		
		log.debug("Exit : public List getLedgersListInProjectTagWise(int orgID, int projectID,String fromDate,String toDate,int groupID)");
		return projectList;
		
	}
	
	
	public List getLedgersListInProjectCategoryWise(int orgID, List  categoryList,String fromDate,String toDate,int groupID,int summary,int rootID,final int categoryID){
		log.debug("Entry : public List getLedgersListInProjectCategoryWise(int orgID, List  categoryList,String fromDate,String toDate,int groupID,int summary,int rootID)");
		List projectList=null;
		String strCondition="";
		String strGroupCondition="";
		String strRootID="";
		if(summary==1){
			strGroupCondition=" groupID order by seq_no";
		}else strGroupCondition=" ledgerID order by ledgerID";
		if(groupID>0){
			strCondition=" AND ac.id=:groupID ";
		}
		if(rootID>0){
			strRootID=" AND ac.root_id=:rootID ";
		}
		
		String orCondition="";
		String andCondition="";
		
		for(int i=0;categoryList.size()>i;i++){
			ProjectDetailsVO projVO=(ProjectDetailsVO) categoryList.get(i);
			  if(categoryList.size()==1){
		        	orCondition=orCondition+" pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
		        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
		        	
		        }else{
		       
		        	if(i==0){
		        	orCondition=orCondition+" pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
		        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
		        	}else{
		        	orCondition=orCondition+" or pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
		        	andCondition=andCondition+" or ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
		        	}}
			
		}
		
		
		try {
			
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String query="SELECT projects.*,SUM(actualAmount) AS actualAmt ,SUM(txAmt) AS txAmount,root_id FROM(SELECT h.org_id,h.project_id,h.project_name,h.project_acronyms,h.ledgerID,h.ledgerName,SUM(h.txAmt) AS txAmt, (SUM(h.actualAmount)) AS actualAmount ,h.groupID ,h.root_id ,h.project_cost,h.group_name, h.seq_no FROM (SELECT a.org_id,pm.project_id,pm.project_name,pm.project_acronyms,l.id AS ledgerID,l.ledger_name AS ledgerName,(al.amount) AS txAmt, ((al.amount)) AS actualAmount ,ac.id AS groupID ,ac.root_id ,pm.project_cost,ac.group_name, ac.seq_no "
					+ " FROM account_ledgers_"+societyVO.getSocietyID()+" l,account_transactions_"+societyVO.getSocietyID()+" a,account_ledger_entry_"+societyVO.getSocietyID()+" al,account_groups ac,account_category ag ,project_master_details pm WHERE a.tx_id=al.tx_id "+
										" AND a.org_id=:orgID AND a.org_id=pm.org_id AND l.sub_group_id=ac.id AND ac.category_id=ag.category_id AND a.is_deleted=0 AND al.ledger_id=l.id "+strCondition+strRootID
										+ " AND ( a.tx_date >= :fromDate AND a.tx_date <= :toDate) AND ("+orCondition+") AND ("+andCondition+" ) GROUP BY al.id,l.id) AS h GROUP BY ledgerID) AS projects GROUP BY "+strGroupCondition;

			log.debug("Query : "+query);
			
			
			Map hMap = new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("fromDate", fromDate);
			hMap.put("toDate", toDate);
			hMap.put("groupID", groupID);
			hMap.put("rootID", rootID);
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  ProjectDetailsVO projectVO = new ProjectDetailsVO();
			            projectVO.setProjectID(rs.getInt("project_id"));
			            projectVO.setProjectName(rs.getString("project_name"));
			            //projectVO.setDescription(rs.getString("description"));
			            projectVO.setOrgID(rs.getInt("org_id"));
			          //  projectVO.setAllocationAmount(rs.getBigDecimal("amount"));
			            projectVO.setActualAmount(rs.getBigDecimal("actualAmt"));
			            projectVO.setProjectCost(rs.getBigDecimal("project_cost"));     
			            projectVO.setLedgerName(rs.getString("ledgerName"));
			           // projectVO.setCategoryName(rs.getString("category_name"));
			            projectVO.setGroupName(rs.getString("group_name"));
			            projectVO.setTxAmount(rs.getBigDecimal("txAmount"));
			            projectVO.setGroupID(rs.getInt("groupID"));
			            projectVO.setLedgerID(rs.getInt("ledgerID"));
			            projectVO.setRootID(rs.getInt("root_id"));
			            projectVO.setProjectCategoryID(categoryID);
			            return projectVO;
				}
			};
			
			
			projectList=namedParameterJdbcTemplate.query(query, hMap, RMapper);
		} catch (DataAccessException e) {
			
			log.error("Exception : public List getLedgersListInProjectCategoryWise(int orgID, List  categoryList,String fromDate,String toDate,int groupID,int summary,int rootID) "+e);
		} catch (Exception e) {
			
			log.error("Exception : public List getLedgersListInProjectCategoryWise(int orgID, List  categoryList,String fromDate,String toDate,int groupID,int summary,int rootID) "+e);
		}
		
		log.debug("Exit : public List getLedgersListInProjectCategoryWise(int orgID, List  categoryList,String fromDate,String toDate,int groupID,int summary,int rootID)");
		return projectList;
		
	}
	
	//========= New cost center list ===================//

	public List getProjectItemListTagWise(int orgID,String projectAcronyms){
		log.debug("Entry : public List getProjectItemListTagWise(int orgID,String projectName)");
		List projectList=null;
		
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String query="SELECT l.id,p.*,l.ledger_name  FROM project_master_details p,account_ledgers_"+societyVO.getSocietyID()+" l WHERE l.project_tags LIKE '%#"+projectAcronyms+"%' AND p.org_id=l.org_id AND l.is_deleted=0 AND p.project_acronyms=:projectAcronyms AND p.org_id=:orgID ;";
			
			
			
			Map hMap = new HashMap();
			hMap.put("orgID",orgID);
			hMap.put("projectAcronyms", projectAcronyms);
			
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  ProjectDetailsVO projectVO = new ProjectDetailsVO();
					    projectVO.setItemID(rs.getInt("id"));
			            projectVO.setProjectID(rs.getInt("project_id"));
			            projectVO.setProjectName(rs.getString("project_name"));
			            projectVO.setProjectAcronyms(rs.getString("project_acronyms"));
			            projectVO.setLedgerName(rs.getString("ledger_name"));
			            projectVO.setOrgID(rs.getInt("org_id"));
			            
			            return projectVO;
				}
			};
			
			
			projectList=namedParameterJdbcTemplate.query(query, hMap, RMapper);
		} catch (DataAccessException e) {
			
			log.error("Exception : public int getProjectItemListTagWise(int orgID,String projectName) "+e);
		} catch (Exception e) {
			
			log.error("Exception : public int getProjectItemListTagWise(int orgID,String projectName) "+e);
		}
		
		log.debug("Exit : public List getProjectItemListTagWise(int orgID,String projectName)");
		return projectList;
		
	}
	//============== Cost center category Details ============//
	public List getCostCeneterCategoryList(int orgID){
		log.debug("Entry : public List getCostCeneterCategoryList(int orgID)");
		List projectList=null;
		
		try {
			String query="SELECT * FROM cc_category_details WHERE is_deleted=0 and org_id=:orgID;";
			
			
			
			Map hMap = new HashMap();
			hMap.put("orgID",orgID);
			
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  ProjectDetailsVO projectVO = new ProjectDetailsVO();
			            projectVO.setProjectCategoryID(rs.getInt("id"));
			            projectVO.setProjectCategoryName(rs.getString("category_name"));
			            projectVO.setDescription(rs.getString("description"));
			            projectVO.setOrgID(rs.getInt("org_id"));
			                  
			            return projectVO;
				}
			};
			
			
			projectList=namedParameterJdbcTemplate.query(query, hMap, RMapper);
		} catch (DataAccessException e) {
			
			log.error("Exception : public int getCostCeneterCategoryList "+e);
		} catch (Exception e) {
			
			log.error("Exception : public int getCostCeneterCategoryList "+e);
		}
		
		log.debug("Exit : public List getCostCeneterCategoryList(int orgID)");
		return projectList;
		
	}
	
	public List getCostCeneterCategoryLinkingList(int categoryID){
		log.debug("Entry : public List getCostCeneterCategoryLinkingList(int categoryID)");
		List projectList=null;
		
		try {
			String query="SELECT * FROM project_master_details p,cc_category_linking c WHERE p.project_id=c.cc_project_id and p.is_deleted=0 AND c.cc_category_id=:categoryID;";
			
			
			
			Map hMap = new HashMap();
			hMap.put("categoryID",categoryID);
			
			
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					  ProjectDetailsVO projectVO = new ProjectDetailsVO();
			            projectVO.setProjectID(rs.getInt("project_id"));
			            projectVO.setProjectName(rs.getString("project_name"));
			            projectVO.setProjectAcronyms(rs.getString("project_acronyms"));
			            projectVO.setDescription(rs.getString("description"));
			            projectVO.setOrgID(rs.getInt("org_id"));
			            projectVO.setType(rs.getString("type"));     
			            return projectVO;
				}
			};
			
			
			projectList=namedParameterJdbcTemplate.query(query, hMap, RMapper);
	
		} catch (Exception e) {
			
			log.error("Exception : public int getCostCeneterCategoryLinkingList "+e);
		}
		
		log.debug("Exit : public List getCostCeneterCategoryLinkingList(int orgID)");
		return projectList;
		
	}
	
	 public List getCashBasedCostCenterReport(String fromDate, String uptoDate,
				int societyId, int isConsolidated,String projectName) {
			List incExpList = new ArrayList();
			try {
				log
						.debug("Entry: public List getCashBasedCostCenterReport(String fromDate, String uptoDate,int societyId, String txType,int isConsolidated) "+ fromDate + uptoDate);
				String orgCondition="";
				String orgClause="";
				String strTxTypeClause="";
				String orCondition="";
				String andCondition="";
				if(isConsolidated==0){
					orgCondition=" AND l.org_id=:orgID ";
					orgClause=" And al.org_id=:orgID ";
				}
				
				//Remove whitespace and split by comma 
		        List<String> result = Arrays.asList(projectName.split("\\s*,\\s*"));
		        if(result.size()==1){
		        	orCondition=orCondition+" pm.project_acronyms='"+projectName.trim()+"'";
		        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projectName.trim()+"%')OR(al.project_tags LIKE '%#"+projectName.trim()+"%'))";
		        	
		        }else{
		        for(int i=0;result.size()>i;i++){
		        	String projectAcronyms=result.get(i);
		        	if(i==0){
		        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronyms.trim()+"'";
		        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(al.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
		        	}else{
		        	orCondition=orCondition+" or pm.project_acronyms='"+projectAcronyms.trim()+"'";
		        	andCondition=andCondition+" and ((ale.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(al.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
		        	}}}
				
				SocietyVO societyVO=societyService.getSocietyDetails(societyId);
			
				String sqlQuery ="SELECT SUM(ABS(tx_negative)) AS txNegative,SUM(ABS(tx_positive)) AS txPositive,SUM(tx_sum) AS txSum,tabl.* FROM (SELECT root_id,g.id AS groupID,g.category_id,g.group_name,ale.type,ale.tx_id,ats.tx_type, al.id AS tx_ledger_id,al.ledger_name "+
							" ,(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END) AS tx_negative, (CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END) AS tx_positive, (SUM( ale.amount)) AS tx_sum FROM  (SELECT a.* FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_ledgers_"+societyVO.getDataZoneID()+" l ,account_groups g  "+
						    " WHERE a.is_deleted=0 AND a.tx_id=al.tx_id AND a.org_id=l.org_id "+orgCondition+" AND a.tx_date BETWEEN :fromDate AND :toDate AND al.ledger_id=l.id AND l.sub_group_id=g.id AND a.tx_type!='Contra' AND (g.category_id=3 OR g.category_id=101 OR g.category_id=64 OR g.category_id=65 )) AS ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups g,project_master_details pm WHERE ats.tx_date "+
	                        " BETWEEN :fromDate AND :toDate AND ats.is_deleted = 0 AND ats.org_id=pm.org_id AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND ats.org_id=ale.org_id AND ale.org_id=al.org_id "+orgClause+ 
	                        " AND al.sub_group_id = g.id AND ( "+orCondition+" ) AND ( "+andCondition+"  ) AND  ats.tx_type!='Contra'  GROUP BY ale.id) AS tabl WHERE  tabl.category_id!=3 AND tabl.category_id!=101 AND tabl.category_id!=64 AND tabl.category_id!=65 GROUP BY tabl.groupID,TYPE ORDER BY group_name;";
				log.debug("query : " + sqlQuery);

				Map hashMap = new HashMap();
				hashMap.put("fromDate", fromDate);
				hashMap.put("toDate", uptoDate);
				hashMap.put("orgID", societyId);

				RowMapper rMapper = new RowMapper() {
					String tempGroupName = "";

					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						// TODO Auto-generated method stub
						ReportDetailsVO rptVO = new ReportDetailsVO();
						rptVO.setLedgerName(rs.getString("ledger_name"));
						rptVO.setLedgerID(rs.getInt("tx_ledger_id"));
						rptVO.setGroupName(rs.getString("group_name"));
						rptVO.setTxType(rs.getString("type"));
						rptVO.setCreditBalance(rs.getBigDecimal("txNegative").abs().setScale(2, RoundingMode.HALF_UP));
						rptVO.setDebitBalance(rs.getBigDecimal("txPositive").abs().setScale(2, RoundingMode.HALF_UP));
						rptVO.setGroupID(rs.getInt("groupID"));
						rptVO.setRootID(rs.getInt("root_id"));
						
						
					

						return rptVO;
					}
				};
				incExpList = namedParameterJdbcTemplate
						.query(sqlQuery, hashMap, rMapper);
				//	logger.info("###############################"+incExpList.size()+incExpList);

			} catch (BadSqlGrammarException ex) {
				log.info("Exception in getCashBasedCostCenterReport: " + ex);
			} catch (Exception ex) {
				log.error("Exception in getCashBasedCostCenterReport : "
						+ ex);
			}
			log
					.debug("Exit:public List getCashBasedCostCenterReport(String fromDate, String uptoDate,int societyId, String txType,int isConsolidated)"
							+ incExpList.size());
			return incExpList;

		}
	 
	 public List getCashBasedCostCenterDetailsReport(String fromDate, String uptoDate,
				int societyId,int isConsolidated,String projectName) {
			List incExpList = new ArrayList();
			try {
				log
						.debug("Entry: public List getCashBasedReceiptPaymentDetailsReport(String fromDate, String uptoDate,int societyId, String txType,int isConsolidated)"
								+ fromDate + uptoDate);
				String strTxTypeClause="";
				String orgCondition="";
				String orgClause="";
				String andCondition="";
				String orCondition="";
				if(isConsolidated==0){
					orgCondition=" AND l.org_id=:orgID ";
					orgClause=" And al.org_id=:orgID ";
				}
				 
				//Remove whitespace and split by comma 
		        List<String> result = Arrays.asList(projectName.split("\\s*,\\s*"));
		        if(result.size()==1){
		        	orCondition=orCondition+" pm.project_acronyms='"+projectName.trim()+"'";
		        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projectName.trim()+"%')OR(al.project_tags LIKE '%#"+projectName.trim()+"%'))";
		        	
		        }else{
		        for(int i=0;result.size()>i;i++){
		        	String projectAcronyms=result.get(i);
		        	if(i==0){
		        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronyms.trim()+"'";
		        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(al.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
		        	}else{
		        	orCondition=orCondition+" or pm.project_acronyms='"+projectAcronyms.trim()+"'";
		        	andCondition=andCondition+" and ((ale.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(al.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
		        	}}}
			
				SocietyVO societyVO=societyService.getSocietyDetails(societyId);
				String sqlQuery ="SELECT SUM(ABS(tx_negative)) AS txNegative,SUM(ABS(tx_positive)) AS txPositive,SUM(tx_sum) AS txSum,tabl.* fROM(SELECT root_id,g.id AS groupID,g.group_name,g.category_id,ale.type,ale.tx_id,ats.tx_type, al.id AS tx_ledger_id,al.ledger_name "+
						" ,(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END) AS tx_negative, (CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END) AS tx_positive, (SUM( ale.amount)) AS tx_sum FROM (SELECT a.* FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_ledgers_"+societyVO.getDataZoneID()+" l ,account_groups g "+
					    " WHERE a.is_deleted=0 AND a.tx_id=al.tx_id AND a.org_id=l.org_id "+orgCondition+" AND a.tx_date BETWEEN :fromDate AND :toDate AND al.ledger_id=l.id and l.sub_group_id=g.id AND a.tx_type!='Contra' AND (g.category_id=3 OR g.category_id=101 OR g.category_id=64 OR g.category_id=65 )) AS ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups g, project_master_details pm WHERE ats.tx_date "+
	                    " BETWEEN :fromDate AND :toDate AND ats.is_deleted = 0 AND ats.org_id=pm.org_id AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND ats.org_id=ale.org_id AND ale.org_id=al.org_id "+orgClause+ 
	                    " AND al.sub_group_id = g.id AND ( "+orCondition+" ) AND ( "+andCondition+"  ) AND  ats.tx_type!='Contra'  GROUP BY ale.id) AS tabl WHERE  tabl.category_id!=3 AND tabl.category_id!=101 AND tabl.category_id!=64 AND tabl.category_id!=65 GROUP BY tabl.tx_ledger_id,TYPE ORDER BY group_name;";
			log.debug("query : " + sqlQuery);

				Map hashMap = new HashMap();
				hashMap.put("fromDate", fromDate);
				hashMap.put("toDate", uptoDate);
				hashMap.put("orgID",societyId);

				RowMapper rMapper = new RowMapper() {
					String tempGroupName = "";

					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						// TODO Auto-generated method stub
						ReportDetailsVO rptVO = new ReportDetailsVO();
						rptVO.setLedgerName(rs.getString("ledger_name"));
						rptVO.setLedgerID(rs.getInt("tx_ledger_id"));
						rptVO.setGroupName(rs.getString("group_name"));
						rptVO.setTxType(rs.getString("type"));
						rptVO.setCreditBalance(rs.getBigDecimal("txNegative").abs().setScale(2, RoundingMode.HALF_UP));
						rptVO.setDebitBalance(rs.getBigDecimal("txPositive").abs().setScale(2, RoundingMode.HALF_UP));
						rptVO.setGroupID(rs.getInt("groupID"));
						rptVO.setRootID(rs.getInt("root_id"));
						
										
						return rptVO;
					}
				};
				incExpList = namedParameterJdbcTemplate
						.query(sqlQuery, hashMap, rMapper);
				//	logger.info("###############################"+incExpList.size()+incExpList);

			} catch (BadSqlGrammarException ex) {
				log.info("Exception in getCashBasedReceiptPaymentDetailsReport: " + ex);
			} catch (Exception ex) {
				log.error("Exception in getCashBasedReceiptPaymentDetailsReport : "
						+ ex);
			}
			log
					.debug("Exit: public List getCashBasedReceiptPaymentDetailsReport(String fromDate, String uptoDate,	int societyId, String txType,int isConsolidated)"
							+ incExpList.size());
			return incExpList;

		}
	 
	 public List getMonthWiseCashBasedCostCenterReportForChart(int societyID,String fromDate,String uptoDate,String type, int isConsolidated,String projectName)
	 	{   
	 	   List groupNameList=new ArrayList();
	 		try
	 		{
	 			log.debug("Entry : public List getMonthWiseCashBasedCostCenterReportForChart(int intSocietyID)");
	 			String strWhereClause = "";
	 			String strGroupClause = "";
	 			String strColumn="";
	 			String strTxTypeClause=" ";
	 			
	 			String orgCondition="";
				String orgClause="";
				String andCondition="";
				String orCondition="";
				if(isConsolidated==0){
					orgCondition=" AND l.org_id=:orgID ";
					orgClause=" And al.org_id=:orgID ";
				}
	 			
				if (type.equalsIgnoreCase("R")){//Root
					strWhereClause = "  WHERE ( alg.sub_group_id!=4 and alg.sub_group_id!=5 and alg.sub_group_id!=95 ) GROUP BY txTable.type ORDER BY alg.group_name ";
				    strGroupClause=" table_secondary.type, ats.tx_type ";
				    strColumn="";
	 		     }else if(type.equalsIgnoreCase("G")){//Account Group
					
					strGroupClause=" table_secondary.ledger_id, ats.tx_type ";
					strColumn=" SUM(AprNeg) AS AprNeg,SUM(AprPos) AS AprPos,SUM(MayNeg) AS MayNeg,SUM(MayPos) AS MayPos,SUM(JunNeg) AS JunNeg,SUM(JunPos) AS JunPos,SUM(JulNeg) AS JulNeg,SUM(JulPos) AS JulPos,SUM(AugNeg) AS AugNeg,SUM(AugPos) AS AugPos,"+
                            " SUM(SepNeg) AS SepNeg,SUM(SepPos) AS SepPos, "+
                           " SUM(OctNeg) AS OctNeg,SUM(OctPos) AS OctPos,SUM(NovNeg) AS NovNeg,SUM(NovPos) AS NovPos,SUM(DecNeg) AS DecNeg,SUM(DecPos) AS DecPos,SUM(JanNeg) AS JanNeg,SUM(JanPos) AS JanPos,SUM(FebNeg) AS FebNeg,SUM(FebPos) AS FebPos,"+
                            " SUM(MarNeg) AS MarNeg,SUM(MarPos) AS MarPos,";
				}
				
				//Remove whitespace and split by comma 
		        List<String> result = Arrays.asList(projectName.split("\\s*,\\s*"));
		        if(result.size()==1){
		        	orCondition=orCondition+" pm.project_acronyms='"+projectName.trim()+"'";
		        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projectName.trim()+"%')OR(al.project_tags LIKE '%#"+projectName.trim()+"%'))";
		        	
		        }else{
		        for(int i=0;result.size()>i;i++){
		        	String projectAcronyms=result.get(i);
		        	if(i==0){
		        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronyms.trim()+"'";
		        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(al.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
		        	}else{
		        	orCondition=orCondition+" or pm.project_acronyms='"+projectAcronyms.trim()+"'";
		        	andCondition=andCondition+" and ((ale.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(al.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
		        	}}}
				
				
				 SocietyVO societyVO=societyService.getSocietyDetails(societyID);
								
				String sql="SELECT SUM(ABS(tx_negative)) AS txNegative,SUM(ABS(tx_positive)) AS txPositive,SUM(tx_sum) AS txSum,"+strColumn+" tabl.* fROM(SELECT "+
					     "CASE WHEN MONTHNAME(ats.tx_date) = 'April' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS AprNeg, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'April' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS AprPos, "+												
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'May' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS MayNeg, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'May' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS MayPos, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'June' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS JunNeg, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'June' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS JunPos,"+												
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'July' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS JulNeg,"+ 
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'July' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS JulPos, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'August' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS AugNeg,"+ 
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'August' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS AugPos, "+												
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'September' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS SepNeg, "+ 
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'September' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS SepPos, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'October' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS OctNeg, "+ 
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'October' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS OctPos, "+													
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'November' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS NovNeg, "+ 
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'November' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS NovPos, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'December' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS DecNeg, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'December' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS DecPos, "+												
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'January' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS JanNeg, "+ 
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'January' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS JanPos, "+
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'February' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS FebNeg, "+ 
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'February' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS FebPos, "+												
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'March' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS MarNeg, "+ 
						 " CASE WHEN MONTHNAME(ats.tx_date) = 'March' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS MarPos,"+
						" root_id,g.id AS groupID,g.group_name,g.category_id,ale.type,ale.tx_id,ats.tx_type, al.id AS tx_ledger_id,al.ledger_name "+
						" ,(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END) AS tx_negative, (CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END) AS tx_positive, (SUM( ale.amount)) AS tx_sum FROM (SELECT a.* FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_ledgers_"+societyVO.getDataZoneID()+" l ,account_groups g"+
					    " WHERE a.is_deleted=0 AND a.tx_id=al.tx_id AND a.org_id=l.org_id "+orgCondition+" AND a.tx_date BETWEEN :fromDate AND :toDate AND al.ledger_id=l.id and l.sub_group_id=g.id AND a.tx_type!='Contra' AND (g.category_id=3 OR g.category_id=101 OR g.category_id=64 OR g.category_id=65 )) AS ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups g, project_master_details pm WHERE ats.tx_date "+
	                    " BETWEEN :fromDate AND :toDate AND ats.is_deleted = 0 AND ats.org_id=pm.org_id AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND ats.org_id=ale.org_id AND ale.org_id=al.org_id "+orgClause+ 
	                    " AND al.sub_group_id = g.id AND ( "+orCondition+" ) AND ( "+andCondition+"  ) AND  ats.tx_type!='Contra'  GROUP BY ale.id) AS tabl WHERE  tabl.category_id!=3 AND tabl.category_id!=101 AND tabl.category_id!=64 AND tabl.category_id!=65 GROUP BY tabl.groupID,TYPE ORDER BY group_name;";
				
				
				
				log.debug("Query: "+sql);
				Map hMap = new HashMap();
				hMap.put("orgID", societyID);
				hMap.put("fromDate", fromDate);
				hMap.put("toDate",uptoDate);

				RowMapper rMapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int num) throws SQLException {
						// TODO Auto-generated method stub
						MonthwiseChartVO reportVO=new MonthwiseChartVO();
						reportVO.setTransactionType(rs.getString("tx_type"));
						reportVO.setRootGroupID(rs.getInt("root_id"));
						reportVO.setTxType(rs.getString("type"));
						reportVO.setGroupName(rs.getString("group_name"));
						reportVO.setGroupID(rs.getInt("groupID"));
						reportVO.setJanCr(rs.getBigDecimal("JanNeg"));
						reportVO.setFebCr(rs.getBigDecimal("FebNeg"));
						reportVO.setMarCr(rs.getBigDecimal("MarNeg"));
						reportVO.setAprCr(rs.getBigDecimal("AprNeg"));
						reportVO.setMayCr(rs.getBigDecimal("MayNeg"));
						reportVO.setJunCr(rs.getBigDecimal("JunNeg"));
						reportVO.setJulCr(rs.getBigDecimal("JulNeg"));
						reportVO.setAugCr(rs.getBigDecimal("AugNeg"));
						reportVO.setSeptCr(rs.getBigDecimal("SepNeg"));
						reportVO.setOctCr(rs.getBigDecimal("OctNeg"));
						reportVO.setNovCr(rs.getBigDecimal("NovNeg"));
						reportVO.setDecCr(rs.getBigDecimal("DecNeg"));
						reportVO.setJanDb(rs.getBigDecimal("JanPos"));
						reportVO.setFebDb(rs.getBigDecimal("FebPos"));
						reportVO.setMarDb(rs.getBigDecimal("MarPos"));
						reportVO.setAprDb(rs.getBigDecimal("AprPos"));
						reportVO.setMayDb(rs.getBigDecimal("MayPos"));
						reportVO.setJunDb(rs.getBigDecimal("JunPos"));
						reportVO.setJulDb(rs.getBigDecimal("JulPos"));
						reportVO.setAugDb(rs.getBigDecimal("AugPos"));
						reportVO.setSeptDb(rs.getBigDecimal("SepPos"));
						reportVO.setOctDb(rs.getBigDecimal("OctPos"));
						reportVO.setNovDb(rs.getBigDecimal("NovPos"));
						reportVO.setDecDb(rs.getBigDecimal("DecPos"));
						return reportVO;
					}
				};
				groupNameList = namedParameterJdbcTemplate
						.query(sql, hMap, rMapper);
	 		    
	 		    log.debug("Exit : public List getMonthWiseCashBasedCostCenterReportForChart(int intSocietyID)");
	 		}
	 		catch(Exception ex)
	 		{
	 			//ex.printStackTrace();
	 			log.error("Exception in getMonthWiseCashBasedCostCenterReportForChart : "+ex);
	 		}
	 	
	 		return groupNameList;
	 		
	 	}
	  
	  
	  public List getQuaterWiseCashBasedCostCenterReportForChart(int societyID,String fromDate,String uptoDate,String type,String projectName)
	 	{   
	 	   List groupNameList=new ArrayList();
	 		try
	 		{
	 			log.debug("Entry : public List getQuaterWiseCashBasedCostCenterReportForChart(int intSocietyID)");
	 			String strWhereClause = "";
	 			String strGroupClause = "";
	 			String strColumn="";
	 			String strTxTypeClause="";
	 			String andCondition="";
	 			String orCondition="";
	 			
	 		String orgCondition=" AND l.org_id=:orgID ";
	 		String orgClause=" And al.org_id=:orgID ";
	 			
				if (type.equalsIgnoreCase("R")){//Root
					strWhereClause = " WHERE ( alg.sub_group_id!=4 AND alg.sub_group_id!=5 and alg.sub_group_id!=95) GROUP BY txTable.type ORDER BY alg.group_name ";
				    strGroupClause=" ats.tx_type ";
				    strColumn="";
	 		     }else if(type.equalsIgnoreCase("G")){//Account Group
					strWhereClause = " WHERE (alg.sub_group_id!=4 AND alg.sub_group_id!=5 and alg.sub_group_id!=95) GROUP BY  alg.sub_group_id,tx_type ORDER BY alg.group_name ";
					strGroupClause=" table_secondary.ledger_id, ats.tx_type ";
					strColumn=" SUM(Q1Neg) AS Q1Neg,SUM(Q1Pos) AS Q1Pos,SUM(Q2Neg) AS Q2Neg,SUM(Q2Pos) AS Q2Pos, SUM(Q3Neg) AS Q3Neg,SUM(Q3Pos) AS Q3Pos,SUM(Q4Neg) AS Q4Neg,SUM(Q4Pos) AS Q4Pos,";
				}
				
				//Remove whitespace and split by comma 
		        List<String> result = Arrays.asList(projectName.split("\\s*,\\s*"));
		        if(result.size()==1){
		        	orCondition=orCondition+" pm.project_acronyms='"+projectName.trim()+"'";
		        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projectName.trim()+"%')OR(al.project_tags LIKE '%#"+projectName.trim()+"%'))";
		        	
		        }else{
		        for(int i=0;result.size()>i;i++){
		        	String projectAcronyms=result.get(i);
		        	if(i==0){
		        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronyms.trim()+"'";
		        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(al.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
		        	}else{
		        	orCondition=orCondition+" or pm.project_acronyms='"+projectAcronyms.trim()+"'";
		        	andCondition=andCondition+" and ((ale.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(al.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
		        	}}}
				
										    
	 		   
	 		   SocietyVO societyVO=societyService.getSocietyDetails(societyID);
         
				
              String sql="SELECT SUM(ABS(tx_negative)) AS txNegative,SUM(ABS(tx_positive)) AS txPositive,SUM(tx_sum) AS txSum,"+strColumn+" tabl.* fROM(SELECT "+
					     "CASE WHEN QUARTER(ats.tx_date) = '2' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS Q1Neg, "+
						 " CASE WHEN QUARTER(ats.tx_date) = '2' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS Q1Pos, "+												
						 " CASE WHEN QUARTER(ats.tx_date) = '3' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS Q2Neg, "+
						 " CASE WHEN QUARTER(ats.tx_date) = '3' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS Q2Pos, "+
						 " CASE WHEN QUARTER(ats.tx_date) = '4' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS Q3Neg, "+
						 " CASE WHEN QUARTER(ats.tx_date) = '4' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS Q3Pos,"+												
						 " CASE WHEN QUARTER(ats.tx_date) = '1' THEN ABS(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS Q4Neg,"+ 
						 " CASE WHEN QUARTER(ats.tx_date) = '1' THEN ABS(CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END)  ELSE '0.00' END AS Q4Pos, "+
						" root_id,g.id AS groupID,g.group_name,g.category_id,ale.type,ale.tx_id,ats.tx_type, al.id AS tx_ledger_id,al.ledger_name "+
						" ,(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END) AS tx_negative, (CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END) AS tx_positive, (SUM( ale.amount)) AS tx_sum FROM (SELECT a.* FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_ledgers_"+societyVO.getDataZoneID()+" l, account_groups g "+
					  " WHERE a.is_deleted=0 AND a.tx_id=al.tx_id AND a.org_id=l.org_id "+orgCondition+" AND a.tx_date BETWEEN :fromDate AND :toDate AND al.ledger_id=l.id AND l.sub_group_id=g.id and a.tx_type!='Contra' AND (g.category_id=3 OR g.category_id=101 OR g.category_id=64 OR g.category_id=65 )) AS ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups g , project_master_details pm WHERE ats.tx_date "+
		                 " BETWEEN :fromDate AND :toDate AND ats.is_deleted = 0 AND ats.org_id=pm.org_id AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND ats.org_id=ale.org_id AND ale.org_id=al.org_id "+orgClause+ 
		                 " AND al.sub_group_id = g.id AND ( "+orCondition+" ) AND ( "+andCondition+"  ) AND  ats.tx_type!='Contra' GROUP BY ale.id) AS tabl WHERE tabl.category_id!=3 AND tabl.category_id!=101 AND tabl.category_id!=64 AND tabl.category_id!=65 GROUP BY tabl.groupID,TYPE ORDER BY group_name;";
              
				log.debug("sql "+sql);
				Map hMap = new HashMap();
				hMap.put("orgID", societyID);
				hMap.put("fromDate", fromDate);
				hMap.put("toDate",uptoDate);

				RowMapper rMapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int num) throws SQLException {
						// TODO Auto-generated method stub
						MonthwiseChartVO reportVO=new MonthwiseChartVO();
						reportVO.setTransactionType(rs.getString("tx_type"));
						reportVO.setRootGroupID(rs.getInt("root_id"));
						reportVO.setTxType(rs.getString("type"));
						reportVO.setGroupName(rs.getString("group_name"));
						reportVO.setGroupID(rs.getInt("groupID"));
						reportVO.setQ1Cr(rs.getBigDecimal("Q1Neg"));
						reportVO.setQ2Cr(rs.getBigDecimal("Q2Neg"));
						reportVO.setQ3Cr(rs.getBigDecimal("Q3Neg"));
						reportVO.setQ4Cr(rs.getBigDecimal("Q4Neg"));
						reportVO.setQ1Db(rs.getBigDecimal("Q1Pos"));
						reportVO.setQ2Db(rs.getBigDecimal("Q2Pos"));
						reportVO.setQ3Db(rs.getBigDecimal("Q3Pos"));
						reportVO.setQ4Db(rs.getBigDecimal("Q4Pos"));
						return reportVO;
					}
				};
				groupNameList = namedParameterJdbcTemplate
						.query(sql, hMap, rMapper);
	 		    
	 		    log.debug("Exit : public List getQuaterWiseCashBasedCostCenterReportForChart(int intSocietyID)");
	 		}
	 		catch(Exception ex)
	 		{
	 			//ex.printStackTrace();
	 			log.error("Exception in getQuaterWiseCashBasedCostCenterReportForChart : "+ex);
	 		}
	 	
	 		return groupNameList;
	 		
	 	}
	  
	  
	  public List getCashBasedCostCenterCategoryReport(String fromDate, String uptoDate,
				int societyId, int isConsolidated,List categoryList) {
			List incExpList = new ArrayList();
			try {
				log
						.debug("Entry: public List getCashBasedCostCenterReport(String fromDate, String uptoDate,int societyId, String txType,int isConsolidated) "+ fromDate + uptoDate);
				String orgCondition="";
				String orgClause="";
				String strTxTypeClause="";
				String orCondition="";
				String andCondition="";
				if(isConsolidated==0){
					orgCondition=" AND l.org_id=:orgID ";
					orgClause=" And a.org_id=:orgID ";
				}
				
				for(int i=0;categoryList.size()>i;i++){
					ProjectDetailsVO projVO=(ProjectDetailsVO) categoryList.get(i);
					  if(categoryList.size()==1){
				        	orCondition=orCondition+" pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	
				        }else{
				       
				        	if(i==0){
				        	orCondition=orCondition+" pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	}else{
				        	orCondition=orCondition+" or pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" or ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	}}
					
				}
				
				SocietyVO societyVO=societyService.getSocietyDetails(societyId);
			
				
				String sqlQuery="SELECT SUM(ABS(tx_negative)) AS txNegative,SUM(ABS(tx_positive)) AS txPositive,SUM(tx_sum) AS txSum,projects.* FROM "+
						   " (SELECT h.* FROM (SELECT al.id,a.org_id,pm.project_id,pm.project_name,pm.project_acronyms,l.id AS ledgerID,l.ledger_name AS ledgerName,(al.amount) AS txAmt, ((al.amount)) AS actualAmount ,ac.id AS groupID ,ac.root_id ,pm.project_cost, ac.seq_no,ac.group_name,ac.category_id,al.type,al.tx_id,a.tx_type, al.id AS tx_ledger_id,l.ledger_name "+
						   " ,(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END) AS tx_negative, (CASE WHEN al.amount>0 THEN al.amount ELSE 0 END) AS tx_positive, (SUM( al.amount)) AS tx_sum  FROM account_ledgers_"+societyVO.getDataZoneID()+" l,(SELECT a.* FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_ledgers_"+societyVO.getDataZoneID()+" l ,account_groups g  WHERE a.is_deleted=0 AND a.tx_id=al.tx_id "+
						   " AND a.org_id=l.org_id  "+orgCondition+"  AND a.tx_date BETWEEN :fromDate AND :toDate AND al.ledger_id=l.id AND l.sub_group_id=g.id AND a.tx_type!='Contra' AND (g.category_id=3 OR g.category_id=101 OR g.category_id=64 OR g.category_id=65)) AS a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_groups ac,account_category ag ,project_master_details pm WHERE a.tx_id=al.tx_id "+orgClause+" AND a.org_id=pm.org_id AND l.sub_group_id=ac.id AND ac.category_id=ag.category_id AND a.is_deleted=0 AND al.ledger_id=l.id  AND ( a.tx_date >= :fromDate AND a.tx_date <= :toDate ) "+
						   " AND ( "+orCondition+" ) AND ( "+andCondition+" ) GROUP BY al.id) AS h GROUP BY h.id) AS projects where  projects.category_id!=3 AND projects.category_id!=101 AND projects.category_id!=64 AND projects.category_id!=65 GROUP BY projects.groupID,TYPE ORDER BY group_name;;";
				
				log.debug("query : " + sqlQuery);

				Map hashMap = new HashMap();
				hashMap.put("fromDate", fromDate);
				hashMap.put("toDate", uptoDate);
				hashMap.put("orgID", societyId);

				RowMapper rMapper = new RowMapper() {
					String tempGroupName = "";

					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						// TODO Auto-generated method stub
						ReportDetailsVO rptVO = new ReportDetailsVO();
						rptVO.setLedgerName(rs.getString("ledger_name"));
						rptVO.setLedgerID(rs.getInt("tx_ledger_id"));
						rptVO.setGroupName(rs.getString("group_name"));
						rptVO.setTxType(rs.getString("type"));
						rptVO.setCreditBalance(rs.getBigDecimal("txNegative").abs().setScale(2, RoundingMode.HALF_UP));
						rptVO.setDebitBalance(rs.getBigDecimal("txPositive").abs().setScale(2, RoundingMode.HALF_UP));
						rptVO.setGroupID(rs.getInt("groupID"));
						rptVO.setRootID(rs.getInt("root_id"));
						
						
					

						return rptVO;
					}
				};
				incExpList = namedParameterJdbcTemplate
						.query(sqlQuery, hashMap, rMapper);
				//	logger.info("###############################"+incExpList.size()+incExpList);

			} catch (BadSqlGrammarException ex) {
				log.info("Exception in getCashBasedCostCenterReport: " + ex);
			} catch (Exception ex) {
				log.error("Exception in getCashBasedCostCenterReport : "
						+ ex);
			}
			log
					.debug("Exit:public List getCashBasedCostCenterReport(String fromDate, String uptoDate,int societyId, String txType,int isConsolidated)"
							+ incExpList.size());
			return incExpList;

		}
	 
	 public List getCashBasedCostCenterCategoryDetailsReport(String fromDate, String uptoDate,
				int societyId,int isConsolidated,List categoryList) {
			List incExpList = new ArrayList();
			try {
				log
						.debug("Entry: public List getCashBasedReceiptPaymentDetailsReport(String fromDate, String uptoDate,int societyId, String txType,int isConsolidated)"
								+ fromDate + uptoDate);
				String strTxTypeClause="";
				String orgCondition="";
				String orgClause="";
				String andCondition="";
				String orCondition="";
				if(isConsolidated==0){
					orgCondition=" AND l.org_id=:orgID ";
					orgClause=" And a.org_id=:orgID ";
				}
				 
				for(int i=0;categoryList.size()>i;i++){
					ProjectDetailsVO projVO=(ProjectDetailsVO) categoryList.get(i);
					  if(categoryList.size()==1){
				        	orCondition=orCondition+" pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	
				        }else{
				       
				        	if(i==0){
				        	orCondition=orCondition+" pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	}else{
				        	orCondition=orCondition+" or pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" or ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	}}
					
				}
			
				SocietyVO societyVO=societyService.getSocietyDetails(societyId);
			
				String sqlQuery="SELECT SUM(ABS(tx_negative)) AS txNegative,SUM(ABS(tx_positive)) AS txPositive,SUM(tx_sum) AS txSum,projects.* FROM "+
						   " (SELECT h.* FROM (SELECT al.id,a.org_id,pm.project_id,pm.project_name,pm.project_acronyms,l.id AS ledgerID,l.ledger_name AS ledgerName,(al.amount) AS txAmt, ((al.amount)) AS actualAmount ,ac.id AS groupID ,ac.root_id ,pm.project_cost, ac.seq_no,ac.group_name,ac.category_id,al.type,al.tx_id,a.tx_type, al.id AS tx_ledger_id,l.ledger_name "+
						   " ,(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END) AS tx_negative, (CASE WHEN al.amount>0 THEN al.amount ELSE 0 END) AS tx_positive, (SUM( al.amount)) AS tx_sum  FROM account_ledgers_"+societyVO.getDataZoneID()+" l,(SELECT a.* FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_ledgers_"+societyVO.getDataZoneID()+" l ,account_groups g  WHERE a.is_deleted=0 AND a.tx_id=al.tx_id "+
						   " AND a.org_id=l.org_id  "+orgCondition+"  AND a.tx_date BETWEEN :fromDate AND :toDate AND al.ledger_id=l.id AND l.sub_group_id=g.id AND a.tx_type!='Contra' AND (g.category_id=3 OR g.category_id=101 OR g.category_id=64 OR g.category_id=65 )) AS a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_groups ac,account_category ag ,project_master_details pm WHERE a.tx_id=al.tx_id "+orgClause+" AND a.org_id=pm.org_id AND l.sub_group_id=ac.id AND ac.category_id=ag.category_id AND a.is_deleted=0 AND al.ledger_id=l.id  AND ( a.tx_date >= :fromDate AND a.tx_date <= :toDate ) "+
						   " AND ( "+orCondition+" ) AND ( "+andCondition+" ) GROUP BY al.id) AS h GROUP BY h.id) AS projects where projects.category_id!=3 AND projects.category_id!=101 AND projects.category_id!=64 AND projects.category_id!=65 GROUP BY projects.tx_ledger_id,TYPE ORDER BY group_name;;";
			log.debug("query : " + sqlQuery);

				Map hashMap = new HashMap();
				hashMap.put("fromDate", fromDate);
				hashMap.put("toDate", uptoDate);
				hashMap.put("orgID",societyId);

				RowMapper rMapper = new RowMapper() {
					String tempGroupName = "";

					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						// TODO Auto-generated method stub
						ReportDetailsVO rptVO = new ReportDetailsVO();
						rptVO.setLedgerName(rs.getString("ledger_name"));
						rptVO.setLedgerID(rs.getInt("tx_ledger_id"));
						rptVO.setGroupName(rs.getString("group_name"));
						rptVO.setTxType(rs.getString("type"));
						rptVO.setCreditBalance(rs.getBigDecimal("txNegative").abs().setScale(2, RoundingMode.HALF_UP));
						rptVO.setDebitBalance(rs.getBigDecimal("txPositive").abs().setScale(2, RoundingMode.HALF_UP));
						rptVO.setGroupID(rs.getInt("groupID"));
						rptVO.setRootID(rs.getInt("root_id"));
						
										
						return rptVO;
					}
				};
				incExpList = namedParameterJdbcTemplate
						.query(sqlQuery, hashMap, rMapper);
				//	logger.info("###############################"+incExpList.size()+incExpList);

			} catch (BadSqlGrammarException ex) {
				log.info("Exception in getCashBasedReceiptPaymentDetailsReport: " + ex);
			} catch (Exception ex) {
				log.error("Exception in getCashBasedReceiptPaymentDetailsReport : "
						+ ex);
			}
			log
					.debug("Exit: public List getCashBasedReceiptPaymentDetailsReport(String fromDate, String uptoDate,	int societyId, String txType,int isConsolidated)"
							+ incExpList.size());
			return incExpList;

		}
	 
	 public List getMonthWiseCashBasedCostCenterCategoryReportForChart(int societyID,String fromDate,String uptoDate,String type, int isConsolidated,List categoryList)
	 	{   
	 	   List groupNameList=new ArrayList();
	 		try
	 		{
	 			log.debug("Entry : public List getMonthWiseCashBasedCostCenterReportForChart(int intSocietyID)");
	 			String strWhereClause = "";
	 			String strGroupClause = "";
	 			String strColumn="";
	 			String strTxTypeClause=" ";
	 			
	 			String orgCondition="";
				String orgClause="";
				String andCondition="";
				String orCondition="";
				if(isConsolidated==0){
					orgCondition=" AND l.org_id=:orgID ";
					orgClause=" And a.org_id=:orgID ";
				}
	 			
				if (type.equalsIgnoreCase("R")){//Root
					strWhereClause = "  WHERE ( alg.sub_group_id!=4 and alg.sub_group_id!=5 and alg.sub_group_id!=95 ) GROUP BY txTable.type ORDER BY alg.group_name ";
				    strGroupClause=" table_secondary.type, ats.tx_type ";
				    strColumn="";
	 		     }else if(type.equalsIgnoreCase("G")){//Account Group
					
					strGroupClause=" table_secondary.ledger_id, ats.tx_type ";
					strColumn=" SUM(AprNeg) AS AprNeg,SUM(AprPos) AS AprPos,SUM(MayNeg) AS MayNeg,SUM(MayPos) AS MayPos,SUM(JunNeg) AS JunNeg,SUM(JunPos) AS JunPos,SUM(JulNeg) AS JulNeg,SUM(JulPos) AS JulPos,SUM(AugNeg) AS AugNeg,SUM(AugPos) AS AugPos,"+
                          " SUM(SepNeg) AS SepNeg,SUM(SepPos) AS SepPos, "+
                         " SUM(OctNeg) AS OctNeg,SUM(OctPos) AS OctPos,SUM(NovNeg) AS NovNeg,SUM(NovPos) AS NovPos,SUM(DecNeg) AS DecNeg,SUM(DecPos) AS DecPos,SUM(JanNeg) AS JanNeg,SUM(JanPos) AS JanPos,SUM(FebNeg) AS FebNeg,SUM(FebPos) AS FebPos,"+
                          " SUM(MarNeg) AS MarNeg,SUM(MarPos) AS MarPos,";
				}
				
				for(int i=0;categoryList.size()>i;i++){
					ProjectDetailsVO projVO=(ProjectDetailsVO) categoryList.get(i);
					  if(categoryList.size()==1){
				        	orCondition=orCondition+" pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	
				        }else{
				       
				        	if(i==0){
				        	orCondition=orCondition+" pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	}else{
				        	orCondition=orCondition+" or pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" or ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	}}
					
				}
				
				
				 SocietyVO societyVO=societyService.getSocietyDetails(societyID);
								
					
				
			
				String sql="SELECT SUM(ABS(tx_negative)) AS txNegative,SUM(ABS(tx_positive)) AS txPositive,SUM(tx_sum) AS txSum,"+strColumn+" projects.* FROM "+
						   " (SELECT h.* FROM (SELECT "+
					     "CASE WHEN MONTHNAME(a.tx_date) = 'April' THEN ABS(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS AprNeg, "+
						 " CASE WHEN MONTHNAME(a.tx_date) = 'April' THEN ABS(CASE WHEN al.amount>0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS AprPos, "+												
						 " CASE WHEN MONTHNAME(a.tx_date) = 'May' THEN ABS(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS MayNeg, "+
						 " CASE WHEN MONTHNAME(a.tx_date) = 'May' THEN ABS(CASE WHEN al.amount>0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS MayPos, "+
						 " CASE WHEN MONTHNAME(a.tx_date) = 'June' THEN ABS(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS JunNeg, "+
						 " CASE WHEN MONTHNAME(a.tx_date) = 'June' THEN ABS(CASE WHEN al.amount>0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS JunPos,"+												
						 " CASE WHEN MONTHNAME(a.tx_date) = 'July' THEN ABS(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS JulNeg,"+ 
						 " CASE WHEN MONTHNAME(a.tx_date) = 'July' THEN ABS(CASE WHEN al.amount>0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS JulPos, "+
						 " CASE WHEN MONTHNAME(a.tx_date) = 'August' THEN ABS(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS AugNeg,"+ 
						 " CASE WHEN MONTHNAME(a.tx_date) = 'August' THEN ABS(CASE WHEN al.amount>0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS AugPos, "+												
						 " CASE WHEN MONTHNAME(a.tx_date) = 'September' THEN ABS(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS SepNeg, "+ 
						 " CASE WHEN MONTHNAME(a.tx_date) = 'September' THEN ABS(CASE WHEN al.amount>0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS SepPos, "+
						 " CASE WHEN MONTHNAME(a.tx_date) = 'October' THEN ABS(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS OctNeg, "+ 
						 " CASE WHEN MONTHNAME(a.tx_date) = 'October' THEN ABS(CASE WHEN al.amount>0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS OctPos, "+													
						 " CASE WHEN MONTHNAME(a.tx_date) = 'November' THEN ABS(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS NovNeg, "+ 
						 " CASE WHEN MONTHNAME(a.tx_date) = 'November' THEN ABS(CASE WHEN al.amount>0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS NovPos, "+
						 " CASE WHEN MONTHNAME(a.tx_date) = 'December' THEN ABS(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS DecNeg, "+
						 " CASE WHEN MONTHNAME(a.tx_date) = 'December' THEN ABS(CASE WHEN al.amount>0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS DecPos, "+												
						 " CASE WHEN MONTHNAME(a.tx_date) = 'January' THEN ABS(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS JanNeg, "+ 
						 " CASE WHEN MONTHNAME(a.tx_date) = 'January' THEN ABS(CASE WHEN al.amount>0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS JanPos, "+
						 " CASE WHEN MONTHNAME(a.tx_date) = 'February' THEN ABS(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS FebNeg, "+ 
						 " CASE WHEN MONTHNAME(a.tx_date) = 'February' THEN ABS(CASE WHEN al.amount>0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS FebPos, "+												
						 " CASE WHEN MONTHNAME(a.tx_date) = 'March' THEN ABS(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS MarNeg, "+ 
						 " CASE WHEN MONTHNAME(a.tx_date) = 'March' THEN ABS(CASE WHEN al.amount>0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS MarPos, al.id,a.org_id,pm.project_id,pm.project_name,pm.project_acronyms,l.id AS ledgerID,l.ledger_name AS ledgerName,(al.amount) AS txAmt, ((al.amount)) AS actualAmount ,ac.id AS groupID ,ac.root_id ,pm.project_cost, ac.seq_no,ac.group_name,ac.category_id,al.type,al.tx_id,a.tx_type, al.id AS tx_ledger_id,l.ledger_name "+
						   " ,(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END) AS tx_negative, (CASE WHEN al.amount>0 THEN al.amount ELSE 0 END) AS tx_positive, (SUM( al.amount)) AS tx_sum  FROM account_ledgers_"+societyVO.getDataZoneID()+" l,(SELECT a.* FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_ledgers_"+societyVO.getDataZoneID()+" l ,account_groups g  WHERE a.is_deleted=0 AND a.tx_id=al.tx_id "+
						   " AND a.org_id=l.org_id  "+orgCondition+"  AND a.tx_date BETWEEN :fromDate AND :toDate AND al.ledger_id=l.id AND l.sub_group_id=g.id AND a.tx_type!='Contra' AND (g.category_id=3 OR g.category_id=101 OR g.category_id=64 OR g.category_id=65)) AS a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_groups ac,account_category ag ,project_master_details pm WHERE a.tx_id=al.tx_id "+orgClause+" AND a.org_id=pm.org_id AND l.sub_group_id=ac.id AND ac.category_id=ag.category_id AND a.is_deleted=0 AND al.ledger_id=l.id  AND ( a.tx_date >= :fromDate AND a.tx_date <= :toDate ) "+
						   " AND ( "+orCondition+" ) AND ( "+andCondition+" ) GROUP BY al.id) AS h GROUP BY h.id) AS projects where projects.category_id!=3 AND projects.category_id!=101 AND projects.category_id!=64 AND projects.category_id!=65 GROUP BY projects.groupID,TYPE ORDER BY group_name;;";
				
				
				log.debug("Query: "+sql);
				Map hMap = new HashMap();
				hMap.put("orgID", societyID);
				hMap.put("fromDate", fromDate);
				hMap.put("toDate",uptoDate);

				RowMapper rMapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int num) throws SQLException {
						// TODO Auto-generated method stub
						MonthwiseChartVO reportVO=new MonthwiseChartVO();
						reportVO.setTransactionType(rs.getString("tx_type"));
						reportVO.setRootGroupID(rs.getInt("root_id"));
						reportVO.setTxType(rs.getString("type"));
						reportVO.setGroupName(rs.getString("group_name"));
						reportVO.setGroupID(rs.getInt("groupID"));
						reportVO.setJanCr(rs.getBigDecimal("JanNeg"));
						reportVO.setFebCr(rs.getBigDecimal("FebNeg"));
						reportVO.setMarCr(rs.getBigDecimal("MarNeg"));
						reportVO.setAprCr(rs.getBigDecimal("AprNeg"));
						reportVO.setMayCr(rs.getBigDecimal("MayNeg"));
						reportVO.setJunCr(rs.getBigDecimal("JunNeg"));
						reportVO.setJulCr(rs.getBigDecimal("JulNeg"));
						reportVO.setAugCr(rs.getBigDecimal("AugNeg"));
						reportVO.setSeptCr(rs.getBigDecimal("SepNeg"));
						reportVO.setOctCr(rs.getBigDecimal("OctNeg"));
						reportVO.setNovCr(rs.getBigDecimal("NovNeg"));
						reportVO.setDecCr(rs.getBigDecimal("DecNeg"));
						reportVO.setJanDb(rs.getBigDecimal("JanPos"));
						reportVO.setFebDb(rs.getBigDecimal("FebPos"));
						reportVO.setMarDb(rs.getBigDecimal("MarPos"));
						reportVO.setAprDb(rs.getBigDecimal("AprPos"));
						reportVO.setMayDb(rs.getBigDecimal("MayPos"));
						reportVO.setJunDb(rs.getBigDecimal("JunPos"));
						reportVO.setJulDb(rs.getBigDecimal("JulPos"));
						reportVO.setAugDb(rs.getBigDecimal("AugPos"));
						reportVO.setSeptDb(rs.getBigDecimal("SepPos"));
						reportVO.setOctDb(rs.getBigDecimal("OctPos"));
						reportVO.setNovDb(rs.getBigDecimal("NovPos"));
						reportVO.setDecDb(rs.getBigDecimal("DecPos"));
						return reportVO;
					}
				};
				groupNameList = namedParameterJdbcTemplate
						.query(sql, hMap, rMapper);
	 		    
	 		    log.debug("Exit : public List getMonthWiseCashBasedCostCenterReportForChart(int intSocietyID)");
	 		}
	 		catch(Exception ex)
	 		{
	 			//ex.printStackTrace();
	 			log.error("Exception in getMonthWiseCashBasedCostCenterReportForChart : "+ex);
	 		}
	 	
	 		return groupNameList;
	 		
	 	}
	  
	  
	  public List getQuaterWiseCashBasedCostCenterCategoryReportForChart(int societyID,String fromDate,String uptoDate,String type,List categoryList)
	 	{   
	 	   List groupNameList=new ArrayList();
	 		try
	 		{
	 			log.debug("Entry : public List getQuaterWiseCashBasedCostCenterReportForChart(int intSocietyID)");
	 			String strWhereClause = "";
	 			String strGroupClause = "";
	 			String strColumn="";
	 			String strTxTypeClause="";
	 			String andCondition="";
	 			String orCondition="";
	 			
	 		String orgCondition=" AND l.org_id=:orgID ";
	 		String orgClause=" And al.org_id=:orgID ";
	 			
				if (type.equalsIgnoreCase("R")){//Root
					strWhereClause = " WHERE ( alg.sub_group_id!=4 AND alg.sub_group_id!=5 and alg.sub_group_id!=95) GROUP BY txTable.type ORDER BY alg.group_name ";
				    strGroupClause=" ats.tx_type ";
				    strColumn="";
	 		     }else if(type.equalsIgnoreCase("G")){//Account Group
					strWhereClause = " WHERE (alg.sub_group_id!=4 AND alg.sub_group_id!=5 and alg.sub_group_id!=95) GROUP BY  alg.sub_group_id,tx_type ORDER BY alg.group_name ";
					strGroupClause=" table_secondary.ledger_id, ats.tx_type ";
					strColumn=" SUM(Q1Neg) AS Q1Neg,SUM(Q1Pos) AS Q1Pos,SUM(Q2Neg) AS Q2Neg,SUM(Q2Pos) AS Q2Pos, SUM(Q3Neg) AS Q3Neg,SUM(Q3Pos) AS Q3Pos,SUM(Q4Neg) AS Q4Neg,SUM(Q4Pos) AS Q4Pos,";
				}
				
				for(int i=0;categoryList.size()>i;i++){
					ProjectDetailsVO projVO=(ProjectDetailsVO) categoryList.get(i);
					  if(categoryList.size()==1){
				        	orCondition=orCondition+" pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	
				        }else{
				       
				        	if(i==0){
				        	orCondition=orCondition+" pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	}else{
				        	orCondition=orCondition+" or pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" or ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	}}
					
				}
				
										    
	 		   
	 		   SocietyVO societyVO=societyService.getSocietyDetails(societyID);
       
			     
        	String sql="SELECT SUM(ABS(tx_negative)) AS txNegative,SUM(ABS(tx_positive)) AS txPositive,SUM(tx_sum) AS txSum,"+strColumn+" projects.* FROM "+
					   " (SELECT h.* FROM (SELECT "+
					   "CASE WHEN QUARTER(a.tx_date) = '2' THEN ABS(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS Q1Neg, "+
						 " CASE WHEN QUARTER(a.tx_date) = '2' THEN ABS(CASE WHEN al.amount>0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS Q1Pos, "+												
						 " CASE WHEN QUARTER(a.tx_date) = '3' THEN ABS(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS Q2Neg, "+
						 " CASE WHEN QUARTER(a.tx_date) = '3' THEN ABS(CASE WHEN al.amount>0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS Q2Pos, "+
						 " CASE WHEN QUARTER(a.tx_date) = '4' THEN ABS(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS Q3Neg, "+
						 " CASE WHEN QUARTER(a.tx_date) = '4' THEN ABS(CASE WHEN al.amount>0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS Q3Pos,"+												
						 " CASE WHEN QUARTER(a.tx_date) = '1' THEN ABS(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS Q4Neg,"+ 
						 " CASE WHEN QUARTER(a.tx_date) = '1' THEN ABS(CASE WHEN al.amount>0 THEN al.amount ELSE 0 END)  ELSE '0.00' END AS Q4Pos, al.id,a.org_id,pm.project_id,pm.project_name,pm.project_acronyms,l.id AS ledgerID,l.ledger_name AS ledgerName,(al.amount) AS txAmt, ((al.amount)) AS actualAmount ,ac.id AS groupID ,ac.root_id ,pm.project_cost, ac.seq_no,ac.group_name,ac.category_id,al.type,al.tx_id,a.tx_type, al.id AS tx_ledger_id,l.ledger_name "+
					   " ,(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END) AS tx_negative, (CASE WHEN al.amount>0 THEN al.amount ELSE 0 END) AS tx_positive, (SUM( al.amount)) AS tx_sum  FROM account_ledgers_"+societyVO.getDataZoneID()+" l,(SELECT a.* FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_ledgers_"+societyVO.getDataZoneID()+" l ,account_groups g  WHERE a.is_deleted=0 AND a.tx_id=al.tx_id "+
					   " AND a.org_id=l.org_id  "+orgCondition+"  AND a.tx_date BETWEEN :fromDate AND :toDate AND al.ledger_id=l.id AND l.sub_group_id=g.id AND a.tx_type!='Contra' AND (g.category_id=3 OR g.category_id=101 OR g.category_id=64 OR g.category_id=65)) AS a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_groups ac,account_category ag ,project_master_details pm WHERE a.tx_id=al.tx_id "+orgClause+" AND a.org_id=pm.org_id AND l.sub_group_id=ac.id AND ac.category_id=ag.category_id AND a.is_deleted=0 AND al.ledger_id=l.id  AND ( a.tx_date >= :fromDate AND a.tx_date <= :toDate ) "+
					   " AND ( "+orCondition+" ) AND ( "+andCondition+" ) GROUP BY al.id) AS h GROUP BY h.id) AS projects where projects.category_id!=3 AND projects.category_id!=101 AND projects.category_id!=64 AND projects.category_id!=65 GROUP BY projects.groupID,TYPE ORDER BY group_name;;";
			
            
            
				log.debug("sql "+sql);
				Map hMap = new HashMap();
				hMap.put("orgID", societyID);
				hMap.put("fromDate", fromDate);
				hMap.put("toDate",uptoDate);

				RowMapper rMapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int num) throws SQLException {
						// TODO Auto-generated method stub
						MonthwiseChartVO reportVO=new MonthwiseChartVO();
						reportVO.setTransactionType(rs.getString("tx_type"));
						reportVO.setRootGroupID(rs.getInt("root_id"));
						reportVO.setTxType(rs.getString("type"));
						reportVO.setGroupName(rs.getString("group_name"));
						reportVO.setGroupID(rs.getInt("groupID"));
						reportVO.setQ1Cr(rs.getBigDecimal("Q1Neg"));
						reportVO.setQ2Cr(rs.getBigDecimal("Q2Neg"));
						reportVO.setQ3Cr(rs.getBigDecimal("Q3Neg"));
						reportVO.setQ4Cr(rs.getBigDecimal("Q4Neg"));
						reportVO.setQ1Db(rs.getBigDecimal("Q1Pos"));
						reportVO.setQ2Db(rs.getBigDecimal("Q2Pos"));
						reportVO.setQ3Db(rs.getBigDecimal("Q3Pos"));
						reportVO.setQ4Db(rs.getBigDecimal("Q4Pos"));
						return reportVO;
					}
				};
				groupNameList = namedParameterJdbcTemplate
						.query(sql, hMap, rMapper);
	 		    
	 		    log.debug("Exit : public List getQuaterWiseCashBasedCostCenterReportForChart(int intSocietyID)");
	 		}
	 		catch(Exception ex)
	 		{
	 			//ex.printStackTrace();
	 			log.error("Exception in getQuaterWiseCashBasedCostCenterReportForChart : "+ex);
	 		}
	 	
	 		return groupNameList;
	 		
	 	}
	  
	  public List getCashBasedCostCenterLedgerReport(final int societyID, final String fromDate,
				String uptoDate, int groupID,String txType,int isConsolidated,String projectName) {

			List<AccountHeadVO> ledgerList = new ArrayList<AccountHeadVO>();

			try {
				String strCondition="";
				String orCondition="";
				String andCondition="";
				if(txType.equalsIgnoreCase("D")){
					strCondition=" and ale.type='D' ";
				}else if(txType.equalsIgnoreCase("C")){
					strCondition="  AND ale.type='C' ";
				}else if(txType.equalsIgnoreCase("Receipt")){
					strCondition=" AND ats.tx_type='Receipt' ";
				}else if(txType.equalsIgnoreCase("Payment")){
					strCondition=" AND ats.tx_type='Payment' ";
				}else strCondition="";
				
				String orgCondition="";
				String orgClause="";
				if(isConsolidated==0){
					orgCondition=" AND al.org_id=:orgID ";
					orgClause=" And l.org_id=:orgID ";
				}
				
				//Remove whitespace and split by comma 
		        List<String> result = Arrays.asList(projectName.split("\\s*,\\s*"));
		        if(result.size()==1){
		        	orCondition=orCondition+" pm.project_acronyms='"+projectName.trim()+"'";
		        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projectName.trim()+"%')OR(al.project_tags LIKE '%#"+projectName.trim()+"%'))";
		        	
		        }else{
		        for(int i=0;result.size()>i;i++){
		        	String projectAcronyms=result.get(i);
		        	if(i==0){
		        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronyms.trim()+"'";
		        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(al.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
		        	}else{
		        	orCondition=orCondition+" or pm.project_acronyms='"+projectAcronyms.trim()+"'";
		        	andCondition=andCondition+" and ((ale.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(al.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
		        	}}}

				log
						.debug("Entry : public List getLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String effectiveDate,int isConsolidated)"
								+ groupID);
				SocietyVO societyVO=societyService.getSocietyDetails(societyID);
										
				String sqlQuery="SELECT SUM(ABS(tx_negative)) AS txNegative,SUM(ABS(tx_positive)) AS txPositive,SUM(tx_sum) AS txSum,tabl.* FROM (SELECT root_id,g.id AS groupID,g.category_id,g.group_name,ale.type,ale.tx_id,ats.tx_type, al.id AS tx_ledger_id,al.ledger_name "+
						" ,(CASE WHEN ale.amount<0 THEN ale.amount ELSE 0 END) AS tx_negative, (CASE WHEN ale.amount>0 THEN ale.amount ELSE 0 END) AS tx_positive, (SUM( ale.amount)) AS tx_sum FROM  (SELECT a.* FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_ledgers_"+societyVO.getDataZoneID()+" l ,account_groups g  "+
					    " WHERE a.is_deleted=0 AND a.tx_id=al.tx_id AND a.org_id=l.org_id "+orgClause+" AND a.tx_date BETWEEN :fromDate AND :toDate AND al.ledger_id=l.id AND l.sub_group_id=g.id AND a.tx_type!='Contra' AND (g.category_id=3 OR g.category_id=101 OR g.category_id=64 OR g.category_id=65)) AS ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups g,project_master_details pm WHERE ats.tx_date "+
                        " BETWEEN :fromDate AND :toDate AND ats.is_deleted = 0 AND ats.org_id=pm.org_id AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND ats.org_id=ale.org_id AND ale.org_id=al.org_id "+orgCondition+strCondition+ 
                        " AND al.sub_group_id = g.id AND ( "+orCondition+" ) AND ( "+andCondition+"  ) AND  ats.tx_type!='Contra'  GROUP BY ale.id) AS tabl WHERE  tabl.category_id!=3 AND tabl.category_id!=101 AND tabl.category_id!=64 AND tabl.category_id!=65 AND tabl.groupID=:groupID  GROUP BY tabl.tx_ledger_id,TYPE ORDER BY group_name;";
				
				log.debug(sqlQuery);
				Map hMap = new HashMap();
				hMap.put("fromDate", fromDate);
				hMap.put("toDate", uptoDate);
				hMap.put("groupID", groupID);
				hMap.put("orgID",societyID);

				RowMapper Rmapper = new RowMapper() {
					String normalBalance = "";

					int rootID = 0;

					String groupName = "";

					int groupID = 0;

					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {

						AccountHeadVO acHVO = new AccountHeadVO();

					
						
						acHVO.setCrClsBal(rs.getBigDecimal("txNegative").abs().setScale(2, RoundingMode.HALF_UP));
						acHVO.setDrClsBal(rs.getBigDecimal("txPositive").abs().setScale(2, RoundingMode.HALF_UP));
						acHVO.setRootID(rs.getInt("root_id"));
						acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
						acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
						acHVO.setAccountGroupID(rs.getInt("groupID"));
						acHVO.setLedgerID(rs.getInt("tx_ledger_id"));
						acHVO.setAccType(rs.getString("type"));
					

						return acHVO;
					}

				};
				ledgerList = namedParameterJdbcTemplate.query(sqlQuery, hMap, Rmapper);

				log
						.debug("Exit : public List getLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String effectiveDate,int isConsolidated)"
								+ ledgerList.size());
			} catch (BadSqlGrammarException ex) {
				log.info("Exception in getLedgerReport : " + ex);
			} catch (Exception e) {
				log.error("Exception : " + e);
			}

			return ledgerList;
		}
	  
	  public List getCashBasedCostCenterCategoryLedgerReport(final int societyID, final String fromDate,
				String uptoDate, int groupID,String txType,int isConsolidated,List categoryList) {

			List<AccountHeadVO> ledgerList = new ArrayList<AccountHeadVO>();

			try {
				String strCondition="";
				String orCondition="";
				String andCondition="";
				if(txType.equalsIgnoreCase("D")){
					strCondition=" and al.type='D' ";
				}else if(txType.equalsIgnoreCase("C")){
					strCondition="  AND al.type='C' ";
				}else if(txType.equalsIgnoreCase("Receipt")){
					strCondition=" AND ats.tx_type='Receipt' ";
				}else if(txType.equalsIgnoreCase("Payment")){
					strCondition=" AND ats.tx_type='Payment' ";
				}else strCondition="";
				
				String orgCondition="";
				String orgClause="";
				String strTxTypeClause="";
				
				if(isConsolidated==0){
					orgCondition=" AND l.org_id=:orgID ";
					orgClause=" And a.org_id=:orgID ";
				}
				
				for(int i=0;categoryList.size()>i;i++){
					ProjectDetailsVO projVO=(ProjectDetailsVO) categoryList.get(i);
					  if(categoryList.size()==1){
				        	orCondition=orCondition+" pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	
				        }else{
				       
				        	if(i==0){
				        	orCondition=orCondition+" pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	}else{
				        	orCondition=orCondition+" or pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" or ((al.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	}}
					
				}
				
				SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			
				
				String sqlQuery="SELECT SUM(ABS(tx_negative)) AS txNegative,SUM(ABS(tx_positive)) AS txPositive,SUM(tx_sum) AS txSum,projects.* FROM "+
						   " (SELECT h.* FROM (SELECT al.id,a.org_id,pm.project_id,pm.project_name,pm.project_acronyms,l.id AS ledgerID,l.ledger_name AS ledgerName,(al.amount) AS txAmt, ((al.amount)) AS actualAmount ,ac.id AS groupID ,ac.root_id ,pm.project_cost, ac.seq_no,ac.group_name,ac.category_id,al.type,al.tx_id,a.tx_type, al.id AS tx_ledger_id,l.ledger_name "+
						   " ,(CASE WHEN al.amount<0 THEN al.amount ELSE 0 END) AS tx_negative, (CASE WHEN al.amount>0 THEN al.amount ELSE 0 END) AS tx_positive, (SUM( al.amount)) AS tx_sum  FROM account_ledgers_"+societyVO.getDataZoneID()+" l,(SELECT a.* FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_ledgers_"+societyVO.getDataZoneID()+" l ,account_groups g  WHERE a.is_deleted=0 AND a.tx_id=al.tx_id "+
						   " AND a.org_id=l.org_id  "+orgCondition+"  AND a.tx_date BETWEEN :fromDate AND :toDate AND al.ledger_id=l.id AND l.sub_group_id=g.id AND a.tx_type!='Contra' AND (g.category_id=3 OR g.category_id=101 OR g.category_id=64 OR g.category_id=65)) AS a,account_ledger_entry_"+societyVO.getDataZoneID()+" al,account_groups ac,account_category ag ,project_master_details pm WHERE a.tx_id=al.tx_id "+orgClause+strCondition+" AND a.org_id=pm.org_id AND l.sub_group_id=ac.id AND ac.category_id=ag.category_id AND a.is_deleted=0 AND al.ledger_id=l.id  AND ( a.tx_date >= :fromDate AND a.tx_date <= :toDate ) "+
						   " AND ( "+orCondition+" ) AND ( "+andCondition+" ) GROUP BY al.id) AS h GROUP BY h.id) AS projects where  projects.category_id!=3 AND projects.category_id!=101 AND projects.category_id!=64 AND projects.category_id!=65 and projects.groupID=:groupID GROUP BY projects.ledgerID,TYPE ORDER BY group_name;";
				
				
				log.debug(sqlQuery);
				Map hMap = new HashMap();
				hMap.put("fromDate", fromDate);
				hMap.put("toDate", uptoDate);
				hMap.put("groupID", groupID);
				hMap.put("orgID",societyID);

				RowMapper Rmapper = new RowMapper() {
					String normalBalance = "";

					int rootID = 0;

					String groupName = "";

					int groupID = 0;

					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {

						AccountHeadVO acHVO = new AccountHeadVO();

					
						
						acHVO.setCrClsBal(rs.getBigDecimal("txNegative").abs().setScale(2, RoundingMode.HALF_UP));
						acHVO.setDrClsBal(rs.getBigDecimal("txPositive").abs().setScale(2, RoundingMode.HALF_UP));
						acHVO.setRootID(rs.getInt("root_id"));
						acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
						acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
						acHVO.setAccountGroupID(rs.getInt("groupID"));
						acHVO.setLedgerID(rs.getInt("ledgerID"));
						acHVO.setAccType(rs.getString("type"));
					

						return acHVO;
					}

				};
				ledgerList = namedParameterJdbcTemplate.query(sqlQuery, hMap, Rmapper);

				log
						.debug("Exit : public List getLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String effectiveDate,int isConsolidated)"
								+ ledgerList.size());
			} catch (BadSqlGrammarException ex) {
				log.info("Exception in getLedgerReport : " + ex);
			} catch (Exception e) {
				log.error("Exception : " + e);
			}

			return ledgerList;
		}
	  
	  public List getMonthWiseBreakUpReportForReceivablesForCostCenters(int societyID,String fromDate,String uptoDate,String groupName,String projectName)
	 	{   
	 	   List groupNameList=new ArrayList();
	 		try
	 		{
	 			log.debug("Entry : public List getMonthWiseBreakUpReportForReceivablesForCostCenters(int intSocietyID)");
	 			String strWhereClause = null;
	 			String strConditions=null;
	 			String strClause=null;
	 			String andCondition="";
	 			String orCondition="";
	 			if(groupName.equalsIgnoreCase("G"))// Grouped
	 			{
	 				strWhereClause="c.category_id ";
	 				strConditions="";
	 				strClause="category_id ";
	 			}else if(groupName.equalsIgnoreCase("I"))// Individual ledger wise
	 			{
	 				strWhereClause="al.id ";
	 				strConditions=" chart.ledgerID=op_balance.id AND ";
	 				strClause="ledger.id ";
	 			}
	 			
	 			//Remove whitespace and split by comma 
		        List<String> result = Arrays.asList(projectName.split("\\s*,\\s*"));
		        if(result.size()==1){
		        	orCondition=orCondition+" pm.project_acronyms='"+projectName.trim()+"'";
		        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projectName.trim()+"%')OR(al.project_tags LIKE '%#"+projectName.trim()+"%'))";
		        	
		        }else{
		        for(int i=0;result.size()>i;i++){
		        	String projectAcronyms=result.get(i);
		        	if(i==0){
		        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronyms.trim()+"'";
		        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(al.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
		        	}else{
		        	orCondition=orCondition+" or pm.project_acronyms='"+projectAcronyms.trim()+"'";
		        	andCondition=andCondition+" and ((ale.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(al.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
		        	}}}
	 			
				SocietyVO societyVO=societyService.getSocietyDetails(societyID);
				String sqlDue = "SELECT op_balance.id,op_balance.ledger_name AS ledgerName,op_balance.sub_group_id,op_balance.category_id AS categoryID ,chart.ledgerID AS ID,chart.ledger_name AS ledger_ame,chart.groupID,chart.category_id,ifnull(Aprs,0) as Apr,ifnull(Mays,0) as May,ifnull(Juns,0) as Jun,ifnull(Juls,0) as Jul,ifnull(Augs,0) as Aug,ifnull(Septs,0) as Sept, "
						+ " ifnull(Octs,0) as Oct,ifnull(Novs,0) as Nov, ifnull(Decs,0) as Decss, ifnull(Jans,0) as Jan, ifnull(Febs,0) as Feb, ifnull(Mars,0) as Mar , chart.*,op_balance.opening_balance FROM(  SELECT al.id AS ledgerID,al.ledger_name, account_root_group.root_group_name,account_root_group.root_id, account_groups.group_name,account_groups.id AS groupID, c.category_id ,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'April' THEN ale.amount ELSE '0.00' END) AS Aprs, "+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'May' THEN ale.amount ELSE '0.00' END) AS Mays,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'June' THEN ale.amount ELSE '0.00' END) AS Juns,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'July' THEN ale.amount ELSE '0.00' END) AS Juls,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'August' THEN ale.amount ELSE '0.00' END) AS Augs,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'September' THEN ale.amount ELSE '0.00' END) AS Septs,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'October' THEN ale.amount ELSE '0.00' END) AS Octs,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'November' THEN ale.amount ELSE '0.00' END) AS Novs,"+
												"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'December' THEN ale.amount ELSE '0.00' END) AS Decs,"+
												"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'January' THEN ale.amount ELSE '0.00' END) AS Jans,"+
												"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'February' THEN ale.amount ELSE '0.00' END) AS Febs,"+
												"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'March' THEN ale.amount ELSE '0.00' END) AS Mars "+
												"  FROM account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al ,account_groups,account_root_group ,account_category c, project_master_details pm "+
												" WHERE ats.tx_date BETWEEN :fromDate AND :toDate AND ats.is_deleted=0  and ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID  "+
												" AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND al.sub_group_id = account_groups.id AND account_groups.root_id = account_root_group.root_id AND account_groups.category_id=c.category_id and c.category_id=1 and ("+orCondition+") AND ("+andCondition+" ) GROUP BY "+strWhereClause+"  ORDER BY al.id ) as chart left join "+
												 " (SELECT  ledger.id,ledger.ledger_name,SUM(ledger.opening_balance + iFNULL(s.closing_balance,0) ) AS opening_balance,ledger.sub_group_id,ledger.category_id FROM (SELECT al.id,ale.ledger_id,al.ledger_name,al.opening_balance ,sub_group_id, "+
												" IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount)) AS sumAmt, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount)) AS closing_balance, c.category_id "+
												" FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale ,account_groups g,account_category c,project_master_details pm  WHERE ats.org_id=pm.org_id and ats.org_id=ale.org_id AND ale.org_id=al.org_id AND al.org_id=:societyID AND ats.tx_id=ale.tx_id AND ats.tx_date < :fromDate AND al.sub_group_id=g.id AND g.category_id=c.category_id AND ats.is_deleted=0 AND ale.ledger_id = al.id and c.category_id=1 and ("+orCondition+") AND ("+andCondition+" ) "+ 
												" GROUP BY al.id ) s  left JOIN (SELECT a.*,g.category_id FROM account_ledgers_"+societyVO.getDataZoneID()+" a,account_groups g  WHERE a.sub_group_id=g.id AND g.category_id=1 AND a.org_id=:societyID AND a.is_deleted=0) ledger ON s.id=ledger.id GROUP BY "+strClause+" ) AS op_balance on "+strConditions+"  chart.category_id=op_balance.category_id  ;" ;
				
				log.debug("sql "+sqlDue);
				Map hMap = new HashMap();
				hMap.put("societyID", societyID);
				hMap.put("fromDate", fromDate);
				hMap.put("toDate",uptoDate);

				RowMapper rMapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int num) throws SQLException {
						// TODO Auto-generated method stub
						MonthwiseChartVO reportVO=new MonthwiseChartVO();
						if(rs.getString("ledgerName")!=null){
						reportVO.setLedgerName(rs.getString("ledgerName"));
						reportVO.setLedgerID(rs.getInt("id"));
						reportVO.setRootGroupName(rs.getString("root_group_name"));
						reportVO.setRootGroupID(rs.getInt("root_id"));
						reportVO.setGroupName(rs.getString("group_name"));
						reportVO.setGroupID(rs.getInt("sub_group_id"));
						}else{
							reportVO.setLedgerName(rs.getString("ledger_ame"));
							reportVO.setLedgerID(rs.getInt("ledgerID"));
							reportVO.setRootGroupName(rs.getString("root_group_name"));
							reportVO.setRootGroupID(rs.getInt("root_id"));
							reportVO.setGroupName(rs.getString("group_name"));
							reportVO.setGroupID(rs.getInt("groupID"));
							
						}
						reportVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
						reportVO.setJanAmt(rs.getBigDecimal("Jan").setScale(2, RoundingMode.HALF_UP));
						reportVO.setFebAmt(rs.getBigDecimal("Feb").setScale(2, RoundingMode.HALF_UP));
						reportVO.setMarAmt(rs.getBigDecimal("Mar").setScale(2, RoundingMode.HALF_UP));
						reportVO.setAprAmt(rs.getBigDecimal("Apr").setScale(2, RoundingMode.HALF_UP));
						reportVO.setMayAmt(rs.getBigDecimal("May").setScale(2, RoundingMode.HALF_UP));
						reportVO.setJunAmt(rs.getBigDecimal("Jun").setScale(2, RoundingMode.HALF_UP));
						reportVO.setJulAmt(rs.getBigDecimal("Jul").setScale(2, RoundingMode.HALF_UP));
						reportVO.setAugAmt(rs.getBigDecimal("Aug").setScale(2, RoundingMode.HALF_UP));
						reportVO.setSeptAmt(rs.getBigDecimal("Sept").setScale(2, RoundingMode.HALF_UP));
						reportVO.setOctAmt(rs.getBigDecimal("Oct").setScale(2, RoundingMode.HALF_UP));
						reportVO.setNovAmt(rs.getBigDecimal("Nov").setScale(2, RoundingMode.HALF_UP));
						reportVO.setDecAmt(rs.getBigDecimal("Decss").setScale(2, RoundingMode.HALF_UP));
						return reportVO;
					}
				};
				groupNameList = namedParameterJdbcTemplate
						.query(sqlDue, hMap, rMapper);
	 		    
	 		    log.debug("Exit : public List getMonthWiseBreakUpReportForReceivablesForCostCenters(int intSocietyID)");
	 		}
	 		catch(Exception ex)
	 		{
	 			//ex.printStackTrace();
	 			log.error("Exception in getMonthWiseBreakUpReportForReceivablesForCostCenters : "+ex);
	 		}
	 	
	 		return groupNameList;
	 		
	 	}
	  
	  public List getReceivableReportByCostCenter(int societyID,String fromDate, String uptoDate,String projectName){

			List<AccountHeadVO> ledgerList = new ArrayList<AccountHeadVO>();
			String orgCondition="";
			String orgClause="";
			String andCondition="";
			String orCondition="";
			
			try {
				
				//Remove whitespace and split by comma 
		        List<String> result = Arrays.asList(projectName.split("\\s*,\\s*"));
		        if(result.size()==1){
		        	orCondition=orCondition+" pm.project_acronyms='"+projectName.trim()+"'";
		        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projectName.trim()+"%')OR(al.project_tags LIKE '%#"+projectName.trim()+"%'))";
		        	
		        }else{
		        for(int i=0;result.size()>i;i++){
		        	String projectAcronyms=result.get(i);
		        	if(i==0){
		        	orCondition=orCondition+" pm.project_acronyms='"+projectAcronyms.trim()+"'";
		        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(al.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
		        	}else{
		        	orCondition=orCondition+" or pm.project_acronyms='"+projectAcronyms.trim()+"'";
		        	andCondition=andCondition+" and ((ale.tx_le_tags LIKE '%#"+projectAcronyms.trim()+"%')OR(al.project_tags LIKE '%#"+projectAcronyms.trim()+"%'))";
		        	}}}

				log
						.debug("Entry : public List getReceivableReportByCostCenter(int societyID,String fromDate, String uptoDate,String projectName)");
				SocietyVO societyVO=societyService.getSocietyDetails(societyID);
				String query = " SELECT a.id AS ledger_id,txTable.closing_balance,txTable.opening_balance,a.opening_balance AS openingBalWhenNoTx,ag.group_name,a.sub_group_id AS group_id,a.ledger_name AS ledger_name,ag.root_id,ar.normal_balance FROM account_groups ag,account_root_group ar, account_category c,account_ledgers_"
						+  societyVO.getDataZoneID()
						+ " a right JOIN "
						+ " (SELECT closingBal.ledger_id AS ledger_id, closingBal.closing_balance AS closing_balance,openingBal.opening_balance,group_name,root_id,normal_balance,closingBal.org_id  FROM(SELECT ale.org_id,ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS closing_balance,al.is_deleted ,r.root_id,r.normal_balance,g.group_name   "
						+ "  FROM account_ledgers_"
						+ societyVO.getDataZoneID()
						+ " al, account_transactions_"
						+  societyVO.getDataZoneID()
						+ " ats,account_ledger_entry_"
						+  societyVO.getDataZoneID()
						+ " ale,account_groups g,account_root_group r, project_master_details pm WHERE ats.org_id=pm.org_id and ats.org_id=ale.org_id and ale.org_id=al.org_id "+orgClause+" and ats.tx_id=ale.tx_id AND al.sub_group_id=g.id AND g.root_id=r.root_id AND ats.tx_date <= :toDate AND ats.is_deleted=0 "
						+ " AND ale.ledger_id = al.id and ("+orCondition+") AND ("+andCondition+" ) GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS closingBal LEFT JOIN (SELECT ale.org_id, ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS opening_balance,al.is_deleted  "
						+ " FROM account_ledgers_"
						+  societyVO.getDataZoneID()
						+ " al, account_transactions_"
						+  societyVO.getDataZoneID()
						+ " ats,account_ledger_entry_"
						+  societyVO.getDataZoneID()
						+ " ale, project_master_details pm WHERE ats.org_id=pm.org_id and ats.org_id=ale.org_id and ale.org_id=al.org_id "+orgClause+" and ats.tx_id=ale.tx_id  AND ats.tx_date <:fromDate AND ats.is_deleted=0 AND ale.ledger_id = al.id and ("+orCondition+") AND ("+andCondition+" ) GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS openingBal ON closingBal.ledger_id=openingBal.ledger_id ) AS txTable ON a.id=txTable.ledger_id WHERE "+orgCondition+"  ag.category_id=c.category_id AND c.category_id=1 and a.is_deleted=0 AND a.sub_group_id=ag.id AND ag.root_id=ar.root_id;";
				log.debug(query);
				Map hMap = new HashMap();
				hMap.put("societyID", societyID);
				hMap.put("fromDate", fromDate);
				hMap.put("toDate", uptoDate);
				

				RowMapper Rmapper = new RowMapper() {
					String normalBalance = "";

					int rootID = 0;

					String groupName = "";

					int groupID = 0;

					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {

						AccountHeadVO acHVO = new AccountHeadVO();

						acHVO
								.setOpeningBalance(rs
										.getBigDecimal("opening_balance"));
						acHVO
								.setClosingBalance(rs
										.getBigDecimal("closing_balance"));

						acHVO.setRootID(rs.getInt("root_id"));
						acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
						acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
						acHVO.setAccountGroupID(rs.getInt("group_id"));
						acHVO.setLedgerName(rs.getString("ledger_name"));
						//acHVO.setCategoryID(rs.getInt("group_id"));
						acHVO.setCategoryName(rs.getString("group_name"));
						acHVO.setLedgerID(rs.getInt("ledger_id"));
						if (rs.getString("normal_balance") != null) {
							acHVO.setNormalBalance(rs.getString("normal_balance"));
							normalBalance = rs.getString("normal_balance");
							rootID = rs.getInt("root_id");
							groupName = rs.getString("group_name");
							groupID = rs.getInt("group_id");

						}
						if ((rs.getBigDecimal("opening_balance") == null)
								&& (rs.getBigDecimal("closing_balance") == null)) {

							acHVO.setClosingBalance(rs
									.getBigDecimal("openingBalWhenNoTx"));
							acHVO.setOpeningBalance(rs
									.getBigDecimal("openingBalWhenNoTx"));
							acHVO.setNormalBalance(normalBalance);
							acHVO.setRootID(rootID);
							acHVO.setAccountGroupID(groupID);
							acHVO.setStrAccountPrimaryHead(groupName);
						} else if (rs.getBigDecimal("opening_balance") == null) {
							acHVO.setNormalBalance(normalBalance);
							acHVO.setRootID(rootID);
							acHVO.setAccountGroupID(groupID);
							acHVO.setStrAccountPrimaryHead(groupName);
							acHVO.setOpeningBalance(rs
									.getBigDecimal("openingBalWhenNoTx"));
						}

						return acHVO;
					}

				};
				ledgerList = namedParameterJdbcTemplate.query(query, hMap, Rmapper);

				log
						.debug("Exit : public List getReceivableReportByCostCenter(int societyID,String fromDate, String uptoDate,String projectName)"
								+ ledgerList.size());
			} catch (BadSqlGrammarException ex) {
				log.info("Exception in public List getReceivableReportByCostCenter(int societyID,String fromDate, String uptoDate,String projectName) : " + ex);
			} catch (Exception e) {
				log.error("Exception : public List getReceivableReportByCostCenter(int societyID,String fromDate, String uptoDate,String projectName)" + e);
			}

			return ledgerList;
		}
	  
	  
	  public List getMonthWiseBreakUpReportForReceivablesForCostCategory(int societyID,String fromDate,String uptoDate,String groupName,List categoryList)
	 	{   
	 	   List groupNameList=new ArrayList();
	 		try
	 		{
	 			log.debug("Entry : public List getMonthWiseBreakUpReportForReceivablesForCostCenters(int intSocietyID)");
	 			String strWhereClause = null;
	 			String strConditions=null;
	 			String strClause=null;
	 			String andCondition="";
	 			String orCondition="";
	 			if(groupName.equalsIgnoreCase("G"))// Grouped
	 			{
	 				strWhereClause="c.category_id ";
	 				strConditions="";
	 				strClause="category_id ";
	 			}else if(groupName.equalsIgnoreCase("I"))// Individual ledger wise
	 			{
	 				strWhereClause="al.id ";
	 				strConditions=" chart.ledgerID=op_balance.id AND ";
	 				strClause="ledger.id ";
	 			}
	 			
	 			for(int i=0;categoryList.size()>i;i++){
					ProjectDetailsVO projVO=(ProjectDetailsVO) categoryList.get(i);
					  if(categoryList.size()==1){
				        	orCondition=orCondition+" pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(al.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	
				        }else{
				       
				        	if(i==0){
				        	orCondition=orCondition+" pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(al.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	}else{
				        	orCondition=orCondition+" or pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" or ((ale.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(al.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	}}
					
				}
	 			
				SocietyVO societyVO=societyService.getSocietyDetails(societyID);
				String sqlDue = "SELECT op_balance.id,op_balance.ledger_name AS ledgerName,op_balance.sub_group_id,op_balance.category_id AS categoryID,ifnull(Aprs,0) as Apr,ifnull(Mays,0) as May,ifnull(Juns,0) as Jun,ifnull(Juls,0) as Jul,ifnull(Augs,0) as Aug,ifnull(Septs,0) as Sept, "
						+ " ifnull(Octs,0) as Oct,ifnull(Novs,0) as Nov, ifnull(Decs,0) as Decss, ifnull(Jans,0) as Jan, ifnull(Febs,0) as Feb, ifnull(Mars,0) as Mar , chart.*,op_balance.opening_balance FROM(  SELECT al.id AS ledgerID,al.ledger_name, account_root_group.root_group_name,account_root_group.root_id, account_groups.group_name,account_groups.id AS groupID, c.category_id ,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'April' THEN ale.amount ELSE '0.00' END) AS Aprs, "+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'May' THEN ale.amount ELSE '0.00' END) AS Mays,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'June' THEN ale.amount ELSE '0.00' END) AS Juns,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'July' THEN ale.amount ELSE '0.00' END) AS Juls,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'August' THEN ale.amount ELSE '0.00' END) AS Augs,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'September' THEN ale.amount ELSE '0.00' END) AS Septs,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'October' THEN ale.amount ELSE '0.00' END) AS Octs,"+
												" SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'November' THEN ale.amount ELSE '0.00' END) AS Novs,"+
												"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'December' THEN ale.amount ELSE '0.00' END) AS Decs,"+
												"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'January' THEN ale.amount ELSE '0.00' END) AS Jans,"+
												"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'February' THEN ale.amount ELSE '0.00' END) AS Febs,"+
												"  SUM(CASE WHEN MONTHNAME(ats.tx_date) = 'March' THEN ale.amount ELSE '0.00' END) AS Mars "+
												"  FROM account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_ledgers_"+societyVO.getDataZoneID()+" al ,account_groups,account_root_group ,account_category c, project_master_details pm "+
												" WHERE ats.tx_date BETWEEN :fromDate AND :toDate AND ats.is_deleted=0  and ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID  "+
												" AND ale.tx_id = ats.tx_id AND ale.ledger_id = al.id  AND al.sub_group_id = account_groups.id AND account_groups.root_id = account_root_group.root_id AND account_groups.category_id=c.category_id and c.category_id=1 and ("+orCondition+") AND ("+andCondition+" ) GROUP BY "+strWhereClause+"  ORDER BY al.id ) as chart left join "+
												 " (SELECT  ledger.id,ledger.ledger_name,SUM(ledger.opening_balance + iFNULL(s.closing_balance,0) ) AS opening_balance,ledger.sub_group_id,ledger.category_id FROM (SELECT al.id,ale.ledger_id,al.ledger_name,al.opening_balance ,sub_group_id, "+
												" IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount)) AS sumAmt, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount)) AS closing_balance, c.category_id "+
												" FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale ,account_groups g,account_category c,project_master_details pm  WHERE ats.org_id=pm.org_id and ats.org_id=ale.org_id AND ale.org_id=al.org_id AND al.org_id=:societyID AND ats.tx_id=ale.tx_id AND ats.tx_date < :fromDate AND al.sub_group_id=g.id AND g.category_id=c.category_id AND ats.is_deleted=0 AND ale.ledger_id = al.id and c.category_id=1 and ("+orCondition+") AND ("+andCondition+" ) "+ 
												" GROUP BY al.id ) s  left JOIN (SELECT a.*,g.category_id FROM account_ledgers_"+societyVO.getDataZoneID()+" a,account_groups g  WHERE a.sub_group_id=g.id AND g.category_id=1 AND a.org_id=:societyID AND a.is_deleted=0) ledger ON s.id=ledger.id GROUP BY "+strClause+" ) AS op_balance on "+strConditions+"  chart.category_id=op_balance.category_id  ;" ;
				
				log.debug("sql "+sqlDue);
				Map hMap = new HashMap();
				hMap.put("societyID", societyID);
				hMap.put("fromDate", fromDate);
				hMap.put("toDate",uptoDate);

				RowMapper rMapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int num) throws SQLException {
						// TODO Auto-generated method stub
						MonthwiseChartVO reportVO=new MonthwiseChartVO();
						reportVO.setLedgerName(rs.getString("ledger_name"));
						reportVO.setLedgerID(rs.getInt("id"));
						reportVO.setRootGroupName(rs.getString("root_group_name"));
						reportVO.setRootGroupID(rs.getInt("root_id"));
						reportVO.setGroupName(rs.getString("group_name"));
						reportVO.setGroupID(rs.getInt("sub_group_id"));
						reportVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
						reportVO.setJanAmt(rs.getBigDecimal("Jan").setScale(2, RoundingMode.HALF_UP));
						reportVO.setFebAmt(rs.getBigDecimal("Feb").setScale(2, RoundingMode.HALF_UP));
						reportVO.setMarAmt(rs.getBigDecimal("Mar").setScale(2, RoundingMode.HALF_UP));
						reportVO.setAprAmt(rs.getBigDecimal("Apr").setScale(2, RoundingMode.HALF_UP));
						reportVO.setMayAmt(rs.getBigDecimal("May").setScale(2, RoundingMode.HALF_UP));
						reportVO.setJunAmt(rs.getBigDecimal("Jun").setScale(2, RoundingMode.HALF_UP));
						reportVO.setJulAmt(rs.getBigDecimal("Jul").setScale(2, RoundingMode.HALF_UP));
						reportVO.setAugAmt(rs.getBigDecimal("Aug").setScale(2, RoundingMode.HALF_UP));
						reportVO.setSeptAmt(rs.getBigDecimal("Sept").setScale(2, RoundingMode.HALF_UP));
						reportVO.setOctAmt(rs.getBigDecimal("Oct").setScale(2, RoundingMode.HALF_UP));
						reportVO.setNovAmt(rs.getBigDecimal("Nov").setScale(2, RoundingMode.HALF_UP));
						reportVO.setDecAmt(rs.getBigDecimal("Decss").setScale(2, RoundingMode.HALF_UP));
						return reportVO;
					}
				};
				groupNameList = namedParameterJdbcTemplate
						.query(sqlDue, hMap, rMapper);
	 		    
	 		    log.debug("Exit : public List getMonthWiseBreakUpReportForReceivablesForCostCenters(int intSocietyID)");
	 		}
	 		catch(Exception ex)
	 		{
	 			//ex.printStackTrace();
	 			log.error("Exception in getMonthWiseBreakUpReportForReceivablesForCostCenters : "+ex);
	 		}
	 	
	 		return groupNameList;
	 		
	 	}
	  
	  public List getReceivableReportByCostCategory(int societyID,String fromDate, String uptoDate,List categoryList){

			List<AccountHeadVO> ledgerList = new ArrayList<AccountHeadVO>();
			String orgCondition="";
			String orgClause="";
			String andCondition="";
			String orCondition="";
			
			try {
				
				for(int i=0;categoryList.size()>i;i++){
					ProjectDetailsVO projVO=(ProjectDetailsVO) categoryList.get(i);
					  if(categoryList.size()==1){
				        	orCondition=orCondition+" pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(al.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	
				        }else{
				       
				        	if(i==0){
				        	orCondition=orCondition+" pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" ((ale.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(al.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	}else{
				        	orCondition=orCondition+" or pm.project_acronyms='"+projVO.getProjectAcronyms().trim()+"'";
				        	andCondition=andCondition+" or ((ale.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(al.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
				        	}}
					
				}

				log
						.debug("Entry : public List getReceivableReportByCostCategory(int societyID,String fromDate, String uptoDate,String projectName)");
				SocietyVO societyVO=societyService.getSocietyDetails(societyID);
				String query = " SELECT a.id AS ledger_id,txTable.closing_balance,txTable.opening_balance,a.opening_balance AS openingBalWhenNoTx,ag.group_name,a.sub_group_id AS group_id,a.ledger_name AS ledger_name,ag.root_id,ar.normal_balance FROM account_groups ag,account_root_group ar, account_category c,account_ledgers_"
						+  societyVO.getDataZoneID()
						+ " a right JOIN "
						+ " (SELECT closingBal.ledger_id AS ledger_id, closingBal.closing_balance AS closing_balance,openingBal.opening_balance,group_name,root_id,normal_balance,closingBal.org_id  FROM(SELECT ale.org_id,ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS closing_balance,al.is_deleted ,r.root_id,r.normal_balance,g.group_name   "
						+ "  FROM account_ledgers_"
						+ societyVO.getDataZoneID()
						+ " al, account_transactions_"
						+  societyVO.getDataZoneID()
						+ " ats,account_ledger_entry_"
						+  societyVO.getDataZoneID()
						+ " ale,account_groups g,account_root_group r, project_master_details pm WHERE ats.org_id=pm.org_id and ats.org_id=ale.org_id and ale.org_id=al.org_id "+orgClause+" and ats.tx_id=ale.tx_id AND al.sub_group_id=g.id AND g.root_id=r.root_id AND ats.tx_date <= :toDate AND ats.is_deleted=0 "
						+ " AND ale.ledger_id = al.id and ("+orCondition+") AND ("+andCondition+" ) GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS closingBal LEFT JOIN (SELECT ale.org_id, ale.ledger_id,al.ledger_name,al.sub_group_id, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS opening_balance,al.is_deleted  "
						+ " FROM account_ledgers_"
						+  societyVO.getDataZoneID()
						+ " al, account_transactions_"
						+  societyVO.getDataZoneID()
						+ " ats,account_ledger_entry_"
						+  societyVO.getDataZoneID()
						+ " ale, project_master_details pm WHERE ats.org_id=pm.org_id and ats.org_id=ale.org_id and ale.org_id=al.org_id "+orgClause+" and ats.tx_id=ale.tx_id  AND ats.tx_date <:fromDate AND ats.is_deleted=0 AND ale.ledger_id = al.id and ("+orCondition+") AND ("+andCondition+" ) GROUP BY ale.ledger_id  ORDER BY ale.ledger_id) AS openingBal ON closingBal.ledger_id=openingBal.ledger_id ) AS txTable ON a.id=txTable.ledger_id WHERE "+orgCondition+"  ag.category_id=c.category_id AND c.category_id=1 and a.is_deleted=0 AND a.sub_group_id=ag.id AND ag.root_id=ar.root_id;";
				log.debug(query);
				Map hMap = new HashMap();
				hMap.put("societyID", societyID);
				hMap.put("fromDate", fromDate);
				hMap.put("toDate", uptoDate);
				

				RowMapper Rmapper = new RowMapper() {
					String normalBalance = "";

					int rootID = 0;

					String groupName = "";

					int groupID = 0;

					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {

						AccountHeadVO acHVO = new AccountHeadVO();

						acHVO
								.setOpeningBalance(rs
										.getBigDecimal("opening_balance"));
						acHVO
								.setClosingBalance(rs
										.getBigDecimal("closing_balance"));

						acHVO.setRootID(rs.getInt("root_id"));
						acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
						acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
						acHVO.setAccountGroupID(rs.getInt("group_id"));
						acHVO.setLedgerName(rs.getString("ledger_name"));
						//acHVO.setCategoryID(rs.getInt("group_id"));
						acHVO.setCategoryName(rs.getString("group_name"));
						acHVO.setLedgerID(rs.getInt("ledger_id"));
						if (rs.getString("normal_balance") != null) {
							acHVO.setNormalBalance(rs.getString("normal_balance"));
							normalBalance = rs.getString("normal_balance");
							rootID = rs.getInt("root_id");
							groupName = rs.getString("group_name");
							groupID = rs.getInt("group_id");

						}
						if ((rs.getBigDecimal("opening_balance") == null)
								&& (rs.getBigDecimal("closing_balance") == null)) {

							acHVO.setClosingBalance(rs
									.getBigDecimal("openingBalWhenNoTx"));
							acHVO.setOpeningBalance(rs
									.getBigDecimal("openingBalWhenNoTx"));
							acHVO.setNormalBalance(normalBalance);
							acHVO.setRootID(rootID);
							acHVO.setAccountGroupID(groupID);
							acHVO.setStrAccountPrimaryHead(groupName);
						} else if (rs.getBigDecimal("opening_balance") == null) {
							acHVO.setNormalBalance(normalBalance);
							acHVO.setRootID(rootID);
							acHVO.setAccountGroupID(groupID);
							acHVO.setStrAccountPrimaryHead(groupName);
							acHVO.setOpeningBalance(rs
									.getBigDecimal("openingBalWhenNoTx"));
						}

						return acHVO;
					}

				};
				ledgerList = namedParameterJdbcTemplate.query(query, hMap, Rmapper);

				log
						.debug("Exit : public List getReceivableReportByCostCategory(int societyID,String fromDate, String uptoDate,String projectName)"
								+ ledgerList.size());
			} catch (BadSqlGrammarException ex) {
				log.info("Exception in public List getReceivableReportByCostCategory(int societyID,String fromDate, String uptoDate,String projectName) : " + ex);
			} catch (Exception e) {
				log.error("Exception : public List getReceivableReportByCostCategory(int societyID,String fromDate, String uptoDate,String projectName)" + e);
			}

			return ledgerList;
		}
	  
	
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}


	/**
	 * @param societyService the societyService to set
	 */
	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}

	
	
}
