package com.emanager.server.projects.valueObjects;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

public class ProjectDetailsVO {
	
	int projectID;
	int orgID;
	String projectName;
	String description;
	BigDecimal projectCost;
	int ledgerID;
	BigDecimal allocationAmount;// Allocation percentage or whole amount
	String allocationType; // P-percentage (%) / F- Fixed
	BigDecimal actualAmount;
	int groupID;// ledgerGroupID
	String groupName;// LedgerGroup Name
	String categoryName;// Ledger CategoryName
	String ledgerName;
	BigDecimal txAmount;// Sum of total transaction of the given ledger/group/project
	List <Object>objectList;
	String fromDate;
	String toDate;
	BigDecimal incomeTotal;
	BigDecimal expenseTotal;
	BigDecimal liabilityTotal;
	BigDecimal assetsTotal;
	BigDecimal totalAmount;
	int rootID;
	BigDecimal budgetAmt;
	BigDecimal budgetPercent;
	BigDecimal budgetVarience;
	int createdBy;
	int updatedBy;
	Timestamp createDate;
	Timestamp updateDate;
	List<ProjectDetailsVO> itemList;
	int itemID;
	String type;
	String projectAcronyms;
	String referenceID;
	int projectCategoryID;
	String projectCategoryName;
	int isDeleted;
	
	/**
	 * @return the projectID
	 */
	public int getProjectID() {
		return projectID;
	}
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @return the projectCost
	 */
	public BigDecimal getProjectCost() {
		return projectCost;
	}
	/**
	 * @return the ledgerID
	 */
	public int getLedgerID() {
		return ledgerID;
	}

	/**
	 * @return the allocationType
	 */
	public String getAllocationType() {
		return allocationType;
	}
	/**
	 * @param projectID the projectID to set
	 */
	public void setProjectID(int projectID) {
		this.projectID = projectID;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @param projectCost the projectCost to set
	 */
	public void setProjectCost(BigDecimal projectCost) {
		this.projectCost = projectCost;
	}
	/**
	 * @param ledgerID the ledgerID to set
	 */
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}

	/**
	 * @param allocationType the allocationType to set
	 */
	public void setAllocationType(String allocationType) {
		this.allocationType = allocationType;
	}
	/**
	 * @return the allocationAmount
	 */
	public BigDecimal getAllocationAmount() {
		return allocationAmount;
	}
	/**
	 * @return the actualAmount
	 */
	public BigDecimal getActualAmount() {
		return actualAmount;
	}
	/**
	 * @param allocationAmount the allocationAmount to set
	 */
	public void setAllocationAmount(BigDecimal allocationAmount) {
		this.allocationAmount = allocationAmount;
	}
	/**
	 * @param actualAmount the actualAmount to set
	 */
	public void setActualAmount(BigDecimal actualAmount) {
		this.actualAmount = actualAmount;
	}
	/**
	 * @return the groupID
	 */
	public int getGroupID() {
		return groupID;
	}
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}
	/**
	 * @return the ledgerName
	 */
	public String getLedgerName() {
		return ledgerName;
	}
	/**
	 * @param groupID the groupID to set
	 */
	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}
	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	/**
	 * @param categoryName the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	/**
	 * @param ledgerName the ledgerName to set
	 */
	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}
	/**
	 * @return the txAmount
	 */
	public BigDecimal getTxAmount() {
		return txAmount;
	}
	/**
	 * @param txAmount the txAmount to set
	 */
	public void setTxAmount(BigDecimal txAmount) {
		this.txAmount = txAmount;
	}
	
	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	/**
	 * @return the objectList
	 */
	public List<Object> getObjectList() {
		return objectList;
	}
	/**
	 * @param objectList the objectList to set
	 */
	public void setObjectList(List<Object> objectList) {
		this.objectList = objectList;
	}
	/**
	 * @return the incomeTotal
	 */
	public BigDecimal getIncomeTotal() {
		return incomeTotal;
	}
	/**
	 * @return the expenseTotal
	 */
	public BigDecimal getExpenseTotal() {
		return expenseTotal;
	}
	/**
	 * @return the totalAmount
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	/**
	 * @return the rootID
	 */
	public int getRootID() {
		return rootID;
	}
	/**
	 * @param incomeTotal the incomeTotal to set
	 */
	public void setIncomeTotal(BigDecimal incomeTotal) {
		this.incomeTotal = incomeTotal;
	}
	/**
	 * @param expenseTotal the expenseTotal to set
	 */
	public void setExpenseTotal(BigDecimal expenseTotal) {
		this.expenseTotal = expenseTotal;
	}
	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	/**
	 * @param rootID the rootID to set
	 */
	public void setRootID(int rootID) {
		this.rootID = rootID;
	}
	/**
	 * @return the budgetAmt
	 */
	public BigDecimal getBudgetAmt() {
		return budgetAmt;
	}
	/**
	 * @return the budgetPercent
	 */
	public BigDecimal getBudgetPercent() {
		return budgetPercent;
	}
	/**
	 * @return the budgetVarience
	 */
	public BigDecimal getBudgetVarience() {
		return budgetVarience;
	}
	/**
	 * @param budgetAmt the budgetAmt to set
	 */
	public void setBudgetAmt(BigDecimal budgetAmt) {
		this.budgetAmt = budgetAmt;
	}
	/**
	 * @param budgetPercent the budgetPercent to set
	 */
	public void setBudgetPercent(BigDecimal budgetPercent) {
		this.budgetPercent = budgetPercent;
	}
	/**
	 * @param budgetVarience the budgetVarience to set
	 */
	public void setBudgetVarience(BigDecimal budgetVarience) {
		this.budgetVarience = budgetVarience;
	}
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	public int getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Timestamp getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
	public Timestamp getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
	public List<ProjectDetailsVO> getItemList() {
		return itemList;
	}
	public void setItemList(List<ProjectDetailsVO> itemList) {
		this.itemList = itemList;
	}
	public int getItemID() {
		return itemID;
	}
	public void setItemID(int itemID) {
		this.itemID = itemID;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getProjectAcronyms() {
		return projectAcronyms;
	}
	public void setProjectAcronyms(String projectAcronyms) {
		this.projectAcronyms = projectAcronyms;
	}
	public String getReferenceID() {
		return referenceID;
	}
	public void setReferenceID(String referenceID) {
		this.referenceID = referenceID;
	}
	/**
	 * @return the liabilityTotal
	 */
	public BigDecimal getLiabilityTotal() {
		return liabilityTotal;
	}
	/**
	 * @param liabilityTotal the liabilityTotal to set
	 */
	public void setLiabilityTotal(BigDecimal liabilityTotal) {
		this.liabilityTotal = liabilityTotal;
	}
	/**
	 * @return the assetsTotal
	 */
	public BigDecimal getAssetsTotal() {
		return assetsTotal;
	}
	/**
	 * @param assetsTotal the assetsTotal to set
	 */
	public void setAssetsTotal(BigDecimal assetsTotal) {
		this.assetsTotal = assetsTotal;
	}
	/**
	 * @return the projectCategoryID
	 */
	public int getProjectCategoryID() {
		return projectCategoryID;
	}
	/**
	 * @param projectCategoryID the projectCategoryID to set
	 */
	public void setProjectCategoryID(int projectCategoryID) {
		this.projectCategoryID = projectCategoryID;
	}
	/**
	 * @return the projectCategoryName
	 */
	public String getProjectCategoryName() {
		return projectCategoryName;
	}
	/**
	 * @param projectCategoryName the projectCategoryName to set
	 */
	public void setProjectCategoryName(String projectCategoryName) {
		this.projectCategoryName = projectCategoryName;
	}
	/**
	 * @return the isDeleted
	 */
	public int getIsDeleted() {
		return isDeleted;
	}
	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	

}
