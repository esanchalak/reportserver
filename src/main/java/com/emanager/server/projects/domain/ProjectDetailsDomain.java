package com.emanager.server.projects.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.financialReports.valueObject.MonthwiseChartVO;
import com.emanager.server.financialReports.valueObject.ReportDetailsVO;
import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.projects.dataAccessObject.ProjectDetailsDAO;
import com.emanager.server.projects.valueObjects.ProjectDetailsVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;

public class ProjectDetailsDomain {

	private static final Logger log=Logger.getLogger(ProjectDetailsDomain.class);
	
	ProjectDetailsDAO projectDetailsDAO;
	SocietyService societyServiceBean;
	LedgerService ledgerService;
	DateUtility dateUtil=new DateUtility();
	
	
	//-----Add Project Details -------//
			public int addProjectDetails(ProjectDetailsVO projectVO){
				log.debug("Entry : public int addProjectDetails(ProjectDetailsVO projectVO)");
				int success=0;
				try{
					
					success=projectDetailsDAO.addProjectDetails(projectVO);		
														
				}catch(Exception e){
					log.error("Exception in adding project details "+e);
				}
				
				
				log.debug("Exit : public int addProjectDetails(ProjectDetailsVO projectVO)");
				return success;
			}
			
			//-----Update Project Details -------//
				public int updateProjectDetails(ProjectDetailsVO projectVO){
					log.debug("Entry : public int updateProjectDetails(ProjectDetailsVO projectVO)");
					int success=0;
					try{
						
						success=projectDetailsDAO.updateProjectDetails(projectVO);			
						
					}catch(Exception e){
						log.error("Exception in updating project details "+e);
					}
					
					
					log.debug("Exit : public int updateProjectDetails(ProjectDetailsVO projectVO)");
					return success;
				}
			
				//-----Delete Project Details -------//
						public int deleteProjectDetails(ProjectDetailsVO projectVO){
							log.debug("Entry : public int deleteProjectDetails(ProjectDetailsVO projectVO)");
							int success=0;
							try{
								
								success=projectDetailsDAO.deleteProjectDetails(projectVO);			
								
								if(success>0){
									int successTx=projectDetailsDAO.unlinkProjectAssociationFromTransactions(projectVO);
									int successLdgr=projectDetailsDAO.unlinkProjectAssociationFromLedgers(projectVO);
								}
								
							}catch(Exception e){
								log.error("Exception in deleting project details "+e);
							}
							
							
							log.debug("Exit : public int deleteProjectDetails(BudgetDetailsVO budgetVO)");
							return success;
						}
						
				
						//-----Add Project Item Details -------//
						public int addProjectItemDetails(ProjectDetailsVO projectVO){
							log.debug("Entry : public int addProjectItemDetails(ProjectDetailsVO projectVO)");
							int success=0;
							try{
								
								success=projectDetailsDAO.addProjectItemDetails(projectVO);			
								
							}catch(Exception e){
								log.error("Exception in adding Project details "+e);
							}
							
							
							log.debug("Exit : public int addProjectItemDetails(ProjectDetailsVO projectVO)");
							return success;
						}
						
						//-----Add bulk Project Item Details -------//
						public int addBulkProjectItemDetails(List projectList){
							log.debug("Entry : public int addBulkProjectItemDetails(List projectList)");
							int success=0;
							int projectSuccess=0;
							try{
								
								for(int i=0;projectList.size()>i;i++){
									ProjectDetailsVO projectDetailsVO=(ProjectDetailsVO) projectList.get(i);
									projectSuccess=projectDetailsDAO.addProjectItemDetails(projectDetailsVO);
									if(projectSuccess>0)
									success=success+1;
								}
								
							}catch(Exception e){
								log.error("Exception in adding Project details "+e);
							}
							
							
							log.debug("Exit : public int addBulkProjectItemDetails(List budgetList)");
							return success;
						}
						
						//-----Update Project Item Details -------//
							public int updateProjectItemDetails(ProjectDetailsVO projectVO){
								log.debug("Entry : public int updateProjectItemDetails(ProjectDetailsVO projectVO)");
								int success=0;
								try{
									
									success=projectDetailsDAO.updateProjectItemDetails(projectVO);			
									
								}catch(Exception e){
									log.error("Exception in updating Project details "+e);
								}
								
								
								log.debug("Exit : public int updateProjectItemDetails(ProjectDetailsVO projectVO)");
								return success;
							}
						
							//-----Delete Project Item Details -------//
							public int deleteProjectItemDetails(ProjectDetailsVO projectVO){
										log.debug("Entry : public int deleteProjectItemDetails(ProjectDetailsVO ProjectVO)");
										int success=0;
										try{
											
											success=projectDetailsDAO.deleteProjectItemDetails(projectVO);			
											
										}catch(Exception e){
											log.error("Exception in deleting Project details "+e);
										}
										
										
										log.debug("Exit : public int deleteProjectItemDetails(ProjectDetailsVO Project)");
										return success;
									}
	

	public List getProjectDetailsList(int orgID){
		log.debug("Entry :  public List getProjectDetailsList(int orgID)");
		List projectList=null;
		
		projectList=projectDetailsDAO.getProjectDetailsList(orgID);
		
		log.debug("Exit : public List getProjectDetailsList(int orgID)");
		return projectList;
		
	}
	
	
	public List getProjectDetailsListTagWise(ProjectDetailsVO projectVO){
		log.debug("Entry :  public List getProjectDetailsList(ProjectDetailsVO projectVO)");
		List projectList=null;
		
		projectList=projectDetailsDAO.getProjectDetailsListTagWise(projectVO);
		
		log.debug("Exit : public List getProjectDetailsList(ProjectDetailsVO projectVO)");
		return projectList;
		
	}
	public List getProjectDetailsListWithDetails(int orgID,String fromDate,String toDate){
		log.debug("Entry :  public List getProjectDetailsListWithDetails(int orgID,String fromDate,String toDate)");
		List projectList=null;
		
		projectList=projectDetailsDAO.getProjectDetailsListWithDetails(orgID,fromDate,toDate);
		
		if(projectList.size()>0){
			
			for(int i=0;projectList.size()>i;i++){
				BigDecimal incomeTotal=BigDecimal.ZERO;
				BigDecimal expenseTotal=BigDecimal.ZERO;
				BigDecimal totalAmount=BigDecimal.ZERO;
				ProjectDetailsVO projectVO=(ProjectDetailsVO) projectList.get(i);
				List objectList=projectDetailsDAO.getLedgersListInProject(orgID, projectVO.getProjectID(), fromDate, toDate);
				for(int j=0;objectList.size()>j;j++){
					ProjectDetailsVO projectDetVO=(ProjectDetailsVO) objectList.get(j);
					if((projectDetVO.getRootID()==1)||(projectDetVO.getRootID()==4)){
						incomeTotal=incomeTotal.add(projectDetVO.getActualAmount());
					}else if((projectDetVO.getRootID()==2)||(projectDetVO.getRootID()==3)){
						expenseTotal=expenseTotal.add(projectDetVO.getActualAmount());
					}
					
				}
					totalAmount=incomeTotal.subtract(expenseTotal);
					projectVO.setTotalAmount(totalAmount);
					projectVO.setIncomeTotal(incomeTotal);
					projectVO.setExpenseTotal(expenseTotal);
     				projectVO.setObjectList(objectList);
			}
			
			
		}
		
		
		log.debug("Exit : public List getProjectDetailsListWithDetails(int orgID,String fromDate,String toDate)");
		return projectList;
		
	}
	
	public List getProjectItemList(int orgID,int projectID){
		log.debug("Entry :  public List getProjectItemList(int orgID,int projectID)");
		List projectList=null;
		
		projectList=projectDetailsDAO.getProjectItemList(orgID,projectID);
		
		log.debug("Exit : public List getProjectItemList(int orgID,int projectID)");
		return projectList;
		
	}
	
	public ProjectDetailsVO getProjectDetails(int orgID,int projectID, String fromDate, String toDate){
		log.debug("Entry : public ProjectDetailsVO getProjectDetails(int orgID,int projectID, String fromDate, String toDate)");
		ProjectDetailsVO projectVO=new ProjectDetailsVO();
		ProjectDetailsVO pvVO=new ProjectDetailsVO();
		BigDecimal incomeTotal=BigDecimal.ZERO;
		BigDecimal expenseTotal=BigDecimal.ZERO;
		BigDecimal totalAmount=BigDecimal.ZERO;
		List objectList=projectDetailsDAO.getLedgersListInProject(orgID, projectID, fromDate, toDate);
		if(objectList.size()>0){
		 pvVO=(ProjectDetailsVO) objectList.get(0);
			projectVO.setProjectID(pvVO.getProjectID());
			projectVO.setProjectCost(pvVO.getProjectCost());
			projectVO.setProjectName(pvVO.getProjectName());
			projectVO.setDescription(pvVO.getDescription());
			projectVO.setAllocationAmount(pvVO.getAllocationAmount());
			projectVO.setObjectList(objectList);
			
			for(int i=0;objectList.size()>i;i++){
				ProjectDetailsVO projectDetVO=(ProjectDetailsVO) objectList.get(i);
				if((projectDetVO.getRootID()==1)||(projectDetVO.getRootID()==4)){
					incomeTotal=incomeTotal.add(projectDetVO.getActualAmount());
				}else if((projectDetVO.getRootID()==2)||(projectDetVO.getRootID()==3)){
					expenseTotal=expenseTotal.add(projectDetVO.getActualAmount());
				}
				
			}
				totalAmount=incomeTotal.subtract(expenseTotal);
				projectVO.setTotalAmount(totalAmount);
				projectVO.setIncomeTotal(incomeTotal);
				projectVO.setExpenseTotal(expenseTotal);
			
		}
		
		
	
		
		log.debug("Exit : public ProjectDetailsVO getProjectDetails(int orgID,int projectID, String fromDate, String toDate)");
		return projectVO;
		
	}
	
	//========= New cost center list ===================//
	public List getProjectDetailsListWithDetailsTagWise(int orgID,String fromDate,String toDate){
		log.debug("Entry :  public List getProjectDetailsListWithDetails(int orgID,String fromDate,String toDate)");
		List projectList=null;
		
		projectList=projectDetailsDAO.getProjectDetailsListWithDetailsTagWise(orgID,"");
		
		if(projectList.size()>0){
			
			for(int i=0;projectList.size()>i;i++){
				BigDecimal incomeTotal=BigDecimal.ZERO;
				BigDecimal expenseTotal=BigDecimal.ZERO;
				BigDecimal totalAmount=BigDecimal.ZERO;
				ProjectDetailsVO projectVO=(ProjectDetailsVO) projectList.get(i);
				List objectList=projectDetailsDAO.getLedgersListInProjectTagWise(orgID, projectVO.getProjectAcronyms(), fromDate, toDate,0,0,projectVO.getRootID());
				for(int j=0;objectList.size()>j;j++){
					ProjectDetailsVO projectDetVO=(ProjectDetailsVO) objectList.get(j);
					if((projectDetVO.getRootID()==1)||(projectDetVO.getRootID()==4)){
						incomeTotal=incomeTotal.add(projectDetVO.getActualAmount());
					}else if((projectDetVO.getRootID()==2)||(projectDetVO.getRootID()==3)){
						expenseTotal=expenseTotal.add(projectDetVO.getActualAmount());
					}
					
				}
					totalAmount=incomeTotal.subtract(expenseTotal);
					projectVO.setTotalAmount(totalAmount);
					projectVO.setIncomeTotal(incomeTotal);
					projectVO.setExpenseTotal(expenseTotal);
     				projectVO.setObjectList(objectList);
			}
			
			
		}
		
		
		log.debug("Exit : public List getProjectDetailsListWithDetails(int orgID,String fromDate,String toDate)");
		return projectList;
		
	}
	
	//========= New cost center list ===================//
		public List getProjectDetailsSummaryListWithDetailsTagWise(int orgID,String fromDate,String toDate){
			log.debug("Entry :  public List getProjectDetailsListWithDetails(int orgID,String fromDate,String toDate)");
			List projectList=null;
			
			projectList=projectDetailsDAO.getProjectDetailsListWithDetailsTagWise(orgID,"");
			
			if(projectList.size()>0){
				
				for(int i=0;projectList.size()>i;i++){
					BigDecimal incomeTotal=BigDecimal.ZERO;
					BigDecimal expenseTotal=BigDecimal.ZERO;
					BigDecimal liabilityTotal=BigDecimal.ZERO;
					BigDecimal assetsTotal=BigDecimal.ZERO;
					BigDecimal totalAmount=BigDecimal.ZERO;
					BigDecimal IncExpTotal=BigDecimal.ZERO;
					BigDecimal liaAssetTotal=BigDecimal.ZERO;
					ProjectDetailsVO projectVO=(ProjectDetailsVO) projectList.get(i);
					List objectList=projectDetailsDAO.getLedgersSummaryList(orgID, projectVO.getProjectAcronyms(), fromDate, toDate);
					for(int j=0;objectList.size()>j;j++){
						ProjectDetailsVO projectDetVO=(ProjectDetailsVO) objectList.get(j);
						if(projectDetVO.getRootID()==1){
							incomeTotal=incomeTotal.add(projectDetVO.getActualAmount());
						}else if(projectDetVO.getRootID()==2){
							expenseTotal=expenseTotal.add(projectDetVO.getActualAmount());
						}else if(projectDetVO.getRootID()==3){
							assetsTotal=assetsTotal.add(projectDetVO.getActualAmount());
						}else if(projectDetVO.getRootID()==4){
							liabilityTotal=liabilityTotal.add(projectDetVO.getActualAmount());
						}
						
						List ledgerList=projectDetailsDAO.getLedgersListInProjectTagWise(orgID, projectVO.getProjectAcronyms(), fromDate, toDate, projectDetVO.getGroupID(),0,projectDetVO.getRootID());
						projectDetVO.setObjectList(ledgerList);
						
					}
						IncExpTotal=incomeTotal.subtract(expenseTotal);
						liaAssetTotal=assetsTotal.subtract(liabilityTotal);
						totalAmount=IncExpTotal.add(liaAssetTotal);
						projectVO.setTotalAmount(totalAmount);
						projectVO.setIncomeTotal(incomeTotal);
						projectVO.setExpenseTotal(expenseTotal);
						projectVO.setAssetsTotal(assetsTotal);
						projectVO.setLiabilityTotal(liabilityTotal);
	     				projectVO.setObjectList(objectList);
				}
				
				
			}
			
			
			log.debug("Exit : public List getProjectDetailsListWithDetails(int orgID,String fromDate,String toDate)");
			return projectList;
			
		}
		
	
	//========= New cost center list ===================//
	public List getProjectListWithTagWise(int orgID,String type){
		log.debug("Entry :  public List getProjectListWithTagWise(int orgID)");
		List projectList=null;
		
		projectList=projectDetailsDAO.getProjectDetailsListWithDetailsTagWise(orgID,type);
		
		log.debug("Exit : public List getProjectListWithTagWise(int orgID)");
		return projectList;
		
	}
	
	public List getProjectItemListTagWise(int orgID,String projectName){
		log.debug("Entry :  public List getProjectItemListTagWise(int orgID,String projectName)");
		List projectList=null;
		
		projectList=projectDetailsDAO.getProjectItemListTagWise(orgID,projectName);
		
		log.debug("Exit : public List getProjectItemListTagWise(int orgID,String projectName)");
		return projectList;
		
	}
	
	public ProjectDetailsVO getProjectDetailsTagWise(int orgID,String projectName, String fromDate, String toDate,int rootID){
		log.debug("Entry : public ProjectDetailsVO getProjectDetailsTagWise(int orgID,String projectName, String fromDate, String toDate)");
		ProjectDetailsVO projectVO=new ProjectDetailsVO();
		ProjectDetailsVO pvVO=new ProjectDetailsVO();
		BigDecimal incomeTotal=BigDecimal.ZERO;
		BigDecimal expenseTotal=BigDecimal.ZERO;
		BigDecimal liabilityTotal=BigDecimal.ZERO;
		BigDecimal assetsTotal=BigDecimal.ZERO;
		BigDecimal totalAmount=BigDecimal.ZERO;
		BigDecimal IncExpTotal=BigDecimal.ZERO;
		BigDecimal liaAssetTotal=BigDecimal.ZERO;
		
		List objectList=projectDetailsDAO.getLedgersListInProjectTagWise(orgID, projectName, fromDate, toDate,0,1,rootID);
		for(int j=0;objectList.size()>j;j++){
			ProjectDetailsVO projectDetVO=(ProjectDetailsVO) objectList.get(j);
			if(projectDetVO.getRootID()==1){
				incomeTotal=incomeTotal.add(projectDetVO.getActualAmount());
			}else if(projectDetVO.getRootID()==2){
				expenseTotal=expenseTotal.add(projectDetVO.getActualAmount());
			}else if(projectDetVO.getRootID()==3){
				assetsTotal=assetsTotal.add(projectDetVO.getActualAmount());
			}else if(projectDetVO.getRootID()==4){
				liabilityTotal=liabilityTotal.add(projectDetVO.getActualAmount());
			}
			
			List ledgerList=projectDetailsDAO.getLedgersListInProjectTagWise(orgID, projectName, fromDate, toDate, projectDetVO.getGroupID(),0,projectDetVO.getRootID());
			projectDetVO.setObjectList(ledgerList);
		}
		
		IncExpTotal=incomeTotal.subtract(expenseTotal);
		liaAssetTotal=assetsTotal.subtract(liabilityTotal);
		totalAmount=IncExpTotal.add(liaAssetTotal);
		projectVO.setTotalAmount(totalAmount);
		projectVO.setIncomeTotal(incomeTotal);
		projectVO.setExpenseTotal(expenseTotal);
		projectVO.setAssetsTotal(assetsTotal);
		projectVO.setLiabilityTotal(liabilityTotal);
		projectVO.setObjectList(objectList);
		
	
		
		log.debug("Exit : public ProjectDetailsVO getProjectDetailsTagWise(int orgID,String projectName, String fromDate, String toDate)");
		return projectVO;
		
	}
	
	
	public ProjectDetailsVO getProjectDetailsCategoryWise(int orgID,int categoryID, String fromDate, String toDate,int rootID){
		log.debug("Entry : public ProjectDetailsVO getProjectDetailsTagWise(int orgID,int categoryID, String fromDate, String toDate)");
		ProjectDetailsVO projectVO=new ProjectDetailsVO();
		ProjectDetailsVO pvVO=new ProjectDetailsVO();
		BigDecimal incomeTotal=BigDecimal.ZERO;
		BigDecimal expenseTotal=BigDecimal.ZERO;
		BigDecimal liabilityTotal=BigDecimal.ZERO;
		BigDecimal assetsTotal=BigDecimal.ZERO;
		BigDecimal totalAmount=BigDecimal.ZERO;
		BigDecimal IncExpTotal=BigDecimal.ZERO;
		BigDecimal liaAssetTotal=BigDecimal.ZERO;
		List categoryList=projectDetailsDAO.getCostCeneterCategoryLinkingList(categoryID);
		
		List objectList=projectDetailsDAO.getLedgersListInProjectCategoryWise(orgID, categoryList, fromDate, toDate,0,1,rootID,categoryID);
		for(int j=0;objectList.size()>j;j++){
			ProjectDetailsVO projectDetVO=(ProjectDetailsVO) objectList.get(j);
			if(projectDetVO.getRootID()==1){
				incomeTotal=incomeTotal.add(projectDetVO.getActualAmount());
			}else if(projectDetVO.getRootID()==2){
				expenseTotal=expenseTotal.add(projectDetVO.getActualAmount());
			}else if(projectDetVO.getRootID()==3){
				assetsTotal=assetsTotal.add(projectDetVO.getActualAmount());
			}else if(projectDetVO.getRootID()==4){
				liabilityTotal=liabilityTotal.add(projectDetVO.getActualAmount());
			}
			
			List ledgerList=projectDetailsDAO.getLedgersListInProjectCategoryWise(orgID, categoryList, fromDate, toDate, projectDetVO.getGroupID(),0,projectDetVO.getRootID(),categoryID);
			projectDetVO.setObjectList(ledgerList);
		}
		
		IncExpTotal=incomeTotal.subtract(expenseTotal);
		liaAssetTotal=assetsTotal.subtract(liabilityTotal);
		totalAmount=IncExpTotal.add(liaAssetTotal);
		projectVO.setTotalAmount(totalAmount);
		projectVO.setIncomeTotal(incomeTotal);
		projectVO.setExpenseTotal(expenseTotal);
		projectVO.setAssetsTotal(assetsTotal);
		projectVO.setLiabilityTotal(liabilityTotal);
		projectVO.setObjectList(objectList);
		
	
		
		log.debug("Exit : public ProjectDetailsVO getProjectDetailsTagWise(int orgID,String projectName, String fromDate, String toDate)");
		return projectVO;
		
	}
	
	
	public List getCostCeneterCategoryList(int orgID){
		log.debug("Entry : public List getCostCeneterCategoryList(int orgID)");
		List projectList=null;
		
		try {
			
			
		projectList=projectDetailsDAO.getCostCeneterCategoryList(orgID);	
			
	
		} catch (Exception e) {
			
			log.error("Exception : public int getCostCeneterCategoryList "+e);
		}
		
		log.debug("Exit : public List getCostCeneterCategoryList(int orgID)");
		return projectList;
		
	}
	
	public List getCostCeneterCategoryLinkingList(int categoryID){
		log.debug("Entry : public List getCostCeneterCategoryLinkingList(int categoryID)");
		List projectList=null;
		
		try {
			
			projectList=projectDetailsDAO.getCostCeneterCategoryLinkingList(categoryID);
	
		} catch (Exception e) {
			
			log.error("Exception : public int getCostCeneterCategoryLinkingList "+e);
		}
		
		log.debug("Exit : public List getCostCeneterCategoryLinkingList(int orgID)");
		return projectList;
		
	}
	
	public MonthwiseChartVO getCashBasedCostCenterReport(int societyID,String fromDate,String uptoDate,String rptType,int isConsolidated,String projectName)
  	{   
  	   MonthwiseChartVO reportVO=new MonthwiseChartVO();
  	  ReportVO monthlyObj=new ReportVO();
	  ReportVO quaterlyObj=new ReportVO();
  		try
  		{
  			log.debug("Entry : getCashBasedCostCenterReport(int societyID,String fromDate,String uptoDate,String rptType,int isConsolidated,String projectName)");
  			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
  			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
  			java.util.Date parse =  sdf.parse(fromDate);
  			Calendar c = Calendar.getInstance();
  			c.setTime(parse);
  			int month=c.get(Calendar.MONTH+1);
  			int year=c.get(Calendar.YEAR);
  			
  			//Fill year object
  			reportVO.setYearObj(getCashBasedCostCenterDetailsReport(societyID,year+"-04-01" ,sdf.format(dateUtil.getMonthLastDate(3,(year+1))),rptType,isConsolidated,projectName));
  			
  			//Fill monthwise breakup object
  			List monthwiseRcptPymtList=this.getMonthWiseCashBasedCostCenterReportForChart(societyID,year+"-04-01" ,sdf.format(dateUtil.getMonthLastDate(3,(year+1))),"G",isConsolidated,projectName);
  			ReportVO mntRcptVo=processMonthlyCashBasedReceiptPaymentObject(monthwiseRcptPymtList);
  			List incomeMonthwiseList=mntRcptVo.getIncomeTxList();
		    List expenseMonthwiseList=mntRcptVo.getExpenseTxList();
		    monthlyObj.setIncomeTxList(incomeMonthwiseList);
		    monthlyObj.setExpenseTxList(expenseMonthwiseList);
		    reportVO.setMonthlyObj(monthlyObj);
 			
		   //Fill quaterwise breakup object
		    List qtrWiseList=projectDetailsDAO.getQuaterWiseCashBasedCostCenterReportForChart(societyID,year+"-04-01" ,sdf.format(dateUtil.getMonthLastDate(3,(year+1))),"G",projectName);
  			ReportVO qtrWiseVO=processMonthlyCashBasedReceiptPaymentObject(qtrWiseList);
		    List incomeQuaterwiseList=qtrWiseVO.getIncomeTxList();
		    List expenseQuaterwiseList=qtrWiseVO.getExpenseTxList();
		    quaterlyObj.setIncomeTxList(incomeQuaterwiseList);
		    quaterlyObj.setExpenseTxList(expenseQuaterwiseList);
		    reportVO.setQuaterlyObj(quaterlyObj);
		    		   
 			// Fill chart object
		    List chartList=new ArrayList<>();
		    chartList.add(convertMonthlyChartObject(incomeMonthwiseList,"C"));
		    chartList.add(convertMonthlyChartObject(expenseMonthwiseList,"D"));
		    reportVO.setChartList(chartList);
 			
  			
  			
  			
  		    
  		    log.debug("Exit : getCashBasedCostCenterReport(int societyID,String fromDate,String uptoDate,String rptType,int isConsolidated,String projectName)");
  		}
  		catch(Exception ex)
  		{
  			//ex.printStackTrace();
  			log.error("Exception in getCashBasedCostCenterReport(int societyID,String fromDate,String uptoDate,String rptType,int isConsolidated,String projectName) : "+ex);
  		}
  	
  		return reportVO;
  		
  	}
	
	public ReportVO getCashBasedCostCenterDetailsReport(int societyID,String fromDate,String uptoDate,String rptType,int isConsolidated,String projectName){
		ReportVO rptVO =new ReportVO();
		SocietyVO societyVO=new SocietyVO();
		String convertedFrmDate=null;
		String convertedToDate=null;
		log.debug("Entry : public ReportVO getCashBasedCostCenterDetailsReport(int societyID,String fromDate,String uptoDate,int isConsolidated)"+societyID);
		
		try {

			List rcptPymtList=projectDetailsDAO.getCashBasedCostCenterReport(fromDate, uptoDate, societyID, isConsolidated,projectName);
		    ReportVO rcptVo=processCashBasedReceiptPaymentObject(rcptPymtList);
		    List incomeList=rcptVo.getIncomeTxList();
			List expeList=rcptVo.getExpenseTxList();		
			List rcptPymtDtList=projectDetailsDAO.getCashBasedCostCenterDetailsReport(fromDate, uptoDate, societyID, isConsolidated, projectName);
			ReportVO rcptDetVO=processCashBasedReceiptPaymentObject(rcptPymtDtList);
			List incomeDetailList=rcptDetVO.getIncomeTxList();
		    List expenseDetailList=rcptDetVO.getExpenseTxList();
		    
			societyVO= societyServiceBean.getSocietyDetails("0", societyID);
				
			/*	for(int i=0;cashInHandList.size()>i;i++){
					RptMonthYearVO achVO=(RptMonthYearVO) cashInHandList.get(i);
					bankBalanceList.add(achVO);
					
				}*/
				rptVO.setSocietyVO(societyVO);
				log.debug("Society Name: "+societyVO.getSocietyName()+" Address: "+societyVO.getAddress());
				
			   
				rptVO.setIncomeTxList(incomeList);
				rptVO.setExpenseTxList(expeList);
				//rptVO.setAccountBalanceList(bankBalanceList);
				rptVO.setIncomeDetailsTxList(incomeDetailList);
				rptVO.setExpenseDetailsTxList(expenseDetailList);
				rptVO=this.getProfitLossBalance(rptVO,rptType,"S");
				String type = rptVO.getExcessType();
				if(type.equalsIgnoreCase("Excess of Expenditure Over Income")){
					rptVO.setExcessType("Excess of Payment Over Receipt");
				}else if(type.equalsIgnoreCase("Excess of Income Over Expenditure")){
					rptVO.setExcessType("Excess of Receipt Over Payment");
				}
				ReportVO dateVO=dateUtil.getReportDate(fromDate, uptoDate);
				
				rptVO.setRptFromDate(dateVO.getFromDate());
				rptVO.setRptUptoDate(dateVO.getUptDate());
				rptVO.setFromDate(fromDate);
				rptVO.setUptDate(uptoDate);
				
				
				
		        log.debug("Converted From Date Format: "+rptVO.getFromDate()+" To date: "+ rptVO.getUptDate());
		} catch (NullPointerException ex) {
			log.info("getCashBasedCostCenterDetailsReport No details found for societyID:  "+societyID);

		} catch (Exception e) {
			log.error("Exception occured while preparing getCashBasedCostCenterDetailsReport Report "+e);
		}
			
		
		log.debug("Exit : public ReportVO getCashBasedCostCenterDetailsReport(int societyID,String fromDate,String uptoDate,int isConsolidated)");
		return rptVO;
	}
	
	private ReportVO processCashBasedReceiptPaymentObject(List rcptPymtList){
		log.debug("Entry : private ReportVO processCashBasedReceiptPaymentObject(List rcptPymtList)");
		ReportVO reportVO=new ReportVO();
		List incomeList=new ArrayList<>();
		List expenseList=new ArrayList<>();
		try{
			if(rcptPymtList.size()>0){
				
				for(int i=0;rcptPymtList.size()>i;i++){
					ReportDetailsVO rptVO=(ReportDetailsVO) rcptPymtList.get(i);
					
					if(rptVO.getRootID()==1){
						if(rptVO.getTxType().equalsIgnoreCase("C")){
							rptVO.setTotalAmount(rptVO.getDebitBalance());
							incomeList.add(rptVO);
						}else{
							rptVO.setTotalAmount(rptVO.getCreditBalance());
							expenseList.add(rptVO);
						}
						
					}else if(rptVO.getRootID()==2){
						if(rptVO.getTxType().equalsIgnoreCase("D")){
							rptVO.setTotalAmount(rptVO.getDebitBalance());
							expenseList.add(rptVO);
						}else{
							rptVO.setTotalAmount(rptVO.getCreditBalance());
							incomeList.add(rptVO);
						}
					}else if(rptVO.getRootID()==3){
						if(rptVO.getTxType().equalsIgnoreCase("C")){
							rptVO.setTotalAmount(rptVO.getCreditBalance());
							incomeList.add(rptVO);
						}else{
							rptVO.setTotalAmount(rptVO.getDebitBalance());
							expenseList.add(rptVO);
						}
						
					}else if(rptVO.getRootID()==4){
						if(rptVO.getTxType().equalsIgnoreCase("D")){
						rptVO.setTotalAmount(rptVO.getCreditBalance());
						expenseList.add(rptVO);
					}else{
						rptVO.setTotalAmount(rptVO.getDebitBalance());
						incomeList.add(rptVO);
					}
						
					}
					
					
					
					
					
				}
				
				
			}
			reportVO.setIncomeTxList(incomeList);
			reportVO.setExpenseTxList(expenseList);
			
			
		}catch(Exception e){
			log.error("Exception : private ReportVO processCashBasedReceiptPaymentObject(List rcptPymtList)"+e);
		}
		
		log.debug("Entry : private ReportVO processCashBasedReceiptPaymentObject(List rcptPymtList)");
		
		return reportVO;
	}
	
	private ReportVO processMonthlyCashBasedReceiptPaymentObject(List rcptPymtList){
		log.debug("Entry : private ReportVO processMonthlyCashBasedReceiptPaymentObject(List rcptPymtList)");
		ReportVO reportVO=new ReportVO();
		List incomeList=new ArrayList<>();
		List expenseList=new ArrayList<>();
		try{
			if(rcptPymtList.size()>0){
				
				for(int i=0;rcptPymtList.size()>i;i++){
					MonthwiseChartVO rptVO=(MonthwiseChartVO) rcptPymtList.get(i);
					if(rptVO.getRootGroupID()==1){
						if(rptVO.getTxType().equalsIgnoreCase("C")){
							//rptVO.setTotalAmount(rptVO.getDebitBalance());
							rptVO.setAprAmt(rptVO.getAprDb());
							rptVO.setMayAmt(rptVO.getMayDb());
							rptVO.setJunAmt(rptVO.getJunDb());
							rptVO.setJulAmt(rptVO.getJulDb());
							rptVO.setAugAmt(rptVO.getAugDb());
							rptVO.setSeptAmt(rptVO.getSeptDb());
							rptVO.setOctAmt(rptVO.getOctDb());
							rptVO.setNovAmt(rptVO.getNovDb());
							rptVO.setDecAmt(rptVO.getDecDb());
							rptVO.setJanAmt(rptVO.getJanDb());
							rptVO.setFebAmt(rptVO.getFebDb());
							rptVO.setMarAmt(rptVO.getMarDb());
							rptVO.setQ1Amt(rptVO.getQ1Db());
							rptVO.setQ2Amt(rptVO.getQ2Db());
							rptVO.setQ3Amt(rptVO.getQ3Db());
							rptVO.setQ4Amt(rptVO.getQ4Db());
							
							incomeList.add(rptVO);
						}else{
							//rptVO.setTotalAmount(rptVO.getCreditBalance());
							rptVO.setAprAmt(rptVO.getAprCr());
							rptVO.setMayAmt(rptVO.getMayCr());
							rptVO.setJunAmt(rptVO.getJunCr());
							rptVO.setJulAmt(rptVO.getJulCr());
							rptVO.setAugAmt(rptVO.getAugCr());
							rptVO.setSeptAmt(rptVO.getSeptCr());
							rptVO.setOctAmt(rptVO.getOctCr());
							rptVO.setNovAmt(rptVO.getNovCr());
							rptVO.setDecAmt(rptVO.getDecCr());
							rptVO.setJanAmt(rptVO.getJanCr());
							rptVO.setFebAmt(rptVO.getFebCr());
							rptVO.setMarAmt(rptVO.getMarCr());
							expenseList.add(rptVO);
						}
						
					}else if(rptVO.getRootGroupID()==2){
						if(rptVO.getTxType().equalsIgnoreCase("D")){
							//rptVO.setTotalAmount(rptVO.getDebitBalance());
							rptVO.setAprAmt(rptVO.getAprDb());
							rptVO.setMayAmt(rptVO.getMayDb());
							rptVO.setJunAmt(rptVO.getJunDb());
							rptVO.setJulAmt(rptVO.getJulDb());
							rptVO.setAugAmt(rptVO.getAugDb());
							rptVO.setSeptAmt(rptVO.getSeptDb());
							rptVO.setOctAmt(rptVO.getOctDb());
							rptVO.setNovAmt(rptVO.getNovDb());
							rptVO.setDecAmt(rptVO.getDecDb());
							rptVO.setJanAmt(rptVO.getJanDb());
							rptVO.setFebAmt(rptVO.getFebDb());
							rptVO.setMarAmt(rptVO.getMarDb());
							rptVO.setQ1Amt(rptVO.getQ1Db());
							rptVO.setQ2Amt(rptVO.getQ2Db());
							rptVO.setQ3Amt(rptVO.getQ3Db());
							rptVO.setQ4Amt(rptVO.getQ4Db());
							expenseList.add(rptVO);
						}else{
							//rptVO.setTotalAmount(rptVO.getCreditBalance());
							rptVO.setAprAmt(rptVO.getAprCr());
							rptVO.setMayAmt(rptVO.getMayCr());
							rptVO.setJunAmt(rptVO.getJunCr());
							rptVO.setJulAmt(rptVO.getJulCr());
							rptVO.setAugAmt(rptVO.getAugCr());
							rptVO.setSeptAmt(rptVO.getSeptCr());
							rptVO.setOctAmt(rptVO.getOctCr());
							rptVO.setNovAmt(rptVO.getNovCr());
							rptVO.setDecAmt(rptVO.getDecCr());
							rptVO.setJanAmt(rptVO.getJanCr());
							rptVO.setFebAmt(rptVO.getFebCr());
							rptVO.setMarAmt(rptVO.getMarCr());
							rptVO.setQ1Amt(rptVO.getQ1Cr());
							rptVO.setQ2Amt(rptVO.getQ2Cr());
							rptVO.setQ3Amt(rptVO.getQ3Cr());
							rptVO.setQ4Amt(rptVO.getQ4Cr());
							incomeList.add(rptVO);
						}
					}else if(rptVO.getRootGroupID()==3){
						if(rptVO.getTxType().equalsIgnoreCase("C")){
							//rptVO.setTotalAmount(rptVO.getCreditBalance());
							rptVO.setAprAmt(rptVO.getAprCr());
							rptVO.setMayAmt(rptVO.getMayCr());
							rptVO.setJunAmt(rptVO.getJunCr());
							rptVO.setJulAmt(rptVO.getJulCr());
							rptVO.setAugAmt(rptVO.getAugCr());
							rptVO.setSeptAmt(rptVO.getSeptCr());
							rptVO.setOctAmt(rptVO.getOctCr());
							rptVO.setNovAmt(rptVO.getNovCr());
							rptVO.setDecAmt(rptVO.getDecCr());
							rptVO.setJanAmt(rptVO.getJanCr());
							rptVO.setFebAmt(rptVO.getFebCr());
							rptVO.setMarAmt(rptVO.getMarCr());
							rptVO.setQ1Amt(rptVO.getQ1Cr());
							rptVO.setQ2Amt(rptVO.getQ2Cr());
							rptVO.setQ3Amt(rptVO.getQ3Cr());
							rptVO.setQ4Amt(rptVO.getQ4Cr());
							incomeList.add(rptVO);
						}else{
							//rptVO.setTotalAmount(rptVO.getDebitBalance());
							rptVO.setAprAmt(rptVO.getAprDb());
							rptVO.setMayAmt(rptVO.getMayDb());
							rptVO.setJunAmt(rptVO.getJunDb());
							rptVO.setJulAmt(rptVO.getJulDb());
							rptVO.setAugAmt(rptVO.getAugDb());
							rptVO.setSeptAmt(rptVO.getSeptDb());
							rptVO.setOctAmt(rptVO.getOctDb());
							rptVO.setNovAmt(rptVO.getNovDb());
							rptVO.setDecAmt(rptVO.getDecDb());
							rptVO.setJanAmt(rptVO.getJanDb());
							rptVO.setFebAmt(rptVO.getFebDb());
							rptVO.setMarAmt(rptVO.getMarDb());
							rptVO.setQ1Amt(rptVO.getQ1Db());
							rptVO.setQ2Amt(rptVO.getQ2Db());
							rptVO.setQ3Amt(rptVO.getQ3Db());
							rptVO.setQ4Amt(rptVO.getQ4Db());
							expenseList.add(rptVO);
						}
						
					}else if(rptVO.getRootGroupID()==4){
						if(rptVO.getTxType().equalsIgnoreCase("D")){
						//rptVO.setTotalAmount(rptVO.getCreditBalance());
						rptVO.setAprAmt(rptVO.getAprCr());
						rptVO.setMayAmt(rptVO.getMayCr());
						rptVO.setJunAmt(rptVO.getJunCr());
						rptVO.setJulAmt(rptVO.getJulCr());
						rptVO.setAugAmt(rptVO.getAugCr());
						rptVO.setSeptAmt(rptVO.getSeptCr());
						rptVO.setOctAmt(rptVO.getOctCr());
						rptVO.setNovAmt(rptVO.getNovCr());
						rptVO.setDecAmt(rptVO.getDecCr());
						rptVO.setJanAmt(rptVO.getJanCr());
						rptVO.setFebAmt(rptVO.getFebCr());
						rptVO.setMarAmt(rptVO.getMarCr());
						rptVO.setQ1Amt(rptVO.getQ1Cr());
						rptVO.setQ2Amt(rptVO.getQ2Cr());
						rptVO.setQ3Amt(rptVO.getQ3Cr());
						rptVO.setQ4Amt(rptVO.getQ4Cr());
						expenseList.add(rptVO);
					}else{
					//	rptVO.setTotalAmount(rptVO.getDebitBalance());
						rptVO.setAprAmt(rptVO.getAprDb());
						rptVO.setMayAmt(rptVO.getMayDb());
						rptVO.setJunAmt(rptVO.getJunDb());
						rptVO.setJulAmt(rptVO.getJulDb());
						rptVO.setAugAmt(rptVO.getAugDb());
						rptVO.setSeptAmt(rptVO.getSeptDb());
						rptVO.setOctAmt(rptVO.getOctDb());
						rptVO.setNovAmt(rptVO.getNovDb());
						rptVO.setDecAmt(rptVO.getDecDb());
						rptVO.setJanAmt(rptVO.getJanDb());
						rptVO.setFebAmt(rptVO.getFebDb());
						rptVO.setMarAmt(rptVO.getMarDb());
						rptVO.setQ1Amt(rptVO.getQ1Db());
						rptVO.setQ2Amt(rptVO.getQ2Db());
						rptVO.setQ3Amt(rptVO.getQ3Db());
						rptVO.setQ4Amt(rptVO.getQ4Db());
						incomeList.add(rptVO);
					}
					
					
					
				}
				
				}
			}
			reportVO.setIncomeTxList(incomeList);
			reportVO.setExpenseTxList(expenseList);
			
			
		}catch(Exception e){
			log.error("Exception : private ReportVO processMonthlyCashBasedReceiptPaymentObject(List rcptPymtList)"+e);
		}
		
		log.debug("Entry : private ReportVO processMonthlyCashBasedReceiptPaymentObject(List rcptPymtList)");
		
		return reportVO;
	}

	public ReportVO getProfitLossBalance(ReportVO reportVO,String rptType,String rptFrmt){
		
		log.debug("Entry : public ReportVO getProfitLossBalance(ReportVO reportVO)");
			

			BigDecimal incomeTotal = BigDecimal.ZERO;
			BigDecimal prevIncTotal=BigDecimal.ZERO;
			BigDecimal expenseTotal =  BigDecimal.ZERO;
			BigDecimal prevExpTotal=BigDecimal.ZERO;
			BigDecimal incomeTempTotal = BigDecimal.ZERO;
			BigDecimal expenseTempTotal =  BigDecimal.ZERO;
			BigDecimal prevIncTempTotal=BigDecimal.ZERO;
			BigDecimal prevExpTempTotal=BigDecimal.ZERO;
			String condition="before";
			log.debug("Income : "+incomeTotal);
			try {
				 //total income
				 for(int i=0;reportVO.getIncomeTxList().size()>i;i++){
						ReportDetailsVO incomeVO=(ReportDetailsVO) reportVO.getIncomeTxList().get(i);
						incomeTotal=incomeTotal.add(incomeVO.getTotalAmount());
						if(incomeVO.getPrevTotalAmount()!=null)
							prevIncTotal=prevIncTotal.add(incomeVO.getPrevTotalAmount());
						
				 }


				 //total expense
				 for(int i=0;reportVO.getExpenseTxList().size()>i;i++){
					 ReportDetailsVO expenseVO=(ReportDetailsVO) reportVO.getExpenseTxList().get(i);
						expenseTotal=expenseTotal.add(expenseVO.getTotalAmount());
						if(expenseVO.getPrevTotalAmount()!=null)
							prevExpTotal=prevExpTotal.add(expenseVO.getPrevTotalAmount());
				 }
				log.debug("Total Income: "+incomeTotal+" Total Expense: "+expenseTotal);
				int resultCur=incomeTotal.compareTo(expenseTotal);
				if(resultCur==1){				
					expenseTempTotal=incomeTotal.subtract(expenseTotal);				
				}else{
					incomeTempTotal=expenseTotal.subtract(incomeTotal);				
				}
				
				int resultPrev=prevIncTotal.compareTo(prevExpTotal);
				if(resultPrev==1){
					prevExpTempTotal=prevIncTotal.subtract(prevExpTotal);
				}else{
					prevIncTempTotal=prevExpTotal.subtract(prevIncTotal);
				}
				
				if(rptFrmt.equalsIgnoreCase("I")){
					condition="after";
				}
				
				if(!(incomeTempTotal.toString().equalsIgnoreCase("0"))){
					if(rptType.equalsIgnoreCase("IE")){
					reportVO.setExcessType("Excess of Expenditure Over Income");
					}else if(rptType.equalsIgnoreCase("PL")){
					reportVO.setExcessType("Net Loss "+condition+" tax");
				}else if(rptType.equalsIgnoreCase("CF"))
						reportVO.setExcessType("Net cash out flow");
					
					reportVO.setExcessAmount(incomeTempTotal);
					reportVO.setExpOverInc(incomeTempTotal);
					
				}else if(!(expenseTempTotal.toString().equalsIgnoreCase("0"))){
					if(rptType.equalsIgnoreCase("IE")){
						reportVO.setExcessType("Excess of Income Over Expenditure");
						}else if(rptType.equalsIgnoreCase("PL")){
						reportVO.setExcessType("Net Profit "+condition+" tax");
				}else if(rptType.equalsIgnoreCase("CF"))
					reportVO.setExcessType("Net cash in flow");
					
					reportVO.setExcessAmount(expenseTempTotal);
					reportVO.setIncOverExp(expenseTempTotal);
				}
				reportVO.setIncomeTotal(incomeTotal);
				reportVO.setExpenseTotal(expenseTotal);
				
				if(!(prevIncTempTotal.toString().equalsIgnoreCase("0"))){
					if(rptType.equalsIgnoreCase("IE")){
					reportVO.setPrevExcessType("Excess of Expenditure Over Income");
					}else if(rptType.equalsIgnoreCase("PL")){
					reportVO.setPrevExcessType("Net Loss "+condition+" tax");
				}else if(rptType.equalsIgnoreCase("CF"))
						reportVO.setPrevExcessType("Net cash out flow");
					
					reportVO.setPrevExcessAmount(prevIncTempTotal);
					reportVO.setPrevExpOverInc(prevIncTempTotal);
					
				}else if(!(prevExpTempTotal.toString().equalsIgnoreCase("0"))){
					if(rptType.equalsIgnoreCase("IE")){
						reportVO.setPrevExcessType("Excess of Income Over Expenditure");
						}else if(rptType.equalsIgnoreCase("PL")){
						reportVO.setPrevExcessType("Net Profit "+condition+" tax");
				}else if(rptType.equalsIgnoreCase("CF"))
					reportVO.setPrevExcessType("Net cash in flow");
					
					reportVO.setPrevExcessAmount(prevExpTempTotal);
					reportVO.setPrevIncOverExp(prevExpTempTotal);
				}
				
				reportVO.setPrevIncTotal(prevIncTotal);
				reportVO.setPrevExpTotal(prevExpTotal);
				log.debug("Expense: "+expenseTempTotal+" Income: "+incomeTempTotal+" Type: "+reportVO.getExcessType()+" Amount Expense: "+reportVO.getExcessAmount());
				
			} catch (Exception e) {
				log.error("Exception occured while preparing getProfitLossBalance Report "+e);
			}
				
			
			log.debug("Exit : public ReportVO getProfitLossBalance(ReportVO reportVO)");

			return reportVO;
		}
	
	public List getMonthWiseCashBasedCostCenterReportForChart(int societyID,String fromDate,String uptoDate,String type, int isConsolidated,String projectName)
  	{   
  	   List groupNameList=new ArrayList();
  		try
  		{
  			log.debug("Entry : public List getMonthWiseCashBasedCostCenterReportForChart(int intSocietyID,int isConsolidated)");
  			
  			groupNameList=projectDetailsDAO.getMonthWiseCashBasedCostCenterReportForChart(societyID,fromDate,uptoDate,type,isConsolidated,projectName);
  		    
  		    log.debug("Exit : public List getMonthWiseCashBasedCostCenterReportForChart(int intSocietyID,int isConsolidated)");
  		}
  		catch(Exception ex)
  		{
  			//ex.printStackTrace();
  			log.error("Exception in getMonthWiseReceiptPaymentGroupReportForChart : "+ex);
  		}
  	
  		return groupNameList;
  		
  	}
	
	private MonthwiseChartVO convertMonthlyChartObject(List chartList,String type){
   	 log.debug("Entry : private MonthwiseChartVO convertMonthlyChartObject(List chartList)");
   	 MonthwiseChartVO outputObject=new MonthwiseChartVO();
   	 String txType="";
   	 String rootGroupName="";
   	 String groupName="";
   	 BigDecimal aprBal=BigDecimal.ZERO;
   	 BigDecimal mayBal=BigDecimal.ZERO;
   	 BigDecimal junBal=BigDecimal.ZERO;
   	 BigDecimal julBal=BigDecimal.ZERO;
   	 BigDecimal augBal=BigDecimal.ZERO;
   	 BigDecimal sepBal=BigDecimal.ZERO;
   	 BigDecimal octBal=BigDecimal.ZERO;
   	 BigDecimal novBal=BigDecimal.ZERO;
   	 BigDecimal decBal=BigDecimal.ZERO;
   	 BigDecimal janBal=BigDecimal.ZERO;
   	 BigDecimal febBal=BigDecimal.ZERO;
   	 BigDecimal marBal=BigDecimal.ZERO;
   	 
   	 if(chartList.size()>0){
   		 
   		 for(int i=0;chartList.size()>i;i++){
   			 MonthwiseChartVO monthVO=(MonthwiseChartVO) chartList.get(i);
   			 outputObject.setTxType(monthVO.getTxType());
   			 outputObject.setRootGroupName(monthVO.getRootGroupName());
   			 outputObject.setGroupName(monthVO.getGroupName());
   			 aprBal=aprBal.add(monthVO.getAprAmt()).setScale(2,RoundingMode.HALF_UP);
   			 mayBal=mayBal.add(monthVO.getMayAmt()).setScale(2,RoundingMode.HALF_UP);
   			 junBal=junBal.add(monthVO.getJunAmt()).setScale(2,RoundingMode.HALF_UP);
   			 julBal=julBal.add(monthVO.getJulAmt()).setScale(2,RoundingMode.HALF_UP);
   			 augBal=augBal.add(monthVO.getAugAmt()).setScale(2,RoundingMode.HALF_UP);
   			 sepBal=sepBal.add(monthVO.getSeptAmt()).setScale(2,RoundingMode.HALF_UP);
   			 octBal=octBal.add(monthVO.getOctAmt()).setScale(2,RoundingMode.HALF_UP);
   			 novBal=novBal.add(monthVO.getNovAmt()).setScale(2,RoundingMode.HALF_UP);
   			 decBal=decBal.add(monthVO.getDecAmt()).setScale(2,RoundingMode.HALF_UP);
   			 janBal=janBal.add(monthVO.getJanAmt()).setScale(2,RoundingMode.HALF_UP);
   			 febBal=febBal.add(monthVO.getFebAmt()).setScale(2,RoundingMode.HALF_UP);
   			 marBal=marBal.add(monthVO.getMarAmt()).setScale(2,RoundingMode.HALF_UP);
   			 outputObject.setAprAmt(aprBal);
   			 outputObject.setMayAmt(mayBal);
   			 outputObject.setJunAmt(junBal);
   			 outputObject.setJulAmt(julBal);
   			 outputObject.setAugAmt(augBal);
   			 outputObject.setSeptAmt(sepBal);
   			 outputObject.setOctAmt(octBal);
   			 outputObject.setNovAmt(novBal);
   			 outputObject.setDecAmt(decBal);
   			 outputObject.setJanAmt(janBal);
   			 outputObject.setFebAmt(febBal);
   			 outputObject.setMarAmt(marBal);
   			 
   		 }
   		 
   		 
   	 }else{
   		 	 outputObject.setTxType(type);
   		 	 outputObject.setAprAmt(aprBal);
			 outputObject.setMayAmt(mayBal);
			 outputObject.setJunAmt(junBal);
			 outputObject.setJulAmt(julBal);
			 outputObject.setAugAmt(augBal);
			 outputObject.setSeptAmt(sepBal);
			 outputObject.setOctAmt(octBal);
			 outputObject.setNovAmt(novBal);
			 outputObject.setDecAmt(decBal);
			 outputObject.setJanAmt(janBal);
			 outputObject.setFebAmt(febBal);
			 outputObject.setMarAmt(marBal);
			 
   	 }
   	 
   	 
   	 
   	 log.debug("Exit : private MonthwiseChartVO convertMonthlyChartObject(List chartList)");
   	 return outputObject;
    }
	
	public MonthwiseChartVO getCashBasedCostCenterCategoryReport(int societyID,String fromDate,String uptoDate,String rptType,int isConsolidated,int ccCategoryID)
  	{   
  	   MonthwiseChartVO reportVO=new MonthwiseChartVO();
  	  ReportVO monthlyObj=new ReportVO();
	  ReportVO quaterlyObj=new ReportVO();
  		try
  		{
  			log.debug("Entry : getCashBasedCostCenterCategoryReport(int societyID,String fromDate,String uptoDate,String rptType,int isConsolidated,String projectName)");
  			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
  			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
  			java.util.Date parse =  sdf.parse(fromDate);
  			Calendar c = Calendar.getInstance();
  			c.setTime(parse);
  			int month=c.get(Calendar.MONTH+1);
  			int year=c.get(Calendar.YEAR);
  			List categoryList=projectDetailsDAO.getCostCeneterCategoryLinkingList(ccCategoryID);
  			//Fill year object
  			reportVO.setYearObj(getCashBasedCostCenterCategoryDetailsReport(societyID,year+"-04-01" ,sdf.format(dateUtil.getMonthLastDate(3,(year+1))),rptType,isConsolidated,categoryList));
  			
  			//Fill monthwise breakup object
  			List monthwiseRcptPymtList=projectDetailsDAO.getMonthWiseCashBasedCostCenterCategoryReportForChart(societyID,year+"-04-01" ,sdf.format(dateUtil.getMonthLastDate(3,(year+1))),"G",isConsolidated,categoryList);
  			ReportVO mntRcptVo=processMonthlyCashBasedReceiptPaymentObject(monthwiseRcptPymtList);
  			List incomeMonthwiseList=mntRcptVo.getIncomeTxList();
		    List expenseMonthwiseList=mntRcptVo.getExpenseTxList();
		    monthlyObj.setIncomeTxList(incomeMonthwiseList);
		    monthlyObj.setExpenseTxList(expenseMonthwiseList);
		    reportVO.setMonthlyObj(monthlyObj);
 			
		   //Fill quaterwise breakup object
		    List qtrWiseList=projectDetailsDAO.getQuaterWiseCashBasedCostCenterCategoryReportForChart(societyID,year+"-04-01" ,sdf.format(dateUtil.getMonthLastDate(3,(year+1))),"G",categoryList);
  			ReportVO qtrWiseVO=processMonthlyCashBasedReceiptPaymentObject(qtrWiseList);
		    List incomeQuaterwiseList=qtrWiseVO.getIncomeTxList();
		    List expenseQuaterwiseList=qtrWiseVO.getExpenseTxList();
		    quaterlyObj.setIncomeTxList(incomeQuaterwiseList);
		    quaterlyObj.setExpenseTxList(expenseQuaterwiseList);
		    reportVO.setQuaterlyObj(quaterlyObj);
		    		   
 			// Fill chart object
		    List chartList=new ArrayList<>();
		    chartList.add(convertMonthlyChartObject(incomeMonthwiseList,"C"));
		    chartList.add(convertMonthlyChartObject(expenseMonthwiseList,"D"));
		    reportVO.setChartList(chartList);
 			
  			
  			
  			
  		    
  		    log.debug("Exit : getCashBasedCostCenterCategoryReport(int societyID,String fromDate,String uptoDate,String rptType,int isConsolidated,String projectName)");
  		}
  		catch(Exception ex)
  		{
  			//ex.printStackTrace();
  			log.error("Exception in getCashBasedCostCenterCategoryReport(int societyID,String fromDate,String uptoDate,String rptType,int isConsolidated,String projectName) : "+ex);
  		}
  	
  		return reportVO;
  		
  	}
	
	public ReportVO getCashBasedCostCenterCategoryDetailsReport(int societyID,String fromDate,String uptoDate,String rptType,int isConsolidated,List categoryList){
		ReportVO rptVO =new ReportVO();
		SocietyVO societyVO=new SocietyVO();
		String convertedFrmDate=null;
		String convertedToDate=null;
		log.debug("Entry : public ReportVO getCashBasedCostCenterCategoryDetailsReport(int societyID,String fromDate,String uptoDate,int isConsolidated)"+societyID);
		
		try {

			List rcptPymtList=projectDetailsDAO.getCashBasedCostCenterCategoryReport(fromDate, uptoDate, societyID, isConsolidated,categoryList);
		    ReportVO rcptVo=processCashBasedReceiptPaymentObject(rcptPymtList);
		    List incomeList=rcptVo.getIncomeTxList();
			List expeList=rcptVo.getExpenseTxList();		
			List rcptPymtDtList=projectDetailsDAO.getCashBasedCostCenterCategoryDetailsReport(fromDate, uptoDate, societyID, isConsolidated, categoryList);
			ReportVO rcptDetVO=processCashBasedReceiptPaymentObject(rcptPymtDtList);
			List incomeDetailList=rcptDetVO.getIncomeTxList();
		    List expenseDetailList=rcptDetVO.getExpenseTxList();
		    
			societyVO= societyServiceBean.getSocietyDetails("0", societyID);
				
			/*	for(int i=0;cashInHandList.size()>i;i++){
					RptMonthYearVO achVO=(RptMonthYearVO) cashInHandList.get(i);
					bankBalanceList.add(achVO);
					
				}*/
				rptVO.setSocietyVO(societyVO);
				log.debug("Society Name: "+societyVO.getSocietyName()+" Address: "+societyVO.getAddress());
				
			   
				rptVO.setIncomeTxList(incomeList);
				rptVO.setExpenseTxList(expeList);
				//rptVO.setAccountBalanceList(bankBalanceList);
				rptVO.setIncomeDetailsTxList(incomeDetailList);
				rptVO.setExpenseDetailsTxList(expenseDetailList);
				rptVO=this.getProfitLossBalance(rptVO,rptType,"S");
				String type = rptVO.getExcessType();
				if(type.equalsIgnoreCase("Excess of Expenditure Over Income")){
					rptVO.setExcessType("Excess of Payment Over Receipt");
				}else if(type.equalsIgnoreCase("Excess of Income Over Expenditure")){
					rptVO.setExcessType("Excess of Receipt Over Payment");
				}
				ReportVO dateVO=dateUtil.getReportDate(fromDate, uptoDate);
				
				rptVO.setRptFromDate(dateVO.getFromDate());
				rptVO.setRptUptoDate(dateVO.getUptDate());
				rptVO.setFromDate(fromDate);
				rptVO.setUptDate(uptoDate);
				
				
				
		        log.debug("Converted From Date Format: "+rptVO.getFromDate()+" To date: "+ rptVO.getUptDate());
		} catch (NullPointerException ex) {
			log.info("getCashBasedCostCenterCategoryDetailsReport No details found for societyID:  "+societyID);

		} catch (Exception e) {
			log.error("Exception occured while preparing getCashBasedCostCenterCategoryDetailsReport Report "+e);
		}
			
		
		log.debug("Exit : public ReportVO getCashBasedCostCenterCategoryDetailsReport(int societyID,String fromDate,String uptoDate,int isConsolidated)");
		return rptVO;
	}
	
	public List getCashBasedCostCenterLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType,int isConsolidated,String projectName){
		List accHeadListList=new ArrayList();
		
		try{
		log.debug("Entry : public List getCashBasedCostCenterLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType,int isConsolidated,String projectName)"+fromDate+uptoDate);
		
		accHeadListList=projectDetailsDAO.getCashBasedCostCenterLedgerReport(societyID, fromDate, uptoDate, groupID,txType,isConsolidated,projectName);
		accHeadListList=processCashBasedLedgerObject(accHeadListList);
			
		log.debug("Exit : public List getCashBasedCostCenterLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType,int isConsolidated,String projectName)"+accHeadListList.size());
		}catch (Exception e) {
			log.error("Exception: public List getCashBasedCostCenterLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType,int isConsolidated,String projectName) "+e );
		}
		return accHeadListList;
	}
	
	public List getCashBasedCostCenterCategoryLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType,int isConsolidated,int ccCategoryID){
		List accHeadListList=new ArrayList();
		
		try{
		log.debug("Entry : public List getCashBasedCostCenterCategoryLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType,int isConsolidated,int ccCategoryID)"+fromDate+uptoDate);
		List categoryList=projectDetailsDAO.getCostCeneterCategoryLinkingList(ccCategoryID);
		accHeadListList=projectDetailsDAO.getCashBasedCostCenterCategoryLedgerReport(societyID, fromDate, uptoDate, groupID,txType,isConsolidated,categoryList);
		accHeadListList=processCashBasedLedgerObject(accHeadListList);
			
		log.debug("Exit : public List getCashBasedCostCenterCategoryLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType,int isConsolidated,int ccCategoryID)"+accHeadListList.size());
		}catch (Exception e) {
			log.error("Exception: public List getCashBasedCostCenterCategoryLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType,int isConsolidated,int ccCategoryID) "+e );
		}
		return accHeadListList;
	}
	
	private List processCashBasedLedgerObject(List rcptPymtList){
		log.debug("Entry : private ReportVO processCashBasedLedgerObject(List rcptPymtList)");
		ReportVO reportVO=new ReportVO();
		
		try{
			if(rcptPymtList.size()>0){
				
				for(int i=0;rcptPymtList.size()>i;i++){
					AccountHeadVO rptVO=(AccountHeadVO) rcptPymtList.get(i);
					
					if(rptVO.getRootID()==1){
						if(rptVO.getAccType().equalsIgnoreCase("C")){
							rptVO.setClosingBalance(rptVO.getDrClsBal());
							
						}else{
							rptVO.setClosingBalance(rptVO.getCrClsBal());
							
						}
						
					}else if(rptVO.getRootID()==2){
						if(rptVO.getAccType().equalsIgnoreCase("D")){
							rptVO.setClosingBalance(rptVO.getDrClsBal());
							
						}else{
							rptVO.setClosingBalance(rptVO.getCrClsBal());
							
						}
					}else if(rptVO.getRootID()==3){
						if(rptVO.getAccType().equalsIgnoreCase("C")){
							rptVO.setClosingBalance(rptVO.getCrClsBal());
							
						}else{
							rptVO.setClosingBalance(rptVO.getDrClsBal());
							
						}
						
					}else if(rptVO.getRootID()==4){
						if(rptVO.getAccType().equalsIgnoreCase("D")){
						rptVO.setClosingBalance(rptVO.getCrClsBal());
						
					}else{
						rptVO.setClosingBalance(rptVO.getDrClsBal());
						
					}
						
					}
					
					
					
					
					
				}
				
				
			}
			
			
			
		}catch(Exception e){
			log.error("Exception : private ReportVO processCashBasedLedgerObject(List rcptPymtList)"+e);
		}
		
		log.debug("Entry : private ReportVO processCashBasedLedgerObject(List rcptPymtList)");
		
		return rcptPymtList;
	}
	
	public MonthwiseChartVO getMonthWiseBreakUpReportForReceivablesForCostCenters(int societyID,String fromDate,String uptoDate,String projectName)
  	{   
  	   MonthwiseChartVO reportVO=new MonthwiseChartVO();
  	   ReportVO monthlyObj=new ReportVO();
  	   ReportVO quaterlyObj=new ReportVO();
  		try
  		{
  			log.debug("Entry : public List getMonthWiseBreakUpReportForReceivablesForCostCenters(int intSocietyID)");
  			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
  			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
  			java.util.Date parse =  sdf.parse(fromDate);
  			Calendar c = Calendar.getInstance();
  			c.setTime(parse);
  			int month=c.get(Calendar.MONTH+1);
  			int year=c.get(Calendar.YEAR);
  			
  			
  			//Fill monthwise breakup object
  			List monthlyList=processReceivablesList(projectDetailsDAO.getMonthWiseBreakUpReportForReceivablesForCostCenters(societyID,year+"-04-01" ,sdf.format(dateUtil.getMonthLastDate(3,(year+1))),"I",projectName));
  			
  			monthlyObj.setIncomeTxList(monthlyList);
  			reportVO.setMonthlyObj(monthlyObj);
  			
  			reportVO.setChartList(processReceivablesList(projectDetailsDAO.getMonthWiseBreakUpReportForReceivablesForCostCenters(societyID,year+"-04-01" ,sdf.format(dateUtil.getMonthLastDate(3,(year+1))),"G",projectName)));
  		    
  		    log.debug("Exit : public List getMonthWiseBreakUpReportForReceivablesForCostCenters(int intSocietyID)");
  		}
  		catch(Exception ex)
  		{
  			//ex.printStackTrace();
  			log.error("Exception in getMonthWiseBreakUpReportForReceivables : "+ex);
  		}
  	
  		return reportVO;
  		
  	}
	
	private List processReceivablesList(List monthWiseReceivablesList){
   	 BigDecimal baseOpBal=BigDecimal.ZERO;
   	 for(int i=0;monthWiseReceivablesList.size()>i;i++){
   		 MonthwiseChartVO monthwiseChartVO=(MonthwiseChartVO) monthWiseReceivablesList.get(i);
   		 if(monthwiseChartVO.getOpeningBalance()!=null)
   		  monthwiseChartVO.setAprAmt(monthwiseChartVO.getAprAmt().add(monthwiseChartVO.getOpeningBalance()));
   		 if(monthwiseChartVO.getAprAmt().compareTo(BigDecimal.ZERO)<0){
   			 monthwiseChartVO.setAprAmt(BigDecimal.ZERO);
   		 }
   		  monthwiseChartVO.setMayAmt(monthwiseChartVO.getMayAmt().add(monthwiseChartVO.getAprAmt()));
   		  if(monthwiseChartVO.getMayAmt().compareTo(BigDecimal.ZERO)<0){
    			 monthwiseChartVO.setMayAmt(BigDecimal.ZERO);
    		 }
   		  monthwiseChartVO.setJunAmt(monthwiseChartVO.getJunAmt().add(monthwiseChartVO.getMayAmt()));
   		  if(monthwiseChartVO.getJunAmt().compareTo(BigDecimal.ZERO)<0){
     			 monthwiseChartVO.setJunAmt(BigDecimal.ZERO);
     		 }
   		  monthwiseChartVO.setJulAmt(monthwiseChartVO.getJulAmt().add(monthwiseChartVO.getJunAmt()));
   		  if(monthwiseChartVO.getJulAmt().compareTo(BigDecimal.ZERO)<0){
     			 monthwiseChartVO.setJulAmt(BigDecimal.ZERO);
     		 }
   		  monthwiseChartVO.setAugAmt(monthwiseChartVO.getAugAmt().add(monthwiseChartVO.getJulAmt()));
   		  if(monthwiseChartVO.getAugAmt().compareTo(BigDecimal.ZERO)<0){
     			 monthwiseChartVO.setAugAmt(BigDecimal.ZERO);
     		 }
   		  monthwiseChartVO.setSeptAmt(monthwiseChartVO.getSeptAmt().add(monthwiseChartVO.getAugAmt()));
   		  if(monthwiseChartVO.getSeptAmt().compareTo(BigDecimal.ZERO)<0){
     			 monthwiseChartVO.setSeptAmt(BigDecimal.ZERO);
     		 }
   		  monthwiseChartVO.setOctAmt(monthwiseChartVO.getOctAmt().add(monthwiseChartVO.getSeptAmt()));
   		  if(monthwiseChartVO.getOctAmt().compareTo(BigDecimal.ZERO)<0){
     			 monthwiseChartVO.setOctAmt(BigDecimal.ZERO);
     		 }
   		  monthwiseChartVO.setNovAmt(monthwiseChartVO.getNovAmt().add(monthwiseChartVO.getOctAmt()));
   		  if(monthwiseChartVO.getNovAmt().compareTo(BigDecimal.ZERO)<0){
     			 monthwiseChartVO.setNovAmt(BigDecimal.ZERO);
     		 }
   		  monthwiseChartVO.setDecAmt(monthwiseChartVO.getDecAmt().add(monthwiseChartVO.getNovAmt()));
   		  if(monthwiseChartVO.getDecAmt().compareTo(BigDecimal.ZERO)<0){
     			 monthwiseChartVO.setDecAmt(BigDecimal.ZERO);
     		 }
   		  monthwiseChartVO.setJanAmt(monthwiseChartVO.getJanAmt().add(monthwiseChartVO.getDecAmt()));
   		  if(monthwiseChartVO.getJanAmt().compareTo(BigDecimal.ZERO)<0){
     			 monthwiseChartVO.setJanAmt(BigDecimal.ZERO);
     		 }
   		  monthwiseChartVO.setFebAmt(monthwiseChartVO.getFebAmt().add(monthwiseChartVO.getJanAmt()));
   		  if(monthwiseChartVO.getFebAmt().compareTo(BigDecimal.ZERO)<0){
     			 monthwiseChartVO.setFebAmt(BigDecimal.ZERO);
     		 }
   		  monthwiseChartVO.setMarAmt(monthwiseChartVO.getMarAmt().add(monthwiseChartVO.getFebAmt()));
   		  if(monthwiseChartVO.getMarAmt().compareTo(BigDecimal.ZERO)<0){
     			 monthwiseChartVO.setMarAmt(BigDecimal.ZERO);
     		 }
   	 }
   	 
   	 return monthWiseReceivablesList;
    }
	
	
	public List getReceivableReportByCostCenter(int societyID,String fromDate, String uptoDate,String projectName){
		List ledgerList=new ArrayList();
		List accHeadListList=new ArrayList();
		
		try{
		log.debug("Entry : public List getReceivableReportByCostCenter(int societyID,String fromDate, String uptoDate,String projectName)"+fromDate+uptoDate);
		
		accHeadListList=projectDetailsDAO.getReceivableReportByCostCenter(societyID, fromDate, uptoDate, projectName);
		
		for(int i=0;accHeadListList.size()>i;i++){
			AccountHeadVO achVO=(AccountHeadVO) accHeadListList.get(i);
			
			achVO=ledgerService.unsignedOpeningClosingBalance(achVO);
			
			ledgerList.add(achVO);
		}
		
				
		log.debug("Exit : public List getReceivableReportByCostCenter(int societyID,String fromDate, String uptoDate,String projectName)"+ledgerList.size());
		}catch (Exception e) {
			log.error("Exception: public List getReceivableReportByCostCenter(int societyID,String fromDate, String uptoDate,String projectName) "+e );
		}
		return ledgerList;
	}
	
	
	public MonthwiseChartVO getMonthWiseBreakUpReportForReceivablesForCostCategory(int societyID,String fromDate,String uptoDate,int ccCategoryID)
  	{   
  	   MonthwiseChartVO reportVO=new MonthwiseChartVO();
  	   ReportVO monthlyObj=new ReportVO();
  	   ReportVO quaterlyObj=new ReportVO();
  		try
  		{
  			log.debug("Entry : public List getMonthWiseBreakUpReportForReceivablesForCostCenters(int intSocietyID)");
  			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
  			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
  			java.util.Date parse =  sdf.parse(fromDate);
  			Calendar c = Calendar.getInstance();
  			c.setTime(parse);
  			int month=c.get(Calendar.MONTH+1);
  			int year=c.get(Calendar.YEAR);
  			
  			List categoryList=projectDetailsDAO.getCostCeneterCategoryLinkingList(ccCategoryID);
  			//Fill monthwise breakup object
  			List monthlyList=processReceivablesList(projectDetailsDAO.getMonthWiseBreakUpReportForReceivablesForCostCategory(societyID,year+"-04-01" ,sdf.format(dateUtil.getMonthLastDate(3,(year+1))),"I",categoryList));
  			
  			monthlyObj.setIncomeTxList(monthlyList);
  			reportVO.setMonthlyObj(monthlyObj);
  			
  			reportVO.setChartList(processReceivablesList(projectDetailsDAO.getMonthWiseBreakUpReportForReceivablesForCostCategory(societyID,year+"-04-01" ,sdf.format(dateUtil.getMonthLastDate(3,(year+1))),"G",categoryList)));
  		    
  		    log.debug("Exit : public List getMonthWiseBreakUpReportForReceivablesForCostCenters(int intSocietyID)");
  		}
  		catch(Exception ex)
  		{
  			//ex.printStackTrace();
  			log.error("Exception in getMonthWiseBreakUpReportForReceivables : "+ex);
  		}
  	
  		return reportVO;
  		
  	}
	
	public List getReceivableReportByCostCategory(int societyID,String fromDate, String uptoDate,int ccCategoryID){
		List ledgerList=new ArrayList();
		List accHeadListList=new ArrayList();
		
		try{
		log.debug("Entry : public List getReceivableReportByCostCategory(int societyID,String fromDate, String uptoDate,String projectName)"+fromDate+uptoDate);
		
		List categoryList=projectDetailsDAO.getCostCeneterCategoryLinkingList(ccCategoryID);
		
		accHeadListList=projectDetailsDAO.getReceivableReportByCostCategory(societyID, fromDate, uptoDate, categoryList);
		
		for(int i=0;accHeadListList.size()>i;i++){
			AccountHeadVO achVO=(AccountHeadVO) accHeadListList.get(i);
			
			achVO=ledgerService.unsignedOpeningClosingBalance(achVO);
			
			ledgerList.add(achVO);
		}
		
				
		log.debug("Exit : public List getReceivableReportByCostCategory(int societyID,String fromDate, String uptoDate,String projectName)"+ledgerList.size());
		}catch (Exception e) {
			log.error("Exception: public List getReceivableReportByCostCategory(int societyID,String fromDate, String uptoDate,String projectName) "+e );
		}
		return ledgerList;
	}
	
	
	/**
	 * @param projectDAO the projectDAO to set
	 */
	public void setProjectDetailsDAO(ProjectDetailsDAO projectDetailsDAO) {
		this.projectDetailsDAO = projectDetailsDAO;
	}

	/**
	 * @param societyServiceBean the societyServiceBean to set
	 */
	public void setSocietyServiceBean(SocietyService societyServiceBean) {
		this.societyServiceBean = societyServiceBean;
	}

	/**
	 * @param ledgerService the ledgerService to set
	 */
	public void setLedgerService(LedgerService ledgerService) {
		this.ledgerService = ledgerService;
	}

	
	
}
