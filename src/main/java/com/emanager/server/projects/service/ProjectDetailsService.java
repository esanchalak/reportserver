package com.emanager.server.projects.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.financialReports.valueObject.MonthwiseChartVO;
import com.emanager.server.projects.domain.ProjectDetailsDomain;
import com.emanager.server.projects.valueObjects.ProjectDetailsVO;

public class ProjectDetailsService {

	private static final Logger log=Logger.getLogger(ProjectDetailsService.class);
	ProjectDetailsDomain projectDetailsDomain;
	
	
	//-----Add Project Details -------//
		public int addProjectDetails(ProjectDetailsVO projectVO){
			log.debug("Entry : public int addProjectDetails(ProjectDetailsVO projectVO)");
			int success=0;
			try{
				
				success=projectDetailsDomain.addProjectDetails(projectVO);			
				
			}catch(Exception e){
				log.error("Exception in adding project details "+e);
			}
			
			
			log.debug("Exit : public int addProjectDetails(ProjectDetailsVO projectVO)");
			return success;
		}
		
		//-----Update Project Details -------//
			public int updateProjectDetails(ProjectDetailsVO projectVO){
				log.debug("Entry : public int updateProjectDetails(ProjectDetailsVO projectVO)");
				int success=0;
				try{
					
					success=projectDetailsDomain.updateProjectDetails(projectVO);			
					
				}catch(Exception e){
					log.error("Exception in updating project details "+e);
				}
				
				
				log.debug("Exit : public int updateProjectDetails(ProjectDetailsVO projectVO)");
				return success;
			}
		
			//-----Delete Project Details -------//
					public int deleteProjectDetails(ProjectDetailsVO projectVO){
						log.debug("Entry : public int deleteProjectDetails(ProjectDetailsVO projectVO)");
						int success=0;
						try{
							
							success=projectDetailsDomain.deleteProjectDetails(projectVO);			
							
						}catch(Exception e){
							log.error("Exception in deleting project details "+e);
						}
						
						
						log.debug("Exit : public int deleteProjectDetails(ProjectDetailsVO projectVO)");
						return success;
					}
	
					//-----Add Project Item Details -------//
					public int addProjectItemDetails(ProjectDetailsVO projectVO){
						log.debug("Entry : public int addProjectItemDetails(ProjectDetailsVO projectVO)");
						int success=0;
						try{
							
							success=projectDetailsDomain.addProjectItemDetails(projectVO);			
							
						}catch(Exception e){
							log.error("Exception in adding Project details "+e);
						}
						
						
						log.debug("Exit : public int addProjectItemDetails(ProjectDetailsVO projectVO)");
						return success;
					}
					
					//-----Add bulk Project Item Details -------//
					public int addBulkProjectItemDetails(List projectList){
						log.debug("Entry : public int addBulkProjectItemDetails(List projectList)");
						int success=0;
						try{
							
							success=projectDetailsDomain.addBulkProjectItemDetails(projectList);			
							
						}catch(Exception e){
							log.error("Exception in adding Project details "+e);
						}
						
						
						log.debug("Exit : public int addBulkProjectItemDetails(List budgetList)");
						return success;
					}
					
					//-----Update Project Item Details -------//
						public int updateProjectItemDetails(ProjectDetailsVO projectVO){
							log.debug("Entry : public int updateProjectItemDetails(ProjectDetailsVO projectVO)");
							int success=0;
							try{
								
								success=projectDetailsDomain.updateProjectItemDetails(projectVO);			
								
							}catch(Exception e){
								log.error("Exception in updating Project details "+e);
							}
							
							
							log.debug("Exit : public int updateProjectItemDetails(ProjectDetailsVO projectVO)");
							return success;
						}
					
						//-----Delete Project Item Details -------//
						public int deleteProjectItemDetails(ProjectDetailsVO projectVO){
									log.debug("Entry : public int deleteProjectItemDetails(ProjectDetailsVO ProjectVO)");
									int success=0;
									try{
										
										success=projectDetailsDomain.deleteProjectItemDetails(projectVO);			
										
									}catch(Exception e){
										log.error("Exception in deleting Project details "+e);
									}
									
									
									log.debug("Exit : public int deleteProjectItemDetails(ProjectDetailsVO Project)");
									return success;
								}
					
	public List getProjectDetailsList(int orgID){
		log.debug("Entry :  public List getProjectDetailsList(int orgID)");
		List projectList=null;
		
		projectList=projectDetailsDomain.getProjectDetailsList(orgID);
		
		
		log.debug("Exit : public List getProjectDetailsList(int orgID)");
		return projectList;
		
	}
	
	public List getProjectDetailsListTagWise(ProjectDetailsVO projectVO){
		log.debug("Entry :  public List getProjectDetailsList(ProjectDetailsVO projectVO)");
		List projectList=null;
		
		projectList=projectDetailsDomain.getProjectDetailsListTagWise(projectVO);
		
		
		log.debug("Exit : public List getProjectDetailsList(ProjectDetailsVO projectVO)");
		return projectList;
		
	}
	
	public List getProjectDetailsListWithDetails(int orgID,String fromDate,String toDate){
		log.debug("Entry :  public List getProjectDetailsListWithDetails(int orgID,String fromDate,String toDate)");
		List projectList=null;
		
		projectList=projectDetailsDomain.getProjectDetailsListWithDetails(orgID,fromDate,toDate);
		
		log.debug("Exit : public List getProjectDetailsListWithDetails(int orgID,String fromDate,String toDate)");
		return projectList;
		
	}
	
	public List getProjectItemList(int orgID,int projectID){
		log.debug("Entry :  public List getProjectItemList(int orgID,int projectID)");
		List projectList=null;
		
		projectList=projectDetailsDomain.getProjectItemList(orgID,projectID);
		
		
		log.debug("Exit : public List getProjectDetailsList(int orgID,int projectID)");
		return projectList;
		
	}
	
	public ProjectDetailsVO getProjectDetails(int orgID,int projectID, String fromDate, String toDate){
		log.debug("Entry : public ProjectDetailsVO getProjectDetails(int orgID,int projectID, String fromDate, String toDate)");
		ProjectDetailsVO projectVO=new ProjectDetailsVO();
		
		projectVO=projectDetailsDomain.getProjectDetails(orgID,projectID, fromDate,  toDate);
		
		
		log.debug("Exit : public ProjectDetailsVO getProjectDetails(int orgID,int projectID, String fromDate, String toDate)");
		return projectVO;
		
	}
	
	//========= New cost center list ===================//
	public List getProjectListWithTagWise(int orgID,String type){
		log.debug("Entry :  public List getProjectListWithTagWise(int orgID)");
		List projectList=null;
		
		projectList=projectDetailsDomain.getProjectListWithTagWise(orgID,type);
		
		log.debug("Exit : public List getProjectListWithTagWise(int orgID)");
		return projectList;
		
	}
	
	
	//========= New cost center list ===================//
	public List getProjectDetailsListWithDetailsTagWise(int orgID,String fromDate,String toDate){
		log.debug("Entry :  public List getProjectDetailsListWithDetailsTagWise(int orgID,String fromDate,String toDate)");
		List projectList=null;
		
		projectList=projectDetailsDomain.getProjectDetailsListWithDetailsTagWise(orgID,fromDate,toDate);
		
		log.debug("Exit : public List getProjectDetailsListWithDetailsTagWise(int orgID,String fromDate,String toDate)");
		return projectList;
		
	}
	
	//========= New cost center Summary list ===================//
		public List getProjectDetailsSummaryListWithDetailsTagWise(int orgID,String fromDate,String toDate){
			log.debug("Entry :  public List getProjectDetailsSummaryListWithDetailsTagWise(int orgID,String fromDate,String toDate)");
			List projectList=null;
			
			projectList=projectDetailsDomain.getProjectDetailsSummaryListWithDetailsTagWise(orgID,fromDate,toDate);
			
			log.debug("Exit : public List getProjectDetailsSummaryListWithDetailsTagWise(int orgID,String fromDate,String toDate)");
			return projectList;
			
		}
	
	//========= New cost center Item list ===================//
	public List getProjectItemListTagWise(int orgID,String projectName){
		log.debug("Entry :  public List getProjectItemListTagWise(int orgID,String projectName)");
		List projectList=null;
		
		projectList=projectDetailsDomain.getProjectItemListTagWise(orgID,projectName);
		
		log.debug("Exit : public List getProjectItemListTagWise(int orgID,String projectName)");
		return projectList;
		
	}
	
	//========= New cost center details tag wise ===================//
	public ProjectDetailsVO getProjectDetailsTagWise(int orgID,String projectName, String fromDate, String toDate,int rootID){
		log.debug("Entry : public ProjectDetailsVO getProjectDetailsTagWise(int orgID,String projectName, String fromDate, String toDate)");
		ProjectDetailsVO projectVO=new ProjectDetailsVO();
		
		projectVO=projectDetailsDomain.getProjectDetailsTagWise(orgID, projectName, fromDate, toDate,rootID);
		
		
		log.debug("Exit : public ProjectDetailsVO getProjectDetailsTagWise(int orgID,String projectName, String fromDate, String toDate)");
		return projectVO;
		
	}
	
	
		public ProjectDetailsVO getProjectDetailsCategoryWise(int orgID,int categoryID, String fromDate, String toDate,int rootID){
			log.debug("Entry : public ProjectDetailsVO getProjectDetailsTagWise(int orgID,int categoryID, String fromDate, String toDate)");
			ProjectDetailsVO projectVO=new ProjectDetailsVO();
			
			projectVO=projectDetailsDomain.getProjectDetailsCategoryWise(orgID, categoryID, fromDate, toDate,rootID);
			
			
			log.debug("Exit : public ProjectDetailsVO getProjectDetailsTagWise(int orgID,int categoryID, String fromDate, String toDate)");
			return projectVO;
			
		}
	
	public List getCostCeneterCategoryList(int orgID){
		log.debug("Entry : public List getCostCeneterCategoryList(int orgID)");
		List projectList=null;
		
		try {
			
			
		projectList=projectDetailsDomain.getCostCeneterCategoryList(orgID);	
			
	
		} catch (Exception e) {
			
			log.error("Exception : public int getCostCeneterCategoryList "+e);
		}
		
		log.debug("Exit : public List getCostCeneterCategoryList(int orgID)");
		return projectList;
		
	}
	
	public List getCostCeneterCategoryLinkingList(int categoryID){
		log.debug("Entry : public List getCostCeneterCategoryLinkingList(int categoryID)");
		List projectList=null;
		
		try {
			
			projectList=projectDetailsDomain.getCostCeneterCategoryLinkingList(categoryID);
	
		} catch (Exception e) {
			
			log.error("Exception : public int getCostCeneterCategoryLinkingList "+e);
		}
		
		log.debug("Exit : public List getCostCeneterCategoryLinkingList(int orgID)");
		return projectList;
		
	}
	
	public MonthwiseChartVO getCashBasedCostCenterReport(int societyID,String fromDate,String uptoDate,String rptType,String projectName)
  	{   
  	   MonthwiseChartVO reportVO=null;
  		try
  		{
  			log.debug("Entry : public List getCashBasedCostCenterReport(int intSocietyID)");
  			
  			reportVO=projectDetailsDomain.getCashBasedCostCenterReport(societyID,fromDate,uptoDate,rptType,0,projectName);
  		    
  		    log.debug("Exit : public List getCashBasedCostCenterReport(int intSocietyID)");
  		}
  		catch(Exception ex)
  		{
  			//ex.printStackTrace();
  			log.error("Exception in getCashBasedCostCenterReport : "+ex);
  		}
  	
  		return reportVO;
  		
  	}
	
	public MonthwiseChartVO getCashBasedCostCenterCategoryReport(int societyID,String fromDate,String uptoDate,String rptType,int ccCategoryID)
  	{   
  	   MonthwiseChartVO reportVO=null;
  		try
  		{
  			log.debug("Entry : public List getCashBasedCostCenterCategoryReport(int intSocietyID)");
  			
  			reportVO=projectDetailsDomain.getCashBasedCostCenterCategoryReport(societyID,fromDate,uptoDate,rptType,0,ccCategoryID);
  		    
  		    log.debug("Exit : public List getCashBasedCostCenterCategoryReport(int intSocietyID)");
  		}
  		catch(Exception ex)
  		{
  			//ex.printStackTrace();
  			log.error("Exception in getCashBasedCostCenterReport : "+ex);
  		}
  	
  		return reportVO;
  		
  	}
	
	public List getCashBasedCostCenterLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType,int isConsolidated,String projectName){
		List accHeadListList=new ArrayList();
		
		try{
		log.debug("Entry : public List getCashBasedCostCenterLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType,int isConsolidated,String projectName)"+fromDate+uptoDate);
		
		accHeadListList=projectDetailsDomain.getCashBasedCostCenterLedgerReport(societyID, fromDate, uptoDate, groupID,txType,isConsolidated,projectName);
					
		log.debug("Exit : public List getCashBasedCostCenterLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType,int isConsolidated,String projectName)"+accHeadListList.size());
		}catch (Exception e) {
			log.error("Exception: public List getCashBasedCostCenterLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType,int isConsolidated,String projectName) "+e );
		}
		return accHeadListList;
	}
	
	public List getCashBasedCostCenterCategoryLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType,int isConsolidated,int ccCategoryID){
		List accHeadListList=new ArrayList();
		
		try{
		log.debug("Entry : public List getCashBasedCostCenterCategoryLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType,int isConsolidated,int ccCategoryID)"+fromDate+uptoDate);
		
		accHeadListList=projectDetailsDomain.getCashBasedCostCenterCategoryLedgerReport(societyID, fromDate, uptoDate, groupID,txType,isConsolidated,ccCategoryID);
					
		log.debug("Exit : public List getCashBasedCostCenterCategoryLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType,int isConsolidated,int ccCategoryID)"+accHeadListList.size());
		}catch (Exception e) {
			log.error("Exception: public List getCashBasedCostCenterCategoryLedgerReport(int societyID,String fromDate, String uptoDate,int groupID,String txType,int isConsolidated,int ccCategoryID) "+e );
		}
		return accHeadListList;
	}
	
	public MonthwiseChartVO getMonthWiseBreakUpReportForReceivablesForCostCenters(int societyID,String fromDate,String uptoDate,String projectAccronyms)
  	{   
  	   MonthwiseChartVO reportVO=null;
  		
  			log.debug("Entry : public List getMonthWiseBreakUpReportForReceivablesForCostCenters(int intSocietyID)");
  			
  			reportVO=projectDetailsDomain.getMonthWiseBreakUpReportForReceivablesForCostCenters(societyID,fromDate,uptoDate,projectAccronyms);
  		    
  		    log.debug("Exit : public List getMonthWiseBreakUpReportForReceivablesForCostCenters(int intSocietyID)");
  		
  	
  		return reportVO;
  		
  	}
	
	
	public List getReceivableReportByCostCenter(int societyID,String fromDate, String uptoDate,String projectName){
		List ledgerList=null;
		List accHeadListList=new ArrayList();
		try{
		log.debug("Entry : public List getReceivableReportByCostCenter(String societyID,String fromDate, String uptoDate,String projectName)");
		
		accHeadListList=projectDetailsDomain.getReceivableReportByCostCenter(societyID, fromDate, uptoDate, projectName);
		
		
		log.debug("Exit : public List getReceivableReportByCostCenter(String societyID,String fromDate, String uptoDate,String projectName)");
		}catch (Exception e) {
			log.error("Exception: public List getReceivableReportByCostCenter(String societyID,String fromDate, String uptoDate,String projectName) "+e );
		}
		return accHeadListList;
	}
	
	public MonthwiseChartVO getMonthWiseBreakUpReportForReceivablesForCostCategory(int societyID,String fromDate,String uptoDate,int ccCategoryID)
  	{   
  	   MonthwiseChartVO reportVO=null;
  		
  			log.debug("Entry : public MonthwiseChartVO getMonthWiseBreakUpReportForReceivablesForCostCategory(int societyID,String fromDate,String uptoDate,int ccCategoryID)");
  			
  			reportVO=projectDetailsDomain.getMonthWiseBreakUpReportForReceivablesForCostCategory(societyID, fromDate, uptoDate, ccCategoryID);
  		    
  		    log.debug("Exit : public MonthwiseChartVO getMonthWiseBreakUpReportForReceivablesForCostCategory(int societyID,String fromDate,String uptoDate,int ccCategoryID)");
  		
  	
  		return reportVO;
  		
  	}
	
	public List getReceivableReportByCostCategory(int societyID,String fromDate, String uptoDate,int ccCategoryID){
		List ledgerList=null;
		List accHeadListList=new ArrayList();
		try{
		log.debug("Entry : public List getReceivableReportByCostCategory(String societyID,String fromDate, String uptoDate,String projectName)");
		
		accHeadListList=projectDetailsDomain.getReceivableReportByCostCategory(societyID, fromDate, uptoDate, ccCategoryID);
		
		
		log.debug("Exit : public List getReceivableReportByCostCategory(String societyID,String fromDate, String uptoDate,String projectName)");
		}catch (Exception e) {
			log.error("Exception: public List getReceivableReportByCostCategory(String societyID,String fromDate, String uptoDate,String projectName) "+e );
		}
		return accHeadListList;
	}
	
	/**
	 * @param projectDetailsDomain the projectDetailsDomain to set
	 */
	public void setProjectDetailsDomain(ProjectDetailsDomain projectDetailsDomain) {
		this.projectDetailsDomain = projectDetailsDomain;
	}


	


	
	
	
}
