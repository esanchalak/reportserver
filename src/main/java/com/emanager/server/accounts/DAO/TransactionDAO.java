package com.emanager.server.accounts.DAO;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.BankStatement;
import com.emanager.server.accounts.DataAccessObjects.ChargesVO;
import com.emanager.server.accounts.DataAccessObjects.LedgerEntries;
import com.emanager.server.accounts.DataAccessObjects.OnlinePaymentGatewayVO;
import com.emanager.server.accounts.DataAccessObjects.PenaltyChargesVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionMetaVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.DataAccessObjects.TxCounterVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceVO;
import com.emanager.server.projects.valueObjects.ProjectDetailsVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;



public class TransactionDAO {
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	private JdbcTemplate jdbcTemplate;
	SocietyService societyService;
	Logger log=Logger.getLogger(TransactionDAO.class);
	 
	

	
	/* Used for insering transaction and receiving tx_id for inserting ledger_entries */
	public int createTransaction(final TransactionVO transaction,int orgID)  {
		// TODO Auto-generated method stub
		log.debug("Entry :public int createTransaction(final TransactionVO transaction)");
		int key = 0;
		
		try{
			   SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			   java.util.Date today = new java.util.Date();
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			   transaction.setCreateDate(currentTimestamp);
			   transaction.setUpdatedDate(currentTimestamp);
			   if(transaction.getAmount().equals(BigDecimal.ZERO)){
				   log.debug(" Transaction amount is zero ");
			   }else{
			   String SQL = "INSERT INTO account_transactions_"+societyVO.getDataZoneID()+" " +
			   		"(tx_number,invoice_id,tx_mode ,tx_type,tx_date,bank_date,doc_id,amount,ref_no,description,create_date,update_date,created_by,updated_by,amt_in_word,is_deleted,org_id,tx_tags)  " +
			                "VALUES (:transactionNumber, :invoiceID,:transactionMode, :transactionType, :transactionDate, :bankDate, :docID, :amount, :refNumber, :description, :createDate, :updatedDate,:createdBy ,:updatedBy,:amtInWord,:isDeleted,"+orgID+",:txTags)";
			   SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(transaction);
			   KeyHolder keyHolder = new GeneratedKeyHolder();
			   getNamedParameterJdbcTemplate().update(SQL, fileParameters, keyHolder);
			   key=keyHolder.getKey().intValue();
			   
			   }	   
			   
			   log.debug("Exit :public int createTransaction(final TransactionVO transaction)"+key); 
			}catch(Exception se){
				log.error("Exception in public int createTransaction(final TransactionVO transaction)"+se);
			   
			}
		return key;
	}
	
	
	/* Used for inserting  ledger_entries */
	public int createSingleLedgerEntries(LedgerEntries ledgerEntry,int txID,int orgID)  {
		// TODO Auto-generated method stub
		int success=0;
		
		log.debug("Entry :public int createSingleLedgerEntries(LedgerEntries ledgerEntry,int txID)");
		try{
			   SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			   java.util.Date today = new java.util.Date();
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			   
			   
			   String str = "INSERT INTO account_ledger_entry_"+societyVO.getDataZoneID()+" "+					
				   "(ledger_id,tx_id,inv_line_item_id,TYPE,amount,int_amount,balance,description,insert_date,org_id,tx_le_tags)  " +
	                "VALUES (:ledgerID,:txID,:invLineItemID,:type,:amount,:intAmount,:balance,:description,:insertDate,:orgID,:txLeTags)";
			   
			   
			   Map hMap=new HashMap();
			   hMap.put("ledgerID", ledgerEntry.getLedgerID());
			   hMap.put("txID", txID);
			   hMap.put("type", ledgerEntry.getCreditDebitFlag());
			   hMap.put("amount", ledgerEntry.getAmount());
			   hMap.put("intAmount", ledgerEntry.getIntAmount());
			   hMap.put("balance", ledgerEntry.getBalance());
			   hMap.put("description", ledgerEntry.getStrComments());
			   hMap.put("insertDate", currentTimestamp);
			   hMap.put("orgID", orgID);
			   hMap.put("txLeTags", ledgerEntry.getLedgerTags());
			   hMap.put("invLineItemID", ledgerEntry.getInvLineItemID());
			   
			   success=namedParameterJdbcTemplate.update(str, hMap);
			   log.debug("Exit :public int createSingleLedgerEntries(LedgerEntries ledgerEntry,int txID)"+success);
			}catch(Exception se){
				log.debug("Exception :public int createSingleLedgerEntries(LedgerEntries ledgerEntry,int txID) "+se);
			   success=0;
			}
		return success;
	}
	
	
	
	/* Used to get ledgerEntries related for transaction */
	public List getLedgersEntryList(TransactionVO transactionVO,String societyID)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public List getLedgersEntryList(TransactionVO transactionVO)");
		List ledgerEntryList=null;
		int success=0;
	
		try{
			 int orgID=Integer.parseInt(societyID);
			 SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			   String str = "SELECT al.*,group_name,root_group_name,cr_action,dr_action,ale.*,ale.id AS LEID,ale.type as txType ,CONCAT(IFNULL(ale.tx_le_tags,''),IFNULL(al.project_tags,'')) AS tx_le_tag FROM account_ledgers_"+societyVO.getDataZoneID()+" al,account_ledger_entry_"+societyVO.getDataZoneID()+" ale, account_groups ag,account_root_group asg "+
			   				" WHERE al.sub_group_id=ag.id AND ag.root_id=asg.root_id AND ale.ledger_id=al.id and ale.org_id=al.org_id and al.org_id=:orgID AND ale.tx_id=:txID ORDER BY ale.id; " ;
			   				
			   
			   Map hMap=new HashMap();
			   hMap.put("txID", transactionVO.getTransactionID());
			   hMap.put("orgID", orgID);
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						LedgerEntries txVO=new LedgerEntries();
						txVO.setItemID(rs.getInt("LEID"));
						txVO.setGroupID(rs.getInt("sub_group_id"));
						txVO.setLedgerID(rs.getInt("ledger_id"));
						txVO.setInvLineItemID(rs.getInt("inv_line_item_id"));
						txVO.setAmount(rs.getBigDecimal("amount"));
						txVO.setLedgerName(rs.getString("ledger_name"));
						txVO.setBalance(rs.getBigDecimal("closing_balance"));
						txVO.setCreditDebitFlag(rs.getString("txType"));
						txVO.setCreditAction(rs.getInt("cr_action"));
						txVO.setDebitAction(rs.getInt("dr_action"));
						txVO.setStrComments(rs.getString("description"));
						txVO.setTxID(rs.getInt("tx_id"));
						txVO.setInsertDate(rs.getTimestamp("insert_date"));
						txVO.setAmount(rs.getBigDecimal("amount"));
						txVO.setIntAmount(rs.getBigDecimal("int_amount"));
						txVO.setLedgerTags(rs.getString("tx_le_tag"));
						txVO.setProjectTags(rs.getString("project_tags"));
						if(rs.getString("txType").equalsIgnoreCase("C")){
						
							txVO.setCredit(txVO.getAmount().abs());
						
						}else{
							
							txVO.setDebit(txVO.getAmount().abs());
							
						}
						return txVO;
					}

				   };
			   ledgerEntryList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit : public List getLedgersEntryList(TransactionVO transactionVO)");
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at List getLedgersEntryList(TransactionVO transactionVO) "+Ex);
			
			}catch(Exception se){
				log.error("Exception : public List getLedgersEntryList(TransactionVO transactionVO) "+se);
			   success=0;
			}
		return ledgerEntryList;
	}
	
	/* Used for updating transaction */
	public int updateTransactionWithoutChangingAmt(TransactionVO transaction,int orgID,int txID)  {
		// TODO Auto-generated method stub
		int success=0;
		log.debug("Entry : public public int updateTransactionWithoutChangingAmt(TransactionVO transaction)");
		try{
			 SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			   java.util.Date today = new java.util.Date();
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			   if(transaction.getAmount().equals(BigDecimal.ZERO)){
				   log.debug(" Transaction amount is zero ");
			   }else{
			   String str = "UPDATE account_transactions_"+societyVO.getDataZoneID()+" SET tx_mode=:txMode," +
			   		" tx_type=:txType, doc_id=:docID, ref_no=:refNo, is_deleted=:isDeleted , description=:description, tx_date=:txDate,update_date=CURRENT_TIMESTAMP,updated_by=:updatedBy WHERE tx_id=:txID and org_id=:orgID ";				
				   
			   
			   Map hMap=new HashMap();
			   hMap.put("txID", txID);
			   hMap.put("txMode", transaction.getTransactionMode());
			   hMap.put("txNumber", transaction.getTransactionNumber());
			   hMap.put("txType", transaction.getTransactionType());
			   hMap.put("docID", transaction.getDocID());
			   hMap.put("refNo", transaction.getRefNumber());
			   hMap.put("updatedBy", transaction.getUpdatedBy());
			   hMap.put("description", transaction.getDescription());
			   hMap.put("txDate", transaction.getTransactionDate());
			   hMap.put("updateDate", currentTimestamp);
			   hMap.put("amount", transaction.getAmount());
			   hMap.put("amtInwords", transaction.getAmtInWord());
			   hMap.put("isDeleted", transaction.getIsDeleted());
			   hMap.put("orgID", orgID);
			   success=namedParameterJdbcTemplate.update(str, hMap);
			   }
			   log.debug("Exit : public public int updateTransactionWithoutChangingAmt(TransactionVO transaction)"+success+"  "+transaction.getUpdatedBy());
			}catch(Exception se){
				log.debug("Exceptuion in public public int updateTransactionWithoutChangingAmt(TransactionVO transaction) "+se);
			   success=0;
			}
		return success;
	}
	
	/* Used for updating transaction */
	public int updateTransaction(TransactionVO transaction,int societyID,int txID)  {
		// TODO Auto-generated method stub
		int success=0;
		log.debug("Entry : public public int updateTransaction(TransactionVO transaction)"+txID);
		try{
				SocietyVO societyVO=societyService.getSocietyDetails(societyID);
			   java.util.Date today = new java.util.Date();
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			   if(transaction.getAmount().signum()!=0){
			   
			   String str = "UPDATE account_transactions_"+societyVO.getDataZoneID()+" SET  tx_mode=:txMode," +
			   		" tx_type=:txType, doc_id=:docID, ref_no=:refNo,   description=:description, tx_date=:txDate,update_date=CURRENT_TIMESTAMP,amount=:amount,updated_by=:updatedBy,amt_in_word=:amtInwords WHERE tx_id=:txID  and org_id=:orgID ";				
			   log.debug("Query : "+str);	   
			   
			   Map hMap=new HashMap();
			   hMap.put("txID", txID);
			   hMap.put("txMode", transaction.getTransactionMode());
			   hMap.put("txNumber", transaction.getTransactionNumber());
			   hMap.put("txType", transaction.getTransactionType());
			   hMap.put("docID", transaction.getDocID());
			   hMap.put("refNo", transaction.getRefNumber());
			   hMap.put("updatedBy", transaction.getUpdatedBy());
			   hMap.put("description", transaction.getDescription());
			   hMap.put("txDate", transaction.getTransactionDate());
			   hMap.put("updateDate", currentTimestamp);
			   hMap.put("amount", transaction.getAmount());
			   hMap.put("amtInwords", transaction.getAmtInWord());
			   hMap.put("orgID", transaction.getSocietyID());
			   
			   success=namedParameterJdbcTemplate.update(str, hMap);
			   }
			   log.debug("Exit : public public int updateTransaction(TransactionVO transaction)"+success+"  "+transaction.getUpdatedBy());
			}catch(Exception se){
				log.debug("Exceptuion in public public int updateTransaction(TransactionVO transaction) "+se);
			   success=0;
			}
		return success;
	}
	
	public int deleteLedgerEntry(LedgerEntries ledgerEntry,String societyID) {
		int success=1;
		
		log.debug("Entry :public int deleteLedgerEntry(LedgerEntries ledgerEntry,String societyID)");
		 int orgID=Integer.parseInt(societyID);
		 SocietyVO societyVO=societyService.getSocietyDetails(orgID);
		   String str = "Delete from account_ledger_entry_"+societyVO.getDataZoneID()+" where id="+ledgerEntry.getItemID()+"  and org_id=:orgID;";
		 
		   
		   Map hMap=new HashMap();
		   hMap.put("ID", ledgerEntry.getItemID());
		   hMap.put("orgID",ledgerEntry.getOrgID());
		   
		   
		   success=namedParameterJdbcTemplate.update(str, hMap);
		
		
		log.debug("Exit :public int deleteLedgerEntry(LedgerEntries ledgerEntry,String societyID)");
		return success;
	}

	public int deleteLedgerEntries(int txID,int orgID) {
		int success=1;
		
		log.debug("Entry :public int deleteLedgerEntry(LedgerEntries ledgerEntry,String societyID)");
		 SocietyVO societyVO=societyService.getSocietyDetails(orgID);
		 
		   String str = "Delete from account_ledger_entry_"+societyVO.getDataZoneID()+" where tx_id=:txID and org_id=:orgID ;";
		   
		   
		   Map hMap=new HashMap();
		   hMap.put("txID", txID);
		   hMap.put("orgID", orgID);
		   
		   
		   success=namedParameterJdbcTemplate.update(str, hMap);
		
		
		log.debug("Exit :public int deleteLedgerEntry(LedgerEntries ledgerEntry,String societyID)");
		return success;
	}
	
	
	
	/* Used for updating balances of ledger entries */
	public int updateAllLedgersEntries(LedgerEntries ledgerEntry,String societyID)  {
		// TODO Auto-generated method stub
		int success=0;
		log.debug("Entry : public int updateAllLedgersEntries(LedgerEntries ledgerEntry)");
		try{
			int orgID=Integer.parseInt(societyID);
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String	strSQL = "update account_ledger_entry_"+societyVO.getDataZoneID()+" set balance=balance+(:updatedBalance) WHERE tx_id>:deletedTxID and ledger_id=:ledgerID and insert_date>:insertDate and org_id=:orgID ;" ;
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("deletedTxID", ledgerEntry.getTxID());
			namedParameters.put("updatedBalance", ledgerEntry.getUpdatedBalance());
			namedParameters.put("ledgerID", ledgerEntry.getLedgerID());
			namedParameters.put("insertDate", ledgerEntry.getInsertDate());
			namedParameters.put("orgID", ledgerEntry.getOrgID());
			
			success = namedParameterJdbcTemplate.update(strSQL,namedParameters);
			
			log.debug("Exit : public int updateAllLedgersEntries(LedgerEntries ledgerEntry)");
			 
			}catch(Exception se){
			log.debug("Exception in public int updateAllLedgersEntries(LedgerEntries ledgerEntry) "+se);
			   success=0;
			}
		return success;
	}
	
	
	

	/* Used for deleting transactions i.e. adding is_deleted flag */
	public int deleteTransaction(TransactionVO transaction,int orgID) {
		int success=0;
		 java.util.Date today = new java.util.Date();
		 final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
		log.debug("Entry : public int deleteTransaction(TransactionVO transaction)");
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String	strSQL = "update account_transactions_"+societyVO.getDataZoneID()+" set is_deleted=1,description=:notes,update_date=:time, updated_by=:updatedBy WHERE tx_id=:txID and org_id=:orgID;" ;
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("txID", transaction.getTransactionID());
			namedParameters.put("notes", transaction.getDescription());
			namedParameters.put("time", currentTimestamp);
			namedParameters.put("updatedBy", transaction.getUpdatedBy());
			namedParameters.put("orgID", orgID);
			success = namedParameterJdbcTemplate.update(strSQL,namedParameters);
			
			log.debug("Exit : public int deleteTransaction(TransactionVO transaction)"+success);	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug("Exception in public int deleteTransaction(TransactionVO transaction) "+e);
		}
		
		return success;
	}
	
	
	/* Used for deleting transactions i.e. adding is_deleted flag */
	public int changeTransactionStatus(TransactionVO transaction,int orgID) {
		int success=0;
		 java.util.Date today = new java.util.Date();
		 final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
		log.debug("Entry : public int changeTransactionStatus(TransactionVO transaction)"+transaction.getIsDeleted());
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String	strSQL = "update account_transactions_"+societyVO.getDataZoneID()+" set is_deleted=:statusID,description=:notes,update_date=:time, updated_by=:updatedBy WHERE tx_id=:txID and org_id=:orgID;" ;
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("txID", transaction.getTransactionID());
			namedParameters.put("notes", transaction.getDescription());
			namedParameters.put("time", currentTimestamp);
			namedParameters.put("updatedBy", transaction.getUpdatedBy());
			namedParameters.put("orgID", orgID);
			namedParameters.put("statusID",transaction.getIsDeleted());
			success = namedParameterJdbcTemplate.update(strSQL,namedParameters);
			
			log.debug("Exit : public int changeTransactionStatus(TransactionVO transaction)"+success);	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug("Exception in public int changeTransactionStatus(TransactionVO transaction) "+e);
		}
		
		return success;
	}
	
	/* Used for updating audit flag of  transactions i.e. adding is_audited flag */
	public int changeAuditedTransactionStatus(TransactionVO transaction,int orgID) {
		int success=0;
		 java.util.Date today = new java.util.Date();
		 final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
		log.debug("Entry : public int changeAuditedTransactionStatus(TransactionVO transaction)"+transaction.getIsDeleted());
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String	strSQL = "update account_transactions_"+societyVO.getDataZoneID()+" set is_audited=:statusID,update_date=:time, updated_by=:updatedBy WHERE tx_id=:txID and org_id=:orgID;" ;
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("txID", transaction.getTransactionID());
			namedParameters.put("notes", transaction.getDescription());
			namedParameters.put("time", currentTimestamp);
			namedParameters.put("updatedBy", transaction.getUpdatedBy());
			namedParameters.put("orgID", orgID);
			namedParameters.put("statusID",transaction.getIsAudited());
			success = namedParameterJdbcTemplate.update(strSQL,namedParameters);
			
			log.debug("Exit : public int changeAuditedTransactionStatus(TransactionVO transaction)"+success);	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug("Exception in public int changeAuditedTransactionStatus(TransactionVO transaction) "+e);
		}
		
		return success;
	}
	
	
	/* Used for deleting transactions i.e. adding is_deleted flag */
	public int deleteJournalEntries(InvoiceVO invoiceVO) {
		int success=0;
		log.debug("Entry : public int deleteJournalEntries(int invoiceID,String societyID)");
		try {
		    SocietyVO societyVO=societyService.getSocietyDetails(invoiceVO.getOrgID());
			String	strSQL = "update account_transactions_"+societyVO.getDataZoneID()+" set is_deleted=1, updated_by=:updatedBy WHERE invoice_id=:invoiceID  and org_id=:orgID;" ;
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("invoiceID", invoiceVO.getInvoiceID());
			namedParameters.put("orgID",invoiceVO.getOrgID());
			namedParameters.put("updatedBy", invoiceVO.getUpdatedBy());
			
			success = namedParameterJdbcTemplate.update(strSQL,namedParameters);
			
			log.debug("Exit : public int deleteJournalEntries(int invoiceID,String societyID)");	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug("Exception in public int deleteJournalEntries(int transactionNo,String societyID) "+e);
		}
		
		return success;
	}
	
	
	/* Used for updating report group  of transaction line item */
	public int updateReportGroupJournalEntry(TransactionVO transactionVO) {
		int success=0;
		log.debug("Entry : public int updateReportGroupJournalEntry(TransactionVO transactionVO)");
		  java.util.Date today = new java.util.Date();
		   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
		   String strClause="";
		 if(transactionVO.getTxLineItemID()==0){
			 strClause= "inv_line_item_id=:invLineItemID ";	
		 }else{
			 strClause= "id=:txLineItemID ";
		 }
		 
		try {
		    SocietyVO societyVO=societyService.getSocietyDetails(transactionVO.getSocietyID());
			String	strSQL = " Update account_ledger_entry_"+societyVO.getDataZoneID()+"  set tx_le_tags=:lineItemTag,insert_date=:insertDate WHERE "+strClause+";" ;
			
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("invLineItemID", transactionVO.getInvLineItemID());
			namedParameters.put("lineItemTag",transactionVO.getTxTags());
			namedParameters.put("txLineItemID",transactionVO.getTxLineItemID());
			namedParameters.put("insertDate", currentTimestamp.toString());
			
			success = namedParameterJdbcTemplate.update(strSQL,namedParameters);
			
			log.debug("Exit : public int updateReportGroupJournalEntry(TransactionVO transactionVO)"+success);	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug("Exception in public int updateReportGroupJournalEntry(TransactionVO transactionVO) "+e);
		}
		
		return success;
	}
	
	
	
	
	/* Used to get transactions related to ledger */
	public List getTransactionList(int orgID,int ledgerID,String fromDate,String uptoDate,String type)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public List getTransactionList(String societyID,String ledgerID,String fromDate,String uptoDate)"+ledgerID+""+fromDate+""+uptoDate+" "+type);
		List TransactionList=null;
		String strCondition="";
		if(!type.equalsIgnoreCase("All")){
		strCondition=" AND table_primary.type=:ledgerType "	;
		}
	
		try{
			 SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String str="Select txTable.*,s.tx_id,s.tag_values,s.accountant_comments,s.auditor_comments,SUM(amount) AS txAmount ,SUM(int_amount) AS int_amount  FROM (SELECT ats.org_id,ats.tx_id as TXID,ats.doc_id,ats.tx_date,ats.ref_no,ats.create_date,ats.update_date,ats.bank_date,ats.description,ats.tx_number,ats.tx_type,tx_mode,ats.created_by,ats.updated_by,m.full_name AS createdBy,mi.full_name AS updatedBy,table_primary.amount, table_primary.int_amount, table_primary.type, table_primary.tx_id,table_secondary.ledger_id,account_ledgers_"+societyVO.getDataZoneID()+".ledger_name ,sub_group_id,amt_in_word "+
					   " FROM account_ledger_entry_"+societyVO.getDataZoneID()+" table_primary,account_ledger_entry_"+societyVO.getDataZoneID()+" table_secondary,account_ledgers_"+societyVO.getDataZoneID()+",account_transactions_"+societyVO.getDataZoneID()+" ats,um_users m ,um_users mi "+
					   " WHERE table_primary.tx_id = table_secondary.tx_id "+
					   " AND table_primary.org_id=table_secondary.org_id "+
					   " AND table_primary.ledger_id=:ledgerID "+ 
					   " AND table_primary.type!=table_secondary.type "+
					   strCondition+
					   " AND account_ledgers_"+societyVO.getDataZoneID()+".id = table_secondary.ledger_id "+
					   " AND account_ledgers_"+societyVO.getDataZoneID()+".org_id=table_secondary.org_id  AND account_ledgers_"+societyVO.getDataZoneID()+".org_id=ats.org_id "+
					   " AND ats.tx_id = table_primary.tx_id AND (tx_date BETWEEN :fromDate AND :uptoDate) "+ 
				       " AND ats.is_deleted=0 AND m.id=ats.created_by AND mi.id=ats.updated_by and ats.org_id=:orgID GROUP BY table_secondary.tx_id ORDER BY ats.tx_date ) AS txTable LEFT JOIN soc_tx_meta s ON txTable.TXID=s.tx_id AND s.society_id=:orgID GROUP BY txTable.ledger_id,txTable.tx_id ORDER BY txTable.tx_date; ";
			
			/*String str="SELECT root.led_name,sub_group_id, ats.*,m.full_name AS createdBy,mi.full_name AS updatedBy,ale.amount AS amt,ale.id,ale.TYPE AS typ,ale.description AS des "+
					"FROM account_transactions_"+societyID+" ats,account_ledger_entry_"+societyID+" ale, member_details m , members_info mi, (SELECT al.ledger_name AS led_name,ale.tx_id,sub_group_id FROM account_ledgers_"+societyID+" al,account_ledger_entry_"+societyID+" ale " +
					" WHERE tx_ID IN (SELECT ats.tx_id FROM account_transactions_"+societyID+" ats,account_ledger_entry_"+societyID+" ale" +
					" WHERE ats.tx_id=ale.tx_id AND ale.ledger_id=:ledgerID AND (tx_date BETWEEN :fromDate AND :uptoDate )" +
					" AND is_deleted=0 "+strCondition+" ORDER BY ale.id ) AND ale.ledger_id !=:ledgerID AND al.id = ale.ledger_id GROUP BY tx_id)" +
					" AS root WHERE ats.tx_id=ale.tx_id AND root.tx_id = ats.tx_id AND ale.ledger_id=:ledgerID AND" +
					" (tx_date BETWEEN :fromDate AND :uptoDate) AND is_deleted=0 AND m.member_id=ats.created_by AND mi.member_id=ats.updated_by ORDER BY ats.tx_date ";*/

			 /* String str = "SELECT ats.*, sub_group_id,ledger_entries.* FROM account_transactions_"+societyID+" ats," +
			  		" account_ledgers_"+societyID+" l,(SELECT al.ledger_name,ale.tx_id,ledger_id,ale.amount AS amt,type as typ,ale.description AS des,ale.TYPE FROM account_ledgers_"+societyID+" al,account_ledger_entry_"+societyID+" ale "+
			  		" WHERE tx_ID IN (SELECT ats.tx_id FROM account_transactions_"+societyID+" ats,account_ledger_entry_"+societyID+" ale  WHERE ats.tx_id=ale.tx_id AND ale.ledger_id=:ledgerID AND (tx_date BETWEEN :fromDate AND :uptoDate ) AND is_deleted=0 ORDER BY ats.tx_date )"+
			  		" AND ale.ledger_id !=:ledgerID AND al.id = ale.ledger_id "+strCondition+" GROUP BY tx_id,ale.type) AS ledger_entries "+
			  		" WHERE ats.tx_id = ledger_entries.tx_ID AND ledger_entries.ledger_id=l.id GROUP BY ats.tx_id";*/
			   				 
			 /* String str = "SELECT ats.*,ale.amount AS amt,type as typ,ale.description AS des FROM account_transactions_"+societyID+" ats,account_ledger_entry_"+societyID+" ale "+
				"WHERE ats.tx_id=ale.tx_id AND ale.ledger_id=:ledgerID AND (tx_date BETWEEN :fromDate AND :uptoDate) "+strCondition+" and is_deleted=0 ORDER BY ats.tx_date;";*/
			log.debug("Query is "+str);
			   
			   Map hMap=new HashMap();
			   hMap.put("orgID", orgID);
			   hMap.put("ledgerID", ledgerID);
			   hMap.put("fromDate", fromDate);
			   hMap.put("uptoDate", uptoDate);
			   hMap.put("ledgerType", type);
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						List accheadVOList=null;
						TransactionVO txVO=new TransactionVO();
						txVO.setTransactionID(rs.getInt("tx_id"));
						txVO.setSocietyID(rs.getInt("org_id"));
						txVO.setCreateDate(rs.getTimestamp("create_date"));
						txVO.setUpdatedDate(rs.getTimestamp("update_date"));
						txVO.setTransactionDate(rs.getString("tx_date"));
						txVO.setBankDate(rs.getString("bank_date"));
						txVO.setDescription(rs.getString("description"));
						txVO.setDocID(rs.getString("doc_id"));
						txVO.setTransactionNumber(rs.getString("tx_number"));
						txVO.setRefNumber(rs.getString("ref_no"));
						txVO.setTransactionType(rs.getString("tx_type"));
						txVO.setTransactionMode(rs.getString("tx_mode"));
						txVO.setCreditDebitFlag(rs.getString("type"));
						txVO.setAmount(rs.getBigDecimal("amount"));
						txVO.setIntAmount(rs.getBigDecimal("int_amount"));
						txVO.setLedgerName(rs.getString("ledger_name"));
						txVO.setGroupID(rs.getInt("sub_group_id"));
						txVO.setCreatorName(rs.getString("createdBy"));
						txVO.setUpdaterName(rs.getString("updatedBy"));
						txVO.setAmtInWord(rs.getString("amt_in_word"));
						txVO.setHashComments(rs.getString("tag_values"));
						txVO.setAccComments(rs.getString("accountant_comments"));
						txVO.setAuditorComments(rs.getString("auditor_comments"));
						txVO.setLedgerID(rs.getInt("ledger_id"));
						if(rs.getString("type").equalsIgnoreCase("C")){
						
								txVO.setCreditAmt(txVO.getAmount().abs());
							
							}else{
								
									txVO.setDebitAmt(txVO.getAmount().abs());
							
							}
						
						
						return txVO;
						
					}

				   };
				   TransactionList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit :  public List getTransactionList(String societyID,String ledgerID,String fromDate,String uptoDate)"+TransactionList.size());
			}catch(EmptyResultDataAccessException Ex){
				log.info("No Data found at List getTransactionList "+Ex);
			
		
				}catch(Exception se){
				log.error("Exception : public List getTransactionList(String societyID,String ledgerID,String fromDate,String uptoDate)"+se);
			   
			}
		return TransactionList;
	}
	
	
	
	/* Used to get transactions related to ledger */
	public List getQuickTransactionList(int orgID,int ledgerID,String fromDate,String uptoDate,String type)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public List getQuickTransactionList(String societyID,String ledgerID,String fromDate,String uptoDate)"+ledgerID+""+fromDate+""+uptoDate+" "+type);
		List TransactionList=null;
		String strCondition="";
		if(!type.equalsIgnoreCase("All")){
		strCondition=" AND table_primary.type=:ledgerType "	;
		}
	
		try{
			 SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String str="Select txTable.*,SUM(amount) AS txAmount ,SUM(int_amount) AS int_amount  FROM (SELECT ats.org_id,ats.tx_id as TXID,ats.doc_id,ats.tx_date,ats.ref_no,ats.create_date,ats.update_date,ats.bank_date,ats.description,ats.tx_number,ats.tx_type,tx_mode,ats.created_by,ats.updated_by,m.full_name AS createdBy,mi.full_name AS updatedBy,table_primary.amount, table_primary.int_amount, table_primary.type, table_primary.tx_id,table_secondary.ledger_id,account_ledgers_"+societyVO.getDataZoneID()+".ledger_name ,sub_group_id,amt_in_word ,ats.tx_tags "+
					   " FROM (SELECT ledger_id,SUM(amount) AS amount,SUM(int_amount) AS int_amount,type,org_id,tx_id FROM account_ledger_entry_"+societyVO.getDataZoneID()+" WHERE org_id=:orgID GROUP BY ledger_id,tx_id) table_primary,account_ledger_entry_"+societyVO.getDataZoneID()+" table_secondary,account_ledgers_"+societyVO.getDataZoneID()+",account_transactions_"+societyVO.getDataZoneID()+" ats,um_users m ,um_users mi "+
					   " WHERE table_primary.tx_id = table_secondary.tx_id "+
					   " AND table_primary.org_id=table_secondary.org_id "+
					   " AND table_primary.ledger_id=:ledgerID "+ 
					   " AND table_primary.type!=table_secondary.type "+
					   strCondition+
					   " AND account_ledgers_"+societyVO.getDataZoneID()+".id = table_secondary.ledger_id "+
					   " AND account_ledgers_"+societyVO.getDataZoneID()+".org_id=table_secondary.org_id  AND account_ledgers_"+societyVO.getDataZoneID()+".org_id=ats.org_id "+
					   " AND ats.tx_id = table_primary.tx_id AND (tx_date BETWEEN :fromDate AND :uptoDate) "+ 
				       " AND ats.is_deleted=0 AND m.id=ats.created_by AND mi.id=ats.updated_by and ats.org_id=:orgID GROUP BY table_secondary.tx_id ORDER BY ats.tx_date ) AS txTable  GROUP BY txTable.ledger_id,txTable.tx_id ORDER BY txTable.tx_date; ";
			
			
			log.debug("Query is "+str);
			   
			   Map hMap=new HashMap();
			   hMap.put("orgID", orgID);
			   hMap.put("ledgerID", ledgerID);
			   hMap.put("fromDate", fromDate);
			   hMap.put("uptoDate", uptoDate);
			   hMap.put("ledgerType", type);
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						List accheadVOList=null;
						TransactionVO txVO=new TransactionVO();
						txVO.setTransactionID(rs.getInt("tx_id"));
						txVO.setSocietyID(rs.getInt("org_id"));
						txVO.setCreateDate(rs.getTimestamp("create_date"));
						txVO.setUpdatedDate(rs.getTimestamp("update_date"));
						txVO.setTransactionDate(rs.getString("tx_date"));
						txVO.setBankDate(rs.getString("bank_date"));
						txVO.setDescription(rs.getString("description"));
						txVO.setDocID(rs.getString("doc_id"));
						txVO.setTransactionNumber(rs.getString("tx_number"));
						txVO.setRefNumber(rs.getString("ref_no"));
						txVO.setTransactionType(rs.getString("tx_type"));
						txVO.setTransactionMode(rs.getString("tx_mode"));
						txVO.setCreditDebitFlag(rs.getString("type"));
						txVO.setAmount(rs.getBigDecimal("amount"));
						txVO.setIntAmount(rs.getBigDecimal("int_amount"));
						txVO.setLedgerName(rs.getString("ledger_name"));
						txVO.setGroupID(rs.getInt("sub_group_id"));
						txVO.setCreatorName(rs.getString("createdBy"));
						txVO.setUpdaterName(rs.getString("updatedBy"));
						txVO.setAmtInWord(rs.getString("amt_in_word"));
						txVO.setTxTags(rs.getString("tx_tags"));
						txVO.setLedgerID(rs.getInt("ledger_id"));
						if(rs.getString("type").equalsIgnoreCase("C")){
						
								txVO.setCreditAmt(txVO.getAmount().abs());
							
							}else{
								
									txVO.setDebitAmt(txVO.getAmount().abs());
							
							}
						
						
						return txVO;
						
					}

				   };
				   TransactionList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit :  public List getQuickTransactionList(String societyID,String ledgerID,String fromDate,String uptoDate)"+TransactionList.size());
			}catch(EmptyResultDataAccessException Ex){
				log.info("No Data found at List getTransactionList "+Ex);
			
		
				}catch(Exception se){
				log.error("Exception : public List getQuickTransactionList(String societyID,String ledgerID,String fromDate,String uptoDate)"+se);
			   
			}
		return TransactionList;
	}
	
	
	/* Used to get transactions reconcilation related to ledger */
	public List getReconcilationList(int orgID,int ledgerID,String fromDate,String uptoDate,String type)  {
    	log.debug("Entry : public List getReconcilationList(String societyID,String ledgerID,String fromDate,String uptoDate)"+ledgerID+""+fromDate+""+uptoDate+" "+type);
		List TransactionList=null;
		String strCondition="";
		String orderByCondition="";
		//RL: reconciled list , UL: unreconciled list
		if(type.equalsIgnoreCase("RL")){
		     strCondition=" AND (bank_date BETWEEN :fromDate AND :uptoDate) AND ats.bank_date IS NOT NULL  ";
		     orderByCondition= " txTable.bank_date ";
		}else if(type.equalsIgnoreCase("UL")){
			 strCondition="  AND (tx_date BETWEEN :fromDate AND :uptoDate) AND ats.bank_date IS NULL  ";
			 orderByCondition= " txTable.tx_date ";
		}
	
		try{
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String str="Select txTable.*,SUM(amount) AS txAmount ,SUM(int_amount) AS int_amount  FROM (SELECT ats.org_id,ats.tx_id as TXID,ats.doc_id,ats.tx_date,ats.ref_no,ats.create_date,ats.bank_date,ats.description,ats.tx_number,ats.tx_type,tx_mode,ats.created_by,ats.updated_by,m.full_name AS createdBy,mi.full_name AS updatedBy,table_primary.amount, table_primary.int_amount, table_primary.type, table_primary.tx_id,table_secondary.ledger_id,account_ledgers_"+societyVO.getDataZoneID()+".ledger_name ,sub_group_id,amt_in_word "+
					   " FROM account_ledger_entry_"+societyVO.getDataZoneID()+" table_primary,account_ledger_entry_"+societyVO.getDataZoneID()+" table_secondary,account_ledgers_"+societyVO.getDataZoneID()+",account_transactions_"+societyVO.getDataZoneID()+" ats,um_users m ,um_users mi "+
					   " WHERE table_primary.tx_id = table_secondary.tx_id"
					   + " AND table_primary.org_id=table_secondary.org_id  "+
					   " AND table_primary.ledger_id=:ledgerID "+ 
					   " AND table_primary.type!=table_secondary.type "+
					   " AND account_ledgers_"+societyVO.getDataZoneID()+".id = table_secondary.ledger_id"
					   	+" AND account_ledgers_"+societyVO.getDataZoneID()+".org_id=table_secondary.org_id  and account_ledgers_"+societyVO.getDataZoneID()+".org_id=ats.org_id "+
					   " AND ats.tx_id = table_primary.tx_id  "+ 
					   strCondition+
				       " AND ats.is_deleted=0 AND m.id=ats.created_by AND mi.id=ats.updated_by and ats.org_id=:orgID GROUP BY table_secondary.tx_id ORDER BY ats.tx_date ) AS txTable  GROUP BY txTable.ledger_id,txTable.tx_id ORDER BY "+orderByCondition ;
			
		
			log.debug("Query is "+str);
			   
			   Map hMap=new HashMap();
			   hMap.put("orgID", orgID);
			   hMap.put("ledgerID", ledgerID);
			   hMap.put("fromDate", fromDate);
			   hMap.put("uptoDate", uptoDate);
			   
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						List accheadVOList=null;
						TransactionVO txVO=new TransactionVO();
						txVO.setTransactionID(rs.getInt("TXID"));
						txVO.setSocietyID(rs.getInt("org_id"));
						txVO.setCreateDate(rs.getTimestamp("create_date"));
						txVO.setTransactionDate(rs.getString("tx_date"));
						txVO.setBankDate(rs.getString("bank_date"));
						txVO.setDescription(rs.getString("description"));
						txVO.setDocID(rs.getString("doc_id"));
						txVO.setTransactionNumber(rs.getString("tx_number"));
						txVO.setRefNumber(rs.getString("ref_no"));
						txVO.setTransactionType(rs.getString("tx_type"));
						txVO.setTransactionMode(rs.getString("tx_mode"));
						txVO.setCreditDebitFlag(rs.getString("type"));
						txVO.setAmount(rs.getBigDecimal("amount"));
						txVO.setIntAmount(rs.getBigDecimal("int_amount"));
						txVO.setCreatorName(rs.getString("createdBy"));
						txVO.setUpdaterName(rs.getString("updatedBy"));
						txVO.setLedgerName(rs.getString("ledger_name"));
						txVO.setGroupID(rs.getInt("sub_group_id"));
						txVO.setAmtInWord(rs.getString("amt_in_word"));
						
						if(rs.getString("type").equalsIgnoreCase("C")){
						
								txVO.setCreditAmt(txVO.getAmount().abs());
							
							}else{
								
								txVO.setDebitAmt(txVO.getAmount().abs());
							
							}
						
						
						return txVO;
						
					}

				   };
				   TransactionList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit :  public List getReconcilationList(String societyID,String ledgerID,String fromDate,String uptoDate)"+TransactionList.size());
			}catch(EmptyResultDataAccessException Ex){
				log.info("No Data found at List getReconcilationList "+Ex);
			
		
				}catch(Exception se){
				log.error("Exception : public List getReconcilationList(String societyID,String ledgerID,String fromDate,String uptoDate)"+se);
			   
			}
		return TransactionList;
	}
	
	/* Used to get transactions list of Project tags */
	public List getTransactionListOfProjects(int orgID,int ledgerID,String fromDate,String uptoDate,String projectName)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public List getTransactionListOfProjects(int orgID,int ledgerID,String fromDate,String uptoDate,String projectName)");
		List TransactionList=null;
		String strCondition="";
		
	
		try{
			 SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String str="Select txTable.*,SUM(amount) AS txAmount ,SUM(int_amount) AS int_amount  FROM (SELECT ats.org_id,ats.tx_id as TXID,ats.doc_id,ats.tx_date,ats.ref_no,ats.create_date,ats.bank_date,ats.description,ats.tx_number,ats.tx_type,tx_mode,ats.created_by,ats.updated_by,m.full_name AS createdBy,mi.full_name AS updatedBy,table_primary.amount, table_primary.int_amount, table_primary.type, table_primary.tx_id,table_secondary.ledger_id,account_ledgers_"+societyVO.getDataZoneID()+".ledger_name ,sub_group_id,amt_in_word ,ats.tx_tags "+
					   " FROM account_ledger_entry_"+societyVO.getDataZoneID()+" table_primary,account_ledger_entry_"+societyVO.getDataZoneID()+" table_secondary,account_ledgers_"+societyVO.getDataZoneID()+",account_transactions_"+societyVO.getDataZoneID()+" ats,um_users m ,um_users mi "+
					   " WHERE table_primary.tx_id = table_secondary.tx_id "+
					   " AND table_primary.org_id=table_secondary.org_id "+
					   " AND table_primary.ledger_id=:ledgerID "+ 
					   " AND table_primary.type!=table_secondary.type "+
					   strCondition+
					   " AND account_ledgers_"+societyVO.getDataZoneID()+".id = table_secondary.ledger_id "+
					   " AND account_ledgers_"+societyVO.getDataZoneID()+".org_id=table_secondary.org_id  AND account_ledgers_"+societyVO.getDataZoneID()+".org_id=ats.org_id "+
					   " AND ats.tx_id = table_primary.tx_id AND (tx_date BETWEEN :fromDate AND :uptoDate) "+ 
				       " AND ats.is_deleted=0 AND m.id=ats.created_by AND mi.id=ats.updated_by and ats.org_id=:orgID AND table_primary.tx_le_tags LIKE '%"+projectName+"%' GROUP BY table_secondary.tx_id ORDER BY ats.tx_date ) AS txTable  GROUP BY txTable.ledger_id,txTable.tx_id ORDER BY txTable.tx_date; ";
			
			
			log.debug("Query is "+str);
			   
			   Map hMap=new HashMap();
			   hMap.put("orgID", orgID);
			   hMap.put("ledgerID", ledgerID);
			   hMap.put("fromDate", fromDate);
			   hMap.put("uptoDate", uptoDate);
			   
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						List accheadVOList=null;
						TransactionVO txVO=new TransactionVO();
						txVO.setTransactionID(rs.getInt("tx_id"));
						txVO.setSocietyID(rs.getInt("org_id"));
						txVO.setCreateDate(rs.getTimestamp("create_date"));
						txVO.setTransactionDate(rs.getString("tx_date"));
						txVO.setBankDate(rs.getString("bank_date"));
						txVO.setDescription(rs.getString("description"));
						txVO.setDocID(rs.getString("doc_id"));
						txVO.setTransactionNumber(rs.getString("tx_number"));
						txVO.setRefNumber(rs.getString("ref_no"));
						txVO.setTransactionType(rs.getString("tx_type"));
						txVO.setTransactionMode(rs.getString("tx_mode"));
						txVO.setCreditDebitFlag(rs.getString("type"));
						txVO.setAmount(rs.getBigDecimal("amount"));
						txVO.setIntAmount(rs.getBigDecimal("int_amount"));
						txVO.setLedgerName(rs.getString("ledger_name"));
						txVO.setGroupID(rs.getInt("sub_group_id"));
						txVO.setCreatorName(rs.getString("createdBy"));
						txVO.setUpdaterName(rs.getString("updatedBy"));
						txVO.setAmtInWord(rs.getString("amt_in_word"));
						txVO.setTxTags(rs.getString("tx_tags"));
						txVO.setLedgerID(rs.getInt("ledger_id"));
						if(rs.getString("type").equalsIgnoreCase("C")){
						
								txVO.setCreditAmt(txVO.getAmount().abs());
							
							}else{
								
									txVO.setDebitAmt(txVO.getAmount().abs());
							
							}
						
						
						return txVO;
						
					}

				   };
				   TransactionList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit :  public List getTransactionListOfProjects(int orgID,int ledgerID,String fromDate,String uptoDate,String projectName)"+TransactionList.size());
			}catch(EmptyResultDataAccessException Ex){
				log.info("No Data found at List getTransactionList "+Ex);
			
		
				}catch(Exception se){
				log.error("Exception : public List getTransactionListOfProjects(int orgID,int ledgerID,String fromDate,String uptoDate,String projectName)"+se);
			   
			}
		return TransactionList;
	}
	
	/* Used to get transactions list of Project tags */
	public List getTransactionListOfCategoryWise(int orgID,int ledgerID,String fromDate,String uptoDate,List categoryList)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public List getTransactionListOfProjects(int orgID,int ledgerID,String fromDate,String uptoDate,String projectName)");
		List TransactionList=null;
		String strCondition="";
		String orCondition="";
		for(int i=0;categoryList.size()>i;i++){
			ProjectDetailsVO projVO=(ProjectDetailsVO) categoryList.get(i);
			  if(categoryList.size()==1){
		        	
				  orCondition=orCondition+" ((table_primary.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
		        	
		        }else{
		       
		        	if(i==0){
		        	
		        		orCondition=orCondition+" ((table_primary.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
		        	}else{
		        	
		        		orCondition=orCondition+" or ((table_primary.tx_le_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%')OR(l.project_tags LIKE '%#"+projVO.getProjectAcronyms().trim()+"%'))";
		        	}}
			
		}
	
		try{
			 SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String str="Select txTable.*,SUM(amount) AS txAmount ,SUM(int_amount) AS int_amount  FROM (SELECT ats.org_id,ats.tx_id as TXID,ats.doc_id,ats.tx_date,ats.ref_no,ats.create_date,ats.bank_date,ats.description,ats.tx_number,ats.tx_type,tx_mode,ats.created_by,ats.updated_by,m.full_name AS createdBy,mi.full_name AS updatedBy,table_primary.amount, table_primary.int_amount, table_primary.type, table_primary.tx_id,table_secondary.ledger_id,l.ledger_name ,sub_group_id,amt_in_word ,ats.tx_tags "+
					   " FROM account_ledger_entry_"+societyVO.getDataZoneID()+" table_primary,account_ledger_entry_"+societyVO.getDataZoneID()+" table_secondary,account_ledgers_"+societyVO.getDataZoneID()+" l,account_transactions_"+societyVO.getDataZoneID()+" ats,um_users m ,um_users mi "+
					   " WHERE table_primary.tx_id = table_secondary.tx_id "+
					   " AND table_primary.org_id=table_secondary.org_id "+
					   " AND table_primary.ledger_id=:ledgerID "+ 
					   " AND table_primary.type!=table_secondary.type "+
					   strCondition+
					   " AND l.id = table_secondary.ledger_id "+
					   " AND l.org_id=table_secondary.org_id  AND l.org_id=ats.org_id "+
					   " AND ats.tx_id = table_primary.tx_id AND (tx_date BETWEEN :fromDate AND :uptoDate) "+ 
				       " AND ats.is_deleted=0 AND m.id=ats.created_by AND mi.id=ats.updated_by and ats.org_id=:orgID AND ("+orCondition+") GROUP BY table_secondary.tx_id ORDER BY ats.tx_date ) AS txTable  GROUP BY txTable.ledger_id,txTable.tx_id ORDER BY txTable.tx_date; ";
			
			
			log.debug("Query is "+str);
			   
			   Map hMap=new HashMap();
			   hMap.put("orgID", orgID);
			   hMap.put("ledgerID", ledgerID);
			   hMap.put("fromDate", fromDate);
			   hMap.put("uptoDate", uptoDate);
			   
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						List accheadVOList=null;
						TransactionVO txVO=new TransactionVO();
						txVO.setTransactionID(rs.getInt("tx_id"));
						txVO.setSocietyID(rs.getInt("org_id"));
						txVO.setCreateDate(rs.getTimestamp("create_date"));
						txVO.setTransactionDate(rs.getString("tx_date"));
						txVO.setBankDate(rs.getString("bank_date"));
						txVO.setDescription(rs.getString("description"));
						txVO.setDocID(rs.getString("doc_id"));
						txVO.setTransactionNumber(rs.getString("tx_number"));
						txVO.setRefNumber(rs.getString("ref_no"));
						txVO.setTransactionType(rs.getString("tx_type"));
						txVO.setTransactionMode(rs.getString("tx_mode"));
						txVO.setCreditDebitFlag(rs.getString("type"));
						txVO.setAmount(rs.getBigDecimal("amount"));
						txVO.setIntAmount(rs.getBigDecimal("int_amount"));
						txVO.setLedgerName(rs.getString("ledger_name"));
						txVO.setGroupID(rs.getInt("sub_group_id"));
						txVO.setCreatorName(rs.getString("createdBy"));
						txVO.setUpdaterName(rs.getString("updatedBy"));
						txVO.setAmtInWord(rs.getString("amt_in_word"));
						txVO.setTxTags(rs.getString("tx_tags"));
						txVO.setLedgerID(rs.getInt("ledger_id"));
						if(rs.getString("type").equalsIgnoreCase("C")){
						
								txVO.setCreditAmt(txVO.getAmount().abs());
							
							}else{
								
									txVO.setDebitAmt(txVO.getAmount().abs());
							
							}
						
						
						return txVO;
						
					}

				   };
				   TransactionList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit :  public List getTransactionListOfProjects(int orgID,int ledgerID,String fromDate,String uptoDate,String projectName)"+TransactionList.size());
			}catch(EmptyResultDataAccessException Ex){
				log.info("No Data found at List getTransactionList "+Ex);
			
		
				}catch(Exception se){
				log.error("Exception : public List getTransactionListOfProjects(int orgID,int ledgerID,String fromDate,String uptoDate,String projectName)"+se);
			   
			}
		return TransactionList;
	}
	
	
	/* Used to get Bill transactions related to ledger */
	public List getBillTransactionList(int orgID,int ledgerID,String fromDate,String uptoDate,String type)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public List getBillTransactionList(String societyID,String ledgerID,String fromDate,String uptoDate)"+ledgerID+""+fromDate+""+uptoDate+" "+type);
		List TransactionList=null;
		String strCondition="";
		
	
		try{
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String str="Select txTable.*,s.tx_id,s.tag_values,s.accountant_comments,s.auditor_comments,SUM(amount) AS txAmount,SUM(int_amount) AS int_amount  FROM (SELECT ats.org_id,ats.tx_id as TXID,ats.doc_id,ats.tx_date,ats.ref_no,ats.create_date,ats.bank_date,ats.description,ats.tx_number,ats.tx_type,tx_mode,ats.created_by,ats.updated_by,table_secondary.amount,table_secondary.int_amount, table_primary.type, table_primary.tx_id,table_secondary.ledger_id,account_ledgers_"+societyVO.getDataZoneID()+".ledger_name ,sub_group_id,amt_in_word ,root_id ,ats.tx_tags"+
					   " FROM account_ledger_entry_"+societyVO.getDataZoneID()+" table_primary,account_ledger_entry_"+societyVO.getDataZoneID()+" table_secondary,account_ledgers_"+societyVO.getDataZoneID()+",account_transactions_"+societyVO.getDataZoneID()+" ats  ,account_groups ag"+
					   " WHERE table_primary.tx_id = table_secondary.tx_id "+
					   " And table_primary.org_id=table_secondary.org_id "+
					   " AND table_primary.ledger_id=:ledgerID "+ 
					    strCondition+
					   " AND account_ledgers_"+societyVO.getDataZoneID()+".id = table_secondary.ledger_id  AND account_ledgers_"+societyVO.getDataZoneID()+".sub_group_id=ag.id"+
					    " AND account_ledgers_"+societyVO.getDataZoneID()+".org_id=table_secondary.org_id and account_ledgers_"+societyVO.getDataZoneID()+".org_id=ats.org_id "+
					   " AND ats.tx_id = table_primary.tx_id AND (tx_date BETWEEN :fromDate AND :uptoDate) "+ 
				       " AND ats.is_deleted=0  and ats.org_id=:orgID GROUP BY table_secondary.id ORDER BY ats.tx_date ) AS txTable LEFT JOIN soc_tx_meta s ON txTable.TXID=s.tx_id AND s.society_id=:orgID GROUP BY txTable.ledger_id ORDER BY txTable.tx_date; ";
			log.debug("Query is "+str);
			   
			   Map hMap=new HashMap();
			   hMap.put("orgID", orgID);
			   hMap.put("ledgerID", ledgerID);
			   hMap.put("fromDate", fromDate);
			   hMap.put("uptoDate", uptoDate);
			   hMap.put("ledgerType", type);
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						List accheadVOList=null;
						TransactionVO txVO=new TransactionVO();
						txVO.setTransactionID(rs.getInt("tx_id"));
						txVO.setSocietyID(rs.getInt("org_id"));
						txVO.setLedgerID(rs.getInt("ledger_id"));
						txVO.setCreateDate(rs.getTimestamp("create_date"));
						txVO.setTransactionDate(rs.getString("tx_date"));
						txVO.setBankDate(rs.getString("bank_date"));
						txVO.setDescription(rs.getString("description"));
						txVO.setDocID(rs.getString("doc_id"));
						txVO.setTransactionNumber(rs.getString("tx_number"));
						txVO.setRefNumber(rs.getString("ref_no"));
						txVO.setTransactionType(rs.getString("tx_type"));
						txVO.setTransactionMode(rs.getString("tx_mode"));
						txVO.setCreditDebitFlag(rs.getString("type"));
						txVO.setAmount(rs.getBigDecimal("txAmount"));
						txVO.setIntAmount(rs.getBigDecimal("int_amount"));
						txVO.setLedgerName(rs.getString("ledger_name"));
						txVO.setGroupID(rs.getInt("root_id"));
						txVO.setAmtInWord(rs.getString("amt_in_word"));
						txVO.setTxTags(rs.getString("tx_tags"));
						txVO.setHashComments(rs.getString("tag_values"));
						txVO.setAccComments(rs.getString("accountant_comments"));
						txVO.setAuditorComments(rs.getString("auditor_comments"));
						if(rs.getString("type").equalsIgnoreCase("C")){
						
								txVO.setCreditAmt(txVO.getAmount().abs());
							
							}else{
								
									txVO.setDebitAmt(txVO.getAmount().abs());
							
							}
						
						
						return txVO;
						
					}

				   };
				   TransactionList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit :  public List getBillTransactionList(String societyID,String ledgerID,String fromDate,String uptoDate)"+TransactionList.size());
			}catch(EmptyResultDataAccessException Ex){
				log.info("No Data found at List getBillTransactionList "+Ex);
			
		
				}catch(Exception se){
				log.error("Exception : public List getBillTransactionList(String societyID,String ledgerID,String fromDate,String uptoDate)"+se);
			   
			}
		return TransactionList;
	}
	
	
	/* Used to get single transactions related to ledger */
	public TransactionVO getTransactionDetails(int orgID, int transactionID)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public TransactionVO getTransactionDetails(int societyID, int transactionID)");
		TransactionVO transactionVO=new TransactionVO();
		
		try{
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String str=" SELECT txTable.*,s.tx_id,s.tag_values,s.accountant_comments,s.auditor_comments,s.cheque_name,s.ext_tx_id,s.ext_app_name FROM (SELECT ats.tx_id AS TXID,ats.* FROM account_transactions_"+societyVO.getDataZoneID()+" ats " +
					   " WHERE ats.tx_id=:transactionID  and ats.org_id=:orgID  )AS txTable LEFT JOIN soc_tx_meta s ON txTable.TXID =s.tx_id AND s.society_id=:orgID";

			log.debug("Query is "+str);
			   
			   Map hMap=new HashMap();
			   hMap.put("orgID", orgID);
			   hMap.put("transactionID", transactionID);
			  
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						List accheadVOList=null;
						TransactionVO txVO=new TransactionVO();
						TransactionMetaVO txMetaVO=new TransactionMetaVO();
						txVO.setTransactionID(rs.getInt("tx_id"));
						txVO.setSocietyID(rs.getInt("org_id"));
						txVO.setCreateDate(rs.getTimestamp("create_date"));
						txVO.setTransactionDate(rs.getString("tx_date"));
						txVO.setBankDate(rs.getString("bank_date"));
						txVO.setDescription(rs.getString("description"));
						txVO.setDocID(rs.getString("doc_id"));
						txVO.setTransactionNumber(rs.getString("tx_number"));
						txVO.setRefNumber(rs.getString("ref_no"));
						txVO.setTransactionType(rs.getString("tx_type"));
						txVO.setTransactionMode(rs.getString("tx_mode"));
						txVO.setInvoiceID(rs.getInt("invoice_id"));
						txVO.setTxTags(rs.getString("tx_tags"));
						//txVO.setCreditDebitFlag(rs.getString("type"));
						txVO.setAmount(rs.getBigDecimal("amount"));
						//	txVO.setGroupID(rs.getInt("sub_group_id"));
						//txVO.setCreatorName(rs.getString("createdBy"));
						//txVO.setUpdaterName(rs.getString("updatedBy"));
						txVO.setUpdatedBy(rs.getInt("updated_by"));
						txVO.setAmtInWord(rs.getString("amt_in_word"));
						txVO.setHashComments(rs.getString("tag_values"));
						txVO.setAccComments(rs.getString("accountant_comments"));
						txVO.setAuditorComments(rs.getString("auditor_comments"));
						txMetaVO.setChequeName(rs.getString("cheque_name"));
						txMetaVO.setExtTxID(rs.getString("ext_tx_id"));
						txMetaVO.setExtAppName(rs.getString("ext_app_name"));
						/*if(rs.getString("type").equalsIgnoreCase("C")){
						
								txVO.setCreditAmt(txVO.getAmount().abs());
							
							}else{
								
									txVO.setDebitAmt(txVO.getAmount().abs());
							
							}*/
						txVO.setAdditionalProps(txMetaVO);
						
						return txVO;
						
					}

				   };
				   transactionVO=(TransactionVO) namedParameterJdbcTemplate.queryForObject(str, hMap, Rmapper);
					transactionVO.setTransactionID(transactionID);
			   log.debug("Exit :  public TransactionVO getTransactionDetails(int societyID, int transactionID)");
			}catch(EmptyResultDataAccessException Ex){
				log.info("No Data found at  getTransactionDetails "+Ex);
			
		
				}catch(Exception se){
				log.error("Exception : public TransactionVO getTransactionDetails(int societyID, int transactionID)"+se);
			   
			}
		return transactionVO;
	}
	
	
	public int updateTrasactionBankDate(String bankDate,String txId, String societyID)
	{
		int sucess=0;
		java.util.Date today = new java.util.Date();
		final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
		String condition="";
		if(bankDate.equalsIgnoreCase("UNRECONCILE")){
			condition= " bank_date =NULL ";
			
		}else{
			condition= " bank_date=:bankDate ";
		}
		try
		{  
			log.debug("Entry : public int updateTrasactionBankDate(String bankDate,String txId )");
			int orgID=Integer.parseInt(societyID);
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			if(bankDate.length()>1){
			 String str = "Update account_transactions_"+societyVO.getDataZoneID()+" set "+condition+" , update_date=:time where tx_id=:txID and org_id=:orgID ";
			   
			   
			   Map hMap=new HashMap();
			
			   hMap.put("txID", txId);
			   hMap.put("orgID",orgID);
			   hMap.put("bankDate", bankDate);
			   hMap.put("time", currentTimestamp);
			   
			   
			   sucess=namedParameterJdbcTemplate.update(str, hMap);
			
					
						
			
			}else{
				sucess=2;
			}
		}
		catch(DataIntegrityViolationException e){
			log.info("No Data found updateTrasactionBankDate(int memberID) "+e);
			
			}
		catch(EmptyResultDataAccessException Ex){
		log.info("No Data found updateTrasactionBankDate(int memberID) "+Ex);
		
		}
		catch(Exception ex)
		{
			log.error("Exception in updateTrasactionBankDate(String bankDate,String txId ) : "+ex);
			
		}
		
		log.debug("Exit : public intupdateTrasactionBankDate(String bankDate,String txId )"+sucess);
		return sucess;
	}
	
	/* Get list of all receipt for the period for a member */
	public List getReceiptForMember(int orgID ,int ledgerID,String fromDate,String uptoDate){
		List receiptList=null;
		log.debug("Entry : public List getReceiptForMember(String societyID ,String aptID,String fromDate,String uptoDate)"+ledgerID+fromDate+uptoDate);
		
try{
			
		
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);			 
			  String str = "SELECT ats.*,ale.amount AS amt,type as typ,ale.description AS des FROM account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale "+
							"WHERE ats.tx_id=ale.tx_id AND ats.org_id=ale.org_id and  ale.ledger_id=:ledgerID AND (tx_date BETWEEN :fromDate AND :uptoDate) and tx_type='Receipt' and is_deleted=0 ORDER BY ats.tx_date;";
			
			   log.info(str);
			   Map hMap=new HashMap();
			   hMap.put("orgID", orgID);
			   hMap.put("ledgerID", ledgerID);
			   hMap.put("fromDate", fromDate);
			   hMap.put("uptoDate", uptoDate);
			   
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						List accheadVOList=null;
						TransactionVO txVO=new TransactionVO();
						txVO.setTransactionID(rs.getInt("tx_id"));
						txVO.setSocietyID(rs.getInt("org_id"));
						txVO.setCreateDate(rs.getTimestamp("create_date"));
						txVO.setTransactionDate(rs.getString("tx_date"));
						txVO.setBankDate(rs.getString("bank_date"));
						txVO.setDescription(rs.getString("description"));
						txVO.setDocID(rs.getString("doc_id"));
						txVO.setTransactionNumber(rs.getString("tx_number"));
						txVO.setRefNumber(rs.getString("ref_no"));
						txVO.setTransactionType(rs.getString("tx_type"));
						txVO.setTransactionMode(rs.getString("tx_mode"));
						txVO.setCreditDebitFlag(rs.getString("typ"));
						txVO.setAmount(rs.getBigDecimal("amt"));
						//txVO.setLedgerName(rs.getString("led_name"));
						//txVO.setGroupID(rs.getInt("sub_group_id"));
						
						if(rs.getString("typ").equalsIgnoreCase("C")){
						
								txVO.setCreditAmt(txVO.getAmount().abs());
							
							}else{
								
									txVO.setDebitAmt(txVO.getAmount().abs());
							
							}
						
						
						return txVO;
						
					}

				   };
				   receiptList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit :  public List getTransactionList(String societyID,String ledgerID,String fromDate,String uptoDate)"+receiptList.size());
			}catch(EmptyResultDataAccessException Ex){
				log.info("No Data found at List getTransactionList "+Ex);
			
		
				}catch(Exception se){
				log.error("Exception : public List getTransactionList(String societyID,String ledgerID,String fromDate,String uptoDate)"+se);
			   
			}
		
		
		log.debug("Exit : public List getReceiptForMember(String societyID)");
		return receiptList;		
	}
	
	/* Get list of all receipt for the period for a member */
	public List getReceiptForMemberApp(int orgID ,int ledgerID){
		List receiptList=null;
		log.debug("Entry : public List getReceiptForMemberApp(String societyID ,String aptID)"+ledgerID);
		
try{
			
		
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);			 
			  String str = "SELECT ats.*,ale.amount AS amt,type as typ,ale.description AS des FROM account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale "+
							"WHERE ats.tx_id=ale.tx_id AND ats.org_id=ale.org_id and  ale.ledger_id=:ledgerID  and tx_type='Receipt' and is_deleted=0 ORDER BY ats.tx_date desc limit 12;";
			
			   log.info(str);
			   Map hMap=new HashMap();
			   hMap.put("orgID", orgID);
			   hMap.put("ledgerID", ledgerID);
			  
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						List accheadVOList=null;
						TransactionVO txVO=new TransactionVO();
						txVO.setTransactionID(rs.getInt("tx_id"));
						txVO.setSocietyID(rs.getInt("org_id"));
						txVO.setCreateDate(rs.getTimestamp("create_date"));
						txVO.setTransactionDate(rs.getString("tx_date"));
						txVO.setBankDate(rs.getString("bank_date"));
						txVO.setDescription(rs.getString("description"));
						txVO.setDocID(rs.getString("doc_id"));
						txVO.setTransactionNumber(rs.getString("tx_number"));
						txVO.setRefNumber(rs.getString("ref_no"));
						txVO.setTransactionType(rs.getString("tx_type"));
						txVO.setTransactionMode(rs.getString("tx_mode"));
						txVO.setCreditDebitFlag(rs.getString("typ"));
						txVO.setAmount(rs.getBigDecimal("amt"));
						//txVO.setLedgerName(rs.getString("led_name"));
						//txVO.setGroupID(rs.getInt("sub_group_id"));
						
						if(rs.getString("typ").equalsIgnoreCase("C")){
						
								txVO.setCreditAmt(txVO.getAmount().abs());
							
							}else{
								
									txVO.setDebitAmt(txVO.getAmount().abs());
							
							}
						
						
						return txVO;
						
					}

				   };
				   receiptList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit :  public List getReceiptForMemberApp(String societyID,String ledgerID,String fromDate,String uptoDate)"+receiptList.size());
			}catch(EmptyResultDataAccessException Ex){
				log.info("No Data found at List getReceiptForMemberApp "+Ex);
			
		
				}catch(Exception se){
				log.error("Exception : public List getReceiptForMemberApp(String societyID,String ledgerID,String fromDate,String uptoDate)"+se);
			   
			}
		
		
		log.debug("Exit : public List getReceiptForMemberApp(String societyID)");
		return receiptList;		
	}
	
public List getLatestTransactions(int orgID,int ledgerID,String txType){
		
		List transactionList=new ArrayList();
		try{
		log.debug("Entry : public List getLatestTransactions(String societyID,String ledgerID)");
		
		SocietyVO societyVO=societyService.getSocietyDetails(orgID);
		 String str = "SELECT ats.*,ale.amount AS amt,type as typ,ale.description AS des FROM account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale "+
			"WHERE ats.tx_id=ale.tx_id AND ats.org_id=ale.org_id and ale.ledger_id=:ledgerID AND tx_mode!='Cash' and tx_type=:txType and is_deleted=0 ORDER BY ats.tx_date desc limit 12;";


		 Map hMap=new HashMap();
		 hMap.put("orgID", orgID);
		 hMap.put("ledgerID", ledgerID);
		 hMap.put("txType", txType);
		 


		 RowMapper Rmapper = new RowMapper() {
			 public Object mapRow(ResultSet rs, int rowNum)
			throws SQLException {
				 List accheadVOList=null;
				 TransactionVO txVO=new TransactionVO();
				 txVO.setTransactionID(rs.getInt("tx_id"));
				 txVO.setSocietyID(rs.getInt("org_id"));
				 txVO.setCreateDate(rs.getTimestamp("create_date"));
				 txVO.setTransactionDate(rs.getString("tx_date"));
				 txVO.setBankDate(rs.getString("bank_date"));
				 txVO.setDescription(rs.getString("description"));
				 txVO.setDocID(rs.getString("doc_id"));
				 txVO.setTransactionNumber(rs.getString("tx_number"));
				 txVO.setRefNumber(rs.getString("ref_no"));
				 txVO.setTransactionType(rs.getString("tx_type"));
				 txVO.setTransactionMode(rs.getString("tx_mode"));
				 txVO.setCreditDebitFlag(rs.getString("typ"));
				 txVO.setAmount(rs.getBigDecimal("amount"));
		
				 if(rs.getString("typ").equalsIgnoreCase("C")){
		
				txVO.setCreditAmt(txVO.getAmount().abs());
			
				 }else{
				
					txVO.setDebitAmt(txVO.getAmount().abs());
			
				 }
		
		
		return txVO;
		
			 }

		 };
		transactionList=namedParameterJdbcTemplate.query(
		str, hMap, Rmapper);
		
		
		
		
		
		log.debug("Exit : ppublic List getLatestTransactions(String societyID,String ledgerID)");
		}catch (Exception e) {
			log.error("Exception: public List getLatestTransactions(String societyID,String ledgerID) "+e );
		}
		return transactionList;
	}

public int getLatestInvoiceNo(String societyiD){
	int invoiceNo=0;
	try
	{
		log.debug("Entry :public int getLatestInvoiceNo(String societyiD)");
		
		String sqlQry=" SELECT counter FROM society_settings WHERE society_id=:societyID" ;
				
		Map hMap=new HashMap();
		hMap.put("societyID", societyiD);
		
		invoiceNo=namedParameterJdbcTemplate.queryForObject(sqlQry, hMap, Integer.class);
		
			//invoiceNo=namedParameterJdbcTemplate.queryForInt(sqlQry, hMap);
		
	}catch(Exception ex)
	{
		ex.printStackTrace();
		log.error("Exception in getCategoryHead : "+ex);
		
	}
		log.debug("Exit : public int getLatestInvoiceNo(String societyiD)");
	return invoiceNo;
	}


public int getLatestCounterNo(int orgID,String counterType){
	int counterNo=0;
	try
	{
		log.debug("Entry : public int getLatestCounterNo(int orgID,String counterType)");
		
		String sqlQry=" SELECT  "+counterType+" as counter FROM org_tx_counter_details WHERE org_id=:orgID" ;
				
		Map hMap=new HashMap();
		hMap.put("orgID", orgID);
		
		counterNo=namedParameterJdbcTemplate.queryForObject(sqlQry, hMap, Integer.class);
		
			//invoiceNo=namedParameterJdbcTemplate.queryForInt(sqlQry, hMap);
		
	}catch(Exception ex)
	{
	
		log.error("Exception in public int getLatestCounterNo(int orgID,String counterType) : "+ex);
		
	}
		log.debug("Exit : public int getLatestCounterNo(int orgID,String counterType)");
	return counterNo;
	}


public TxCounterVO getCountersForOrg(int orgID){
	TxCounterVO txCounterVO=new TxCounterVO();
	try
	{
		log.debug("Entry : public int getCountersForOrg(int orgID)");
		
		String sqlQry=" SELECT * FROM org_tx_counter_details WHERE org_id=:orgID limit 1" ;
				
		Map hMap=new HashMap();
		hMap.put("orgID", orgID);
		
		RowMapper Rmapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				TxCounterVO counterVO=new TxCounterVO();
			    counterVO.setTxCounterID(rs.getInt("id"));
			    counterVO.setOrgID(rs.getInt("org_id"));
			    counterVO.setContra(rs.getInt("contra"));
			    counterVO.setCreditNote(rs.getInt("credit_note"));
			    counterVO.setDebitNote(rs.getInt("debit_note"));
			    counterVO.setJobworkIn(rs.getInt("jobwork_in"));
			    counterVO.setJobworkOut(rs.getInt("jobwork_out"));
			    counterVO.setJournal(rs.getInt("journal"));
			    counterVO.setMaterialIn(rs.getInt("material_in"));
			    counterVO.setMaterialOut(rs.getInt("material_out"));
			    counterVO.setMemorandum(rs.getInt("memorandum"));
			    counterVO.setPayment(rs.getInt("payment"));
			    counterVO.setPhysicalStock(rs.getInt("physical_stock"));
			    counterVO.setPurchase(rs.getInt("purchase"));
				counterVO.setPurchaseOrder(rs.getInt("purchase_order"));
				counterVO.setReceipt(rs.getInt("receipt"));
				counterVO.setReceiptNote(rs.getInt("receipt_note"));
				counterVO.setRejectionIn(rs.getInt("rejection_in"));
				counterVO.setRejectionOut(rs.getInt("rejection_out"));
				counterVO.setReversingJournal(rs.getInt("reversing_journal"));
				counterVO.setSales(rs.getInt("sales"));
				counterVO.setSalesOrder(rs.getInt("sales_order"));
				counterVO.setStockJournal(rs.getInt("stock_journal"));
				return counterVO;
			}

		   };
		
		txCounterVO=(TxCounterVO)namedParameterJdbcTemplate.queryForObject(sqlQry, hMap , Rmapper);
		
	}catch(Exception ex)
	{
		ex.printStackTrace();
		log.error("Exception in public int getCountersForOrg(int orgID) : "+ex);
		
	}
		log.debug("Exit : public int getCountersForOrg(int orgID)");
	return txCounterVO;
	}

public TransactionVO getReceiptDetails(int orgID,String ledgerID,String receiptID){
	TransactionVO transactionVO=new TransactionVO();
			
	try {
		log.debug("Entry :public TransactionVO getReceiptDetails(String societyID,String aptID,String transactionID)"+ledgerID+""+receiptID);
		SocietyVO societyVO=societyService.getSocietyDetails(orgID);
		String sqlQuery=" SELECT ats.tx_number, ats.*,ale.amount AS amt,ale.type AS typ,ale.description AS des,CONCAT(m.title,' ',m.full_name)AS ledger_name,CONCAT(b.building_name,' - ',a.apt_name)AS unit "
                       +" FROM account_transactions_"+societyVO.getDataZoneID()+" ats, account_ledger_entry_"+societyVO.getDataZoneID()+" ale,ledger_user_mapping_"+societyVO.getDataZoneID()+" lum,member_details m,account_ledgers_"+societyVO.getDataZoneID()+" al,apartment_details a,building_details b "
                       +" WHERE lum.user_id=m.member_id and ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=lum.org_id AND lum.ledger_type='M' AND ale.type='C' AND ats.tx_id=ale.tx_id AND lum.ledger_id=al.id AND b.building_id=a.building_id AND a.apt_id=m.apt_id "
                       +" AND m.type='P' AND is_current!=0  AND tx_type='Receipt' AND ats.is_deleted=0 AND al.id=:ledgerID AND ats.tx_id=:receiptID and ats.org_id=:orgID;";
		
		log.debug(receiptID+" Query is "+sqlQuery);
		 Map hMap=new HashMap();
		 hMap.put("orgID", orgID);
		 hMap.put("ledgerID", ledgerID);
		 hMap.put("receiptID", receiptID);
		
		RowMapper Rmapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				TransactionVO transactionVO=new TransactionVO();
				transactionVO.setTransactionID(rs.getInt("tx_id"));
				transactionVO.setSocietyID(rs.getInt("org_id"));
				transactionVO.setTransactionDate(rs.getString("tx_date"));
				transactionVO.setBankDate(rs.getString("bank_date"));
				transactionVO.setDescription(rs.getString("description"));
				transactionVO.setDocID(rs.getString("doc_id"));
				transactionVO.setTransactionNumber(rs.getString("tx_number"));
				transactionVO.setRefNumber(rs.getString("ref_no"));
				transactionVO.setTransactionType(rs.getString("tx_type"));
				transactionVO.setTransactionMode(rs.getString("tx_mode"));
				transactionVO.setAmount(rs.getBigDecimal("amount"));
				transactionVO.setLedgerName(rs.getString("ledger_name"));
				transactionVO.setUnit(rs.getString("unit"));
				transactionVO.setAmtInWord(rs.getString("amt_in_word"));
				transactionVO.setTxTags(rs.getString("tx_tags"));
				transactionVO.setCreateDate(rs.getTimestamp("create_date")); 
				return transactionVO;
			}

		   };
		   transactionVO= (TransactionVO) namedParameterJdbcTemplate.queryForObject(
				sqlQuery, hMap, Rmapper);
		
		
		log.debug("Exit :public TransactionVO getReceiptDetails(String societyID,String aptID,String transactionID)"+transactionVO.getTransactionID());
	}catch(EmptyResultDataAccessException Ex){
		log.info("No Data found at List getReceiptDetails "+Ex);
		
	} catch (Exception e) {
		log.error("Exception : public TransactionVO getReceiptDetails(String societyID,String aptID,String transactionID) "+e );
	}
	return transactionVO;
	
}

public TransactionVO getReceiptDetailsForMemberApp(int orgID,String ledgerID,String refNo){
	TransactionVO transactionVO=new TransactionVO();
			
	try {
		log.debug("Entry : public TransactionVO getReceiptDetailsForMemberApp(int orgID,String ledgerID,String refNo)"+ledgerID+""+refNo);
		SocietyVO societyVO=societyService.getSocietyDetails(orgID);
		String sqlQuery=" SELECT ats.tx_number, ats.*,ale.amount AS amt,ale.type AS typ,ale.description AS des,CONCAT(m.title,' ',m.full_name)AS ledger_name,CONCAT(b.building_name,' - ',a.apt_name)AS unit "
                       +" FROM account_transactions_"+societyVO.getDataZoneID()+" ats, account_ledger_entry_"+societyVO.getDataZoneID()+" ale,ledger_user_mapping_"+societyVO.getDataZoneID()+" lum,member_details m,account_ledgers_"+societyVO.getDataZoneID()+" al,apartment_details a,building_details b "
                       +" WHERE lum.user_id=m.member_id and ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=lum.org_id AND lum.ledger_type='M' AND ale.type='C' AND ats.tx_id=ale.tx_id AND lum.ledger_id=al.id AND b.building_id=a.building_id AND a.apt_id=m.apt_id "
                       +" AND m.type='P' AND is_current!=0  AND tx_type='Receipt' AND ats.is_deleted=0 AND al.id=:ledgerID AND ats.ref_no=:refNo and ats.org_id=:orgID;";
		
		log.debug(" Query is "+sqlQuery);
		 Map hMap=new HashMap();
		 hMap.put("orgID", orgID);
		 hMap.put("ledgerID", ledgerID);
		 hMap.put("refNo", refNo);
		
		RowMapper Rmapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				TransactionVO transactionVO=new TransactionVO();
				transactionVO.setTransactionID(rs.getInt("tx_id"));
				transactionVO.setSocietyID(rs.getInt("org_id"));
				transactionVO.setTransactionDate(rs.getString("tx_date"));
				transactionVO.setBankDate(rs.getString("bank_date"));
				transactionVO.setDescription(rs.getString("description"));
				transactionVO.setDocID(rs.getString("doc_id"));
				transactionVO.setTransactionNumber(rs.getString("tx_number"));
				transactionVO.setRefNumber(rs.getString("ref_no"));
				transactionVO.setTransactionType(rs.getString("tx_type"));
				transactionVO.setTransactionMode(rs.getString("tx_mode"));
				transactionVO.setAmount(rs.getBigDecimal("amount"));
				transactionVO.setLedgerName(rs.getString("ledger_name"));
				transactionVO.setUnit(rs.getString("unit"));
				transactionVO.setAmtInWord(rs.getString("amt_in_word"));
				transactionVO.setTxTags(rs.getString("tx_tags"));
							
				return transactionVO;
			}

		   };
		   transactionVO= (TransactionVO) namedParameterJdbcTemplate.queryForObject(
				sqlQuery, hMap, Rmapper);
		
		
		log.debug("Exit : public TransactionVO getReceiptDetailsForMemberApp(int orgID,String ledgerID,String refNo)"+transactionVO.getTransactionID());
	}catch(EmptyResultDataAccessException Ex){
		log.info("No Data found at List getReceiptDetails "+Ex);
		
	} catch (Exception e) {
		log.error("Exception : public TransactionVO getReceiptDetailsForMemberApp(int orgID,String ledgerID,String refNo) "+e );
	}
	return transactionVO;
	
}

public List getMemberReceiptList(int orgID,String fromDate,String toDate){
	
	List receiptList=null;		
	try {
		log.debug("Entry :public TransactionVO getMemberReceiptList(int orgID,String fromDate,String toDate)"+orgID+""+fromDate+" "+toDate);
		SocietyVO societyVO=societyService.getSocietyDetails(orgID);
		String sqlQuery=" SELECT ats.*,ale.amount AS amt,ale.type AS typ,ale.description AS des,ale.ledger_id,CONCAT(m.title,' ',m.full_name)AS ledger_name,CONCAT(m.building_name,' - ',m.apt_name)AS unit  "
                       +" FROM account_transactions_"+societyVO.getDataZoneID()+" ats, account_ledger_entry_"+societyVO.getDataZoneID()+" ale,ledger_user_mapping_"+societyVO.getDataZoneID()+" lum,members_info m "
                       +" WHERE ats.tx_id=ale.tx_id AND ats.org_id=ale.org_id AND ale.TYPE='C' AND (tx_date BETWEEN :fromDate AND :toDate) AND tx_type='Receipt' AND is_deleted=0  "
                       +" AND lum.user_id=m.member_id AND lum.ledger_type='M' AND lum.ledger_id=ale.ledger_id   AND m.type='P'   AND is_current!=0  and ats.org_id=:orgID ORDER BY ats.tx_date; ";
		
		log.debug(" Query is "+sqlQuery);
		 Map hMap=new HashMap();
		 hMap.put("orgID", orgID);
		 hMap.put("fromDate", fromDate);
		 hMap.put("toDate", toDate);
		
		RowMapper Rmapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				TransactionVO transactionVO=new TransactionVO();
				transactionVO.setTransactionID(rs.getInt("tx_id"));
				transactionVO.setSocietyID(rs.getInt("org_id"));
				transactionVO.setTransactionDate(rs.getString("tx_date"));
				transactionVO.setBankDate(rs.getString("bank_date"));
				transactionVO.setDescription(rs.getString("description"));
				transactionVO.setDocID(rs.getString("doc_id"));
				transactionVO.setTransactionNumber(rs.getString("tx_number"));
				transactionVO.setRefNumber(rs.getString("ref_no"));
				transactionVO.setTransactionType(rs.getString("tx_type"));
				transactionVO.setTransactionMode(rs.getString("tx_mode"));
				transactionVO.setAmount(rs.getBigDecimal("amount"));
				transactionVO.setLedgerName(rs.getString("ledger_name"));
				transactionVO.setUnit(rs.getString("unit"));
				transactionVO.setAmtInWord(rs.getString("amt_in_word"));
				transactionVO.setTxTags(rs.getString("tx_tags"));
							
				return transactionVO;
			}

		   };
		   receiptList=namedParameterJdbcTemplate.query(sqlQuery, hMap, Rmapper);
		
		
		log.debug("Exit :public TransactionVO getMemberReceiptList(int orgID,String fromDate,String toDate)"+receiptList.size());
	}catch(EmptyResultDataAccessException Ex){
		log.info("No Data found at List getMemberReceiptList "+Ex);
		
	} catch (Exception e) {
		log.error("Exception : public TransactionVO getMemberReceiptList(int orgID,String fromDate,String toDate) "+e );
	}
	return receiptList;
	
}


/* Get list of line item of receipt details   */
public List getLineItemsForReceipt(int orgID ,String receiptID){
	List receiptList=null;
	log.debug("Entry : public List getLineItemsForReceipt(String societyID ,String receiptID)"+receiptID);
	
	try{
		SocietyVO societyVO=societyService.getSocietyDetails(orgID);
		String sql = " SELECT itd.invoice_type,rim.receipt_id,GROUP_CONCAT(rim.invoice_id ORDER BY rim.invoice_id  SEPARATOR ',') AS invoiceID,SUM(rim.amount)AS totalAmount FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" rim,in_invoice_type_details itd,in_invoice_"+societyVO.getDataZoneID()+" ins "
                    +" WHERE itd.id=ins.invoice_type_id and ins.org_id=rim.org_id AND ins.id=rim.invoice_id AND rim.receipt_id=:receiptID and ins.org_id=:orgID GROUP BY ins.invoice_type_id ORDER BY itd.id ";
		
	Map hashMap = new HashMap();

	hashMap.put("orgID",orgID);
	hashMap.put("receiptID", receiptID);
	

	RowMapper rMapper = new RowMapper() {

		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			TransactionVO transactionVO=new TransactionVO();
			transactionVO.setType(rs.getString("invoice_type"));
			transactionVO.setAmount(rs.getBigDecimal("totalAmount"));	
			transactionVO.setTransactionNumber(rs.getString("invoiceID"));
			
			return transactionVO;
		}
	};
		
	receiptList=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
		}catch(Exception e){
			log.error("Exception in getLineItemsForReceipt "+e);
		}
	
		log.debug("Exit : public List getLineItemsForReceipt(String societyID ,String receiptID)"+receiptList.size());
	return receiptList;		
}


public TransactionVO getDiscountedAmount(AccountHeadVO discntVO,int txID,int orgID){
	TransactionVO transactionVO=new TransactionVO();
			
	try {
		log.debug("Entry : public TransactionVO getDiscountedAmount(AccountHeadVO discntVO,int txID)"+txID+" "+discntVO.getLedgerID());
		SocietyVO societyVO=societyService.getSocietyDetails(orgID);
		String sqlQuery=" SELECT SUM(amount) as discount FROM account_ledger_entry_"+societyVO.getDataZoneID()+" WHERE tx_id=:txID AND ledger_id=:ledgerID and org_id=:orgID GROUP BY ledger_id;";
		
		
		 Map hMap=new HashMap();
		 hMap.put("orgID", orgID);
		 hMap.put("txID", txID);
		 hMap.put("ledgerID", discntVO.getLedgerID());
		
		RowMapper Rmapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				TransactionVO transactionVO=new TransactionVO();
				transactionVO.setDiscountAmt(rs.getBigDecimal("discount"));
								
				return transactionVO;
			}

		   };
		   transactionVO= (TransactionVO) namedParameterJdbcTemplate.queryForObject(
				sqlQuery, hMap, Rmapper);
		
		
		
	}catch(EmptyResultDataAccessException Ex){
		log.info("No Data found at getDiscountedAmount(AccountHeadVO discntVO,int txID) "+Ex);
		transactionVO.setDiscountAmt(BigDecimal.ZERO);
		
	} catch (Exception e) {
		log.error("Exception : public TransactionVO getDiscountedAmount(AccountHeadVO discntVO,int txID) "+e );
	}
	
	log.debug("Exit :public TransactionVO getDiscountedAmount(AccountHeadVO discntVO,int txID)"+transactionVO.getDiscountAmt());
	return transactionVO;
	
}


public int incrementInvoiceNumber(int societyiD){
	int invoiceNo=0;
	log.debug("Entry : public int incrementInvoiceNumber(String societyiD)");
	try
	{
		log.debug("Entry :public int getLatestInvoiceNo(String societyiD)");
		
		String sqlQry=" Update society_settings set counter=counter+1 where society_id=:societyID" ;
				
		Map hMap=new HashMap();
		hMap.put("societyID", societyiD);
		
		
		
			invoiceNo=namedParameterJdbcTemplate.update(sqlQry, hMap);
		
	}catch(Exception ex)
	{
		ex.printStackTrace();
		log.error("Exception in getCategoryHead : "+ex);
		
	}
		log.debug("Exit : public int incrementInvoiceNumber(String societyiD)");
	return invoiceNo;
	}

public int incrementCounterNumber(int orgID,String counterType){
	int counterNo=0;
	log.debug("Entry : public int incrementCounterNumber(String societyiD)");
	try
	{
		log.debug("Entry :public int incrementCounterNumber(String societyiD)");
				 
		String sqlQry=" Update org_tx_counter_details set "+counterType+"="+counterType+"+1 where org_id=:orgID" ;
				
		Map hMap=new HashMap();
		hMap.put("orgID", orgID);
		
		counterNo=namedParameterJdbcTemplate.update(sqlQry, hMap);
		
	}catch(Exception ex)
	{
		ex.printStackTrace();
		log.error("Exception in incrementCounterNumber : "+ex);
		
	}
		log.debug("Exit : public int incrementCounterNumber(String societyiD)");
	return counterNo;
	}
	
/* Used for adding Additional properties  */
public int insertAdditionalProperties(TransactionVO txAddPropVO) {
	int success=0;
	 java.util.Date today = new java.util.Date();
	 final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
	log.debug("Entry : public int insertAdditionalProperties(TransactionVO txAddPropVO)");
	try {
		TransactionMetaVO txMeta=txAddPropVO.getAdditionalProps();
		if(txMeta==null){
			txMeta=new TransactionMetaVO();
		}
		if(txAddPropVO.getGenerateInvoice())
			txMeta.setInvoiceNO("INV_"+txAddPropVO.getTransactionID());
		   			   
	
		
		   String str = "INSERT INTO soc_tx_meta "+					
			   "(society_id,tx_id ,vendor_id, bill_id, invoice_no, ledger_id, invoice_date, invoice_type_id, tds_section_no, tds_rate, tds_amount, tax_amount , challan_date, challan_no, bsr_code, tag_values,accountant_comments,auditor_comments,cheque_name,ext_tx_id,ext_app_name)  " +
                "VALUES (:societyID,:txID, :vendorID, :billID, :invoiceNo, :ledgerID, :invoiceDate, :invoiceTypeID, :tdsSectionNo, :tdsRate, :tdsAmount, :taxAmount, :challanDate, :challanNo, :bsrCode, :tagValue,:accountantComments,:auditor_comments,:chequeName, :extTxID, :extAppName)";
		   
		   
		   Map hMap=new HashMap();
		   hMap.put("societyID", txAddPropVO.getSocietyID());
		   hMap.put("txID", txAddPropVO.getTransactionID());
		   hMap.put("vendorID", txMeta.getVendorID());
		   hMap.put("billID", txMeta.getBillID());
		   hMap.put("invoiceNo", txMeta.getInvoiceNO());
		   hMap.put("ledgerID", txMeta.getLedgerID());
		   hMap.put("invoiceDate", txMeta.getInvoiceDate());
		   hMap.put("invoiceTypeID", txMeta.getInvoiceTypeID());
		   hMap.put("tdsSectionNo", txMeta.getTdsSectionNo());
		   hMap.put("tdsRate", txMeta.getTdsRate());
		   hMap.put("tdsAmount", txMeta.getTdsAmount());
		   hMap.put("taxAmount", txMeta.getTaxAmount());
		   hMap.put("challanDate", txMeta.getChallanDate());
		   hMap.put("challanNo", txMeta.getChallanNo());
		   hMap.put("bsrCode", txMeta.getBsrCode());
		   hMap.put("tagValue", txAddPropVO.getHashComments());
		   hMap.put("accountantComments", txMeta.getAccountantComments());
		   hMap.put("auditor_comments", txMeta.getAuditorsComments());
		   hMap.put("chequeName", txMeta.getChequeName());
		   hMap.put("extTxID", txMeta.getExtTxID());
		   hMap.put("extAppName", txMeta.getExtAppName());
		   
		   
		   
		   success=namedParameterJdbcTemplate.update(str, hMap);
		
		log.debug("Exit : public int insertAdditionalProperties(TransactionVO txAddPropVO)");	
	} catch (Exception e) {
		// TODO Auto-generated catch block
		log.error("Exception in public int insertAdditionalProperties(TransactionVO txAddPropVO) "+e);
	}
	
	return success;
}

/* Used for adding Additional properties  */
public int updateAdditionalProperties(TransactionVO txAddPropVO,int societyID) {
	int success=0;
	 java.util.Date today = new java.util.Date();
	 final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
	log.debug("Entry : public int updateAdditionalProperties(TransactionVO txAddPropVO)");
	try {
		TransactionMetaVO txMeta=txAddPropVO.getAdditionalProps();
		if(txMeta==null){
			txMeta=new TransactionMetaVO();
		}			   
		
		String str = "Update soc_tx_meta set invoice_no=:invNO,invoice_date=:invoiceDate,billing_cycle_id=:billingCycleID, invoice_type_id=:invoiceTypeID, status_id=:statusID ,ledger_id=:ledgerID,invoice_mode=:invoiceMode, "
				+ " bill_id=:billID, tag_values=:tagValues ,accountant_comments=:accountantComments , auditor_comments=:auditorComments, cheque_name=:chequeName, ext_tx_id=:extTxID, ext_app_name=:extAppName "
				+ " where tx_id=:txID and society_id=:societyID ;  ";
                
		   
		   
		   Map hMap=new HashMap();
		   hMap.put("societyID", txAddPropVO.getSocietyID());
		   hMap.put("txID", txAddPropVO.getTransactionID());
		   hMap.put("invNO", txMeta.getInvoiceNO());
		   hMap.put("invoiceDate", txAddPropVO.getTransactionDate());
		   hMap.put("billingCycleID", txMeta.getBillingCycleID());
		   hMap.put("invoiceTypeID", txMeta.getInvoiceTypeID());
		   hMap.put("statusID", txMeta.getStatusID());
		   hMap.put("invoiceMode", txMeta.getInvoiceMode());
		   hMap.put("ledgerID", txMeta.getLedgerID());
		   hMap.put("billID", txMeta.getBillID());
		   hMap.put("tagValues", txAddPropVO.getHashComments());
		   hMap.put("accountantComments", txMeta.getAccountantComments());
		   hMap.put("auditorComments", txMeta.getAuditorsComments());
		   hMap.put("chequeName", txMeta.getChequeName());
		   hMap.put("extTxID", txMeta.getExtTxID());
		   hMap.put("extAppName", txMeta.getExtAppName());
		   
		   
		   success=namedParameterJdbcTemplate.update(str, hMap);
		
		   if(success==0){
			   success=insertAdditionalProperties(txAddPropVO);
		   }
		   
		   
		log.debug("Exit : public int updateAdditionalProperties(TransactionVO txAddPropVO)"+success);	
	} catch (Exception e) {
		
		log.debug("Exception in public int updateAdditionalProperties(TransactionVO txAddPropVO) "+e);
	}
	
	return success;
}
/* Used to get day book transactions count  */
public int getAllTransactionsCount(TransactionVO txVO)  {
	log.debug("Entry : public List getAllTransactionsCount(TransactionVO txVO)");
	int transactionCount=0;
	List transactionList=null;
	String txTypeCondition="";
	String txStatusCondition="";
	
	if(!txVO.getTransactionType().equalsIgnoreCase("all")){
		txTypeCondition=" and ats.tx_type=:txType ";
	}
	if(!txVO.getStatusDescription().equalsIgnoreCase("All")){
		txStatusCondition=" and ats.is_deleted=:statusID ";
	}
	
	

	try{
		SocietyVO societyVO=societyService.getSocietyDetails(txVO.getSocietyID());
		String str="SELECT count(ats.tx_id) as txID  "+
					" FROM account_ledger_entry_"+societyVO.getDataZoneID()+" table_primary,account_ledger_entry_"+societyVO.getDataZoneID()+" table_secondary,account_ledgers_"+societyVO.getDataZoneID()+",account_transactions_"+societyVO.getDataZoneID()+" ats,um_users m , um_users mi "+ 
					" WHERE table_primary.tx_id = table_secondary.tx_id AND table_primary.type!=table_secondary.type "+
					" AND table_primary.org_id=table_secondary.org_id "+
					" AND account_ledgers_"+societyVO.getDataZoneID()+".id = table_primary.ledger_id "+txStatusCondition+" "+txTypeCondition+
					" AND ats.tx_id = table_primary.tx_id AND (tx_date BETWEEN :fromDate AND :uptoDate ) "+  
					"  AND ats.org_id=account_ledgers_"+societyVO.getDataZoneID()+".org_id and ats.org_id=table_primary.org_id "+					
					" AND m.id=ats.created_by AND mi.id=ats.updated_by and ats.org_id=:orgID GROUP BY table_secondary.tx_id ORDER BY ats.tx_date " ;
				    
		
		
		log.debug("Query is "+str);
		   
		   Map hMap=new HashMap();
		   hMap.put("orgID", txVO.getSocietyID());
		  
		   hMap.put("fromDate", txVO.getFromDate());
		   hMap.put("uptoDate", txVO.getToDate());
		   hMap.put("txType", txVO.getTransactionType());
		   hMap.put("statusID", txVO.getStatusDescription());
		  
		   RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					List accheadVOList=null;
					TransactionVO txVO=new TransactionVO();
					txVO.setTransactionID(rs.getInt("txID"));
				
					
					
					return txVO;
					
				}

			   };
			   transactionList=namedParameterJdbcTemplate.query(str, hMap, Rmapper);
			   transactionCount=transactionList.size();
		   log.debug("Exit :  public List getAllTransactionsCount(TransactionVO txVO)"+transactionCount);
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at public List getAllTransactionsCount(TransactionVO txVO) "+Ex);
		
	
			}catch(Exception se){
			log.error("Exception : public List getAllTransactionsCount(TransactionVO txVO) for Org ID : "+txVO.getSocietyID()+", for the period "+txVO.getFromDate()+" to "+txVO.getToDate()+ "    " +se);
		   
		}
	return transactionCount;
}

/* Used to  get day book transactions  */
public List getAllTransactionsList(TransactionVO txVO)  {
	log.debug("Entry : public List getAllTransactionsList(TransactionVO txVO)");
	List TransactionList=null;
	String txTypeCondition="";
	String txStatusCondition="";
	String groupIDCondition="";
	
	
	if(!txVO.getTransactionType().equalsIgnoreCase("all")){
		txTypeCondition=" and ats.tx_type=:txType  ";
	}
	if(!txVO.getStatusDescription().equalsIgnoreCase("All")){
		txStatusCondition=" and ats.is_deleted=:statusID ";
	}
	if(txVO.getGroupID()>0){
		groupIDCondition=" AND a.sub_group_id=:groupID ";
	}
	

	try{
		SocietyVO societyVO=societyService.getSocietyDetails(txVO.getSocietyID());
		String str="SELECT ats.org_id,ats.tx_id AS TXID,ats.invoice_id,ats.tx_date,ats.doc_id,ats.ref_no,ats.create_date,ats.bank_date,ats.description,ats.tx_number,ats.tx_type,tx_mode,ats.created_by,ats.updated_by,m.full_name AS createdBy,mi.full_name AS updatedBy,ats.amount, table_primary.type, table_primary.tx_id,table_secondary.ledger_id,account_ledgers_"+societyVO.getDataZoneID()+".ledger_name ,account_ledgers_"+societyVO.getDataZoneID()+".sub_group_id,amt_in_word ,ats.is_deleted ,a.ledger_name AS LedgerName ,CONCAT(IFNULL(table_secondary.tx_le_tags,''),IFNULL(a.project_tags,'')) AS tx_le_tags "+
					" FROM account_ledger_entry_"+societyVO.getDataZoneID()+" table_primary,account_ledger_entry_"+societyVO.getDataZoneID()+" table_secondary,account_ledgers_"+societyVO.getDataZoneID()+",account_transactions_"+societyVO.getDataZoneID()+" ats,um_users m , um_users mi ,account_ledgers_"+societyVO.getDataZoneID()+" a"+ 
					" WHERE table_primary.tx_id = table_secondary.tx_id AND table_primary.type!=table_secondary.type "+
					" AND table_primary.org_id=table_secondary.org_id  AND a.id=table_secondary.ledger_id"+
					" AND account_ledgers_"+societyVO.getDataZoneID()+".id = table_primary.ledger_id "+txStatusCondition+" "+txTypeCondition +groupIDCondition +
					" AND ats.tx_id = table_primary.tx_id AND (tx_date BETWEEN :fromDate AND :uptoDate ) "+  
					"  AND ats.org_id=account_ledgers_"+societyVO.getDataZoneID()+".org_id and ats.org_id=table_primary.org_id "+					
					" AND m.id=ats.created_by AND mi.id=ats.updated_by and ats.org_id=:orgID GROUP BY table_secondary.tx_id ORDER BY ats.tx_date " ;
				    
		
		
		log.debug("Query is "+str);
		   
		   Map hMap=new HashMap();
		   hMap.put("orgID", txVO.getSocietyID());
		  
		   hMap.put("fromDate", txVO.getFromDate());
		   hMap.put("uptoDate", txVO.getToDate());
		   hMap.put("txType", txVO.getTransactionType());
		   hMap.put("statusID", txVO.getStatusDescription());
		   hMap.put("groupID", txVO.getGroupID());
		  
		   RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					List accheadVOList=null;
					TransactionVO txVO=new TransactionVO();
					txVO.setTransactionID(rs.getInt("txID"));
					txVO.setSocietyID(rs.getInt("org_id"));
					txVO.setTransactionDate(rs.getString("tx_date"));
					txVO.setBankDate(rs.getString("bank_date"));
					txVO.setDescription(rs.getString("description"));
					txVO.setTransactionNumber(rs.getString("tx_number"));
					txVO.setInvoiceID(rs.getInt("invoice_id"));
					txVO.setDocID(rs.getString("doc_id"));
					txVO.setRefNumber(rs.getString("ref_no"));
					txVO.setTransactionType(rs.getString("tx_type"));
					txVO.setTransactionMode(rs.getString("tx_mode"));
					txVO.setCreditDebitFlag(rs.getString("type"));
					txVO.setAmount(rs.getBigDecimal("amount"));
					txVO.setLedgerName(rs.getString("ledger_name"));
					txVO.setGroupID(rs.getInt("sub_group_id"));
					txVO.setCreatorName(rs.getString("createdBy"));
					txVO.setUpdaterName(rs.getString("updatedBy"));
					txVO.setIsDeleted(rs.getInt("is_deleted"));
					txVO.setAmtInWord(rs.getString("amt_in_word"));
					txVO.setDisplayNote(rs.getString("LedgerName"));
					txVO.setTxTags(rs.getString("tx_le_tags"));
					if(rs.getString("type").equalsIgnoreCase("C")){
					
							txVO.setCreditAmt(txVO.getAmount().abs());
						
						}else{
							
								txVO.setDebitAmt(txVO.getAmount().abs());
						
						}
					
					
					return txVO;
					
				}

			   };
			   TransactionList=namedParameterJdbcTemplate.query(
					str, hMap, Rmapper);
		   log.debug("Exit :  public List getAllTransactionsList(TransactionVO txVO)"+TransactionList.size());
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at public List getAllTransactionsList(TransactionVO txVO) "+Ex);
		
	
			}catch(Exception se){
			log.error("Exception : public List getAllTransactionsList(TransactionVO txVO) for Org ID : "+txVO.getSocietyID()+"  , for the period "+txVO.getFromDate()+" to "+txVO.getToDate()+ "    " +se);
		   
		}
	return TransactionList;
}


/* Used to  get day book transactions  */
public List getAllOldTransactionsList(int orgID)  {
	log.debug("Entry : public List getAllTransactionsList(TransactionVO txVO)");
	List TransactionList=null;
	
	

	try{
		SocietyVO societyVO=societyService.getSocietyDetails(orgID);
		String str="SELECT ats.org_id,ats.tx_id AS TXID,ats.invoice_id,ats.tx_date,ats.doc_id,ats.ref_no,ats.create_date,ats.bank_date,ats.description,ats.tx_number,ats.tx_type,tx_mode,ats.created_by,ats.updated_by,m.full_name AS createdBy,mi.full_name AS updatedBy,ats.amount, table_primary.type, table_primary.tx_id,table_secondary.ledger_id,account_ledgers_"+orgID+".ledger_name ,account_ledgers_"+orgID+".sub_group_id,amt_in_word ,ats.is_deleted ,a.ledger_name AS LedgerName "+
					" FROM account_ledger_entry_"+orgID+" table_primary,account_ledger_entry_"+orgID+" table_secondary,account_ledgers_"+orgID+",account_transactions_"+orgID+" ats,um_users m , um_users mi ,account_ledgers_"+orgID+" a"+ 
					" WHERE table_primary.tx_id = table_secondary.tx_id AND table_primary.type!=table_secondary.type "+
					" AND table_primary.org_id=table_secondary.org_id  AND a.id=table_secondary.ledger_id"+
					" AND account_ledgers_"+orgID+".id = table_primary.ledger_id "+
					" AND ats.tx_id = table_primary.tx_id  "+  
					"  AND ats.org_id=account_ledgers_"+orgID+".org_id and ats.org_id=table_primary.org_id "+					
					" AND m.id=ats.created_by AND mi.id=ats.updated_by and ats.org_id=:orgID GROUP BY table_secondary.tx_id ORDER BY ats.tx_date " ;
				    
		
		
		log.debug("Query is "+str);
		   
		   Map hMap=new HashMap();
		   hMap.put("orgID", orgID);
		  
		 
		  
		   RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					List accheadVOList=null;
					TransactionVO txVO=new TransactionVO();
					txVO.setTransactionID(rs.getInt("txID"));
					txVO.setSocietyID(rs.getInt("org_id"));
					txVO.setInvoiceID(rs.getInt("invoice_id"));
					txVO.setTransactionDate(rs.getString("tx_date"));
					txVO.setBankDate(rs.getString("bank_date"));
					txVO.setDescription(rs.getString("description"));
					txVO.setTransactionNumber(rs.getString("tx_number"));
					txVO.setDocID(rs.getString("doc_id"));
					txVO.setRefNumber(rs.getString("ref_no"));
					txVO.setTransactionType(rs.getString("tx_type"));
					txVO.setTransactionMode(rs.getString("tx_mode"));
					txVO.setCreditDebitFlag(rs.getString("type"));
					txVO.setAmount(rs.getBigDecimal("amount"));
					txVO.setLedgerName(rs.getString("ledger_name"));
					txVO.setGroupID(rs.getInt("sub_group_id"));
					txVO.setCreatorName(rs.getString("createdBy"));
					txVO.setUpdaterName(rs.getString("updatedBy"));
					txVO.setCreatedBy(rs.getInt("created_by"));
					txVO.setUpdatedBy(rs.getInt("updated_by"));
					txVO.setIsDeleted(rs.getInt("is_deleted"));
					txVO.setAmtInWord(rs.getString("amt_in_word"));
					txVO.setDisplayNote(rs.getString("LedgerName"));
					if(rs.getString("type").equalsIgnoreCase("C")){
					
							txVO.setCreditAmt(txVO.getAmount().abs());
						
						}else{
							
								txVO.setDebitAmt(txVO.getAmount().abs());
						
						}
					
					
					return txVO;
					
				}

			   };
			   TransactionList=namedParameterJdbcTemplate.query(
					str, hMap, Rmapper);
		   log.debug("Exit :  public List getAllTransactionsList(TransactionVO txVO)"+TransactionList.size());
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at public List getAllTransactionsList(TransactionVO txVO) "+Ex);
		
	
			}catch(Exception se){
			log.error("Exception : public List getAllTransactionsList(TransactionVO txVO) " +se);
		   
		}
	return TransactionList;
}

/* Used to get non recorded transactions  */
public List getAllNonRecordedTransactionsReportForLedger(TransactionVO txVO)  {
	log.debug("Entry : public List getAllNonRecordedTransactionsReportForLedger(TransactionVO txVO)");
	List TransactionList=null;
	String txTypeCondition="";
	String txStatusCondition=" and ats.is_deleted!=0 ";
	
	if(!txVO.getTransactionType().equalsIgnoreCase("all")){
		txTypeCondition=" and ats.tx_type=:txType  ";
	}
	if(!txVO.getStatusDescription().equalsIgnoreCase("All")){
		txStatusCondition=" and ats.is_deleted=:statusID ";
	}
	
	

	try{
		SocietyVO societyVO=societyService.getSocietyDetails(txVO.getSocietyID());
		String str="SELECT ats.org_id,ats.tx_id AS TXID,ats.tx_date,ats.doc_id,ats.ref_no,ats.create_date,ats.bank_date,ats.description,ats.tx_number,ats.tx_type,tx_mode,ats.created_by,ats.updated_by,m.full_name AS createdBy,mi.full_name AS updatedBy,ats.amount, table_primary.type, table_primary.tx_id,table_secondary.ledger_id,account_ledgers_"+societyVO.getDataZoneID()+".ledger_name ,sub_group_id,amt_in_word ,ats.is_deleted "+
					" FROM account_ledger_entry_"+societyVO.getDataZoneID()+" table_primary,account_ledger_entry_"+societyVO.getDataZoneID()+" table_secondary,account_ledgers_"+societyVO.getDataZoneID()+",account_transactions_"+societyVO.getDataZoneID()+" ats,um_users m , um_users mi "+ 
					" WHERE table_primary.tx_id = table_secondary.tx_id AND table_primary.type!=table_secondary.type "+
					" AND table_primary.org_id=table_secondary.org_id  and table_primary.ledger_id=:ledgerID "+
					" AND account_ledgers_"+societyVO.getDataZoneID()+".id = table_primary.ledger_id "+txStatusCondition+" "+txTypeCondition +
					" AND ats.tx_id = table_primary.tx_id AND (tx_date BETWEEN :fromDate AND :uptoDate )  "+  
					"  AND ats.org_id=account_ledgers_"+societyVO.getDataZoneID()+".org_id and ats.org_id=table_primary.org_id "+					
					" AND m.id=ats.created_by AND mi.id=ats.updated_by and ats.org_id=:orgID GROUP BY table_secondary.tx_id ORDER BY ats.tx_date " ;
				    
		
		
		log.debug("Query is "+str);
		   
		   Map hMap=new HashMap();
		   hMap.put("orgID", txVO.getSocietyID());
		   hMap.put("ledgerID", txVO.getLedgerID());
		   hMap.put("fromDate", txVO.getFromDate());
		   hMap.put("uptoDate", txVO.getToDate());
		   hMap.put("txType", txVO.getTransactionType());
		   hMap.put("statusID", txVO.getStatusDescription());
		  
		   RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					List accheadVOList=null;
					TransactionVO txVO=new TransactionVO();
					txVO.setTransactionID(rs.getInt("txID"));
					txVO.setSocietyID(rs.getInt("org_id"));
					txVO.setTransactionDate(rs.getString("tx_date"));
					txVO.setBankDate(rs.getString("bank_date"));
					txVO.setDescription(rs.getString("description"));
					txVO.setTransactionNumber(rs.getString("tx_number"));
					txVO.setDocID(rs.getString("doc_id"));
					txVO.setRefNumber(rs.getString("ref_no"));
					txVO.setTransactionType(rs.getString("tx_type"));
					txVO.setTransactionMode(rs.getString("tx_mode"));
					txVO.setCreditDebitFlag(rs.getString("type"));
					txVO.setAmount(rs.getBigDecimal("amount"));
					txVO.setLedgerName(rs.getString("ledger_name"));
					txVO.setGroupID(rs.getInt("sub_group_id"));
					txVO.setCreatorName(rs.getString("createdBy"));
					txVO.setUpdaterName(rs.getString("updatedBy"));
					txVO.setIsDeleted(rs.getInt("is_deleted"));
					txVO.setAmtInWord(rs.getString("amt_in_word"));
				
					if(rs.getString("type").equalsIgnoreCase("C")){
					
							txVO.setCreditAmt(txVO.getAmount().abs());
						
						}else{
							
								txVO.setDebitAmt(txVO.getAmount().abs());
						
						}
					
					
					return txVO;
					
				}

			   };
			   TransactionList=namedParameterJdbcTemplate.query(
					str, hMap, Rmapper);
		   log.debug("Exit :  public List getAllNonRecordedTransactionsReportForLedger(TransactionVO txVO)"+TransactionList.size());
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at public List getAllNonRecordedTransactionsReportForLedger(TransactionVO txVO)) "+Ex);
		
	
			}catch(Exception se){
			log.error("Exception : public List getAllNonRecordedTransactionsReportForLedger(TransactionVO txVO) : for Org ID : "+txVO.getSocietyID()+" ,ledger ID "+txVO.getLedgerID()+" , for the period "+txVO.getFromDate()+" to "+txVO.getToDate()+ "    " +se);
		   
		}
	return TransactionList;
}


public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID, String type){
	OnlinePaymentGatewayVO onlinePaymentGatewayVO=new OnlinePaymentGatewayVO();
	
	
	try {
		String str="SELECT m.*,g.* FROM pg_merchant_details m,pg_gateway_details g"
				+ "  WHERE  m.gateway_id=g.id AND  m.society_id=:societyID AND m.type=:type  ";
		log.debug("query : "+str);

		Map hMap=new HashMap();
		hMap.put("societyID", societyID);
		hMap.put("type", type);
		
		RowMapper Rmapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
		
				OnlinePaymentGatewayVO onlPVO=new OnlinePaymentGatewayVO();
				onlPVO.setSocietyID(rs.getInt("society_id"));
				onlPVO.setMerchantID(rs.getString("merchant_id"));
				onlPVO.setMerchantKey(rs.getString("merchant_key"));
				onlPVO.setMerchantSaltKey(rs.getString("merchant_salt_key"));
				onlPVO.setGatewayName(rs.getString("name"));
				onlPVO.setGatewayUrl(rs.getString("g.url"));
				onlPVO.setGatewayID(rs.getInt("m.gateway_id"));
				onlPVO.setIsOnlnPymtAccptd(1);
				onlPVO.setType(rs.getString("type"));
				onlPVO.setMtNetBanking(rs.getBigDecimal("m.net_banking"));
				onlPVO.setMtCreditCard(rs.getBigDecimal("m.credit_card"));
				onlPVO.setMtDcLess1000(rs.getBigDecimal("m.dc_l_1000"));
				onlPVO.setMtDc1000to2000(rs.getBigDecimal("m.dc_1000_to_2000"));
				onlPVO.setMtDcGreater2000(rs.getBigDecimal("m.dc_g_2000"));
				onlPVO.setGtNetBanking(rs.getBigDecimal("g.net_banking"));
		        onlPVO.setGtCreditCard(rs.getBigDecimal("g.credit_card"));
		        onlPVO.setGtDcLess1000(rs.getBigDecimal("g.dc_l_1000"));
		        onlPVO.setGtDc1000to2000(rs.getBigDecimal("g.dc_1000_to_2000"));
		        onlPVO.setGtDcGreater2000(rs.getBigDecimal("g.dc_g_2000"));
		        onlPVO.setIsPayableAmountEditable(rs.getString("is_amount_editable"));
		        onlPVO.setIsCalculationInclusive(rs.getString("is_calculation_inclusive")); 
		
		return onlPVO;
		
	}

   };
   
   onlinePaymentGatewayVO=(OnlinePaymentGatewayVO) namedParameterJdbcTemplate.queryForObject(str, hMap, Rmapper);
log.debug("Exit :  public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID)");
	}catch(EmptyResultDataAccessException ex){
		log.info("No online payment gateway information available for society ID "+societyID+" "+ex);
		onlinePaymentGatewayVO.setIsOnlnPymtAccptd(0);
	} catch (Exception e) {
		log.error("Exception : public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID) "+e );
	}
	return onlinePaymentGatewayVO;
	
}
	
public List getOnlinePaymentDetailsList(int societyID ){
	List onlinePaymentGatewayList=null;
	
	try {
		String str="SELECT m.*,g.* FROM pg_merchant_details m,pg_gateway_details g  WHERE m.society_id=:societyID AND m.gateway_id=g.id ORDER BY TYPE DESC  ";
		

		Map hMap=new HashMap();
		hMap.put("societyID", societyID);

		RowMapper Rmapper = new RowMapper() {
		public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
		
		OnlinePaymentGatewayVO onlPVO=new OnlinePaymentGatewayVO();
		onlPVO.setSocietyID(rs.getInt("society_id"));
		onlPVO.setMerchantID(rs.getString("merchant_id"));
		onlPVO.setMerchantKey(rs.getString("merchant_key"));
		onlPVO.setMerchantSaltKey(rs.getString("merchant_salt_key"));
		onlPVO.setGatewayName(rs.getString("name"));
		onlPVO.setGatewayUrl(rs.getString("g.url"));
		onlPVO.setIsOnlnPymtAccptd(1);
		onlPVO.setType(rs.getString("type"));
		
        onlPVO.setGtDcLess1000(rs.getBigDecimal("g.dc_l_1000"));
        onlPVO.setGtNetBanking(rs.getBigDecimal("g.net_banking"));
		return onlPVO;
		
	}

   };
   
   onlinePaymentGatewayList= namedParameterJdbcTemplate.query(
			str, hMap, Rmapper);
log.debug("Exit :  public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID)");
	}catch(EmptyResultDataAccessException ex){
		log.info("No online payment gateway information available for society ID "+societyID+" "+ex);
		
	} catch (Exception e) {
		log.error("Exception : public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID) "+e );
	}
	return onlinePaymentGatewayList;
	
}


public AccountHeadVO getTDSLedgerDetails(int societyID){
	AccountHeadVO achVO=new AccountHeadVO();
	log.debug("Entry : public AccountHeadVO getTDSLedgerDetails(int societyID)");
	try {
		String str="Select * from society_settings where society_id=:societyID order by id limit 1 ";




		Map hMap=new HashMap();
		hMap.put("societyID", societyID);




		RowMapper Rmapper = new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum)
				throws SQLException {
		
		AccountHeadVO acVO=new AccountHeadVO();
		acVO.setLedgerID(rs.getInt("tds_ledger_id"));
		
		
		return acVO;
		
	}

   };
   	achVO=(AccountHeadVO) namedParameterJdbcTemplate.queryForObject(str, hMap, Rmapper);
log.debug("Exit :  public AccountHeadVO getTDSLedgerDetails(int societyID)");
	}catch(EmptyResultDataAccessException ex){
		log.info("No online TDS ledger found for Society ID "+societyID+" "+ex);
		
	} catch (Exception e) {
		log.error("Exception : public AccountHeadVO getTDSLedgerDetails(int societyID) "+e );
	}
	return achVO;
	
}
	
/* Get Charge type list */
	public List getChargeTypeList(int societyID){
	List invoiceList=null;
	log.debug("Entry : public List getChargeTypeList(int societyID)");
	String sqlQuery=null;
	try {
		if(societyID==0){
		sqlQuery="Select * from charge_type_details order by id";
		}else
			sqlQuery=" SELECT c.* FROM charge_type_details c,scheduled_charges m WHERE m.charge_type_id=c.id AND m.org_id=:societyID GROUP BY c.id ;";
		Map hmap=new HashMap();
		hmap.put("societyID", societyID);
		
		RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
			ChargesVO chargeVO=new ChargesVO();
			chargeVO.setChargeTypeID(rs.getInt("id"));
			chargeVO.setChargeType(rs.getString("charge_type"));
			chargeVO.setDescription(rs.getString("description"));
			return chargeVO;
			}
		};
		
		invoiceList=namedParameterJdbcTemplate.query(sqlQuery, hmap, rMapper);
		
	}catch(EmptyResultDataAccessException e){
		log.info("No details available for the society "+e);
	
	} catch (Exception e) {
		log.error("Exception occurred in public List getChargeTypeList(int societyID)");
	}
	
	
	
	log.debug("Exit : public List getChargeTypeList(int societyID)");
	return invoiceList;		
}
	
	
	/* Get list of all applicable fees for the given member */
	public List getApplicableCharges(int societyID,int chargeTypeID) {
		log.debug("Entry : public List List getApplicableCharges(int societyID,String fromDate,String toDate,int chargeTypeID)");
		List chargeList = null;
		String strCondition="";
		if(chargeTypeID!=0){
			strCondition="And i.id=:invoiceTypeID";
		}
		
		
		
		
		try {
			String sql = "SELECT m.*,i.invoice_type AS chargeType,s.precision ,s.gstin "
					+ " FROM scheduled_charges m,in_invoice_type_details i ,society_settings s WHERE m.charge_type_id=i.id  AND m.org_id=s.society_id AND (m.org_id=:societyID )" +
							" AND m.status='Active'" +
							" AND (next_charge_date <=curdate()) "+strCondition+"  ORDER BY charge_sequence; ";
			log.debug(sql+"  "+societyID+"   "+chargeTypeID);
			Map hashMap = new HashMap();

			hashMap.put("societyID", societyID);
			
			hashMap.put("invoiceTypeID", chargeTypeID);
			
			

			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					ChargesVO chargeVO = new ChargesVO();
					chargeVO.setChargeID(rs.getInt("id"));
					chargeVO.setCalMethod(rs.getString("cal_method"));
					chargeVO.setGroupName(rs.getString("group_name"));
					chargeVO.setGroupTypeID(rs.getString("group_type_id"));
					chargeVO.setDebitLedgerID(rs.getInt("debit_ledger_id"));
					chargeVO.setCreditLedgerID(rs.getInt("credit_ledger_id"));
					chargeVO.setAmount(rs.getBigDecimal("amount"));
					chargeVO.setChargeSequence(rs.getInt("charge_sequence"));
					chargeVO.setStartDate(rs.getString("start_date"));
					chargeVO.setEndDate(rs.getString("end_date"));
					chargeVO.setChargeType(rs.getString("chargeType"));
					chargeVO.setChargeTypeID(rs.getInt("charge_type_id"));
					chargeVO.setDescription(rs.getString("description"));
					chargeVO.setNextChargeDate(rs.getString("next_charge_date"));
					chargeVO.setTxDate(rs.getString("tx_date"));
					chargeVO.setChargeFrequency(rs.getString("charge_frequency"));
					chargeVO.setCreateInvoices(rs.getInt("generate_invoice"));
					chargeVO.setPrecison(rs.getInt("precision"));
					chargeVO.setItemID(rs.getInt("item_id"));
					chargeVO.setGstRate(rs.getBigDecimal("gst"));
					chargeVO.setSocGstIN(rs.getString("gstin"));
					chargeVO.setIsApplicable(false);
					
					return chargeVO;
				}
			};
			chargeList = namedParameterJdbcTemplate.query(sql, hashMap,rMapper);

		} catch (Exception e) {
			log.error("Exception at List getApplicableCharges(int societyID,String fromDate,String toDate,int chargeTypeID) " + e);
		}
		log.debug("Exit : public List List getApplicableCharges(int societyID,String fromDate,String toDate,int chargeTypeID))"+chargeList.size());
		return chargeList;
	}
	
	/* Get list of all penalty charges for the given society */
	public List getApplicablePenaltyCharges(int societyID) {
		log.debug("Entry : public List getApplicablePenaltyCharges(int societyID)");
		List chargeList = null;
		
		
		
		
		
		try {
			String sql = "SELECT m.*,s.precision ,s.gstin"
					+ " FROM penalty_charge_details m ,society_settings s WHERE  m.society_id=s.society_id AND (m.society_id=:societyID )" +
							" AND m.status='Active'" +
							" AND (next_charge_date <=curdate())  ; ";
			log.debug(sql+"  "+societyID);
			Map hashMap = new HashMap();

			hashMap.put("societyID", societyID);
			
			
			
			

			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					PenaltyChargesVO chargeVO = new PenaltyChargesVO();
					chargeVO.setPenaltyChargeID(rs.getInt("id"));
					chargeVO.setCalMethod(rs.getString("cal_method"));
					chargeVO.setCalMode(rs.getString("cal_mode"));
					chargeVO.setLedgerID(rs.getInt("ledger_id"));					
					chargeVO.setAmount(rs.getBigDecimal("amount"));					
					chargeVO.setStartDate(rs.getString("start_date"));
					chargeVO.setEndDate(rs.getString("end_date"));
					chargeVO.setChargeTypeID(rs.getInt("charge_type_id"));
					chargeVO.setDescription(rs.getString("description"));
					chargeVO.setNextChargeDate(rs.getString("next_charge_date"));
					chargeVO.setPrevChargeDate(rs.getString("prev_charge_date"));
					chargeVO.setPenaltyChargeDate(rs.getString("penalty_charge_date"));
					chargeVO.setChargeFrequency(rs.getString("charge_frequency"));
					chargeVO.setCreateInvoices(rs.getInt("generate_invoice"));
					chargeVO.setTxDate(rs.getString("tx_date"));
					chargeVO.setPrecison(rs.getInt("precision"));
					chargeVO.setItemID(rs.getInt("item_id"));
					chargeVO.setSocGstIn(rs.getString("gstin"));
					chargeVO.setIsApplicable(false);
					
					return chargeVO;
				}
			};
			chargeList = namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
		} catch (EmptyResultDataAccessException e) {
			log.info("No Penalty charges available for Society "+societyID+"  " + e);
		
		
		} catch (Exception e) {
			log.error("Exception at public List getApplicablePenaltyCharges(int societyID) " + e);
		}
		log.debug("Exit : public List getApplicablePenaltyCharges(int societyID)"+chargeList.size());
		return chargeList;
	}
	
	/* Get details of a charge  */
	public ChargesVO getChargeDetails(int chargeID,int societyID){
		ChargesVO chargeVO=new ChargesVO();
		
		log.debug("Entry : public List getChargeDetails(int chargeID,int societyID)");
		
		try{
			if(chargeID!=0){
			String sql = "SELECT *  FROM scheduled_charges  WHERE id=:chargeID and org_id=:societyID ;";
			
		Map hashMap = new HashMap();

		
		hashMap.put("chargeID", chargeID);
		hashMap.put("societyID", societyID);
		

		RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				ChargesVO chargesVO = new ChargesVO();
				chargesVO.setChargeID(rs.getInt("id"));
				
				chargesVO.setChargeFrequency(rs.getString("charge_frequency"));
				
				chargesVO.setDescription(rs.getString("description"));
				
					
			
				
				return chargesVO;
			}
		};
			
			chargeVO=(ChargesVO) namedParameterJdbcTemplate.queryForObject(sql, hashMap,rMapper);
			}
			}catch(EmptyResultDataAccessException e){
				log.info("No details available for the given charge ID "+e);
			}
		catch(Exception e){
				log.error("Exception in InvoiceLineItemsVO getChargeDetails "+e);
			}
		
		log.debug("Exit : public List getChargeDetails(int chargeID,int societyID)");
		return chargeVO;		
	}
	
	
	/* Update the charge period after adding the invoice  */
	public int updateChargeStatus(int societyID,ChargesVO chargevO){
		int insertFlag=0;
		log.debug("Entry : public int updateChargeStatus(int societyID,ChargesVO chargevO)"+chargevO.getChargeID());
		String sql="";
		String condition="";
		if(chargevO.getChargeID()!=0){
		try {
			if(chargevO.getChargeFrequency().equalsIgnoreCase("OneTime")){
				sql="Update scheduled_charges set status='Inactive' where id=:ID and org_id=:societyID ;";
			}else{
				if(chargevO.getChargeFrequency().equalsIgnoreCase("Monthly")){
					condition="INTERVAL 1 MONTH";
				}else if(chargevO.getChargeFrequency().equalsIgnoreCase("BIMONTHLY")){
					condition="INTERVAL 2 MONTH";
				}else if(chargevO.getChargeFrequency().equalsIgnoreCase("triannualy")){
					condition="INTERVAL 4 MONTH";
				}
				else if(chargevO.getChargeFrequency().equalsIgnoreCase("Quarterly")){
					condition="INTERVAL 3 MONTH";
				}else if(chargevO.getChargeFrequency().equalsIgnoreCase("HalfYearly")){
					condition="INTERVAL 6 MONTH";
				}else if(chargevO.getChargeFrequency().equalsIgnoreCase("Yearly")){
					condition="INTERVAL 1 YEAR";
				}
				
				sql="Update scheduled_charges set next_charge_date=DATE_ADD(next_charge_date, "+condition+" ),tx_date=DATE_ADD(tx_date, "+condition+" ) where id=:ID and org_id=:societyID; ";
			}
			
			Map hashMap = new HashMap();

			hashMap.put("societyID",societyID);
			hashMap.put("ID", chargevO.getChargeID());
			
			insertFlag=namedParameterJdbcTemplate.update(sql, hashMap);
		} catch (Exception e) {
		log.error("Exception at updateChargeStatus "+e);
		}
		}
		log.debug("Exit : public int updateChargeStatus(int societyID,ChargesVO chargevO)"+insertFlag);
		return insertFlag;		
	}
	
	

		/* Update the Societys Late fee charging date  */
	public int updateLateFeeChargeDate(int societyID,ChargesVO chargeVO){
		int insertFlag=0;
		log.debug("Entry : public int updateLateFeeChargeDate(int societyID,ChargesVO chargeVO)"+chargeVO.getChargeID());
		String sql="";
		String condition="";
		
		try {
			if(chargeVO.getChargeID()!=0){
			if(chargeVO.getChargeFrequency().equalsIgnoreCase("OneTime")){
				sql="Update society_settings set invoice_date='NULL' where society_id=:societyID ;";
			}else{
				if(chargeVO.getChargeFrequency().equalsIgnoreCase("Monthly")){
					condition="INTERVAL 1 MONTH";
				}else if(chargeVO.getChargeFrequency().equalsIgnoreCase("BIMONTHLY")){
					condition="INTERVAL 2 MONTH";
				}
				else if(chargeVO.getChargeFrequency().equalsIgnoreCase("Quarterly")){
					condition="INTERVAL 3 MONTH";
				}else if(chargeVO.getChargeFrequency().equalsIgnoreCase("HalfYearly")){
					condition="INTERVAL 6 MONTH";
				}else if(chargeVO.getChargeFrequency().equalsIgnoreCase("Yearly")){
					condition="INTERVAL 1 YEAR";
				}
				
				sql="Update society_settings set invoice_date=DATE_ADD(invoice_date, "+condition+" ) where  society_id=:societyID; ";
			}
			
			Map hashMap = new HashMap();

			hashMap.put("societyID",societyID);
			hashMap.put("ID", chargeVO.getChargeID());
			
			insertFlag=namedParameterJdbcTemplate.update(sql, hashMap);
			}
			} catch (Exception e) {
		log.error("Exception at updateChargeStatus "+e);
		}
		
		log.debug("Exit : public int updateLateFeeChargeDate(int societyID,int apartmentID)"+insertFlag);
		return insertFlag;		
	}
	
	/* Get details of a Penalty charge  */
	public ChargesVO getPenaltyChargeDetails(int chargeID,int societyID){
		ChargesVO chargeVO=new ChargesVO();
		
		log.debug("Entry : public List getChargeDetails(int chargeID,int societyID)");
		
		try{
			if(chargeID!=0){
			String sql = "SELECT *  FROM penalty_charge_details  WHERE id=:chargeID and society_id=:societyID ;";
			
		Map hashMap = new HashMap();

		
		hashMap.put("chargeID", chargeID);
		hashMap.put("societyID", societyID);
		

		RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				ChargesVO chargesVO = new ChargesVO();
				chargesVO.setChargeID(rs.getInt("id"));
				
				chargesVO.setChargeFrequency(rs.getString("charge_frequency"));
				
				chargesVO.setDescription(rs.getString("description"));
				
					
			
				
				return chargesVO;
			}
		};
			
			chargeVO=(ChargesVO) namedParameterJdbcTemplate.queryForObject(sql, hashMap,rMapper);
			}
			}catch(EmptyResultDataAccessException e){
				log.info("No details available for the given charge ID "+e);
			}
		catch(Exception e){
				log.error("Exception in InvoiceLineItemsVO getChargeDetails "+e);
			}
		
		log.debug("Exit : public List getChargeDetails(int chargeID,int societyID)");
		return chargeVO;		
	}
	
	/* Update the charge period after adding the invoice  */
	public int updatePenaltyChargeStatus(int societyID,ChargesVO chargevO){
		int insertFlag=0;
		log.debug("Entry : public int updatePenaltyChargeStatus(int societyID,ChargesVO chargevO)"+chargevO.getChargeID());
		String sql="";
		String condition="";
		if(chargevO.getChargeID()!=0){
		try {
			if(chargevO.getChargeFrequency().equalsIgnoreCase("OneTime")){
				sql="Update penalty_charge_details set status='Inactive' where id=:ID and society_id=:societyID ;";
			}else{
				if(chargevO.getChargeFrequency().equalsIgnoreCase("Monthly")){
					condition="INTERVAL 1 MONTH";
				}else if(chargevO.getChargeFrequency().equalsIgnoreCase("BIMONTHLY")){
					condition="INTERVAL 2 MONTH";
				}
				else if(chargevO.getChargeFrequency().equalsIgnoreCase("Quarterly")){
					condition="INTERVAL 3 MONTH";
				}else if(chargevO.getChargeFrequency().equalsIgnoreCase("HalfYearly")){
					condition="INTERVAL 6 MONTH";
				}else if(chargevO.getChargeFrequency().equalsIgnoreCase("Yearly")){
					condition="INTERVAL 1 YEAR";
				}
				//IF(LAST_DAY(penalty_charge_date), LAST_DAY(DATE_ADD((penalty_charge_date),INTERVAL 1 MONTH)),DATE_ADD((penalty_charge_date),INTERVAL 1 MONTH))
				sql="Update penalty_charge_details set next_charge_date=IF(LAST_DAY(next_charge_date) ,DATE_ADD((next_charge_date),"+condition+"), LAST_DAY(DATE_ADD((next_charge_date),"+condition+"))),"
						+ "prev_charge_date=IF(LAST_DAY(prev_charge_date) ,DATE_ADD((prev_charge_date),"+condition+"), LAST_DAY(DATE_ADD((prev_charge_date),"+condition+"))) , "
						+ "penalty_charge_date=IF(LAST_DAY(penalty_charge_date) ,DATE_ADD((penalty_charge_date),"+condition+"), LAST_DAY(DATE_ADD((penalty_charge_date),"+condition+")))  "
						+ ",tx_date=IF(LAST_DAY(tx_date),DATE_ADD((tx_date),"+condition+"), LAST_DAY(DATE_ADD((tx_date),"+condition+")))  where id=:ID and society_id=:societyID; ";
			}
			
			Map hashMap = new HashMap();

			hashMap.put("societyID",societyID);
			hashMap.put("ID", chargevO.getChargeID());
			
			insertFlag=namedParameterJdbcTemplate.update(sql, hashMap);
		} catch (Exception e) {
		log.error("Exception at updateChargeStatus "+e);
		}
		}
		log.debug("Exit : public int updateChargeStatus(int societyID,ChargesVO chargevO)"+insertFlag);
		return insertFlag;		
	}
	
	/* Add raw bank statements after parsing    */
	public int addBankStatements(BankStatement bankStmt){
		int insertFlag=0;
		log.debug("Entry : public int addBankStatements(List bankStatementList)");
		try{
			   java.util.Date today = new java.util.Date();
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			
			String sql = "Insert into `import_bank_statement_details`(`bank_acc_no`,`tx_date`,`bank_date`,`tx_mode`,`ref_no`,`deposit`,`withdrawal`,`description`,`balance`,`raw_data`,`batch_id`,`insert_date`,`bank_ledger_id`,`suspense_ledger_id`,`org_id`,`tx_type`) VALUES"
					+ " (:bankAccNo,:txDate,:bankDate,:txMode,:refNo,:deposit,:withdrawal,:description,:balance,:rawData,:batchID,:insertDate,:bankLedgerID,:suspenseLedgerID,:orgID,:txType); ";
			
			Map hashMap = new HashMap();

			
			hashMap.put("bankAccNo", bankStmt.getBankAccNumber());
			hashMap.put("txDate", bankStmt.getValueDate());
			hashMap.put("bankDate", bankStmt.getBookingDate());
			hashMap.put("txMode", bankStmt.getPayMode());
			hashMap.put("refNo", bankStmt.getReferenceNumber());
			hashMap.put("deposit", bankStmt.getDeposit());
			hashMap.put("withdrawal", bankStmt.getWithdrawal());
			hashMap.put("description", bankStmt.getDescription());
			hashMap.put("balance", bankStmt.getBalance());
			hashMap.put("rawData", bankStmt.getRawData());
			hashMap.put("batchID", bankStmt.getBatchID());
			hashMap.put("insertDate", currentTimestamp);
			hashMap.put("bankLedgerID", bankStmt.getBankLedgerID());
			hashMap.put("suspenseLedgerID", bankStmt.getSuspenceLedgerID());
			hashMap.put("orgID", bankStmt.getOrgID());
			hashMap.put("txType", bankStmt.getTxType());
			
			
					insertFlag=namedParameterJdbcTemplate.update(sql, hashMap);
					
					
		} catch (Exception e) {
		log.error("Exception at addBankStatements "+e);
		}
		
		log.debug("Exit : public int addBankStatements(List bankStatementList)"+insertFlag);
		return insertFlag;		
	}
	
	/* Remove bank statements for the period    */
	public int deleteBankStatements(BankStatement bankStmt){
		int insertFlag=0;
		log.debug("Entry : public int deleteBankStatements(String fromDate,String toDate)");
		try{
			   java.util.Date today = new java.util.Date();
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			
			String sql = "delete from import_bank_statement_details where bank_acc_no=:bankAccNo and (tx_date between  :fromDate and :toDate)";
			
			Map hashMap = new HashMap();

			
			hashMap.put("bankAccNo", bankStmt.getBankAccNumber());
			hashMap.put("fromDate", bankStmt.getFromDate());
			hashMap.put("toDate", bankStmt.getToDate());
		
			insertFlag=namedParameterJdbcTemplate.update(sql, hashMap);
					
					
		} catch (Exception e) {
		log.error("Exception at public int deleteBankStatements(String fromDate,String toDate) "+e);
		}
		
		log.debug("Exit : public int deleteBankStatements(String fromDate,String toDate)"+insertFlag);
		return insertFlag;		
	}
	
	/* Get the last date of imported statement    */
	public BankStatement getLastBankStatementsObj(final BankStatement bankStmt){
		int insertFlag=0;
		BankStatement bankStatement=new BankStatement();
		log.debug("Entry : public BankStatement getLastBankStatementsObj(BankStatement bankStmt)");
		try{
			   java.util.Date today = new java.util.Date();
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			
			String sql = "select MAX(tx_date) AS to_date from import_bank_statement_details where bank_acc_no=:bankAccNo and (tx_date between  :fromDate and :toDate)";
			
			Map hashMap = new HashMap();

			
			hashMap.put("bankAccNo", bankStmt.getBankAccNumber());
			hashMap.put("fromDate", bankStmt.getFromDate());
			hashMap.put("toDate", bankStmt.getToDate());
			
			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					BankStatement bankStatementVO=new BankStatement();
					bankStatementVO.setToDate(rs.getDate("to_date"));
					bankStatementVO.setBankAccNumber(bankStmt.getBankAccNumber());
															
					return bankStatementVO;
				}
			};
		
			bankStatement=(BankStatement)namedParameterJdbcTemplate.queryForObject(sql, hashMap, rMapper);
			
		}catch(EmptyResultDataAccessException e){
		
			log.error("Exception at public BankStatement getLastBankStatementsObj(BankStatement bankStmt) : No bank statement is available for period "+e);
			bankStatement.setBankAccNumber("0");
		} catch (Exception e) {
		log.error("Exception at public BankStatement getLastBankStatementsObj(BankStatement bankStmt)"+e);
		}
		
		log.debug("Exit : public BankStatement getLastBankStatementsObj(BankStatement bankStmt)"+insertFlag);
		return bankStatement	;	
	}
	
	/* Add raw bank statements after parsing    */
	public List getMatchingTransactionList(BankStatement bankStatementVO){
		List matchingTxs=new ArrayList<>();
		log.debug("Entry : public List getMatchingTransactionList(BankStatement bankStatementVO)");
		try{
			SocietyVO societyVO=societyService.getSocietyDetails(bankStatementVO.getOrgID()) ;
		String sql="select  * from account_transactions_"+societyVO.getDataZoneID()+"  where  amount=:amount and ref_no=:refNO and is_deleted=0 and org_id=:orgID; ";
		
		log.info(sql);
		Map hashMap = new HashMap();

		hashMap.put("amount",bankStatementVO.getTxAmount());
		hashMap.put("refNO", bankStatementVO.getReferenceNumber());
		hashMap.put("orgID", bankStatementVO.getOrgID());
		
		RowMapper rMapper = new RowMapper() {

			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				TransactionVO txVO=new TransactionVO();
				txVO.setTransactionID(rs.getInt("tx_id"));
				txVO.setSocietyID(rs.getInt("org_id"));
				txVO.setAmount(rs.getBigDecimal("amount"));
				txVO.setTransactionDate(rs.getString("tx_date"));
				txVO.setDescription(rs.getString("description"));
				txVO.setRefNumber(rs.getString("ref_no"));
													
				return txVO;
			}
		};
		
		matchingTxs=namedParameterJdbcTemplate.query(sql, hashMap,rMapper);
		
		} catch (Exception e) {
		log.error("Exception at public List getMatchingTransactionList(BankStatement bankStatementVO) "+e);
		}
		
		log.debug("Exit : public List getMatchingTransactionList(BankStatement bankStatementVO)"+matchingTxs.size());
		return matchingTxs;		
	}
	
	
	/*Reconcile the imported statements    */
	public int  reconcileTransactionsWithBankStatement(BankStatement bankStmt){
		int updateFlag=0;
		log.debug("Entry : public int  reconcileTransactionsWithBankStatement(BankStatement bankStmt)");
		try{
			SocietyVO societyVO=societyService.getSocietyDetails(bankStmt.getOrgID()) ;  			
			String sql = " update account_transactions_"+societyVO.getDataZoneID()+" a,import_bank_statement_details b set b.tx_id=a.tx_id , a.bank_date=b.bank_date WHERE a.ref_no=b.ref_no AND (a.amount=b.deposit OR a.amount=b.withdrawal) AND b.org_id=:orgID and a.org_id=b.org_id AND a.is_deleted=0 AND a.tx_type!='Journal' AND a.ref_no!='' and  (b.tx_date between :fromDate and :toDate) and  (b.tx_id is null or b.tx_id=0) and b.bank_acc_no=:bankAccNo and b.tx_date=a.tx_date  ;";
			
			Map hashMap = new HashMap();

			
			hashMap.put("bankAccNo", bankStmt.getBankAccNumber());
			hashMap.put("fromDate", bankStmt.getFromDate());
			hashMap.put("toDate", bankStmt.getToDate());
			hashMap.put("orgID", bankStmt.getOrgID());
			
			
			updateFlag=namedParameterJdbcTemplate.update(sql, hashMap);
		
		}catch(EmptyResultDataAccessException e){
		
			log.error("Exception at public int  reconcileTransactionsWithBankStatement(BankStatement bankStmt) : No bank statement is available for period "+e);
		
		} catch (Exception e) {
		log.error("Exception at public int  reconcileTransactionsWithBankStatement(BankStatement bankStmt)"+e);
		}
		
		log.debug("Exit : public int  reconcileTransactionsWithBankStatement(BankStatement bankStmt)"+updateFlag);
		return updateFlag	;	
	}
	
	
	/* Add PayUMoney statements after parsing    */
	public int addPayUStatements(OnlinePaymentGatewayVO payUVO){
		int insertFlag=0;
		log.debug("Entry : public int addPayUStatements(OnlinePaymentGatewayVO payUVO)");
		try{
			   java.util.Date today = new java.util.Date();
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			
			String sql = "insert into `payment_gateway_statement_details`(`org_id`,`tx_date`,`member_ledger_id`,`description`,`child_payment_id`,`total_amount`,`commission_amount`,`payu_tdr`,`actual_amount`,`commission_received`,`parent_utr`,`customer_name`,`email`,`phone`,`source_type`,`parent_payment_id`,`child_utr`,`insert_date`,`merchant_salt_key`) VALUES"
					+ " (:orgID,:txDate,:memberLedgerID,:description,:childPaymentID,:totalAmount,:commissionAmount,:payUTDR,:actualAmount,:commissionReceived,:parentUtr,:customerName,:email,:phone,:sourceType,:parentPaymentID,:childUtr,:insertDate,:merchantSaltKey); ";
			
			Map hashMap = new HashMap();

			
			hashMap.put("orgID", payUVO.getOrgID());
			hashMap.put("txDate", payUVO.getTxDate());
			hashMap.put("memberLedgerID", payUVO.getMemberLedgerID());
			hashMap.put("description", payUVO.getDescription());
			hashMap.put("childPaymentID", payUVO.getChildPaymentID());
			hashMap.put("totalAmount", payUVO.getTotalAmount());
			hashMap.put("commissionAmount", payUVO.getCommissionAmount());
			hashMap.put("payUTDR", payUVO.getGtProcessingFees());
			hashMap.put("actualAmount", payUVO.getActualAmount());
			hashMap.put("commissionReceived", payUVO.getMtProcessingFees());
			hashMap.put("parentUtr", payUVO.getParentUtr());
			hashMap.put("customerName", payUVO.getCustomerName());
			hashMap.put("insertDate", currentTimestamp);
			hashMap.put("email", payUVO.getCustomerEmail());
			hashMap.put("phone", payUVO.getCustomerPhone());
			hashMap.put("sourceType", payUVO.getPaymentMode());
			hashMap.put("parentPaymentID", payUVO.getParentPaymentID());
			hashMap.put("childUtr", payUVO.getChildUtr());
			hashMap.put("merchantSaltKey", payUVO.getMerchantSaltKey());
			
			
					insertFlag=namedParameterJdbcTemplate.update(sql, hashMap);
					
					
		} catch (Exception e) {
		log.error("Exception at addPayUStatements(OnlinePaymentGatewayVO payUVO) "+e);
		}
		
		log.debug("Exit : public int addPayUStatements(OnlinePaymentGatewayVO payUVO)"+insertFlag);
		return insertFlag;		
	}
	
	
	
	
	public List getInvoiceRceiptList(int orgID,int invoiceID){
		List invoiceReceiptList=null;
	
			log.debug("Entry :  public List getInvoiceRceiptList(int orgID,int invoiceID)");
			try{
				SocietyVO societyVO=societyService.getSocietyDetails(orgID);
				String str="Select txTable.*,s.tx_id,s.tag_values,s.accountant_comments,s.auditor_comments,SUM(amount) AS txAmount FROM (SELECT ats.org_id,ats.tx_id as TXID,ats.doc_id,ats.tx_date,ats.ref_no,ats.create_date,ats.bank_date,ats.description,ats.tx_number,ats.tx_type,tx_mode,ats.created_by,ats.updated_by,m.full_name AS createdBy,mi.full_name AS updatedBy,r.amount, table_primary.type, table_primary.tx_id,table_secondary.ledger_id,account_ledgers_"+societyVO.getDataZoneID()+".ledger_name ,sub_group_id,amt_in_word,r.receipt_id,r.id As receiptLinkID "+
						   " FROM account_ledger_entry_"+societyVO.getDataZoneID()+" table_primary,account_ledger_entry_"+societyVO.getDataZoneID()+" table_secondary,account_ledgers_"+societyVO.getDataZoneID()+",account_transactions_"+societyVO.getDataZoneID()+" ats,um_users m ,um_users mi ,receipt_invoice_mapping_"+societyVO.getDataZoneID()+" r "+
						   " WHERE table_primary.tx_id = table_secondary.tx_id"+
						   " AND table_primary.org_id=table_secondary.org_id"+
						   " AND r.receipt_id=ats.tx_id "+
						   " AND r.invoice_id=:invoiceID "+
						   " AND table_primary.type!=table_secondary.type "+						 
						   " AND account_ledgers_"+societyVO.getDataZoneID()+".id = table_secondary.ledger_id  "+
						   " AND account_ledgers_"+societyVO.getDataZoneID()+".org_id=table_secondary.org_id and ats.org_id=table_secondary.org_id "+
						   " AND ats.tx_id = table_primary.tx_id  "+ 
					       " AND ats.is_deleted=0 AND m.id=ats.created_by AND mi.id=ats.updated_by and ats.org_id GROUP BY table_secondary.tx_id ORDER BY ats.tx_date ) AS txTable LEFT JOIN soc_tx_meta s ON txTable.TXID=s.tx_id AND s.society_id=:orgID GROUP BY txTable.ledger_id,txTable.tx_id ORDER BY txTable.tx_date; ";
		
				   
				   Map hMap=new HashMap();
				   hMap.put("orgID", orgID);
				   hMap.put("invoiceID", invoiceID);
				  
				  
				   RowMapper Rmapper = new RowMapper() {
						public Object mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							List accheadVOList=null;
							TransactionVO txVO=new TransactionVO();
							txVO.setTransactionID(rs.getInt("tx_id"));
							txVO.setSocietyID(rs.getInt("org_id"));
							txVO.setCreateDate(rs.getTimestamp("create_date"));
							txVO.setTransactionDate(rs.getString("tx_date"));
							txVO.setBankDate(rs.getString("bank_date"));
							txVO.setDescription(rs.getString("description"));
							txVO.setDocID(rs.getString("doc_id"));
							txVO.setTransactionNumber(rs.getString("tx_number"));
							txVO.setRefNumber(rs.getString("ref_no"));
							txVO.setTransactionType(rs.getString("tx_type"));
							txVO.setTransactionMode(rs.getString("tx_mode"));
							txVO.setCreditDebitFlag(rs.getString("type"));
							txVO.setAmount(rs.getBigDecimal("amount"));
							txVO.setLedgerName(rs.getString("ledger_name"));
							txVO.setGroupID(rs.getInt("sub_group_id"));
							txVO.setCreatorName(rs.getString("createdBy"));
							txVO.setUpdaterName(rs.getString("updatedBy"));
							txVO.setAmtInWord(rs.getString("amt_in_word"));
							txVO.setHashComments(rs.getString("tag_values"));
							txVO.setAccComments(rs.getString("accountant_comments"));
							txVO.setAuditorComments(rs.getString("auditor_comments"));
							txVO.setLedgerID(rs.getInt("ledger_id"));
							txVO.setReceiptID(rs.getInt("receipt_id"));
							txVO.setReceiptLinkID(rs.getInt("receiptLinkID"));
							if(rs.getString("type").equalsIgnoreCase("C")){
							
									txVO.setCreditAmt(txVO.getAmount().abs());
								
								}else{
									
										txVO.setDebitAmt(txVO.getAmount().abs());
								
								}
							
							
							return txVO;
							
						}

					   };
					   invoiceReceiptList=namedParameterJdbcTemplate.query(
							str, hMap, Rmapper);
				   log.debug("Exit :  public List getInvoiceRceiptList(int orgID,int invoiceID)"+invoiceReceiptList.size());
				}catch(EmptyResultDataAccessException Ex){
					log.info("No Data found at  List getInvoiceRceiptList(int orgID,int invoiceID) "+Ex);
				
			
					}catch(Exception se){
					log.error("Exception :public List getInvoiceRceiptList(int orgID,int invoiceID)"+se);
				   
				}
			
			log.debug("Exit :  public List getInvoiceRceiptList(int orgID,int invoiceID)");
		return invoiceReceiptList;
		}
	

	public List getUnlinkedRceiptList(int orgID,int ledgerID, String fromDate, String toDate){
		List invoiceReceiptList=null;
	
			log.debug("Entry :  public List getUnlinkedRceiptList(int orgID,int ledgerID)");
			try{
				SocietyVO societyVO=societyService.getSocietyDetails(orgID);
				String str="SELECT a.*,ABS(SUM(amt))-IFNULL(SUM(r.amount),0)  AS avBal,a.amt,r.id,r.receipt_id,r.amount as recAmt FROM (SELECT a.*,l.amount AS amt,l.ledger_id,al.ledger_name "
						+ "FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" l,account_ledgers_"+societyVO.getDataZoneID()+" al WHERE a.tx_id=l.tx_id AND al.id=l.ledger_id AND a.is_deleted=0 AND l.ledger_id=:ledgerID and l.org_id=:orgID and a.org_id=l.org_id and a.org_id=al.org_id AND ( a.tx_type='Receipt' OR  a.tx_type='Payment' OR a.tx_type='Credit Note' OR a.tx_type='Debit Note' ) AND (a.tx_date BETWEEN :fromDate AND :toDate)  ) AS a LEFT JOIN ( SELECT id,org_id,receipt_id,invoice_id,SUM(amount) AS amount FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" where org_id=:orgID AND ( ledger_id=:ledgerID OR ledger_id=0 OR ledger_id IS NULL) group by receipt_id ) r ON r.receipt_id=a.tx_id AND a.org_id=r.org_id GROUP BY a.tx_id;";
		
				log.debug("query: "+str);
				
				   Map hMap=new HashMap();
				   hMap.put("orgID", orgID);
				   hMap.put("ledgerID", ledgerID);
				   hMap.put("fromDate", fromDate);
				   hMap.put("toDate", toDate);
				  
				   RowMapper Rmapper = new RowMapper() {
						public Object mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							List accheadVOList=null;
							TransactionVO txVO=new TransactionVO();
							txVO.setTransactionID(rs.getInt("tx_id"));
							txVO.setCreateDate(rs.getTimestamp("create_date"));
							txVO.setTransactionDate(rs.getString("tx_date"));
							txVO.setBankDate(rs.getString("bank_date"));
							txVO.setDescription(rs.getString("description"));
							txVO.setDocID(rs.getString("doc_id"));
							txVO.setTransactionNumber(rs.getString("tx_number"));
							txVO.setRefNumber(rs.getString("ref_no"));
							txVO.setTransactionType(rs.getString("tx_type"));
							txVO.setTransactionMode(rs.getString("tx_mode"));
							txVO.setAmount(rs.getBigDecimal("amount"));
							txVO.setAmtInWord(rs.getString("amt_in_word"));
							txVO.setLedgerID(rs.getInt("ledger_id"));
							txVO.setBalance(rs.getBigDecimal("avBal"));
							txVO.setReceiptID(rs.getInt("receipt_id"));
							txVO.setReceiptLinkID(rs.getInt("id"));
							txVO.setInvoiceID(rs.getInt("invoice_id"));
							txVO.setLedgerName(rs.getString("ledger_name"));				
							
							return txVO;
							
						}

					   };
					   invoiceReceiptList=namedParameterJdbcTemplate.query(
							str, hMap, Rmapper);
				   log.debug("Exit :  public List getUnlinkedRceiptList(int orgID,int ledgerID)"+invoiceReceiptList.size());
				}catch(EmptyResultDataAccessException Ex){
					log.info("No Data found at  List getUnlinkedRceiptList(int orgID,int invoiceID) "+Ex);
				
			
					}catch(Exception se){
					log.error("Exception :public List getUnlinkedRceiptList(int orgID,int ledgerID)"+se);
				   
				}
			
			log.debug("Exit : public List getUnlinkedRceiptList(int orgID,int ledgerID)");
		return invoiceReceiptList;
		}
	
	/* Update invoice balance with the transaction */
	public int insertReceiptAgainstInvoice(int orgID ,TransactionVO txVO){
		int success=0;
		log.debug("Entry : public int insertReceiptAgainstInvoice(int orgID ,TransactionVO txVO)");
		
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String sqlQuery="INSERT INTO `receipt_invoice_mapping_"+societyVO.getDataZoneID()+"`(`receipt_id`,`bill_id`,`invoice_id`,`amount`,`org_id`,`ledger_id`)" +
							" VALUES ( :receiptID,:billID,:invoiceID,:balance ,:orgID, :ledgerID)"; 
		log.debug("query : " + sqlQuery);
			Map hmap=new HashMap();
			
			hmap.put("receiptID", txVO.getTransactionID());
			hmap.put("billID",txVO.getBillID());
			hmap.put("invoiceID",txVO.getInvoiceID());
			hmap.put("balance", txVO.getBalance());	
			hmap.put("orgID", orgID);
			hmap.put("ledgerID", txVO.getLedgerID());
			
			success=namedParameterJdbcTemplate.update(sqlQuery, hmap);
			
		} catch (Exception e) {
			log.error("Exception at public int insertReceiptAgainstInvoice(int orgID ,TransactionVO txVO) "+e);
		}
		
		log.debug("Exit : public int insertReceiptAgainstInvoice(int orgID ,TransactionVO txVO)");
		return success;		
	}
	
	
	/* Unlink  receipt from invoice */
	public int unlinkReceiptFromInvoice(int orgID ,TransactionVO txVO){
		int success=0;
		log.debug("Entry : public int unlinkReceiptFromInvoice(int orgID ,TransactionVO txVO)");
		
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			   String str = "Delete from receipt_invoice_mapping_"+societyVO.getDataZoneID()+" where id=:receiptLinkID and receipt_id=:receiptID and invoice_id=:invoiceID and org_id=:orgID ;";
			 
			   
			   Map hMap=new HashMap();
			   hMap.put("receiptLinkID", txVO.getReceiptLinkID());
			   hMap.put("receiptID", txVO.getReceiptID());
			   hMap.put("invoiceID", txVO.getInvoiceID());
			   hMap.put("orgID", orgID);
			   
			   
			   success=namedParameterJdbcTemplate.update(str, hMap);
			
			
		} catch (Exception e) {
			log.error("Exception at public int unlinkReceiptFromInvoice(int orgID ,TransactionVO txVO) "+e);
		}
		
		log.debug("Exit : public int unlinkReceiptFromInvoice(int orgID ,TransactionVO txVO)");
		return success;		
	}
	
	public List getPaymentReceiptListReport(int orgID,int ledgerID, String fromDate, String toDate,String type){
		List txList=null;
	
			log.debug("Entry :  public List getPaymentReceiptListReport(int orgID,int ledgerID)");
			String ledgerCondition="";
			String typeCondition="";
			String str="";
			try{
				
				if(type.equalsIgnoreCase("P")){
					typeCondition="'Payment' AND l.type='D'";
				}else if(type.equalsIgnoreCase("R")){
					typeCondition="'Receipt' AND l.type='C'";
				}
				
				
				SocietyVO societyVO=societyService.getSocietyDetails(orgID);
				
				if(ledgerID!=0){
					ledgerCondition="AND primary_table.ledger_id=:ledgerID";
					if(type.equalsIgnoreCase("P")){
						typeCondition="'Payment' ";
					}else if(type.equalsIgnoreCase("R")){
						typeCondition="'Receipt'";
					}
					
					str="SELECT a.*,ABS(SUM(a.amt))-IFNULL(SUM(r.amount),0)  AS avBal,a.amt,r.id,r.receipt_id,r.amount as recAmt FROM (SELECT a.*,primary_table.amount AS amt,primary_table.ledger_id,al.ledger_name "
						+ "FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" primary_table,account_ledger_entry_"+societyVO.getDataZoneID()+" secondary_table,account_ledgers_"+societyVO.getDataZoneID()+" al WHERE a.tx_id=primary_table.tx_id "+ledgerCondition+" AND al.id=secondary_table.ledger_id AND  primary_table.type!=secondary_table.type AND primary_table.tx_id=secondary_table.tx_id AND a.is_deleted=0  AND primary_table.org_id=:orgID AND a.org_id=primary_table.org_id AND a.org_id=al.org_id AND a.tx_type="+typeCondition+" AND (a.tx_date BETWEEN :fromDate AND :toDate) GROUP BY secondary_table.tx_id ORDER BY a.tx_date  ) AS a LEFT JOIN ( SELECT id,org_id,receipt_id,invoice_id,SUM(amount) AS amount FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" where org_id=:orgID AND ( ledger_id=:ledgerID OR ledger_id=0 OR ledger_id IS NULL) group by receipt_id ) r ON r.receipt_id=a.tx_id AND a.org_id=r.org_id GROUP BY a.tx_id;";
					
				}else{
					
					if(type.equalsIgnoreCase("P")){
						typeCondition="'Payment' AND l.type='D'";
					}else if(type.equalsIgnoreCase("R")){
						typeCondition="'Receipt' AND l.type='C'";
					}
								
				str="SELECT a.*,ABS(SUM(a.amt))-IFNULL(SUM(r.amount),0)  AS avBal,a.amt,r.id,r.receipt_id,r.amount as recAmt FROM (SELECT a.*,l.amount AS amt,l.ledger_id,al.ledger_name "
						+ "FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" l,account_ledgers_"+societyVO.getDataZoneID()+" al WHERE a.tx_id=l.tx_id AND al.id=l.ledger_id AND a.is_deleted=0 "+ledgerCondition+" and l.org_id=:orgID and a.org_id=l.org_id and a.org_id=al.org_id AND a.tx_type="+typeCondition+" AND (a.tx_date BETWEEN :fromDate AND :toDate) GROUP BY l.tx_id ORDER BY a.tx_date ) AS a LEFT JOIN ( SELECT id,org_id,receipt_id,invoice_id,SUM(amount) AS amount FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" where org_id=:orgID AND ( ledger_id=:ledgerID OR ledger_id=0 OR ledger_id IS NULL) group by receipt_id ) r ON r.receipt_id=a.tx_id AND a.org_id=r.org_id GROUP BY a.tx_id;";
				}
				log.debug("query: "+str);
				
				   Map hMap=new HashMap();
				   hMap.put("orgID", orgID);
				   hMap.put("ledgerID", ledgerID);
				   hMap.put("fromDate", fromDate);
				   hMap.put("toDate", toDate);
				  
				   RowMapper Rmapper = new RowMapper() {
						public Object mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							List accheadVOList=null;
							TransactionVO txVO=new TransactionVO();
							txVO.setTransactionID(rs.getInt("tx_id"));
							txVO.setCreateDate(rs.getTimestamp("create_date"));
							txVO.setTransactionDate(rs.getString("tx_date"));
							txVO.setBankDate(rs.getString("bank_date"));
							txVO.setDescription(rs.getString("description"));
							txVO.setDocID(rs.getString("doc_id"));
							txVO.setTransactionNumber(rs.getString("tx_number"));
							txVO.setRefNumber(rs.getString("ref_no"));
							txVO.setTransactionType(rs.getString("tx_type"));
							txVO.setTransactionMode(rs.getString("tx_mode"));
							txVO.setAmount(rs.getBigDecimal("amount"));
							txVO.setAmtInWord(rs.getString("amt_in_word"));
							txVO.setLedgerID(rs.getInt("ledger_id"));
							txVO.setBalance(rs.getBigDecimal("avBal"));
							txVO.setReceiptID(rs.getInt("receipt_id"));
							txVO.setReceiptLinkID(rs.getInt("id"));
							txVO.setInvoiceID(rs.getInt("invoice_id"));
							txVO.setLedgerName(rs.getString("ledger_name"));				
							
							return txVO;
							
						}

					   };
					   txList=namedParameterJdbcTemplate.query(
							str, hMap, Rmapper);
				   log.debug("Exit :  public List getPaymentReceiptListReport(int orgID,int ledgerID)"+txList.size());
				}catch(EmptyResultDataAccessException Ex){
					log.info("No Data found at  List getPaymentReceiptListReport(int orgID,int invoiceID) "+Ex);
				
			
					}catch(Exception se){
					log.error("Exception :public List getPaymentReceiptListReport(int orgID,int ledgerID)"+se);
				   
				}
			
			log.debug("Exit : public List getPaymentReceiptListReport(int orgID,int ledgerID)");
		return txList;
		}
	
	public AccountHeadVO getLedgerDetails(int orgID,String ledgerName){
		AccountHeadVO accVO=new AccountHeadVO();
	
			log.debug("Entry :  public AccountHeadVO getLedgerDetails(int orgID,String ledgerName)");
			try{
				SocietyVO societyVO=societyService.getSocietyDetails(orgID);
				String str="SELECT * FROM account_ledgers_"+societyVO.getDataZoneID()+" WHERE org_id=:orgID AND ledger_name=:ledgerName ;";
		
				log.debug("query: "+str);
				
				   Map hMap=new HashMap();
				   hMap.put("orgID", orgID);
				   hMap.put("ledgerName", ledgerName);
				  
				  
				   RowMapper Rmapper = new RowMapper() {
						public Object mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							AccountHeadVO ledgerVO=new AccountHeadVO();
							ledgerVO.setLedgerID(rs.getInt("id"));
							ledgerVO.setLedgerName(rs.getString("ledger_name"));
							ledgerVO.setOrgID(rs.getInt("org_id"));
							
							return ledgerVO;
							
						}

					   };
					   accVO=(AccountHeadVO)namedParameterJdbcTemplate.queryForObject(
							str, hMap, Rmapper);
				   log.debug("Exit :  public AccountHeadVO getLedgerDetails(int orgID,String ledgerName)");
				}catch(EmptyResultDataAccessException Ex){
					log.info("No Data found at public AccountHeadVO getLedgerDetails(int orgID,String ledgerName) "+Ex);
				
			
					}catch(Exception se){
					log.error("Exception : public AccountHeadVO getLedgerDetails(int orgID,String ledgerName)"+se);
				   
				}
			
			log.debug("Exit : public List getUnlinkedRceiptList(int orgID,int ledgerID)");
		return accVO;
		}
	
	/*----Get Invoice List of Link to Receipt or Payment--*/
	public List getTransactionLinkedInvoiceList(int orgID,int transactionID){
		List invoiceReceiptList=null;
	
			log.debug("Entry :  public List getTransactionLinkedInvoiceList(int orgID,int transactionID)");
			try{
				SocietyVO societyVO=societyService.getSocietyDetails(orgID);
				String str=" SELECT recpt.*, inv.invoice_number,inv.doc_id,inv.notes FROM receipt_invoice_mapping_"+societyVO.getDataZoneID()+" recpt,invoice_details inv WHERE inv.id=recpt.invoice_id AND inv.org_id=recpt.org_id "+
                           " AND (inv.invoice_type='Sales' OR inv.invoice_type='Purchase' OR inv.invoice_type='Journal') AND recpt.org_id=:orgID AND recpt.receipt_id=:transactionID AND inv.is_deleted=0 ";
		
				log.debug("query: "+str);
				
				   Map hMap=new HashMap();
				   hMap.put("orgID", orgID);
				   hMap.put("transactionID", transactionID);
				
				  
				   RowMapper Rmapper = new RowMapper() {
						public Object mapRow(ResultSet rs, int rowNum)
								throws SQLException {
						
							TransactionVO txVO=new TransactionVO();
							txVO.setDescription(rs.getString("notes"));
							txVO.setDocID(rs.getString("doc_id"));
							txVO.setTransactionNumber(rs.getString("invoice_number"));
							txVO.setAmount(rs.getBigDecimal("amount"));
							//txVO.setBalance(rs.getBigDecimal("avBal"));
							txVO.setReceiptID(rs.getInt("receipt_id"));
							txVO.setReceiptLinkID(rs.getInt("id"));
							txVO.setInvoiceID(rs.getInt("invoice_id"));									
							
							return txVO;
							
						}

					   };
					   invoiceReceiptList=namedParameterJdbcTemplate.query(
							str, hMap, Rmapper);
				   log.debug("Exit :  public List getTransactionLinkedInvoiceList(int orgID,int transactionID)"+invoiceReceiptList.size());
				}catch(EmptyResultDataAccessException Ex){
					log.info("No Data found at  List getTransactionLinkedInvoiceList(int orgID,int transactionID) "+Ex);
				
			
					}catch(Exception se){
					log.error("Exception :public List getTransactionLinkedInvoiceList(int orgID,int transactionID)"+se);
				   
				}
			
			log.debug("Exit : public List getTransactionLinkedInvoiceList(int orgID,int transactionID)");
		return invoiceReceiptList;
		}
	
	/* Re allocate the interest amounts */
	public int reallocateIntestAmount(TransactionVO txVO){
		int success=0;
		log.debug("Entry : public int reallocateIntestAmount(int orgID ,TransactionVO txVO)");
		
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(txVO.getSocietyID());
			   String str = "update  account_ledger_entry_"+societyVO.getDataZoneID()+" set int_amount=:intAmount WHERE tx_id=:txID AND ledger_id=:ledgerID and org_id=:orgID;";
			 
			   log.debug(str);
			   
			   Map hMap=new HashMap();
			   hMap.put("intAmount", txVO.getIntAmount());
			   hMap.put("ledgerID", txVO.getLedgerID());
			   hMap.put("txID", txVO.getTransactionID());
			   hMap.put("orgID", txVO.getSocietyID());
			   
			   
			   success=namedParameterJdbcTemplate.update(str, hMap);
			
			
		} catch (Exception e) {
			log.error("Exception at public int reallocateIntestAmount(int orgID ,TransactionVO txVO) "+e);
		}
		
		log.debug("Exit : public int reallocateIntestAmount(int orgID ,TransactionVO txVO)");
		return success;		
	}
	
	
	//============ Member App related APIs =============//

	
	/* Used to get non recorded transactions  */
	public List getAllNonRecordedTransactionsReportForMemberAppLedger(TransactionVO txVO)  {
		log.debug("Entry : public List getAllNonRecordedTransactionsReportForMemberAppLedger(TransactionVO txVO)");
		List TransactionList=null;
		String txTypeCondition="";
		String txStatusCondition=" and ats.is_deleted!=0 ";
		
		if(!txVO.getTransactionType().equalsIgnoreCase("all")){
			txTypeCondition=" and ats.tx_type=:txType  ";
		}
		if(!txVO.getStatusDescription().equalsIgnoreCase("All")){
			txStatusCondition=" and ats.is_deleted=:statusID ";
		}
		
		

		try{
			SocietyVO societyVO=societyService.getSocietyDetails(txVO.getSocietyID());
			String str="SELECT ats.org_id,ats.tx_id AS TXID,ats.tx_date,ats.doc_id,ats.ref_no,ats.create_date,ats.bank_date,ats.description,ats.tx_number,ats.tx_type,tx_mode,ats.created_by,ats.updated_by,m.full_name AS createdBy,mi.full_name AS updatedBy,ats.amount, table_primary.type, table_primary.tx_id,table_secondary.ledger_id,account_ledgers_"+societyVO.getDataZoneID()+".ledger_name ,sub_group_id,amt_in_word ,ats.is_deleted "+
						" FROM account_ledger_entry_"+societyVO.getDataZoneID()+" table_primary,account_ledger_entry_"+societyVO.getDataZoneID()+" table_secondary,account_ledgers_"+societyVO.getDataZoneID()+",account_transactions_"+societyVO.getDataZoneID()+" ats,um_users m , um_users mi "+ 
						" WHERE table_primary.tx_id = table_secondary.tx_id AND table_primary.type!=table_secondary.type "+
						" AND table_primary.org_id=table_secondary.org_id  and table_primary.ledger_id=:ledgerID "+
						" AND account_ledgers_"+societyVO.getDataZoneID()+".id = table_primary.ledger_id "+txStatusCondition+" "+txTypeCondition +
						" AND ats.tx_id = table_primary.tx_id "+  
						"  AND ats.org_id=account_ledgers_"+societyVO.getDataZoneID()+".org_id and ats.org_id=table_primary.org_id "+					
						" AND m.id=ats.created_by AND mi.id=ats.updated_by and ats.org_id=:orgID GROUP BY table_secondary.tx_id ORDER BY ats.tx_date desc limit 12 " ;
					    
			
			
			log.debug("Query is "+str);
			   
			   Map hMap=new HashMap();
			   hMap.put("orgID", txVO.getSocietyID());
			   hMap.put("ledgerID", txVO.getLedgerID());
			   hMap.put("fromDate", txVO.getFromDate());
			   hMap.put("uptoDate", txVO.getToDate());
			   hMap.put("txType", txVO.getTransactionType());
			   hMap.put("statusID", txVO.getStatusDescription());
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						List accheadVOList=null;
						TransactionVO txVO=new TransactionVO();
						txVO.setTransactionID(rs.getInt("txID"));
						txVO.setSocietyID(rs.getInt("org_id"));
						txVO.setTransactionDate(rs.getString("tx_date"));
						txVO.setBankDate(rs.getString("bank_date"));
						txVO.setDescription(rs.getString("description"));
						txVO.setTransactionNumber(rs.getString("tx_number"));
						txVO.setDocID(rs.getString("doc_id"));
						txVO.setRefNumber(rs.getString("ref_no"));
						txVO.setTransactionType(rs.getString("tx_type"));
						txVO.setTransactionMode(rs.getString("tx_mode"));
						txVO.setCreditDebitFlag(rs.getString("type"));
						txVO.setAmount(rs.getBigDecimal("amount"));
						txVO.setLedgerName(rs.getString("ledger_name"));
						txVO.setGroupID(rs.getInt("sub_group_id"));
						txVO.setCreatorName(rs.getString("createdBy"));
						txVO.setUpdaterName(rs.getString("updatedBy"));
						txVO.setIsDeleted(rs.getInt("is_deleted"));
						txVO.setAmtInWord(rs.getString("amt_in_word"));
					
						if(rs.getString("type").equalsIgnoreCase("C")){
						
								txVO.setCreditAmt(txVO.getAmount().abs());
							
							}else{
								
									txVO.setDebitAmt(txVO.getAmount().abs());
							
							}
						
						
						return txVO;
						
					}

				   };
				   TransactionList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit :  public List getAllNonRecordedTransactionsReportForMemberAppLedger(TransactionVO txVO)"+TransactionList.size());
			}catch(EmptyResultDataAccessException Ex){
				log.info("No Data found at public List getAllNonRecordedTransactionsReportForMemberAppLedger(TransactionVO txVO)) "+Ex);
			
		
				}catch(Exception se){
				log.error("Exception : public List getAllNonRecordedTransactionsReportForMemberAppLedger(TransactionVO txVO) : for Org ID : "+txVO.getSocietyID()+" ,ledger ID "+txVO.getLedgerID()+" , for the period "+txVO.getFromDate()+" to "+txVO.getToDate()+ "    " +se);
			   
			}
		return TransactionList;
	}
	
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	/**
	 * @param societyService the societyService to set
	 */
	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}

/*
	@Override
	public RptTransactions getTransactions(ReportRequest request) {
		// TODO Auto-generated method stub
		return tallyTransactions.getVouchersByRange(request);
	}

	@Override
	public RptTransactions getTransactionsByID(ReportRequest request) {
		// TODO Auto-generated method stub
		return tallyTransactions.getVouchersByID(request);
	}

	@Override
	public RptTransactions getTransactionsByAccount(ReportRequest request) {
		// TODO Auto-generated method stub
		return tallyTransactions.getVouchersByLedger(request);
	}
*/
	
	
	
	

}
