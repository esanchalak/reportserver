package com.emanager.server.accounts.DAO;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.LedgerEntries;
import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.invoice.dataAccessObject.CustomerVO;
import com.emanager.server.invoice.dataAccessObject.ProfileDetailsVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.taxation.dataAccessObject.TDSDetailsVO;

public class LedgerDAO {
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	private JdbcTemplate jdbcTemplate;
	Logger log=Logger.getLogger(LedgerDAO.class);
	SocietyService societyService;
	
	/* Used for insering  ledger */
	public int insertLedger(AccountHeadVO acHeadVO,int orgID)  {
		// TODO Auto-generated method stub
		int success=0;
		int generatedId=0;
		
		log.debug("Entry :public int inserttLedger(AccountHeadVO acHeadVO,String societyID)");
		try{
			  
			   
			 String param="";
			 String argument="";
			   if((acHeadVO.getLockDate()!=null)){
				   param=", lock_date";
				   argument=",:lockDate";
			   }
			   SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			   String sql = "INSERT INTO account_ledgers_"+societyVO.getDataZoneID()+" "+					
				   "(org_id,sub_group_id,ledger_name ,effective_date,opening_balance,int_balance,create_date,update_date,is_hidden,description"+param+",type,group_tags,project_tags)  " +
	                "VALUES (:orgID,:accountGroupID,:strAccountHeadName,:effectiveDate,:openingBalance,:intOpnBalance,:createDate,:updateDate,:isHidden,:description"+argument+",:accType,:groupTags,:projectTags)";
			   log.debug("Sql: "+sql);
			   
			   SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(acHeadVO);
			   KeyHolder keyHolder = new GeneratedKeyHolder();
			   getNamedParameterJdbcTemplate().update(sql, fileParameters, keyHolder);
			   generatedId=keyHolder.getKey().intValue();
			  /* Map hMap=new HashMap();
			   hMap.put("subGrpID", acHeadVO.getAccountGroupID());
			   hMap.put("ledgerName", acHeadVO.getStrAccountHeadName());
			   hMap.put("effectiveDate", acHeadVO.getEffectiveDate());
			   hMap.put("openingBal", acHeadVO.getOpeningBalance());
			   hMap.put("createDate", currentDate);
			   hMap.put("updateDate", currentTimestamp);
			   
			   success=namedParameterJdbcTemplate.update(str, hMap);*/
			   log.debug("Exit :public int inserttLedger(AccountHeadVO acHeadVO,String societyID)"+generatedId);
		}catch (DuplicateKeyException e) {
			generatedId=-1;
			log.info("Duplicate Ledger Name can not be added");
			
		}catch(Exception se){
				log.error("Exception :public int inserttLedger(AccountHeadVO acHeadVO,String societyID) "+se);
			   generatedId=0;
			}
		return generatedId;
	}
	
	
	/* Used check wheither the given ledger is present or not */
	public List checkLedgerDetails(AccountHeadVO achVO,int orgID)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public Boolean checkLedgerDetails(AccountHeadVO achVO)"+achVO.getStrAccountHeadName());
		List ledgerList=new ArrayList();
		Boolean isDuplicate=false;
			
		try{
			  
			   
			   
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			   String str = "SELECT * FROM account_ledgers_"+societyVO.getDataZoneID()+" WHERE ledger_name=:ledgerName AND is_deleted=0 and org_id=:orgID ;";
			   log.debug(str);
			   Map hMap=new HashMap();
			   hMap.put("ledgerName", achVO.getStrAccountHeadName());
			   hMap.put("orgID", orgID);
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						AccountHeadVO txVO=new AccountHeadVO();
						txVO.setLedgerID(rs.getInt("id"));
						txVO.setStrAccountHeadName(rs.getString("ledger_name"));
						txVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
						
						return txVO;
					}

				   };
			   ledgerList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   
			   if(ledgerList.size()>0){
				   isDuplicate=true;
			   }
			   
			   log.debug("Exit : public Boolean checkLedgerDetails(AccountHeadVO achVO)"+ledgerList.size()+isDuplicate); 
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at public Boolean checkLedgerDetails(AccountHeadVO achVO) "+Ex);
			}catch(Exception se){
				log.error("Exception in public Boolean checkLedgerDetails(AccountHeadVO achVO) "+se);
			   
			}
		return ledgerList;
	}
	
	
	
	/* Used for update  ledger */
	public int updateLedger(AccountHeadVO acHeadVO,int orgID,int ledgerID)  {
		// TODO Auto-generated method stub
		int success=0;
		
		log.debug("Entry :public int updateLedger(AccountHeadVO acHeadVO,String societyID)"+acHeadVO.getAccountGroupID()+" "+ledgerID+" "+acHeadVO.getOpeningBalance());
		try{
			  
			   java.util.Date today = new java.util.Date();
			   java.sql.Date currentDate=new java.sql.Date(today.getTime());
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			   String strCondtion="";
			   if(acHeadVO.getLockDate()!=null){
				   strCondtion=",lock_date=:lockDate";
			   }
			   SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			   String str = "Update account_ledgers_"+societyVO.getDataZoneID()+" set ledger_name=:ledgerName,sub_group_id=:subGrpID,effective_date=:effectiveDate,opening_balance=:openingBal,int_balance=:intBalance,is_hidden=:isHidden,description=:description,type=:accType,update_date=:update,group_tags=:groupTags,project_tags=:projectTags "+strCondtion+" where id=:ledgerID and org_id=:orgID ";
			   log.debug("Query: "+str);
			   
			   Map hMap=new HashMap();
			   hMap.put("subGrpID", acHeadVO.getAccountGroupID());
			   hMap.put("ledgerName", acHeadVO.getStrAccountHeadName());
			   hMap.put("effectiveDate", acHeadVO.getEffectiveDate());
			   hMap.put("openingBal", acHeadVO.getOpeningBalance());
			   hMap.put("intBalance", acHeadVO.getIntOpnBalance());
			   hMap.put("ledgerID", ledgerID);
			   hMap.put("isHidden", acHeadVO.getIsHidden());
			   hMap.put("description", acHeadVO.getDescription());
			   hMap.put("lockDate", acHeadVO.getLockDate());
			   hMap.put("accType", acHeadVO.getAccType());
			   hMap.put("update", currentTimestamp);
			   hMap.put("orgID", orgID);
			   hMap.put("groupTags", acHeadVO.getGroupTags());
			   hMap.put("projectTags", acHeadVO.getProjectTags());
			   
			   
			   success=namedParameterJdbcTemplate.update(str, hMap);
			   log.debug("Exit :public int updateLedger(AccountHeadVO acHeadVO,String societyID)"+success);
			}catch(Exception se){
				log.debug("Exception :public int updateLedger(AccountHeadVO acHeadVO,String societyID) "+se);
			   success=0;
			}
		return success;
	}
	
	/* Used for update  ledger */
	public int deleteLedger(int orgID,int ledgerID)  {
		// TODO Auto-generated method stub
		int success=0;
		
		log.debug("Entry :public int deleteLedger(String societyID,int ledgerID)");
		try{
			  
			   java.util.Date today = new java.util.Date();
			   java.sql.Date currentDate=new java.sql.Date(today.getTime());
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			   
			   SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			   String str = "Update account_ledgers_"+societyVO.getDataZoneID()+" set is_deleted=1,update_date=:update where id=:ledgerID and org_id=:orgID ";
			   
			   
			   Map hMap=new HashMap();
			
			   hMap.put("ledgerID", ledgerID);
			   hMap.put("update", currentTimestamp);
			   hMap.put("orgID", orgID);
			   
			   success=namedParameterJdbcTemplate.update(str, hMap);
			   log.debug("Exit :public int deleteLedger(String societyID,int ledgerID)"+success);
			}catch(Exception se){
				log.debug("Exception : public int deleteLedger(String societyID,int ledgerID) "+se);
			   success=0;
			}
		return success;
	}
	/* Used for to get subGroupDetails of single ledger */
	public LedgerEntries getSubGroupDetails(int subGrpID)  {
		// TODO Auto-generated method stub
		log.debug("Entry :public LedgerEntries getSubGroupDetails(int ledgerID)");
		LedgerEntries ledgerEntry=new LedgerEntries();
			
		try{
			  
			   
			   
			   
			   String str = "SELECT * FROM account_groups ag,account_root_group asg WHERE ag.root_id=asg.root_id AND ag.id=:subGroupID ";
			   			
			   
			   Map hMap=new HashMap();
			   hMap.put("subGroupID", subGrpID);
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						LedgerEntries txVO=new LedgerEntries();
						txVO.setLedgerID(rs.getInt("id"));
						
						txVO.setCreditAction(rs.getInt("cr_action"));
						txVO.setDebitAction(rs.getInt("dr_action"));
						return txVO;
					}

				   };
			   ledgerEntry=(LedgerEntries)namedParameterJdbcTemplate.queryForObject(
						str, hMap, Rmapper);
			   log.debug("Exit :public LedgerEntries getSubGroupDetails(int ledgerID)");
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at public LedgerEntries getSubGroupDetails(int ledgerID) "+Ex);
			
		
			}catch(Exception se){
				log.error("Exception in public LedgerEntries getSubGroupDetails(int ledgerID) "+se);
			   
			}
		return ledgerEntry;
	}
	
	/* Used for to get ledgerDetails of single ledger */
	public LedgerEntries getLedgersDetails(int ledgerID,int orgID)  {
		// TODO Auto-generated method stub
		log.debug("Entry :public LedgerEntries getLedgersDetails(int ledgerID)"+ledgerID);
		LedgerEntries ledgerEntry=new LedgerEntries();
			
		try{
			  
			   
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			   
			   String str = "Select al.*,group_name,root_group_name,cr_action,dr_action from account_ledgers_"+societyVO.getDataZoneID()+" al, account_groups ag,account_root_group asg " +
			   				" WHERE al.sub_group_id=ag.id AND ag.root_id=asg.root_id and al.id=:ledgerID and al.org_id=:orgID ";
			   
			   Map hMap=new HashMap();
			   hMap.put("ledgerID", ledgerID);
			   hMap.put("orgID", orgID);
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						LedgerEntries txVO=new LedgerEntries();
						txVO.setLedgerID(rs.getInt("id"));
						txVO.setOrgID(rs.getInt("org_id"));
						txVO.setLedgerName(rs.getString("ledger_name"));
						txVO.setBalance(rs.getBigDecimal("closing_balance"));
						txVO.setCreditAction(rs.getInt("cr_action"));
						txVO.setDebitAction(rs.getInt("dr_action"));
						return txVO;
					}

				   };
			   ledgerEntry=(LedgerEntries)namedParameterJdbcTemplate.queryForObject(
						str, hMap, Rmapper);
			   log.debug("Exit :public LedgerEntries getLedgersDetails(int ledgerID)");
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at public LedgerEntries getLedgersDetails(int ledgerID) "+Ex);
			}catch(Exception se){
				log.error("Exception in public LedgerEntries getLedgersDetails(int ledgerID) "+se);
			   
			}
		return ledgerEntry;
	}
	
	
	
	public AccountHeadVO getSumOpeningBalance(int orgID,int ledgerID,String date,String type){
		AccountHeadVO acHVO=new AccountHeadVO();
		String strCondition="";
		if(type.equalsIgnoreCase("L")){
			strCondition=" And ale.id= ";
		}else if(type.equalsIgnoreCase("G")){
			strCondition=" And ag.id= ";
		}
			
		try {
			log.debug("Entry :public AccountHeadVO getOpeningBalance(String societyID,int ledgerID,String fromDate, String uptoDate)"+ledgerID+""+date);
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String sqlQuery=" SELECT IF(SUM(ale.opening_balance) IS NULL, 0, SUM(ale.opening_balance)) AS sumOpn, IF(SUM(ale.int_balance) IS NULL, 0, SUM(ale.int_balance)) AS sumIntOpn ,ag.*,ale.*,asg.*,asg.root_id FROM account_ledgers_"+societyVO.getDataZoneID()+" ale ,account_groups ag,account_root_group asg "+
							" WHERE ale.sub_group_id=ag.id "+strCondition+":ledgerID and ag.root_id=asg.root_id AND ale.is_deleted=0 AND effective_date<=:fromDate and ale.org_id=:orgID  ";
			
			log.debug(ledgerID+" Query is "+sqlQuery);
			 Map hMap=new HashMap();
			 hMap.put("ledgerID", ledgerID);
			 hMap.put("fromDate", date);
			 hMap.put("orgID", orgID);
			
			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					
					
					AccountHeadVO acHVO=new AccountHeadVO();
					acHVO.setLedgerID(rs.getInt("id"));
					acHVO.setOrgID(rs.getInt("org_id"));
					acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
					acHVO.setOpeningBalance(rs.getBigDecimal("sumOpn"));
					acHVO.setIntOpnBalance(rs.getBigDecimal("sumIntOpn"));
					acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
					acHVO.setAccountGroupID(rs.getInt("sub_group_id"));
					acHVO.setCreateDate(rs.getString("create_date"));
					acHVO.setUpdateDate(rs.getString("update_date"));
					acHVO.setNormalBalance(rs.getString("normal_balance"));
					acHVO.setRootID(rs.getInt("root_id"));
					
					return acHVO;
				}

			   };
		   acHVO=(AccountHeadVO) namedParameterJdbcTemplate.queryForObject(
					sqlQuery, hMap, Rmapper);
			
			
			log.debug("Exit :public AccountHeadVO getOpeningBalance(String societyID,int ledgerID,String fromDate, String uptoDate)"+acHVO.getRootID());
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at List getSumOpeningBalance "+Ex);
			
		} catch (Exception e) {
			log.error("Exception : public AccountHeadVO getOpeningBalance(String societyID,int ledgerID,String fromDate, String uptoDate) "+e );
		}
		return acHVO;
		
	}
	
	
	
	public AccountHeadVO getSumTransactions(int orgID,int ledgerID,String date,String type){
		AccountHeadVO acHVO=new AccountHeadVO();
		String strCondition="";
		if(type.equalsIgnoreCase("L")){
			strCondition=" And ale.ledger_id= ";
		}else if(type.equalsIgnoreCase("G")){
			strCondition=" And ag.id= ";
		}
			
		try {
			log.debug("Entry :public AccountHeadVO getOpeningBalance(String societyID,int ledgerID,String fromDate, String uptoDate)"+ledgerID+""+date);
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String sqlQuery="SELECT IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount)) AS sumAmt, IF(SUM(ale.int_amount) IS NULL, 0, SUM(ale.int_amount)) AS sumIntAmt FROM account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_groups ag ,account_ledgers_"+societyVO.getDataZoneID()+" al WHERE al.org_id=ale.org_id and ale.org_id=ats.org_id and ats.org_id=:orgID and  al.id=ale.ledger_id AND al.sub_group_id=ag.id AND ats.tx_id=ale.tx_id "+strCondition+":ledgerID AND ats.tx_date<:fromDate and ats.is_deleted=0;";
				
			
			 log.debug(ledgerID+"Here"+sqlQuery);
			 Map hMap=new HashMap();
			 hMap.put("ledgerID", ledgerID);
			 hMap.put("fromDate", date);
			 hMap.put("orgID", orgID);
			
			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					
					
					AccountHeadVO acHVO=new AccountHeadVO();
					
					
					acHVO.setSum(rs.getBigDecimal("sumAmt"));
					acHVO.setSumInterest(rs.getBigDecimal("sumIntAmt"));		
					
					return acHVO;
				}

			   };
		   acHVO=(AccountHeadVO) namedParameterJdbcTemplate.queryForObject(
					sqlQuery, hMap, Rmapper);
			
			
			log.debug("Exit :public AccountHeadVO getOpeningBalance(String societyID,int ledgerID,String fromDate, String uptoDate)");
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at List getSumOpeningBalance "+Ex);
			
		} catch (Exception e) {
			log.error("Exception : public AccountHeadVO getOpeningBalance(String societyID,int ledgerID,String fromDate, String uptoDate) "+e );
		}
		return acHVO;
		
	}
	
	/* Get sum of reconcilation transactions*/
	public AccountHeadVO getSumReconcileTransactions(int orgID,int ledgerID,String date,String type){
		AccountHeadVO acHVO=new AccountHeadVO();
		String strCondition="";
		if(type.equalsIgnoreCase("L")){
			strCondition=" And ale.ledger_id= ";
		}else if(type.equalsIgnoreCase("G")){
			strCondition=" And ag.id= ";
		}
			
		try {
			log.debug("Entry :public AccountHeadVO getSumReconcileTransactions(int societyID,int ledgerID,String date,String type)"+ledgerID+""+date);
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String sqlQuery="SELECT IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount)) AS sumAmt FROM account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_groups ag ,account_ledgers_"+societyVO.getDataZoneID()+" al WHERE al.org_id=ale.org_id and ale.org_id=ats.org_id and ats.org_id=:orgID and  al.id=ale.ledger_id AND al.sub_group_id=ag.id AND ats.tx_id=ale.tx_id "+strCondition+":ledgerID AND ats.bank_date<:fromDate AND ats.bank_date IS NOT NULL  and ats.is_deleted=0;";
			
			 log.debug(ledgerID+"Here"+sqlQuery);
			 Map hMap=new HashMap();
			 hMap.put("ledgerID", ledgerID);
			 hMap.put("fromDate", date);
			 hMap.put("orgID", orgID);
			
			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
									
					AccountHeadVO acHVO=new AccountHeadVO();				
					acHVO.setSum(rs.getBigDecimal("sumAmt"));				
					return acHVO;
				}

			   };
		   acHVO=(AccountHeadVO) namedParameterJdbcTemplate.queryForObject(
					sqlQuery, hMap, Rmapper);
			
			
			log.debug("Exit :public AccountHeadVO getSumReconcileTransactions(int societyID,int ledgerID,String date,String type)");
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at List getSumReconcileTransactions "+Ex);
			
		} catch (Exception e) {
			log.error("Exception : public AccountHeadVO getSumReconcileTransactions(int societyID,int ledgerID,String date,String type) "+e );
		}
		return acHVO;
		
	}
	
	public AccountHeadVO getLedgerID(int userID,int orgID,String userType,String identifier){
		AccountHeadVO achVO=new AccountHeadVO();
		String condition="";
		if(identifier.equalsIgnoreCase("APTID")){
			condition="m.apt_id";
		}else if(identifier.equalsIgnoreCase("MEMBERID")){
			condition="m.member_id";
		}
		log.debug("Entry : ppublic AccountHeadVO getLedgerID(int memberID)"+identifier+" "+userType+"  "+userID);
			try{
				SocietyVO societyVO=societyService.getSocietyDetails(orgID);
				String sqlQuery=" SELECT  * from ledger_user_mapping_"+societyVO.getDataZoneID()+" l,member_details m where l.user_id=m.member_id and  "+condition+"=:userID AND m.type='P' and l.org_id=:orgID  ;";

								
				log.debug(sqlQuery);
				 
				 Map hMap=new HashMap();
				 hMap.put("userID", userID);
				 hMap.put("userType", userType);
				 hMap.put("orgID",orgID);
				
				RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						
						AccountHeadVO acHVO=new AccountHeadVO();
						acHVO.setLedgerID(rs.getInt("ledger_id"));
						acHVO.setLedgerType(rs.getString("ledger_type"));
						return acHVO;
					}

				   };
			   achVO=(AccountHeadVO) namedParameterJdbcTemplate.queryForObject(
						sqlQuery, hMap, Rmapper);
			   log.debug("Exit : public AccountHeadVO getLedgerID(int memberID)"+achVO.getLedgerID());
			}catch(EmptyResultDataAccessException Ex){
				log.info("No Data found getLedgerID(int memberID) "+Ex);
				
				
		} catch (Exception e) {
			log.error("Exception : getLedgerID(int memberID)"+e);
		}
		return achVO;
	}
	
	
	
	
	/* Used to get subgroups of that society */
	public List getAllGroupList(int rootID,int orgID)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public List getAllGroupList(int rootID,int orgID)");
		List ledgerList=null;
		String strCondition="";
		if(rootID!=0){
		strCondition="and a.root_id=:rootID "	;
		}
		SocietyVO societyVO=societyService.getSocietyDetails(orgID);
		try{
			  String str = "SELECT ap.short_name,ap.root_group_name,a.*,ac.category_name FROM account_groups a,account_root_group ap,account_category ac where a.category_id=ac.category_id AND a.root_id=ap.root_id "+strCondition+" AND org_category LIKE '%"+societyVO.getOrgType()+"%' and (a.org_id=0 or a.org_id=:orgID) and a.is_deleted=0 order by seq_no, root_id";
			   				 
			   				
			   
			   Map hMap=new HashMap();
			   hMap.put("rootID", rootID);
			   hMap.put("orgID", orgID);
			  
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						AccountHeadVO acHVO=new AccountHeadVO();
						acHVO.setAccountGroupID(rs.getInt("id"));
						acHVO.setStrAccountHeadName(rs.getString("group_name").trim());
						acHVO.setRootID(rs.getInt("root_id"));
						acHVO.setLedgerShortName(rs.getString("short_name"));
						acHVO.setRootGroupName(rs.getString("root_group_name"));
						acHVO.setCategoryID(rs.getInt("category_id"));
						acHVO.setCategoryName(rs.getString("category_name"));
						acHVO.setOrgID(rs.getInt("org_id"));
						return acHVO;
					}

				   };
			   ledgerList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit : public List getSubGroupList(String societyID)"+ledgerList.size());
			}catch(Exception se){
				log.error("Exception : public List getSubGroupList(String societyID) "+se);
			   
			}
		return ledgerList;
	}
	
	
	
	/* Used to get subCategories of that society */
	public List getAllSubCategoryList(int parentGroupID)  {
			log.debug("Entry : public List getAllSubCategoryList(int parentGroupID)"+parentGroupID);
		List ledgerList=null;
		String strCondition="";
		if(parentGroupID!=0){
		strCondition="and a.id=:parentGroupID "	;
		}
	
		try{
			  String str = "SELECT a.id AS parent_id,a.*,ap.*,ap.id as category_id FROM account_groups a,account_sub_category_details ap WHERE a.id=ap.group_id "+strCondition+" ORDER BY category_name;";
			   				 
			   				
			   
			   Map hMap=new HashMap();
			   hMap.put("parentGroupID", parentGroupID);
			  
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						AccountHeadVO acHVO=new AccountHeadVO();
						acHVO.setAccountGroupID(rs.getInt("parent_id"));
						acHVO.setStrAccountHeadName(rs.getString("group_name").trim());
						acHVO.setCategoryID(rs.getInt("category_id"));
						acHVO.setCategoryName(rs.getString("category_name"));
						return acHVO;
					}

				   };
			   ledgerList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit : public List getAllSubCategoryList(String societyID)"+ledgerList.size());
			}catch(Exception se){
				log.error("Exception : public List getAllSubCategoryList(String societyID) "+se);
			   
			}
		return ledgerList;
	}
	
	public AccountHeadVO getLedgerDetails(int ledgerID,int orgID){
		AccountHeadVO achVO=new AccountHeadVO();
	
		log.debug("Entry : public AccountHeadVO getLedgerDetails(int ledgerID,String societyID)");
			try{
				SocietyVO societyVO=societyService.getSocietyDetails(orgID);
				String sqlQuery=" SELECT  * from account_ledgers_"+societyVO.getDataZoneID()+" l,account_groups g,account_root_group r where l.sub_group_id=g.id and g.root_id=r.root_id and l.id=:ledgerID and l.org_id=:orgID ;";

								
				log.debug(sqlQuery);
				 
				 Map hMap=new HashMap();
				 hMap.put("ledgerID", ledgerID);
				 hMap.put("orgID", orgID);
				
				
				RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						
						AccountHeadVO acHVO=new AccountHeadVO();
						acHVO.setLedgerID(rs.getInt("id"));
						acHVO.setOrgID(rs.getInt("org_id"));
						acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
						acHVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
						acHVO.setClosingBalance(rs.getBigDecimal("closing_balance"));
						acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
						acHVO.setAccountGroupID(rs.getInt("sub_group_id"));
						acHVO.setCreateDate(rs.getString("create_date"));
						acHVO.setCreditAction(rs.getInt("cr_action"));
						acHVO.setDebitAction(rs.getInt("dr_action"));
						acHVO.setUpdateDate(rs.getString("update_date"));
						acHVO.setEffectiveDate(rs.getString("effective_date"));
						acHVO.setNormalBalance(rs.getString("normal_balance"));
						acHVO.setRootID(rs.getInt("root_id"));
						acHVO.setIsHidden(rs.getInt("is_hidden"));
						acHVO.setDescription(rs.getString("description"));
						acHVO.setIsPrimary(rs.getInt("is_primary"));
						acHVO.setLedgerShortName(rs.getString("short_name"));
						acHVO.setIsDeleted(rs.getInt("is_deleted"));
						acHVO.setLockDate(rs.getString("lock_date"));
						acHVO.setRootGroupName(rs.getString("root_group_name"));
						acHVO.setAccType(rs.getString("type"));
						acHVO.setIntOpnBalance(rs.getBigDecimal("int_balance"));
						acHVO.setGroupTags(rs.getString("group_tags"));
						acHVO.setProjectTags(rs.getString("project_tags"));
						acHVO.setCategoryID(rs.getInt("category_id"));
						return acHVO;
					}

				   };
			   achVO=(AccountHeadVO) namedParameterJdbcTemplate.queryForObject(
						sqlQuery, hMap, Rmapper);
			   achVO.setOrgID(orgID);
			   log.debug("Exit : public AccountHeadVO getLedgerDetails(int ledgerID,String societyID)"+achVO.getLedgerID());
			}catch(EmptyResultDataAccessException Ex){
				log.info("No Data found getLedgerDetails(int ledgerID) for orgID: "+orgID+" Ledger ID "+ledgerID+Ex);
				
				
		} catch (Exception e) {
			log.error("Exception : getLedgerID(int memberID)"+e);
		}
	    	
		return achVO;
	}
	
	
	
	/* Used to get ledgerEntries related for transaction */
	public List getLedgersList(int orgID,int groupOrRootID,String type,String isHide)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public List getLedgersList(String societyID,String ledgerType)");
		List ledgerList=null;
		String strCondition="";
		String strShowLedgerCondition="";
		if(type.equalsIgnoreCase("R")){
			if(groupOrRootID!=0){
				strCondition=" AND asg.root_id=:groupOrRootID order by ag.id"	;	
				}
		}else if(type.equalsIgnoreCase("G")){
			if(groupOrRootID!=0){
				strCondition=" AND sub_group_id=:groupOrRootID order by ag.id "	;	
				}
		}
		else if(type.equalsIgnoreCase("C")){
			if(groupOrRootID!=0){
				strCondition=" AND ag.category_id=:groupOrRootID order by ag.id "	;	
				}
		}
		//A: show all ledger ,V: show only visible ledger
		if(isHide.equalsIgnoreCase("A")){
			
			strShowLedgerCondition=" "	;	
			
		}else if(isHide.equalsIgnoreCase("V")){
				strShowLedgerCondition=" AND is_hidden='0' "	;	
				
		}
		if((type.equalsIgnoreCase("G"))&&(groupOrRootID==4)){
			strCondition=" AND ( sub_group_id=:groupOrRootID or sub_group_id=95) order by ag.id ";
		}
		if((type.equalsIgnoreCase("C"))&&(groupOrRootID==64)){
			strCondition=" AND ( ag.category_id=:groupOrRootID or ag.category_id=101) order by ag.id ";
		}
		
		try{
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			  String str = "SELECT al.*,group_name,ag.root_id,asg.normal_balance,short_name,asg.root_group_name,ag.category_id FROM account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups ag ,account_root_group asg WHERE al.sub_group_id=ag.id and al.is_deleted=0 AND al.org_id=:orgID AND ag.root_id=asg.root_id "+strShowLedgerCondition+strCondition+" ";
			   log.debug("sql query is "+str); 
			   				
			   
			   Map hMap=new HashMap();
			   hMap.put("orgID", orgID);
			   hMap.put("groupOrRootID", groupOrRootID);			  
			   
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						AccountHeadVO acHVO=new AccountHeadVO();
						acHVO.setLedgerID(rs.getInt("id"));
						acHVO.setOrgID(rs.getInt("org_id"));
						acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
						acHVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
						acHVO.setClosingBalance(rs.getBigDecimal("closing_balance"));
						acHVO.setIntOpnBalance(rs.getBigDecimal("int_balance"));
						acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
						acHVO.setAccountGroupID(rs.getInt("sub_group_id"));
						acHVO.setCreateDate(rs.getString("create_date"));
						acHVO.setUpdateDate(rs.getString("update_date"));
						acHVO.setEffectiveDate(rs.getString("effective_date"));
						acHVO.setNormalBalance(rs.getString("normal_balance"));
						acHVO.setRootID(rs.getInt("root_id"));
						acHVO.setIsHidden(rs.getInt("is_hidden"));
						acHVO.setDescription(rs.getString("description"));
						acHVO.setIsPrimary(rs.getInt("is_primary"));
						acHVO.setLedgerShortName(rs.getString("short_name"));
						acHVO.setLockDate(rs.getString("lock_date"));
						acHVO.setRootGroupName(rs.getString("root_group_name"));
						acHVO.setAccType(rs.getString("type"));
						acHVO.setGroupTags(rs.getString("group_tags"));
						acHVO.setProjectTags(rs.getString("project_tags"));
						acHVO.setCategoryID(rs.getInt("category_id"));
						return acHVO;
					}

				   };
			   ledgerList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit :  public List getLedgersList(String societyID,String ledgerType)"+ledgerList.size());
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at List getLedgersList(TransactionVO transactionVO) "+Ex);
			
			}catch(Exception se){
				log.error("Exception : public List getLedgersList(String societyID,String ledgerType) "+se);
			   
			}
		return ledgerList;
	}
	
	/* Used to get ledgerEntries related for transaction */
	public List getLedgersListWithClosingBal(int orgID,int groupOrRootID,String type,String isHide,String fromDate,String toDate)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public List getLedgersListWithClosingBal(int societyID,int groupOrRootID,String type,String isHide,String fromDate,String toDate) ");
		List ledgerList=null;
		String strCondition="";
		String strShowLedgerCondition="";
		if(type.equalsIgnoreCase("R")){
			if(groupOrRootID!=0){
				strCondition=" AND asg.root_id=:groupOrRootID order by ag.id"	;	
				}
		}else if(type.equalsIgnoreCase("G")){
			if(groupOrRootID!=0){
				strCondition=" AND sub_group_id=:groupOrRootID order by ag.id "	;	
				}
		}
		//A: show all ledger ,V: show only visible ledger
		if(isHide.equalsIgnoreCase("A")){
			
			strShowLedgerCondition=" "	;	
			
		}else if(isHide.equalsIgnoreCase("V")){
				strShowLedgerCondition=" AND is_hidden='0' "	;	
				
		}
		
		
		try{
				SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			    String str="SELECT a.*,t.closingBalance as closingBalance,t.openingBalance AS openingBalance,a.opening_balance AS openingBalWhenNoTx ,t.closingIntBalance,t.openingIntBalance,a.int_balance AS openingIntBalWhenNoTx  FROM (SELECT al.*,group_name,ag.root_id,asg.normal_balance,short_name,asg.root_group_name FROM account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups ag ,account_root_group asg WHERE al.org_id=:orgID AND al.sub_group_id=ag.id AND al.is_deleted=0 AND ag.root_id=asg.root_id "+strShowLedgerCondition+strCondition+"  ) a LEFT JOIN "+
			    					 " (SELECT * FROM (SELECT ale.ledger_id,al.ledger_name,IF(SUM(ale.int_amount) IS NULL,0,SUM(ale.int_amount)) AS sumInt,IF(SUM(ale.int_amount) IS NULL, 0, SUM(ale.int_amount) + al.int_balance) AS closingIntBalance,  IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount)) AS sumAmt, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS closingBalance "+
			    					 " FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale "+
			    					 " WHERE al.org_id=ale.org_id and ale.org_id=ats.org_id and ats.org_id=:orgID AND ats.tx_id=ale.tx_id AND ats.tx_date <= :toDate  AND ats.is_deleted=0 AND ale.ledger_id = al.id  GROUP BY al.id) clsBal LEFT JOIN "+
			    					  " (SELECT ale.ledger_id AS ledgerID,IF(SUM(ale.int_amount) IS NULL, 0, SUM(ale.int_amount) + al.int_balance) AS openingIntBalance,  IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS openingBalance "+
			    					  " FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale "+
			    					  " WHERE al.org_id=ale.org_id and ale.org_id=ats.org_id and ats.org_id=:orgID AND ats.tx_id=ale.tx_id AND ats.tx_date <:fromDate  AND ats.is_deleted=0 AND ale.ledger_id = al.id  GROUP BY al.id) opnBal ON clsBal.ledger_id=opnBal.ledgerID ) t ON a.id=t.ledger_id ";			
					
			
		//	  String str = "SELECT al.*,group_name,ag.root_id,asg.normal_balance,short_name,asg.root_group_name FROM account_ledgers_"+societyID+" al,account_groups ag ,account_root_group asg WHERE al.sub_group_id=ag.id and al.is_deleted=0 AND ag.root_id=asg.root_id "+strShowLedgerCondition+strCondition+" ";
			   log.debug("sql query is "+str); 
			   				
			   
			   Map hMap=new HashMap();
			   hMap.put("orgID", orgID);
			   hMap.put("groupOrRootID", groupOrRootID);			  
			   hMap.put("toDate", toDate);
			   hMap.put("fromDate", fromDate);
			   
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						AccountHeadVO acHVO=new AccountHeadVO();
						acHVO.setLedgerID(rs.getInt("id"));
						acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
						acHVO.setOpeningBalance(rs.getBigDecimal("openingBalance"));
						acHVO.setIntOpnBalance(rs.getBigDecimal("openingIntBalance"));
						acHVO.setClosingBalance(rs.getBigDecimal("closingBalance"));
						acHVO.setIntClsBalance(rs.getBigDecimal("closingIntBalance"));
						if ((rs.getBigDecimal("openingBalance") == null)
								&& (rs.getBigDecimal("closingBalance") == null)) {

							acHVO.setClosingBalance(rs
									.getBigDecimal("openingBalWhenNoTx"));
							acHVO.setOpeningBalance(rs
									.getBigDecimal("openingBalWhenNoTx"));
							
						} else if (rs.getBigDecimal("openingBalance") == null) {
						
							acHVO.setOpeningBalance(rs
									.getBigDecimal("openingBalWhenNoTx"));
						}
						if ((rs.getBigDecimal("openingIntBalance") == null)
								&& (rs.getBigDecimal("closingIntBalance") == null)) {

							acHVO.setIntClsBalance(rs
									.getBigDecimal("openingIntBalWhenNoTx"));
							acHVO.setIntOpnBalance(rs
									.getBigDecimal("openingIntBalWhenNoTx"));
							
						} else if (rs.getBigDecimal("openingIntBalance") == null) {
						
							acHVO.setIntOpnBalance(rs
									.getBigDecimal("openingIntBalWhenNoTx"));
						}
						acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
						acHVO.setAccountGroupID(rs.getInt("sub_group_id"));
						acHVO.setCreateDate(rs.getString("create_date"));
						acHVO.setUpdateDate(rs.getString("update_date"));
						acHVO.setEffectiveDate(rs.getString("effective_date"));
						acHVO.setNormalBalance(rs.getString("normal_balance"));
						acHVO.setRootID(rs.getInt("root_id"));
						acHVO.setIsHidden(rs.getInt("is_hidden"));
						acHVO.setDescription(rs.getString("description"));
						acHVO.setIsPrimary(rs.getInt("is_primary"));
						acHVO.setLedgerShortName(rs.getString("short_name"));
						acHVO.setLockDate(rs.getString("lock_date"));
						acHVO.setRootGroupName(rs.getString("root_group_name"));
						acHVO.setAccType(rs.getString("type"));
						acHVO.setGroupTags(rs.getString("group_tags"));
						acHVO.setProjectTags(rs.getString("project_tags"));
						return acHVO;
					}

				   };
			   ledgerList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit :  public List getLedgersListWithClosingBal(int societyID,int groupOrRootID,String type,String isHide,String fromDate,String toDate) "+ledgerList.size());
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at List public List getLedgersListWithClosingBal(int societyID,int groupOrRootID,String type,String isHide,String fromDate,String toDate)  "+Ex);
			
			}catch(Exception se){
				log.error("Exception : public List getLedgersListWithClosingBal(int societyID,int groupOrRootID,String type,String isHide,String fromDate,String toDate)  "+se);
			   
			}
		return ledgerList;
	}
	
	
	/* Used to get ledger List related for customer / vendors */
	public List getCustomerVendorLedgersList(int orgID,int categoryID,String type)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public List getLedgersList(String societyID,String ledgerType)");
		List ledgerList=null;
		String str="";
			
		try{
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			if(societyVO.getOrgType().equalsIgnoreCase("S")&&(categoryID==1)&&(type.equalsIgnoreCase("M"))){
				
			str= " SELECT * FROM (SELECT al.*,group_name,ag.root_id,asg.normal_balance,short_name,asg.root_group_name,m.pan_no as mPan,m.gstin as mGstin,m.state_of_supply as mState FROM account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups ag ,account_root_group asg, member_details m,ledger_user_mapping_"+societyVO.getDataZoneID()+" lu WHERE al.id=lu.ledger_id AND lu.user_id=m.member_id AND al.sub_group_id=ag.id AND al.is_deleted=0 AND al.org_id=:orgID  AND ag.root_id=asg.root_id AND ag.category_id=:categoryID ORDER BY ag.id) AS acc LEFT JOIN acc_profiles c ON acc.id=c.ledger_id AND acc.org_id=c.org_id AND c.org_id=:orgID AND c.profile_type=:type; ";
			
			log.debug("sql query is "+str); 
			   Map hMap=new HashMap();
			   hMap.put("orgID", orgID);
			   hMap.put("categoryID", categoryID);
			   hMap.put("type",type);
			   
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						AccountHeadVO acHVO=new AccountHeadVO();
						ProfileDetailsVO profVO=new ProfileDetailsVO();
						acHVO.setLedgerID(rs.getInt("id"));
						acHVO.setOrgID(rs.getInt("org_id"));
						acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
						acHVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
						acHVO.setClosingBalance(rs.getBigDecimal("closing_balance"));
						acHVO.setIntOpnBalance(rs.getBigDecimal("int_balance"));
						acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
						acHVO.setAccountGroupID(rs.getInt("sub_group_id"));
						acHVO.setCreateDate(rs.getString("create_date"));
						acHVO.setUpdateDate(rs.getString("update_date"));
						acHVO.setEffectiveDate(rs.getString("effective_date"));
						acHVO.setNormalBalance(rs.getString("normal_balance"));
						acHVO.setRootID(rs.getInt("root_id"));
						acHVO.setIsHidden(rs.getInt("is_hidden"));
						acHVO.setDescription(rs.getString("description"));
						acHVO.setIsPrimary(rs.getInt("is_primary"));
						acHVO.setLedgerShortName(rs.getString("short_name"));
						acHVO.setLockDate(rs.getString("lock_date"));
						acHVO.setRootGroupName(rs.getString("root_group_name"));
						acHVO.setAccType(rs.getString("type"));
						acHVO.setGroupTags(rs.getString("group_tags"));
						acHVO.setProjectTags(rs.getString("project_tags"));
						profVO.setProfileID(rs.getInt("profile_id"));
						profVO.setOrgID(rs.getInt("org_id"));
						profVO.setTitle(rs.getString("title"));
						profVO.setProfileName(rs.getString("profile_name"));
						profVO.setProfileEmailID(rs.getString("email"));
						profVO.setProfileMobile(rs.getString("mobile"));
						profVO.setProfileType(rs.getString("profile_type"));
						profVO.setProfileLedgerID(rs.getInt("ledger_id"));
						profVO.setGstIN(rs.getString("mGstin"));
						profVO.setPanNo(rs.getString("mPan"));
						profVO.setPanName(rs.getString("pan_name"));
						profVO.setTanNO(rs.getString("tan_no"));					
						profVO.setAdditionalProps(rs.getString("ledger_meta"));
						profVO.setDeducteeCode(rs.getString("deductee_code"));
						profVO.setTdsSectionID(rs.getString("tds_section_id"));
						profVO.setTdsSectionNo(rs.getString("section_no"));
						profVO.setTdsSectionName(rs.getString("section_name"));
						profVO.setTdsCutOffAmount(rs.getBigDecimal("cutoff_amount"));
						profVO.setTdsRateForIndiv(rs.getBigDecimal("individual_rate"));
						profVO.setTdsRateForOther(rs.getBigDecimal("other_rate"));
						profVO.setVirtualAccNo(rs.getString("virtual_acc_no"));
						profVO.setThresholdBalance(rs.getBigDecimal("threshold_balance"));
						profVO.setTags(rs.getString("tags"));
						profVO.setProfileCode(rs.getString("profile_code"));
					    acHVO.setProfileVO(profVO);
						return acHVO;
					}

				   };
			   ledgerList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			
			}else {
			  str = "SELECT * FROM (SELECT al.*,group_name,ag.root_id,asg.normal_balance,short_name,asg.root_group_name FROM account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups ag ,account_root_group asg WHERE al.sub_group_id=ag.id AND al.is_deleted=0 AND al.org_id=:orgID  AND ag.root_id=asg.root_id AND ag.category_id=:categoryID ORDER BY ag.id) AS acc LEFT JOIN acc_profiles c ON acc.id=c.ledger_id and acc.org_id=c.org_id and c.org_id=:orgID AND c.profile_type=:type ;";
			  
			  log.debug("sql query is "+str); 
			   Map hMap=new HashMap();
			   hMap.put("orgID", orgID);
			   hMap.put("categoryID", categoryID);
			   hMap.put("type",type);
			   
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						AccountHeadVO acHVO=new AccountHeadVO();
						ProfileDetailsVO profVO=new ProfileDetailsVO();
						acHVO.setLedgerID(rs.getInt("id"));
						acHVO.setOrgID(rs.getInt("org_id"));
						acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
						acHVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
						acHVO.setClosingBalance(rs.getBigDecimal("closing_balance"));
						acHVO.setIntOpnBalance(rs.getBigDecimal("int_balance"));
						acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
						acHVO.setAccountGroupID(rs.getInt("sub_group_id"));
						acHVO.setCreateDate(rs.getString("create_date"));
						acHVO.setUpdateDate(rs.getString("update_date"));
						acHVO.setEffectiveDate(rs.getString("effective_date"));
						acHVO.setNormalBalance(rs.getString("normal_balance"));
						acHVO.setRootID(rs.getInt("root_id"));
						acHVO.setIsHidden(rs.getInt("is_hidden"));
						acHVO.setDescription(rs.getString("description"));
						acHVO.setIsPrimary(rs.getInt("is_primary"));
						acHVO.setLedgerShortName(rs.getString("short_name"));
						acHVO.setLockDate(rs.getString("lock_date"));
						acHVO.setRootGroupName(rs.getString("root_group_name"));
						acHVO.setAccType(rs.getString("type"));
						acHVO.setGroupTags(rs.getString("group_tags"));
						acHVO.setProjectTags(rs.getString("project_tags"));
						profVO.setProfileID(rs.getInt("profile_id"));
						profVO.setOrgID(rs.getInt("org_id"));
						profVO.setTitle(rs.getString("title"));
						profVO.setProfileName(rs.getString("profile_name"));
						profVO.setProfileEmailID(rs.getString("email"));
						profVO.setProfileMobile(rs.getString("mobile"));
						profVO.setProfileType(rs.getString("profile_type"));
						profVO.setProfileLedgerID(rs.getInt("ledger_id"));
						profVO.setGstIN(rs.getString("gstin"));
						profVO.setPanNo(rs.getString("pan_no"));
						profVO.setPanName(rs.getString("pan_name"));
						profVO.setTanNO(rs.getString("tan_no"));					
						profVO.setAdditionalProps(rs.getString("ledger_meta"));
						profVO.setDeducteeCode(rs.getString("deductee_code"));
						profVO.setTdsSectionID(rs.getString("tds_section_id"));
						profVO.setTdsSectionNo(rs.getString("section_no"));
						profVO.setTdsSectionName(rs.getString("section_name"));
						profVO.setTdsCutOffAmount(rs.getBigDecimal("cutoff_amount"));
						profVO.setTdsRateForIndiv(rs.getBigDecimal("individual_rate"));
						profVO.setTdsRateForOther(rs.getBigDecimal("other_rate"));
						profVO.setVirtualAccNo(rs.getString("virtual_acc_no"));
						profVO.setThresholdBalance(rs.getBigDecimal("threshold_balance"));
						profVO.setTags(rs.getString("tags"));
						profVO.setProfileCode(rs.getString("profile_code"));
					    acHVO.setProfileVO(profVO);
						return acHVO;
					}

				   };
			   ledgerList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			}  				
			 
			   log.debug("Exit :  public List getLedgersList(String societyID,String ledgerType)"+ledgerList.size());
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at List getLedgersList(TransactionVO transactionVO) "+Ex);
			
			}catch(Exception se){
				log.error("Exception : public List getLedgersList(String societyID,String ledgerType) "+se);
			   
			}
		return ledgerList;
	}
	
	/* Used to get ledger List related for customer / vendors current non current */
	public List getCustomerVendorLedgersCNCList(int orgID,int categoryID1,int categoryID2,String type)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public List getLedgersList(String societyID,String ledgerType)");
		List ledgerList=null;
			
		try{
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			  String str = "SELECT * FROM (SELECT al.*,group_name,ag.root_id,asg.normal_balance,short_name,asg.root_group_name FROM account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups ag ,account_root_group asg WHERE al.sub_group_id=ag.id AND al.is_deleted=0 AND al.org_id=:orgID  AND ag.root_id=asg.root_id AND ( ag.category_id=:categoryID1 or ag.category_id=:categoryID2) ORDER BY ag.id) AS acc LEFT JOIN acc_profiles c ON acc.id=c.ledger_id and acc.org_id=c.org_id and c.org_id=:orgID AND c.profile_type=:type ;";
			   log.debug("sql query is "+str); 
			   				
			   
			   Map hMap=new HashMap();
			   hMap.put("orgID", orgID);
			   hMap.put("categoryID1", categoryID1);
			   hMap.put("categoryID2", categoryID2);
			   hMap.put("type",type);
			   
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						AccountHeadVO acHVO=new AccountHeadVO();
						ProfileDetailsVO profVO=new ProfileDetailsVO();
						acHVO.setLedgerID(rs.getInt("id"));
						acHVO.setOrgID(rs.getInt("org_id"));
						acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
						acHVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
						acHVO.setClosingBalance(rs.getBigDecimal("closing_balance"));
						acHVO.setIntOpnBalance(rs.getBigDecimal("int_balance"));
						acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
						acHVO.setAccountGroupID(rs.getInt("sub_group_id"));
						acHVO.setCreateDate(rs.getString("create_date"));
						acHVO.setUpdateDate(rs.getString("update_date"));
						acHVO.setEffectiveDate(rs.getString("effective_date"));
						acHVO.setNormalBalance(rs.getString("normal_balance"));
						acHVO.setRootID(rs.getInt("root_id"));
						acHVO.setIsHidden(rs.getInt("is_hidden"));
						acHVO.setDescription(rs.getString("description"));
						acHVO.setIsPrimary(rs.getInt("is_primary"));
						acHVO.setLedgerShortName(rs.getString("short_name"));
						acHVO.setLockDate(rs.getString("lock_date"));
						acHVO.setRootGroupName(rs.getString("root_group_name"));
						acHVO.setAccType(rs.getString("type"));
						acHVO.setGroupTags(rs.getString("group_tags"));
						acHVO.setProjectTags(rs.getString("project_tags"));
						profVO.setProfileID(rs.getInt("profile_id"));
						profVO.setOrgID(rs.getInt("org_id"));
						profVO.setTitle(rs.getString("title"));
						profVO.setProfileName(rs.getString("profile_name"));
						profVO.setProfileEmailID(rs.getString("email"));
						profVO.setProfileMobile(rs.getString("mobile"));
						profVO.setProfileType(rs.getString("profile_type"));
						profVO.setProfileLedgerID(rs.getInt("ledger_id"));
						profVO.setGstIN(rs.getString("gstin"));
						profVO.setPanNo(rs.getString("pan_no"));
						profVO.setPanName(rs.getString("pan_name"));
						profVO.setTanNO(rs.getString("tan_no"));					
						profVO.setAdditionalProps(rs.getString("ledger_meta"));
						profVO.setDeducteeCode(rs.getString("deductee_code"));
						profVO.setTdsSectionID(rs.getString("tds_section_id"));
						profVO.setTdsSectionNo(rs.getString("section_no"));
						profVO.setTdsSectionName(rs.getString("section_name"));
						profVO.setTdsCutOffAmount(rs.getBigDecimal("cutoff_amount"));
						profVO.setTdsRateForIndiv(rs.getBigDecimal("individual_rate"));
						profVO.setTdsRateForOther(rs.getBigDecimal("other_rate"));
						profVO.setVirtualAccNo(rs.getString("virtual_acc_no"));
						profVO.setThresholdBalance(rs.getBigDecimal("threshold_balance"));
						profVO.setTags(rs.getString("tags"));
						profVO.setProfileCode(rs.getString("profile_code"));
					    acHVO.setProfileVO(profVO);
						return acHVO;
					}

				   };
			   ledgerList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit :  public List getLedgersList(String societyID,String ledgerType)"+ledgerList.size());
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at List getLedgersList(TransactionVO transactionVO) "+Ex);
			
			}catch(Exception se){
				log.error("Exception : public List getLedgersList(String societyID,String ledgerType) "+se);
			   
			}
		return ledgerList;
	}
	
	
	/* Used to get list of ledgers which are noted with some properties*/
	public List getLedgersListByProperties(int orgID,String properties)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public List getLedgersListByProperties(int societyID,String properties)");
		List ledgerList=null;
	
		
		
		try{
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			  String str = "SELECT al.*,group_name,ag.root_id,asg.normal_balance,short_name,asg.root_group_name,ledger_meta FROM account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups ag ,account_root_group asg ,account_ledger_profile_details ap WHERE al.sub_group_id=ag.id and al.is_deleted=0 AND al.id=ap.ledger_id and al.org_id=ap.org_id and ap.org_id=:orgID and  ag.root_id=asg.root_id  and  ap.ledger_meta  LIKE '"+properties+"' ;";
			   log.debug("sql query is "+str); 
			   				
			   
			   Map hMap=new HashMap();
			   hMap.put("orgID", orgID);
			   hMap.put("properties", properties);
			  	  
			   
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						AccountHeadVO acHVO=new AccountHeadVO();
						acHVO.setLedgerID(rs.getInt("id"));
						acHVO.setOrgID(rs.getInt("org_id"));
						acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
						acHVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
						acHVO.setClosingBalance(rs.getBigDecimal("closing_balance"));
						acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
						acHVO.setAccountGroupID(rs.getInt("sub_group_id"));
						acHVO.setCreateDate(rs.getString("create_date"));
						acHVO.setUpdateDate(rs.getString("update_date"));
						acHVO.setEffectiveDate(rs.getString("effective_date"));
						acHVO.setNormalBalance(rs.getString("normal_balance"));
						acHVO.setRootID(rs.getInt("root_id"));
						acHVO.setIsHidden(rs.getInt("is_hidden"));
						acHVO.setDescription(rs.getString("description"));
						acHVO.setIsPrimary(rs.getInt("is_primary"));
						acHVO.setLedgerShortName(rs.getString("short_name"));
						acHVO.setLockDate(rs.getString("lock_date"));
						acHVO.setRootGroupName(rs.getString("root_group_name"));
						acHVO.setAccType(rs.getString("type"));
						acHVO.setAdditionalProps(rs.getString("ledger_meta"));
						acHVO.setGroupTags(rs.getString("group_tags"));
						acHVO.setProjectTags(rs.getString("project_tags"));
						
						return acHVO;
					}

				   };
			   ledgerList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit :  public List getLedgersListByProperties(int societyID,String properties)"+ledgerList.size());
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at public List getLedgersListByProperties(int societyID,String properties) "+Ex);
			
			}catch(Exception se){
				log.error("Exception : public List getLedgersListByProperties(int societyID,String properties) "+se);
			   
			}
		return ledgerList;
	}
	
	/* Used to get List of tds Ledgers for transaction */
	public List getTDSLedgersList(int orgID)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public List getTDSLedgersList(int societyID)");
		List ledgerList=null;
		String strCondition="";
		
		
		
		try{
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			  String str = "SELECT al.*,group_name,ag.root_id,asg.normal_balance,short_name,asg.root_group_name,tds_ledger_id ,service_ledger_id FROM account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups ag ,account_root_group asg, vendor_ledger_relation v,society_settings s WHERE al.sub_group_id=ag.id AND v.vendor_ledger_id=al.id AND al.is_deleted=0 AND al.org_id=s.society_id and  s.society_id=:societyID  AND v.society_id=s.society_id AND ag.root_id=asg.root_id AND is_hidden='0' ;";
			   log.debug("sql query is "+str); 
			   				
			   
			   Map hMap=new HashMap();
			   hMap.put("societyID", orgID);
		
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						AccountHeadVO acHVO=new AccountHeadVO();
						acHVO.setLedgerID(rs.getInt("id"));
						acHVO.setOrgID(rs.getInt("org_id"));
						acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
						acHVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
						acHVO.setClosingBalance(rs.getBigDecimal("closing_balance"));
						acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
						acHVO.setAccountGroupID(rs.getInt("sub_group_id"));
						acHVO.setCreateDate(rs.getString("create_date"));
						acHVO.setUpdateDate(rs.getString("update_date"));
						acHVO.setEffectiveDate(rs.getString("effective_date"));
						acHVO.setNormalBalance(rs.getString("normal_balance"));
						acHVO.setRootID(rs.getInt("root_id"));
						acHVO.setIsHidden(rs.getInt("is_hidden"));
						acHVO.setDescription(rs.getString("description"));
						acHVO.setIsPrimary(rs.getInt("is_primary"));
						acHVO.setLedgerShortName(rs.getString("short_name"));
						acHVO.setLockDate(rs.getString("lock_date"));
						acHVO.setRootGroupName(rs.getString("root_group_name"));
						acHVO.setAccType(rs.getString("type"));
						acHVO.setTdsLedgerID(rs.getInt("tds_ledger_id"));
						acHVO.setServiceLedgerID(rs.getInt("service_ledger_id"));
						acHVO.setGroupTags(rs.getString("group_tags"));
						acHVO.setProjectTags(rs.getString("project_tags"));
						
						return acHVO;
					}

				   };
			   ledgerList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit :  public List getLedgersList(String societyID,String ledgerType)"+ledgerList.size());
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at List getLedgersList(TransactionVO transactionVO) "+Ex);
			
			}catch(Exception se){
				log.error("Exception : public List getLedgersList(String societyID,String ledgerType) "+se);
			   
			}
		return ledgerList;
	}
	
	/* Used to get TDS ledger Details */
	public TDSDetailsVO getLedgerListForTDSCalculation(int societyID,int ledgerID,int serviceLedgerID)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public TDSDetailsVO getLedgerListForTDSCalculation(int societyID,int ledgerID,int serviceLedgerID) ");
		TDSDetailsVO tdsVO=new TDSDetailsVO();
	
		
		
		try{
			  String str = "SELECT * FROM vendor_ledger_relation v,tds_rate_details t,vendor_details vd WHERE v.tds_section_id=t.id AND v.vendor_id=vd.vendor_id AND v.society_id=:societyID AND v.service_ledger_id=:serviceLedgerID and v.vendor_ledger_id=:ledgerID ;";
			   log.debug("sql query is "+str); 
			   				
			   
			   Map hMap=new HashMap();
			   hMap.put("societyID", societyID);
			   hMap.put("ledgerID", ledgerID);
			   hMap.put("serviceLedgerID", serviceLedgerID);
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						TDSDetailsVO acHVO=new TDSDetailsVO();
						acHVO.setLedgerID(rs.getInt("vendor_ledger_id"));
						acHVO.setVendorID(rs.getInt("vendor_id"));
						acHVO.setDeducteeCode(rs.getInt("deductee_code"));
						acHVO.setSocietyID(rs.getInt("society_id"));
						acHVO.setTdsSectionID(rs.getInt("tds_section_id"));
						acHVO.setTdsSectionNo(rs.getString("section_no"));
						acHVO.setTdsSectionName(rs.getString("section_name"));
						acHVO.setTdsCutOffAmount(rs.getBigDecimal("cutoff_amount"));
						acHVO.setTdsRateForIndiv(rs.getBigDecimal("individual_rate"));
						acHVO.setTdsRateForOther(rs.getBigDecimal("other_rate"));
						acHVO.setServiceLedgerID(rs.getInt("service_ledger_id"));
						acHVO.setServiceLedgerName(rs.getString("service_name"));
					
						return acHVO;
					}

				   };
			   tdsVO=(TDSDetailsVO) namedParameterJdbcTemplate.queryForObject(
						str, hMap, Rmapper);
			   log.debug("Exit :  public List getLedgerListForTDSCalculation(String societyID,String ledgerType)");
		}catch(EmptyResultDataAccessException Ex){
			log.info("No Data found at List getLedgerListForTDSCalculation(TransactionVO transactionVO) "+Ex);
			
			}catch(Exception se){
				log.error("Exception : public List getLedgerListForTDSCalculation(String societyID,String ledgerType) "+se);
			   
			}
		return tdsVO;
	}
	
	/* Used for updating ledgerBalance */
	public int updateLedgersBalance(LedgerEntries ledgerEntry,int orgID)  {
		// TODO Auto-generated method stub
		int success=0;
		log.debug("Entry : public int updateLedgersBalance(LedgerEntries ledgerEntry)");
		try{
//			
		     		     
			
			   //Assume a valid connection object conn
			  
			   java.util.Date today = new java.util.Date();
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			   
			   SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			   String str = "UPDATE account_ledgers_"+societyVO.getDataZoneID()+" SET closing_balance=:balance,update_date=:updateDate WHERE id=:ledgerID and org_id=:orgID ";				
				   
			   
			   Map hMap=new HashMap();
			   hMap.put("ledgerID", ledgerEntry.getLedgerID());
			   hMap.put("updateDate", currentTimestamp);
			   hMap.put("balance", ledgerEntry.getBalance());
			   hMap.put("orgID", orgID);
			   
			   success=namedParameterJdbcTemplate.update(str, hMap);
			   log.debug("Exit : public int updateLedgersBalance(LedgerEntries ledgerEntry)");
			}catch(Exception se){
				log.debug("Exception : public int updateLedgersBalance(LedgerEntries ledgerEntry) "+se);
			   success=0;
			}
		return success;
	}

	
	public List getReconciledBalance(int orgID,int ledgerID,String fromDate,String uptoDate) {
		List ledgerList=new ArrayList();

		try {
			log.debug("Entry : public List getReconciledBalance(int societyID,int ledgerID,String fromDate,String uptoDate))"
							+ orgID);
			String strWhereClause=null;
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			//String sqlDue="SELECT MAX(tx_to_date) , soc_tx_"+intSocietyID+".*, MAX(tx_to_date) AS maxdt,member_details.* FROM soc_tx_"+intSocietyID+" ,member_details WHERE member_details.member_id=soc_tx_"+intSocietyID+".table_primary_id AND charge_type = 'Monthly maintenance Charges' GROUP BY soc_tx_"+intSocietyID+".table_primary_id;";
			String sql = " SELECT  ale.ledger_id,al.ledger_name,al.opening_balance, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount)) AS sumAmt, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS closing_balance "+
						" FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale "+ 
						" WHERE ats.org_id=ale.org_id and al.org_id=ale.org_id AND ats.tx_id=ale.tx_id AND ats.tx_date < :uptoDate AND ats.is_deleted=0 AND ale.ledger_id = al.id AND ale.ledger_id=:ledgerID AND (ats.bank_date!='')  and al.org_id"+
						" GROUP BY ale.ledger_id ORDER BY al.id ";
			
			/*String sqlDue = "SELECT * FROM member_details m,apartment_details a,building_details b,ledger_user_mapping_"+societyID+" lu "
					+ " WHERE m.apt_id=a.apt_id   "+strWhereClause+" AND a.building_id=b.building_id AND m.member_id=lu.user_id AND lu.ledger_type='M' AND b.society_id=:societyID order by seq_no;";*/
					
			 
			Map hMap = new HashMap();
			hMap.put("orgID", orgID);
			hMap.put("uptoDate", uptoDate);
			hMap.put("ledgerID", ledgerID);
			

			RowMapper rMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int num) throws SQLException {
					// TODO Auto-generated method stub
					AccountHeadVO bankVO = new AccountHeadVO();
					bankVO.setLedgerID(rs.getInt("ledger_id"));
					bankVO.setClosingBalance(rs.getBigDecimal("closing_balance"));	
					
					return bankVO;
				}
			};
			ledgerList = namedParameterJdbcTemplate
					.query(sql, hMap, rMapper);

			log.debug("Outside Query : " + ledgerList.size());
			log.debug("Exit : public List getReconciledBalance(int societyID,int ledgerID,String fromDate,String uptoDate)"
							);
		}
		 catch (BadSqlGrammarException ex) {
		log.info("Exception in public List getReconciledBalance(int societyID,int ledgerID,String fromDate,String uptoDate) : " + ex);
		 }catch(IllegalArgumentException ex)
		{
			//ex.printStackTrace();
			ledgerList=null;
			log.info("Exception in  : "+ex);
		}
		
		catch (Exception ex) {
			log.error("Exception in getClosingBalances : " + ex);
		}
		return ledgerList;
	}
	
	/*Get reconciled transactions balance*/
	public AccountHeadVO getReconciledTXBalance(int orgID,int ledgerID,String fromDate,String uptoDate) {
		AccountHeadVO bankAcVO=new AccountHeadVO();

		try {
			log.debug("Entry : public List getReconciledTXBalance(int societyID,int ledgerID,String fromDate,String uptoDate))"
							+ orgID+ledgerID+fromDate+uptoDate);
			String strWhereClause=null;
			
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String sql = " SELECT  ale.ledger_id,al.ledger_name,al.opening_balance, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount)) AS sumAmt, IF(SUM(ale.amount) IS NULL, 0, SUM(ale.amount) + al.opening_balance) AS closing_balance "+
						" FROM account_ledgers_"+societyVO.getDataZoneID()+" al, account_transactions_"+societyVO.getDataZoneID()+" ats,account_ledger_entry_"+societyVO.getDataZoneID()+" ale "+ 
						" WHERE ats.org_id=al.org_id and al.org_id=ale.org_id and ats.tx_id=ale.tx_id  AND (ats.bank_date BETWEEN :fromDate AND :uptoDate) AND ats.is_deleted=0 AND ale.ledger_id = al.id AND ale.ledger_id=:ledgerID  AND ats.bank_date IS NOT NULL and al.org_id=:orgID  "+
						" GROUP BY ale.ledger_id ORDER BY al.id ";
			
			log.debug("sql query is "+sql); 
				
			Map hMap = new HashMap();
			hMap.put("orgID", orgID);
			hMap.put("fromDate", fromDate);
			hMap.put("uptoDate", uptoDate);
			hMap.put("ledgerID", ledgerID);
			

			RowMapper rMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int num) throws SQLException {
					// TODO Auto-generated method stub
					AccountHeadVO bankVO = new AccountHeadVO();
					bankVO.setLedgerID(rs.getInt("ledger_id"));
					bankVO.setSum(rs.getBigDecimal("sumAmt"));	
					bankVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));	
					bankVO.setClosingBalance(rs.getBigDecimal("closing_balance"));	
					
					
					return bankVO;
				}
			};
			bankAcVO = (AccountHeadVO) namedParameterJdbcTemplate
					.queryForObject(sql, hMap, rMapper);

			log.debug("Outside Query : " + bankAcVO.getSum());
			log.debug("Exit : public List getReconciledTXBalance(int societyID,int ledgerID,String fromDate,String uptoDate)"
							);
		}
		catch (EmptyResultDataAccessException ex) {
			log.info("No Data found for the given period " + ex);
			bankAcVO.setSum(BigDecimal.ZERO);
		}
		 catch (NullPointerException ex) {
				log.info("No Data found for the given period " + ex);
				bankAcVO.setSum(BigDecimal.ZERO);
		 }
		 catch (BadSqlGrammarException ex) {
		log.info("Exception in public List getReconciledTXBalance(int societyID,int ledgerID,String fromDate,String uptoDate) : " + ex);
		 }catch(IllegalArgumentException ex)
		{
			//ex.printStackTrace();
			
			log.info("Exception in  : "+ex);
		}
		
		catch (Exception ex) {
			log.error("Exception in getClosingBalances : " + ex);
		}
		return bankAcVO;
	}
	
	public AccountHeadVO getLedgerByType(int orgID,String type){
		AccountHeadVO achVO=new AccountHeadVO();
		log.debug("Entry : public AccountHeadVO getLedgerByType(int orgID)");
		
		try {
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String str = "SELECT * FROM account_ledgers_"+societyVO.getDataZoneID()+" WHERE type=:type AND is_deleted=0 and org_id=:orgID group by id; ";
			   log.debug("sql query is "+str); 
			   				
			   
			   Map hMap=new HashMap();
			   hMap.put("orgID", orgID);
			   hMap.put("type", type);
			 
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						AccountHeadVO acHVO=new AccountHeadVO();
						acHVO.setLedgerID(rs.getInt("id"));
						acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
						acHVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
						acHVO.setClosingBalance(rs.getBigDecimal("closing_balance"));
						
						acHVO.setAccountGroupID(rs.getInt("sub_group_id"));
						return acHVO;
					}};
					
					achVO=(AccountHeadVO) namedParameterJdbcTemplate.queryForObject(str, hMap, Rmapper);
		
		}catch(EmptyResultDataAccessException Ex){
			log.info("No "+type+" found for Organization "+orgID+" "+Ex);
			
			
		} catch (Exception e) {
			log.error("Exception at public AccountHeadVO getLedgerByType "+e);
		}		
		log.debug("Exit : public AccountHeadVO getDiscountLedger(int orgID)");
		return achVO;
	}
	
	public List getSubAccountList(int orgID,int parentID){
		List ledgerList=null;
		log.debug("Entry : public List getSubAccountList(int orgID,int parentID)");

			try{
				SocietyVO societyVO=societyService.getSocietyDetails(orgID);
				  String str ="SELECT al.*,group_name,ag.root_id,asg.normal_balance,short_name,asg.root_group_name FROM account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups ag ,account_root_group asg WHERE al.org_id=:orgID and al.sub_group_id=ag.id AND al.is_deleted=0 AND ag.root_id=asg.root_id AND al.parent_ledger_id=:parentID AND is_primary=1; ";
				   log.debug("sql query is "+str); 
				   				
				   
				   Map hMap=new HashMap();
				   hMap.put("orgID", orgID);
				   hMap.put("parentID", parentID);
				  
				   RowMapper Rmapper = new RowMapper() {
						public Object mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							
							AccountHeadVO acHVO=new AccountHeadVO();
							acHVO.setLedgerID(rs.getInt("id"));
							acHVO.setOrgID(rs.getInt("org_id"));
							acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
							acHVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
							acHVO.setClosingBalance(rs.getBigDecimal("closing_balance"));
							acHVO.setStrAccountPrimaryHead(rs.getString("group_name"));
							acHVO.setAccountGroupID(rs.getInt("sub_group_id"));
							acHVO.setCreateDate(rs.getString("create_date"));
							acHVO.setUpdateDate(rs.getString("update_date"));
							acHVO.setEffectiveDate(rs.getString("effective_date"));
							acHVO.setNormalBalance(rs.getString("normal_balance"));
							acHVO.setRootID(rs.getInt("root_id"));
							acHVO.setIsHidden(rs.getInt("is_hidden"));
							acHVO.setDescription(rs.getString("description"));
							acHVO.setIsPrimary(rs.getInt("is_primary"));
							acHVO.setLedgerShortName(rs.getString("short_name"));
							acHVO.setLockDate(rs.getString("lock_date"));
							acHVO.setRootGroupName(rs.getString("root_group_name"));
							acHVO.setAccType(rs.getString("type"));
							return acHVO;
						}

					   };
				   ledgerList=namedParameterJdbcTemplate.query(
							str, hMap, Rmapper);
		
			
							
			log.debug("Exit : public List getSubAccountList(int orgID,int parentID)"+ledgerList.size());
		} catch (Exception e) {
			log.error("Exception : public List getSubAccountList(int orgID,int parentID)"+e);
		}
		return ledgerList;
	}
	
	public ProfileDetailsVO getVendorDetails(int orgID,int ledgerID){
		ProfileDetailsVO vendorDetails=new ProfileDetailsVO();
		log.debug("Entry : public ProfileDetailsVO getVendorDetailsList(int orgID,int ledgerID)");

			try{
				  String str ="SELECT * FROM vendor_details v,vendor_ledger_relation vr,tax_details t WHERE v.vendor_id=vr.vendor_id AND vr.society_id=:orgID AND vr.vendor_ledger_id=:ledgerID  "+
						  				" AND v.vendor_id=t.entity_id AND t.entity='v' ;";
				   log.debug("sql query is "+str); 
				   				
				   
				   Map hMap=new HashMap();
				   hMap.put("orgID", orgID);
				   hMap.put("ledgerID", ledgerID);
				  
				   RowMapper Rmapper = new RowMapper() {
						public Object mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							
							ProfileDetailsVO profileVO=new ProfileDetailsVO();
							profileVO.setProfileID(rs.getInt("vendor_id"));
							profileVO.setProfileName(rs.getString("company_name"));
							profileVO.setGstIN(rs.getString("gstin"));
							profileVO.setPanNo(rs.getString("pan_no"));
							profileVO.setCntctPrsnEmail(rs.getString("authorized_person_email"));
							profileVO.setCntctPrsnMobile(rs.getString("authorized_person_mobile"));
							profileVO.setTanNO(rs.getString("tan_no"));
							
							
							return profileVO;
						}

					   };
				   vendorDetails=(ProfileDetailsVO)namedParameterJdbcTemplate.queryForObject(
							str, hMap, Rmapper);
		
			
							
			log.debug("Exit : public ProfileDetailsVO getVendorDetailsList(int orgID,int ledgerID)");
			}catch(EmptyResultDataAccessException e){
				log.debug("Here no profile details found for ledger ID "+ledgerID+"  and Org ID "+orgID);
		} catch (Exception e) {
			log.error("Exception : public ProfileDetailsVO getVendorDetailsList(int orgID,int ledgerID)"+e);
		}
		return vendorDetails;
	}
	
	
	public ProfileDetailsVO getCustomerDetails(int orgID,int ledgerID){
		ProfileDetailsVO customerDetails=new ProfileDetailsVO();
		log.debug("Entry : public ProfileDetailsVO getCustomerDetails(int orgID,int ledgerID)");

			try{
				  String str ="SELECT * FROM customer_details c,customer_org_mapping cr,tax_details t WHERE c.customer_id=cr.cust_id AND cr.org_id=:orgID AND cr.ledger_id=:ledgerID  "+
						  				" AND c.customer_id=t.entity_id AND t.entity='C' ;";
				   log.debug("sql query is "+str); 
				   				
				   
				   Map hMap=new HashMap();
				   hMap.put("orgID", orgID);
				   hMap.put("ledgerID", ledgerID);
				  
				   RowMapper Rmapper = new RowMapper() {
						public Object mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							
							ProfileDetailsVO profileVO=new ProfileDetailsVO();
							profileVO.setProfileID(rs.getInt("customer_id"));
							profileVO.setProfileName(rs.getString("full_name"));
							profileVO.setGstIN(rs.getString("gstin"));
							profileVO.setPanNo(rs.getString("pan_no"));
							profileVO.setCntctPrsnEmail(rs.getString("authorized_person_email"));
							profileVO.setCntctPrsnMobile(rs.getString("authorized_person_mobile"));
							profileVO.setTanNO(rs.getString("tan_no"));
							
							
							return profileVO;
						}

					   };
					   customerDetails=(ProfileDetailsVO)namedParameterJdbcTemplate.queryForObject(
							str, hMap, Rmapper);
		
			
							
			log.debug("Exit : public ProfileDetailsVO getCustomerDetails(int orgID,int ledgerID)");
			}catch(EmptyResultDataAccessException e){
				log.debug("Here no profile details found for ledger ID "+ledgerID+"  and Org ID "+orgID);
		
		} catch (Exception e) {
			log.error("Exception : public ProfileDetailsVO getCustomerDetails(int orgID,int ledgerID)"+e);
		}
		return customerDetails;
	}
	

	


	/* Used for adding   ledger  profile*/
	public int insertLedgerProfile(ProfileDetailsVO profileVO,AccountHeadVO achVO)  {
		// TODO Auto-generated method stub
		int success=0;
		
		log.debug("Entry :public int insertLedgerProfile(ProfileDetailsVO profileVO,AccountHeadVO achVO)"+achVO.getAccountGroupID());
		try{
			  
			   java.util.Date today = new java.util.Date();
			   java.sql.Date currentDate=new java.sql.Date(today.getTime());
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			   String strCondtion="";
			 
			   
			   String str = "INSERT INTO `account_ledger_profile_details`(`org_id`,`ledger_id`,`title`,`profile_name`,`email`,`mobile`,`profile_type`,`ledger_meta`,`description`,`pan_no`,`tan_no`,`gstin`,`deductee_code`,`tds_section_id`,`pan_name`,`tags`,`profile_code`)"
			   		+ " VALUES ( :orgID,:ledgerID,:title,:profileName,:email,:mobile,:profileType,:ledgerMeta,:description,:panNo,:tanNo,:gstIN,:deducteeCode, :tdsSectionID,:panName,:tags, :profileCode); ";

			   Map hMap=new HashMap();
			   hMap.put("orgID", profileVO.getOrgID());
			   hMap.put("ledgerID", profileVO.getProfileLedgerID());
			   hMap.put("title", profileVO.getTitle());
			   hMap.put("profileName", profileVO.getProfileName());
			   hMap.put("email", profileVO.getProfileEmailID());
			   hMap.put("mobile", profileVO.getProfileMobile());
			   hMap.put("profileType", profileVO.getProfileType());
			   hMap.put("ledgerMeta", achVO.getAdditionalProps());
			   hMap.put("description", achVO.getDescription());
			   hMap.put("panNo", profileVO.getPanNo());
			   hMap.put("panName", profileVO.getPanName());
			   hMap.put("tanNo", profileVO.getTanNO());
			   hMap.put("gstIN",profileVO.getGstIN());
			   hMap.put("deducteeCode", profileVO.getDeducteeCode());
			   hMap.put("tdsSectionID", profileVO.getTdsSectionID());
			   hMap.put("tags", profileVO.getTags());
			   hMap.put("profileCode", profileVO.getProfileCode());
			   
			   
			   success=namedParameterJdbcTemplate.update(str, hMap);
			   log.debug("Exit :public int insertLedgerProfile(ProfileDetailsVO profileVO,AccountHeadVO achVO)"+success);
			}catch(Exception se){
				log.debug("Exception :public int insertLedgerProfile(ProfileDetailsVO profileVO,AccountHeadVO achVO)"+se);
			   success=0;
			}
		return success;
	}
	
	
public int insertProfileProperties(ProfileDetailsVO profileVO){
		
		int insertFlag = 0;
		
	    int genID = 0;
	    
		try {
			log.debug("Entry : public int insertProfileProperties(ProfileDetailsVO profileVO)");
			
		    String insertFileString = "INSERT INTO tax_details(entity_id,entity,pan_no,tan_no,gstin,authorized_person_name,authorized_person_email,authorized_person_mobile,org_id) " +
		    		"VALUES( :entityID, :entity, :panNo, :tanNo, :gstIN, :authorizedPersonName, :authorizedPersonEmail, :authorizedPersonMobile,:orgID)";
		    log.debug("query : " + insertFileString);
		    
		    Map hmap=new HashMap();
			hmap.put("entityID", profileVO.getProfileLedgerID());
			hmap.put("entity", profileVO.getProfileType());
			hmap.put("panNo", profileVO.getPanNo());
			hmap.put("gstIN", profileVO.getGstIN());
			hmap.put("tanNo", profileVO.getTanNO());
			hmap.put("authorizedPersonName", profileVO.getContactPrsnName());
			hmap.put("authorizedPersonEmail",profileVO.getCntctPrsnEmail());
			hmap.put("authorizedPersonMobile", profileVO.getCntctPrsnMobile());
			hmap.put("orgID", profileVO.getOrgID());
			
			insertFlag=namedParameterJdbcTemplate.update(insertFileString,hmap);		
		   
		    log.debug("Exit : public int insertProfileProperties(ProfileDetailsVO profileVO)"+genID);
		}catch (DataAccessException exc) {
		  log.error("DataAccessException : public int insertProfileProperties(ProfileDetailsVO profileVO)" + exc);
		}
		catch (Exception e) {
		  log.error("Exception in public int insertProfileProperties(ProfileDetailsVO profileVO) : " + e);
		}

		return genID;
		}

public ProfileDetailsVO getLedgerProfileDetails(int ledgerID,int orgID){
	ProfileDetailsVO profileVO=new ProfileDetailsVO();

	log.debug("Entry : public ProfileDetailsVO getLedgerProfileDetails(int ledgerID,int orgID)");
		try{
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String sqlQuery=" SELECT  l.*,r.* from (SELECT l.*,a.ledger_name,g.category_id FROM account_ledger_profile_details l, account_ledgers_"+societyVO.getDataZoneID()+" a ,account_groups g WHERE a.sub_group_id=g.id AND a.org_id=l.org_id AND l.ledger_id=a.id AND a.is_deleted=0 AND a.org_id=:orgID AND a.id=:ledgerID) l  LEFT JOIN tds_rate_details r ON r.id=l.tds_section_id  "
					      + " Where l.ledger_id=:ledgerID and l.org_id=:orgID ;";
                   
							
			log.debug(sqlQuery);
			 
			 Map hMap=new HashMap();
			 hMap.put("ledgerID", ledgerID);
			 hMap.put("orgID", orgID);
			
			
			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
										
					ProfileDetailsVO acHVO=new ProfileDetailsVO();
					acHVO.setProfileID(rs.getInt("id"));
					acHVO.setOrgID(rs.getInt("org_id"));
					acHVO.setTitle(rs.getString("title"));
					acHVO.setProfileName(rs.getString("profile_name"));
					acHVO.setProfileEmailID(rs.getString("email"));
					acHVO.setProfileMobile(rs.getString("mobile"));
					acHVO.setProfileType(rs.getString("profile_type"));
					acHVO.setProfileLedgerID(rs.getInt("ledger_id"));
					acHVO.setGstIN(rs.getString("gstin"));
					acHVO.setPanNo(rs.getString("pan_no"));
					acHVO.setPanName(rs.getString("pan_name"));
					acHVO.setTanNO(rs.getString("tan_no"));					
					acHVO.setAdditionalProps(rs.getString("ledger_meta"));
					acHVO.setDeducteeCode(rs.getString("deductee_code"));
					acHVO.setTdsSectionID(rs.getString("tds_section_id"));
					acHVO.setTdsSectionNo(rs.getString("section_no"));
					acHVO.setTdsSectionName(rs.getString("section_name"));
					acHVO.setTdsCutOffAmount(rs.getBigDecimal("cutoff_amount"));
					acHVO.setTdsRateForIndiv(rs.getBigDecimal("individual_rate"));
					acHVO.setTdsRateForOther(rs.getBigDecimal("other_rate"));
					acHVO.setVirtualAccNo(rs.getString("virtual_acc_no"));
					acHVO.setThresholdBalance(rs.getBigDecimal("threshold_balance"));
					acHVO.setProfileLedgerName(rs.getString("ledger_name"));
					acHVO.setTags(rs.getString("tags"));
					acHVO.setProfileCode(rs.getString("profile_code"));
					acHVO.setCategoryID(rs.getInt("category_id"));
					acHVO.setRateCardID(rs.getInt("rate_card_id"));
					return acHVO;
				}

			   };
		   profileVO=(ProfileDetailsVO) namedParameterJdbcTemplate.queryForObject(sqlQuery, hMap, Rmapper);
		   
		   log.debug("Exit : public ProfileDetailsVO getLedgerProfileDetails(int ledgerID,int orgID)"+profileVO.getProfileLedgerID());
		}catch(EmptyResultDataAccessException Ex){
			log.debug("No Data found public ProfileDetailsVO getLedgerProfileDetails(int ledgerID,int orgID) "+Ex);
			
			
	} catch (Exception e) {
		log.error("Exception : public ProfileDetailsVO getLedgerProfileDetails(int ledgerID,int orgID)"+e);
	}
    	
	return profileVO;
}
	

public List getLedgerProfileDetailsList(int orgID,int groupID,String tags){
	List profileList=new ArrayList();

	log.debug("Entry : public List getLedgerProfileDetails(int profileID)");
	String condition="";
	if(!tags.equalsIgnoreCase("all")){
		condition="AND a.tags LIKE '%"+tags+"%'";
	}
		try{
			SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			String sqlQuery=" SELECT al.*,group_name,ag.root_id,asg.normal_balance,short_name,asg.root_group_name,a.* FROM account_ledgers_"+societyVO.getDataZoneID()+" al,account_groups ag ,account_root_group asg,account_ledger_profile_details a WHERE a.ledger_id=al.id AND a.org_id=al.org_id AND al.sub_group_id=ag.id AND al.is_deleted=0 AND al.org_id=:orgID AND ag.root_id=asg.root_id AND sub_group_id=:groupID "+condition+" ORDER BY ag.id ;";
                   
							
			log.debug(sqlQuery);
			 
			 Map hMap=new HashMap();
			 hMap.put("orgID", orgID);
			 hMap.put("groupID", groupID);
						
			
			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
										
					ProfileDetailsVO acHVO=new ProfileDetailsVO();
					acHVO.setProfileID(rs.getInt("id"));
					acHVO.setOrgID(rs.getInt("org_id"));
					acHVO.setTitle(rs.getString("title"));
					acHVO.setProfileName(rs.getString("profile_name"));
					acHVO.setProfileEmailID(rs.getString("email"));
					acHVO.setProfileMobile(rs.getString("mobile"));
					acHVO.setProfileType(rs.getString("profile_type"));
					acHVO.setProfileLedgerID(rs.getInt("ledger_id"));
					acHVO.setGstIN(rs.getString("gstin"));
					acHVO.setPanNo(rs.getString("pan_no"));
					acHVO.setPanName(rs.getString("pan_name"));
					acHVO.setTanNO(rs.getString("tan_no"));					
					acHVO.setAdditionalProps(rs.getString("ledger_meta"));
					acHVO.setDeducteeCode(rs.getString("deductee_code"));
					acHVO.setTdsSectionID(rs.getString("tds_section_id"));
					acHVO.setVirtualAccNo(rs.getString("virtual_acc_no"));
					acHVO.setThresholdBalance(rs.getBigDecimal("threshold_balance"));
					acHVO.setTags(rs.getString("tags"));
					acHVO.setProfileCode(rs.getString("profile_code"));
					return acHVO;
				}

			   };
		   profileList= namedParameterJdbcTemplate.query(sqlQuery, hMap, Rmapper);
		   
		   log.debug("Exit : public ProfileDetailsVO getLedgerProfileDetails(int profileID)"+profileList);
		}catch(EmptyResultDataAccessException Ex){
				log.debug("No Data found public ProfileDetailsVO getLedgerProfileDetails(int profileID) "+Ex);
			
			
	} catch (Exception e) {
		log.error("Exception : public ProfileDetailsVO getLedgerProfileDetails(int profileID)"+e);
	}
    	
	return profileList;
}

public ProfileDetailsVO getLedgerProfileDetails(int profileID){
	ProfileDetailsVO profileVO=new ProfileDetailsVO();

	log.debug("Entry : public ProfileDetailsVO getLedgerProfileDetails(int profileID)");
		try{
			
			String sqlQuery=" SELECT  l.*,r.* from account_ledger_profile_details l  LEFT JOIN tds_rate_details r ON r.id=l.tds_section_id  "
					      + " Where l.id=:profileID ;";
                   
							
			log.debug(sqlQuery);
			 
			 Map hMap=new HashMap();
			 hMap.put("profileID", profileID);
						
			
			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
										
					ProfileDetailsVO acHVO=new ProfileDetailsVO();
					acHVO.setProfileID(rs.getInt("id"));
					acHVO.setOrgID(rs.getInt("org_id"));
					acHVO.setTitle(rs.getString("title"));
					acHVO.setProfileName(rs.getString("profile_name"));
					acHVO.setProfileEmailID(rs.getString("email"));
					acHVO.setProfileMobile(rs.getString("mobile"));
					acHVO.setProfileType(rs.getString("profile_type"));
					acHVO.setProfileLedgerID(rs.getInt("ledger_id"));
					acHVO.setGstIN(rs.getString("gstin"));
					acHVO.setPanNo(rs.getString("pan_no"));
					acHVO.setPanName(rs.getString("pan_name"));
					acHVO.setTanNO(rs.getString("tan_no"));					
					acHVO.setAdditionalProps(rs.getString("ledger_meta"));
					acHVO.setDeducteeCode(rs.getString("deductee_code"));
					acHVO.setTdsSectionID(rs.getString("tds_section_id"));
					acHVO.setTdsSectionNo(rs.getString("section_no"));
					acHVO.setTdsSectionName(rs.getString("section_name"));
					acHVO.setTdsCutOffAmount(rs.getBigDecimal("cutoff_amount"));
					acHVO.setTdsRateForIndiv(rs.getBigDecimal("individual_rate"));
					acHVO.setTdsRateForOther(rs.getBigDecimal("other_rate"));
					acHVO.setVirtualAccNo(rs.getString("virtual_acc_no"));
					acHVO.setThresholdBalance(rs.getBigDecimal("threshold_balance"));
					acHVO.setTags(rs.getString("tags"));
					acHVO.setProfileCode(rs.getString("profile_code"));
					return acHVO;
				}

			   };
		   profileVO=(ProfileDetailsVO) namedParameterJdbcTemplate.queryForObject(sqlQuery, hMap, Rmapper);
		   
		   log.debug("Exit : public ProfileDetailsVO getLedgerProfileDetails(int profileID)"+profileVO.getProfileLedgerID());
		}catch(EmptyResultDataAccessException Ex){
			profileVO.setProfileID(0);
			log.debug("No Data found public ProfileDetailsVO getLedgerProfileDetails(int profileID) "+Ex);
			
			
	} catch (Exception e) {
		log.error("Exception : public ProfileDetailsVO getLedgerProfileDetails(int profileID)"+e);
	}
    	
	return profileVO;
}

public int updateLedgerProfileDetails(ProfileDetailsVO profileVO) {

	int flag = 0;
	int updateFlag=0;
	int flagM=0;
	String strSQLQuery = "";
	
	try {
		log.debug("Entry : public int updateLedgerProfileDetails(ProfileDetailsVO profileVO)");
		
		strSQLQuery = "UPDATE account_ledger_profile_details c SET title=:title,profile_name=:fullName, email=:emailPrimary, mobile=:contactPrimary, c.org_id=:orgID ,c.pan_no= :panNo,c.pan_name=:panName,  "+
                      " c.tan_no= :tanNo, c.gstin= :gstIN, c.deductee_code=:deducteeCode, c.tds_section_id=:tdsSectionID, c.tags=:tags ,c.profile_code=:profileCode " +
                      " WHERE c.id=:profileID and c.profile_type=:profileType;";
		
		log.debug("query : " + strSQLQuery);

		Map hmap = new HashMap();
		hmap.put("profileID", profileVO.getProfileID());
		hmap.put("profileType", profileVO.getProfileType());
		hmap.put("title", profileVO.getTitle());
		hmap.put("fullName", profileVO.getProfileName());
		hmap.put("emailPrimary", profileVO.getProfileEmailID());
		hmap.put("contactPrimary", profileVO.getProfileMobile());
		hmap.put("orgID", profileVO.getOrgID());
		hmap.put("panNo",profileVO.getPanNo());
		hmap.put("panName", profileVO.getPanName());
		hmap.put("tanNo",profileVO.getTanNO());
		hmap.put("gstIN",profileVO.getGstIN());
		hmap.put("deducteeCode", profileVO.getDeducteeCode());
		hmap.put("tdsSectionID", profileVO.getTdsSectionID());
		hmap.put("tags", profileVO.getTags());
		hmap.put("profileCode", profileVO.getProfileCode());
		
		flag=namedParameterJdbcTemplate.update(strSQLQuery, hmap);		
		
		log.debug("Exit : public int updateLedgerProfileDetails(ProfileDetailsVO profileVO) "+flag);

	} catch (Exception Ex) {
		log.error("Exception in updateLedgerProfileDetails(ProfileDetailsVO profileVO)  : "+Ex);
	}

	return flag;

}

public int deleteLedgerProfile(int ledgerID,int orgID){
	
	int sucess = 0;
	String strSQL = "";
	try {
			log.debug("Entry : public int deleteLedgerProfile(int ledgerID,int orgID)");
			strSQL = "delete from  account_ledger_profile_details  WHERE ledger_id=:ledgerID and org_id=:orgID ;" ;
			log.debug("query : " + strSQL);
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("ledgerID", ledgerID);
			namedParameters.put("orgID",orgID);
			
			sucess = namedParameterJdbcTemplate.update(strSQL,namedParameters);
			
			log.debug(sucess);
			log.debug("Exit : public int deleteLedgerProfile(int ledgerID,int orgID)");

	}catch(Exception Ex){
		
		Ex.printStackTrace();
		log.error("Exception in public int deleteLedgerProfile(int ledgerID,int orgID) : "+Ex);
		
	}
	return sucess;
}

public int deleteLedgerProfileFromTaxTable(int ledgerID,int orgID){
	
	int sucess = 0;
	String strSQL = "";
	try {
			log.debug("Entry : public int deleteLedgerProfileFromTaxTable(int profileID,int ledgerID)");
			strSQL = "delete from  tax_details  WHERE entity_id=:ledgerID and org_id=:orgID;" ;
			log.debug("query : " + strSQL);
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("orgID", orgID);
			namedParameters.put("ledgerID",ledgerID);
			
			sucess = namedParameterJdbcTemplate.update(strSQL,namedParameters);
			
			log.debug(sucess);
			log.debug("Exit : public int deleteLedgerProfileFromTaxTable(int profileID,int ledgerID)");

	}catch(Exception Ex){
		
		Ex.printStackTrace();
		log.error("Exception in public int deleteLedgerProfileFromTaxTable(int profileID,int ledgerID) : "+Ex);
		
	}
	return sucess;
}

/* Used to get list of ledgers associated with given properties properties*/
public List getLedgersListForImportStament(String properties)  {
	// TODO Auto-generated method stub
	log.debug("Entry : public List getLedgersListForImportStament(String properties) ");
	List ledgerList=null;

	
	
	try{
		  String str = "SELECT * FROM account_ledger_profile_details ap WHERE  ap.ledger_meta  LIKE '%"+properties+"%' ;";
		   log.debug("sql query is "+str); 
		   				
		   
		   Map hMap=new HashMap();
		    hMap.put("properties", properties);
		  	  
		   
		   RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					
					AccountHeadVO acHVO=new AccountHeadVO();
					acHVO.setLedgerID(rs.getInt("ledger_id"));
					acHVO.setOrgID(rs.getInt("org_id"));
					acHVO.setAdditionalProps(rs.getString("ledger_meta"));
					
					return acHVO;
				}

			   };
		   ledgerList=namedParameterJdbcTemplate.query(
					str, hMap, Rmapper);
		   log.debug("Exit :  public List getLedgersListForImportStament(String properties) "+ledgerList.size());
	}catch(EmptyResultDataAccessException Ex){
		log.info("No Data found at public List getLedgersListForImportStament(String properties)  "+Ex);
		
		}catch(Exception se){
			log.error("Exception : public List getLedgersListForImportStament(String properties)  "+se);
		   
		}
	return ledgerList;
}


/* Used to get list of ledgers ledgers who are mapped with virtual account nos*/
public List getVirtualAccLedgersListForImportStament(int orgID)  {
	
	log.debug("Entry : public List getVirtualAccLedgersListForImportStament(int orgID) ");
	List ledgerList=null;

	
	
	try{
		 SocietyVO societyVO=societyService.getSocietyDetails(orgID);
		  String str = "SELECT * FROM account_ledgers_"+societyVO.getDataZoneID()+" a,account_ledger_profile_details ap WHERE a.is_deleted=0 AND a.id=ap.ledger_id AND a.org_id=ap.org_id AND ap.org_id=:orgID AND ap.virtual_acc_no IS NOT NULL;";
		   log.debug("sql query is "+str); 
		   				
		   
		   Map hMap=new HashMap();
		    hMap.put("orgID", orgID);
		  	  
		   
		   RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					
					AccountHeadVO acHVO=new AccountHeadVO();
					acHVO.setLedgerID(rs.getInt("id"));
					acHVO.setStrAccountHeadName(rs.getString("ledger_name"));
					acHVO.setOpeningBalance(rs.getBigDecimal("opening_balance"));
					acHVO.setClosingBalance(rs.getBigDecimal("closing_balance"));
					acHVO.setAdditionalProps(rs.getString("virtual_acc_no"));
					acHVO.setAccountGroupID(rs.getInt("sub_group_id"));
					acHVO.setCreateDate(rs.getString("create_date"));
					acHVO.setUpdateDate(rs.getString("update_date"));
					acHVO.setEffectiveDate(rs.getString("effective_date"));
					acHVO.setIsHidden(rs.getInt("is_hidden"));
					acHVO.setDescription(rs.getString("description"));
					acHVO.setIsPrimary(rs.getInt("is_primary"));
					acHVO.setLockDate(rs.getString("lock_date"));
					acHVO.setAccType(rs.getString("type"));
					return acHVO;
				}

			   };
		   ledgerList=namedParameterJdbcTemplate.query(
					str, hMap, Rmapper);
		   log.debug("Exit :  public List getLedgersListForImportStament(String properties) "+ledgerList.size());
	}catch(EmptyResultDataAccessException Ex){
		log.info("No Data found at public List getLedgersListForImportStament(String properties)  "+Ex);
		
		}catch(Exception se){
			log.error("Exception : public List getLedgersListForImportStament(String properties)  "+se);
		   
		}
	return ledgerList;
}


/* Used to get groups of that org */
	public List getGroupListByCategory(int categoryID,int orgID,int rootID)  {
	// TODO Auto-generated method stub
	log.debug("Entry : public List getGroupListByCategory(int rootID,int orgID)");
	List ledgerList=null;
	String strCondition="";
	if(rootID!=0){
	strCondition="and a.root_id=:rootID "	;
	}
	SocietyVO societyVO=societyService.getSocietyDetails(orgID);
	try{
		  String str = "SELECT ap.short_name,ap.root_group_name,a.* FROM account_groups a,account_root_group ap where a.root_id=ap.root_id "+strCondition+" AND org_category LIKE '%"+societyVO.getOrgType()+"%' and ( a.org_id=0 or a.org_id=:orgID)  and a.category_id=:categoryID order by seq_no, root_id";
		   				 
		   				
		   
		   Map hMap=new HashMap();
		   hMap.put("rootID", rootID);
		   hMap.put("orgID",orgID);
		   hMap.put("categoryID",categoryID);
		  
		  
		   RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					
					AccountHeadVO acHVO=new AccountHeadVO();
					acHVO.setAccountGroupID(rs.getInt("id"));
					acHVO.setStrAccountHeadName(rs.getString("group_name").trim());
					acHVO.setRootID(rs.getInt("root_id"));
					acHVO.setLedgerShortName(rs.getString("short_name"));
					acHVO.setRootGroupName(rs.getString("root_group_name"));
					return acHVO;
				}

			   };
		   ledgerList=namedParameterJdbcTemplate.query(
					str, hMap, Rmapper);
		   log.debug("Exit : public List getGroupListByCategory)"+ledgerList.size());
		}catch(Exception se){
			log.error("Exception : public List getGroupListByCategory(String societyID) "+se);
		   
		}
	return ledgerList;
}

	
	
	public List getCategoryList(int orgID)  {
		
		log.debug("Entry : public List getCategoryList(int orgID)");
		List categoryList=null;
		
		
		
		try{
			  String str = "  SELECT * FROM account_category order by category_id ;";
			   				 
			   				
			   
			   Map hMap=new HashMap();
			   hMap.put("orgID", orgID);
			  
			  
			   RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						
						AccountHeadVO acHVO=new AccountHeadVO();
						acHVO.setCategoryID(rs.getInt("category_id"));
						acHVO.setCategoryName(rs.getString("category_name"));
						acHVO.setRootID(rs.getInt("root_id"));
						acHVO.setDescription(rs.getString("description"));
						return acHVO;
					}

				   };
			   categoryList=namedParameterJdbcTemplate.query(
						str, hMap, Rmapper);
			   log.debug("Exit : public List getSubGroupList(String societyID)"+categoryList.size());
			}catch(Exception se){
				log.error("Exception : public List getSubGroupList(String societyID) "+se);
			   
			}
		return categoryList;
	}
	
	
	/* Used for inserting  groups */
	public int insertGroup(AccountHeadVO acHeadVO,int orgID)  {
		// TODO Auto-generated method stub
		int success=0;
		int generatedId=0;
		
		log.debug("Entry : public int insertGroup(AccountHeadVO acHeadVO,int orgID)");
		try{
			  
			   
			 String orgType="";
			 String argument="";
			   
			 SocietyVO societyVO=societyService.getSocietyDetails(orgID);
			 if(societyVO.getOrgType().equalsIgnoreCase("C")){
				 acHeadVO.setAccType("C");
			 }else if(societyVO.getOrgType().equalsIgnoreCase("S")){
				 acHeadVO.setAccType("S");
			 }
			 
			   String sql = "INSERT INTO account_groups "+					
				   "(org_id,group_name,category_id ,root_id,org_category)  " +
	                "VALUES (:orgID,:strAccountPrimaryHead,:categoryID,:rootID,:accType)";
			   log.debug("Sql: "+sql);
			   
			   SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(acHeadVO);
			   KeyHolder keyHolder = new GeneratedKeyHolder();
			   getNamedParameterJdbcTemplate().update(sql, fileParameters, keyHolder);
			   generatedId=keyHolder.getKey().intValue();
			 
			   log.debug("Exit :public int inserttLedger(AccountHeadVO acHeadVO,String societyID)"+generatedId);
		}catch (DuplicateKeyException e) {
			generatedId=-1;
			log.info("Duplicate Group Name can not be added");
			
		}catch(Exception se){
				log.error("Exception :public int inserttLedger(AccountHeadVO acHeadVO,String societyID) "+se);
			   generatedId=0;
			}
		return generatedId;
	}
	
	
	
	
	
	
	/* Used for update  Group */
	public int updateGroup(AccountHeadVO acHeadVO,int groupID)  {
		// TODO Auto-generated method stub
		int success=0;
		
		log.debug("Entry : public int updateGroup(AccountHeadVO acHeadVO,int groupID)");
		try{
			  
			   java.util.Date today = new java.util.Date();
			   java.sql.Date currentDate=new java.sql.Date(today.getTime());
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			   String strCondtion="";
			  			  
			   String str = "Update account_groups set group_name=:groupName,category_id=:categoryID where id=:groupID and org_id=:orgID ";
			   log.debug("Query: "+str);
			   
			   Map hMap=new HashMap();
			   hMap.put("groupName", acHeadVO.getStrAccountPrimaryHead());
			   hMap.put("categoryID", acHeadVO.getCategoryID());
			   hMap.put("groupID", groupID);
			   hMap.put("orgID", acHeadVO.getOrgID());
			   
			  			   
			   success=namedParameterJdbcTemplate.update(str, hMap);
			   log.debug("Exit : public int updateGroup(AccountHeadVO acHeadVO,int groupID)"+success);
			}catch(Exception se){
				log.debug("Exception : public int updateGroup(AccountHeadVO acHeadVO,int groupID) "+se);
			   success=0;
			}
		return success;
	}
	
	/* Used for delete group */
	public int deleteGroup(int orgID,int groupID)  {
		// TODO Auto-generated method stub
		int success=0;
		
		log.debug("Entry : public int deleteGroup(int orgID,int groupID)");
		try{
			   String str = "Update account_groups  set is_deleted=1 where id=:groupID and org_id=:orgID ";
			   
			   Map hMap=new HashMap();
			
			   hMap.put("groupID", groupID);
			   hMap.put("orgID", orgID);
			   
			   success=namedParameterJdbcTemplate.update(str, hMap);
			   log.debug("Exit : public int deleteGroup(int orgID,int groupID)"+success);
			}catch(Exception se){
				log.debug("Exception : public int deleteGroup(int orgID,int groupID) "+se);
			   success=0;
			}
		return success;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}


	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}


	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}


	/**
	 * @param societyService the societyService to set
	 */
	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}
	
}
