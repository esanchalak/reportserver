package com.emanager.server.accounts.DataAccessObjects;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class LedgerEntries {
	
	
	private int itemID;
	private int txID;
	private String creditDebitFlag;
	private String ledgerName;
	private BigDecimal amount;
	private BigDecimal balance;
	private BigDecimal intAmount;
	private String strComments;
	private int ledgerID;
	private int creditAction;
	private int debitAction;
	private String updatedBalance;
	private Timestamp insertDate;
	private BigDecimal credit;
	private BigDecimal debit;
	private String isDeleted;
	private int chargeID;
	private String chargeFrequency;
	private int orgID;
	private String ledgerTags;
	private String projectTags;
	private int invLineItemID;
	private int groupID;

	/**
	 * @return the itemID
	 */
	public int getItemID() {
		return itemID;
	}
	/**
	 * @param itemID the itemID to set
	 */
	public void setItemID(int itemID) {
		this.itemID = itemID;
	}
	/**
	 * @return the strAccountsHead
	 */
	public String getLedgerName() {
		return ledgerName;
	}
	/**
	 * @param strAccountsHead the strAccountsHead to set
	 */
	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}
	/**
	 * @return the bdItemAmount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * @param bdItemAmount the bdItemAmount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * @return the strComments
	 */
	public String getStrComments() {
		return strComments;
	}
	/**
	 * @param strComments the strComments to set
	 */
	public void setStrComments(String strComments) {
		this.strComments = strComments;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public String getCreditDebitFlag() {
		return creditDebitFlag;
	}
	public void setCreditDebitFlag(String creditDebitFlag) {
		this.creditDebitFlag = creditDebitFlag;
	}
	public int getLedgerID() {
		return ledgerID;
	}
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	public int getCreditAction() {
		return creditAction;
	}
	public void setCreditAction(int creditAction) {
		this.creditAction = creditAction;
	}
	public int getDebitAction() {
		return debitAction;
	}
	public void setDebitAction(int debitAction) {
		this.debitAction = debitAction;
	}
	public String getUpdatedBalance() {
		return updatedBalance;
	}
	public void setUpdatedBalance(String updatedBalance) {
		this.updatedBalance = updatedBalance;
	}
	public int getTxID() {
		return txID;
	}
	public void setTxID(int txID) {
		this.txID = txID;
	}
	public Timestamp getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(Timestamp insertDate) {
		this.insertDate = insertDate;
	}
	public BigDecimal getCredit() {
		return credit;
	}
	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}
	public BigDecimal getDebit() {
		return debit;
	}
	public void setDebit(BigDecimal debit) {
		this.debit = debit;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	/**
	 * @return the chargeID
	 */
	public int getChargeID() {
		return chargeID;
	}
	/**
	 * @return the chargeFrequency
	 */
	public String getChargeFrequency() {
		return chargeFrequency;
	}
	/**
	 * @param chargeID the chargeID to set
	 */
	public void setChargeID(int chargeID) {
		this.chargeID = chargeID;
	}
	/**
	 * @param chargeFrequency the chargeFrequency to set
	 */
	public void setChargeFrequency(String chargeFrequency) {
		this.chargeFrequency = chargeFrequency;
	}
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	/**
	 * @return the intAmount
	 */
	public BigDecimal getIntAmount() {
		return intAmount;
	}
	/**
	 * @param intAmount the intAmount to set
	 */
	public void setIntAmount(BigDecimal intAmount) {
		this.intAmount = intAmount;
	}
	public String getLedgerTags() {
		return ledgerTags;
	}
	public void setLedgerTags(String ledgerTags) {
		this.ledgerTags = ledgerTags;
	}
	
	public String getProjectTags() {
		return projectTags;
	}
	public void setProjectTags(String projectTags) {
		this.projectTags = projectTags;
	}
	public int getInvLineItemID() {
		return invLineItemID;
	}
	public void setInvLineItemID(int invLineItemID) {
		this.invLineItemID = invLineItemID;
	}
	public int getGroupID() {
		return groupID;
	}
	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}
	
}
