package com.emanager.server.accounts.DataAccessObjects;

import java.math.BigDecimal;
import java.util.List;

public class ChargesVO {
	
	private int orgID;
	private int ledgerID;
	private int groupID;
	private int rootID;
	private int chargeID;
	private int debitLedgerID;
	private int creditLedgerID;
	private String groupName;
	private String groupTypeID;
	private BigDecimal amount;
	private String transactionType;
	private String calMethod;
	private int chargeSequence;
	private String startDate;
	private String endDate;
	private String createDate;
	private String updateDate;
	private String status;
	private String description;
	private int chargeTypeID;
	private String chargeType;
	private String chargeFrequency;
	private String nextChargeDate;
	private int createInvoices;
	private Boolean isApplicable;
	private int precison;
	private String txDate;
	private int itemID;
	private String itemName;
	private BigDecimal iGstRate=BigDecimal.ZERO;
	private BigDecimal iGstAmount=BigDecimal.ZERO;
	private BigDecimal sGstRate=BigDecimal.ZERO;
	private BigDecimal sGstAmount=BigDecimal.ZERO;
	private BigDecimal cGstRate=BigDecimal.ZERO;
	private BigDecimal cGstAmount=BigDecimal.ZERO;
	private BigDecimal uGstRate=BigDecimal.ZERO;
	private BigDecimal uGstAmount=BigDecimal.ZERO;
	private BigDecimal gstRate=BigDecimal.ZERO;
	private BigDecimal gstAmount=BigDecimal.ZERO;
	private String socGstIN;
	private String hsnsacCode;
	private BigDecimal purchasePrice=BigDecimal.ZERO;
	private BigDecimal sellingPrice=BigDecimal.ZERO;
	private int gstCategoryID;
	
	
	
		
	
	public int getCreateInvoices() {
		return createInvoices;
	}
	public void setCreateInvoices(int createInvoices) {
		this.createInvoices = createInvoices;
	}
	public int getOrgID() {
		return orgID;
	}
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	public int getLedgerID() {
		return ledgerID;
	}
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	public int getGroupID() {
		return groupID;
	}
	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}
	public int getRootID() {
		return rootID;
	}
	public void setRootID(int rootID) {
		this.rootID = rootID;
	}
	public int getChargeID() {
		return chargeID;
	}
	public void setChargeID(int chargeID) {
		this.chargeID = chargeID;
	}
	public int getDebitLedgerID() {
		return debitLedgerID;
	}
	public void setDebitLedgerID(int debitLedgerID) {
		this.debitLedgerID = debitLedgerID;
	}
	public int getCreditLedgerID() {
		return creditLedgerID;
	}
	public void setCreditLedgerID(int creditLedgerID) {
		this.creditLedgerID = creditLedgerID;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupTypeID() {
		return groupTypeID;
	}
	public void setGroupTypeID(String groupTypeID) {
		this.groupTypeID = groupTypeID;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getCalMethod() {
		return calMethod;
	}
	public void setCalMethod(String calMethod) {
		this.calMethod = calMethod;
	}
	public int getChargeSequence() {
		return chargeSequence;
	}
	public void setChargeSequence(int chargeSequence) {
		this.chargeSequence = chargeSequence;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getChargeTypeID() {
		return chargeTypeID;
	}
	public void setChargeTypeID(int chargeTypeID) {
		this.chargeTypeID = chargeTypeID;
	}
	public String getChargeFrequency() {
		return chargeFrequency;
	}
	public void setChargeFrequency(String chargeFrequency) {
		this.chargeFrequency = chargeFrequency;
	}
	public String getNextChargeDate() {
		return nextChargeDate;
	}
	public void setNextChargeDate(String nextChargeDate) {
		this.nextChargeDate = nextChargeDate;
	}
	
	/**
	 * @return the chargeType
	 */
	public String getChargeType() {
		return chargeType;
	}
	/**
	 * @param chargeType the chargeType to set
	 */
	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}
	/**
	 * @return the isApplicable
	 */
	public Boolean getIsApplicable() {
		return isApplicable;
	}
	/**
	 * @param isApplicable the isApplicable to set
	 */
	public void setIsApplicable(Boolean isApplicable) {
		this.isApplicable = isApplicable;
	}
	/**
	 * @return the precison
	 */
	public int getPrecison() {
		return precison;
	}
	/**
	 * @param precison the precison to set
	 */
	public void setPrecison(int precison) {
		this.precison = precison;
	}
	/**
	 * @return the txDate
	 */
	public String getTxDate() {
		return txDate;
	}
	/**
	 * @param txDate the txDate to set
	 */
	public void setTxDate(String txDate) {
		this.txDate = txDate;
	}
	/**
	 * @return the itemID
	 */
	public int getItemID() {
		return itemID;
	}
	/**
	 * @return the iGstRate
	 */
	public BigDecimal getiGstRate() {
		return iGstRate;
	}
	/**
	 * @return the iGstAmount
	 */
	public BigDecimal getiGstAmount() {
		return iGstAmount;
	}
	/**
	 * @return the sGstRate
	 */
	public BigDecimal getsGstRate() {
		return sGstRate;
	}
	/**
	 * @return the sGstAmount
	 */
	public BigDecimal getsGstAmount() {
		return sGstAmount;
	}
	/**
	 * @return the cGstRate
	 */
	public BigDecimal getcGstRate() {
		return cGstRate;
	}
	/**
	 * @return the cGstAmount
	 */
	public BigDecimal getcGstAmount() {
		return cGstAmount;
	}
	/**
	 * @return the uGstRate
	 */
	public BigDecimal getuGstRate() {
		return uGstRate;
	}
	/**
	 * @return the uGstAmount
	 */
	public BigDecimal getuGstAmount() {
		return uGstAmount;
	}
	/**
	 * @return the gstRate
	 */
	public BigDecimal getGstRate() {
		return gstRate;
	}
	/**
	 * @return the gstAmount
	 */
	public BigDecimal getGstAmount() {
		return gstAmount;
	}
	/**
	 * @param itemID the itemID to set
	 */
	public void setItemID(int itemID) {
		this.itemID = itemID;
	}
	/**
	 * @param iGstRate the iGstRate to set
	 */
	public void setiGstRate(BigDecimal iGstRate) {
		this.iGstRate = iGstRate;
	}
	/**
	 * @param iGstAmount the iGstAmount to set
	 */
	public void setiGstAmount(BigDecimal iGstAmount) {
		this.iGstAmount = iGstAmount;
	}
	/**
	 * @param sGstRate the sGstRate to set
	 */
	public void setsGstRate(BigDecimal sGstRate) {
		this.sGstRate = sGstRate;
	}
	/**
	 * @param sGstAmount the sGstAmount to set
	 */
	public void setsGstAmount(BigDecimal sGstAmount) {
		this.sGstAmount = sGstAmount;
	}
	/**
	 * @param cGstRate the cGstRate to set
	 */
	public void setcGstRate(BigDecimal cGstRate) {
		this.cGstRate = cGstRate;
	}
	/**
	 * @param cGstAmount the cGstAmount to set
	 */
	public void setcGstAmount(BigDecimal cGstAmount) {
		this.cGstAmount = cGstAmount;
	}
	/**
	 * @param uGstRate the uGstRate to set
	 */
	public void setuGstRate(BigDecimal uGstRate) {
		this.uGstRate = uGstRate;
	}
	/**
	 * @param uGstAmount the uGstAmount to set
	 */
	public void setuGstAmount(BigDecimal uGstAmount) {
		this.uGstAmount = uGstAmount;
	}
	/**
	 * @param gstRate the gstRate to set
	 */
	public void setGstRate(BigDecimal gstRate) {
		this.gstRate = gstRate;
	}
	/**
	 * @param gstAmount the gstAmount to set
	 */
	public void setGstAmount(BigDecimal gstAmount) {
		this.gstAmount = gstAmount;
	}
	/**
	 * @return the socGstIN
	 */
	public String getSocGstIN() {
		return socGstIN;
	}
	/**
	 * @param socGstIN the socGstIN to set
	 */
	public void setSocGstIN(String socGstIN) {
		this.socGstIN = socGstIN;
	}
	/**
	 * @return the hsnsacCode
	 */
	public String getHsnsacCode() {
		return hsnsacCode;
	}
	/**
	 * @return the purchasePrice
	 */
	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}
	/**
	 * @return the sellingPrice
	 */
	public BigDecimal getSellingPrice() {
		return sellingPrice;
	}
	/**
	 * @param hsnsacCode the hsnsacCode to set
	 */
	public void setHsnsacCode(String hsnsacCode) {
		this.hsnsacCode = hsnsacCode;
	}
	/**
	 * @param purchasePrice the purchasePrice to set
	 */
	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	/**
	 * @param sellingPrice the sellingPrice to set
	 */
	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}
	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}
	/**
	 * @param itemName the itemName to set
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	/**
	 * @return the gstCategoryID
	 */
	public int getGstCategoryID() {
		return gstCategoryID;
	}
	/**
	 * @param gstCategoryID the gstCategoryID to set
	 */
	public void setGstCategoryID(int gstCategoryID) {
		this.gstCategoryID = gstCategoryID;
	}

}
