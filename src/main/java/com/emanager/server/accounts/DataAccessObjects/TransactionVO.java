package com.emanager.server.accounts.DataAccessObjects;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import com.emanager.server.adminReports.valueObject.PrintReportVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceDetailsVO;
import com.emanager.server.society.valueObject.BankInfoVO;


public class TransactionVO extends PrintReportVO {
	
	private int iTransactionID = 1;
	private String masterID;
	private int transactionID;
	private String transactionNumber;
	private String transactionType; // Payment, Reciept, Sales, Purchase, Asset, Journal, Contra, etc.
	private String strAction;// Create, Update, Delete will be ignored for now.
	private Date dtTransactionDate = new Date(0);
	private BigDecimal amount ;
	private String description;
	private String displayNote;
	private Timestamp createDate;
	private Timestamp updatedDate;
	private List<LedgerEntries> ledgerEntries;
	private String ledgerName;
	private String transactionMode;
	private String docID;
	private String refNumber;
	private int bankID;
	private String bankDate;
	private int isDeleted;
	private int ledgerID;
	private int invoiceID;
	private int groupID;
	private String creditDebitFlag;
	private BigDecimal creditAmt;
	private BigDecimal debitAmt;
	private String transactionDate;
	private String isUpdated;
	private BigDecimal sumAmt;
	private Boolean autoInc=false;
	private String creatorName;
	private String updaterName;
	private int createdBy;
	private int updatedBy;
	private String amtInWord;
	private AccountHeadVO accountHeadVO;
	private String fromDate;
	private String toDate;
	private String type;	
	private String unit;
	private BigDecimal balance;
	private BigDecimal discountAmt;
	private int societyID;
	private String hashComments;
	private String accComments;
	private String auditorComments;
	private BankInfoVO bankInfoVO;
	private String dueDate;
	private String aptId;
	private TransactionMetaVO additionalProps;
	private List<InvoiceDetailsVO> invoiceDetailsVO;
	private String billID;
	private int subGroupID;
	private int statusCode;
	private String errorMessage;
	private int chargeTypeID;
	private int billingCycleID;
	private Boolean generateInvoice=false;
	private String counterType;
	private Boolean validation=true;
	private int receiptID;
	private int receiptLinkID;
	private String virtualAccNo;
	private BigDecimal intAmount; //interest amount
	private BigDecimal principleAmount; // Principal amount , principal=closing bal- interst amount
	private String isCalculationInclusive; //for gateway 0:exclusive,1:inclusive
	private String txTags;
	private List<TransactionVO> transactionsList;
	private int isAudited;
	private int invLineItemID;
	private int txLineItemID;
	private BigDecimal intBalance;//Interest balance
	
	public Boolean getValidation() {
		return validation;
	}
	public void setValidation(Boolean validation) {
		this.validation = validation;
	}
	/**
	 * @return the statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}
	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getBankDate() {
		return bankDate;
	}
	public void setBankDate(String bankDate) {
		this.bankDate = bankDate;
	}
	public int getBankID() {
		return bankID;
	}
	public void setBankID(int bankID) {
		this.bankID = bankID;
	}
	
	public Timestamp getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDocID() {
		return docID;
	}
	public void setDocID(String docID) {
		this.docID = docID;
	}
	public Date getDtTransactionDate() {
		return dtTransactionDate;
	}
	public void setDtTransactionDate(Date dtTransactionDate) {
		this.dtTransactionDate = dtTransactionDate;
	}
	public int getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	public int getITransactionID() {
		return iTransactionID;
	}
	public void setITransactionID(int transactionID) {
		iTransactionID = transactionID;
	}
	public List<LedgerEntries> getLedgerEntries() {
		return ledgerEntries;
	}
	public void setLedgerEntries(List<LedgerEntries> ledgerEntries) {
		this.ledgerEntries = ledgerEntries;
	}
	public String getMasterID() {
		return masterID;
	}
	public void setMasterID(String masterID) {
		this.masterID = masterID;
	}
	public String getRefNumber() {
		return refNumber;
	}
	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}
	
	public String getLedgerName() {
		return ledgerName;
	}
	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}
	public String getStrAction() {
		return strAction;
	}
	public void setStrAction(String strAction) {
		this.strAction = strAction;
	}
	
	public String getTransactionMode() {
		return transactionMode;
	}
	public void setTransactionMode(String transactionMode) {
		this.transactionMode = transactionMode;
	}
	public String getTransactionNumber() {
		return transactionNumber;
	}
	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public int getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(int transactionID) {
		this.transactionID = transactionID;
	}
	public int getLedgerID() {
		return ledgerID;
	}
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	public String getCreditDebitFlag() {
		return creditDebitFlag;
	}
	public void setCreditDebitFlag(String creditDebitFlag) {
		this.creditDebitFlag = creditDebitFlag;
	}
	public BigDecimal getCreditAmt() {
		return creditAmt;
	}
	public void setCreditAmt(BigDecimal creditAmt) {
		this.creditAmt = creditAmt;
	}
	public BigDecimal getDebitAmt() {
		return debitAmt;
	}
	public void setDebitAmt(BigDecimal debitAmt) {
		this.debitAmt = debitAmt;
	}
	
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getIsUpdated() {
		return isUpdated;
	}
	public void setIsUpdated(String isUpdated) {
		this.isUpdated = isUpdated;
	}
	public BigDecimal getSumAmt() {
		return sumAmt;
	}
	public void setSumAmt(BigDecimal sumAmt) {
		this.sumAmt = sumAmt;
	}
	public String getDisplayNote() {
		return displayNote;
	}
	public void setDisplayNote(String displayNote) {
		this.displayNote = displayNote;
	}
	
	public int getGroupID() {
		return groupID;
	}
	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}
	public int getInvoiceID() {
		return invoiceID;
	}
	public void setInvoiceID(int invoiceID) {
		this.invoiceID = invoiceID;
	}
	public Boolean getAutoInc() {
		return autoInc;
	}
	public void setAutoInc(Boolean autoInc) {
		this.autoInc = autoInc;
	}
	public String getCreatorName() {
		return creatorName;
	}
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	public String getUpdaterName() {
		return updaterName;
	}
	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName;
	}
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	public int getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getAmtInWord() {
		return amtInWord;
	}
	public void setAmtInWord(String amtInWord) {
		this.amtInWord = amtInWord;
	}
	public AccountHeadVO getAccountHeadVO() {
		return accountHeadVO;
	}
	public void setAccountHeadVO(AccountHeadVO accountHeadVO) {
		this.accountHeadVO = accountHeadVO;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public BigDecimal getDiscountAmt() {
		return discountAmt;
	}
	public void setDiscountAmt(BigDecimal discountAmt) {
		this.discountAmt = discountAmt;
	}
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	
	public String getHashComments() {
		return hashComments;
	}
	public void setHashComments(String hashComments) {
		this.hashComments = hashComments;
	}
	public String getAccComments() {
		return accComments;
	}
	public void setAccComments(String accComments) {
		this.accComments = accComments;
	}
	public String getAuditorComments() {
		return auditorComments;
	}
	public void setAuditorComments(String auditorComments) {
		this.auditorComments = auditorComments;
	}
	public BankInfoVO getBankInfoVO() {
		return bankInfoVO;
	}
	public void setBankInfoVO(BankInfoVO bankInfoVO) {
		this.bankInfoVO = bankInfoVO;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getAptId() {
		return aptId;
	}
	public void setAptId(String aptId) {
		this.aptId = aptId;
	}
	public TransactionMetaVO getAdditionalProps() {
		return additionalProps;
	}
	public void setAdditionalProps(TransactionMetaVO additionalProps) {
		this.additionalProps = additionalProps;
	}
	public List<InvoiceDetailsVO> getInvoiceDetailsVO() {
		return invoiceDetailsVO;
	}
	public void setInvoiceDetailsVO(List<InvoiceDetailsVO> invoiceDetailsVO) {
		this.invoiceDetailsVO = invoiceDetailsVO;
	}
	public String getBillID() {
		return billID;
	}
	public void setBillID(String billID) {
		this.billID = billID;
	}
	public int getSubGroupID() {
		return subGroupID;
	}
	public void setSubGroupID(int subGroupID) {
		this.subGroupID = subGroupID;
	}
	/**
	 * @return the chargeTypeID
	 */
	public int getChargeTypeID() {
		return chargeTypeID;
	}
	/**
	 * @param chargeTypeID the chargeTypeID to set
	 */
	public void setChargeTypeID(int chargeTypeID) {
		this.chargeTypeID = chargeTypeID;
	}
	/**
	 * @return the billingCycleID
	 */
	public int getBillingCycleID() {
		return billingCycleID;
	}
	/**
	 * @param billingCycleID the billingCycleID to set
	 */
	public void setBillingCycleID(int billingCycleID) {
		this.billingCycleID = billingCycleID;
	}
	/**
	 * @return the generateInvoice
	 */
	public Boolean getGenerateInvoice() {
		return generateInvoice;
	}
	/**
	 * @param generateInvoice the generateInvoice to set
	 */
	public void setGenerateInvoice(Boolean generateInvoice) {
		this.generateInvoice = generateInvoice;
	}
	/**
	 * @return the counterType
	 */
	public String getCounterType() {
		return counterType;
	}
	/**
	 * @param counterType the counterType to set
	 */
	public void setCounterType(String counterType) {
		this.counterType = counterType;
	}
	/**
	 * @return the receiptID
	 */
	public int getReceiptID() {
		return receiptID;
	}
	/**
	 * @param receiptID the receiptID to set
	 */
	public void setReceiptID(int receiptID) {
		this.receiptID = receiptID;
	}
	/**
	 * @return the receiptLinkID
	 */
	public int getReceiptLinkID() {
		return receiptLinkID;
	}
	/**
	 * @param receiptLinkID the receiptLinkID to set
	 */
	public void setReceiptLinkID(int receiptLinkID) {
		this.receiptLinkID = receiptLinkID;
	}
	/**
	 * @return the virtualAccNo
	 */
	public String getVirtualAccNo() {
		return virtualAccNo;
	}
	/**
	 * @param virtualAccNo the virtualAccNo to set
	 */
	public void setVirtualAccNo(String virtualAccNo) {
		this.virtualAccNo = virtualAccNo;
	}
	/**
	 * @return the intAmount
	 */
	public BigDecimal getIntAmount() {
		return intAmount;
	}
	/**
	 * @return the principleAmount
	 */
	public BigDecimal getPrincipleAmount() {
		return principleAmount;
	}
	/**
	 * @param intAmount the intAmount to set
	 */
	public void setIntAmount(BigDecimal intAmount) {
		this.intAmount = intAmount;
	}
	/**
	 * @param principleAmount the principleAmount to set
	 */
	public void setPrincipleAmount(BigDecimal principleAmount) {
		this.principleAmount = principleAmount;
	}
	/**
	 * @return the isCalculationInclusive
	 */
	public String getIsCalculationInclusive() {
		return isCalculationInclusive;
	}
	/**
	 * @param isCalculationInclusive the isCalculationInclusive to set
	 */
	public void setIsCalculationInclusive(String isCalculationInclusive) {
		this.isCalculationInclusive = isCalculationInclusive;
	}
	public String getTxTags() {
		return txTags;
	}
	public void setTxTags(String txTags) {
		this.txTags = txTags;
	}
	public List<TransactionVO> getTransactionsList() {
		return transactionsList;
	}
	public void setTransactionsList(List<TransactionVO> transactionsList) {
		this.transactionsList = transactionsList;
	}
	public int getIsAudited() {
		return isAudited;
	}
	public void setIsAudited(int isAudited) {
		this.isAudited = isAudited;
	}
	public int getInvLineItemID() {
		return invLineItemID;
	}
	public void setInvLineItemID(int invLineItemID) {
		this.invLineItemID = invLineItemID;
	}
	public int getTxLineItemID() {
		return txLineItemID;
	}
	public void setTxLineItemID(int txLineItemID) {
		this.txLineItemID = txLineItemID;
	}
	/**
	 * @return the intBalance
	 */
	public BigDecimal getIntBalance() {
		return intBalance;
	}
	/**
	 * @param intBalance the intBalance to set
	 */
	public void setIntBalance(BigDecimal intBalance) {
		this.intBalance = intBalance;
	}

	
	
	
	

	
	
}
