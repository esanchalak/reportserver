package com.emanager.server.accounts.DataAccessObjects;

import java.math.BigDecimal;
import java.util.List;

public class LedgerTrnsVO {
	
	private int orgID;
	private int ledgerID;
	private int groupID;
	private int rootID;
	private BigDecimal openingBalance;
	private BigDecimal drOpnBalance;
	private BigDecimal crOpnBalance;
	private BigDecimal closingBalance;
	private BigDecimal drClsBalance;
	private BigDecimal crClsBalance;
	private String ledgerName;
	private String groupName;
	private String fromDate;
	private String toDate;
	private List<TransactionVO> transactionList;
	private BigDecimal recBalance;
	private BigDecimal totalAmount;
	private BigDecimal principleBalance;
	private BigDecimal interestBalance;
	private String description; //Ledger description
	private BigDecimal intOpeningBalance;
	private BigDecimal intClosingBalance;
	
	public BigDecimal getClosingBalance() {
		return closingBalance;
	}
	public void setClosingBalance(BigDecimal closingBalance) {
		this.closingBalance = closingBalance;
	}
	public BigDecimal getCrClsBalance() {
		return crClsBalance;
	}
	public void setCrClsBalance(BigDecimal crClsBalance) {
		this.crClsBalance = crClsBalance;
	}
	public BigDecimal getCrOpnBalance() {
		return crOpnBalance;
	}
	public void setCrOpnBalance(BigDecimal crOpnBalance) {
		this.crOpnBalance = crOpnBalance;
	}
	public BigDecimal getDrClsBalance() {
		return drClsBalance;
	}
	public void setDrClsBalance(BigDecimal drClsBalance) {
		this.drClsBalance = drClsBalance;
	}
	public BigDecimal getDrOpnBalance() {
		return drOpnBalance;
	}
	public void setDrOpnBalance(BigDecimal drOpnBalance) {
		this.drOpnBalance = drOpnBalance;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public int getGroupID() {
		return groupID;
	}
	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public int getLedgerID() {
		return ledgerID;
	}
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	public String getLedgerName() {
		return ledgerName;
	}
	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}
	public BigDecimal getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(BigDecimal openingBalance) {
		this.openingBalance = openingBalance;
	}
	public int getOrgID() {
		return orgID;
	}
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	public int getRootID() {
		return rootID;
	}
	public void setRootID(int rootID) {
		this.rootID = rootID;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public List<TransactionVO> getTransactionList() {
		return transactionList;
	}
	public void setTransactionList(List<TransactionVO> transactionList) {
		this.transactionList = transactionList;
	}
	public BigDecimal getRecBalance() {
		return recBalance;
	}
	public void setRecBalance(BigDecimal recBalance) {
		this.recBalance = recBalance;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	/**
	 * @return the principleBalance
	 */
	public BigDecimal getPrincipleBalance() {
		return principleBalance;
	}
	/**
	 * @return the interestBalance
	 */
	public BigDecimal getInterestBalance() {
		return interestBalance;
	}
	/**
	 * @param principleBalance the principleBalance to set
	 */
	public void setPrincipleBalance(BigDecimal principleBalance) {
		this.principleBalance = principleBalance;
	}
	/**
	 * @param interestBalance the interestBalance to set
	 */
	public void setInterestBalance(BigDecimal interestBalance) {
		this.interestBalance = interestBalance;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getIntOpeningBalance() {
		return intOpeningBalance;
	}
	public void setIntOpeningBalance(BigDecimal intOpeningBalance) {
		this.intOpeningBalance = intOpeningBalance;
	}
	public BigDecimal getIntClosingBalance() {
		return intClosingBalance;
	}
	public void setIntClosingBalance(BigDecimal intClosingBalance) {
		this.intClosingBalance = intClosingBalance;
	}

}
