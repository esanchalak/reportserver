package com.emanager.server.accounts.DataAccessObjects;

import java.math.BigDecimal;
import java.util.List;

import com.emanager.server.society.valueObject.BankInfoVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;

public class TransactionMetaVO {
	
	private int societyID;
	private int txID;
	private int vendorID;
	private String invoiceNO;
	private String invoiceDate;
	private String tdsSectionNo;
	private BigDecimal tdsRate;
	private BigDecimal taxAmount;
	private BigDecimal tdsAmount;
	private String challanDate;
	private String challanNo;
	private String bsrCode;
	private String tagValues;
	private String accountantComments;
	private String auditorsComments;
	private int billingCycleID;
	private int invoiceTypeID;
	private int statusID;
	private String invoiceMode;
	private int ledgerID;
	private int billID;
	private String chequeName;
	private String extTxID;
	private String extAppName;
	
	
	public int getBillingCycleID() {
		return billingCycleID;
	}
	public void setBillingCycleID(int billingCycleID) {
		this.billingCycleID = billingCycleID;
	}
	public int getInvoiceTypeID() {
		return invoiceTypeID;
	}
	public void setInvoiceTypeID(int invoiceTypeID) {
		this.invoiceTypeID = invoiceTypeID;
	}
	public int getStatusID() {
		return statusID;
	}
	public void setStatusID(int statusID) {
		this.statusID = statusID;
	}
	public String getInvoiceMode() {
		return invoiceMode;
	}
	public void setInvoiceMode(String invoiceMode) {
		this.invoiceMode = invoiceMode;
	}
	public int getLedgerID() {
		return ledgerID;
	}
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	public int getBillID() {
		return billID;
	}
	public void setBillID(int billID) {
		this.billID = billID;
	}
	public String getAccountantComments() {
		return accountantComments;
	}
	public void setAccountantComments(String accountantComments) {
		this.accountantComments = accountantComments;
	}
	public String getAuditorsComments() {
		return auditorsComments;
	}
	public void setAuditorsComments(String auditorsComments) {
		this.auditorsComments = auditorsComments;
	}
	public String getBsrCode() {
		return bsrCode;
	}
	public void setBsrCode(String bsrCode) {
		this.bsrCode = bsrCode;
	}
	public String getChallanDate() {
		return challanDate;
	}
	public void setChallanDate(String challanDate) {
		this.challanDate = challanDate;
	}
	public String getChallanNo() {
		return challanNo;
	}
	public void setChallanNo(String challanNo) {
		this.challanNo = challanNo;
	}
	public String getInvoiceNO() {
		return invoiceNO;
	}
	public void setInvoiceNO(String invoiceNO) {
		this.invoiceNO = invoiceNO;
	}
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	public String getTagValues() {
		return tagValues;
	}
	public void setTagValues(String tagValues) {
		this.tagValues = tagValues;
	}
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}
	public BigDecimal getTdsAmount() {
		return tdsAmount;
	}
	public void setTdsAmount(BigDecimal tdsAmount) {
		this.tdsAmount = tdsAmount;
	}
	public BigDecimal getTdsRate() {
		return tdsRate;
	}
	public void setTdsRate(BigDecimal tdsRate) {
		this.tdsRate = tdsRate;
	}
	public String getTdsSectionNo() {
		return tdsSectionNo;
	}
	public void setTdsSectionNo(String tdsSectionNo) {
		this.tdsSectionNo = tdsSectionNo;
	}
	public int getTxID() {
		return txID;
	}
	public void setTxID(int txID) {
		this.txID = txID;
	}
	public int getVendorID() {
		return vendorID;
	}
	public void setVendorID(int vendorID) {
		this.vendorID = vendorID;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	/**
	 * @return the chequeName
	 */
	public String getChequeName() {
		return chequeName;
	}
	/**
	 * @param chequeName the chequeName to set
	 */
	public void setChequeName(String chequeName) {
		this.chequeName = chequeName;
	}
	/**
	 * @return the extTxID
	 */
	public String getExtTxID() {
		return extTxID;
	}
	/**
	 * @param extTxID the extTxID to set
	 */
	public void setExtTxID(String extTxID) {
		this.extTxID = extTxID;
	}
	/**
	 * @return the extAppName
	 */
	public String getExtAppName() {
		return extAppName;
	}
	/**
	 * @param extAppName the extAppName to set
	 */
	public void setExtAppName(String extAppName) {
		this.extAppName = extAppName;
	}
	

	
	
	
	
	

}
