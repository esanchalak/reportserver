package com.emanager.server.accounts.DataAccessObjects;

import java.math.BigDecimal;
import java.util.List;
//
public class PenaltyChargesVO {
	
	private int orgID;
	private int ledgerID;
	private int groupID;
	private int rootID;
	private int penaltyChargeID;
	private String penaltyMode;
	private BigDecimal amount;
	private String calMode;
	private String calMethod;
	private String startDate;
	private String endDate;
	private String createDate;
	private String updateDate;
	private String status;
	private String description;
	private int chargeTypeID;
	private String chargeType;
	private String chargeFrequency;
	private String prevChargeDate;
	private String nextChargeDate;
	private String penaltyChargeDate;
	private String txDate;
	private int createInvoices;
	private Boolean isApplicable;
	private int precison;
	private int itemID;
	private String itemName;
	private String socGstIn;
	private BigDecimal iGstRate=BigDecimal.ZERO;
	private BigDecimal iGstAmount=BigDecimal.ZERO;
	private BigDecimal sGstRate=BigDecimal.ZERO;
	private BigDecimal sGstAmount=BigDecimal.ZERO;
	private BigDecimal cGstRate=BigDecimal.ZERO;
	private BigDecimal cGstAmount=BigDecimal.ZERO;
	private BigDecimal uGstRate=BigDecimal.ZERO;
	private BigDecimal uGstAmount=BigDecimal.ZERO;
	private BigDecimal gstRate=BigDecimal.ZERO;
	private BigDecimal gstAmount=BigDecimal.ZERO;
	private String hsnsacCode;
	private BigDecimal purchasePrice=BigDecimal.ZERO;
	private BigDecimal sellingPrice=BigDecimal.ZERO;
	private int gstCategoryID;
	
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	/**
	 * @return the ledgerID
	 */
	public int getLedgerID() {
		return ledgerID;
	}
	/**
	 * @param ledgerID the ledgerID to set
	 */
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	/**
	 * @return the groupID
	 */
	public int getGroupID() {
		return groupID;
	}
	/**
	 * @param groupID the groupID to set
	 */
	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}
	/**
	 * @return the rootID
	 */
	public int getRootID() {
		return rootID;
	}
	/**
	 * @param rootID the rootID to set
	 */
	public void setRootID(int rootID) {
		this.rootID = rootID;
	}
	/**
	 * @return the penaltyChargeID
	 */
	public int getPenaltyChargeID() {
		return penaltyChargeID;
	}
	/**
	 * @param penaltyChargeID the penaltyChargeID to set
	 */
	public void setPenaltyChargeID(int penaltyChargeID) {
		this.penaltyChargeID = penaltyChargeID;
	}
	/**
	 * @return the penaltyMode
	 */
	public String getPenaltyMode() {
		return penaltyMode;
	}
	/**
	 * @param penaltyMode the penaltyMode to set
	 */
	public void setPenaltyMode(String penaltyMode) {
		this.penaltyMode = penaltyMode;
	}
	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * @return the calMode
	 */
	public String getCalMode() {
		return calMode;
	}
	/**
	 * @param calMode the calMode to set
	 */
	public void setCalMode(String calMode) {
		this.calMode = calMode;
	}
	/**
	 * @return the calMethod
	 */
	public String getCalMethod() {
		return calMethod;
	}
	/**
	 * @param calMethod the calMethod to set
	 */
	public void setCalMethod(String calMethod) {
		this.calMethod = calMethod;
	}
	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the createDate
	 */
	public String getCreateDate() {
		return createDate;
	}
	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	/**
	 * @return the updateDate
	 */
	public String getUpdateDate() {
		return updateDate;
	}
	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the chargeTypeID
	 */
	public int getChargeTypeID() {
		return chargeTypeID;
	}
	/**
	 * @param chargeTypeID the chargeTypeID to set
	 */
	public void setChargeTypeID(int chargeTypeID) {
		this.chargeTypeID = chargeTypeID;
	}
	/**
	 * @return the chargeType
	 */
	public String getChargeType() {
		return chargeType;
	}
	/**
	 * @param chargeType the chargeType to set
	 */
	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}
	/**
	 * @return the chargeFrequency
	 */
	public String getChargeFrequency() {
		return chargeFrequency;
	}
	/**
	 * @param chargeFrequency the chargeFrequency to set
	 */
	public void setChargeFrequency(String chargeFrequency) {
		this.chargeFrequency = chargeFrequency;
	}
	/**
	 * @return the prevChargeDate
	 */
	public String getPrevChargeDate() {
		return prevChargeDate;
	}
	/**
	 * @param prevChargeDate the prevChargeDate to set
	 */
	public void setPrevChargeDate(String prevChargeDate) {
		this.prevChargeDate = prevChargeDate;
	}
	/**
	 * @return the nextChargeDate
	 */
	public String getNextChargeDate() {
		return nextChargeDate;
	}
	/**
	 * @param nextChargeDate the nextChargeDate to set
	 */
	public void setNextChargeDate(String nextChargeDate) {
		this.nextChargeDate = nextChargeDate;
	}
	/**
	 * @return the createInvoices
	 */
	public int getCreateInvoices() {
		return createInvoices;
	}
	/**
	 * @param createInvoices the createInvoices to set
	 */
	public void setCreateInvoices(int createInvoices) {
		this.createInvoices = createInvoices;
	}
	/**
	 * @return the isApplicable
	 */
	public Boolean getIsApplicable() {
		return isApplicable;
	}
	/**
	 * @param isApplicable the isApplicable to set
	 */
	public void setIsApplicable(Boolean isApplicable) {
		this.isApplicable = isApplicable;
	}
	/**
	 * @return the precison
	 */
	public int getPrecison() {
		return precison;
	}
	/**
	 * @param precison the precison to set
	 */
	public void setPrecison(int precison) {
		this.precison = precison;
	}
	/**
	 * @return the penaltyChargeDate
	 */
	public String getPenaltyChargeDate() {
		return penaltyChargeDate;
	}
	/**
	 * @param penaltyChargeDate the penaltyChargeDate to set
	 */
	public void setPenaltyChargeDate(String penaltyChargeDate) {
		this.penaltyChargeDate = penaltyChargeDate;
	}
	/**
	 * @return the itemID
	 */
	public int getItemID() {
		return itemID;
	}
	/**
	 * @return the gstin
	 */
	public String getSocGstIn() {
		return socGstIn;
	}
	/**
	 * @param itemID the itemID to set
	 */
	public void setItemID(int itemID) {
		this.itemID = itemID;
	}
	/**
	 * @param gstin the gstin to set
	 */
	public void setSocGstIn(String gstin) {
		this.socGstIn = gstin;
	}
	/**
	 * @return the iGstRate
	 */
	public BigDecimal getiGstRate() {
		return iGstRate;
	}
	/**
	 * @return the iGstAmount
	 */
	public BigDecimal getiGstAmount() {
		return iGstAmount;
	}
	/**
	 * @return the sGstRate
	 */
	public BigDecimal getsGstRate() {
		return sGstRate;
	}
	/**
	 * @return the sGstAmount
	 */
	public BigDecimal getsGstAmount() {
		return sGstAmount;
	}
	/**
	 * @return the cGstRate
	 */
	public BigDecimal getcGstRate() {
		return cGstRate;
	}
	/**
	 * @return the cGstAmount
	 */
	public BigDecimal getcGstAmount() {
		return cGstAmount;
	}
	/**
	 * @return the uGstRate
	 */
	public BigDecimal getuGstRate() {
		return uGstRate;
	}
	/**
	 * @return the uGstAmount
	 */
	public BigDecimal getuGstAmount() {
		return uGstAmount;
	}
	/**
	 * @return the gstRate
	 */
	public BigDecimal getGstRate() {
		return gstRate;
	}
	/**
	 * @return the gstAmount
	 */
	public BigDecimal getGstAmount() {
		return gstAmount;
	}
	/**
	 * @return the hsnsacCode
	 */
	public String getHsnsacCode() {
		return hsnsacCode;
	}
	/**
	 * @return the purchasePrice
	 */
	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}
	/**
	 * @return the sellingPrice
	 */
	public BigDecimal getSellingPrice() {
		return sellingPrice;
	}
	/**
	 * @param iGstRate the iGstRate to set
	 */
	public void setiGstRate(BigDecimal iGstRate) {
		this.iGstRate = iGstRate;
	}
	/**
	 * @param iGstAmount the iGstAmount to set
	 */
	public void setiGstAmount(BigDecimal iGstAmount) {
		this.iGstAmount = iGstAmount;
	}
	/**
	 * @param sGstRate the sGstRate to set
	 */
	public void setsGstRate(BigDecimal sGstRate) {
		this.sGstRate = sGstRate;
	}
	/**
	 * @param sGstAmount the sGstAmount to set
	 */
	public void setsGstAmount(BigDecimal sGstAmount) {
		this.sGstAmount = sGstAmount;
	}
	/**
	 * @param cGstRate the cGstRate to set
	 */
	public void setcGstRate(BigDecimal cGstRate) {
		this.cGstRate = cGstRate;
	}
	/**
	 * @param cGstAmount the cGstAmount to set
	 */
	public void setcGstAmount(BigDecimal cGstAmount) {
		this.cGstAmount = cGstAmount;
	}
	/**
	 * @param uGstRate the uGstRate to set
	 */
	public void setuGstRate(BigDecimal uGstRate) {
		this.uGstRate = uGstRate;
	}
	/**
	 * @param uGstAmount the uGstAmount to set
	 */
	public void setuGstAmount(BigDecimal uGstAmount) {
		this.uGstAmount = uGstAmount;
	}
	/**
	 * @param gstRate the gstRate to set
	 */
	public void setGstRate(BigDecimal gstRate) {
		this.gstRate = gstRate;
	}
	/**
	 * @param gstAmount the gstAmount to set
	 */
	public void setGstAmount(BigDecimal gstAmount) {
		this.gstAmount = gstAmount;
	}
	/**
	 * @param hsnsacCode the hsnsacCode to set
	 */
	public void setHsnsacCode(String hsnsacCode) {
		this.hsnsacCode = hsnsacCode;
	}
	/**
	 * @param purchasePrice the purchasePrice to set
	 */
	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	/**
	 * @param sellingPrice the sellingPrice to set
	 */
	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}
	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}
	/**
	 * @param itemName the itemName to set
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	/**
	 * @return the txDate
	 */
	public String getTxDate() {
		return txDate;
	}
	/**
	 * @param txDate the txDate to set
	 */
	public void setTxDate(String txDate) {
		this.txDate = txDate;
	}
	/**
	 * @return the gstCategoryID
	 */
	public int getGstCategoryID() {
		return gstCategoryID;
	}
	/**
	 * @param gstCategoryID the gstCategoryID to set
	 */
	public void setGstCategoryID(int gstCategoryID) {
		this.gstCategoryID = gstCategoryID;
	}
	
	
	
		
}
