package com.emanager.server.accounts.DataAccessObjects;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.Map;

public class BankStatement {
	
	private String rawTag61;
	private int ID;
	private Date valueDate;
	private Date BookingDate;
	private String payMode;
	private String referenceNumber;
	private BigDecimal deposit=BigDecimal.ZERO;
	private BigDecimal withdrawal=BigDecimal.ZERO;
	private BigDecimal balance=BigDecimal.ZERO;
	private String rawTag86;
	private String Description;
	private String bankAccNumber;
	private String rawData;
	private String batchID;
	private List<BankStatement> bankStatementList;
	private int orgID;
	private int txID;
	private int bankLedgerID;
	private int suspenceLedgerID;
	private String txType;
	private Date fromDate;
	private Date toDate;
	private BigDecimal txAmount;
	private int override=0;
	private Map<String,AccountHeadVO> virtualAccNos;
	private int fetchVirtualAccNo=0;
	private List<TransactionVO> transactionList;
	private String dataZoneID;
	
	/**
	 * @return the rawTag61
	 */
	public String getRawTag61() {
		return rawTag61;
	}
	/**
	 * @return the valueDate
	 */
	public Date getValueDate() {
		return valueDate;
	}
	/**
	 * @return the bookingDate
	 */
	public Date getBookingDate() {
		return BookingDate;
	}
	/**
	 * @return the payMode
	 */
	public String getPayMode() {
		return payMode;
	}
	/**
	 * @return the referenceNumber
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * @return the rawTag86
	 */
	public String getRawTag86() {
		return rawTag86;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return Description;
	}
	/**
	 * @return the bankAccNumber
	 */
	public String getBankAccNumber() {
		return bankAccNumber;
	}
	/**
	 * @param rawTag61 the rawTag61 to set
	 */
	public void setRawTag61(String rawTag61) {
		this.rawTag61 = rawTag61;
	}
	/**
	 * @param valueDate the valueDate to set
	 */
	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}
	/**
	 * @param bookingDate the bookingDate to set
	 */
	public void setBookingDate(Date bookingDate) {
		BookingDate = bookingDate;
	}
	/**
	 * @param payMode the payMode to set
	 */
	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}
	/**
	 * @param referenceNumber the referenceNumber to set
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	/**
	 * @param rawTag86 the rawTag86 to set
	 */
	public void setRawTag86(String rawTag86) {
		this.rawTag86 = rawTag86;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		Description = description;
	}
	/**
	 * @param bankAccNumber the bankAccNumber to set
	 */
	public void setBankAccNumber(String bankAccNumber) {
		this.bankAccNumber = bankAccNumber;
	}
	/**
	 * @return the rawData
	 */
	public String getRawData() {
		return rawData;
	}
	/**
	 * @param rawData the rawData to set
	 */
	public void setRawData(String rawData) {
		this.rawData = rawData;
	}
	/**
	 * @return the bankStatementList
	 */
	public List<BankStatement> getBankStatementList() {
		return bankStatementList;
	}
	/**
	 * @param bankStatementList the bankStatementList to set
	 */
	public void setBankStatementList(List<BankStatement> bankStatementList) {
		this.bankStatementList = bankStatementList;
	}
	/**
	 * @return the batchID
	 */
	public String getBatchID() {
		return batchID;
	}
	/**
	 * @param batchID the batchID to set
	 */
	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}
	/**
	 * @return the deposit
	 */
	public BigDecimal getDeposit() {
		return deposit;
	}
	/**
	 * @return the withdrawal
	 */
	public BigDecimal getWithdrawal() {
		return withdrawal;
	}
	/**
	 * @return the balance
	 */
	public BigDecimal getBalance() {
		return balance;
	}
	/**
	 * @param deposit the deposit to set
	 */
	public void setDeposit(BigDecimal deposit) {
		this.deposit = deposit;
	}
	/**
	 * @param withdrawal the withdrawal to set
	 */
	public void setWithdrawal(BigDecimal withdrawal) {
		this.withdrawal = withdrawal;
	}
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @return the txID
	 */
	public int getTxID() {
		return txID;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	/**
	 * @param txID the txID to set
	 */
	public void setTxID(int txID) {
		this.txID = txID;
	}
	/**
	 * @return the bankLedgerID
	 */
	public int getBankLedgerID() {
		return bankLedgerID;
	}
	/**
	 * @return the suspenceLedgerID
	 */
	public int getSuspenceLedgerID() {
		return suspenceLedgerID;
	}
	/**
	 * @param bankLedgerID the bankLedgerID to set
	 */
	public void setBankLedgerID(int bankLedgerID) {
		this.bankLedgerID = bankLedgerID;
	}
	/**
	 * @param suspenceLedgerID the suspenceLedgerID to set
	 */
	public void setSuspenceLedgerID(int suspenceLedgerID) {
		this.suspenceLedgerID = suspenceLedgerID;
	}
	/**
	 * @return the txType
	 */
	public String getTxType() {
		return txType;
	}
	/**
	 * @param txType the txType to set
	 */
	public void setTxType(String txType) {
		this.txType = txType;
	}
	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}
	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}
	/**
	 * @param iD the iD to set
	 */
	public void setID(int iD) {
		ID = iD;
	}
	/**
	 * @return the txAmount
	 */
	public BigDecimal getTxAmount() {
		return txAmount;
	}
	/**
	 * @param txAmount the txAmount to set
	 */
	public void setTxAmount(BigDecimal txAmount) {
		this.txAmount = txAmount;
	}
	/**
	 * @return the override
	 */
	public int getOverride() {
		return override;
	}
	/**
	 * @param override the override to set
	 */
	public void setOverride(int override) {
		this.override = override;
	}
	/**
	 * @return the virtualAccNos
	 */
	public Map<String, AccountHeadVO> getVirtualAccNos() {
		return virtualAccNos;
	}
	/**
	 * @param virtualAccNos the virtualAccNos to set
	 */
	public void setVirtualAccNos(Map<String, AccountHeadVO> virtualAccNos) {
		this.virtualAccNos = virtualAccNos;
	}
	/**
	 * @return the fetchVirtualAccNo
	 */
	public int getFetchVirtualAccNo() {
		return fetchVirtualAccNo;
	}
	/**
	 * @param fetchVirtualAccNo the fetchVirtualAccNo to set
	 */
	public void setFetchVirtualAccNo(int fetchVirtualAccNo) {
		this.fetchVirtualAccNo = fetchVirtualAccNo;
	}
	/**
	 * @return the transactionList
	 */
	public List<TransactionVO> getTransactionList() {
		return transactionList;
	}
	/**
	 * @param transactionList the transactionList to set
	 */
	public void setTransactionList(List<TransactionVO> transactionList) {
		this.transactionList = transactionList;
	}
	/**
	 * @return the dataZoneID
	 */
	public String getDataZoneID() {
		return dataZoneID;
	}
	/**
	 * @param dataZoneID the dataZoneID to set
	 */
	public void setDataZoneID(String dataZoneID) {
		this.dataZoneID = dataZoneID;
	}
	
}
