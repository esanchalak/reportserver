package com.emanager.server.accounts.DataAccessObjects;

import java.math.BigDecimal;
import java.util.List;

import com.emanager.server.society.valueObject.BankInfoVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;

public class OnlinePaymentGatewayVO {
	
	private int societyID;
	private String merchantKey;
	private String merchantSaltKey;
	private String gatewayName;
	private MemberVO memberVO;
	private SocietyVO societyVO;
	private BankInfoVO bankInfoVO;
	private int isOnlnPymtAccptd;
	private BigDecimal processingFees;
	private String gatewayUrl;
	private String type;
	private String merchantID;
	private int gatewayID;
	private BigDecimal gtNetBanking;
	private BigDecimal gtCreditCard;	
	private BigDecimal gtDcLess1000;
	private BigDecimal gtDc1000to2000;
	private BigDecimal gtDcGreater2000;
	private BigDecimal mtNetBanking;
	private BigDecimal mtCreditCard;	
	private BigDecimal mtDcLess1000;
	private BigDecimal mtDc1000to2000;
	private BigDecimal mtDcGreater2000;
    private BigDecimal totalAmount;
    private String childMerchantID;
    private BigDecimal gtProcessingFees;//Gateway processing fees
    private BigDecimal mtProcessingFees;//Received commissions
    private String paymentMode;
    private String isPayableAmountEditable;
    private String isCalculationInclusive; //for gateway 0:exclusive,1:inclusive
    private int orgID;
    private int memberLedgerID;
    private String description;
    private String childPaymentID;
    private String parentPaymentID;
    private String parentUtr;
    private String childUtr;
    private String customerName;
    private String customerEmail;
    private String customerPhone;
    private int isDeleted;
    private List<OnlinePaymentGatewayVO> objectList;
    private String txDate;
    private String fromDate;
    private String toDate;
    private BigDecimal commissionAmount;
    private BigDecimal actualAmount;
    private BigDecimal payUTDR;
    private int ID;
    private int txID;
    private String societyName;
    private int isComplete;
    private String sourceType;
    private BigDecimal gtUpi;
	private BigDecimal mtUpi;
    
    public String getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(String insertDate) {
		this.insertDate = insertDate;
	}
	private String insertDate;
    
    public String getSourceType() {
		return sourceType;
	}
	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	public BigDecimal getCommissionReceived() {
		return commissionReceived;
	}
	public void setCommissionReceived(BigDecimal commissionReceived) {
		this.commissionReceived = commissionReceived;
	}
	private BigDecimal commissionReceived;
    
	
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	public String getIsPayableAmountEditable() {
		return isPayableAmountEditable;
	}
	public void setIsPayableAmountEditable(String isPayableAmountEditable) {
		this.isPayableAmountEditable = isPayableAmountEditable;
	}
	public BigDecimal getMtDc1000to2000() {
		return mtDc1000to2000;
	}
	public void setMtDc1000to2000(BigDecimal mtDc1000to2000) {
		this.mtDc1000to2000 = mtDc1000to2000;
	}
	public BigDecimal getMtDcGreater2000() {
		return mtDcGreater2000;
	}
	public void setMtDcGreater2000(BigDecimal mtDcGreater2000) {
		this.mtDcGreater2000 = mtDcGreater2000;
	}
	public BigDecimal getGtCreditCard() {
		return gtCreditCard;
	}
	public void setGtCreditCard(BigDecimal gtCreditCard) {
		this.gtCreditCard = gtCreditCard;
	}
	public BigDecimal getGtDc1000to2000() {
		return gtDc1000to2000;
	}
	public void setGtDc1000to2000(BigDecimal gtDc1000to2000) {
		this.gtDc1000to2000 = gtDc1000to2000;
	}
	public BigDecimal getGtDcGreater2000() {
		return gtDcGreater2000;
	}
	public void setGtDcGreater2000(BigDecimal gtDcGreater2000) {
		this.gtDcGreater2000 = gtDcGreater2000;
	}
	public BigDecimal getMtCreditCard() {
		return mtCreditCard;
	}
	public void setMtCreditCard(BigDecimal mtCreditCard) {
		this.mtCreditCard = mtCreditCard;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public BigDecimal getGtProcessingFees() {
		return gtProcessingFees;
	}
	public void setGtProcessingFees(BigDecimal gtProcessingFees) {
		this.gtProcessingFees = gtProcessingFees;
	}
	public BigDecimal getMtProcessingFees() {
		return mtProcessingFees;
	}
	public void setMtProcessingFees(BigDecimal mtProcessingFees) {
		this.mtProcessingFees = mtProcessingFees;
	}
	
	public String getChildMerchantID() {
		return childMerchantID;
	}
	public void setChildMerchantID(String childMerchantID) {
		this.childMerchantID = childMerchantID;
	}
	public BigDecimal getGtNetBanking() {
		return gtNetBanking;
	}
	public void setGtNetBanking(BigDecimal gtNetBanking) {
		this.gtNetBanking = gtNetBanking;
	}
	
	public BigDecimal getGtDcLess1000() {
		return gtDcLess1000;
	}
	public void setGtDcLess1000(BigDecimal gtDcLess1000) {
		this.gtDcLess1000 = gtDcLess1000;
	}
	
	public BigDecimal getMtNetBanking() {
		return mtNetBanking;
	}
	public void setMtNetBanking(BigDecimal mtNetBanking) {
		this.mtNetBanking = mtNetBanking;
	}
	
	
	public BigDecimal getMtDcLess1000() {
		return mtDcLess1000;
	}
	public void setMtDcLess1000(BigDecimal mtDcLess1000) {
		this.mtDcLess1000 = mtDcLess1000;
	}
	
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	public int getGatewayID() {
		return gatewayID;
	}
	public void setGatewayID(int gatewayID) {
		this.gatewayID = gatewayID;
	}

	public String getMerchantID() {
		return merchantID;
	}
	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public BankInfoVO getBankInfoVO() {
		return bankInfoVO;
	}
	public void setBankInfoVO(BankInfoVO bankInfoVO) {
		this.bankInfoVO = bankInfoVO;
	}
	public String getGatewayName() {
		return gatewayName;
	}
	public void setGatewayName(String gatewayName) {
		this.gatewayName = gatewayName;
	}
	public int getIsOnlnPymtAccptd() {
		return isOnlnPymtAccptd;
	}
	public void setIsOnlnPymtAccptd(int isOnlnPymtAccptd) {
		this.isOnlnPymtAccptd = isOnlnPymtAccptd;
	}
	public MemberVO getMemberVO() {
		return memberVO;
	}
	public void setMemberVO(MemberVO memberVO) {
		this.memberVO = memberVO;
	}
	public String getMerchantKey() {
		return merchantKey;
	}
	public void setMerchantKey(String merchantKey) {
		this.merchantKey = merchantKey;
	}
	public String getMerchantSaltKey() {
		return merchantSaltKey;
	}
	public void setMerchantSaltKey(String merchantSaltKey) {
		this.merchantSaltKey = merchantSaltKey;
	}
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	public SocietyVO getSocietyVO() {
		return societyVO;
	}
	public void setSocietyVO(SocietyVO societyVO) {
		this.societyVO = societyVO;
	}
	public String getGatewayUrl() {
		return gatewayUrl;
	}
	public void setGatewayUrl(String gatewayUrl) {
		this.gatewayUrl = gatewayUrl;
	}
	public BigDecimal getProcessingFees() {
		return processingFees;
	}
	public void setProcessingFees(BigDecimal processingFees) {
		this.processingFees = processingFees;
	}
	/**
	 * @return the isCalculationInclusive
	 */
	public String getIsCalculationInclusive() {
		return isCalculationInclusive;
	}
	/**
	 * @param isCalculationInclusive the isCalculationInclusive to set
	 */
	public void setIsCalculationInclusive(String isCalculationInclusive) {
		this.isCalculationInclusive = isCalculationInclusive;
	}
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	/**
	 * @return the memberLedgerID
	 */
	public int getMemberLedgerID() {
		return memberLedgerID;
	}
	/**
	 * @param memberLedgerID the memberLedgerID to set
	 */
	public void setMemberLedgerID(int memberLedgerID) {
		this.memberLedgerID = memberLedgerID;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the childPaymentID
	 */
	public String getChildPaymentID() {
		return childPaymentID;
	}
	/**
	 * @param childPaymentID the childPaymentID to set
	 */
	public void setChildPaymentID(String childPaymentID) {
		this.childPaymentID = childPaymentID;
	}
	/**
	 * @return the parentUtr
	 */
	public String getParentUtr() {
		return parentUtr;
	}
	/**
	 * @param parentUtr the parentUtr to set
	 */
	public void setParentUtr(String parentUtr) {
		this.parentUtr = parentUtr;
	}
	/**
	 * @return the childUtr
	 */
	public String getChildUtr() {
		return childUtr;
	}
	/**
	 * @param childUtr the childUtr to set
	 */
	public void setChildUtr(String childUtr) {
		this.childUtr = childUtr;
	}
	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}
	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	/**
	 * @return the customerEmail
	 */
	public String getCustomerEmail() {
		return customerEmail;
	}
	/**
	 * @param customerEmail the customerEmail to set
	 */
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	/**
	 * @return the customerPhone
	 */
	public String getCustomerPhone() {
		return customerPhone;
	}
	/**
	 * @param customerPhone the customerPhone to set
	 */
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	/**
	 * @return the isDeleted
	 */
	public int getIsDeleted() {
		return isDeleted;
	}
	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	/**
	 * @return the objectList
	 */
	public List<OnlinePaymentGatewayVO> getObjectList() {
		return objectList;
	}
	/**
	 * @param objectList the objectList to set
	 */
	public void setObjectList(List<OnlinePaymentGatewayVO> objectList) {
		this.objectList = objectList;
	}
	/**
	 * @return the txDate
	 */
	public String getTxDate() {
		return txDate;
	}
	/**
	 * @param txDate the txDate to set
	 */
	public void setTxDate(String txDate) {
		this.txDate = txDate;
	}
	/**
	 * @return the commissionAmount
	 */
	public BigDecimal getCommissionAmount() {
		return commissionAmount;
	}
	/**
	 * @param commissionAmount the commissionAmount to set
	 */
	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}
	/**
	 * @return the actualAmount
	 */
	public BigDecimal getActualAmount() {
		return actualAmount;
	}
	/**
	 * @param actualAmount the actualAmount to set
	 */
	public void setActualAmount(BigDecimal actualAmount) {
		this.actualAmount = actualAmount;
	}
	/**
	 * @return the parentPaymentID
	 */
	public String getParentPaymentID() {
		return parentPaymentID;
	}
	/**
	 * @param parentPaymentID the parentPaymentID to set
	 */
	public void setParentPaymentID(String parentPaymentID) {
		this.parentPaymentID = parentPaymentID;
	}
	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}
	/**
	 * @param iD the iD to set
	 */
	public void setID(int iD) {
		ID = iD;
	}
	/**
	 * @return the txID
	 */
	public int getTxID() {
		return txID;
	}
	/**
	 * @param txID the txID to set
	 */
	public void setTxID(int txID) {
		this.txID = txID;
	}
	
	public int getIsComplete() {
		return isComplete;
	}
	public void setIsComplete(int isComplete) {
		this.isComplete = isComplete;
	}
	public BigDecimal getPayUTDR() {
		return payUTDR;
	}
	public void setPayUTDR(BigDecimal payUTDR) {
		this.payUTDR = payUTDR;
	}
	public BigDecimal getGtUpi() {
		return gtUpi;
	}
	public void setGtUpi(BigDecimal gtUpi) {
		this.gtUpi = gtUpi;
	}
	public BigDecimal getMtUpi() {
		return mtUpi;
	}
	public void setMtUpi(BigDecimal mtUpi) {
		this.mtUpi = mtUpi;
	}

}
