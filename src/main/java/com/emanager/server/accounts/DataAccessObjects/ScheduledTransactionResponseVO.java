package com.emanager.server.accounts.DataAccessObjects;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ScheduledTransactionResponseVO  {
	
	
	private int eventID;
	private int memberID;
	private int societyID;
	private String societyName;
	private int successCount;
	private int failureCount;
	private String responseEmailID;
	private String failureTxs;
	private String successTxs;
	private BigDecimal totalTxAmount;
	private String frequency;
	private int lateFeeSucccessCount;
	private int lateFeeFailureCount;
	private int isLateFee;
	private ArrayList<String> chargeList;
	
	
	
	
	
	
	public int getEventID() {
		return eventID;
	}
	public void setEventID(int eventID) {
		this.eventID = eventID;
	}
	public int getMemberID() {
		return memberID;
	}
	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}
	public int getSocietyID() {
		return societyID;
	}
	public void setSocietyID(int societyID) {
		this.societyID = societyID;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	public int getSuccessCount() {
		return successCount;
	}
	public void setSuccessCount(int successCount) {
		this.successCount = successCount;
	}
	public int getFailureCount() {
		return failureCount;
	}
	public void setFailureCount(int failureCount) {
		this.failureCount = failureCount;
	}
	public String getResponseEmailID() {
		return responseEmailID;
	}
	public void setResponseEmailID(String responseEmailID) {
		this.responseEmailID = responseEmailID;
	}
	public String getFailureTxs() {
		return failureTxs;
	}
	public void setFailureTxs(String failureTxs) {
		this.failureTxs = failureTxs;
	}
	public String getSuccessTxs() {
		return successTxs;
	}
	public void setSuccessTxs(String successTxs) {
		this.successTxs = successTxs;
	}
	public BigDecimal getTotalTxAmount() {
		return totalTxAmount;
	}
	public void setTotalTxAmount(BigDecimal totalTxAmount) {
		this.totalTxAmount = totalTxAmount;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public int getIsLateFee() {
		return isLateFee;
	}
	public void setIsLateFee(int isLateFee) {
		this.isLateFee = isLateFee;
	}
	/**
	 * @return the chargeList
	 */
	public ArrayList<String> getChargeList() {
		return chargeList;
	}
	/**
	 * @param chargeList the chargeList to set
	 */
	public void setChargeList(ArrayList<String> chargeList) {
		this.chargeList = chargeList;
	}
	/**
	 * @return the lateFeeSucccessCount
	 */
	public int getLateFeeSucccessCount() {
		return lateFeeSucccessCount;
	}
	/**
	 * @return the lateFeeFailureCount
	 */
	public int getLateFeeFailureCount() {
		return lateFeeFailureCount;
	}
	/**
	 * @param lateFeeSucccessCount the lateFeeSucccessCount to set
	 */
	public void setLateFeeSucccessCount(int lateFeeSucccessCount) {
		this.lateFeeSucccessCount = lateFeeSucccessCount;
	}
	/**
	 * @param lateFeeFailureCount the lateFeeFailureCount to set
	 */
	public void setLateFeeFailureCount(int lateFeeFailureCount) {
		this.lateFeeFailureCount = lateFeeFailureCount;
	}
	
	
	
	
	
	
}
