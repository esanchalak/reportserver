package com.emanager.server.accounts.DataAccessObjects;

import java.math.BigDecimal;
import java.util.List;

import com.emanager.server.adminReports.valueObject.PrintReportVO;
import com.emanager.server.invoice.dataAccessObject.ProfileDetailsVO;
import com.emanager.server.taxation.dataAccessObject.TDSDetailsVO;


public class AccountHeadVO extends PrintReportVO {
	
	/* Check TallyGroupConstants for Standard Primary Group Names in Tally.
	 * In Tally Primary or Parent Ledgers are mandatory while creating a group.
	 * Groups = Ledgers */
	
	
	
	private String strAccountPrimaryHead; 
	/* To be used to fetch or create new Group Account*/
	
	private int accountGroupID; 
	/* Ledger Name */
	private String strAccountHeadName;
	/* To be used for Update group Name */
	private String strNewAccountHeadName; 
	
	private BigDecimal openingBalance=BigDecimal.ZERO;
	private BigDecimal crOpnBal=BigDecimal.ZERO;
	private BigDecimal drOpnBal=BigDecimal.ZERO;
	private BigDecimal sumOfCreditBal=BigDecimal.ZERO;
	//Positive Balance
	private BigDecimal opPosBal=BigDecimal.ZERO;
	private BigDecimal clPosBal=BigDecimal.ZERO;
	
	private BigDecimal closingBalance=BigDecimal.ZERO;
	private BigDecimal crClsBal=BigDecimal.ZERO;
	private BigDecimal drClsBal=BigDecimal.ZERO;
	//Negative Balance
	private BigDecimal clNegBal=BigDecimal.ZERO;
	private BigDecimal opNegBal=BigDecimal.ZERO;
	private BigDecimal sumOfDebitBal=BigDecimal.ZERO;
	private int ledgerID;
	
	private String ledgerType;
	
	private String updateDate;
	
	private String createDate;
	
	private String lockDate;
	
	private BigDecimal sum=BigDecimal.ZERO;
	
	private BigDecimal sumInterest=BigDecimal.ZERO;
	
	private String effectiveDate;
	
	private String normalBalance;
	
	private int rootID;
	
	private int isHidden;
	
	private int isDeleted;
	
	private String description; 
	
	private int isPrimary;
	
	private String ledgerShortName;
	
	private String rootGroupName;
	
	private BigDecimal recBalance=BigDecimal.ZERO;
	
	private String accType;
	
	private int tdsLedgerID;
	
	private String tdsSection;
	
	private String tdsSectionDescription;
	
	private BigDecimal cutOffAmt=BigDecimal.ZERO;
	
	private BigDecimal tdsRtIndiv=BigDecimal.ZERO;
	
	private BigDecimal tdsRtOther=BigDecimal.ZERO;;
	
	private String tdsSectionNo;
	
	private BigDecimal tdsRate=BigDecimal.ZERO;;
	
	private TDSDetailsVO tdsVO;
	
	private int serviceLedgerID;
	
	private String subCategoryName;
	
	private int subCategoryID;
	
	private String categoryName;
	
	private int categoryID;
	
	private int creditAction;
	
	private int debitAction;
	
	private BigDecimal amount=BigDecimal.ZERO;;
	
	private int orgID;
	
	private int statusCode;
	
	private String message;
	
	private String additionalProps;
	
	private ProfileDetailsVO profileVO;
	
	private BigDecimal intOpnBalance=BigDecimal.ZERO;;
	
	private BigDecimal intClsBalance=BigDecimal.ZERO;;
	
	private String ledgerName;
	
	private int packageID;
	
	private String groupTags;
	
	private String projectTags;
	
	/**
	 * @return the objectList
	 */
	private List objectList;
	
	private int rptID;
	private int id;
	private int parentID;
	
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}

	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}

	/**
	 * @return the group Name
	 */
	public String getStrAccountPrimaryHead() {
		return strAccountPrimaryHead;
	}

	/**
	 * @param group name the group name to set
	 */
	public void setStrAccountPrimaryHead(String strAccountPrimaryHead) {
		this.strAccountPrimaryHead = strAccountPrimaryHead;
	}

	/**
	 * @return the ledgerName
	 */
	public String getStrAccountHeadName() {
		return strAccountHeadName;
	}

	/**
	 * @param ledger name the ledger Name to set
	 */
	public void setStrAccountHeadName(String strAccountHeadName) {
		this.strAccountHeadName = strAccountHeadName;
	}

	
	/**
	 * @return the strNewAccountHeadpName
	 */
	public String getStrNewAccountHeadName() {
		return strNewAccountHeadName;
	}

	/**
	 * @param strNewAccountHeadpName the strNewAccountHeadpName to set
	 */
	public void setStrNewAccountHeadName(String strNewAccountHeadpName) {
		this.strNewAccountHeadName = strNewAccountHeadpName;
	}

	/**
	 * @return the openingBalance
	 */
	public BigDecimal getOpeningBalance() {
		return openingBalance;
	}

	/**
	 * @param openingBalance the openingBalance to set
	 */
	public void setOpeningBalance(BigDecimal openingBalance) {
		this.openingBalance = openingBalance;
	}

	/**
	 * @return the closingBalance
	 */
	public BigDecimal getClosingBalance() {
		return closingBalance;
	}

	/**
	 * @param closingBalance the closingBalance to set
	 */
	public void setClosingBalance(BigDecimal closingBalance) {
		this.closingBalance = closingBalance;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public int getLedgerID() {
		return ledgerID;
	}

	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}

	public String getLedgerType() {
		return ledgerType;
	}

	public void setLedgerType(String ledgerType) {
		this.ledgerType = ledgerType;
	}

	public int getAccountGroupID() {
		return accountGroupID;
	}

	public void setAccountGroupID(int accountGroupID) {
		this.accountGroupID = accountGroupID;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public BigDecimal getSum() {
		return sum;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	

	public int getRootID() {
		return rootID;
	}

	public void setRootID(int rootID) {
		this.rootID = rootID;
	}

	public String getNormalBalance() {
		return normalBalance;
	}

	public void setNormalBalance(String normalBalance) {
		this.normalBalance = normalBalance;
	}

	public BigDecimal getCrClsBal() {
		return crClsBal;
	}

	public void setCrClsBal(BigDecimal crClsBal) {
		this.crClsBal = crClsBal;
	}

	public BigDecimal getCrOpnBal() {
		return crOpnBal;
	}

	public void setCrOpnBal(BigDecimal crOpnBal) {
		this.crOpnBal = crOpnBal;
	}

	public BigDecimal getDrClsBal() {
		return drClsBal;
	}

	public void setDrClsBal(BigDecimal drClsBal) {
		this.drClsBal = drClsBal;
	}

	public BigDecimal getDrOpnBal() {
		return drOpnBal;
	}

	public void setDrOpnBal(BigDecimal drOpnBal) {
		this.drOpnBal = drOpnBal;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getIsHidden() {
		return isHidden;
	}

	public void setIsHidden(int isHidden) {
		this.isHidden = isHidden;
	}

	public int getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(int isPrimary) {
		this.isPrimary = isPrimary;
	}

	public String getLedgerShortName() {
		return ledgerShortName;
	}

	public void setLedgerShortName(String ledgerShortName) {
		this.ledgerShortName = ledgerShortName;
	}

	public String getLockDate() {
		return lockDate;
	}

	public void setLockDate(String lockDate) {
		this.lockDate = lockDate;
	}

	public String getRootGroupName() {
		return rootGroupName;
	}

	public void setRootGroupName(String rootGroupName) {
		this.rootGroupName = rootGroupName;
	}

	public BigDecimal getClNegBal() {
		return clNegBal;
	}

	public void setClNegBal(BigDecimal clNegBal) {
		this.clNegBal = clNegBal;
	}

	public BigDecimal getClPosBal() {
		return clPosBal;
	}

	public void setClPosBal(BigDecimal clPosBal) {
		this.clPosBal = clPosBal;
	}

	public BigDecimal getOpNegBal() {
		return opNegBal;
	}

	public void setOpNegBal(BigDecimal opNegBal) {
		this.opNegBal = opNegBal;
	}

	public BigDecimal getOpPosBal() {
		return opPosBal;
	}

	public void setOpPosBal(BigDecimal opPosBal) {
		this.opPosBal = opPosBal;
	}

	public BigDecimal getRecBalance() {
		return recBalance;
	}

	public void setRecBalance(BigDecimal recBalance) {
		this.recBalance = recBalance;
	}

	public String getAccType() {
		return accType;
	}

	public void setAccType(String accType) {
		this.accType = accType;
	}

	public BigDecimal getCutOffAmt() {
		return cutOffAmt;
	}

	public void setCutOffAmt(BigDecimal cutOffAmt) {
		this.cutOffAmt = cutOffAmt;
	}

	public BigDecimal getTdsRtIndiv() {
		return tdsRtIndiv;
	}

	public void setTdsRtIndiv(BigDecimal tdsRtIndiv) {
		this.tdsRtIndiv = tdsRtIndiv;
	}

	public BigDecimal getTdsRtOther() {
		return tdsRtOther;
	}

	public void setTdsRtOther(BigDecimal tdsRtOther) {
		this.tdsRtOther = tdsRtOther;
	}

	public String getTdsSection() {
		return tdsSection;
	}

	public void setTdsSection(String tdsSection) {
		this.tdsSection = tdsSection;
	}

	public String getTdsSectionDescription() {
		return tdsSectionDescription;
	}

	public void setTdsSectionDescription(String tdsSectionDescription) {
		this.tdsSectionDescription = tdsSectionDescription;
	}

	public String getTdsSectionNo() {
		return tdsSectionNo;
	}

	public void setTdsSectionNo(String tdsSectionNo) {
		this.tdsSectionNo = tdsSectionNo;
	}

	public BigDecimal getTdsRate() {
		return tdsRate;
	}

	public void setTdsRate(BigDecimal tdsRate) {
		this.tdsRate = tdsRate;
	}

	public TDSDetailsVO getTdsVO() {
		return tdsVO;
	}

	public void setTdsVO(TDSDetailsVO tdsVO) {
		this.tdsVO = tdsVO;
	}

	public int getTdsLedgerID() {
		return tdsLedgerID;
	}

	public void setTdsLedgerID(int tdsLedgerID) {
		this.tdsLedgerID = tdsLedgerID;
	}

	public BigDecimal getSumOfcreditBal() {
		return sumOfCreditBal;
	}

	public void setSumOfCreditBal(BigDecimal sumOfCreditBal) {
		this.sumOfCreditBal = sumOfCreditBal;
	}

	public BigDecimal getSumOfDebitBal() {
		return sumOfDebitBal;
	}

	public void setSumOfDebitBal(BigDecimal sumOfDebitBal) {
		this.sumOfDebitBal = sumOfDebitBal;
	}

	public int getServiceLedgerID() {
		return serviceLedgerID;
	}

	public void setServiceLedgerID(int serviceLedgerID) {
		this.serviceLedgerID = serviceLedgerID;
	}

	public BigDecimal getSumOfCreditBal() {
		return sumOfCreditBal;
	}

	public int getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	public int getCreditAction() {
		return creditAction;
	}

	public void setCreditAction(int creditAction) {
		this.creditAction = creditAction;
	}

	public int getDebitAction() {
		return debitAction;
	}

	public void setDebitAction(int debitAction) {
		this.debitAction = debitAction;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the additionalProps
	 */
	public String getAdditionalProps() {
		return additionalProps;
	}

	/**
	 * @return the profileVO
	 */
	public ProfileDetailsVO getProfileVO() {
		return profileVO;
	}

	/**
	 * @param additionalProps the additionalProps to set
	 */
	public void setAdditionalProps(String additionalProps) {
		this.additionalProps = additionalProps;
	}

	/**
	 * @param profileVO the profileVO to set
	 */
	public void setProfileVO(ProfileDetailsVO profileVO) {
		this.profileVO = profileVO;
	}

	/**
	 * @return the objectList
	 */
	public List getObjectList() {
		return objectList;
	}

	/**
	 * @param objectList the objectList to set
	 */
	public void setObjectList(List objectList) {
		this.objectList = objectList;
	}

	/**
	 * @return the subCategoryName
	 */
	public String getSubCategoryName() {
		return subCategoryName;
	}

	/**
	 * @return the subCategoryID
	 */
	public int getSubCategoryID() {
		return subCategoryID;
	}

	/**
	 * @param subCategoryName the subCategoryName to set
	 */
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	/**
	 * @param subCategoryID the subCategoryID to set
	 */
	public void setSubCategoryID(int subCategoryID) {
		this.subCategoryID = subCategoryID;
	}

	/**
	 * @return the intOpnBalance
	 */
	public BigDecimal getIntOpnBalance() {
		return intOpnBalance;
	}

	/**
	 * @return the intClsBalance
	 */
	public BigDecimal getIntClsBalance() {
		return intClsBalance;
	}

	/**
	 * @param intOpnBalance the intOpnBalance to set
	 */
	public void setIntOpnBalance(BigDecimal intOpnBalance) {
		this.intOpnBalance = intOpnBalance;
	}

	/**
	 * @param intClsBalance the intClsBalance to set
	 */
	public void setIntClsBalance(BigDecimal intClsBalance) {
		this.intClsBalance = intClsBalance;
	}

	/**
	 * @return the sumInterest
	 */
	public BigDecimal getSumInterest() {
		return sumInterest;
	}

	/**
	 * @param sumInterest the sumInterest to set
	 */
	public void setSumInterest(BigDecimal sumInterest) {
		this.sumInterest = sumInterest;
	}


	public String getLedgerName() {
		return ledgerName;
	}

	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}

	/**
	 * @return the packageID
	 */
	public int getPackageID() {
		return packageID;
	}

	/**
	 * @param packageID the packageID to set
	 */
	public void setPackageID(int packageID) {
		this.packageID = packageID;
	}

	/**
	 * @return the groupTags
	 */
	public String getGroupTags() {
		return groupTags;
	}

	/**
	 * @param groupTags the groupTags to set
	 */
	public void setGroupTags(String groupTags) {
		this.groupTags = groupTags;
	}

	/**
	 * @return the projectTags
	 */
	public String getProjectTags() {
		return projectTags;
	}

	/**
	 * @param projectTags the projectTags to set
	 */
	public void setProjectTags(String projectTags) {
		this.projectTags = projectTags;
	}

	/**
	 * @return the rptID
	 */
	public int getRptID() {
		return rptID;
	}

	/**
	 * @param rptID the rptID to set
	 */
	public void setRptID(int rptID) {
		this.rptID = rptID;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the parentID
	 */
	public int getParentID() {
		return parentID;
	}

	/**
	 * @param parentID the parentID to set
	 */
	public void setParentID(int parentID) {
		this.parentID = parentID;
	}

}
