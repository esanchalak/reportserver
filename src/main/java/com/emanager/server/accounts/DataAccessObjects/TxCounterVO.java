package com.emanager.server.accounts.DataAccessObjects;

public class TxCounterVO {

	private int txCounterID;
	private int orgID;
	private int contra;
	private int creditNote;
	private int debitNote;
	private int jobworkIn;
	private int jobworkOut;
	private int journal;
	private int materialIn;
	private int materialOut;
	private int memorandum;
	private int payment;
	private int physicalStock;
	private int purchase;
	private int purchaseOrder;
	private int receipt;
	private int receiptNote;
	private int rejectionIn;
	private int rejectionOut;
	private int reversingJournal;
	private int sales;
	private int salesOrder;
	private int stockJournal;
	/**
	 * @return the txCounterID
	 */
	public int getTxCounterID() {
		return txCounterID;
	}
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @return the contra
	 */
	public int getContra() {
		return contra;
	}
	/**
	 * @return the creditNote
	 */
	public int getCreditNote() {
		return creditNote;
	}
	/**
	 * @return the debitNote
	 */
	public int getDebitNote() {
		return debitNote;
	}
	/**
	 * @return the jobworkIn
	 */
	public int getJobworkIn() {
		return jobworkIn;
	}
	/**
	 * @return the jobworkOut
	 */
	public int getJobworkOut() {
		return jobworkOut;
	}
	/**
	 * @return the journal
	 */
	public int getJournal() {
		return journal;
	}
	/**
	 * @return the materialIn
	 */
	public int getMaterialIn() {
		return materialIn;
	}
	/**
	 * @return the materialOut
	 */
	public int getMaterialOut() {
		return materialOut;
	}
	/**
	 * @return the memorandum
	 */
	public int getMemorandum() {
		return memorandum;
	}
	/**
	 * @return the payment
	 */
	public int getPayment() {
		return payment;
	}
	/**
	 * @return the physicalStock
	 */
	public int getPhysicalStock() {
		return physicalStock;
	}
	/**
	 * @return the purchase
	 */
	public int getPurchase() {
		return purchase;
	}
	/**
	 * @return the purchaseOrder
	 */
	public int getPurchaseOrder() {
		return purchaseOrder;
	}
	/**
	 * @return the receipt
	 */
	public int getReceipt() {
		return receipt;
	}
	/**
	 * @return the receiptNote
	 */
	public int getReceiptNote() {
		return receiptNote;
	}
	/**
	 * @return the rejectionIn
	 */
	public int getRejectionIn() {
		return rejectionIn;
	}
	/**
	 * @return the rejectionOut
	 */
	public int getRejectionOut() {
		return rejectionOut;
	}
	/**
	 * @return the reversingJournal
	 */
	public int getReversingJournal() {
		return reversingJournal;
	}
	/**
	 * @return the sales
	 */
	public int getSales() {
		return sales;
	}
	/**
	 * @return the salesOrder
	 */
	public int getSalesOrder() {
		return salesOrder;
	}
	/**
	 * @return the stockJournal
	 */
	public int getStockJournal() {
		return stockJournal;
	}
	/**
	 * @param txCounterID the txCounterID to set
	 */
	public void setTxCounterID(int txCounterID) {
		this.txCounterID = txCounterID;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	/**
	 * @param contra the contra to set
	 */
	public void setContra(int contra) {
		this.contra = contra;
	}
	/**
	 * @param creditNote the creditNote to set
	 */
	public void setCreditNote(int creditNote) {
		this.creditNote = creditNote;
	}
	/**
	 * @param debitNote the debitNote to set
	 */
	public void setDebitNote(int debitNote) {
		this.debitNote = debitNote;
	}
	/**
	 * @param jobworkIn the jobworkIn to set
	 */
	public void setJobworkIn(int jobworkIn) {
		this.jobworkIn = jobworkIn;
	}
	/**
	 * @param jobworkOut the jobworkOut to set
	 */
	public void setJobworkOut(int jobworkOut) {
		this.jobworkOut = jobworkOut;
	}
	/**
	 * @param journal the journal to set
	 */
	public void setJournal(int journal) {
		this.journal = journal;
	}
	/**
	 * @param materialIn the materialIn to set
	 */
	public void setMaterialIn(int materialIn) {
		this.materialIn = materialIn;
	}
	/**
	 * @param materialOut the materialOut to set
	 */
	public void setMaterialOut(int materialOut) {
		this.materialOut = materialOut;
	}
	/**
	 * @param memorandum the memorandum to set
	 */
	public void setMemorandum(int memorandum) {
		this.memorandum = memorandum;
	}
	/**
	 * @param payment the payment to set
	 */
	public void setPayment(int payment) {
		this.payment = payment;
	}
	/**
	 * @param physicalStock the physicalStock to set
	 */
	public void setPhysicalStock(int physicalStock) {
		this.physicalStock = physicalStock;
	}
	/**
	 * @param purchase the purchase to set
	 */
	public void setPurchase(int purchase) {
		this.purchase = purchase;
	}
	/**
	 * @param purchaseOrder the purchaseOrder to set
	 */
	public void setPurchaseOrder(int purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}
	/**
	 * @param receipt the receipt to set
	 */
	public void setReceipt(int receipt) {
		this.receipt = receipt;
	}
	/**
	 * @param receiptNote the receiptNote to set
	 */
	public void setReceiptNote(int receiptNote) {
		this.receiptNote = receiptNote;
	}
	/**
	 * @param rejectionIn the rejectionIn to set
	 */
	public void setRejectionIn(int rejectionIn) {
		this.rejectionIn = rejectionIn;
	}
	/**
	 * @param rejectionOut the rejectionOut to set
	 */
	public void setRejectionOut(int rejectionOut) {
		this.rejectionOut = rejectionOut;
	}
	/**
	 * @param reversingJournal the reversingJournal to set
	 */
	public void setReversingJournal(int reversingJournal) {
		this.reversingJournal = reversingJournal;
	}
	/**
	 * @param sales the sales to set
	 */
	public void setSales(int sales) {
		this.sales = sales;
	}
	/**
	 * @param salesOrder the salesOrder to set
	 */
	public void setSalesOrder(int salesOrder) {
		this.salesOrder = salesOrder;
	}
	/**
	 * @param stockJournal the stockJournal to set
	 */
	public void setStockJournal(int stockJournal) {
		this.stockJournal = stockJournal;
	}
}
