package com.emanager.server.accounts.Service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.accounts.DAO.TransactionDAO;
import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.BankStatement;
import com.emanager.server.accounts.DataAccessObjects.ChargesVO;
import com.emanager.server.accounts.DataAccessObjects.LedgerEntries;
import com.emanager.server.accounts.DataAccessObjects.OnlinePaymentGatewayVO;
import com.emanager.server.accounts.DataAccessObjects.ScheduledTransactionResponseVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.DataAccessObjects.TxCounterVO;
import com.emanager.server.accounts.Domain.ReceiptInvoiceDomain;
import com.emanager.server.accounts.Domain.TransactionsDomain;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.invoice.dataAccessObject.InvoiceVO;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.webservice.JSONResponseVO;


public class TransactionService {

	
	TransactionDAO accTransactionDAO;
	TransactionsDomain transactionsDomain;
	ReceiptInvoiceDomain receiptInvoiceDomain;
	Logger log=Logger.getLogger(TransactionService.class);
	DateUtility dateUtil=new DateUtility();
	
	
			
	
	/* Used for insering transaction and inserting ledger_entries */
	public TransactionVO addTransaction(TransactionVO transactionVO,int societyID){
		int success=0;
		int insertTxFlag=0;
		int insertLdgrEntries=0;
		List ledgerEntries=null;
		try {
			
			transactionVO=transactionsDomain.insertTransaction(transactionVO, societyID);
			
			
		} catch (Exception e) {
			log.error("Exception in adding transaction "+e.getMessage()+" "+e);
		}
		
		
		
		return transactionVO;
		
	}
	
		
	/* Used for insering Receipt transaction and inserting ledger_entries */
	public int insertReceiptTransaction(TransactionVO transactionVO,String societyID){
		int success=0;
		int insertTxFlag=0;
		int insertLdgrEntries=0;
		List ledgerEntries=null;
		log.debug("Entry : public int insertReceiptTransaction(TransactionVO transactionVO,String societyID)");
		try {
			
			
			
			
			
		} catch (Exception e) {
			log.error("Exception in adding transaction "+e.getMessage()+" "+e);
		}
		
		
		log.debug("Exit : public int insertReceiptTransaction(TransactionVO transactionVO,String societyID)");
		return success;
		
	}
	/* Used for updating transaction without affecting amount */
	public TransactionVO updateTransaction(TransactionVO transaction,int societyID,int txID) {
		// TODO Auto-generated method stub
		log.debug("Entry : public int updateTransaction(TransactionVO transaction,String societyID)");
		int success=0;
		List ledgerEntryList=null;
		transaction.setTransactionID(txID);
		try{
			
		transaction=transactionsDomain.updateTransaction(transaction,societyID,txID);
		
		
		}catch(NullPointerException e){
			log.error("Exception in updateTx "+e);
		}
		
		
		log.debug("Exit : public int updateTransaction(TransactionVO transaction,String societyID)"+success);
		
		return transaction;
	}
	
	/* Used for inserting transaction and inserting ledger_entries */
	public TransactionVO addInvoice(TransactionVO transactionVO,int societyID){
		log.debug(" Entry : public TransactionVO addInvoice(TransactionVO transactionVO,int societyID)");
		try {
			
			transactionVO=transactionsDomain.addInvoice(transactionVO, societyID);
			
			
		} catch (Exception e) {
			log.error("Exception in adding transaction "+e.getMessage()+" "+e);
		}
		
		log.debug(" Exit : public TransactionVO addInvoice(TransactionVO transactionVO,int societyID)");
		
		return transactionVO;
		
	}
	
	/*This method is used to get single transaction details*/
	public TransactionVO getTransctionDetails(int societyID,int transactionID){		
		TransactionVO transactionVO=new TransactionVO();
		SocietyVO societyVO=new SocietyVO();
		AccountHeadVO accountHeadVO=new AccountHeadVO();
		try {
			log.debug("Entry :public TransactionVO getTransctionDetails(int societyID,int transactionID)");
			
			transactionVO=transactionsDomain.getTransctionDetails(societyID, transactionID);			
			
			log.debug("Exit :public TransactionVO getTransctionDetails(int societyID,int transactionID)");
		} catch (Exception e) {
			log.error("Exception :public TransactionVO getTransctionDetails(int societyID,int transactionID)"+e );
		}
		return transactionVO;
		
	}
	
	
	public int deleteLedgerEntry(LedgerEntries ledgerEntry,String societyID) {
		int success=1;
		
		log.debug("Entry :public int deleteLedgerEntry(LedgerEntries ledgerEntry,String societyID)");
			
		success=transactionsDomain.deleteLedgerEntry(ledgerEntry, societyID);
		
		log.debug("Exit :public int deleteLedgerEntry(LedgerEntries ledgerEntry,String societyID)");
		return success;
	}

	
	
	


	/* Used for deleting transactions i.e. adding is_deleted flag */
	public TransactionVO deleteTransaction(TransactionVO transaction,int societyID,int subGroupID) {
		int success=0;
		log.debug("Entry : public int deleteTransaction(TransactionVO transaction,String societyID)");
		try {
			log.debug("Here tx ID is "+transaction.getTransactionID()+" "+transaction.getTransactionType()+"  "+transaction.getGroupID());
			
			
			transaction=transactionsDomain.deleteTransaction(transaction,societyID,subGroupID);
			
			
		} catch (Exception e) {
			log.error("Exception occured in deleteTransaction "+e);
		}
		
		log.debug("Exit : public int deleteTransaction(TransactionVO transaction,String societyID)");
		return transaction;
	}
	
	/* Used for updating audit flag of  transactions i.e. adding is_audited flag */
	public TransactionVO changeAuditedTransactionStatus(TransactionVO transaction,int societyID) {
		int success=0;
		log.debug("Entry : public int changeAuditedTransactionStatus(TransactionVO transaction,String societyID)");
		try {
			log.debug("Here tx ID is "+transaction.getTransactionID()+" "+transaction.getTransactionType());
			
			
			transaction=transactionsDomain.changeAuditedTransactionStatus(transaction,societyID);
			
			
		} catch (Exception e) {
			log.error("Exception occured in changeAuditedTransactionStatus "+e);
		}
		
		log.debug("Exit : public int changeAuditedTransactionStatus(TransactionVO transaction,String societyID)");
		return transaction;
	}
	
	/* Used for updating audit flag of  transactionList i.e. adding is_audited flag */
	public TransactionVO changeAuditedTransactionListStatus(TransactionVO transaction,int orgID) {
		int success=0;
		log.debug("Entry : public int changeAuditedTransactionListStatus(TransactionVO transaction,String societyID)");
		try {
			
		transaction=transactionsDomain.changeAuditedTransactionListStatus(transaction,orgID);
			
			
		} catch (Exception e) {
			log.error("Exception occured in changeAuditedTransactionListStatus "+e);
		}
		
		log.debug("Exit : public int changeAuditedTransactionListStatus(TransactionVO transaction,String societyID)");
		return transaction;
	}
	
	/* Used for deleting transactions i.e. adding is_deleted flag */
	public TransactionVO changeTransactionStatus(TransactionVO transaction,int societyID,int subGroupID) {
		int success=0;
		log.debug("Entry : public int changeTransactionStatus(TransactionVO transaction,String societyID)");
		try {
			log.debug("Here tx ID is "+transaction.getTransactionID()+" "+transaction.getTransactionType()+"  "+transaction.getGroupID());
			
			
			transaction=transactionsDomain.changeTransactionStatus(transaction,societyID,subGroupID);
			
			
		} catch (Exception e) {
			log.error("Exception occured in changeTransactionStatus "+e);
		}
		
		log.debug("Exit : public int changeTransactionStatus(TransactionVO transaction,String societyID)");
		return transaction;
	}
	
	
	/* Used for deleting transactions i.e. adding is_deleted flag */
	public int deleteJournalEntries(InvoiceVO invoiceVO) {
		int success=0;
		log.debug("Entry : public int deleteJournalEntries(InvoiceVO invoiceVO)");
		try {
			
			
			
			success = transactionsDomain.deleteJournalEntries(invoiceVO);
			
			log.debug("Exit : public int deleteJournalEntries(InvoiceVO invoiceVO)");	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug("Exception in public int deleteJournalEntries(InvoiceVO invoiceVO) "+e);
		}
		
		return success;
	}
	
	/* Used for updating report group  of transaction line item */
	public TransactionVO updateReportGroupJournalEntry(TransactionVO transactionVO) {
		int success=0;
		log.debug("Entry : public int updateReportGroupJournalEntry(TransactionVO transactionVO)");
		try {
						
			transactionVO = transactionsDomain.updateReportGroupJournalEntry(transactionVO);
						
			
			log.debug("Exit : public int updateReportGroupJournalEntry(TransactionVO transactionVO)");	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug("Exception in public int updateReportGroupJournalEntry(TransactionVO transactionVO) "+e);
		}
		
		return transactionVO;
	}
	
	
//	This method is used to get ledgerEntries for the given transaction
	public List getLedgersEntryList(TransactionVO transactionVO,String societyID){
		List ledgerEntryList=null;
		log.debug("Entry : public List getLedgersEntryList(TransactionVO transactionVO,String societyID)");
		
		ledgerEntryList=transactionsDomain.getLedgersEntryList(transactionVO, societyID);
		
		log.debug("Exit : public List getLedgersEntryList(TransactionVO transactionVO,String societyID)"+ledgerEntryList.size());
		return ledgerEntryList;
	}
	
	

	
		
	
	public List getTransctionList(int societyID,int ledgerID,String fromDate, String uptoDate,String type){
		List TransactionList=null;
		try {
			log.debug("Entry :public List getTransctionList(String societyID,int ledgerID,String fromDate, String uptoDate)");
			
			TransactionList=transactionsDomain.getTransctionList(societyID, ledgerID, fromDate, uptoDate, type);
			
			
			log.debug("Exit :public List getTransctionList(String societyID,int ledgerID,String fromDate, String uptoDate)"+TransactionList.size());
		} catch (Exception e) {
			log.error("Exception :public List getTransctionList(String societyID,int ledgerID,String fromDate, String uptoDate) "+e );
		}
		return TransactionList;
		
	}
	
	public List getQuickTransctionList(int societyID,int ledgerID,String fromDate, String uptoDate,String type){
		List TransactionList=null;
		try {
			log.debug("Entry :public List getQuickTransctionList(String societyID,int ledgerID,String fromDate, String uptoDate)");
			
			TransactionList=transactionsDomain.getQuickTransctionList(societyID, ledgerID, fromDate, uptoDate, type);
			
			
			log.debug("Exit :public List getQuickTransctionList(String societyID,int ledgerID,String fromDate, String uptoDate)"+TransactionList.size());
		} catch (Exception e) {
			log.error("Exception :public List getTransctionList(String societyID,int ledgerID,String fromDate, String uptoDate) "+e );
		}
		return TransactionList;
		
	}
	
	public List getReconcilationList(int societyID,int ledgerID,String fromDate, String uptoDate,String type){
		List TransactionList=null;
		try {
			log.debug("Entry :public List getReconcilationList(String societyID,int ledgerID,String fromDate, String uptoDate,,String type)");
			
			TransactionList=transactionsDomain.getReconcilationList(societyID, ledgerID, fromDate, uptoDate, type);
			
			
			log.debug("Exit :public List getReconcilationList(String societyID,int ledgerID,String fromDate, String uptoDate,,String type)"+TransactionList.size());
		} catch (Exception e) {
			log.error("Exception :public List getReconcilationList(String societyID,int ledgerID,String fromDate, String uptoDate,String type) "+e );
		}
		return TransactionList;
		
	}
	
	public List getTransactionListOfProjects(int societyID,int ledgerID,String fromDate, String uptoDate,String projectName){
		List TransactionList=null;
		try {
			log.debug("Entry :public List getTransactionListOfProjects(String societyID,int ledgerID,String fromDate, String uptoDate)");
			
			TransactionList=transactionsDomain.getTransactionListOfProjects(societyID, ledgerID, fromDate, uptoDate, projectName);
			
			
			log.debug("Exit :public List getTransactionListOfProjects(String societyID,int ledgerID,String fromDate, String uptoDate)"+TransactionList.size());
		} catch (Exception e) {
			log.error("Exception :public List getTransactionListOfProjects(String societyID,int ledgerID,String fromDate, String uptoDate) "+e );
		}
		return TransactionList;
		
	}
	
	public List getTransactionListOfCategoryWise(int societyID,int ledgerID,String fromDate, String uptoDate,List categoryList){
		List TransactionList=null;
		SocietyVO societyVO=new SocietyVO();
		AccountHeadVO accountHeadVO=new AccountHeadVO();
		try {
			log.debug("Entry :public List getTransactionListOfProjects(String societyID,int ledgerID,String fromDate, String uptoDate)");
			
			TransactionList=transactionsDomain.getTransactionListOfCategoryWise(societyID, ledgerID, fromDate, uptoDate, categoryList);
			
			
			log.debug("Exit :public List getTransactionListOfProjects(String societyID,int ledgerID,String fromDate, String uptoDate)"+TransactionList.size());
		} catch (Exception e) {
			log.error("Exception :public List getTransactionListOfProjects(String societyID,int ledgerID,String fromDate, String uptoDate) "+e );
		}
		return TransactionList;
		
	}
	

	public List getBillTransactionList(int societyID,int ledgerID,String fromDate, String uptoDate,String type){
		List TransactionList=null;
		try {
			log.debug("Entry :public List getBillTransactionList(String societyID,int ledgerID,String fromDate, String uptoDate)");
			
			TransactionList=transactionsDomain.getBillTransactionList(societyID, ledgerID, fromDate, uptoDate, type);
			
			
			log.debug("Exit :public List getBillTransactionList(String societyID,int ledgerID,String fromDate, String uptoDate)"+TransactionList.size());
		} catch (Exception e) {
			log.error("Exception :public List getBillTransactionList(String societyID,int ledgerID,String fromDate, String uptoDate) "+e );
		}
		return TransactionList;
		
	}
	
	
	
	
	
	public TransactionVO updateTrasactionBankDate(String bankDate,String txId, String societyID )
	{
		int sucess=0;
		TransactionVO txVO=new TransactionVO();
		try
		{
			
			log.debug("Entry : public int updateTrasactionBankDate(String bankDate,String txId )");
			
			
			txVO=transactionsDomain.updateTrasactionBankDate(bankDate,txId, societyID);
			
			
		}
		catch(Exception ex)
		{
			log.error("Exception in updateTrasactionBankDate(String bankDate,String txId ) : "+ex);
			
		}
		
		log.debug("Exit : public intupdateTrasactionBankDate(String bankDate,String txId )");
		return txVO;
	}
	
	//@Override
	

	/* Get invoice list for WebService */
	public JSONResponseVO getReceiptForMember(int societyID,int aptID,String fromDate,String uptoDate){
		JSONResponseVO jsonVO=new JSONResponseVO();
		AccountHeadVO achVo=new AccountHeadVO();
		List invoiceList=new ArrayList();
		log.debug("Entry : JsonRespAccountsVO getReceiptForMember(String societyID,String aptID,String fromDate,String uptoDate)");
		
		jsonVO=transactionsDomain.getReceiptForMember(societyID,  fromDate, uptoDate,aptID);
		
		log.debug("Exit : JsonRespAccountsVO getReceiptForMember(String societyID,String aptID,String fromDate,String uptoDate)");
		
		
		return jsonVO;
	}
	
	/* Get invoice list for WebService */
	public JSONResponseVO getReceiptForMemberApp(int societyID,int aptID){
		JSONResponseVO jsonVO=new JSONResponseVO();
		AccountHeadVO achVo=new AccountHeadVO();
		
		log.debug("Entry : public JSONResponseVO getReceiptForMemberApp(int societyID,int aptID)");
		
		jsonVO=transactionsDomain.getReceiptForMemberApp(societyID,aptID);
		
		log.debug("Exit : public JSONResponseVO getReceiptForMemberApp(int societyID,int aptID)");
		
		
		return jsonVO;
	}
	public TransactionDAO getAccTransactionDAO() {
		return accTransactionDAO;
	}


	
	public List getLatestTransactions(int societyID,int ledgerID, String txType){
		
		List transactionList=new ArrayList();
		try{
		log.debug("Entry : public List getLatestTransactions(String societyID,String ledgerID)");
		
		transactionList=transactionsDomain.getLatestTransactions(societyID, ledgerID, txType);
		
		
		log.debug("Exit : ppublic List getLatestTransactions(String societyID,String ledgerID)");
		}catch (Exception e) {
			log.error("Exception: public List getLatestTransactions(String societyID,String ledgerID) "+e );
		}
		return transactionList;
	}
	
	
	public int getLatestInvoiceNo(String societyiD){
		int invoiceNo=0;
		try
		{
			log.debug("Entry :public int getLatestInvoiceNo(String societyiD)");
					
				invoiceNo=transactionsDomain.getLatestInvoiceNo(societyiD);
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
			log.error("Exception in getCategoryHead : "+ex);
			
		}
			log.debug("Exit : public int getLatestInvoiceNo(String societyiD)");
		return invoiceNo;
		}
	
    public TransactionVO getReceiptDetails(int societyID,String ledgerID,String receiptID){
		
		TransactionVO transactionVO=new TransactionVO();
		try {
			log.debug("Entry : public TransactionVO getReceiptDetails(String societyID,String aptID,String receiptID)");

			transactionVO=transactionsDomain.getReceiptDetails(societyID, ledgerID, receiptID);
						
			log.debug("Exit : public TransactionVO getReceiptDetails(String societyID,String aptID,String receiptID)");
		} catch (Exception e) {
			log.error("Exception : public TransactionVO getReceiptDetails(String societyID,String aptID,String transactionID)"+e);
		}
		return transactionVO;
	}
    
 public TransactionVO getReceiptDetailsForMemberApp(int societyID,String ledgerID,String refNo){
		
		TransactionVO transactionVO=new TransactionVO();
		try {
			log.debug("Entry : public TransactionVO getReceiptDetailsForMemberApp(int societyID,String ledgerID,String refNo)");

			transactionVO=transactionsDomain.getReceiptDetailsForMemberApp(societyID, ledgerID, refNo);
						
			log.debug("Exit : public TransactionVO getReceiptDetailsForMemberApp(int societyID,String ledgerID,String refNo)");
		} catch (Exception e) {
			log.error("Exception : public TransactionVO getReceiptDetailsForMemberApp(int societyID,String ledgerID,String refNo)"+e);
		}
		return transactionVO;
	}
	
    public List getMemberReceiptList(int orgID,String fromDate,String toDate){
		List receiptList=null;	
		try {
			log.debug("Entry :public List getMemberReceiptList(int orgID,String fromDate,String toDate)");
						
			receiptList=accTransactionDAO.getMemberReceiptList(orgID, fromDate, toDate);
						
			log.debug("Exit : public List getMemberReceiptList(int orgID,String fromDate,String toDate)"+receiptList.size());
		} catch (Exception e) {
			log.error("Exception : getMemberReceiptList"+e);
		}
		return receiptList;
	}
   
	public int getAllTransactionsCount(TransactionVO txVO){
		int transactionCount=0;
		
		try {
			log.debug("Entry : public int getAllTransactionsCount(TransactionVO txVO)");
			
			transactionCount=transactionsDomain.getAllTransactionsCount( txVO);
			
			
			log.debug("Exit : public List getAllTransactionsCount(TransactionVO txVO)"+transactionCount);
		} catch (Exception e) {
			log.error("Exception : public int getAllTransactionsCount(TransactionVO txVO) "+e );
		}
		return transactionCount;
		
	}
    
	public List getAllTransactionsList(TransactionVO txVO){
		List TransactionList=null;
		
		try {
			log.debug("Entry : public List getAllTransactionsList(TransactionVO txVO)");
			
			TransactionList=transactionsDomain.getAllTransactionsList(txVO);
			
			
			log.debug("Exit : public List getAllTransactionsList(TransactionVO txVO)");
		} catch (Exception e) {
			log.error("Exception : public List getAllTransactionsList(TransactionVO txVO) "+e );
		}
		return TransactionList;
		
	}
	
	public List getAllNonRecordedTransactionsReportForLedger(TransactionVO txVO){
		List TransactionList=null;
		
		try {
			log.debug("Entry : public List getAllNonRecordedTransactionsReportForLedger(TransactionVO txVO)");
			
			TransactionList=transactionsDomain.getAllNonRecordedTransactionsReportForLedger( txVO);
			
			
			log.debug("Exit : public List getAllNonRecordedTransactionsReportForLedger(TransactionVO txVO)");
		} catch (Exception e) {
			log.error("Exception : public List getAllNonRecordedTransactionsReportForLedger(TransactionVO txVO) "+e );
		}
		return TransactionList;
		
	}
	
	
	public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID,String type){
		OnlinePaymentGatewayVO onlinePaymentGatewayVO=new OnlinePaymentGatewayVO();
		
		try {
			log.debug("Entry : public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID)");
			
			onlinePaymentGatewayVO=transactionsDomain.getOnlinePaymentDetails(societyID, type);
			
			
			log.debug("Exit : public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID)");
		} catch (Exception e) {
			log.error("Exception : public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID) "+e );
		}
		return onlinePaymentGatewayVO;
		
	}
	
	public List getOnlinePaymentDetailsList(int societyID){
		
		List OnlinePaymentGatewayList=null;
		try {
			log.debug("Entry : public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID)");
			
			OnlinePaymentGatewayList=transactionsDomain.getOnlinePaymentDetailsList(societyID);
			
			
			log.debug("Exit : public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID)");
		} catch (Exception e) {
			log.error("Exception : public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID) "+e );
		}
		return OnlinePaymentGatewayList;
		
	}
	
	public List generateScheduledTransactions(int societyID,int billingCycleID){
		List transactionList=new ArrayList();
		
		try {
			log.debug("Entry : public ResponseVO generateScheduledTransactions(int societyID,int billingCycleID)");
			
			transactionList=transactionsDomain.generateScheduledTransactions(societyID, billingCycleID);
			
			
			log.debug("Exit : public ResponseVO generateScheduledTransactions(int societyID,int billingCycleID)");
		} catch (Exception e) {
			log.error("Exception :public ResponseVO generateScheduledTransactions(int societyID,int billingCycleID)"+e );
		}
		return transactionList;
		
	}
	
	public List generateScheduledLateFeeTransactions(int societyID,int billingCycleID){
		ScheduledTransactionResponseVO responceObj=new ScheduledTransactionResponseVO();
		List transactionsList=new ArrayList();
		
		try {
			log.debug("Entry : public ResponseVO generateScheduledLateFeeTransactions(int societyID,int billingCycleID)");
			
			transactionsList=transactionsDomain.generateScheduledLateFeeTransactions(societyID, billingCycleID);
			
			
			log.debug("Exit : public ResponseVO generateScheduledLateFeeTransactions(int societyID,int billingCycleID)");
		} catch (Exception e) {
			log.error("Exception :public ResponseVO generateScheduledLateFeeTransactions(int societyID,int billingCycleID)"+e );
		}
		return transactionsList;
		
	}
	
	/*-- Add Bulk Journal entries---*/
	public ScheduledTransactionResponseVO addBulkTransactions(List transactionList,int societyID){
		log.debug("Entry : public ScheduledTransactionResponseVO addBulkTransactions(List transactionList,int societyID)");
		ScheduledTransactionResponseVO responseVO=new ScheduledTransactionResponseVO();
		
		responseVO=transactionsDomain.addBulkTransactions(transactionList, societyID);
		
		log.debug("Exit : public ScheduledTransactionResponseVO addBulkTransactions(List transactionList,int societyID)");
		return responseVO;
	}
	
	
	/* Get list of all active members of the given society  */
	public List getActiveMemberListAll(int societyID){
		List memberList=null;
		
		log.debug("Entry : public List getActiveMemberList(String societyID)");
		
		try{
		
		memberList=transactionsDomain.getActiveMemberListAll(societyID);
		
		log.debug("Entry : public List getActiveMemberList(String societyID)");
		
		}catch (Exception e) {
			log.error("Exception in getActiveMemberList "+e);
		}
		
		log.debug("Exit : public List getActiveMemberList(String societyID)"+memberList.size());
		return memberList;		
	}
	
	
	/* Get details of a charge  */
	public ChargesVO getChargeDetails(int chargeID,int societyID){
		ChargesVO chargeVO=new ChargesVO();
		
		log.debug("Entry : public List getChargeDetails(int chargeID,int societyID)");
		
		try{
		
		chargeVO=transactionsDomain.getChargeDetails(chargeID,societyID);
		
		
		
		}catch (Exception e) {
			log.error("Exception in public List getChargeDetails(int chargeID,int societyID) "+e);
		}
		
		log.debug("Exit : public List getChargeDetails(int chargeID,int societyID)");
		return chargeVO;		
	}
	
	/* Get details of a penalty charge  */
	public ChargesVO getPenaltyChargeDetails(int chargeID,int societyID){
		ChargesVO chargeVO=new ChargesVO();
		
		log.debug("Entry : public List getChargeDetails(int chargeID,int societyID)");
		
		try{
		
		chargeVO=transactionsDomain.getPenaltyChargeDetails(chargeID, societyID);
		
		
		
		}catch (Exception e) {
			log.error("Exception in public List getChargeDetails(int chargeID,int societyID) "+e);
		}
		
		log.debug("Exit : public List getChargeDetails(int chargeID,int societyID)");
		return chargeVO;		
	}
	
	/* Update the charge period after adding the invoice  */
	public int updateChargeStatus(int societyID,ChargesVO chargeVO){
		int insertFlag=0;
		log.debug("Entry : public int updateChargeStatus(int societyID,ChargesVO chargeVO)");
		
		insertFlag=transactionsDomain.updateChargeStatus( societyID, chargeVO);
		
		log.debug("Exit : public int updateChargeStatus(int societyID,ChargesVO chargeVO)");
		return insertFlag;		
	}
	
	/* Update the penalty charge period after adding the invoice  */
	public int updatePenaltyChargeStatus(int societyID,ChargesVO chargeVO){
		int insertFlag=0;
		log.debug("Entry : public int updatePenaltyChargeStatus(int societyID,ChargesVO chargeVO)");
		
		insertFlag=transactionsDomain.updatePenaltyChargeStatus( societyID, chargeVO);
		
		log.debug("Exit : public int updatePenaltyChargeStatus(int societyID,ChargesVO chargeVO)");
		return insertFlag;		
	}
	
	/* Update the Societys Late fee charging date  */
	public int updateLateFeeChargeDate(int societyID,ChargesVO chargeVO){
		int insertFlag=0;
		log.debug("Entry : public int updateLateFeeChargeDate(int societyID,ChargesVO chargeVO)"+chargeVO.getChargeID());
		try{
			
			insertFlag=transactionsDomain.updateLateFeeChargeDate(societyID, chargeVO);
		} catch (Exception e) {
		log.error("Exception at updateChargeStatus "+e);
		}
		
		log.debug("Exit : public int updateLateFeeChargeDate(int societyID,ChargesVO chargeVO)"+insertFlag);
		return insertFlag;		
	}
	
	/* Add raw bank statements after parsing    */
	public int addBankStatements(BankStatement bankStatementVO){
		int insertFlag=0;
		log.debug("Entry : public int addBankStatements(List bankStatementList)");
		try{
			
			insertFlag=transactionsDomain.addBankStatements(bankStatementVO);
		} catch (Exception e) {
		log.error("Exception at addBankStatements "+e);
		}
		
		log.debug("Exit : public int addBankStatements(List bankStatementList)"+insertFlag);
		return insertFlag;		
	}
	
	
	/* Add payUMoney statements after parsing   */
	public int addPayUStatements(OnlinePaymentGatewayVO payUVO){
		int insertFlag=0;
		log.debug("Entry : public int addPayUStatements(OnlinePaymentGatewayVO payUVO)");
		try{
			
			insertFlag=transactionsDomain.addPayUStatements(payUVO);
		} catch (Exception e) {
		log.error("Exception at addPayUStatements(OnlinePaymentGatewayVO payUVO) "+e);
		}
		
		log.debug("Exit : public int addPayUStatements(OnlinePaymentGatewayVO payUVO)"+insertFlag);
		return insertFlag;		
	}
	
	/* Add  bank transactions statements after parsing    */
	public int addBankTransactions(BankStatement bankStatementVO){
		int insertFlag=0;
		log.debug("Entry : public int addBankTransactions(List bankStatementList)");
		try{
			
			insertFlag=transactionsDomain.addBankTransactions(bankStatementVO);
		} catch (Exception e) {
		log.error("Exception at addBankTransactions "+e);
		}
		
		log.debug("Exit : public int addBankTransactions(List bankStatementList)"+insertFlag);
		return insertFlag;		
	}
	
	/* Add raw bank statements after parsing    */
	public int reoncileTransaction(BankStatement bankStatementVO){
		int insertFlag=0;
		log.debug("Entry : public int reoncileTransaction(BankStatement bankStatementVO)");
		try{
			
			insertFlag=transactionsDomain.reoncileTransaction(bankStatementVO);
		} catch (Exception e) {
		log.error("Exception at public int reoncileTransaction(BankStatement bankStatementVO) "+e);
		}
		
		log.debug("Exit : public int reoncileTransaction(BankStatement bankStatementVO)"+insertFlag);
		return insertFlag;		
	}
	
	public TxCounterVO getCountersForOrg(int orgID){
		TxCounterVO txCounterVO=new TxCounterVO();
		try
		{
			log.debug("Entry : public int getCountersForOrg(int orgID)");
			
			txCounterVO=transactionsDomain.getCountersForOrg(orgID);
			
		}catch(Exception ex)
		{
	
			log.error("Exception in public int getCountersForOrg(int orgID) : "+ex);
			
		}
			log.debug("Exit : public int getCountersForOrg(int orgID)");
		return txCounterVO;
		}
	
	public String getCounterType(String transactionType) {		
		String counterType="";
		log.debug("Entry :public String getCounterType(String transactionType)");
		try{
			
			counterType=transactionsDomain.getCounterType(transactionType);
			
		}catch(Exception ex){	
			log.error("Exception in getCounterType : "+ex);			
		}
		log.debug("Exit :public String getCounterType(String transactionType)");
		return counterType;
	}	
	
	public int incrementCounterNumber(int orgID,String counterType){
		int success=0;
		log.debug("Entry : public int incrementCounterNumber(int orgID,String counterType)");
		try{
			success=accTransactionDAO.incrementCounterNumber(orgID,counterType);
			
		}catch(Exception ex){	
			log.error("Exception in public int incrementCounterNumber(int orgID,String counterType) : "+ex);			
		}
			log.debug("Exit : public int incrementCounterNumber(int orgID,String counterType)");
		return success;
	}
	
	public int getLatestCounterNo(int orgID,String counterType){
		int success=0;
		log.debug("Entry : public int getLatestCounterNo(int orgID,String counterType)");
		try{
			success=transactionsDomain.getLatestCounterNo(orgID,counterType);
			
		}catch(Exception ex){	
			log.error("Exception in public int incrementCounterNumber(int orgID,String counterType) : "+ex);			
		}
			log.debug("Exit : public int getLatestCounterNo(int orgID,String counterType)");
		return success;
	}
	
	public List getChargeTypeList(int orgID){
		List chargeTypeList=null;
	
			log.debug("Entry : public List getChargeTypeList(int orgID)");
			
			chargeTypeList=transactionsDomain.getChargeTypeList(orgID);
			
			log.debug("Exit : public List getChargeTypeList(int orgID)");
		return chargeTypeList;
		}
	
	public List getApplicableCharges(int orgID,int chargeTypeID){
		List chargeTypeList=null;
	
			log.debug("Entry :  public List getApplicableCharges(int orgID,int chargeTypeID)");
			
			chargeTypeList=transactionsDomain.getApplicableCharges(orgID,chargeTypeID);
			
			log.debug("Exit :  public List getApplicableCharges(int orgID,int chargeTypeID)");
		return chargeTypeList;
		}
	
	public ChargesVO getConditionalBalance(ChargesVO chargeVO,int orgID,MemberVO memberVO){
		
			log.debug("Entry :  public ChargesVO getConditionalBalance(ChargesVO chargeVO,int orgID,MemberVO memberVO)");
			
			chargeVO=transactionsDomain.getConditionalBalance(chargeVO, orgID, memberVO);
			
			log.debug("Exit :  public ChargesVO getConditionalBalance(ChargesVO chargeVO,int orgID,MemberVO memberVO)");
		return chargeVO;
		}
	
	public List getApplicablePenaltyCharges(int orgID){
		List chargeTypeList=null;
	
			log.debug("Entry :  public List getApplicableCharges(int orgID)");
			
			chargeTypeList=transactionsDomain.getApplicablePenaltyCharges(orgID);
			
			log.debug("Exit :  public List getApplicableCharges(int orgID)");
		return chargeTypeList;
		}
	
	public List getInvoiceRceiptList(int orgID,int invoiceID){
		List invoiceReceiptList=null;
	
			log.debug("Entry :  public List getInvoiceRceiptList(int orgID,int invoiceID)");
			
			invoiceReceiptList=transactionsDomain.getInvoiceRceiptList(orgID,invoiceID);
			
			log.debug("Exit :  public List getInvoiceRceiptList(int orgID,int invoiceID)");
		return invoiceReceiptList;
		}
	
	/* Used for validation transaction */
	public TransactionVO validateTransaction(TransactionVO transactionVO,int societyID){
		log.debug("Entry :public TransactionVO validateTransaction(TransactionVO transactionVO,int societyID)");
		try{
			transactionVO=transactionsDomain.validateTransaction(transactionVO, societyID);
					
		}catch(Exception ex){	
			log.error("Exception in validateTransaction : "+ex);			
		}
			log.debug("Exit : public TransactionVO validateTransaction(TransactionVO transactionVO,int societyID)");
		return transactionVO;
	}
	
	public List getUnlinkedRceiptList(int orgID,int ledgerID,String fromDate, String toDate){
		List invoiceReceiptList=null;
	
			log.debug("Entry :  public List getUnlinkedRceiptList(int orgID,int ledgerID)");
			
			invoiceReceiptList=transactionsDomain.getUnlinkedRceiptList(orgID, ledgerID, fromDate, toDate);
			
			log.debug("Exit :  public List getUnlinkedRceiptList(int orgID,int ledgerID)");
		return invoiceReceiptList;
		}
	
	public int linkRceiptList(int orgID,List receiptList){
		int successCount=0;
	
			log.debug("Entry :  public int LinkRceiptList(int orgID,List receiptList)");
			
			successCount=transactionsDomain.linkRceiptList(orgID, receiptList);
			
		
			log.debug("Exit : public int LinkRceiptList(int orgID,List receiptList)");
		return successCount;
		}
	
	public int unlinkReceiptFromInvoice(int orgID ,TransactionVO txVO){
		int success=0;
		log.debug("Entry : public int unlinkReceiptFromInvoice(int orgID ,TransactionVO txVO)");
		  
			   success=accTransactionDAO.unlinkReceiptFromInvoice(orgID, txVO);			
					
		log.debug("Exit : public int unlinkReceiptFromInvoice(int orgID ,TransactionVO txVO)");
		return success;		
	}

	public List getPaymentReceiptListReport(int orgID,int ledgerID,String fromDate, String toDate,String type){
		List invoiceReceiptList=null;
	
			log.debug("Entry :  public List getPaymentReceiptListReport(int orgID,int ledgerID)");
			
			invoiceReceiptList=transactionsDomain.getPaymentReceiptListReport(orgID, ledgerID, fromDate, toDate,type);
			
			log.debug("Exit :  public List getPaymentReceiptListReport(int orgID,int ledgerID)");
		return invoiceReceiptList;
		}
	
	/* Transfer transactions from one organization to other org    */
	public int transferOrganizationsTransaction(int orgID,int newOrgID){
		int insertFlag=0;
		log.debug("Entry : public int transferOrganizationsTransaction(int orgID,int newOrgID)");
		try{
			
			insertFlag=transactionsDomain.transferOrganizationsTransaction(orgID,newOrgID);
		} catch (Exception e) {
		log.error("Exception at public int transferOrganizationsTransaction(int orgID,int newOrgID) "+e);
		}
		
		log.debug("Exit : public int transferOrganizationsTransaction(int orgID,int newOrgID)"+insertFlag);
		return insertFlag;		
	}
	
	
	public List getTransactionLinkedInvoiceList(int orgID,int transactionID){
		List invoiceReceiptList=null;
	
		log.debug("Entry :  public List getTransactionLinkedInvoiceList(int orgID,int transactionID)");
			
			invoiceReceiptList=transactionsDomain.getTransactionLinkedInvoiceList(orgID, transactionID);
									
		log.debug("Exit :  public List getTransactionLinkedInvoiceList(int orgID,int transactionID)");
		return invoiceReceiptList;
	}
	
	/*------------------Reallocate interest amount for ledger------------------*/
	public int reallocateInterestAmounts(TransactionVO txVO){
		int success=0;
		log.debug("Entry : public int reallocateInterestAmounts(TransactionVO txVO)");
		
		success=transactionsDomain.reallocateInterestAmounts(txVO);
		
		log.debug("Exit : public int reallocateInterestAmounts(TransactionVO txVO)");
		return success;
	}
	
	 /*------------------Reallocate interest amount for Organization------------------*/
	public int reallocateInterestAmountsForOrg(TransactionVO txVO){
		int success=0;
		log.debug("Entry : public int reallocateInterestAmountsForOrg(TransactionVO txVO)");
		
		success=transactionsDomain.reallocateInterestAmountsForOrg(txVO);
		
		log.debug("Exit : public int reallocateInterestAmountsForOrg(TransactionVO txVO)");
		return success;
	}
	
	//============ Member App related APIs =============//
	
	
	public List getAllNonRecordedTransactionsReportForMemberAppLedger(TransactionVO txVO){
		List TransactionList=null;
		
		try {
			log.debug("Entry : public List getAllNonRecordedTransactionsReportForMemberAppLedger(TransactionVO txVO)");
			
			TransactionList=transactionsDomain.getAllNonRecordedTransactionsReportForMemberAppLedger( txVO);
			
			
			log.debug("Exit : public List getAllNonRecordedTransactionsReportForMemberAppLedger(TransactionVO txVO)");
		} catch (Exception e) {
			log.error("Exception : public List getAllNonRecordedTransactionsReportForMemberAppLedger(TransactionVO txVO) "+e );
		}
		return TransactionList;
		
	}
	
	
	
	public void setAccTransactionDAO(TransactionDAO accTransactionDAO) {
		this.accTransactionDAO = accTransactionDAO;
	}


	public ReceiptInvoiceDomain getReceiptInvoiceDomain() {
		return receiptInvoiceDomain;
	}


	public void setReceiptInvoiceDomain(ReceiptInvoiceDomain receiptInvoiceDomain) {
		this.receiptInvoiceDomain = receiptInvoiceDomain;
	}


	

	public TransactionsDomain getTransactionsDomain() {
		return transactionsDomain;
	}


	public void setTransactionsDomain(TransactionsDomain transactionsDomain) {
		this.transactionsDomain = transactionsDomain;
	}


	
	

}
