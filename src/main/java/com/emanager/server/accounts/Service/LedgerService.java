package com.emanager.server.accounts.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;

import com.emanager.server.accounts.DAO.TransactionDAO;
import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.BankStatement;
import com.emanager.server.accounts.DataAccessObjects.LedgerEntries;
import com.emanager.server.accounts.DataAccessObjects.LedgerTrnsVO;
import com.emanager.server.accounts.Domain.LedgerDomain;
import com.emanager.server.accounts.Domain.ReceiptInvoiceDomain;
import com.emanager.server.accounts.Domain.TransactionsDomain;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.financialReports.valueObject.RptMonthYearVO;
//import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.emanager.server.invoice.dataAccessObject.CustomerVO;
import com.emanager.server.invoice.dataAccessObject.ProfileDetailsVO;

public class LedgerService {
	
	LedgerDomain ledgerDomain;
	ReceiptInvoiceDomain receiptInvoiceDomain;
	Logger log=Logger.getLogger(LedgerService.class);
	DateUtility dateUtil=new DateUtility();
	
	
//	This method is used to add new ledgers 
	public AccountHeadVO insertLedger(AccountHeadVO achVO,int societyID){
		int success=0;
		try {
			log.debug("Entry : public AccountHeadVO insertLedger(String societyID,AccountHeadVO achVO)");
			
			
			achVO=ledgerDomain.insertLedger(achVO,societyID);
			
			
			
			log.debug("Exit : public AccountHeadVO insertLedger(String societyID,AccountHeadVO achVO))");
		} catch (Exception e) {
			log.error("Exception : public AccountHeadVO insertLedger(String societyID,AccountHeadVO achVO)"+e);
		}
		return achVO;
	}
//	This method is used to update new ledgers 
	public AccountHeadVO updateLedger(AccountHeadVO ledgerVO,int societyID,int ledgerID){
		int success=0;
		try {
			log.debug("Entry : public AccountHeadVO updateLedger(String societyID,AccountHeadVO achVO)");
			
			
			ledgerVO=ledgerDomain.updateLedger(ledgerVO, societyID, ledgerID);	
			
			log.debug("Exit : public AccountHeadVO updateLedger(String societyID,AccountHeadVO achVO))");
		} catch (Exception e) {
			log.error("Exception : public AccountHeadVO updateLedger(String societyID,AccountHeadVO achVO)"+e);
		}
		return ledgerVO;
	}
	
	//	This method is used to Delete new ledgers 
	public AccountHeadVO deleteLedger(int societyID,int ledgerID){
		int success=0;
		AccountHeadVO ledgerVO=new AccountHeadVO();
		try {
			log.debug("Entry :public int deleteLedger(String societyID,int ledgerID))");

			ledgerVO=ledgerDomain.deleteLedger( societyID, ledgerID);	
			
			log.debug("Exit : public int deleteLedger(String societyID,int ledgerID)");
		} catch (Exception e) {
			log.error("Exception : public int deleteLedger(String societyID,int ledgerID)"+e);
		}
		return ledgerVO;
	}
	
	
	
	public AccountHeadVO getLedgerID(int userID,int societyID,String userType,String identifier ){
		AccountHeadVO achVO=new AccountHeadVO();
		try {
			log.debug("Entry : ppublic AccountHeadVO getLedgerID(int memberID)");
			
			
			achVO=ledgerDomain.getLedgerID(userID,societyID,userType,identifier);	
			
			log.debug("Exit : public getLedgerID(int memberID)");
		} catch (Exception e) {
			log.error("Exception : getLedgerID(int memberID)"+e);
		}
		return achVO;
	}
	
	
	public AccountHeadVO getLedgerDetails(int ledgerID,int societyID){
		AccountHeadVO achVO=new AccountHeadVO();
		try {
			log.debug("Entry : public AccountHeadVO getLedgerDetails(String ledgerID,String societyID)");
			
			
			achVO=ledgerDomain.getLedgerDetails(ledgerID, societyID);	
			
			log.debug("Exit : public AccountHeadVO getLedgerDetails(String ledgerID,String societyID)");
		} catch (Exception e) {
			log.error("Exception : public AccountHeadVO getLedgerDetails(String ledgerID,String societyID)"+e);
		}
		return achVO;
	}
	

	public List getAllGroupList(int rootID,int orgID){
		List ledgerList=null;
		try {
			log.debug("Entry : public getAllGroupList(int rootID,int orgID)");

			ledgerList=ledgerDomain.getAllGroupList(rootID,orgID);	
			
			log.debug("Exit : public List getAllGroupList(int rootID,int orgID)");
		} catch (Exception e) {
			log.error("Exception : public List getAllGroupList(int rootID,int orgID)"+e);
		}
		return ledgerList;
	}
	
	public List getAllSubCategoryList(int parentGroupID){
		List categoryList=null;
		try {
			log.debug("Entry : public List getAllSubCategoryList(int parentGroupID)");

			categoryList=ledgerDomain.getAllSubCategoryList(parentGroupID);	
			
			log.debug("Exit : public List getAllSubCategoryList(int parentGroupID)"+categoryList.size());
		} catch (Exception e) {
			log.error("Exception :public List getAllSubCategoryList(int parentGroupID)"+e);
		}
		return categoryList;
	}
	
	
	
	
	public AccountHeadVO getOpeningClosingBalance(int societyID,int ledgerID,String fromDate,String uptoDate,String type) throws ParseException{
		AccountHeadVO acHVO=new AccountHeadVO();
		
		String normalBal="";
		try {
			log.debug("Entry :public AccountHeadVO getOpeningBalance(String societyID,int ledgerID,String fromDate, String uptoDate)");
			
			acHVO=ledgerDomain.getOpeningClosingBalance(societyID, ledgerID, fromDate, uptoDate, type);
			
			log.debug("Exit :public AccountHeadVO getOpeningBalance(String societyID,int ledgerID,String fromDate, String uptoDate)"+acHVO.getSum());
		} catch (NullPointerException ne) {
					log.debug("Exception : public AccountHeadVO getOpeningBalance(String societyID,int ledgerID,String fromDate, String uptoDate) "+ne );
				
		} catch (Exception e) {
			log.error("Exception : public AccountHeadVO getOpeningBalance(String societyID,int ledgerID,String fromDate, String uptoDate) "+e );
		}
		return acHVO;
		
	}
	
//	This method is used to give reverse effects to ledger balances while doing delete transaction
	public List getLedgerList(int societyID,int subGroupID,String type){
		List ledgerList=null;
		try {
			log.debug("Entry : public List getLedgersList(String societyID,String ledgerType)");

			ledgerList=ledgerDomain.getLedgerList(societyID, subGroupID,type);
									
			log.debug("Exit : public List getLedgersList(String societyID,String ledgerType)"+ledgerList.size());
		} catch (Exception e) {
			log.error("Exception : public List getLedgersList(String societyID,String ledgerType)"+e);
		}
		return ledgerList;
	}
	
	public List getLedgerListWithClosingBalance(int societyID,int subGroupID,String type,String fromDate,String toDate){
		List ledgerList=null;
		try {
			log.debug("Entry : public List getLedgersList(String societyID,String ledgerType)");

			ledgerList=ledgerDomain.getLedgersListWithClosingBal(societyID, subGroupID, type, fromDate, toDate);
									
			log.debug("Exit : public List getLedgersList(String societyID,String ledgerType)"+ledgerList.size());
		} catch (Exception e) {
			log.error("Exception : public List getLedgersList(String societyID,String ledgerType)"+e);
		}
		return ledgerList;
	}
	
	//This method is used to give reverse effects to ledger balances while doing delete transaction
	public List getLedgerList(int societyID,int subGroupID){
		List ledgerList=null;
		try {
			log.debug("Entry : public List getLedgersList(String societyID,String ledgerType)");

			ledgerList=ledgerDomain.getLedgerList(societyID, subGroupID,"G");		
						
			log.debug("Exit : public List getLedgersList(String societyID,String ledgerType)"+ledgerList.size());
		} catch (Exception e) {
			log.error("Exception : public List getLedgersList(String societyID,String ledgerType)"+e);
		}
		return ledgerList;
	}
	
     //	This method is used to get master ledger list
	public List getMasterLedgerList(int societyID,int subGroupID,int categoryID){
		List ledgerList=null;
		try {
			log.debug("Entry : public List getMasterLedgerList(String societyID,String ledgerType)");

			if(categoryID!=0){
				ledgerList=ledgerDomain.getMasterLedgerList(societyID, categoryID, "C");
			}else{
			ledgerList=ledgerDomain.getMasterLedgerList(societyID, subGroupID,"G");	
			}			
									
			log.debug("Exit : public List getMasterLedgerList(String societyID,String ledgerType)"+ledgerList.size());
		} catch (Exception e) {
			log.error("Exception : public List getMasterLedgerList(String societyID,String ledgerType)"+e);
		}
		return ledgerList;
	}
	
    //	This method is used to get master ledger list
	public List getMasterLedgersListRootWise(int societyID,int rootID){
		List ledgerList=null;
		try {
			log.debug("Entry : public List getMasterLedgersListRootWise(String societyID,String ledgerType)");

			ledgerList=ledgerDomain.getMasterLedgerList(societyID, rootID,"R");	
									
			log.debug("Exit : public List getMasterLedgersListRootWise(String societyID,String ledgerType)"+ledgerList.size());
		} catch (Exception e) {
			log.error("Exception : public List getMasterLedgersListRootWise(String societyID,String ledgerType)"+e);
		}
		return ledgerList;
	}
	
	/* Used to get list of ledgers which are noted with some properties*/
	public List getLedgersListByProperties(int societyID,String properties)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public List getLedgersListByProperties(int societyID,String properties)");
		List ledgerList=null;
	try{
			 
			   ledgerList=ledgerDomain.getLedgersListByProperties(societyID, properties);
			   
			   log.debug("Exit :  public List getLedgersListByProperties(int societyID,String properties)"+ledgerList.size());
	     }catch(Exception se){
				log.error("Exception : public List getLedgersListByProperties(int societyID,String properties) "+se);
			   
			}
		return ledgerList;
	}
	
//	This method is used to give reverse effects to ledger balances while doing delete transaction
	public List getLedgerListForTDSCalculation(int societyID,int subGroupID){
		List ledgerList=null;
		try {
			log.debug("Entry : public List getLedgerListForTDSCalculation(int societyID,int subGroupID)");

			ledgerList=ledgerDomain.getLedgerListForTDSCalculation(societyID, subGroupID,"G");
			
			
						
			log.debug("Exit : public List getLedgerListForTDSCalculation(int societyID,int subGroupID)"+ledgerList.size());
		} catch (Exception e) {
			log.error("Exception : public List getLedgerListForTDSCalculation(int societyID,int subGroupID)"+e);
		}
		return ledgerList;
	}
	
	
	public AccountHeadVO getBalancesForTrialBal(AccountHeadVO achVO){
		log.debug("Entry : public AccountHeadVO getBalancesForTrialBal(AccountHeadVO achVO)");
		AccountHeadVO achsVO=new AccountHeadVO();
		
		achsVO=ledgerDomain.getBalancesForTrialBal(achVO);
		
		
		log.debug("Exit : public AccountHeadVO getBalancesForTrialBal(AccountHeadVO achVO)");
		return achsVO;
	}
	
	public AccountHeadVO unsignedOpeningClosingBalance(AccountHeadVO achVO){
		log.debug("Entry : public AccountHeadVO unsignedOpeningClosingBalance(AccountHeadVO achVO)");
		AccountHeadVO achsVO=new AccountHeadVO();
		
		achsVO=ledgerDomain.unsignedOpeningClosingBalance(achVO);
		
		
		log.debug("Exit : public AccountHeadVO unsignedOpeningClosingBalance(AccountHeadVO achVO)");
		return achsVO;
	}
	
	public AccountHeadVO getUpdatedBalances(LedgerEntries ledgerEntry,int societyID ){
		String updatedBalance="";
		BigDecimal balance=BigDecimal.ZERO;
		AccountHeadVO ledgerVO=new AccountHeadVO();
		try {
		log.debug("Entry : BigDecimal getUpdatedBalances(LedgerEntries ledgerEntry,String societyID )");
		BigDecimal amount=ledgerEntry.getAmount().abs();

		
			
			ledgerVO=ledgerDomain.getUpdatedBalances(ledgerEntry, societyID);
			
			
		} catch (Exception e) {
			log.error("Exception : getUpdatedBalances"+e);
		}
		
		log.debug("Exit : BigDecimal getUpdatedBalances(LedgerEntries ledgerEntry,String societyID )");
		return ledgerVO;
		
	}
//	This method is used to profit and loss ledger
	public AccountHeadVO getProfitLossLedger(int societyID){
		List ledgerList=null;
		AccountHeadVO profitLossVO=new AccountHeadVO();
		try {
			log.debug("Entry : public List getProfitLossLedger(String societyID,int gruoupOrRootID,String type)");
				
				 
			profitLossVO=ledgerDomain.getProfitLossLedger(societyID);
			
			
						
			log.debug("Exit : public List getProfitLossLedger(String societyID,String ledgerType)");
		} catch (Exception e) {
			log.error("Exception : public List getProfitLossLedger(String societyID,String ledgerType)"+e);
		}
		return profitLossVO;
	}
	
	public AccountHeadVO getProffitLossBalance(int societyID,String fromDate,String uptoDate,String bsOrTb,int isConsolidated,String formatType){
		AccountHeadVO proffitLossVO=new AccountHeadVO();
		log.debug("Entry  : private AccountHeadVO getProffitLossBalance(String societyID,String fromDate,String uptoDate,String formatType)");
		try{
		
		
		proffitLossVO=ledgerDomain.getProffitLossBalance(societyID, fromDate, uptoDate,bsOrTb,isConsolidated,formatType);
		
		}catch(Exception e){
			log.error("Exception occurred in getProffitLossBalance "+e);
		}
		log.debug("Exit  : private AccountHeadVO getProffitLossBalance(String societyID,String fromDate,String uptoDate,String formatType)");
		return proffitLossVO;
	}
	
	
	public AccountHeadVO getConsolidatedProffitLossBalance(int societyID,String fromDate,String uptoDate,String bsOrTb,String formatType){
		AccountHeadVO proffitLossVO=new AccountHeadVO();
		log.debug("Entry  : private AccountHeadVO getConsolidatedProffitLossBalance(String societyID,String fromDate,String uptoDate,String formatType)");
		try{
		
		
		proffitLossVO=ledgerDomain.getConsolidatedProffitLossBalance(societyID, fromDate, uptoDate,bsOrTb,formatType);
		
		}catch(Exception e){
			log.error("Exception occurred in getConsolidatedProffitLossBalance "+e);
		}
		log.debug("Exit  : private AccountHeadVO getConsolidatedProffitLossBalance(String societyID,String fromDate,String uptoDate,String formatType)");
		return proffitLossVO;
	}
	
	public List getAccountLedgerList(int societyID,int subGroupID){
		List ledgerList=null;
		try {
			log.debug("Entry : public List getAccountLedgerList(String societyID,String ledgerType)");

			ledgerList=getLedgerList(societyID,subGroupID);
			
		
			
						
			log.debug("Exit : public List getAccountLedgerList(String societyID,String ledgerType)"+ledgerList.size());
		} catch (Exception e) {
			log.error("Exception : public List getAccountLedgerList(String societyID,String ledgerType)"+e);
		}
		return ledgerList;
	}
	
	public LedgerTrnsVO getLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate,String type){
		LedgerTrnsVO ledgerTransactionVO=null;
		log.debug("Entry : public LedgerTrnsVO getLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate)");
		
		try {
			ledgerTransactionVO = ledgerDomain.getLedgerTransactionList(orgID,ledgerID, fromDate, toDate,type);
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getLedgerTransactionList "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate)");
		return ledgerTransactionVO;
	}
	
	
	public LedgerTrnsVO getQuickLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate,String type){
		LedgerTrnsVO ledgerTransactionVO=null;
		log.debug("Entry : public LedgerTrnsVO getQuickLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate)");
		
		try {
			ledgerTransactionVO = ledgerDomain.getQuickLedgerTransactionList(orgID,ledgerID, fromDate, toDate,type);
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getQuickLedgerTransactionList "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getQuickLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate)");
		return ledgerTransactionVO;
	}
	
	public LedgerTrnsVO getReconcilationList(int orgID,int ledgerID,String fromDate,String toDate,String type){
		LedgerTrnsVO ledgerTransactionVO=null;
		log.debug("Entry : public LedgerTrnsVO getReconcilationList(int orgID,int ledgerID,String fromDate,String toDate)");
		
		try {
			ledgerTransactionVO = ledgerDomain.getReconcilationList(orgID, ledgerID, fromDate, toDate,type);
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getLedgerTransactionList "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getReconcilationList(int orgID,String fromDate,String toDate)");
		return ledgerTransactionVO;
	}
	
	public LedgerTrnsVO getTransactionListOfProjects(int orgID,int ledgerID,String fromDate,String toDate,String projectName){
		LedgerTrnsVO ledgerTransactionVO=null;
		log.debug("Entry : public LedgerTrnsVO getTransactionListOfProjects(int orgID,int ledgerID,String fromDate,String toDate)");
		
		try {
			ledgerTransactionVO = ledgerDomain.getTransactionListOfProjects(orgID,ledgerID, fromDate, toDate,projectName);
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getTransactionListOfProjects "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getTransactionListOfProjects(int orgID,int ledgerID,String fromDate,String toDate)");
		return ledgerTransactionVO;
	}
	
	
	public LedgerTrnsVO getTransactionListOfProjectsWithCategory(int orgID,int ledgerID,String fromDate,String toDate,int categoryID){
		LedgerTrnsVO ledgerTransactionVO=null;
		log.debug("Entry : public LedgerTrnsVO getTransactionListOfProjectsWithCategory(int orgID,int ledgerID,String fromDate,String toDate)");
		
		try {
			ledgerTransactionVO = ledgerDomain.getTransactionListOfProjectsWithCategory(orgID,ledgerID, fromDate, toDate,categoryID);
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getTransactionListOfProjectsWithCategory "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getTransactionListOfProjectsWithCategory(int orgID,int ledgerID,String fromDate,String toDate)");
		return ledgerTransactionVO;
	}
	public LedgerTrnsVO getProfitLossLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate,String type){
		LedgerTrnsVO ledgerTransactionVO=null;
		log.debug("Entry : public LedgerTrnsVO getProfitLossLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate)");
		
		try {
			ledgerTransactionVO = ledgerDomain.getProfitLossLedgerTransactionList(orgID,ledgerID, fromDate, toDate,type);
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getProfitLossLedgerTransactionList "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getProfitLossLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate)");
		return ledgerTransactionVO;
	}
	
	public LedgerTrnsVO getLedgerTransactionListSubAccounts(int orgID,int ledgerID,String fromDate,String toDate,String type){
		LedgerTrnsVO ledgerTransactionVO=null;
		log.debug("Entry : public LedgerTrnsVO getLedgerTransactionListSubAccounts(int orgID,int ledgerID,String fromDate,String toDate)");
		
		try {
			ledgerTransactionVO = ledgerDomain.getLedgerTransactionListSubAccounts(orgID,ledgerID, fromDate, toDate,type);
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getLedgerTransactionListSubAccounts "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getLedgerTransactionListSubAccounts(int orgID,int ledgerID,String fromDate,String toDate)");
		return ledgerTransactionVO;
	}
	
	public LedgerTrnsVO getBillTransactionList(int orgID,int ledgerID,String fromDate,String toDate,String type){
		LedgerTrnsVO ledgerTransactionVO=null;
		log.debug("Entry : public LedgerTrnsVO getBillTransactionList(int orgID,int ledgerID,String fromDate,String toDate)");
		
		try {
			ledgerTransactionVO = ledgerDomain.getBillTransactionList(orgID,ledgerID, fromDate, toDate,type);
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getBillTransactionList "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate)");
		return ledgerTransactionVO;
	}
	
	public AccountHeadVO getGroupBalance(int orgID,int groupID,String fromDate,String toDate){
		AccountHeadVO achVO=new AccountHeadVO();
		log.debug("Entry : public AccountHeadVO getGroupBalance(int orgID,int groupID,String fromDate,String toDate)");
		
		try {
			achVO = ledgerDomain.getGroupBalance(orgID,groupID, fromDate, toDate);
		} catch (Exception e) {
			log.error("Exception at public AccountHeadVO getGroupBalance "+e);
		}		
		log.debug("Exit : public AccountHeadVO getGroupBalance(int orgID,int groupID,String fromDate,String toDate)");
		return achVO;
	}
	
	public AccountHeadVO getLedgerByType(int orgID,String type){
		AccountHeadVO achVO=new AccountHeadVO();
		log.debug("Entry : public AccountHeadVO getDiscountLedger(int orgID)");
		
		try {
			achVO = ledgerDomain.getLedgerByType(orgID,type);
		} catch (Exception e) {
			log.error("Exception at public AccountHeadVO getDiscountLedger "+e);
		}		
		log.debug("Exit : public AccountHeadVO getDiscountLedger(int orgID)");
		return achVO;
	}
	
	public AccountHeadVO getBankCashPrimaryLedger(int orgID,String ledgerType){
		AccountHeadVO achVO=new AccountHeadVO();
		log.debug("Entry : public AccountHeadVO getBankCashPrimaryLedger(int orgID)");
		
		try {
			achVO = ledgerDomain.getBankCashPrimaryLedger(orgID,ledgerType);
		} catch (Exception e) {
			log.error("Exception at public AccountHeadVO getBankCashPrimaryLedger "+e);
		}		
		log.debug("Exit : public AccountHeadVO getBankCashPrimaryLedger(int orgID)");
		return achVO;
	}
	
	public AccountHeadVO getPaymentGatewayLedger(int orgID){
		AccountHeadVO achVO=new AccountHeadVO();
		log.debug("Entry : public AccountHeadVO getPaymentGatewayLedger(int orgID)");
		
		try {
			achVO = ledgerDomain.getPaymentGatewayLedger(orgID);
		} catch (Exception e) {
			log.error("Exception at public AccountHeadVO getPaymentGatewayLedger "+e);
		}		
		log.debug("Exit : public AccountHeadVO getPaymentGatewayLedger(int orgID)");
		return achVO;
	}
	
	
	public List getLedgerProfileList(int societyID,String type){
		List ledgerList=null;
		try {
			log.debug("Entry : public List getLedgerProfileList(int societyID,String type)");

			ledgerList=ledgerDomain.getLedgerProfileList(societyID,type);		
						
			log.debug("Exit : public List getLedgerProfileList(int societyID,String type)"+ledgerList.size());
		} catch (Exception e) {
			log.error("Exception : public List getLedgerProfileList(int societyID,String type)"+e);
		}
		return ledgerList;
	}
	
	public List getCurrentNonCurrentLedgerProfileList(int societyID,String type){
		List ledgerList=null;
		try {
			log.debug("Entry : public List getCurrentNonCurrentLedgerProfileList(int societyID,String type)");

			ledgerList=ledgerDomain.getCurrentNonCurrentLedgerProfileList(societyID,type);		
						
			log.debug("Exit : public List getCurrentNonCurrentLedgerProfileList(int societyID,String type)"+ledgerList.size());
		} catch (Exception e) {
			log.error("Exception : public List getCurrentNonCurrentLedgerProfileList(int societyID,String type)"+e);
		}
		return ledgerList;
	}
	
	
	public List getLedgerProfilesList(int orgID,String type,String tag){
		List ledgerList=null;
		try {
			log.debug("Entry : public List getLedgerProfilesList(int societyID,String type,String tag)");

			ledgerList=ledgerDomain.getLedgerProfilesList(orgID, type, tag);		
						
			log.debug("Exit : public List getLedgerProfilesList(int societyID,String type,String tag)"+ledgerList.size());
		} catch (Exception e) {
			log.error("Exception : public List getLedgerProfilesList(int societyID,String type,String tag)"+e);
		}
		return ledgerList;
	}
	
	public AccountHeadVO addLedgerWithProfiles(AccountHeadVO accountHeadVO){
		AccountHeadVO ledgerVO=new AccountHeadVO();
		try {
			log.debug("Entry : public List addLedgerWithProfiles((AccountHeadVO accountHeadVO))");

			ledgerVO=ledgerDomain.addLedgerWithProfiles(accountHeadVO);
						
			log.debug("Exit : public List addLedgerWithProfiles(AccountHeadVO accountHeadVO)");
		} catch (Exception e) {
			log.error("Exception : public List addLedgerWithProfiles(AccountHeadVO accountHeadVO)"+e);
		}
		return ledgerVO;
	}
	

	public int updateLedgerProfileDetails(AccountHeadVO achVO) {

		int sucess = 0;  
		int successAdress=0;
		try{
		
		    log.debug("Entry : public int updateLedgerProfileDetails(AccountHeadVO achVO)");
		
		    sucess = ledgerDomain.updateLedgerProfileDetails(achVO);
		    
		   					
		    log.debug("Exit : public int updateLedgerProfileDetails(AccountHeadVO achVO)");

		}catch(Exception Ex){
			
		
			log.error("Exception in public int updateLedgerProfileDetails(AccountHeadVO achVO) : "+Ex);
			
		}
		return sucess;
	}
	
	public int deleteLedgerProfile(int ledgerID,int orgID){
		
		int sucess = 0;
		try{
		
		log.debug("Entry : public int deleteLedgerProfile(int ledgerID,int orgID)");
		
		sucess = ledgerDomain.deleteLedgerProfile( ledgerID,orgID);
				
		log.debug("Exit : public int deleteLedgerProfile(int ledgerID,int orgID)");

		}catch(Exception Ex){		
			log.error("Exception in public int deleteLedgerProfile(int ledgerID,int orgID) : "+Ex);			
		}
		return sucess;
	}

	public ProfileDetailsVO getLedgerProfile(int ledgerID,int orgID){
		ProfileDetailsVO profileVO=new ProfileDetailsVO();
		try {
			log.debug("Entry : public ProfileDetailsVO getLedgerProfile((int ledgerID,int orgID))");

			profileVO=ledgerDomain.getLedgerProfile(ledgerID,orgID);
						
			log.debug("Exit : public List getLedgerProfile(int ledgerID,int orgID)");
		} catch (Exception e) {
			log.error("Exception : public ProfileDetailsVO getLedgerProfile(int ledgerID,int orgID)"+e);
		}
		return profileVO;
	}
	
	public ProfileDetailsVO getLedgerProfileDetails(int profileID){
		ProfileDetailsVO profileVO=new ProfileDetailsVO();
		try {
			log.debug("Entry : public ProfileDetailsVO getLedgerProfileDetails(int profileID)");

			profileVO=ledgerDomain.getLedgerProfileDetails(profileID);				
						
			log.debug("Exit : public ProfileDetailsVO getLedgerProfileDetails(int profileID)");
		} catch (Exception e) {
			log.error("Exception : public ProfileDetailsVO getLedgerProfileDetails(int profileID)"+e);
		}
		return profileVO;
	}
	
	
	public BankStatement getLedgersDetailsForImport(BankStatement bankStmt){
		try {
			log.debug("Entry : public BankStatement getLedgersDetailsForImport(BankStatement bankStmt)");

			bankStmt=ledgerDomain.getLedgersDetailsForImport(bankStmt);
						
			log.debug("Exit : public BankStatement getLedgersDetailsForImport(BankStatement bankStmt)");
		} catch (Exception e) {
			log.error("Exception : public BankStatement getLedgersDetailsForImport(BankStatement bankStmt)"+e);
		}
		return bankStmt;
	}
	
	/* Used to get groups of that org */
	public List getGroupListByCategory(int categoryID,int orgID,int rootID)  {
	// TODO Auto-generated method stub
	log.debug("Entry : public List getGroupListByCategory(int rootID,int orgID)");
	List groupList=null;
	
	try{
		 
		   groupList=ledgerDomain.getGroupListByCategory(categoryID, orgID, rootID);
		   log.debug("Exit : public List getGroupListByCategory)"+groupList.size());
		}catch(Exception se){
			log.error("Exception : public List getGroupListByCategory(String societyID) "+se);
		   
		}
	return groupList;
}
	
	public List getCategoryList(int orgID){
		List categoryList=null;
		try {
			log.debug("Entry : public getCategoryList(int orgID)");

			categoryList=ledgerDomain.getCategoryList(orgID);	
			
			log.debug("Exit : public List getCategoryList(int orgID)");
		} catch (Exception e) {
			log.error("Exception : public List getCategoryList(int orgID)"+e);
		}
		return categoryList;
	}
	
	//	This method is used to add new groups 
	public AccountHeadVO insertGroup(AccountHeadVO achVO,int orgID){
		int success=0;
		try {
			log.debug("Entry : public AccountHeadVO insertGroup(AccountHeadVO achVO,int orgID)");
			
			
			achVO=ledgerDomain.insertGroup(achVO,orgID);
			
			
			
			log.debug("Exit : public AccountHeadVO insertGroup(AccountHeadVO achVO,int orgID)");
		} catch (Exception e) {
			log.error("Exception : public AccountHeadVO insertGroup(AccountHeadVO achVO,int orgID)"+e);
		}
		return achVO;
	}
//	This method is used to update groups 
	public AccountHeadVO updateGroup(AccountHeadVO ledgerVO,int groupID){
		int success=0;
		try {
			log.debug("Entry : public AccountHeadVO updateGroup(AccountHeadVO ledgerVO,int ledgerID)");
			
			
			ledgerVO=ledgerDomain.updateGroup(ledgerVO, groupID);	
			
			log.debug("Exit : public AccountHeadVO updateGroup(AccountHeadVO ledgerVO,int ledgerID)");
		} catch (Exception e) {
			log.error("Exception : public AccountHeadVO updateGroup(AccountHeadVO ledgerVO,int ledgerID)"+e);
		}
		return ledgerVO;
	}
	
	//	This method is used to Delete groups 
	public AccountHeadVO deleteGroup(int orgID,int groupID){
		int success=0;
		AccountHeadVO ledgerVO=new AccountHeadVO();
		try {
			log.debug("Entry : public AccountHeadVO deleteGroup(int orgID,int groupID)");

			ledgerVO=ledgerDomain.deleteGroup( orgID, groupID);	
			
			log.debug("Exit : public AccountHeadVO deleteGroup(int orgID,int groupID)");
		} catch (Exception e) {
			log.error("Exception : public AccountHeadVO deleteGroup(int orgID,int groupID)"+e);
		}
		return ledgerVO;
	}
	
	public LedgerDomain getLedgerDomain() {
		return ledgerDomain;
	}
	public void setLedgerDomain(LedgerDomain ledgerDomain) {
		this.ledgerDomain = ledgerDomain;
	}
	
	
	
}
