package com.emanager.server.accounts.Domain;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.emanager.server.accounts.DAO.LedgerDAO;
import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.BankStatement;
import com.emanager.server.accounts.DataAccessObjects.LedgerEntries;
import com.emanager.server.accounts.DataAccessObjects.LedgerTrnsVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.services.AddressService;
import com.emanager.server.financialReports.domainObject.AmountInWords;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.financialReports.services.TrialBalanceService;
import com.emanager.server.financialReports.valueObject.ReportDetailsVO;
import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.financialReports.valueObject.RptMonthYearVO;
import com.emanager.server.invoice.Services.CustomerService;
import com.emanager.server.invoice.dataAccessObject.CustomerVO;
import com.emanager.server.invoice.dataAccessObject.ProfileDetailsVO;
import com.emanager.server.projects.service.ProjectDetailsService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.taxation.DAO.TdsDAO;
import com.emanager.server.taxation.dataAccessObject.TDSDetailsVO;
import com.emanager.server.vendorManagement.service.VendorService;
import com.emanager.server.vendorManagement.valueObject.VendorVO;

public class LedgerDomain {
	Logger log=Logger.getLogger(LedgerDomain.class);
	AmountInWords amtInWrds=new AmountInWords();
	DateUtility dateUtil=new DateUtility();
	LedgerDAO ledgerDAO;
	ReportService reportService;
	TransactionService transactionService;
	TrialBalanceService trialBalanceService;
	AddressService addressServiceBean;
	CustomerService customerServiceBean;
	VendorService vendorServiceBean;
	SocietyService societyServiceBean;
	ProjectDetailsService projectDetailsService;
	
	
//	This method is used to add new ledgers .
	public AccountHeadVO insertLedger(AccountHeadVO achVO,int societyID){
		int success=0;
		Boolean isDuplicate=false;
		List ledgerList=new ArrayList();
		try {
			log.debug("Entry : public AccountHeadVO insertLedger(int societyID,AccountHeadVO achVO)");
			java.util.Date today = new java.util.Date();
			   java.sql.Date currentDate=new java.sql.Date(today.getTime());
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());

			 AccountHeadVO ledgerValidateVO=new AccountHeadVO();
			 if(achVO.getOpeningBalance().compareTo(BigDecimal.ZERO)!=0){
				 ledgerValidateVO=validateLedgersForInsert(achVO, societyID);
			 }else
			 ledgerValidateVO.setStatusCode(200);
			 if(ledgerValidateVO.getStatusCode()==200){
			 
			achVO.setOpeningBalance(convertToSignedAmt(achVO, societyID));
			achVO.setCreateDate(""+currentDate);
			achVO.setUpdateDate(""+currentTimestamp);
			
			ledgerList=ledgerDAO.checkLedgerDetails(achVO, societyID);
			 if(ledgerList.size()>0){
				   isDuplicate=true;
				   achVO.setStatusCode(534);
					achVO.setMessage("Ledger "+achVO.getStrAccountHeadName()+" already exist in database. Please select other ledger name");
					return achVO;
			 }
			if(!isDuplicate){
			
			success=ledgerDAO.insertLedger(achVO,societyID);
			if(success>0){
				achVO.setStatusCode(200);
				achVO.setLedgerID(success);
				achVO.setMessage("Ledger added successfully.");
			}else{
				achVO.setStatusCode(400);
				
				achVO.setMessage("Problem in adding Ledger.");
			}
			
			}
			 }else{
				 achVO.setStatusCode(ledgerValidateVO.getStatusCode());
				 achVO.setMessage(ledgerValidateVO.getMessage());
			 }
			log.debug("Exit : public AccountHeadVO insertLedger(int societyID,AccountHeadVO achVO))");
		} catch (Exception e) {
			log.error("Exception : public AccountHeadVO insertLedger(int societyID,AccountHeadVO achVO)"+e);
		}
		return achVO;
	}
//	This method is used to update new ledgers 
	public AccountHeadVO updateLedger(AccountHeadVO ledgerVO,int societyID,int ledgerID){
		int success=0;
		List ledgerList=new ArrayList();
		try {
			
			log.debug("Entry : public AccountHeadVO updateLedger(int societyID,AccountHeadVO achVO)");
			AccountHeadVO validateLedgerVO=ledgerDAO.getLedgerDetails(ledgerID, societyID);
			validateLedgerVO=validateLedgersForUpdate(validateLedgerVO, societyID);
			if(((validateLedgerVO.getOpeningBalance().compareTo(ledgerVO.getOpeningBalance())!=0))&&((validateLedgerVO.getStatusCode()!=200))){
			
				ledgerVO.setStatusCode(validateLedgerVO.getStatusCode());
				ledgerVO.setMessage(validateLedgerVO.getMessage());
			
			}else{
			
			ledgerVO.setOpeningBalance(convertToSignedAmt(ledgerVO, societyID));
			ledgerList=ledgerDAO.checkLedgerDetails(ledgerVO, societyID);
			if(ledgerList.size()!=0){
				for(int i=0;ledgerList.size()>i;i++){
					
					AccountHeadVO acHVO=(AccountHeadVO) ledgerList.get(i);
					
					if(acHVO.getLedgerID()==ledgerID){
						
						success=ledgerDAO.updateLedger(ledgerVO, societyID, ledgerID);
												
						break;
					}else success=-1;
					
				}
			
			}
			else success=ledgerDAO.updateLedger(ledgerVO, societyID, ledgerID);
			

			if(success>0){
				ledgerVO.setStatusCode(200);
				ledgerVO.setMessage("Ledger has been updated successfully.");
			
			}else if(success==-1){
				ledgerVO.setStatusCode(534);
				ledgerVO.setMessage(ledgerVO.getStrAccountHeadName()+" is already present , Please select any other Ledger Name");
				
			}else{
				ledgerVO.setStatusCode(400);
				
				ledgerVO.setMessage("Problem in Update Ledger.");
				
			}
			}
			
			log.debug("Exit : public AccountHeadVO updateLedger(int societyID,AccountHeadVO achVO))");
		} catch (Exception e) {
			log.error("Exception : public AccountHeadVO updateLedger(int societyID,AccountHeadVO achVO)"+e);
		}
		return ledgerVO;
	}
	
	//	This method is used to Delete new ledgers 
	public AccountHeadVO deleteLedger(int societyID,int ledgerID){
		int success=0;
		AccountHeadVO ledgerVO=new AccountHeadVO();
		String fromDate="2014-04-01";
		Date curentDate=dateUtil.findCurrentDate();
		try {
			log.debug("Entry :public int deleteLedger(String societyID,int ledgerID))"+curentDate.toString());

			List transactionList=transactionService.getTransctionList(societyID, ledgerID, fromDate, curentDate.toString(), "ALL");
			if(transactionList.size()>0){
				success=-1;
			}else
			success=ledgerDAO.deleteLedger( societyID, ledgerID);	
			
			
			
			if(success>0){
				ledgerVO.setStatusCode(200);
				ledgerVO.setMessage("Ledger has been deleted successfully.");
			
			}else if(success==-1){
				ledgerVO.setStatusCode(534);
				ledgerVO.setMessage("This ledger has few transactions.By removing this ledger Balance Sheet might get affected");
				
			}else{
				ledgerVO.setStatusCode(400);
				
				ledgerVO.setMessage("Problem in adding Ledger.");
				
			}
			
			log.debug("Exit : public int deleteLedger(String societyID,int ledgerID)");
		} catch (Exception e) {
			log.error("Exception : public int deleteLedger(String societyID,int ledgerID)"+e);
		}
		return ledgerVO;
	}
	
	
	/* Used for validation ledger insert */
	public AccountHeadVO validateLedgersForInsert(AccountHeadVO ledgerVO,int orgID){
		log.debug("Entry : public AccountHeadVO validateLedgersForInsert(AccountHeadVO ledgerVO,int orgID)"+orgID);
		BigDecimal sumOfCr=BigDecimal.ZERO;
		BigDecimal sumOfDr=BigDecimal.ZERO;
		SocietyVO societyVO =new SocietyVO();
	    Boolean isAuditDateValid=false;
	    Boolean isGSTDateValid=false;
	    Boolean isSocDateValid=false;
		try{		
			Date socLocDate;
			Date auditLocDate;
			Date gstLocDate;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date effectiveDate=sdf.parse(ledgerVO.getEffectiveDate());
			societyVO=societyServiceBean.getSocietyDetails(orgID);
			
			//audit lock date
			if(societyVO.getAuditCloseDate()!=null){
				auditLocDate=sdf.parse(societyVO.getAuditCloseDate());				
				if(effectiveDate.compareTo(auditLocDate)<=0){
					isAuditDateValid=false;
					ledgerVO.setStatusCode(530);
					ledgerVO.setMessage("Your organization audits has been locked till "+dateUtil.getConvetedDate(societyVO.getAuditCloseDate())+", So that ledgers can not be added");
					return ledgerVO;
				}else{
					ledgerVO.setStatusCode(200);
				}				
			}else{
				ledgerVO.setStatusCode(200);
			}
			
			
			//society lock date
			if(societyVO.getTxCloseDate()!=null){
				socLocDate=sdf.parse(societyVO.getTxCloseDate());
				if(effectiveDate.compareTo(socLocDate)<=0){ 
					isSocDateValid=false;
					ledgerVO.setStatusCode(530);
					ledgerVO.setMessage("Your organization accounts has been locked till "+dateUtil.getConvetedDate(societyVO.getTxCloseDate())+", So that ledgers can not be added");
					return ledgerVO;
				}else{					
					ledgerVO.setStatusCode(200);
				}
			
			}else{
				ledgerVO.setStatusCode(200);
			}
			
								
			log.debug("Status code : "+ledgerVO.getStatusCode()+" Message: "+ledgerVO.getMessage());
			
			
		}catch(Exception e){
			log.error("Exception in validateLedgersForInsert "+e);
		}
		
		log.debug("Exit :public AccountHeadVO validateLedgersForInsert(TransactionVO transactionVO,int societyID)");
		return ledgerVO;
	
	}
	
	/* Used for validation ledger update */
	public AccountHeadVO validateLedgersForUpdate(AccountHeadVO ledgerVO,int orgID){
		log.debug("Entry : public AccountHeadVO validateLedgersForUpdate(AccountHeadVO ledgerVO,int orgID)"+orgID);
		BigDecimal sumOfCr=BigDecimal.ZERO;
		BigDecimal sumOfDr=BigDecimal.ZERO;
		SocietyVO societyVO =new SocietyVO();
	    Boolean isAuditDateValid=false;
	    Boolean isLedgerDateValid=false;
	    Boolean isSocDateValid=false;
		try{		
			Date socLocDate;
			Date auditLocDate;
			Date ledgerLocDate;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date effectiveDate=sdf.parse(ledgerVO.getEffectiveDate());
			societyVO=societyServiceBean.getSocietyDetails(orgID);
			
			//audit lock date
			if(societyVO.getAuditCloseDate()!=null){
				auditLocDate=sdf.parse(societyVO.getAuditCloseDate());				
				if(effectiveDate.compareTo(auditLocDate)<=0){
					isAuditDateValid=false;
					ledgerVO.setStatusCode(530);
					ledgerVO.setMessage("Your organization audits has been locked till "+dateUtil.getConvetedDate(societyVO.getAuditCloseDate())+", So that ledgers can not be added");
					return ledgerVO;
				}else{
					ledgerVO.setStatusCode(200);
				}				
			}else{
				ledgerVO.setStatusCode(200);
			}
			
			
			//society lock date
			if(societyVO.getTxCloseDate()!=null){
				socLocDate=sdf.parse(societyVO.getTxCloseDate());
				if(effectiveDate.compareTo(socLocDate)<=0){ 
					isSocDateValid=false;
					ledgerVO.setStatusCode(530);
					ledgerVO.setMessage("Your organization accounts has been locked till "+dateUtil.getConvetedDate(societyVO.getTxCloseDate())+", So that ledgers can not be added");
					return ledgerVO;
				}else{					
					ledgerVO.setStatusCode(200);
				}
			
			}else{
				ledgerVO.setStatusCode(200);
			}
			
			
			//Ledger lock date
			AccountHeadVO validateLedgerVO=ledgerDAO.getLedgerDetails(ledgerVO.getLedgerID(), orgID);
			if(validateLedgerVO.getLockDate()!=null){
				ledgerLocDate=sdf.parse(validateLedgerVO.getLockDate());
				if(effectiveDate.compareTo(ledgerLocDate)<=0){ 
					isSocDateValid=false;
					ledgerVO.setStatusCode(531);
					ledgerVO.setMessage("Cannot complete your request since Ledger "+validateLedgerVO.getStrAccountHeadName()+" has been locked till "+dateUtil.getConvetedDate(validateLedgerVO.getLockDate())+", Please co-ordinate with your administrator to remove ledger or account lock.");
					return ledgerVO;
				}else{					
					ledgerVO.setStatusCode(200);
				}
			
			}else{
				ledgerVO.setStatusCode(200);
			}
								
			log.debug("Status code : "+ledgerVO.getStatusCode()+" Message: "+ledgerVO.getMessage());
			
			
		}catch(Exception e){
			log.error("Exception in validateLedgersForUpdate "+e);
		}
		
		log.debug("Exit :public AccountHeadVO validateLedgersForUpdate(TransactionVO transactionVO,int societyID)");
		return ledgerVO;
	
	}
	
	private BigDecimal convertToSignedAmt(AccountHeadVO achVO,int societyID){
		log.debug("Entry : private BigDecimal convertToSignedAmt(AccountHeadVO achVO,String societyID)");
		BigDecimal updatedBal=BigDecimal.ZERO;
		String updatedBalance="";
		BigDecimal opBal=BigDecimal.ZERO;
		opBal=achVO.getOpeningBalance().abs();
		try{
		
		LedgerEntries tempLedgerEntry=new LedgerEntries();
		tempLedgerEntry=ledgerDAO.getSubGroupDetails(achVO.getAccountGroupID());
			
		if(achVO.getLedgerType().equalsIgnoreCase("C")){
			
			if(tempLedgerEntry.getCreditAction()==1){
				
				updatedBalance="+"+opBal;
			}else 
				updatedBalance="-"+opBal;
			
		}else if(achVO.getLedgerType().equalsIgnoreCase("D")){
			
			if(tempLedgerEntry.getDebitAction()==1){
				updatedBalance="+"+opBal;
			}else
				updatedBalance="-"+opBal;
			
		}
		updatedBal=new BigDecimal(updatedBalance);
		
		}catch(Exception e){
			log.error("Exception in conversion "+e+" - "+e.getLocalizedMessage());
		}
		log.debug("Entry : private BigDecimal convertToSignedAmt(AccountHeadVO achVO,String societyID)"+updatedBalance);
		return updatedBal;
	}
	
	public AccountHeadVO getLedgerID(int userID,int societyID,String userType,String identifier){
		AccountHeadVO achVO=new AccountHeadVO();
		try {
			log.debug("Entry : ppublic AccountHeadVO getLedgerID(int memberID)");
			
			
			achVO=ledgerDAO.getLedgerID(userID,societyID,userType,identifier);	
			
			log.debug("Exit : public getLedgerID(int memberID)");
		} catch (Exception e) {
			log.error("Exception : getLedgerID(int memberID)"+e);
		}
		return achVO;
	}
	
	public AccountHeadVO getLedgerDetails(int ledgerID,int societyID){
		AccountHeadVO achVO=new AccountHeadVO();
		try {
			log.debug("Entry : public AccountHeadVO getLedgerDetails(String ledgerID,String societyID)");
			
			
			achVO=ledgerDAO.getLedgerDetails(ledgerID, societyID);	
			
			log.debug("Exit : public AccountHeadVO getLedgerDetails(String ledgerID,String societyID)");
		} catch (Exception e) {
			log.error("Exception : public AccountHeadVO getLedgerDetails(String ledgerID,String societyID)"+e);
		}
		return achVO;
	}
	
	
	public List getAllGroupList(int rootID,int orgID){
		List ledgerList=null;
		try {
			log.debug("Entry : public List getAllGroupList(int rootID,int orgID)");

			ledgerList=ledgerDAO.getAllGroupList(rootID,orgID);	
			
			log.debug("Exit : public List getAllGroupList(int rootID,int orgID)");
		} catch (Exception e) {
			log.error("Exception : public List getAllGroupList(int rootID,int orgID)"+e);
		}
		return ledgerList;
	}
	
	
	public List getAllSubCategoryList(int parentGroupID){
		List categoryList=null;
		try {
			log.debug("Entry : public List getAllSubCategoryList(int parentGroupID)");

			categoryList=ledgerDAO.getAllSubCategoryList(parentGroupID);	
			
			log.debug("Exit : public List getAllSubCategoryList(int parentGroupID)"+categoryList.size());
		} catch (Exception e) {
			log.error("Exception :public List getAllSubCategoryList(int parentGroupID)"+e);
		}
		return categoryList;
	}
	
//	This method is used to give reverse effects to ledger balances while doing delete transaction
	public List getLedgerList(int societyID,int gruoupOrRootID,String type){
		List ledgerList=null;
		try {
			log.debug("Entry : public List getLedgersList(String societyID,int gruoupOrRootID,String type)");
			BigDecimal sumOfCredit=BigDecimal.ZERO;
			BigDecimal sumOfDebit=BigDecimal.ZERO;
			ledgerList=ledgerDAO.getLedgersList(societyID,gruoupOrRootID,type,"A");
			if(ledgerList.size()>0){
			 for(int i=0;ledgerList.size()>i;i++){
				 AccountHeadVO acHVO=(AccountHeadVO) ledgerList.get(i);
				 
				 acHVO=unsignedOpeningClosingBalance(acHVO);
				 sumOfCredit=acHVO.getCrOpnBal().add(sumOfCredit);
				 sumOfDebit=acHVO.getDrOpnBal().add(sumOfDebit);
				 
				 
				 
				
			 }
			
			AccountHeadVO ac=(AccountHeadVO)ledgerList.get(0);
			ac.setSumOfCreditBal(sumOfCredit);
			ac.setSumOfDebitBal(sumOfDebit);
			ledgerList.remove(0);
			ledgerList.add(0, ac);
			}
						
			log.debug("Exit : public List getLedgersList(String societyID,String ledgerType)"+ledgerList.size());
		} catch (Exception e) {
			log.error("Exception : public List getLedgersList(String societyID,String ledgerType) for society id "+societyID+" for group ID "+gruoupOrRootID+e);
		}
		return ledgerList;
	}
	
//	This method is used to give reverse effects to ledger balances while doing delete transaction
	public List getLedgersListWithClosingBal(int societyID,int groupOrRootID,String type,String fromDate,String toDate) {
		List ledgerList=null;
		try {
			log.debug("Entry : public List getLedgersList(String societyID,int gruoupOrRootID,String type)");
			BigDecimal sumOfCredit=BigDecimal.ZERO;
			BigDecimal sumOfDebit=BigDecimal.ZERO;
			ledgerList=ledgerDAO.getLedgersListWithClosingBal(societyID,groupOrRootID,type,"A",fromDate,toDate);
			if(ledgerList.size()>0){
			 for(int i=0;ledgerList.size()>i;i++){
				 AccountHeadVO acHVO=(AccountHeadVO) ledgerList.get(i);
				 
				 acHVO=unsignedOpeningClosingBalance(acHVO);
				 sumOfCredit=acHVO.getCrOpnBal().add(sumOfCredit);
				 sumOfDebit=acHVO.getDrOpnBal().add(sumOfDebit);
				 
				 
				 
				
			 }
			
			AccountHeadVO ac=(AccountHeadVO)ledgerList.get(0);
			ac.setSumOfCreditBal(sumOfCredit);
			ac.setSumOfDebitBal(sumOfDebit);
			ledgerList.remove(0);
			ledgerList.add(0, ac);
			}
						
			log.debug("Exit : public List getLedgersListWithClosingBal(int societyID,int groupOrRootID,String type,String isHide,String fromDate,String toDate) "+ledgerList.size());
		} catch (Exception e) {
			log.error("Exception : public List getLedgersListWithClosingBal(int societyID,int groupOrRootID,String type,String isHide,String fromDate,String toDate)  for society id "+societyID+" for group ID "+groupOrRootID+e);
		}
		return ledgerList;
	}
	
	
	 //	This method is used to get master ledger list
	public List getMasterLedgerList(int societyID,int gruoupOrRootID,String type){
		List ledgerList=null;
		try {
			log.debug("Entry : public List getMasterLedgerList(String societyID,String ledgerType)");

			ledgerList=ledgerDAO.getLedgersList(societyID,gruoupOrRootID,type,"V");	
			
			/*if(ledgerList.size()>0){


				for(int i=0;ledgerList.size()>i;i++){

					AccountHeadVO achVO=(AccountHeadVO) ledgerList.get(i);
					
					ProfileDetailsVO profileVO=ledgerDAO.getLedgerProfileDetails(achVO.getLedgerID(), societyID);
				
					achVO.setProfileVO(profileVO);
					
					


				}
			}*/

									
			log.debug("Exit : public List getMasterLedgerList(String societyID,String ledgerType)"+ledgerList.size());
		} catch (Exception e) {
			log.error("Exception : public List getMasterLedgerList(String societyID,String ledgerType)"+e);
		}
		return ledgerList;
	}
	
	/* Used to get list of ledgers which are noted with some properties*/
	public List getLedgersListByProperties(int societyID,String properties)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public List getLedgersListByProperties(int societyID,String properties)");
		List ledgerList=null;
	try{
			 
			   ledgerList=ledgerDAO.getLedgersListByProperties(societyID, properties);
			   
			   log.debug("Exit :  public List getLedgersListByProperties(int societyID,String properties)"+ledgerList.size());
		}catch(Exception se){
				log.error("Exception : public List getLedgersListByProperties(int societyID,String properties) "+se);
			   
			}
		return ledgerList;
	}
	
//	This method is used to give reverse effects to ledger balances while doing delete transaction
	public List getLedgerListForTDSCalculation(int societyID,int gruoupOrRootID,String type){
		List ledgerList=null;
		try {
			log.debug("Entry : public List getLedgersList(String societyID,int gruoupOrRootID,String type)");

			ledgerList=ledgerDAO.getTDSLedgersList(societyID);
			 for(int i=0;ledgerList.size()>i;i++){
				 AccountHeadVO acHVO=(AccountHeadVO) ledgerList.get(i);
				 
				 acHVO=unsignedOpeningClosingBalance(acHVO);
				 
				 TDSDetailsVO tdsVO=ledgerDAO.getLedgerListForTDSCalculation(societyID, acHVO.getLedgerID(),acHVO.getServiceLedgerID());
				 
				 acHVO.setTdsSection(tdsVO.getTdsSectionName());
				 acHVO.setTdsSectionNo(tdsVO.getTdsSectionNo());
				 acHVO.setCutOffAmt(tdsVO.getTdsCutOffAmount());
				 acHVO.setTdsRtIndiv(tdsVO.getTdsRateForIndiv());
				 acHVO.setTdsRtOther(tdsVO.getTdsRateForOther());
				 acHVO.setStrAccountHeadName(acHVO.getStrAccountHeadName()+" - "+tdsVO.getServiceLedgerName());
				 acHVO.setTdsVO(tdsVO);
				 if(tdsVO.getDeducteeCode()==1){
					 acHVO.setTdsRate(acHVO.getTdsRtIndiv());
				 }else
					 acHVO.setTdsRate(acHVO.getTdsRtOther());
				
				
			 }
			
			 log.debug("0------->"+ledgerList.size());
						
			log.debug("Exit : public List getLedgerListForTDSCalculation(String societyID,String ledgerType)"+ledgerList.size());
		} catch (Exception e) {
			log.error("Exception : public List getLedgerListForTDSCalculation(String societyID,String ledgerType)"+e);
		}
		return ledgerList;
	}
	
	
	public AccountHeadVO getBalancesForTrialBal(AccountHeadVO achVO){
		log.debug("Entry : public AccountHeadVO getBalancesForTrialBal(AccountHeadVO achVO)"+achVO.getRootID());
		try {
		BigDecimal opPos=achVO.getOpPosBal();
		BigDecimal opNeg=achVO.getOpNegBal();
		BigDecimal clPos=achVO.getClPosBal();
		BigDecimal clNeg=achVO.getClNegBal();
		
		
			if((achVO.getRootID()==1)||(achVO.getRootID()==4)){
				//log.info("==opneg "+opNeg+"   opPos "+opPos+" clNeg "+clNeg+" clPos "+clPos);
				achVO.setDrOpnBal(opNeg);
				achVO.setCrOpnBal(opPos);
				achVO.setDrClsBal(clNeg);
				achVO.setCrClsBal(clPos);
				//log.info("==opneg "+achVO.getDrOpnBal()+"   opPos "+achVO.getCrOpnBal()+" clNeg "+clNeg+" clPos "+clPos);
			}else{
				achVO.setDrOpnBal(opPos);
				achVO.setCrOpnBal(opNeg);
				achVO.setDrClsBal(clPos);
				achVO.setCrClsBal(clNeg);
			
			}
			if((achVO.getRootID()==1)||(achVO.getRootID()==2)){
				achVO.setDrClsBal(achVO.getDrClsBal());
				achVO.setCrClsBal(achVO.getCrClsBal());
				achVO.setDrOpnBal(BigDecimal.ZERO);
				achVO.setCrOpnBal(BigDecimal.ZERO);
			}
			
			
			
		} catch (Exception e) {
			log.error("Exception at getBalancesForTrialBal "+e);
		}
		
		
		log.debug("Exit : public AccountHeadVO getBalancesForTrialBal(AccountHeadVO achVO)");
		return achVO;
	}
	
	

	public AccountHeadVO getOpeningClosingBalance(int societyID,int ledgerID,String fromDate,String uptoDate,String ledgerOrGroup) throws ParseException{
		AccountHeadVO acHVO=new AccountHeadVO();
		AccountHeadVO ledgerVO=new AccountHeadVO();
		BigDecimal openingBalance=BigDecimal.ZERO;
		BigDecimal crOpeningBal=BigDecimal.ZERO;
		BigDecimal drOpeningBal=BigDecimal.ZERO;
		AccountHeadVO ahVO=new AccountHeadVO();
		BigDecimal closingBalance=BigDecimal.ZERO;
		BigDecimal crClosingBal=BigDecimal.ZERO;
		BigDecimal drClosingBal=BigDecimal.ZERO;
		BigDecimal sumOpn=BigDecimal.ZERO;
		BigDecimal sumCls=BigDecimal.ZERO;
		BigDecimal sumIntOpn=BigDecimal.ZERO;
		BigDecimal sumIntCls=BigDecimal.ZERO;
		BigDecimal intOpnBalance=BigDecimal.ZERO;
		BigDecimal intClsBalance=BigDecimal.ZERO;
		int groupID;
		String groupName="";
		String ledgerName="";
		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd" );   
		Calendar calFrm = Calendar.getInstance();    
		Calendar calTo=Calendar.getInstance();
		calTo.setTime(dateFormat.parse(uptoDate));
		calFrm.setTime( dateFormat.parse(fromDate));    
		calFrm.add( Calendar.DATE, -1 );    
		calTo.add(Calendar.DATE,1);
		String beforeDate=dateUtil.getPrevOrNextDate(fromDate, 1);  
		String afterDate=dateUtil.getPrevOrNextDate(uptoDate, 0);
		String normalBal="";
		int rootID=0;
		
		try {
			log.debug("Entry :public AccountHeadVO getOpeningBalance(String societyID,int ledgerID,String fromDate, String uptoDate)"+fromDate+" "+beforeDate+" "+afterDate);
			
			ledgerVO=ledgerDAO.getLedgerDetails(ledgerID, societyID);
			acHVO=ledgerDAO.getSumOpeningBalance(societyID, ledgerID, fromDate,ledgerOrGroup);
			rootID=ledgerVO.getRootID();
			groupName=ledgerVO.getStrAccountPrimaryHead();
			ledgerName=ledgerVO.getStrAccountHeadName();
			normalBal=ledgerVO.getNormalBalance();
			groupID=ledgerVO.getAccountGroupID();
			log.debug("Here is  -- "+ledgerVO.getStrAccountHeadName()+"  --- "+ledgerVO.getStrAccountPrimaryHead()+"------"+normalBal);
			openingBalance=acHVO.getOpeningBalance();
			if(acHVO.getIntOpnBalance()!=null)
			intOpnBalance=acHVO.getIntOpnBalance();
			log.debug(ledgerID+"-------1>Opening Bal"+openingBalance);
			acHVO=ledgerDAO.getSumTransactions(societyID, ledgerID, fromDate, ledgerOrGroup);
			sumOpn=acHVO.getSum();
			if(acHVO.getSumInterest()!=null)
			sumIntOpn=acHVO.getSumInterest();
			log.debug(ledgerID+"-------1>Sum of Tx Bal"+sumOpn);		
			openingBalance=openingBalance.add(sumOpn);
			intOpnBalance=intOpnBalance.add(sumIntOpn);
			log.debug("-------2> Opening Bal "+openingBalance);
			
			
			ahVO=ledgerDAO.getSumOpeningBalance(societyID, ledgerID, afterDate,ledgerOrGroup);
			closingBalance=ahVO.getOpeningBalance();
			if(ahVO.getIntOpnBalance()!=null)
			intClsBalance=ahVO.getIntOpnBalance();
			log.debug(afterDate+"-------1> Closing Bal "+closingBalance+" "+uptoDate);
			ahVO=ledgerDAO.getSumTransactions(societyID, ledgerID, afterDate, ledgerOrGroup);
			sumCls=ahVO.getSum();
			if(ahVO.getSumInterest()!=null)
			sumIntCls=ahVO.getSumInterest();
			
			log.debug("-------2> Closing Bal "+sumCls);
			closingBalance=closingBalance.add(sumCls);
			intClsBalance=intClsBalance.add(sumIntCls);
			log.debug("-------2> Closing Bal "+closingBalance);
			
			
			
			ledgerVO.setClosingBalance(closingBalance);
			ledgerVO.setOpeningBalance(openingBalance);
			ledgerVO.setIntOpnBalance(intOpnBalance);
			ledgerVO.setIntClsBalance(intClsBalance);
			ledgerVO.setNormalBalance(normalBal);
			ledgerVO.setRootID(rootID);
			if(sumCls.intValue()!=sumOpn.intValue())
			ledgerVO.setClPosBal(sumCls);
			else 
				ledgerVO.setClPosBal(BigDecimal.ZERO);
			ledgerVO=unsignedOpeningClosingBalance(ledgerVO);
			ledgerVO.setAccountGroupID(groupID);
			ledgerVO.setStrAccountHeadName(ledgerName);
			ledgerVO.setStrAccountPrimaryHead(groupName);
			
			//log.debug("Exit :public AccountHeadVO getOpeningBalance(String societyID,int ledgerID,String fromDate, String uptoDate)"+acHVO.getSum());
		} catch (NullPointerException ne) {
					log.debug("Exception : public AccountHeadVO getOpeningBalance(String societyID,int ledgerID,String fromDate, String uptoDate) "+ne );
				
		} catch (Exception e) {
			log.error("Exception : public AccountHeadVO getOpeningBalance(String societyID,int ledgerID,String fromDate, String uptoDate) "+e );
		}
		return ledgerVO;
		
	}
	
	/* Get reconciled closing balance*/
	public AccountHeadVO getReconciledClosingBalance(int societyID,int ledgerID,String fromDate,String uptoDate,String ledgerOrGroup) throws ParseException{
		AccountHeadVO acHVO=new AccountHeadVO();
		AccountHeadVO ledgerVO=new AccountHeadVO();
		BigDecimal openingBalance=BigDecimal.ZERO;
		BigDecimal crOpeningBal=BigDecimal.ZERO;
		BigDecimal drOpeningBal=BigDecimal.ZERO;
		AccountHeadVO ahVO=new AccountHeadVO();
		BigDecimal closingBalance=BigDecimal.ZERO;
		BigDecimal crClosingBal=BigDecimal.ZERO;
		BigDecimal drClosingBal=BigDecimal.ZERO;
		BigDecimal sumOpn=BigDecimal.ZERO;
		BigDecimal sumCls=BigDecimal.ZERO;
		int groupID;
		String groupName="";
		String ledgerName="";
		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd" );   
		Calendar calFrm = Calendar.getInstance();    
		Calendar calTo=Calendar.getInstance();
		calTo.setTime(dateFormat.parse(uptoDate));
		calFrm.setTime( dateFormat.parse(fromDate));    
		calFrm.add( Calendar.DATE, -1 );    
		calTo.add(Calendar.DATE,1);
		String beforeDate=dateUtil.getPrevOrNextDate(fromDate, 1);  
		String afterDate=dateUtil.getPrevOrNextDate(uptoDate, 0);
		String normalBal="";
		int rootID=0;
		
		try {
			log.debug("Entry :public AccountHeadVO getReconciledClosingBalance(int societyID,int ledgerID,String fromDate,String uptoDate,String ledgerOrGroup)"+fromDate+" "+beforeDate+" "+afterDate);
			
			ledgerVO=ledgerDAO.getLedgerDetails(ledgerID, societyID);
			rootID=ledgerVO.getRootID();
			groupName=ledgerVO.getStrAccountPrimaryHead();
			ledgerName=ledgerVO.getStrAccountHeadName();
			normalBal=ledgerVO.getNormalBalance();
			groupID=ledgerVO.getAccountGroupID();
						
			ahVO=ledgerDAO.getSumOpeningBalance(societyID, ledgerID, afterDate,ledgerOrGroup);
			closingBalance=ahVO.getOpeningBalance();
			log.debug(afterDate+"-------1> Closing Bal "+closingBalance+" "+uptoDate);
			ahVO=ledgerDAO.getSumReconcileTransactions(societyID, ledgerID, afterDate, ledgerOrGroup);
			sumCls=ahVO.getSum();
			
			log.debug("-------2> Closing Bal "+sumCls);
			closingBalance=closingBalance.add(sumCls);
			log.debug("-------2> Closing Bal "+closingBalance);
						
			ledgerVO.setClosingBalance(closingBalance);
			ledgerVO.setOpeningBalance(openingBalance);
			ledgerVO.setNormalBalance(normalBal);
			ledgerVO.setRootID(rootID);
			if(sumCls.intValue()!=sumOpn.intValue())
			ledgerVO.setClPosBal(sumCls);
			else 
				ledgerVO.setClPosBal(BigDecimal.ZERO);
			ledgerVO=unsignedOpeningClosingBalance(ledgerVO);
			ledgerVO.setAccountGroupID(groupID);
			ledgerVO.setStrAccountHeadName(ledgerName);
			ledgerVO.setStrAccountPrimaryHead(groupName);
			
			//log.debug("Exit :public AccountHeadVO getOpeningBalance(String societyID,int ledgerID,String fromDate, String uptoDate)"+acHVO.getSum());
		} catch (NullPointerException ne) {
					log.debug("Exception : getReconciledClosingBalance "+ne );
				
		} catch (Exception e) {
			log.error("Exception : public AccountHeadVO getReconciledClosingBalance(int societyID,int ledgerID,String fromDate,String uptoDate,String ledgerOrGroup) "+e );
		}
		return ledgerVO;
		
	}
	
	public AccountHeadVO unsignedOpeningClosingBalance(AccountHeadVO acHVO){
		log.debug("Entry : private AccountHeadVO unsignedOpeningClosingBalance(AccountHeadVO achVO)");
		BigDecimal openingBalance=BigDecimal.ZERO;
		BigDecimal closingBalance=BigDecimal.ZERO;
		BigDecimal crOpnBal=BigDecimal.ZERO;
		BigDecimal crClsBal=BigDecimal.ZERO;
		BigDecimal drOpnBal=BigDecimal.ZERO;
		BigDecimal drClsBal=BigDecimal.ZERO;
		BigDecimal zero=new BigDecimal("0.00");
		openingBalance=acHVO.getOpeningBalance();
		closingBalance=acHVO.getClosingBalance();
		String normalBal=acHVO.getNormalBalance();
		
		try{
		
		
		
		if(normalBal.equalsIgnoreCase("C")){
			if(openingBalance.compareTo(BigDecimal.ZERO) >= 0){
			crOpnBal=openingBalance;
			
			}else
				drOpnBal=openingBalance.abs();
			if(closingBalance.compareTo(BigDecimal.ZERO)>=0){
				crClsBal=closingBalance;
				
			}else
				drClsBal=closingBalance.abs();
			
		}else{
			if(openingBalance.compareTo(BigDecimal.ZERO) >= 0){
				drOpnBal=openingBalance;
				
				
				}else
					crOpnBal=openingBalance.abs();
				if(closingBalance.compareTo(BigDecimal.ZERO)>=0){
					drClsBal=closingBalance;
					
				}else
					crClsBal=closingBalance.abs();
		}
			
		
		if((acHVO.getRootID()==1)||(acHVO.getRootID()==2)){
			
			try {
				
				closingBalance=closingBalance.subtract(openingBalance);
				
				crClsBal=crClsBal.subtract(crOpnBal);
				drClsBal=drClsBal.subtract(drOpnBal);
				openingBalance=zero;
				crOpnBal=zero;
				drOpnBal=zero;
				
				if(normalBal.equalsIgnoreCase("C")){
					
					if(closingBalance.compareTo(BigDecimal.ZERO)>=0){
						crClsBal=closingBalance;
						drClsBal=zero;
					}else{
						drClsBal=closingBalance.abs();
						crClsBal=zero;
					}
				}else{
					
						if(closingBalance.compareTo(BigDecimal.ZERO)>=0){
							drClsBal=closingBalance;
							crClsBal=zero;
						}else{
							crClsBal=closingBalance.abs();
							drClsBal=zero;
						}
				}
				
				
			} catch (Exception e) {
				log.error("Exception : "+e);
			}
		
		
		}
		
		acHVO.setClosingBalance(closingBalance);
		acHVO.setOpeningBalance(openingBalance);
		acHVO.setCrOpnBal(crOpnBal);
		acHVO.setDrOpnBal(drOpnBal);
		acHVO.setCrClsBal(crClsBal);
		acHVO.setDrClsBal(drClsBal);
		
		}catch (NullPointerException e) {
			acHVO.setClosingBalance(zero);
			acHVO.setOpeningBalance(zero);
			acHVO.setCrOpnBal(zero);
			acHVO.setDrOpnBal(zero);
			acHVO.setCrClsBal(zero);
			acHVO.setDrClsBal(zero);
			log.debug("Exception : No details found "+e);
		
		}catch (Exception e) {
			log.error("Exception : private AccountHeadVO unsignedOpeningClosingBalance(AccountHeadVO achVO)"+e);
		}
		log.debug("Exit : private AccountHeadVO unsignedOpeningClosingBalance(AccountHeadVO achVO)");
		return acHVO;
	}
	
	
	public AccountHeadVO getUpdatedBalances(LedgerEntries ledgerEntry,int societyID ){
		String updatedBalance="";
		BigDecimal balance=BigDecimal.ZERO;
		AccountHeadVO ledgerVO=new AccountHeadVO();
		
		LedgerEntries tempLedgerEntry=new LedgerEntries();
		BigDecimal amount=ledgerEntry.getAmount().abs();

		try {
			log.debug("Entry : BigDecimal getUpdatedBalances(LedgerEntries ledgerEntry,String societyID )"+ledgerEntry.getCreditDebitFlag()+" "+ledgerEntry.getAmount());
			ledgerVO=ledgerDAO.getLedgerDetails(ledgerEntry.getLedgerID(), societyID);
			
			
			
			if(ledgerEntry.getCreditDebitFlag().equalsIgnoreCase("C")){
				
				if(ledgerVO.getCreditAction()==1){
					
					updatedBalance="+"+amount;
				}else 
					updatedBalance="-"+amount;
				
			}else if(ledgerEntry.getCreditDebitFlag().equalsIgnoreCase("D")){
				
				if(ledgerVO.getDebitAction()==1){
					updatedBalance="+"+amount;
				}else
					updatedBalance="-"+amount;
				
			}
			
			balance=new BigDecimal(updatedBalance);
			ledgerVO.setAmount(balance);
			
			
		} catch (Exception e) {
			log.error("Exception : getUpdatedBalances"+e);
		}
		
		log.debug("Exit : BigDecimal getUpdatedBalances(LedgerEntries ledgerEntry,String societyID )");
		return ledgerVO;
		
	}
	
	
	public AccountHeadVO getProffitLossBalance(int societyID,String fromDate,String uptoDate,String bsOrTb,int isConsolidated,String formatType){
		AccountHeadVO proffitLossVO=new AccountHeadVO();
		log.debug("Entry  : private AccountHeadVO getProffitLossBalance(String societyID,String fromDate,String uptoDate,int isConsolidated,String formatType)");
		try{
		
		ReportVO rptVO=new ReportVO();
		SocietyVO societyVO=societyServiceBean.getSocietyDetails(societyID);
		ReportDetailsVO tempRptVO=new ReportDetailsVO();
		rptVO=reportService.getIncomeExpenseReport(societyID, fromDate, uptoDate,isConsolidated,formatType);
		
		tempRptVO=reportService.calculateProfitLossDetails(rptVO.getIncomeTxList(), rptVO.getExpenseTxList(), societyID, fromDate, uptoDate,bsOrTb,formatType);
		
		if(societyVO.getOrgType().equalsIgnoreCase("S")){
		proffitLossVO.setStrAccountPrimaryHead("Income and Expenditure A/c");
		}else if(societyVO.getOrgType().equalsIgnoreCase("C")){
			proffitLossVO.setStrAccountPrimaryHead("Profit Loss A/c");
		}
		proffitLossVO.setClosingBalance(tempRptVO.getOpeningBalance());
		proffitLossVO.setOpeningBalance(tempRptVO.getClosingBalance());
		log.debug("-------------------------->"+tempRptVO.getOpeningBalance()+"  "+tempRptVO.getClosingBalance()+"       "+tempRptVO.getDifferenceInCurrentBal());	
		
		/*if(proffitLossVO.getOpeningBalance().compareTo(BigDecimal.ZERO) >= 0){
			//crOpnBal=openingBalance;
			proffitLossVO.setCrOpnBal(proffitLossVO.getOpeningBalance());
			}else
				//drOpnBal=openingBalance.abs();
				proffitLossVO.setDrOpnBal(proffitLossVO.getOpeningBalance().abs());
			if(proffitLossVO.getClosingBalance().compareTo(BigDecimal.ZERO)>=0){
				//crClsBal=closingBalance;
				proffitLossVO.setCrClsBal(proffitLossVO.getClosingBalance());
				
			}else
				//drClsBal=closingBalance.abs();
				proffitLossVO.setDrClsBal(proffitLossVO.getClosingBalance().abs());*/
		
		if(proffitLossVO.getOpeningBalance().signum()<0){
			
			proffitLossVO.setDrOpnBal(tempRptVO.getOpeningBalance().abs());
			proffitLossVO.setDrClsBal(tempRptVO.getOpeningBalance().add(tempRptVO.getDifferenceInCurrentBal().abs()).abs());
		}else{
			
		proffitLossVO.setCrOpnBal(tempRptVO.getOpeningBalance());
		proffitLossVO.setCrClsBal(tempRptVO.getOpeningBalance().add(tempRptVO.getDifferenceInCurrentBal()));
		}
		
		}catch(Exception e){
			log.error("Exception occurred in getProffitLossBalance "+e);
		}
		log.debug("Exit  : private AccountHeadVO getProffitLossBalance(String societyID,String fromDate,String uptoDate)"+proffitLossVO.getCrClsBal()+" "+proffitLossVO.getCrOpnBal()+" "+proffitLossVO.getDrClsBal()+" "+proffitLossVO.getDrOpnBal());
		return proffitLossVO;
	}
	
	
	public AccountHeadVO getConsolidatedProffitLossBalance(int societyID,String fromDate,String uptoDate,String bsOrTb,String formatType){
		AccountHeadVO proffitLossVO=new AccountHeadVO();
		BigDecimal differencial=BigDecimal.ZERO;
		log.debug("Entry  : private AccountHeadVO getConsolidatedProffitLossBalance(String societyID,String fromDate,String uptoDate,String formatType)");
		try{
		SocietyVO societyVo=societyServiceBean.getSocietyDetails(societyID);
		List societyList=societyServiceBean.getSocietyListInDatazone(societyVo.getDataZoneID());
		for(int i=0;societyList.size()>i;i++){
			SocietyVO societyVO=(SocietyVO) societyList.get(i);
		ReportVO rptVO=new ReportVO();
		ReportDetailsVO tempRptVO=new ReportDetailsVO();
		rptVO=reportService.getIncomeExpenseReport(societyVO.getSocietyID(), fromDate, uptoDate,0,formatType);
		
		tempRptVO=reportService.calculateProfitLossDetails(rptVO.getIncomeTxList(), rptVO.getExpenseTxList(), societyVO.getSocietyID(), fromDate, uptoDate,bsOrTb,formatType);
		
		if(societyVo.getOrgType().equalsIgnoreCase("S")){
		proffitLossVO.setStrAccountPrimaryHead("Income and Expenditure A/c");
		}else if(societyVo.getOrgType().equalsIgnoreCase("C")){
			proffitLossVO.setStrAccountPrimaryHead("Profit Loss A/c");
		}
		proffitLossVO.setClosingBalance(proffitLossVO.getClosingBalance().add(tempRptVO.getOpeningBalance()));
		proffitLossVO.setOpeningBalance(proffitLossVO.getOpeningBalance().add(tempRptVO.getClosingBalance()));
		log.debug("-------------------------->"+tempRptVO.getOpeningBalance()+"  "+tempRptVO.getClosingBalance()+"       "+tempRptVO.getDifferenceInCurrentBal());	
		differencial=differencial.add(tempRptVO.getDifferenceInCurrentBal());
		/*if(proffitLossVO.getOpeningBalance().compareTo(BigDecimal.ZERO) >= 0){
			//crOpnBal=openingBalance;
			proffitLossVO.setCrOpnBal(proffitLossVO.getOpeningBalance());
			}else
				//drOpnBal=openingBalance.abs();
				proffitLossVO.setDrOpnBal(proffitLossVO.getOpeningBalance().abs());
			if(proffitLossVO.getClosingBalance().compareTo(BigDecimal.ZERO)>=0){
				//crClsBal=closingBalance;
				proffitLossVO.setCrClsBal(proffitLossVO.getClosingBalance());
				
			}else
				//drClsBal=closingBalance.abs();
				proffitLossVO.setDrClsBal(proffitLossVO.getClosingBalance().abs());*/
		
		
		}
	
		if(proffitLossVO.getOpeningBalance().signum()<0){
			
			proffitLossVO.setDrOpnBal(proffitLossVO.getOpeningBalance().abs());
			proffitLossVO.setDrClsBal(proffitLossVO.getOpeningBalance().add(differencial.abs()).abs());
		}else{
			
		proffitLossVO.setCrOpnBal(proffitLossVO.getOpeningBalance());
		proffitLossVO.setCrClsBal(proffitLossVO.getOpeningBalance().add(differencial));
		}
		
	
		}catch(Exception e){
			log.error("Exception occurred in getConsolidatedProffitLossBalance "+e);
		}
		log.debug("Exit  : private AccountHeadVO getConsolidatedProffitLossBalance(String societyID,String fromDate,String uptoDate)"+proffitLossVO.getCrClsBal()+" "+proffitLossVO.getCrOpnBal()+" "+proffitLossVO.getDrClsBal()+" "+proffitLossVO.getDrOpnBal());
		return proffitLossVO;
	}
	
	
	public List getAccountLedgerList(int societyID,int subGroupID,String type){
		List ledgerList=null;
		try {
			log.debug("Entry : public List getAccountLedgerList(String societyID,String ledgerType)");

			ledgerList=getLedgerList(societyID,subGroupID,type);
			
		
			
						
			log.debug("Exit : public List getAccountLedgerList(String societyID,String ledgerType)"+ledgerList.size());
		} catch (Exception e) {
			log.error("Exception : public List getAccountLedgerList(String societyID,String ledgerType)"+e);
		}
		return ledgerList;
	}
	
	//This method is used to profit and loss ledger
	public AccountHeadVO getProfitLossLedger(int societyID){
		List ledgerList=null;
		AccountHeadVO profitLossVO=new AccountHeadVO();
		try {
			log.debug("Entry : public List getProfitLossLedger(String societyID)");

			ledgerList=ledgerDAO.getLedgersList(societyID,5,"R","A");
			
				profitLossVO=(AccountHeadVO) ledgerList.get(0);
				 
				 profitLossVO=unsignedOpeningClosingBalance(profitLossVO);
				 
			
			
			
						
			log.debug("Exit : public List getProfitLossLedger(String societyID)"+ledgerList.size());
		}catch (IndexOutOfBoundsException e) {
			log.info("No profitloss Ledger available for society ID "+societyID+" Exception "+e);
		
		} catch (Exception e) {
			log.error("Exception : public List getProfitLossLedger(String societyID) for Society ID"+societyID+"  "+e);
		}
		return profitLossVO;
	}
	
	
	
//	This method is used to give reverse effects to ledger balances while doing delete transaction
	private LedgerEntries giveReverseEffectToLedgerBalance(LedgerEntries ledgerEntry,String societyID){
		
		if(ledgerEntry.getCreditAction()==1){
			ledgerEntry.setCreditAction(0);
		}else
			ledgerEntry.setCreditAction(1);
		
		
		if(ledgerEntry.getDebitAction()==1){
			ledgerEntry.setDebitAction(0);
		
		}else 
			ledgerEntry.setDebitAction(1);
		return ledgerEntry;
	}
	
	
	public LedgerTrnsVO getLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate,String type){
		LedgerTrnsVO ledgerTransactionVO=new LedgerTrnsVO();
		AccountHeadVO accntHeadVO=new AccountHeadVO();
		AccountHeadVO bankVO=new AccountHeadVO();
		log.debug("Entry : public LedgerTrnsVO getLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate)");
		
		try {
			
			accntHeadVO=getOpeningClosingBalance(orgID, ledgerID, fromDate, toDate,"L" );
			List transactionList=transactionService.getTransctionList(orgID, ledgerID, fromDate, toDate, type);
			BigDecimal balance=BigDecimal.ZERO;
			BigDecimal principleBalance=BigDecimal.ZERO;
			if(transactionList.size()>0){
			TransactionVO txBaseVO=(TransactionVO) transactionList.get(0);
			balance=accntHeadVO.getOpeningBalance().add(txBaseVO.getAmount());
			principleBalance=balance.subtract(accntHeadVO.getIntOpnBalance());
			for(int i=0;transactionList.size()>i;i++){
				
				TransactionVO txVO=(TransactionVO) transactionList.get(i);
				
				if(i==0){
					principleBalance=principleBalance.subtract(txVO.getIntAmount());
					txVO.setBalance(balance);
					txVO.setPrincipleAmount(principleBalance);
				}else{
				
				balance=balance.add(txVO.getAmount());
				txVO.setBalance(balance);
				principleBalance=principleBalance.add(txVO.getAmount());
				principleBalance=principleBalance.subtract(txVO.getIntAmount());
				txVO.setPrincipleAmount(principleBalance);
				
				}
				
			}
			}
			if(accntHeadVO.getAccountGroupID()==4)
			bankVO=getReconciledBalance(orgID,ledgerID,fromDate,toDate);
			
			
			ledgerTransactionVO.setOrgID(orgID);
			ledgerTransactionVO.setLedgerID(ledgerID);
			if(accntHeadVO.getStrAccountHeadName()!=null){
			ledgerTransactionVO.setLedgerName(accntHeadVO.getStrAccountHeadName().trim());
			ledgerTransactionVO.setGroupName(accntHeadVO.getStrAccountPrimaryHead().trim());
			}ledgerTransactionVO.setGroupID(accntHeadVO.getAccountGroupID());
			ledgerTransactionVO.setRootID(accntHeadVO.getRootID());
			ledgerTransactionVO.setFromDate(fromDate);
			ledgerTransactionVO.setToDate(toDate);
			ledgerTransactionVO.setOpeningBalance(accntHeadVO.getOpeningBalance());
			ledgerTransactionVO.setDrOpnBalance(accntHeadVO.getDrOpnBal());
			ledgerTransactionVO.setCrOpnBalance(accntHeadVO.getCrOpnBal());
			ledgerTransactionVO.setClosingBalance(accntHeadVO.getClosingBalance());
			ledgerTransactionVO.setDrClsBalance(accntHeadVO.getDrClsBal());
			ledgerTransactionVO.setCrClsBalance(accntHeadVO.getCrClsBal());
			ledgerTransactionVO.setTransactionList(transactionList);
			ledgerTransactionVO.setRecBalance(bankVO.getClosingBalance());
			
						
			
			
			
			
		}catch (NullPointerException e) {
			log.info("Exception at public LedgerTrnsVO getLedgerTransactionList for "+orgID+" ledgerID "+ledgerID+" from date is "+fromDate+" and uptoDate  "+toDate+" "+e);
						
			
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getLedgerTransactionList "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate)");
		return ledgerTransactionVO;
	}
	
	public LedgerTrnsVO getQuickLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate,String type){
		LedgerTrnsVO ledgerTransactionVO=new LedgerTrnsVO();
		AccountHeadVO accntHeadVO=new AccountHeadVO();
		AccountHeadVO bankVO=new AccountHeadVO();
		log.debug("Entry : public LedgerTrnsVO getQuickLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate)");
		
		try {
			
			accntHeadVO=getOpeningClosingBalance(orgID, ledgerID, fromDate, toDate,"L" );
			List transactionList=transactionService.getQuickTransctionList(orgID, ledgerID, fromDate, toDate, "all");
			BigDecimal balance=BigDecimal.ZERO;
			BigDecimal principleBalance=BigDecimal.ZERO;
			BigDecimal intBalance=BigDecimal.ZERO;
			if(transactionList.size()>0){
			TransactionVO txBaseVO=(TransactionVO) transactionList.get(0);
			balance=accntHeadVO.getOpeningBalance().add(txBaseVO.getAmount());
			principleBalance=balance.subtract(accntHeadVO.getIntOpnBalance());
			intBalance=accntHeadVO.getIntOpnBalance();
			txBaseVO.setIntBalance(intBalance);
			for(int i=0;transactionList.size()>i;i++){
				
				TransactionVO txVO=(TransactionVO) transactionList.get(i);
				
				if(i==0){
					if(txVO.getIntAmount().compareTo(BigDecimal.ZERO)==0){
						principleBalance=principleBalance.subtract(txVO.getIntAmount());
					}else
					principleBalance=balance.subtract(txVO.getIntAmount());
					intBalance=intBalance.add(txVO.getIntAmount());
					txVO.setBalance(balance);
					txVO.setPrincipleAmount(principleBalance);
					txVO.setIntBalance(intBalance);
				}else{
				
				balance=balance.add(txVO.getAmount());
				txVO.setBalance(balance);
				principleBalance=principleBalance.add(txVO.getAmount());
				principleBalance=principleBalance.subtract(txVO.getIntAmount());
				intBalance=intBalance.add(txVO.getIntAmount());
				txVO.setPrincipleAmount(principleBalance);
				txVO.setIntBalance(intBalance);
				
				}
				
			}
			}
			if(accntHeadVO.getAccountGroupID()==4)
			bankVO=getReconciledBalance(orgID,ledgerID,fromDate,toDate);
			
			
			ledgerTransactionVO.setOrgID(orgID);
			ledgerTransactionVO.setLedgerID(ledgerID);
			if(accntHeadVO.getStrAccountHeadName()!=null){
			ledgerTransactionVO.setLedgerName(accntHeadVO.getStrAccountHeadName().trim());
			ledgerTransactionVO.setGroupName(accntHeadVO.getStrAccountPrimaryHead().trim());
			}ledgerTransactionVO.setGroupID(accntHeadVO.getAccountGroupID());
			ledgerTransactionVO.setRootID(accntHeadVO.getRootID());
			ledgerTransactionVO.setFromDate(fromDate);
			ledgerTransactionVO.setToDate(toDate);
			ledgerTransactionVO.setOpeningBalance(accntHeadVO.getOpeningBalance());
			ledgerTransactionVO.setDrOpnBalance(accntHeadVO.getDrOpnBal());
			ledgerTransactionVO.setCrOpnBalance(accntHeadVO.getCrOpnBal());
			ledgerTransactionVO.setClosingBalance(accntHeadVO.getClosingBalance());
			ledgerTransactionVO.setInterestBalance(accntHeadVO.getIntClsBalance());
			ledgerTransactionVO.setIntOpeningBalance(accntHeadVO.getIntOpnBalance());
			ledgerTransactionVO.setIntClosingBalance(accntHeadVO.getIntClsBalance());
			ledgerTransactionVO.setDrClsBalance(accntHeadVO.getDrClsBal());
			ledgerTransactionVO.setCrClsBalance(accntHeadVO.getCrClsBal());
			ledgerTransactionVO.setTransactionList(transactionList);
			ledgerTransactionVO.setRecBalance(bankVO.getClosingBalance());
			
						
			
			
			
			
		}catch (NullPointerException e) {
			log.info("Exception at public LedgerTrnsVO getQuickLedgerTransactionList for "+orgID+" ledgerID "+ledgerID+" from date is "+fromDate+" and uptoDate  "+toDate+" "+e);
						
			
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getQuickLedgerTransactionList "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getQuickLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate)");
		return ledgerTransactionVO;
	}
	
	
	public LedgerTrnsVO getTransactionListOfProjects(int orgID,int ledgerID,String fromDate,String toDate,String projectName){
		LedgerTrnsVO ledgerTransactionVO=new LedgerTrnsVO();
		AccountHeadVO accntHeadVO=new AccountHeadVO();
		AccountHeadVO bankVO=new AccountHeadVO();
		log.debug("Entry : public LedgerTrnsVO getTransactionListOfProjects(int orgID,int ledgerID,String fromDate,String toDate)");
		
		try {
			
			accntHeadVO=getOpeningClosingBalance(orgID, ledgerID, fromDate, toDate,"L" );
			List transactionList=transactionService.getTransactionListOfProjects(orgID, ledgerID, fromDate, toDate, projectName);
			BigDecimal balance=BigDecimal.ZERO;
			BigDecimal principleBalance=BigDecimal.ZERO;
			if(transactionList.size()>0){
			TransactionVO txBaseVO=(TransactionVO) transactionList.get(0);
			balance=accntHeadVO.getOpeningBalance().add(txBaseVO.getAmount());
			principleBalance=balance.subtract(accntHeadVO.getIntOpnBalance());
			for(int i=0;transactionList.size()>i;i++){
				
				TransactionVO txVO=(TransactionVO) transactionList.get(i);
				
				if(i==0){
					principleBalance=principleBalance.subtract(txVO.getIntAmount());
					txVO.setBalance(balance);
					txVO.setPrincipleAmount(principleBalance);
				}else{
				
				balance=balance.add(txVO.getAmount());
				txVO.setBalance(balance);
				principleBalance=principleBalance.add(txVO.getAmount());
				principleBalance=principleBalance.subtract(txVO.getIntAmount());
				txVO.setPrincipleAmount(principleBalance);
				
				}
				
			}
			}
			if(accntHeadVO.getAccountGroupID()==4)
			bankVO=getReconciledBalance(orgID,ledgerID,fromDate,toDate);
			
			
			ledgerTransactionVO.setOrgID(orgID);
			ledgerTransactionVO.setLedgerID(ledgerID);
			if(accntHeadVO.getStrAccountHeadName()!=null){
			ledgerTransactionVO.setLedgerName(accntHeadVO.getStrAccountHeadName().trim());
			ledgerTransactionVO.setGroupName(accntHeadVO.getStrAccountPrimaryHead().trim());
			}ledgerTransactionVO.setGroupID(accntHeadVO.getAccountGroupID());
			ledgerTransactionVO.setRootID(accntHeadVO.getRootID());
			ledgerTransactionVO.setFromDate(fromDate);
			ledgerTransactionVO.setToDate(toDate);
			ledgerTransactionVO.setOpeningBalance(accntHeadVO.getOpeningBalance());
			ledgerTransactionVO.setDrOpnBalance(accntHeadVO.getDrOpnBal());
			ledgerTransactionVO.setCrOpnBalance(accntHeadVO.getCrOpnBal());
			ledgerTransactionVO.setClosingBalance(accntHeadVO.getClosingBalance());
			ledgerTransactionVO.setDrClsBalance(accntHeadVO.getDrClsBal());
			ledgerTransactionVO.setCrClsBalance(accntHeadVO.getCrClsBal());
			ledgerTransactionVO.setTransactionList(transactionList);
			ledgerTransactionVO.setRecBalance(bankVO.getClosingBalance());
			
						
			
			
			
			
		}catch (NullPointerException e) {
			log.info("Exception at public LedgerTrnsVO getTransactionListOfProjects for "+orgID+" ledgerID "+ledgerID+" from date is "+fromDate+" and uptoDate  "+toDate+" "+e);
						
			
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getTransactionListOfProjects "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getTransactionListOfProjects(int orgID,int ledgerID,String fromDate,String toDate)");
		return ledgerTransactionVO;
	}
	
	public LedgerTrnsVO getTransactionListOfProjectsWithCategory(int orgID,int ledgerID,String fromDate,String toDate,int categoryID){
		LedgerTrnsVO ledgerTransactionVO=new LedgerTrnsVO();
		AccountHeadVO accntHeadVO=new AccountHeadVO();
		AccountHeadVO bankVO=new AccountHeadVO();
		log.debug("Entry : public LedgerTrnsVO getTransactionListOfProjects(int orgID,int ledgerID,String fromDate,String toDate)");
		
		try {
			List categoryList=projectDetailsService.getCostCeneterCategoryLinkingList(categoryID);
			accntHeadVO=getOpeningClosingBalance(orgID, ledgerID, fromDate, toDate,"L" );
			List transactionList=transactionService.getTransactionListOfCategoryWise(orgID, ledgerID, fromDate, toDate, categoryList);
			BigDecimal balance=BigDecimal.ZERO;
			BigDecimal principleBalance=BigDecimal.ZERO;
			if(transactionList.size()>0){
			TransactionVO txBaseVO=(TransactionVO) transactionList.get(0);
			balance=accntHeadVO.getOpeningBalance().add(txBaseVO.getAmount());
			principleBalance=balance.subtract(accntHeadVO.getIntOpnBalance());
			for(int i=0;transactionList.size()>i;i++){
				
				TransactionVO txVO=(TransactionVO) transactionList.get(i);
				
				if(i==0){
					principleBalance=principleBalance.subtract(txVO.getIntAmount());
					txVO.setBalance(balance);
					txVO.setPrincipleAmount(principleBalance);
				}else{
				
				balance=balance.add(txVO.getAmount());
				txVO.setBalance(balance);
				principleBalance=principleBalance.add(txVO.getAmount());
				principleBalance=principleBalance.subtract(txVO.getIntAmount());
				txVO.setPrincipleAmount(principleBalance);
				
				}
				
			}
			}
			if(accntHeadVO.getAccountGroupID()==4)
			bankVO=getReconciledBalance(orgID,ledgerID,fromDate,toDate);
			
			
			ledgerTransactionVO.setOrgID(orgID);
			ledgerTransactionVO.setLedgerID(ledgerID);
			if(accntHeadVO.getStrAccountHeadName()!=null){
			ledgerTransactionVO.setLedgerName(accntHeadVO.getStrAccountHeadName().trim());
			ledgerTransactionVO.setGroupName(accntHeadVO.getStrAccountPrimaryHead().trim());
			}ledgerTransactionVO.setGroupID(accntHeadVO.getAccountGroupID());
			ledgerTransactionVO.setRootID(accntHeadVO.getRootID());
			ledgerTransactionVO.setFromDate(fromDate);
			ledgerTransactionVO.setToDate(toDate);
			ledgerTransactionVO.setOpeningBalance(accntHeadVO.getOpeningBalance());
			ledgerTransactionVO.setDrOpnBalance(accntHeadVO.getDrOpnBal());
			ledgerTransactionVO.setCrOpnBalance(accntHeadVO.getCrOpnBal());
			ledgerTransactionVO.setClosingBalance(accntHeadVO.getClosingBalance());
			ledgerTransactionVO.setDrClsBalance(accntHeadVO.getDrClsBal());
			ledgerTransactionVO.setCrClsBalance(accntHeadVO.getCrClsBal());
			ledgerTransactionVO.setTransactionList(transactionList);
			ledgerTransactionVO.setRecBalance(bankVO.getClosingBalance());
			
						
			
			
			
			
		}catch (NullPointerException e) {
			log.info("Exception at public LedgerTrnsVO getTransactionListOfProjects for "+orgID+" ledgerID "+ledgerID+" from date is "+fromDate+" and uptoDate  "+toDate+" "+e);
						
			
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getTransactionListOfProjects "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getTransactionListOfProjects(int orgID,int ledgerID,String fromDate,String toDate)");
		return ledgerTransactionVO;
	}
	
	/*Reconcilation method*/
	public LedgerTrnsVO getReconcilationList(int orgID,int ledgerID,String fromDate,String toDate,String type){
		LedgerTrnsVO ledgerTransactionVO=new LedgerTrnsVO();
		AccountHeadVO accntHeadVO=new AccountHeadVO();
		AccountHeadVO bankVO=new AccountHeadVO();
		log.debug("Entry : public LedgerTrnsVO getReconcilationList(int orgID,String fromDate,String toDate)");
		
		try {
			
			if(type.equalsIgnoreCase("UL")){
				ledgerTransactionVO=getUnReconcilationTransactionList(orgID, ledgerID, fromDate, toDate, type);
				
			}else if (type.equalsIgnoreCase("RL")){
				ledgerTransactionVO=getReconcilationTransactionList(orgID, ledgerID, fromDate, toDate, type);
				
			}
					
			
		}catch (NullPointerException e) {
			log.info("Exception at public LedgerTrnsVO getReconcilationList for "+orgID+" ledgerID "+ledgerID+" from date is "+fromDate+" and uptoDate  "+toDate+" "+e);
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getReconcilationList "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getReconcilationList(int orgID,int ledgerID,String fromDate,String toDate)");
		return ledgerTransactionVO;
	}	
	
	/* Get unreconciled transaction*/
	public LedgerTrnsVO getUnReconcilationTransactionList(int orgID,int ledgerID,String fromDate,String toDate,String type){
		LedgerTrnsVO ledgerTransactionVO=new LedgerTrnsVO();
		AccountHeadVO accntHeadVO=new AccountHeadVO();
		AccountHeadVO bankVO=new AccountHeadVO();
		log.debug("Entry : public LedgerTrnsVO getUnReconcilationTransactionList(int orgID,int ledgerID,String fromDate,String toDate,String type)");
		try {
			
			//accntHeadVO=getOpeningClosingBalance(orgID, ledgerID, fromDate, toDate,"L" );
			List transactionList=transactionService.getReconcilationList(orgID, ledgerID, fromDate, toDate, type);
			/*BigDecimal balance=BigDecimal.ZERO;
			if(transactionList.size()>0){
			TransactionVO txBaseVO=(TransactionVO) transactionList.get(0);
			balance=accntHeadVO.getOpeningBalance().add(txBaseVO.getAmount());
			for(int i=0;transactionList.size()>i;i++){
				
				TransactionVO txVO=(TransactionVO) transactionList.get(i);
				
				if(i==0){
					
					txVO.setBalance(balance);
					
				}else{
				balance=balance.add(txVO.getAmount());
				txVO.setBalance(balance);
				
				
				}
				
			}
			}
			if(accntHeadVO.getAccountGroupID()==4)
			bankVO=getReconciledBalance(orgID,ledgerID,fromDate,toDate);*/
			
			
			ledgerTransactionVO.setOrgID(orgID);
			ledgerTransactionVO.setLedgerID(ledgerID);
			/*if(accntHeadVO.getStrAccountHeadName()!=null){
			ledgerTransactionVO.setLedgerName(accntHeadVO.getStrAccountHeadName().trim());
			ledgerTransactionVO.setGroupName(accntHeadVO.getStrAccountPrimaryHead().trim());
			}ledgerTransactionVO.setGroupID(accntHeadVO.getAccountGroupID());
			ledgerTransactionVO.setRootID(accntHeadVO.getRootID());
			ledgerTransactionVO.setFromDate(fromDate);
			ledgerTransactionVO.setToDate(toDate);
			ledgerTransactionVO.setOpeningBalance(accntHeadVO.getOpeningBalance());
			ledgerTransactionVO.setDrOpnBalance(accntHeadVO.getDrOpnBal());
			ledgerTransactionVO.setCrOpnBalance(accntHeadVO.getCrOpnBal());
			ledgerTransactionVO.setClosingBalance(accntHeadVO.getClosingBalance());
			ledgerTransactionVO.setDrClsBalance(accntHeadVO.getDrClsBal());
			ledgerTransactionVO.setCrClsBalance(accntHeadVO.getCrClsBal());
			ledgerTransactionVO.setRecBalance(bankVO.getClosingBalance());	*/	
			ledgerTransactionVO.setTransactionList(transactionList);
			
			
			
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getUnReconcilationTransactionList "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getUnReconcilationTransactionList(int orgID,int ledgerID,String fromDate,String toDate)");
		return ledgerTransactionVO;
	}
	
	/* Get reconciled transaction list*/
	public LedgerTrnsVO getReconcilationTransactionList(int orgID,int ledgerID,String fromDate,String toDate,String type){
		LedgerTrnsVO ledgerTransactionVO=new LedgerTrnsVO();
		AccountHeadVO accntHeadVO=new AccountHeadVO();
		AccountHeadVO bankVO=new AccountHeadVO();
		AccountHeadVO bankVO1=new AccountHeadVO();
		java.sql.Date firstDate = null;
		String fyFirstDay="";
		log.debug("Entry : public LedgerTrnsVO getReconcilationTransactionList(int orgID,int ledgerID,String fromDate,String toDate,String type)");
		try {
			
			DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");	           
	        LocalDate frmDt = formatter.parseLocalDate(fromDate);		       
	        int monthFrmDt=frmDt.getMonthOfYear();
	        int yearFrmDt=frmDt.getYear();
		
	        fyFirstDay= dateUtil.getPrevOrNextDate(fromDate, 1);
			log.debug("fyFirstDay : "+ fyFirstDay);
  		    accntHeadVO=getReconciledClosingBalance(orgID, ledgerID, fyFirstDay, fyFirstDay,"L" );
						
			log.debug(fyFirstDay+" Opening balance: "+accntHeadVO.getClosingBalance());
			List transactionList=transactionService.getReconcilationList(orgID, ledgerID, fromDate, toDate, type);
			
			//Balance opening
			BigDecimal balance=accntHeadVO.getClosingBalance();
			log.debug("Balance: "+balance+" Transaction list"+transactionList.size());
			
			if(transactionList.size()>0){
			
			  TransactionVO txBaseVO=(TransactionVO) transactionList.get(0);
			  balance=balance.add(txBaseVO.getAmount());
			  
			  for(int i=0;transactionList.size()>i;i++){
				
				TransactionVO txVO=(TransactionVO) transactionList.get(i);
				
				if(i==0){
					
					txVO.setBalance(balance);
					
				}else{
					
				  balance=balance.add(txVO.getAmount());
				  txVO.setBalance(balance);				
				}
				 int lastIndex=transactionList.size()-1;
				   if(i==lastIndex)
				    	ledgerTransactionVO.setRecBalance(balance);
				
			  }
			
			}else{
				ledgerTransactionVO.setRecBalance(balance);
			}
			
			
			
			ledgerTransactionVO.setOrgID(orgID);
			ledgerTransactionVO.setLedgerID(ledgerID);
			if(accntHeadVO.getStrAccountHeadName()!=null){
			  ledgerTransactionVO.setLedgerName(accntHeadVO.getStrAccountHeadName().trim());
			  ledgerTransactionVO.setGroupName(accntHeadVO.getStrAccountPrimaryHead().trim());
			}ledgerTransactionVO.setGroupID(accntHeadVO.getAccountGroupID());
			ledgerTransactionVO.setRootID(accntHeadVO.getRootID());
			ledgerTransactionVO.setFromDate(fromDate);
			ledgerTransactionVO.setToDate(toDate);
			ledgerTransactionVO.setOpeningBalance(accntHeadVO.getOpeningBalance());
			ledgerTransactionVO.setDrOpnBalance(accntHeadVO.getDrOpnBal());
			ledgerTransactionVO.setCrOpnBalance(accntHeadVO.getCrOpnBal());
			ledgerTransactionVO.setClosingBalance(accntHeadVO.getClosingBalance());
			ledgerTransactionVO.setDrClsBalance(accntHeadVO.getDrClsBal());
			ledgerTransactionVO.setCrClsBalance(accntHeadVO.getCrClsBal());
			ledgerTransactionVO.setTransactionList(transactionList);
			
			
			
			
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getReconcilationTransactionList "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getReconcilationTransactionList(int orgID,int ledgerID,String fromDate,String toDate)");
		return ledgerTransactionVO;
	}
	
	public LedgerTrnsVO getProfitLossLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate,String type){
		LedgerTrnsVO ledgerTransactionVO=new LedgerTrnsVO();
		AccountHeadVO accntHeadVO=new AccountHeadVO();
		AccountHeadVO bankVO=new AccountHeadVO();
		log.debug("Entry : public LedgerTrnsVO getLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate)");
		
		try {
			
			accntHeadVO=getProffitLossBalance(orgID, fromDate, toDate, "tb",0,"S");
			List transactionList=transactionService.getTransctionList(orgID, ledgerID, fromDate, toDate, type);
			BigDecimal balance=BigDecimal.ZERO;
			if(transactionList.size()>0){
			TransactionVO txBaseVO=(TransactionVO) transactionList.get(0);
			balance=accntHeadVO.getOpeningBalance().add(txBaseVO.getAmount());
			for(int i=0;transactionList.size()>i;i++){
				
				TransactionVO txVO=(TransactionVO) transactionList.get(i);
				
				if(i==0){
					
					txVO.setBalance(balance);
					
				}else{
				balance=balance.add(txVO.getAmount());
				txVO.setBalance(balance);
				
				
				}
				
			}
			}
			if(accntHeadVO.getAccountGroupID()==4)
			bankVO=getReconciledBalance(orgID,ledgerID,fromDate,toDate);
			
			
			ledgerTransactionVO.setOrgID(orgID);
			ledgerTransactionVO.setLedgerID(ledgerID);
			if(accntHeadVO.getStrAccountHeadName()!=null){
			ledgerTransactionVO.setLedgerName(accntHeadVO.getStrAccountHeadName().trim());
			ledgerTransactionVO.setGroupName(accntHeadVO.getStrAccountPrimaryHead().trim());
			}ledgerTransactionVO.setGroupID(accntHeadVO.getAccountGroupID());
			ledgerTransactionVO.setRootID(accntHeadVO.getRootID());
			ledgerTransactionVO.setFromDate(fromDate);
			ledgerTransactionVO.setToDate(toDate);
			ledgerTransactionVO.setOpeningBalance(accntHeadVO.getOpeningBalance());
			ledgerTransactionVO.setDrOpnBalance(accntHeadVO.getDrOpnBal());
			ledgerTransactionVO.setCrOpnBalance(accntHeadVO.getCrOpnBal());
			ledgerTransactionVO.setClosingBalance(accntHeadVO.getClosingBalance());
			ledgerTransactionVO.setDrClsBalance(accntHeadVO.getDrClsBal());
			ledgerTransactionVO.setCrClsBalance(accntHeadVO.getCrClsBal());
			ledgerTransactionVO.setTransactionList(transactionList);
			ledgerTransactionVO.setRecBalance(bankVO.getClosingBalance());
			
						
			
			
			
			
		}catch (NullPointerException e) {
			log.info("Exception at public LedgerTrnsVO getLedgerTransactionList for "+orgID+" ledgerID "+ledgerID+" from date is "+fromDate+" and uptoDate  "+toDate+" "+e);
						
			
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getLedgerTransactionList for "+orgID+" ledgerID "+ledgerID+" from date is "+fromDate+" and uptoDate  "+toDate+" "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getLedgerTransactionList(int orgID,int ledgerID,String fromDate,String toDate)");
		return ledgerTransactionVO;
	}
	
	public LedgerTrnsVO getLedgerTransactionListSubAccounts(int orgID,int ledgerID,String fromDate,String toDate,String type){
		LedgerTrnsVO ledgerTransactionVO=null;
		List subAccountList=new ArrayList();
		AccountHeadVO accntHeadVO=new AccountHeadVO();
		BigDecimal sumOfOpeningBalance=BigDecimal.ZERO;
		BigDecimal sumOfClosingBalance=BigDecimal.ZERO;
		BigDecimal sumOfCrOpnBalance=BigDecimal.ZERO;
		BigDecimal sumOfDrOpnBalance=BigDecimal.ZERO;
		BigDecimal sumOfCrClsBalance=BigDecimal.ZERO;
		BigDecimal sumOfDrClsBalance=BigDecimal.ZERO;
		List transactionList=new ArrayList();
		log.debug("Entry : public LedgerTrnsVO getLedgerTransactionListSubAccounts(int orgID,int ledgerID,String fromDate,String toDate)");
		
		try {
			accntHeadVO=getOpeningClosingBalance(orgID, ledgerID, fromDate, toDate,"L" );
			subAccountList=getSubAccountList(orgID, ledgerID);
			if(subAccountList.size()>0){
				for(int i=0;subAccountList.size()>i;i++){
					AccountHeadVO achVO=(AccountHeadVO) subAccountList.get(i);
					LedgerTrnsVO ledgerTrnsVO=getLedgerTransactionList(orgID, achVO.getLedgerID(), fromDate, toDate, type);
					
					sumOfClosingBalance=sumOfClosingBalance.add(ledgerTrnsVO.getClosingBalance());
					sumOfOpeningBalance=sumOfOpeningBalance.add(ledgerTrnsVO.getOpeningBalance());
					sumOfCrOpnBalance=sumOfCrOpnBalance.add(ledgerTrnsVO.getCrOpnBalance());
					sumOfDrOpnBalance=sumOfDrOpnBalance.add(ledgerTrnsVO.getDrOpnBalance());
					sumOfCrClsBalance=sumOfCrClsBalance.add(ledgerTrnsVO.getCrClsBalance());
					sumOfDrClsBalance=sumOfDrClsBalance.add(ledgerTrnsVO.getDrClsBalance());
					transactionList.add(ledgerTrnsVO.getTransactionList());
					
				}
			}
			
			ledgerTransactionVO.setOrgID(orgID);
			ledgerTransactionVO.setLedgerID(ledgerID);
			if(accntHeadVO.getStrAccountHeadName()!=null){
			ledgerTransactionVO.setLedgerName(accntHeadVO.getStrAccountHeadName().trim());
			ledgerTransactionVO.setGroupName(accntHeadVO.getStrAccountPrimaryHead().trim());
			}ledgerTransactionVO.setGroupID(accntHeadVO.getAccountGroupID());
			ledgerTransactionVO.setRootID(accntHeadVO.getRootID());
			ledgerTransactionVO.setFromDate(fromDate);
			ledgerTransactionVO.setToDate(toDate);
			ledgerTransactionVO.setOpeningBalance(sumOfOpeningBalance);
			ledgerTransactionVO.setDrOpnBalance(sumOfDrOpnBalance);
			ledgerTransactionVO.setCrOpnBalance(sumOfCrOpnBalance);
			ledgerTransactionVO.setClosingBalance(sumOfClosingBalance);
			ledgerTransactionVO.setDrClsBalance(sumOfDrClsBalance);
			ledgerTransactionVO.setCrClsBalance(sumOfCrClsBalance);
			ledgerTransactionVO.setTransactionList(transactionList);
			
			
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getLedgerTransactionListSubAccounts "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getLedgerTransactionListSubAccounts(int orgID,int ledgerID,String fromDate,String toDate)");
		return ledgerTransactionVO;
	}
	
	public List getSubAccountList(int orgID,int parentID){
		List ledgerList=null;
		try {
			log.debug("Entry : public List getSubAccountList(int orgID,int parentID)");

			ledgerList=ledgerDAO.getSubAccountList(orgID,parentID);
			
								
			log.debug("Exit : public List getSubAccountList(int orgID,int parentID)"+ledgerList.size());
		} catch (Exception e) {
			log.error("Exception : public List getSubAccountList(int orgID,int parentID)"+e);
		}
		return ledgerList;
	}
	
	public LedgerTrnsVO getBillTransactionList(int orgID,int ledgerID,String fromDate,String toDate,String type){
		LedgerTrnsVO ledgerTransactionVO=new LedgerTrnsVO();
		AccountHeadVO accntHeadVO=new AccountHeadVO();
		AccountHeadVO bankVO=new AccountHeadVO();
		log.debug("Entry : public LedgerTrnsVO getBillTransactionList(int orgID,int ledgerID,String fromDate,String toDate)");
		
		try {
			
			accntHeadVO=getOpeningClosingBalance(orgID, ledgerID, fromDate, toDate,"L" );
			List transactionList=transactionService.getBillTransactionList(orgID, ledgerID, fromDate, toDate, type);
			BigDecimal balance=BigDecimal.ZERO;
			BigDecimal total=BigDecimal.ZERO;
			BigDecimal receiptTotal=BigDecimal.ZERO;
			BigDecimal chargeTotal=BigDecimal.ZERO;
			
			if(transactionList.size()>0){
			TransactionVO txBaseVO=(TransactionVO) transactionList.get(0);
			balance=accntHeadVO.getOpeningBalance().add(txBaseVO.getAmount());
			for(int i=0;transactionList.size()>i;i++){
				
				TransactionVO txVO=(TransactionVO) transactionList.get(i);
				
				if(txVO.getLedgerID()==ledgerID){
					transactionList.remove(i);
					--i;
					continue;
				}
								
				if((!txVO.getTransactionType().equalsIgnoreCase("Receipt")&&(txVO.getCreditDebitFlag().equalsIgnoreCase("C")))){
					if(txVO.getAmount().intValue()>0)
					txVO.setAmount(txVO.getAmount().negate());
					
				}if((txVO.getGroupID()==2)&&(txVO.getCreditDebitFlag().equalsIgnoreCase("D"))){
					txVO.setAmount(txVO.getAmount().negate());
				}
				
				if(txVO.getTransactionType().equalsIgnoreCase("Receipt")){
					chargeTotal=chargeTotal.add(txVO.getAmount());
					transactionList.remove(i);
					--i;
					
				}else{
					if(txVO.getAmount().compareTo(BigDecimal.ZERO)==0){
						transactionList.remove(i);
						--i;
					}
					receiptTotal=receiptTotal.add(txVO.getAmount());
					
				}
				
			}
			}
			if(accntHeadVO.getAccountGroupID()==4)
			bankVO=getReconciledBalance(orgID,ledgerID,fromDate,toDate);
			
			ledgerTransactionVO.setDescription(accntHeadVO.getDescription());
			ledgerTransactionVO.setOrgID(orgID);
			ledgerTransactionVO.setTotalAmount(total);
			ledgerTransactionVO.setLedgerID(ledgerID);
			if(accntHeadVO.getStrAccountHeadName()!=null){
			ledgerTransactionVO.setLedgerName(accntHeadVO.getStrAccountHeadName().trim());
			ledgerTransactionVO.setGroupName(accntHeadVO.getStrAccountPrimaryHead().trim());
			}ledgerTransactionVO.setGroupID(accntHeadVO.getAccountGroupID());
			ledgerTransactionVO.setRootID(accntHeadVO.getRootID());
			ledgerTransactionVO.setFromDate(fromDate);
			ledgerTransactionVO.setToDate(toDate);
			ledgerTransactionVO.setOpeningBalance(accntHeadVO.getOpeningBalance());
			ledgerTransactionVO.setDrOpnBalance(chargeTotal);
			ledgerTransactionVO.setCrOpnBalance(receiptTotal);
			ledgerTransactionVO.setClosingBalance(accntHeadVO.getClosingBalance());
			ledgerTransactionVO.setDrClsBalance(accntHeadVO.getDrClsBal());
			ledgerTransactionVO.setCrClsBalance(accntHeadVO.getCrClsBal());
			ledgerTransactionVO.setPrincipleBalance(accntHeadVO.getClosingBalance().subtract(accntHeadVO.getIntClsBalance()));
			ledgerTransactionVO.setInterestBalance(accntHeadVO.getIntClsBalance());
			ledgerTransactionVO.setTransactionList(transactionList);
			ledgerTransactionVO.setRecBalance(bankVO.getClosingBalance());	
			
			
		}catch (NullPointerException e) {
			log.info("Exception at public LedgerTrnsVO getBillTransactionList for "+orgID+" ledgerID "+ledgerID+" from date is "+fromDate+" and uptoDate  "+toDate+" "+e);
						
			
		} catch (Exception e) {
			log.error("Exception at public LedgerTrnsVO getBillTransactionList "+e);
		}		
		log.debug("Exit : public LedgerTrnsVO getBillTransactionList(int orgID,int ledgerID,String fromDate,String toDate)"+ledgerTransactionVO.getTransactionList());
		return ledgerTransactionVO;
	}
	
	public AccountHeadVO getGroupBalance(int orgID,int groupID,String fromDate,String toDate){
		AccountHeadVO achVO=new AccountHeadVO();
		List ledgerList=new ArrayList<AccountHeadVO>();
		BigDecimal openingBalance=BigDecimal.ZERO;
		BigDecimal closingBalance=BigDecimal.ZERO;
		log.debug("Entry : public AccountHeadVO getGroupBalance(int orgID,int groupID,String fromDate,String toDate)");
		
		try {
			ledgerList = trialBalanceService.getLedgerReport(orgID, fromDate, toDate, groupID,0);
			
			if(ledgerList.size()>0){
				for(int i=0;ledgerList.size()>i;i++){
				 AccountHeadVO ldgrVO=(AccountHeadVO) ledgerList.get(i);
				 openingBalance=openingBalance.add(ldgrVO.getOpeningBalance());
				 closingBalance=closingBalance.add(ldgrVO.getClosingBalance());
				 achVO.setAccountGroupID(ldgrVO.getAccountGroupID());
				 achVO.setStrAccountPrimaryHead(ldgrVO.getStrAccountPrimaryHead());
				 
				}
			}
			
			achVO.setOpeningBalance(openingBalance);
			achVO.setClosingBalance(closingBalance);
		} catch (Exception e) {
			log.error("Exception at public AccountHeadVO getGroupBalance "+e);
		}		
		log.debug("Exit : public AccountHeadVO getGroupBalance(int orgID,int groupID,String fromDate,String toDate)");
		return achVO;
	}
	
	public AccountHeadVO getReconciledBalance(int orgID,int ledgerID,String fromDate,String toDate){
		AccountHeadVO achVO=new AccountHeadVO();
		
		BigDecimal openingBalance=BigDecimal.ZERO;
		BigDecimal closingBalance=BigDecimal.ZERO;
		log.debug("Entry : public AccountHeadVO getReconciledBalance(int orgID,int groupID,String fromDate,String toDate)");
		
		try {
			List ldgrList = ledgerDAO.getReconciledBalance(orgID,ledgerID,fromDate,toDate);
			if(ldgrList.size()>0){
			 achVO=(AccountHeadVO) ldgrList.get(0);
			}
						
		} catch (Exception e) {
			log.error("Exception at public AccountHeadVO getReconciledBalance for ledger "+ledgerID+" with societyID "+orgID+" and for the period "+fromDate+" to "+toDate+" ."+e);
			
		}		
		log.debug("Exit : public AccountHeadVO getReconciledBalance(int orgID,int groupID,String fromDate,String toDate)");
		return achVO;
	}
	
	
	public AccountHeadVO getLedgerByType(int orgID,String type){
		AccountHeadVO achVO=new AccountHeadVO();
		log.debug("Entry : public AccountHeadVO getLedgerByType(int orgID)");
		
		try {
			achVO = ledgerDAO.getLedgerByType(orgID,type);
		} catch (Exception e) {
			log.error("Exception at public AccountHeadVO getLedgerByType "+e);
		}		
		log.debug("Exit : public AccountHeadVO getLedgerByType(int orgID)");
		return achVO;
	}
	
	public AccountHeadVO getBankCashPrimaryLedger(int orgID,String ledgerType){
		AccountHeadVO achVO=new AccountHeadVO();
		
		log.debug("Entry : public AccountHeadVO getBankCashPrimaryLedger(int orgID)");
		try {
			int groupID=0;
			if(ledgerType.equalsIgnoreCase("B")){
				groupID=4;
			}else if(ledgerType.equalsIgnoreCase("C")){
				groupID=5;
			}
			
			
			List ledgerList=ledgerDAO.getLedgersList(orgID, groupID, "G","V");
			if(ledgerList.size()>1){
				for(int i=0;ledgerList.size()>i;i++){
					AccountHeadVO bankVO=(AccountHeadVO) ledgerList.get(i);
					if(bankVO.getIsPrimary()==1){
						achVO=bankVO;
					}
				}
				if(achVO.getLedgerID()==0){
					achVO=(AccountHeadVO) ledgerList.get(0);
				}
				
			}else{
				achVO=(AccountHeadVO) ledgerList.get(0);
			}
		} catch (Exception e) {
			log.error("Exception at public AccountHeadVO getBankCashPrimaryLedger "+e);
		}		
		log.debug("Exit : public AccountHeadVO getBankCashPrimaryLedger(int orgID)");
		return achVO;
	}
	
	public AccountHeadVO getPaymentGatewayLedger(int orgID){
		AccountHeadVO achVO=new AccountHeadVO();
		
		log.debug("Entry : public AccountHeadVO getPaymentGatewayLedger(int orgID)");
		try {
			
			List ledgerList=ledgerDAO.getLedgersListByProperties(orgID, "%payumoney%");
			if(ledgerList.size()>1){
				for(int i=0;ledgerList.size()>i;i++){
					AccountHeadVO bankVO=(AccountHeadVO) ledgerList.get(i);
					if(bankVO.getAdditionalProps().contains("#payumoney")){
						achVO=bankVO;
					}
				}
			}else{
				achVO=(AccountHeadVO) ledgerList.get(0);
			}
		} catch (Exception e) {
			log.error("Exception at public AccountHeadVO getBankLedger "+e);
		}		
		log.debug("Exit : public AccountHeadVO getBankLedger(int orgID)");
		return achVO;
	}
	
	 public List getLedgerProfileList(int societyID,String type){
			List ledgerList=null;
			try {
				log.debug("Entry : public List getLedgerProfileList(int societyID,String type)");
				
				if(type.equalsIgnoreCase("V")){
					ledgerList=ledgerDAO.getCustomerVendorLedgersList(societyID, 96,  "V");
					/*if(ledgerList.size()>0)
					for(int i=0;ledgerList.size()>i;i++){
						AccountHeadVO achVO=(AccountHeadVO) ledgerList.get(i);
						
						ProfileDetailsVO profileVO=ledgerDAO.getLedgerProfileDetails(achVO.getLedgerID(), societyID);
						
						if(profileVO.getProfileID()>0){
							
							profileVO.setAddress(addressServiceBean.getAddressDetails(profileVO.getProfileID(), "", "V", societyID));
						}
						
						
						achVO.setProfileVO(profileVO);
						
						
					}*/
					
					
					
				}else if(type.equalsIgnoreCase("C")){
	       			ledgerList=ledgerDAO.getCustomerVendorLedgersList(societyID,1, "C");		
	       			/*if(ledgerList.size()>0)
	    				for(int i=0;ledgerList.size()>i;i++){
	    					AccountHeadVO achVO=(AccountHeadVO) ledgerList.get(i);
	    					
	    					ProfileDetailsVO profileVO=ledgerDAO.getLedgerProfileDetails(achVO.getLedgerID(), societyID);
	    					
	    					if(profileVO.getProfileID()>0){
	    						
	    						profileVO.setAddress(addressServiceBean.getAddressDetails(profileVO.getProfileID(), "", "C", societyID));
	    					}
	    					
	    					
	    					achVO.setProfileVO(profileVO);
	    					
	    					
	    				}*/
				}else if(type.equalsIgnoreCase("M")){
					ledgerList=ledgerDAO.getCustomerVendorLedgersList(societyID, 1,  "M");
				/*	if(ledgerList.size()>0)
					for(int i=0;ledgerList.size()>i;i++){
						AccountHeadVO achVO=(AccountHeadVO) ledgerList.get(i);
						
						ProfileDetailsVO profileVO=ledgerDAO.getLedgerProfileDetails(achVO.getLedgerID(), societyID);
						
						if(profileVO.getProfileID()>0){
							
							profileVO.setAddress(addressServiceBean.getAddressDetails(profileVO.getProfileID(), "", "M", societyID));
						}
						
						
						achVO.setProfileVO(profileVO);
						
					}*/
				}else if(type.equalsIgnoreCase("E")){
						ledgerList=ledgerDAO.getCustomerVendorLedgersList(societyID, 100, "E");
					/*	if(ledgerList.size()>0)
						for(int i=0;ledgerList.size()>i;i++){
							AccountHeadVO achVO=(AccountHeadVO) ledgerList.get(i);
							
							ProfileDetailsVO profileVO=ledgerDAO.getLedgerProfileDetails(achVO.getLedgerID(), societyID);
							
							if(profileVO.getProfileID()>0){
								
								profileVO.setAddress(addressServiceBean.getAddressDetails(profileVO.getProfileID(), "", "E", societyID));
							}
							
							
							achVO.setProfileVO(profileVO);
							
							
						}*/
					}else if(type.equalsIgnoreCase("NM")){ //non member
						ledgerList=ledgerDAO.getCustomerVendorLedgersList(societyID, 1, "NM");
					/*	if(ledgerList.size()>0)
						for(int i=0;ledgerList.size()>i;i++){
							AccountHeadVO achVO=(AccountHeadVO) ledgerList.get(i);
							
							ProfileDetailsVO profileVO=ledgerDAO.getLedgerProfileDetails(achVO.getLedgerID(), societyID);
							
							if(profileVO.getProfileID()>0){
								
								profileVO.setAddress(addressServiceBean.getAddressDetails(profileVO.getProfileID(), "", "NM", societyID));
							}
							
							
							achVO.setProfileVO(profileVO);
							
							
					}*/
					
					
					
				}
				log.debug("Exit : public List getLedgerProfileList(int societyID,String type)"+ledgerList.size());
			} catch (Exception e) {
				log.error("Exception : public List getLedgerProfileList(int societyID,String type)"+e);
			}
			return ledgerList;
		}
	 
	 public List getCurrentNonCurrentLedgerProfileList(int societyID,String type){
			List ledgerList=null;
			try {
				log.debug("Entry : public List getCurrentNonCurrentLedgerProfileList(int societyID,String type)");
				
				if(type.equalsIgnoreCase("V")){
					ledgerList=ledgerDAO.getCustomerVendorLedgersCNCList(societyID,84, 96,  "V");
					
					
					
					
				}else if(type.equalsIgnoreCase("C")){
	       			ledgerList=ledgerDAO.getCustomerVendorLedgersCNCList(societyID,1,49, "C");		
	       			
				}else if(type.equalsIgnoreCase("M")){
					ledgerList=ledgerDAO.getCustomerVendorLedgersList(societyID, 1,  "M");
				
				}else if(type.equalsIgnoreCase("E")){
						ledgerList=ledgerDAO.getCustomerVendorLedgersList(societyID, 100, "E");
					
					}else if(type.equalsIgnoreCase("NM")){ //non member
						ledgerList=ledgerDAO.getCustomerVendorLedgersList(societyID, 1, "NM");
					
					
					
					
				}
				log.debug("Exit : public List getCurrentNonCurrentLedgerProfileList(int societyID,String type)"+ledgerList.size());
			} catch (Exception e) {
				log.error("Exception : public List getCurrentNonCurrentLedgerProfileList(int societyID,String type)"+e);
			}
			return ledgerList;
		}
	
	 
	 public List getLedgerProfilesList(int orgID,String type,String tag){
			List profileList=null;
			try {
				log.debug("Entry :  public List getLedgerProfilesList(int orgID,String type,String tag)");
				
				if(type.equalsIgnoreCase("V")){
					profileList=ledgerDAO.getLedgerProfileDetailsList(orgID, 8, tag);
					if(profileList.size()>0)
					for(int i=0;profileList.size()>i;i++){
											
						ProfileDetailsVO profileVO=(ProfileDetailsVO) profileList.get(i);
						
						if(profileVO.getProfileID()>0){
							
							profileVO.setAddress(addressServiceBean.getAddressDetails(profileVO.getProfileID(), "", "V", orgID));
						}
						
						
						
						
						
					}
					
					
					
				}else if(type.equalsIgnoreCase("C")){
					profileList=ledgerDAO.getLedgerProfileDetailsList(orgID, 1, tag);		
	       			if(profileList.size()>0)
	    				for(int i=0;profileList.size()>i;i++){
	    						    					
	    					ProfileDetailsVO profileVO=(ProfileDetailsVO) profileList.get(i);
	    					
	    					if(profileVO.getProfileID()>0){
	    						
	    						profileVO.setAddress(addressServiceBean.getAddressDetails(profileVO.getProfileID(), "", "C", orgID));
	    					}
	    					
	    					
	    					
	    					
	    					
	    				}
				}else if(type.equalsIgnoreCase("M")){
					profileList=ledgerDAO.getLedgerProfileDetailsList(orgID, 1, tag);
					if(profileList.size()>0)
					for(int i=0;profileList.size()>i;i++){
											
						ProfileDetailsVO profileVO=(ProfileDetailsVO) profileList.get(i);
						
						if(profileVO.getProfileID()>0){
							
							profileVO.setAddress(addressServiceBean.getAddressDetails(profileVO.getProfileID(), "", "M", orgID));
						}
						
					}
				}else if(type.equalsIgnoreCase("E")){
					profileList=ledgerDAO.getLedgerProfileDetailsList(orgID, 82, tag);
						if(profileList.size()>0)
						for(int i=0;profileList.size()>i;i++){
													
							ProfileDetailsVO profileVO=(ProfileDetailsVO) profileList.get(i);
							
							if(profileVO.getProfileID()>0){
								
								profileVO.setAddress(addressServiceBean.getAddressDetails(profileVO.getProfileID(), "", "E", orgID));
							}
							
							
														
							
						}
					}else if(type.equalsIgnoreCase("NM")){ //non member
						profileList=ledgerDAO.getLedgerProfileDetailsList(orgID, 93, tag);
						if(profileList.size()>0)
						for(int i=0;profileList.size()>i;i++){
										
							ProfileDetailsVO profileVO=(ProfileDetailsVO) profileList.get(i);
							
							if(profileVO.getProfileID()>0){
								
								profileVO.setAddress(addressServiceBean.getAddressDetails(profileVO.getProfileID(), "", "NM", orgID));
							}
							
					}
					
					
					
				}
				log.debug("Exit : public List getLedgerProfileList(int societyID,String type)"+profileList.size());
			} catch (Exception e) {
				log.error("Exception : public List getLedgerProfileList(int societyID,String type)"+e);
			}
			return profileList;
		}
	 
	 
	 public AccountHeadVO addLedgerWithProfiles(AccountHeadVO achVO){
			int insertFlag=0;
			AccountHeadVO ledgerVO=new AccountHeadVO();
			try {
				log.debug("Entry : public int addLedgerWithProfiles(AccountHeadVO achVO)");
				ProfileDetailsVO profileVO=achVO.getProfileVO();
				ProfileDetailsVO addrsProfileVO=new ProfileDetailsVO();
				
				Date today = new Date();
			    Calendar cal = Calendar.getInstance();
			    cal.setTime(today); // don't forget this if date is arbitrary
			    int month = cal.get(Calendar.MONTH); // 0 being January
			    int year = cal.get(Calendar.YEAR);
			    
			    if(month<=2){
			    	year=year-1;
			    }
			    
			    String date=year+"-03-31";
			    achVO.setEffectiveDate(date);
				
				int ledgerID=0;
				if(profileVO!=null){
					
					ledgerVO=this.insertLedger(achVO, achVO.getOrgID());
						
						if(ledgerVO.getLedgerID()>0){
							profileVO.setProfileLedgerID(ledgerVO.getLedgerID());
							profileVO.setOrgID(achVO.getOrgID());
							
							int success=ledgerDAO.insertLedgerProfile(profileVO, achVO);
							//int addTaxProp=ledgerDAO.insertProfileProperties(profileVO);
							
							
							
							if((profileVO.getAddress()!=null)){
								profileVO.getAddress().setPerson_category(profileVO.getProfileType());
								addrsProfileVO =ledgerDAO.getLedgerProfileDetails(ledgerVO.getLedgerID(), achVO.getOrgID());
							    success = addressServiceBean.addUpdateAddressDetails(profileVO.getAddress(),addrsProfileVO.getProfileID());
							}
							}
			
			
					insertFlag=ledgerVO.getLedgerID();
					
					
				}else ledgerVO=this.insertLedger(achVO, achVO.getOrgID());
				      insertFlag=ledgerVO.getLedgerID(); 
				
			
				log.debug("Exit : public int addLedgerWithProfiles(AccountHeadVO achVO)");
			} catch (Exception e) {
				log.error("Exception : public int addLedgerWithProfiles(AccountHeadVO achVO)"+e);
			}
			return ledgerVO;
		}
	 
	 public int updateLedgerProfileDetails(AccountHeadVO achVO) {

			int sucess = 0;  
			int successAdress=0;
			ProfileDetailsVO addrsProfileVO=new ProfileDetailsVO();
			try{
			
			    log.debug("Entry : public int updateLedgerProfileDetails(AccountHeadVO achVO)");
			   if(achVO.getProfileVO().getProfileID()==0){
				   ProfileDetailsVO profileVO=achVO.getProfileVO();
				    profileVO.setProfileLedgerID(achVO.getLedgerID());
					profileVO.setOrgID(achVO.getOrgID());
					
				   sucess = ledgerDAO.insertLedgerProfile(achVO.getProfileVO(), achVO);
				   //int addTaxProp=ledgerDAO.insertProfileProperties(profileVO);
			   }else{
			      sucess = ledgerDAO.updateLedgerProfileDetails(achVO.getProfileVO());
			   } 
			    if(achVO.getProfileVO().getAddress().getAddLine1().length()>0){
			    	addrsProfileVO =ledgerDAO.getLedgerProfileDetails(achVO.getLedgerID(), achVO.getOrgID());	
			      successAdress=addressServiceBean.addUpdateAddressDetails(achVO.getProfileVO().getAddress(), addrsProfileVO.getProfileID());
			    }
			    log.debug("Exit : public int updateLedgerProfileDetails(AccountHeadVO achVO)"+sucess);

			}catch(Exception Ex){
				
			
				log.error("Exception in public int updateLedgerProfileDetails(AccountHeadVO achVO) : "+Ex);
				
			}
			return sucess;
		}
		
		public int deleteLedgerProfile(int ledgerID,int orgID){
			
			int sucess = 0;
			int successTaxDetails=0;
			int successLedgerDetails=0;
			try{
			
			log.debug("Entry : public int deleteLedgerProfile(int ledgerID,int orgID)");
			
			sucess = ledgerDAO.deleteLedgerProfile( ledgerID,orgID);
			
			//successTaxDetails=ledgerDAO.deleteLedgerProfileFromTaxTable(ledgerID,orgID);
			
			AccountHeadVO ledgerVO=this.deleteLedger(orgID, ledgerID);
			
			log.debug("Exit : public int deleteLedgerProfile(int ledgerID,int orgID)");

			}catch(Exception Ex){		
				log.error("Exception in public int deleteLedgerProfile(int ledgerID,int orgID) : "+Ex);			
			}
			return sucess;
		}
	 
		public ProfileDetailsVO getLedgerProfile(int ledgerID,int orgID){
			ProfileDetailsVO profileVO=new ProfileDetailsVO();
			try {
				log.debug("Entry : public ProfileDetailsVO getLedgerProfile((int ledgerID,int orgID)");

				profileVO=ledgerDAO.getLedgerProfileDetails(ledgerID,orgID);	
				
				if(profileVO.getProfileID()>0){					
				   profileVO.setAddress(addressServiceBean.getAddressDetails(profileVO.getProfileID(), "", profileVO.getProfileType(), orgID));
				}
				
				log.debug("Exit : public List getLedgerProfile(int ledgerID,int orgID)");
			} catch (Exception e) {
				log.error("Exception : public ProfileDetailsVO getLedgerProfile(int ledgerID,int orgID)"+e);
			}
			return profileVO;
		}
		
		public ProfileDetailsVO getLedgerProfileDetails(int profileID){
			ProfileDetailsVO profileVO=new ProfileDetailsVO();
			try {
				log.debug("Entry : public ProfileDetailsVO getLedgerProfileDetails(int profileID)");

				profileVO=ledgerDAO.getLedgerProfileDetails(profileID);	
				
				if(profileVO.getProfileID()>0){					
				   profileVO.setAddress(addressServiceBean.getAddressDetails(profileVO.getProfileID(), "", profileVO.getProfileType(), profileVO.getOrgID()));
				}
				
				log.debug("Exit : public ProfileDetailsVO getLedgerProfileDetails(int profileID)");
			} catch (Exception e) {
				log.error("Exception : public ProfileDetailsVO getLedgerProfileDetails(int profileID)"+e);
			}
			return profileVO;
		}
		
		public BankStatement getLedgersDetailsForImport(BankStatement bankStmt){
			try {
				log.debug("Entry : public BankStatement getLedgersDetailsForImport(BankStatement bankStmt)");
				AccountHeadVO acHVO=new AccountHeadVO();
				AccountHeadVO suspenseVO=new AccountHeadVO();
				List bankLedgerList=ledgerDAO.getLedgersListForImportStament(bankStmt.getBankAccNumber());
				
				if(bankLedgerList.size()>0){
				 acHVO=(AccountHeadVO) bankLedgerList.get(0);
				}
				
				List suspenceAccList=ledgerDAO.getLedgersList(acHVO.getOrgID(), 36, "G","V" );
				if(suspenceAccList.size()>0){
				suspenseVO=(AccountHeadVO) suspenceAccList.get(0);
				}
				
				if(bankStmt.getFetchVirtualAccNo()!=0){
					List virtualAccNoList=ledgerDAO.getVirtualAccLedgersListForImportStament(acHVO.getOrgID());
					
					Map<String,AccountHeadVO> virtualAccMap=new HashMap<>();
					
					if(virtualAccNoList.size()>0){
						for(int i=0;virtualAccNoList.size()>i;i++){
							AccountHeadVO accVo=(AccountHeadVO) virtualAccNoList.get(i);
							virtualAccMap.put(accVo.getAdditionalProps(), accVo);
							
						}
						bankStmt.setVirtualAccNos(virtualAccMap);
					}
					
					
				}
				
				
				bankStmt.setBankLedgerID(acHVO.getLedgerID());
				bankStmt.setSuspenceLedgerID(suspenseVO.getLedgerID());
				bankStmt.setOrgID(acHVO.getOrgID());
				log.debug("Exit : public BankStatement getLedgersDetailsForImport(BankStatement bankStmt)");
			} catch (Exception e) {
				log.error("Exception : public BankStatement getLedgersDetailsForImport(BankStatement bankStmt)"+e);
			}
			return bankStmt;
		}
	
		
		/* Used to get groups of that org */
		public List getGroupListByCategory(int categoryID,int orgID,int rootID)  {
		// TODO Auto-generated method stub
		log.debug("Entry : public List getGroupListByCategory(int rootID,int orgID)");
		List groupList=null;
		
		try{
			 
			   groupList=ledgerDAO.getGroupListByCategory(categoryID, orgID, rootID);
			   log.debug("Exit : public List getGroupListByCategory)"+groupList.size());
			}catch(Exception se){
				log.error("Exception : public List getGroupListByCategory(String societyID) "+se);
			   
			}
		return groupList;
	}
		
		public List getCategoryList(int orgID){
			List categoryList=null;
			try {
				log.debug("Entry : public getCategoryList(int orgID)");

				categoryList=ledgerDAO.getCategoryList(orgID);	
				
				log.debug("Exit : public List getCategoryList(int orgID)");
			} catch (Exception e) {
				log.error("Exception : public List getCategoryList(int orgID)"+e);
			}
			return categoryList;
		}
		
		
//		This method is used to add new groups .
		public AccountHeadVO insertGroup(AccountHeadVO achVO,int orgID){
			int success=0;
			Boolean isDuplicate=false;
			List ledgerList=new ArrayList();
			try {
				log.debug("Entry : public AccountHeadVO insertGroup(AccountHeadVO achVO,int orgID)");
				
				if(achVO.getOrgID()==0){
					achVO.setStatusCode(534);
					achVO.setMessage("Only org specific groups can be added");
				}else{
				
				success=ledgerDAO.insertGroup(achVO,orgID);
				if(success==-1){
					achVO.setStatusCode(534);
					achVO.setLedgerID(success);
					achVO.setMessage(achVO.getStrAccountPrimaryHead()+" is already present , Please select any other group name");					
				
				}else if(success>0){
					achVO.setStatusCode(200);
					achVO.setAccountGroupID(success);
					achVO.setMessage("Group added successfully.");
				}else{
					achVO.setStatusCode(400);
					
					achVO.setMessage("Problem in adding Group.");
				}
				
				}
				
				log.debug("Exit : public AccountHeadVO insertGroup(AccountHeadVO achVO,int orgID)");
			} catch (Exception e) {
				log.error("Exception : public AccountHeadVO insertGroup(AccountHeadVO achVO,int orgID)"+e);
			}
			return achVO;
		}
//		This method is used to update group 
		public AccountHeadVO updateGroup(AccountHeadVO groupVO,int groupID){
			int success=0;
			List ledgerList=new ArrayList();
			try {
				
				log.debug("Entry : public AccountHeadVO updateGroup(AccountHeadVO groupVO,int groupID)");
			
				
				if(groupVO.getOrgID()==0){
					groupVO.setStatusCode(534);
					groupVO.setMessage("Only org specific groups can be updated");
				}else{
			
				success=ledgerDAO.updateGroup(groupVO, groupID);
				

				if(success>0){
					groupVO.setStatusCode(200);
					groupVO.setMessage("Group has been updated successfully.");
				
				}else if(success==-1){
					groupVO.setStatusCode(534);
					groupVO.setMessage(groupVO.getStrAccountPrimaryHead()+" is already present , Please select any other Group Name");
					
				}else{
					groupVO.setStatusCode(400);
					
					groupVO.setMessage("Problem in updating group.");
					
				}
				}
				
				log.debug("Exit : public AccountHeadVO updateGroup(AccountHeadVO groupVO,int groupID)");
			} catch (Exception e) {
				log.error("Exception : public AccountHeadVO updateGroup(AccountHeadVO groupVO,int groupID)"+e);
			}
			return groupVO;
		}
		
		//	This method is used to Delete group 
		public AccountHeadVO deleteGroup(int orgID,int groupID){
			int success=0;
			AccountHeadVO groupVO=new AccountHeadVO();
			try {
				log.debug("Entry : public AccountHeadVO deleteGroup(int orgID,int groupID)");
				
			
				if(orgID==0){
					groupVO.setStatusCode(534);
					groupVO.setMessage("Only org specific groups can be deleted");
				}else{
				
				List ledgerList=ledgerDAO.getLedgersList(orgID, groupID, "G", "A");
				if(ledgerList.size()>0){
					success=-1;
				}else
				success=ledgerDAO.deleteGroup( orgID, groupID);	
				
				
				
				if(success>0){
					groupVO.setStatusCode(200);
					groupVO.setMessage("Group has been deleted successfully.");
				
				}else if(success==-1){
					groupVO.setStatusCode(534);
					groupVO.setMessage("This group has "+ledgerList.size()+" ledgers. Please remove the ledgers first and then delete this group.");
					
				}else{
					groupVO.setStatusCode(400);
					
					groupVO.setMessage("Problem in deleting group.");
					
				}
				}
				
				log.debug("Exit : public AccountHeadVO deleteGroup(int orgID,int groupID)");
			} catch (Exception e) {
				log.error("Exception : public AccountHeadVO deleteGroup(int orgID,int groupID)"+e);
			}
			return groupVO;
		}
		
		
	public AddressService getAddressServiceBean() {
		return addressServiceBean;
	}
	public void setAddressServiceBean(AddressService addressServiceBean) {
		this.addressServiceBean = addressServiceBean;
	}
	public LedgerDAO getLedgerDAO() {
		return ledgerDAO;
	}
	public void setLedgerDAO(LedgerDAO ledgerDAO) {
		this.ledgerDAO = ledgerDAO;
	}
	public ReportService getReportService() {
		return reportService;
	}
	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}
	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}
	public void setTrialBalanceService(TrialBalanceService trialBalanceService) {
		this.trialBalanceService = trialBalanceService;
	}
	/**
	 * @param customerServiceBean the customerServiceBean to set
	 */
	public void setCustomerServiceBean(CustomerService customerServiceBean) {
		this.customerServiceBean = customerServiceBean;
	}
	/**
	 * @param vendorServiceBean the vendorServiceBean to set
	 */
	public void setVendorServiceBean(VendorService vendorServiceBean) {
		this.vendorServiceBean = vendorServiceBean;
	}
	/**
	 * @param societyServiceBean the societyServiceBean to set
	 */
	public void setSocietyServiceBean(SocietyService societyServiceBean) {
		this.societyServiceBean = societyServiceBean;
	}
	/**
	 * @param projectDetailsService the projectDetailsService to set
	 */
	public void setProjectDetailsService(ProjectDetailsService projectDetailsService) {
		this.projectDetailsService = projectDetailsService;
	}
}
