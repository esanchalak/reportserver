package com.emanager.server.accounts.Domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.invoice.DAO.InvoiceDAO;
import com.emanager.server.invoice.dataAccessObject.InvoiceDetailsVO;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.valueObject.MemberVO;


public class ReceiptInvoiceDomain {
	
	InvoiceDAO invoiceDAO;
	TransactionService transactionService;
	MemberService memberServiceBean;
	
	private static final Logger logger = Logger.getLogger(ReceiptInvoiceDomain.class);
	
	
	
	
	
	/* Get list of all inovoices for the period for a member */
	public List getUnpaidInvoicesForApartment(int societyID ,int ledgerID,int invoiceTypeID){
		List invoiceList=new ArrayList();
		List newInvoiceList=new ArrayList();
		logger.debug("Entry : public List getInvoicesForApartment(String societyID ,int aptID,String type)");
		String sledgerID=""+ledgerID;
		
		 MemberVO memberVO=memberServiceBean.getMemberInfoThroughLedgerID(societyID, sledgerID);

				
				
				
				//invoiceList=invoiceDAO.getInvoicesForApartment(societyID, memberVO.getAptID(), invoiceTypeID);
				for(int i=0;invoiceList.size()>i;i++){
					InvoiceDetailsVO invVO=(InvoiceDetailsVO) invoiceList.get(i);
					newInvoiceList.add(invVO);
				}
				
		 
		 
		
		logger.debug("Exit : public List getInvoicesForApartment(String societyID ,int aptID,String type)"+invoiceList.size());
		return newInvoiceList;		
	}
	
	/* Get list of all line items of the given invoice   */
	public List getLineItemsForInvoice(int societyID ,int invoiceID){
		List invoiceList=null;
		logger.debug("Entry : public List getLineItemsForInvoice(String societyID)");
		
		invoiceList=invoiceDAO.getLineItemsForInvoice(societyID, invoiceID);
		
		logger.debug("Exit : public List getLineItemsForInvoice(String societyID)"+invoiceList.size());
		return invoiceList;		
	}
	
	
	
	
	/* Insert the invoice  */
	public int insertNewInvoice(int societyID,int apartmentID){
		int insertFlag=0;
		List memberChargeList=null;
		logger.debug("Entry : public int insertNewInvoice(int societyID,int apartmentID)");
		
		//memberChargeList=getApplicableChargesToMember(societyID, apartmentID);
		
		logger.debug("Exit : public int insertNewInvoice(int societyID,int apartmentID)");
		return insertFlag;		
	}
	
	/* Insert the invoice  */
	public TransactionVO addReceptForInvoice(int societyID,TransactionVO txVO,List invoiceList){
		int insertFlag=0;
		List memberChargeList=null;
		logger.debug("Entry : public int addReceptForInvoice(String societyID,TransactionVO txVO)");
		
		txVO=transactionService.addTransaction(txVO, societyID);
		
		logger.debug("Receipt no is "+txVO.getTransactionID());
		
		if(txVO.getTransactionID()!=0){
		
		insertFlag=updateInvoiceBalance(societyID, txVO, insertFlag,invoiceList);
		}
		logger.debug("Exit : public int insertReceptForInvoice(String societyID,TransactionVO txVO)");
		return txVO;		
	}
	
	
	/* Insert the invoice  */
	public TransactionVO addReceptForInvoiceForOnlinePayment(int societyID,TransactionVO txVO){
		int insertFlag=0;
		List memberChargeList=null;
		logger.debug("Entry : public int addReceptForInvoice(String societyID,TransactionVO txVO)");
		
		txVO=transactionService.addTransaction(txVO, societyID);
		
		logger.debug("Receipt no is "+insertFlag);
		
		int updtBalFlag=updateInvoiceBalanceForOnlinePayment(societyID, txVO, insertFlag);
		
		logger.debug("Exit : public int insertReceptForInvoice(String societyID,TransactionVO txVO)");
		return txVO;		
	}
	
	/* Update invoice balance with the transaction */
	public int updateInvoiceBalance(int societyID ,TransactionVO txVO,int receiptID,List invoiceTypeList){
		int success=0;
		List invoiceList=new ArrayList();
		BigDecimal txBal=BigDecimal.ZERO;
		logger.debug("Entry : public int updateInvoiceBalance(String societyID ,TransactionVO txVO)");
		
		
		
		try {
			for(int j=0;invoiceTypeList.size()>j;j++){
				
				InvoiceDetailsVO invoiceTypeVO=(InvoiceDetailsVO) invoiceTypeList.get(j);
				txBal=invoiceTypeVO.getPaidAmount();
				
			invoiceList=getUnpaidInvoicesForApartment(societyID, txVO.getLedgerID(), invoiceTypeVO.getInvoiceTypeID());
			//invoiceList=txVO.getInvoiceList();
			
			for(int i=0;invoiceList.size()>i;i++){
				BigDecimal balance=BigDecimal.ZERO;
				BigDecimal currentBalance=BigDecimal.ZERO;
				InvoiceDetailsVO updtInvoiceVO=new InvoiceDetailsVO();
				InvoiceDetailsVO insrtInvoiceVO=new InvoiceDetailsVO();
				InvoiceDetailsVO invoiceVO=(InvoiceDetailsVO) invoiceList.get(i);
				logger.debug("-------------------->"+invoiceVO.getInvoiceID()+" "+invoiceVO.getInvoiceTypeID());
				//updtInvoiceVO=invoiceVO;
				balance=invoiceVO.getBalance();
				currentBalance=invoiceVO.getCurrentBalance();
				
				
				balance=balance.subtract(txBal);
				txBal=balance.abs();
				
				updtInvoiceVO.setBalance(balance);
				updtInvoiceVO.setInvoiceID(invoiceVO.getInvoiceID());
				insrtInvoiceVO.setInvoiceID(invoiceVO.getInvoiceID());
				
				//For Negative Balance
				if(balance.signum()<0){
					updtInvoiceVO.setStatusID(3);
					logger.debug(" Here balance is -Negative-->"+updtInvoiceVO.getInvoiceID()+"------->"+updtInvoiceVO.getBalance()+"  "+balance+"  "+invoiceVO.getBalance());
					
					if(((invoiceList.size()-1)==i)){
						
						updtInvoiceVO.setBalance(balance);
						
						updtInvoiceVO.setCurrentBalance(currentBalance);
						
					insrtInvoiceVO.setBalance(invoiceVO.getBalance().subtract(balance));
					insrtInvoiceVO.setCurrentBalance(currentBalance);
					//success=invoiceDAO.updateInvoiceBalance(societyID, updtInvoiceVO);
					
					}else{
					insrtInvoiceVO.setBalance(invoiceVO.getBalance());
					insrtInvoiceVO.setCurrentBalance(currentBalance);
					updtInvoiceVO.setBalance(BigDecimal.ZERO);
					
					
						
						 updtInvoiceVO.setCurrentBalance(currentBalance);
				//	success=invoiceDAO.updateInvoiceBalance(societyID, updtInvoiceVO);
					}
					logger.debug(" Here balance is -Negative-->"+insrtInvoiceVO.getInvoiceID()+"------->"+insrtInvoiceVO.getBalance()+"------"+invoiceVO.getBalance());
					success=invoiceDAO.insertReceiptInvoice(societyID, insrtInvoiceVO, receiptID);
				}//For Positive Balance
				else if(balance.signum()==0){
					//
					updtInvoiceVO.setStatusID(2);
					logger.debug(" Here balance is -Zero-->"+updtInvoiceVO.getInvoiceID()+"------->"+updtInvoiceVO.getBalance()+"  "+balance);
					if(balance.signum()<=currentBalance.signum()){
						updtInvoiceVO.setCurrentBalance(currentBalance);
					}else
					updtInvoiceVO.setCurrentBalance(invoiceTypeVO.getPaidAmount().abs());
				//	success=invoiceDAO.updateInvoiceBalance(societyID, updtInvoiceVO);
					insrtInvoiceVO.setBalance(invoiceVO.getBalance().subtract(balance));
					insrtInvoiceVO.setCurrentBalance(currentBalance);
					logger.debug(" Here balance is -Zero-->"+insrtInvoiceVO.getInvoiceID()+"------->"+insrtInvoiceVO.getBalance());
					success=invoiceDAO.insertReceiptInvoice(societyID, insrtInvoiceVO, receiptID);
					break;
				}//When amount paid=Balance
				else{
					//if(invoiceVO.getTotalAmount().intValue()<invoiceVO.getCarriedBalance())
					logger.debug(" Here balance is -Positive--> "+updtInvoiceVO.getInvoiceID()+"---"+invoiceTypeVO.getPaidAmount()+"----> "+updtInvoiceVO.getBalance()+"  "+balance+"----->"+invoiceVO.getBalance()+"   "+updtInvoiceVO.getCurrentBalance());
					if((invoiceVO.getTotalAmount().intValue()>0)&&(invoiceVO.getCarriedBalance().intValue()>=0)){
					updtInvoiceVO.setStatusID(1);
					logger.debug("Case 1 Total amount is positive and invoice carried balance is positve..");
					updtInvoiceVO.setCurrentBalance(invoiceVO.getCurrentBalance().subtract(balance));
					insrtInvoiceVO.setCurrentBalance(invoiceVO.getCurrentBalance().subtract(balance));
					}else if((invoiceVO.getTotalAmount().intValue()>0)&&(invoiceVO.getCarriedBalance().intValue()<0)){
						if(invoiceVO.getCarriedBalance().intValue()>invoiceVO.getTotalAmount().intValue()){
						
						updtInvoiceVO.setStatusID(2);
						logger.debug("Case 2 A -- Total amount is positive and carried balance is negative , Carried balance is greater than total amount");
						updtInvoiceVO.setCurrentBalance(invoiceVO.getCurrentBalance());
						insrtInvoiceVO.setCurrentBalance(invoiceVO.getCurrentBalance());
						
						}else{
							if(invoiceVO.getCurrentBalance().intValue()<invoiceTypeVO.getPaidAmount().intValue()){
								updtInvoiceVO.setStatusID(2);
								logger.debug("Case 2 B .a --  Total amount is positive and carried balance is negative , Current balance is less than paid amount");
								updtInvoiceVO.setCurrentBalance(invoiceVO.getCurrentBalance());
								insrtInvoiceVO.setCurrentBalance(currentBalance);
							}else{
							updtInvoiceVO.setStatusID(1);
							logger.debug("Case 2 B .b --  Total amount is positive and carried balance is negative , Current balance is greater than paid amount");
							updtInvoiceVO.setCurrentBalance(invoiceTypeVO.getPaidAmount());
							insrtInvoiceVO.setCurrentBalance(invoiceTypeVO.getPaidAmount());
							}
							
						}
						logger.debug("Here  "+updtInvoiceVO.getCurrentBalance()+"---- "+insrtInvoiceVO.getCurrentBalance());
					}else{
					updtInvoiceVO.setStatusID(2);
					logger.debug("Case 3");
					updtInvoiceVO.setCurrentBalance(BigDecimal.ZERO);
					insrtInvoiceVO.setCurrentBalance(currentBalance);
					}
					
				//	success=invoiceDAO.updateInvoiceBalance(societyID, updtInvoiceVO);
					insrtInvoiceVO.setBalance(invoiceVO.getBalance().subtract(balance));
					
					logger.debug(" Here balance is -Positive-->"+insrtInvoiceVO.getInvoiceID()+"------->"+insrtInvoiceVO.getBalance());
					success=invoiceDAO.insertReceiptInvoice(societyID, insrtInvoiceVO, receiptID);
					break;
				}
				
			
		}
		}}
		 catch (Exception e) {
			logger.error("Exception : updateInvoiceBalance(String societyID,TransactionVO txVO) "+e);
		
		
		
		}
		logger.debug("Exit : public int updateInvoiceBalance(String societyID ,TransactionVO txVO)");
		return success;		
	}
	
	
	/* Update invoice balance with the transaction */
	public int updateInvoiceBalanceForOnlinePayment(int societyID ,TransactionVO txVO,int receiptID){
		int success=0;
		List invoiceList=new ArrayList();
		BigDecimal txBal=BigDecimal.ZERO;
		logger.debug("Entry : public int updateInvoiceBalance(String societyID ,TransactionVO txVO)");
		
		
		
		try {
			
				InvoiceDetailsVO invoiceTypeVO=new InvoiceDetailsVO();
				invoiceTypeVO.setPaidAmount(txVO.getAmount());
				txBal=txVO.getAmount();
				
			invoiceList=getUnpaidInvoicesForApartment(societyID, txVO.getLedgerID(), 0);
			//invoiceList=txVO.getInvoiceList();
			
			for(int i=0;invoiceList.size()>i;i++){
				BigDecimal balance=BigDecimal.ZERO;
				BigDecimal currentBalance=BigDecimal.ZERO;
				InvoiceDetailsVO updtInvoiceVO=new InvoiceDetailsVO();
				InvoiceDetailsVO insrtInvoiceVO=new InvoiceDetailsVO();
				InvoiceDetailsVO invoiceVO=(InvoiceDetailsVO) invoiceList.get(i);
				logger.debug("-------------------->"+invoiceVO.getInvoiceID()+" "+invoiceVO.getInvoiceTypeID());
				//updtInvoiceVO=invoiceVO;
				balance=invoiceVO.getBalance();
				currentBalance=invoiceVO.getCurrentBalance();
				
				
				balance=balance.subtract(txBal);
				txBal=balance.abs();
				
				updtInvoiceVO.setBalance(balance);
				updtInvoiceVO.setInvoiceID(invoiceVO.getInvoiceID());
				insrtInvoiceVO.setInvoiceID(invoiceVO.getInvoiceID());
				
				//For Negative Balance
				if(balance.signum()<0){
					updtInvoiceVO.setStatusID(3);
					logger.debug(" Here balance is -Negative-->"+updtInvoiceVO.getInvoiceID()+"------->"+updtInvoiceVO.getBalance()+"  "+balance+"  "+invoiceVO.getBalance());
					
					if(((invoiceList.size()-1)==i)){
						
						updtInvoiceVO.setBalance(balance);
						
						updtInvoiceVO.setCurrentBalance(currentBalance);
						
					insrtInvoiceVO.setBalance(invoiceVO.getBalance().subtract(balance));
					insrtInvoiceVO.setCurrentBalance(currentBalance);
				//	success=invoiceDAO.updateInvoiceBalance(societyID, updtInvoiceVO);
					
					}else{
					insrtInvoiceVO.setBalance(invoiceVO.getBalance());
					insrtInvoiceVO.setCurrentBalance(currentBalance);
					updtInvoiceVO.setBalance(BigDecimal.ZERO);
					
					
						
						 updtInvoiceVO.setCurrentBalance(currentBalance);
				//	success=invoiceDAO.updateInvoiceBalance(societyID, updtInvoiceVO);
					}
					logger.debug(" Here balance is -Negative-->"+insrtInvoiceVO.getInvoiceID()+"------->"+insrtInvoiceVO.getBalance()+"------"+invoiceVO.getBalance());
					success=invoiceDAO.insertReceiptInvoice(societyID, insrtInvoiceVO, receiptID);
				}//For Positive Balance
				else if(balance.signum()==0){
					//
					updtInvoiceVO.setStatusID(2);
					logger.debug(" Here balance is -Zero-->"+updtInvoiceVO.getInvoiceID()+"------->"+updtInvoiceVO.getBalance()+"  "+balance);
					if(balance.signum()<=currentBalance.signum()){
						updtInvoiceVO.setCurrentBalance(currentBalance);
					}else
					updtInvoiceVO.setCurrentBalance(invoiceTypeVO.getPaidAmount().abs());
				//	success=invoiceDAO.updateInvoiceBalance(societyID, updtInvoiceVO);
					insrtInvoiceVO.setBalance(invoiceVO.getBalance().subtract(balance));
					insrtInvoiceVO.setCurrentBalance(currentBalance);
					logger.debug(" Here balance is -Zero-->"+insrtInvoiceVO.getInvoiceID()+"------->"+insrtInvoiceVO.getBalance());
					success=invoiceDAO.insertReceiptInvoice(societyID, insrtInvoiceVO, receiptID);
					break;
				}//When amount paid=Balance
				else{
					//if(invoiceVO.getTotalAmount().intValue()<invoiceVO.getCarriedBalance())
					logger.debug(" Here balance is -Positive--> "+updtInvoiceVO.getInvoiceID()+"---"+invoiceTypeVO.getPaidAmount()+"----> "+updtInvoiceVO.getBalance()+"  "+balance+"----->"+invoiceVO.getBalance()+"   "+updtInvoiceVO.getCurrentBalance());
					if((invoiceVO.getTotalAmount().intValue()>0)&&(invoiceVO.getCarriedBalance().intValue()>=0)){
					updtInvoiceVO.setStatusID(1);
					logger.debug("Case 1 Total amount is positive and invoice carried balance is positve..");
					updtInvoiceVO.setCurrentBalance(invoiceVO.getCurrentBalance().subtract(balance));
					insrtInvoiceVO.setCurrentBalance(invoiceVO.getCurrentBalance().subtract(balance));
					}else if((invoiceVO.getTotalAmount().intValue()>0)&&(invoiceVO.getCarriedBalance().intValue()<0)){
						if(invoiceVO.getCarriedBalance().intValue()>invoiceVO.getTotalAmount().intValue()){
						
						updtInvoiceVO.setStatusID(2);
						logger.debug("Case 2 A -- Total amount is positive and carried balance is negative , Carried balance is greater than total amount");
						updtInvoiceVO.setCurrentBalance(invoiceVO.getCurrentBalance());
						insrtInvoiceVO.setCurrentBalance(invoiceVO.getCurrentBalance());
						
						}else{
							if(invoiceVO.getCurrentBalance().intValue()<invoiceTypeVO.getPaidAmount().intValue()){
								updtInvoiceVO.setStatusID(2);
								logger.debug("Case 2 B .a --  Total amount is positive and carried balance is negative , Current balance is less than paid amount");
								updtInvoiceVO.setCurrentBalance(invoiceVO.getCurrentBalance());
								insrtInvoiceVO.setCurrentBalance(currentBalance);
							}else{
							updtInvoiceVO.setStatusID(1);
							logger.debug("Case 2 B .b --  Total amount is positive and carried balance is negative , Current balance is greater than paid amount");
							updtInvoiceVO.setCurrentBalance(invoiceTypeVO.getPaidAmount());
							insrtInvoiceVO.setCurrentBalance(invoiceTypeVO.getPaidAmount());
							}
							
						}
						logger.debug("Here  "+updtInvoiceVO.getCurrentBalance()+"---- "+insrtInvoiceVO.getCurrentBalance());
					}else{
					updtInvoiceVO.setStatusID(2);
					logger.debug("Case 3");
					updtInvoiceVO.setCurrentBalance(BigDecimal.ZERO);
					insrtInvoiceVO.setCurrentBalance(currentBalance);
					}
					
				//	success=invoiceDAO.updateInvoiceBalance(societyID, updtInvoiceVO);
					insrtInvoiceVO.setBalance(invoiceVO.getBalance().subtract(balance));
					
					logger.debug(" Here balance is -Positive-->"+insrtInvoiceVO.getInvoiceID()+"------->"+insrtInvoiceVO.getBalance());
					success=invoiceDAO.insertReceiptInvoice(societyID, insrtInvoiceVO, receiptID);
					break;
				}
				
			
		}
		}
		 catch (Exception e) {
			logger.error("Exception : updateInvoiceBalance(String societyID,TransactionVO txVO) "+e);
		
		
		
		}
		logger.debug("Exit : public int updateInvoiceBalance(String societyID ,TransactionVO txVO)");
		return success;		
	}
	
	
	
	/* Insert the invoice  */
	public int updateDeletedReceiptBalances(String societyID,TransactionVO txVO){
		int insertFlag=0;
		List invoiceList=new ArrayList();
		logger.debug("Entry : public int updateDeletedReceiptBalances(String societyID,TransactionVO txVO)");
		
	//	invoiceList=invoiceDAO.getReceiptInvoiceDetailsList(societyID, txVO.getTransactionID());
		
		for(int i=0;invoiceList.size()>i;i++){
			InvoiceDetailsVO invoiceVO=(InvoiceDetailsVO) invoiceList.get(i);
		//	insertFlag=invoiceDAO.updateInvoiceBalanceAfterCancel(societyID, invoiceVO);
			
		}
		
		
		logger.debug("Receipt no is "+insertFlag);
		
		
		
		logger.debug("Exit : public int updateDeletedReceiptBalances(String societyID,TransactionVO txVO)");
		return insertFlag;		
	}
	
	public InvoiceDAO getInvoiceDAO() {
		return invoiceDAO;
	}

	public void setInvoiceDAO(InvoiceDAO invoiceDAO) {
		this.invoiceDAO = invoiceDAO;
	}


	
	public MemberService getMemberServiceBean() {
		return memberServiceBean;
	}

	public void setMemberServiceBean(MemberService memberServiceBean) {
		this.memberServiceBean = memberServiceBean;
	}

	public TransactionService getTransactionService() {
		return transactionService;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}


	




}
