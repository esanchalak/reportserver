package com.emanager.server.accounts.Domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.accounts.DAO.TransactionDAO;
import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.BankStatement;
import com.emanager.server.accounts.DataAccessObjects.ChargesVO;
import com.emanager.server.accounts.DataAccessObjects.LedgerEntries;
import com.emanager.server.accounts.DataAccessObjects.OnlinePaymentGatewayVO;
import com.emanager.server.accounts.DataAccessObjects.PenaltyChargesVO;
import com.emanager.server.accounts.DataAccessObjects.ScheduledTransactionResponseVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionMetaVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.DataAccessObjects.TxCounterVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.commonUtils.domainObject.CalculateLateFeeForTransactions;
import com.emanager.server.commonUtils.domainObject.CommonUtility;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.domainObject.InterestManager;
import com.emanager.server.financialReports.domainObject.AmountInWords;
import com.emanager.server.financialReports.services.ReportService;
import com.emanager.server.financialReports.valueObject.ReportVO;
import com.emanager.server.invoice.DAO.InvoiceDAO;
import com.emanager.server.invoice.Services.InvoiceService;
import com.emanager.server.invoice.dataAccessObject.BillDetailsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceBillingCycleVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceVO;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.webservice.JSONResponseVO;


public class TransactionsDomain {

	
	TransactionDAO accTransactionDAO;
	ReportService reportService;
	ReceiptInvoiceDomain receiptInvoiceDomain;
	SocietyService societyService;
	LedgerService ledgerService;
	MemberService memberServiceBean;
	InvoiceService invoiceService;
	Logger log=Logger.getLogger(TransactionsDomain.class);
	AmountInWords amtInWrds=new AmountInWords();
	DateUtility dateUtil=new DateUtility();
	CalculateLateFeeForTransactions calculateLateFeesForTransaction;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
	CommonUtility commonUtil=new CommonUtility();
	InterestManager interestManager;
			
	
	/* Used for insering transaction and inserting ledger_entries */
	public TransactionVO insertTransaction(TransactionVO transactionVO,int societyID){
		int success=0;
		int insertTxFlag=0;
		int insertLdgrEntries=0;
		int successMeta=0;
		List ledgerEntries=null;
		BigDecimal amount=BigDecimal.ZERO;
		BigDecimal sumOfCr=BigDecimal.ZERO;
		BigDecimal sumOfDr=BigDecimal.ZERO;
		TransactionVO validationVO=new TransactionVO();
		TransactionVO duplicateVO=new TransactionVO();
		SocietyVO societyVO=societyService.getSocietyDetails(societyID);
		log.debug("Entry : public int insertTransaction(TransactionVO transactionVO,int societyID) ");
		try {
		transactionVO.setSocietyID(societyID);
	      //Validation
		   if(transactionVO.getValidation()){
			   validationVO=this.validateTransaction(transactionVO, societyID);
		   }else{
			   validationVO.setStatusCode(200);
		   }
			   		   
		   duplicateVO=checkForDuplicateTransactions(transactionVO, societyID);
		   log.debug("Validation "+validationVO.getStatusCode()+" duplicateVO "+duplicateVO.getStatusCode());
	       if((validationVO.getStatusCode()!=200)){
	    	   
	    	   transactionVO.setStatusCode(validationVO.getStatusCode());
	    	   transactionVO.setErrorMessage(validationVO.getErrorMessage());
	    	   
	       }else if(((duplicateVO.getStatusCode()==534)||(duplicateVO.getStatusCode()==531))){
	    	  
	    	   transactionVO.setStatusCode(duplicateVO.getStatusCode());
	    	   transactionVO.setErrorMessage(duplicateVO.getErrorMessage());
	    	   
	       }else {
	    		
				if((transactionVO.getAutoInc())&&((transactionVO.getTransactionType()!=null)&&(transactionVO.getTransactionType().length()>0))){

	 				String counterType=this.getCounterType(transactionVO.getTransactionType());

	 				transactionVO.setCounterType(counterType);

	 				int autoIncNo=accTransactionDAO.getLatestCounterNo(societyID, transactionVO.getCounterType());
	             
	 				if((transactionVO.getDocID()==null)||(transactionVO.getDocID().length()==0)){
	                  transactionVO.setDocID(""+autoIncNo);
	 				}

	                 transactionVO.setTransactionNumber(""+autoIncNo);

	 			}
				log.debug("Amount "+transactionVO.getAmount());
				
				amount=transactionVO.getAmount().setScale(2);
				transactionVO.setAmtInWord(amtInWrds.convertToAmt(amount.toString()));
				ledgerEntries=transactionVO.getLedgerEntries();
				log.debug("Amount in words is "+transactionVO.getAmtInWord());
				transactionVO=interestManager.checkForInterest(transactionVO);				
				for(int i=0;ledgerEntries.size()>i;i++){
					String updatedBalance="";
					
					
					LedgerEntries ledgerEntry=(LedgerEntries) ledgerEntries.get(i);
					
					AccountHeadVO tempLedgerVO=ledgerService.getUpdatedBalances(ledgerEntry, societyID);
					
				
						ledgerEntry.setAmount(tempLedgerVO.getAmount());
						if(ledgerEntry.getCreditDebitFlag().equalsIgnoreCase("C")){
						sumOfCr=sumOfCr.add(ledgerEntry.getAmount().abs());
						}else{
							sumOfDr=sumOfDr.add(ledgerEntry.getAmount().abs());
						}
					
					
					
				
					
				}
				sumOfCr=sumOfCr.setScale(2);
				sumOfDr=sumOfDr.setScale(2);
				log.info("Sum of Cr "+sumOfCr+" Sum of Db  "+sumOfDr);
				
			
				
	
				if(transactionVO.getCreatedBy()==0)
					transactionVO.setCreatedBy(15);
				if(transactionVO.getUpdatedBy()==0)
					transactionVO.setUpdatedBy(15);

				if((sumOfCr.compareTo(BigDecimal.ZERO)>0)&&(sumOfCr.compareTo(sumOfDr)==0)){
				insertTxFlag=accTransactionDAO.createTransaction(transactionVO,societyID);
					log.debug("Transaction ID: "+insertTxFlag);
				}	

				if(insertTxFlag!=0){
					transactionVO.setTransactionID(insertTxFlag);
					if((transactionVO.getAutoInc())&&(transactionVO.getCounterType()!=null)){
						  int counterNo=accTransactionDAO.incrementCounterNumber(societyID, transactionVO.getCounterType());
						}
					for(int i=0;ledgerEntries.size()>i;i++){
						String updatedBalance="";
									
						LedgerEntries ledgerEntry=(LedgerEntries) ledgerEntries.get(i);
					insertLdgrEntries=accTransactionDAO.createSingleLedgerEntries(ledgerEntry,transactionVO.getTransactionID(),societyID);
					
					}
					TransactionMetaVO txMeta=new TransactionMetaVO();
					successMeta=accTransactionDAO.updateAdditionalProperties(transactionVO,societyID);
					transactionVO.setStatusCode(200);
					transactionVO.setErrorMessage("Transaction inserted successfully");
					if((societyVO.getOrgType().equalsIgnoreCase("S"))&&((transactionVO.getTransactionType().equalsIgnoreCase("Receipt")||(transactionVO.getTransactionDate().equalsIgnoreCase("Credit Note"))))){
						ReportVO reportVO= dateUtil.getCurrentFYDates(transactionVO.getTransactionDate());
						transactionVO.setFromDate(reportVO.getFromDate());
						transactionVO.setToDate(reportVO.getUptDate());
						reallocateInterestAmounts(transactionVO);
					}
					
					
				}else{
					transactionVO.setStatusCode(406);
					transactionVO.setErrorMessage("Unable to add transaction");
				}
			}				
					
	
			log.debug("Exit : public int insertTransaction(TransactionVO transactionVO,int societyID) ");
		} catch (NullPointerException e) {
			log.info("NullPointerException in adding transaction , no hashComments available "+e.getMessage()+" "+e+transactionVO.getHashComments());
			
		} catch (Exception e) {
			log.error("Exception in adding transaction "+e.getMessage()+" "+e);
		}
		
		
		
		return transactionVO;
		
	}
	
	private TransactionVO checkForDuplicateTransactions(TransactionVO txVO,int societyID){
		log.debug("Entry : private TransactionVO checkForDuplicateTransactions(TransactionVO txVO,int societyID)");
		Boolean duplicate=false;
		List ledgerEntryList=txVO.getLedgerEntries();
		try{
		if((txVO.getStatusCode()!=531)&&(!txVO.getTransactionMode().equalsIgnoreCase("Cash"))){
			
		for(int i=0;ledgerEntryList.size()>i;i++){
			LedgerEntries tempLedgerEntry=(LedgerEntries) ledgerEntryList.get(i);
			if((txVO.getTransactionType().equalsIgnoreCase("Receipt")||txVO.getTransactionType().equalsIgnoreCase("Cash Receipt"))){
				
				if(tempLedgerEntry.getCreditDebitFlag().equalsIgnoreCase("C")){
					List txList=getLatestTransactions(societyID, tempLedgerEntry.getLedgerID(),txVO.getTransactionType());
					
					if(txList.size()>0){
						
						for(int j=0;txList.size()>j;j++){
							TransactionVO tempTxVO=(TransactionVO) txList.get(j);
							
							if((txVO.getAmount().compareTo(tempTxVO.getAmount())==0)){
								if((txVO.getRefNumber()!=null)&&((txVO.getRefNumber().length()!=0)&&(!txVO.getRefNumber().equalsIgnoreCase("-"))&&(!txVO.getRefNumber().equalsIgnoreCase("NOREF")))&&(txVO.getRefNumber().equalsIgnoreCase(tempTxVO.getRefNumber()))){
									txVO.setStatusCode(534);
									txVO.setErrorMessage("Ref no "+tempTxVO.getRefNumber()+" and amount Rs. "+tempTxVO.getAmount()+" is already present please select some other Amount or Ref no");
									duplicate=true;
								}
									break;
								
							}
						
					}
					
					
				}
				
				
			}
					
		} else if((txVO.getTransactionType().equalsIgnoreCase("Payment"))){
				
				if(tempLedgerEntry.getCreditDebitFlag().equalsIgnoreCase("C")){
					List txList=getLatestTransactions(societyID, tempLedgerEntry.getLedgerID(),txVO.getTransactionType());
					
					if(txList.size()>0){
						
						for(int j=0;txList.size()>j;j++){
							TransactionVO tempTxVO=(TransactionVO) txList.get(j);
							
							if((txVO.getAmount().compareTo(tempTxVO.getAmount())==0)){
								if((txVO.getRefNumber()!=null) &&(txVO.getRefNumber().equalsIgnoreCase(tempTxVO.getRefNumber()))){
									txVO.setStatusCode(534);
									txVO.setErrorMessage("Ref no "+tempTxVO.getRefNumber()+" and amount Rs. "+tempTxVO.getAmount()+" is already present please select some other Amount or Ref no");
									duplicate=true;
								}
									break;
								
							}
						
					}
					
					
				}
				
				
			}
					
		}else {
			
			if((txVO.getTransactionType().equalsIgnoreCase("Journal"))||(txVO.getTransactionType().equalsIgnoreCase("Contra"))|| (txVO.getTransactionMode().equalsIgnoreCase("Cash"))){
				log.debug(" checkForDuplicateTransactions type: "+txVO.getTransactionType());
			}else{
				List txList=getLatestTransactions(societyID, tempLedgerEntry.getLedgerID(),txVO.getTransactionType());
			
				if(txList.size()>0){
					
					for(int j=0;txList.size()>j;j++){
						TransactionVO tempTxVO=(TransactionVO) txList.get(j);
						
						if((txVO.getAmount().compareTo(tempTxVO.getAmount())==0)){
							if((txVO.getRefNumber()!=null) &&(txVO.getRefNumber().equalsIgnoreCase(tempTxVO.getRefNumber()))){
								txVO.setStatusCode(534);
								txVO.setErrorMessage("Ref no "+tempTxVO.getRefNumber()+" and amount Rs. "+tempTxVO.getAmount()+" is already present please select some other Amount or Ref no");
								duplicate=true;
							}
								break;
							
						}
					
				     }			
			  	
			     }
			 }
			
		
				
	}
			
			
			
		}
		}
		}catch(NullPointerException e){
			log.info("Error occured in checkForDuplicateTransactions "+e);
		
		
		}catch(Exception e){
			log.error("Error occured in checkForDuplicateTransactions "+e);
		}
		
		log.debug("Exit : private TransactionVO checkForDuplicateTransactions(TransactionVO txVO,int societyID)");
		return txVO;
	}
	
	
	
		
	/* Used for insering Receipt transaction and inserting ledger_entries */
	public int insertReceiptTransaction(TransactionVO transactionVO,String societyID){
		int success=0;
		int insertTxFlag=0;
		int insertLdgrEntries=0;
		List ledgerEntries=null;
		log.debug("Entry : public int insertReceiptTransaction(TransactionVO transactionVO,String societyID)");
		try {
			
			
			
			
			
		} catch (Exception e) {
			log.error("Exception in adding transaction "+e.getMessage()+" "+e);
		}
		
		
		log.debug("Exit : public int insertReceiptTransaction(TransactionVO transactionVO,String societyID)");
		return success;
		
	}
	/* Used for updating transaction without affecting amount */
	public TransactionVO updateTransaction(TransactionVO transaction,int societyID,int txID) {
		// TODO Auto-generated method stub
		log.debug("Entry : public int updateTransaction(TransactionVO transaction,String societyID)"+transaction.getAutoInc());
		int success=0;
		int successMeta=0;
		List ledgerEntryList=null;
		BigDecimal amount=BigDecimal.ZERO;
		BigDecimal sumOfCr=BigDecimal.ZERO;
		BigDecimal sumOfDr=BigDecimal.ZERO;
		transaction.setTransactionID(txID);
		TransactionVO validationVO=new TransactionVO();
		try{
		
			if(transaction.getCreatedBy()==0)
				transaction.setCreatedBy(15);
			if(transaction.getUpdatedBy()==0)
				transaction.setUpdatedBy(15);
			
			if(transaction.getValidation()){
				validationVO=validateTransaction(transaction, societyID);
			}else{
				validationVO.setStatusCode(200);
			}
			
			
			if(validationVO.getStatusCode()==200){
				transaction.setAmtInWord(amtInWrds.convertToAmt(transaction.getAmount().setScale(2).toString()));
				if(transaction.getIsUpdated().equalsIgnoreCase("Y")){
					
					log.debug("Amount in words is "+transaction.getAmtInWord());

						for(int i=0;transaction.getLedgerEntries().size()>i;i++){
						String updatedBalance="";
						
						
						LedgerEntries ledgerEntry=(LedgerEntries) transaction.getLedgerEntries().get(i);
						
						AccountHeadVO tempLedgerVO=ledgerService.getUpdatedBalances(ledgerEntry, societyID);
						
					
							ledgerEntry.setAmount(tempLedgerVO.getAmount());
							if(ledgerEntry.getCreditDebitFlag().equalsIgnoreCase("C")){
							sumOfCr=sumOfCr.add(ledgerEntry.getAmount().abs());
							}else{
								sumOfDr=sumOfDr.add(ledgerEntry.getAmount().abs());
							}
						
						
						
					
						
					}
					sumOfCr=sumOfCr.setScale(2);
					sumOfDr=sumOfDr.setScale(2);
					log.debug("Sum of Cr "+sumOfCr+" Sum of Db  "+sumOfDr);
					transaction=interestManager.checkForInterest(transaction);
					if((sumOfCr.compareTo(BigDecimal.ZERO)!=0)&&(sumOfCr.abs().intValue()==sumOfDr.abs().intValue())){
					success=accTransactionDAO.updateTransaction(transaction,societyID,txID);
					if(success!=0){
						
						success=accTransactionDAO.deleteLedgerEntries(txID, societyID);
						for(int i=0;transaction.getLedgerEntries().size()>i;i++){
							String updatedBalance="";
											
								LedgerEntries ledgerEntry=(LedgerEntries) transaction.getLedgerEntries().get(i);
							success=accTransactionDAO.createSingleLedgerEntries(ledgerEntry,txID,societyID);
							
						}
							
						transaction.setStatusCode(200);
					}

					}

					
				}else{
					
					 success=accTransactionDAO.updateTransactionWithoutChangingAmt(transaction, societyID, txID);
				}
				
				transaction.setSocietyID(societyID);				
				if(success!=0){
					successMeta=accTransactionDAO.updateAdditionalProperties(transaction,societyID);
					transaction.setStatusCode(200);
					transaction.setErrorMessage("Transaction updated successfully");
				}else{
					transaction.setStatusCode(530);
					transaction.setErrorMessage("Unable to update transaction");
					
				}
				
			}else{
				transaction.setStatusCode(validationVO.getStatusCode());
				transaction.setErrorMessage(validationVO.getErrorMessage());
				
			}

			
			
		}catch(Exception e){
			log.error("Exception in updateTx "+e);
		}
		
		
		log.debug("Exit : public int updateTransaction(TransactionVO transaction,String societyID)"+success);
		
		return transaction;
	}
	
	/* Used for validation transaction */
	public TransactionVO validateTransaction(TransactionVO transactionVO,int societyID){
		log.debug("Entry :public TransactionVO validateTransaction(TransactionVO transactionVO,int societyID)"+societyID);
		BigDecimal sumOfCr=BigDecimal.ZERO;
		BigDecimal sumOfDr=BigDecimal.ZERO;
		SocietyVO societyVO =new SocietyVO();
	    Boolean isAuditDateValid=false;
	    Boolean isGSTDateValid=false;
	    Boolean isSocDateValid=false;
		try{		
			Date socLocDate;
			Date auditLocDate;
			Date gstLocDate;
			Date txDate=sdf.parse(transactionVO.getTransactionDate());
			societyVO=societyService.getSocietyDetails(societyID);
			
			//audit lock date
			if(societyVO.getAuditCloseDate()!=null){
				auditLocDate=sdf.parse(societyVO.getAuditCloseDate());				
				if(txDate.compareTo(auditLocDate)<=0){
					isAuditDateValid=false;
					transactionVO.setStatusCode(530);
					transactionVO.setErrorMessage("Your organization audits has been locked till "+dateUtil.getConvetedDate(societyVO.getAuditCloseDate())+", So that no transactions can be added");
					return transactionVO;
				}else{
					isAuditDateValid=true;
				}				
			}else{
				isAuditDateValid=true;
			}
			
			//gst lock date
			if((societyVO.getGstCloseDate()!=null)&&(societyVO.getGstIN()!=null)&&(societyVO.getGstIN().length()>0)){
				gstLocDate=sdf.parse(societyVO.getGstCloseDate());
				if((txDate.compareTo(gstLocDate)<=0)&&((transactionVO.getTransactionType().equalsIgnoreCase("Sales"))||(transactionVO.getTransactionType().equalsIgnoreCase("Purchase"))||(transactionVO.getTransactionType().equalsIgnoreCase("Credit Note"))||(transactionVO.getTransactionType().equalsIgnoreCase("Debit Note")))){ 
					isGSTDateValid=false;
					transactionVO.setStatusCode(530);
					transactionVO.setErrorMessage("Your organization GST has been locked till "+dateUtil.getConvetedDate(societyVO.getGstCloseDate())+", So that no transactions can be added");
					return transactionVO;
				}else{					
					isGSTDateValid=true;
				}
			
			}else{
				isGSTDateValid=true;
			}
			
			//society lock date
			if(societyVO.getTxCloseDate()!=null){
				socLocDate=sdf.parse(societyVO.getTxCloseDate());
				if(txDate.compareTo(socLocDate)<=0){ 
					isSocDateValid=false;
					transactionVO.setStatusCode(530);
					transactionVO.setErrorMessage("Your organization accounts has been locked till "+dateUtil.getConvetedDate(societyVO.getTxCloseDate())+", So that no transactions can be added");
					return transactionVO;
				}else{					
					isSocDateValid=true;
				}
			
			}else{
				isSocDateValid=true;
			}
			
			//ledger lock date
			if((isAuditDateValid)&&(isGSTDateValid)&&(isSocDateValid)){ 				
				
				for(int i=0;transactionVO.getLedgerEntries().size()>i;i++){					
					
					LedgerEntries ledgerEntry=(LedgerEntries) transactionVO.getLedgerEntries().get(i);
					
					AccountHeadVO tempLedgerVO=ledgerService.getUpdatedBalances(ledgerEntry, societyID);
					if(txDate.compareTo(sdf.parse(tempLedgerVO.getEffectiveDate()))<=0){
						transactionVO.setStatusCode(553);
						transactionVO.setErrorMessage("Cannot complete your request since, ledger "+tempLedgerVO.getStrAccountHeadName()+"  effective date is "+dateUtil.getConvetedDate(tempLedgerVO.getEffectiveDate())+"  , Please check the ledger.");
						break;
					}else	if(tempLedgerVO.getIsDeleted()!=0){
						transactionVO.setStatusCode(552);
						transactionVO.setErrorMessage("Cannot complete your request since Ledger "+tempLedgerVO.getStrAccountHeadName()+" is inactive , Please check the ledger.");
						break;
					}else	if((tempLedgerVO.getLockDate()!=null)){
						if(txDate.compareTo(sdf.parse(tempLedgerVO.getLockDate())) >= 0){
							transactionVO.setStatusCode(200);
							transactionVO.setErrorMessage("Transaction validated successfully");
						}else{
							transactionVO.setStatusCode(531);
							transactionVO.setErrorMessage("Cannot complete your request since Ledger "+tempLedgerVO.getStrAccountHeadName()+" has been locked till "+dateUtil.getConvetedDate(tempLedgerVO.getLockDate())+", Please co-ordinate with your administrator to remove ledger or account lock.");
							break;
						 }
					 }else{
						
						transactionVO.setStatusCode(200);
						transactionVO.setErrorMessage("Transaction validated successfully");
					 }		
				
					
				  }
				
					
				}
			   
					
			log.debug("Status code : "+transactionVO.getStatusCode()+" Message: "+transactionVO.getErrorMessage());
			
		}catch(NullPointerException ne){
			log.info("Exception in validateTransaction "+ne+"org ID"+transactionVO.getSocietyID() +"ledger ID  "+transactionVO.getLedgerID()+" tx date "+transactionVO.getTransactionDate()+" tx Type :"+transactionVO.getTransactionType());
		
			
		}catch(Exception e){
			log.error("Exception in validateTransaction "+e);
		}
		
		
		log.debug("Exit :public TransactionVO validateTransaction(TransactionVO transactionVO,int societyID)");
		return transactionVO;
	
	}
	
	
	/* Used for validation of transactions by given ledger */
	public TransactionVO validateTransactionLedgerWise(TransactionVO transactionVO,int societyID,LedgerEntries ledgerEntry){
		log.debug("Entry : public TransactionVO validateTransactionLedgerWise(TransactionVO transactionVO,int societyID,int ledgerID)");
		BigDecimal sumOfCr=BigDecimal.ZERO;
		BigDecimal sumOfDr=BigDecimal.ZERO;
		SocietyVO societyVO =new SocietyVO();
	    Boolean isAuditDateValid=false;
	   // Boolean isGSTDateValid=false;
	    Boolean isSocDateValid=false;
		try{		
			Date socLocDate;
			Date auditLocDate;
			Date gstLocDate;
			Date txDate=sdf.parse(transactionVO.getTransactionDate());
			societyVO=societyService.getSocietyDetails(societyID);
			
			//audit lock date
			if(societyVO.getAuditCloseDate()!=null){
				auditLocDate=sdf.parse(societyVO.getAuditCloseDate());				
				if(txDate.compareTo(auditLocDate)<=0){
					isAuditDateValid=false;
					transactionVO.setStatusCode(530);
					transactionVO.setErrorMessage("Your organization audits has been locked till "+dateUtil.getConvetedDate(societyVO.getAuditCloseDate())+", So that no transactions can be added");
					return transactionVO;
				}else{
					isAuditDateValid=true;
				}				
			}else{
				isAuditDateValid=true;
			}
			
			/*//gst lock date
			if((societyVO.getGstCloseDate()!=null)&&(societyVO.getGstIN()!=null)&&(societyVO.getGstIN().length()>0)){
				gstLocDate=sdf.parse(societyVO.getGstCloseDate());
				if((txDate.compareTo(gstLocDate)<=0)&&((transactionVO.getTransactionType().equalsIgnoreCase("Sales"))||(transactionVO.getTransactionType().equalsIgnoreCase("Purchase"))||(transactionVO.getTransactionType().equalsIgnoreCase("Credit Note"))||(transactionVO.getTransactionType().equalsIgnoreCase("Debit Note")))){ 
					isGSTDateValid=false;
					transactionVO.setStatusCode(530);
					transactionVO.setErrorMessage("Your organization GST has been locked till "+dateUtil.getConvetedDate(societyVO.getGstCloseDate())+", So that no transactions can be added");
					return transactionVO;
				}else{					
					isGSTDateValid=true;
				}
			
			}else{
				isGSTDateValid=true;
			}*/
			
			//society lock date
			if(societyVO.getTxCloseDate()!=null){
				socLocDate=sdf.parse(societyVO.getTxCloseDate());
				if(txDate.compareTo(socLocDate)<=0){ 
					isSocDateValid=false;
					transactionVO.setStatusCode(530);
					transactionVO.setErrorMessage("Your organization accounts has been locked till "+dateUtil.getConvetedDate(societyVO.getTxCloseDate())+", So that no transactions can be added");
					return transactionVO;
				}else{					
					isSocDateValid=true;
				}
			
			}else{
				isSocDateValid=true;
			}
			
			//ledger lock date
			if((isAuditDateValid)&&(isSocDateValid)){ 				
				
								
					AccountHeadVO tempLedgerVO=ledgerService.getUpdatedBalances(ledgerEntry, societyID);
					if(txDate.compareTo(sdf.parse(tempLedgerVO.getEffectiveDate()))<=0){
						transactionVO.setStatusCode(553);
						transactionVO.setErrorMessage("Cannot complete your request since, ledger "+tempLedgerVO.getStrAccountHeadName()+"  effective date is "+dateUtil.getConvetedDate(tempLedgerVO.getEffectiveDate())+"  , Please check the ledger.");
						
						transactionVO.setStatusCode(552);
						transactionVO.setErrorMessage("Cannot complete your request since Ledger "+tempLedgerVO.getStrAccountHeadName()+" is inactive , Please check the ledger.");
					
					}else	if((tempLedgerVO.getLockDate()!=null)){
						if(txDate.compareTo(sdf.parse(tempLedgerVO.getLockDate())) >= 0){
							transactionVO.setStatusCode(200);
							transactionVO.setErrorMessage("Transaction validated successfully");
						}else{
							transactionVO.setStatusCode(531);
							transactionVO.setErrorMessage("Cannot complete your request since Ledger "+tempLedgerVO.getStrAccountHeadName()+" has been locked till "+dateUtil.getConvetedDate(tempLedgerVO.getLockDate())+", Please co-ordinate with your administrator to remove ledger or account lock.");
						
						 }
					 }else{
						
						transactionVO.setStatusCode(200);
						transactionVO.setErrorMessage("Transaction validated successfully");
					 }		
				
					
				  
				
					
				}
			   
					
			log.debug("Status code : "+transactionVO.getStatusCode()+" Message: "+transactionVO.getErrorMessage());
			
			
		}catch(Exception e){
			log.error("Exception in public TransactionVO validateTransactionLedgerWise(TransactionVO transactionVO,int societyID,int ledgerID) "+e);
		}
		
		log.debug("Exit : public TransactionVO validateTransactionLedgerWise(TransactionVO transactionVO,int societyID,int ledgerID)");
		return transactionVO;
	
	}
	
	
	/* Used for inserting transaction and inserting ledger_entries */
	public TransactionVO addInvoice(TransactionVO transactionVO,int societyID){
		int success=0;
		log.debug(" Entry : public TransactionVO addInvoice(TransactionVO transactionVO,int societyID)");
		try {
		TransactionMetaVO txMeta=transactionVO.getAdditionalProps();	
			transactionVO=insertTransaction(transactionVO, societyID);
			
			if(transactionVO.getTransactionID()>0){
			txMeta.setBillID(transactionVO.getTransactionID());
			txMeta.setInvoiceNO("INV_"+transactionVO.getTransactionID());
			transactionVO.setAdditionalProps(txMeta);
			success=accTransactionDAO.updateAdditionalProperties(transactionVO, societyID);
			
			}
			
		} catch (Exception e) {
			log.error("Exception in adding transaction "+e.getMessage()+" "+e);
		}
		log.debug(" Exit : public TransactionVO addInvoice(TransactionVO transactionVO,int societyID)");
		
		return transactionVO;
		
	}
	
	private int updateLedgerEntries(List newLedgerList,TransactionVO transaction,int societyID,int txID){
		log.debug("Entry : private int updateLedgerEntries(List oldLedgerList,List newLedgerList,String societyID)");
		int success=0;
		success=accTransactionDAO.deleteLedgerEntries(txID, societyID);
		try{
			for(int i=0;newLedgerList.size()>i;i++){
				
				LedgerEntries newEntry=(LedgerEntries) newLedgerList.get(i);
				AccountHeadVO tempLedger=ledgerService.getUpdatedBalances(newEntry, societyID);
				newEntry.setAmount(tempLedger.getAmount());
				
				success=accTransactionDAO.createSingleLedgerEntries(newEntry, txID, societyID);
				
			}
						
		}catch(Exception e){
			log.debug("Exception in private int updateLedgerEntries(List oldLedgerList,List newLedgerList,String societyID) "+e.getMessage());
		}
		
		
		
		
		log.debug("Exit : private int updateLedgerEntries(List oldLedgerList,List newLedgerList,String societyID)");
		return success;
		
	}
	
	public int deleteLedgerEntry(LedgerEntries ledgerEntry,String societyID) {
		int success=1;
		
		log.debug("Entry :public int deleteLedgerEntry(LedgerEntries ledgerEntry,String societyID)");
			
		success=accTransactionDAO.deleteLedgerEntry(ledgerEntry, societyID);
		
		log.debug("Exit :public int deleteLedgerEntry(LedgerEntries ledgerEntry,String societyID)");
		return success;
	}

	//@Override
	/* Used for deleting transactions i.e. adding is_deleted flag */
	public TransactionVO deleteTransaction(TransactionVO transactionVO,int societyID,int subGroupID) {
		int success=0;
		TransactionVO transaction =new TransactionVO();
		TransactionVO validationVO =new TransactionVO();
		log.debug("Entry : public int deleteTransaction(TransactionVO transaction,String societyID)");
		try {
			log.debug("Here tx ID is "+transactionVO.getTransactionID()+" "+transactionVO.getTransactionType()+"  "+transactionVO.getGroupID());
			Boolean deletable=false;
			Date socLocDate;
			transaction=getTransctionDetails(societyID, transactionVO.getTransactionID());
			transaction.setDescription(transactionVO.getDescription());
			transaction.setUpdatedBy(transactionVO.getUpdatedBy());
			
			validationVO=validateTransaction(transaction, societyID);
			
			if(validationVO.getStatusCode()==200){
			       success=accTransactionDAO.deleteTransaction(transaction,societyID);
				if(success!=0){
					if((transaction.getGroupID()==36)||(subGroupID==36)){
						success=1;
					}else if((transaction.getTransactionType().equalsIgnoreCase("Receipt"))&&((transaction.getGroupID()==1)||(transaction.getGroupID()==4)||(transaction.getGroupID()==5))){
				        int successDelRcpt=receiptInvoiceDomain.updateDeletedReceiptBalances(""+societyID, transaction);
				    }
					
					if((transaction.getHashComments()!=null)&&(transaction.getHashComments().length()>0)){
					    transaction.setSocietyID(societyID);
				        int successAddProps=accTransactionDAO.updateAdditionalProperties(transaction, societyID);
					}
					
					//Invoice delete
					if((transaction.getInvoiceID()>0)&&(transaction.getTransactionType().equalsIgnoreCase("Sales"))||(transaction.getTransactionType().equalsIgnoreCase("Purchase"))
							||(transaction.getTransactionType().equalsIgnoreCase("Credit Note"))||(transaction.getTransactionType().equalsIgnoreCase("Debit Note"))||(transaction.getTransactionType().equalsIgnoreCase("Journal"))){
						InvoiceVO invoiceVO=new InvoiceVO();
						invoiceVO.setInvoiceID(transaction.getInvoiceID());
						invoiceVO.setOrgID(societyID);
						invoiceVO.setNotes("");
						invoiceVO=invoiceService.deleteGSTInvoice(invoiceVO);
				    }
					transaction.setStatusCode(200);
					
				}else{
					transaction.setStatusCode(406);
					transaction.setErrorMessage("Unable to delete transaction");
				}
			}else{
				transaction.setStatusCode(validationVO.getStatusCode());
				transaction.setErrorMessage(validationVO.getErrorMessage());
			}
		
		} catch (NullPointerException e) {
			transaction.setStatusCode(400);
			transaction.setErrorMessage("Transaction cannot be deleted. Please contact helpdesk@esanchalak.com");
			log.info("NullPointerException in adding transaction , no hashComments available "+e.getMessage()+" "+e+transaction.getHashComments());
		
		} catch (Exception e) {
			transaction.setStatusCode(400);
			transaction.setErrorMessage("Transaction cannot be deleted. Please contact helpdesk@esanchalak.com");
			log.error("Exception occured in deleteTransaction "+e);
		}
		
		log.debug("Exit : public int deleteTransaction(TransactionVO transaction,String societyID)");
		return transaction;
	}
	
	/* Used for deleting transactions i.e. adding is_deleted flag */
	public TransactionVO changeTransactionStatus(TransactionVO transactionVO,int societyID,int subGroupID) {
		int success=0;
		TransactionVO transaction =new TransactionVO();
		TransactionVO validationVO =new TransactionVO();
		log.debug("Entry : public int changeTransactionStatus(TransactionVO transaction,String societyID)");
		try {
			log.debug("Here tx ID is "+transactionVO.getTransactionID()+" "+transactionVO.getTransactionType()+"  "+transactionVO.getGroupID());
			Boolean deletable=false;
			Date socLocDate;
			transaction=getTransctionDetails(societyID, transactionVO.getTransactionID());
			transaction.setDescription(transactionVO.getDescription());
			transaction.setUpdatedBy(transactionVO.getUpdatedBy());
			transaction.setIsDeleted(transactionVO.getIsDeleted());
			validationVO=validateTransaction(transaction, societyID);
			
			if(validationVO.getStatusCode()==200){
			       success=accTransactionDAO.changeTransactionStatus(transaction,societyID);
				if(success!=0){
					InvoiceVO invoiceVO=new InvoiceVO();
					invoiceVO.setInvoiceID(transaction.getInvoiceID());
					invoiceVO.setOrgID(transaction.getSocietyID());
					invoiceVO.setNotes(transaction.getDescription());
					invoiceVO.setUpdatedBy(transactionVO.getUpdatedBy());
					invoiceVO.setIsDeleted(transactionVO.getIsDeleted());
					if((invoiceVO.getInvoiceID()>0)&&(invoiceVO.getOrgID()>0)){
					
						int invSuccess=invoiceService.changeInvoiceStatus(invoiceVO);
					 
					}
					transaction.setStatusCode(200);
					
				}else{
					transaction.setStatusCode(406);
					transaction.setErrorMessage("Unable to changeTransactionStatus transaction");
				}
			}else{
				transaction.setStatusCode(validationVO.getStatusCode());
				transaction.setErrorMessage(validationVO.getErrorMessage());
			}
		
		} catch (NullPointerException e) {
			transaction.setStatusCode(400);
			transaction.setErrorMessage("Transaction cannot be updated. Please contact helpdesk@esanchalak.com");
			log.info("NullPointerException in updating transaction , no hashComments available "+e.getMessage()+" "+e+transaction.getHashComments());
		
		} catch (Exception e) {
			transaction.setStatusCode(400);
			transaction.setErrorMessage("Transaction cannot be updated. Please contact helpdesk@esanchalak.com");
			log.error("Exception occured in changeTransactionStatus "+e);
		}
		
		log.debug("Exit : public int changeTransactionStatus(TransactionVO transaction,String societyID)");
		return transaction;
	}
	
	/* Used for updating audit flag of  transactions i.e. adding is_audited flag */
	public TransactionVO changeAuditedTransactionStatus(TransactionVO transaction,int societyID) {
		int success=0;
		log.debug("Entry : public int changeAuditedTransactionStatus(TransactionVO transaction,String societyID)");
		try {
			log.debug("Here tx ID is "+transaction.getTransactionID()+" "+transaction.getTransactionType());
			
			
			success=accTransactionDAO.changeAuditedTransactionStatus(transaction,societyID);
			if(success>0){
				transaction.setStatusCode(200);
				transaction.setStatusDescription("Transactions audit status updated.");
			}
			
			
		} catch (Exception e) {
			log.error("Exception occured in changeAuditedTransactionStatus "+e);
		}
		
		log.debug("Exit : public int changeAuditedTransactionStatus(TransactionVO transaction,String societyID)");
		return transaction;
	}
	
	/* Used for updating audit flag of  transactionList i.e. adding is_audited flag */
	public TransactionVO changeAuditedTransactionListStatus(TransactionVO transaction,int orgID) {
		int success=0;
		log.debug("Entry : public int changeAuditedTransactionListStatus(TransactionVO transaction,String societyID)");
		try {
			
		if(transaction.getTransactionsList().size()>0){
			
			for(int i=0;transaction.getTransactionsList().size()>i;i++){
				TransactionVO txVO=transaction.getTransactionsList().get(i);
				txVO.setIsAudited(transaction.getIsAudited());
				txVO=changeAuditedTransactionStatus(transaction, orgID);
				transaction.setStatusCode(txVO.getStatusCode());
				transaction.setStatusDescription(txVO.getStatusDescription());
			}
			
			
		}
			
			
		} catch (Exception e) {
			log.error("Exception occured in changeAuditedTransactionListStatus "+e);
		}
		
		log.debug("Exit : public int changeAuditedTransactionListStatus(TransactionVO transaction,String societyID)");
		return transaction;
	}
	
	
	/* Used for deleting transactions i.e. adding is_deleted flag */
	public int deleteJournalEntries(InvoiceVO invoiceVO) {
		int success=0;
		log.debug("Entry : public int deleteJournalEntries(iInvoiceVO invoiceVO)");
		try {
			
			
			
			success = accTransactionDAO.deleteJournalEntries(invoiceVO);			
			
			log.debug("Exit : public int deleteJournalEntries(InvoiceVO invoiceVO)");	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug("Exception in public int deleteJournalEntries(InvoiceVO invoiceVO) "+e);
		}
		
		return success;
	}
	
	/* Used for updating report group  of transaction line item */
	public TransactionVO updateReportGroupJournalEntry(TransactionVO transactionVO) {
		int success=0;
		log.debug("Entry : public int updateReportGroupJournalEntry(TransactionVO transactionVO)");
		try {
			
			
			success = accTransactionDAO.updateReportGroupJournalEntry(transactionVO);
			
			if(success>0){
				
				if(transactionVO.getInvLineItemID()>0){
					
				 InvoiceVO invoiceVO=new InvoiceVO();
				 invoiceVO.setInvLineItemID(transactionVO.getInvLineItemID());
				 invoiceVO.setOrgID(transactionVO.getSocietyID());
				 invoiceVO.setInvoiceTags(transactionVO.getTxTags());
				 invoiceVO.setTransactionID("0");
				 
				 invoiceVO=invoiceService.updateReportGroupInvoiceLineItem(invoiceVO);
				 
						 if(invoiceVO.getStatusCode()==200){						
							 transactionVO.setStatusCode(200);
							}else{						
								transactionVO.setStatusCode(406);
								transactionVO.setErrorMessage("Unable to update report group");
							}
				}else{
					transactionVO.setStatusCode(200);				
				}
				 
				  
			}else{
				
				transactionVO.setStatusCode(406);
				transactionVO.setErrorMessage("Unable to update report group");
			}
			
			log.debug("Exit : public int updateReportGroupJournalEntry(TransactionVO transactionVO)");	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug("Exception in public int updateReportGroupJournalEntry(TransactionVO transactionVO) "+e);
		}
		
		return transactionVO;
	}
	
	
	
	/* Used for updating balances of ledger entries */
	private int updateAllLedgerEntriesBalance(LedgerEntries ledgerEntry,String societyID){
		int success=0;
		String updatedBalance="";
		BigDecimal closingBalance=BigDecimal.ZERO;
		BigDecimal currentBalance=BigDecimal.ZERO;
		currentBalance=ledgerEntry.getBalance();
		if(ledgerEntry.getCreditDebitFlag().equalsIgnoreCase("C")){
			if(ledgerEntry.getCreditAction()==1){
				updatedBalance="+"+ledgerEntry.getAmount();
				closingBalance=closingBalance.add(ledgerEntry.getAmount());
			}else{
				updatedBalance="-"+ledgerEntry.getAmount();
				closingBalance=closingBalance.subtract(ledgerEntry.getAmount());
			}
			
		}else if(ledgerEntry.getCreditDebitFlag().equalsIgnoreCase("D")){
			if(ledgerEntry.getDebitAction()==1){
				updatedBalance="+"+ledgerEntry.getAmount();
				closingBalance=closingBalance.add(ledgerEntry.getAmount());
			}else{
				updatedBalance="-"+ledgerEntry.getAmount();
				closingBalance=closingBalance.subtract(ledgerEntry.getAmount());
			}
		}
		currentBalance=currentBalance.add(closingBalance).abs();
		ledgerEntry.setBalance(currentBalance);
		//System.out.println(ledgerEntry.getLedgerID()+" the updated balance is "+updatedBalance);
		ledgerEntry.setUpdatedBalance(updatedBalance);
		
		success=accTransactionDAO.updateAllLedgersEntries(ledgerEntry,societyID);
		
		
		return success;
	}
	
	
	
//	This method is used to get ledgerEntries for the given transaction
	public List getLedgersEntryList(TransactionVO transactionVO,String societyID){
		List ledgerEntryList=null;
		log.debug("Entry : public List getLedgersEntryList(TransactionVO transactionVO,String societyID)");
		
		ledgerEntryList=accTransactionDAO.getLedgersEntryList(transactionVO, societyID);
		
		log.debug("Exit : public List getLedgersEntryList(TransactionVO transactionVO,String societyID)"+ledgerEntryList.size());
		return ledgerEntryList;
	}
	
	
		
	
	/*public List getTransctionList(String societyID,int ledgerID,String fromDate, String uptoDate,String type){
		List TransactionList=null;
		try {
			log.debug("Entry :public List getTransctionList(String societyID,int ledgerID,String fromDate, String uptoDate)");
			
			TransactionList=accTransactionDAO.getTransactionList(societyID, ledgerID, fromDate, uptoDate,type);
			BigDecimal sum=BigDecimal.ZERO;
			for(int i=0;TransactionList.size()>i;i++){
				
				TransactionVO lentry=(TransactionVO) TransactionList.get(i);
				sum=sum.add(lentry.getAmount());
				
				lentry.setSumAmt(sum);
				
			}
			
			log.debug("Exit :public List getTransctionList(String societyID,int ledgerID,String fromDate, String uptoDate)"+TransactionList.size());
		} catch (Exception e) {
			log.error("Exception :public List getTransctionList(String societyID,int ledgerID,String fromDate, String uptoDate) "+e );
		}
		return TransactionList;
		
	}*/
	
	
	public List getTransctionList(int societyID,int ledgerID,String fromDate, String uptoDate,String type){
		List TransactionList=null;
		SocietyVO societyVO=new SocietyVO();
		AccountHeadVO accountHeadVO=new AccountHeadVO();
		try {
			log.debug("Entry :public List getTransctionList(String societyID,int ledgerID,String fromDate, String uptoDate)");
			
			TransactionList=accTransactionDAO.getTransactionList(societyID, ledgerID, fromDate, uptoDate,type);
			
			
			log.debug("Exit :public List getTransctionList(String societyID,int ledgerID,String fromDate, String uptoDate)"+TransactionList.size());
		} catch (Exception e) {
			log.error("Exception :public List getTransctionList(String societyID,int ledgerID,String fromDate, String uptoDate) "+e );
		}
		return TransactionList;
		
	}
	
	public List getQuickTransctionList(int societyID,int ledgerID,String fromDate, String uptoDate,String type){
		List TransactionList=null;
		SocietyVO societyVO=new SocietyVO();
		AccountHeadVO accountHeadVO=new AccountHeadVO();
		try {
			log.debug("Entry :public List getQuickTransctionList(String societyID,int ledgerID,String fromDate, String uptoDate)");
			
			TransactionList=accTransactionDAO.getQuickTransactionList(societyID, ledgerID, fromDate, uptoDate, type);
			
			
			log.debug("Exit :public List getQuickTransctionList(String societyID,int ledgerID,String fromDate, String uptoDate)"+TransactionList.size());
		} catch (Exception e) {
			log.error("Exception :public List getQuickTransctionList(String societyID,int ledgerID,String fromDate, String uptoDate) "+e );
		}
		return TransactionList;
		
	}
	
	public List getReconcilationList(int societyID,int ledgerID,String fromDate, String uptoDate,String type){
		List TransactionList=null;
		SocietyVO societyVO=new SocietyVO();
		AccountHeadVO accountHeadVO=new AccountHeadVO();
		try {
			log.debug("Entry :public List getReconcilationList(String societyID,int ledgerID,String fromDate, String uptoDate,String type)");
			
			TransactionList=accTransactionDAO.getReconcilationList(societyID, ledgerID, fromDate, uptoDate,type);
			
			
			log.debug("Exit :public List getReconcilationList(String societyID,int ledgerID,String fromDate, String uptoDate,String type)"+TransactionList.size());
		} catch (Exception e) {
			log.error("Exception :public List getReconcilationList(String societyID,int ledgerID,String fromDate, String uptoDate,String type) "+e );
		}
		return TransactionList;
		
	}
	
	public List getBillTransactionList(int societyID,int ledgerID,String fromDate, String uptoDate,String type){
		List TransactionList=null;
		try {
			log.debug("Entry :public List getBillTransactionList(String societyID,int ledgerID,String fromDate, String uptoDate)");
			
			TransactionList=accTransactionDAO.getBillTransactionList(societyID, ledgerID, fromDate, uptoDate, type);
			
			
			log.debug("Exit :public List getBillTransactionList(String societyID,int ledgerID,String fromDate, String uptoDate)"+TransactionList.size());
		} catch (Exception e) {
			log.error("Exception :public List getBillTransactionList(String societyID,int ledgerID,String fromDate, String uptoDate) "+e );
		}
		return TransactionList;
		
	}
	
	public List getTransactionListOfProjects(int societyID,int ledgerID,String fromDate, String uptoDate,String projectName){
		List TransactionList=null;
		SocietyVO societyVO=new SocietyVO();
		AccountHeadVO accountHeadVO=new AccountHeadVO();
		try {
			log.debug("Entry :public List getTransactionListOfProjects(String societyID,int ledgerID,String fromDate, String uptoDate)");
			
			TransactionList=accTransactionDAO.getTransactionListOfProjects(societyID, ledgerID, fromDate, uptoDate, projectName);
			
			
			log.debug("Exit :public List getTransactionListOfProjects(String societyID,int ledgerID,String fromDate, String uptoDate)"+TransactionList.size());
		} catch (Exception e) {
			log.error("Exception :public List getTransactionListOfProjects(String societyID,int ledgerID,String fromDate, String uptoDate) "+e );
		}
		return TransactionList;
		
	}
	
	public List getTransactionListOfCategoryWise(int societyID,int ledgerID,String fromDate, String uptoDate,List categoryList){
		List TransactionList=null;
		SocietyVO societyVO=new SocietyVO();
		AccountHeadVO accountHeadVO=new AccountHeadVO();
		try {
			log.debug("Entry :public List getTransactionListOfProjects(String societyID,int ledgerID,String fromDate, String uptoDate)");
			
			TransactionList=accTransactionDAO.getTransactionListOfCategoryWise(societyID, ledgerID, fromDate, uptoDate, categoryList);
			
			
			log.debug("Exit :public List getTransactionListOfProjects(String societyID,int ledgerID,String fromDate, String uptoDate)"+TransactionList.size());
		} catch (Exception e) {
			log.error("Exception :public List getTransactionListOfProjects(String societyID,int ledgerID,String fromDate, String uptoDate) "+e );
		}
		return TransactionList;
		
	}
	
	/*This method is used to get trialBalnce report*/
	public List getTrialBalanceReport(int societyID,String fromDate, String uptoDate){
		List<AccountHeadVO> ledgerList=new ArrayList<AccountHeadVO>();
		List<AccountHeadVO> groupList=new ArrayList<AccountHeadVO>();
		List<AccountHeadVO> accHeadListList=new ArrayList<AccountHeadVO>();
		try{
		log.debug("Entry : public List getTrialBalanceReport(String societyID,String fromDate, String uptoDate)"+dateUtil.getPrevOrNextDate(fromDate, 0)+" ");
		
		//fromDate=dateUtil.getPrevOrNextDate(fromDate, 0);
		groupList=ledgerService.getAllGroupList(0,societyID);
		
		for(AccountHeadVO acHvO:groupList){
			BigDecimal crOpnBal=BigDecimal.ZERO;
			BigDecimal drOpnBal=BigDecimal.ZERO;
			BigDecimal crClsBal=BigDecimal.ZERO;
			BigDecimal drClsBal=BigDecimal.ZERO;
			BigDecimal openingBal=BigDecimal.ZERO;
			BigDecimal closingBal=BigDecimal.ZERO;
			
			
			int groupID=acHvO.getAccountGroupID();
			int rootID=acHvO.getRootID();
			
			String groupName=acHvO.getStrAccountHeadName();
			
			//log.info("---------------->"+acHvO.getStrAccountPrimaryHead()+acHvO.getStrAccountHeadName()+"  "+acHvO.getAccountGroupID());
			
			ledgerList=ledgerService.getLedgerList(societyID, acHvO.getAccountGroupID(),"G");
			//acHvO=getOpeningClosingBalance(societyID, groupID, fromDate, uptoDate, "G");
			if(acHvO.getAccountGroupID()==39){
				AccountHeadVO rptVo=ledgerService.getProffitLossBalance(societyID, fromDate, uptoDate,"tb",0,"S");
				
				acHvO.setClosingBalance(rptVo.getClosingBalance());
				acHvO.setOpeningBalance(rptVo.getOpeningBalance());
				
				acHvO.setOpeningBalance(rptVo.getOpeningBalance());
				
				//acHvO.setDrOpnBal(drOpnBal);
				if(rptVo.getOpeningBalance().signum()<0){
					acHvO.setDrOpnBal(rptVo.getOpeningBalance().abs());
					acHvO.setDrClsBal(rptVo.getOpeningBalance().abs());
				}else{
				acHvO.setCrOpnBal(rptVo.getOpeningBalance());
				acHvO.setCrClsBal(rptVo.getOpeningBalance());
				}acHvO.setAccountGroupID(groupID);
				acHvO.setStrAccountPrimaryHead(groupName);
			}else{
		
		for(AccountHeadVO acHVO:ledgerList){
					
			acHVO=ledgerService.getOpeningClosingBalance(societyID, acHVO.getLedgerID(), fromDate, uptoDate,"L");
			
			crClsBal=crClsBal.add(acHVO.getCrClsBal());
			drClsBal=drClsBal.add(acHVO.getDrClsBal());
			crOpnBal=crOpnBal.add(acHVO.getCrOpnBal());
			drOpnBal=drOpnBal.add(acHVO.getDrOpnBal());
			openingBal=openingBal.add(acHVO.getOpeningBalance());
			closingBal=closingBal.add(acHVO.getClosingBalance());
			
			
		}
		
		
		
		acHvO.setAccountGroupID(groupID);
		acHvO.setStrAccountPrimaryHead(groupName);
		acHvO.setClosingBalance(closingBal);
		acHvO.setOpeningBalance(openingBal);
		acHvO.setCrOpnBal(crOpnBal);
		acHvO.setDrOpnBal(drOpnBal);
		acHvO.setCrClsBal(crClsBal);
		acHvO.setDrClsBal(drClsBal);
			}
		accHeadListList.add(acHvO);
		
		}
		log.debug("Exit : public List getTrialBalanceReport(String societyID,String fromDate, String uptoDate)");
		}catch (Exception e) {
			log.error("Exception: public List getTrialBalanceReport(String societyID,String fromDate, String uptoDate) "+e );
		}
		return accHeadListList;
	}
	
	
	
	
	public List getLedgerReport(int societyID,String fromDate, String uptoDate,int groupID){
		List ledgerList=null;
		List accHeadListList=new ArrayList();
		try{
		log.debug("Entry : public List getLedgerReport(String societyID,String fromDate, String uptoDate)"+groupID);
		
		//fromDate=dateUtil.getPrevOrNextDate(fromDate, 0);
		ledgerList=ledgerService.getLedgerList(societyID, groupID,"G");
		
		for(int i=0;ledgerList.size()>i;i++){
			AccountHeadVO acHVO=(AccountHeadVO) ledgerList.get(i);
			String ledgerName=acHVO.getStrAccountHeadName();
			int ledgerID=acHVO.getLedgerID();
			acHVO=ledgerService.getOpeningClosingBalance(societyID, ledgerID, fromDate, uptoDate,"L");
			
			acHVO.setClosingBalance(acHVO.getClosingBalance());
			acHVO.setOpeningBalance(acHVO.getOpeningBalance());
			acHVO.setLedgerID(ledgerID);
			acHVO.setStrAccountHeadName(ledgerName);
			
			
			accHeadListList.add(i,acHVO);
		}
		
		
		log.debug("Exit : public List getLedgerReport(String societyID,String fromDate, String uptoDate)");
		}catch (Exception e) {
			log.error("Exception: public List getLedgerReport(String societyID,String fromDate, String uptoDate) "+e );
		}
		return accHeadListList;
	}
	
	
	public TransactionVO updateTrasactionBankDate(String bankDate,String txId, String societyID )
	{
		int sucess=0;
		TransactionVO txVO=new TransactionVO();
		int orgID=Integer.parseInt(societyID);
		int txID=Integer.parseInt(txId);
		
		try
		{
			log.debug("Entry : public int updateTrasactionBankDate(String bankDate,String txId )");
			
			TransactionVO duplicateVO=new TransactionVO();
			txVO.setTransactionID(txID);
			List ledgerEntryList=null;
			
			
			txVO=getTransctionDetails(orgID, txID);
			
			ledgerEntryList=txVO.getLedgerEntries();
			
			for(int i=0;ledgerEntryList.size()>i;i++){
				LedgerEntries ledgerEntry=(LedgerEntries) ledgerEntryList.get(i);
				
				if((ledgerEntry.getGroupID()==4)||(ledgerEntry.getGroupID()==95)||(ledgerEntry.getGroupID()==94)){
					duplicateVO=validateTransactionLedgerWise(txVO, orgID, ledgerEntry);
				}
				
							
			}
			
			if(duplicateVO.getStatusCode()==200){
				
				sucess=accTransactionDAO.updateTrasactionBankDate(bankDate,txId, societyID);
				if(sucess==1){
					txVO.setStatusCode(200);
					txVO.setErrorMessage("");
				}
				
				
			}else if((duplicateVO.getStatusCode()!=200)){
		    	   
		    	   txVO.setStatusCode(duplicateVO.getStatusCode());
		    	   txVO.setErrorMessage(duplicateVO.getErrorMessage());
		    	   
		       }
			
			
			
			
			
		}
		catch(Exception ex)
		{
			log.error("Exception in updateTrasactionBankDate(String bankDate,String txId ) : "+ex);
			
		}
		
		log.debug("Exit : public intupdateTrasactionBankDate(String bankDate,String txId )");
		return txVO;
	}
	
	//@Override
	
	/* Get list of all receipt for the period for a member */
	public List getReceiptForMember(int societyID ,int ledgerID,String fromDate,String uptoDate){
		List receiptList=null;
		log.debug("Entry : public List getReceiptForMember(String societyID ,String aptID,String fromDate,String uptoDate)");
		
		receiptList=accTransactionDAO.getReceiptForMember(societyID, ledgerID,fromDate,uptoDate);
		
		
		
		log.debug("Exit : public List getReceiptForMember(String societyID)");
		return receiptList;		
	}
	
	/* Get invoice list for WebService */
	public JSONResponseVO getReceiptForMember(int societyID,String fromDate,String uptoDate,int aptID){
		JSONResponseVO jsonVO=new JSONResponseVO();
		AccountHeadVO achVo=new AccountHeadVO();
		List invoiceList=new ArrayList();
		log.debug("Entry : JsonRespAccountsVO getReceiptForMember(String societyID,String aptID,String fromDate,String uptoDate)");
		int aptId=aptID;
		try {
			achVo=ledgerService.getLedgerID(aptId, societyID, "M","APTID");
			invoiceList=getReceiptForMember(societyID, achVo.getLedgerID(), fromDate, uptoDate);
			
			jsonVO.setAptID(aptID);
			jsonVO.setSocietyID(societyID);
			jsonVO.setFromDate(fromDate);
			jsonVO.setUptoDate(uptoDate);
			if(invoiceList.size()==0){
				jsonVO.setStatusCode(0);
			}else{
				jsonVO.setStatusCode(1);
				jsonVO.setObjectList(invoiceList);
			}
		} catch (Exception e) {
			jsonVO.setStatusCode(0);
			log.error("Exception occurred in JsonRespAccountsVO getInvoiceForMember(String societyID,String aptID,String fromDate,String uptoDate) "+e);
		}
		return jsonVO;
	}
	
	/* Get invoice list for WebService */
	public JSONResponseVO getReceiptForMemberApp(int societyID,int aptID){
		JSONResponseVO jsonVO=new JSONResponseVO();
		AccountHeadVO achVo=new AccountHeadVO();
		List invoiceList=new ArrayList();
		log.debug("Entry : JsonRespAccountsVO getReceiptForMember(String societyID,String aptID,String fromDate,String uptoDate)");
		int aptId=aptID;
		try {
			achVo=ledgerService.getLedgerID(aptId, societyID, "M","APTID");
			invoiceList=getReceiptForMemberAppList(societyID, achVo.getLedgerID());
			
			jsonVO.setAptID(aptID);
			jsonVO.setSocietyID(societyID);
			
			if(invoiceList.size()==0){
				jsonVO.setStatusCode(0);
			}else{
				jsonVO.setStatusCode(1);
				jsonVO.setObjectList(invoiceList);
			}
		} catch (Exception e) {
			jsonVO.setStatusCode(0);
			log.error("Exception occurred in JsonRespAccountsVO getInvoiceForMember(String societyID,String aptID,String fromDate,String uptoDate) "+e);
		}
		return jsonVO;
	}
	
	/* Get list of all receipt for the period for a member */
	public List getReceiptForMemberAppList(int societyID ,int ledgerID){
		List receiptList=null;
		log.debug("Entry : public List getReceiptForMemberApp(String societyID ,String aptID)");
		
		receiptList=accTransactionDAO.getReceiptForMemberApp(societyID, ledgerID);
		
		
		
		log.debug("Exit : public List getReceiptForMemberApp(String societyID)");
		return receiptList;		
	}
	
	public TransactionDAO getAccTransactionDAO() {
		return accTransactionDAO;
	}

	
	
	public List getLatestTransactions(int societyID,int ledgerID, String txType){
		
		List transactionList=new ArrayList();
		try{
		log.debug("Entry : public List getLatestTransactions(String societyID,String ledgerID)");
		
		transactionList=accTransactionDAO.getLatestTransactions(societyID, ledgerID, txType);
		
		
		log.debug("Exit : ppublic List getLatestTransactions(String societyID,String ledgerID)");
		}catch (Exception e) {
			log.error("Exception: public List getLatestTransactions(String societyID,String ledgerID) "+e );
		}
		return transactionList;
	}
	
	public int getLatestInvoiceNo(String societyiD){
		int invoiceNo=0;
		try
		{
			log.debug("Entry :public int getLatestInvoiceNo(String societyiD)");
			
			
				invoiceNo=accTransactionDAO.getLatestInvoiceNo(societyiD);
			
		}catch(Exception ex)
		{
			
			log.error("Exception in getCategoryHead : "+ex);
			
		}
			log.debug("Exit : public int getLatestInvoiceNo(String societyiD)");
		return invoiceNo;
		}
		
	/*This method is used to get single transaction details*/
	public TransactionVO getTransctionDetails(int societyID,int transactionID){		
		TransactionVO transactionVO=new TransactionVO();
		SocietyVO societyVO=new SocietyVO();
		List ledgerEntryList=null;
		
		try {
			log.debug("Entry :public TransactionVO getTransctionDetails(int societyID,int transactionID)");
			
			transactionVO=accTransactionDAO.getTransactionDetails(societyID, transactionID);
			
			ledgerEntryList=getLedgersEntryList(transactionVO, Integer.toString(societyID));
			
			transactionVO.setLedgerEntries(ledgerEntryList);
			
			log.debug("Exit :public TransactionVO getTransctionDetails(int societyID,int transactionID)");
		} catch (Exception e) {
			log.error("Exception :public TransactionVO getTransctionDetails(int societyID,int transactionID)"+e );
		}
		return transactionVO;
		
	}
	

	public TransactionVO getReceiptDetails(int societyID,String ledgerID,String receiptID){
		
		TransactionVO transactionVO=new TransactionVO();
		TransactionVO discVO=new TransactionVO();
	    List invoiceReceiptList=null;	
		try {
			log.debug("Entry : public TransactionVO getReceiptDetails(String societyID,String aptID,String receiptID)");
			//AccountHeadVO discountLdgr=ledgerService.getLedgerByType(societyID,"discount");
			
			transactionVO=accTransactionDAO.getReceiptDetails(societyID, ledgerID, receiptID);			
		    
            invoiceReceiptList=accTransactionDAO.getTransactionLinkedInvoiceList(societyID,Integer.parseInt(receiptID));
            transactionVO.setTransactionsList(invoiceReceiptList);
			/*listOfLineItem=accTransactionDAO.getLineItemsForReceipt(societyID, receiptID);
			discVO=accTransactionDAO.getDiscountedAmount(discountLdgr, transactionVO.getTransactionID(), societyID);
			transactionVO.setDiscountAmt(discVO.getDiscountAmt());
			transactionVO.setLedgerEntries(listOfLineItem);
			BigDecimal total=BigDecimal.ZERO;
			for(int i=0; i<listOfLineItem.size();i++){				
				TransactionVO txVO= (TransactionVO) listOfLineItem.get(i);
				total=total.add(txVO.getAmount());				
				transactionVO.setBalance(total);
			}*/
			
			log.debug("Exit : public TransactionVO getReceiptDetails(String societyID,String aptID,String receiptID)");
		} catch (Exception e) {
			log.error("Exception : public TransactionVO getReceiptDetails(String societyID,String aptID,String receiptID)"+e);
		}
		return transactionVO;
	}
	
public TransactionVO getReceiptDetailsForMemberApp(int societyID,String ledgerID,String refNo){
		
		TransactionVO transactionVO=new TransactionVO();
		TransactionVO discVO=new TransactionVO();
	    List invoiceReceiptList=null;	
		try {
			log.debug("Entry : public TransactionVO getReceiptDetailsForMemberApp(int societyID,String ledgerID,String refNo)");
			//AccountHeadVO discountLdgr=ledgerService.getLedgerByType(societyID,"discount");
			
			transactionVO=accTransactionDAO.getReceiptDetailsForMemberApp(societyID, ledgerID, refNo);			
		    
            invoiceReceiptList=accTransactionDAO.getTransactionLinkedInvoiceList(societyID,transactionVO.getTransactionID());
            transactionVO.setTransactionsList(invoiceReceiptList);
			
			
			log.debug("Exit : public TransactionVO getReceiptDetailsForMemberApp(int societyID,String ledgerID,String refNo)");
		} catch (Exception e) {
			log.error("Exception : public TransactionVO getReceiptDetailsForMemberApp(int societyID,String ledgerID,String refNo)"+e);
		}
		return transactionVO;
	}
	
	public List getMemberReceiptList(int orgID,String fromDate,String toDate){
		List receiptList=null;	
		try {
			log.debug("Entry :public List getMemberReceiptList(int orgID,String fromDate,String toDate)");
						
			receiptList=accTransactionDAO.getMemberReceiptList(orgID, fromDate, toDate);
						
			log.debug("Exit : public List getMemberReceiptList(int orgID,String fromDate,String toDate)"+receiptList.size());
		} catch (Exception e) {
			log.error("Exception : getMemberReceiptList"+e);
		}
		return receiptList;
	}
	
	public int getAllTransactionsCount(TransactionVO txVO){
		int transactionCount=0;
		
		try {
			log.debug("Entry : public int getAllTransactionsCount(TransactionVO txVO)");
			
			transactionCount=accTransactionDAO.getAllTransactionsCount(txVO);
			
			
			log.debug("Exit : public List getAllTransactionsCount(TransactionVO txVO)");
		}catch(NullPointerException ne){
			log.debug("Exit : public List getAllTransactionsCount(TransactionVO txVO)"+ne);
		} catch (Exception e) {
			log.error("Exception : public int getAllTransactionsCount(TransactionVO txVO) "+e );
		}
		return transactionCount;
		
	}
	
	public List getAllTransactionsList(TransactionVO txVO){
		List transactionList=null;
		
		try {
			log.debug("Entry : public List getDayBookTxList(TransactionVO txVO)");
			
			transactionList=accTransactionDAO.getAllTransactionsList(txVO);
			
			
			log.debug("Exit :  public List getAllTransactionsList(TransactionVO txVO)");
		} catch (Exception e) {
			log.error("Exception :  public List getAllTransactionsList(TransactionVO txVO) "+e );
		}
		return transactionList;
		
	}
	
	public List getAllNonRecordedTransactionsReportForLedger(TransactionVO txVO){
		List TransactionList=null;
		
		try {
			log.debug("Entry : public List getAllNonRecordedTransactionsReportForLedger(TransactionVO txVO)");
			
			TransactionList=accTransactionDAO.getAllNonRecordedTransactionsReportForLedger(txVO);
			
			
			log.debug("Exit : public List getAllNonRecordedTransactionsReportForLedger(TransactionVO txVO)");
		} catch (Exception e) {
			log.error("Exception : public List getAllNonRecordedTransactionsReportForLedger(TransactionVO txVO) "+e );
		}
		return TransactionList;
		
	}
	
	public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID, String type){
		OnlinePaymentGatewayVO onlinePaymentGatewayVO=new OnlinePaymentGatewayVO();
		
		try {
			log.debug("Entry : public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID)");
			
			onlinePaymentGatewayVO=accTransactionDAO.getOnlinePaymentDetails(societyID ,type);
			
			
			log.debug("Exit : public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID)");
		} catch (Exception e) {
			log.error("Exception : public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID) "+e );
		}
		return onlinePaymentGatewayVO;
		
	}
	
	public List getOnlinePaymentDetailsList(int societyID){
		
		List OnlinePaymentGatewayList=null;
		try {
			log.debug("Entry : public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID)");
			
			//OnlinePaymentGatewayList=accTransactionDAO.getOnlinePaymentDetailsList(societyID);
			
			
			log.debug("Exit : public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID)");
		} catch (Exception e) {
			log.error("Exception : public OnlinePaymentGatewayVO getOnlinePaymentDetails(int societyID) "+e );
		}
		return OnlinePaymentGatewayList;
		
	}
	
	public List generateScheduledTransactions(int societyID,int billingCycleID){
		ScheduledTransactionResponseVO responceObj=new ScheduledTransactionResponseVO();
		List transactionList=new ArrayList();
		
		try {
			log.debug("Entry : public ResponseVO generateScheduledTransactions(int societyID,int billingCycleID)");
			
			InvoiceBillingCycleVO billingCycleVO=new InvoiceBillingCycleVO();
			List chargeTypeList=accTransactionDAO.getChargeTypeList(societyID);
			if(billingCycleID==0){
			List billingCycleList=invoiceService.getBillingCycleList(societyID);
			billingCycleVO=(InvoiceBillingCycleVO) billingCycleList.get(billingCycleList.size()-1);
			}else billingCycleVO=invoiceService.getBillingCycleDetails(billingCycleID);
			List memberList=getActiveMemberListAll(societyID);
			for(int j=0;chargeTypeList.size()>j;j++){
				
			ChargesVO chargeTypeVO=(ChargesVO) chargeTypeList.get(j);
			if(chargeTypeVO.getChargeTypeID()!=10){
			
			for(int i=0;memberList.size()>i;i++){
				MemberVO memberVO=(MemberVO) memberList.get(i);
				
				TransactionVO txForMember=calculateTransaction(societyID, memberVO, chargeTypeVO.getChargeTypeID(),billingCycleVO);
				
				if(txForMember.getAmount().intValue()!=0){
				//txForMember=insertTransaction(txForMember, societyID);
				
				transactionList.add(txForMember);
				}
				
			}
			}
			}
			
			
			log.debug("Exit : public ResponseVO generateScheduledTransactions(int societyID,int billingCycleID)");
		} catch (Exception e) {
			log.error("Exception :public ResponseVO generateScheduledTransactions(int societyID,int billingCycleID)"+e );
		}
		return transactionList;
		
	}
	
	public List generateScheduledLateFeeTransactions(int societyID,int billingCycleID){
		ScheduledTransactionResponseVO responceObj=new ScheduledTransactionResponseVO();
		List transactionList=new ArrayList();
		
		try {
			log.debug("Entry : public ResponseVO generateScheduledLateFeeTransactions(int societyID,int billingCycleID)");
			
			InvoiceBillingCycleVO billingCycleVO=new InvoiceBillingCycleVO();
			
			if(billingCycleID==0){
			List billingCycleList=invoiceService.getBillingCycleList(societyID);
			billingCycleVO=(InvoiceBillingCycleVO) billingCycleList.get(billingCycleList.size()-1);
			}else
				billingCycleVO=invoiceService.getBillingCycleDetails(billingCycleID);
			List memberList=getActiveMemberListAll(societyID);
			int chargeTypeID=10;
			
			for(int i=0;memberList.size()>i;i++){
				MemberVO memberVO=(MemberVO) memberList.get(i);
				
				TransactionVO txForMember=calculateLateFeeTransaction(societyID, memberVO, chargeTypeID, billingCycleVO);
				
				if(txForMember.getAmount().intValue()!=0){
				//txForMember=insertTransaction(txForMember, societyID);
				
				transactionList.add(txForMember);
				}
			}
			
			
			
			log.debug("Exit : public ResponseVO generateScheduledLateFeeTransactions(int societyID,int billingCycleID)");
		} catch (Exception e) {
			log.error("Exception :public ResponseVO generateScheduledLateFeeTransactions(int societyID,int billingCycleID)"+e );
		}
		return transactionList;
		
	}
	
	
	/* Add bulk transactions  */
	public ScheduledTransactionResponseVO addBulkTransactions(List transactionList,int societyID){
		log.debug("Entry : public ScheduledTransactionResponseVO addBulkTransactions(List transactionList,int societyID)");
		int success=0;
		ScheduledTransactionResponseVO responseVO=new ScheduledTransactionResponseVO();
		try {
			ArrayList<String> chargeList=new ArrayList<String>();
			ArrayList<String> unique=new ArrayList<String>();
			int successCount=0;
			int failureCount=0;
			BigDecimal totalInvoicedAmount=BigDecimal.ZERO;
			String failureMember="";
			for(int i=0;transactionList.size()>i;i++){
				int insertFlag=0;
				List invList=new ArrayList();
				List memberChargeList=new ArrayList();
				TransactionVO txVO=(TransactionVO) transactionList.get(i);
				
				memberChargeList=txVO.getLedgerEntries();
				txVO=addInvoice(txVO, societyID);
				
				if(txVO.getTransactionID()!=0){
					totalInvoicedAmount=totalInvoicedAmount.add(txVO.getAmount());
					for(int j=0;memberChargeList.size()>j;j++){
						LedgerEntries ledgerEntry= (LedgerEntries) memberChargeList.get(j);
						//insertFlag=updateChargeStatus( societyID, livo);
						chargeList.add(""+ledgerEntry.getChargeID());
						responseVO.setFrequency(ledgerEntry.getChargeFrequency());
					}
				}
				
				if(txVO.getTransactionID()!=0){
					successCount=successCount+1;
					
					
				}else{
					failureCount=failureCount+1;
					failureMember=failureMember+" ,"+txVO.getLedgerName();
				}
				
				
			}
			log.debug(chargeList.size()+" Heere chargeList is "+ commonUtil.removeDuplicates(chargeList));
			responseVO.setSocietyID(societyID);
			responseVO.setChargeList(commonUtil.removeDuplicates(chargeList));
			responseVO.setSuccessCount(successCount);
			responseVO.setTotalTxAmount(totalInvoicedAmount);
			responseVO.setFailureCount(failureCount);
			responseVO.setFailureTxs(failureMember);
		} catch (Exception e) {
			log.error("Exception in public addBulkTransactions "+e);
		}
		
		
		log.debug("Exit : public ScheduledTransactionResponseVO addBulkTransactions(List transactionList,int societyID)");
		return responseVO;
	}
	
	
	
	/* Get list of all active members of the given society  */
	public List getActiveMemberListAll(int societyID){
		List memberList=null;
		log.debug("Entry : public List getActiveMemberList(String societyID)");
		
		try{
		
		memberList=memberServiceBean.getActivePrimaryMemberList(societyID);
		
		}catch (Exception e) {
			log.error("Exception in getActiveMemberList "+e);
		}
		
		log.debug("Exit : public List getActiveMemberList(String societyID)");
		return memberList;		
	}
	
	
	
	/* Get transactions for member  */
	public TransactionVO createTransactionForMember(int societyID,MemberVO memberVO,int chargeTypeID,InvoiceBillingCycleVO billingCycleVO){
		TransactionVO txVO=null;
		log.debug("Entry : public List createTransactionForMember(int societyID,MemberVO memberVO,int chargeTypeID)");
		
		try{
			
		List memberChargesList=accTransactionDAO.getApplicableCharges(societyID,  chargeTypeID);
		
		
	
		
		}catch (Exception e) {
			log.error("Exception in getActiveMemberList "+e);
		}
		
		log.debug("Exit : public List createTransactionForMember(int societyID,MemberVO memberVO,int chargeTypeID)");
		return txVO;		
	}
	
	
	/* Create transactions for member  */
	public TransactionVO calculateTransaction(int societyID,MemberVO memberVO,int chargeTypeID,InvoiceBillingCycleVO billingCycleVO){
		TransactionVO txVO=new TransactionVO();
		BigDecimal grandTotal=BigDecimal.ZERO;
		String txDate="";
		String invMode="";
		List lineItems=new ArrayList();
		ChargesVO roundOffVO=new ChargesVO();
		roundOffVO.setIsApplicable(false);
		
		log.debug("Entry : public TransactionVO calculateTransaction(int societyID,MemberVO memberVO,int chargeTypeID)");
		
		try{
			
			txVO.setLedgerID(memberVO.getLedgerID());
			txVO.setLedgerName(memberVO.getFullName());
			List chargesList=accTransactionDAO.getApplicableCharges(societyID, chargeTypeID);
		
		if(chargesList.size()>0){
			

			for(int i=0;chargesList.size()>i;i++){
				
				
				ChargesVO chargeVO=(ChargesVO) chargesList.get(i);
				
				chargeVO=getConditionalBalance(chargeVO, societyID,memberVO);
				
				if(chargeVO.getCalMethod().equalsIgnoreCase("Rnd")){
					roundOffVO=chargeVO;
					chargesList.remove(i);
					--i;
				}
				
				if((chargeVO.getIsApplicable())&&(chargeVO.getChargeTypeID()!=10)&&(chargeTypeID!=10)){
					grandTotal=grandTotal.add(chargeVO.getAmount()).setScale(2,2);
					
				if((chargeVO.getChargeTypeID()==1)||(chargeVO.getChargeTypeID()==10)||(chargeVO.getChargeTypeID()==2) ){
					invMode="R";
				}else invMode="O";
			
				txDate=chargeVO.getTxDate();
				txVO.setChargeTypeID(chargeVO.getChargeTypeID());				
				txVO.setBillingCycleID(billingCycleVO.getBillingCycleID());
				lineItems=chargesList;
				
				}else{
					chargesList.remove(i);
					--i;
				}
				
				
				
				
			}
		}
		
		
		if(memberVO.getTenantFeeOnFullAmt()!=0){
			if(memberVO.getIsRented()==1){
			ChargesVO tenantCharge=new ChargesVO();
			tenantCharge.setCreditLedgerID(memberVO.getTenantLedgerID());
			BigDecimal tenantFee=grandTotal.divide(new BigDecimal(10)).setScale(2,2);
			tenantCharge.setAmount(tenantFee);
			tenantCharge.setCalMethod("SR");
			tenantCharge.setGroupTypeID("0");
			lineItems.add(tenantCharge);
			grandTotal=grandTotal.add(tenantFee);
			
			}
		}
		if(roundOffVO.getIsApplicable()){
			BigDecimal roundedTotal=grandTotal;
			BigDecimal roundedAmt=BigDecimal.ZERO;
			if(roundOffVO.getGroupTypeID().equals("1")){
			roundedTotal=grandTotal.setScale(0,RoundingMode.UP);
			roundedAmt=roundedTotal.subtract(grandTotal);
			roundOffVO.setAmount(roundedAmt);
			}else 	if(roundOffVO.getGroupTypeID().equals("0")){
				roundedTotal=grandTotal.setScale(0,RoundingMode.DOWN);
				roundedAmt=roundedTotal.subtract(grandTotal);
				roundOffVO.setAmount(roundedAmt);
						}
		
			
			lineItems.add(roundOffVO);
			grandTotal=grandTotal.add(roundedAmt);
			
			}
		
		
		
		txVO.setAmount(grandTotal);
		txVO.setTransactionNumber("0");
	
			TransactionMetaVO txMeta=new TransactionMetaVO();
			txMeta.setInvoiceTypeID(chargeTypeID);
			txMeta.setLedgerID(memberVO.getLedgerID());
			txMeta.setInvoiceDate(txDate);
			txMeta.setInvoiceMode(invMode);
			txVO.setAdditionalProps(txMeta);
			
		
		txVO.setTransactionMode("");	
		txVO.setTransactionType("Journal");
		txVO.setTransactionDate(txDate);
		txVO.setDescription("For the period of "+dateUtil.getConvetedDate(billingCycleVO.getFromDate())+" to "+dateUtil.getConvetedDate(billingCycleVO.getUptoDate()));
		txVO=convertChargesIntoTransactionObj(txVO, lineItems);
		
		}catch (Exception e) {
			log.error("Exception in getActiveMemberList "+e);
		}
		
		log.debug("Exit : public TransactionVO calculateTransaction(int societyID,MemberVO memberVO,int chargeTypeID)");
		return txVO;		
	}
	
	/* Create late fee transactions for member  */
	public TransactionVO calculateLateFeeTransaction(int societyID,MemberVO memberVO,int chargeTypeID,InvoiceBillingCycleVO billingCycleVO){
		TransactionVO txVO=new TransactionVO();
		String description="";
		BigDecimal grandTotal=BigDecimal.ZERO;
		String penaltyTxDate="";
		List lineItems=new ArrayList();
		String invMode="";
		
		log.debug("Entry : public TransactionVO calculateTransaction(int societyID,MemberVO memberVO,int chargeTypeID)");
		
		try{
			
			txVO.setLedgerID(memberVO.getLedgerID());
			txVO.setLedgerName(memberVO.getFullName());
			List chargesList=accTransactionDAO.getApplicablePenaltyCharges(societyID);
		
		if(chargesList.size()>0){
			

			for(int i=0;chargesList.size()>i;i++){
				
				
				PenaltyChargesVO chargeVO=(PenaltyChargesVO) chargesList.get(i);
				
				 int dateDiff=dateUtil.getDateDiffernceInDays(dateUtil.convertStringDateToSql(chargeVO.getPenaltyChargeDate()),dateUtil.convertStringDateToSql(billingCycleVO.getDueDate()) );

				    if(dateDiff>=0){
				 	
					TransactionVO lateTxVO=new TransactionVO();
					
					txVO.setChargeTypeID(chargeVO.getChargeTypeID());
					
					penaltyTxDate=chargeVO.getPenaltyChargeDate();
										
					lateTxVO=calculateLateFeesForTransaction.getLateFee(chargeVO, societyID, memberVO, dateUtil.getPrevOrNextDate(chargeVO.getPrevChargeDate(), 0), chargeVO.getPenaltyChargeDate(),dateUtil.getPrevOrNextDate(billingCycleVO.getDueDate(), 0),billingCycleVO);
					
					if(lateTxVO.getAmount()!=null){
					grandTotal=grandTotal.add(lateTxVO.getAmount());
					
					description=lateTxVO.getDescription();
					}
					
					if((chargeVO.getChargeTypeID()==1)||(chargeVO.getChargeTypeID()==10)||(chargeVO.getChargeTypeID()==2) ){
						invMode="R";
					}else invMode="O";
					txVO.setChargeTypeID(chargeVO.getChargeTypeID());
					txVO.setBillingCycleID(billingCycleVO.getBillingCycleID());
					
				
				if(grandTotal.intValue()>0){
				chargeVO.setAmount(grandTotal);
				lineItems.add(chargeVO);
				}
				
			
				
				    }
				
				
			}
		}
		
		txVO.setAmount(grandTotal);
		txVO.setTransactionNumber("0");
		
			TransactionMetaVO txMeta=new TransactionMetaVO();
			txMeta.setInvoiceDate(penaltyTxDate);
			txMeta.setInvoiceTypeID(chargeTypeID);
			txMeta.setLedgerID(memberVO.getLedgerID());
			txMeta.setInvoiceMode(invMode);
			txVO.setGenerateInvoice(true);
			txVO.setAdditionalProps(txMeta);
			
		
		txVO.setTransactionMode("");	
		txVO.setTransactionType("Journal");
		txVO.setTransactionDate(penaltyTxDate);
		txVO=convertPenaltyChargesIntoTransactionObj(txVO, lineItems);
		if(description.length()<1){
			description="For the period of "+dateUtil.getConvetedDate(billingCycleVO.getFromDate())+" to "+dateUtil.getConvetedDate(billingCycleVO.getUptoDate());
		}
		txVO.setDescription(description);
		}catch (Exception e) {
			log.error("Exception in getActiveMemberList "+e);
		}
		
		log.debug("Exit : public TransactionVO calculateTransaction(int societyID,MemberVO memberVO,int chargeTypeID)");
		return txVO;		
	}
	
	
	public ChargesVO getConditionalBalance(ChargesVO chargeVO,int societyID,MemberVO memberVO){
		//log.debug("Entry : private InvoiceLineItemsVO getConditionalBalance(InvoiceLineItemsVO inVo,String societyID)"+inVo.getIsApplicable()+"  "+inVo.getChargeID());
		try{
			if((chargeVO.getCreditLedgerID()==0)&&(chargeVO.getDebitLedgerID()==00)){
				chargeVO.setIsApplicable(false);
			}else if((chargeVO.getCreditLedgerID()!=0)&&(chargeVO.getDebitLedgerID()!=0)){
				
				
			}else if((chargeVO.getCreditLedgerID()!=0)||(chargeVO.getDebitLedgerID()!=0)){
				
				if(chargeVO.getCalMethod().equalsIgnoreCase("A")){
					if(chargeVO.getGroupName().equalsIgnoreCase("S")){
						
							chargeVO.setIsApplicable(true);
							
					
					}else if((chargeVO.getGroupName().equalsIgnoreCase("U"))&&(chargeVO.getGroupTypeID().equalsIgnoreCase(String.valueOf(memberVO.getUnitID())))){
						
						chargeVO.setIsApplicable(true);
						
						
						
					}else if((chargeVO.getGroupName().equalsIgnoreCase("B")&&(chargeVO.getGroupTypeID().equalsIgnoreCase(String.valueOf(memberVO.getBuildingID()))))){
						
						chargeVO.setIsApplicable(true);
						
						
						
					}else if((chargeVO.getGroupName().equalsIgnoreCase("C"))){
						
						chargeVO.setIsApplicable(true);
						
						
						
					}else if((chargeVO.getGroupName().equalsIgnoreCase("Sq"))&&(new BigDecimal(chargeVO.getGroupTypeID()).compareTo(memberVO.getArea())==0)){
						
						chargeVO.setIsApplicable(true);
						
						
						
					}else if((chargeVO.getGroupName().equalsIgnoreCase("A")&&(chargeVO.getGroupTypeID().equalsIgnoreCase(String.valueOf(memberVO.getAptID()))))){
						
						chargeVO.setIsApplicable(true);
						
						
						
					}
					else if((chargeVO.getGroupName().equalsIgnoreCase("R"))||(chargeVO.getGroupName().equalsIgnoreCase("SR"))||(chargeVO.getGroupName().equalsIgnoreCase("UR"))||(chargeVO.getGroupName().equalsIgnoreCase("BR"))||(chargeVO.getGroupName().equalsIgnoreCase("AR"))||(chargeVO.getGroupName().equalsIgnoreCase("SQR"))){
						if(memberVO.getIsRented()==1)
							if(chargeVO.getGroupName().equalsIgnoreCase("SR")){
								
								chargeVO.setIsApplicable(true);
								
						
						}else if((chargeVO.getGroupName().equalsIgnoreCase("UR")&&(chargeVO.getGroupTypeID().equalsIgnoreCase(String.valueOf(memberVO.getUnitID()))))){
							
							chargeVO.setIsApplicable(true);
							
							
							
						}else if((chargeVO.getGroupName().equalsIgnoreCase("BR")&&(chargeVO.getGroupTypeID().equalsIgnoreCase(String.valueOf(memberVO.getBuildingID()))))){
							
							chargeVO.setIsApplicable(true);
							
							
							
						}else if((chargeVO.getGroupName().equalsIgnoreCase("CR"))){
							
							chargeVO.setIsApplicable(true);
							
							
							
						}else if((chargeVO.getGroupName().equalsIgnoreCase("Sqr"))&&(new BigDecimal(chargeVO.getGroupTypeID()).compareTo(memberVO.getArea())==0)){
							
							chargeVO.setIsApplicable(true);
							
							
							
						}else if((chargeVO.getGroupName().equalsIgnoreCase("AR")&&(chargeVO.getGroupTypeID().equalsIgnoreCase(String.valueOf(memberVO.getAptID()))))){
							
							chargeVO.setIsApplicable(true);
							
							
							
						}else
							chargeVO.setIsApplicable(false);
						
						
						
					}else if((chargeVO.getGroupName().equalsIgnoreCase("G")||(chargeVO.getGroupName().equalsIgnoreCase("GR")))){
						if(memberVO.getInvoiceGroups().length()>0){
							String str = memberVO.getInvoiceGroups();
							ArrayList aList= new ArrayList(Arrays.asList(str.split(",")));
							String entity=chargeVO.getGroupTypeID();
							if(chargeVO.getGroupName().equalsIgnoreCase("GR")&&(aList.contains(chargeVO.getGroupTypeID()))){
								if(memberVO.getIsRented()==1){
									chargeVO.setIsApplicable(true);
								}else chargeVO.setIsApplicable(false);
							}else 	if(chargeVO.getGroupName().equalsIgnoreCase("G")&&(aList.contains(chargeVO.getGroupTypeID()))){
								
									chargeVO.setIsApplicable(true);
								}else chargeVO.setIsApplicable(false);
							}else 
							chargeVO.setIsApplicable(false);
							
					}else chargeVO.setIsApplicable(false);
					
				}else if(chargeVO.getCalMethod().equalsIgnoreCase("C")){
					 if((chargeVO.getGroupName().equalsIgnoreCase("C"))){
						 BigDecimal due=BigDecimal.ZERO;
						 BigDecimal percentage=chargeVO.getAmount().multiply(new BigDecimal("0.01")).setScale(6);
							due=memberVO.getCostOfUnit().multiply(percentage).setScale(chargeVO.getPrecison(),RoundingMode.HALF_UP);
							chargeVO.setAmount(due);
						chargeVO.setIsApplicable(true);
						
						
						
					}else  if(chargeVO.getGroupName().equalsIgnoreCase("CR")){
						if(memberVO.getIsRented()==1){
						BigDecimal due=BigDecimal.ZERO;
						BigDecimal percentage=chargeVO.getAmount().multiply(new BigDecimal("0.01")).setScale(6);
						due=memberVO.getCostOfUnit().multiply(percentage).setScale(chargeVO.getPrecison(),RoundingMode.HALF_UP);
						chargeVO.setAmount(due);
						}else
							chargeVO.setIsApplicable(false);
						
						
						
					}else if((chargeVO.getGroupName().equalsIgnoreCase("G")||(chargeVO.getGroupName().equalsIgnoreCase("GR")))){
						if(memberVO.getInvoiceGroups().length()>0){
							String str = memberVO.getInvoiceGroups();
							ArrayList aList= new ArrayList(Arrays.asList(str.split(",")));
							String entity=chargeVO.getGroupTypeID();
							if(chargeVO.getGroupName().equalsIgnoreCase("GR")&&(aList.contains(chargeVO.getGroupTypeID()))){
								if(memberVO.getIsRented()==1){
									BigDecimal due=BigDecimal.ZERO;
									BigDecimal percentage=chargeVO.getAmount().multiply(new BigDecimal("0.01")).setScale(6);
									due=memberVO.getCostOfUnit().multiply(percentage).setScale(chargeVO.getPrecison(),RoundingMode.HALF_UP);
									chargeVO.setAmount(due);
								}else chargeVO.setIsApplicable(false);
							}
							
							if(chargeVO.getGroupName().equalsIgnoreCase("G")&&(aList.contains(chargeVO.getGroupTypeID()))){
								
								 BigDecimal due=BigDecimal.ZERO;
								 BigDecimal percentage=chargeVO.getAmount().multiply(new BigDecimal("0.01")).setScale(6);
									due=memberVO.getCostOfUnit().multiply(percentage).setScale(chargeVO.getPrecison(),RoundingMode.HALF_UP);
									chargeVO.setAmount(due);
								chargeVO.setIsApplicable(true);
								}else chargeVO.setIsApplicable(false);
							}else 
							chargeVO.setIsApplicable(false);
							
					}
					 
					 
					 }else if(chargeVO.getCalMethod().equalsIgnoreCase("Sq")){
						 if((chargeVO.getGroupName().equalsIgnoreCase("Sq"))){
							 BigDecimal due=BigDecimal.ZERO;
							due=memberVO.getArea().multiply(chargeVO.getAmount()).setScale(chargeVO.getPrecison(),RoundingMode.HALF_UP);
							chargeVO.setIsApplicable(true);
							chargeVO.setAmount(due);
							
							
							
						}else  if(chargeVO.getGroupName().equalsIgnoreCase("SQR")){
							if(memberVO.getIsRented()==1){
							BigDecimal due=BigDecimal.ZERO;
							BigDecimal rental=chargeVO.getAmount().divide(new BigDecimal("10")).setScale(2);
							due=memberVO.getArea().multiply(rental).setScale(chargeVO.getPrecison(),RoundingMode.UP);
							if(due.intValue()==0){
									chargeVO.setIsApplicable(false);
								}else chargeVO.setIsApplicable(true);
								
								chargeVO.setAmount(due);
							}else
								chargeVO.setIsApplicable(false);
							
							
							
						}else if((chargeVO.getGroupName().equalsIgnoreCase("G")||(chargeVO.getGroupName().equalsIgnoreCase("GR")))){
							if(memberVO.getInvoiceGroups().length()>0){
								String str = memberVO.getInvoiceGroups();
								ArrayList aList= new ArrayList(Arrays.asList(str.split(",")));
								String entity=chargeVO.getGroupTypeID();
								if(chargeVO.getGroupName().equalsIgnoreCase("GR")&&(aList.contains(chargeVO.getGroupTypeID()))){
									if(memberVO.getIsRented()==1){
										BigDecimal due=BigDecimal.ZERO;
										BigDecimal rental=chargeVO.getAmount().divide(new BigDecimal("10")).setScale(2);
										due=memberVO.getArea().multiply(rental).setScale(chargeVO.getPrecison(),RoundingMode.UP);
									
										if(chargeVO.getPrecison()==10){
											due=new BigDecimal(due.setScale(-1, RoundingMode.UP).toPlainString());
										}
										if(due.intValue()==0){
											chargeVO.setIsApplicable(false);
										}else chargeVO.setIsApplicable(true);
											chargeVO.setAmount(due);
									}else chargeVO.setIsApplicable(false);
								}else if(chargeVO.getGroupName().equalsIgnoreCase("G")&&(aList.contains(chargeVO.getGroupTypeID()))){
									
									 BigDecimal due=BigDecimal.ZERO;
										due=memberVO.getArea().multiply(chargeVO.getAmount()).setScale(chargeVO.getPrecison(),RoundingMode.HALF_UP);
										
										//due=due.multiply(new BigDecimal("12")).setScale(2, RoundingMode.HALF_UP);
										chargeVO.setIsApplicable(true);
										
										
										if(chargeVO.getPrecison()==10){
											due=new BigDecimal(due.setScale(-1, RoundingMode.UP).toPlainString());
										}
										chargeVO.setAmount(due);
									}else chargeVO.setIsApplicable(false);
								}else 
								chargeVO.setIsApplicable(false);
								
						}
					
					
					
					
					
				}else if(chargeVO.getCalMethod().equalsIgnoreCase("M")){
					String str = memberVO.getInvoiceGroups();
					if(str.length()>0){
					ArrayList aList= new ArrayList(Arrays.asList(str.split(",")));
					
					if((chargeVO.getGroupName().equalsIgnoreCase("M"))&&(aList.contains("M"))){//M - Multiplyer number.
						
						BigDecimal due=BigDecimal.ZERO;
						String multiplier=(String) aList.get(1);
						BigDecimal multplier=new BigDecimal(multiplier);					
						
						due=multplier.multiply(chargeVO.getAmount()).setScale(chargeVO.getPrecison(),RoundingMode.HALF_UP);
						chargeVO.setAmount(due);
						chargeVO.setIsApplicable(true);
						
						
					}
					}else chargeVO.setIsApplicable(false);
				
			}else if(chargeVO.getCalMethod().equalsIgnoreCase("Rnd")){
				String str = memberVO.getInvoiceGroups();
			
				
		
					chargeVO.setIsApplicable(true);
					
					
				
		
			
		}

				if((chargeVO.getIsApplicable()&&(chargeVO.getSocGstIN()!=null)&&(chargeVO.getHsnsacCode()!=null))){
					if((memberVO.getMemberStateName()==null)||(memberVO.getSocietyStateName()==null)){
						BigDecimal cgst=BigDecimal.ZERO;
					BigDecimal sgst=BigDecimal.ZERO;
					BigDecimal gstRate=chargeVO.getGstRate().divide(new BigDecimal("2")).setScale(2);
					BigDecimal percentage=gstRate.multiply(new BigDecimal("0.01")).setScale(6);
					cgst=chargeVO.getAmount().multiply(percentage).setScale(2);
					sgst=chargeVO.getAmount().multiply(percentage).setScale(2);
					chargeVO.setcGstAmount(cgst);
					chargeVO.setcGstRate(gstRate);
					chargeVO.setsGstAmount(sgst);
					chargeVO.setsGstRate(gstRate);
				
				}else{
						if(memberVO.getMemberStateName().equalsIgnoreCase(memberVO.getSocietyStateName())){
						BigDecimal cgst=BigDecimal.ZERO;
						BigDecimal sgst=BigDecimal.ZERO;
						BigDecimal gstRate=chargeVO.getGstRate().divide(new BigDecimal("2")).setScale(2);
						BigDecimal percentage=gstRate.multiply(new BigDecimal("0.01")).setScale(6);
						cgst=chargeVO.getAmount().multiply(percentage).setScale(2);
						sgst=chargeVO.getAmount().multiply(percentage).setScale(2);
						chargeVO.setcGstAmount(cgst);
						chargeVO.setcGstRate(gstRate);
						chargeVO.setsGstAmount(sgst);
						chargeVO.setsGstRate(gstRate);
					}else{
					BigDecimal igst=BigDecimal.ZERO;
					BigDecimal gstRate=chargeVO.getGstRate();
					BigDecimal percentage=gstRate.multiply(new BigDecimal("0.01")).setScale(6);
					igst=chargeVO.getAmount().multiply(percentage).setScale(2);
					chargeVO.setiGstAmount(igst);
					chargeVO.setiGstRate(gstRate);
					}
				}
				
			}			
			}
		}catch(Exception e){
			log.error("Exception in getConditionalBalance : "+e+" "+e.getMessage());
		}
		log.debug("Exit : private InvoiceLineItemsVO getConditionalBalance(InvoiceLineItemsVO inVo,String societyID)"+chargeVO.getIsApplicable()+"  "+chargeVO.getChargeID() );
		return chargeVO;
	}
	
	private TransactionVO convertChargesIntoTransactionObj(TransactionVO txVO,List chargeList){
		
		List ledgerEntries=new ArrayList();
		LedgerEntries revLedgerEntry=new LedgerEntries();
		log.debug("Entry : private List convertChargesIntoLedgerEntries(List chargeList) ");
		
		try {
			if(chargeList.size()>0){
				for(int i=0;chargeList.size()>i;i++){
					ChargesVO chargeVO=(ChargesVO) chargeList.get(i);
					LedgerEntries tempLdgrEnty=new LedgerEntries();
					tempLdgrEnty.setLedgerID(chargeVO.getCreditLedgerID());
					tempLdgrEnty.setAmount(chargeVO.getAmount());
					tempLdgrEnty.setStrComments(chargeVO.getDescription());
					tempLdgrEnty.setChargeID(chargeVO.getChargeID());
					tempLdgrEnty.setChargeFrequency(chargeVO.getChargeFrequency());
					if((chargeVO.getCalMethod().equalsIgnoreCase("Rnd")&&(chargeVO.getGroupTypeID().equalsIgnoreCase("0")))){
						tempLdgrEnty.setCreditDebitFlag("D");
					}else
					tempLdgrEnty.setCreditDebitFlag("C");
					ledgerEntries.add(tempLdgrEnty);
					
				}
				
				revLedgerEntry=convertInvoiceTOLedgerEntries(txVO);
				
				ledgerEntries.add(revLedgerEntry);
				txVO.setLedgerEntries(ledgerEntries);
				
				
				
			}
		} catch (Exception e) {
			log.debug("Exception : private List convertChargesIntoLedgerEntries(List chargeList) ");
		}
		
		
		log.debug("Exit : private List convertChargesIntoLedgerEntries(List chargeList) ");
		return txVO;
		
	}
	
	private TransactionVO convertPenaltyChargesIntoTransactionObj(TransactionVO txVO,List chargeList){
		
		List ledgerEntries=new ArrayList();
		LedgerEntries revLedgerEntry=new LedgerEntries();
		log.debug("Entry : private List convertChargesIntoLedgerEntries(List chargeList) ");
		
		try {
			if(chargeList.size()>0){
				for(int i=0;chargeList.size()>i;i++){
					PenaltyChargesVO chargeVO=(PenaltyChargesVO) chargeList.get(i);
					LedgerEntries tempLdgrEnty=new LedgerEntries();
					tempLdgrEnty.setLedgerID(chargeVO.getLedgerID());
					tempLdgrEnty.setAmount(chargeVO.getAmount());
					tempLdgrEnty.setStrComments(chargeVO.getDescription());
					tempLdgrEnty.setChargeID(chargeVO.getPenaltyChargeID());
					tempLdgrEnty.setChargeFrequency(chargeVO.getChargeFrequency());
					tempLdgrEnty.setCreditDebitFlag("C");
					ledgerEntries.add(tempLdgrEnty);
					
				}
				
				revLedgerEntry=convertInvoiceTOLedgerEntries(txVO);
				
				ledgerEntries.add(revLedgerEntry);
				txVO.setLedgerEntries(ledgerEntries);
				
				
				
			}
		} catch (Exception e) {
			log.debug("Exception : private List convertChargesIntoLedgerEntries(List chargeList) ");
		}
		
		
		log.debug("Exit : private List convertChargesIntoLedgerEntries(List chargeList) ");
		return txVO;
		
	}
	
	
	/* Convert Invoice VO to TxVO  */
	private LedgerEntries convertInvoiceTOLedgerEntries(TransactionVO txVO){
		LedgerEntries ledgerEntry=new LedgerEntries();
		
		log.debug("Entry : private LedgerEntries convertInvoiceLineItemsTOLedgerEntries(InvoiceLineItemsVO invoiceVO)");
		
		
		
		ledgerEntry.setAmount(txVO.getAmount());
		
		if(ledgerEntry.getAmount().signum()>0){
			ledgerEntry.setCreditDebitFlag("D");
			}else ledgerEntry.setCreditDebitFlag("C");
		ledgerEntry.setLedgerID(txVO.getLedgerID());
		ledgerEntry.setStrComments("Auto generated journal entry from "+txVO.getFromDate()+" to "+txVO.getToDate());
		
		log.debug("amount >"+ledgerEntry.getAmount()+" ledgerID -->"+ledgerEntry.getLedgerID()+"  "+ledgerEntry.getStrComments());
			
		
		
		log.debug("Exit : private LedgerEntries convertInvoiceLineItemsTOLedgerEntries(InvoiceLineItemsVO invoiceVO)");
		return ledgerEntry;		
	}
	
	/* Get details of a charge  */
	public ChargesVO getChargeDetails(int chargeID,int societyID){
		ChargesVO chargeVO=new ChargesVO();
		
		log.debug("Entry : public List getChargeDetails(int chargeID,int societyID)");
		
		try{
		
		chargeVO=accTransactionDAO.getChargeDetails(chargeID,societyID);
		
		
		
		}catch (Exception e) {
			log.error("Exception in public List getChargeDetails(int chargeID,int societyID) "+e);
		}
		
		log.debug("Exit : public List getChargeDetails(int chargeID,int societyID)");
		return chargeVO;		
	}
	/* Get details of a penalty charge  */
	public ChargesVO getPenaltyChargeDetails(int chargeID,int societyID){
		ChargesVO chargeVO=new ChargesVO();
		
		log.debug("Entry : public List getChargeDetails(int chargeID,int societyID)");
		
		try{
		
		chargeVO=accTransactionDAO.getPenaltyChargeDetails(chargeID, societyID);
		
		
		
		}catch (Exception e) {
			log.error("Exception in public List getChargeDetails(int chargeID,int societyID) "+e);
		}
		
		log.debug("Exit : public List getChargeDetails(int chargeID,int societyID)");
		return chargeVO;		
	}
	
	/* Update the charge period after adding the invoice  */
	public int updateChargeStatus(int societyID,ChargesVO chargeVO){
		int insertFlag=0;
		log.debug("Entry : public int updateChargeStatus(int societyID,ChargesVO chargeVO)");
		
		insertFlag=accTransactionDAO.updateChargeStatus( societyID, chargeVO);
		
		log.debug("Exit : public int updateChargeStatus(int societyID,ChargesVO chargeVO)");
		return insertFlag;		
	}
	
	/* Update the penalty charge period after adding the invoice  */
	public int updatePenaltyChargeStatus(int societyID,ChargesVO chargeVO){
		int insertFlag=0;
		log.debug("Entry : public int updatePenaltyChargeStatus(int societyID,ChargesVO chargeVO)");
		
		insertFlag=accTransactionDAO.updatePenaltyChargeStatus( societyID, chargeVO);
		
		log.debug("Exit : public int updatePenaltyChargeStatus(int societyID,ChargesVO chargeVO)");
		return insertFlag;		
	}
	
	
	/* Update the Societys Late fee charging date  */
	public int updateLateFeeChargeDate(int societyID,ChargesVO chargeVO){
		int insertFlag=0;
		log.debug("Entry : public int updateLateFeeChargeDate(int societyID,ChargesVO chargeVO)");
		try{
			
			insertFlag=accTransactionDAO.updateLateFeeChargeDate(societyID, chargeVO);
		} catch (Exception e) {
		log.error("Exception at updateChargeStatus "+e);
		}
		
		log.debug("Exit : public int updateLateFeeChargeDate(int societyID,int apartmentID)"+insertFlag);
		return insertFlag;		
	}
	
	
	/* Add raw bank statements after parsing    */
	public int addBankStatements(BankStatement bankStatementVO){
		int insertFlag=0;
		log.debug("Entry : public int addBankStatements(List bankStatementList)");
		try{
			
			if(bankStatementVO.getOverride()==1){
				
				int deleteFlag=accTransactionDAO.deleteBankStatements(bankStatementVO);
				
				if(bankStatementVO.getBankStatementList().size()>0){
					
					for(int i=0;bankStatementVO.getBankStatementList().size()>i;i++){
						
						BankStatement bankStmt=(BankStatement) bankStatementVO.getBankStatementList().get(i);
						
						
						 insertFlag=accTransactionDAO.addBankStatements(bankStmt);
					
					}
					
				}
				
			}else if(bankStatementVO.getOverride()==0){
				BankStatement latestTx=accTransactionDAO.getLastBankStatementsObj(bankStatementVO);
				
				if(latestTx.getToDate()!=null){
					
					if(bankStatementVO.getBankStatementList().size()>0){
						
						for(int i=0;bankStatementVO.getBankStatementList().size()>i;i++){
							
							BankStatement bankStmt=(BankStatement) bankStatementVO.getBankStatementList().get(i);
							
							int diffInDays=dateUtil.getDateDiffernceInDays( bankStmt.getValueDate(),latestTx.getToDate());
							if(diffInDays>0){
								
								
							
							 insertFlag=accTransactionDAO.addBankStatements(bankStmt);
							}
						}
						
					}
					
				}else if((latestTx.getBankAccNumber().equalsIgnoreCase("0")||(latestTx.getToDate()==null))){
					
					if(bankStatementVO.getBankStatementList().size()>0){
						
						for(int i=0;bankStatementVO.getBankStatementList().size()>i;i++){
							
							BankStatement bankStmt=(BankStatement) bankStatementVO.getBankStatementList().get(i);
							
							
							 insertFlag=accTransactionDAO.addBankStatements(bankStmt);
							}
						}
						
					
				
			}
					
				}
				
		} catch (Exception e) {
		log.error("Exception at addBankStatements "+e);
		}
		
		log.debug("Exit : public int addBankStatements(List bankStatementList)"+insertFlag);
		return insertFlag;		
	}
	
	
	/* Add payUMoney  statements after parsing    */
	public int addPayUStatements(OnlinePaymentGatewayVO payUVO){
		int insertFlag=0;
		log.debug("Entry : public int addPayUStatements(OnlinePaymentGatewayVO payUVO)");
		try{
			
			
				
				
				
					
					if(payUVO.getObjectList().size()>0){
						
						for(int i=0;payUVO.getObjectList().size()>i;i++){
							
							OnlinePaymentGatewayVO onlinePayUVO=payUVO.getObjectList().get(i);
							
							insertFlag=accTransactionDAO.addPayUStatements(onlinePayUVO);
							
						}
						
					}
					
			
					
				
				
		} catch (Exception e) {
		log.error("Exception at addPayUStatements(OnlinePaymentGatewayVO payUVO) "+e);
		}
		
		log.debug("Exit : public int addPayUStatements(OnlinePaymentGatewayVO payUVO)"+insertFlag);
		return insertFlag;		
	}
	
	
	/* Add raw bank statements after parsing    */
	public int addBankTransactions(BankStatement bankStatementVO){
		int insertFlag=0;
		log.debug("Entry : public int addBankStatements(List bankStatementList)");
		try{
			
			
					
					if(bankStatementVO.getTransactionList().size()>0){
						
						for(int i=0;bankStatementVO.getTransactionList().size()>i;i++){
							
							TransactionVO txVO=bankStatementVO.getTransactionList().get(i);
							
							
							 txVO=insertTransaction(txVO, bankStatementVO.getOrgID());
							 if(txVO.getStatusCode()==200)
								 insertFlag++;
							}
						}
						
					
				
			
					
				
				
		} catch (Exception e) {
		log.error("Exception at addBankStatements "+e);
		}
		
		log.debug("Exit : public int addBankStatements(List bankStatementList)"+insertFlag);
		return insertFlag;		
	}
	
	/* Add raw bank statements after parsing    */
	public int reoncileTransaction(BankStatement bankStatementVO){
		int txID=0;
		log.debug("Entry : public int reoncileTransaction(BankStatement bankStatementVO)");
		try{
		/*	List matchingTxs=accTransactionDAO.getMatchingTransactionList(bankStatementVO);
			TransactionVO txVo=new TransactionVO();
			if(matchingTxs.size()>0){
				if(matchingTxs.size()>1){
					txVo=getNearestDate(matchingTxs, bankStatementVO.getBookingDate().toString());
				}else
						txVo=(TransactionVO) matchingTxs.get(0);
				
				
				
			
				
				txID=txVo.getTransactionID();
				
				int updateFlag=updateTrasactionBankDate(bankStatementVO.getBookingDate().toString(), String.valueOf(txID), String.valueOf(bankStatementVO.getOrgID()));*/
			
			int updateFlag=accTransactionDAO.reconcileTransactionsWithBankStatement(bankStatementVO);
				
			
			
			
		} catch (Exception e) {
		log.error("Exception at public int reoncileTransaction(BankStatement bankStatementVO) "+e);
		}
		
		log.debug("Exit : public int reoncileTransaction(BankStatement bankStatementVO)"+txID);
		return txID;		
	}
	
	public  TransactionVO getNearestDate(List<TransactionVO> txList, String currentStrDate) {
		TransactionVO nearestTx=new TransactionVO();
		try {
			Date currentDate=df.parse(currentStrDate);
			
		  long minDiff = -1, currentTime = currentDate.getTime();
		  Date minDate = null;
		  for (TransactionVO txVO : txList) {
			  Date txDate;
		
				txDate = df.parse(txVO.getTransactionDate());
		
		    long diff = Math.abs(currentTime - txDate.getTime());
		    if ((minDiff == -1) || (diff < minDiff)) {
		      minDiff = diff;
		      nearestTx=txVO;
		    }
			
		  }
		  } catch (ParseException e) {
				
				log.error("ParseException at getNearestDate "+e);
			}
		  return nearestTx;
		}
	
	public TxCounterVO getCountersForOrg(int orgID){
		TxCounterVO txCounterVO=new TxCounterVO();
		try
		{
			log.debug("Entry : public int getCountersForOrg(int orgID)");
			
			txCounterVO=accTransactionDAO.getCountersForOrg(orgID);
			
		}catch(Exception ex)
		{
	
			log.error("Exception in public int getCountersForOrg(int orgID) : "+ex);
			
		}
			log.debug("Exit : public int getCountersForOrg(int orgID)");
		return txCounterVO;
		}
	
	public String getCounterType(String transactionType) {		
		String counterType="";
		log.debug("Entry :public String getCounterType(String transactionType)");
		try{
			
		switch (transactionType) {
		   
		   case "Contra":
	          counterType="contra";	   	  
	          break; 
	          
		   case "Credit Note":
		          counterType="credit_note";	   	  
		          break;   
		          
		   case "Debit Note":
		          counterType="debit_note";	   	  
		          break;     
		          
		   case "Job Work In":
		          counterType="jobwork_in";	   	  
		          break;   
		          
		   case "Job Work Out":
		          counterType="jobwork_ount";	   	  
		          break;        
		          
		   case "Journal":
		          counterType="journal";	   	  
		          break;
		          
		   case "Material In":
		          counterType="material_in";	   	  
		          break;      
		   
		   case "Material Out":
		          counterType="material_out";	   	  
		          break;      
		   
		   case "Memorandum":
		          counterType="memorandum";	   	  
		          break;    
		          
		   case "Payment":
		          counterType="payment";	   	  
		          break;      
		   
		   case "Physical Stock":
		          counterType="physical_stock";	   	  
		          break;  
		          
		   case "Purchase":
		          counterType="purchase";	   	  
		          break;     
		          
		   case "Purchase Order":
		          counterType="purchase_order";	   	  
		          break;   
		          
		   case "Receipt":
		          counterType="receipt";	   	  
		          break;       
		          
		   case "Receipt Note":
		          counterType="receipt_note";	   	  
		          break;     
		          
		   case "Rejection In":
		          counterType="rejection_in";	   	  
		          break;     
		          
		   case "Rejection Out":
		          counterType="rejection_out";	   	  
		          break;   
		          
		   case "Reversing Journal":
		          counterType="reversing_journal";	   	  
		          break;     
		          
		   case "Sales":
		          counterType="sales";	   	  
		          break;     
		          
		   case "Sales Order":
		          counterType="sales_order";	   	  
		          break;      
		   
		   case "Proforma":
		          counterType="sales_order";	   	  
		          break;        
		          
		   case "Stock Journal":
		          counterType="stock_journal";	   	  
		          break;         
	       
	     }       
		
		
		}catch(Exception ex){	
			log.error("Exception in getCounterType : "+ex);			
		}
		log.debug("Exit :public String getCounterType(String transactionType)");
		return counterType;
	}	
	
	public int incrementCounterNumber(int orgID,String counterType){
		int success=0;
		log.debug("Entry : public int incrementCounterNumber(int orgID,String counterType)");
		try{
			success=accTransactionDAO.incrementCounterNumber(orgID,counterType);
			
		}catch(Exception ex){	
			log.error("Exception in public int incrementCounterNumber(int orgID,String counterType) : "+ex);			
		}
			log.debug("Exit : public int incrementCounterNumber(int orgID,String counterType)");
		return success;
	}
	
	public int getLatestCounterNo(int orgID,String counterType){
		int success=0;
		log.debug("Entry : public int getLatestCounterNo(int orgID,String counterType)");
		try{
			success=accTransactionDAO.getLatestCounterNo(orgID,counterType);
			
		}catch(Exception ex){	
			log.error("Exception in public int incrementCounterNumber(int orgID,String counterType) : "+ex);			
		}
			log.debug("Exit : public int getLatestCounterNo(int orgID,String counterType)");
		return success;
	}
	
	
	public List getChargeTypeList(int orgID){
		List chargeTypeList=null;
		try
		{
			log.debug("Entry : public List getChargeTypeList(int orgID)");
			
			chargeTypeList=accTransactionDAO.getChargeTypeList(orgID);
			
		}catch(Exception ex)
		{
	
			log.error("Exception in public List getChargeTypeList(int orgID) : "+ex);
			
		}
			log.debug("Exit : public List getChargeTypeList(int orgID)");
		return chargeTypeList;
		}
	
	public List getApplicableCharges(int orgID,int chargeTypeID){
		List chargeTypeList=null;
	
			log.debug("Entry :  public List getApplicableCharges(int orgID,int chargeTypeID)");
			
			chargeTypeList=accTransactionDAO.getApplicableCharges(orgID,chargeTypeID);
			
			log.debug("Exit :  public List getApplicableCharges(int orgID,int chargeTypeID)");
		return chargeTypeList;
		}
	
	public List getApplicablePenaltyCharges(int orgID){
		List chargeTypeList=null;
	
			log.debug("Entry :  public List getApplicableCharges(int orgID)");
			
			chargeTypeList=accTransactionDAO.getApplicablePenaltyCharges(orgID);
			
			log.debug("Exit :  public List getApplicableCharges(int orgID)");
		return chargeTypeList;
		}
	
	public List getInvoiceRceiptList(int orgID,int invoiceID){
		List invoiceReceiptList=null;
	
			log.debug("Entry :  public List getInvoiceRceiptList(int orgID,int invoiceID)");
			
			invoiceReceiptList=accTransactionDAO.getInvoiceRceiptList(orgID,invoiceID);
			
			log.debug("Exit :  public List getInvoiceRceiptList(int orgID,int invoiceID)");
		return invoiceReceiptList;
		}


	public List getUnlinkedRceiptList(int orgID,int ledgerID,String fromDate, String toDate){
		List invoiceReceiptList=null;
	
			log.debug("Entry :  public List getUnlinkedRceiptList(int orgID,int ledgerID)");
			
			invoiceReceiptList=accTransactionDAO.getUnlinkedRceiptList(orgID, ledgerID, fromDate, toDate);
			if(invoiceReceiptList.size()>0){
			for(int i=0;invoiceReceiptList.size()>i;i++){
				TransactionVO txVO=(TransactionVO) invoiceReceiptList.get(i);
				if((txVO.getBalance().compareTo(BigDecimal.ZERO)<=0)){
					invoiceReceiptList.remove(i);
					--i;
				}
			}
			}
			
			
			log.debug("Exit :  public List getUnlinkedRceiptList(int orgID,int ledgerID)");
		return invoiceReceiptList;
		}
	
	public int linkRceiptList(int orgID,List receiptList){
		int successCount=0;
	
			log.debug("Entry :  public int LinkRceiptList(int orgID,List receiptList)");
			
			
					if(receiptList.size()>0){
						for(int i=0;receiptList.size()>i;i++){
							TransactionVO txVO=(TransactionVO) receiptList.get(i);
							successCount=successCount+accTransactionDAO.insertReceiptAgainstInvoice(orgID, txVO);
							
						}
						
					}
					
				
			log.debug("Exit : public int LinkRceiptList(int orgID,List receiptList)"+successCount);
		return successCount;
		}
	
	/* Unlink  receipt from invoice */
	public int unlinkReceiptFromInvoice(int orgID ,TransactionVO txVO){
		int success=0;
		log.debug("Entry : public int unlinkReceiptFromInvoice(int orgID ,TransactionVO txVO)");
		  
			   success=accTransactionDAO.unlinkReceiptFromInvoice(orgID, txVO);			
					
		log.debug("Exit : public int unlinkReceiptFromInvoice(int orgID ,TransactionVO txVO)");
		return success;		
	}
	
	public List getPaymentReceiptListReport(int orgID,int ledgerID,String fromDate, String toDate,String type){
		List invoiceReceiptList=null;
	
			log.debug("Entry :  public List getPaymentReceiptListReport(int orgID,int ledgerID)");
			
			invoiceReceiptList=accTransactionDAO.getPaymentReceiptListReport(orgID, ledgerID, fromDate, toDate,type);
					
			
			log.debug("Exit :  public List getPaymentReceiptListReport(int orgID,int ledgerID)");
		return invoiceReceiptList;
		}

	/* Transfer transactions from one organization to other org    */
	public int transferOrganizationsTransaction(int orgID,int newOrgID){
		int insertFlag=0;
		log.debug("Entry : public int transferOrganizationsTransaction(int orgID,int newOrgID)");
		try{
			
			List oldTxList=accTransactionDAO.getAllOldTransactionsList(orgID);
			
			for(int i=0;oldTxList.size()>i;i++){
				TransactionVO txVO=(TransactionVO) oldTxList.get(i);
				
				List ledgersList=getLedgersEntryList(txVO, ""+orgID);
				
				for(int j=0;ledgersList.size()>j;j++){
					LedgerEntries ledgers=(LedgerEntries) ledgersList.get(j);
					
					AccountHeadVO accVO=accTransactionDAO.getLedgerDetails(orgID, ledgers.getLedgerName());
					
					ledgers.setLedgerID(accVO.getLedgerID());
					ledgers.setOrgID(accVO.getOrgID());
					
				}
				txVO.setSocietyID(orgID);
				txVO.setLedgerEntries(ledgersList);
				txVO.setInvoiceID(txVO.getInvoiceID());
				txVO.setTransactionNumber(txVO.getTransactionNumber());
				
				TransactionVO newTxVO=insertTransaction(txVO, orgID);
				
				
			}
			
			
			
		} catch (Exception e) {
		log.error("Exception at public int transferOrganizationsTransaction(int orgID,int newOrgID) "+e);
		}
		
		log.debug("Exit : public int transferOrganizationsTransaction(int orgID,int newOrgID)"+insertFlag);
		return insertFlag;		
	}
		
	public List getTransactionLinkedInvoiceList(int orgID,int transactionID){
		List invoiceReceiptList=null;
	
		log.debug("Entry :  public List getTransactionLinkedInvoiceList(int orgID,int transactionID)");
			
			invoiceReceiptList=accTransactionDAO.getTransactionLinkedInvoiceList(orgID, transactionID);
									
		log.debug("Exit :  public List getTransactionLinkedInvoiceList(int orgID,int transactionID)");
		return invoiceReceiptList;
	}
	
	/*------------------Reallocate interest amount for ledger------------------*/
	public int reallocateInterestAmounts(TransactionVO txVO){
		int success=0;
		log.debug("Entry : public int reallocateInterestAmounts(TransactionVO txVO)");
		

		try {
			AccountHeadVO accntHeadVO=ledgerService.getOpeningClosingBalance(txVO.getSocietyID(), txVO.getLedgerID(), txVO.getFromDate(), txVO.getToDate(),"L" );
			List transactionList=getQuickTransctionList(txVO.getSocietyID(), txVO.getLedgerID(), txVO.getFromDate(), txVO.getToDate(), "all");
			AccountHeadVO interstLedgerVO=ledgerService.getLedgerByType(txVO.getSocietyID(), "Interest");
			BigDecimal balance=BigDecimal.ZERO;
			int ledgerID=txVO.getLedgerID();
			BigDecimal principleBalance=BigDecimal.ZERO;
			BigDecimal intBalance=BigDecimal.ZERO;
			if(transactionList.size()>0){
			TransactionVO txBaseVO=(TransactionVO) transactionList.get(0);
			balance=accntHeadVO.getOpeningBalance().add(txBaseVO.getAmount());
			
			intBalance=accntHeadVO.getIntOpnBalance();
			//principleBalance=balance.subtract(intBalance);
			txBaseVO.setIntBalance(intBalance);
			for(int i=0;transactionList.size()>i;i++){
				
				TransactionVO txvo=(TransactionVO) transactionList.get(i);
				BigDecimal intAmount=txvo.getIntAmount();
				
				if(i==0){
					if(txvo.getIntAmount().compareTo(BigDecimal.ZERO)==0){
						principleBalance=principleBalance.subtract(txvo.getIntAmount());
					}else
					principleBalance=balance.subtract(txvo.getIntAmount());
					
					txvo.setBalance(balance);
					txvo.setPrincipleAmount(principleBalance);
					txvo.setIntBalance(accntHeadVO.getIntOpnBalance());
				}else{
				
				balance=balance.add(txvo.getAmount());
				txvo.setBalance(balance);
				principleBalance=principleBalance.add(txvo.getAmount());
				principleBalance=principleBalance.subtract(txvo.getIntAmount());
				//
				txvo.setPrincipleAmount(principleBalance);
				txvo.setIntBalance(intBalance);
				
				}
				
				
				if(txvo.getLedgerID()!=interstLedgerVO.getLedgerID()){
					
					if(txvo.getCreditDebitFlag().equalsIgnoreCase("C")){
						if(txvo.getAmount().abs().compareTo(intBalance)<0){
							if(txvo.getAmount().compareTo(BigDecimal.ZERO)<0){
								txvo.setIntAmount(txvo.getAmount());
							}else
							
							
							txvo.setIntAmount(intBalance.negate());
						}else{
							txvo.setIntAmount(intBalance.negate());
						}
					}
					txvo.setLedgerID(ledgerID);
					success=accTransactionDAO.reallocateIntestAmount( txvo);
					
					
				}intBalance=intBalance.add(txvo.getIntAmount());
				
				
				
			}
			}	
		}catch (NullPointerException e) {
			log.info("Exception at public int reallocateInterestAmounts(TransactionVO txVO) for "+txVO.getSocietyID()+" ledgerID "+txVO.getLedgerID()+" from date is "+txVO.getFromDate()+" and uptoDate  "+txVO.getToDate()+" "+e);
		
		} catch (Exception e) {
			log.error("Exception at public int reallocateInterestAmounts(TransactionVO txVO) "+e);
		}
		log.debug("Exit : public int reallocateInterestAmounts(TransactionVO txVO)");
		return success;
	}
	
	 /*------------------Reallocate interest amount for Organization------------------*/
		public int reallocateInterestAmountsForOrg(TransactionVO txVO){
			int success=0;
			log.debug("Entry : public int reallocateInterestAmountsForOrg(TransactionVO txVO)");
			
			List memberList=reportService.getReceivableListInBillFormat(txVO.getSocietyID(), txVO.getFromDate(), txVO.getToDate());
			
			if(memberList.size()>0){
				for(int i=0;memberList.size()>i;i++){
					
					BillDetailsVO billVO=(BillDetailsVO) memberList.get(i);
					TransactionVO txnVO=new TransactionVO();
					txnVO.setSocietyID(txVO.getSocietyID());
					txnVO.setLedgerID(billVO.getLedgerID());
					txnVO.setFromDate(txVO.getFromDate());
					txnVO.setToDate(txVO.getToDate());
					
					success=success+reallocateInterestAmounts(txnVO);
					
				}
				
				
			}
			
			
			log.debug("Exit : public int reallocateInterestAmountsForOrg(TransactionVO txVO)");
			return success;
		}
		
		
		
		//============ Member App related APIs =============//
		
		
		public List getAllNonRecordedTransactionsReportForMemberAppLedger(TransactionVO txVO){
			List TransactionList=null;
			
			try {
				log.debug("Entry : public List getAllNonRecordedTransactionsReportForMemberAppLedger(TransactionVO txVO)");
				
				TransactionList=accTransactionDAO.getAllNonRecordedTransactionsReportForMemberAppLedger( txVO);
				
				
				log.debug("Exit : public List getAllNonRecordedTransactionsReportForMemberAppLedger(TransactionVO txVO)");
			} catch (Exception e) {
				log.error("Exception : public List getAllNonRecordedTransactionsReportForMemberAppLedger(TransactionVO txVO) "+e );
			}
			return TransactionList;
			
		}
		
		
	
	public void setAccTransactionDAO(TransactionDAO accTransactionDAO) {
		this.accTransactionDAO = accTransactionDAO;
	}


	public ReceiptInvoiceDomain getReceiptInvoiceDomain() {
		return receiptInvoiceDomain;
	}


	public void setReceiptInvoiceDomain(ReceiptInvoiceDomain receiptInvoiceDomain) {
		this.receiptInvoiceDomain = receiptInvoiceDomain;
	}


	public SocietyService getSocietyService() {
		return societyService;
	}


	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}


	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}


	public void setLedgerService(LedgerService ledgerService) {
		this.ledgerService = ledgerService;
	}




	/**
	 * @param memberServiceBean the memberServiceBean to set
	 */
	public void setMemberServiceBean(MemberService memberServiceBean) {
		this.memberServiceBean = memberServiceBean;
	}




	/**
	 * @param invoiceService the invoiceService to set
	 */
	public void setInvoiceService(InvoiceService invoiceService) {
		this.invoiceService = invoiceService;
	}




	/**
	 * @param calculateLateFeesForTransaction the calculateLateFeesForTransaction to set
	 */
	public void setCalculateLateFeesForTransaction(CalculateLateFeeForTransactions calculateLateFeesForTransaction) {
		this.calculateLateFeesForTransaction = calculateLateFeesForTransaction;
	}

	/**
	 * @param interestManager the interestManager to set
	 */
	public void setInterestManager(InterestManager interestManager) {
		this.interestManager = interestManager;
	}


	

}
