package com.emanager.server.userManagement.dataAccessObject;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.valueObject.AddressVO;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.userManagement.valueObject.AppVO;
import com.emanager.server.userManagement.valueObject.PermissionVO;
import com.emanager.server.userManagement.valueObject.UserVO;

public class UserDAO {
	private static final Logger logger = Logger.getLogger(UserDAO.class);
	
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	public DateUtility dateUtility=new DateUtility();
	public ConfigManager config=new ConfigManager();
	
	public UserVO getUserDetailsByLoginId(String userName,String type){
		UserVO userVO =new UserVO();
		String strSQLQuery = "";
		String strCondition="";
		if(type.equalsIgnoreCase("name")){
			strCondition=" login_id =:loginUserId ";
		}else if(type.equalsIgnoreCase("id")){
			strCondition=" id =:loginUserId ";
		}else if(type.equalsIgnoreCase("mobile")){
			strCondition=" mobile =:loginUserId ";
		}
		try{
			logger.debug("Entry : public UserVO getUserDetailsByLoginId(String userName,String type)"+userName+type );
//			1. SQL Query
			strSQLQuery = " SELECT * FROM um_users WHERE "+strCondition+" and is_deleted=0 ";				
			logger.debug("query : " + strSQLQuery);		
			
			 Map hMap=new HashMap();
			 hMap.put("loginUserId", userName);
			 			 
//			3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			        	UserVO userVO = new UserVO();			        	
			        	userVO.setUserID(rs.getString("id"));
			        	userVO.setLoginName(rs.getString("login_id").trim());
			        	userVO.setFullName(rs.getString("full_name"));
			        	userVO.setMobile(rs.getString("mobile"));
			        	userVO.setPassword(rs.getString("password").trim());
			        	userVO.setLastLogin(rs.getString("last_login"));
			        	userVO.setIsLocked(rs.getInt("is_locked"));
			        	userVO.setLoginCounter(rs.getInt("login_counter"));
			        	userVO.setPasswordValidity(rs.getTimestamp("password_validity"));			        	
			            userVO.setIsDeleted(rs.getInt("is_deleted"));
			            userVO.setIsSystemUser(rs.getInt("is_system_user"));
			            userVO.setPasswordType(rs.getString("password_type"));
			         	return userVO;
			        }};
			
			
			        userVO = (UserVO) namedParameterJdbcTemplate.queryForObject(strSQLQuery,hMap,RMapper);		
			
			logger.debug("Exit : public UserVO getUserDetailsByLoginId(String userName,String type)");
			userVO.setLoginSuccess(true);
		}catch(EmptyResultDataAccessException ex){
			userVO.setLoginSuccess(false);
			userVO.setStatusCode(521);
		    logger.info("No user details found for emailID:  "+userName);            
		    	
		}catch(RecoverableDataAccessException ex){
			userVO.setLoginSuccess(false);
			userVO.setStatusCode(500);
	        logger.info("RecoverableDataAccessException in getUserDetailsByLoginId ");            
	    }catch(Exception ex){
			userVO.setLoginSuccess(false);
			userVO.setStatusCode(500);
	        logger.error("Exception in getUserDetailsByLoginId : "+ex);            
	    }
	   logger.debug("Exit : public UserVO getUserDetailsByLoginId(String userName, String password) ");
		return userVO;	
	}
	
	
	public UserVO getUserDetailsByCustomName(String customName, String appID){
		UserVO userVO =new UserVO();
		String strSQLQuery = "";
		String strCondition="";
		
		try{
			logger.debug("Entry : public UserVO getUserDetailsByCustomID(String customName, String appID)"+customName+appID );
//			1. SQL Query
			strSQLQuery = " SELECT * FROM um_user_app_role WHERE LOWER(custom_username)=:customName AND app_id=:appID ";				
			logger.debug("query : " + strSQLQuery);		
			
			 Map hMap=new HashMap();
			 hMap.put("customName", customName.toLowerCase());
			 hMap.put("appID", appID);
			 			 
//			3. RowMapper.
			 RowMapper RMapper = new RowMapper() {
			        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			        	UserVO userVO = new UserVO();			        	
			        	userVO.setUserID(rs.getString("user_id"));
			        	userVO.setCustomUserName(rs.getString("custom_username"));
			        	userVO.setAppID(rs.getString("app_id"));
			        	userVO.setOrgID(rs.getString("org_id"));
			        	userVO.setRoleID(rs.getString("role_id"));
			         	return userVO;
			        }};
			
			
			        userVO = (UserVO) namedParameterJdbcTemplate.queryForObject(strSQLQuery,hMap,RMapper);		
			
			logger.debug("Exit : public UserVO getUserDetailsByCustomID(String customName, String appID)");
			userVO.setLoginSuccess(true);
		}catch(EmptyResultDataAccessException ex){
			userVO.setLoginSuccess(false);
			userVO.setStatusCode(521);
		    logger.info("No user details found for customName:  "+customName+" appID: "+appID);            
		    	
		}catch(RecoverableDataAccessException ex){
			userVO.setLoginSuccess(false);
			userVO.setStatusCode(500);
	        logger.info("RecoverableDataAccessException in getUserDetailsByCustomName ");            
	    }catch(IncorrectResultSizeDataAccessException erx){
			userVO.setLoginSuccess(false);
			userVO.setStatusCode(521);
		    logger.info("Duplicate details found for customName:  "+customName+" appID: "+appID);            
		    	
		}catch(Exception ex){
			userVO.setLoginSuccess(false);
			userVO.setStatusCode(500);
	        logger.error("Exception in getUserDetailsByCustomName : "+ex);            
	    }
	   logger.debug("Exit : public UserVO getUserDetailsByCustomName(String customName, String appID) ");
		return userVO;	
	}
	
	 public List getPermissionList(String userID,String roleID){
		 List permissionList=null;
			String sql=null;
			try{
				logger.debug("Entry : public List getPermissionList(int userID)"+userID+roleID );
							
				
				sql=" SELECT p.id,p.access_name,p.app_id,p.server_access_method,p.is_system_permission,p.description FROM um_user_app_role upr,um_permission p,um_role_permission rp "
                     +" WHERE p.id=rp.permission_id AND upr.app_id=p.app_id AND upr.role_id=rp.role_id AND upr.user_id=:userID AND upr.role_id=:roleID;"; 
				logger.debug("Query : " + sql);
				
				Map namedParam = new HashMap();
				namedParam.put("roleID",roleID);
				namedParam.put("userID",userID);
				
				RowMapper RMapper=new RowMapper()
				{
					public Object mapRow(ResultSet rs, int n) throws SQLException {
						// TODO Auto-generated method stub
						PermissionVO permissionVO= new PermissionVO();						
						permissionVO.setAccessID(rs.getInt("id"));
						permissionVO.setAccessName(rs.getString("access_name"));	
						permissionVO.setServerAccessMethod(rs.getString("server_access_method"));
						permissionVO.setIsSystemPermission(rs.getInt("is_system_permission"));
						permissionVO.setDescription(rs.getString("description"));
						return permissionVO;
					}
					
				};
				
				permissionList=namedParameterJdbcTemplate.query(sql, namedParam, RMapper);
			}catch(EmptyResultDataAccessException Ex){
				logger.info("No Data found getPermissionList "+Ex);									
				
			}catch(Exception ex){
				logger.error("Exception in getPermissionList : "+ex);			
			}
			logger.debug("Exit :public List getPermissionList(String userID,String roleID)");
			return permissionList;
			
		}
	 
	 public List getAllPermissionList(String appID){
		 List permissionList=null;
			String sql="";
			String strCondition="";
			try{
				logger.debug("Entry :public List getAllPermissionList(String appID)"+appID);
						
				sql=" SELECT p.* FROM um_permission p where app_id=:appID and is_system_permission=0 " ;
                     
				logger.debug("Query : " + sql);
				
				Map namedParam = new HashMap();
				namedParam.put("appID",appID);
												
				RowMapper RMapper=new RowMapper()
				{
					public Object mapRow(ResultSet rs, int n) throws SQLException {
						// TODO Auto-generated method stub
						PermissionVO permissionVO= new PermissionVO();						
						permissionVO.setAccessID(rs.getInt("id"));
						permissionVO.setAccessName(rs.getString("access_name"));	
						permissionVO.setServerAccessMethod(rs.getString("server_access_method"));
						permissionVO.setIsSystemPermission(rs.getInt("is_system_permission"));
						permissionVO.setDescription(rs.getString("description"));
						return permissionVO;
					}
					
				};
				
				permissionList=namedParameterJdbcTemplate.query(sql, namedParam, RMapper);
			}catch(EmptyResultDataAccessException Ex){
				logger.info("No Data found getAllPermissionList "+Ex);									
				
			}catch(Exception ex){
				logger.error("Exception in getAllPermissionList : "+ex);			
			}
			logger.debug("Exit :public List getAllPermissionList(String appID,String roleID)");
			return permissionList;
			
		}
	 
	 public List getRolePermissionList(String roleID, String appID, String orgID){
		 List permissionList=new ArrayList();
			String sql=null;
			try{
				logger.debug("Entry : public List getRolePermissionList(String roleID, String appID, String orgID)"+roleID+appID+orgID );
						
				sql=" SELECT p.id,p.access_name,p.app_id,p.server_access_method,p.is_system_permission,p.description FROM um_role r, um_permission p,um_role_permission rp "
                     +" WHERE p.id=rp.permission_id AND r.app_id=p.app_id AND r.id=rp.role_id AND r.is_system_role='0' AND r.id=:roleID AND r.app_id=:appID AND r.org_id=:orgID;"; 
				logger.debug("Query : " + sql);
				
				Map namedParam = new HashMap();
				namedParam.put("roleID",roleID);
				namedParam.put("appID",appID);
				namedParam.put("orgID",orgID);
			
				
				RowMapper RMapper=new RowMapper()
				{
					public Object mapRow(ResultSet rs, int n) throws SQLException {
						// TODO Auto-generated method stub
						PermissionVO permissionVO= new PermissionVO();						
						permissionVO.setAccessID(rs.getInt("id"));
						permissionVO.setAccessName(rs.getString("access_name"));	
						permissionVO.setServerAccessMethod(rs.getString("server_access_method"));
						permissionVO.setIsSystemPermission(rs.getInt("is_system_permission"));
						permissionVO.setDescription(rs.getString("description"));
						return permissionVO;
					}
					
				};
				
				permissionList=namedParameterJdbcTemplate.query(sql, namedParam, RMapper);
			}catch(EmptyResultDataAccessException Ex){
				logger.info("No Data found getRolePermissionList "+Ex);									
				
			}catch(Exception ex){
				logger.error("Exception in getRolePermissionList : "+ex);			
			}
			logger.debug("Exit :public List getRolePermissionList(String roleID, String appID, String orgID)"+permissionList.size());
			return permissionList;
			
		}
	 
	 
	 public List getAppUserOrgnizationList(String userID, String appID){
		 List societyList=null;
			String sql="";
			try{
				logger.debug("Entry : public UserVO getAppPermissionList(String userID,  String appID)");
				String condition="";
				if(appID.equalsIgnoreCase("0")){
					condition=" ";
				}else{
					condition=" AND upr.app_id=:appID ";
				}
				
				sql=" SELECT upr.*,am.name AS appName,r.name AS roleName,sd.*,a.*  FROM um_users u, um_user_app_role upr, um_app_master am, um_role r, society_details sd, society_address a "
                  + " WHERE u.id=upr.user_id AND upr.app_id=am.id  AND r.id=upr.role_id "
                  + " AND sd.society_id=upr.org_id AND sd.working='1' AND sd.society_id=a.society_id  AND upr.user_id =:userID "+condition+" ORDER BY default_org_id DESC; ";
				logger.debug("Query : " + sql);
				
				Map namedParam = new HashMap();
				namedParam.put("userID",userID);
				namedParam.put("appID",appID);
				
				RowMapper RMapper=new RowMapper()
				{

					public Object mapRow(ResultSet rs, int n) throws SQLException {
						// TODO Auto-generated method stub
						 SocietyVO societyVO=new SocietyVO();
				         AddressVO addressVO=new AddressVO();
				            societyVO.setAppID(rs.getString("upr.app_id"));
				            societyVO.setAppName(rs.getString("appName"));
				            societyVO.setRoleID(rs.getString("upr.role_id"));
				            societyVO.setRoleName(rs.getString("roleName"));
				            societyVO.setDefaultOrgID(rs.getString("upr.default_org_id"));
				            societyVO.setSocietyID(rs.getInt("society_id"));
				            societyVO.setSocFullName(rs.getString("society_full_name"));
				            societyVO.setOrgType(rs.getString("org_type"));
				            societyVO.setSocShortName(rs.getString("short_name"));
				            societyVO.setSocietyRegNo(rs.getString("register_no"));
				            societyVO.setMailListM(rs.getString("mailingList_member"));
				            societyVO.setMailListA(rs.getString("mailingList_committee"));
				            societyVO.setSocietyName(rs.getString("society_name"));
				            addressVO.setAddLine1(rs.getString("line_1"));
				            addressVO.setAddLine2(rs.getString("line_2"));
				            addressVO.setCity(rs.getString("city"));
				            addressVO.setZip(rs.getString("postal_code"));
				            addressVO.setState(rs.getString("state"));
				            societyVO.setAddress(rs.getString("line_1")+", "+rs.getString("line_2")+", "+rs.getString("city")+", "+rs.getString("postal_code"));
				            societyVO.setSocietyAddress(addressVO);
						
						return societyVO;
					}
					
				};
				
				societyList=  namedParameterJdbcTemplate.query(sql, namedParam, RMapper);
			}catch(EmptyResultDataAccessException Ex){
				logger.info("No Data found getAppUserOrgnizationList "+Ex);		
									
			}catch(Exception ex){
				logger.error("Exception in getAppUserOrgnizationList : "+ex);			
			}
			logger.debug("Exit :public SocietyVO getAppUserOrgnizationList(String userID,  String appID)");
			return societyList;
			
		}
	 

		public Boolean setResetLoginCounter(String userID, int attemptCount)  {
			// TODO Auto-generated method stub
			Boolean flag=false;
			int success=0;
						
			logger.debug("Entry :public Boolean setResetLoginCounter(String userID, int attemptCount)");
			try{
				  
				   java.util.Date today = new java.util.Date();
				   java.sql.Date currentDate=new java.sql.Date(today.getTime());
				   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
				   				   
				   String str = "Update um_users set login_counter=:counter,update_date=:update where id=:userID";
				   logger.debug("Sql : "+str);		   
				   
				   Map hMap=new HashMap();				
				   hMap.put("userID", userID);
				   hMap.put("counter", attemptCount);
				   hMap.put("update", currentTimestamp);
				   
				   success=namedParameterJdbcTemplate.update(str, hMap);
				   if(success!=0){
					   flag=true; 
				   }
				   logger.debug("Exit :public Boolean setResetLoginCounter(String userID, int attemptCount)"+success+flag);
				}catch(Exception se){
					logger.debug("Exception : public Boolean setResetLoginCounter(String userID, int attemptCount) "+se);
					flag=false;
				}
			return flag;
		}
		
		public Boolean setRefreshToken(String userID, String token)  {
			// TODO Auto-generated method stub
			Boolean flag=false;
			int success=0;
						
			logger.debug("Entry :public Boolean setRefreshToken(String userID, int token)");
			try{
				  
				   java.util.Date today = new java.util.Date();
				   java.sql.Date currentDate=new java.sql.Date(today.getTime());
				   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
				   				   
				   String str = "Update um_users set refresh_token=:token,last_login=:lastLogin,update_date=:update where id=:userID";
				   logger.debug("Sql : "+str);	
				   
				   Map hMap=new HashMap();				
				   hMap.put("userID", userID);
				   hMap.put("token", token);
				   hMap.put("lastLogin", currentTimestamp);
				   hMap.put("update", currentTimestamp);
				   
				   success=namedParameterJdbcTemplate.update(str, hMap);
				   if(success!=0){
					   flag=true; 
				   }
				   logger.debug("Exit :public Boolean setRefreshToken(String userID, int token)"+success+flag);
				}catch(Exception se){
					logger.debug("Exception : public Boolean setRefreshToken(String userID, int token) "+se);
					flag=false;
				}
			return flag;
		}
		
		public Boolean setAccessToken(String userID, String orgID, String appID, String token)  {
			// TODO Auto-generated method stub
			Boolean flag=false;
			int success=0;
						
			logger.debug("Entry :public Boolean setAccessToken(String userID, int token)"+userID+orgID+appID+token);
			try{
				  				   				   
				   String str = "Update um_user_app_role set access_token=:token where user_id=:userID and org_id=:orgID and app_id=:appID ";
				   logger.debug("Sql : "+str);		   
				   Map hMap=new HashMap();
				
				   hMap.put("userID", userID);
				   hMap.put("orgID", orgID);
				   hMap.put("appID", appID);
				   hMap.put("token", token);
				  
				   
				   success=namedParameterJdbcTemplate.update(str, hMap);
				   if(success!=0){
					   flag=true; 
				   }
				   
				   logger.debug("Exit :public Boolean setAccessToken(String userID, int token)"+success+flag);
				}catch(Exception se){
					logger.debug("Exception : public Boolean setAccessToken(String userID, int token) "+se);
					flag=false;
				}
			return flag;
		}
     
		public Boolean matchAccessToken(String orgID, String expiredToken){
			String strSQLQuery = "";
			Boolean isMatch=false;
			int count=0;
			try{
				logger.debug("Entry : public Boolean matchAccessToken(String orgID, String token)"+orgID+expiredToken );
                 //	1. SQL Query
				strSQLQuery = " SELECT user_id FROM um_user_app_role upr WHERE upr.org_id=:orgID and access_token=:token " ;				
				logger.debug("query : " + strSQLQuery);		
				
				 Map hMap=new HashMap();
				 hMap.put("orgID", orgID);
				 hMap.put("token", expiredToken);
				 			 
				 count=namedParameterJdbcTemplate.queryForObject(strSQLQuery, hMap, Integer.class);
				 if(count>0){
					 isMatch=true; 
				 }
				logger.debug("Exit :public Boolean matchAccessToken(String orgID, String token)"+isMatch+count);
				
			}catch(EmptyResultDataAccessException e){
				logger.info("Not a valid access token user");
				isMatch=false;
		
			}catch(Exception ex){
		         logger.error("Exception in matchAccessToken : "+ex);       
		         isMatch=false;
		    }
		   logger.debug("Exit : public Boolean matchAccessToken(String orgID, String token)");
			return isMatch;	
		}
		
		//Two step auth update
				public Boolean setTwoStepOTP(String userID, String token,java.sql.Timestamp validity)  {
					// TODO Auto-generated method stub
					Boolean flag=false;
					int success=0;
								
					logger.debug("Entry :public Boolean setTwoStepOTP(String userID, int otp)");
					try{
						  
						   java.util.Date today = new java.util.Date();
						   java.sql.Date currentDate=new java.sql.Date(today.getTime());
						   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
						   				   
						   String str = "Update um_users set otp=:token,otp_validity=:validity,update_date=:update where id=:userID";
						   logger.debug("Sql : "+str);	
						   
						   Map hMap=new HashMap();				
						   hMap.put("userID", userID);
						   hMap.put("token", token);
						   hMap.put("validity", validity);
						   hMap.put("update", currentTimestamp);
						   
						   success=namedParameterJdbcTemplate.update(str, hMap);
						   if(success!=0){
							   flag=true; 
						   }
						   logger.debug("Exit :public Boolean setTwoStepOTP(String userID, int token)"+success+flag);
						}catch(Exception se){
							logger.debug("Exception : public Boolean setTwoStepOTP(String userID, int token) "+se);
							flag=false;
						}
					return flag;
				}
				
				public Boolean matchOTPToken(String userID,String otpToken){
					String strSQLQuery = "";
					Boolean isMatch=false;
					UserVO matchVO=new UserVO();
					int success=0;
					try{
						logger.debug("Entry : public Boolean matchAccessToken(String userID, String token)"+userID+otpToken );
						  java.util.Date today = new java.util.Date();
						   java.sql.Date currentDate=new java.sql.Date(today.getTime());
						   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
						   
						//	1. SQL Query
						strSQLQuery = " SELECT *  FROM um_users  WHERE id=:userID and otp=:otpToken " ;				
						logger.debug("query : " + strSQLQuery);		
						
						 Map hMap=new HashMap();
						 hMap.put("userID", userID);
						 hMap.put("otpToken", otpToken);
						 
						
							 RowMapper RMapper = new RowMapper() {
							        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
							        	UserVO userVO = new UserVO();			        	
							        	userVO.setUserID(rs.getString("id"));					
							        	userVO.setFullName(rs.getString("full_name"));							        	
							            userVO.setOTP(rs.getString("otp"));
							            userVO.setOtpValidity(rs.getTimestamp("otp_validity"));
							         	return userVO;
							        }};
							
						 			 
						 matchVO=(UserVO) namedParameterJdbcTemplate.queryForObject(strSQLQuery,hMap,RMapper);
						 
						
						 if(matchVO.getOtpValidity().after(currentTimestamp)){
							 isMatch=true; 
						 }
						 
						 if(isMatch){
							 //remove otp token 
							 String updateQuery = " Update um_users set otp='',otp_validity=NULL where id="+userID+" ";
							 Map hMap1=new HashMap();
							success=namedParameterJdbcTemplate.update(updateQuery, hMap1);
						 }
						logger.debug("Exit :public Boolean matchOTPToken(String userID, String token)"+isMatch);
						
					}catch(EmptyResultDataAccessException e){
						logger.info("Not a valid access otp token user");
						isMatch=false;
				
					}catch(Exception ex){
				         logger.error("Exception in matchOTPToken : "+ex);       
				         isMatch=false;
				    }
				   logger.debug("Exit : public Boolean matchOTPToken(String orgID, String token)");
					return isMatch;	
				}
						
		
		 public List getUserList(String orgID, String appID){
			 List userList=null;
				String sql=null;
				try{
					logger.debug("Entry :  public List getUserList(String orgID, String appID)"+orgID+appID );
								
					
					sql=" SELECT IF(usr.is_system_user='1','System User',usr.full_name) AS fullName, upr.role_id,r.name,upr.app_id, "
                        +" IF(usr.is_system_user='1','System User',usr.login_id) AS loginID, usr.*, " 
                        +"(SELECT full_name  FROM um_users WHERE um_users.id=usr.created_by)AS creatorName, "
                        +"(SELECT full_name  FROM um_users WHERE um_users.id=usr.updated_by)AS updaterName " 
                        +" FROM um_users usr, um_user_app_role upr, um_role r, um_app_master am, society_details s "
                        +" WHERE upr.app_id=am.id AND upr.org_id=s.society_id AND usr.id=upr.user_id AND r.id=upr.role_id "
                        + " AND upr.app_id=:appID AND upr.org_id=:orgID AND usr.is_deleted=0 ORDER BY usr.id "; 
					logger.debug("Query : " + sql);
					
					Map namedParam = new HashMap();
					namedParam.put("orgID",orgID);
					namedParam.put("appID",appID);
										
					RowMapper RMapper=new RowMapper()
					{
						public Object mapRow(ResultSet rs, int n) throws SQLException {
							// TODO Auto-generated method stub
							UserVO userVO= new UserVO();						
							userVO.setUserID(rs.getString("id"));
							userVO.setLoginName(rs.getString("loginID"));
							userVO.setFullName(rs.getString("fullName"));
							userVO.setCreateDate(rs.getString("create_date"));
							userVO.setUpdateDate(rs.getString("update_date"));
							userVO.setAppID(rs.getString("app_id"));
							userVO.setRoleID(rs.getString("role_id"));
							userVO.setRoleName(rs.getString("name"));
							userVO.setIsLocked(rs.getInt("is_locked"));
							userVO.setLoginCounter(rs.getInt("login_counter"));							
							userVO.setCreatedBy(rs.getString("created_by"));
							userVO.setUpdatedBy(rs.getString("updated_by"));
							userVO.setCreaterName(rs.getString("creatorName"));
							userVO.setUpdaterName(rs.getString("updaterName"));
							userVO.setIsSystemUser(rs.getInt("is_system_user"));
							userVO.setIsDeleted(rs.getInt("is_deleted"));
							return userVO;
						}
						
					};
					
					userList=namedParameterJdbcTemplate.query(sql, namedParam, RMapper);
				}catch(EmptyResultDataAccessException Ex){
					logger.info("No Data found getUserList "+Ex);									
					
				}catch(Exception ex){
					logger.error("Exception in getUserList : "+ex);			
				}
				logger.debug("Exit :public List getUserList(String orgID, String appID)"+userList.size());
				return userList;
				
			}
		 
		 public List getAppwiseUserList(String orgID, String appID){
			 List userList=null;
				String sql=null;
				try{
					logger.debug("Entry :  public List getAppwiseUserList(String orgID, String appID)"+orgID+appID );
								
					
					sql=" SELECT usr.*, upr.role_id,r.name,upr.app_id, " 
                        +"(SELECT full_name  FROM um_users WHERE um_users.id=usr.created_by)AS creatorName, "
                        +"(SELECT full_name  FROM um_users WHERE um_users.id=usr.updated_by)AS updaterName " 
                        +" FROM um_users usr, um_user_app_role upr, um_role r, um_app_master am, society_details s "
                        +" WHERE upr.app_id=am.id AND upr.org_id=s.society_id AND usr.id=upr.user_id AND r.id=upr.role_id "
                        + " AND upr.app_id=:appID AND upr.org_id=:orgID AND usr.is_deleted=0 ORDER BY usr.id "; 
					logger.debug("Query : " + sql);
					
					Map namedParam = new HashMap();
					namedParam.put("orgID",orgID);
					namedParam.put("appID",appID);
										
					RowMapper RMapper=new RowMapper()
					{
						public Object mapRow(ResultSet rs, int n) throws SQLException {
							// TODO Auto-generated method stub
							UserVO userVO= new UserVO();						
							userVO.setUserID(rs.getString("id"));
							userVO.setLoginName(rs.getString("login_id"));
							userVO.setMobile(rs.getString("mobile"));
							userVO.setFullName(rs.getString("full_name"));
							userVO.setCreateDate(rs.getString("create_date"));
							userVO.setUpdateDate(rs.getString("update_date"));
							userVO.setAppID(rs.getString("app_id"));
							userVO.setRoleID(rs.getString("role_id"));
							userVO.setRoleName(rs.getString("name"));
							userVO.setIsLocked(rs.getInt("is_locked"));
							userVO.setLastLogin(rs.getString("last_login"));
							userVO.setLoginCounter(rs.getInt("login_counter"));							
							userVO.setCreatedBy(rs.getString("created_by"));
							userVO.setUpdatedBy(rs.getString("updated_by"));
							userVO.setCreaterName(rs.getString("creatorName"));
							userVO.setUpdaterName(rs.getString("updaterName"));
							userVO.setIsSystemUser(rs.getInt("is_system_user"));
							userVO.setIsDeleted(rs.getInt("is_deleted"));
							return userVO;
						}
						
					};
					
					userList=namedParameterJdbcTemplate.query(sql, namedParam, RMapper);
				}catch(EmptyResultDataAccessException Ex){
					logger.info("No Data found getAppwiseUserList "+Ex);									
					
				}catch(Exception ex){
					logger.error("Exception in getAppwiseUserList : "+ex);			
				}
				logger.debug("Exit :public List getAppwiseUserList(String orgID, String appID)"+userList.size());
				return userList;
				
			}
		 
		 public List getRoles(String orgID, String appID){
			 List roleList=null;
				String sql=null;
				try{
					logger.debug("Entry : public List getRoles(String orgID, String appID)"+orgID+appID );
							
					sql=" SELECT * FROM um_role ur WHERE ur.app_id=:appID AND (ur.org_id=:orgID || ur.org_id='0') AND id!='1'"; 
					logger.debug("Query : " + sql);
					
					Map namedParam = new HashMap();
					namedParam.put("appID",appID);
					namedParam.put("orgID",orgID);
					
					RowMapper RMapper=new RowMapper()
					{
						public Object mapRow(ResultSet rs, int n) throws SQLException {
							// TODO Auto-generated method stub
							UserVO roleVO= new UserVO();						
							roleVO.setRoleID(rs.getString("id"));
							roleVO.setRoleName(rs.getString("name"));
							roleVO.setAppID(rs.getString("app_id"));
							roleVO.setOrgID(rs.getString("org_id"));
							roleVO.setIsSystemRole(rs.getInt("is_system_role"));
							roleVO.setIsSignUpRole(rs.getInt("sign_up_role"));
							return roleVO;
						}
						
					};
					
					roleList=namedParameterJdbcTemplate.query(sql, namedParam, RMapper);
				}catch(EmptyResultDataAccessException Ex){
					logger.info("No Data found getRoles "+Ex);									
					
				}catch(Exception ex){
					logger.error("Exception in getRoles : "+ex);			
				}
				logger.debug("Exit :public List getRoles(String orgID, String appID)"+roleList.size());
				return roleList;
				
			}
		 
		 
		 public int createUser(UserVO userVO){
				int generatedUserID= 0;
			
				try{
					logger.debug("Entry : public Boolean createUser(UserVO userVO)");	
					   java.util.Date today = new java.util.Date();
					   java.sql.Date currentDate=new java.sql.Date(today.getTime());
					   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
					   java.sql.Timestamp passwordTimeStamp=dateUtility.AddingHHMMSSToDate(Integer.parseInt(config.getPropertiesValue("pwd.temporaryValidity")), 0, 0);
					   String condition="";
					   String conditionValue="";
					   if((userVO.getMobile()!=null)&&(userVO.getMobile().length()==10)&&(!userVO.getMobile().equalsIgnoreCase("0000000000"))){
						   condition=",mobile";
						   conditionValue=", :mobile";
					   }
					   userVO.setPasswordValidity(passwordTimeStamp);
					   userVO.setUpdateDate(""+currentTimestamp);
					   userVO.setPasswordType("T");
					   
					   
						String sqlQuery="INSERT INTO um_users(login_id, password, full_name, password_validity, password_type, update_date, created_by, updated_by "+condition+") "+
						 " VALUES ( :loginName, :password, :fullName, :passwordValidity, :passwordType, :updateDate, :createdBy, :updatedBy "+conditionValue+" );" ;
						logger.debug("query : " + sqlQuery);
						
					
						 
						   SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(userVO);
						   KeyHolder keyHolder = new GeneratedKeyHolder();
						   getNamedParameterJdbcTemplate().update(sqlQuery, fileParameters, keyHolder);
						   generatedUserID=keyHolder.getKey().intValue();
					 
														
					logger.debug("Exit :public Boolean createUser(UserVO userVO)"+generatedUserID);
				}catch(DuplicateKeyException duplicateEntry){
					logger.info("DuplicateKeyException for emailID : "+userVO.getLoginName());      
					//logger.error("DuplicateKeyException in createUser : "+duplicateEntry);
					generatedUserID=2;
					return generatedUserID;	
				}catch(Exception Ex){
					logger.error("Exception in createUser : "+Ex);
					generatedUserID=0;
					return generatedUserID;
				}
				return generatedUserID;
				
				
			}
		 
		 public int assignRole(UserVO userVO){
				int insertFlag= 0;
				
				try{
					logger.debug("Entry : public Boolean assignRole(UserVO userVO)"+userVO.getUserID()+userVO.getRoleID());	
					 
						String sqlQuery="INSERT INTO um_user_app_role(user_id, role_id, app_id, org_id) "+
						 " VALUES ( :userID, :roleID, :appID, :orgID );" ;
						logger.debug("query : " + sqlQuery);
						
						 Map hmap = new HashMap();
						
						 hmap.put("userID", userVO.getUserID());
						 hmap.put("roleID", userVO.getRoleID());
						 hmap.put("appID", userVO.getAppID());
						 hmap.put("orgID", userVO.getOrgID());
												 
						 insertFlag = namedParameterJdbcTemplate.update(sqlQuery, hmap);															
					
				}catch(DuplicateKeyException duplicateEntry){
					logger.info("DuplicateKeyException in assignRole : UserID "+userVO.getUserID()+" OrgID: "+userVO.getOrgID());
					insertFlag=2;
					return insertFlag;	
					
				}catch(Exception Ex){
					logger.error("Exception in assignRole : "+Ex);
					insertFlag=0;
					return insertFlag;
				}
				
				logger.debug("Exit :public Boolean assignRole(UserVO userVO)"+insertFlag);
				return insertFlag;
							
			}
		 
		 public Boolean updateUserRole(UserVO userVO){
				int flag= 0;
				Boolean isUpdated=false;
				try{
					logger.debug("Entry : public Boolean updateUserRole(UserVO userVO)"+userVO.getUserID()+userVO.getRoleID());	
					 
						String sqlQuery="Update um_user_app_role set role_id=:roleID  where user_id=:userID and app_id=:appID and org_id=:orgID ;" ;
						logger.debug("query : " + sqlQuery);
						
						 Map hmap = new HashMap();
						
						 hmap.put("userID", userVO.getUserID());
						 hmap.put("roleID", userVO.getRoleID());
						 hmap.put("appID", userVO.getAppID());
						 hmap.put("orgID", userVO.getOrgID());
						 
						 flag = namedParameterJdbcTemplate.update(sqlQuery, hmap);
					 
						 if(flag!=0){
							 isUpdated=true;
						 }
									
					logger.debug("Exit :public Boolean updateUserRole(UserVO userVO)"+isUpdated);
					
				}catch(Exception Ex){
					logger.error("Exception in updateUserRole : "+Ex);
					isUpdated=false;
				}
				return isUpdated;
				
				
			}
		 
			public Boolean updateUser(UserVO userVO)  {
				// TODO Auto-generated method stub
				Boolean isUpdated=false;
				int success=0;
							
				logger.debug("Entry :public Boolean updateUser(UserVO userVO)");
				try{
					  java.util.Date today = new java.util.Date();
					   java.sql.Date currentDate=new java.sql.Date(today.getTime());
					   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
					 
					   
					   String str = "Update um_users set full_name=:fullName, is_locked=:isLocked, update_date=:updateDate, updated_by=:updatedBy  where id=:userID  ";
					   logger.debug("Sql : "+str);		   
					   Map hMap=new HashMap();
					   hMap.put("userID", userVO.getUserID());
					   hMap.put("fullName", userVO.getFullName());
					   hMap.put("isLocked", userVO.getIsLocked());
					   hMap.put("updateDate", currentTimestamp);
					   hMap.put("updatedBy", userVO.getUpdatedBy());
					  					   
					   success=namedParameterJdbcTemplate.update(str, hMap);
					   if(success!=0){
						   isUpdated=true; 
					   }
					   
					   logger.debug("Exit :public Boolean updateUser(UserVO userVO)"+success+isUpdated);
					}catch(Exception se){
						logger.debug("Exception : public Boolean updateUser(UserVO userVO) "+se);
						isUpdated=false;
					}
				return isUpdated;
			}
			
			
			public Boolean deleteUser(String userID, String updatedBy)  {
				// TODO Auto-generated method stub
				Boolean isDeleted=false;
				int success=0;
							
				logger.debug("Entry :public Boolean deleteUser(String userID, String updatedBy)"+userID+updatedBy);
				try{
					   java.util.Date today = new java.util.Date();
					   java.sql.Date currentDate=new java.sql.Date(today.getTime());
					   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());  				   				   
					   String str = "Update um_users set is_deleted=1, update_date=:updateDate, updated_by=:updatedBy where id=:userID and is_system_user=0 ";
					   logger.debug("Sql : "+str);		   
					   Map hMap=new HashMap();
					
					   hMap.put("userID", userID);
					   hMap.put("updateDate", currentTimestamp);
					   hMap.put("updatedBy", updatedBy);
					   
					   success=namedParameterJdbcTemplate.update(str, hMap);
					   if(success!=0){
						   isDeleted=true; 
					   }
					   
					   logger.debug("Exit :public Boolean deleteUser(String userID, String updatedBy)"+success+isDeleted);
					}catch(Exception se){
						logger.debug("Exception in deleteUser: "+se);
						isDeleted=false;
					}
				return isDeleted;
			}
			
			public Boolean updateUserAndriodDeviceID(UserVO userVO)  {
				// TODO Auto-generated method stub
				Boolean isUpdated=false;
				int success=0;
							
				logger.debug("Entry :public Boolean updateUserAndriodDeviceID(UserVO userVO)");
				try{
					   java.util.Date today = new java.util.Date();
					   java.sql.Date currentDate=new java.sql.Date(today.getTime());
					   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());  				   				   

					   String str = "insert into um_device_relation ( fcm_token,device_model,device_platform,device_version,update_date,user_id,app_id)"
					   		+ " values( :deviceID, :model, :platform, :version,:updateDate,:userID,:appID)  on duplicate key update fcm_token=:deviceID,"
					   		+ " device_model=:model,device_platform=:platform,device_version=:version,update_date=:updateDate ; ";

			
					   logger.debug("Sql : "+str);		   
					   Map hMap=new HashMap();
					
					   hMap.put("userID", userVO.getUserID());
					   hMap.put("deviceID", userVO.getAndriodDeviceID());
					   hMap.put("updateDate", currentTimestamp);					   
					   hMap.put("platform", userVO.getDevicePlatform());
					   hMap.put("model", userVO.getDeviceModel());
					   hMap.put("version", userVO.getDeviceVersion());
					   hMap.put("appID", userVO.getAppID());
					   success=namedParameterJdbcTemplate.update(str, hMap);
					   if(success!=0){
						   isUpdated=true; 
					   }
					   
					   logger.debug("Exit :public Boolean updateUserAndriodDeviceID(UserVO userVO)"+success+isUpdated);
					}catch(Exception se){
						logger.debug("Exception in updateUserAndriodDeviceID: "+se);
						isUpdated=false;
					}
				return isUpdated;
			}
			
				
			public Boolean resetPassword(String userID, String password ,String updatedBy, String validityType)  {
				// TODO Auto-generated method stub
				Boolean isReset=false;
				int success=0;
				
				logger.debug("Entry :public Boolean resetPassword(String userID, String password) "+userID);
				try{
					   java.util.Date today = new java.util.Date();
					   java.sql.Date currentDate=new java.sql.Date(today.getTime());
					   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
					   java.sql.Timestamp passwordTimeStamp=null;			
					   //temporaryValidity
					   if(validityType.equalsIgnoreCase("T")){
					     passwordTimeStamp=dateUtility.AddingHHMMSSToDate(Integer.parseInt(config.getPropertiesValue("pwd.temporaryValidity")), 0, 0);
				       }else if(validityType.equalsIgnoreCase("P")){
				    	   passwordTimeStamp=dateUtility.AddingHHMMSSToDate(Integer.parseInt(config.getPropertiesValue("pwd.validity")), 0, 0);
				       }
				
					   String str = "Update um_users set password=:password, password_validity=:passwordValidity, password_type=:passwordType, update_date=:updateDate, updated_by=:updatedBy where id=:userID and is_system_user=0 ";
					   logger.debug("Sql : "+str);		   
					   Map hMap=new HashMap();
					
					   hMap.put("userID", userID);
					   hMap.put("password", password);
					   hMap.put("passwordValidity", passwordTimeStamp);
					   hMap.put("updateDate", currentTimestamp);
					   hMap.put("updatedBy", updatedBy);
					   hMap.put("passwordType", validityType);
					   
					   success=namedParameterJdbcTemplate.update(str, hMap);
					   if(success!=0){
						   isReset=true; 
					   }
					   
					   logger.debug("Exit :public Boolean resetPassword(String userID, String password) "+success+isReset);
					}catch(Exception se){
						logger.debug("Exception in resetPassword: "+se);
						isReset=false;
					}
				return isReset;
			}
			
			public Boolean revokeOrgAccess(String userID, String appID, String orgID){
				Boolean isRevoked=false;
				int sucess = 0;
				String strSQL = "";
				try {
						logger.debug("Entry : public Boolean revokeOrgAccess(String userID, String appID, String orgID)");
						strSQL = " DELETE FROM um_user_app_role WHERE user_id="+userID+" and app_id="+appID+" and org_id="+orgID+" " ;
						logger.debug("query : " + strSQL);
						// 2. Parameters.
						Map hMap = new HashMap();
									
						sucess=namedParameterJdbcTemplate.update(strSQL, hMap);
						
						if(sucess!=0){
							isRevoked=true;
						}
						logger.debug("Exit : public Boolean revokeOrgAccess(String userID, String appID, String orgID)"+sucess+isRevoked);

				}catch(Exception Ex){	
					isRevoked=false;
					logger.error("Exception in revokeOrgAccess : "+Ex);		
				}
				return isRevoked;
			}	

			public UserVO getUserRole(String userID, String appID, String orgID){
				UserVO userVO =new UserVO();
				String strSQLQuery = "";
				
				try{
					logger.debug("Entry : public UserVO getUserRole(String userID, String appID, String orgID)" );
//					1. SQL Query
					//strSQLQuery = " SELECT * FROM um_user_app_role WHERE user_id=:userID and app_id=:appID and org_id=:orgID ";				
					strSQLQuery = " SELECT u.*, r.name FROM um_user_app_role u, um_role r  WHERE u.user_id=:userID and u.app_id=:appID and u.org_id=:orgID and r.id=u.role_id ";	

					logger.debug("query : " + strSQLQuery);		
					
					 Map hMap=new HashMap();
					 hMap.put("userID", userID);
					 hMap.put("appID", appID);
					 hMap.put("orgID", orgID);
					 
//					3. RowMapper.
					 RowMapper RMapper = new RowMapper() {
					        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					        	UserVO userVO = new UserVO();			        	
					        	userVO.setUserID(rs.getString("user_id"));
					        	userVO.setAppID(rs.getString("app_id"));
					        	userVO.setRoleID(rs.getString("role_id"));
					        	userVO.setOrgID(rs.getString("org_id"));
					        	userVO.setRoleName(rs.getString("name"));
					        	
					        	return userVO;
					        }};
										
					        userVO = (UserVO) namedParameterJdbcTemplate.queryForObject(strSQLQuery,hMap,RMapper);		
					
					logger.debug("Exit : public UserVO getUserRole(String userID, String appID, String orgID)");
					userVO.setLoginSuccess(true);
				     
				    	
				 }catch(Exception ex){
					userVO.setLoginSuccess(false);
					userVO.setStatusCode(500);
			         logger.error("Exception in getUserRole : "+ex);            
			    }
			   logger.debug("Exit : public UserVO getUserRole(String userID, String appID, String orgID) ");
				return userVO;	
			}
			
			 
			 public int createRole(UserVO roleVO){
				    int roleID = 0;
				
					try{
						logger.debug("Entry : public int createRole(UserVO userVO)");	
						 
						 String SQL = "INSERT INTO um_role (name,app_id,org_id )  " +
							        "VALUES (:roleName, :appID, :orgID)";
							   SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(roleVO);
							   KeyHolder keyHolder = new GeneratedKeyHolder();
							   getNamedParameterJdbcTemplate().update(SQL, fileParameters, keyHolder);
							   roleID=keyHolder.getKey().intValue();
										
						logger.debug("Exit :public int createRole(UserVO userVO)"+roleID);
						
					}catch(Exception Ex){
						logger.error("Exception in createRole : "+Ex);
						roleID = 0;
					}
					return roleID;		
					
			}
			 
			 public Boolean updateRole(UserVO roleVO)  {
					// TODO Auto-generated method stub
					Boolean isUpdated=false;
					int success=0;
								
					logger.debug("Entry : public Boolean updateRole(UserVO roleVO)"+roleVO.getRoleID());
					try{
						  				   				   
						   String str = "Update um_role set name=:roleName where id=:roleID and is_system_role=0 ";
						   logger.debug("Sql : "+str);		   
						   Map hMap=new HashMap();
						
						   hMap.put("roleID", roleVO.getRoleID());
						   hMap.put("roleName", roleVO.getRoleName());
						 
						   success=namedParameterJdbcTemplate.update(str, hMap);
						   if(success!=0){
							   isUpdated=true; 
						   }
						   
						   logger.debug("Exit  public Boolean updateRole(UserVO roleVO))"+success+isUpdated);
						}catch(Exception se){
							logger.debug("Exception in updateRole: "+se);
							isUpdated=false;
						}
					return isUpdated;
				} 
			 
			 public Boolean deleteRole(String roleID, String appID)  {
					Boolean isDeleted=false;
					int success=0;
								
					logger.debug("Entry :public Boolean deleteRole(String roleID, String appID)"+roleID+appID);
					try{
						  
						   String str = " Delete from um_role where id=:roleID and app_id=:appID and is_system_role=0 ";
						   logger.debug("Sql : "+str);	
						   
						   Map hMap=new HashMap();
						   hMap.put("roleID", roleID);
						   hMap.put("appID", appID);
						  					   
						   success=namedParameterJdbcTemplate.update(str, hMap);
						   if(success!=0){
							   isDeleted=true; 
						   }
						   
						   logger.debug("Exit :public Boolean deleteRole(String roleID, String appID)"+success+isDeleted);
						}catch(Exception se){
							logger.debug("Exception in deleteRole: "+se);
							isDeleted=false;
						}
					return isDeleted;
			}    
			 
			 public Boolean checkRoleForUser(String roleID){
					String strSQLQuery = "";
					Boolean isPresent=true;
					int count=0;
					try{
						logger.debug("Entry : public Boolean checkRoleForUser(String roleID)"+roleID );
		                 //	1. SQL Query
						strSQLQuery = " SELECT role_id FROM um_user_app_role WHERE role_id=:roleID  LIMIT 1 " ;				
						logger.debug("query : " + strSQLQuery);		
						
						 Map hMap=new HashMap();
						 hMap.put("roleID", roleID);
												 			 
						 count=namedParameterJdbcTemplate.queryForObject(strSQLQuery, hMap, Integer.class);
						 if(count>0){
							 isPresent=true; 
						 }
						logger.debug("Exit :public Boolean matchAccessToken(String orgID, String token)"+isPresent);
						
					}catch(EmptyResultDataAccessException e){
						logger.debug("Role is not assign to any user");
						isPresent=false;
				
					}catch(Exception ex){
				         logger.error("Exception in checkRoleForUser : "+ex);       
				         isPresent=false;
				    }
				   logger.debug("Exit : public Boolean checkRoleForUser(String roleID)");
					return isPresent;	
				} 
			 
			 public Boolean insertRolePermission(List<PermissionVO> permissionVO){
				 Boolean isSuccess =false;
				
					try{
						logger.debug("Entry : public int insertRolePermission(List<PermissionVO> permissionVO)");	
						 
						 String SQL = "INSERT INTO um_role_permission (role_id,permission_id )  " +
							        "VALUES (:roleID, :accessID)";
						 SqlParameterSource[] params =
									SqlParameterSourceUtils.createBatch(permissionVO.toArray());
							  
						 int[] updateCounts=namedParameterJdbcTemplate.batchUpdate(SQL, params);
					 
						 logger.debug("Role permission update Counts: "+updateCounts.length); 
						 if(updateCounts.length>0){
							 isSuccess=true;
						 }
										
						logger.debug("Exit :public int insertRolePermission(List<PermissionVO> permissionVO)"+updateCounts.length);
						
					}catch(Exception Ex){
						logger.error("Exception in insertRolePermission : "+Ex);
						isSuccess = false;
					}
					return isSuccess;		
					
			}
			 
			 public Boolean deleteRolePermission(String roleID)  {
					// TODO Auto-generated method stub
					Boolean isDeleted=false;
					int success=0;
								
					logger.debug("Entry :public Boolean deleteRolePermission(String roleID)"+roleID);
					try{
						  
						   String str = " Delete from um_role_permission where role_id="+roleID+" ";
						   logger.debug("Sql : "+str);		   
						  
						   Map hMap=new HashMap();						
						   hMap.put("roleID", roleID);
						  					   
						   success=namedParameterJdbcTemplate.update(str, hMap);
						   
						   if(success!=0){
							   isDeleted=true; 
						   }
						   
						   logger.debug("Exit :public Boolean deleteRolePermission(String roleID)"+success+isDeleted);
						}catch(Exception se){
							logger.debug("Exception in deleteRolePermission: "+se);
							isDeleted=false;
						}
					return isDeleted;
				}     
		 
			 
			 public AppVO getAppDetails(String appName,String type){
				 AppVO appVO = new AppVO();		
					String strSQLQuery = "";
					String strCondition="";
					if(type.equalsIgnoreCase("name")){
						strCondition=" name =:appName ";
					}else if(type.equalsIgnoreCase("id")){
						strCondition=" id =:appName ";
					}
					try{
						logger.debug("Entry : public AppVO getAppDetails(String appName,String type)"+appName+type );
                      //	1. SQL Query
						strSQLQuery = " SELECT * FROM um_app_master WHERE "+strCondition ;				
						logger.debug("query : " + strSQLQuery);		
						
						 Map hMap=new HashMap();
						 hMap.put("appName", appName);
						 			 
                       //3. RowMapper.
						 RowMapper RMapper = new RowMapper() {
						        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						        	AppVO appsVO = new AppVO();			        	
						        	appsVO.setAppID(rs.getString("id"));
						        	appsVO.setAppName(rs.getString("name"));
						        	appsVO.setAppFullName(rs.getString("app_full_name"));
						        	appsVO.setDomain(rs.getString("domain"));
						        	appsVO.setIpAddress(rs.getString("ip_address"));
						        	appsVO.setForgotPasswordURL(rs.getString("forgot_password_url"));
						        	appsVO.setPwdTempoaryValidity(rs.getInt("pwd_tempoary_validity"));
						        	appsVO.setPwdValidity(rs.getInt("pwd_validity"));
						        	appsVO.setOtpValidity(rs.getInt("otp_validity"));
						        	appsVO.setAccessTokenValidity(rs.getInt("access_token_validity"));
						        	appsVO.setRefreshTokenValidity(rs.getInt("refresh_token_validity"));
						        	appsVO.setIsOTPVerify(rs.getInt("is_otp_verify"));
						        	appsVO.setSendEmailID(rs.getString("send_email_id"));
						        	return appsVO;
						        }};
						
						
					    appVO = (AppVO) namedParameterJdbcTemplate.queryForObject(strSQLQuery,hMap,RMapper);		
											
					}catch(EmptyResultDataAccessException ex){
					    logger.error("EmptyResultDataAccessException in getAppDetails : "+ex);     
				    }catch(Exception ex){					
				         logger.error("Exception in getAppDetails : "+ex);            
				    }
				   logger.debug("Exit :public AppVO getAppDetails(String appName,String type ");
					return appVO;	
				}
			 
			 public SocietyVO getOrgAppSubscriptionDetails(String appID,String orgID){	
			    	SocietyVO societyVO=new SocietyVO();		    	
			                
			    	try
			    	{
			    		logger.debug("Entry : public SocietyVO getOrgAppSubscriptionDetails(int appID,int orgID)");
			    		 String query = "";
					    	
					        query=" SELECT oas.*,s.*,ss.* FROM org_app_subscription oas , society_details s ,society_settings ss "+
		                          " WHERE s.society_id=oas.org_id AND ss.society_id=s.society_id AND oas.org_id =:orgID AND oas.app_id=:appID ";
					        
			    		Map hashMap=new HashMap();
			    		hashMap.put("appID", appID);
			    		hashMap.put("orgID", orgID);
			    		
			    		 RowMapper RMapper = new RowMapper() {
			 		        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			 		            SocietyVO societyVO=new SocietyVO();
			 		            societyVO.setSocietyID(rs.getInt("s.society_id"));
			 		            societyVO.setSocietyName(rs.getString("society_name"));
			 		            societyVO.setSocietyRegNo(rs.getString("register_no"));
			 		            societyVO.setSocFullName(rs.getString("society_full_name"));
			 		            societyVO.setMailListM(rs.getString("mailingList_member"));
			 		            societyVO.setSocShortName(rs.getString("short_name"));
			 		            societyVO.setNotes(rs.getString("notes"));
			 		            societyVO.setEffectiveDate(rs.getString("effective_date"));
			 		            societyVO.setThresholdBalance(rs.getBigDecimal("min_amount"));
			 		            societyVO.setTxCloseDate(rs.getString("tx_close_date"));
			 		            societyVO.setAccountant(rs.getString("accountant"));
			 		            societyVO.setServiceTax(rs.getBigDecimal("service_tax"));
			 		            societyVO.setAppSubscriptionValidity(rs.getTimestamp("expired_date"));
			 		           societyVO.setIsGracePeriod(rs.getInt("subscription_id"));
			 		            return societyVO;
			 		        }};
			 	
			    					    		   					    		
			    		societyVO=(SocietyVO) namedParameterJdbcTemplate.queryForObject(query, hashMap, RMapper);
			       		
			    	}
			    	catch(EmptyResultDataAccessException e){
			    		logger.info("No organization Details available"+e);
			    	}
			    	catch(NullPointerException Ex){
			    		logger.info("No organization details available : "+Ex);
			    	}
			    	catch(Exception Ex){
			    		logger.error("Exception in public SocietyVO getOrgAppSubscriptionDetails(int appID,int orgID) : "+Ex);
			    	}
			    	logger.debug("Exit : public SocietyVO getOrgAppSubscriptionDetails(int appID,int orgID)");
			    return societyVO;	
			    }
			 
			 
			 
			 public List getUserApartmentList(String userID){
				 List societyList=null;
					String sql=null;
					try{
						logger.debug("Entry : public List getUserApartmentList(String userID))");
									
						
						sql=" SELECT m.* FROM um_user_apartment u,members_info m  WHERE m.apt_id=u.apt_id AND m.type='P' AND m.is_current='1' AND u.user_id=:userID  ";
						logger.debug("Query : " + sql);
						
						Map namedParam = new HashMap();
						namedParam.put("userID",userID);
						
						
						RowMapper RMapper=new RowMapper()
						{

							public Object mapRow(ResultSet rs, int n) throws SQLException {
								// TODO Auto-generated method stub
								 MemberVO memberVO=new MemberVO();
								 memberVO.setAptID(rs.getInt("apt_id"));
								 memberVO.setMemberID(rs.getInt("member_id"));
								 memberVO.setFlatNo(rs.getString("unit_name"));
								 memberVO.setBuildingID(rs.getInt("building_id"));
								 memberVO.setSocietyID(rs.getInt("society_id"));
								 memberVO.setFullName(rs.getString("full_name"));
								 memberVO.setIsRented(rs.getInt("is_rented"));
								return memberVO;
							}
							
						};
						
						societyList=  namedParameterJdbcTemplate.query(sql, namedParam, RMapper);
					}catch(EmptyResultDataAccessException Ex){
						logger.info("No Data found getUserApartmentList "+Ex);		
											
					}catch(Exception ex){
						logger.error("Exception in getUserApartmentList : "+ex);			
					}
					logger.debug("Exit :public List getUserApartmentList(String userID)");
					return societyList;
					
				}
			 
			 
			 public List getListOfAllOrgInfoWithUserEmail(String emailID,String appID){
				 List organizationList=null;
					String sql=null;
					try{
						logger.debug("Entry : public List getListOfAllOrgInfoWithUserEmail(String emailID)"+emailID+appID);
									
						
						sql=" SELECT m.apt_id,m.society_id,app_id,full_name,mobile FROM members_info m, society_details s, org_app_subscription o "
                            + " WHERE m.email=:emailID AND s.society_id=m.society_id AND s.working='1' AND o.org_id=s.society_id AND o.app_id=:appID AND o.expired_date>NOW() GROUP BY apt_id ";


						logger.debug("Query : " + sql);
						
						Map namedParam = new HashMap();
						namedParam.put("emailID",emailID);
						namedParam.put("appID",appID);
						
						RowMapper RMapper=new RowMapper()
						{
							public Object mapRow(ResultSet rs, int n) throws SQLException {
								// TODO Auto-generated method stub
								UserVO userVO= new UserVO();	
								userVO.setAppID(rs.getString("app_id"));
								userVO.setOrgID(rs.getString("society_id"));
								userVO.setApartmentID(rs.getInt("apt_id"));
								userVO.setFullName(rs.getString("full_name"));
								if(!rs.getString("mobile").equalsIgnoreCase("0000000000"))
								  userVO.setMobile(rs.getString("mobile"));
								
								return userVO;
							}
							
						};
						
						organizationList=namedParameterJdbcTemplate.query(sql, namedParam, RMapper);
					}catch(EmptyResultDataAccessException Ex){
						logger.info("getListOfAllOrgInfoWithUserEmail No Apartment found for email "+emailID);									
						
					}catch(Exception ex){
						logger.error("Exception in getListOfAllOrgInfoWithUserEmail : "+ex);			
					}
					logger.debug("Exit :public List getListOfAllOrgInfoWithUserEmail(String emailID)");
					return organizationList;
					
				}

			 
			 public int assignApartment(UserVO userVO){
					int insertFlag= 0;
					
					try{
						logger.debug("Entry :  public int assignApartment(UserVO userVO) "+userVO.getUserID()+userVO.getApartmentID());	
						 
							String sqlQuery="INSERT INTO um_user_apartment(user_id, apt_id,user_type) VALUES ( :userID, :aptID, :userType );" ;
							logger.debug("query : " + sqlQuery);
							
							 Map hmap = new HashMap();
							
							 hmap.put("userID", userVO.getUserID());
							 hmap.put("aptID", userVO.getApartmentID());		
							 hmap.put("userType", userVO.getUserType());
							 
							 insertFlag = namedParameterJdbcTemplate.update(sqlQuery, hmap);															
						
					}catch(DuplicateKeyException duplicateEntry){
						logger.info("AssignApartment duplicate entry for UserID : "+userVO.getUserID()+" ApartmentID: "+userVO.getApartmentID());
						insertFlag=2;
						return insertFlag;	
						
					}catch(Exception Ex){
						logger.error("Exception in assignApartment : "+Ex);
						insertFlag=0;
						return insertFlag;
					}
					
					logger.debug("Exit :public Boolean assignApartment(UserVO userVO)"+insertFlag);
					return insertFlag;
								
				}
			 
			 public List getAppUserOrgApartmentList(String userID, String appID){
				 List societyList=null;
					String sql=null;
					try{
						logger.debug("Entry : public List getAppUserOrgApartmentList(String userID,  String appID)");
									
						
						sql=" SELECT upr.*,apt.apt_id, m.apt_name,m.building_name,m.unit_name,am.name AS appName,r.name AS roleName,sd.*,a.*  FROM um_users u, um_user_app_role upr,um_user_apartment apt,members_info m, um_app_master am, um_role r, society_details sd, society_address a "
		                  + " WHERE u.id=upr.user_id AND upr.app_id=am.id  AND r.id=upr.role_id AND sd.society_id=upr.org_id AND sd.working='1' AND sd.society_id=a.society_id AND m.type='P' AND m.is_current='1'  AND m.society_id=upr.org_id "
		                  + " AND apt.user_id=u.id AND apt.user_type='M' AND m.apt_id=apt.apt_id AND upr.user_id =:userID AND upr.app_id=:appID ORDER BY default_org_id DESC; ";
						logger.debug("Query : " + sql);
						
						Map namedParam = new HashMap();
						namedParam.put("userID",userID);
						namedParam.put("appID",appID);
						
						RowMapper RMapper=new RowMapper()
						{

							public Object mapRow(ResultSet rs, int n) throws SQLException {
								// TODO Auto-generated method stub
								 SocietyVO societyVO=new SocietyVO();
						         AddressVO addressVO=new AddressVO();
						            societyVO.setAppID(rs.getString("app_id"));
						            societyVO.setAppName(rs.getString("appName"));
						            societyVO.setRoleID(rs.getString("role_id"));
						            societyVO.setRoleName(rs.getString("roleName"));
						            societyVO.setDefaultOrgID(rs.getString("default_org_id"));
						            societyVO.setApartmentID(rs.getInt("apt_id"));
						            societyVO.setUnitName(rs.getString("unit_name"));
						            societyVO.setSocietyID(rs.getInt("society_id"));
						            societyVO.setSocFullName(rs.getString("society_full_name"));
						            societyVO.setSocShortName(rs.getString("short_name"));
						            societyVO.setSocietyRegNo(rs.getString("register_no"));
						            societyVO.setMailListM(rs.getString("mailingList_member"));
						            societyVO.setMailListA(rs.getString("mailingList_committee"));
						            societyVO.setSocietyName(rs.getString("society_name"));
						            addressVO.setAddLine1(rs.getString("line_1"));
						            addressVO.setAddLine2(rs.getString("line_2"));
						            addressVO.setCity(rs.getString("city"));
						            addressVO.setZip(rs.getString("postal_code"));
						            addressVO.setState(rs.getString("state"));
						            societyVO.setAddress(rs.getString("line_1")+", "+rs.getString("line_2")+", "+rs.getString("city")+", "+rs.getString("postal_code"));
						            societyVO.setSocietyAddress(addressVO);
								
								return societyVO;
							}
							
						};
						
						societyList=  namedParameterJdbcTemplate.query(sql, namedParam, RMapper);
					}catch(EmptyResultDataAccessException Ex){
						logger.info("No Data found getAppUserOrgApartmentList "+Ex);		
											
					}catch(Exception ex){
						logger.error("Exception in getAppUserOrgApartmentList : "+ex);			
					}
					logger.debug("Exit :public List getAppUserOrgApartmentList(String userID,  String appID)");
					return societyList;
					
				}
			 
			 public Boolean verifyUserApartment(String userID, int apartmentID, String userType){
					String strSQLQuery = "";
					Boolean isPresent=false;
					int count=0;
					try{
						logger.debug("Entry : public Boolean verifyUserApartment(String roleID)"+userID+ apartmentID+ userType);
		                 //	1. SQL Query
						strSQLQuery = " SELECT apt_id FROM um_user_apartment WHERE apt_id=:apartmentID and user_id=:userID and user_type=:userType  LIMIT 1 " ;				
						logger.debug("query : " + strSQLQuery);		
						
						 Map hMap=new HashMap();
						 hMap.put("userID", userID);
						 hMap.put("apartmentID", apartmentID);
						 hMap.put("userType", userType);
						 
						 count=namedParameterJdbcTemplate.queryForObject(strSQLQuery, hMap, Integer.class);
						 if(count>0){
							 isPresent=true; 
						 }
						logger.debug("Exit : public Boolean verifyUserApartment(String userID, String apartmentID)"+isPresent);
						
					}catch(EmptyResultDataAccessException e){
						logger.debug("Apartment is not assign to user");
						isPresent=false;
				
					}catch(Exception ex){
				         logger.error("Exception in verifyUserApartment : "+ex);       
				         isPresent=false;
				    }
				   logger.debug("Exit : public Boolean verifyUserApartment(String roleID)");
					return isPresent;	
				} 
			 
			 
			 
			 public Boolean verifyPermission(String roleID, String permission){
					String strSQLQuery = "";
					Boolean isValid=false;
					int count=0;
					try{
						logger.debug("Entry : public Boolean verifyPermission(String roleID, String permission)"+roleID+ permission);
		                 //	1. SQL Query
						strSQLQuery = " SELECT p.id FROM um_permission p,um_role_permission r WHERE r.role_id=:roleID AND r.permission_id=p.id AND TRIM(p.server_access_method)=:permission LIMIT 1 " ;				
						logger.debug("query : " + strSQLQuery);		
						
						 Map hMap=new HashMap();
						 hMap.put("roleID", roleID);
						 hMap.put("permission", permission.trim());
												 			 
						 count=namedParameterJdbcTemplate.queryForObject(strSQLQuery, hMap, Integer.class);
						 if(count>0){
							 isValid=true; 
						 }
						logger.debug("Exit : public Boolean verifyPermission(String roleID, String permission)"+isValid);
						
					}catch(EmptyResultDataAccessException e){
						logger.debug("No permission for this role");
						isValid=false;
				
					}catch(NullPointerException e){
						logger.debug("NullPointerException : No permission for this role");
						isValid=false;
				
					}catch(Exception ex){
				         logger.error("Exception in verifyPermission : "+ex);       
				         isValid=false;
				    }
				   logger.debug("Exit : public Boolean verifyPermission(String roleID, String permission)");
					return isValid;	
				} 
			 

			 public UserVO getUserDeviceDetails(String userID,String appID){
					UserVO userVO =new UserVO();
					String strSQLQuery=null;
									try{
						logger.debug("Entry :  public UserVO getUserDeviceDetails(int userID,int appID)");
//						1. SQL Query
						strSQLQuery = " SELECT * FROM um_device_relation WHERE user_id=:userID and app_id=:appID ; ";				
						logger.debug("query : " + strSQLQuery);		
						
						 Map hMap=new HashMap();
						 hMap.put("userID", userID);
						 hMap.put("appID", appID);
						 			 
//						3. RowMapper.
						 RowMapper RMapper = new RowMapper() {
						        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						        	UserVO userVO = new UserVO();			        	
						        	
						        	userVO.setAndriodDeviceID(rs.getString("fcm_token"));
						        	userVO.setDeviceModel(rs.getString("device_model"));
						        	userVO.setDevicePlatform(rs.getString("device_platform"));
						        	userVO.setDeviceVersion(rs.getString("device_version"));
						        	return userVO;
						        }};
						
						
						        userVO = (UserVO) namedParameterJdbcTemplate.queryForObject(strSQLQuery,hMap,RMapper);		
						
						logger.debug("Exit : public UserVO getUserDeviceDetails(int userID,int appID)");
						userVO.setLoginSuccess(true);
					}catch(EmptyResultDataAccessException ex){
						userVO.setLoginSuccess(false);
						userVO.setStatusCode(521);
					//    logger.info("No user details found for emailID:  "+userName);            
					    	
					}catch(RecoverableDataAccessException ex){
						userVO.setLoginSuccess(false);
						userVO.setStatusCode(500);
				        logger.info("RecoverableDataAccessException in getUserDetailsByLoginId ");            
				    }catch(Exception ex){
						userVO.setLoginSuccess(false);
						userVO.setStatusCode(500);
				        logger.error("Exception in public UserVO getUserDeviceDetails(int userID,int appID)  for user ID :"+userID+" and App ID "+appID+" "+ex);            
				    }
				   logger.debug("Exit : public UserVO getUserDeviceDetails(int userID,int appID) ");
					return userVO;	
				}		 
			    
			
			 public UserVO getUserDeviceDetailsFromMemberID(int memberID,String appID,String userType){
					UserVO userVO =new UserVO();
					String strSQLQuery=null;
									try{
						logger.debug("Entry :  public UserVO getUserDeviceDetailsFromMemberID(int memberID,String appID)");
//						1. SQL Query
						strSQLQuery = "  SELECT u.*,ud.* FROM members_info m,um_users u,um_user_apartment ua,um_device_relation ud WHERE m.apt_id=ua.apt_id AND ua.user_type=:userType and ua.user_id=u.id AND u.id=ud.user_id AND ud.app_id=:appID AND m.member_id=:memberID GROUP BY fcm_token order by ud.id limit 1 ; ";				
						logger.debug("query : " + strSQLQuery);		
						
						 Map hMap=new HashMap();
						 hMap.put("memberID", memberID);
						 hMap.put("appID", appID);
						 hMap.put("userType", userType);
						 			 
//						3. RowMapper.
						 RowMapper RMapper = new RowMapper() {
						        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						        	UserVO userVO = new UserVO();			        	
						        	
						        	userVO.setAndriodDeviceID(rs.getString("fcm_token"));
						        	userVO.setDeviceModel(rs.getString("device_model"));
						        	userVO.setDevicePlatform(rs.getString("device_platform"));
						        	userVO.setDeviceVersion(rs.getString("device_version"));
						        	return userVO;
						        }};
						
						
						        userVO = (UserVO) namedParameterJdbcTemplate.queryForObject(strSQLQuery,hMap,RMapper);		
						
						logger.debug("Exit : public UserVO getUserDeviceDetailsFromMemberID(int memberID,String appID)");

					}catch(EmptyResultDataAccessException ex){
						
					//    logger.info("No user details found for emailID:  "+userName);            
					    	
					}catch(RecoverableDataAccessException ex){
					
				        logger.info("RecoverableDataAccessException in getUserDeviceDetailsFromMemberID ");            
				    }catch(Exception ex){
						
				        logger.error("Exception in public UserVO getUserDeviceDetailsFromMemberID(int userID,int appID)  for member ID :"+memberID+" and App ID "+appID+" "+ex);            
				    }
				   logger.debug("Exit : public UserVO getUserDeviceDetailsFromMemberID(int userID,int appID) ");
					return userVO;	
				}		 


			 
			 
			 public List getAppUserProfileOrganizationList(String userID, String appID, String orgID, String profileType){
				 List orgList=null;
					String sql=null;
					String condition="";
					try{
						logger.debug("Entry : public List getAppUserProfileOrganizationList(String userID, String orgID, String appID)"+userID+appID+profileType);
					    if(orgID.equalsIgnoreCase("0"))	{
					    	condition=" ";
					    }else{
					    	condition=" AND upr.org_id="+orgID+"  ";
					    }
						
						sql=" SELECT p.id As profileID,p.profile_type,upr.*,apt.apt_id, am.name AS appName,r.name AS roleName,sd.*,a.*  FROM um_users u, um_user_app_role upr,um_user_apartment apt, account_ledger_profile_details p, um_app_master am, um_role r, society_details sd, society_address a "
		                  + " WHERE u.id=upr.user_id AND upr.app_id=am.id  AND r.id=upr.role_id AND sd.society_id=upr.org_id AND sd.working='1' AND sd.society_id=a.society_id  AND p.id=apt.apt_id  "
		                  + " AND apt.user_type=p.profile_type AND apt.user_id=u.id AND apt.user_type=:profileType AND upr.user_id =:userID AND upr.app_id=:appID "+condition+" ORDER BY default_org_id DESC; ";
						logger.info("Query : " + sql);
						
						Map namedParam = new HashMap();
						namedParam.put("userID",userID);
						namedParam.put("appID",appID);
						namedParam.put("profileType",profileType);
												
						RowMapper RMapper=new RowMapper()
						{

							public Object mapRow(ResultSet rs, int n) throws SQLException {
								// TODO Auto-generated method stub
								 SocietyVO societyVO=new SocietyVO();
						         AddressVO addressVO=new AddressVO();
						            societyVO.setAppID(rs.getString("app_id"));
						            societyVO.setAppName(rs.getString("appName"));
						            societyVO.setApartmentID(rs.getInt("profileID"));
						            societyVO.setProfileType(rs.getString("profile_type"));
						            societyVO.setRoleID(rs.getString("role_id"));
						            societyVO.setRoleName(rs.getString("roleName"));
						            societyVO.setDefaultOrgID(rs.getString("default_org_id"));
						            societyVO.setSocietyID(rs.getInt("society_id"));
						            societyVO.setSocFullName(rs.getString("society_full_name"));
						            societyVO.setSocShortName(rs.getString("short_name"));
						            societyVO.setSocietyRegNo(rs.getString("register_no"));
						            societyVO.setMailListM(rs.getString("mailingList_member"));
						            societyVO.setMailListA(rs.getString("mailingList_committee"));
						            societyVO.setSocietyName(rs.getString("society_name"));
						            addressVO.setAddLine1(rs.getString("line_1"));
						            addressVO.setAddLine2(rs.getString("line_2"));
						            addressVO.setCity(rs.getString("city"));
						            addressVO.setZip(rs.getString("postal_code"));
						            addressVO.setState(rs.getString("state"));
						            societyVO.setAddress(rs.getString("line_1")+", "+rs.getString("line_2")+", "+rs.getString("city")+", "+rs.getString("postal_code"));
						            societyVO.setSocietyAddress(addressVO);
								
								return societyVO;
							}
							
						};
						
						orgList=  namedParameterJdbcTemplate.query(sql, namedParam, RMapper);
					}catch(EmptyResultDataAccessException Ex){
						logger.info("No Data found getAppUserProfileOrganizationList "+Ex);		
											
					}catch(Exception ex){
						logger.error("Exception in getAppUserProfileOrganizationList : "+ex);			
					}
					logger.debug("Exit :public List getAppUserProfileOrganizationList(String userID,  String appID)"+orgList.size());
					return orgList;
					
				}

			 
			 //=========== Add SignUp validity for orgs which are on trial period ============//
			 
			 public int insertOrganisationValidity(int orgID,int appID,int validity,int isTrial){
					int insertFlag= 0;
					
					try{
						logger.debug("Entry : public int insertOrganisationValidity(int orgID,int appID,int validity,int isTrial)");	
						  java.util.Date today = new java.util.Date();
						   java.sql.Date currentDate=new java.sql.Date(today.getTime());
						   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
						   java.sql.Timestamp expiryDate=new java.sql.Timestamp(today.getTime());	
						   Calendar cal = Calendar.getInstance();
						   cal.setTime(currentDate);
						
					if(isTrial==1){
							cal.add(Calendar.DAY_OF_WEEK, Integer.parseInt(config.getPropertiesValue("signUp.validity")));
							}else{
							cal.add(Calendar.DAY_OF_WEEK, validity);
							}
						
						expiryDate.setTime(cal.getTime().getTime());
						
						
							String sqlQuery="INSERT INTO org_app_subscription(org_id,  app_id, expired_date) "+
							 " VALUES ( :orgID, :appID, :expiredDate );" ;
							logger.debug("query : " + sqlQuery);
							
							 Map hmap = new HashMap();
							
							 hmap.put("appID", appID);
							 hmap.put("orgID", orgID);
							 hmap.put("expiredDate", expiryDate);
													 
							 insertFlag = namedParameterJdbcTemplate.update(sqlQuery, hmap);															
						
					}catch(DuplicateKeyException duplicateEntry){
						logger.info("DuplicateKeyException in insertOrganisationValidity : appD "+appID+" OrgID: "+orgID);
						insertFlag=2;
						return insertFlag;	
						
					}catch(Exception Ex){
						logger.error("Exception in public int insertOrganisationValidity(int orgID,int appID,int validity,int isTrial) : "+Ex);
						insertFlag=0;
						return insertFlag;
					}
					
					logger.debug("Exit :  public int insertOrganisationValidity(int orgID,int appID,int validity,int isTrial)"+insertFlag);
					return insertFlag;
								
				}
			 
			 
			 
			 public void insertAuditActivity(String userID,String appID,String activity,String module) {
				 Date currentDatetime = new Date(System.currentTimeMillis());   
				 java.sql.Timestamp timestamp = new java.sql.Timestamp(currentDatetime.getTime());
				
				 int flag = 0;
				
				try{
					
					logger.debug(" Entry : public int insertAuditActivity(String member_id,String activity,String module) ");
					 
					String sqlQuery=" INSERT INTO audit_trial(user_id,app_id,timestamp,activity,module) VALUES (:userID,:appID,:timestamp,:activity,:module); ";
					 Map hmap = new HashMap();
						
					 hmap.put("appID", appID);
					 hmap.put("userID", userID);
					 hmap.put("timestamp", timestamp);
					 hmap.put("activity", activity);
					 hmap.put("module", module);
											 
					 flag = namedParameterJdbcTemplate.update(sqlQuery, hmap);		                                             
					
					
					logger.debug(" Exit :public int insertAuditActivity(String member_id,String activity,String module) ");
					
					logger.debug("flag for insert in audit:"+flag);
					 
				 }	catch(Exception Ex){
					 
					 logger.error("domain error : " + Ex);
				 } 
				
				
			}
			 
			 
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	 
    
}
