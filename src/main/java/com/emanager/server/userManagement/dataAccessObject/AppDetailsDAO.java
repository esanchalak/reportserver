package com.emanager.server.userManagement.dataAccessObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.emanager.server.userManagement.valueObject.AppVO;

public class AppDetailsDAO {

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private static final Logger logger = Logger.getLogger(AppDetailsDAO.class);
	


	public List getAppDetailsList() {
		String strSQLQuery = "";
		List appsList=null;	

		try {
			logger.debug("Entry : public List getAppDetailsList() ");
//			1. SQL Query
			strSQLQuery = " SELECT * FROM um_app_master ;";
			logger.debug("Query : " + strSQLQuery);
			// 2. Parameters.
			Map namedParameters = new HashMap();
			

			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					AppVO appsVO = new AppVO();
					appsVO.setAppFullName(rs.getString("app_full_name"));
					appsVO.setAppName(rs.getString("name"));
					appsVO.setAppID(rs.getString("id"));
					appsVO.setDomain(rs.getString("domain"));
					appsVO.setIpAddress(rs.getString("ip_address"));
					appsVO.setForgotPasswordURL(rs.getString("forgot_password_url"));
					appsVO.setLogoUrl(rs.getString("logo_url"));
					appsVO.setDescription(rs.getString("description"));
					appsVO.setPwdTempoaryValidity(rs.getInt("pwd_tempoary_validity"));
		        	appsVO.setPwdValidity(rs.getInt("pwd_validity"));
		        	appsVO.setOtpValidity(rs.getInt("otp_validity"));
		        	appsVO.setAccessTokenValidity(rs.getInt("access_token_validity"));
		        	appsVO.setRefreshTokenValidity(rs.getInt("refresh_token_validity"));
		        	appsVO.setIsOTPVerify(rs.getInt("is_otp_verify"));
		        	appsVO.setSendEmailID(rs.getString("send_email_id"));
					return appsVO;
				}
			};

			appsList=namedParameterJdbcTemplate.query(strSQLQuery, namedParameters, RMapper);

			logger.debug("Exit public List public List getAppDetailsList()");

		}catch(EmptyResultDataAccessException Ex) {
			logger.info("No app details are available  ");
		} catch (Exception e) {
			logger.error("Exception in public List getAppDetailsList() : "+e);
		}

		return appsList;

	}
	
	 public AppVO getAppDetails(String appName,String type){
		 AppVO appVO = new AppVO();		
			String strSQLQuery = "";
			String strCondition="";
			if(type.equalsIgnoreCase("name")){
				strCondition=" name =:appName ";
			}else if(type.equalsIgnoreCase("id")){
				strCondition=" id =:appName ";
			}
			try{
				logger.debug("Entry : public AppVO getAppDetails(String appName,String type)"+appName+type );
              //	1. SQL Query
				strSQLQuery = " SELECT * FROM um_app_master WHERE "+strCondition ;				
				logger.debug("query : " + strSQLQuery);		
				
				 Map hMap=new HashMap();
				 hMap.put("appName", appName);
				 			 
               //3. RowMapper.
				 RowMapper RMapper = new RowMapper() {
				        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				        	AppVO appsVO = new AppVO();			        	
				        	appsVO.setAppID(rs.getString("id"));
				        	appsVO.setAppName(rs.getString("name"));
				        	appsVO.setAppFullName(rs.getString("app_full_name"));
				        	appsVO.setDomain(rs.getString("domain"));
				        	appsVO.setIpAddress(rs.getString("ip_address"));
				        	appsVO.setForgotPasswordURL(rs.getString("forgot_password_url"));
				        	appsVO.setDescription(rs.getString("description"));
				        	appsVO.setPwdTempoaryValidity(rs.getInt("pwd_tempoary_validity"));
				        	appsVO.setPwdValidity(rs.getInt("pwd_validity"));
				        	appsVO.setOtpValidity(rs.getInt("otp_validity"));
				        	appsVO.setAccessTokenValidity(rs.getInt("access_token_validity"));
				        	appsVO.setRefreshTokenValidity(rs.getInt("refresh_token_validity"));
				        	appsVO.setIsOTPVerify(rs.getInt("is_otp_verify"));
				        	appsVO.setSendEmailID(rs.getString("send_email_id"));
				        	return appsVO;
				        }};
				
				
			    appVO = (AppVO) namedParameterJdbcTemplate.queryForObject(strSQLQuery,hMap,RMapper);		
									
			}catch(EmptyResultDataAccessException ex){
			    logger.error("EmptyResultDataAccessException in getAppDetails : "+ex);     
		    }catch(Exception ex){					
		         logger.error("Exception in getAppDetails : "+ex);            
		    }
		   logger.debug("Exit :public AppVO getAppDetails(String appName,String type ");
			return appVO;	
		}

	    
   public int updateSubscriptionToGracePeriod(int orgID){	
  	  int success=0;
  	 
 	  	try
 	  	{
 	  		logger.debug("Entry : public int updateSubscriptionToGracePeriod(int orgID)");
 	  		
 	  		
 	  		String sql="UPDATE org_app_subscription SET expired_date=expired_date+ INTERVAL 1 MONTH,subscription_id=1 WHERE org_id=:orgID;";
 	  				
 	  		Map hashMap=new HashMap();
 	  		hashMap.put("orgID", orgID);
 	  		
 	  		success= namedParameterJdbcTemplate.update(sql, hashMap);
 	     		
 	  	}
 	  	catch(EmptyResultDataAccessException e){
 	  		logger.info("No society Details available"+e);
 	  	}
 	  	catch(NullPointerException Ex){
 	  		logger.info("No society details available : "+Ex);
 	  	}
 	  	catch(Exception Ex){
 	  		logger.error("Exception in public int updateSubscriptionToGracePeriod(int orgID) : "+Ex);
 	  	}
 	  	logger.debug("Exit :  public int updateSubscriptionToGracePeriod(int orgID)");
 	 
     return success;	
     }
   
   public int restrictLoginAccessInGracePeriod(int orgID){	
    	  int success=0;
    	 
   	  	try
   	  	{
   	  		logger.debug("Entry : public int restrictLoginAccessInGracePeriod(int orgID)");
   	  		
   	  		
   	  		String sql="UPDATE um_user_app_role SET role_id=3 WHERE org_id=:orgID AND app_id=2 AND role_id!=1;";
   	  				
   	  		Map hashMap=new HashMap();
   	  		hashMap.put("orgID", orgID);
   	  		
   	  		success= namedParameterJdbcTemplate.update(sql, hashMap);
   	     		
   	  	}
   	  	catch(EmptyResultDataAccessException e){
   	  		logger.info("No society Details available"+e);
   	  	}
   	  	catch(NullPointerException Ex){
   	  		logger.info("No society details available : "+Ex);
   	  	}
   	  	catch(Exception Ex){
   	  		logger.error("Exception in public int restrictLoginAccessInGracePeriod(int orgID) : "+Ex);
   	  	}
   	  	logger.debug("Exit :  public int restrictLoginAccessInGracePeriod(int orgID)");
   	 
       return success;	
       }
   
   
   public int updateSubscriptionOfOrganziationApp(AppVO appVO){	
	  	  int success=0;
	  	 
	 	  	try
	 	  	{
	 	  		logger.debug("Entry : public int updateSubscriptionOfOrganziationApp(AppVO appVO)");
	 	  		
	 	  		
	 	  		String sql="UPDATE org_app_subscription SET expired_date=:subscription,subscription_id=:subscriptionID WHERE org_id=:orgID and app_id=:appID;";
	 	  				
	 	  		Map hashMap=new HashMap();
	 	  		hashMap.put("orgID", appVO.getOrgID());
	 	  		hashMap.put("appID", appVO.getAppID());
	 	  		hashMap.put("subscription", appVO.getAppSubscriptionValidity());
	 	  		hashMap.put("subscriptionID", appVO.getSubscriptionID());
	 	  		
	 	  		success= namedParameterJdbcTemplate.update(sql, hashMap);
	 	     		
	 	  	}
	 	  	catch(EmptyResultDataAccessException e){
	 	  		logger.info("No organization Details available"+e);
	 	  	}
	 	  	catch(NullPointerException Ex){
	 	  		logger.info("No organization details available : "+Ex);
	 	  	}
	 	  	catch(Exception Ex){
	 	  		logger.error("Exception in public int updateSubscriptionToGracePeriod(int orgID) : "+Ex);
	 	  	}
	 	  	logger.debug("Exit :  public int updateSubscriptionOfOrganziationApp(AppVO appVO) ");
	 	 
	     return success;	
	     }
   
   
   public List getOrgAssignAppList(int orgID) {
		String strSQLQuery = "";
		List appsList=null;	

		try {
			logger.debug("Entry : public List getOrgAssignAppList() ");
//			1. SQL Query
			strSQLQuery = "  SELECT am.*, os.expired_date,os.subscription_id FROM  um_app_master am, org_app_subscription os, society_details sd "+
                 " WHERE am.id=os.app_id AND sd.society_id=os.org_id AND sd.working='1'  AND sd.society_id =:orgID ";
			logger.debug("Query : " + strSQLQuery);
			// 2. Parameters.
			Map namedParameters = new HashMap();
			namedParameters.put("orgID", orgID);

			//3. RowMapper.
			RowMapper RMapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					AppVO appsVO = new AppVO();
					appsVO.setAppFullName(rs.getString("app_full_name"));
					appsVO.setAppName(rs.getString("name"));
					appsVO.setAppID(rs.getString("id"));
					appsVO.setDomain(rs.getString("domain"));
					appsVO.setIpAddress(rs.getString("ip_address"));
					appsVO.setForgotPasswordURL(rs.getString("forgot_password_url"));
					appsVO.setLogoUrl(rs.getString("logo_url"));
					appsVO.setAppSubscriptionValidity(rs.getTimestamp("expired_date"));
					appsVO.setSubscriptionID(rs.getInt("subscription_id"));
					appsVO.setDescription(rs.getString("description"));
					appsVO.setPwdTempoaryValidity(rs.getInt("pwd_tempoary_validity"));
		        	appsVO.setPwdValidity(rs.getInt("pwd_validity"));
		        	appsVO.setOtpValidity(rs.getInt("otp_validity"));
		        	appsVO.setAccessTokenValidity(rs.getInt("access_token_validity"));
		        	appsVO.setRefreshTokenValidity(rs.getInt("refresh_token_validity"));
		        	appsVO.setIsOTPVerify(rs.getInt("is_otp_verify"));
		        	appsVO.setSendEmailID(rs.getString("send_email_id"));
					return appsVO;
				}
			};

			appsList=namedParameterJdbcTemplate.query(strSQLQuery, namedParameters, RMapper);

			logger.debug("Exit public List public List getOrgAssignAppList()");

		}catch(EmptyResultDataAccessException Ex) {
			logger.info("No app details are available  ");
		} catch (Exception e) {
			logger.error("Exception in public List getOrgAssignAppList() : "+e);
		}

		return appsList;

	}



	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

}
