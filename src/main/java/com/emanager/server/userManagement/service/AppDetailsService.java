package com.emanager.server.userManagement.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;

import com.emanager.server.userManagement.domainObject.AppDetailsDomain;
import com.emanager.server.userManagement.valueObject.AppVO;

public class AppDetailsService {
	
	public AppDetailsDomain appDetailsDomain;
	private static final Logger logger = Logger.getLogger(AppDetailsService.class);
	
	public List getAppDetailsList() {
		List appsList=null;	
		logger.debug("Entry : public List getAppDetailsList() ");
		try {
		
			appsList=appDetailsDomain.getAppDetailsList();

			logger.debug("Exit public List public List getAppDetailsList()");

		}catch(EmptyResultDataAccessException Ex) {
			logger.info("No app details are available  ");
		} catch (Exception e) {
			logger.error("Exception in public List getAppDetailsList() : "+e);
		}

		return appsList;

	}

	public AppVO getAppDetails(String appName,String type){
		 AppVO appVO = new AppVO();		
		
		logger.debug("Entry : public AppVO getAppDetails(String appName,String type) ");
		try {
		
			appVO=appDetailsDomain.getAppDetails(appName, type);

			logger.debug("Exit public AppVO getAppDetails(String appName,String type)");

		}catch(EmptyResultDataAccessException Ex) {
			logger.info("No app details are available  ");
		} catch (Exception e) {
			logger.error("Exception in public AppVO getAppDetails(String appName,String type) : "+e);
		}

		return appVO;

	}
	    
   public int updateSubscriptionToGracePeriod(int orgID){	
  	  int success=0;
  	 
 	  	try
 	  	{
 	  		logger.debug("Entry : public int updateSubscriptionToGracePeriod(int orgID)");
 	  		
 	  		
 	  		success= appDetailsDomain.updateSubscriptionToGracePeriod(orgID);
 	     		
 	  	}
 	  	catch(EmptyResultDataAccessException e){
 	  		logger.info("No org subscription Details available"+e);
 	  	}
 	  	catch(NullPointerException Ex){
 	  		logger.info("No org subscription Details available : "+Ex);
 	  	}
 	  	catch(Exception Ex){
 	  		logger.error("Exception in public int updateSubscriptionToGracePeriod(int orgID) : "+Ex);
 	  	}
 	  	logger.debug("Exit :  public int updateSubscriptionToGracePeriod(int orgID)");
 	 
     return success;	
     }
   
   public int restrictLoginAccessInGracePeriod(int orgID){	
    	  int success=0;
    	 
   	  	try
   	  	{
   	  		logger.debug("Entry : public int restrictLoginAccessInGracePeriod(int orgID)");
   	  			  		
   	    		success= appDetailsDomain.restrictLoginAccessInGracePeriod(orgID);
   	     		
   	  	}
   	  	catch(EmptyResultDataAccessException e){
   	  		logger.info("No org subscription Details available"+e);
   	  	}
   	  	catch(NullPointerException Ex){
   	  		logger.info("No org subscription Details available : "+Ex);
   	  	}
   	  	catch(Exception Ex){
   	  		logger.error("Exception in public int restrictLoginAccessInGracePeriod(int orgID) : "+Ex);
   	  	}
   	  	logger.debug("Exit :  public int restrictLoginAccessInGracePeriod(int orgID)");
   	 
       return success;	
       }
   
   public int updateSubscriptionOfOrganziationApp(AppVO appVO){	
	  	  int success=0;
	  	 
	 	  	try
	 	  	{
	 	  		logger.debug("Entry : public int updateSubscriptionOfOrganziationApp(AppVO appVO)");
	 	  		
	 	  		
	 	  		success= appDetailsDomain.updateSubscriptionOfOrganziationApp(appVO);
	 	     		
	 	  	}
	 	  	catch(EmptyResultDataAccessException e){
	 	  		logger.info("No org subscription Details available"+e);
	 	  	}
	 	  	catch(NullPointerException Ex){
	 	  		logger.info("No org subscription Details available : "+Ex);
	 	  	}
	 	  	catch(Exception Ex){
	 	  		logger.error("Exception in public int updateSubscriptionOfOrganziationApp : "+Ex);
	 	  	}
	 	  	logger.debug("Exit :  public int updateSubscriptionOfOrganziationApp(AppVO appVO)");
	 	 
	     return success;	
	     }
   
   public List getOrgAssignAppList(int orgID){
 		List appList=null;
 		logger.debug("Entry : public List getOrgAssignAppList(String orgID) ");
 		try{			
 			
 			appList=appDetailsDomain.getOrgAssignAppList(orgID);	
 			
 		}catch(Exception ex){
 		     logger.error("Exception in getOrgAssignAppList : "+ex);            
 	    }
 	   logger.debug("Exit : public List getOrgAssignAppList(String orgID) ");
 		return appList;	
 	}

   


/**
 * @param appDetailsDomain the appDetailsDomain to set
 */
public void setAppDetailsDomain(AppDetailsDomain appDetailsDomain) {
	this.appDetailsDomain = appDetailsDomain;
}
	

}
