package com.emanager.server.userManagement.service;

import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.userManagement.domainObject.UserAuthenticationDomain;
import com.emanager.server.userManagement.domainObject.UserAuthorizationDomain;
import com.emanager.server.userManagement.domainObject.UserDomain;
import com.emanager.server.userManagement.valueObject.UserVO;

public class UserService {
	private static final Logger logger = Logger.getLogger(UserService.class);
	public UserDomain userDomain;
	public UserAuthenticationDomain userAuthenticationDomain;
	public UserAuthorizationDomain userAuthorizationDomain;
	
	public UserVO authenticateUser(UserVO userVO){
		
		try{
			
			userVO=userAuthenticationDomain.authenticateUser(userVO);
			
		}catch(Exception ex){
	         logger.error("Exception in authenticateUser : "+ex);            
	    }
	   logger.debug("Exit : public UserVO authenticateUser(UserVO userVO) ");
		return userVO;	
	}
	
	 public UserVO socialLoginAuthenticateUser(UserVO userVO){
			
			try{
				
				userVO=userAuthenticationDomain.socialLoginAuthenticateUser(userVO);
				
			}catch(Exception ex){
		         logger.error("Exception in socialLoginAuthenticateUser : "+ex);            
		    }
		   logger.debug("Exit : public UserVO socialLoginAuthenticateUser(UserVO userVO) ");
			return userVO;	
		}
	
	public UserVO changeUserOrgnization(UserVO orgVO){
		UserVO userVO =new UserVO();
		logger.debug("Entry : public UserVO changeUserOrgnization((UserVO orgVO) ");
		try{
			
			userVO=userAuthenticationDomain.changeUserOrgnization(orgVO);
			
		}catch(Exception ex){
	         logger.error("Exception in changeUserOrgnization : "+ex);            
	    }
	   logger.debug("Exit : public UserVO changeUserOrgnization((UserVO orgVO) ");
		return userVO;	
	}
	
	public UserVO getUserOrganizationList(String userID, String appID){
		UserVO userVO =new UserVO();
		logger.debug("Entry : public UserVO getUserOrganizationList(String userID, String appID) ");
		try{
			
			userVO=userDomain.getUserOrganizationList(userID, appID);
			
		}catch(Exception ex){
	         logger.error("Exception in getUserOrganizationList : "+ex);            
	    }
	   logger.debug("Exit : public UserVO getUserOrganizationList(String userID, String appID) ");
		return userVO;	
	}
	
	public UserVO changeUserApartment(UserVO orgVO){
		UserVO userVO =new UserVO();
		logger.debug("Entry : public UserVO changeUserApartment((UserVO orgVO) ");
		try{
			
			userVO=userAuthenticationDomain.changeUserApartment(orgVO);
			
		}catch(Exception ex){
	         logger.error("Exception in changeUserApartment : "+ex);            
	    }
	   logger.debug("Exit : public UserVO changeUserApartment((UserVO orgVO) ");
		return userVO;	
	}
	
	public Boolean verifyAccessToken(String accessToken){
		Boolean flag= false;	
		logger.debug("Entry : public Boolean verifyAccessToken(String accessToken) ");
		try{
			
		  flag=userAuthenticationDomain.verifyAccessToken(accessToken);
		
		}catch(Exception ex){
	         logger.error("Exception in verifyAccessToken : "+ex);            
	    }
		logger.debug("Exit : public Boolean verifyAccessToken(String accessToken) ");
		return flag;	
	}
   
	public Boolean verifyRefreshToken(UserVO decrptVO){
		Boolean flag= false;		
		logger.debug("Entry : public Boolean verifyRefreshToken(UserVO decrptVO) ");
		try{
			
		  flag=userAuthenticationDomain.verifyRefreshToken(decrptVO);
		
		}catch(Exception ex){
	        logger.error("Exception in verifyRefreshToken : "+ex);            
	   }
		logger.debug("Exit : public Boolean verifyRefreshToken(UserVO decrptVO) ");
		return flag;	
	}
	
	public String verifyGenerateAccessToken(String expiredAccessToken){
		String newAccessToken=null;		
		logger.debug("Entry : public String verifyGenerateAccessToken(String expiredAccessToken) ");
		try{
			
		  newAccessToken=userAuthenticationDomain.verifyGenerateAccessToken(expiredAccessToken);
		
		}catch(Exception ex){
	        logger.error("Exception in verifyGenerateAccessToken : "+ex);            
	    }
		logger.debug("Exit : public String verifyGenerateAccessToken(String expiredAccessToken) ");
		return newAccessToken;	
	}
	
	
	public UserVO createUser(UserVO userVO){
		logger.debug("Entry : public UserVO createUser(UserVO userVO) ");
		 try{
			 
            userVO=userDomain.createUser(userVO);
            
		  }catch(Exception ex){
			  logger.error("Exception in createUser : "+ex);   			   
		  }
		 logger.debug("Exit : public UserVO createUser(UserVO userVO) ");
		return userVO;
	}
	
     public Boolean updateUser(UserVO userVO){
    	 Boolean isUpdated=false;
    	 logger.debug("Entry : public Boolean updateUser(UserVO userVO) ");
		 try{
			 
			 isUpdated=userDomain.updateUser(userVO);
            
		  }catch(Exception ex){
			  isUpdated=false;
			  logger.error("Exception in updateUser : "+ex);   			   
		  }
		 logger.debug("Exit : public Boolean updateUser(UserVO userVO) ");
		return isUpdated;
	}
    
     public UserVO permitOrgAccess(UserVO orgVO){
    	 UserVO userVO =new UserVO();
    	 logger.debug("Entry : public UserVO getUserDetailsByLoginName(String loginName) ");
		 try{
			 
            userVO=userAuthorizationDomain.permitOrgAccess(orgVO);
            
		  }catch(Exception ex){
			  logger.error("Exception in permitOrgAccess : "+ex);   			   
		  }
		 logger.debug("Exit : public UserVO permitOrgAccess(String loginName) ");
		  		  
  		return userVO;
  	}
     
     public Boolean revokeOrgAccess(String userID, String appID, String orgID){
    	 Boolean isRevoked=false;
    	 logger.debug("Entry :  public Boolean revokeOrgAccess(String userID) ");
		 try{
			 
			 isRevoked=userAuthorizationDomain.revokeOrgAccess(userID, appID, orgID);
            
		  }catch(Exception ex){
			  isRevoked=false;
			  logger.error("Exception in revokeOrgAccess : "+ex);   			   
		  }
		 logger.debug("Exit :  public Boolean revokeOrgAccess(String userID, String appID, String orgID) ");
		return isRevoked;
 	}
     
     
     public Boolean deleteUser(String userID, String updatedBy){
    	 Boolean isDeleted= false;		
 		logger.debug("Entry : public Boolean deleteUser(String userID)");
 		try{
 			
 			isDeleted=userDomain.deleteUser(userID,updatedBy);
 		
 		}catch(Exception ex){
 			isDeleted=false;
 	        logger.error("Exception in deleteUser : "+ex);            
 	   }
 		logger.debug("Exit : public Boolean deleteUser(String userID, String updatedBy) ");
 		return isDeleted;	
  	}
     
     public Boolean updateUserAndriodDeviceID(UserVO userVO){
    	 Boolean isUpdated= false;		
 		logger.debug("Entry : public Boolean updateUserAndriodDeviceID(String userID, String deviceID)");
 		try{
 			
 			isUpdated=userDomain.updateUserAndriodDeviceID(userVO);
 		
 		}catch(Exception ex){
 			isUpdated=false;
 	        logger.error("Exception in updateUserAndriodDeviceID : "+ex);            
 	   }
 		logger.debug("Exit : public Boolean updateUserAndriodDeviceID(String userID, String deviceID) ");
 		return isUpdated;	
  	}
     
     public UserVO getUserDetailsByID(String userID){
    	 UserVO userVO =new UserVO();
    	 logger.debug("Entry : public UserVO getUserDetailsByID(String userID) ");
		 try{
			 
            userVO=userDomain.getUserDetailsByID(userID);
            
		  }catch(Exception ex){
			  logger.error("Exception in getUserDetailsByID : "+ex);   			   
		  }
		 logger.debug("Exit : public UserVO getUserDetailsByID(String userID) ");
		  		  
  		return userVO;
  	}
     
     public UserVO getUser(UserVO userVO){
      	
    	 logger.debug("Entry : public Boolean getUser(UserVO userVO) ");
		 try{
			 String orgID=userVO.getOrgID();
	    	 String appID=userVO.getAppID();
	    	 String userID=userVO.getUserID();
	    	
			 
			 userVO=userDomain.getUserDetailsByID(userID);
			 userVO.setPassword("");
			 UserVO roleVO = userAuthorizationDomain.getUserRole(userID, appID, orgID);
			 userVO.setRoleID(roleVO.getRoleID());
			 userVO.setRoleName(roleVO.getRoleName());
			 userVO.setOrgID(orgID);
			 userVO.setAppID(appID);
            
		  }catch(Exception ex){
			 logger.error("Exception in getUser : "+ex);   			   
		  }
		 logger.debug("Exit : public Boolean getUser(UserVO userVO) ");
		return userVO;
	}
     
     
     public UserVO getUserDetailsByLoginName(String loginName){
    	 UserVO userVO =new UserVO();
    	 logger.debug("Entry : public UserVO getUserDetailsByLoginName(String loginName) ");
		 try{
			 
            userVO=userDomain.getUserDetailsByLoginName(loginName);
            
		  }catch(Exception ex){
			  logger.error("Exception in getUserDetailsByLoginName : "+ex);   			   
		  }
		 logger.debug("Exit : public UserVO getUserDetailsByLoginName(String loginName) ");
		  		  
  		return userVO;
  	}
     
     public List getUserList(String orgID, String appID){
 		List userList=null;
 		logger.debug("Entry : public UserVO getUserList(String orgID, String appID) ");
 		try{			
 			
 			userList=userDomain.getUserList(orgID, appID);	
 			
 		}catch(Exception ex){
 		     logger.error("Exception in getUserList : "+ex);            
 	    }
 	   logger.debug("Exit : public UserVO getUserList(String orgID, String appID) ");
 		return userList;	
 	}
     
     public List getAppwiseUserList(String orgID, String appID){
  		List userList=null;
  		logger.debug("Entry : public UserVO getUserList(String orgID, String appID) ");
  		try{			
  			
  			userList=userDomain.getAppwiseUserList(orgID, appID);	
  			
  		}catch(Exception ex){
  		     logger.error("Exception in getAppwiseUserList : "+ex);            
  	    }
  	   logger.debug("Exit : public UserVO getAppwiseUserList(String orgID, String appID) ");
  		return userList;	
  	}
     
     public List getRoles(String orgID, String appID){
 		List roleList=null;
 		logger.debug("Entry : public List getRoles(String orgID, String appID) ");
 		try{			
 			
 			roleList=userAuthorizationDomain.getRoles(orgID, appID);		
 			
 		}catch(Exception ex){
 		     logger.error("Exception in getRoles : "+ex);            
 	    }
 	   logger.debug("Exit : public List getRoles(String orgID, String appID) ");
 		return roleList;	
 	} 
     
  
     public List getAllPermissions(String appID){
 		List permissionList=null;
 		 logger.debug("Entry : public List getAllPermissions(String appID) ");
 		try{
 			
 			permissionList=userAuthorizationDomain.getAllPermissions(appID);
 			
 		}catch(Exception ex){
 			 logger.error("Exception in getAllPermissions : "+ex);            
 	    }
 		 logger.debug("Entry : public List getAllPermissions(String appID) ");
 		return permissionList;
 	} 
     
     public Boolean createRole(UserVO roleVO){
 		Boolean isCreated=false;
 		logger.debug("Exit : public Boolean createRole(UserVO roleVO) ");
 	    try{
 	    	
 	    	isCreated=userAuthorizationDomain.createRole(roleVO);
 			
 		}catch(Exception ex){
 	         logger.error("Exception in createRole : "+ex);  
 	         isCreated=false;
 	    }
 	    logger.debug("Exit : public Boolean createRole(UserVO roleVO) "+isCreated);
 		return isCreated;		
 	} 
     
     public UserVO getUserRolePermission(String roleID, String appID, String orgID){
    	 UserVO userVO =new UserVO();
    	 logger.debug("Entry : public UserVO getUserRolePermission(String roleID, String appID, String orgID) ");
		 try{
			 
            userVO=userAuthorizationDomain.getUserRolePermission(roleID, appID, orgID);
            
		  }catch(Exception ex){
			  logger.error("Exception in getUserRolePermission : "+ex);   			   
		  }
		 logger.debug("Exit : public UserVO getUserRolePermission(String roleID, String appID, String orgID) ");
		  		  
  		return userVO;
  	} 
 	
 	public Boolean updateRole(UserVO roleVO){
 		Boolean isUpdated=false;
 		logger.debug("Entry : public Boolean updateRole(UserVO roleVO) ");
 	    try{
 	    	
 	    	isUpdated=userAuthorizationDomain.updateRole(roleVO);
 			
 		}catch(Exception ex){
 	         logger.error("Exception in updateRole : "+ex);  
 	         isUpdated=false;
 	    }
 	    logger.debug("Exit : public Boolean updateRole(UserVO roleVO) "+isUpdated);
 		return isUpdated;			
 	}  
 	
	public UserVO deleteRole(UserVO roleVO){
		UserVO deleteVO =new UserVO();
 		logger.debug("Entry : public UserVO deleteRole(UserVO roleVO) ");
 	    try{
 	    	
 	    	deleteVO =userAuthorizationDomain.deleteRole(roleVO);
 			
 		}catch(Exception ex){
 	         logger.error("Exception in deleteRole : "+ex);  
 	        
 	    }
 	    logger.debug("Exit : public UserVO deleteRole(UserVO roleVO) ");
 		return deleteVO;			
 	}  
 	
 	
 	
 	public Boolean changedPassword(String userID, String password, String updatedBy){
 		Boolean isUpdated=false;
 		logger.debug("Entry : public Boolean changedPassword(String userID, String password, String updatedBy) ");
 	    try{
 	    	
 	    	isUpdated=userAuthenticationDomain.changePassword(userID, password, updatedBy);
 			
 		}catch(Exception ex){
 	         logger.error("Exception in changedPassword : "+ex);  
 	         isUpdated=false;
 	    }
 	    logger.debug("Exit : public Boolean changedPassword(String userID, String password, String updatedBy) "+isUpdated);
 		return isUpdated;			
 	}  
 	
 	public UserVO forgotPassword(String email, String appID){
		UserVO userVO =new UserVO();
		logger.debug("Entry : public UserVO forgotPassword(String email, String appID) ");
		try{
			
			userVO=userAuthenticationDomain.forgotPassword(email, appID);
			
		}catch(Exception ex){
	         logger.error("Exception in forgotPassword : "+ex);            
	    }
	   logger.debug("Exit : public UserVO forgotPassword(String email, String appID) ");
		return userVO;	
	}
    
 
	public Boolean resetPassword(String password, String resettoken){
		Boolean isSuccess = false;
       try{	
			isSuccess=userAuthenticationDomain.resetPassword(password, resettoken);
			
		}catch(Exception ex){
			isSuccess=false;
		    logger.error("Exception in resetPassword : "+ex);    	        
		}
		
		return isSuccess;	
	}
	
	
	public UserVO memberAppRegisterUser(UserVO userVO){
		logger.debug("Entry : public UserVO memberAppRegisterUser(UserVO userVO) ");
		 try{
			 
            userVO=userDomain.memberAppRegisterUser(userVO);
            
		  }catch(Exception ex){
			  logger.error("Exception in memberAppRegisterUser : "+ex);   			   
		  }
		 logger.debug("Exit : public UserVO memberAppRegisterUser(UserVO userVO) ");
		return userVO;
	}
	
	public UserVO getUserOrgApartmentList(String userID, String appID){
		UserVO userVO =new UserVO();
		logger.debug("Entry : public UserVO getUserOrgApartmentList(String userID, String appID) ");
		try{
			
			userVO=userDomain.getUserOrgApartmentList(userID, appID);
			
		}catch(Exception ex){
	         logger.error("Exception in getUserOrganizationList : "+ex);            
	    }
	   logger.debug("Exit : public UserVO getUserOrganizationList(String userID, String appID) ");
		return userVO;	
	}
	
	
	public Boolean verifyUserApartment(String userID, int apartmentID, String userType){
		Boolean flag= false;	
		logger.debug("Entry : public Boolean verifyUserApartment(String userID, String apartmentID) ");
		try{
			
		  flag=userAuthenticationDomain.verifyUserApartment(userID, apartmentID,userType);
		
		}catch(Exception ex){
	         logger.error("Exception in verifyUserApartment : "+ex);            
	    }
		logger.debug("Exit : public Boolean verifyUserApartment(String userID, String apartmentID) ");
		return flag;	
	}
	
	//Customer, Vendor and student
	public Boolean verifyProfileDetails(String userID,int profileID){
		Boolean flag= false;	
		logger.debug("Entry : public Boolean verifyProfileDetails(int profileID) "+userID+profileID);
		try{
			
		  flag=userAuthenticationDomain.verifyProfileDetails(userID, profileID);
		
		}catch(Exception ex){
	         logger.error("Exception in verifyProfileDetails : "+ex);            
	    }
		logger.debug("Exit : public Boolean verifyProfileDetails(int profileID) ");
		return flag;	
	}
	//Profile related API
	public UserVO getProfileOrganizationList(String userID, String appID, String orgID,String userType){
		UserVO userVO =new UserVO();
		logger.debug("Entry : public UserVO getUserOrgApartmentList(String userID, String appID) ");
		try{
			
			userVO=userDomain.getProfileOrganizationList(userID, appID,orgID,userType);
			
		}catch(Exception ex){
	         logger.error("Exception in getProfileOrganizationList : "+ex);            
	    }
	   logger.debug("Exit : public UserVO getProfileOrganizationList(String userID, String appID) ");
		return userVO;	
	}
	
	public UserVO changeProfileOrg(UserVO orgVO){
		UserVO userVO =new UserVO();
		logger.debug("Entry : public UserVO changeProfileOrg((UserVO orgVO) ");
		try{
			
			userVO=userAuthenticationDomain.changeProfileOrg(orgVO);
			
		}catch(Exception ex){
	         logger.error("Exception in changeProfileOrg : "+ex);            
	    }
	   logger.debug("Exit : public UserVO changeProfileOrg((UserVO orgVO) ");
		return userVO;	
	}
	
	public Boolean verifyPermission(String roleID, String permission){
		Boolean isValid= false;	
		logger.debug("Entry : public Boolean verifyPermission(String roleID, String permission) ");
		try{
			
			isValid=userAuthenticationDomain.verifyPermission(roleID, permission);
		
		}catch(Exception ex){
	         logger.error("Exception in verifyUserApartment : "+ex);            
	    }
		logger.debug("Exit : public Boolean verifyPermission(String roleID, String permission) ");
		return isValid;	
	}
	
	public UserAuthenticationDomain getUserAuthenticationDomain() {
		return userAuthenticationDomain;
	}

	public UserVO getUserDeviceDetails(String userID,String appID){
		UserVO userDetailsVO =new UserVO();
		logger.debug("Entry : public UserVO getUserDeviceDetails(String userID,String appID)");
		try{
			
			userDetailsVO=userDomain.getUserDeviceDetails(userID, appID);
		
			
		logger.debug("Exit : public UserVO getUserDeviceDetails(String userID,String appID)");	
		}catch(Exception ex){
		     logger.error("Exception in getUserDeviceDetails : "+ex);   
		     
	    }
		return userDetailsVO;	
	}

	public UserVO getUserDeviceDetailsFromMemberID(int memberID,String appID,String userType){
		UserVO userDetailsVO =new UserVO();
		logger.debug("Entry : public UserVO getUserDeviceDetailsFromMemberID(int memberID,String appID)");
		try{
			
			userDetailsVO=userDomain.getUserDeviceDetailsFromMemberID(memberID, appID,userType);
		
			
		logger.debug("Exit : public UserVO getUserDeviceDetailsFromMemberID(int memberID,String appID)");	
		}catch(Exception ex){
		     logger.error("Exception in public UserVO getUserDeviceDetailsFromMemberID(int memberID,String appID) : "+ex);   
		     
	    }
		return userDetailsVO;	
	}
	
	
	public UserVO signUpForTrial(UserVO userVO){

		logger.debug("Entry : public UserVO signUpForTrial(UserVO userVO)");
		
		userVO=userDomain.signUpForTrial(userVO);
		
		logger.debug("Exit : public UserVO signUpForTrial(UserVO userVO)");
		
		return userVO;
	}
	
	public UserVO signUpForTrialForRegisteredUser(UserVO userVO){

		logger.debug("Entry : public UserVO signUpForTrialForRegisteredUser(UserVO userVO)");
		
		userVO=userDomain.signUpForTrialForRegisteredUser(userVO);
		
		logger.debug("Exit : public UserVO signUpForTrialForRegisteredUser(UserVO userVO)");
		
		return userVO;
	}
	
	public Boolean validateOneTimePassword(String userID, String token){
		Boolean isValid=false;
		logger.debug("Entry : public UserVO validateOneTimePassword(String userID, String token) ");
		try{
			
			isValid=userAuthenticationDomain.validateOneTimePassword(userID, token);
			
		}catch(Exception ex){
	         logger.error("Exception in validateOneTimePassword : "+ex);            
	    }
	   logger.debug("Exit : public UserVO validateOneTimePassword(String userID, String token) ");
		return isValid;	
	}
	
	
	public void setUserAuthenticationDomain(
			UserAuthenticationDomain userAuthenticationDomain) {
		this.userAuthenticationDomain = userAuthenticationDomain;
	}

	public UserDomain getUserDomain() {
		return userDomain;
	}

	public void setUserDomain(UserDomain userDomain) {
		this.userDomain = userDomain;
	}

	public UserAuthorizationDomain getUserAuthorizationDomain() {
		return userAuthorizationDomain;
	}

	public void setUserAuthorizationDomain(
			UserAuthorizationDomain userAuthorizationDomain) {
		this.userAuthorizationDomain = userAuthorizationDomain;
	}
}
