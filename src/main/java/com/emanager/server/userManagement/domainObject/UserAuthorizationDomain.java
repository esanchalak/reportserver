package com.emanager.server.userManagement.domainObject;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.xpath.operations.Bool;

import com.emanager.server.userManagement.dataAccessObject.UserDAO;
import com.emanager.server.userManagement.valueObject.PermissionVO;
import com.emanager.server.userManagement.valueObject.UserVO;

public class UserAuthorizationDomain {
	private static final Logger logger = Logger.getLogger(UserAuthorizationDomain.class);
    
	public UserDAO userDAO;
	public UserAuthenticationDomain userAuthenticationDomain;
	
	public List getAllPermissions(String appID){
		List permissionList=null;
		 logger.debug("Entry : public List getAllPermissions(String appID) ");
		try{
			
			permissionList=userDAO.getAllPermissionList(appID);
			
		}catch(Exception ex){
			 logger.error("Exception in getAllPermissions : "+ex);            
	    }
		 logger.debug("Entry : public List getAllPermissions(String appID) ");
		return permissionList;
	} 
	
	public UserVO getUserRole(String userID, String appID, String orgID){
		UserVO roleVO =new UserVO();
		 logger.debug("Entry : public String getUserRole(String userID, String appID) ");
		try{
			roleVO=userDAO.getUserRole(userID, appID, orgID);
			
		}catch(Exception ex){
			roleVO.setLoginSuccess(false);
	         logger.error("Exception in getUserRole : "+ex);            
	    }
	   logger.debug("Exit : public String getUserRole(String userID, String appID) ");
		return roleVO;		
	} 
	
	public Boolean assignRole(UserVO userVO){
		Boolean isAssigned=false;
		int isInsert=0;
		logger.debug("Exit : public Boolean assignRole(UserVO userVO) ");
	    try{
	    	
	    	 isInsert=userDAO.assignRole(userVO);
	    	
	    	if(isInsert==1){
	    		isAssigned=true;
	       	}
			
		}catch(Exception ex){
	         logger.error("Exception in assignRole : "+ex);  
	         isAssigned=false;
	    }
	    logger.debug("Exit : public Boolean assignRole(UserVO userVO) "+isAssigned);
		return isAssigned;	
	} 
	
	public Boolean createRole(UserVO roleVO){
		int roleID=0;
		Boolean isCreated=false;
		logger.debug("Exit : public Boolean assignRole(UserVO roleVO) ");
	    try{
	    	
	    	roleID=userDAO.createRole(roleVO);
	    	if(roleID>0){
		        for(int i=0; i< roleVO.getPermissionList().size();i++){
		        	PermissionVO permsssionVo= roleVO.getPermissionList().get(i);
		        	permsssionVo.setRoleID(roleID);
		        	
		        }
	    	 Boolean isAdded=userDAO.insertRolePermission(roleVO.getPermissionList());
	    	 if(isAdded){
	    		isCreated=true;
	    		
	    	 }
	    		
	    	}
			
		}catch(Exception ex){
	         logger.error("Exception in createRole : "+ex);  
	         isCreated=false;
	    }
	    logger.debug("Exit : public Boolean createRole(UserVO roleVO) "+isCreated);
		return isCreated;		
	} 
	
	public Boolean updateRole(UserVO roleVO){
		Boolean isUpdated=false;
		Boolean isRoleUpdated=false;
		Boolean isPermissionDelete=false;
		Boolean isPermissionAdded=false;
		logger.debug("Entry : public Boolean updateRole(UserVO roleVO) ");
	    try{
	    	
	    	isRoleUpdated=userDAO.updateRole(roleVO);
	    	
	    	if(isRoleUpdated){
	    		
	    		isPermissionDelete= userDAO.deleteRolePermission(roleVO.getRoleID());
	    		
	    		if(isPermissionDelete){			
	    		
	    		  for(int i=0; i< roleVO.getPermissionList().size();i++){
			        	PermissionVO permsssionVo= roleVO.getPermissionList().get(i);
			        	permsssionVo.setRoleID(Integer.parseInt(roleVO.getRoleID()));
			        	
			        }
		    	   isPermissionAdded=userDAO.insertRolePermission(roleVO.getPermissionList());
		    	
	    		
	    		}
	    	}
	    	
	    	if((isRoleUpdated) && (isPermissionDelete) && (isPermissionAdded)){
	    		isUpdated=true;	    		
	    	}else{
	    		 isUpdated=false;
	    	}
			
		}catch(Exception ex){
	         logger.error("Exception in updateRole : "+ex);  
	         isUpdated=false;
	    }
	    logger.debug("Exit : public Boolean updateRole(UserVO roleVO) "+isUpdated);
		return isUpdated;			
	} 
	
	public UserVO deleteRole(UserVO roleVO){
 		Boolean isDeleted=false;
 		Boolean isRolePresent=false;
 		Boolean isPermssionDeleted=false;
 		UserVO deleteVO =new UserVO();
 		logger.debug("Entry : public UserVO deleteRole(UserVO roleVO) ");
 	    try{
 	    	isRolePresent=userDAO.checkRoleForUser(roleVO.getRoleID());
 	    	
 	    	if(!isRolePresent){
 	    		isDeleted=userDAO.deleteRole(roleVO.getRoleID(),roleVO.getAppID());
 	    			    		
 	    		if(isDeleted){ 
 	    			isPermssionDeleted=userDAO.deleteRolePermission(roleVO.getRoleID()); 	    			    			
 	    		}
 	    		
 	    		
 	    		if((isDeleted) && (isPermssionDeleted)){
 	    			deleteVO.setStatusCode(200);	    		
 		    	}else{
 		    		deleteVO.setStatusCode(406);
 		    	}	
 	    			    		
 	    	}else{
 	    		deleteVO.setStatusCode(533);
 	    		
 	    	}
 	    	
 			
 		}catch(Exception ex){
 	         logger.error("Exception in deleteRole : "+ex);  
 	        deleteVO.setStatusCode(406);
 	    }
 	    logger.debug("Exit : public UserVO deleteRole(UserVO roleVO) "+isDeleted);
 		return deleteVO;			
 	}  
	
	
	public List getRoles(String orgID, String appID){
		List roleList=null;
		logger.debug("Entry : public List getRoles(String orgID, String appID) ");
		try{			
			
			roleList=userDAO.getRoles(orgID, appID);		
			
		}catch(Exception ex){
		     logger.error("Exception in getRoles : "+ex);            
	    }
	   logger.debug("Exit : public List getRoles(String orgID, String appID) ");
		return roleList;	
	}
	
	public UserVO permitOrgAccess(UserVO orgVO){
   	 UserVO userVO =new UserVO();
   	 int isInserted =0;
   	 logger.debug("Entry : public UserVO getUserDetailsByLoginName(String loginName) ");
		 try{
			 userVO= userDAO.getUserDetailsByLoginId(orgVO.getLoginName(), "name");
			 
			 if(userVO.getLoginSuccess()){
				 orgVO.setUserID(userVO.getUserID());
				 isInserted=userDAO.assignRole(orgVO);
				 
				 if(isInserted==1){					 
					 userVO.setStatusCode(200);
				 }else if(isInserted==2){						 
					 userVO.setStatusCode(532);
				 }else{
					 userVO.setStatusCode(406);
				 }
			 }
                    
		  }catch(Exception ex){
			  logger.error("Exception in permitOrgAccess : "+ex);   			   
		  }
		 logger.debug("Exit : public UserVO permitOrgAccess(String loginName) ");
		  		  
 		return userVO;
 	}
	
	public Boolean revokeOrgAccess(String userID, String appID, String orgID){
	   	 Boolean isRevoked=false;
	   	 logger.debug("Entry :  public Boolean revokeOrgAccess(String userID, String appID, String orgID) ");
		 try{
				 
			 isRevoked=userDAO.revokeOrgAccess(userID, appID, orgID);
	            
		  }catch(Exception ex){
			  isRevoked=false;
			  logger.error("Exception in revokeOrgAccess : "+ex);   			   
		  }
		 logger.debug("Exit :  public Boolean revokeOrgAccess(String userID, String appID, String orgID) "+isRevoked);
		 return isRevoked;
	 }
	
	//Get user role permission list and sorted all permission list for update permission
	public UserVO getUserRolePermission(String roleID, String appID, String orgID){
		UserVO roleVO =new UserVO();
		List allPermission=new ArrayList();
		List rolePermission=new ArrayList();
		 logger.debug("Entry : public UserVO getUserRolePermission(String roleID, String appID, String orgID) ");
		try{
			//All permission
			 allPermission=userDAO.getAllPermissionList(appID);
			
			//Role permission
			 rolePermission=userDAO.getRolePermissionList(roleID, appID, orgID);
			
			logger.debug("allPermission: "+allPermission.size()+" rolePermission: "+rolePermission.size());
			//Remove Access name from All permssion
			for(int i=0;rolePermission.size()>i;i++){
				PermissionVO rolePermissionVO= (PermissionVO) rolePermission.get(i);
				logger.debug(rolePermissionVO.getAccessID() );
				for(int j=0;allPermission.size()>j;j++){
					PermissionVO permissionVO= (PermissionVO) allPermission.get(j);
					
					if(permissionVO.getAccessID().compareTo(rolePermissionVO.getAccessID())==0){
						logger.debug(permissionVO.getAccessID() +" --> role: "+rolePermissionVO.getAccessID());
			           
			          
			           allPermission.remove(j);
			           j--;
					}
					
				}
				
			}
			logger.debug("After sorted allPermission: "+allPermission.size()+" rolePermission: "+rolePermission.size());
			roleVO.setRoleID(roleID);
			roleVO.setAppID(appID);
			roleVO.setOrgID(orgID);
			if(allPermission.size()>0 && rolePermission.size()>0){
				roleVO.setPermissionList(rolePermission);
				roleVO.setObjectList(allPermission);
				roleVO.setStatusCode(200);
			}else{
				roleVO.setStatusCode(406);
				
			}
			
			
			
		}catch(Exception ex){
			roleVO.setStatusCode(406);
	         logger.error("Exception in getUserRolePermission : "+ex);            
	    }
	   logger.debug("Exit :public UserVO getUserRolePermission(String roleID, String appID, String orgID) "+roleVO.getStatusCode());
		return roleVO;		
	} 
	

	public UserAuthenticationDomain getUserAuthenticationDomain() {
		return userAuthenticationDomain;
	}

	public void setUserAuthenticationDomain(
			UserAuthenticationDomain userAuthenticationDomain) {
		this.userAuthenticationDomain = userAuthenticationDomain;
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	} 
	
}
