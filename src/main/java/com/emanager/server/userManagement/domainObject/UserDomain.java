package com.emanager.server.userManagement.domainObject;

import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.commonUtils.domainObject.CommonUtility;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.invoice.dataAccessObject.ProfileDetailsVO;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.taskAndEventManagement.valueObject.TicketVO;
import com.emanager.server.userManagement.dataAccessObject.UserDAO;
import com.emanager.server.userManagement.valueObject.UserVO;



public class UserDomain {
	private static final Logger logger = Logger.getLogger(UserDomain.class);
	public UserDAO userDAO;
	public UserAuthenticationDomain userAuthenticationDomain;
	public UserAuthorizationDomain userAuthorizationDomain;
	public CommonUtility commonUtility=new CommonUtility();
	public EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
	public ConfigManager configManager =new ConfigManager();
	public SocietyService societyService;
	MemberService memberService;
	public LedgerService ledgerService;
	
	public UserVO createUser(UserVO userVO){
		UserVO userDetailsVO =new UserVO();
		int isInserted=0;
		Boolean isAssigned=false;
		logger.debug("Entry : public UserVO createUser(UserVO userVO) ");
		try{			
			userVO.setPassword(enDeUtility.encrypt(commonUtility.generateOTP()));
			
			isInserted=userDAO.createUser(userVO);	
			userVO.setUserID(String.valueOf(isInserted));
			if(isInserted>2){
				//get user details
				userDetailsVO=userDAO.getUserDetailsByLoginId(userVO.getLoginName(),"name");
				if(userDetailsVO.getLoginSuccess()){
					userVO.setUserID(userDetailsVO.getUserID());
					//assign role to user
					isAssigned=userAuthorizationDomain.assignRole(userVO);	
					userVO.setStatusCode(200);
				}else{					
					userVO.setStatusCode(409);
					
				}
			}else if(isInserted==2){
				userVO.setLoginSuccess(false);
				userVO.setStatusCode(532);
				return userVO;		
			
			}else if(isInserted==0){
				userVO.setLoginSuccess(false);
				userVO.setStatusCode(409);
				return userVO;					
			}
			
			
		}catch(Exception ex){
		     logger.error("Exception in createUser : "+ex);   
		     userVO.setStatusCode(500);
	    }
	   logger.debug("Exit :public UserVO createUser(UserVO userVO) ");
		return userVO;		
	}
	
	public Boolean updateUser(UserVO userVO){
		Boolean isUpdated=false;
		Boolean isRoleUpdated=false;
		Boolean isSuccess=false;
	   logger.debug("Entry :public UserVO updateUser(UserVO userVO) ");
		try{			
			isUpdated=userDAO.updateUser(userVO);
			if(isUpdated){
				isRoleUpdated=userDAO.updateUserRole(userVO);
							
				if(isRoleUpdated){
					isSuccess=true;
				}else{
					isSuccess=false;
				}
			}else{
				isSuccess=false;
			}
		}catch(Exception ex){
		     logger.error("Exception in updateUser : "+ex);   
		     isSuccess=false;
		     
	    }
	   logger.debug("Exit :public UserVO updateUser(UserVO userVO) ");
		return isSuccess;			
	}
	
	 public Boolean deleteUser(String userID, String updatedBy){
    	 Boolean isDeleted= false;		
 		logger.debug("Entry : public Boolean deleteUser(String userID, String updatedBy)");
 		try{
 			
 			isDeleted=userDAO.deleteUser(userID, updatedBy);
 		
 		}catch(Exception ex){
 			isDeleted=false;
 	        logger.error("Exception in deleteUser : "+ex);            
 	   }
 		logger.debug("Exit : public Boolean deleteUser(String userID, String updatedBy) ");
 		return isDeleted;	
  	}
	 
	 public Boolean updateUserAndriodDeviceID(UserVO userVO){
    	 Boolean isUpdated= false;		
 		logger.debug("Entry : public Boolean updateUserAndriodDeviceID(String userID, String deviceID)");
 		try{
 			
 			isUpdated=userDAO.updateUserAndriodDeviceID(userVO);
 		
 		}catch(Exception ex){
 			isUpdated=false;
 	        logger.error("Exception in updateUserAndriodDeviceID : "+ex);            
 	   }
 		logger.debug("Exit : public Boolean updateUserAndriodDeviceID(String userID, String deviceID) ");
 		return isUpdated;	
  	}
	
	public UserVO getUserDetailsByID(String  userID){
		UserVO userDetailsVO =new UserVO();
		try{
			
			userDetailsVO=userDAO.getUserDetailsByLoginId(userID,"id");
		
		}catch(Exception ex){
		     logger.error("Exception in getUserDetails : "+ex);   
		     userDetailsVO.setLoginSuccess(false);
		     userDetailsVO.setStatusCode(500);
	    }
		return userDetailsVO;	
	}
	
	public UserVO getUserDetailsByLoginName(String loginName){
		UserVO userDetailsVO =new UserVO();
		try{
			
			userDetailsVO=userDAO.getUserDetailsByLoginId(loginName,"name");
		
		}catch(Exception ex){
		     logger.error("Exception in getUserDetailsByLoginName : "+ex);   
		     userDetailsVO.setLoginSuccess(false);
		     userDetailsVO.setStatusCode(500);
	    }
		return userDetailsVO;	
	}
	
	public Boolean setResetLoginCounter(String userID,int attemptCount){
		Boolean flag=false;
		try{
			
			flag=userDAO.setResetLoginCounter(userID,attemptCount);
			
		}catch(Exception ex){
	         logger.error("Exception in setResetLoginCounter : "+ex);    
	         flag=false;
	    }
	   logger.debug("Exit : public Boolean setResetLoginCounter(String userID, String resetType) ");
		return flag;	
		
	}
	
	public UserVO getUserOrganizationList(String userID, String appID){
		UserVO userVO =new UserVO();
		List societyList=null;
		try{
			
			societyList=userDAO.getAppUserOrgnizationList(userID, appID);
			userVO.setAppID(appID);
			userVO.setUserID(userID);
			if(societyList.size()>0){
				userVO.setSocietyAppList(societyList);
				userVO.setLoginSuccess(true);
				
			}else{
				userVO.setLoginSuccess(false);
				userVO.setStatusCode(524);
			}
			
			
		}catch(Exception ex){
			userVO.setLoginSuccess(false);
			userVO.setStatusCode(524);
	         logger.error("Exception in getUserOrganizationList : "+ex);            
	    }
	   logger.debug("Exit : public UserVO getUserOrganizationList(String userID, String appID) ");
		return userVO;	
	}
	
	public List getUserList(String orgID, String appID){
		List userList=null;
		logger.debug("Entry : public List getUserList(String orgID, String appID) ");
		try{			
			
			userList=userDAO.getUserList(orgID, appID);		
			
		}catch(Exception ex){
		     logger.error("Exception in getUserList : "+ex);            
	    }
	   logger.debug("Exit : public List getUserList(String orgID, String appID) ");
		return userList;	
	}
	
	 public List getAppwiseUserList(String orgID, String appID){
	  		List userList=null;
	  		logger.debug("Entry : public UserVO getUserList(String orgID, String appID) ");
	  		try{			
	  			
	  			userList=userDAO.getAppwiseUserList(orgID, appID);	
	  			
	  		}catch(Exception ex){
	  		     logger.error("Exception in getAppwiseUserList : "+ex);            
	  	    }
	  	   logger.debug("Exit : public UserVO getAppwiseUserList(String orgID, String appID) ");
	  		return userList;	
	  	}
	
	
	/*
	 * Member App registration
	 * 
	 * */
	
	public UserVO memberAppRegisterUser(UserVO userVO){
		
		List apartmentList=null;
		int isInserted=0;
		Boolean isAptAssigned=false;
		Boolean isRoleAssigned=false;
		int isUserCreated=0;
		TicketVO requestVO =new TicketVO();
		logger.debug("Entry : public UserVO memberAppRegisterUser(UserVO userVO) ");		
		try{
			
			//Check user is registered or not
			UserVO checkUser = userDAO.getUserDetailsByLoginId(userVO.getLoginName(), "name");
			
			//Already registered user
			if(checkUser.getLoginSuccess()){
				logger.info("User email "+userVO.getLoginName()+" is aleady registered.");
				
				//check user already assign apartment
				UserVO checkApartmentVO=this.getUserOrgApartmentList(checkUser.getUserID(), userVO.getAppID());
				if(checkApartmentVO.getLoginSuccess()){
					userVO.setLoginSuccess(false);
					userVO.setStatusCode(536);
					return userVO;
				}else{			
					//Check email is associate with apartment list
					apartmentList = userDAO.getListOfAllOrgInfoWithUserEmail(userVO.getLoginName(), userVO.getAppID());
					 if(apartmentList.size()>0){
						//assign role and apartment to user
						for(int i=0;i<apartmentList.size();i++){
							UserVO apartmentVO =new UserVO();
							apartmentVO =(UserVO) apartmentList.get(i);
							
							apartmentVO.setUserID(checkUser.getUserID());
							apartmentVO.setRoleID(userVO.getRoleID());
							apartmentVO.setUserType("M");
							
							assignApartment(apartmentVO);	
							userAuthorizationDomain.assignRole(apartmentVO);
						}
					  
						 userVO.setLoginSuccess(true);
						 userVO.setStatusCode(539);		
						 return userVO;
					}else{			
						logger.info("Unable to registered user email "+userVO.getLoginName()+" is not associated with any apartment");
						  userVO.setLoginSuccess(false);
						  userVO.setStatusCode(521);
						  return userVO;
					 }
				
				}
				
				
			}else{//Not registered user
				
				//Check email is associate with apartment list
				apartmentList = userDAO.getListOfAllOrgInfoWithUserEmail(userVO.getLoginName(), userVO.getAppID());
				logger.info("User Email: "+userVO.getLoginName()+"  Associated apartment size : "+apartmentList.size());
				if(apartmentList.size()>0){
					UserVO aptVO =(UserVO) apartmentList.get(0);
					userVO.setFullName(aptVO.getFullName());
					userVO.setMobile(aptVO.getMobile());
					userVO.setPassword(enDeUtility.encrypt(commonUtility.generateOTP()));
					userVO.setCreatedBy("15");
					userVO.setUpdatedBy("15");

					isInserted=userDAO.createUser(userVO);	
					
					logger.debug("User is inserted : "+isInserted);
					if(isInserted>2){
						//get user details
					  UserVO userDetailsVO=userDAO.getUserDetailsByLoginId(userVO.getLoginName(),"name");
						if(userDetailsVO.getLoginSuccess()){
													
							//assign role and apartment to user
							for(int i=0;i<apartmentList.size();i++){
								UserVO apartmentVO =new UserVO();
								apartmentVO =(UserVO) apartmentList.get(i);
								
								apartmentVO.setUserID(userDetailsVO.getUserID());
								apartmentVO.setRoleID("7");
								apartmentVO.setUserType("M");
								assignApartment(apartmentVO);	
								userAuthorizationDomain.assignRole(apartmentVO);
							}
							
						/*	//add user to ticket system
							SocietyVO societyVO =societyService.getSocietyDetails(Integer.parseInt(aptVO.getOrgID()));
							requestVO.setName(aptVO.getFullName());
							requestVO.setEmail(userVO.getLoginName());
							requestVO.setOrg_id(societyVO.getOrgID());
							isUserCreated=memberService.createUserInTicketPortal(requestVO);*/
							
							//Send reset password link to user
							UserVO emailVO=userAuthenticationDomain.forgotPassword(userVO.getLoginName(), userVO.getAppID());
							
							userVO.setLoginSuccess(true);
							userVO.setStatusCode(537);
							return userVO;	
							
						}else{			
							userVO.setLoginSuccess(false);
							userVO.setStatusCode(538);							
						}
					}else if(isInserted==2){
						userVO.setLoginSuccess(false);
						userVO.setStatusCode(532);
						return userVO;		
					
					}else if(isInserted==0){
						userVO.setLoginSuccess(false);
						userVO.setStatusCode(409);
						return userVO;					
					}
					 
					 
					 
				}else{			
					logger.info("Unable to registered user email "+userVO.getLoginName()+" is not associated with any apartment");
					  userVO.setLoginSuccess(false);
					  userVO.setStatusCode(521);					 
				 }
				 
			}
			
			
		
		}catch(Exception ex){
			 userVO.setLoginSuccess(false);
			  userVO.setStatusCode(524);
	         logger.error("Exception in memberAppRegisterUser : "+ex);    	        
	    }
		return userVO;	
	}
	
	public Boolean assignApartment(UserVO userVO){
		Boolean isAssigned=false;
		int isInsert=0;
		logger.debug("Exit : public Boolean assignApartment(UserVO userVO) ");
	    try{
	    	
	    	 isInsert=userDAO.assignApartment(userVO);
	    	
	    	if(isInsert==1){
	    		isAssigned=true;
	       	}
			
		}catch(Exception ex){
	         logger.error("Exception in assignApartment : "+ex);  
	         isAssigned=false;
	    }
	    logger.debug("Exit : public Boolean assignApartment(UserVO userVO) "+isAssigned);
		return isAssigned;	
	} 
	
	
	public UserVO getUserOrgApartmentList(String userID, String appID){
		UserVO userVO =new UserVO();
		List societyList=null;
		try{
			
			societyList=userDAO.getAppUserOrgApartmentList(userID, appID);
			userVO.setAppID(appID);
			userVO.setUserID(userID);
			if(societyList.size()>0){
				userVO.setSocietyAppList(societyList);
				userVO.setLoginSuccess(true);
				
			}else{
				userVO.setLoginSuccess(false);
				userVO.setStatusCode(524);
			}
			
			
		}catch(Exception ex){
			userVO.setLoginSuccess(false);
			userVO.setStatusCode(524);
	         logger.error("Exception in getUserOrganizationList : "+ex);            
	    }
	   logger.debug("Exit : public UserVO getUserOrganizationList(String userID, String appID) ");
		return userVO;	
	}
	
	public UserVO getUserDeviceDetails(String userID,String appID){
		UserVO userDetailsVO =new UserVO();
		logger.debug("Entry : public UserVO getUserDeviceDetails(String userID,String appID)");
		try{
			
			userDetailsVO=userDAO.getUserDeviceDetails(userID, appID);
		
			
		logger.debug("Exit : public UserVO getUserDeviceDetails(String userID,String appID)");	
		}catch(Exception ex){
		     logger.error("Exception in getUserDeviceDetails : "+ex);   
		     
	    }
		return userDetailsVO;	
	}
   
	public UserVO getUserDeviceDetailsFromMemberID(int memberID,String appID,String userType){
		UserVO userDetailsVO =new UserVO();
		logger.debug("Entry : public UserVO getUserDeviceDetailsFromMemberID(int memberID,String appID)");
		try{
			
			userDetailsVO=userDAO.getUserDeviceDetailsFromMemberID(memberID, appID,userType);
		
			
		logger.debug("Exit : public UserVO getUserDeviceDetailsFromMemberID(int memberID,String appID)");	
		}catch(Exception ex){
		     logger.error("Exception in public UserVO getUserDeviceDetailsFromMemberID(int memberID,String appID) : "+ex);   
		     
	    }
		return userDetailsVO;	
	}
	
	
	public UserVO getProfileOrganizationList(String userID, String appID,String orgID, String userType){
		UserVO userVO =new UserVO();
		List orgList=null;
		ProfileDetailsVO profileVO=new ProfileDetailsVO();
		try{
		
			orgList=userDAO.getAppUserProfileOrganizationList(userID, appID,orgID,userType);
			userVO.setAppID(appID);
			userVO.setUserID(userID);
			userVO.setUserType(userType);
			
			if(orgList.size()>0){
				userVO.setSocietyAppList(orgList);
				userVO.setLoginSuccess(true);
				
			}else{
				userVO.setLoginSuccess(false);
				userVO.setStatusCode(524);
			}
			
			
		}catch(Exception ex){
			userVO.setLoginSuccess(false);
			userVO.setStatusCode(524);
	         logger.error("Exception in getProfileOrganizationList : "+ex);            
	    }
	   logger.debug("Exit : public UserVO getProfileOrganizationList(String userID, String appID) ");
		return userVO;	
	}
	
	//================Sign Up For Trial======================//
	public UserVO signUpForTrial(UserVO userVO){

		logger.debug("Entry : public UserVO signUpForTrial(UserVO userVO)");
		int userID=0;
		int insertRoleID=0;
		try{
			SocietyVO societyVO=userVO.getSocietyVO();
			UserVO checkUser = userDAO.getUserDetailsByLoginId(userVO.getLoginName(), "name");
			if(!checkUser.getLoginSuccess()){
			
			SocietyVO orgVO=societyService.getOrgDetailsByOrgName(societyVO.getSocietyName());
			if(orgVO.getSocietyID()==0){
			
			userVO.setPassword(enDeUtility.encrypt(commonUtility.generateOTP()));
			userVO.setCreatedBy("15");
			userVO.setUpdatedBy("15");
			userID=userDAO.createUser(userVO);
			
			int orgID=societyService.insertSociety(userVO.getSocietyVO());
			
			int success=userDAO.insertOrganisationValidity(orgID, Integer.parseInt(userVO.getAppID()), 0,1);
			
			List roleList=userDAO.getRoles(""+orgID,userVO.getAppID());
			int roleID=0;
			if(roleList.size()>0){
				for(int i=0;roleList.size()>i;i++){
					UserVO roleVO=(UserVO) roleList.get(i);
					
					if(roleVO.getIsSignUpRole()==1){
						roleID=Integer.parseInt(roleVO.getRoleID());
					}
					
				}
				if((userID>2)&&(orgID>0)&&(roleID>0)){
					userVO.setUserID(""+userID);
					userVO.setOrgID(""+orgID);
					userVO.setRoleID(""+roleID);
					insertRoleID=userDAO.assignRole(userVO);
					
				}
				
				if((insertRoleID>0)&&(userID>2)&&(orgID>0)&&(roleID>0)){
					userVO.setStatusCode(200);
				}
				
			}
			}else userVO.setStatusCode(550);
			}else userVO.setStatusCode(532);
			
			
		}catch(Exception Ex){
			
			logger.error("Exception : public UserVO signUpForTrial(UserVO userVO) "+Ex );
		}
			logger.debug("Exit : public UserVO signUpForTrial(UserVO userVO)");
		
		return userVO;
	}
		
		//================Sign Up For Trial======================//
		public UserVO signUpForTrialForRegisteredUser(UserVO userVO){

			logger.debug("Entry : public UserVO signUpForTrialForRegisteredUser(UserVO userVO)");
			String userID=userVO.getUserID();
			int insertRoleID=0;
			int orgID=0;
			int success=0;
			try{
				SocietyVO societyVO=userVO.getSocietyVO();
				
				SocietyVO orgVO=societyService.getOrgDetailsByOrgName(societyVO.getSocietyName());
				if(orgVO.getSocietyID()==0){
				
				orgID=societyService.insertSociety(userVO.getSocietyVO());
				}else orgID=orgVO.getSocietyID();
				success=userDAO.insertOrganisationValidity(orgID, Integer.parseInt(userVO.getAppID()), 0,1);
				if(success==1){
				List roleList=userDAO.getRoles(""+orgID,userVO.getAppID());
				int roleID=0;
				if(roleList.size()>0){
					for(int i=0;roleList.size()>i;i++){
						UserVO roleVO=(UserVO) roleList.get(i);
						
						if(roleVO.getIsSignUpRole()==1){
							roleID=Integer.parseInt(roleVO.getRoleID());
						}
						
					}
					if((orgID>0)&&(roleID>0)){
						userVO.setUserID(""+userID);
						userVO.setOrgID(""+orgID);
						userVO.setRoleID(""+roleID);
						insertRoleID=userDAO.assignRole(userVO);
						
					}
					
					if((insertRoleID>0)&&(orgID>0)&&(roleID>0)){
						userVO.setStatusCode(200);
					}
					
				}
				}else if(success==2){
					userVO.setStatusCode(551);
				}
				
				
				
			}catch(Exception Ex){
				
				logger.error("Exception : public UserVO signUpForTrialForRegisteredUser(UserVO userVO) "+Ex );
			}
		
		
		logger.debug("Exit : public UserVO signUpForTrialForRegisteredUser(UserVO userVO)");
		
		return userVO;
	}
	
	
	
	public MemberService getMemberService() {
		return memberService;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	public SocietyService getSocietyService() {
		return societyService;
	}

	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public UserAuthenticationDomain getUserAuthenticationDomain() {
		return userAuthenticationDomain;
	}

	public void setUserAuthenticationDomain(
			UserAuthenticationDomain userAuthenticationDomain) {
		this.userAuthenticationDomain = userAuthenticationDomain;
	}

	public UserAuthorizationDomain getUserAuthorizationDomain() {
		return userAuthorizationDomain;
	}

	public void setUserAuthorizationDomain(
			UserAuthorizationDomain userAuthorizationDomain) {
		this.userAuthorizationDomain = userAuthorizationDomain;
	}

	public LedgerService getLedgerService() {
		return ledgerService;
	}

	public void setLedgerService(LedgerService ledgerService) {
		this.ledgerService = ledgerService;
	}




}
