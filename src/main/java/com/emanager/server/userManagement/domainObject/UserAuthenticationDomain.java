package com.emanager.server.userManagement.domainObject;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.spec.EncodedKeySpec;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.jar.JarEntry;

import net.sf.jasperreports.engine.util.JsonUtil;

import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.joda.time.DateTime;

import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.commonUtils.domainObject.CommonUtility;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.commonUtils.domainObject.EncryptDecryptUtility;
import com.emanager.server.commonUtils.domainObject.VMTemplatePreparator;
import com.emanager.server.invoice.dataAccessObject.ProfileDetailsVO;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.taskAndEventManagement.Service.NotificationService;
import com.emanager.server.taskAndEventManagement.valueObject.EmailMessage;
import com.emanager.server.taskAndEventManagement.valueObject.NotificationVO;
import com.emanager.server.taskAndEventManagement.valueObject.SMSMessage;
import com.emanager.server.userLogin.dataaccessObject.AuditActivityDAO;
import com.emanager.server.userManagement.dataAccessObject.UserDAO;
import com.emanager.server.userManagement.service.AppDetailsService;
import com.emanager.server.userManagement.valueObject.AppVO;
import com.emanager.server.userManagement.valueObject.PermissionVO;
import com.emanager.server.userManagement.valueObject.UserVO;


public class UserAuthenticationDomain {
	private static final Logger logger = Logger.getLogger(UserAuthenticationDomain.class);
    
	public UserDAO userDAO;
	public UserDomain userDomain;
	public UserAuthorizationDomain userAuthorizationDomain;
	public EncryptDecryptUtility enDeUtility=new EncryptDecryptUtility();
	public DateUtility dateUtility=new DateUtility();
	public ConfigManager configManager =new ConfigManager();
	public CommonUtility commonUtility=new CommonUtility();
	public VMTemplatePreparator templatePreparator=new VMTemplatePreparator();
	public MemberService memberService;
	public EncryptDecryptUtility endeUtil=new EncryptDecryptUtility();
	public NotificationService notificationService;
	public SocietyService societyService;
	public LedgerService ledgerService;
	public AppDetailsService appDetailsService;
	public AuditActivityDAO auditActivityDAO;
	
	public UserVO authenticateUser(UserVO loginUserVO){
		UserVO userVO =new UserVO();
		SocietyVO societyVO= new SocietyVO();
		PermissionVO permissionVO=new PermissionVO();		
		List permissionList=null;
		String method="";
		
		 logger.debug("Entry : public UserVO authenticateUser(String userName, String password, String appID) "+loginUserVO.getPassword());
		try{
			if(commonUtility.validateEmailID(loginUserVO.getLoginName())){
				 logger.info("Email Validated");
				userVO=userDAO.getUserDetailsByLoginId(loginUserVO.getLoginName(),"name");
				method="email";
				
			}else if(commonUtility.validatePhoneNumber(loginUserVO.getLoginName())){
				logger.info("Mobile Number Validated");
				userVO=userDAO.getUserDetailsByLoginId(loginUserVO.getLoginName(),"mobile");
				method="mobile";
				
			}else if(commonUtility.validateCustomName(loginUserVO.getLoginName())){
				logger.info("Custom Name Validated");
				UserVO customUserVO=userDAO.getUserDetailsByCustomName(loginUserVO.getLoginName(),loginUserVO.getAppID());
				if(!customUserVO.getLoginSuccess()){
					return customUserVO;
				}
				
				userVO=userDAO.getUserDetailsByLoginId(customUserVO.getUserID(),"id");
				userVO.setCustomUserName(customUserVO.getCustomUserName());
				method="Custom";
			}else{
				userVO.setLoginSuccess(false);
				userVO.setStatusCode(521);
				return userVO;		
			}
			
			
			if(!userVO.getLoginSuccess()){
				return userVO;
				
			}else if(userVO.getUserID()!=null){
				if((loginUserVO.getAppID().equalsIgnoreCase("3"))&&(loginUserVO.getAndriodDeviceID()!=null)){
				userVO.setAppID("0");
                }else userVO.setAppID(loginUserVO.getAppID());
				userVO=this.verifyUserloginDetails(userVO, loginUserVO.getPassword(), "login");
                UserVO deviceVO=userDAO.getUserDeviceDetails(userVO.getUserID(), loginUserVO.getAppID());
    	           userVO.setAndriodDeviceID(deviceVO.getAndriodDeviceID());
                if(userVO.getLoginSuccess()){
                	UserVO tokenVO=this.generateRefreshToken(userVO.getUserID(),loginUserVO.getAppID());					   
    			    userVO.setRefreshToken(enDeUtility.encodeBeanToString(tokenVO));
    			    userVO.setIsOTPVerifyRequired(tokenVO.getIsOTPVerifyRequired());
    			    userDAO.insertAuditActivity(userVO.getUserID(),userVO.getAppID(),"login",method);
    			   //Two Step
    			    if((tokenVO.getIsOTPVerifyRequired()==1)&&(tokenVO.getOTP().length()>0)){
    			    	this.sendOTPSmsAndMail(tokenVO.getOTP(), userVO);
    			    }
    			     
                }else{
                	return userVO;                	
                }				
			}else{
				userVO.setLoginSuccess(false);
				userVO.setStatusCode(521);
				return userVO;				
			}

		}catch(Exception ex){
	         logger.error("Exception in authenticateUser : "+ex);    
	         userVO.setLoginSuccess(false);
	         userVO.setStatusCode(500);
	        
	    }
	   logger.debug("Exit : public UserVO authenticateUser(String userName, String password, String appName) ");
		return userVO;	
	}
	
	public UserVO socialLoginAuthenticateUser(UserVO loginUserVO){
		UserVO userVO =new UserVO();
		SocietyVO societyVO= new SocietyVO();
		PermissionVO permissionVO=new PermissionVO();		
		List permissionList=null;
		
		 logger.debug("Entry : public UserVO socialLoginAuthenticateUser(UserVO loginUserVO) ");
		try{
			 logger.info("Social Login Email Validated");
			 userVO=userDAO.getUserDetailsByLoginId(loginUserVO.getLoginName(),"name");
			 
			 if(!userVO.getLoginSuccess()){
					return userVO;
					
				}else if(userVO.getUserID()!=null){
					if((loginUserVO.getAppID().equalsIgnoreCase("3"))&&(loginUserVO.getAndriodDeviceID()!=null)){
					userVO.setAppID("0");
	                }else userVO.setAppID(loginUserVO.getAppID());
					userVO=this.verifyUserloginDetails(userVO, "", "socialLogin");
	                UserVO deviceVO=userDAO.getUserDeviceDetails(userVO.getUserID(), loginUserVO.getAppID());
	    	           userVO.setAndriodDeviceID(deviceVO.getAndriodDeviceID());
	                if(userVO.getLoginSuccess()){
	                	UserVO tokenVO=this.generateRefreshToken(userVO.getUserID(),loginUserVO.getAppID());					   
	    			    userVO.setRefreshToken(enDeUtility.encodeBeanToString(tokenVO));
	    			    userVO.setIsOTPVerifyRequired(tokenVO.getIsOTPVerifyRequired());
	    			    userDAO.insertAuditActivity(userVO.getUserID(),userVO.getAppID(),"login","social");
	    			  //Two Step
	    			    if((tokenVO.getIsOTPVerifyRequired()==1)&&(tokenVO.getOTP().length()>0)){
	    			    	this.sendOTPSmsAndMail(tokenVO.getOTP(), userVO);
	    			    }
	                }else{
	                	return userVO;                	
	                }				
				}else{
					userVO.setLoginSuccess(false);
					userVO.setStatusCode(521);
					return userVO;				
				}
			
		}catch(Exception ex){
	         logger.error("Exception in socialLoginAuthenticateUser : "+ex);    
	         userVO.setLoginSuccess(false);
	         userVO.setStatusCode(500);
	        
	    }
	   logger.debug("Exit : public UserVO socialLoginAuthenticateUser(UserVO loginUserVO) ");
		return userVO;	
	}
	
	
	public UserVO changeUserOrgnization(UserVO orgVO){
		UserVO userVO = new UserVO(); 
		UserVO tokenVO=new UserVO();
		SocietyVO societyVO=new SocietyVO();
		SocietyVO societySettingsVO=new SocietyVO();
		String generatedRefreshToken=null;
		List permissionList=null;
		Boolean isValid=false;
		HashMap<String, String> clientPermisionHashMap=new HashMap<String, String>();
		HashMap<String, String> serverPermisionHashMap=new HashMap<String, String>();
		 logger.debug("Entry : public UserVO changeUserOrgnization(String userID, String orgID, String appID,String roleID) ");
		try{
	      userVO.setUserID(orgVO.getUserID());
	      userVO.setAppID(orgVO.getAppID());
	      userVO.setOrgID(orgVO.getOrgID());
	      userVO.setRoleID(orgVO.getRoleID());
	      
	      logger.info("Org ID: "+orgVO.getOrgID()+" and  Role ID: "+orgVO.getRoleID());
	      
	      //Check subscription of organization
	      societyVO=userDAO.getOrgAppSubscriptionDetails(orgVO.getAppID(),orgVO.getOrgID());
	      if(societyVO.getAppSubscriptionValidity()!=null){
	        isValid=dateUtility.checkTokenDateExpired(societyVO.getAppSubscriptionValidity());
		  }
	      logger.info("Org ID: "+orgVO.getOrgID()+" Subscription is valid: "+isValid);
	      userVO.setAppSubscriptionValidity(societyVO.getAppSubscriptionValidity());
	      if(!isValid){
	    	 userVO.setLoginSuccess(false);
			 userVO.setStatusCode(535);
			 return userVO;
	      }else{
	    	
	       //access token
		  tokenVO=this.generateAccessToken(orgVO.getUserID(),orgVO.getAppID(),orgVO.getOrgID());			  		  
		  userVO.setAccessToken(tokenVO.getAccessToken());
		  userVO.setAccessTokenValidity(tokenVO.getAccessTokenValidity());
		  logger.debug("Access token: "+tokenVO.getAccessToken()+" Validity: "+tokenVO.getAccessTokenValidity());	
		  
		  //refresh token
		  UserVO refreshVO=enDeUtility.decodeStringToBean(orgVO.getRefreshToken());	
		  userVO.setRefreshToken(refreshVO.getRefreshToken());
		  userVO.setRefeshTokenValidity(refreshVO.getRefeshTokenValidity());
		  
		  logger.debug("Refresh token: "+refreshVO.getRefreshToken()+" Validity: "+refreshVO.getRefeshTokenValidity());	
		  
	   	  permissionList=userDAO.getPermissionList(orgVO.getUserID(), orgVO.getRoleID());
	   	  logger.debug("User ID: "+orgVO.getUserID()+" Permission Size: "+permissionList.size());
	   	  if(permissionList.size()>0){
	   	 	//serverPermisionHashMap=commonUtility.createServerPermissionHashMap(permissionList);
	   		//userVO.setPermissionHashMap(serverPermisionHashMap);
	   		clientPermisionHashMap=commonUtility.createClientPermissionHashMap(permissionList);
	   		
			String encryptedAccessToken = enDeUtility.encodeBeanToString(userVO);			
			  userVO.setPermissionHashMap(clientPermisionHashMap);
			  userVO.setAccessToken(encryptedAccessToken);
			  userVO.setRefreshToken("");
			  userVO.setLoginSuccess(true);
			  
			   // society settings vo
			  societySettingsVO=societyService.getSocietyDetails("0",Integer.parseInt(orgVO.getOrgID()));	      
			  societySettingsVO.setIsGracePeriod(societyVO.getIsGracePeriod());			  
			  userVO.setSocietyVO(societySettingsVO); 	      
		     
		  }else{
			  userVO.setLoginSuccess(false);
			  userVO.setStatusCode(524);
		  }
	     	
	     }
	      
		}catch(Exception ex){
			 userVO.setLoginSuccess(false);
			  userVO.setStatusCode(524);
	         logger.error("Exception in changeUserOrgnization : "+ex);    	        
	    }
		return userVO;	
	}

	public Boolean verifyAccessToken(String accessToken){
		Boolean isValid= false;		
		UserVO decrptVO=new UserVO();
		try{
		    decrptVO=enDeUtility.decodeStringToBean(accessToken);	
	       isValid=dateUtility.checkTokenDateExpired(decrptVO.getAccessTokenValidity());	
		}catch(Exception ex){
	         logger.error("Exception in verifyAccessToken : "+ex);    	        
	    }
		return isValid;	
	}
   
	public Boolean verifyRefreshToken(UserVO decrptVO){
		Boolean isValid= false;			
		try{			
			 isValid=dateUtility.checkTokenDateExpired(decrptVO.getRefeshTokenValidity());;
		}catch(Exception ex){
	         logger.error("Exception in verifyRefreshToken : "+ex);    	        
	    }
		return isValid;	
	} 
	
	public UserVO generateAccessToken(String userID,String appID,String orgID ){
		String generatedOtp=null;
		String token=null;
		int validityInseconds=0;
		java.sql.Timestamp expiredDateTime=null;
		UserVO tokenVO=new UserVO();
		try{
			logger.debug("generateAccessToken: User ID: "+userID+" App ID "+appID+ " orgID: "+orgID);
			//validityInseconds=Integer.parseInt(configManager.getPropertiesValue("accessToken.validity"));
			AppVO appVO = appDetailsService.getAppDetails(appID, "id");
			expiredDateTime=dateUtility.AddingHHMMSSToDate(0, 0, appVO.getAccessTokenValidity());
			token=commonUtility.generateOTP();
			
			userDAO.setAccessToken(userID, orgID, appID, token);
			tokenVO.setOrgID(orgID);
			tokenVO.setAppID(appID);
			tokenVO.setUserID(userID);
			tokenVO.setAccessToken(token);
			tokenVO.setAccessTokenValidity(expiredDateTime);
			
		}catch(Exception ex){
	         logger.error("Exception in generateAccessToken : "+ex);    	        
	    }
		logger.debug("User ID: "+userID+" generateAccessToken "+token+ " Token validity: "+expiredDateTime);
		return tokenVO;	
	} 
	
	public UserVO generateRefreshToken(String userID, String appID){
		String generatedOtp=null;
		int validityInseconds=0;
		java.sql.Timestamp expiredDateTime=null;
		String token=null;
		java.sql.Timestamp optExpDateTime=null;
		UserVO tokenVO=new UserVO();
		try{
			logger.debug("generateRefreshToken: User ID: "+userID);
			AppVO appVO = appDetailsService.getAppDetails(appID, "id");
			//validityInseconds=Integer.parseInt(configManager.getPropertiesValue("refreshToken.validity"));
			expiredDateTime=dateUtility.AddingHHMMSSToDate(0, 0, appVO.getRefreshTokenValidity());
			token=commonUtility.generateOTP();
			
			userDAO.setRefreshToken(userID, token);
			tokenVO.setUserID(userID);
			tokenVO.setRefreshToken(token);
			tokenVO.setRefeshTokenValidity(expiredDateTime);
			
			if(appVO.getIsOTPVerify()==1){//2FA verify
				generatedOtp=commonUtility.generateOTPPwd();
				optExpDateTime=dateUtility.AddingHHMMSSToDate(0, 0, appVO.getOtpValidity());
				userDAO.setTwoStepOTP(userID, generatedOtp,optExpDateTime);
				tokenVO.setIsOTPVerifyRequired(1);
				tokenVO.setOTP(generatedOtp);
			}else{
				tokenVO.setIsOTPVerifyRequired(0);
				tokenVO.setOTP("");
			}
			
		
		 }catch(Exception ex){
	         logger.error("Exception in generateRefreshToken : "+ex);    	        
	    }
		
		logger.debug("User ID: "+userID+"generateRefreshToken "+token+ " Token validity: "+expiredDateTime);	
		return tokenVO;	
	} 
	
	
	public String verifyGenerateAccessToken(String expiredAccessToken){			
		UserVO decrptVO=enDeUtility.decodeStringToBean(expiredAccessToken);	
		 List permissionList=null;
		 HashMap<String, String> permisionHashMap=new HashMap<String, String>();;
		 UserVO tokenVO=new UserVO();
		 SocietyVO societyVO= new SocietyVO();
		 Boolean isValid=false;
		 String encryptedAccessToken=null;
		 try{
			 logger.debug("matchAccessToken: User ID: "+decrptVO.getUserID());
			 
			  Boolean flag=userDAO.matchAccessToken(decrptVO.getOrgID(), decrptVO.getAccessToken());
	          if(flag==false){
	        	  encryptedAccessToken="0";
	          }else{
	        	  //verify user details
	        	  UserVO verifyVO=this.verifyUserloginDetails(decrptVO, "0", "expiredToken");
	        	  if(verifyVO.getLoginSuccess()){
	        		  // Org subscription expired date	        	  
	        		  societyVO=userDAO.getOrgAppSubscriptionDetails(verifyVO.getAppID(),verifyVO.getOrgID());
	        	      isValid=dateUtility.checkTokenDateExpired(societyVO.getAppSubscriptionValidity());
	        	      logger.info("App ID: "+verifyVO.getAppID()+" Org ID: "+verifyVO.getOrgID()+" Subscription is valid: "+isValid);
	        	      if(!isValid){
	        	    	  encryptedAccessToken="535";
	        			 return encryptedAccessToken;  
	        	      }
	        		  //Verify role for org
	        		 UserVO roleVO= userDAO.getUserRole(decrptVO.getUserID(),decrptVO.getAppID(),decrptVO.getOrgID());
	        		 if(roleVO.getLoginSuccess()){
	        			  tokenVO=this.generateAccessToken(roleVO.getUserID(),roleVO.getAppID(),roleVO.getOrgID());
				  		  tokenVO.setRoleID(roleVO.getRoleID());
				  		  tokenVO.setRefreshToken(decrptVO.getRefreshToken());
				  		  tokenVO.setRefeshTokenValidity(decrptVO.getRefeshTokenValidity());
				   		
				  		 /*  permissionList=userDAO.getPermissionList(decrptVO.getUserID(), decrptVO.getRoleID());
		        	     
				   		 if(permissionList.size()>0){
				   			 permisionHashMap = commonUtility.createServerPermissionHashMap(permissionList);
				   			 tokenVO.setPermissionHashMap(permisionHashMap);
						  }*/
				   		 
				   		 encryptedAccessToken = enDeUtility.encodeBeanToString(tokenVO);
	        			 
	        			 
	        		 }else{
	        			 encryptedAccessToken="0";
	        		 }
	        		  
	        	  
	        	  }else{
	        		  
	        		  encryptedAccessToken="0";
	        	  }
						  
	   		
			    
			  
	          }
		 }catch(Exception ex){
			 encryptedAccessToken="0";
	         logger.error("Exception in verifyGenerateAccessToken : "+ex);    	        
	    }
		
		return encryptedAccessToken;	
	} 
	
	
	public UserVO verifyUserloginDetails(UserVO userVO, String userPassword, String type ){
		List societyList=null;
		Boolean isValidPWDDate=false;
		
		 try{	
		// check user when token expired
		if(type.equalsIgnoreCase("expiredToken")){
			UserVO loginUserVO=userDAO.getUserDetailsByLoginId(userVO.getUserID(),"id");
			isValidPWDDate=dateUtility.checkTokenDateExpired(loginUserVO.getPasswordValidity());;
			
			if((loginUserVO.getIsLocked()>0 )){
				userVO.setLoginSuccess(false);
				userVO.setStatusCode(523);
				return userVO;
			}else if(loginUserVO.getLoginCounter()>3){
				userVO.setLoginSuccess(false);
				userVO.setStatusCode(541);
				return userVO;
			}else if(!isValidPWDDate){	
				userVO.setStatusCode(528);
				userVO.setLoginSuccess(false);				
				return userVO;
			}else{
				societyList=userDAO.getAppUserOrgnizationList(userVO.getUserID(), userVO.getAppID());
				logger.info("User ID: "+loginUserVO.getUserID()+" Society List Size: "+societyList.size());
				if(societyList.size()==0){				
				  // userVO.setLoginSuccess(false);
				   userVO.setStatusCode(524);
				   
				}else{
					userVO.setLoginSuccess(true);
				}
				return userVO;
			  }
				
		
		// check user when login
		}else if (type.equalsIgnoreCase("login")){
		    Boolean isValidPasswordDate=false;
			String passwordFromDB=enDeUtility.decrypt(userVO.getPassword());	
			logger.debug("passwordFromDB:  "+passwordFromDB+" Enter Password: "+userPassword);
			userVO.setPassword("");
			isValidPasswordDate=dateUtility.checkTokenDateExpired(userVO.getPasswordValidity());;
			
			if((userVO.getIsLocked()>0 )){
				userVO.setLoginSuccess(false);
				userVO.setStatusCode(523);
				return userVO;
			}else if(userVO.getLoginCounter()>3){
				userVO.setLoginSuccess(false);
				userVO.setStatusCode(541);
				return userVO;
			}else if(!isValidPasswordDate){	
				userVO.setStatusCode(528);
				userVO.setLoginSuccess(false);				
				return userVO;
			}else if(!passwordFromDB.equals(userPassword)){	
				userVO.setStatusCode(522);
				userVO.setLoginSuccess(false);
				userDomain.setResetLoginCounter(userVO.getUserID(),userVO.getLoginCounter()+1);
				return userVO;
			
			}else{
				societyList=userDAO.getAppUserOrgnizationList(userVO.getUserID(), userVO.getAppID());
				logger.info("User ID: "+userVO.getUserID()+" Org List Size: "+societyList.size());
				if(societyList.size()==0){				
				 userVO.setLoginSuccess(false);
				  userVO.setStatusCode(524);
				   
				}else{
					userVO.setLoginSuccess(true);
					userDomain.setResetLoginCounter(userVO.getUserID(),0);
				}
				
				return userVO;
			  }
			
		 }	// check user when social login
		    else if (type.equalsIgnoreCase("socialLogin")){				
				
						if((userVO.getIsLocked()>0 )){
							userVO.setLoginSuccess(false);
							userVO.setStatusCode(523);
							return userVO;
						}else if(userVO.getLoginCounter()>3){
							userVO.setLoginSuccess(false);
							userVO.setStatusCode(541);
							return userVO;
						
						}else{
							societyList=userDAO.getAppUserOrgnizationList(userVO.getUserID(), userVO.getAppID());
							logger.info("User ID: "+userVO.getUserID()+" Org List Size: "+societyList.size());
							if(societyList.size()==0){				
							 userVO.setLoginSuccess(false);
							  userVO.setStatusCode(524);
							   
							}else{
								userVO.setLoginSuccess(true);
								userDomain.setResetLoginCounter(userVO.getUserID(),0);
							}
							
							return userVO;
						  }
			
		     }
		
		
		}catch(Exception ex){
			  userVO.setLoginSuccess(false);
	         logger.error("Exception in verifyUserloginDetails : "+ex);    	        
	    }
		return userVO;
		
	}
	
	public Boolean sendOTPSmsAndMail(String otpToken, UserVO userVO){
		Boolean isSuccess = false;
		String resetURL=null;
		NotificationVO notificationVO=new NotificationVO();
		EmailMessage emailVO=new EmailMessage();
		 SMSMessage smsMsg=new SMSMessage();
		List emailList=new ArrayList<EmailMessage>();
		List smsList=new ArrayList<SMSMessage>();
		try{	
						
			AppVO appVO = userDAO.getAppDetails(userVO.getAppID(), "id");
			
  		    logger.info("App ID " +userVO.getAppID() +" User: "+userVO.getFullName()+" One Time Password : "+otpToken);
			 
  			 
			//Prepare email and send
			 emailVO.setDefaultSender(appVO.getAppFullName());
			 emailVO.setFromEmail(appVO.getSendEmailID());
			 
			//Prepare email and send								
			 emailVO.setEmailTO(userVO.getLoginName());
			 emailVO.setSubject("Your One Time Password for Login");

			 String message="";
			 message="<html><head></head>"
                +"<body width='600'>"
                +"<table  align='center' style='min-width:300px;max-width:600px;font-family:OpenSans-Light, Helvetica Neue, Helvetica,Calibri, Arial, sans-serif;line-height:1.6;'> "
			    +"<tr>"
			    +"<td  style='border:1px solid #f0f0f0;' >"
			    
			 	+" <table  align='center' cellpadding='0' cellspacing='0' style='font-family:OpenSans-Light,Calibri, Arial, sans-serif;font-size:14px;'  bgcolor='#FFFFFF'>"
				  +"<tbody>"
					+"<tr>"
					+"<td style='padding-top:5px;padding-left:15px;padding-right:15px;padding-bottom:15px'>"
					+" <p ><strong>Dear "+userVO.getFullName()+",</strong></p>"

					+" <p >One time password is&nbsp; "+otpToken+" to log in to your account. OTP is valid for 20 minutes.</p>"

					+"	<p style='font-size:13px;'>Regards,<br />"
					+"	Team "+appVO.getAppFullName()+"</p>"
					+"</td>"
					+"</tr>"
					+"<tr>"
					+  "<td align='center' style='padding:10px;vertical-align:top;display:block;font-size:11px; font-weight:300; font-family: OpenSans, helvetica, sans-serif;' bgcolor='#F7F7F7'>&nbsp;&copy; 2019. All rights reserved</td>"
				    +"</tr>"
				+"</tbody>"		
				+"</table>"
				
                +"<td>"				
                +"<tr>"
			   +"</table>"
			+"</body>"
			+"</html>"	;
			 
			 emailVO.setMessage(message);	

		     emailList.add(emailVO);
		     notificationVO.setEmailList(emailList);
		     
		     //SMS 
		     smsMsg.setMobileNumber(userVO.getMobile());
		     smsMsg.setMessage(otpToken+" is your one time password to procced on "+appVO.getAppFullName()+" portal. It is valid for 20 minutes.");
		     smsList.add(smsMsg);
		     notificationVO.setSmsList(smsList);
		    
		 	 notificationVO.setSendEmail(true);
		 	 notificationVO.setSendSMS(true);
		 	 notificationVO.setSendPushNotifications(false);
		     notificationVO.setOrgID(35);
		 	 
		 	notificationVO=notificationService.SendNotifications(notificationVO);
			
		}catch(Exception ex){
		    logger.error("Exception in sendOTPSMSAndMail : "+ex);    	        
		}
		return isSuccess;	
	} 
	
	public Boolean sendResetPasswordMail(String resetToken, UserVO userVO){
		Boolean isSuccess = false;
		String resetURL=null;
		NotificationVO notificationVO=new NotificationVO();
		EmailMessage emailVO=new EmailMessage();
		List emailList=new ArrayList<EmailMessage>();
		try{	
						
			AppVO appVO = userDAO.getAppDetails(userVO.getAppID(), "id");
			
			// resetURL=appVO.getForgotPasswordURL()+"/"+URLEncoder.encode(resetToken);
			 
			 logger.info("App ID " +userVO.getAppID() +" User: "+userVO.getFullName()+" Reset Password URL: "+resetToken);
			 
			 emailVO.setDefaultSender(appVO.getAppFullName());
			 emailVO.setFromEmail(appVO.getSendEmailID());
			 
			//Prepare email and send								
			 emailVO.setEmailTO(userVO.getLoginName());
			 emailVO.setSubject("Your Password for Login");
		      
		    // VelocityContext model = new VelocityContext();
		    // model.put("fullName", userVO.getFullName());
		    // model.put("resetURL", resetURL);
		   
		   //  emailVO.setMessage(templatePreparator.createMessage(model, "forgot.vm"));	
			 String message="";
			 message="<html><head></head>"
                +"<body width='600'>"
                +"<table  align='center' style='min-width:300px;max-width:600px;font-family:OpenSans-Light, Helvetica Neue, Helvetica,Calibri, Arial, sans-serif;line-height:1.6;'> "
			    +"<tr>"
			    +"<td  style='border:1px solid #f0f0f0;' >"
			    
			 	+" <table  align='center' cellpadding='0' cellspacing='0' style='font-family:OpenSans-Light,Calibri, Arial, sans-serif;font-size:14px;'  bgcolor='#FFFFFF'>"
				  +"<tbody>"
					+"<tr>"
					+"<td style='padding-top:5px;padding-left:15px;padding-right:15px;padding-bottom:15px'>"
					+" <p ><strong>Dear "+userVO.getFullName()+",</strong></p>"

					+" <p >Use the Temporary password&nbsp; "+resetToken+" to log in to your account then change your password. Temporary password is valid for 2 hours.</p>"

					+"	<p style='font-size:13px;'>Regards,<br />"
					+"	Team "+appVO.getAppFullName()+"</p>"
					+"</td>"
					+"</tr>"
					+"<tr>"
					+  "<td align='center' style='padding:10px;vertical-align:top;display:block;font-size:11px; font-weight:300; font-family: OpenSans, helvetica, sans-serif;' bgcolor='#F7F7F7'>&nbsp;&copy; 2018. All rights reserved</td>"
				    +"</tr>"
				+"</tbody>"		
				+"</table>"
				
                +"<td>"				
                +"<tr>"
			   +"</table>"
			+"</body>"
			+"</html>"	;
			 
			 emailVO.setMessage(message);	

		     emailList.add(emailVO);
		     
		     notificationVO.setEmailList(emailList);
		 	 notificationVO.setSendEmail(true);
		 	 notificationVO.setSendSMS(false);
		 	 notificationVO.setSendPushNotifications(false);
		 	 
		 	notificationVO=notificationService.SendNotifications(notificationVO);
			
		}catch(Exception ex){
		    logger.error("Exception in sendresetPasswordMail : "+ex);    	        
		}
		return isSuccess;	
	} 
	
	/*
	 * Verify if user Exist if yes send an reset password link by mail.
	 * 
	 * */
	
	public UserVO forgotPassword(String email, String appID){
		Boolean isSuccess = false;
		UserVO verifyVO=new UserVO();
		int validityInseconds=0;
		java.sql.Timestamp expiredDateTime=null;
		String token=null;
		Boolean isSent = false;
		
		try{	
			verifyVO=userDAO.getUserDetailsByLoginId(email, "name");
			if(!verifyVO.getLoginSuccess()){
				if(verifyVO.getStatusCode()==521){
					verifyVO.setStatusCode(529);					
				}
				return verifyVO;
			}else if(verifyVO.getIsLocked()>0 ){
				verifyVO.setStatusCode(523);
				return verifyVO;
			}else{
				userDomain.setResetLoginCounter(verifyVO.getUserID(),0);
				verifyVO.setAppID(appID);
			//	validityInseconds=Integer.parseInt(configManager.getPropertiesValue("pwd.resetValidity"));
			//	expiredDateTime=dateUtility.AddingHHMMSSToDate(0, 0, validityInseconds);
				token=commonUtility.generateOTPPwd();
				  isSuccess=userDAO.resetPassword(verifyVO.getUserID(), enDeUtility.encrypt(token), verifyVO.getUserID(), "T");
				/*String resetToken=enDeUtility.encrypt(verifyVO.getUserID()+":"+token+":"+expiredDateTime);
				logger.info("User ID: "+verifyVO.getUserID()+" resetToken: "+resetToken);
				verifyVO.setAppID(appID);
				
				if(resetToken.contains("/")){
					resetToken=resetToken.replaceAll("/", "_");
				}*/
				logger.info("User ID: "+verifyVO.getUserID()+" token: "+token);
				//Sent email
				isSent=this.sendResetPasswordMail(token, verifyVO);				
				
			    verifyVO.setStatusCode(200);
			    
			}
			
		}catch(Exception ex){
			verifyVO.setStatusCode(500);
		    logger.error("Exception in forgotPassword : "+ex);    	        
		}
		return verifyVO;	
	} 
	
			
	/*
	 * Validate the token.
	 * Reset Password.
	 * */
	public Boolean resetPassword(String password, String resettoken){
		Boolean isSuccess = false;
		logger.debug("Entry: public Boolean resetPassword(String password, String resettoken)"+password+" "+resettoken);
       try{	
    	   String decrptResetToken= enDeUtility.decrypt(resettoken);
    	   final String[] values = decrptResetToken.split(":",3);
    	   String userID=values[0];
    	   String token=values[1];
    	   String tokenValidity=values[2];
    	   
    	   if(userID.length()>0 && token.length()>0 && tokenValidity.length()>0){
    		   logger.debug("User ID: "+userID+" resettoken: "+token+" tokenValidity: "+tokenValidity);
    		   Boolean isValid = dateUtility.checkTokenDateExpired(Timestamp.valueOf(tokenValidity));
    		   if(isValid){
    			   
    			   isSuccess=userDAO.resetPassword(userID, enDeUtility.encrypt(password), userID, "P");
    			   logger.debug("User ID: "+userID+" Reset token is valid and reset password succesfully");
    		   }else{
    			   logger.debug("User ID: "+userID+" Reset token is invalid and unable reset password succesfully");
    			   isSuccess=false;
    		   }
    		   
    		   
    	   }else{
    		   logger.debug("User ID: "+userID+" Reset token is invalid and unable reset password succesfully");
    		   isSuccess=false;
    	   }
	
			
			
		}catch(Exception ex){
			isSuccess=false;
		    logger.error("Exception in resetPassword : "+ex);    	        
		}
       logger.debug("Exit: public Boolean resetPassword(String password, String resettoken)"+isSuccess);
		return isSuccess;	
	}
	
	
	/*
	 * Validate the token.
	 * Change Password.
	 * */
	public Boolean changePassword(String userID, String password, String updatedBy){
		Boolean isSuccess = false;
		String validityType="";
        try{	
			if(userID.equalsIgnoreCase(updatedBy)){
				validityType="P";
			}else {
				validityType="T";
			}
        	isSuccess=userDAO.resetPassword(userID, enDeUtility.encrypt(password), updatedBy, validityType);
        	
		}catch(Exception ex){
			isSuccess=false;
		    logger.error("Exception in changePassword : "+ex);    	        
		}
		
		return isSuccess;	
	}
	
	
	/*
	 * Change User Apartment
	 * 
	 * */
	
	public UserVO changeUserApartment(UserVO orgVO){
		UserVO userVO = new UserVO(); 
		UserVO tokenVO=new UserVO();
		SocietyVO societyVO=new SocietyVO();
		String generatedRefreshToken=null;
		List permissionList=null;
		Boolean isValid=false;
		HashMap<String, String> clientPermisionHashMap=new HashMap<String, String>();
		HashMap<String, String> serverPermisionHashMap=new HashMap<String, String>();
		 logger.debug("Entry : public UserVO changeUserApartment(UserVO orgVO) ");
		try{
	      userVO.setUserID(orgVO.getUserID());
	      userVO.setAppID(orgVO.getAppID());
	      userVO.setOrgID(orgVO.getOrgID());
	      userVO.setRoleID(orgVO.getRoleID());	      
	      
	      logger.info("Org ID: "+orgVO.getOrgID()+" and  Role ID: "+orgVO.getRoleID()+" Apartment ID: "+orgVO.getApartmentID());
	      
	      //Check subscription of organization
	      societyVO=userDAO.getOrgAppSubscriptionDetails(orgVO.getAppID(),orgVO.getOrgID());
	      if(societyVO.getAppSubscriptionValidity()!=null){
	      isValid=dateUtility.checkTokenDateExpired(societyVO.getAppSubscriptionValidity());
	      }
	      logger.info("Org ID: "+orgVO.getOrgID()+" Subscription is valid: "+isValid);
	      userVO.setAppSubscriptionValidity(societyVO.getAppSubscriptionValidity());
	      if(!isValid){
	    	 userVO.setLoginSuccess(false);
			 userVO.setStatusCode(535);
			 return userVO;
	      }else{     	      
	     
	      //access token
		  tokenVO=this.generateAccessToken(orgVO.getUserID(),orgVO.getAppID(),orgVO.getOrgID());			  		  
		  userVO.setAccessToken(tokenVO.getAccessToken());
		  userVO.setAccessTokenValidity(tokenVO.getAccessTokenValidity());
		  logger.debug("Access token: "+tokenVO.getAccessToken()+" Validity: "+tokenVO.getAccessTokenValidity());	
		  
		  //refresh token
		  UserVO refreshVO=enDeUtility.decodeStringToBean(orgVO.getRefreshToken());	
		  userVO.setRefreshToken(refreshVO.getRefreshToken());
		  userVO.setRefeshTokenValidity(refreshVO.getRefeshTokenValidity());
		  
		  logger.debug("Refresh token: "+refreshVO.getRefreshToken()+" Validity: "+refreshVO.getRefeshTokenValidity());	
		  
	   	  permissionList=userDAO.getPermissionList(orgVO.getUserID(), orgVO.getRoleID());
	   	  logger.debug("User ID: "+orgVO.getUserID()+" Permission Size: "+permissionList.size());
	   	  if(permissionList.size()>0){
	   	 	//serverPermisionHashMap=commonUtility.createServerPermissionHashMap(permissionList);
	   		//userVO.setPermissionHashMap(serverPermisionHashMap);
	   		clientPermisionHashMap=commonUtility.createClientPermissionHashMap(permissionList);
	   		
	   		//get token
			String encryptedAccessToken = enDeUtility.encodeBeanToString(userVO);
			  userVO.setPermissionHashMap(clientPermisionHashMap);
			  userVO.setAccessToken(encryptedAccessToken);
			  userVO.setRefreshToken("");
			  userVO.setApartmentID(orgVO.getApartmentID());
			  
			  //Get Member info
			  MemberVO memberVO=memberService.getMemberInfo(orgVO.getApartmentID());
			  userVO.setSecretKey(endeUtil.encrypt(orgVO.getOrgID()+","+memberVO.getLedgerID()+","+orgVO.getApartmentID()+",M"));
			  userVO.setLedgerID(memberVO.getLedgerID());
			  userVO.setMemberID(memberVO.getMemberID());
			  
			  userVO.setLoginSuccess(true);
		  }else{
			  userVO.setLoginSuccess(false);
			  userVO.setStatusCode(524);
		  }
	     	
	     }
	      
		}catch(Exception ex){
			 userVO.setLoginSuccess(false);
			  userVO.setStatusCode(524);
	         logger.error("Exception in changeUserApartment : "+ex);    	        
	    }
		return userVO;	
	}
	
	
	
	public Boolean verifyUserApartment(String userID, int apartmentID,String userType){
		Boolean flag= false;	
		logger.debug("Entry : public Boolean verifyUserApartment(String userID, String apartmentID) "+userID+apartmentID);
		try{
			
		  flag=userDAO.verifyUserApartment(userID, apartmentID,userType);
		
		}catch(Exception ex){
	         logger.error("Exception in verifyUserApartment : "+ex);            
	    }
		logger.debug("Exit : public Boolean verifyUserApartment(String userID, String apartmentID) ");
		return flag;	
	}
	
	
	public Boolean verifyPermission(String roleID, String permission){
		Boolean isValid= false;	
		logger.debug("Entry : public Boolean verifyPermission(String roleID, String permission) ");
		try{
			
			isValid=userDAO.verifyPermission(roleID, permission);
		
		}catch(Exception ex){
	         logger.error("Exception in verifyPermission : "+ex);            
	    }
		logger.debug("Exit : public Boolean verifyPermission(String roleID, String permission) ");
		return isValid;	
	}
	
	
	public Boolean verifyProfileDetails(String userID,int profileID){
		Boolean flag= false;	
		ProfileDetailsVO profileVO=new ProfileDetailsVO();
		logger.debug("Entry : public Boolean verifyProfileDetails(int profileID) "+userID+profileID);
		try{
			
			profileVO=ledgerService.getLedgerProfileDetails(profileID);
			
			if(profileVO.getProfileID()>0){
				logger.debug("UserID: "+userID+" ProfileID: "+ profileID+" Type: "+profileVO.getProfileType());
				flag=userDAO.verifyUserApartment(userID, profileID,profileVO.getProfileType());
			}
			
		
		}catch(Exception ex){
	         logger.error("Exception in verifyProfileDetails : "+ex);            
	    }
		logger.debug("Exit : public Boolean verifyProfileDetails(int profileID) ");
		return flag;	
	}
	
	
	/*
	 * Change User Profile Org C,S,V
	 * 
	 * */
	
	public UserVO changeProfileOrg(UserVO orgVO){
		UserVO userVO = new UserVO(); 
		UserVO tokenVO=new UserVO();
		SocietyVO societyVO=new SocietyVO();
		String generatedRefreshToken=null;
		List permissionList=null;
		Boolean isValid=false;
		HashMap<String, String> clientPermisionHashMap=new HashMap<String, String>();
		HashMap<String, String> serverPermisionHashMap=new HashMap<String, String>();
		 logger.debug("Entry : public UserVO changeProfileOrg(UserVO orgVO) ");
		try{
	      userVO.setUserID(orgVO.getUserID());
	      userVO.setAppID(orgVO.getAppID());
	      userVO.setOrgID(orgVO.getOrgID());
	      userVO.setRoleID(orgVO.getRoleID());	      
	     
	      logger.info("Org ID: "+orgVO.getOrgID()+" and  Role ID: "+orgVO.getRoleID()+" Profile ID: "+orgVO.getApartmentID());
	      
	      //Check subscription of organization
	      societyVO=userDAO.getOrgAppSubscriptionDetails(orgVO.getAppID(),orgVO.getOrgID());
	      if(societyVO.getAppSubscriptionValidity()!=null){
	      isValid=dateUtility.checkTokenDateExpired(societyVO.getAppSubscriptionValidity());
	      }
	      logger.info("Org ID: "+orgVO.getOrgID()+" Subscription is valid: "+isValid);
	      userVO.setAppSubscriptionValidity(societyVO.getAppSubscriptionValidity());
	      if(!isValid){
	    	 userVO.setLoginSuccess(false);
			 userVO.setStatusCode(535);
			 return userVO;
	      }else{     	      
	     
	      //access token
		  tokenVO=this.generateAccessToken(orgVO.getUserID(),orgVO.getAppID(),orgVO.getOrgID());			  		  
		  userVO.setAccessToken(tokenVO.getAccessToken());
		  userVO.setAccessTokenValidity(tokenVO.getAccessTokenValidity());
		  logger.debug("Access token: "+tokenVO.getAccessToken()+" Validity: "+tokenVO.getAccessTokenValidity());	
		  
		  //refresh token
		  UserVO refreshVO=enDeUtility.decodeStringToBean(orgVO.getRefreshToken());	
		  userVO.setRefreshToken(refreshVO.getRefreshToken());
		  userVO.setRefeshTokenValidity(refreshVO.getRefeshTokenValidity());
		  
		  logger.debug("Refresh token: "+refreshVO.getRefreshToken()+" Validity: "+refreshVO.getRefeshTokenValidity());	
		  
	   	  permissionList=userDAO.getPermissionList(orgVO.getUserID(), orgVO.getRoleID());
	   	  logger.debug("User ID: "+orgVO.getUserID()+" Permission Size: "+permissionList.size());
	   	  if(permissionList.size()>0){
	   	 	//serverPermisionHashMap=commonUtility.createServerPermissionHashMap(permissionList);
	   		//userVO.setPermissionHashMap(serverPermisionHashMap);
	   		clientPermisionHashMap=commonUtility.createClientPermissionHashMap(permissionList);
	   		
	   		//get token
			String encryptedAccessToken = enDeUtility.encodeBeanToString(userVO);
			  userVO.setPermissionHashMap(clientPermisionHashMap);
			  userVO.setAccessToken(encryptedAccessToken);
			  userVO.setRefreshToken(orgVO.getRefreshToken());
			  userVO.setApartmentID(orgVO.getApartmentID());
			  
			  //Get Profile info
			  ProfileDetailsVO profileDetailsVO=ledgerService.getLedgerProfileDetails(orgVO.getApartmentID());
			  userVO.setSecretKey(endeUtil.encrypt(orgVO.getOrgID()+","+profileDetailsVO.getProfileLedgerID()+","+profileDetailsVO.getProfileID()+","+profileDetailsVO.getProfileType()));
			  userVO.setLedgerID(profileDetailsVO.getProfileLedgerID());
			  
			  
					  
			  userVO.setLoginSuccess(true);
		  }else{
			  userVO.setLoginSuccess(false);
			  userVO.setStatusCode(524);
		  }
	     	
	     }
	      
		}catch(Exception ex){
			 userVO.setLoginSuccess(false);
			  userVO.setStatusCode(524);
	         logger.error("Exception in changeProfileOrg : "+ex);    	        
	    }
		return userVO;	
	}
	
	public Boolean validateOneTimePassword(String userID, String otpToken){
		Boolean isValid=false;
		logger.debug("Entry : public UserVO validateOneTimePassword(String userID, String token) ");
		try{
			
			isValid=userDAO.matchOTPToken(userID, otpToken);
			
		}catch(Exception ex){
	         logger.error("Exception in validateOneTimePassword : "+ex);            
	    }
	   logger.debug("Exit : public Boolean validateOneTimePassword(String userID, String token) ");
		return isValid;	
	}

	public SocietyService getSocietyService() {
		return societyService;
	}

	public void setSocietyService(SocietyService societyService) {
		this.societyService = societyService;
	}

	public MemberService getMemberService() {
		return memberService;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	public NotificationService getNotificationService() {
		return notificationService;
	}

	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
	}

	
	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public UserAuthorizationDomain getUserAuthorizationDomain() {
		return userAuthorizationDomain;
	}

	public void setUserAuthorizationDomain(
			UserAuthorizationDomain userAuthorizationDomain) {
		this.userAuthorizationDomain = userAuthorizationDomain;
	}

	public UserDomain getUserDomain() {
		return userDomain;
	}

	public void setUserDomain(UserDomain userDomain) {
		this.userDomain = userDomain;
	}

	public LedgerService getLedgerService() {
		return ledgerService;
	}

	public void setLedgerService(LedgerService ledgerService) {
		this.ledgerService = ledgerService;
	}

	public AppDetailsService getAppDetailsService() {
		return appDetailsService;
	}

	public void setAppDetailsService(AppDetailsService appDetailsService) {
		this.appDetailsService = appDetailsService;
	}

	/**
	 * @param auditActivityDAO the auditActivityDAO to set
	 */
	public void setAuditActivityDAO(AuditActivityDAO auditActivityDAO) {
		this.auditActivityDAO = auditActivityDAO;
	}
	
	
}
