package com.emanager.server.userManagement.valueObject;

import java.sql.Timestamp;
import java.util.List;

import com.emanager.server.society.valueObject.SocietyVO;

public class AppVO {
   
	private String appID;
	private String appName;
	private String domain;
	private String ipAddress;
	private String forgotPasswordURL;
	private String logoUrl;
	private int orgID;
	private String expiredDate;
	private int subscriptionID;
	private List<AppVO> orgAppList;
	private Timestamp appSubscriptionValidity;
	private String description;
	private int pwdTempoaryValidity; //in hours
	private int pwdValidity; //in hours
	private int otpValidity; //in seconds
	private int isOTPVerify; 
	private int accessTokenValidity; //in hours
	private int refreshTokenValidity; //in hours
	private String appFullName;
	private String sendEmailID;
	
	public String getAppID() {
		return appID;
	}
	public void setAppID(String appID) {
		this.appID = appID;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getForgotPasswordURL() {
		return forgotPasswordURL;
	}
	public void setForgotPasswordURL(String forgotPasswordURL) {
		this.forgotPasswordURL = forgotPasswordURL;
	}
	/**
	 * @return the logoUrl
	 */
	public String getLogoUrl() {
		return logoUrl;
	}
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @return the expiredDate
	 */
	public String getExpiredDate() {
		return expiredDate;
	}
	
	public int getSubscriptionID() {
		return subscriptionID;
	}
	public void setSubscriptionID(int subscriptionID) {
		this.subscriptionID = subscriptionID;
	}
	/**
	 * @param logoUrl the logoUrl to set
	 */
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	/**
	 * @param expiredDate the expiredDate to set
	 */
	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}
	
	public List<AppVO> getOrgAppList() {
		return orgAppList;
	}
	public void setOrgAppList(List<AppVO> orgAppList) {
		this.orgAppList = orgAppList;
	}
	public Timestamp getAppSubscriptionValidity() {
		return appSubscriptionValidity;
	}
	public void setAppSubscriptionValidity(Timestamp appSubscriptionValidity) {
		this.appSubscriptionValidity = appSubscriptionValidity;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getPwdTempoaryValidity() {
		return pwdTempoaryValidity;
	}
	public void setPwdTempoaryValidity(int pwdTempoaryValidity) {
		this.pwdTempoaryValidity = pwdTempoaryValidity;
	}
	public int getPwdValidity() {
		return pwdValidity;
	}
	public void setPwdValidity(int pwdValidity) {
		this.pwdValidity = pwdValidity;
	}
	public int getOtpValidity() {
		return otpValidity;
	}
	public void setOtpValidity(int otpValidity) {
		this.otpValidity = otpValidity;
	}
	public int getIsOTPVerify() {
		return isOTPVerify;
	}
	public void setIsOTPVerify(int isOTPVerify) {
		this.isOTPVerify = isOTPVerify;
	}
	public int getAccessTokenValidity() {
		return accessTokenValidity;
	}
	public void setAccessTokenValidity(int accessTokenValidity) {
		this.accessTokenValidity = accessTokenValidity;
	}
	public int getRefreshTokenValidity() {
		return refreshTokenValidity;
	}
	public void setRefreshTokenValidity(int refreshTokenValidity) {
		this.refreshTokenValidity = refreshTokenValidity;
	}
	public String getAppFullName() {
		return appFullName;
	}
	public void setAppFullName(String appFullName) {
		this.appFullName = appFullName;
	}
	public String getSendEmailID() {
		return sendEmailID;
	}
	public void setSendEmailID(String sendEmailID) {
		this.sendEmailID = sendEmailID;
	}
}
