package com.emanager.server.userManagement.valueObject;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

import com.emanager.server.society.valueObject.SocietyVO;



public class UserVO {
	
	private String userID;
	private String loginName;
	private String password;
	private String fullName;
	private String orgID;
	private String roleID;
	private String roleName;
	private String appID;
	private String appName;
	private String accessToken;
	private String refreshToken;
	private Integer isLocked;
	private Integer loginCounter;
	private String OTP;
	private Timestamp accessTokenValidity;
	private Timestamp refeshTokenValidity;
	private Timestamp otpValidity;
	private String createDate;
	private String updateDate;
	private String lastLogin;
	private List<SocietyVO> societyAppList;
	private List<PermissionVO> permissionList;
	private Boolean loginSuccess;
	private Integer statusCode;
	private Timestamp passwordValidity;
	private List objectList;
	private String createdBy;
	private String updatedBy;
	private String createrName;
	private String updaterName;
	private Integer isSystemUser;
	private Integer isSystemRole;
	private Integer isDeleted;
	private Timestamp appSubscriptionValidity;
	private int apartmentID;
	private int ledgerID;
	private int memberID;
	private String secretKey;
	private String requestURL;
	private String message;
	private SocietyVO societyVO;
	private Timestamp subscriptionValidity;
	private String andriodDeviceID;
	private String deviceModel;
	private String devicePlatform;
	private String deviceVersion;
	private String mobile;
	private String customUserName;
	private String userType; //M:member, C:Customer, V:Vendor
	private int profileID;
	private String passwordType; //T:tempoary, P:Permanent
	private int isSignUpRole;
	private int isOTPVerifyRequired;//1:required
	
	public SocietyVO getSocietyVO() {
		return societyVO;
	}
	public void setSocietyVO(SocietyVO societyVO) {
		this.societyVO = societyVO;
	}
	public String getRequestURL() {
		return requestURL;
	}
	public void setRequestURL(String requestURL) {
		this.requestURL = requestURL;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getLedgerID() {
		return ledgerID;
	}
	public void setLedgerID(int ledgerID) {
		this.ledgerID = ledgerID;
	}
	public int getMemberID() {
		return memberID;
	}
	public void setMemberID(int memberID) {
		this.memberID = memberID;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public int getApartmentID() {
		return apartmentID;
	}
	public void setApartmentID(int apartmentID) {
		this.apartmentID = apartmentID;
	}
	public Timestamp getAppSubscriptionValidity() {
		return appSubscriptionValidity;
	}
	public void setAppSubscriptionValidity(Timestamp appSubscriptionValidity) {
		this.appSubscriptionValidity = appSubscriptionValidity;
	}
	public Integer getIsSystemRole() {
		return isSystemRole;
	}
	public void setIsSystemRole(Integer isSystemRole) {
		this.isSystemRole = isSystemRole;
	}
	public Timestamp getOtpValidity() {
		return otpValidity;
	}
	public void setOtpValidity(Timestamp otpValidity) {
		this.otpValidity = otpValidity;
	}
	public List getObjectList() {
		return objectList;
	}
	public void setObjectList(List objectList) {
		this.objectList = objectList;
	}
	public String getCreaterName() {
		return createrName;
	}
	public void setCreaterName(String createrName) {
		this.createrName = createrName;
	}
	public String getUpdaterName() {
		return updaterName;
	}
	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName;
	}
	public Integer getIsSystemUser() {
		return isSystemUser;
	}
	public void setIsSystemUser(Integer isSystemUser) {
		this.isSystemUser = isSystemUser;
	}
	public Integer getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Timestamp getPasswordValidity() {
		return passwordValidity;
	}
	public void setPasswordValidity(Timestamp passwordValidity) {
		this.passwordValidity = passwordValidity;
	}
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	private HashMap<String, String> permissionHashMap;
	
	
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	public String getAppID() {
		return appID;
	}
	public void setAppID(String appID) {
		this.appID = appID;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Integer getIsLocked() {
		return isLocked;
	}
	public void setIsLocked(Integer isLocked) {
		this.isLocked = isLocked;
	}
	public String getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}
	public Integer getLoginCounter() {
		return loginCounter;
	}
	public void setLoginCounter(Integer loginCounter) {
		this.loginCounter = loginCounter;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public Boolean getLoginSuccess() {
		return loginSuccess;
	}
	public void setLoginSuccess(Boolean loginSuccess) {
		this.loginSuccess = loginSuccess;
	}
	public String getOTP() {
		return OTP;
	}
	public void setOTP(String otp) {
		OTP = otp;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<PermissionVO> getPermissionList() {
		return permissionList;
	}
	public void setPermissionList(List<PermissionVO> permissionList) {
		this.permissionList = permissionList;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	public String getRoleID() {
		return roleID;
	}
	public void setRoleID(String roleID) {
		this.roleID = roleID;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	
	public List<SocietyVO> getSocietyAppList() {
		return societyAppList;
	}
	public void setSocietyAppList(List<SocietyVO> societyAppList) {
		this.societyAppList = societyAppList;
	}
	public String getOrgID() {
		return orgID;
	}
	public void setOrgID(String orgID) {
		this.orgID = orgID;
	}
	public HashMap<String, String> getPermissionHashMap() {
		return permissionHashMap;
	}
	public void setPermissionHashMap(HashMap<String, String> permissionHashMap) {
		this.permissionHashMap = permissionHashMap;
	}
  
	public Timestamp getAccessTokenValidity() {
		return accessTokenValidity;
	}
	public void setAccessTokenValidity(Timestamp accessTokenValidity) {
		this.accessTokenValidity = accessTokenValidity;
	}
	public Timestamp getRefeshTokenValidity() {
		return refeshTokenValidity;
	}
	public void setRefeshTokenValidity(Timestamp refeshTokenValidity) {
		this.refeshTokenValidity = refeshTokenValidity;
	}
	public Timestamp getSubscriptionValidity() {
		return subscriptionValidity;
	}
	public void setSubscriptionValidity(Timestamp subscriptionValidity) {
		this.subscriptionValidity = subscriptionValidity;
	}
	public String getAndriodDeviceID() {
		return andriodDeviceID;
	}
	public void setAndriodDeviceID(String andriodDeviceID) {
		this.andriodDeviceID = andriodDeviceID;
	}
	/**
	 * @return the deviceModel
	 */
	public String getDeviceModel() {
		return deviceModel;
	}
	/**
	 * @return the devicePlatform
	 */
	public String getDevicePlatform() {
		return devicePlatform;
	}
	/**
	 * @return the deviceVersion
	 */
	public String getDeviceVersion() {
		return deviceVersion;
	}
	/**
	 * @param deviceModel the deviceModel to set
	 */
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}
	/**
	 * @param devicePlatform the devicePlatform to set
	 */
	public void setDevicePlatform(String devicePlatform) {
		this.devicePlatform = devicePlatform;
	}
	/**
	 * @param deviceVersion the deviceVersion to set
	 */
	public void setDeviceVersion(String deviceVersion) {
		this.deviceVersion = deviceVersion;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getCustomUserName() {
		return customUserName;
	}
	public void setCustomUserName(String customUserName) {
		this.customUserName = customUserName;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public int getProfileID() {
		return profileID;
	}
	public void setProfileID(int profileID) {
		this.profileID = profileID;
	}
	public String getPasswordType() {
		return passwordType;
	}
	public void setPasswordType(String passwordType) {
		this.passwordType = passwordType;
	}
	/**
	 * @return the isSignUpRole
	 */
	public int getIsSignUpRole() {
		return isSignUpRole;
	}
	/**
	 * @param isSignUpRole the isSignUpRole to set
	 */
	public void setIsSignUpRole(int isSignUpRole) {
		this.isSignUpRole = isSignUpRole;
	}
	public int getIsOTPVerifyRequired() {
		return isOTPVerifyRequired;
	}
	public void setIsOTPVerifyRequired(int isOTPVerifyRequired) {
		this.isOTPVerifyRequired = isOTPVerifyRequired;
	} 
	
	
	
}
