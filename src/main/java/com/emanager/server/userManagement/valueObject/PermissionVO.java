package com.emanager.server.userManagement.valueObject;

public class PermissionVO {
	
	
	private Integer accessID;
	private String accessName;
	private String serverAccessMethod;
	private Integer accessFlag;
	private Integer roleID;
	private Integer isSystemPermission;
	private String description;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getIsSystemPermission() {
		return isSystemPermission;
	}
	public void setIsSystemPermission(Integer isSystemPermission) {
		this.isSystemPermission = isSystemPermission;
	}
	public Integer getRoleID() {
		return roleID;
	}
	public void setRoleID(Integer roleID) {
		this.roleID = roleID;
	}
	public String getServerAccessMethod() {
		return serverAccessMethod;
	}
	public void setServerAccessMethod(String serverAccessMethod) {
		this.serverAccessMethod = serverAccessMethod;
	}
	public Integer getAccessFlag() {
		return accessFlag;
	}
	public void setAccessFlag(Integer accessFlag) {
		this.accessFlag = accessFlag;
	}	
	
	public String getAccessName() {
		return accessName;
	}
	public void setAccessName(String accessName) {
		this.accessName = accessName;
	}
	public Integer getAccessID() {
		return accessID;
	}
	public void setAccessID(Integer accessID) {
		this.accessID = accessID;
	}
	
}
