package com.emanager.server.accountsAutomation.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.emanager.server.accounts.DataAccessObjects.BankStatement;
import com.emanager.server.accounts.DataAccessObjects.OnlinePaymentGatewayVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accountsAutomation.domain.AccountsAutoDomain;
import com.emanager.server.invoice.dataAccessObject.InvoiceBillingCycleVO;


public class AccountsAutoService {
	
	Logger logger=Logger.getLogger(AccountsAutoService.class);
	AccountsAutoDomain accountsAutoDomain;
	
	public List getRawStatement(String bankAccNo,String fromDate, String toDate){
		logger.debug("Entry : public List getRawStatement(String bankAccNo,String fromDate, String toDate) ");
		List rawStatementList=new ArrayList();
		
		rawStatementList=accountsAutoDomain.getRawStatement(bankAccNo, fromDate, toDate);
		
		
		logger.debug("Exit : public List getRawStatement(String bankAccNo,String fromDate, String toDate) ");
		return rawStatementList;
	}
	
	public List getRawStatementFromLedgerID(int bankLedgerID,String fromDate, String toDate,int orgID){
		logger.debug("Entry : public List getRawStatementFromLedgerID(String bankAccNo,String fromDate, String toDate) ");
		List rawStatementList=new ArrayList();
		
		rawStatementList=accountsAutoDomain.getRawStatementFromLedgerID(bankLedgerID, fromDate, toDate,orgID);
		
		
		logger.debug("Exit : public List getRawStatementFromLedgerID(String bankAccNo,String fromDate, String toDate) ");
		return rawStatementList;
	}
	
	//-----------Used to add one by one transactions from bank statement--------------//
	public TransactionVO addSingleTransaction(TransactionVO txVO){
		logger.debug("Entry :public int addSingleTransaction(TransactionVO txVO) ");
		
		txVO=accountsAutoDomain.addSingleTransaction(txVO);
			
		logger.debug("Exit : public int addSingleTransaction(TransactionVO txVO) ");
		return txVO;
	}
	
	//-----------Used to add one by one transactions from bank statement--------------//
		public TransactionVO addSinglePayUTransaction(TransactionVO txVO){
			logger.debug("Entry :public int addSinglePayUTransaction(TransactionVO txVO) ");
			
			txVO=accountsAutoDomain.addSinglePayUTransaction(txVO);
				
			logger.debug("Exit : public int addSinglePayUTransaction(TransactionVO txVO) ");
			return txVO;
		}
		
		//-----------Used to add one by one transactions from bankx statement--------------//
		public TransactionVO addSingleBankxTransaction(TransactionVO txVO){
		logger.debug("Entry :public int addSingleBankxTransaction(TransactionVO txVO) ");
							
			txVO=accountsAutoDomain.addSingleBankxTransaction(txVO);
								
		logger.debug("Exit : public int addSingleBankxTransaction(TransactionVO txVO) ");			
		return txVO;
		}	
	
	//------Used for adding multiple transactions from bank Statement-----------//
	public int addStatementToDatabase(BankStatement bankStatementVO){
		logger.debug("Entry :public int addStatementToDatabase(LBankStatement bankStatementVO) ");
		int successCount=0;
		
		successCount=accountsAutoDomain.addStatementToDatabase(bankStatementVO);
		
		
		logger.debug("Exit : public int addStatementToDatabase(BankStatement bankStatementVO) ");
		return successCount;
	}
	
	public int reconcileTheBankStatement(BankStatement bankStatementVO){
		logger.debug("Entry : public int reconcileTheBankStatement(BankStatement bankStatementVO)");
		int successCount=0;
		
		successCount=accountsAutoDomain.reconcileTheBankStatement(bankStatementVO);
		
		
		logger.debug("Exit :public int reconcileTheBankStatement(BankStatement bankStatementVO) ");
		return successCount;
	}
	
	//------------------PayUMoney Statement API-----------------//
	public List getPayUStatement(int orgID,String fromDate, String toDate){
		logger.debug("Entry : public List getPayUStatement(int orgID,String fromDate, String toDate) ");
		List rawStatementList=new ArrayList();
		
		rawStatementList=accountsAutoDomain.getPayUStatement(orgID, fromDate, toDate);
		
		
		logger.debug("Exit : public List getPayUStatement(int orgID,String fromDate, String toDate)");
		return rawStatementList;
	}
	
	public int processPayUStatement(){
		logger.debug("Entry : public List processPayUStatement() ");
		int insertFlag=0;
		
		insertFlag=accountsAutoDomain.processPayUStatement();
		
		
		logger.debug("Exit : public List processPayUStatement()");
		return insertFlag;
	}

	public List getPayUStatementForSummary(String fromDate, String toDate){
		logger.debug("Entry : public List getPayUStatement(int orgID,String fromDate, String toDate)");
		List rawStatementList=new ArrayList();
		
		try{
					
		rawStatementList=accountsAutoDomain.getPayUStatementForSummary(fromDate,toDate);
		
			
		}catch(Exception e){
			logger.error("Exception :  public List getPayUStatement(int orgID,String fromDate, String toDate)");
		}
		
		
		
		
		logger.debug("Exit : public List getPayUStatement(int orgID,String fromDate, String toDate)");
		return rawStatementList;
	}
	
	
	public List getPayUStatementNotReconciled(){
		logger.debug("Entry : public List getPayUStatementNotReconciled()");
		List rawStatementList=new ArrayList();
		
		try{
		
			rawStatementList=accountsAutoDomain.getPayUStatementNotReconciled();
		
			
		}catch(Exception e){
			logger.error("Exception :  public List getPayUStatementNotReconciled()");
		}
		logger.debug("Exit : public List getPayUStatementNotReconciled()");
		return rawStatementList;		
	}
	
	/* Add PayUMoney statements after parsing    */
	public int addPayUStatements(OnlinePaymentGatewayVO payUVO){
		int insertFlag=0;
		logger.debug("Entry : public int addPayUStatements(OnlinePaymentGatewayVO payUVO)");
		try{			  	
			
			insertFlag=accountsAutoDomain.addPayUStatements(payUVO);
			
													
		} catch (Exception e) {
		logger.error("Exception at addPayUStatements(OnlinePaymentGatewayVO payUVO) "+e);
		}
		
		logger.debug("Exit : public int addPayUStatements(OnlinePaymentGatewayVO payUVO)"+insertFlag);
		return insertFlag;		
	}
	
	public int generateBillingCycles(){
		int success=0;
		try {
		
		logger.debug("Entry : public void generateBillingCycles()");
		List societyList = getActiveSocietyList();
		for (int i = 0; i < societyList.size(); i++) {
			
				InvoiceBillingCycleVO billingVO = (InvoiceBillingCycleVO) societyList.get(i);
				success=accountsAutoDomain.generateBillingCycle(billingVO);
				
				
				
		}
			}catch (Exception e) {
				logger.error("Exception in generateBillingCycles method : "+e);
			}
		
		logger.debug("Exit : public void generateBillingCycles()");
		return success;
	}
	
	/* Get list of active society list with billing cycle details */
	public List getActiveSocietyList(){
		List societyList=null;
		try{
		logger.debug("Entry : public List getActiveSocietyList()");
		
		societyList=accountsAutoDomain.getActiveSocietyList();
		
		
		
		logger.debug("Exit : public List getActiveSocietyList()");
		}catch (Exception e) {
			logger.debug("Exception : public List getActiveSocietyList()");
		}
		return societyList;		
	}
	
	public List getTransactionDetails(int societyID,String fromDate,String uptoDate){
		List transcationList=null;
		logger.debug("Entry : public List getTransactionDetails(int societyID,String fromDate,String uptoDate)"+fromDate+uptoDate );
		
		try {
		
				 transcationList=accountsAutoDomain.getTransactionDetails(societyID, fromDate, uptoDate);
							 
			
		}	 catch (Exception es) {
			logger.error("Exception at getTransactionDetails "+es);
		}
			 
		logger.debug("Exit : public List getTransactionDetails(int societyID,String fromDate,String uptoDate)");
		return transcationList;		
		}
	
	public List getConfiguredBankStatementList(){
		logger.debug("Entry : public List getRawStatementFromLedgerID() ");
		List rawStatementList=new ArrayList();
		
		rawStatementList=accountsAutoDomain.getConfiguredBankStatementList();
		
		
		logger.debug("Exit : public List getConfiguredBankStatementList() ");
		return rawStatementList;
	}
	
	public List getPackageDetailsList(int orgID){
		logger.debug("Entry : List getPackageDetailsList(int orgID) ");
		List packageList=new ArrayList();
		
		packageList=accountsAutoDomain.getPackageDetailsList(orgID);
		
		
		logger.debug("Exit : List getPackageDetailsList(int orgID) ");
		return packageList;
	}
	
	public  List getApplicableCharges(int orgID,int packageID,int checkDate){
		logger.debug("Entry : public List getApplicableCharges(int orgID,int packageID,int checkDate) ");
		List packageChargeList=new ArrayList();
		
		packageChargeList=accountsAutoDomain.getApplicableCharges(orgID, packageID,checkDate);
		
		
		logger.debug("Exit : public List getApplicableCharges(int orgID,int packageID) ");
		return packageChargeList;
	}
	
	public  List getProfileDetailsListForPackages(int orgID,int packageID){
		logger.debug("Entry : public List getProfileDetailsListForPackages(int orgID,int packageID) ");
		List packageProfileList=new ArrayList();
		
		packageProfileList=accountsAutoDomain.getProfileDetailsListForPackages(orgID, packageID);
		
		
		logger.debug("Exit : public List getProfileDetailsListForPackages(int orgID,int packageID) ");
		return packageProfileList;
	}
	
	
	//====== Generate Recurring Invoices for Organizations API ========//
	public List generateScheduledInvoicesForOrg(int societyID,int billingCycleID){
		List invoiceList=new ArrayList();
		
		try {
			logger.debug("Entry : public List generateScheduledInvoices(int societyID,int billingCycleID)");
			
			invoiceList=accountsAutoDomain.generateScheduledInvoicesForOrg(societyID, billingCycleID);
			
			
			logger.debug("Exit : public List generateScheduledInvoices(int societyID,int billingCycleID)");
		} catch (Exception e) {
			logger.error("Exception :public List generateScheduledInvoices(int societyID,int billingCycleID)"+e );
		}
		return invoiceList;
		
	}
	
	
	/**
	 * @param accountsAutoDomain the accountsAutoDomain to set
	 */
	public void setAccountsAutoDomain(AccountsAutoDomain accountsAutoDomain) {
		this.accountsAutoDomain = accountsAutoDomain;
	}
	
	
	
	

}
