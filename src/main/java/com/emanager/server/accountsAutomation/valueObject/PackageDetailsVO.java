package com.emanager.server.accountsAutomation.valueObject;

public class PackageDetailsVO {

	private int packageID;
	private String packageName;
	private String description;
	private int orgID;
	private int billingCycleID;
	private int masterBillingCycleID;
	private int profileID;
	
	/**
	 * @return the packageID
	 */
	public int getPackageID() {
		return packageID;
	}
	/**
	 * @return the packageName
	 */
	public String getPackageName() {
		return packageName;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @return the orgID
	 */
	public int getOrgID() {
		return orgID;
	}
	/**
	 * @return the billingCycleID
	 */
	public int getBillingCycleID() {
		return billingCycleID;
	}
	/**
	 * @return the profileID
	 */
	public int getProfileID() {
		return profileID;
	}
	/**
	 * @param packageID the packageID to set
	 */
	public void setPackageID(int packageID) {
		this.packageID = packageID;
	}
	/**
	 * @param packageName the packageName to set
	 */
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
	/**
	 * @param billingCycleID the billingCycleID to set
	 */
	public void setBillingCycleID(int billingCycleID) {
		this.billingCycleID = billingCycleID;
	}
	/**
	 * @param profileID the profileID to set
	 */
	public void setProfileID(int profileID) {
		this.profileID = profileID;
	}
	/**
	 * @return the masterBillingCycleID
	 */
	public int getMasterBillingCycleID() {
		return masterBillingCycleID;
	}
	/**
	 * @param masterBillingCycleID the masterBillingCycleID to set
	 */
	public void setMasterBillingCycleID(int masterBillingCycleID) {
		this.masterBillingCycleID = masterBillingCycleID;
	}
	
	
}
