package com.emanager.server.accountsAutomation.domain;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.RowMapper;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.BankStatement;
import com.emanager.server.accounts.DataAccessObjects.ChargesVO;
import com.emanager.server.accounts.DataAccessObjects.LedgerEntries;
import com.emanager.server.accounts.DataAccessObjects.OnlinePaymentGatewayVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionMetaVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accounts.Service.LedgerService;
import com.emanager.server.accounts.Service.TransactionService;
import com.emanager.server.accountsAutomation.dataAccessObject.AccountsAutoDAO;
import com.emanager.server.accountsAutomation.valueObject.PackageDetailsVO;
import com.emanager.server.adminReports.Services.PrintReportService;
import com.emanager.server.adminReports.valueObject.PrintReportVO;
import com.emanager.server.commonUtils.domainObject.CommonUtility;
import com.emanager.server.commonUtils.domainObject.ConfigManager;
import com.emanager.server.commonUtils.domainObject.DateUtility;
import com.emanager.server.invoice.Services.InvoiceService;
import com.emanager.server.invoice.dataAccessObject.InvoiceBillingCycleVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceVO;
import com.emanager.server.invoice.dataAccessObject.ItemDetailsVO;
import com.emanager.server.invoice.dataAccessObject.ProfileDetailsVO;
import com.emanager.server.society.services.MemberService;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.MemberVO;
import com.emanager.server.society.valueObject.SocietyVO;
import com.emanager.server.taskAndEventManagement.Service.NotificationService;
import com.emanager.server.taskAndEventManagement.valueObject.EmailMessage;
import com.emanager.server.taskAndEventManagement.valueObject.NotificationVO;
import com.emanager.webservice.APIResponseVO;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;




public class AccountsAutoDomain {

	Logger logger=Logger.getLogger(AccountsAutoDomain.class);
	AccountsAutoDAO accountsAutoDAO;
	TransactionService transactionService;
	InvoiceService invoiceService;
	SocietyService societyServiceBean;
	LedgerService ledgerService;
	PrintReportService printReportService;
	MemberService memberServiceBeen;
	NotificationService notificationService;
	DateUtility dateUtil=new DateUtility();
	
	
	public List getRawStatement(String bankAccNo,String fromDate, String toDate){
		logger.debug("Entry : public List getRawStatement(String bankAccNo,String fromDate, String toDate) ");
		List rawStatementList=new ArrayList();
		
		rawStatementList=accountsAutoDAO.getRawStatement(bankAccNo, fromDate, toDate);
		
		
		logger.debug("Exit : public List getRawStatement(String bankAccNo,String fromDate, String toDate) ");
		return rawStatementList;
	}
	
	
	public List getRawStatementFromLedgerID(int bankLedgerID,String fromDate, String toDate,int orgID){
		logger.debug("Entry : public List getRawStatementFromLedgerID(String bankAccNo,String fromDate, String toDate) ");
		List rawStatementList=new ArrayList();
		
		rawStatementList=accountsAutoDAO.getRawStatementFromLedgerID(bankLedgerID, fromDate, toDate,orgID);
		
		
		logger.debug("Exit : public List getRawStatementFromLedgerID(String bankAccNo,String fromDate, String toDate) ");
		return rawStatementList;
	}
	
	//-----------Used to add one by one transactions from bank statement--------------//
	public TransactionVO addSingleTransaction(TransactionVO txVO){
		logger.debug("Entry :public int addSingleTransaction(TransactionVO txVO) ");
		int successCount=0;
				BankStatement bankStatementVO=new BankStatement();
				bankStatementVO.setID(txVO.getBankID());
				bankStatementVO.setOrgID(txVO.getSocietyID());
				txVO=transactionService.addTransaction(txVO, txVO.getSocietyID());
				if(txVO.getStatusCode()==200){
					bankStatementVO.setTxID(txVO.getTransactionID());
					int s = accountsAutoDAO.markMatchingTransactionInStatement(bankStatementVO);
					successCount=successCount+1;
				
			}
			
		logger.debug("Exit : public int addSingleTransaction(TransactionVO txVO) ");
		return txVO;
	}
	
	//-----------Used to add one by one transactions from PayU statement--------------//
		public TransactionVO addSinglePayUTransaction(TransactionVO txVO){
			logger.debug("Entry :public int addSinglePayUTransaction(TransactionVO txVO) ");
			int successCount=0;
					
					txVO=transactionService.addTransaction(txVO, txVO.getSocietyID());
					
				
			logger.debug("Exit : public int addSinglePayUTransaction(TransactionVO txVO) ");
			return txVO;
		}
		
	
		//-----------Used to add one by one transactions from bankx statement--------------//		
				public TransactionVO addSingleBankxTransaction(TransactionVO txVO){
				logger.debug("Entry :public int addSingleBankxTransaction(TransactionVO txVO) ");
				
									
				txVO=transactionService.addTransaction(txVO, txVO.getSocietyID());
				if(txVO.getTransactionType().equalsIgnoreCase("Receipt")){
				sendReceipt(txVO);
				}					
								
				logger.debug("Exit : public int addSingleBankxTransaction(TransactionVO txVO) ");
				return txVO;
				}
				
				private APIResponseVO sendReceipt(TransactionVO txVO){
					TransactionVO printTxVO=new TransactionVO();
					APIResponseVO apiResponse=new APIResponseVO();
					PrintReportVO printVO=new PrintReportVO();
					try {
						HashMap<String, Object> param= new HashMap<String, Object>();
						MemberVO memberV=memberServiceBeen.getMemberInfoThroughLedgerID(txVO.getSocietyID(),""+txVO.getLedgerID());
						NotificationVO notificationVO=new NotificationVO();
						
						param.put("ledgerID", ""+txVO.getLedgerID());
						param.put("message", "<p><strong>Hi "+memberV.getFlatNo()+" "+memberV.getFullName()+",</strong></p><p>Please find the soft copy of the receipt attached for your reference. Thank you.</p><p><strong>Regards,<br />Team MyAkountant</strong></p>");
						param.put("orgID", ""+txVO.getSocietyID());
						param.put("receiptID", ""+txVO.getTransactionID());
						param.put("societyName", memberV.getFlatNo()+" "+memberV.getFullName());
						param.put("subject", "Receipt Number-"+txVO.getTransactionNumber());
						param.put("emailTo", memberV.getEmail());
						
						EmailMessage emailVO=new EmailMessage();
						List emailList=new ArrayList<EmailMessage>();
						ConfigManager confMgnr=new ConfigManager();
						logger.debug("Entry : public @ResponseBody PrintReportVO sendEmailMemberReceipt(@RequestBody PrintReportVO printVO) ");
						try {

						
						
						printTxVO.setPrintParam(param);
						printTxVO.setSourceType("BEAN");
						printTxVO.setReportContentType("pdf");

						printVO=printReportService.printMemberReceipt(printTxVO);
						printVO.setBeanList(null);
						//Write file to other server
						  Channel channel;
						try {
						 String user = confMgnr.getPropertiesValue("pushServer.user");
						 String pass = confMgnr.getPropertiesValue("pushServer.password");
						 String hostConn = confMgnr.getPropertiesValue("pushServer.IP");
						 int portNo = Integer.valueOf(confMgnr.getPropertiesValue("pushServer.port"));
						 
						 JSch jsch = new JSch();
						  com.jcraft.jsch.Session sessions = null;
						try {
						sessions = jsch.getSession(user, hostConn, portNo);

						  sessions.setPassword(pass);
						  sessions.setConfig("StrictHostKeyChecking", "no");
						  System.out.println("Establishing Connection...");
						  sessions.connect();
						} catch (Exception e1) {
						// TODO Auto-generated catch block
						logger.error("Unable to establish connection to server");
						}
						 
						channel = sessions.openChannel("sftp");

						          channel.connect();
						          ChannelSftp sftspChannel = (ChannelSftp) channel;
						         
						          sftspChannel.cd(confMgnr.getPropertiesValue("pushServer.directory"));
						          File localFile = new File(printVO.getReportURL());
						         
						          sftspChannel.put(localFile.getAbsolutePath(),localFile.getName());
						}catch(Exception e){
						logger.error("Exception e "+e);
						}
						
						 //Fill email object
						String ledgerID =(String) param.get("ledgerID");
						String emailId =(String) param.get("emailTo");
						String subject =(String) param.get("subject");
						String message =(String) param.get("message");

						logger.info("Email  "+emailId+" Subject: "+subject+" message "+message );        
						emailVO.setDefaultSender(printVO.getSocietyVO().getSocietyName());
						emailVO.setEmailTO(emailId);
						emailVO.setSubject(subject);

						File file= new File(printVO.getReportURL());
						String attachemnetURL=confMgnr.getPropertiesValue("pushServer.URL")+file.getName();

						message=message+" <a href="+attachemnetURL+"><img src='https://www.esanchalak.com/member/img/attachement.png' style='width:20px;height:20px;' /> Receipt.pdf </a>";
						logger.info("Receipt pdf URL: "+attachemnetURL);
						emailVO.setMessage(message);

						emailList.add(emailVO);
						   
						notificationVO.setEmailList(emailList);
						notificationVO.setSendEmail(true);
						notificationVO.setSendSMS(false);
						notificationVO.setSendPushNotifications(false);

						notificationVO=notificationService.SendNotifications(notificationVO);

						if(notificationVO.getSentEmailsCount()==0){
						logger.info("Unable to send email ledgerID: "+ledgerID);
						apiResponse.setStatusCode(400);
						apiResponse.setMessage("Unable to send email receipt.");
						
						}else{
						apiResponse.setStatusCode(200);
						apiResponse.setMessage("Email has been sent successfully.");
						logger.info("Attachemnt Email has been sent successfully ledgerID : "+ledgerID);
						}

						  } catch (Exception e) {
						  logger.error("Exception in sendEmailMemberReceipt: "+e);
						  }
					} catch (Exception e) {
						// TODO Auto-generated catch block
						logger.error("Exception in private APIResponseVO sendReceipt(TransactionVO txVO): "+e);
					}
					return apiResponse;
				}
	
	
	//------Used for adding multiple transactions from bank Statement-----------//
	public int addStatementToDatabase(BankStatement bankStatementVO){
		logger.debug("Entry :public int addStatementToDatabase(BankStatement bankStatementVO) ");
		int successCount=0;
		
		if(bankStatementVO.getBankStatementList().size()>0){
			
			this.reconcileTheBankStatement(bankStatementVO);
			for(int i=0;bankStatementVO.getBankStatementList().size()>i;i++){
				BankStatement bankStatement=(BankStatement) bankStatementVO.getBankStatementList().get(i);
				TransactionVO txVo=prepareTxFromStatement(bankStatement);
				
				txVo=transactionService.addTransaction(txVo, bankStatement.getOrgID());
				if(txVo.getStatusCode()==200){
					bankStatement.setTxID(txVo.getTransactionID());
					int s = accountsAutoDAO.markMatchingTransactionInStatement(bankStatement);
					successCount=successCount+1;
				
				
			}
			}
			
		}
		
		
		
		logger.debug("Exit : public int addStatementToDatabase(List rawStatementList,String fromDate, String toDate) ");
		return successCount;
	}
	
	
	private TransactionVO prepareTxFromStatement(BankStatement bankStatement){
		logger.debug("Entry : private TransactionVO prepareTxFromStatement(BankStatement bankStatement) ");
		TransactionVO txVO=new TransactionVO();
	try {
			TransactionMetaVO txMetaVO=new TransactionMetaVO();
			txVO.setAdditionalProps(txMetaVO);
			txVO.setSocietyID(bankStatement.getOrgID());
			txVO.setTransactionDate(bankStatement.getValueDate().toString());
			txVO.setRefNumber(bankStatement.getReferenceNumber());
			txVO.setTransactionNumber("0");
			txVO.setAutoInc(true);
			if(bankStatement.getDeposit().compareTo(BigDecimal.ZERO)!=0){
			txVO.setAmount(bankStatement.getDeposit());
			txVO.setTransactionType("Receipt");
			}else if (bankStatement.getWithdrawal().compareTo(BigDecimal.ZERO)!=0){
				txVO.setAmount(bankStatement.getWithdrawal());
				txVO.setTransactionType("Payment");
			}
		
			txVO.setDescription(bankStatement.getDescription());
			if(bankStatement.getBookingDate()!=null)
			txVO.setBankDate(bankStatement.getBookingDate().toString());
			txVO.setTransactionMode(bankStatement.getPayMode());
			
			txVO.setLedgerEntries(prepareLedgerEntriesFromStatement(bankStatement));
			
			
			
		
		
		} catch (Exception e) {
		logger.error("Exeption : private TransactionVO prepareTxFromStatement(BankStatement bankStatement)"+e);
		}
		
		
		logger.debug("Exit : private TransactionVO prepareTxFromStatement(BankStatement bankStatement) ");
		return txVO;
	}
	
	
	private List prepareLedgerEntriesFromStatement(BankStatement bankStatement){
		logger.debug("Entry : private List prepareLedgerEntriesFromStatement(BankStatement bankStatement)");
		List<LedgerEntries> list=new ArrayList<>();
	try {
			LedgerEntries bankEntry=new LedgerEntries();
			bankEntry.setLedgerID(bankStatement.getBankLedgerID());
			if(bankStatement.getDeposit().compareTo(BigDecimal.ZERO)!=0){
				bankEntry.setAmount(bankStatement.getDeposit());
				bankEntry.setCreditDebitFlag("D");
				}else if (bankStatement.getWithdrawal().compareTo(BigDecimal.ZERO)!=0){
					bankEntry.setAmount(bankStatement.getWithdrawal());
					bankEntry.setCreditDebitFlag("C");
				}
			list.add(bankEntry);
			
			LedgerEntries oppositeEntry=new LedgerEntries();
			oppositeEntry.setLedgerID(bankStatement.getSuspenceLedgerID());
			if(bankStatement.getDeposit().compareTo(BigDecimal.ZERO)!=0){
				oppositeEntry.setAmount(bankStatement.getDeposit());
				oppositeEntry.setCreditDebitFlag("C");
				}else if (bankStatement.getWithdrawal().compareTo(BigDecimal.ZERO)!=0){
					oppositeEntry.setAmount(bankStatement.getWithdrawal());
					oppositeEntry.setCreditDebitFlag("D");
				}
			list.add(oppositeEntry);
			
			
		
		
		} catch (Exception e) {
		logger.error("Exception : private List prepareLedgerEntriesFromStatement(BankStatement bankStatement)"+e);
		}
		
		
		logger.debug("Exit : private List prepareLedgerEntriesFromStatement(BankStatement bankStatement) ");
		return list;
	}
	
	//==============Bank Reconciliation ==================// 
	
	public int reconcileTheBankStatement(BankStatement bankStatementVO){
		logger.debug("Entry : public int reconcileTheBankStatement(BankStatement bankStatementVO)");
		int successCount=0;
		
	try {
		
					
					successCount=reconcileTransaction(bankStatementVO);
				
						
					
					
				
		
	} catch (Exception e) {
		logger.error("Exception : public int reconcileTheBankStatement(BankStatement bankStatementVO)"+e);
	}
		
		logger.debug("Exit :public int reconcileTheBankStatement(BankStatement bankStatementVO) ");
		return successCount;
	}
	
	
	private int reconcileTransaction(BankStatement bankStatement){
		logger.debug("Entry : private TransactionVO prepareTxFromStatement(BankStatement bankStatement) ");
		int success=0;
	try {
		if(bankStatement.getDeposit().compareTo(BigDecimal.ZERO)!=0){
			bankStatement.setTxAmount(bankStatement.getDeposit());
			
			}else if (bankStatement.getWithdrawal().compareTo(BigDecimal.ZERO)!=0){
			bankStatement.setTxAmount(bankStatement.getWithdrawal());
			}
		
			
		int txID=transactionService.reoncileTransaction(bankStatement);
		
		if(txID!=0){
			bankStatement.setTxID(txID);
			
			success=accountsAutoDAO.markMatchingTransactionInStatement(bankStatement);
			
		}
			
			
			
		
		
		} catch (Exception e) {
		logger.error("Exeption : private TransactionVO prepareTxFromStatement(BankStatement bankStatement)"+e);
		}
		
		
		logger.debug("Exit : private TransactionVO prepareTxFromStatement(BankStatement bankStatement) ");
		return success;
	}
	
	//------------------PayUMoney Statement API-----------------//
		public List getPayUStatement(int orgID,String fromDate, String toDate){
			logger.debug("Entry : public List getPayUStatement(int orgID,String fromDate, String toDate) ");
			List rawStatementList=new ArrayList();
			
			rawStatementList=accountsAutoDAO.getPayUStatementForGivenOrg(orgID, fromDate, toDate);
			
			
			logger.debug("Exit : public List getPayUStatement(int orgID,String fromDate, String toDate)");
			return rawStatementList;
		}
	
		public int processPayUStatement(){
			logger.debug("Entry : public List processPayUStatement() ");
			int insertFlag=0;
			
			
			try {
				List OrgList=accountsAutoDAO.getPayUStatement(1, 1,0);
				
				if(OrgList.size()>0){
					
					for(int i=0;OrgList.size()>i;i++){
						
						OnlinePaymentGatewayVO payUorgVO=(OnlinePaymentGatewayVO) OrgList.get(i);
						
						List unreconciledList=accountsAutoDAO.getPayUStatement(1, 0, payUorgVO.getOrgID());
						
						if(unreconciledList.size()>0){
							
							for(int j=0;unreconciledList.size()>j;j++){
								
								OnlinePaymentGatewayVO unrecPayUVO=(OnlinePaymentGatewayVO) unreconciledList.get(j);
								
								List receiptList=accountsAutoDAO.getReceiptsStatement(unrecPayUVO);
								
								if(receiptList.size()>0){
									TransactionVO receiptVO=(TransactionVO) receiptList.get(0);
									unrecPayUVO.setTxID(receiptVO.getTransactionID());
									accountsAutoDAO.markMatchingTransactionInPayUStatement(unrecPayUVO);
								}
								
								if(receiptList.size()==0){
								
									TransactionVO txVO=new TransactionVO();
									LedgerEntries debtLedgerEntry=new LedgerEntries();
									LedgerEntries credLedgerEntry=new LedgerEntries();
									AccountHeadVO bankVO=new AccountHeadVO();
								
									txVO.setRefNumber(unrecPayUVO.getChildPaymentID());
									txVO.setTransactionDate(unrecPayUVO.getTxDate());
									txVO.setTransactionMode("Online Payment");
									txVO.setTransactionType("Receipt");
									txVO.setDescription("Being amount received by Online Payment");
									txVO.setAmount(unrecPayUVO.getActualAmount());
									txVO.setSocietyID(unrecPayUVO.getOrgID());
									List journalEntries=new ArrayList<>();
									txVO.setLedgerEntries(null);
									credLedgerEntry.setLedgerID(unrecPayUVO.getMemberLedgerID());
									credLedgerEntry.setCreditDebitFlag("C");
									credLedgerEntry.setAmount(unrecPayUVO.getActualAmount());
									journalEntries.add(credLedgerEntry);
									
									//bankVO=ledgerService.getBankLedger(txVO.getSocietyID());
									bankVO=ledgerService.getPaymentGatewayLedger(txVO.getSocietyID());
									logger.info(" Bank Ledger ID: "+bankVO.getLedgerID());
									if(bankVO.getLedgerID()!=0){
									debtLedgerEntry.setLedgerID(bankVO.getLedgerID());
									debtLedgerEntry.setCreditDebitFlag("D");
									debtLedgerEntry.setAmount(credLedgerEntry.getAmount());
									txVO.setLedgerID(credLedgerEntry.getLedgerID());
									
									journalEntries.add(0,debtLedgerEntry);
									}
									txVO.setLedgerEntries(journalEntries);
									logger.info("Ledger Entry size: "+txVO.getLedgerEntries().size()+" Amount: "+txVO.getAmount()+" Memebr ledgerID:  "+txVO.getLedgerID());								
									if(txVO.getTransactionType().equalsIgnoreCase("Receipt")){
									    txVO=invoiceService.addReceptForInvoiceForOnlinePayment(txVO.getSocietyID(), txVO);
									}else{
										txVO=transactionService.addTransaction(txVO, txVO.getSocietyID());
									}
									
									
									
									if(txVO.getTransactionID()!=0){
											
										logger.info("The online payment for Society ID :"+txVO.getSocietyID()+" of Amount : "+txVO.getAmount()+" for the ledgerID  "+txVO.getLedgerID()+" has been inserted successfully");
									}else{
											logger.info("The online payment for Society ID :"+txVO.getSocietyID()+" of Amount : "+txVO.getAmount()+" for the ledgerID  "+txVO.getLedgerID()+" has been failed, Please look into this transaction");
									}
									
									if(txVO.getStatusCode()==200){
										insertFlag=insertFlag+accountsAutoDAO.markMatchingTransactionInPayUStatement(unrecPayUVO);			
										
									}
					              
									
								}
								
								
								
							}
							
							
						}
						
					}
					
					
				}
			} catch (Exception e) {
				logger.error("Exception : public List processPayUStatement() "+e);
			}
			
			
			
			
			
			logger.debug("Exit : public List processPayUStatement()");
			return insertFlag;
		}
		
		
		public List getPayUStatementForSummary(String fromDate, String toDate){
			logger.debug("Entry : public List getPayUStatement(int orgID,String fromDate, String toDate)");
			List rawStatementList=new ArrayList();
			
			try{
						
			rawStatementList=accountsAutoDAO.getPayUStatementForSummary(fromDate,toDate);
			
				
			}catch(Exception e){
				logger.error("Exception :  public List getPayUStatement(int orgID,String fromDate, String toDate)");
			}
			
			
			
			
			logger.debug("Exit : public List getPayUStatement(int orgID,String fromDate, String toDate)");
			return rawStatementList;
		}
		
		
		public List getPayUStatementNotReconciled(){
			logger.debug("Entry : public List getPayUStatementNotReconciled()");
			List rawStatementList=new ArrayList();
			
			try{
			
				rawStatementList=accountsAutoDAO.getPayUStatementNotReconciled();
			
				
			}catch(Exception e){
				logger.error("Exception :  public List getPayUStatementNotReconciled()");
			}
			logger.debug("Exit : public List getPayUStatementNotReconciled()");
			return rawStatementList;		
		}
		
		/* Add PayUMoney statements after parsing    */
		public int addPayUStatements(OnlinePaymentGatewayVO payUVO){
			int insertFlag=0;
			logger.debug("Entry : public int addPayUStatements(OnlinePaymentGatewayVO payUVO)");
			try{			  	
				
				insertFlag=accountsAutoDAO.addPayUStatements(payUVO);
				
														
			} catch (Exception e) {
			logger.error("Exception at addPayUStatements(OnlinePaymentGatewayVO payUVO) "+e);
			}
			
			logger.debug("Exit : public int addPayUStatements(OnlinePaymentGatewayVO payUVO)"+insertFlag);
			return insertFlag;		
		}
		
		
	
	/* Get list of active society list */
	public List getActiveSocietyList(){
		List societyList=null;
		logger.debug("Entry : public List getActiveSocietyList()");
		
		societyList=accountsAutoDAO.getActiveSocietyList();
		
		logger.debug("Exit : public List getActiveSocietyList()");
		return societyList;		
	}
	
	public int generateBillingCycle(InvoiceBillingCycleVO billingVO){
		logger.debug("Entry : public void generateBillingCycle(int societyID)");
		
		int success=0;
		try {
			List billingCycleList=invoiceService.getBillingCycleList(billingVO.getSocietyId());
			InvoiceBillingCycleVO billingCycleVO=(InvoiceBillingCycleVO) billingCycleList.get((billingCycleList.size()-1));
			DateFormat sDF=new SimpleDateFormat("MMM yy");
			SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
			String newBillingDate=dateUtil.updatePeriodtoNextDate(billingCycleVO.getNextRunDate(), billingVO.getInvoicePeriod());
			billingCycleVO.setRunDate(billingCycleVO.getNextRunDate());
			billingCycleVO.setNextRunDate(newBillingDate);
			billingCycleVO.setFromDate(dateUtil.updatePeriodtoNextDate(billingCycleVO.getFromDate(), billingVO.getInvoicePeriod()));
			billingCycleVO.setUptoDate(dateUtil.updatePeriodtoNextDate(billingCycleVO.getUptoDate(), billingVO.getInvoicePeriod()));
			billingCycleVO.setDueDate(dateUtil.updatePeriodtoNextDate(billingCycleVO.getDueDate(), billingVO.getInvoicePeriod()));
			if(billingVO.getInvoicePeriod().equalsIgnoreCase("Monthly")){
				billingCycleVO.setDescription(sDF.format(myFormat.parse(billingCycleVO.getFromDate())));
			}else
			billingCycleVO.setDescription(sDF.format(myFormat.parse(billingCycleVO.getFromDate()))+" - "+sDF.format(myFormat.parse(billingCycleVO.getUptoDate())));
			
			
			success=accountsAutoDAO.generateBillingCycle(billingCycleVO);
			
			if(success!=0){
				success=accountsAutoDAO.updateNextRunDate(billingCycleVO);
			}
		} catch (Exception e) {
			logger.error("Exception in generateBillingCycle(int societyID)"+e);
		}
		
		logger.debug("Entry : public void generateBillingCycle(int societyID)");
		return success;
	}
	
	
	public List getTransactionDetails(int societyID,String fromDate,String uptoDate){
		List transcationList=null;
		logger.debug("Entry : public List getTransactionDetails(int societyID,String fromDate,String uptoDate)"+fromDate+uptoDate );
		
		try {
		
			
				 transcationList=accountsAutoDAO.getTransactionDetails(societyID, fromDate, uptoDate);
				 
				 if(transcationList.size()>0){
					 for(int i=0;transcationList.size()>i;i++){
						 TransactionVO txVO=(TransactionVO) transcationList.get(i);
						  txVO.setSocietyID(societyID);
						 List<LedgerEntries> ledgerEntries=accountsAutoDAO.getLedgerEntries(txVO.getSocietyID(), txVO.getTransactionID());
						 
						 txVO.setLedgerEntries(ledgerEntries);
						 System.out.println("Sr No ========== "+i);
					 }
					 
				 }
				 
		}	 catch (Exception es) {
			logger.error("Exception at getTransactionDetails "+es);
		}
			 
		logger.debug("Exit : public List getTransactionDetails(int societyID,String fromDate,String uptoDate)");
		return transcationList;		
		}
	
	public List getConfiguredBankStatementList(){
		logger.debug("Entry : public List getConfiguredBankStatementList() ");
		List rawStatementList=new ArrayList();
		
		rawStatementList=accountsAutoDAO.getConfiguredBankStatementList();
		
		
		logger.debug("Exit : public List getConfiguredBankStatementList() ");
		return rawStatementList;
	}
	
	
	public List getPackageDetailsList(int orgID){
		logger.debug("Entry : List getPackageDetailsList(int orgID) ");
		List packageList=new ArrayList();
		
		packageList=accountsAutoDAO.getPackageDetailsList(orgID);
		
		
		logger.debug("Exit : List getPackageDetailsList(int orgID) ");
		return packageList;
	}
	
	public  List getApplicableCharges(int orgID,int packageID,int checkDate){
		logger.debug("Entry : public List getApplicableCharges(int orgID,int packageID) ");
		List packageChargeList=new ArrayList();
		
		packageChargeList=accountsAutoDAO.getApplicableCharges(orgID, packageID,checkDate);
		
		
		logger.debug("Exit : public List getApplicableCharges(int orgID,int packageID) ");
		return packageChargeList;
	}
	
	public  List getProfileDetailsListForPackages(int orgID,int packageID){
		logger.debug("Entry : public List getProfileDetailsListForPackages(int orgID,int packageID) ");
		List packageProfileList=new ArrayList();
		
		packageProfileList=accountsAutoDAO.getProfileDetailsListForPackages(orgID, packageID);
		
		
		logger.debug("Exit : public List getProfileDetailsListForPackages(int orgID,int packageID) ");
		return packageProfileList;
	}
	
	//====== Generate Scheduled Invoices for Organizations ================//
			public List generateScheduledInvoicesForOrg(int orgID,int billingCycleID){
				List invoiceList=new ArrayList();
				
				try {
					logger.debug("Entry : public List generateScheduledInvoicesForOrg(int orgID,int billingCycleID)");
					
					InvoiceBillingCycleVO billingCycleVO=new InvoiceBillingCycleVO();
					List packageList=accountsAutoDAO.getPackageDetailsList(orgID);
				
					for(int j=0;packageList.size()>j;j++){
						PackageDetailsVO packageVO=(PackageDetailsVO) packageList.get(j);
						if(billingCycleID==0){
							List billingCycleList=accountsAutoDAO.getBillingCycleList(orgID,packageVO.getMasterBillingCycleID());
							billingCycleVO=(InvoiceBillingCycleVO) billingCycleList.get(billingCycleList.size()-1);
							}else billingCycleVO=invoiceService.getBillingCycleDetails(billingCycleID);
							List ledgerList=accountsAutoDAO.getProfileDetailsListForPackages(orgID,packageVO.getPackageID());
					
							
				for(int i=0;ledgerList.size()>i;i++){
						AccountHeadVO accVO= (AccountHeadVO) ledgerList.get(i);
						
						InvoiceVO invForMember=prepareInvoice(orgID, accVO, billingCycleVO,packageVO);
						
						if(invForMember.getInvoiceAmount().compareTo(BigDecimal.ZERO)!=0){
						//txForMember=insertTransaction(txForMember, societyID);
							
						logger.info(i+"---- Invoice Ledger ID "+invForMember.getEntityID()+" Amount "+invForMember.getInvoiceAmount());
						
						invoiceList.add(invForMember);
						}
						
					}
					
					}
					
					
					logger.debug("Exit : public List generateScheduledInvoices(int societyID,int billingCycleID)");
				} catch (ArrayIndexOutOfBoundsException e) {
					logger.info("Exception :public List generateScheduledInvoices(int societyID,int billingCycleID)"+e );
				} catch (Exception e) {
					logger.error("Exception :public List generateScheduledInvoices(int societyID,int billingCycleID)"+e );
				}
				return invoiceList;
				
			}
	
	
			/* Create transactions for member  */
			public InvoiceVO prepareInvoice(int orgID,AccountHeadVO accVO,InvoiceBillingCycleVO billingCycleVO,PackageDetailsVO packageVO){
				InvoiceVO invoiceVO=new InvoiceVO();
				BigDecimal grandTotal=BigDecimal.ZERO;
				String txDate="";
				String invMode="";
				List lineItems=new ArrayList();
				ChargesVO roundOffVO=new ChargesVO();
				CommonUtility commonUtils=new CommonUtility();
				roundOffVO.setIsApplicable(false);
				
				logger.debug("Entry : public InvoiceVO prepareInvoice(int societyID,MemberVO memberVO,int chargeTypeID,InvoiceBillingCycleVO billingCycleVO)");
				
				try{
					SocietyVO societyVO=societyServiceBean.getSocietyDetails(orgID);
					invoiceVO.setLedgerID(accVO.getLedgerID());
					invoiceVO.setEntityID(accVO.getLedgerID());
					List chargesList=accountsAutoDAO.getApplicableCharges(orgID, packageVO.getPackageID(),1);
					ProfileDetailsVO proFileVO=ledgerService.getLedgerProfile(accVO.getLedgerID(), orgID);
					String ledgerState=proFileVO.getAddress().getState();
					String orgState=societyVO.getStateName();
				if(chargesList.size()>0){
					

					for(int i=0;chargesList.size()>i;i++){
						
						
						ChargesVO chargeVO=(ChargesVO) chargesList.get(i);
						if(chargeVO.getItemID()>0){
						ItemDetailsVO itemVO=invoiceService.getProductServiceLedgerDetails(chargeVO.getItemID(), orgID);
						chargeVO.setHsnsacCode(itemVO.getHsnsacCode());
						chargeVO.setGstRate(itemVO.getGst());
						chargeVO.setItemName(itemVO.getItemName());
						chargeVO.setPurchasePrice(itemVO.getPurchasePrice());
						chargeVO.setSellingPrice(chargeVO.getSellingPrice());
						chargeVO.setDescription(itemVO.getDescription());
						}
						chargeVO=getConditionalBalance(chargeVO, orgID,accVO,ledgerState,orgState);
						
						if(chargeVO.getCalMethod().equalsIgnoreCase("Rnd")){
							roundOffVO=chargeVO;
							chargesList.remove(i);
							--i;
						}
						
						if((chargeVO.getIsApplicable())&&(chargeVO.getChargeTypeID()!=10)){
							grandTotal=grandTotal.add(chargeVO.getAmount()).setScale(2,2);
							
						if((chargeVO.getChargeTypeID()==1)||(chargeVO.getChargeTypeID()==10)||(chargeVO.getChargeTypeID()==2) ){
							invMode="R";
						}else invMode="O";
					
						txDate=chargeVO.getTxDate();
						invoiceVO.setInvoiceTypeID(chargeVO.getChargeTypeID());				
						invoiceVO.setBillCycleID(billingCycleVO.getBillingCycleID());
						lineItems=chargesList;
						
						}else{
							chargesList.remove(i);
							--i;
						}
						
						
						
						
					}
				}
				
				
					if(roundOffVO.getIsApplicable()){
					BigDecimal roundedTotal=grandTotal;
					BigDecimal roundedAmt=BigDecimal.ZERO;
					if(roundOffVO.getGroupTypeID().equals("1")){
					roundedTotal=grandTotal.setScale(0,RoundingMode.UP);
					roundedAmt=roundedTotal.subtract(grandTotal);
					roundOffVO.setAmount(roundedAmt);
					invoiceVO.setTotalRoundUpAmount(roundedAmt);
					}else 	if(roundOffVO.getGroupTypeID().equals("0")){
						roundedTotal=grandTotal.setScale(0,RoundingMode.DOWN);
						roundedAmt=roundedTotal.subtract(grandTotal);
						roundOffVO.setAmount(roundedAmt);
						invoiceVO.setTotalRoundUpAmount(roundedAmt);
								}else 	if(roundOffVO.getGroupTypeID().equals("-1")){
									roundedTotal=grandTotal.setScale(-1,RoundingMode.HALF_UP);
									roundedAmt=roundedTotal.subtract(grandTotal);
									roundOffVO.setAmount(roundedAmt);
									invoiceVO.setTotalRoundUpAmount(roundedAmt);
											}
				
					
					grandTotal=grandTotal.add(roundedAmt);
					
					
					}
				
				
				invoiceVO.setEntityType(accVO.getLedgerType());
				invoiceVO.setInvoiceAmount(grandTotal);
				invoiceVO.setTotalAmount(grandTotal);
			
			
					TransactionMetaVO txMeta=new TransactionMetaVO();
					txMeta.setInvoiceTypeID(1);
					txMeta.setLedgerID(accVO.getLedgerID());
					txMeta.setInvoiceDate(txDate);
					txMeta.setInvoiceMode(invMode);
			//		txVO.setAdditionalProps(txMeta);
					
				if((societyVO.getGstIN()!=null)&&(societyVO.getGstIN().length()>0)&&(commonUtils.validGSTIN(societyVO.getGstIN()))){
					invoiceVO.setIsSales(1);
					invoiceVO.setMyGSTIN(societyVO.getGstIN());
				}
				 invoiceVO.setOrgID(orgID);
				invoiceVO.setInvoiceType("Journal");
				invoiceVO.setInvoiceDate(txDate);
				invoiceVO.setNotes("For the period of "+dateUtil.getConvetedDate(billingCycleVO.getFromDate())+" to "+dateUtil.getConvetedDate(billingCycleVO.getUptoDate()));
				invoiceVO=invoiceService.convertChargesIntoLineItemsObj(invoiceVO, lineItems);
				
				}catch (Exception e) {
					logger.error("Exception in prepareInvoice "+e);
				}
				
				logger.debug("Exit : public InvoiceVO prepareInvoice(int societyID,MemberVO memberVO,int chargeTypeID,InvoiceBillingCycleVO billingCycleVO)");
				return invoiceVO;		
			}
	
			public ChargesVO getConditionalBalance(ChargesVO chargeVO,int societyID,AccountHeadVO accVO,String ledgerStateName,String orgStateName){
				logger.debug("Entry : public ChargesVO getConditionalBalance(ChargesVO chargeVO,int societyID,AccountHeadVO accVO)");
				try{
					if((chargeVO.getCreditLedgerID()==0)&&(chargeVO.getDebitLedgerID()==00)){
						chargeVO.setIsApplicable(false);
					}else if((chargeVO.getCreditLedgerID()!=0)&&(chargeVO.getDebitLedgerID()!=0)){
						
						
					}else if((chargeVO.getCreditLedgerID()!=0)||(chargeVO.getDebitLedgerID()!=0)){
						
						 if(chargeVO.getCalMethod().equalsIgnoreCase("P")){
														
							 if((chargeVO.getGroupName().equalsIgnoreCase("P"))&&(chargeVO.getGroupTypeID().equalsIgnoreCase(String.valueOf(accVO.getPackageID())))){
									
									chargeVO.setIsApplicable(true);
								
							
							}else chargeVO.setIsApplicable(false);
						
					}else if(chargeVO.getCalMethod().equalsIgnoreCase("Rnd")){
								
							chargeVO.setIsApplicable(true);
					}

						if((chargeVO.getIsApplicable()&&(chargeVO.getSocGstIN()!=null)&&(chargeVO.getHsnsacCode()!=null))){
							if((ledgerStateName==null)||(orgStateName==null)){
								BigDecimal cgst=BigDecimal.ZERO;
							BigDecimal sgst=BigDecimal.ZERO;
							BigDecimal gstRate=chargeVO.getGstRate().divide(new BigDecimal("2")).setScale(2);
							BigDecimal percentage=gstRate.multiply(new BigDecimal("0.01")).setScale(6);
							cgst=chargeVO.getAmount().multiply(percentage).setScale(2);
							sgst=chargeVO.getAmount().multiply(percentage).setScale(2);
							chargeVO.setcGstAmount(cgst);
							chargeVO.setcGstRate(gstRate);
							chargeVO.setsGstAmount(sgst);
							chargeVO.setsGstRate(gstRate);
						
						}else{
								if(ledgerStateName.equalsIgnoreCase(orgStateName)){
								BigDecimal cgst=BigDecimal.ZERO;
								BigDecimal sgst=BigDecimal.ZERO;
								BigDecimal gstRate=chargeVO.getGstRate().divide(new BigDecimal("2")).setScale(2);
								BigDecimal percentage=gstRate.multiply(new BigDecimal("0.01")).setScale(6);
								cgst=chargeVO.getAmount().multiply(percentage).setScale(2);
								sgst=chargeVO.getAmount().multiply(percentage).setScale(2);
								chargeVO.setcGstAmount(cgst);
								chargeVO.setcGstRate(gstRate);
								chargeVO.setsGstAmount(sgst);
								chargeVO.setsGstRate(gstRate);
							}else{
							BigDecimal igst=BigDecimal.ZERO;
							BigDecimal gstRate=chargeVO.getGstRate();
							BigDecimal percentage=gstRate.multiply(new BigDecimal("0.01")).setScale(6);
							igst=chargeVO.getAmount().multiply(percentage).setScale(2);
							chargeVO.setiGstAmount(igst);
							chargeVO.setiGstRate(gstRate);
							}
						}
						
					}			
					}
				}catch(Exception e){
					logger.error("Exception in public ChargesVO getConditionalBalance(ChargesVO chargeVO,int societyID,AccountHeadVO accVO) : "+e+" "+e.getMessage());
				}
				logger.debug("Exit : public ChargesVO getConditionalBalance(ChargesVO chargeVO,int societyID,AccountHeadVO accVO)"+chargeVO.getIsApplicable()+"  "+chargeVO.getChargeID() );
				return chargeVO;
			}
			
			
	/**
	 * @param accountsAutoDAO the accountsAutoDAO to set
	 */
	public void setAccountsAutoDAO(AccountsAutoDAO accountsAutoDAO) {
		this.accountsAutoDAO = accountsAutoDAO;
	}


	/**
	 * @param transactionService the transactionService to set
	 */
	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}


	/**
	 * @param invoiceService the invoiceService to set
	 */
	public void setInvoiceService(InvoiceService invoiceService) {
		this.invoiceService = invoiceService;
	}


	/**
	 * @param societyServiceBean the societyServiceBean to set
	 */
	public void setSocietyServiceBean(SocietyService societyServiceBean) {
		this.societyServiceBean = societyServiceBean;
	}


	/**
	 * @param ledgerService the ledgerService to set
	 */
	public void setLedgerService(LedgerService ledgerService) {
		this.ledgerService = ledgerService;
	}


	/**
	 * @param printReportService the printReportService to set
	 */
	public void setPrintReportService(PrintReportService printReportService) {
		this.printReportService = printReportService;
	}


	/**
	 * @param memberServiceBean the memberServiceBean to set
	 */
	public void setMemberServiceBeen(MemberService memberServiceBeen) {
		this.memberServiceBeen = memberServiceBeen;
	}


	/**
	 * @param notificationService the notificationService to set
	 */
	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
	}

}
