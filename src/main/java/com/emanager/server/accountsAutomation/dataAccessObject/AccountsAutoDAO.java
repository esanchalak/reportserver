package com.emanager.server.accountsAutomation.dataAccessObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.emanager.server.accounts.DataAccessObjects.AccountHeadVO;
import com.emanager.server.accounts.DataAccessObjects.BankStatement;
import com.emanager.server.accounts.DataAccessObjects.ChargesVO;
import com.emanager.server.accounts.DataAccessObjects.LedgerEntries;
import com.emanager.server.accounts.DataAccessObjects.OnlinePaymentGatewayVO;
import com.emanager.server.accounts.DataAccessObjects.TransactionVO;
import com.emanager.server.accountsAutomation.valueObject.PackageDetailsVO;
import com.emanager.server.invoice.dataAccessObject.InvoiceBillingCycleVO;
import com.emanager.server.society.services.SocietyService;
import com.emanager.server.society.valueObject.BankInfoVO;
import com.emanager.server.society.valueObject.SocietyVO;



public class AccountsAutoDAO {

	Logger logger=Logger.getLogger(AccountsAutoDAO.class);
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	SocietyService societyServiceBean;
	
	public List getRawStatement(String bankAccNo,String fromDate, String toDate){
		logger.debug("Entry : public List getRawStatement(String bankAccNo,String fromDate, String toDate) ");
		List rawStatementList=new ArrayList();
		
		try{
		String sqlQuery=" SELECT * FROM import_bank_statement_details WHERE bank_acc_no=:bankAccNo AND ( tx_date BETWEEN :fromDate AND :toDate) and (tx_id is null or tx_id=0)  ORDER BY tx_date,id; ";
			
		Map hMap=new HashMap();	
		hMap.put("bankAccNo", bankAccNo);
		hMap.put("fromDate", fromDate);
		hMap.put("toDate", toDate);
		
		RowMapper rMapper=new RowMapper(){
			
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
					BankStatement bankStmt=new BankStatement();
					bankStmt.setID(rs.getInt("id"));
					bankStmt.setBankAccNumber(rs.getString("bank_acc_no"));
					bankStmt.setValueDate(rs.getDate("tx_date"));
					bankStmt.setReferenceNumber(rs.getString("ref_no"));
					bankStmt.setBookingDate(rs.getDate("bank_date"));
					bankStmt.setDeposit(rs.getBigDecimal("deposit"));
					bankStmt.setWithdrawal(rs.getBigDecimal("withdrawal"));
					bankStmt.setBalance(rs.getBigDecimal("balance"));
					bankStmt.setDescription(rs.getString("description"));
					bankStmt.setRawData(rs.getString("raw_data"));
					bankStmt.setTxID(rs.getInt("tx_id"));
					bankStmt.setOrgID(rs.getInt("org_id"));
					bankStmt.setTxType(rs.getString("tx_type"));
					bankStmt.setSuspenceLedgerID(rs.getInt("suspense_ledger_id"));
					bankStmt.setBankLedgerID(rs.getInt("bank_ledger_id"));
					bankStmt.setPayMode(rs.getString("tx_mode"));
				
					
					return bankStmt;
			}
		};
		
		rawStatementList=namedParameterJdbcTemplate.query(sqlQuery, hMap, rMapper);
		
			
		}catch(Exception e){
			logger.error("Exception :  public List getRawStatement(String bankAccNo,String fromDate, String toDate)");
		}
		
		
		
		
		logger.debug("Exit : public List getRawStatement(String bankAccNo,String fromDate, String toDate) ");
		return rawStatementList;
		
		
		
		
	}
	
	public List getRawStatementFromLedgerID(int bankLedgerID,String fromDate, String toDate,int orgID){
		logger.debug("Entry : public List getRawStatementFromLedgerID(String bankAccNo,String fromDate, String toDate) ");
		List rawStatementList=new ArrayList();
		
		try{
		String sqlQuery=" SELECT * FROM import_bank_statement_details WHERE bank_ledger_id=:bankLedgerID and org_id=:orgID AND ( tx_date BETWEEN :fromDate AND :toDate)   ORDER BY tx_date,id; ";
			
		Map hMap=new HashMap();	
		hMap.put("bankLedgerID", bankLedgerID);
		hMap.put("fromDate", fromDate);
		hMap.put("toDate", toDate);
		hMap.put("orgID", orgID);
		
		RowMapper rMapper=new RowMapper(){
			
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
					BankStatement bankStmt=new BankStatement();
					bankStmt.setID(rs.getInt("id"));
					bankStmt.setBankAccNumber(rs.getString("bank_acc_no"));
					bankStmt.setValueDate(rs.getDate("tx_date"));
					bankStmt.setReferenceNumber(rs.getString("ref_no"));
					bankStmt.setBookingDate(rs.getDate("bank_date"));
					bankStmt.setDeposit(rs.getBigDecimal("deposit"));
					bankStmt.setWithdrawal(rs.getBigDecimal("withdrawal"));
					bankStmt.setBalance(rs.getBigDecimal("balance"));
					bankStmt.setDescription(rs.getString("description"));
					bankStmt.setRawData(rs.getString("raw_data"));
					bankStmt.setTxID(rs.getInt("tx_id"));
					bankStmt.setOrgID(rs.getInt("org_id"));
					bankStmt.setTxType(rs.getString("tx_type"));
					bankStmt.setSuspenceLedgerID(rs.getInt("suspense_ledger_id"));
					bankStmt.setBankLedgerID(rs.getInt("bank_ledger_id"));
					bankStmt.setPayMode(rs.getString("tx_mode"));
				
					
					return bankStmt;
			}
		};
		
		rawStatementList=namedParameterJdbcTemplate.query(sqlQuery, hMap, rMapper);
		
			
		}catch(Exception e){
			logger.error("Exception :  public List getRawStatementFromLedgerID(String bankAccNo,String fromDate, String toDate)");
		}
		
		
		
		
		logger.debug("Exit : public List getRawStatementFromLedgerID(String bankAccNo,String fromDate, String toDate) ");
		return rawStatementList;
		
		
		
		
	}
	
	/* Add raw bank statements after parsing    */
	public int markMatchingTransactionInStatement(BankStatement bankStatementVO){
		int updateFlag=0;
		logger.debug("Entry : public int markMatchingTransactionInStatement(BankStatement bankStatementVO)");
		try{
			
		String sql="update import_bank_statement_details  set tx_id=:txID  where  org_id=:orgID and  id=:ID ;  ";
		
		
		Map hashMap = new HashMap();

		hashMap.put("txID",bankStatementVO.getTxID());
		hashMap.put("orgID", bankStatementVO.getOrgID());
		hashMap.put("ID", bankStatementVO.getID());
		
			updateFlag=namedParameterJdbcTemplate.update(sql, hashMap);
		
		} catch (Exception e) {
		logger.error("Exception at public int markMatchingTransactionInStatement(BankStatement bankStatementVO) "+e);
		}
		
		logger.debug("Exit : public int markMatchingTransactionInStatement(BankStatement bankStatementVO)");
		return updateFlag;		
	}
	
	
	public List getPayUStatementForGivenOrg(int orgID,String fromDate, String toDate){
		logger.debug("Entry : public List getPayUStatement(int orgID,String fromDate, String toDate)");
		List rawStatementList=new ArrayList();
		
		try{
		String sqlQuery=" SELECT * FROM payment_gateway_statement_details WHERE org_id=:orgID AND ( tx_date BETWEEN :fromDate AND :toDate) and is_deleted=0 ORDER BY tx_date,id ; ";
			
		Map hMap=new HashMap();	
		hMap.put("fromDate", fromDate);
		hMap.put("toDate", toDate);
		hMap.put("orgID", orgID);
		
		RowMapper rMapper=new RowMapper(){
			
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
					OnlinePaymentGatewayVO payUVO=new OnlinePaymentGatewayVO();
					payUVO.setID(rs.getInt("id"));
					payUVO.setOrgID(rs.getInt("org_id"));
					payUVO.setTxDate(rs.getString("tx_date"));
					payUVO.setMemberLedgerID(rs.getInt("member_ledger_id"));
					payUVO.setDescription(rs.getString("description"));
					payUVO.setChildPaymentID(rs.getString("child_payment_id"));
					payUVO.setTotalAmount(rs.getBigDecimal("total_amount"));
					payUVO.setCommissionAmount(rs.getBigDecimal("commission_amount"));
					payUVO.setGtProcessingFees(rs.getBigDecimal("payu_tdr"));
					payUVO.setActualAmount(rs.getBigDecimal("actual_amount"));
					payUVO.setMtProcessingFees(rs.getBigDecimal("commission_received"));
					payUVO.setParentUtr(rs.getString("parent_utr"));
					payUVO.setCustomerName(rs.getString("customer_name"));
					payUVO.setCustomerEmail(rs.getString("email"));
					payUVO.setCustomerPhone(rs.getString("phone"));
					payUVO.setPaymentMode(rs.getString("source_type"));
					payUVO.setParentPaymentID(rs.getString("parent_payment_id"));
					payUVO.setChildUtr(rs.getString("child_utr"));
					payUVO.setMerchantSaltKey(rs.getString("merchant_salt_key"));
					
					return payUVO;
			}
		};
		
		rawStatementList=namedParameterJdbcTemplate.query(sqlQuery, hMap, rMapper);
		
			
		}catch(Exception e){
			logger.error("Exception :  public List getPayUStatement(int orgID,String fromDate, String toDate)");
		}
		
		
		
		
		logger.debug("Exit : public List getPayUStatement(int orgID,String fromDate, String toDate)");
		return rawStatementList;
		
		
		
		
	}
	
	public List getPayUStatementForSummary(String fromDate, String toDate){
		logger.debug("Entry : public List getPayUStatement(int orgID,String fromDate, String toDate)");
		List rawStatementList=new ArrayList();
		
		try{
		String sqlQuery=" SELECT p.*,society_name FROM payment_gateway_statement_details p, society_details s WHERE p.org_id=s.society_id AND ( tx_date BETWEEN :fromDate AND :toDate) and is_deleted=0 ORDER BY tx_date,id ; ";
			
		Map hMap=new HashMap();	
		hMap.put("fromDate", fromDate);
		hMap.put("toDate", toDate);
		
		
		RowMapper rMapper=new RowMapper(){
			
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
					OnlinePaymentGatewayVO payUVO=new OnlinePaymentGatewayVO();
					payUVO.setID(rs.getInt("id"));
					payUVO.setOrgID(rs.getInt("org_id"));
					payUVO.setTxDate(rs.getString("tx_date"));
					payUVO.setMemberLedgerID(rs.getInt("member_ledger_id"));
					payUVO.setDescription(rs.getString("description"));
					payUVO.setChildPaymentID(rs.getString("child_payment_id"));
					payUVO.setTotalAmount(rs.getBigDecimal("total_amount"));
					payUVO.setCommissionAmount(rs.getBigDecimal("commission_amount"));
					payUVO.setGtProcessingFees(rs.getBigDecimal("payu_tdr"));
					payUVO.setActualAmount(rs.getBigDecimal("actual_amount"));
					payUVO.setMtProcessingFees(rs.getBigDecimal("commission_received"));
					payUVO.setParentUtr(rs.getString("parent_utr"));
					payUVO.setCustomerName(rs.getString("customer_name"));
					payUVO.setCustomerEmail(rs.getString("email"));
					payUVO.setCustomerPhone(rs.getString("phone"));
					payUVO.setPaymentMode(rs.getString("source_type"));
					payUVO.setParentPaymentID(rs.getString("parent_payment_id"));
					payUVO.setChildUtr(rs.getString("child_utr"));
					payUVO.setMerchantSaltKey(rs.getString("merchant_salt_key"));
					payUVO.setSocietyName(rs.getString("society_name"));
					payUVO.setTxID(rs.getInt("tx_id"));
					payUVO.setIsComplete(rs.getInt("is_complete"));
					return payUVO;
			}
		};
		
		rawStatementList=namedParameterJdbcTemplate.query(sqlQuery, hMap, rMapper);
		
			
		}catch(Exception e){
			logger.error("Exception :  public List getPayUStatement(int orgID,String fromDate, String toDate)");
		}
		
		
		
		
		logger.debug("Exit : public List getPayUStatement(int orgID,String fromDate, String toDate)");
		return rawStatementList;
	}
	
	
	public List getPayUStatementNotReconciled(){
		logger.debug("Entry : public List getPayUStatementNotReconciled()");
		List rawStatementList=new ArrayList();
		
		
		
		try{
		String sqlQuery=" SELECT p.*,society_name FROM payment_gateway_statement_details p,society_details s WHERE p.org_id=s.society_id and is_deleted=0 AND is_complete=0 ORDER BY tx_date,id ; ";
			
		Map hMap=new HashMap();	
		
		
		RowMapper rMapper=new RowMapper(){
			
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
					OnlinePaymentGatewayVO payUVO=new OnlinePaymentGatewayVO();
					payUVO.setID(rs.getInt("id"));
					payUVO.setOrgID(rs.getInt("org_id"));
					payUVO.setTxDate(rs.getString("tx_date"));
					payUVO.setMemberLedgerID(rs.getInt("member_ledger_id"));
					payUVO.setDescription(rs.getString("description"));
					payUVO.setChildPaymentID(rs.getString("child_payment_id"));
					payUVO.setTotalAmount(rs.getBigDecimal("total_amount"));
					payUVO.setCommissionAmount(rs.getBigDecimal("commission_amount"));
					payUVO.setGtProcessingFees(rs.getBigDecimal("payu_tdr"));
					payUVO.setActualAmount(rs.getBigDecimal("actual_amount"));
					payUVO.setMtProcessingFees(rs.getBigDecimal("commission_received"));
					payUVO.setParentUtr(rs.getString("parent_utr"));
					payUVO.setCustomerName(rs.getString("customer_name"));
					payUVO.setCustomerEmail(rs.getString("email"));
					payUVO.setCustomerPhone(rs.getString("phone"));
					payUVO.setPaymentMode(rs.getString("source_type"));
					payUVO.setParentPaymentID(rs.getString("parent_payment_id"));
					payUVO.setChildUtr(rs.getString("child_utr"));
					payUVO.setMerchantSaltKey(rs.getString("merchant_salt_key"));
					payUVO.setSocietyName(rs.getString("society_name"));
					payUVO.setTxID(rs.getInt("tx_id"));
					payUVO.setIsComplete(rs.getInt("is_complete"));
					return payUVO;
			}
		};
		
		rawStatementList=namedParameterJdbcTemplate.query(sqlQuery, hMap, rMapper);
		
			
		}catch(Exception e){
			logger.error("Exception :  public List getPayUStatementNotReconciled()");
		}
		
		
		
		
		logger.debug("Exit : public List getPayUStatementNotReconciled()");
		return rawStatementList;		
	}
	
	/* Add PayUMoney statements after parsing    */
	public int addPayUStatements(OnlinePaymentGatewayVO payUVO){
		int key=0;
		logger.debug("Entry : public int addPayUStatements(OnlinePaymentGatewayVO payUVO)");
		try{
			   java.util.Date today = new java.util.Date();
			   final java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(today.getTime());
			
			String sql = "insert into `payment_gateway_statement_details`(`org_id`,`tx_date`,`member_ledger_id`,`description`,`child_payment_id`,`total_amount`,`commission_amount`,`payu_tdr`,`actual_amount`,`commission_received`,`parent_utr`,`customer_name`,`email`,`phone`,`source_type`,`parent_payment_id`,`child_utr`,`insert_date`,`merchant_salt_key`) VALUES"
					+ " (:orgID,:txDate,:memberLedgerID,:description,:childPaymentID,:totalAmount,:commissionAmount,:payUTDR,:actualAmount,:commissionReceived,:parentUtr,:customerName,:customerEmail,:customerPhone,:sourceType,:parentPaymentID,:childUtr,:insertDate,:merchantSaltKey); ";
			
			Map hashMap = new HashMap();
			
			SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(payUVO);
			   KeyHolder keyHolder = new GeneratedKeyHolder();
			   getNamedParameterJdbcTemplate().update(sql, fileParameters, keyHolder);
			   key=keyHolder.getKey().intValue();

			
			
					
		} catch (DuplicateKeyException e) {
					key=-1;
					logger.info("Duplicate transactions can not be added with parent payment ID :"+payUVO.getParentPaymentID());
					
					
		} catch (Exception e) {
		logger.error("Exception at addPayUStatements(OnlinePaymentGatewayVO payUVO) "+e);
		}
		
		logger.debug("Exit : public int addPayUStatements(OnlinePaymentGatewayVO payUVO)"+key);
		return key;		
	}
	
	public List getPayUStatement(int unReconciled,int isGroup,int orgID){
		logger.debug("Entry : public List getPayUStatement(int orgID,String fromDate, String toDate)");
		List rawStatementList=new ArrayList();
		String strUnReconciled="";
		String strGroup="";
		String strOrgID="";
		
		if(unReconciled==1){
			strUnReconciled=" AND tx_id is null ";
		}
		if(isGroup==1){
			strGroup=" Group by org_id ";
		}
		if(orgID>0){
			strOrgID=" org_id=:orgID and ";
		}
		
		
		try{
		String sqlQuery=" SELECT * FROM payment_gateway_statement_details WHERE "+strOrgID+" is_deleted=0 "+strUnReconciled+" "+strGroup+"  ORDER BY tx_date,id ; ";
			
		Map hMap=new HashMap();	
		hMap.put("orgID", orgID);
		
		RowMapper rMapper=new RowMapper(){
			
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
					OnlinePaymentGatewayVO payUVO=new OnlinePaymentGatewayVO();
					payUVO.setID(rs.getInt("id"));
					payUVO.setOrgID(rs.getInt("org_id"));
					payUVO.setTxDate(rs.getString("tx_date"));
					payUVO.setMemberLedgerID(rs.getInt("member_ledger_id"));
					payUVO.setDescription(rs.getString("description"));
					payUVO.setChildPaymentID(rs.getString("child_payment_id"));
					payUVO.setTotalAmount(rs.getBigDecimal("total_amount"));
					payUVO.setCommissionAmount(rs.getBigDecimal("commission_amount"));
					payUVO.setGtProcessingFees(rs.getBigDecimal("payu_tdr"));
					payUVO.setActualAmount(rs.getBigDecimal("actual_amount"));
					payUVO.setMtProcessingFees(rs.getBigDecimal("commission_received"));
					payUVO.setParentUtr(rs.getString("parent_utr"));
					payUVO.setCustomerName(rs.getString("customer_name"));
					payUVO.setCustomerEmail(rs.getString("email"));
					payUVO.setCustomerPhone(rs.getString("phone"));
					payUVO.setPaymentMode(rs.getString("source_type"));
					payUVO.setParentPaymentID(rs.getString("parent_payment_id"));
					payUVO.setChildUtr(rs.getString("child_utr"));
					payUVO.setMerchantSaltKey(rs.getString("merchant_salt_key"));
					
					return payUVO;
			}
		};
		
		rawStatementList=namedParameterJdbcTemplate.query(sqlQuery, hMap, rMapper);
		
			
		}catch(Exception e){
			logger.error("Exception :  public List getPayUStatement(int orgID,String fromDate, String toDate)");
		}
		
		
		
		
		logger.debug("Exit : public List getPayUStatement(int orgID,String fromDate, String toDate)");
		return rawStatementList;
		
		
		
		
	}
	
	public List getReceiptsStatement(OnlinePaymentGatewayVO payUVO){
		logger.debug("Entry : public List getReceiptsStatement(OnlinePaymentGatewayVO payUVO)");
		List rawStatementList=new ArrayList();
		
		SocietyVO societyVO=societyServiceBean.getSocietyDetails(payUVO.getOrgID());
		
		
		try{
		String sqlQuery=" SELECT a.* FROM account_transactions_"+societyVO.getDataZoneID()+" a,account_ledger_entry_"+societyVO.getDataZoneID()+" l WHERE a.tx_id=l.tx_id And a.is_deleted=0 AND a.org_id=:orgID and l.ledger_id=:ledgerID AND a.tx_date=:txDate "+
							" AND (ref_no=:childPaymentID OR ref_no=:parentPaymentID); ";
			
		Map hMap=new HashMap();	
		hMap.put("orgID", payUVO.getOrgID());
		hMap.put("ledgerID", payUVO.getMemberLedgerID());
		hMap.put("txDate", payUVO.getTxDate());
		hMap.put("childPaymentID", payUVO.getChildPaymentID());
		hMap.put("parentPaymentID", payUVO.getParentPaymentID());
		
		RowMapper rMapper=new RowMapper(){
			
			public Object mapRow(ResultSet rs, int rowNum)
					throws SQLException {
					TransactionVO payUVO=new TransactionVO();
					payUVO.setTransactionID(rs.getInt("tx_id"));
					payUVO.setAmount(rs.getBigDecimal("amount"));
					payUVO.setTransactionDate(rs.getString("tx_date"));
					
					return payUVO;
			}
		};
		
		rawStatementList=namedParameterJdbcTemplate.query(sqlQuery, hMap, rMapper);
		
			
		}catch(Exception e){
			logger.error("Exception : public List getReceiptsStatement(OnlinePaymentGatewayVO payUVO)");
		}
		
		
		
		
		logger.debug("Exit : public List getReceiptsStatement(OnlinePaymentGatewayVO payUVO)");
		return rawStatementList;
		
		
		
		
	}
	
	/* Add raw bank statements after parsing    */
	public int markMatchingTransactionInPayUStatement(OnlinePaymentGatewayVO payUStatementVO){
		int updateFlag=0;
		logger.debug("Entry : public int markMatchingTransactionInPayUStatement(OnlinePaymentGatewayVO payUStatementVO)");
		try{
			
		String sql="update payment_gateway_statement_details  set tx_id=:txID  where  org_id=:orgID and  id=:ID ;  ";
		
		
		Map hashMap = new HashMap();

		hashMap.put("txID",payUStatementVO.getTxID());
		hashMap.put("orgID", payUStatementVO.getOrgID());
		hashMap.put("ID", payUStatementVO.getID());
		
			updateFlag=namedParameterJdbcTemplate.update(sql, hashMap);
		
		} catch (Exception e) {
		logger.error("Exception at public int markMatchingTransactionInPayUStatement(OnlinePaymentGatewayVO payUStatementVO) "+e);
		}
		
		logger.debug("Exit : public int markMatchingTransactionInPayUStatement(OnlinePaymentGatewayVO payUStatementVO)");
		return updateFlag;		
	}

	/* Get list of active society list */
	public List getActiveSocietyList() {
		List societyList = null;
		logger.debug("Entry : public List getActiveSocietyList()");
		try {
			String strQuery = "SELECT * FROM society_settings s,society_details sd WHERE sd.working!=0 AND sd.society_id=s.society_id AND next_billing_date<=CURDATE();   ";

			Map nameparam = new HashMap();
			
			RowMapper Rmapper = new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					InvoiceBillingCycleVO inBillingVO = new InvoiceBillingCycleVO();
					inBillingVO.setSocietyId(rs.getInt("society_id"));
					inBillingVO.setInvoicePeriod(rs.getString("invoice_period"));
					inBillingVO.setSocietyName(rs.getString("society_name"));
					inBillingVO.setNextRunDate(rs.getString("next_billing_date"));
					inBillingVO.setInvoiceDate(rs.getString("invoice_date"));
				
					return inBillingVO;
				}
			};
			societyList = namedParameterJdbcTemplate.query(strQuery, nameparam,Rmapper);
		} catch (Exception e) {
			logger.error("Exception at getActiveSocietyList " + e);
		}
		logger.debug("Exit : public List getActiveSocietyList()"+societyList.size());
		return societyList;
	}
	
	public int generateBillingCycle(InvoiceBillingCycleVO billingCycleVO){
		logger.debug("Entry : public void generateBillingCycle(int societyID)"+billingCycleVO.getSocietyId());
		
		int success=0;
	try {
		String sqlQuery="INSERT INTO `in_billing_cycle_details`(`society_id`,`description`,`from_date`,`to_date`,`rundate`,`next_run_date`,`due_date`)" +
						" VALUES ( :societyID,:description,:fromDate,:uptoDate,:runDate,:nextRunDate,:dueDate )"; 
	logger.debug("query : " + sqlQuery);
		Map hmap=new HashMap();
		hmap.put("societyID", billingCycleVO.getSocietyId());
		hmap.put("description",billingCycleVO.getDescription());
		hmap.put("fromDate", billingCycleVO.getFromDate());
		hmap.put("uptoDate", billingCycleVO.getUptoDate());
		hmap.put("nextRunDate", billingCycleVO.getNextRunDate());
		hmap.put("runDate", billingCycleVO.getRunDate());
		hmap.put("dueDate", billingCycleVO.getDueDate());
		
		success=namedParameterJdbcTemplate.update(sqlQuery, hmap);
		
	} catch (DataAccessException e) {
		logger.error("Exception at insertReceiptInvoice(String societyID ,InvoiceDetailsVO invoiceVO,int reciptID) "+e);
	}
	logger.debug("Entry : public void generateBillingCycle(int societyID)");
	return success;
}
	
	public int updateNextRunDate(InvoiceBillingCycleVO billingCycleVO){
		logger.debug("Entry : public void updateNextRunDate(InvoiceBillingCycleVO billingCycleVO)");
		
		int success=0;
	try {
		String sqlQuery="Update society_settings set next_billing_date=:nextDate where society_id=:societyID "; 
	logger.debug("query : " + sqlQuery);
		Map hmap=new HashMap();
		hmap.put("societyID", billingCycleVO.getSocietyId());
		
		hmap.put("nextDate", billingCycleVO.getNextRunDate());
		
		
		success=namedParameterJdbcTemplate.update(sqlQuery, hmap);
		
	} catch (DataAccessException e) {
		logger.error("Exception at updateNextRunDate(InvoiceBillingCycleVO billingCycleVO) "+e);
	}
	logger.debug("Entry : public void updateNextRunDate(InvoiceBillingCycleVO billingCycleVO)");
	return success;
}
	
	public List getTransactionDetails(int societyID,String fromDate,String uptoDate){
		List transcationList=null;
		logger.debug("Entry : public List getTransactionDetails(int societyID,String fromDate,String uptoDate)"+fromDate+uptoDate );
		
		try {
			SocietyVO societyVO=societyServiceBean.getSocietyDetails(societyID);	
			String query="SELECT ats.* FROM account_transactions_"+societyVO.getDataZoneID()+" ats, account_ledger_entry_"+societyVO.getDataZoneID()+" ale , account_ledgers_"+societyVO.getDataZoneID()+" al WHERE ats.tx_id=ale.tx_id and ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID and ale.ledger_id=al.id  and (  tx_date BETWEEN :fromDate AND :uptoDate )  AND ats.is_deleted=0  GROUP BY ats.tx_id order by tx_date ;";
			logger.info(query);
			Map hmap=new HashMap();
			hmap.put("fromDate",fromDate);
			hmap.put("uptoDate", uptoDate);
			hmap.put("societyID", societyID);
			
			RowMapper RMapper = new RowMapper() {
				
				 public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					 TransactionVO tallyVO=new TransactionVO();
					 tallyVO.setTransactionID(rs.getInt("tx_id"));
					 tallyVO.setTransactionNumber(rs.getString("tx_number"));
					 tallyVO.setRefNumber(rs.getString("ref_no"));
					 tallyVO.setAmount(rs.getBigDecimal("amount"));
					 tallyVO.setBankDate(rs.getString("bank_date"));
					 tallyVO.setTransactionType(rs.getString("tx_type"));
					 tallyVO.setTransactionDate(rs.getString("tx_date"));
					 tallyVO.setDescription(rs.getString("description"));
					 return tallyVO ;
				 } };
			
				 transcationList=namedParameterJdbcTemplate.query(query, hmap, RMapper);
				 
				 logger.info("-----------"+transcationList.size());
		} catch (DataAccessException e) {
			logger.error("No Data found "+e);
		}catch (Exception es) {
			logger.error("Exception at getTransactionDetails "+es);
		}
			 
		logger.debug("Exit : public List getTransactionDetails(int societyID,String fromDate,String uptoDate)");
		return transcationList;		
		}
	
	 public List<LedgerEntries> getLedgerEntries(int societyID ,int txID){
			List ledgerEntryList=null;
			logger.debug("Entry : public List getLedgerEntries(int societyID ,int txID)" +txID);
			
			try {
				SocietyVO societyVO=societyServiceBean.getSocietyDetails(societyID);	
				String query="SELECT ale.*,al.ledger_name AS ledger FROM account_ledger_entry_"+societyVO.getDataZoneID()+" ale,account_transactions_"+societyVO.getDataZoneID()+" ats ,account_ledgers_"+societyVO.getDataZoneID()+" al WHERE ale.tx_id=ats.tx_id and ats.org_id=ale.org_id and ale.org_id=al.org_id and al.org_id=:societyID  AND al.id=ale.ledger_id AND ats.is_deleted=0 AND ats.tx_id=:txID ;";
				
				Map hmap=new HashMap();
				hmap.put("txID",txID);
				hmap.put("societyID", societyID);
				
				RowMapper RMapper = new RowMapper() {
					
					 public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						 LedgerEntries tallyVO=new LedgerEntries();
						 tallyVO.setTxID(rs.getInt("tx_id"));
						 
						 tallyVO.setAmount(rs.getBigDecimal("amount"));
						 tallyVO.setCreditDebitFlag(rs.getString("type"));
						 tallyVO.setLedgerName(rs.getString("ledger"));
						 tallyVO.setLedgerID(rs.getInt("ledger_id"));
						 tallyVO.setStrComments(rs.getString("description"));
						 
						 return tallyVO ;
					 } };
				
					 ledgerEntryList=namedParameterJdbcTemplate.query(query, hmap, RMapper);
					 
					 
			} catch (DataAccessException e) {
				logger.error("No Data found "+e);
			}catch (Exception es) {
				logger.error("Exception at public List getLedgerEntries(int societyID ,int txID) "+es);
			}
				 
			logger.debug("Exit : public List getLedgerEntries(int societyID ,int txID) "+ledgerEntryList.size());
			return ledgerEntryList;		
			}
	
	 public List<BankStatement> getConfiguredBankStatementList(){
			List bankList=null;
			logger.debug("Entry : public List getConfiguredBankStatementList()" );
			
			try {
				String query="SELECT * FROM bank_statement_import_config;;";
				
				Map hmap=new HashMap();
				
				
				
				RowMapper RMapper = new RowMapper() {
					
					 public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						BankInfoVO bankStatementVO=new BankInfoVO();
						 bankStatementVO.setEmail(rs.getString("from_email"));
						 bankStatementVO.setFilePath(rs.getString("path"));
						 bankStatementVO.setIsDeleted(rs.getInt("is_deleted"));
						 return bankStatementVO ;
					 } };
				
					 bankList=namedParameterJdbcTemplate.query(query, hmap, RMapper);
					 
					 
			} catch (DataAccessException e) {
				logger.error("No Data found "+e);
			}catch (Exception es) {
				logger.error("Exception at public List getConfiguredBankStatementList() "+es);
			}
				 
			logger.debug("Exit : public List getConfiguredBankStatementList() "+bankList.size());
			return bankList;		
			}
	
	 /* Get Package details list */
		public List getPackageDetailsList(int orgID){
		List packageList=null;
		logger.debug("Entry : public List getPackageDetailsList(int orgID)");
		String sqlQuery=null;
		try {
			
				sqlQuery=" SELECT * FROM package_details WHERE org_id=:orgID  ;";
			Map hmap=new HashMap();
			hmap.put("orgID", orgID);
			
			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
				PackageDetailsVO packageVO=new PackageDetailsVO();
				packageVO.setPackageID(rs.getInt("package_id"));
				packageVO.setPackageName(rs.getString("package_name"));
				packageVO.setDescription(rs.getString("description"));
				packageVO.setOrgID(rs.getInt("org_id"));
				packageVO.setMasterBillingCycleID(rs.getInt("master_billing_id"));
				return packageVO;
				}
			};
			
			packageList=namedParameterJdbcTemplate.query(sqlQuery, hmap, rMapper);
			
		}catch(EmptyResultDataAccessException e){
			logger.info("No package details available for the organization ID : "+orgID+" "+e);
		} catch (Exception e) {
			logger.error("Exception occurred in public List getPackageDetailsList(int orgID)");
		}		
		logger.debug("Exit : public List getPackageDetailsList(int orgID)");
		return packageList;		
	}
		
		 /* Get Package details list */
		public List getProfileDetailsListForPackages(int orgID,int packageID){
		List packageList=null;
		logger.debug("Entry : public List getProfileDetailsListForPackages(int orgID)");
		String sqlQuery=null;
		try {
				SocietyVO societyVO=societyServiceBean.getSocietyDetails(orgID);	
				sqlQuery=" SELECT d.id as profile_id,d.profile_type,d.ledger_meta,a.id as ledger_id,a.ledger_name,a.org_id, p.package_id FROM package_profile_relation p,account_ledger_profile_details d,account_ledgers_"+societyVO.getDataZoneID()+" a WHERE p.package_id=:packageID AND p.entity_id=d.id AND d.ledger_id=a.id;  ;";
			Map hmap=new HashMap();
			hmap.put("orgID", orgID);
			hmap.put("packageID", packageID);
			
			RowMapper rMapper = new RowMapper() {

				public Object mapRow(ResultSet rs, int rowNum)
						throws SQLException {
				AccountHeadVO accVO=new AccountHeadVO();
				accVO.setLedgerID(rs.getInt("ledger_id"));
				accVO.setLedgerName(rs.getString("ledger_name"));
				accVO.setLedgerType(rs.getString("profile_type"));
				accVO.setAdditionalProps(rs.getString("ledger_meta"));
				accVO.setOrgID(rs.getInt("org_id"));
				accVO.setPackageID(rs.getInt("package_id"));
				
				
				return accVO;
				}
			};
			
			packageList=namedParameterJdbcTemplate.query(sqlQuery, hmap, rMapper);
			
		}catch(EmptyResultDataAccessException e){
			logger.info("No profile details  available for the organization ID : "+orgID+" "+e);
		} catch (Exception e) {
			logger.error("Exception occurred in public List getProfileDetailsListForPackages(int orgID)");
		}		
		logger.debug("Exit : public List getProfileDetailsListForPackages(int orgID)");
		return packageList;		
	}
	 
		/* Get list of billingCycle list  of society*/
		public List getBillingCycleList(int orgID,int masterBillingID){
			logger.debug("Entry : public List getBillingCycleList(String societyID)");
			List billingCycleList=null;
			
			try {
				String strQuery = " SELECT * FROM in_billing_cycle_details WHERE society_id=:orgID AND master_billing_id=:masterBillingID ; ";
						

				Map nameparam = new HashMap();
				nameparam.put("orgID", orgID);
				nameparam.put("masterBillingID", masterBillingID);
				RowMapper Rmapper = new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						InvoiceBillingCycleVO inBillingVO = new InvoiceBillingCycleVO();
						inBillingVO.setBillingCycleID(rs.getInt("id"));
						inBillingVO.setSocietyId(rs.getInt("society_id"));
						inBillingVO.setDescription(rs.getString("description"));
						inBillingVO.setFromDate(rs.getString("from_date"));
						inBillingVO.setUptoDate(rs.getString("to_date"));
						inBillingVO.setRunDate(rs.getString("rundate"));
						inBillingVO.setDueDate(rs.getString("due_date"));
						inBillingVO.setNextRunDate(rs.getString("next_run_date"));
						inBillingVO.setMasterBillingID(rs.getInt("master_billing_id"));
						return inBillingVO;
					}
				};
				billingCycleList = namedParameterJdbcTemplate.query(strQuery, nameparam,Rmapper);
			} catch (Exception e) {
				logger.error("Exception at getBillingCycleList " + e);
			}
			
			logger.debug("Exit : public List getBillingCycleList(String societyID)");
			
			return billingCycleList;
		}	
		
		/* Get list of all applicable fees for the given member */
		public List getApplicableCharges(int orgID,int packageID,int checkDate) {
			logger.debug("Entry : public List getApplicableCharges(int orgID,int packageID) ");
			List chargeList = null;
			String strCondition="";
			if(checkDate==1){
				strCondition=" AND (next_charge_date <=curdate()) ";
			}
		
			
			
			try {
				String sql = " SELECT m.* ,s.precision ,s.gstin  "
						+ " FROM scheduled_charges m,society_settings s,package_details p WHERE  m.cal_method=m.group_name AND m.group_name='p' AND group_type_id=p.package_id AND p.package_id=:packageID AND m.org_id=s.society_id AND (m.org_id=:orgID ) " +
						" AND m.status='Active'" +
						strCondition+"  ORDER BY charge_sequence; ";
				
				Map hashMap = new HashMap();

				hashMap.put("orgID", orgID);
				
				hashMap.put("packageID", packageID);
				
				

				RowMapper rMapper = new RowMapper() {

					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						ChargesVO chargeVO = new ChargesVO();
						chargeVO.setChargeID(rs.getInt("id"));
						chargeVO.setCalMethod(rs.getString("cal_method"));
						chargeVO.setGroupName(rs.getString("group_name"));
						chargeVO.setGroupTypeID(rs.getString("group_type_id"));
						chargeVO.setDebitLedgerID(rs.getInt("debit_ledger_id"));
						chargeVO.setCreditLedgerID(rs.getInt("credit_ledger_id"));
						chargeVO.setAmount(rs.getBigDecimal("amount"));
						chargeVO.setChargeSequence(rs.getInt("charge_sequence"));
						chargeVO.setStartDate(rs.getString("start_date"));
						chargeVO.setEndDate(rs.getString("end_date"));
						chargeVO.setDescription(rs.getString("description"));
						chargeVO.setNextChargeDate(rs.getString("next_charge_date"));
						chargeVO.setTxDate(rs.getString("tx_date"));
						chargeVO.setChargeFrequency(rs.getString("charge_frequency"));
						chargeVO.setCreateInvoices(rs.getInt("generate_invoice"));
						chargeVO.setPrecison(rs.getInt("precision"));
						chargeVO.setItemID(rs.getInt("item_id"));
						chargeVO.setGstRate(rs.getBigDecimal("gst"));
						chargeVO.setSocGstIN(rs.getString("gstin"));
						chargeVO.setIsApplicable(false);
						
						return chargeVO;
					}
				};
				chargeList = namedParameterJdbcTemplate.query(sql, hashMap,rMapper);

			} catch (Exception e) {
				logger.error("Exception at List getApplicableCharges(int societyID,String fromDate,String toDate,int chargeTypeID) " + e);
			}
			logger.debug("Exit : public List List getApplicableCharges(int societyID,String fromDate,String toDate,int chargeTypeID))"+chargeList.size());
			return chargeList;
		}
	 
	/**
	 * @param namedParameterJdbcTemplate the namedParameterJdbcTemplate to set
	 */
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	/**
	 * @param societyServiceBean the societyServiceBean to set
	 */
	public void setSocietyServiceBean(SocietyService societyServiceBean) {
		this.societyServiceBean = societyServiceBean;
	}
	
}
